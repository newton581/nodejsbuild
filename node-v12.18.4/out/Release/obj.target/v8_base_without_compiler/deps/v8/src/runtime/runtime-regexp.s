	.file	"runtime-regexp.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5451:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5452:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5452:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5454:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5454:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch16HasNamedCapturesEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch16HasNamedCapturesEv, @function
_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch16HasNamedCapturesEv:
.LFB20226:
	.cfi_startproc
	endbr64
	movzbl	32(%rdi), %eax
	ret
	.cfi_endproc
.LFE20226:
	.size	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch16HasNamedCapturesEv, .-_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch16HasNamedCapturesEv
	.section	.text._ZN2v88internal12_GLOBAL__N_117VectorBackedMatch8GetMatchEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch8GetMatchEv, @function
_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch8GetMatchEv:
.LFB20240:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE20240:
	.size	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch8GetMatchEv, .-_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch8GetMatchEv
	.section	.text._ZN2v88internal12_GLOBAL__N_117VectorBackedMatch16HasNamedCapturesEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch16HasNamedCapturesEv, @function
_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch16HasNamedCapturesEv:
.LFB20243:
	.cfi_startproc
	endbr64
	movzbl	48(%rdi), %eax
	ret
	.cfi_endproc
.LFE20243:
	.size	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch16HasNamedCapturesEv, .-_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch16HasNamedCapturesEv
	.section	.text._ZN2v88internal12_GLOBAL__N_117VectorBackedMatch12CaptureCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch12CaptureCountEv, @function
_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch12CaptureCountEv:
.LFB20244:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	sarq	$3, %rax
	ret
	.cfi_endproc
.LFE20244:
	.size	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch12CaptureCountEv, .-_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch12CaptureCountEv
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS2_6HandleINS2_6StringEEEPNS6_5Match12CaptureStateEEUlS6_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS2_6HandleINS2_6StringEEEPNS6_5Match12CaptureStateEEUlS6_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS2_6HandleINS2_6StringEEEPNS6_5Match12CaptureStateEEUlS6_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation:
.LFB23824:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L10
	cmpl	$3, %edx
	je	.L11
	cmpl	$1, %edx
	je	.L15
.L11:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23824:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS2_6HandleINS2_6StringEEEPNS6_5Match12CaptureStateEEUlS6_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS2_6HandleINS2_6StringEEEPNS6_5Match12CaptureStateEEUlS6_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFN2v88internal6ObjectEiEZNS1_L58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS1_9ArgumentsEPNS1_7IsolateEEUliE_E9_M_invokeERKSt9_Any_dataOi,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFN2v88internal6ObjectEiEZNS1_L58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS1_9ArgumentsEPNS1_7IsolateEEUliE_E9_M_invokeERKSt9_Any_dataOi, @function
_ZNSt17_Function_handlerIFN2v88internal6ObjectEiEZNS1_L58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS1_9ArgumentsEPNS1_7IsolateEEUliE_E9_M_invokeERKSt9_Any_dataOi:
.LFB23864:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movslq	(%rsi), %rdx
	movq	(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE23864:
	.size	_ZNSt17_Function_handlerIFN2v88internal6ObjectEiEZNS1_L58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS1_9ArgumentsEPNS1_7IsolateEEUliE_E9_M_invokeERKSt9_Any_dataOi, .-_ZNSt17_Function_handlerIFN2v88internal6ObjectEiEZNS1_L58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS1_9ArgumentsEPNS1_7IsolateEEUliE_E9_M_invokeERKSt9_Any_dataOi
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internalL58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS2_9ArgumentsEPNS2_7IsolateEEUliE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internalL58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS2_9ArgumentsEPNS2_7IsolateEEUliE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internalL58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS2_9ArgumentsEPNS2_7IsolateEEUliE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation:
.LFB23865:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L18
	cmpl	$3, %edx
	je	.L19
	cmpl	$1, %edx
	je	.L23
.L19:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23865:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internalL58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS2_9ArgumentsEPNS2_7IsolateEEUliE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internalL58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS2_9ArgumentsEPNS2_7IsolateEEUliE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKhEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKhEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKhEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKhEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKhEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation:
.LFB24688:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L25
	cmpl	$3, %edx
	je	.L26
	cmpl	$1, %edx
	je	.L30
.L26:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE24688:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKhEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKhEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKtEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKtEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKtEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKtEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKtEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation:
.LFB24692:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L32
	cmpl	$3, %edx
	je	.L33
	cmpl	$1, %edx
	je	.L37
.L33:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE24692:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKtEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKtEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation
	.section	.text._ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi,comdat
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi:
.LFB24715:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE24715:
	.size	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEENS2_6ObjectEPNS2_7IsolateENS2_6HandleINS2_6StringEEENS8_INS2_8JSRegExpEEENS8_INS2_15RegExpMatchInfoEEENS8_INS2_7JSArrayEEEEUliE_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEENS2_6ObjectEPNS2_7IsolateENS2_6HandleINS2_6StringEEENS8_INS2_8JSRegExpEEENS8_INS2_15RegExpMatchInfoEEENS8_INS2_7JSArrayEEEEUliE_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEENS2_6ObjectEPNS2_7IsolateENS2_6HandleINS2_6StringEEENS8_INS2_8JSRegExpEEENS8_INS2_15RegExpMatchInfoEEENS8_INS2_7JSArrayEEEEUliE_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation:
.LFB24779:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L40
	cmpl	$3, %edx
	je	.L41
	cmpl	$1, %edx
	je	.L45
.L41:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE24779:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEENS2_6ObjectEPNS2_7IsolateENS2_6HandleINS2_6StringEEENS8_INS2_8JSRegExpEEENS8_INS2_15RegExpMatchInfoEEENS8_INS2_7JSArrayEEEEUliE_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEENS2_6ObjectEPNS2_7IsolateENS2_6HandleINS2_6StringEEENS8_INS2_8JSRegExpEEENS8_INS2_15RegExpMatchInfoEEENS8_INS2_7JSArrayEEEEUliE_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation
	.section	.text._ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi:
.LFB25459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r11
	movq	(%rdi), %r14
	movq	8(%rdi), %r13
	movslq	32(%rdi), %r8
	leal	-1(%r11), %edi
	subl	%r11d, %edx
	leaq	42372(%r14), %r10
	addq	$43396, %r14
	movslq	%edi, %r9
	movzbl	0(%r13,%r9), %r9d
	cmpl	%edx, %ecx
	jg	.L59
	movl	%r11d, %ecx
	movzbl	%r9b, %r11d
	movq	%r8, %r15
	leaq	(%r10,%r11,4), %rbx
	subl	$2, %ecx
	movq	%rbx, -56(%rbp)
	movslq	%ecx, %rbx
	movl	$1, %ecx
	subq	%r8, %rcx
	movq	%rbx, -48(%rbp)
	movq	%rcx, -64(%rbp)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L63:
	movl	%edi, %ebx
	subl	(%r10,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L59
.L50:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rcx), %ecx
	cmpb	%r9b, %cl
	jne	.L63
	testl	%edi, %edi
	js	.L46
	movslq	%eax, %r12
	movq	-48(%rbp), %rcx
	addq	%rsi, %r12
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r8, %rcx
.L51:
	movl	%ecx, %ebx
	testl	%ecx, %ecx
	js	.L46
	movzbl	(%rcx,%r12), %r11d
	leaq	-1(%rcx), %r8
	cmpb	%r11b, 0(%r13,%rcx)
	je	.L64
	cmpl	%ebx, %r15d
	jle	.L65
	movq	-56(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jge	.L50
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$-1, %eax
.L46:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	addq	-64(%rbp), %rcx
	subl	(%r10,%r11,4), %ebx
	cmpl	%ebx, (%r14,%rcx,4)
	cmovge	(%r14,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jge	.L50
	jmp	.L59
	.cfi_endproc
.LFE25459:
	.size	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi:
.LFB25464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r11
	movq	8(%rdi), %r14
	movslq	32(%rdi), %r10
	movq	(%rdi), %rdi
	subl	%r11d, %edx
	leaq	42372(%rdi), %r9
	addq	$43396, %rdi
	movq	%rdi, -56(%rbp)
	leal	-1(%r11), %edi
	movslq	%edi, %r8
	movzwl	(%r14,%r8,2), %r8d
	cmpl	%edx, %ecx
	jg	.L79
	movl	%r11d, %ecx
	movzwl	%r8w, %r11d
	movq	%r10, %r15
	leaq	(%r9,%r11,4), %rbx
	subl	$2, %ecx
	movq	%rbx, -64(%rbp)
	movslq	%ecx, %rbx
	movl	$1, %ecx
	subq	%r10, %rcx
	movq	%rbx, -48(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L83:
	movl	%edi, %ebx
	subl	(%r9,%r10,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L79
.L70:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzbl	(%rsi,%rcx), %r10d
	cmpw	%r10w, %r8w
	jne	.L83
	testl	%edi, %edi
	js	.L66
	movslq	%eax, %r13
	movq	-48(%rbp), %rcx
	addq	%rsi, %r13
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%rbx, %rcx
.L71:
	movl	%ecx, %r12d
	testl	%ecx, %ecx
	js	.L66
	movzbl	0(%r13,%rcx), %r11d
	leaq	-1(%rcx), %rbx
	movq	%r11, %r10
	cmpw	%r11w, (%r14,%rcx,2)
	je	.L84
	cmpl	%r12d, %r15d
	jle	.L85
	movq	-64(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jge	.L70
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$-1, %eax
.L66:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	-56(%rbp), %rbx
	addq	-72(%rbp), %rcx
	subl	(%r9,%r10,4), %r12d
	cmpl	%r12d, (%rbx,%rcx,4)
	cmovge	(%rbx,%rcx,4), %r12d
	addl	%r12d, %eax
	cmpl	%eax, %edx
	jge	.L70
	jmp	.L79
	.cfi_endproc
.LFE25464:
	.size	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi:
.LFB25470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rcx
	movq	16(%rdi), %r10
	movq	8(%rdi), %r15
	movl	32(%rdi), %ebx
	leaq	43396(%rcx), %rdi
	subl	%r10d, %edx
	movl	%r10d, -84(%rbp)
	movq	%rdi, -64(%rbp)
	leal	-1(%r10), %edi
	movslq	%edi, %r8
	movl	%ebx, -44(%rbp)
	movzbl	(%r15,%r8), %r8d
	cmpl	%edx, %eax
	jg	.L103
	movl	%r10d, %r14d
	leaq	42372(%rcx), %r9
	movzbl	%r8b, %r10d
	movslq	%ebx, %rcx
	leaq	(%r9,%r10,4), %rbx
	leal	-2(%r14), %r10d
	movq	%rbx, -72(%rbp)
	movslq	%r10d, %rbx
	movl	$1, %r10d
	subq	%rcx, %r10
	movq	%rbx, -56(%rbp)
	movq	%r10, -80(%rbp)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L108:
	cmpw	$255, %cx
	ja	.L90
	movl	%edi, %ebx
	subl	(%r9,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L103
.L92:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	cmpw	%r8w, %cx
	jne	.L108
	testl	%edi, %edi
	js	.L86
	movslq	%eax, %r10
	movl	%edi, -48(%rbp)
	movq	-56(%rbp), %rcx
	leaq	(%rsi,%r10,2), %r12
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%rbx, %rcx
.L93:
	leal	1(%rcx), %r14d
	movl	%ecx, %r13d
	testl	%ecx, %ecx
	js	.L86
	movzwl	(%r12,%rcx,2), %edi
	movzbl	(%r15,%rcx), %r11d
	leaq	-1(%rcx), %rbx
	movq	%rdi, %r10
	cmpl	%edi, %r11d
	je	.L109
	movl	-48(%rbp), %edi
	cmpl	%r13d, -44(%rbp)
	jg	.L98
	movq	-64(%rbp), %rbx
	addq	-80(%rbp), %rcx
	movl	(%rbx,%rcx,4), %ecx
	cmpw	$255, %r10w
	ja	.L97
	movl	%r13d, %r14d
	subl	(%r9,%r10,4), %r14d
.L97:
	cmpl	%r14d, %ecx
	cmovl	%r14d, %ecx
	addl	%ecx, %eax
.L107:
	cmpl	%edx, %eax
	jle	.L92
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$-1, %eax
.L86:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	-72(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	jmp	.L107
.L90:
	addl	-84(%rbp), %eax
	jmp	.L107
	.cfi_endproc
.LFE25470:
	.size	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi:
.LFB25475:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r11
	movq	(%rdi), %r14
	movq	8(%rdi), %r12
	movslq	32(%rdi), %r8
	leal	-1(%r11), %edi
	subl	%r11d, %edx
	leaq	42372(%r14), %r10
	addq	$43396, %r14
	movslq	%edi, %r9
	movzwl	(%r12,%r9,2), %r9d
	cmpl	%edx, %ecx
	jg	.L123
	movl	%r11d, %ecx
	movzbl	%r9b, %r11d
	movq	%r8, %r15
	leaq	(%r10,%r11,4), %rbx
	subl	$2, %ecx
	movq	%rbx, -56(%rbp)
	movslq	%ecx, %rbx
	movl	$1, %ecx
	subq	%r8, %rcx
	movq	%rbx, -48(%rbp)
	movq	%rcx, -64(%rbp)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L127:
	movzbl	%cl, %ecx
	movl	%edi, %ebx
	subl	(%r10,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jl	.L123
.L114:
	leal	(%rdi,%rax), %ecx
	movslq	%ecx, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	cmpw	%r9w, %cx
	jne	.L127
	testl	%edi, %edi
	js	.L110
	movslq	%eax, %r8
	movq	-48(%rbp), %rcx
	leaq	(%rsi,%r8,2), %r13
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r8, %rcx
.L115:
	movl	%ecx, %ebx
	testl	%ecx, %ecx
	js	.L110
	leaq	-1(%rcx), %r8
	movzwl	2(%r13,%r8,2), %r11d
	cmpw	%r11w, (%r12,%rcx,2)
	je	.L128
	cmpl	%ebx, %r15d
	jle	.L129
	movq	-56(%rbp), %rbx
	movl	%edi, %ecx
	subl	(%rbx), %ecx
	addl	%ecx, %eax
	cmpl	%eax, %edx
	jge	.L114
	.p2align 4,,10
	.p2align 3
.L123:
	movl	$-1, %eax
.L110:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	addq	-64(%rbp), %rcx
	movzbl	%r11b, %r11d
	subl	(%r10,%r11,4), %ebx
	cmpl	%ebx, (%r14,%rcx,4)
	cmovge	(%r14,%rcx,4), %ebx
	addl	%ebx, %eax
	cmpl	%eax, %edx
	jge	.L114
	jmp	.L123
	.cfi_endproc
.LFE25475:
	.size	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD2Ev, @function
_ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD2Ev:
.LFB25798:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25798:
	.size	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD2Ev, .-_ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD1Ev,_ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD2Ev, @function
_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD2Ev:
.LFB25802:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25802:
	.size	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD2Ev, .-_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD1Ev,_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD2Ev
	.section	.text._ZNSt17_Function_handlerIFN2v88internal6ObjectEiEZNS1_12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEES2_PNS1_7IsolateENS1_6HandleINS1_6StringEEENS8_INS1_8JSRegExpEEENS8_INS1_15RegExpMatchInfoEEENS8_INS1_7JSArrayEEEEUliE_E9_M_invokeERKSt9_Any_dataOi,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFN2v88internal6ObjectEiEZNS1_12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEES2_PNS1_7IsolateENS1_6HandleINS1_6StringEEENS8_INS1_8JSRegExpEEENS8_INS1_15RegExpMatchInfoEEENS8_INS1_7JSArrayEEEEUliE_E9_M_invokeERKSt9_Any_dataOi, @function
_ZNSt17_Function_handlerIFN2v88internal6ObjectEiEZNS1_12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEES2_PNS1_7IsolateENS1_6HandleINS1_6StringEEENS8_INS1_8JSRegExpEEENS8_INS1_15RegExpMatchInfoEEENS8_INS1_7JSArrayEEEEUliE_E9_M_invokeERKSt9_Any_dataOi:
.LFB24778:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movl	(%rsi), %eax
	movq	(%rdx), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	ret
	.cfi_endproc
.LFE24778:
	.size	_ZNSt17_Function_handlerIFN2v88internal6ObjectEiEZNS1_12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEES2_PNS1_7IsolateENS1_6HandleINS1_6StringEEENS8_INS1_8JSRegExpEEENS8_INS1_15RegExpMatchInfoEEENS8_INS1_7JSArrayEEEEUliE_E9_M_invokeERKSt9_Any_dataOi, .-_ZNSt17_Function_handlerIFN2v88internal6ObjectEiEZNS1_12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEES2_PNS1_7IsolateENS1_6HandleINS1_6StringEEENS8_INS1_8JSRegExpEEENS8_INS1_15RegExpMatchInfoEEENS8_INS1_7JSArrayEEEEUliE_E9_M_invokeERKSt9_Any_dataOi
	.section	.text._ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch12CaptureCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch12CaptureCountEv, @function
_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch12CaptureCountEv:
.LFB20227:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rax
	movq	%rax, %rdx
	shrq	$63, %rax
	sarq	$32, %rdx
	addl	%edx, %eax
	sarl	%eax
	ret
	.cfi_endproc
.LFE20227:
	.size	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch12CaptureCountEv, .-_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch12CaptureCountEv
	.section	.text._ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKhEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"axG",@progbits,_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKhEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKhEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.type	_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKhEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKhEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB24687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rdi), %rsi
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal6String9IsEqualToIhEEbNS0_6VectorIKT_EE@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L137
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L137:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24687:
	.size	_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKhEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKhEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN2v88internal12_GLOBAL__N_118LookupNamedCaptureERKSt8functionIFbNS0_6StringEEENS0_10FixedArrayE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118LookupNamedCaptureERKSt8functionIFbNS0_6StringEEENS0_10FixedArrayE, @function
_ZN2v88internal12_GLOBAL__N_118LookupNamedCaptureERKSt8functionIFbNS0_6StringEEENS0_10FixedArrayE:
.LFB20172:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	xorl	%r13d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	leaq	15(%rsi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movslq	11(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1(%rsi), %rax
	movq	%rax, -72(%rbp)
	sarl	%r14d
.L141:
	cmpl	%r14d, %r13d
	jge	.L143
	movq	(%r12), %rax
	cmpq	$0, 16(%rbx)
	leal	1(%r13), %r15d
	movq	%rax, -64(%rbp)
	je	.L146
	leaq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	addq	$16, %r12
	call	*24(%rbx)
	testb	%al, %al
	je	.L144
	leal	3(%r13,%r13), %eax
	movq	-72(%rbp), %rdx
	sall	$3, %eax
	cltq
	movq	(%rax,%rdx), %rax
	shrq	$32, %rax
.L138:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L147
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movl	%r15d, %r13d
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L143:
	movl	$-1, %eax
	jmp	.L138
.L146:
	call	_ZSt25__throw_bad_function_callv@PLT
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20172:
	.size	_ZN2v88internal12_GLOBAL__N_118LookupNamedCaptureERKSt8functionIFbNS0_6StringEEENS0_10FixedArrayE, .-_ZN2v88internal12_GLOBAL__N_118LookupNamedCaptureERKSt8functionIFbNS0_6StringEEENS0_10FixedArrayE
	.section	.text._ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKtEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"axG",@progbits,_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKtEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKtEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.type	_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKtEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKtEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB24691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rdi), %rsi
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal6String9IsEqualToItEEbNS0_6VectorIKT_EE@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L151
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L151:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24691:
	.size	_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKtEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKtEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi:
.LFB24712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r13d
	leal	1(%r13), %ebx
	movzbl	(%rax), %r15d
	movl	%r15d, %r14d
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L158:
	subq	%r12, %rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpb	(%r12,%rdx), %r14b
	je	.L152
	leal	1(%rax), %ecx
	cmpl	%eax, %r13d
	jle	.L155
.L154:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx), %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L158
.L155:
	movl	$-1, %r8d
.L152:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24712:
	.size	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi:
.LFB24716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movzwl	(%rax), %r15d
	cmpw	$255, %r15w
	ja	.L163
	movl	%edx, %r14d
	subl	16(%rdi), %r14d
	movq	%rsi, %r13
	movl	%r15d, %ebx
	leal	1(%r14), %r12d
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L167:
	subq	%r13, %rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpb	0(%r13,%rdx), %bl
	je	.L159
	leal	1(%rax), %ecx
	cmpl	%eax, %r14d
	jle	.L163
.L162:
	movl	%r12d, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	0(%r13,%rcx), %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L167
.L163:
	movl	$-1, %r8d
.L159:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24716:
	.size	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi:
.LFB24720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r14d
	leal	1(%r14), %ebx
	movzbl	(%rax), %r15d
	movl	%r15d, %r13d
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L174:
	andq	$-2, %rax
	subq	%r12, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpw	(%r12,%rdx,2), %r13w
	je	.L168
	leal	1(%rax), %ecx
	cmpl	%eax, %r14d
	jle	.L171
.L170:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L174
.L171:
	movl	$-1, %r8d
.L168:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24720:
	.size	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi:
.LFB24724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	subl	16(%rdi), %r14d
	leal	1(%r14), %ebx
	movzwl	(%rax), %r13d
	movl	%r13d, %eax
	movzbl	%ah, %eax
	cmpb	%al, %r13b
	movl	%eax, %r15d
	cmova	%r13d, %r15d
	movzbl	%r15b, %r15d
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L181:
	andq	$-2, %rax
	subq	%r12, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	cmpw	(%r12,%rdx,2), %r13w
	je	.L175
	leal	1(%rax), %ecx
	cmpl	%eax, %r14d
	jle	.L178
.L177:
	movl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r12,%rcx,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L181
.L178:
	movl	$-1, %r8d
.L175:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24724:
	.size	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12_GLOBAL__N_122NewJSArrayWithElementsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_122NewJSArrayWithElementsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi, @function
_ZN2v88internal12_GLOBAL__N_122NewJSArrayWithElementsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi:
.LFB20263:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal10FixedArray13ShrinkOrEmptyEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%rax, %rsi
	movq	(%rax), %rax
	movslq	11(%rax), %rcx
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	.cfi_endproc
.LFE20263:
	.size	_ZN2v88internal12_GLOBAL__N_122NewJSArrayWithElementsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi, .-_ZN2v88internal12_GLOBAL__N_122NewJSArrayWithElementsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi
	.section	.text._ZN2v88internal12_GLOBAL__N_133ConstructNamedCaptureGroupsObjectEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEERKSt8functionIFNS0_6ObjectEiEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_133ConstructNamedCaptureGroupsObjectEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEERKSt8functionIFNS0_6ObjectEiEE, @function
_ZN2v88internal12_GLOBAL__N_133ConstructNamedCaptureGroupsObjectEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEERKSt8functionIFNS0_6ObjectEiEE:
.LFB20247:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	%rax, -72(%rbp)
	movq	0(%r13), %rax
	movslq	11(%rax), %rdx
	sarl	%edx
	testl	%edx, %edx
	jle	.L185
	subl	$1, %edx
	movl	$16, %ebx
	addq	$2, %rdx
	salq	$4, %rdx
	movq	%rdx, -80(%rbp)
	leaq	-60(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L197:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L187:
	movq	0(%r13), %rax
	movq	7(%rbx,%rax), %rax
	sarq	$32, %rax
	cmpq	$0, 16(%r12)
	movl	%eax, -60(%rbp)
	je	.L196
	movq	-88(%rbp), %rsi
	movq	%r12, %rdi
	call	*24(%r12)
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L190
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L191:
	movq	-72(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	movq	%r15, %rdi
	addq	$16, %rbx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	cmpq	%rbx, -80(%rbp)
	je	.L185
	movq	0(%r13), %rax
.L193:
	movq	-1(%rbx,%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L197
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L198
.L188:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L190:
	movq	41088(%r15), %rcx
	cmpq	41096(%r15), %rcx
	je	.L199
.L192:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L198:
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L185:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	movq	-72(%rbp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L196:
	.cfi_restore_state
	call	_ZSt25__throw_bad_function_callv@PLT
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20247:
	.size	_ZN2v88internal12_GLOBAL__N_133ConstructNamedCaptureGroupsObjectEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEERKSt8functionIFNS0_6ObjectEiEE, .-_ZN2v88internal12_GLOBAL__N_133ConstructNamedCaptureGroupsObjectEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEERKSt8functionIFNS0_6ObjectEiEE
	.section	.text._ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch8GetMatchEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch8GetMatchEv, @function
_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch8GetMatchEv:
.LFB20223:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rsi
	movq	8(%rdi), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	.cfi_endproc
.LFE20223:
	.size	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch8GetMatchEv, .-_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch8GetMatchEv
	.section	.text._ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD0Ev, @function
_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD0Ev:
.LFB25804:
	.cfi_startproc
	endbr64
	movl	$48, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25804:
	.size	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD0Ev, .-_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD0Ev, @function
_ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD0Ev:
.LFB25800:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25800:
	.size	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD0Ev, .-_ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD0Ev
	.section	.rodata._ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NewArray"
	.section	.text._ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0, @function
_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0:
.LFB26070:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$16, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L205
.L207:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L205:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$16, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L207
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE26070:
	.size	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0, .-_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0
	.section	.text._ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS1_6HandleIS2_EEPNS2_5Match12CaptureStateEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS1_6HandleIS2_EEPNS2_5Match12CaptureStateEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS1_6HandleIS2_EEPNS2_5Match12CaptureStateEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB23823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rdx, -16(%rbp)
	movq	(%rax), %rsi
	movl	$1, %eax
	cmpq	%rdx, %rsi
	je	.L211
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L213
.L214:
	leaq	-16(%rbp), %rdi
	call	_ZN2v88internal6String10SlowEqualsES1_@PLT
.L211:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L218
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L214
	xorl	%eax, %eax
	jmp	.L211
.L218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23823:
	.size	_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS1_6HandleIS2_EEPNS2_5Match12CaptureStateEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS1_6HandleIS2_EEPNS2_5Match12CaptureStateEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi:
.LFB24713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movl	%ecx, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	8(%rax), %r12
	movq	16(%rax), %rax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L224
	movl	%eax, %ecx
	movzbl	(%r12), %r14d
	leal	1(%rbx), %eax
	movq	%rsi, %r15
	movl	%eax, -52(%rbp)
	leal	-1(%rcx), %eax
	movl	%eax, -56(%rbp)
	movl	%r14d, %r13d
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L238:
	subq	%r15, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r15, %rdx
	cmpb	(%rdx), %r13b
	je	.L225
	leal	1(%rax), %edi
	cmpl	%eax, %ebx
	jle	.L224
.L226:
	movl	-52(%rbp), %edx
	movl	%r14d, %esi
	subl	%edi, %edx
	movslq	%edi, %rdi
	addq	%r15, %rdi
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L238
.L224:
	movl	$-1, %r9d
.L219:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L224
	leal	1(%rax), %edi
	xorl	%eax, %eax
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L239:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L219
.L228:
	movzbl	1(%rdx,%rax), %ecx
	cmpb	%cl, 1(%r12,%rax)
	je	.L239
	cmpl	%edi, %ebx
	jge	.L226
	jmp	.L224
	.cfi_endproc
.LFE24713:
	.size	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi:
.LFB24717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	movl	%ecx, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	8(%rax), %r13
	movq	16(%rax), %rax
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L245
	movl	%eax, %ecx
	movzwl	0(%r13), %eax
	movq	%rsi, %r15
	leal	1(%rbx), %esi
	movl	%esi, -52(%rbp)
	movzbl	%ah, %edx
	movl	%eax, %r14d
	cmpb	%dl, %al
	cmovbe	%edx, %eax
	movzbl	%al, %r12d
	leal	-1(%rcx), %eax
	movl	%eax, -56(%rbp)
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L259:
	subq	%r15, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r15, %rdx
	cmpb	%r14b, (%rdx)
	je	.L246
	leal	1(%rax), %edi
	cmpl	%eax, %ebx
	jle	.L245
.L247:
	movl	-52(%rbp), %edx
	movl	%r12d, %esi
	subl	%edi, %edx
	movslq	%edi, %rdi
	addq	%r15, %rdi
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L259
.L245:
	movl	$-1, %r9d
.L240:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L245
	leal	1(%rax), %edi
	xorl	%eax, %eax
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L260:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L240
.L249:
	movzbl	1(%rdx,%rax), %ecx
	cmpw	%cx, 2(%r13,%rax,2)
	je	.L260
	cmpl	%edi, %ebx
	jge	.L247
	jmp	.L245
	.cfi_endproc
.LFE24717:
	.size	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi:
.LFB24721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %r13
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L266
	movzbl	0(%r13), %r14d
	movq	%rsi, %r15
	movl	%eax, %esi
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	leal	-1(%rsi), %eax
	movl	%eax, -56(%rbp)
	movl	%r14d, %r12d
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L280:
	andq	$-2, %rax
	subq	%r15, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	leaq	(%r15,%rdx,2), %rdx
	cmpw	%r12w, (%rdx)
	je	.L267
	leal	1(%rax), %ecx
	cmpl	%eax, %ebx
	jle	.L266
.L268:
	movl	-52(%rbp), %edx
	movl	%r14d, %esi
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	movslq	%edx, %rdx
	leaq	(%r15,%rcx,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L280
.L266:
	movl	$-1, %r9d
.L261:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L266
	leal	1(%rax), %ecx
	xorl	%eax, %eax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L281:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L261
.L270:
	movzbl	1(%r13,%rax), %edi
	movzwl	2(%rdx,%rax,2), %esi
	cmpl	%esi, %edi
	je	.L281
	cmpl	%ecx, %ebx
	jge	.L268
	jmp	.L266
	.cfi_endproc
.LFE24721:
	.size	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi:
.LFB24725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %r14
	subl	%eax, %ebx
	cmpl	%ebx, %ecx
	jg	.L287
	movzwl	(%r14), %r13d
	movl	%eax, %edi
	leal	1(%rbx), %eax
	movq	%rsi, %r15
	movl	%eax, -52(%rbp)
	movslq	%ecx, %rsi
	movl	%r13d, %eax
	movzbl	%ah, %eax
	cmpb	%al, %r13b
	cmova	%r13d, %eax
	movzbl	%al, %r12d
	leal	-1(%rdi), %eax
	movl	%eax, -56(%rbp)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L301:
	andq	$-2, %rax
	subq	%r15, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	cmpw	%r13w, (%r15,%rdx,2)
	je	.L288
	leal	1(%rax), %ecx
	cmpl	%eax, %ebx
	jle	.L287
	movslq	%ecx, %rsi
.L289:
	movl	-52(%rbp), %edx
	leaq	(%r15,%rsi,2), %rdi
	movl	%r12d, %esi
	subl	%ecx, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L301
.L287:
	movl	$-1, %r9d
.L282:
	addq	$24, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L287
	leal	1(%rax), %ecx
	xorl	%eax, %eax
	movslq	%ecx, %rsi
	leaq	(%r15,%rsi,2), %rdx
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L302:
	addq	$1, %rax
	cmpl	%eax, -56(%rbp)
	jle	.L282
.L291:
	movzwl	(%rdx,%rax,2), %edi
	cmpw	%di, 2(%r14,%rax,2)
	je	.L302
	cmpl	%ecx, %ebx
	jge	.L289
	jmp	.L287
	.cfi_endproc
.LFE24725:
	.size	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12_GLOBAL__N_117VectorBackedMatch9GetPrefixEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch9GetPrefixEv, @function
_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch9GetPrefixEv:
.LFB20241:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movl	32(%rdi), %ecx
	movq	(%rax), %rdx
	cmpl	11(%rdx), %ecx
	je	.L307
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE20241:
	.size	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch9GetPrefixEv, .-_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch9GetPrefixEv
	.section	.text._ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch9GetPrefixEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch9GetPrefixEv, @function
_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch9GetPrefixEv:
.LFB20224:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	movq	39(%rax), %rcx
	movq	16(%rdi), %rax
	movq	(%rax), %rdx
	sarq	$32, %rcx
	cmpl	11(%rdx), %ecx
	je	.L314
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE20224:
	.size	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch9GetPrefixEv, .-_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch9GetPrefixEv
	.section	.text._ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch9GetSuffixEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch9GetSuffixEv, @function
_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch9GetSuffixEv:
.LFB20225:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	movq	47(%rax), %rdx
	movq	16(%rdi), %rsi
	movq	8(%rdi), %r8
	movq	(%rsi), %rax
	sarq	$32, %rdx
	movl	11(%rax), %ecx
	jne	.L321
	movq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	movq	%r8, %rdi
	jmp	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	.cfi_endproc
.LFE20225:
	.size	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch9GetSuffixEv, .-_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch9GetSuffixEv
	.section	.text._ZN2v88internal12_GLOBAL__N_117VectorBackedMatch9GetSuffixEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch9GetSuffixEv, @function
_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch9GetSuffixEv:
.LFB20242:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	16(%rdi), %rsi
	movl	32(%rdi), %edx
	movq	8(%rdi), %r8
	movq	(%rsi), %rcx
	movq	(%rax), %rax
	movl	11(%rcx), %ecx
	addl	11(%rax), %edx
	jne	.L326
	movq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	movq	%r8, %rdi
	jmp	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	.cfi_endproc
.LFE20242:
	.size	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch9GetSuffixEv, .-_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch9GetSuffixEv
	.section	.text._ZN2v88internal12_GLOBAL__N_18ToUint32EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPj,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_18ToUint32EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPj, @function
_ZN2v88internal12_GLOBAL__N_18ToUint32EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPj:
.LFB20262:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	(%rsi), %rax
	cmpq	%rax, 88(%rdi)
	jne	.L328
	movl	$-1, (%rdx)
	movq	%rsi, %rax
.L329:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	testb	$1, %al
	jne	.L353
.L331:
	shrq	$32, %rax
.L336:
	movl	%eax, (%r12)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L332
	xorl	%edx, %edx
.L333:
	testb	%dl, %dl
	jne	.L331
	movq	7(%rax), %rsi
	movsd	.LC2(%rip), %xmm2
	movq	%rsi, %xmm1
	andpd	.LC1(%rip), %xmm1
	movq	%rsi, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L337
	movsd	.LC3(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L337
	comisd	.LC4(%rip), %xmm0
	jnb	.L354
	.p2align 4,,10
	.p2align 3
.L337:
	movabsq	$9218868437227405312, %rax
	testq	%rax, %rsi
	je	.L345
	movq	%rsi, %rdi
	xorl	%eax, %eax
	shrq	$52, %rdi
	andl	$2047, %edi
	movl	%edi, %ecx
	subl	$1075, %ecx
	js	.L355
	cmpl	$31, %ecx
	jg	.L336
	movabsq	$4503599627370495, %rax
	andq	%rsi, %rax
	movq	%rax, %rdx
	movabsq	$4503599627370496, %rax
	addq	%rdx, %rax
	salq	%cl, %rax
	movl	%eax, %eax
.L343:
	sarq	$63, %rsi
	orl	$1, %esi
	imull	%esi, %eax
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L332:
	xorl	%edx, %edx
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	je	.L329
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L355:
	cmpl	$-52, %ecx
	jl	.L336
	movabsq	$4503599627370495, %rax
	movl	$1075, %ecx
	andq	%rsi, %rax
	subl	%edi, %ecx
	movq	%rax, %rdx
	movabsq	$4503599627370496, %rax
	addq	%rdx, %rax
	shrq	%cl, %rax
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L354:
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L337
	jne	.L337
	movl	%edx, %eax
	jmp	.L336
.L345:
	xorl	%eax, %eax
	jmp	.L336
	.cfi_endproc
.LFE20262:
	.size	_ZN2v88internal12_GLOBAL__N_18ToUint32EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPj, .-_ZN2v88internal12_GLOBAL__N_18ToUint32EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPj
	.section	.rodata._ZN2v88internal12_GLOBAL__N_117VectorBackedMatch10GetCaptureEiPb.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal12_GLOBAL__N_117VectorBackedMatch10GetCaptureEiPb,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch10GetCaptureEiPb, @function
_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch10GetCaptureEiPb:
.LFB20245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	40(%rdi), %rax
	movslq	%esi, %rsi
	movq	8(%rax), %rcx
	movq	16(%rax), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	%rcx, %r8
	sarq	$3, %r8
	cmpq	%r8, %rsi
	jnb	.L364
	movq	(%rcx,%rsi,8), %rsi
	movq	8(%rdi), %rax
	movq	(%rsi), %rcx
	cmpq	%rcx, 88(%rax)
	jne	.L358
	movb	$0, (%rdx)
	movq	8(%rdi), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	128(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	movb	$1, (%rdx)
	movq	(%rsi), %rax
	movq	8(%rdi), %rdi
	testb	$1, %al
	jne	.L360
.L362:
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
.L361:
	movq	%rsi, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L362
	jmp	.L361
.L364:
	movq	%r8, %rdx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE20245:
	.size	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch10GetCaptureEiPb, .-_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch10GetCaptureEiPb
	.section	.text._ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch10GetCaptureEiPb,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch10GetCaptureEiPb, @function
_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch10GetCaptureEiPb:
.LFB20228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	movl	%r8d, %edx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rsi
	movq	8(%rdi), %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	cmpb	$0, (%r12)
	jne	.L372
	movq	8(%rbx), %rax
	subq	$-128, %rax
.L370:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	movq	(%rax), %rdx
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	testb	$1, %dl
	jne	.L367
.L369:
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L369
	jmp	.L370
	.cfi_endproc
.LFE20228:
	.size	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch10GetCaptureEiPb, .-_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch10GetCaptureEiPb
	.section	.text._ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS4_5Match12CaptureStateE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS4_5Match12CaptureStateE, @function
_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS4_5Match12CaptureStateE:
.LFB20229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS2_6HandleINS2_6StringEEEPNS6_5Match12CaptureStateEEUlS6_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	%r15, %rdi
	movq	(%rax), %rsi
	leaq	_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS1_6HandleIS2_EEPNS2_5Match12CaptureStateEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rbx, -96(%rbp)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_118LookupNamedCaptureERKSt8functionIFbNS0_6StringEEENS0_10FixedArrayE
	movl	%eax, %r13d
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L374
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L374:
	cmpl	$-1, %r13d
	jne	.L375
	movl	$0, (%r14)
	movq	%rbx, %rax
.L376:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L389
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	movq	24(%r12), %rsi
	movq	8(%r12), %rdi
	leaq	-97(%rbp), %rcx
	movl	%r13d, %edx
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	cmpb	$0, -97(%rbp)
	jne	.L390
.L377:
	movl	$1, (%r14)
	movq	8(%r12), %rax
	subq	$-128, %rax
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L390:
	movq	(%rax), %rcx
	movq	8(%r12), %rdi
	movq	%rax, %rdx
	testb	$1, %cl
	jne	.L378
.L381:
	movq	%rax, %rsi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L376
.L380:
	cmpb	$0, -97(%rbp)
	je	.L377
	movl	$2, (%r14)
	movq	%rdx, %rax
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L378:
	movq	-1(%rcx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L381
	jmp	.L380
.L389:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20229:
	.size	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS4_5Match12CaptureStateE, .-_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS4_5Match12CaptureStateE
	.section	.text._ZN2v88internal12_GLOBAL__N_117VectorBackedMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS4_5Match12CaptureStateE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS4_5Match12CaptureStateE, @function
_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS4_5Match12CaptureStateE:
.LFB20246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	56(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r15
	movq	(%rsi), %rax
	movq	-1(%rax), %rcx
	subq	$37592, %r15
	cmpw	$63, 11(%rcx)
	jbe	.L433
.L393:
	movq	(%r12), %rsi
	movl	$3, %ecx
	movq	-1(%rsi), %rdi
	cmpw	$64, 11(%rdi)
	je	.L434
.L397:
	movabsq	$824633720832, %rsi
	movl	%ecx, -224(%rbp)
	movq	%rsi, -212(%rbp)
	movq	%r15, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %ecx
	movq	%r12, %rax
	andl	$-32, %ecx
	cmpl	$32, %ecx
	je	.L435
.L398:
	leaq	-224(%rbp), %r15
	movq	%rax, -192(%rbp)
	movq	%r15, %rdi
	movq	$0, -184(%rbp)
	movq	%rdx, -176(%rbp)
	movq	$0, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
.L396:
	movq	%r15, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	jne	.L399
.L409:
	xorl	%eax, %eax
.L400:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L436
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L399:
	.cfi_restore_state
	shrw	$8, %ax
	jne	.L401
	movl	$0, 0(%r13)
	movq	%r12, %rax
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L433:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %ecx
	testb	$1, %cl
	jne	.L394
	andl	$2, %ecx
	jne	.L393
.L394:
	leaq	-144(%rbp), %rdi
	leaq	-228(%rbp), %rsi
	movq	%rdx, -256(%rbp)
	movq	%rdi, -248(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-248(%rbp), %rdi
	movq	-256(%rbp), %rdx
	testb	%al, %al
	je	.L437
	movabsq	$824633720832, %rax
	movq	%r15, -120(%rbp)
	leaq	-224(%rbp), %r15
	movq	%rax, -132(%rbp)
	movl	-228(%rbp), %eax
	movl	$3, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L434:
	movl	11(%rsi), %ecx
	andl	$1, %ecx
	cmpb	$1, %cl
	sbbl	%ecx, %ecx
	andl	$3, %ecx
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L401:
	movq	56(%rbx), %r15
	movq	8(%rbx), %rdi
	movq	(%r15), %rax
	testb	$1, %al
	jne	.L402
.L404:
	movl	$-1, %edx
	movq	%r15, %rsi
	movq	%rdi, -248(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-248(%rbp), %rdi
	movq	%rax, %rdx
.L403:
	movq	(%r12), %rcx
	movl	$3, %eax
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L405
	movl	11(%rcx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L405:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	(%r12), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L438
.L406:
	leaq	-144(%rbp), %r12
	movq	%r14, -112(%rbp)
	movq	%r12, %rdi
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L407
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rsi
.L408:
	movq	8(%rbx), %rax
	movq	(%rsi), %rcx
	cmpq	%rcx, 88(%rax)
	jne	.L410
	movl	$1, 0(%r13)
	movq	8(%rbx), %rax
	subq	$-128, %rax
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L435:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-248(%rbp), %rdx
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L437:
	movq	(%r12), %rax
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L407:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L408
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L410:
	movl	$2, 0(%r13)
	movq	(%rsi), %rax
	movq	8(%rbx), %rdi
	testb	$1, %al
	jne	.L411
.L413:
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
.L412:
	movq	%rsi, %rax
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L402:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L404
	movq	%r15, %rdx
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L411:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L413
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L438:
	movq	%r12, %rsi
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-248(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L406
.L436:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20246:
	.size	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS4_5Match12CaptureStateE, .-_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS4_5Match12CaptureStateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L445
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L448
.L439:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L448:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L439
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC7:
	.string	"V8.Runtime_Runtime_IsRegExp"
	.section	.text._ZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateE.isra.0:
.LFB25900:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L481
.L450:
	movq	_ZZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateEE29trace_event_unique_atomic1928(%rip), %rbx
	testq	%rbx, %rbx
	je	.L482
.L452:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L483
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L458
.L460:
	movq	120(%r12), %r12
.L459:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L484
.L449:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L485
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L486
.L453:
	movq	%rbx, _ZZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateEE29trace_event_unique_atomic1928(%rip)
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L483:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L487
.L455:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L456
	movq	(%rdi), %rax
	call	*8(%rax)
.L456:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L457
	movq	(%rdi), %rax
	call	*8(%rax)
.L457:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	0(%r13), %rax
	movq	%r14, -120(%rbp)
	testb	$1, %al
	je	.L460
.L458:
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	jne	.L460
	movq	112(%r12), %r12
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L481:
	movq	40960(%rsi), %rax
	movl	$493, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L484:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L487:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L486:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L453
.L485:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25900:
	.size	_ZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL24Stats_Runtime_RegExpExecEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"V8.Runtime_Runtime_RegExpExec"
.LC9:
	.string	"args[0].IsJSRegExp()"
.LC10:
	.string	"Check failed: %s."
.LC11:
	.string	"args[1].IsString()"
.LC12:
	.string	"args[2].IsNumber()"
.LC13:
	.string	"args[2].ToInt32(&index)"
.LC14:
	.string	"args[3].IsRegExpMatchInfo()"
.LC15:
	.string	"0 <= index"
.LC16:
	.string	"subject->length() >= index"
	.section	.text._ZN2v88internalL24Stats_Runtime_RegExpExecEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_RegExpExecEiPmPNS0_7IsolateE, @function
_ZN2v88internalL24Stats_Runtime_RegExpExecEiPmPNS0_7IsolateE:
.LFB20210:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L539
.L489:
	movq	_ZZN2v88internalL24Stats_Runtime_RegExpExecEiPmPNS0_7IsolateEE28trace_event_unique_atomic882(%rip), %rbx
	testq	%rbx, %rbx
	je	.L540
.L491:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L541
.L493:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L497
.L498:
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L540:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L542
.L492:
	movq	%rbx, _ZZN2v88internalL24Stats_Runtime_RegExpExecEiPmPNS0_7IsolateEE28trace_event_unique_atomic882(%rip)
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L497:
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	jne	.L498
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r15
	testb	$1, %al
	jne	.L543
.L499:
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L541:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L544
.L494:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L495
	movq	(%rdi), %rax
	call	*8(%rax)
.L495:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L496
	movq	(%rdi), %rax
	call	*8(%rax)
.L496:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L543:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L499
	movq	-16(%r13), %rax
	testb	$1, %al
	je	.L502
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L545
.L502:
	leaq	-172(%rbp), %rsi
	leaq	-168(%rbp), %rdi
	movl	$0, -172(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L546
	movq	-24(%r13), %rax
	leaq	-24(%r13), %r8
	testb	$1, %al
	jne	.L547
.L505:
	leaq	.LC14(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L547:
	movq	-1(%rax), %rax
	cmpw	$123, 11(%rax)
	jne	.L505
	movl	-172(%rbp), %eax
	testl	%eax, %eax
	js	.L548
	movq	-8(%r13), %rdx
	cmpl	11(%rdx), %eax
	jg	.L549
	movq	40960(%r12), %rdx
	cmpb	$0, 7136(%rdx)
	je	.L509
	movq	7128(%rdx), %rax
.L510:
	testq	%rax, %rax
	je	.L511
	addl	$1, (%rax)
.L511:
	movl	-172(%rbp), %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6RegExp4ExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE@PLT
	testq	%rax, %rax
	je	.L550
	movq	(%rax), %r13
.L513:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L516
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L516:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L551
.L488:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L552
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L509:
	.cfi_restore_state
	movb	$1, 7136(%rdx)
	leaq	7112(%rdx), %rdi
	movq	%r8, -192(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %r8
	movq	%rax, 7128(%rdx)
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L539:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$494, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L550:
	movq	312(%r12), %r13
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L545:
	leaq	.LC12(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L546:
	leaq	.LC13(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L544:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L542:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L548:
	leaq	.LC15(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L549:
	leaq	.LC16(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L551:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L488
.L552:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20210:
	.size	_ZN2v88internalL24Stats_Runtime_RegExpExecEiPmPNS0_7IsolateE, .-_ZN2v88internalL24Stats_Runtime_RegExpExecEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Runtime_Runtime_RegExpInitializeAndCompile"
	.section	.rodata._ZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC18:
	.string	"args[2].IsString()"
	.section	.text._ZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE.isra.0:
.LFB25975:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L595
.L554:
	movq	_ZZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateEE29trace_event_unique_atomic1912(%rip), %r13
	testq	%r13, %r13
	je	.L596
.L556:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L597
.L558:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L598
.L562:
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L596:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L599
.L557:
	movq	%r13, _ZZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateEE29trace_event_unique_atomic1912(%rip)
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L598:
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	jne	.L562
	movq	-8(%rbx), %rax
	leaq	-8(%rbx), %rsi
	testb	$1, %al
	jne	.L600
.L563:
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L597:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L601
.L559:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L560
	movq	(%rdi), %rax
	call	*8(%rax)
.L560:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L561
	movq	(%rdi), %rax
	call	*8(%rax)
.L561:
	leaq	.LC17(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L600:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L563
	movq	-16(%rbx), %rax
	leaq	-16(%rbx), %rdx
	testb	$1, %al
	jne	.L602
.L565:
	leaq	.LC18(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L602:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L565
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSRegExp10InitializeENS0_6HandleIS1_EENS2_INS0_6StringEEES5_@PLT
	testq	%rax, %rax
	jne	.L603
	movq	312(%r12), %r15
.L569:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L572
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L572:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L604
.L553:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L605
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L603:
	.cfi_restore_state
	movq	(%rbx), %r15
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L595:
	movq	40960(%rsi), %rax
	movl	$496, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L604:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L601:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L599:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L557
.L605:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25975:
	.size	_ZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal11HandleScopeD2Ev,"axG",@progbits,_ZN2v88internal11HandleScopeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11HandleScopeD2Ev
	.type	_ZN2v88internal11HandleScopeD2Ev, @function
_ZN2v88internal11HandleScopeD2Ev:
.LFB9176:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L606
	movq	16(%rax), %rdx
	movq	8(%rax), %rax
	subl	$1, 41104(%rdi)
	movq	%rax, 41088(%rdi)
	cmpq	41096(%rdi), %rdx
	je	.L606
	movq	%rdx, 41096(%rdi)
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L606:
	ret
	.cfi_endproc
.LFE9176:
	.size	_ZN2v88internal11HandleScopeD2Ev, .-_ZN2v88internal11HandleScopeD2Ev
	.weak	_ZN2v88internal11HandleScopeD1Ev
	.set	_ZN2v88internal11HandleScopeD1Ev,_ZN2v88internal11HandleScopeD2Ev
	.section	.text._ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE,"axG",@progbits,_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	.type	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE, @function
_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE:
.LFB16774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	(%rsi), %rcx
	movq	-1(%rcx), %rdi
	movq	%rcx, %r12
	cmpw	$63, 11(%rdi)
	jbe	.L625
.L611:
	movq	-1(%r12), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L626
.L620:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_restore_state
	movq	-1(%r12), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L620
	movq	(%rax), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L621
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	.cfi_restore_state
	movq	-1(%rcx), %rdi
	movzwl	11(%rdi), %edi
	andl	$7, %edi
	cmpw	$1, %di
	jne	.L611
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L614
	movq	23(%rcx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L614
	addq	$16, %rsp
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L627
.L623:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	movq	41112(%r13), %rdi
	movq	15(%rcx), %r12
	testq	%rdi, %rdi
	je	.L616
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L616:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L628
.L618:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r12, (%rax)
	jmp	.L611
.L628:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L627:
	movq	%r13, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L623
	.cfi_endproc
.LFE16774:
	.size	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE, .-_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory12NewSubStringENS0_6HandleINS0_6StringEEEii,"axG",@progbits,_ZN2v88internal7Factory12NewSubStringENS0_6HandleINS0_6StringEEEii,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory12NewSubStringENS0_6HandleINS0_6StringEEEii
	.type	_ZN2v88internal7Factory12NewSubStringENS0_6HandleINS0_6StringEEEii, @function
_ZN2v88internal7Factory12NewSubStringENS0_6HandleINS0_6StringEEEii:
.LFB17458:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jne	.L630
	movq	(%rsi), %rax
	cmpl	11(%rax), %ecx
	je	.L632
.L630:
	jmp	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	.p2align 4,,10
	.p2align 3
.L632:
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE17458:
	.size	_ZN2v88internal7Factory12NewSubStringENS0_6HandleINS0_6StringEEEii, .-_ZN2v88internal7Factory12NewSubStringENS0_6HandleINS0_6StringEEEii
	.section	.rodata._ZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"V8.Runtime_Runtime_RegExpSplit"
	.section	.rodata._ZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"args[0].IsJSReceiver()"
	.section	.text._ZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateE, @function
_ZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateE:
.LFB20264:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$312, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L771
.L634:
	movq	_ZZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateEE29trace_event_unique_atomic1559(%rip), %r12
	testq	%r12, %r12
	je	.L772
.L636:
	movq	$0, -240(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L773
.L638:
	movq	41096(%r14), %rax
	addl	$1, 41104(%r14)
	movq	41088(%r14), %r15
	movq	%rax, -264(%rbp)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L642
.L643:
	leaq	.LC20(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L772:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L774
.L637:
	movq	%r12, _ZZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateEE29trace_event_unique_atomic1559(%rip)
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L642:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L643
	leaq	-8(%rbx), %rax
	movq	%rax, -280(%rbp)
	movq	-8(%rbx), %rax
	testb	$1, %al
	jne	.L775
.L644:
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L773:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L776
.L639:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L640
	movq	(%rdi), %rax
	call	*8(%rax)
.L640:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L641
	movq	(%rdi), %rax
	call	*8(%rax)
.L641:
	leaq	.LC19(%rip), %rax
	movq	%r12, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r13, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L775:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L644
	leaq	-16(%rbx), %rax
	movq	%rax, -272(%rbp)
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1047(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L646
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L647:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object18SpeciesConstructorEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_10JSFunctionEEE@PLT
	movq	%rax, -272(%rbp)
	testq	%rax, %rax
	je	.L768
	movq	2528(%r14), %rax
	leaq	2528(%r14), %rsi
	movl	$3, %edx
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L651
	movl	11(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$3, %edx
.L651:
	movabsq	$824633720832, %rax
	movl	%edx, -160(%rbp)
	movq	%rax, -148(%rbp)
	movq	2528(%r14), %rax
	movq	%r14, -136(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L777
.L652:
	leaq	-160(%rbp), %r13
	movq	%rsi, -128(%rbp)
	movq	%r13, %rdi
	movq	$0, -120(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -156(%rbp)
	jne	.L653
	movq	-136(%rbp), %rax
	leaq	88(%rax), %r12
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L655
.L658:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L768
.L657:
	movl	$117, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	movl	$121, %esi
	movq	%r14, %rdi
	movl	%eax, -296(%rbp)
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	testl	%eax, %eax
	js	.L778
.L659:
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0
	movq	%r12, %xmm2
	movq	%rbx, %xmm0
	movq	%r14, %rdi
	punpcklqdq	%xmm2, %xmm0
	movq	-272(%rbp), %rsi
	movq	%rax, %rcx
	movl	$2, %edx
	movups	%xmm0, (%rax)
	movq	%rax, -288(%rbp)
	call	_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEiPS6_@PLT
	movq	-288(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L779
	movq	%r9, %rdi
	call	_ZdaPv@PLT
	leaq	-244(%rbp), %rdx
	leaq	-16(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_18ToUint32EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPj
	testq	%rax, %rax
	jne	.L662
.L768:
	movq	312(%r14), %r12
.L650:
	subl	$1, 41104(%r14)
	movq	-264(%rbp), %rax
	movq	%r15, 41088(%r14)
	cmpq	41096(%r14), %rax
	je	.L719
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L719:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L780
.L633:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L781
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L646:
	.cfi_restore_state
	movq	%r15, %rdx
	cmpq	41096(%r14), %r15
	je	.L782
.L648:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L653:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L768
	movq	(%r12), %rax
	testb	$1, %al
	je	.L658
.L655:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L658
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L662:
	movq	-8(%rbx), %rax
	movl	-244(%rbp), %ecx
	movl	11(%rax), %eax
	movl	%eax, -272(%rbp)
	testl	%ecx, %ecx
	je	.L769
	movl	-272(%rbp), %edx
	testl	%edx, %edx
	jne	.L664
	leaq	88(%r14), %rcx
	leaq	-8(%rbx), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11RegExpUtils10RegExpExecEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L768
	movq	104(%r14), %rcx
	cmpq	%rcx, (%rax)
	jne	.L769
	movl	$1, %esi
	movq	%r14, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-8(%rbx), %r13
	movq	(%rax), %rdi
	movq	%rax, %r12
	movq	%r13, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r13b
	je	.L720
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L669
	movq	%r13, %rdx
	movq	%rsi, -280(%rbp)
	movq	%rdi, -272(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdi
.L669:
	testb	$24, %al
	je	.L720
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L720
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L720:
	movq	(%r12), %rax
	movq	%r12, %rsi
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r14, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r12
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L778:
	movq	-288(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L659
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L777:
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L771:
	movq	40960(%rdx), %rax
	leaq	-200(%rbp), %rsi
	movl	$498, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L769:
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	(%rax), %r12
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L780:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L779:
	movq	%r9, %rdi
	movq	312(%r14), %r12
	call	_ZdaPv@PLT
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L776:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC19(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L774:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L782:
	movq	%r14, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L664:
	movl	-296(%rbp), %eax
	xorl	%edx, %edx
	movl	$8, %esi
	movq	%r14, %rdi
	notl	%eax
	shrl	$31, %eax
	movl	%eax, -308(%rbp)
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movq	%r13, -320(%rbp)
	movl	$0, -336(%rbp)
	movl	$0, -296(%rbp)
	movq	%rax, -288(%rbp)
	xorl	%eax, %eax
	movq	%r15, -304(%rbp)
	movl	%eax, %r15d
.L671:
	movl	%r15d, %r13d
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11RegExpUtils12SetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEm@PLT
	testq	%rax, %rax
	je	.L767
	movq	-280(%rbp), %rdx
	leaq	88(%r14), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11RegExpUtils10RegExpExecEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L767
	movq	104(%r14), %rax
	cmpq	%rax, (%rbx)
	je	.L766
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11RegExpUtils12GetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L767
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L678
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r14), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L679
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L680:
	testq	%rax, %rax
	je	.L767
.L682:
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L683
	movl	$0, -328(%rbp)
	sarq	$32, %rax
	testq	%rax, %rax
	jle	.L684
	movl	-272(%rbp), %ecx
	cmpl	%eax, %ecx
	cmovb	%ecx, %eax
	movl	%eax, -328(%rbp)
.L684:
	movl	-296(%rbp), %ecx
	cmpl	%ecx, -328(%rbp)
	je	.L766
	movl	-296(%rbp), %eax
	testl	%eax, %eax
	jne	.L689
	movq	-280(%rbp), %rax
	movq	-280(%rbp), %rcx
	movq	(%rax), %rax
	cmpl	11(%rax), %r15d
	je	.L690
.L689:
	movl	-296(%rbp), %edx
	movq	-280(%rbp), %rsi
	movl	%r15d, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %rcx
.L690:
	movl	-336(%rbp), %edx
	movq	-288(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	leal	1(%rdx), %r13d
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	%rax, -288(%rbp)
	cmpl	-244(%rbp), %r13d
	je	.L783
	movq	(%rbx), %rax
	leaq	2768(%r14), %r8
	testb	$1, %al
	jne	.L692
.L694:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r8, -296(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-296(%rbp), %r8
	movq	%rax, %r15
.L693:
	movq	2768(%r14), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L695
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L695:
	movl	%eax, -160(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -148(%rbp)
	movq	2768(%r14), %rax
	movq	%r14, -136(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L784
.L696:
	movq	-320(%rbp), %rdi
	movq	%r8, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -156(%rbp)
	jne	.L697
	movq	-136(%rbp), %rax
	addq	$88, %rax
.L698:
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L699
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r14), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L700
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L701:
	testq	%rax, %rax
	je	.L767
.L703:
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L704
	sarq	$32, %rax
	testq	%rax, %rax
	jle	.L734
	movl	%eax, -336(%rbp)
.L705:
	cmpl	$1, -336(%rbp)
	jbe	.L734
.L707:
	movq	%r12, -344(%rbp)
	movl	$1, %r15d
	movq	-320(%rbp), %r12
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L786:
	movq	-136(%rbp), %rax
	leaq	88(%rax), %rcx
.L713:
	movq	-288(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movq	%r14, %rdi
	leal	1(%r13), %r9d
	movl	%r9d, -296(%rbp)
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movl	-296(%rbp), %r9d
	movq	%rax, -288(%rbp)
	cmpl	-244(%rbp), %r9d
	je	.L785
	addl	$1, %r15d
	cmpl	-336(%rbp), %r15d
	jnb	.L735
	movl	%r9d, %r13d
.L716:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L709
.L711:
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
.L710:
	movabsq	$824633720832, %rcx
	movq	%r12, %rdi
	movl	$3, -160(%rbp)
	movq	%rcx, -148(%rbp)
	movq	%r14, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movl	%r15d, -88(%rbp)
	movl	$-1, -84(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -156(%rbp)
	je	.L786
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L713
	.p2align 4,,10
	.p2align 3
.L767:
	movq	-304(%rbp), %r15
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L766:
	movzbl	-308(%rbp), %edx
	movq	-280(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb@PLT
	movl	%eax, %r15d
.L676:
	cmpl	%r15d, -272(%rbp)
	ja	.L671
	movl	-272(%rbp), %ecx
	movl	-296(%rbp), %edx
	movq	%r14, %rdi
	movq	-280(%rbp), %rsi
	movq	-304(%rbp), %r15
	call	_ZN2v88internal7Factory12NewSubStringENS0_6HandleINS0_6StringEEEii
	movl	-336(%rbp), %ebx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-288(%rbp), %rsi
	movq	%rax, %rcx
	movl	%ebx, %edx
	addl	$1, %ebx
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movl	%ebx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10FixedArray13ShrinkOrEmptyEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	(%rax), %rax
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r12
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L709:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L711
	movq	%rbx, %rax
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L683:
	movsd	7(%rax), %xmm0
	comisd	.LC21(%rip), %xmm0
	jb	.L761
	movsd	.LC22(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L762
	cvttsd2siq	%xmm0, %rax
	movl	-272(%rbp), %ecx
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, -328(%rbp)
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L678:
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L679:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L787
.L681:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L697:
	movq	-320(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L698
	jmp	.L767
.L704:
	movsd	7(%rax), %xmm0
	comisd	.LC21(%rip), %xmm0
	jb	.L734
	movsd	.LC22(%rip), %xmm3
	comisd	%xmm0, %xmm3
	jbe	.L764
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -336(%rbp)
	jmp	.L705
.L692:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L694
	movq	%rbx, %r15
	jmp	.L693
.L762:
	movl	-272(%rbp), %eax
	movl	%eax, -328(%rbp)
	jmp	.L684
.L699:
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L701
.L700:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L788
.L702:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L703
.L734:
	movl	-328(%rbp), %eax
	movl	%r13d, -336(%rbp)
	movl	%eax, -296(%rbp)
	movl	%eax, %r15d
	jmp	.L676
.L761:
	movl	$0, -328(%rbp)
	jmp	.L684
.L784:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r8
	jmp	.L696
.L764:
	movl	$-1, -336(%rbp)
	jmp	.L707
.L787:
	movq	%r14, %rdi
	movq	%rsi, -328(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-328(%rbp), %rsi
	jmp	.L681
.L785:
	movl	%r9d, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	-304(%rbp), %r15
	call	_ZN2v88internal12_GLOBAL__N_122NewJSArrayWithElementsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi
	movq	(%rax), %r12
	jmp	.L650
.L788:
	movq	%r14, %rdi
	movq	%rsi, -296(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-296(%rbp), %rsi
	jmp	.L702
.L783:
	movl	%r13d, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	movq	-304(%rbp), %r15
	call	_ZN2v88internal12_GLOBAL__N_122NewJSArrayWithElementsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi
	movq	(%rax), %r12
	jmp	.L650
.L781:
	call	__stack_chk_fail@PLT
.L735:
	movl	-328(%rbp), %eax
	movl	%r9d, -336(%rbp)
	movq	-344(%rbp), %r12
	movl	%eax, -296(%rbp)
	movl	%eax, %r15d
	jmp	.L676
	.cfi_endproc
.LFE20264:
	.size	_ZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateE, .-_ZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal6Object8ToObjectEPNS0_7IsolateENS0_6HandleIS1_EEPKc,"axG",@progbits,_ZN2v88internal6Object8ToObjectEPNS0_7IsolateENS0_6HandleIS1_EEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Object8ToObjectEPNS0_7IsolateENS0_6HandleIS1_EEPKc
	.type	_ZN2v88internal6Object8ToObjectEPNS0_7IsolateENS0_6HandleIS1_EEPKc, @function
_ZN2v88internal6Object8ToObjectEPNS0_7IsolateENS0_6HandleIS1_EEPKc:
.LFB18250:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L790
.L791:
	jmp	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	.p2align 4,,10
	.p2align 3
.L790:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L791
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE18250:
	.size	_ZN2v88internal6Object8ToObjectEPNS0_7IsolateENS0_6HandleIS1_EEPKc, .-_ZN2v88internal6Object8ToObjectEPNS0_7IsolateENS0_6HandleIS1_EEPKc
	.section	.text._ZN2v88internal6Object8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE,"axG",@progbits,_ZN2v88internal6Object8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Object8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal6Object8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal6Object8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18259:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L793
.L794:
	jmp	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	.p2align 4,,10
	.p2align 3
.L793:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L794
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE18259:
	.size	_ZN2v88internal6Object8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal6Object8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal6Object11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE,"axG",@progbits,_ZN2v88internal6Object11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6Object11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE
	.type	_ZN2v88internal6Object11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE, @function
_ZN2v88internal6Object11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE:
.LFB18262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L796
.L798:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r14
.L797:
	movq	(%r12), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L799
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L799:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	(%r12), %rax
	movq	%r13, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L807
.L800:
	leaq	-144(%rbp), %r12
	movq	%r15, -112(%rbp)
	movq	%r12, %rdi
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r14, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L801
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L802:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L808
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L796:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L798
	movq	%rsi, %r14
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L807:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r15
	jmp	.L800
.L808:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18262:
	.size	_ZN2v88internal6Object11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE, .-_ZN2v88internal6Object11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE
	.section	.text._ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE,"axG",@progbits,_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE
	.type	_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE, @function
_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE:
.LFB20044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	(%rsi), %r12
	movq	%r13, %rax
	movslq	11(%r12), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-1(%r12), %rdx
	cmpq	%rdx, -37104(%rax)
	je	.L810
	movq	-1(%r13), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	cmpl	$3, %edx
	je	.L817
	movq	-37496(%rax), %r8
	testl	%esi, %esi
	je	.L817
	cmpb	$5, %dl
	leaq	15(%r12), %rax
	setbe	%cl
	subl	$1, %esi
	leaq	23(%r12,%rsi,8), %r9
	andl	%edx, %ecx
	movzbl	%dl, %esi
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L813:
	andl	$1, %edi
	je	.L814
	testb	%cl, %cl
	jne	.L831
	movl	$2, %esi
.L814:
	addq	$8, %rax
	cmpq	%rax, %r9
	je	.L843
.L816:
	movq	(%rax), %rdi
	cmpq	%rdi, %r8
	jne	.L813
	movl	$1, %ecx
	testb	%sil, %sil
	je	.L827
	cmpb	$4, %sil
	je	.L828
	cmpb	$2, %sil
	je	.L829
	cmpb	$6, %sil
	movl	$7, %edi
	cmove	%edi, %esi
	addq	$8, %rax
	cmpq	%rax, %r9
	jne	.L816
	.p2align 4,,10
	.p2align 3
.L843:
	cmpb	%sil, %dl
	jne	.L841
	.p2align 4,,10
	.p2align 3
.L817:
	movq	%r12, 15(%r13)
	leaq	15(%r13), %rsi
	testb	$1, %r12b
	je	.L826
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L844
	testb	$24, %al
	je	.L826
.L846:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L845
.L826:
	movq	(%r14), %rax
	movq	(%rbx), %rdx
	movslq	11(%rax), %rax
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L844:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L846
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L827:
	movl	$1, %esi
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L810:
	movq	-1(%r13), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$1, %eax
	je	.L842
	movq	-1(%r13), %rax
	testb	$-8, 14(%rax)
	jne	.L817
	testl	%esi, %esi
	je	.L820
	subq	$1, %r12
	addl	$2, %esi
	movl	$2, %ecx
	movabsq	$-2251799814209537, %rdi
	jmp	.L822
	.p2align 4,,10
	.p2align 3
.L821:
	addl	$1, %ecx
	cmpl	%ecx, %esi
	je	.L820
.L822:
	leal	0(,%rcx,8), %eax
	cltq
	cmpq	%rdi, (%rax,%r12)
	jne	.L821
.L842:
	movl	$5, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	(%rbx), %r13
	movq	(%r14), %r12
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L820:
	movl	$4, %esi
.L841:
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	(%rbx), %r13
	movq	(%r14), %r12
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L845:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L828:
	movl	$5, %esi
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L829:
	movl	$3, %esi
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L831:
	movl	$3, %esi
	jmp	.L841
	.cfi_endproc
.LFE20044:
	.size	_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE, .-_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb0EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"unreachable code"
	.section	.text._ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb0EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb0EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE, @function
_ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb0EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE:
.LFB22472:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -200(%rbp)
	movq	(%rdx), %rcx
	movq	%rsi, -144(%rbp)
	movq	%rdx, -168(%rbp)
	movq	23(%rcx), %rdx
	leaq	23(%rcx), %rsi
	movq	%r8, -176(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	cmpb	$0, _ZN2v88internal19FLAG_regexp_tier_upE(%rip)
	jne	.L915
.L854:
	testb	%al, %al
	je	.L855
.L859:
	movq	15(%rdx), %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	je	.L901
	cmpl	$2, %eax
	jne	.L858
	movq	(%rsi), %rax
	movq	79(%rax), %rax
	shrq	$32, %rax
	movq	%rax, -192(%rbp)
.L856:
	movq	-144(%rbp), %rax
	movq	(%rax), %rsi
	movl	11(%rsi), %eax
	movl	%eax, -180(%rbp)
	cmpl	$4096, %eax
	jg	.L908
	leaq	-112(%rbp), %rax
	movq	%rax, -152(%rbp)
.L860:
	movq	-144(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%r15, %rcx
	movq	-152(%rbp), %rdi
	call	_ZN2v88internal17RegExpGlobalCacheC1ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE@PLT
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	js	.L914
	movq	-176(%rbp), %rax
	movq	41112(%r15), %rdi
	movq	(%rax), %rax
	movq	15(%rax), %r12
	testq	%rdi, %rdi
	je	.L873
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %rsi
	cmpl	$15, 11(%r12)
	jle	.L916
.L876:
	leaq	-128(%rbp), %r12
	xorl	%ebx, %ebx
	movl	$-1, %r14d
	movq	%r12, %rdi
	call	_ZN2v88internal17FixedArrayBuilderC1ENS0_6HandleINS0_10FixedArrayEEE@PLT
	movb	$1, -136(%rbp)
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L878:
	movq	41088(%r15), %rax
	movl	4(%r13), %ebx
	addl	$1, 41104(%r15)
	cmpb	$0, -136(%rbp)
	movq	%rax, -160(%rbp)
	movq	41096(%r15), %r13
	je	.L882
	testl	%r14d, %r14d
	jne	.L882
	movq	-144(%rbp), %rax
	movq	(%rax), %rsi
	cmpl	11(%rsi), %ebx
	je	.L881
	.p2align 4,,10
	.p2align 3
.L882:
	movq	-144(%rbp), %rsi
	movl	%ebx, %ecx
	movl	%r14d, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	(%rax), %rsi
.L881:
	movq	%r12, %rdi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_6ObjectE@PLT
	movq	-160(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r13
	je	.L899
	movq	%r13, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L899:
	movb	$0, -136(%rbp)
.L884:
	movq	-152(%rbp), %rdi
	call	_ZN2v88internal17RegExpGlobalCache9FetchNextEv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L877
	movl	(%rax), %r14d
	movl	$5, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17FixedArrayBuilder14EnsureCapacityEPNS0_7IsolateEi@PLT
	cmpl	%r14d, %ebx
	jge	.L878
	movl	%r14d, %eax
	subl	%ebx, %eax
	testl	$-2048, %eax
	jne	.L879
	testl	$-524288, %ebx
	je	.L917
.L879:
	movl	%ebx, %esi
	movq	%r12, %rdi
	subl	%r14d, %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L908:
	leaq	-112(%rbp), %rax
	movq	$0, -112(%rbp)
	movq	23(%rcx), %rdx
	xorl	%r8d, %r8d
	leaq	37592(%r15), %rdi
	movq	%rax, %rcx
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal18RegExpResultsCache6LookupEPNS0_4HeapENS0_6StringENS0_6ObjectEPNS0_10FixedArrayENS1_16ResultsCacheTypeE@PLT
	movq	%rax, %r13
	testb	$1, %al
	je	.L860
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L860
	movl	-192(%rbp), %eax
	leaq	_ZSt7nothrow(%rip), %rsi
	movabsq	$2305843009213693950, %rdx
	leal	2(%rax,%rax), %ebx
	movslq	%ebx, %rax
	leaq	0(,%rax,4), %rcx
	cmpq	%rdx, %rax
	movq	$-1, %rax
	cmovbe	%rcx, %rax
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L918
.L863:
	testl	%ebx, %ebx
	jle	.L867
	leal	-1(%rbx), %eax
	movq	-112(%rbp), %rcx
	movq	%r12, %rdi
	leaq	24(,%rax,8), %rsi
	movl	$16, %eax
	.p2align 4,,10
	.p2align 3
.L868:
	movq	-1(%rax,%rcx), %rdx
	addq	$8, %rax
	addq	$4, %rdi
	sarq	$32, %rdx
	movl	%edx, -4(%rdi)
	cmpq	%rsi, %rax
	jne	.L868
.L867:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L919
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L869:
	leaq	152(%r15), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory21CopyFixedArrayWithMapENS0_6HandleINS0_10FixedArrayEEENS2_INS0_3MapEEE@PLT
	movq	-176(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE
	movl	-192(%rbp), %ecx
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	-144(%rbp), %rdx
	movq	-200(%rbp), %rsi
	call	_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi@PLT
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	movq	(%rbx), %r12
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L901:
	movl	$0, -192(%rbp)
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L914:
	movq	312(%r15), %r12
.L872:
	movq	-152(%rbp), %rdi
	call	_ZN2v88internal17RegExpGlobalCacheD1Ev@PLT
.L897:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L920
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L877:
	.cfi_restore_state
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	js	.L914
	testl	%r14d, %r14d
	js	.L886
	cmpl	-180(%rbp), %ebx
	jl	.L921
.L887:
	movq	-152(%rbp), %rdi
	call	_ZN2v88internal17RegExpGlobalCache19LastSuccessfulMatchEv@PLT
	movl	-192(%rbp), %ecx
	movq	-144(%rbp), %rdx
	movq	%r15, %rdi
	movq	-200(%rbp), %rsi
	movq	%rax, %r8
	call	_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi@PLT
	cmpl	$4096, -180(%rbp)
	jg	.L922
.L889:
	movq	-176(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17FixedArrayBuilder9ToJSArrayENS0_6HandleINS0_7JSArrayEEE@PLT
	movq	(%rax), %r12
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L849:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rdx
	jne	.L853
	.p2align 4,,10
	.p2align 3
.L855:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rdx
	jne	.L859
.L858:
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L915:
	testb	%al, %al
	je	.L849
.L853:
	movq	15(%rdx), %rax
	sarq	$32, %rax
	cmpl	$2, %eax
	je	.L923
.L913:
	movq	23(%rcx), %rdx
	leaq	23(%rcx), %rsi
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L917:
	sall	$11, %ebx
	movq	%r12, %rdi
	movl	%ebx, %esi
	orl	%eax, %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L873:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L924
.L875:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r12, (%rsi)
	cmpl	$15, 11(%r12)
	jg	.L876
.L916:
	movl	$16, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rsi
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L886:
	movq	104(%r15), %r12
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L923:
	leaq	-112(%rbp), %rax
	movq	%rcx, -112(%rbp)
	movq	%rax, %rdi
	call	_ZN2v88internal8JSRegExp21MarkTierUpForNextExecEv@PLT
	movq	-168(%rbp), %rax
	movq	(%rax), %rcx
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L922:
	movl	-192(%rbp), %eax
	xorl	%edx, %edx
	movq	%r15, %rdi
	leal	2(%rax,%rax), %r13d
	movl	%r13d, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-152(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal17RegExpGlobalCache19LastSuccessfulMatchEv@PLT
	testl	%r13d, %r13d
	jle	.L893
	leal	-1(%r13), %edx
	movl	$16, %ecx
	leaq	4(%rax,%rdx,4), %rdi
	.p2align 4,,10
	.p2align 3
.L894:
	movslq	(%rax), %rdx
	movq	(%rbx), %rsi
	addq	$4, %rax
	addq	$8, %rcx
	salq	$32, %rdx
	movq	%rdx, -9(%rcx,%rsi)
	cmpq	%rax, %rdi
	jne	.L894
.L893:
	movl	-120(%rbp), %edx
	movq	-128(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10FixedArray13ShrinkOrEmptyEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	%r15, %rdi
	leaq	152(%r15), %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory21CopyFixedArrayWithMapENS0_6HandleINS0_10FixedArrayEEENS2_INS0_3MapEEE@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %r13
	movq	-168(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L925
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L895:
	movq	-144(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	movq	%r13, %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal18RegExpResultsCache5EnterEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEENS4_INS0_10FixedArrayEEESA_NS1_16ResultsCacheTypeE@PLT
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L921:
	movl	-180(%rbp), %eax
	subl	%ebx, %eax
	testl	$-2048, %eax
	jne	.L888
	testl	$-524288, %ebx
	je	.L926
.L888:
	movl	%ebx, %esi
	subl	-180(%rbp), %esi
	movq	%r12, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	jmp	.L887
.L919:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L927
.L870:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%rsi)
	jmp	.L869
.L925:
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L928
.L896:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L895
.L924:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L875
.L926:
	sall	$11, %ebx
	movq	%r12, %rdi
	orl	%ebx, %eax
	salq	$32, %rax
	movq	%rax, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	jmp	.L887
.L927:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L870
.L928:
	movq	%r15, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L896
.L920:
	call	__stack_chk_fail@PLT
.L918:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L863
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE22472:
	.size	_ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb0EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE, .-_ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb0EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE, @function
_ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE:
.LFB22474:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -352(%rbp)
	movq	(%rdx), %rcx
	movq	%rdx, -288(%rbp)
	movq	%r8, -320(%rbp)
	movq	23(%rcx), %rdx
	leaq	23(%rcx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	cmpb	$0, _ZN2v88internal19FLAG_regexp_tier_upE(%rip)
	jne	.L1075
.L936:
	testb	%al, %al
	je	.L937
.L941:
	movq	15(%rdx), %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	je	.L1017
	cmpl	$2, %eax
	jne	.L940
	movq	(%rsi), %rax
	movq	79(%rax), %rax
	shrq	$32, %rax
	movq	%rax, -280(%rbp)
.L938:
	movq	0(%r13), %rsi
	movl	11(%rsi), %eax
	movl	%eax, -340(%rbp)
	cmpl	$4096, %eax
	jg	.L1062
	leaq	-160(%rbp), %rax
	movq	%rax, -272(%rbp)
.L942:
	movq	-272(%rbp), %rdi
	movq	-288(%rbp), %rsi
	movq	%r15, %rcx
	movq	%r13, %rdx
	call	_ZN2v88internal17RegExpGlobalCacheC1ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE@PLT
	movl	-160(%rbp), %edi
	testl	%edi, %edi
	js	.L1073
	movq	-320(%rbp), %rax
	movq	41112(%r15), %rdi
	movq	(%rax), %rax
	movq	15(%rax), %r12
	testq	%rdi, %rdi
	je	.L955
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %rsi
	cmpl	$15, 11(%r12)
	jle	.L1076
.L958:
	leaq	-176(%rbp), %rax
	movl	$1, %r12d
	movq	%rax, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal17FixedArrayBuilderC1ENS0_6HandleINS0_10FixedArrayEEE@PLT
	movl	-280(%rbp), %ebx
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEENS2_6ObjectEPNS2_7IsolateENS2_6HandleINS2_6StringEEENS8_INS2_8JSRegExpEEENS8_INS2_15RegExpMatchInfoEEENS8_INS2_7JSArrayEEEEUliE_E10_M_managerERSt9_Any_dataRKSJ_St18_Manager_operation(%rip), %rdx
	movl	$0, -204(%rbp)
	movl	$-1, -200(%rbp)
	movq	%rdx, %xmm1
	leal	-1(%rbx), %eax
	leaq	32(,%rax,8), %rax
	movq	%rax, -184(%rbp)
	leal	1(%rbx), %eax
	movl	%eax, -292(%rbp)
	leal	3(%rbx), %eax
	movl	%eax, -296(%rbp)
	sall	$3, %eax
	movslq	%eax, %rbx
	addl	$8, %eax
	cltq
	movq	%rbx, -304(%rbp)
	movq	%rax, -312(%rbp)
	leaq	_ZNSt17_Function_handlerIFN2v88internal6ObjectEiEZNS1_12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEES2_PNS1_7IsolateENS1_6HandleINS1_6StringEEENS8_INS1_8JSRegExpEEENS8_INS1_15RegExpMatchInfoEEENS8_INS1_7JSArrayEEEEUliE_E9_M_invokeERKSt9_Any_dataOi(%rip), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm1
	movaps	%xmm1, -336(%rbp)
	.p2align 4,,10
	.p2align 3
.L995:
	movq	-272(%rbp), %rdi
	call	_ZN2v88internal17RegExpGlobalCache9FetchNextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L959
	movl	(%rax), %eax
	movq	-240(%rbp), %rdi
	movl	$5, %edx
	movq	%r15, %rsi
	movl	%eax, %r14d
	movl	%eax, -200(%rbp)
	call	_ZN2v88internal17FixedArrayBuilder14EnsureCapacityEPNS0_7IsolateEi@PLT
	cmpl	%r14d, -204(%rbp)
	jl	.L1077
.L960:
	movq	41088(%r15), %rax
	movl	4(%rbx), %ecx
	addl	$1, 41104(%r15)
	movq	%rax, -264(%rbp)
	movq	41096(%r15), %rax
	movl	%ecx, -204(%rbp)
	movq	%rax, -248(%rbp)
	testb	%r12b, %r12b
	je	.L1068
	movl	-200(%rbp), %esi
	testl	%esi, %esi
	jne	.L964
	movq	0(%r13), %rax
	movq	%r13, %r14
	cmpl	11(%rax), %ecx
	je	.L963
.L964:
	movl	-204(%rbp), %ecx
.L1068:
	movl	-200(%rbp), %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %r14
.L963:
	movq	-288(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	87(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L966
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, -256(%rbp)
	testb	$1, %sil
	jne	.L1078
.L969:
	movb	$0, -205(%rbp)
	movl	-296(%rbp), %esi
.L971:
	movq	%r15, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	(%r14), %r14
	movq	(%rax), %rdi
	movq	%rax, %r12
	movq	%r14, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r14b
	je	.L1015
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -192(%rbp)
	testl	$262144, %eax
	je	.L973
	movq	%r14, %rdx
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-192(%rbp), %rcx
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
	movq	8(%rcx), %rax
.L973:
	testb	$24, %al
	je	.L1015
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1015
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1015:
	movl	-280(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L1019
	movl	$24, %r14d
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L1079:
	movl	-12(%rbx,%r14), %ecx
	jne	.L978
	movq	0(%r13), %rdx
	cmpl	11(%rdx), %ecx
	je	.L979
.L978:
	movl	%r9d, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	(%rax), %rdx
.L979:
	movq	(%r12), %rdi
	leaq	-1(%rdi,%r14), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L983
.L1072:
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -192(%rbp)
	testl	$262144, %eax
	je	.L985
	movq	%rdx, -232(%rbp)
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-192(%rbp), %rcx
	movq	-232(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
	movq	8(%rcx), %rax
.L985:
	testb	$24, %al
	je	.L983
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L983
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
.L983:
	addq	$8, %r14
	cmpq	%r14, -184(%rbp)
	je	.L1020
.L976:
	movl	-16(%rbx,%r14), %r9d
	testl	%r9d, %r9d
	jns	.L1079
	movq	(%r12), %rdi
	movq	88(%r15), %rdx
	leaq	-1(%rdi,%r14), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	jne	.L1072
	addq	$8, %r14
	cmpq	%r14, -184(%rbp)
	jne	.L976
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	-312(%rbp), %rcx
	movq	-304(%rbp), %rsi
	movl	-292(%rbp), %ebx
.L975:
	movq	-200(%rbp), %rdx
	movq	(%r12), %rax
	salq	$32, %rdx
	movq	%rdx, -1(%rsi,%rax)
	movq	(%r12), %rdi
	movq	0(%r13), %r14
	leaq	-1(%rdi,%rcx), %rsi
	movq	%r14, (%rsi)
	testb	$1, %r14b
	je	.L1012
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -192(%rbp)
	testl	$262144, %eax
	je	.L988
	movq	%r14, %rdx
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-192(%rbp), %rcx
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
	movq	8(%rcx), %rax
.L988:
	testb	$24, %al
	je	.L1012
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1012
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1012:
	cmpb	$0, -205(%rbp)
	jne	.L1080
.L990:
	movq	(%r12), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	-240(%rbp), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_6ObjectE@PLT
	movq	-264(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-248(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L1010
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1010:
	xorl	%r12d, %r12d
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L1062:
	leaq	-160(%rbp), %rax
	leaq	37592(%r15), %rdi
	xorl	%r8d, %r8d
	movq	$0, -160(%rbp)
	movq	23(%rcx), %rdx
	movq	%rax, %rcx
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal18RegExpResultsCache6LookupEPNS0_4HeapENS0_6StringENS0_6ObjectEPNS0_10FixedArrayENS1_16ResultsCacheTypeE@PLT
	movq	%rax, %rbx
	testb	$1, %al
	je	.L942
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L942
	movl	-280(%rbp), %eax
	leaq	_ZSt7nothrow(%rip), %rsi
	movabsq	$2305843009213693950, %rdx
	leal	2(%rax,%rax), %r8d
	movslq	%r8d, %rax
	movl	%r8d, -184(%rbp)
	cmpq	%rdx, %rax
	leaq	0(,%rax,4), %r14
	movq	$-1, %rax
	cmova	%rax, %r14
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-184(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L1081
.L945:
	testl	%r8d, %r8d
	jle	.L949
	leal	-1(%r8), %eax
	movq	-160(%rbp), %rsi
	movq	%r12, %rcx
	leaq	24(,%rax,8), %rdi
	movl	$16, %eax
	.p2align 4,,10
	.p2align 3
.L950:
	movq	-1(%rax,%rsi), %rdx
	addq	$8, %rax
	addq	$4, %rcx
	sarq	$32, %rdx
	movl	%edx, -4(%rcx)
	cmpq	%rdi, %rax
	jne	.L950
.L949:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1082
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L951:
	leaq	152(%r15), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory21CopyFixedArrayWithMapENS0_6HandleINS0_10FixedArrayEEENS2_INS0_3MapEEE@PLT
	movq	-320(%rbp), %rbx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7JSArray10SetContentENS0_6HandleIS1_EENS2_INS0_14FixedArrayBaseEEE
	movq	%r12, %r8
	movq	%r13, %rdx
	movq	%r15, %rdi
	movl	-280(%rbp), %ecx
	movq	-352(%rbp), %rsi
	call	_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi@PLT
	movq	%r12, %rdi
	call	_ZdaPv@PLT
	movq	(%rbx), %r12
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1017:
	movl	$0, -280(%rbp)
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L966:
	movq	41088(%r15), %rax
	movq	%rax, -256(%rbp)
	cmpq	41096(%r15), %rax
	je	.L1083
.L968:
	movq	-256(%rbp), %rdi
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdi)
	testb	$1, %sil
	je	.L969
.L1078:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L969
	movl	-280(%rbp), %eax
	movb	$1, -205(%rbp)
	leal	4(%rax), %esi
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1080:
	leaq	-96(%rbp), %r8
	movdqa	-336(%rbp), %xmm0
	movq	%r15, %rdi
	movq	%r12, -96(%rbp)
	movq	-256(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r8, -192(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_133ConstructNamedCaptureGroupsObjectEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEERKSt8functionIFNS0_6ObjectEiEE
	movq	%rax, %r14
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L991
	movq	-192(%rbp), %r8
	movl	$3, %edx
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	*%rax
.L991:
	movq	(%r12), %rdi
	leal	32(,%rbx,8), %eax
	movq	(%r14), %r14
	cltq
	leaq	-1(%rdi,%rax), %rbx
	movq	%r14, (%rbx)
	testb	$1, %r14b
	je	.L990
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -192(%rbp)
	testl	$262144, %eax
	je	.L993
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-192(%rbp), %rcx
	movq	-216(%rbp), %rdi
	movq	8(%rcx), %rax
.L993:
	testb	$24, %al
	je	.L990
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L990
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L1077:
	movl	-200(%rbp), %eax
	movl	-204(%rbp), %esi
	subl	%esi, %eax
	testl	$-2048, %eax
	jne	.L961
	testl	$-524288, %esi
	je	.L1084
.L961:
	movl	-204(%rbp), %r14d
	movq	-240(%rbp), %rdi
	movl	%r14d, %esi
	subl	-200(%rbp), %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movq	%r14, %rsi
	movq	-240(%rbp), %rdi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	312(%r15), %r12
.L954:
	movq	-272(%rbp), %rdi
	call	_ZN2v88internal17RegExpGlobalCacheD1Ev@PLT
.L1008:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1085
	addq	$312, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L959:
	.cfi_restore_state
	movl	-160(%rbp), %edx
	testl	%edx, %edx
	js	.L1073
	movl	-200(%rbp), %eax
	testl	%eax, %eax
	js	.L997
	movl	-340(%rbp), %ebx
	cmpl	%ebx, -204(%rbp)
	jl	.L1086
.L998:
	movq	-272(%rbp), %rdi
	call	_ZN2v88internal17RegExpGlobalCache19LastSuccessfulMatchEv@PLT
	movl	-280(%rbp), %ecx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	-352(%rbp), %rsi
	movq	%rax, %r8
	call	_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi@PLT
	cmpl	$4096, -340(%rbp)
	jg	.L1087
.L1000:
	movq	-320(%rbp), %rsi
	movq	-240(%rbp), %rdi
	call	_ZN2v88internal17FixedArrayBuilder9ToJSArrayENS0_6HandleINS0_7JSArrayEEE@PLT
	movq	(%rax), %r12
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L1019:
	movl	$32, %ecx
	movl	$24, %esi
	movl	$1, %ebx
	jmp	.L975
.L1083:
	movq	%r15, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	movq	%rax, -256(%rbp)
	jmp	.L968
.L931:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	%rdx, -37504(%rax)
	jne	.L935
.L937:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rdx
	jne	.L941
.L940:
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1075:
	testb	%al, %al
	je	.L931
.L935:
	movq	15(%rdx), %rax
	sarq	$32, %rax
	cmpl	$2, %eax
	je	.L1088
.L1067:
	movq	23(%rcx), %rdx
	leaq	23(%rcx), %rsi
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L936
.L1084:
	sall	$11, %esi
	movq	-240(%rbp), %rdi
	orl	%eax, %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	jmp	.L960
.L955:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1089
.L957:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r12, (%rsi)
	cmpl	$15, 11(%r12)
	jg	.L958
.L1076:
	movl	$16, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movq	%rax, %rsi
	jmp	.L958
.L997:
	movq	104(%r15), %r12
	jmp	.L954
.L1088:
	leaq	-160(%rbp), %rax
	movq	%rcx, -160(%rbp)
	movq	%rax, %rdi
	call	_ZN2v88internal8JSRegExp21MarkTierUpForNextExecEv@PLT
	movq	-288(%rbp), %rax
	movq	(%rax), %rcx
	jmp	.L1067
.L1087:
	movl	-292(%rbp), %ebx
	xorl	%edx, %edx
	movq	%r15, %rdi
	addl	%ebx, %ebx
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-272(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal17RegExpGlobalCache19LastSuccessfulMatchEv@PLT
	testl	%ebx, %ebx
	jle	.L1004
	leal	-1(%rbx), %edx
	movl	$16, %ecx
	leaq	4(%rax,%rdx,4), %rdi
	.p2align 4,,10
	.p2align 3
.L1005:
	movslq	(%rax), %rdx
	movq	(%r12), %rsi
	addq	$4, %rax
	addq	$8, %rcx
	salq	$32, %rdx
	movq	%rdx, -9(%rcx,%rsi)
	cmpq	%rax, %rdi
	jne	.L1005
.L1004:
	movl	-168(%rbp), %edx
	movq	-176(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10FixedArray13ShrinkOrEmptyEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movq	%r15, %rdi
	leaq	152(%r15), %rdx
	movq	%rax, %rsi
	call	_ZN2v88internal7Factory21CopyFixedArrayWithMapENS0_6HandleINS0_10FixedArrayEEENS2_INS0_3MapEEE@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rbx
	movq	-288(%rbp), %rax
	movq	(%rax), %rax
	movq	23(%rax), %r14
	testq	%rdi, %rdi
	je	.L1090
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1006:
	xorl	%r9d, %r9d
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18RegExpResultsCache5EnterEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEENS4_INS0_10FixedArrayEEESA_NS1_16ResultsCacheTypeE@PLT
	jmp	.L1000
.L1086:
	movl	%ebx, %eax
	movl	-204(%rbp), %ebx
	subl	%ebx, %eax
	testl	$-2048, %eax
	jne	.L999
	testl	$-524288, %ebx
	je	.L1091
.L999:
	movl	-204(%rbp), %ebx
	movq	-240(%rbp), %r14
	movl	%ebx, %esi
	subl	-340(%rbp), %esi
	movq	%r14, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	jmp	.L998
.L1082:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L1092
.L952:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%rbx, (%rsi)
	jmp	.L951
.L1090:
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L1093
.L1007:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%r14, (%rdx)
	jmp	.L1006
.L1089:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L957
.L1091:
	movl	%ebx, %esi
	movq	-240(%rbp), %rdi
	sall	$11, %esi
	orl	%eax, %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	jmp	.L998
.L1092:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L952
.L1093:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L1007
.L1081:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	movl	-184(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L945
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L1085:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22474:
	.size	_ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE, .-_ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE
	.section	.rodata._ZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"V8.Runtime_Runtime_RegExpExecMultiple"
	.section	.rodata._ZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC25:
	.string	"args[2].IsRegExpMatchInfo()"
.LC26:
	.string	"args[3].IsJSArray()"
	.section	.rodata._ZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC27:
	.string	"result_array->HasObjectElements()"
	.align 8
.LC28:
	.string	"regexp->GetFlags() & JSRegExp::kGlobal"
	.section	.text._ZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE.isra.0:
.LFB25973:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1166
.L1095:
	movq	_ZZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1381(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1167
.L1097:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1168
.L1099:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1169
.L1103:
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1167:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1170
.L1098:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1381(%rip)
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1169:
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	jne	.L1103
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rsi
	testb	$1, %al
	jne	.L1171
.L1104:
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1168:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1172
.L1100:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1101
	movq	(%rdi), %rax
	call	*8(%rax)
.L1101:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1102
	movq	(%rdi), %rax
	call	*8(%rax)
.L1102:
	leaq	.LC24(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1171:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1104
	movq	-16(%r13), %rdx
	leaq	-16(%r13), %r15
	testb	$1, %dl
	jne	.L1173
.L1106:
	leaq	.LC25(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1173:
	movq	-1(%rdx), %rdx
	cmpw	$123, 11(%rdx)
	jne	.L1106
	movq	-24(%r13), %rdx
	leaq	-24(%r13), %r8
	testb	$1, %dl
	jne	.L1174
.L1108:
	leaq	.LC26(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1174:
	movq	-1(%rdx), %rcx
	cmpw	$1061, 11(%rcx)
	jne	.L1108
	movq	-1(%rdx), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	subl	$2, %edx
	cmpb	$1, %dl
	ja	.L1175
	movq	-1(%rax), %rdx
	movq	%rax, %r9
	cmpw	$63, 11(%rdx)
	ja	.L1113
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L1176
.L1113:
	movq	-1(%r9), %rax
	cmpw	$63, 11(%rax)
	ja	.L1121
	movq	-1(%r9), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L1177
.L1121:
	movq	0(%r13), %rax
	movq	23(%rax), %rdx
	movq	31(%rdx), %rax
	btq	$32, %rax
	jnc	.L1178
	testb	$1, %dl
	jne	.L1128
.L1132:
	movq	15(%rdx), %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	jne	.L1179
.L1133:
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb0EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE
	movq	%rax, %r13
.L1135:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1138
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1138:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1180
.L1094:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1181
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1179:
	.cfi_restore_state
	cmpl	$2, %eax
	jne	.L1131
	movq	79(%rdx), %rax
	shrq	$32, %rax
	je	.L1133
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE
	movq	%rax, %r13
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1177:
	movq	(%rsi), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %r9
	testq	%rdi, %rdi
	je	.L1124
	movq	%r9, %rsi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1176:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1116
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L1116
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	41112(%r12), %rdi
	movq	15(%rax), %r9
	testq	%rdi, %rdi
	je	.L1118
	movq	%r9, %rsi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-168(%rbp), %r8
	movq	(%rax), %r9
	movq	%rax, %rsi
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1128:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rdx
	jne	.L1132
.L1131:
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1166:
	movq	40960(%rsi), %rax
	movl	$495, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1172:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC24(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1170:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1175:
	leaq	.LC27(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1124:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1182
.L1126:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r9, (%rsi)
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1178:
	leaq	.LC28(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1180:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1118:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1183
.L1120:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r9, (%rsi)
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	%r12, %rdi
	movq	%r9, -176(%rbp)
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %r9
	movq	-168(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1126
.L1183:
	movq	%r12, %rdi
	movq	%r9, -176(%rbp)
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %r9
	movq	-168(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1120
.L1181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25973:
	.size	_ZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal19CompiledReplacement5ApplyEPNS0_24ReplacementStringBuilderEiiPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19CompiledReplacement5ApplyEPNS0_24ReplacementStringBuilderEiiPi
	.type	_ZN2v88internal19CompiledReplacement5ApplyEPNS0_24ReplacementStringBuilderEiiPi, @function
_ZN2v88internal19CompiledReplacement5ApplyEPNS0_24ReplacementStringBuilderEiiPi:
.LFB20193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r15
	movq	%rdi, -88(%rbp)
	movl	%edx, -56(%rbp)
	movq	16(%rdi), %r14
	movl	%ecx, -52(%rbp)
	movq	%r8, -96(%rbp)
	testq	%r15, %r15
	je	.L1184
	movl	4(%r15), %eax
	movq	%rsi, %r13
	testq	%rax, %rax
	movq	%rax, -80(%rbp)
	sete	-65(%rbp)
	cmpl	(%r15), %eax
	je	.L1225
	cmpq	%r14, %r15
	je	.L1226
.L1190:
	movl	-52(%rbp), %eax
	xorl	%ebx, %ebx
	leaq	.L1195(%rip), %r12
	movl	%eax, %edi
	andl	$-524288, %edi
	movl	%edi, -112(%rbp)
	movq	%rax, %rdi
	sall	$11, %eax
	movl	%eax, -140(%rbp)
	movl	-56(%rbp), %eax
	salq	$32, %rdi
	movq	%rdi, -128(%rbp)
	movl	$1073741799, %edi
	movl	%eax, %edx
	movl	%eax, %ecx
	subl	%eax, %edi
	negl	%edx
	andl	$-2048, %ecx
	movl	%edi, -108(%rbp)
	movq	%rdx, %rsi
	movl	%ecx, -104(%rbp)
	movq	%rax, %rcx
	salq	$32, %rsi
	salq	$32, %rcx
	movq	%rsi, -120(%rbp)
	leaq	8(%r13), %rsi
	movq	%rcx, -136(%rbp)
	movq	%rsi, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L1192:
	leaq	24(%r14,%rbx,8), %rdx
	cmpl	$6, (%rdx)
	ja	.L1193
	movl	(%rdx), %eax
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19CompiledReplacement5ApplyEPNS0_24ReplacementStringBuilderEiiPi,"a",@progbits
	.align 4
	.align 4
.L1195:
	.long	.L1193-.L1195
	.long	.L1199-.L1195
	.long	.L1198-.L1195
	.long	.L1197-.L1195
	.long	.L1196-.L1195
	.long	.L1196-.L1195
	.long	.L1194-.L1195
	.section	.text._ZN2v88internal19CompiledReplacement5ApplyEPNS0_24ReplacementStringBuilderEiiPi
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	-88(%rbp), %rax
	movslq	4(%rdx), %rdx
	movq	%r13, %rdi
	movq	40(%rax), %rax
	movq	(%rax,%rdx,8), %rsi
	call	_ZN2v88internal24ReplacementStringBuilder9AddStringENS0_6HandleINS0_6StringEEE@PLT
.L1194:
	movl	(%r14), %eax
	addq	$1, %rbx
	cmpq	%rax, %rbx
	jb	.L1208
.L1228:
	movq	8(%r14), %r14
	cmpq	%r14, %r15
	jne	.L1211
	cmpb	$0, -65(%rbp)
	jne	.L1184
.L1211:
	xorl	%ebx, %ebx
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1197:
	movl	4(%rdx), %eax
	movq	-96(%rbp), %rcx
	addl	%eax, %eax
	cltq
	movl	(%rcx,%rax,4), %edx
	movl	4(%rcx,%rax,4), %eax
	testl	%edx, %edx
	js	.L1194
	cmpl	%eax, %edx
	movl	%eax, -100(%rbp)
	movl	%edx, -72(%rbp)
	jge	.L1194
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi@PLT
	movl	-100(%rbp), %eax
	movl	-72(%rbp), %edx
	movl	%eax, %ecx
	subl	%edx, %ecx
	testl	$-2048, %ecx
	jne	.L1206
	testl	$-524288, %edx
	je	.L1227
.L1206:
	movl	%edx, %esi
	movq	-64(%rbp), %rdi
	movl	%ecx, -100(%rbp)
	subl	%eax, %esi
	movl	%edx, -72(%rbp)
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movl	-72(%rbp), %edx
	movq	-64(%rbp), %rdi
	movq	%rdx, %rsi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movl	-100(%rbp), %ecx
.L1207:
	movl	$1073741799, %edx
	movl	32(%r13), %eax
	subl	%ecx, %edx
	cmpl	%edx, %eax
	jg	.L1205
	addl	%eax, %ecx
	addq	$1, %rbx
	movl	%ecx, 32(%r13)
	movl	(%r14), %eax
	cmpq	%rax, %rbx
	jnb	.L1228
	.p2align 4,,10
	.p2align 3
.L1208:
	cmpq	%r14, %r15
	jne	.L1192
	cmpq	%rbx, -80(%rbp)
	jne	.L1192
.L1184:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1198:
	.cfi_restore_state
	movl	4(%rdx), %eax
	cmpl	-52(%rbp), %eax
	jle	.L1194
	movl	$2, %esi
	movq	%r13, %rdi
	movl	%eax, -72(%rbp)
	call	_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi@PLT
	movl	-72(%rbp), %eax
	movl	%eax, %edx
	subl	-52(%rbp), %edx
	movl	%edx, %ecx
	andl	$-2048, %ecx
	orl	-112(%rbp), %ecx
	jne	.L1203
	movl	-140(%rbp), %esi
	movq	-64(%rbp), %rdi
	movl	%edx, -72(%rbp)
	orl	%edx, %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movl	-72(%rbp), %edx
	movl	$1073741799, %ecx
	movl	32(%r13), %eax
	subl	%edx, %ecx
	cmpl	%ecx, %eax
	jle	.L1229
	.p2align 4,,10
	.p2align 3
.L1205:
	movl	$2147483647, 32(%r13)
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1199:
	movl	-56(%rbp), %edx
	testl	%edx, %edx
	jle	.L1194
	movl	$2, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi@PLT
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	jne	.L1200
	movq	-136(%rbp), %rsi
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movl	32(%r13), %eax
	cmpl	-108(%rbp), %eax
	jg	.L1205
.L1202:
	addl	-56(%rbp), %eax
	movl	%eax, 32(%r13)
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	-120(%rbp), %rsi
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movq	-64(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movl	32(%r13), %eax
	cmpl	-108(%rbp), %eax
	jle	.L1202
	movl	$2147483647, 32(%r13)
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1226:
	cmpb	$0, -65(%rbp)
	je	.L1190
	jmp	.L1184
	.p2align 4,,10
	.p2align 3
.L1225:
	movq	8(%r15), %r15
	movb	$1, -65(%rbp)
	movq	$0, -80(%rbp)
	cmpq	%r14, %r15
	jne	.L1190
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1203:
	movl	-52(%rbp), %esi
	movq	-64(%rbp), %rdi
	movl	%edx, -72(%rbp)
	subl	%eax, %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movq	-128(%rbp), %rsi
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movl	-72(%rbp), %edx
	movl	$1073741799, %ecx
	movl	32(%r13), %eax
	subl	%edx, %ecx
	cmpl	%ecx, %eax
	jg	.L1205
.L1229:
	addl	%eax, %edx
	movl	%edx, 32(%r13)
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1227:
	movl	%edx, %esi
	movq	-64(%rbp), %rdi
	movl	%ecx, -72(%rbp)
	sall	$11, %esi
	orl	%ecx, %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movl	-72(%rbp), %ecx
	jmp	.L1207
.L1193:
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20193:
	.size	_ZN2v88internal19CompiledReplacement5ApplyEPNS0_24ReplacementStringBuilderEiiPi, .-_ZN2v88internal19CompiledReplacement5ApplyEPNS0_24ReplacementStringBuilderEiiPi
	.section	.text._ZN2v88internal18Runtime_RegExpExecEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_RegExpExecEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_RegExpExecEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_RegExpExecEiPmPNS0_7IsolateE:
.LFB20211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1257
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1233
.L1234:
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1233:
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	jne	.L1234
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r15
	testb	$1, %al
	jne	.L1258
.L1235:
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1258:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1235
	movq	-16(%rsi), %rax
	testb	$1, %al
	je	.L1238
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1259
.L1238:
	leaq	-68(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movl	$0, -68(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L1260
	movq	-24(%r13), %rax
	leaq	-24(%r13), %r8
	testb	$1, %al
	jne	.L1261
.L1241:
	leaq	.LC14(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	-1(%rax), %rax
	cmpw	$123, 11(%rax)
	jne	.L1241
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	js	.L1262
	movq	-8(%r13), %rdx
	cmpl	11(%rdx), %eax
	jg	.L1263
	movq	40960(%r12), %rdx
	cmpb	$0, 7136(%rdx)
	je	.L1245
	movq	7128(%rdx), %rax
.L1246:
	testq	%rax, %rax
	je	.L1247
	addl	$1, (%rax)
.L1247:
	movl	-68(%rbp), %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6RegExp4ExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE@PLT
	testq	%rax, %rax
	je	.L1264
	movq	(%rax), %r13
.L1249:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1230
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1230:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1265
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1245:
	.cfi_restore_state
	movb	$1, 7136(%rdx)
	leaq	7112(%rdx), %rdi
	movq	%r8, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %r8
	movq	%rax, 7128(%rdx)
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1257:
	call	_ZN2v88internalL24Stats_Runtime_RegExpExecEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1264:
	movq	312(%r12), %r13
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1259:
	leaq	.LC12(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1260:
	leaq	.LC13(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1262:
	leaq	.LC15(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1263:
	leaq	.LC16(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1265:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20211:
	.size	_ZN2v88internal18Runtime_RegExpExecEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_RegExpExecEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE:
.LFB20252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1312
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1313
.L1268:
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1313:
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	jne	.L1268
	movq	-8(%r13), %rax
	leaq	-8(%rsi), %rsi
	testb	$1, %al
	jne	.L1314
.L1269:
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1314:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1269
	movq	-16(%r13), %rdx
	leaq	-16(%r13), %r15
	testb	$1, %dl
	jne	.L1315
.L1271:
	leaq	.LC25(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1315:
	movq	-1(%rdx), %rdx
	cmpw	$123, 11(%rdx)
	jne	.L1271
	movq	-24(%r13), %rdx
	leaq	-24(%r13), %r8
	testb	$1, %dl
	jne	.L1316
.L1273:
	leaq	.LC26(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1316:
	movq	-1(%rdx), %rcx
	cmpw	$1061, 11(%rcx)
	jne	.L1273
	movq	-1(%rdx), %rdx
	movzbl	14(%rdx), %edx
	shrl	$3, %edx
	subl	$2, %edx
	cmpb	$1, %dl
	ja	.L1317
	movq	-1(%rax), %rdx
	movq	%rax, %r9
	cmpw	$63, 11(%rdx)
	ja	.L1278
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L1318
.L1278:
	movq	-1(%r9), %rax
	cmpw	$63, 11(%rax)
	ja	.L1286
	movq	-1(%r9), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L1319
.L1286:
	movq	0(%r13), %rax
	movq	23(%rax), %rdx
	movq	31(%rdx), %rax
	btq	$32, %rax
	jnc	.L1320
	testb	$1, %dl
	jne	.L1293
.L1297:
	movq	15(%rdx), %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	je	.L1298
	cmpl	$2, %eax
	jne	.L1296
	movq	79(%rdx), %rax
	shrq	$32, %rax
	je	.L1298
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb1EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE
	movq	%rax, %r13
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1298:
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_1L20SearchRegExpMultipleILb0EEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEENS6_INS0_15RegExpMatchInfoEEENS6_INS0_7JSArrayEEE
	movq	%rax, %r13
.L1300:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1266
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1266:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1319:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %r9
	testq	%rdi, %rdi
	je	.L1289
	movq	%r9, %rsi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1318:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1281
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L1281
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	41112(%r12), %rdi
	movq	15(%rax), %r9
	testq	%rdi, %rdi
	je	.L1283
	movq	%r9, %rsi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %r8
	movq	(%rax), %r9
	movq	%rax, %rsi
	jmp	.L1278
	.p2align 4,,10
	.p2align 3
.L1293:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rdx
	jne	.L1297
.L1296:
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1312:
	addq	$24, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1317:
	.cfi_restore_state
	leaq	.LC27(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1289:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1321
.L1291:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r9, (%rsi)
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1320:
	leaq	.LC28(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1283:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1322
.L1285:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r9, (%rsi)
	jmp	.L1278
.L1321:
	movq	%r12, %rdi
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1291
.L1322:
	movq	%r12, %rdi
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L1285
	.cfi_endproc
.LFE20252:
	.size	_ZN2v88internal26Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_RegExpExecMultipleEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Runtime_RegExpSplitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_RegExpSplitEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_RegExpSplitEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_RegExpSplitEiPmPNS0_7IsolateE:
.LFB20265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1437
	movq	41096(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r15
	movq	%rax, -168(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1326
.L1327:
	leaq	.LC20(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1326:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1327
	leaq	-8(%rsi), %rax
	movq	%rax, -184(%rbp)
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L1438
.L1328:
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1438:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1328
	leaq	-16(%rsi), %rax
	movq	%rax, -176(%rbp)
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	1047(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1330
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L1331:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object18SpeciesConstructorEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_10JSFunctionEEE@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L1434
	movq	2528(%r14), %rax
	leaq	2528(%r14), %rsi
	movl	$3, %edx
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L1335
	movl	11(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$3, %edx
.L1335:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	2528(%r14), %rax
	movq	%r14, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L1439
.L1336:
	leaq	-144(%rbp), %rbx
	movq	%rsi, -112(%rbp)
	movq	%rbx, %rdi
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L1337
	movq	-120(%rbp), %rax
	leaq	88(%rax), %r13
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1339
.L1342:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1434
.L1341:
	movl	$117, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	movl	$121, %esi
	movq	%r14, %rdi
	movl	%eax, -200(%rbp)
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	testl	%eax, %eax
	js	.L1440
.L1343:
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0
	movq	%r13, %xmm2
	movq	%r12, %xmm0
	movq	%r14, %rdi
	punpcklqdq	%xmm2, %xmm0
	movq	-176(%rbp), %rsi
	movq	%rax, %rcx
	movl	$2, %edx
	movups	%xmm0, (%rax)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal9Execution3NewEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEiPS6_@PLT
	movq	-192(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1441
	movq	%r9, %rdi
	call	_ZdaPv@PLT
	leaq	-148(%rbp), %rdx
	leaq	-16(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_18ToUint32EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEPj
	testq	%rax, %rax
	jne	.L1346
.L1434:
	movq	312(%r14), %r12
.L1334:
	subl	$1, 41104(%r14)
	movq	-168(%rbp), %rax
	movq	%r15, 41088(%r14)
	cmpq	41096(%r14), %rax
	je	.L1323
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1323:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1442
	addq	$200, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1330:
	.cfi_restore_state
	movq	%r15, %rdx
	cmpq	41096(%r14), %r15
	je	.L1443
.L1332:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1337:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1434
	movq	0(%r13), %rax
	testb	$1, %al
	je	.L1342
.L1339:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1342
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1346:
	movq	-8(%r12), %rax
	movl	-148(%rbp), %ecx
	movl	11(%rax), %eax
	movl	%eax, -176(%rbp)
	testl	%ecx, %ecx
	je	.L1435
	movl	-176(%rbp), %edx
	testl	%edx, %edx
	jne	.L1348
	leaq	88(%r14), %rcx
	leaq	-8(%r12), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11RegExpUtils10RegExpExecEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L1434
	movq	104(%r14), %rbx
	cmpq	%rbx, (%rax)
	jne	.L1435
	movl	$1, %esi
	movq	%r14, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	-8(%r12), %r12
	movq	(%rax), %rdi
	movq	%rax, %r13
	movq	%r12, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r12b
	je	.L1403
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L1353
	movq	%r12, %rdx
	movq	%rsi, -184(%rbp)
	movq	%rdi, -176(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-184(%rbp), %rsi
	movq	-176(%rbp), %rdi
.L1353:
	testb	$24, %al
	je	.L1403
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1403
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1403:
	movq	0(%r13), %rax
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r12
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	-192(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L1343
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L1439:
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1437:
	call	_ZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateE
	movq	%rax, %r12
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1435:
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	(%rax), %r12
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	%r9, %rdi
	movq	312(%r14), %r12
	call	_ZdaPv@PLT
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	%r14, %rdi
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1348:
	movl	-200(%rbp), %eax
	movq	%r14, %rdi
	xorl	%edx, %edx
	movl	$8, %esi
	notl	%eax
	shrl	$31, %eax
	movl	%eax, -220(%rbp)
	call	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE@PLT
	movq	%r15, -216(%rbp)
	movl	$0, -224(%rbp)
	movl	$0, -208(%rbp)
	movl	$0, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	%rbx, %rax
	movq	%r14, %rbx
	movq	%rax, %r14
.L1355:
	movl	-192(%rbp), %r15d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal11RegExpUtils12SetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEm@PLT
	testq	%rax, %rax
	je	.L1433
	movq	-184(%rbp), %rdx
	leaq	88(%rbx), %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11RegExpUtils10RegExpExecEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1433
	movq	104(%rbx), %rax
	cmpq	%rax, (%r12)
	je	.L1432
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11RegExpUtils12GetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L1433
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L1362
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%rbx), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L1363
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1364:
	testq	%rax, %rax
	je	.L1433
.L1366:
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L1367
	movl	$0, -232(%rbp)
	sarq	$32, %rax
	testq	%rax, %rax
	jle	.L1368
	movl	-176(%rbp), %ecx
	cmpl	%eax, %ecx
	cmovb	%ecx, %eax
	movl	%eax, -232(%rbp)
.L1368:
	movl	-208(%rbp), %ecx
	cmpl	%ecx, -232(%rbp)
	je	.L1432
	movl	-208(%rbp), %eax
	testl	%eax, %eax
	jne	.L1373
	movq	-184(%rbp), %rax
	movl	-192(%rbp), %ecx
	movq	(%rax), %rax
	cmpl	11(%rax), %ecx
	je	.L1444
.L1373:
	movl	-192(%rbp), %ecx
	movl	-208(%rbp), %edx
	movq	%rbx, %rdi
	movq	-184(%rbp), %rsi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %rcx
.L1374:
	movl	-224(%rbp), %edx
	movq	-200(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	leal	1(%rdx), %r15d
	movl	%r15d, -224(%rbp)
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movq	%rax, -200(%rbp)
	cmpl	-148(%rbp), %r15d
	je	.L1445
	movq	(%r12), %rax
	leaq	2768(%rbx), %r8
	testb	$1, %al
	jne	.L1376
.L1378:
	movl	$-1, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-192(%rbp), %r8
	movq	%rax, %r15
.L1377:
	movq	2768(%rbx), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L1379
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L1379:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	2768(%rbx), %rax
	movq	%rbx, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1446
.L1380:
	movq	%r14, %rdi
	movq	%r8, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r15, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L1381
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L1382:
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L1383
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%rbx), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L1384
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1385:
	testq	%rax, %rax
	je	.L1433
.L1387:
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L1388
	sarq	$32, %rax
	testq	%rax, %rax
	jle	.L1415
	movl	%eax, -208(%rbp)
.L1389:
	cmpl	$1, -208(%rbp)
	jbe	.L1415
.L1391:
	movq	%r13, -240(%rbp)
	movl	$1, %r15d
	movl	-224(%rbp), %r13d
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1448:
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rcx
.L1397:
	movq	-200(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	%r13d, %edx
	movq	%rbx, %rdi
	leal	1(%r13), %r9d
	movl	%r9d, -192(%rbp)
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movl	-192(%rbp), %r9d
	movq	%rax, -200(%rbp)
	cmpl	-148(%rbp), %r9d
	je	.L1447
	addl	$1, %r15d
	cmpl	-208(%rbp), %r15d
	jnb	.L1416
	movl	%r9d, %r13d
.L1400:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L1393
.L1395:
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
.L1394:
	movabsq	$824633720832, %rcx
	movq	%r14, %rdi
	movq	%rbx, -120(%rbp)
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%r15d, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	je	.L1448
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L1397
	.p2align 4,,10
	.p2align 3
.L1433:
	movq	-216(%rbp), %r15
	movq	%rbx, %r14
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L1432:
	movzbl	-220(%rbp), %edx
	movq	-184(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb@PLT
	movl	%eax, -192(%rbp)
	movl	%eax, %ecx
.L1360:
	cmpl	%ecx, -176(%rbp)
	ja	.L1355
	movl	-176(%rbp), %ecx
	movl	-208(%rbp), %edx
	movq	%rbx, %rdi
	movq	%rbx, %r14
	movq	-184(%rbp), %rsi
	movq	-216(%rbp), %r15
	call	_ZN2v88internal7Factory12NewSubStringENS0_6HandleINS0_6StringEEEii
	movl	-224(%rbp), %ebx
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movq	-200(%rbp), %rsi
	movq	%rax, %rcx
	movl	%ebx, %edx
	addl	$1, %ebx
	call	_ZN2v88internal10FixedArray10SetAndGrowEPNS0_7IsolateENS0_6HandleIS1_EEiNS4_INS0_6ObjectEEENS0_14AllocationTypeE@PLT
	movl	%ebx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal10FixedArray13ShrinkOrEmptyEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	(%rax), %rax
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r12
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1393:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1395
	movq	%r12, %rax
	jmp	.L1394
	.p2align 4,,10
	.p2align 3
.L1367:
	movsd	7(%rax), %xmm0
	comisd	.LC21(%rip), %xmm0
	jb	.L1427
	movsd	.LC22(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L1428
	cvttsd2siq	%xmm0, %rax
	movl	-176(%rbp), %ecx
	cmpl	%ecx, %eax
	cmova	%ecx, %eax
	movl	%eax, -232(%rbp)
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1362:
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L1364
	.p2align 4,,10
	.p2align 3
.L1363:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1449
.L1365:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1366
.L1381:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L1382
	jmp	.L1433
.L1388:
	movsd	7(%rax), %xmm0
	comisd	.LC21(%rip), %xmm0
	jb	.L1415
	movsd	.LC22(%rip), %xmm3
	comisd	%xmm0, %xmm3
	jbe	.L1430
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -208(%rbp)
	jmp	.L1389
.L1376:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1378
	movq	%r12, %r15
	jmp	.L1377
.L1428:
	movl	-176(%rbp), %eax
	movl	%eax, -232(%rbp)
	jmp	.L1368
.L1383:
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L1385
.L1384:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1450
.L1386:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1387
.L1415:
	movl	-232(%rbp), %eax
	movl	%eax, -192(%rbp)
	movl	-192(%rbp), %ecx
	movl	%eax, -208(%rbp)
	jmp	.L1360
.L1427:
	movl	$0, -232(%rbp)
	jmp	.L1368
.L1446:
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r8
	jmp	.L1380
.L1444:
	movq	-184(%rbp), %rcx
	jmp	.L1374
.L1430:
	movl	$-1, -208(%rbp)
	jmp	.L1391
.L1449:
	movq	%rbx, %rdi
	movq	%rsi, -232(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	jmp	.L1365
.L1447:
	movl	%r9d, %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	-216(%rbp), %r15
	call	_ZN2v88internal12_GLOBAL__N_122NewJSArrayWithElementsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi
	movq	%rbx, %r14
	movq	(%rax), %r12
	jmp	.L1334
.L1450:
	movq	%rbx, %rdi
	movq	%rsi, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	jmp	.L1386
.L1445:
	movl	-224(%rbp), %edx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%rbx, %r14
	movq	-216(%rbp), %r15
	call	_ZN2v88internal12_GLOBAL__N_122NewJSArrayWithElementsEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEEi
	movq	(%rax), %r12
	jmp	.L1334
.L1442:
	call	__stack_chk_fail@PLT
.L1416:
	movl	-232(%rbp), %eax
	movl	%r9d, -224(%rbp)
	movq	-240(%rbp), %r13
	movl	%eax, -208(%rbp)
	movl	%eax, %ecx
	movl	%eax, -192(%rbp)
	jmp	.L1360
	.cfi_endproc
.LFE20265:
	.size	_ZN2v88internal19Runtime_RegExpSplitEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_RegExpSplitEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE:
.LFB20274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1467
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1468
.L1453:
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1468:
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	jne	.L1453
	movq	-8(%r13), %rax
	leaq	-8(%rsi), %rsi
	testb	$1, %al
	jne	.L1469
.L1454:
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1469:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1454
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rdx
	testb	$1, %al
	jne	.L1470
.L1456:
	leaq	.LC18(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1470:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1456
	movq	%r13, %rdi
	call	_ZN2v88internal8JSRegExp10InitializeENS0_6HandleIS1_EENS2_INS0_6StringEEES5_@PLT
	testq	%rax, %rax
	jne	.L1471
	movq	312(%r12), %r13
.L1460:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1451
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1451:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1471:
	.cfi_restore_state
	movq	0(%r13), %r13
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1467:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rdx, %rsi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20274:
	.size	_ZN2v88internal34Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal16Runtime_IsRegExpEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16Runtime_IsRegExpEiPmPNS0_7IsolateE
	.type	_ZN2v88internal16Runtime_IsRegExpEiPmPNS0_7IsolateE, @function
_ZN2v88internal16Runtime_IsRegExpEiPmPNS0_7IsolateE:
.LFB20277:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L1477
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1474
.L1476:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1474:
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	jne	.L1476
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20277:
	.size	_ZN2v88internal16Runtime_IsRegExpEiPmPNS0_7IsolateE, .-_ZN2v88internal16Runtime_IsRegExpEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEEC2EPNS0_4ZoneENS4_9StartModeE,"axG",@progbits,_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEEC5EPNS0_4ZoneENS4_9StartModeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEEC2EPNS0_4ZoneENS4_9StartModeE
	.type	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEEC2EPNS0_4ZoneENS4_9StartModeE, @function
_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEEC2EPNS0_4ZoneENS4_9StartModeE:
.LFB22395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
	testl	%edx, %edx
	jne	.L1486
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1486:
	.cfi_restore_state
	movq	%rsi, %rdi
	movl	%edx, %eax
	movq	24(%rdi), %rcx
	leaq	24(,%rax,8), %rsi
	movq	16(%rdi), %rax
	subq	%rax, %rcx
	cmpq	%rcx, %rsi
	ja	.L1487
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1481:
	pxor	%xmm0, %xmm0
	movl	$0, 4(%rax)
	movl	%edx, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rax, 24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1487:
	.cfi_restore_state
	movl	%edx, -20(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-20(%rbp), %edx
	jmp	.L1481
	.cfi_endproc
.LFE22395:
	.size	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEEC2EPNS0_4ZoneENS4_9StartModeE, .-_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEEC2EPNS0_4ZoneENS4_9StartModeE
	.weak	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEEC1EPNS0_4ZoneENS4_9StartModeE
	.set	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEEC1EPNS0_4ZoneENS4_9StartModeE,_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEEC2EPNS0_4ZoneENS4_9StartModeE
	.section	.text._ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_,"axG",@progbits,_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	.type	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_, @function
_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_:
.LFB23742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L1497
.L1489:
	movl	4(%rax), %edx
	cmpl	(%rax), %edx
	je	.L1498
.L1492:
	movq	0(%r13), %rcx
	movq	%rcx, 24(%rax,%rdx,8)
	movq	24(%rbx), %rax
	addl	$1, 4(%rax)
	addq	$1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1498:
	.cfi_restore_state
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L1499
.L1493:
	movq	%rax, 24(%rbx)
	movl	4(%rax), %edx
	jmp	.L1492
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	(%rdi), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	$87, %rdx
	jbe	.L1500
	leaq	88(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L1491:
	pxor	%xmm0, %xmm0
	movq	$8, (%rax)
	movups	%xmm0, 8(%rax)
	movq	%rax, 16(%rbx)
	movq	%rax, 24(%rbx)
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1499:
	leal	(%rdx,%rdx), %r12d
	movl	$256, %eax
	movq	(%rbx), %rdi
	cmpl	$256, %r12d
	cmova	%eax, %r12d
	movq	24(%rdi), %rdx
	movl	%r12d, %eax
	leaq	24(,%rax,8), %rsi
	movq	16(%rdi), %rax
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L1501
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1495:
	pxor	%xmm0, %xmm0
	movl	$0, 4(%rax)
	movups	%xmm0, 8(%rax)
	movl	%r12d, (%rax)
	movq	24(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	24(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rbx), %rax
	movq	8(%rax), %rax
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1500:
	movl	$88, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1491
	.p2align 4,,10
	.p2align 3
.L1501:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L1495
	.cfi_endproc
.LFE23742:
	.size	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_, .-_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	.section	.rodata._ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii.str1.1,"aMS",@progbits,1
.LC29:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii
	.type	_ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii, @function
_ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii:
.LFB20192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$184, %rsp
	movq	%rcx, -120(%rbp)
	movq	(%rcx), %rax
	movq	%rsi, -144(%rbp)
	leaq	-105(%rbp), %rsi
	movl	%r8d, -136(%rbp)
	movl	%r9d, -160(%rbp)
	movq	%fs:40, %rdi
	movq	%rdi, -56(%rbp)
	xorl	%edi, %edi
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movl	-136(%rbp), %ecx
	movq	%rdx, -128(%rbp)
	movq	%rax, %r12
	movl	%edx, %r14d
	testl	%ecx, %ecx
	jg	.L1695
.L1503:
	movq	$0, -152(%rbp)
.L1504:
	movq	-128(%rbp), %rax
	shrq	$32, %rax
	cmpl	$1, %eax
	je	.L1696
	movl	-128(%rbp), %eax
	testl	%eax, %eax
	jle	.L1507
	leaq	_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKtEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%r15, %r8
	xorl	%r9d, %r9d
	movq	%r13, %r15
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKtEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation(%rip), %rdx
	movq	%rax, %xmm5
	xorl	%ebx, %ebx
	movq	%r12, %r13
	movq	%rdx, %xmm1
	leaq	.L1543(%rip), %rcx
	punpcklqdq	%xmm5, %xmm1
	movaps	%xmm1, -192(%rbp)
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1538:
	cmpl	%ebx, %r14d
	jle	.L1683
.L1687:
	movslq	%ebx, %rax
	movl	%ebx, %r12d
	leal	1(%rbx), %ebx
	cmpw	$36, 0(%r13,%rax,2)
	leaq	(%rax,%rax), %rdx
	jne	.L1538
	cmpl	%ebx, %r14d
	je	.L1683
	movzwl	2(%r13,%rdx), %esi
	cmpw	$60, %si
	ja	.L1540
	cmpw	$35, %si
	jbe	.L1689
	leal	-36(%rsi), %eax
	cmpw	$24, %ax
	ja	.L1689
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii,"a",@progbits
	.align 4
	.align 4
.L1543:
	.long	.L1547-.L1543
	.long	.L1689-.L1543
	.long	.L1546-.L1543
	.long	.L1545-.L1543
	.long	.L1689-.L1543
	.long	.L1689-.L1543
	.long	.L1689-.L1543
	.long	.L1689-.L1543
	.long	.L1689-.L1543
	.long	.L1689-.L1543
	.long	.L1689-.L1543
	.long	.L1689-.L1543
	.long	.L1544-.L1543
	.long	.L1544-.L1543
	.long	.L1544-.L1543
	.long	.L1544-.L1543
	.long	.L1544-.L1543
	.long	.L1544-.L1543
	.long	.L1544-.L1543
	.long	.L1544-.L1543
	.long	.L1544-.L1543
	.long	.L1544-.L1543
	.long	.L1689-.L1543
	.long	.L1689-.L1543
	.long	.L1542-.L1543
	.section	.text._ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii
.L1547:
	cmpl	%r9d, %r12d
	jg	.L1549
	movl	%ebx, %r9d
.L1689:
	leal	2(%r12), %ebx
	cmpl	%ebx, %r14d
	jg	.L1687
	.p2align 4,,10
	.p2align 3
.L1683:
	movq	%r15, %r13
	movq	%r8, %r15
.L1694:
	movq	-128(%rbp), %rax
	cmpl	%r9d, %eax
	jle	.L1507
	testl	%r9d, %r9d
	je	.L1536
	negl	%r9d
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%eax, -100(%rbp)
	movl	%r9d, -104(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
.L1507:
	movq	24(%r13), %r15
	movq	16(%r13), %r12
	testq	%r15, %r15
	je	.L1567
	movl	4(%r15), %edi
	testq	%rdi, %rdi
	movq	%rdi, -152(%rbp)
	sete	-128(%rbp)
	cmpl	(%r15), %edi
	jne	.L1569
	movb	$1, -128(%rbp)
	movq	8(%r15), %r15
	movq	$0, -152(%rbp)
.L1569:
	cmpq	%r12, %r15
	jne	.L1618
	cmpb	$0, -128(%rbp)
	jne	.L1567
.L1618:
	xorl	%ebx, %ebx
	xorl	%r11d, %r11d
	.p2align 4,,10
	.p2align 3
.L1571:
	leaq	24(%r12,%rbx,8), %r14
	movl	(%r14), %eax
	testl	%eax, %eax
	jle	.L1697
.L1573:
	cmpl	$5, %eax
	je	.L1698
.L1588:
	movl	(%r12), %eax
	addq	$1, %rbx
	cmpq	%rax, %rbx
	jb	.L1601
.L1700:
	movq	8(%r12), %r12
	cmpq	%r12, %r15
	jne	.L1617
	cmpb	$0, -128(%rbp)
	jne	.L1567
.L1617:
	xorl	%ebx, %ebx
	leaq	24(%r12,%rbx,8), %r14
	movl	(%r14), %eax
	testl	%eax, %eax
	jg	.L1573
.L1697:
	movl	%eax, %edx
	movl	4(%r14), %ecx
	negl	%edx
	testl	%eax, %eax
	jne	.L1574
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	cmpl	11(%rax), %ecx
	je	.L1699
.L1574:
	movq	-120(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movl	%r11d, -136(%rbp)
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movl	-136(%rbp), %r11d
	movq	%rax, %rcx
.L1575:
	movq	48(%r13), %rdx
	cmpq	56(%r13), %rdx
	je	.L1576
	movq	%rcx, (%rdx)
	addq	$8, 48(%r13)
.L1577:
	movl	%r11d, 4(%r14)
	addq	$1, %rbx
	addl	$1, %r11d
	movl	$4, (%r14)
	movl	(%r12), %eax
	cmpq	%rax, %rbx
	jnb	.L1700
.L1601:
	cmpq	%r12, %r15
	jne	.L1571
	cmpq	%rbx, -152(%rbp)
	jne	.L1571
.L1567:
	xorl	%eax, %eax
.L1502:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L1701
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1540:
	.cfi_restore_state
	cmpw	$96, %si
	jne	.L1689
	cmpl	%r9d, %r12d
	jg	.L1702
	movq	$1, -104(%rbp)
	jmp	.L1688
.L1542:
	cmpq	$0, -152(%rbp)
	leal	2(%r12), %ebx
	je	.L1538
	cmpl	%ebx, %r14d
	jle	.L1683
	movslq	%ebx, %rsi
	movq	%rsi, %rax
	jmp	.L1560
	.p2align 4,,10
	.p2align 3
.L1703:
	addq	$1, %rax
	cmpl	%eax, %r14d
	jle	.L1687
.L1560:
	cmpw	$62, 0(%r13,%rax,2)
	jne	.L1703
	cmpl	$-1, %eax
	movl	%eax, -208(%rbp)
	je	.L1687
	subq	%rsi, %rax
	movdqa	-192(%rbp), %xmm6
	movq	-152(%rbp), %rsi
	leaq	4(%rdx,%r13), %rdx
	leaq	-96(%rbp), %rdi
	movq	%r8, -200(%rbp)
	movl	%r9d, -176(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rdi, -216(%rbp)
	movaps	%xmm6, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_118LookupNamedCaptureERKSt8functionIFbNS0_6StringEEENS0_10FixedArrayE
	movl	-176(%rbp), %r9d
	movq	-200(%rbp), %r8
	movl	%eax, %ebx
	movq	-80(%rbp), %rax
	movl	-208(%rbp), %r10d
	testq	%rax, %rax
	je	.L1561
	movq	-216(%rbp), %rdi
	movq	%r8, -208(%rbp)
	movl	$3, %edx
	movl	%r9d, -200(%rbp)
	movl	%r10d, -176(%rbp)
	movq	%rdi, %rsi
	call	*%rax
	movq	-208(%rbp), %r8
	movl	-200(%rbp), %r9d
	movl	-176(%rbp), %r10d
.L1561:
	cmpl	%r9d, %r12d
	jg	.L1704
.L1562:
	movl	$3, %eax
	cmpl	$-1, %ebx
	je	.L1705
.L1563:
	movq	%r8, %rsi
	movq	%r15, %rdi
	movl	%ebx, -100(%rbp)
	movl	%r10d, -200(%rbp)
	movq	%r8, -176(%rbp)
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	movl	-200(%rbp), %r10d
	movq	-176(%rbp), %r8
	leaq	.L1543(%rip), %rcx
	leal	1(%r10), %r9d
	movl	%r9d, %ebx
	jmp	.L1538
.L1544:
	leal	-48(%rsi), %eax
	leal	2(%r12), %ebx
	cmpl	%eax, -136(%rbp)
	jl	.L1538
	cmpl	%ebx, %r14d
	jg	.L1706
	testl	%eax, %eax
	je	.L1683
.L1604:
	cmpl	%r9d, %r12d
	jg	.L1707
.L1556:
	movq	%r8, %rsi
	movq	%r15, %rdi
	movl	%eax, -100(%rbp)
	movq	%r8, -176(%rbp)
	movl	$3, -104(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	movq	-176(%rbp), %r8
	movl	%ebx, %r9d
	leaq	.L1543(%rip), %rcx
	jmp	.L1538
.L1545:
	cmpl	%r9d, %r12d
	jg	.L1708
.L1551:
	movl	-160(%rbp), %eax
	movl	$2, -104(%rbp)
	movl	%eax, -100(%rbp)
.L1688:
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -176(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	leal	2(%r12), %r9d
	movq	-176(%rbp), %r8
	leaq	.L1543(%rip), %rcx
	movl	%r9d, %ebx
	jmp	.L1538
.L1546:
	cmpl	%r9d, %r12d
	jg	.L1709
.L1552:
	movq	$3, -104(%rbp)
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1698:
	movq	48(%r13), %rdx
	cmpq	56(%r13), %rdx
	je	.L1589
	movq	-120(%rbp), %rax
	movq	%rax, (%rdx)
	addq	$8, 48(%r13)
.L1590:
	movl	%r11d, 4(%r14)
	addl	$1, %r11d
	jmp	.L1588
	.p2align 4,,10
	.p2align 3
.L1695:
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	87(%rax), %rax
	movq	%rax, -152(%rbp)
	testb	$1, %al
	je	.L1503
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	jbe	.L1504
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1536:
	movl	$1, %eax
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1696:
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal19CompiledReplacement23ParseReplacementPatternIKhEEbPNS2_13ZoneChunkListINS3_15ReplacementPartEEENS2_6VectorIT_EENS2_10FixedArrayEiiEUlNS2_6StringEE_E10_M_managerERSt9_Any_dataRKSH_St18_Manager_operation(%rip), %rdx
	leaq	_ZNSt17_Function_handlerIFbN2v88internal6StringEEZNS1_19CompiledReplacement23ParseReplacementPatternIKhEEbPNS1_13ZoneChunkListINS4_15ReplacementPartEEENS1_6VectorIT_EENS1_10FixedArrayEiiEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	xorl	%r9d, %r9d
	xorl	%ebx, %ebx
	movq	%rdx, %xmm3
	movq	%rax, %xmm6
	movl	-128(%rbp), %edx
	leaq	.L1513(%rip), %rcx
	punpcklqdq	%xmm6, %xmm3
	movaps	%xmm3, -176(%rbp)
	testl	%edx, %edx
	jle	.L1507
	movslq	%ebx, %rax
	leal	1(%rbx), %edx
	cmpb	$36, (%r12,%rax)
	je	.L1710
	.p2align 4,,10
	.p2align 3
.L1508:
	cmpl	%edx, %r14d
	jle	.L1694
.L1529:
	movl	%edx, %ebx
.L1719:
	movslq	%ebx, %rax
	leal	1(%rbx), %edx
	cmpb	$36, (%r12,%rax)
	jne	.L1508
.L1710:
	cmpl	%edx, %r14d
	je	.L1694
	movslq	%edx, %rax
	movzbl	(%r12,%rax), %esi
	cmpb	$60, %sil
	ja	.L1510
	cmpb	$35, %sil
	jbe	.L1686
	leal	-36(%rsi), %eax
	cmpb	$24, %al
	ja	.L1686
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii
	.align 4
	.align 4
.L1513:
	.long	.L1517-.L1513
	.long	.L1686-.L1513
	.long	.L1516-.L1513
	.long	.L1515-.L1513
	.long	.L1686-.L1513
	.long	.L1686-.L1513
	.long	.L1686-.L1513
	.long	.L1686-.L1513
	.long	.L1686-.L1513
	.long	.L1686-.L1513
	.long	.L1686-.L1513
	.long	.L1686-.L1513
	.long	.L1514-.L1513
	.long	.L1514-.L1513
	.long	.L1514-.L1513
	.long	.L1514-.L1513
	.long	.L1514-.L1513
	.long	.L1514-.L1513
	.long	.L1514-.L1513
	.long	.L1514-.L1513
	.long	.L1514-.L1513
	.long	.L1514-.L1513
	.long	.L1686-.L1513
	.long	.L1686-.L1513
	.long	.L1512-.L1513
	.section	.text._ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii
.L1517:
	cmpl	%r9d, %ebx
	jg	.L1519
	movl	%edx, %r9d
.L1686:
	leal	2(%rbx), %edx
	jmp	.L1508
.L1512:
	cmpq	$0, -152(%rbp)
	leal	2(%rbx), %edx
	je	.L1508
	cmpl	%edx, %r14d
	jle	.L1694
	movslq	%edx, %rsi
	movq	%rsi, %rax
	jmp	.L1530
	.p2align 4,,10
	.p2align 3
.L1711:
	addq	$1, %rax
	cmpl	%eax, %r14d
	jle	.L1529
.L1530:
	cmpb	$62, (%r12,%rax)
	jne	.L1711
	cmpl	$-1, %eax
	movl	%eax, -200(%rbp)
	je	.L1529
	subq	%rsi, %rax
	leaq	(%r12,%rsi), %rdx
	leaq	-96(%rbp), %rdi
	movdqa	-176(%rbp), %xmm6
	movq	-152(%rbp), %rsi
	movl	%r9d, -192(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rdi, -208(%rbp)
	movaps	%xmm6, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_118LookupNamedCaptureERKSt8functionIFbNS0_6StringEEENS0_10FixedArrayE
	movl	-192(%rbp), %r9d
	movl	-200(%rbp), %r8d
	movl	%eax, %r10d
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L1531
	movq	-208(%rbp), %rdi
	movl	%r9d, -216(%rbp)
	movl	$3, %edx
	movl	%r10d, -192(%rbp)
	movq	%rdi, %rsi
	call	*%rax
	movl	-216(%rbp), %r9d
	movl	-200(%rbp), %r8d
	movl	-192(%rbp), %r10d
.L1531:
	cmpl	%r9d, %ebx
	jg	.L1712
.L1532:
	movl	$3, %eax
	cmpl	$-1, %r10d
	jne	.L1533
	xorl	%r10d, %r10d
	movl	$6, %eax
.L1533:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%r8d, -192(%rbp)
	movl	%r10d, -100(%rbp)
	movl	%eax, -104(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	movl	-192(%rbp), %r8d
	leaq	.L1513(%rip), %rcx
	leal	1(%r8), %r9d
	movl	%r9d, %edx
	jmp	.L1508
.L1514:
	leal	-48(%rsi), %eax
	leal	2(%rbx), %edx
	cmpl	%eax, -136(%rbp)
	jl	.L1508
	cmpl	%edx, %r14d
	jle	.L1524
	movslq	%edx, %rsi
	movzbl	(%r12,%rsi), %esi
	leal	-48(%rsi), %edi
	cmpb	$9, %dil
	ja	.L1525
	leal	(%rax,%rax,4), %edi
	leal	-48(%rsi,%rdi,2), %esi
	cmpl	%esi, -136(%rbp)
	jl	.L1525
	leal	3(%rbx), %edx
	testl	%esi, %esi
	je	.L1508
	movl	%esi, %eax
	.p2align 4,,10
	.p2align 3
.L1603:
	cmpl	%r9d, %ebx
	jg	.L1713
.L1526:
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%edx, -192(%rbp)
	movl	$3, -104(%rbp)
	movl	%eax, -100(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	movl	-192(%rbp), %edx
	leaq	.L1513(%rip), %rcx
	movl	%edx, %r9d
	jmp	.L1508
.L1515:
	cmpl	%r9d, %ebx
	jg	.L1714
.L1521:
	movl	-160(%rbp), %eax
	movl	$2, -104(%rbp)
	movl	%eax, -100(%rbp)
.L1685:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	leal	2(%rbx), %r9d
	leaq	.L1513(%rip), %rcx
	movl	%r9d, %edx
	jmp	.L1508
.L1516:
	cmpl	%r9d, %ebx
	jg	.L1715
.L1522:
	movq	$3, -104(%rbp)
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1510:
	cmpb	$96, %sil
	jne	.L1686
	cmpl	%r9d, %ebx
	jg	.L1716
.L1520:
	movq	$1, -104(%rbp)
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1576:
	movq	40(%r13), %r8
	movq	%rdx, %r9
	subq	%r8, %r9
	movq	%r9, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1591
	testq	%rax, %rax
	je	.L1609
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1717
	movq	$2147483640, -136(%rbp)
	movl	$2147483640, %esi
.L1579:
	movq	32(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r10
	subq	%rax, %r10
	cmpq	%rsi, %r10
	jb	.L1718
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1582:
	movq	-136(%rbp), %rsi
	leaq	8(%rax), %rdi
	addq	%rax, %rsi
.L1580:
	movq	%rcx, (%rax,%r9)
	cmpq	%r8, %rdx
	je	.L1583
	subq	$8, %rdx
	leaq	15(%rax), %rcx
	subq	%r8, %rdx
	subq	%r8, %rcx
	movq	%rdx, %r9
	shrq	$3, %r9
	cmpq	$30, %rcx
	jbe	.L1612
	movabsq	$2305843009213693948, %rdi
	testq	%rdi, %r9
	je	.L1612
	addq	$1, %r9
	xorl	%ecx, %ecx
	movq	%r9, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1585:
	movdqu	(%r8,%rcx), %xmm2
	movups	%xmm2, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rdi
	jne	.L1585
	movq	%r9, %rcx
	andq	$-2, %rcx
	leaq	0(,%rcx,8), %rdi
	addq	%rdi, %r8
	addq	%rax, %rdi
	cmpq	%r9, %rcx
	je	.L1587
	movq	(%r8), %rcx
	movq	%rcx, (%rdi)
.L1587:
	leaq	16(%rax,%rdx), %rdi
.L1583:
	movq	%rax, %xmm0
	movq	%rdi, %xmm7
	movq	%rsi, 56(%r13)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 40(%r13)
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1699:
	movq	-120(%rbp), %rcx
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1706:
	movzwl	4(%r13,%rdx), %edx
	leal	-48(%rdx), %esi
	cmpw	$9, %si
	ja	.L1555
	leal	(%rax,%rax,4), %esi
	leal	-48(%rdx,%rsi,2), %edx
	cmpl	%edx, -136(%rbp)
	jl	.L1555
	leal	3(%r12), %ebx
	testl	%edx, %edx
	je	.L1538
	movl	%edx, %eax
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1702:
	negl	%r9d
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -176(%rbp)
	movl	%r9d, -104(%rbp)
	movl	%r12d, -100(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	movq	$1, -104(%rbp)
	movq	-176(%rbp), %r8
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1609:
	movq	$8, -136(%rbp)
	movl	$8, %esi
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1519:
	negl	%r9d
	movl	%edx, -100(%rbp)
	movl	%r9d, -104(%rbp)
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1715:
	negl	%r9d
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%ebx, -100(%rbp)
	movl	%r9d, -104(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1714:
	negl	%r9d
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%ebx, -100(%rbp)
	movl	%r9d, -104(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1716:
	negl	%r9d
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%ebx, -100(%rbp)
	movl	%r9d, -104(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	jmp	.L1520
.L1525:
	testl	%eax, %eax
	jne	.L1603
	movl	%edx, %ebx
	jmp	.L1719
	.p2align 4,,10
	.p2align 3
.L1555:
	testl	%eax, %eax
	jne	.L1604
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1717:
	testq	%rsi, %rsi
	jne	.L1720
	movl	$8, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	40(%r13), %rcx
	movq	%rdx, %r8
	subq	%rcx, %r8
	movq	%r8, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L1591
	testq	%rax, %rax
	je	.L1613
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1721
	movl	$2147483640, %r10d
	movl	$2147483640, %r9d
.L1592:
	movq	32(%r13), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	%r10, %rax
	jb	.L1722
	addq	%rsi, %r10
	movq	%r10, 16(%rdi)
.L1595:
	leaq	(%rsi,%r9), %rdi
	leaq	8(%rsi), %rax
.L1593:
	movq	-120(%rbp), %r10
	movq	%r10, (%rsi,%r8)
	cmpq	%rcx, %rdx
	je	.L1596
	subq	$8, %rdx
	leaq	15(%rsi), %rax
	subq	%rcx, %rdx
	subq	%rcx, %rax
	movq	%rdx, %r9
	shrq	$3, %r9
	cmpq	$30, %rax
	jbe	.L1616
	movabsq	$2305843009213693948, %rax
	testq	%rax, %r9
	je	.L1616
	addq	$1, %r9
	xorl	%eax, %eax
	movq	%r9, %r8
	shrq	%r8
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L1598:
	movdqu	(%rcx,%rax), %xmm4
	movups	%xmm4, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%r8, %rax
	jne	.L1598
	movq	%r9, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	addq	%r8, %rcx
	addq	%rsi, %r8
	cmpq	%r9, %rax
	je	.L1600
	movq	(%rcx), %rax
	movq	%rax, (%r8)
.L1600:
	leaq	16(%rsi,%rdx), %rax
.L1596:
	movq	%rsi, %xmm0
	movq	%rax, %xmm7
	movq	%rdi, 56(%r13)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 40(%r13)
	jmp	.L1590
	.p2align 4,,10
	.p2align 3
.L1708:
	negl	%r9d
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -176(%rbp)
	movl	%r9d, -104(%rbp)
	movl	%r12d, -100(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	movq	-176(%rbp), %r8
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1549:
	negl	%r9d
	movl	%ebx, -100(%rbp)
	movl	%r9d, -104(%rbp)
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1709:
	negl	%r9d
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -176(%rbp)
	movl	%r9d, -104(%rbp)
	movl	%r12d, -100(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	movq	-176(%rbp), %r8
	jmp	.L1552
.L1721:
	testq	%rsi, %rsi
	jne	.L1723
	movl	$8, %eax
	xorl	%edi, %edi
	xorl	%esi, %esi
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1524:
	testl	%eax, %eax
	jne	.L1603
	jmp	.L1694
.L1707:
	negl	%r9d
	movq	%r8, %rsi
	movq	%r15, %rdi
	movl	%eax, -200(%rbp)
	movq	%r8, -176(%rbp)
	movl	%r9d, -104(%rbp)
	movl	%r12d, -100(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	movl	-200(%rbp), %eax
	movq	-176(%rbp), %r8
	jmp	.L1556
.L1613:
	movl	$8, %r10d
	movl	$8, %r9d
	jmp	.L1592
.L1705:
	xorl	%ebx, %ebx
	movl	$6, %eax
	jmp	.L1563
.L1704:
	negl	%r9d
	movq	%r8, %rsi
	movq	%r15, %rdi
	movl	%r10d, -200(%rbp)
	movq	%r8, -176(%rbp)
	movl	%r9d, -104(%rbp)
	movl	%r12d, -100(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	movl	-200(%rbp), %r10d
	movq	-176(%rbp), %r8
	jmp	.L1562
.L1612:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1584:
	movq	(%r8,%rcx,8), %rdi
	movq	%rdi, (%rax,%rcx,8)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%r9, %rdi
	jne	.L1584
	jmp	.L1587
.L1616:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	(%rcx,%rax,8), %r8
	movq	%r8, (%rsi,%rax,8)
	movq	%rax, %r8
	addq	$1, %rax
	cmpq	%r9, %r8
	jne	.L1597
	jmp	.L1600
.L1713:
	negl	%r9d
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%eax, -200(%rbp)
	movl	%edx, -192(%rbp)
	movl	%r9d, -104(%rbp)
	movl	%ebx, -100(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	movl	-200(%rbp), %eax
	movl	-192(%rbp), %edx
	jmp	.L1526
.L1718:
	movl	%r11d, -208(%rbp)
	movq	%r9, -200(%rbp)
	movq	%r8, -192(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rcx, -160(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-160(%rbp), %rcx
	movq	-176(%rbp), %rdx
	movq	-192(%rbp), %r8
	movq	-200(%rbp), %r9
	movl	-208(%rbp), %r11d
	jmp	.L1582
.L1712:
	negl	%r9d
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%r8d, -200(%rbp)
	movl	%r10d, -192(%rbp)
	movl	%r9d, -104(%rbp)
	movl	%ebx, -100(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEE9push_backERKS3_
	movl	-200(%rbp), %r8d
	movl	-192(%rbp), %r10d
	jmp	.L1532
.L1722:
	movq	%r10, %rsi
	movl	%r11d, -200(%rbp)
	movq	%r9, -192(%rbp)
	movq	%r8, -176(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-136(%rbp), %rdx
	movq	-160(%rbp), %rcx
	movq	-176(%rbp), %r8
	movq	-192(%rbp), %r9
	movq	%rax, %rsi
	movl	-200(%rbp), %r11d
	jmp	.L1595
.L1701:
	call	__stack_chk_fail@PLT
.L1723:
	movl	$268435455, %r9d
	cmpq	$268435455, %rsi
	cmova	%r9, %rsi
	leaq	0(,%rsi,8), %r9
	movq	%r9, %r10
	jmp	.L1592
.L1720:
	movl	$268435455, %eax
	cmpq	$268435455, %rsi
	cmova	%rax, %rsi
	leaq	0(,%rsi,8), %rax
	movq	%rax, -136(%rbp)
	movq	%rax, %rsi
	jmp	.L1579
.L1591:
	leaq	.LC29(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20192:
	.size	_ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii, .-_ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB23811:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L1738
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1734
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1739
.L1726:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1733:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1740
	testq	%r13, %r13
	jg	.L1729
	testq	%r9, %r9
	jne	.L1732
.L1730:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1740:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1729
.L1732:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1730
	.p2align 4,,10
	.p2align 3
.L1739:
	testq	%rsi, %rsi
	jne	.L1727
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1729:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1730
	jmp	.L1732
	.p2align 4,,10
	.p2align 3
.L1734:
	movl	$4, %r14d
	jmp	.L1726
.L1738:
	leaq	.LC29(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1727:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L1726
	.cfi_endproc
.LFE23811:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZN2v88internal17FindStringIndicesIhtEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj,"axG",@progbits,_ZN2v88internal17FindStringIndicesIhtEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj,comdat
	.p2align 4
	.weak	_ZN2v88internal17FindStringIndicesIhtEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	.type	_ZN2v88internal17FindStringIndicesIhtEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj, @function
_ZN2v88internal17FindStringIndicesIhtEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj:
.LFB22423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$0, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	16(%rbp), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-250(%r8), %eax
	movl	%r8d, -116(%rbp)
	testl	%eax, %eax
	movl	$0, -100(%rbp)
	cmovs	%edx, %eax
	movq	%rdi, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movl	%eax, -64(%rbp)
	movslq	%r8d, %rax
	cmpq	$7, %rax
	leaq	(%rcx,%rax,2), %rdx
	movq	%r8, -80(%rbp)
	movq	%rcx, %rax
	jbe	.L1782
	testb	$7, %cl
	jne	.L1743
	.p2align 4,,10
	.p2align 3
.L1747:
	leaq	16(%rax), %rsi
	cmpq	%rsi, %rdx
	jb	.L1782
	leaq	-16(%rdx), %rsi
	subq	%rax, %rsi
	shrq	$3, %rsi
	leaq	8(%rax,%rsi,8), %rdi
	movabsq	$-71777214294589696, %rsi
	jmp	.L1751
	.p2align 4,,10
	.p2align 3
.L1748:
	addq	$8, %rax
	cmpq	%rax, %rdi
	je	.L1782
.L1751:
	testq	%rsi, (%rax)
	je	.L1748
	cmpq	%rax, %rdx
	jbe	.L1749
	.p2align 4,,10
	.p2align 3
.L1750:
	cmpw	$255, (%rax)
	ja	.L1749
	addq	$2, %rax
.L1782:
	cmpq	%rax, %rdx
	ja	.L1750
.L1749:
	subq	%rcx, %rax
	sarq	%rax
	cmpl	%eax, %r8d
	jg	.L1784
.L1753:
	cmpl	$6, %r8d
	jle	.L1785
	leaq	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
.L1754:
	testl	%ebx, %ebx
	je	.L1741
	leaq	-100(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-96(%rbp), %r14
	movq	%rdi, -128(%rbp)
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1786:
	movl	%eax, (%rsi)
	movl	-116(%rbp), %ecx
	addl	-100(%rbp), %ecx
	addq	$4, 8(%r15)
	movl	%ecx, -100(%rbp)
	subl	$1, %ebx
	je	.L1741
.L1787:
	movq	-72(%rbp), %rax
.L1761:
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, -100(%rbp)
	testl	%eax, %eax
	js	.L1741
	movq	8(%r15), %rsi
	cmpq	16(%r15), %rsi
	jne	.L1786
	movq	-128(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movl	-116(%rbp), %ecx
	addl	-100(%rbp), %ecx
	movl	%ecx, -100(%rbp)
	subl	$1, %ebx
	jne	.L1787
	.p2align 4,,10
	.p2align 3
.L1741:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1788
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1745:
	.cfi_restore_state
	addq	$2, %rax
	testb	$7, %al
	je	.L1747
.L1743:
	cmpw	$255, (%rax)
	jbe	.L1745
	subq	%rcx, %rax
	sarq	%rax
	cmpl	%eax, %r8d
	jle	.L1753
.L1784:
	leaq	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1785:
	cmpl	$1, %r8d
	je	.L1789
	leaq	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1789:
	leaq	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1754
.L1788:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22423:
	.size	_ZN2v88internal17FindStringIndicesIhtEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj, .-_ZN2v88internal17FindStringIndicesIhtEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	.section	.text._ZN2v88internal17FindStringIndicesIthEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj,"axG",@progbits,_ZN2v88internal17FindStringIndicesIthEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj,comdat
	.p2align 4
	.weak	_ZN2v88internal17FindStringIndicesIthEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	.type	_ZN2v88internal17FindStringIndicesIthEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj, @function
_ZN2v88internal17FindStringIndicesIthEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj:
.LFB22424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$0, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	16(%rbp), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-250(%r8), %eax
	movl	%r8d, -116(%rbp)
	testl	%eax, %eax
	movl	$0, -100(%rbp)
	cmovs	%edx, %eax
	movq	%rdi, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movl	%eax, -64(%rbp)
	cmpl	$6, %r8d
	jg	.L1791
	cmpl	$1, %r8d
	je	.L1812
	leaq	_ZN2v88internal12StringSearchIhtE12LinearSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1791:
	leaq	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
.L1793:
	testl	%ebx, %ebx
	je	.L1790
	leaq	-100(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-96(%rbp), %r14
	movq	%rdi, -128(%rbp)
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1813:
	movl	%eax, (%rsi)
	movl	-116(%rbp), %ecx
	addl	-100(%rbp), %ecx
	addq	$4, 8(%r15)
	movl	%ecx, -100(%rbp)
	subl	$1, %ebx
	je	.L1790
.L1814:
	movq	-72(%rbp), %rax
.L1795:
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, -100(%rbp)
	testl	%eax, %eax
	js	.L1790
	movq	8(%r15), %rsi
	cmpq	16(%r15), %rsi
	jne	.L1813
	movq	-128(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movl	-116(%rbp), %ecx
	addl	-100(%rbp), %ecx
	movl	%ecx, -100(%rbp)
	subl	$1, %ebx
	jne	.L1814
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1815
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1812:
	.cfi_restore_state
	leaq	_ZN2v88internal12StringSearchIhtE16SingleCharSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1793
.L1815:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22424:
	.size	_ZN2v88internal17FindStringIndicesIthEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj, .-_ZN2v88internal17FindStringIndicesIthEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	.section	.text._ZN2v88internal17FindStringIndicesIhhEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj,"axG",@progbits,_ZN2v88internal17FindStringIndicesIhhEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj,comdat
	.p2align 4
	.weak	_ZN2v88internal17FindStringIndicesIhhEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	.type	_ZN2v88internal17FindStringIndicesIhhEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj, @function
_ZN2v88internal17FindStringIndicesIhhEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj:
.LFB22422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$0, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	16(%rbp), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-250(%r8), %eax
	movl	%r8d, -116(%rbp)
	testl	%eax, %eax
	movl	$0, -100(%rbp)
	cmovs	%edx, %eax
	movq	%rdi, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movl	%eax, -64(%rbp)
	cmpl	$6, %r8d
	jg	.L1817
	cmpl	$1, %r8d
	je	.L1838
	leaq	_ZN2v88internal12StringSearchIhhE12LinearSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1819
	.p2align 4,,10
	.p2align 3
.L1817:
	leaq	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
.L1819:
	testl	%ebx, %ebx
	je	.L1816
	leaq	-100(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-96(%rbp), %r14
	movq	%rdi, -128(%rbp)
	jmp	.L1821
	.p2align 4,,10
	.p2align 3
.L1839:
	movl	%eax, (%rsi)
	movl	-116(%rbp), %ecx
	addl	-100(%rbp), %ecx
	addq	$4, 8(%r15)
	movl	%ecx, -100(%rbp)
	subl	$1, %ebx
	je	.L1816
.L1840:
	movq	-72(%rbp), %rax
.L1821:
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, -100(%rbp)
	testl	%eax, %eax
	js	.L1816
	movq	8(%r15), %rsi
	cmpq	16(%r15), %rsi
	jne	.L1839
	movq	-128(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movl	-116(%rbp), %ecx
	addl	-100(%rbp), %ecx
	movl	%ecx, -100(%rbp)
	subl	$1, %ebx
	jne	.L1840
	.p2align 4,,10
	.p2align 3
.L1816:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1841
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1838:
	.cfi_restore_state
	leaq	_ZN2v88internal12StringSearchIhhE16SingleCharSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1819
.L1841:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22422:
	.size	_ZN2v88internal17FindStringIndicesIhhEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj, .-_ZN2v88internal17FindStringIndicesIhhEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	.section	.text._ZN2v88internal17FindStringIndicesIttEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj,"axG",@progbits,_ZN2v88internal17FindStringIndicesIttEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj,comdat
	.p2align 4
	.weak	_ZN2v88internal17FindStringIndicesIttEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	.type	_ZN2v88internal17FindStringIndicesIttEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj, @function
_ZN2v88internal17FindStringIndicesIttEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj:
.LFB22426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$0, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	16(%rbp), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-250(%r8), %eax
	movl	%r8d, -116(%rbp)
	testl	%eax, %eax
	movl	$0, -100(%rbp)
	cmovs	%edx, %eax
	movq	%rdi, -96(%rbp)
	movq	%rcx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movl	%eax, -64(%rbp)
	cmpl	$6, %r8d
	jg	.L1843
	cmpl	$1, %r8d
	je	.L1864
	leaq	_ZN2v88internal12StringSearchIttE12LinearSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1843:
	leaq	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
.L1845:
	testl	%ebx, %ebx
	je	.L1842
	leaq	-100(%rbp), %rdi
	xorl	%ecx, %ecx
	leaq	-96(%rbp), %r14
	movq	%rdi, -128(%rbp)
	jmp	.L1847
	.p2align 4,,10
	.p2align 3
.L1865:
	movl	%eax, (%rsi)
	movl	-116(%rbp), %ecx
	addl	-100(%rbp), %ecx
	addq	$4, 8(%r15)
	movl	%ecx, -100(%rbp)
	subl	$1, %ebx
	je	.L1842
.L1866:
	movq	-72(%rbp), %rax
.L1847:
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, -100(%rbp)
	testl	%eax, %eax
	js	.L1842
	movq	8(%r15), %rsi
	cmpq	16(%r15), %rsi
	jne	.L1865
	movq	-128(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movl	-116(%rbp), %ecx
	addl	-100(%rbp), %ecx
	movl	%ecx, -100(%rbp)
	subl	$1, %ebx
	jne	.L1866
	.p2align 4,,10
	.p2align 3
.L1842:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1867
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1864:
	.cfi_restore_state
	leaq	_ZN2v88internal12StringSearchIttE16SingleCharSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L1845
.L1867:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22426:
	.size	_ZN2v88internal17FindStringIndicesIttEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj, .-_ZN2v88internal17FindStringIndicesIttEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	.section	.text._ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m,"axG",@progbits,_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m,comdat
	.p2align 4
	.weak	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	.type	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m, @function
_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m:
.LFB23857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	$-1, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	0(,%rdi,8), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmova	%rax, %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L1870
	movq	%rbx, %rsi
	subq	$1, %rsi
	js	.L1868
	leaq	-2(%rbx), %rdx
	movl	$1, %edi
	cmpq	$-1, %rdx
	cmovge	%rbx, %rdi
	cmpq	$1, %rbx
	je	.L1883
	cmpq	$-1, %rdx
	jl	.L1883
	movq	%rdi, %rsi
	xorl	%edx, %edx
	pxor	%xmm0, %xmm0
	shrq	%rsi
	.p2align 4,,10
	.p2align 3
.L1874:
	movq	%rdx, %rcx
	addq	$1, %rdx
	salq	$4, %rcx
	movups	%xmm0, (%rax,%rcx)
	cmpq	%rdx, %rsi
	jne	.L1874
	movq	%rdi, %rcx
	andq	$-2, %rcx
	leaq	(%rax,%rcx,8), %rdx
	cmpq	%rcx, %rdi
	je	.L1868
.L1872:
	movq	$0, (%rdx)
.L1868:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1883:
	.cfi_restore_state
	movq	%rax, %rdx
	jmp	.L1872
.L1870:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L1887
	movq	%rbx, %rdi
	subq	$1, %rdi
	js	.L1868
	leaq	-2(%rbx), %rcx
	movl	$1, %edx
	addq	$1, %rcx
	cmovge	%rbx, %rdx
	jl	.L1884
	subq	$1, %rbx
	je	.L1884
	movq	%rdx, %rsi
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	shrq	%rsi
.L1878:
	movq	%rcx, %rdi
	addq	$1, %rcx
	salq	$4, %rdi
	movups	%xmm0, (%rax,%rdi)
	cmpq	%rcx, %rsi
	jne	.L1878
	movq	%rdx, %rsi
	andq	$-2, %rsi
	leaq	(%rax,%rsi,8), %rcx
	cmpq	%rsi, %rdx
	je	.L1868
.L1876:
	movq	$0, (%rcx)
	jmp	.L1868
.L1887:
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
.L1884:
	movq	%rax, %rcx
	jmp	.L1876
	.cfi_endproc
.LFE23857:
	.size	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m, .-_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	.section	.rodata._ZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"V8.Runtime_Runtime_StringReplaceNonGlobalRegExpWithFunction"
	.section	.rodata._ZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC31:
	.string	"args[0].IsString()"
.LC32:
	.string	"args[1].IsJSRegExp()"
.LC33:
	.string	"args[2].IsJSReceiver()"
	.section	.text._ZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE.isra.0:
.LFB25974:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -192(%rbp)
	movq	$0, -160(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2010
.L1889:
	movq	_ZZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1408(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2011
.L1891:
	movq	$0, -224(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2012
.L1893:
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	%rax, -264(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -272(%rbp)
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L2013
.L1897:
	leaq	.LC31(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2011:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2014
.L1892:
	movq	%rbx, _ZZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1408(%rip)
	jmp	.L1891
	.p2align 4,,10
	.p2align 3
.L2013:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1897
	movq	-8(%r12), %rax
	leaq	-8(%r12), %r14
	testb	$1, %al
	jne	.L2015
.L1898:
	leaq	.LC32(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2012:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2016
.L1894:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1895
	movq	(%rdi), %rax
	call	*8(%rax)
.L1895:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1896
	movq	(%rdi), %rax
	call	*8(%rax)
.L1896:
	leaq	.LC30(%rip), %rax
	movq	%rbx, -216(%rbp)
	movq	%rax, -208(%rbp)
	leaq	-216(%rbp), %rax
	movq	%r13, -200(%rbp)
	movq	%rax, -224(%rbp)
	jmp	.L1893
	.p2align 4,,10
	.p2align 3
.L2015:
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	jne	.L1898
	movq	-16(%r12), %rdx
	leaq	-16(%r12), %rax
	movq	%rax, -296(%rbp)
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	movb	%al, -288(%rbp)
	je	.L2017
.L1900:
	leaq	.LC33(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2017:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1900
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	1055(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2018
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L1904:
	movq	(%r14), %rax
	movq	23(%rax), %rdx
	movq	31(%rdx), %rbx
	sarq	$32, %rbx
	andl	$8, %ebx
	je	.L1918
	movq	41112(%r15), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1908
	movq	%r8, -280(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-280(%rbp), %r8
	movq	(%rax), %rsi
	movq	%rax, %r9
	testb	$1, %sil
	jne	.L1911
.L2029:
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r15), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L1912
	movq	%r8, -280(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-280(%rbp), %r8
.L1913:
	testq	%rax, %rax
	je	.L2009
.L1915:
	movq	(%rax), %rcx
	testb	$1, %cl
	jne	.L1917
	sarq	$32, %rcx
	testq	%rcx, %rcx
	jg	.L1907
	.p2align 4,,10
	.p2align 3
.L1918:
	xorl	%ecx, %ecx
.L1907:
	movq	41112(%r15), %rdi
	movq	104(%r15), %rsi
	testq	%rdi, %rdi
	je	.L1921
	movq	%r8, -304(%rbp)
	movl	%ecx, -280(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-280(%rbp), %ecx
	movq	-304(%rbp), %r8
	movq	%rax, %r13
.L1922:
	movq	(%r12), %rax
	cmpl	%ecx, 11(%rax)
	jb	.L1924
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6RegExp4ExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2009
.L1924:
	movq	0(%r13), %rax
	cmpq	104(%r15), %rax
	jne	.L1926
	testl	%ebx, %ebx
	jne	.L2019
.L1927:
	movq	(%r12), %r12
.L1916:
	movq	-264(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-272(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L1963
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1963:
	leaq	-224(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2020
.L1888:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2021
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2018:
	.cfi_restore_state
	movq	-264(%rbp), %rax
	movq	%rax, %r8
	cmpq	41096(%r15), %rax
	je	.L2022
.L1905:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r8)
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L1921:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L2023
.L1923:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L1922
	.p2align 4,,10
	.p2align 3
.L1926:
	movq	39(%rax), %rsi
	sarq	$32, %rsi
	movq	%rsi, -304(%rbp)
	movq	47(%rax), %rax
	sarq	$32, %rax
	movq	%rax, -336(%rbp)
	testl	%ebx, %ebx
	jne	.L2024
.L1928:
	leaq	-144(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	(%r12), %rax
	movl	-304(%rbp), %ecx
	movq	%r12, %rsi
	cmpl	11(%rax), %ecx
	je	.L1930
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %rsi
.L1930:
	movq	-312(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	0(%r13), %rax
	movq	15(%rax), %rbx
	movq	$0, -352(%rbp)
	movq	%rbx, %rdx
	shrq	$63, %rbx
	sarq	$32, %rdx
	addl	%edx, %ebx
	sarl	%ebx
	movl	%ebx, -280(%rbp)
	cmpq	$3, %rdx
	jg	.L2025
.L1931:
	cmpl	$65534, -280(%rbp)
	ja	.L1938
	movl	-280(%rbp), %eax
	addl	$2, %eax
	movl	%eax, -324(%rbp)
.L1940:
	movl	-324(%rbp), %eax
	movq	%rdx, -344(%rbp)
	cmpl	$65534, %eax
	ja	.L1938
	movl	%eax, %r14d
	movq	%r14, %rdi
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	movq	-344(%rbp), %rdx
	movq	%r14, -232(%rbp)
	movq	%rax, -240(%rbp)
	leaq	88(%r15), %rax
	movq	%rax, -320(%rbp)
	cmpq	$1, %rdx
	jle	.L1968
	xorl	%ebx, %ebx
	leaq	-241(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L1945:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	cmpb	$0, -241(%rbp)
	je	.L1942
	movq	-240(%rbp), %rdx
	movq	%rax, (%rdx,%rbx,8)
	addq	$1, %rbx
	cmpl	%ebx, -280(%rbp)
	jg	.L1945
.L1943:
	movl	-280(%rbp), %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	cmovle	%eax, %ecx
	movslq	%ecx, %r14
	movq	%r14, %rbx
	salq	$3, %r14
	leaq	8(%r14), %rdx
	addl	$2, %ebx
.L1941:
	movq	-304(%rbp), %rsi
	movq	41112(%r15), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L1946
	movq	%rdx, -280(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-280(%rbp), %rdx
.L1947:
	movq	-240(%rbp), %rcx
	cmpb	$0, -288(%rbp)
	movq	%rax, (%rcx,%r14)
	movq	-240(%rbp), %rax
	movq	%r12, (%rax,%rdx)
	jne	.L2026
.L1949:
	movq	-296(%rbp), %rsi
	movl	-324(%rbp), %ecx
	movq	%r15, %rdi
	movq	-240(%rbp), %r8
	movq	-320(%rbp), %rdx
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2008
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L1953
.L1956:
	movq	%r15, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2008
.L1955:
	movq	-312(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	(%r12), %rax
	movl	11(%rax), %ecx
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	jne	.L2027
.L1958:
	movq	-312(%rbp), %rbx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	je	.L2008
	movq	(%rax), %r12
.L1952:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1916
	call	_ZdaPv@PLT
	jmp	.L1916
	.p2align 4,,10
	.p2align 3
.L1908:
	movq	41088(%r15), %r9
	cmpq	41096(%r15), %r9
	je	.L2028
.L1910:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r9)
	testb	$1, %sil
	je	.L2029
.L1911:
	movq	%r9, %rsi
	movq	%r15, %rdi
	movq	%r8, -280(%rbp)
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-280(%rbp), %r8
	jmp	.L1913
	.p2align 4,,10
	.p2align 3
.L2025:
	movq	(%r14), %rax
	movq	23(%rax), %rax
	movq	87(%rax), %rsi
	testb	$1, %sil
	je	.L1931
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L1931
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1934
	movq	%rdx, -288(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-288(%rbp), %rdx
.L1935:
	movq	%rax, -352(%rbp)
	movl	-280(%rbp), %eax
	movb	$1, -288(%rbp)
	leal	3(%rax), %ecx
	movl	%ecx, -324(%rbp)
	cmpl	$65534, %eax
	jbe	.L1940
	.p2align 4,,10
	.p2align 3
.L1938:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$305, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r12
	jmp	.L1916
	.p2align 4,,10
	.p2align 3
.L2024:
	movq	%rax, %rdx
	movq	(%r14), %rax
	salq	$32, %rdx
	movq	%rdx, 47(%rax)
	jmp	.L1928
	.p2align 4,,10
	.p2align 3
.L2027:
	movq	%r12, %rsi
	movl	%eax, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %r12
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L1942:
	movq	-240(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	%rcx, (%rax,%rbx,8)
	addq	$1, %rbx
	cmpl	%ebx, -280(%rbp)
	jg	.L1945
	jmp	.L1943
	.p2align 4,,10
	.p2align 3
.L1917:
	movsd	7(%rcx), %xmm0
	comisd	.LC21(%rip), %xmm0
	jb	.L1918
	movsd	.LC22(%rip), %xmm1
	movl	$-1, %ecx
	comisd	%xmm0, %xmm1
	jbe	.L1907
	cvttsd2siq	%xmm0, %rcx
	jmp	.L1907
	.p2align 4,,10
	.p2align 3
.L2019:
	movq	(%r14), %rax
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rdx
	movq	%rdx, 47(%rax)
	jmp	.L1927
	.p2align 4,,10
	.p2align 3
.L2009:
	movq	312(%r15), %r12
	jmp	.L1916
	.p2align 4,,10
	.p2align 3
.L1912:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L2030
.L1914:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L1915
	.p2align 4,,10
	.p2align 3
.L2010:
	movq	40960(%rsi), %rax
	movl	$499, %edx
	leaq	-184(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1889
	.p2align 4,,10
	.p2align 3
.L2020:
	leaq	-184(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1888
	.p2align 4,,10
	.p2align 3
.L2016:
	subq	$8, %rsp
	leaq	-96(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC30(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1894
	.p2align 4,,10
	.p2align 3
.L2014:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1892
	.p2align 4,,10
	.p2align 3
.L2022:
	movq	%r15, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1905
	.p2align 4,,10
	.p2align 3
.L2023:
	movq	%r15, %rdi
	movq	%r8, -312(%rbp)
	movq	%rsi, -304(%rbp)
	movl	%ecx, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %r8
	movq	-304(%rbp), %rsi
	movl	-280(%rbp), %ecx
	movq	%rax, %r13
	jmp	.L1923
	.p2align 4,,10
	.p2align 3
.L1946:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L2031
.L1948:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L1947
	.p2align 4,,10
	.p2align 3
.L2008:
	movq	312(%r15), %r12
	jmp	.L1952
	.p2align 4,,10
	.p2align 3
.L2026:
	leaq	-240(%rbp), %rax
	leaq	-96(%rbp), %r14
	movq	%r15, %rdi
	movslq	%ebx, %rbx
	movq	%rax, -96(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internalL58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS2_9ArgumentsEPNS2_7IsolateEEUliE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation(%rip), %rcx
	leaq	_ZNSt17_Function_handlerIFN2v88internal6ObjectEiEZNS1_L58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS1_9ArgumentsEPNS1_7IsolateEEUliE_E9_M_invokeERKSt9_Any_dataOi(%rip), %rax
	movq	%r14, %rdx
	movq	%rcx, %xmm0
	movq	%rax, %xmm2
	movq	-352(%rbp), %rsi
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_133ConstructNamedCaptureGroupsObjectEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEERKSt8functionIFNS0_6ObjectEiEE
	movq	-240(%rbp), %rdx
	movq	%rax, (%rdx,%rbx,8)
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L1949
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1949
	.p2align 4,,10
	.p2align 3
.L2028:
	movq	%r15, %rdi
	movq	%rsi, -304(%rbp)
	movq	%r8, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-304(%rbp), %rsi
	movq	-280(%rbp), %r8
	movq	%rax, %r9
	jmp	.L1910
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1956
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L2030:
	movq	%r15, %rdi
	movq	%rsi, -304(%rbp)
	movq	%r8, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-304(%rbp), %rsi
	movq	-280(%rbp), %r8
	jmp	.L1914
	.p2align 4,,10
	.p2align 3
.L1968:
	movl	$8, %edx
	movl	$2, %ebx
	xorl	%r14d, %r14d
	jmp	.L1941
.L2031:
	movq	%r15, %rdi
	movq	%rdx, -304(%rbp)
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-304(%rbp), %rdx
	movq	-280(%rbp), %rsi
	jmp	.L1948
.L1934:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L2032
.L1936:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L1935
.L2032:
	movq	%r15, %rdi
	movq	%rsi, -320(%rbp)
	movq	%rdx, -288(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	movq	-288(%rbp), %rdx
	jmp	.L1936
.L2021:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25974:
	.size	_ZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal48Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal48Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal48Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal48Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE:
.LFB20255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r15d
	testl	%r15d, %r15d
	jne	.L2132
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -184(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -192(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2133
.L2036:
	leaq	.LC31(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2133:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2036
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r13
	testb	$1, %al
	jne	.L2134
.L2037:
	leaq	.LC32(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2134:
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	jne	.L2037
	movq	-16(%rsi), %rdx
	leaq	-16(%rsi), %rax
	movq	%rax, -208(%rbp)
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	movb	%al, -200(%rbp)
	je	.L2135
.L2039:
	leaq	.LC33(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2135:
	movq	-1(%rdx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2039
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	movq	1055(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2136
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L2043:
	movq	0(%r13), %rax
	movq	23(%rax), %rdx
	movq	31(%rdx), %rbx
	sarq	$32, %rbx
	andl	$8, %ebx
	je	.L2046
	movq	41112(%r14), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2047
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-216(%rbp), %r8
	movq	(%rax), %rsi
	movq	%rax, %r11
	testb	$1, %sil
	jne	.L2050
.L2142:
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r14), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L2051
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-216(%rbp), %r8
.L2052:
	testq	%rax, %rax
	je	.L2131
.L2054:
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L2056
	sarq	$32, %rax
	testq	%rax, %rax
	cmovg	%eax, %r15d
.L2046:
	movq	41112(%r14), %rdi
	movq	104(%r14), %rsi
	testq	%rdi, %rdi
	je	.L2060
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-216(%rbp), %r8
	movq	%rax, %r11
.L2061:
	movq	(%r12), %rax
	cmpl	%r15d, 11(%rax)
	jb	.L2063
	movl	%r15d, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6RegExp4ExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L2131
.L2063:
	movq	(%r11), %rax
	cmpq	104(%r14), %rax
	jne	.L2065
	testl	%ebx, %ebx
	jne	.L2137
.L2066:
	movq	(%r12), %r13
.L2055:
	movq	-184(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-192(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L2033
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2033:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2138
	addq	$232, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2136:
	.cfi_restore_state
	movq	-184(%rbp), %rax
	movq	%rax, %r8
	cmpq	41096(%r14), %rax
	je	.L2139
.L2044:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r8)
	jmp	.L2043
	.p2align 4,,10
	.p2align 3
.L2060:
	movq	41088(%r14), %r11
	cmpq	41096(%r14), %r11
	je	.L2140
.L2062:
	leaq	8(%r11), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r11)
	jmp	.L2061
	.p2align 4,,10
	.p2align 3
.L2047:
	movq	41088(%r14), %r11
	cmpq	41096(%r14), %r11
	je	.L2141
.L2049:
	leaq	8(%r11), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r11)
	testb	$1, %sil
	je	.L2142
.L2050:
	movq	%r11, %rsi
	movq	%r14, %rdi
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-216(%rbp), %r8
	jmp	.L2052
	.p2align 4,,10
	.p2align 3
.L2065:
	movq	39(%rax), %rsi
	sarq	$32, %rsi
	movq	%rsi, -216(%rbp)
	movq	47(%rax), %rax
	sarq	$32, %rax
	movq	%rax, -248(%rbp)
	testl	%ebx, %ebx
	jne	.L2143
.L2067:
	leaq	-144(%rbp), %rax
	movq	%r14, %rsi
	movq	%r11, -232(%rbp)
	movq	%rax, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	(%r12), %rax
	movl	-216(%rbp), %ecx
	movq	%r12, %rsi
	movq	-232(%rbp), %r11
	cmpl	11(%rax), %ecx
	je	.L2069
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r11, -232(%rbp)
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	-232(%rbp), %r11
	movq	%rax, %rsi
.L2069:
	movq	-224(%rbp), %rdi
	movq	%r11, -232(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	-232(%rbp), %r11
	movq	(%r11), %rax
	movq	15(%rax), %rbx
	movq	$0, -264(%rbp)
	movq	%rbx, %r15
	shrq	$63, %rbx
	sarq	$32, %r15
	addl	%r15d, %ebx
	sarl	%ebx
	cmpq	$3, %r15
	jg	.L2144
.L2070:
	cmpl	$65534, %ebx
	ja	.L2077
	leal	2(%rbx), %eax
	movl	%eax, -236(%rbp)
.L2079:
	movq	%r11, -256(%rbp)
	cmpl	$65534, %eax
	ja	.L2077
	movl	%eax, %r13d
	movq	%r13, %rdi
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	movq	%r13, -152(%rbp)
	movq	%rax, -160(%rbp)
	leaq	88(%r14), %rax
	movq	%rax, -232(%rbp)
	cmpq	$1, %r15
	jle	.L2104
	movq	-256(%rbp), %r11
	leaq	-161(%rbp), %r13
	movq	%r12, -272(%rbp)
	xorl	%r15d, %r15d
	movq	%r13, %r12
	movq	%r11, %r13
	.p2align 4,,10
	.p2align 3
.L2084:
	movq	%r12, %rcx
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb@PLT
	cmpb	$0, -161(%rbp)
	je	.L2081
	movq	-160(%rbp), %rdx
	movq	%rax, (%rdx,%r15,8)
	addq	$1, %r15
	cmpl	%r15d, %ebx
	jg	.L2084
.L2129:
	testl	%ebx, %ebx
	movl	$1, %eax
	movq	-272(%rbp), %r12
	cmovle	%eax, %ebx
	leal	1(%rbx), %r13d
	addl	$2, %ebx
	movslq	%r13d, %r13
	salq	$3, %r13
	leaq	-8(%r13), %rdx
.L2080:
	movq	-216(%rbp), %rsi
	movq	41112(%r14), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L2085
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-216(%rbp), %rdx
.L2086:
	movq	-160(%rbp), %rcx
	cmpb	$0, -200(%rbp)
	movq	%rax, (%rcx,%rdx)
	movq	-160(%rbp), %rax
	movq	%r12, (%rax,%r13)
	jne	.L2145
.L2088:
	movq	-208(%rbp), %rsi
	movl	-236(%rbp), %ecx
	movq	%r14, %rdi
	movq	-160(%rbp), %r8
	movq	-232(%rbp), %rdx
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2130
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L2092
.L2095:
	movq	%r14, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2130
.L2094:
	movq	-224(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	(%r12), %rax
	movl	11(%rax), %ecx
	movq	-248(%rbp), %rax
	testq	%rax, %rax
	je	.L2097
	movq	%r12, %rsi
	movl	%eax, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %r12
.L2097:
	movq	-224(%rbp), %rbx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	je	.L2130
	movq	(%rax), %r13
.L2091:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2055
	call	_ZdaPv@PLT
	jmp	.L2055
	.p2align 4,,10
	.p2align 3
.L2081:
	movq	-160(%rbp), %rax
	movq	-232(%rbp), %rcx
	movq	%rcx, (%rax,%r15,8)
	addq	$1, %r15
	cmpl	%r15d, %ebx
	jg	.L2084
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2056:
	movsd	7(%rax), %xmm0
	comisd	.LC21(%rip), %xmm0
	jb	.L2046
	movsd	.LC22(%rip), %xmm1
	movl	$-1, %r15d
	comisd	%xmm0, %xmm1
	jbe	.L2046
	cvttsd2siq	%xmm0, %r15
	jmp	.L2046
	.p2align 4,,10
	.p2align 3
.L2137:
	movq	0(%r13), %rax
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rdx
	movq	%rdx, 47(%rax)
	jmp	.L2066
	.p2align 4,,10
	.p2align 3
.L2131:
	movq	312(%r14), %r13
	jmp	.L2055
	.p2align 4,,10
	.p2align 3
.L2051:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L2146
.L2053:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L2132:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2144:
	movq	0(%r13), %rax
	movq	23(%rax), %rax
	movq	87(%rax), %rsi
	testb	$1, %sil
	je	.L2070
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L2070
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2073
	movq	%r11, -200(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-200(%rbp), %r11
.L2074:
	movq	%rax, -264(%rbp)
	leal	3(%rbx), %eax
	movl	%eax, -236(%rbp)
	movb	$1, -200(%rbp)
	cmpl	$65534, %ebx
	jbe	.L2079
	.p2align 4,,10
	.p2align 3
.L2077:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$305, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
	jmp	.L2055
	.p2align 4,,10
	.p2align 3
.L2143:
	movq	%rax, %rdx
	movq	0(%r13), %rax
	salq	$32, %rdx
	movq	%rdx, 47(%rax)
	jmp	.L2067
	.p2align 4,,10
	.p2align 3
.L2139:
	movq	%r14, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L2044
	.p2align 4,,10
	.p2align 3
.L2085:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L2147
.L2087:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2140:
	movq	%r14, %rdi
	movq	%r8, -224(%rbp)
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %r8
	movq	-216(%rbp), %rsi
	movq	%rax, %r11
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2130:
	movq	312(%r14), %r13
	jmp	.L2091
	.p2align 4,,10
	.p2align 3
.L2145:
	leaq	-160(%rbp), %rax
	leaq	-96(%rbp), %r13
	movq	%r14, %rdi
	movslq	%ebx, %rbx
	movq	%rax, -96(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internalL58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS2_9ArgumentsEPNS2_7IsolateEEUliE_E10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation(%rip), %rcx
	leaq	_ZNSt17_Function_handlerIFN2v88internal6ObjectEiEZNS1_L58__RT_impl_Runtime_StringReplaceNonGlobalRegExpWithFunctionENS1_9ArgumentsEPNS1_7IsolateEEUliE_E9_M_invokeERKSt9_Any_dataOi(%rip), %rax
	movq	%r13, %rdx
	movq	%rcx, %xmm0
	movq	%rax, %xmm2
	movq	-264(%rbp), %rsi
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_133ConstructNamedCaptureGroupsObjectEPNS0_7IsolateENS0_6HandleINS0_10FixedArrayEEERKSt8functionIFNS0_6ObjectEiEE
	movq	-160(%rbp), %rdx
	movq	%rax, (%rdx,%rbx,8)
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L2088
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2088
	.p2align 4,,10
	.p2align 3
.L2141:
	movq	%r14, %rdi
	movq	%rsi, -224(%rbp)
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %r8
	movq	%rax, %r11
	jmp	.L2049
	.p2align 4,,10
	.p2align 3
.L2092:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2095
	jmp	.L2094
	.p2align 4,,10
	.p2align 3
.L2146:
	movq	%r14, %rdi
	movq	%rsi, -224(%rbp)
	movq	%r8, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %r8
	jmp	.L2053
	.p2align 4,,10
	.p2align 3
.L2104:
	xorl	%edx, %edx
	movl	$8, %r13d
	movl	$2, %ebx
	jmp	.L2080
.L2147:
	movq	%r14, %rdi
	movq	%rsi, -256(%rbp)
	movq	%rdx, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	movq	-216(%rbp), %rdx
	jmp	.L2087
.L2073:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L2148
.L2075:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L2074
.L2148:
	movq	%r14, %rdi
	movq	%rsi, -232(%rbp)
	movq	%r11, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-232(%rbp), %rsi
	movq	-200(%rbp), %r11
	jmp	.L2075
.L2138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20255:
	.size	_ZN2v88internal48Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal48Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal11DeleteArrayINS0_6HandleINS0_6ObjectEEEEEvPT_,"axG",@progbits,_ZN2v88internal11DeleteArrayINS0_6HandleINS0_6ObjectEEEEEvPT_,comdat
	.p2align 4
	.weak	_ZN2v88internal11DeleteArrayINS0_6HandleINS0_6ObjectEEEEEvPT_
	.type	_ZN2v88internal11DeleteArrayINS0_6HandleINS0_6ObjectEEEEEvPT_, @function
_ZN2v88internal11DeleteArrayINS0_6HandleINS0_6ObjectEEEEEvPT_:
.LFB23861:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2149
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L2149:
	ret
	.cfi_endproc
.LFE23861:
	.size	_ZN2v88internal11DeleteArrayINS0_6HandleINS0_6ObjectEEEEEvPT_, .-_ZN2v88internal11DeleteArrayINS0_6HandleINS0_6ObjectEEEEEvPT_
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB23884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L2189
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L2167
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L2190
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L2153:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L2191
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L2156:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2190:
	testq	%rdx, %rdx
	jne	.L2192
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L2154:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L2157
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L2170
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L2170
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L2159:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L2159
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L2161
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L2161:
	leaq	16(%rax,%r8), %rcx
.L2157:
	cmpq	%r14, %r12
	je	.L2162
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L2171
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L2171
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L2164:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L2164
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L2166
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L2166:
	leaq	8(%rcx,%r9), %rcx
.L2162:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2167:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L2153
	.p2align 4,,10
	.p2align 3
.L2171:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2163:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L2163
	jmp	.L2166
	.p2align 4,,10
	.p2align 3
.L2170:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2158:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L2158
	jmp	.L2161
.L2191:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L2156
.L2189:
	leaq	.LC29(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2192:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L2153
	.cfi_endproc
.LFE23884:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB24710:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L2207
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L2203
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2208
.L2195:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L2202:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L2209
	testq	%r13, %r13
	jg	.L2198
	testq	%r9, %r9
	jne	.L2201
.L2199:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2209:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L2198
.L2201:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L2199
	.p2align 4,,10
	.p2align 3
.L2208:
	testq	%rsi, %rsi
	jne	.L2196
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L2202
	.p2align 4,,10
	.p2align 3
.L2198:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L2199
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2203:
	movl	$4, %r14d
	jmp	.L2195
.L2207:
	leaq	.LC29(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2196:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L2195
	.cfi_endproc
.LFE24710:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.text._ZN2v88internal24FindTwoByteStringIndicesENS0_6VectorIKtEEtPSt6vectorIiSaIiEEj,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24FindTwoByteStringIndicesENS0_6VectorIKtEEtPSt6vectorIiSaIiEEj
	.type	_ZN2v88internal24FindTwoByteStringIndicesENS0_6VectorIKtEEtPSt6vectorIiSaIiEEj, @function
_ZN2v88internal24FindTwoByteStringIndicesENS0_6VectorIKtEEtPSt6vectorIiSaIiEEj:
.LFB20196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	(%rdi,%rsi,2), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L2210
	movq	%rdi, %r9
	cmpq	%rdi, %r14
	jbe	.L2210
	movl	%edx, %r13d
	movq	%rcx, %r15
	movl	%r8d, %r12d
	movq	%rdi, %rbx
	leaq	-60(%rbp), %rdx
	jmp	.L2215
	.p2align 4,,10
	.p2align 3
.L2212:
	addq	$2, %rbx
	cmpq	%rbx, %r14
	jbe	.L2210
.L2221:
	testl	%r12d, %r12d
	je	.L2210
.L2215:
	cmpw	%r13w, (%rbx)
	jne	.L2212
	movq	%rbx, %rax
	movq	8(%r15), %rsi
	subq	%r9, %rax
	sarq	%rax
	movl	%eax, -60(%rbp)
	cmpq	16(%r15), %rsi
	je	.L2213
	movl	%eax, (%rsi)
	addq	$4, 8(%r15)
.L2214:
	addq	$2, %rbx
	subl	$1, %r12d
	cmpq	%rbx, %r14
	ja	.L2221
	.p2align 4,,10
	.p2align 3
.L2210:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2222
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2213:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	%r9, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %rdx
	jmp	.L2214
.L2222:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20196:
	.size	_ZN2v88internal24FindTwoByteStringIndicesENS0_6VectorIKtEEtPSt6vectorIiSaIiEEj, .-_ZN2v88internal24FindTwoByteStringIndicesENS0_6VectorIKtEEtPSt6vectorIiSaIiEEj
	.section	.text._ZN2v88internal24FindOneByteStringIndicesENS0_6VectorIKhEEhPSt6vectorIiSaIiEEj,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24FindOneByteStringIndicesENS0_6VectorIKhEEhPSt6vectorIiSaIiEEj
	.type	_ZN2v88internal24FindOneByteStringIndicesENS0_6VectorIKhEEhPSt6vectorIiSaIiEEj, @function
_ZN2v88internal24FindOneByteStringIndicesENS0_6VectorIKhEEhPSt6vectorIiSaIiEEj:
.LFB20194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movzbl	%dl, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	(%rdi,%rsi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-60(%rbp), %rax
	movq	%rax, -80(%rbp)
	testl	%r8d, %r8d
	je	.L2223
	.p2align 4,,10
	.p2align 3
.L2228:
	movq	%r12, %rdx
	movl	%r14d, %esi
	subq	%rdi, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	je	.L2223
	movq	%rax, %rdx
	movq	8(%r15), %rsi
	subq	%r13, %rdx
	movl	%edx, -60(%rbp)
	cmpq	16(%r15), %rsi
	je	.L2226
	movl	%edx, (%rsi)
	leaq	1(%rax), %rdi
	addq	$4, 8(%r15)
	subl	$1, %ebx
	jne	.L2228
.L2223:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2241
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2226:
	.cfi_restore_state
	movq	-80(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, -72(%rbp)
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	-72(%rbp), %rax
	leaq	1(%rax), %rdi
	subl	$1, %ebx
	jne	.L2228
	jmp	.L2223
.L2241:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20194:
	.size	_ZN2v88internal24FindOneByteStringIndicesENS0_6VectorIKhEEhPSt6vectorIiSaIiEEj, .-_ZN2v88internal24FindOneByteStringIndicesENS0_6VectorIKhEEhPSt6vectorIiSaIiEEj
	.section	.text._ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj
	.type	_ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj, @function
_ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj:
.LFB20198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-61(%rbp), %r15
	movq	%rdi, %r14
	leaq	-72(%rbp), %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$56, %rsp
	movq	%rsi, -72(%rbp)
	movq	%r15, %rsi
	movq	%rdx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	leaq	-80(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rdx, %r12
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%r12, %rdi
	movq	-88(%rbp), %r10
	movslq	%r12d, %r12
	movq	%rdx, %rsi
	shrq	$32, %rdi
	movq	%rax, %rcx
	movslq	%edx, %r8
	shrq	$32, %rsi
	cmpl	$1, %edi
	je	.L2285
	cmpl	$1, %esi
	je	.L2286
	cmpl	$1, %edx
	je	.L2287
	subq	$8, %rsp
	movq	%r12, %rdx
	movq	%r13, %r9
	movq	%r10, %rsi
	pushq	%rbx
	movq	%r14, %rdi
	call	_ZN2v88internal17FindStringIndicesIttEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	popq	%rax
	popq	%rdx
.L2251:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2288
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2286:
	.cfi_restore_state
	cmpl	$1, %edx
	je	.L2289
	subq	$8, %rsp
	movq	%r10, %rsi
	movq	%r13, %r9
	movq	%r12, %rdx
	pushq	%rbx
	movq	%r14, %rdi
	call	_ZN2v88internal17FindStringIndicesIthEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	popq	%rcx
	popq	%rsi
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2285:
	cmpl	$1, %esi
	jne	.L2244
	cmpl	$1, %edx
	je	.L2290
	subq	$8, %rsp
	movq	%r13, %r9
	movq	%r10, %rsi
	movq	%r12, %rdx
	pushq	%rbx
	movq	%r14, %rdi
	call	_ZN2v88internal17FindStringIndicesIhhEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	popq	%r9
	popq	%r10
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2244:
	subq	$8, %rsp
	movq	%r14, %rdi
	movq	%r13, %r9
	movq	%r10, %rsi
	pushq	%rbx
	movq	%r12, %rdx
	call	_ZN2v88internal17FindStringIndicesIhtEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	popq	%rdi
	popq	%r8
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2289:
	movzbl	(%rax), %r15d
	leaq	(%r10,%r12,2), %r14
	testl	%ebx, %ebx
	je	.L2251
	movq	%r10, %r12
	leaq	-60(%rbp), %rdx
	cmpq	%r10, %r14
	ja	.L2254
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2257:
	addq	$2, %r12
	cmpq	%r12, %r14
	jbe	.L2251
	testl	%ebx, %ebx
	je	.L2251
.L2254:
	cmpw	(%r12), %r15w
	jne	.L2257
	movq	%r12, %rax
	movq	8(%r13), %rsi
	subq	%r10, %rax
	sarq	%rax
	movl	%eax, -60(%rbp)
	cmpq	16(%r13), %rsi
	je	.L2258
	movl	%eax, (%rsi)
	addq	$4, 8(%r13)
.L2259:
	subl	$1, %ebx
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2287:
	movzwl	(%rax), %r15d
	leaq	(%r10,%r12,2), %r14
	testl	%ebx, %ebx
	je	.L2251
	movq	%r10, %r12
	leaq	-60(%rbp), %rdx
	cmpq	%r10, %r14
	ja	.L2262
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2264:
	addq	$2, %r12
	cmpq	%r12, %r14
	jbe	.L2251
	testl	%ebx, %ebx
	je	.L2251
.L2262:
	cmpw	(%r12), %r15w
	jne	.L2264
	movq	%r12, %rax
	movq	8(%r13), %rsi
	subq	%r10, %rax
	sarq	%rax
	movl	%eax, -60(%rbp)
	cmpq	16(%r13), %rsi
	je	.L2265
	movl	%eax, (%rsi)
	addq	$4, 8(%r13)
.L2266:
	subl	$1, %ebx
	jmp	.L2264
	.p2align 4,,10
	.p2align 3
.L2290:
	movzbl	(%rax), %r14d
	addq	%r10, %r12
	movq	%r10, %rdi
	leaq	-60(%rbp), %r15
	testl	%ebx, %ebx
	je	.L2251
	.p2align 4,,10
	.p2align 3
.L2246:
	movq	%r12, %rdx
	movl	%r14d, %esi
	movq	%r10, -88(%rbp)
	subq	%rdi, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	je	.L2251
	movq	-88(%rbp), %r10
	movq	%rax, %rdx
	movq	8(%r13), %rsi
	subq	%r10, %rdx
	movl	%edx, -60(%rbp)
	cmpq	16(%r13), %rsi
	je	.L2249
	movl	%edx, (%rsi)
	leaq	1(%rax), %rdi
	addq	$4, 8(%r13)
	subl	$1, %ebx
	jne	.L2246
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2249:
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	-88(%rbp), %rax
	subl	$1, %ebx
	movq	-96(%rbp), %r10
	leaq	1(%rax), %rdi
	jne	.L2246
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2265:
	movq	%r13, %rdi
	movq	%r10, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	-96(%rbp), %r10
	movq	-88(%rbp), %rdx
	jmp	.L2266
	.p2align 4,,10
	.p2align 3
.L2258:
	movq	%r13, %rdi
	movq	%r10, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	-96(%rbp), %r10
	movq	-88(%rbp), %rdx
	jmp	.L2259
.L2288:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20198:
	.size	_ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj, .-_ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj
	.section	.rodata._ZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"V8.Runtime_Runtime_StringSplit"
	.section	.rodata._ZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC35:
	.string	"0 < limit"
.LC36:
	.string	"0 < pattern_length"
	.section	.text._ZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateE.isra.0:
.LFB25927:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2433
.L2292:
	movq	_ZZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateEE28trace_event_unique_atomic801(%rip), %rax
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2434
.L2294:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2435
.L2296:
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	(%r12), %rsi
	movq	%rax, -288(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -280(%rbp)
	testb	$1, %sil
	jne	.L2300
.L2301:
	leaq	.LC31(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2434:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2436
.L2295:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateEE28trace_event_unique_atomic801(%rip)
	jmp	.L2294
.L2300:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L2301
	leaq	-8(%r12), %rax
	movq	%rax, -272(%rbp)
	movq	-8(%r12), %rax
	testb	$1, %al
	jne	.L2437
.L2302:
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2435:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2438
.L2297:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2298
	movq	(%rdi), %rax
	call	*8(%rax)
.L2298:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2299
	movq	(%rdi), %rax
	call	*8(%rax)
.L2299:
	leaq	.LC34(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2296
.L2437:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2302
	movq	-16(%r12), %rax
	testb	$1, %al
	je	.L2429
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2439
	movq	7(%rax), %rax
	movsd	.LC2(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC1(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L2440
.L2309:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L2314
	movq	%rax, %rdi
	shrq	$52, %rdi
	andl	$2047, %edi
	movl	%edi, %ecx
	subl	$1075, %ecx
	js	.L2441
	cmpl	$31, %ecx
	jg	.L2314
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rdi
	andq	%rax, %rdx
	addq	%rdi, %rdx
	salq	%cl, %rdx
	movl	%edx, %edx
.L2316:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%edx, %eax
	movl	%eax, -264(%rbp)
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2429:
	shrq	$32, %rax
	movq	%rax, -264(%rbp)
.L2308:
	movl	-264(%rbp), %edi
	testl	%edi, %edi
	je	.L2314
	movl	11(%rsi), %eax
	movq	-8(%r12), %rdx
	movl	%eax, -172(%rbp)
	movl	11(%rdx), %eax
	movl	%eax, -192(%rbp)
	testl	%eax, %eax
	jle	.L2442
	cmpl	$-1, -264(%rbp)
	je	.L2443
.L2320:
	movq	-1(%rsi), %rax
	movq	%rsi, %r13
	cmpw	$63, 11(%rax)
	ja	.L2327
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	je	.L2444
.L2327:
	movq	-1(%r13), %rax
	cmpw	$63, 11(%rax)
	ja	.L2335
	movq	-1(%r13), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L2335
	movq	(%r12), %rax
	movq	41112(%r15), %rdi
	movq	15(%rax), %r13
	testq	%rdi, %rdi
	je	.L2338
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L2335:
	movq	-272(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	movq	%rax, %r13
	cmpw	$63, 11(%rdx)
	ja	.L2342
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L2445
.L2342:
	movq	-1(%r13), %rax
	cmpw	$63, 11(%rax)
	ja	.L2350
	movq	-1(%r13), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L2446
.L2350:
	movq	41216(%r15), %rax
	leaq	41216(%r15), %r13
	cmpq	41224(%r15), %rax
	je	.L2356
	movq	%rax, 41224(%r15)
.L2356:
	movl	-264(%rbp), %ebx
	movq	(%r12), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	-272(%rbp), %rax
	movl	%ebx, %r8d
	movq	(%rax), %rdx
	call	_ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj
	movq	41224(%r15), %rsi
	movq	%rsi, %rdx
	subq	41216(%r15), %rdx
	sarq	$2, %rdx
	cmpl	%edx, %ebx
	ja	.L2447
.L2357:
	movl	$2, %esi
	movq	%r15, %rdi
	xorl	%r9d, %r9d
	movl	%edx, %ecx
	movl	$1, %r8d
	movl	%edx, -240(%rbp)
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	41112(%r15), %rdi
	movq	%rax, -296(%rbp)
	movq	(%rax), %rax
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2359
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpl	$1, -240(%rbp)
	movq	%rax, -200(%rbp)
	je	.L2448
.L2362:
	movl	-240(%rbp), %eax
	testl	%eax, %eax
	jle	.L2366
.L2393:
	movl	$0, -236(%rbp)
	movq	%r15, %rax
	xorl	%r9d, %r9d
	movq	%r12, %r15
	xorl	%ebx, %ebx
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L2379:
	movq	41088(%r12), %rax
	movl	41104(%r12), %edx
	addl	$1024, -236(%rbp)
	movl	-240(%rbp), %esi
	movq	%rax, -248(%rbp)
	movq	41096(%r12), %rax
	movl	-236(%rbp), %ecx
	movq	%rax, -256(%rbp)
	leal	1(%rdx), %eax
	movl	%eax, 41104(%r12)
	cmpl	%ecx, %esi
	movl	%ecx, %eax
	cmovle	%esi, %eax
	cmpl	%ebx, %eax
	jle	.L2370
	movl	%ebx, %edx
	movslq	%ebx, %r13
	notl	%edx
	addl	%edx, %eax
	leaq	1(%r13,%rax), %rax
	movq	%rax, -184(%rbp)
	leal	1(%rbx), %eax
	movl	%eax, -188(%rbp)
	movq	%r12, %rax
	movq	%r13, %r12
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L2375:
	movq	41216(%r13), %rax
	movq	41224(%r13), %rdx
	subq	%rax, %rdx
	sarq	$2, %rdx
	cmpq	%r12, %rdx
	jbe	.L2449
	movl	(%rax,%r12,4), %r14d
	movl	%r9d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%r14d, %ecx
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	-200(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	leaq	15(%rdi,%r12,8), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L2388
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r9
	movq	%rax, -208(%rbp)
	testl	$262144, %r9d
	je	.L2373
	movq	%rdx, -232(%rbp)
	movq	%rsi, -224(%rbp)
	movq	%rdi, -216(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-208(%rbp), %rax
	movq	-232(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
	movq	8(%rax), %r9
.L2373:
	andl	$24, %r9d
	je	.L2388
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2388
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2388:
	movl	-192(%rbp), %eax
	leal	(%r14,%rax), %r9d
	movl	-188(%rbp), %eax
	subl	%ebx, %eax
	addl	%r12d, %eax
	addq	$1, %r12
	cmpq	-184(%rbp), %r12
	jne	.L2375
	movq	-248(%rbp), %rbx
	subl	$1, 41104(%r13)
	movq	%r13, %r12
	movq	%rbx, 41088(%r13)
	movq	-256(%rbp), %rbx
	cmpq	41096(%r13), %rbx
	je	.L2394
	movq	%rbx, 41096(%r13)
	movq	%r13, %rdi
	movl	%eax, -184(%rbp)
	movl	%r9d, -188(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-184(%rbp), %eax
	cmpl	%eax, -240(%rbp)
	jle	.L2431
	movl	-188(%rbp), %r9d
	movl	%eax, %ebx
	jmp	.L2379
	.p2align 4,,10
	.p2align 3
.L2394:
	movl	%eax, %ebx
.L2376:
	cmpl	%ebx, -240(%rbp)
	jg	.L2379
.L2431:
	movq	%r12, %rax
	movq	%r15, %r12
	movq	%rax, %r15
.L2366:
	cmpl	$-1, -264(%rbp)
	je	.L2450
.L2381:
	movq	41216(%r15), %rdx
	movq	41232(%r15), %rax
	subq	%rdx, %rax
	cmpq	$32771, %rax
	jbe	.L2383
	cmpq	41224(%r15), %rdx
	je	.L2383
	movq	%rdx, 41224(%r15)
.L2383:
	movq	-296(%rbp), %rax
	movq	(%rax), %r12
.L2325:
	movq	-288(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-280(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L2386
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2386:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2451
.L2291:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2452
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2370:
	.cfi_restore_state
	movl	%edx, 41104(%r12)
	jmp	.L2376
.L2359:
	movq	41088(%r15), %rax
	movq	%rax, -200(%rbp)
	cmpq	41096(%r15), %rax
	je	.L2453
.L2361:
	movq	-200(%rbp), %rbx
	cmpl	$1, -240(%rbp)
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jne	.L2362
.L2448:
	movq	41216(%r15), %rax
	cmpq	41224(%r15), %rax
	je	.L2454
	movl	-172(%rbp), %ebx
	cmpl	%ebx, (%rax)
	jne	.L2393
	movq	-200(%rbp), %rax
	movq	(%r12), %rdx
	movq	(%rax), %r13
	movq	%rdx, 15(%r13)
	leaq	15(%r13), %r14
	testb	$1, %dl
	je	.L2366
	movq	%rdx, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L2368
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -184(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-184(%rbp), %rdx
.L2368:
	testb	$24, %al
	je	.L2366
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2366
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2366
	.p2align 4,,10
	.p2align 3
.L2443:
	leaq	37592(%r15), %rdi
	leaq	-168(%rbp), %rcx
	movl	$1, %r8d
	movq	$0, -168(%rbp)
	call	_ZN2v88internal18RegExpResultsCache6LookupEPNS0_4HeapENS0_6StringENS0_6ObjectEPNS0_10FixedArrayENS1_16ResultsCacheTypeE@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L2321
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %rsi
.L2322:
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	jne	.L2455
	movq	(%r12), %rsi
	jmp	.L2320
.L2440:
	movsd	.LC3(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L2309
	comisd	.LC4(%rip), %xmm0
	jb	.L2309
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L2309
	jne	.L2309
	movl	%edx, -264(%rbp)
	jmp	.L2308
.L2446:
	movq	-272(%rbp), %rax
	movq	41112(%r15), %rdi
	movq	(%rax), %rax
	movq	15(%rax), %r13
	testq	%rdi, %rdi
	je	.L2353
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -272(%rbp)
	jmp	.L2350
.L2321:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L2456
.L2323:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%rsi)
	jmp	.L2322
.L2445:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L2345
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L2345
	movq	-272(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, -272(%rbp)
	jmp	.L2350
.L2444:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L2330
	movq	23(%rsi), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	je	.L2330
	movq	%r12, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	jmp	.L2335
.L2314:
	leaq	.LC35(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2345:
	movq	41112(%r15), %rdi
	movq	15(%rax), %r13
	testq	%rdi, %rdi
	je	.L2347
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -272(%rbp)
	movq	(%rax), %r13
	jmp	.L2342
.L2330:
	movq	41112(%r15), %rdi
	movq	15(%rsi), %r13
	testq	%rdi, %rdi
	je	.L2332
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %r12
	jmp	.L2327
.L2447:
	cmpq	41232(%r15), %rsi
	je	.L2358
	movl	-172(%rbp), %eax
	movl	%eax, (%rsi)
	movq	41224(%r15), %rax
	leaq	4(%rax), %rdx
	movq	%rdx, 41224(%r15)
	subq	41216(%r15), %rdx
	sarq	$2, %rdx
	jmp	.L2357
.L2441:
	cmpl	$-52, %ecx
	jl	.L2314
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rdx
	addq	%rcx, %rdx
	movl	$1075, %ecx
	subl	%edi, %ecx
	shrq	%cl, %rdx
	jmp	.L2316
.L2450:
	movq	-296(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$2, %eax
	cmpb	$1, %al
	ja	.L2381
	movq	-200(%rbp), %rcx
	movl	$1, %r9d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	-272(%rbp), %rdx
	leaq	288(%r15), %r8
	call	_ZN2v88internal18RegExpResultsCache5EnterEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEENS4_INS0_10FixedArrayEEESA_NS1_16ResultsCacheTypeE@PLT
	jmp	.L2381
.L2433:
	movq	40960(%rsi), %rax
	movl	$500, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2292
.L2439:
	leaq	.LC12(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2455:
	movslq	11(%r13), %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r12
	jmp	.L2325
.L2436:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2295
.L2438:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC34(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2297
.L2442:
	leaq	.LC36(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2451:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2291
.L2338:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L2457
.L2340:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%r12)
	jmp	.L2335
.L2353:
	movq	41088(%r15), %rax
	movq	%rax, -272(%rbp)
	cmpq	41096(%r15), %rax
	je	.L2458
.L2355:
	movq	-272(%rbp), %rbx
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%rbx)
	jmp	.L2350
.L2347:
	movq	41088(%r15), %rax
	movq	%rax, -272(%rbp)
	cmpq	41096(%r15), %rax
	je	.L2459
.L2349:
	movq	-272(%rbp), %rbx
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%rbx)
	jmp	.L2342
.L2332:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L2460
.L2334:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%r12)
	jmp	.L2327
.L2458:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, -272(%rbp)
	jmp	.L2355
.L2457:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L2340
.L2453:
	movq	%r15, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, -200(%rbp)
	jmp	.L2361
.L2358:
	leaq	-172(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	41224(%r15), %rdx
	subq	41216(%r15), %rdx
	sarq	$2, %rdx
	jmp	.L2357
.L2456:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2323
.L2449:
	movq	%r12, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2459:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, -272(%rbp)
	jmp	.L2349
.L2460:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L2334
.L2452:
	call	__stack_chk_fail@PLT
.L2454:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE25927:
	.size	_ZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal19Runtime_StringSplitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_StringSplitEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_StringSplitEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_StringSplitEiPmPNS0_7IsolateE:
.LFB20208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2579
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rsi
	movq	%rax, -192(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -184(%rbp)
	testb	$1, %sil
	jne	.L2464
.L2465:
	leaq	.LC31(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2464:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L2465
	leaq	-8(%r12), %rax
	movq	%rax, -176(%rbp)
	movq	-8(%r12), %rax
	testb	$1, %al
	jne	.L2580
.L2466:
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2580:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2466
	movq	-16(%r12), %rax
	testb	$1, %al
	je	.L2575
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L2581
	movq	7(%rax), %rax
	movsd	.LC2(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC1(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L2473
	movsd	.LC3(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L2473
	comisd	.LC4(%rip), %xmm0
	jb	.L2473
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L2473
	jne	.L2473
	movl	%edx, -168(%rbp)
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2473:
	movabsq	$9218868437227405312, %rdx
	testq	%rdx, %rax
	je	.L2478
	movq	%rax, %rdi
	shrq	$52, %rdi
	andl	$2047, %edi
	movl	%edi, %ecx
	subl	$1075, %ecx
	js	.L2582
	cmpl	$31, %ecx
	jg	.L2478
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rdi
	andq	%rax, %rdx
	addq	%rdi, %rdx
	salq	%cl, %rdx
	movl	%edx, %edx
.L2480:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%edx, %eax
	movl	%eax, -168(%rbp)
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2575:
	shrq	$32, %rax
	movq	%rax, -168(%rbp)
.L2472:
	movl	-168(%rbp), %edi
	testl	%edi, %edi
	je	.L2478
	movl	11(%rsi), %eax
	movq	-8(%r12), %rdx
	movl	%eax, -68(%rbp)
	movl	11(%rdx), %eax
	movl	%eax, -96(%rbp)
	testl	%eax, %eax
	jle	.L2583
	cmpl	$-1, -168(%rbp)
	je	.L2584
.L2484:
	movq	-1(%rsi), %rax
	movq	%rsi, %r13
	cmpw	$63, 11(%rax)
	ja	.L2491
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	je	.L2585
.L2491:
	movq	-1(%r13), %rax
	cmpw	$63, 11(%rax)
	ja	.L2499
	movq	-1(%r13), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L2499
	movq	(%r12), %rax
	movq	41112(%r15), %rdi
	movq	15(%rax), %r13
	testq	%rdi, %rdi
	je	.L2502
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L2499:
	movq	-176(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	movq	%rax, %r13
	cmpw	$63, 11(%rdx)
	ja	.L2506
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L2586
.L2506:
	movq	-1(%r13), %rax
	cmpw	$63, 11(%rax)
	ja	.L2514
	movq	-1(%r13), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L2587
.L2514:
	movq	41216(%r15), %rax
	leaq	41216(%r15), %r13
	cmpq	41224(%r15), %rax
	je	.L2520
	movq	%rax, 41224(%r15)
.L2520:
	movl	-168(%rbp), %ebx
	movq	(%r12), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	-176(%rbp), %rax
	movl	%ebx, %r8d
	movq	(%rax), %rdx
	call	_ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj
	movq	41224(%r15), %rsi
	movq	%rsi, %rdx
	subq	41216(%r15), %rdx
	sarq	$2, %rdx
	cmpl	%edx, %ebx
	ja	.L2588
.L2521:
	movl	$2, %esi
	movq	%r15, %rdi
	xorl	%r9d, %r9d
	movl	%edx, %ecx
	movl	$1, %r8d
	movl	%edx, -144(%rbp)
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	movq	41112(%r15), %rdi
	movq	%rax, -200(%rbp)
	movq	(%rax), %rax
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2523
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -104(%rbp)
.L2524:
	cmpl	$1, -144(%rbp)
	je	.L2589
	movl	-144(%rbp), %eax
	testl	%eax, %eax
	jle	.L2530
.L2554:
	movl	$0, -140(%rbp)
	movq	%r15, %rax
	xorl	%r9d, %r9d
	movq	%r12, %r15
	xorl	%ebx, %ebx
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L2543:
	movq	41088(%r12), %rax
	movl	41104(%r12), %edx
	addl	$1024, -140(%rbp)
	movl	-144(%rbp), %esi
	movq	%rax, -152(%rbp)
	movq	41096(%r12), %rax
	movl	-140(%rbp), %ecx
	movq	%rax, -160(%rbp)
	leal	1(%rdx), %eax
	movl	%eax, 41104(%r12)
	cmpl	%ecx, %esi
	movl	%ecx, %eax
	cmovle	%esi, %eax
	cmpl	%ebx, %eax
	jle	.L2534
	movl	%ebx, %edx
	movslq	%ebx, %r13
	notl	%edx
	addl	%edx, %eax
	leaq	1(%r13,%rax), %rax
	movq	%rax, -88(%rbp)
	leal	1(%rbx), %eax
	movl	%eax, -92(%rbp)
	movq	%r12, %rax
	movq	%r13, %r12
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L2539:
	movq	41216(%r13), %rax
	movq	41224(%r13), %rdx
	subq	%rax, %rdx
	sarq	$2, %rdx
	cmpq	%rdx, %r12
	jnb	.L2590
	movl	(%rax,%r12,4), %r14d
	movl	%r9d, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movl	%r14d, %ecx
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	-104(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	leaq	15(%rdi,%r12,8), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L2551
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r9
	movq	%rax, -112(%rbp)
	testl	$262144, %r9d
	je	.L2537
	movq	%rdx, -136(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdi
	movq	8(%rax), %r9
.L2537:
	andl	$24, %r9d
	je	.L2551
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2551
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2551:
	movl	-96(%rbp), %eax
	leal	(%r14,%rax), %r9d
	movl	-92(%rbp), %eax
	subl	%ebx, %eax
	addl	%r12d, %eax
	addq	$1, %r12
	cmpq	-88(%rbp), %r12
	jne	.L2539
	movq	-152(%rbp), %rbx
	subl	$1, 41104(%r13)
	movq	%r13, %r12
	movq	%rbx, 41088(%r13)
	movq	-160(%rbp), %rbx
	cmpq	%rbx, 41096(%r13)
	je	.L2555
	movq	%rbx, 41096(%r13)
	movq	%r13, %rdi
	movl	%eax, -88(%rbp)
	movl	%r9d, -92(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movl	-88(%rbp), %eax
	cmpl	%eax, -144(%rbp)
	jle	.L2577
	movl	-92(%rbp), %r9d
	movl	%eax, %ebx
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2555:
	movl	%eax, %ebx
.L2540:
	cmpl	%ebx, -144(%rbp)
	jg	.L2543
.L2577:
	movq	%r12, %rax
	movq	%r15, %r12
	movq	%rax, %r15
.L2530:
	cmpl	$-1, -168(%rbp)
	je	.L2591
.L2545:
	movq	41216(%r15), %rdx
	movq	41232(%r15), %rax
	subq	%rdx, %rax
	cmpq	$32771, %rax
	jbe	.L2547
	cmpq	41224(%r15), %rdx
	je	.L2547
	movq	%rdx, 41224(%r15)
.L2547:
	movq	-200(%rbp), %rax
	movq	(%rax), %r12
.L2489:
	movq	-192(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-184(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L2461
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2461:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2592
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2534:
	.cfi_restore_state
	movl	%edx, 41104(%r12)
	jmp	.L2540
.L2523:
	movq	41088(%r15), %rax
	movq	%rax, -104(%rbp)
	cmpq	41096(%r15), %rax
	je	.L2593
.L2525:
	movq	-104(%rbp), %rbx
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L2524
.L2582:
	cmpl	$-52, %ecx
	jl	.L2478
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rdx
	addq	%rcx, %rdx
	movl	$1075, %ecx
	subl	%edi, %ecx
	shrq	%cl, %rdx
	jmp	.L2480
.L2587:
	movq	-176(%rbp), %rax
	movq	41112(%r15), %rdi
	movq	(%rax), %rax
	movq	15(%rax), %r13
	testq	%rdi, %rdi
	je	.L2517
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -176(%rbp)
	jmp	.L2514
.L2586:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L2509
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L2509
	movq	-176(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, -176(%rbp)
	jmp	.L2514
.L2585:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L2494
	movq	23(%rsi), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	je	.L2494
	movq	%r12, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r12
	jmp	.L2499
.L2494:
	movq	41112(%r15), %rdi
	movq	15(%rsi), %r13
	testq	%rdi, %rdi
	je	.L2496
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %r12
	jmp	.L2491
.L2509:
	movq	41112(%r15), %rdi
	movq	15(%rax), %r13
	testq	%rdi, %rdi
	je	.L2511
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -176(%rbp)
	movq	(%rax), %r13
	jmp	.L2506
.L2584:
	leaq	37592(%r15), %rdi
	leaq	-64(%rbp), %rcx
	movl	$1, %r8d
	movq	$0, -64(%rbp)
	call	_ZN2v88internal18RegExpResultsCache6LookupEPNS0_4HeapENS0_6StringENS0_6ObjectEPNS0_10FixedArrayENS1_16ResultsCacheTypeE@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L2485
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %rsi
.L2486:
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	jne	.L2594
	movq	(%r12), %rsi
	jmp	.L2484
.L2588:
	cmpq	41232(%r15), %rsi
	je	.L2522
	movl	-68(%rbp), %eax
	movl	%eax, (%rsi)
	movq	41224(%r15), %rax
	leaq	4(%rax), %rdx
	movq	%rdx, 41224(%r15)
	subq	41216(%r15), %rdx
	sarq	$2, %rdx
	jmp	.L2521
.L2579:
	movq	%r12, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r12
	jmp	.L2461
.L2589:
	movq	41216(%r15), %rax
	cmpq	41224(%r15), %rax
	je	.L2595
	movl	-68(%rbp), %ebx
	cmpl	%ebx, (%rax)
	jne	.L2554
	movq	-104(%rbp), %rax
	movq	(%r12), %rdx
	movq	(%rax), %r13
	movq	%rdx, 15(%r13)
	leaq	15(%r13), %r14
	testb	$1, %dl
	je	.L2530
	movq	%rdx, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L2532
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-88(%rbp), %rdx
.L2532:
	testb	$24, %al
	je	.L2530
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2530
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2530
	.p2align 4,,10
	.p2align 3
.L2591:
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$2, %eax
	cmpb	$1, %al
	ja	.L2545
	movq	-104(%rbp), %rcx
	movl	$1, %r9d
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	-176(%rbp), %rdx
	leaq	288(%r15), %r8
	call	_ZN2v88internal18RegExpResultsCache5EnterEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEENS4_INS0_10FixedArrayEEESA_NS1_16ResultsCacheTypeE@PLT
	jmp	.L2545
.L2485:
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L2596
.L2487:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%rsi)
	jmp	.L2486
.L2581:
	leaq	.LC12(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2478:
	leaq	.LC35(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2583:
	leaq	.LC36(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2594:
	movslq	11(%r13), %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r12
	jmp	.L2489
.L2517:
	movq	41088(%r15), %rax
	movq	%rax, -176(%rbp)
	cmpq	41096(%r15), %rax
	je	.L2597
.L2519:
	movq	-176(%rbp), %rbx
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%rbx)
	jmp	.L2514
.L2502:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L2598
.L2504:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%r12)
	jmp	.L2499
.L2593:
	movq	%r15, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, -104(%rbp)
	jmp	.L2525
.L2511:
	movq	41088(%r15), %rax
	movq	%rax, -176(%rbp)
	cmpq	41096(%r15), %rax
	je	.L2599
.L2513:
	movq	-176(%rbp), %rbx
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%rbx)
	jmp	.L2506
.L2496:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L2600
.L2498:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%r12)
	jmp	.L2491
.L2598:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L2504
.L2597:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, -176(%rbp)
	jmp	.L2519
.L2522:
	leaq	-68(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	41224(%r15), %rdx
	subq	41216(%r15), %rdx
	sarq	$2, %rdx
	jmp	.L2521
.L2596:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2487
.L2590:
	movq	%r12, %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L2599:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, -176(%rbp)
	jmp	.L2513
.L2600:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L2498
.L2592:
	call	__stack_chk_fail@PLT
.L2595:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE20208:
	.size	_ZN2v88internal19Runtime_StringSplitEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_StringSplitEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj.constprop.0, @function
_ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj.constprop.0:
.LFB26068:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-101(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-120(%rbp), %rdi
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -120(%rbp)
	movq	%r14, %rsi
	movq	%rdx, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%r14, %rsi
	leaq	-128(%rbp), %rdi
	movq	%rdx, %rbx
	movq	%rax, %r12
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	movq	%rbx, %rsi
	movq	%rax, %rcx
	shrq	$32, %rsi
	movq	%rdx, %rax
	shrq	$32, %rax
	cmpl	$1, %esi
	je	.L2674
	movslq	%ebx, %r10
	movslq	%edx, %r8
	cmpl	$1, %eax
	je	.L2675
	cmpl	$1, %edx
	je	.L2676
	subq	$8, %rsp
	movq	%r10, %rdx
	movq	%r15, %r9
	movq	%r12, %rsi
	pushq	$-1
	movq	%r13, %rdi
	call	_ZN2v88internal17FindStringIndicesIttEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	popq	%rax
	popq	%rdx
.L2631:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2677
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2675:
	.cfi_restore_state
	cmpl	$1, %edx
	je	.L2678
	subq	$8, %rsp
	movq	%r12, %rsi
	movq	%r15, %r9
	movq	%r10, %rdx
	pushq	$-1
	movq	%r13, %rdi
	call	_ZN2v88internal17FindStringIndicesIthEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	popq	%rcx
	popq	%rsi
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2674:
	movslq	%ebx, %rbx
	movslq	%edx, %r8
	cmpl	$1, %eax
	jne	.L2603
	cmpl	$1, %edx
	je	.L2679
	subq	$8, %rsp
	movq	%r13, %rdi
	movq	%r15, %r9
	movq	%r12, %rsi
	pushq	$-1
	movq	%rbx, %rdx
	call	_ZN2v88internal17FindStringIndicesIhhEEvPNS0_7IsolateENS0_6VectorIKT_EENS4_IKT0_EEPSt6vectorIiSaIiEEj
	popq	%rdi
	popq	%r8
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2603:
	leal	-250(%rdx), %eax
	movl	$0, %esi
	movq	%r8, -80(%rbp)
	movq	%r13, %xmm0
	testl	%eax, %eax
	movq	%rcx, %xmm1
	movl	$0, -100(%rbp)
	movl	%edx, %r14d
	cmovs	%esi, %eax
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, %rsi
	movaps	%xmm0, -96(%rbp)
	movl	%eax, -64(%rbp)
	leaq	(%rcx,%r8,2), %rax
	cmpq	$7, %r8
	jbe	.L2672
	testb	$7, %cl
	jne	.L2612
	jmp	.L2616
	.p2align 4,,10
	.p2align 3
.L2614:
	addq	$2, %rsi
	testb	$7, %sil
	je	.L2616
.L2612:
	cmpw	$255, (%rsi)
	jbe	.L2614
.L2618:
	subq	%rcx, %rsi
	sarq	%rsi
	cmpl	%esi, %edx
	jg	.L2680
	cmpl	$6, %edx
	jg	.L2624
	cmpl	$1, %edx
	je	.L2681
	leaq	_ZN2v88internal12StringSearchIthE12LinearSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2619:
	cmpw	$255, (%rsi)
	ja	.L2618
	addq	$2, %rsi
.L2672:
	cmpq	%rsi, %rax
	ja	.L2619
	jmp	.L2618
	.p2align 4,,10
	.p2align 3
.L2678:
	leaq	(%r12,%r10,2), %r14
	movzbl	(%rcx), %ecx
	movq	%r12, %rbx
	movl	$-1, %r13d
	leaq	-96(%rbp), %rdx
	cmpq	%r14, %r12
	jb	.L2634
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2636:
	addq	$2, %rbx
	cmpq	%rbx, %r14
	jbe	.L2631
	testl	%r13d, %r13d
	je	.L2631
.L2634:
	cmpw	(%rbx), %cx
	jne	.L2636
	movq	%rbx, %rax
	movq	8(%r15), %rsi
	subq	%r12, %rax
	sarq	%rax
	movl	%eax, -96(%rbp)
	cmpq	16(%r15), %rsi
	je	.L2637
	movl	%eax, (%rsi)
	addq	$4, 8(%r15)
.L2638:
	subl	$1, %r13d
	jmp	.L2636
	.p2align 4,,10
	.p2align 3
.L2676:
	leaq	(%r12,%r10,2), %r14
	movzwl	(%rcx), %ecx
	movq	%r12, %rbx
	movl	$-1, %r13d
	leaq	-96(%rbp), %rdx
	cmpq	%r14, %r12
	jb	.L2641
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2642:
	addq	$2, %rbx
	cmpq	%rbx, %r14
	jbe	.L2631
	testl	%r13d, %r13d
	je	.L2631
.L2641:
	cmpw	(%rbx), %cx
	jne	.L2642
	movq	%rbx, %rax
	movq	8(%r15), %rsi
	subq	%r12, %rax
	sarq	%rax
	movl	%eax, -96(%rbp)
	cmpq	16(%r15), %rsi
	je	.L2643
	movl	%eax, (%rsi)
	addq	$4, 8(%r15)
.L2644:
	subl	$1, %r13d
	jmp	.L2642
	.p2align 4,,10
	.p2align 3
.L2679:
	leaq	-96(%rbp), %rax
	movzbl	(%rcx), %r14d
	addq	%r12, %rbx
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	movl	$-1, %r13d
	.p2align 4,,10
	.p2align 3
.L2610:
	movq	%rbx, %rdx
	movl	%r14d, %esi
	subq	%rdi, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	je	.L2631
	movq	%rax, %rdx
	movq	8(%r15), %rsi
	subq	%r12, %rdx
	movl	%edx, -96(%rbp)
	cmpq	16(%r15), %rsi
	je	.L2607
	movl	%edx, (%rsi)
	leaq	1(%rax), %rdi
	addq	$4, 8(%r15)
	subl	$1, %r13d
	jne	.L2610
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2607:
	movq	-144(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, -136(%rbp)
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movq	-136(%rbp), %rax
	leaq	1(%rax), %rdi
	subl	$1, %r13d
	jne	.L2610
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2680:
	leaq	_ZN2v88internal12StringSearchIthE10FailSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
.L2623:
	leaq	-96(%rbp), %rdi
	movl	$-1, %r13d
	xorl	%ecx, %ecx
	movq	%rdi, -136(%rbp)
	leaq	-100(%rbp), %rdi
	movq	%rdi, -144(%rbp)
	jmp	.L2630
	.p2align 4,,10
	.p2align 3
.L2682:
	movl	%eax, (%rsi)
	addq	$4, 8(%r15)
.L2673:
	movl	-100(%rbp), %ecx
	addl	%r14d, %ecx
	movl	%ecx, -100(%rbp)
	subl	$1, %r13d
	je	.L2631
	movq	-72(%rbp), %rax
.L2630:
	movq	-136(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	call	*%rax
	movl	%eax, -100(%rbp)
	testl	%eax, %eax
	js	.L2631
	movq	8(%r15), %rsi
	cmpq	16(%r15), %rsi
	jne	.L2682
	movq	-144(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRKiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	jmp	.L2673
	.p2align 4,,10
	.p2align 3
.L2624:
	leaq	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2681:
	leaq	_ZN2v88internal12StringSearchIthE16SingleCharSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	%rax, -72(%rbp)
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2616:
	leaq	16(%rsi), %rdi
	cmpq	%rdi, %rax
	jb	.L2672
	leaq	-16(%rax), %rdi
	subq	%rsi, %rdi
	shrq	$3, %rdi
	leaq	8(%rsi,%rdi,8), %r8
	movabsq	$-71777214294589696, %rdi
	jmp	.L2620
	.p2align 4,,10
	.p2align 3
.L2617:
	addq	$8, %rsi
	cmpq	%r8, %rsi
	je	.L2672
.L2620:
	testq	%rdi, (%rsi)
	je	.L2617
	jmp	.L2672
	.p2align 4,,10
	.p2align 3
.L2643:
	movq	%r15, %rdi
	movl	%ecx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movl	-144(%rbp), %ecx
	movq	-136(%rbp), %rdx
	jmp	.L2644
	.p2align 4,,10
	.p2align 3
.L2637:
	movq	%r15, %rdi
	movl	%ecx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movl	-144(%rbp), %ecx
	movq	-136(%rbp), %rdx
	jmp	.L2638
.L2677:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE26068:
	.size	_ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj.constprop.0, .-_ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj.constprop.0
	.section	.text._ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqTwoByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqTwoByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE, @function
_ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqTwoByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE:
.LFB22434:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	leaq	41216(%rdi), %rcx
	subq	$88, %rsp
	movq	%rsi, -72(%rbp)
	movq	%r8, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41216(%rdi), %rax
	cmpq	41224(%rdi), %rax
	je	.L2684
	movq	%rax, 41224(%rdi)
.L2684:
	movq	(%rdx), %rax
	movq	%r12, %rdi
	movq	23(%rax), %rax
	movq	39(%rax), %rdx
	movq	-72(%rbp), %rax
	movq	(%rax), %rsi
	movl	11(%rdx), %r13d
	movl	11(%rsi), %eax
	movl	%eax, -76(%rbp)
	movq	(%rbx), %rax
	movl	11(%rax), %r14d
	call	_ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj.constprop.0
	movq	41224(%r12), %rax
	movq	41216(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L2707
	subq	%rdx, %rax
	movslq	%r13d, %rcx
	movslq	%r14d, %rdx
	subq	%rcx, %rdx
	sarq	$2, %rax
	imulq	%rdx, %rax
	movslq	-76(%rbp), %rdx
	addq	%rdx, %rax
	cmpq	$1073741799, %rax
	jg	.L2701
	movl	%eax, %esi
	testl	%eax, %eax
	jne	.L2687
	movq	128(%r12), %rax
.L2686:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2708
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2701:
	.cfi_restore_state
	movl	$2147483647, %esi
.L2687:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2709
	movq	41224(%r12), %r9
	movq	41216(%r12), %rax
	movq	%r9, -88(%rbp)
	cmpq	%rax, %r9
	je	.L2702
	testl	%r14d, %r14d
	jg	.L2690
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, -104(%rbp)
	movq	%r15, %r10
	movq	%rax, %rbx
	movl	%r13d, %r15d
	movl	%edx, %r12d
	movl	%r8d, %r13d
	jmp	.L2692
	.p2align 4,,10
	.p2align 3
.L2691:
	addq	$4, %rbx
	leal	(%r15,%r14), %r12d
	cmpq	%rbx, %r9
	je	.L2710
.L2692:
	movl	(%rbx), %r14d
	cmpl	%r12d, %r14d
	jle	.L2691
	movq	-72(%rbp), %rax
	movq	(%r10), %rdx
	movslq	%r13d, %rcx
	movq	%r9, -96(%rbp)
	movq	%r10, -88(%rbp)
	addq	$4, %rbx
	movq	(%rax), %rdi
	leaq	15(%rdx,%rcx,2), %rsi
	movl	%r12d, %edx
	movl	%r14d, %ecx
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	movl	%r14d, %edx
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r10
	subl	%r12d, %edx
	leal	(%r15,%r14), %r12d
	addl	%edx, %r13d
	cmpq	%rbx, %r9
	jne	.L2692
.L2710:
	movl	%r12d, %edx
	movq	-104(%rbp), %r12
	movslq	%r13d, %r8
	movl	%r15d, %r13d
	movq	%r10, %r15
.L2689:
	cmpl	-76(%rbp), %edx
	jl	.L2711
.L2698:
	movq	-72(%rbp), %rdx
	movq	-112(%rbp), %rsi
	leaq	-64(%rbp), %r8
	xorl	%ecx, %ecx
	movq	41224(%r12), %rax
	movq	%r12, %rdi
	movl	-4(%rax), %eax
	addl	%eax, %r13d
	movl	%eax, -64(%rbp)
	movl	%r13d, -60(%rbp)
	call	_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi@PLT
	movq	41216(%r12), %rdx
	movq	41232(%r12), %rax
	subq	%rdx, %rax
	cmpq	$32771, %rax
	jbe	.L2699
	cmpq	41224(%r12), %rdx
	je	.L2699
	movq	%rdx, 41224(%r12)
.L2699:
	movq	(%r15), %rax
	jmp	.L2686
	.p2align 4,,10
	.p2align 3
.L2707:
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	jmp	.L2686
	.p2align 4,,10
	.p2align 3
.L2690:
	xorl	%r8d, %r8d
	movl	%r13d, -96(%rbp)
	xorl	%edx, %edx
	movl	%r14d, %r13d
	movq	%r12, -120(%rbp)
	movq	%r15, %r12
	movl	%r8d, %r15d
	movq	%rbx, -104(%rbp)
	movq	%rax, %rbx
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2694:
	movq	-104(%rbp), %rax
	movq	(%r12), %rdx
	movslq	%r15d, %rcx
	addq	$4, %rbx
	addl	%r13d, %r15d
	movq	(%rax), %rdi
	leaq	15(%rdx,%rcx,2), %rsi
	xorl	%edx, %edx
	movl	%r13d, %ecx
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	movl	-96(%rbp), %eax
	leal	(%r14,%rax), %edx
	cmpq	%rbx, -88(%rbp)
	je	.L2712
.L2697:
	movl	(%rbx), %r14d
	cmpl	%edx, %r14d
	jle	.L2694
	movq	-72(%rbp), %rax
	movq	(%r12), %rcx
	movslq	%r15d, %rsi
	movl	%edx, -80(%rbp)
	movq	(%rax), %rdi
	leaq	15(%rcx,%rsi,2), %rsi
	movl	%r14d, %ecx
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	movl	-80(%rbp), %edx
	movl	%r14d, %eax
	subl	%edx, %eax
	addl	%eax, %r15d
	jmp	.L2694
	.p2align 4,,10
	.p2align 3
.L2712:
	movslq	%r15d, %r8
	movl	%eax, %r13d
	movq	%r12, %r15
	movq	-120(%rbp), %r12
	cmpl	-76(%rbp), %edx
	jge	.L2698
.L2711:
	movq	(%r15), %rax
	movl	-76(%rbp), %ecx
	leaq	15(%rax,%r8,2), %rsi
	movq	-72(%rbp), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	jmp	.L2698
	.p2align 4,,10
	.p2align 3
.L2709:
	movq	312(%r12), %rax
	jmp	.L2686
	.p2align 4,,10
	.p2align 3
.L2702:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	jmp	.L2689
.L2708:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22434:
	.size	_ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqTwoByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE, .-_ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqTwoByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE
	.section	.text._ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqOneByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqOneByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE, @function
_ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqOneByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE:
.LFB22430:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	leaq	41216(%rdi), %rcx
	subq	$88, %rsp
	movq	%rsi, -72(%rbp)
	movq	%r8, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41216(%rdi), %rax
	cmpq	41224(%rdi), %rax
	je	.L2714
	movq	%rax, 41224(%rdi)
.L2714:
	movq	(%rdx), %rax
	movq	%r12, %rdi
	movq	23(%rax), %rax
	movq	39(%rax), %rdx
	movq	-72(%rbp), %rax
	movq	(%rax), %rsi
	movl	11(%rdx), %r13d
	movl	11(%rsi), %eax
	movl	%eax, -76(%rbp)
	movq	(%rbx), %rax
	movl	11(%rax), %r14d
	call	_ZN2v88internal25FindStringIndicesDispatchEPNS0_7IsolateENS0_6StringES3_PSt6vectorIiSaIiEEj.constprop.0
	movq	41224(%r12), %rax
	movq	41216(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L2737
	subq	%rdx, %rax
	movslq	%r13d, %rcx
	movslq	%r14d, %rdx
	subq	%rcx, %rdx
	sarq	$2, %rax
	imulq	%rdx, %rax
	movslq	-76(%rbp), %rdx
	addq	%rdx, %rax
	cmpq	$1073741799, %rax
	jg	.L2731
	movl	%eax, %esi
	testl	%eax, %eax
	jne	.L2717
	movq	128(%r12), %rax
.L2716:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2738
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2731:
	.cfi_restore_state
	movl	$2147483647, %esi
.L2717:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2739
	movq	41224(%r12), %r9
	movq	41216(%r12), %rax
	movq	%r9, -88(%rbp)
	cmpq	%rax, %r9
	je	.L2732
	testl	%r14d, %r14d
	jg	.L2720
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r12, -104(%rbp)
	movq	%r15, %r10
	movq	%rax, %rbx
	movl	%r13d, %r15d
	movl	%edx, %r12d
	movl	%r8d, %r13d
	jmp	.L2722
	.p2align 4,,10
	.p2align 3
.L2721:
	addq	$4, %rbx
	leal	(%r15,%r14), %r12d
	cmpq	%rbx, %r9
	je	.L2740
.L2722:
	movl	(%rbx), %r14d
	cmpl	%r12d, %r14d
	jle	.L2721
	movq	-72(%rbp), %rax
	movq	(%r10), %rcx
	movslq	%r13d, %rdx
	movq	%r9, -96(%rbp)
	movq	%r10, -88(%rbp)
	addq	$4, %rbx
	movq	(%rax), %rdi
	leaq	15(%rcx,%rdx), %rsi
	movl	%r12d, %edx
	movl	%r14d, %ecx
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii@PLT
	movl	%r14d, %edx
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r10
	subl	%r12d, %edx
	leal	(%r15,%r14), %r12d
	addl	%edx, %r13d
	cmpq	%rbx, %r9
	jne	.L2722
.L2740:
	movl	%r12d, %edx
	movq	-104(%rbp), %r12
	movslq	%r13d, %r8
	movl	%r15d, %r13d
	movq	%r10, %r15
.L2719:
	cmpl	-76(%rbp), %edx
	jl	.L2741
.L2728:
	movq	-72(%rbp), %rdx
	movq	-112(%rbp), %rsi
	leaq	-64(%rbp), %r8
	xorl	%ecx, %ecx
	movq	41224(%r12), %rax
	movq	%r12, %rdi
	movl	-4(%rax), %eax
	addl	%eax, %r13d
	movl	%eax, -64(%rbp)
	movl	%r13d, -60(%rbp)
	call	_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi@PLT
	movq	41216(%r12), %rdx
	movq	41232(%r12), %rax
	subq	%rdx, %rax
	cmpq	$32771, %rax
	jbe	.L2729
	cmpq	41224(%r12), %rdx
	je	.L2729
	movq	%rdx, 41224(%r12)
.L2729:
	movq	(%r15), %rax
	jmp	.L2716
	.p2align 4,,10
	.p2align 3
.L2737:
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	jmp	.L2716
	.p2align 4,,10
	.p2align 3
.L2720:
	xorl	%r8d, %r8d
	movl	%r13d, -96(%rbp)
	xorl	%edx, %edx
	movl	%r14d, %r13d
	movq	%r12, -120(%rbp)
	movq	%r15, %r12
	movl	%r8d, %r15d
	movq	%rbx, -104(%rbp)
	movq	%rax, %rbx
	jmp	.L2727
	.p2align 4,,10
	.p2align 3
.L2724:
	movq	-104(%rbp), %rax
	movq	(%r12), %rcx
	movslq	%r15d, %rdx
	addq	$4, %rbx
	addl	%r13d, %r15d
	movq	(%rax), %rdi
	leaq	15(%rcx,%rdx), %rsi
	xorl	%edx, %edx
	movl	%r13d, %ecx
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii@PLT
	movl	-96(%rbp), %eax
	leal	(%r14,%rax), %edx
	cmpq	%rbx, -88(%rbp)
	je	.L2742
.L2727:
	movl	(%rbx), %r14d
	cmpl	%edx, %r14d
	jle	.L2724
	movq	-72(%rbp), %rax
	movq	(%r12), %rsi
	movslq	%r15d, %rcx
	movl	%edx, -80(%rbp)
	movq	(%rax), %rdi
	leaq	15(%rsi,%rcx), %rsi
	movl	%r14d, %ecx
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii@PLT
	movl	-80(%rbp), %edx
	movl	%r14d, %eax
	subl	%edx, %eax
	addl	%eax, %r15d
	jmp	.L2724
	.p2align 4,,10
	.p2align 3
.L2742:
	movslq	%r15d, %r8
	movl	%eax, %r13d
	movq	%r12, %r15
	movq	-120(%rbp), %r12
	cmpl	-76(%rbp), %edx
	jge	.L2728
.L2741:
	movq	(%r15), %rax
	movl	-76(%rbp), %ecx
	leaq	15(%rax,%r8), %rsi
	movq	-72(%rbp), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii@PLT
	jmp	.L2728
	.p2align 4,,10
	.p2align 3
.L2739:
	movq	312(%r12), %rax
	jmp	.L2716
	.p2align 4,,10
	.p2align 3
.L2732:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	jmp	.L2719
.L2738:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22430:
	.size	_ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqOneByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE, .-_ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqOneByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_113RegExpReplaceEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEES8_.str1.1,"aMS",@progbits,1
.LC37:
	.string	"(location_) != nullptr"
	.section	.rodata._ZN2v88internal12_GLOBAL__N_113RegExpReplaceEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEES8_.str1.8,"aMS",@progbits,1
	.align 8
.LC38:
	.string	"../deps/v8/src/runtime/runtime-regexp.cc:638"
	.section	.text._ZN2v88internal12_GLOBAL__N_113RegExpReplaceEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEES8_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_113RegExpReplaceEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEES8_, @function
_ZN2v88internal12_GLOBAL__N_113RegExpReplaceEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEES8_:
.LFB20250:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rbx
	movq	(%rcx), %rax
	sarq	$32, %rbx
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	movl	%ebx, %r8d
	andl	$1, %r8d
	cmpw	$63, 11(%rdx)
	jbe	.L2990
.L2745:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L2991
.L2753:
	movq	12464(%r15), %rax
	movq	39(%rax), %rax
	movq	1055(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2759
	movl	%r8d, -320(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-320(%rbp), %r8d
	movq	%rax, -312(%rbp)
	testl	%r8d, %r8d
	jne	.L2762
.L2999:
	movq	41112(%r15), %rdi
	andl	$8, %ebx
	jne	.L2992
.L2961:
	xorl	%ecx, %ecx
.L2763:
	movq	104(%r15), %rsi
	testq	%rdi, %rdi
	je	.L2776
.L2996:
	movl	%ecx, -320(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-320(%rbp), %ecx
	movq	%rax, %r8
	movq	(%r12), %rax
	cmpl	%ecx, 11(%rax)
	jnb	.L2993
.L2779:
	movq	(%r8), %rax
	cmpq	%rax, 104(%r15)
	jne	.L2781
	testl	%ebx, %ebx
	jne	.L2994
.L2782:
	movq	%r12, %rax
.L2814:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2995
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2991:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L2753
	movq	0(%r13), %rax
	movq	41112(%r15), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2756
	movl	%r8d, -312(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-312(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2992:
	movq	(%r14), %rax
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2764
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
	testb	$1, %sil
	jne	.L2767
.L3006:
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r15), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L2768
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2769:
	testq	%rax, %rax
	je	.L2814
.L2771:
	movq	(%rax), %rcx
	movq	41112(%r15), %rdi
	testb	$1, %cl
	jne	.L2773
	sarq	$32, %rcx
	movl	$0, %eax
	movq	104(%r15), %rsi
	testq	%rcx, %rcx
	cmovle	%eax, %ecx
	testq	%rdi, %rdi
	jne	.L2996
.L2776:
	movq	41088(%r15), %r8
	cmpq	41096(%r15), %r8
	je	.L2997
.L2778:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r8)
	movq	(%r12), %rax
	cmpl	%ecx, 11(%rax)
	jb	.L2779
.L2993:
	movq	-312(%rbp), %r8
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6RegExp4ExecEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEiNS4_INS0_15RegExpMatchInfoEEE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L2779
	.p2align 4,,10
	.p2align 3
.L2909:
	xorl	%eax, %eax
	jmp	.L2814
	.p2align 4,,10
	.p2align 3
.L2990:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L2745
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L2748
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L2748
	xorl	%edx, %edx
	movq	%rcx, %rsi
	movl	%r8d, -312(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movl	-312(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2759:
	movq	41088(%r15), %rax
	movq	%rax, -312(%rbp)
	cmpq	41096(%r15), %rax
	je	.L2998
.L2761:
	movq	-312(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rcx)
	testl	%r8d, %r8d
	je	.L2999
.L2762:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11RegExpUtils12SetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEm@PLT
	testq	%rax, %rax
	je	.L2909
	movq	0(%r13), %rax
	movl	11(%rax), %r8d
	testl	%r8d, %r8d
	jne	.L2816
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	testb	$8, 11(%rax)
	movq	(%r14), %rax
	movq	23(%rax), %rax
	je	.L2817
	testb	$1, %al
	jne	.L3000
.L2818:
	movq	15(%rax), %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	je	.L2989
.L2819:
	leaq	-192(%rbp), %rbx
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%r8d, -320(%rbp)
	call	_ZN2v88internal17RegExpGlobalCacheC1ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE@PLT
	movl	-192(%rbp), %r11d
	movl	-320(%rbp), %r8d
	testl	%r11d, %r11d
	jns	.L2823
.L2971:
	movq	312(%r15), %r12
.L2824:
	movq	%rbx, %rdi
	call	_ZN2v88internal17RegExpGlobalCacheD1Ev@PLT
	jmp	.L2985
	.p2align 4,,10
	.p2align 3
.L2781:
	movq	39(%rax), %rcx
	movq	47(%rax), %rax
	sarq	$32, %rax
	sarq	$32, %rcx
	movq	%rax, -312(%rbp)
	testl	%ebx, %ebx
	jne	.L3001
.L2783:
	leaq	-192(%rbp), %rbx
	movq	%r15, %rsi
	movq	%rcx, -328(%rbp)
	movq	%rbx, %rdi
	movq	%r8, -320(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	(%r12), %rax
	movq	-328(%rbp), %rcx
	movq	%r12, %rsi
	movq	-320(%rbp), %r8
	cmpl	11(%rax), %ecx
	je	.L2785
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r8, -320(%rbp)
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	-320(%rbp), %r8
	movq	%rax, %rsi
.L2785:
	movq	%rbx, %rdi
	movq	%r8, -320(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	0(%r13), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L2915
	movq	-320(%rbp), %r8
	movq	(%r12), %rdx
	movq	%r15, -120(%rbp)
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchE(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	$0, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	$0, -88(%rbp)
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	jbe	.L2787
	movq	%rdx, %rsi
	movq	%r12, %rax
.L2788:
	movq	-1(%rsi), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2796
	movq	-1(%rsi), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	je	.L3002
.L2796:
	movq	(%r14), %rdx
	movq	%rax, -112(%rbp)
	movq	23(%rdx), %rax
	testb	$1, %al
	jne	.L3003
.L2802:
	movq	15(%rax), %rax
	sarq	$32, %rax
	cmpl	$2, %eax
	je	.L3004
.L2803:
	movb	$0, -96(%rbp)
.L2806:
	leaq	-128(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2909
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	%r12, %rsi
	jmp	.L2786
	.p2align 4,,10
	.p2align 3
.L2764:
	movq	41088(%r15), %r8
	cmpq	%r8, 41096(%r15)
	je	.L3005
.L2766:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r8)
	testb	$1, %sil
	je	.L3006
.L2767:
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L2769
	.p2align 4,,10
	.p2align 3
.L2816:
	movq	(%r14), %rcx
	movq	23(%rcx), %rdx
	testb	$1, %dl
	jne	.L2875
.L2878:
	movq	15(%rdx), %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	je	.L2918
	cmpl	$2, %eax
	jne	.L2833
	movq	79(%rdx), %rax
	shrq	$32, %rax
	movq	%rax, -336(%rbp)
.L2876:
	movq	(%r12), %rax
	movl	11(%rax), %eax
	movl	%eax, -328(%rbp)
	movq	23(%rcx), %rax
	testb	$1, %al
	jne	.L3007
.L2879:
	movq	15(%rax), %r11
	sarq	$32, %r11
	cmpl	$2, %r11d
	jne	.L2881
	cmpb	$0, _ZN2v88internal19FLAG_regexp_tier_upE(%rip)
	jne	.L3008
.L2882:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6RegExp15IrregexpPrepareEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEE@PLT
	cmpl	$-1, %eax
	je	.L3009
.L2880:
	leaq	-192(%rbp), %rbx
	movq	41136(%r15), %rsi
	leaq	.LC38(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	leaq	-128(%rbp), %rax
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEEC1EPNS0_4ZoneENS4_9StartModeE
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movl	-328(%rbp), %r9d
	movl	-336(%rbp), %r8d
	movq	%rbx, -96(%rbp)
	movq	-320(%rbp), %rdi
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii
	movb	%al, -340(%rbp)
.L2884:
	leaq	-256(%rbp), %rax
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -352(%rbp)
	call	_ZN2v88internal17RegExpGlobalCacheC1ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE@PLT
	movl	-256(%rbp), %ecx
	testl	%ecx, %ecx
	jns	.L2887
.L2977:
	movq	312(%r15), %r12
.L2888:
	movq	-352(%rbp), %rdi
	call	_ZN2v88internal17RegExpGlobalCacheD1Ev@PLT
.L2886:
	movq	%rbx, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
.L2883:
	testb	$1, %r12b
	je	.L2909
	movq	-1(%r12), %rax
	cmpw	$63, 11(%rax)
	ja	.L2909
	.p2align 4,,10
	.p2align 3
.L2985:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2906
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L2814
	.p2align 4,,10
	.p2align 3
.L2915:
	movq	%r12, %rsi
.L2786:
	movq	(%r12), %rax
	movl	11(%rax), %ecx
	movq	-312(%rbp), %rax
	testq	%rax, %rax
	je	.L2813
	movq	%r12, %rsi
	movl	%eax, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %rsi
.L2813:
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	jmp	.L2814
	.p2align 4,,10
	.p2align 3
.L2756:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L3010
.L2758:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2994:
	movq	(%r14), %rax
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rdx
	movq	%rdx, 47(%rax)
	jmp	.L2782
	.p2align 4,,10
	.p2align 3
.L3001:
	movq	%rax, %rdx
	movq	(%r14), %rax
	salq	$32, %rdx
	movq	%rdx, 47(%rax)
	jmp	.L2783
	.p2align 4,,10
	.p2align 3
.L2773:
	movsd	7(%rcx), %xmm0
	comisd	.LC21(%rip), %xmm0
	jb	.L2961
	movsd	.LC22(%rip), %xmm1
	movl	$-1, %ecx
	comisd	%xmm0, %xmm1
	jbe	.L2763
	cvttsd2siq	%xmm0, %rcx
	jmp	.L2763
	.p2align 4,,10
	.p2align 3
.L2748:
	movq	41112(%r15), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2750
	movl	%r8d, -312(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-312(%rbp), %r8d
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L2745
	.p2align 4,,10
	.p2align 3
.L2768:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3011
.L2770:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2771
	.p2align 4,,10
	.p2align 3
.L2787:
	movq	-1(%rdx), %rax
	movq	%rdx, %rsi
	movzwl	11(%rax), %ecx
	movq	%r12, %rax
	andl	$7, %ecx
	cmpw	$1, %cx
	jne	.L2788
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L2791
	movq	23(%rdx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L2791
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2998:
	movq	%r15, %rdi
	movq	%rsi, -328(%rbp)
	movl	%r8d, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-328(%rbp), %rsi
	movl	-320(%rbp), %r8d
	movq	%rax, -312(%rbp)
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L2817:
	testb	$1, %al
	jne	.L3012
.L2848:
	movq	15(%rax), %rax
	sarq	$32, %rax
	cmpl	$1, %eax
	je	.L2989
.L2849:
	leaq	-128(%rbp), %rax
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movl	%r8d, -328(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal17RegExpGlobalCacheC1ENS0_6HandleINS0_8JSRegExpEEENS2_INS0_6StringEEEPNS0_7IsolateE@PLT
	movl	-128(%rbp), %r8d
	testl	%r8d, %r8d
	movl	-328(%rbp), %r8d
	jns	.L2853
.L2974:
	movq	312(%r15), %r12
.L2854:
	movq	-320(%rbp), %rdi
	call	_ZN2v88internal17RegExpGlobalCacheD1Ev@PLT
	jmp	.L2985
	.p2align 4,,10
	.p2align 3
.L3003:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37504(%rcx), %rax
	je	.L2803
	jmp	.L2802
	.p2align 4,,10
	.p2align 3
.L2750:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L3013
.L2752:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L2745
	.p2align 4,,10
	.p2align 3
.L2918:
	movl	$0, -336(%rbp)
	jmp	.L2876
	.p2align 4,,10
	.p2align 3
.L3002:
	movq	(%rax), %rax
	movq	41112(%r15), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2799
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L3009:
	movq	312(%r15), %r12
	jmp	.L2883
	.p2align 4,,10
	.p2align 3
.L3008:
	leaq	-128(%rbp), %rdi
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal8JSRegExp21MarkTierUpForNextExecEv@PLT
	jmp	.L2882
	.p2align 4,,10
	.p2align 3
.L3000:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37504(%rcx), %rax
	je	.L2819
	jmp	.L2818
	.p2align 4,,10
	.p2align 3
.L3012:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37504(%rcx), %rax
	je	.L2849
	jmp	.L2848
.L3013:
	movq	%r15, %rdi
	movq	%rsi, -320(%rbp)
	movl	%r8d, -312(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	movl	-312(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L2752
	.p2align 4,,10
	.p2align 3
.L2906:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3014
.L2908:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%r12, (%rax)
	jmp	.L2814
	.p2align 4,,10
	.p2align 3
.L2997:
	movq	%r15, %rdi
	movq	%rsi, -328(%rbp)
	movl	%ecx, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-328(%rbp), %rsi
	movl	-320(%rbp), %ecx
	movq	%rax, %r8
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2881:
	leaq	-192(%rbp), %rbx
	movq	41136(%r15), %rsi
	leaq	.LC38(%rip), %rdx
	movq	%r11, -352(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	leaq	-128(%rbp), %rax
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal13ZoneChunkListINS0_19CompiledReplacement15ReplacementPartEEC1EPNS0_4ZoneENS4_9StartModeE
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movl	-328(%rbp), %r9d
	movl	-336(%rbp), %r8d
	movq	%rbx, -96(%rbp)
	movq	-320(%rbp), %rdi
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii
	movq	-352(%rbp), %r11
	movb	%al, -340(%rbp)
	cmpl	$1, %r11d
	jne	.L2884
	testb	%al, %al
	je	.L2884
	movq	(%r12), %rax
	movq	-1(%rax), %rax
	testb	$8, 11(%rax)
	je	.L2885
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	testb	$8, 11(%rax)
	jne	.L3015
.L2885:
	movq	-312(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqTwoByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE
	movq	%rax, %r12
	jmp	.L2886
	.p2align 4,,10
	.p2align 3
.L2989:
	movq	-1(%rdx), %rax
	leaq	128(%r15), %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	-312(%rbp), %r8
	movq	%r15, %rdi
	testb	$8, 11(%rax)
	je	.L2850
	call	_ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqOneByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE
	movq	%rax, %r12
	jmp	.L2985
	.p2align 4,,10
	.p2align 3
.L2875:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rdx
	jne	.L2878
.L2833:
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3007:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rax
	je	.L2880
	jmp	.L2879
	.p2align 4,,10
	.p2align 3
.L3004:
	movq	23(%rdx), %rax
	movq	87(%rax), %rsi
	testb	$1, %sil
	je	.L2803
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	subl	$123, %eax
	cmpw	$14, %ax
	ja	.L2803
	movq	41112(%r15), %rdi
	movb	$1, -96(%rbp)
	testq	%rdi, %rdi
	je	.L3016
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2809:
	movq	%rax, -88(%rbp)
	jmp	.L2806
	.p2align 4,,10
	.p2align 3
.L3005:
	movq	%r15, %rdi
	movq	%rsi, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L2766
	.p2align 4,,10
	.p2align 3
.L2887:
	movq	-352(%rbp), %rdi
	call	_ZN2v88internal17RegExpGlobalCache9FetchNextEv@PLT
	testq	%rax, %rax
	je	.L3017
	movq	%rax, -360(%rbp)
	movq	-120(%rbp), %rax
	leaq	-304(%rbp), %r14
	movq	%r12, %rdx
	leaq	37592(%r15), %rsi
	movq	%r14, %rdi
	leal	5(,%rax,4), %ecx
	call	_ZN2v88internal24ReplacementStringBuilderC1EPNS0_4HeapENS0_6HandleINS0_6StringEEEi@PLT
	movq	-360(%rbp), %r8
	xorl	%eax, %eax
	movq	%r13, -368(%rbp)
	movq	%r15, -384(%rbp)
	movl	%eax, %r13d
	movq	-352(%rbp), %r15
	movq	%r12, -392(%rbp)
	movq	%r8, %r12
	movq	%rbx, -400(%rbp)
	jmp	.L2898
	.p2align 4,,10
	.p2align 3
.L3019:
	movq	-368(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal24ReplacementStringBuilder9AddStringENS0_6HandleINS0_6StringEEE@PLT
.L2980:
	movq	%r15, %rdi
	call	_ZN2v88internal17RegExpGlobalCache9FetchNextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2968
	movl	%ebx, %r13d
.L2898:
	movl	(%r12), %edx
	movl	4(%r12), %ebx
	cmpl	%r13d, %edx
	jg	.L3018
.L2891:
	cmpb	$0, -340(%rbp)
	jne	.L3019
	movq	-320(%rbp), %rdi
	movq	%r12, %r8
	movl	%ebx, %ecx
	movq	%r14, %rsi
	call	_ZN2v88internal19CompiledReplacement5ApplyEPNS0_24ReplacementStringBuilderEiiPi
	jmp	.L2980
	.p2align 4,,10
	.p2align 3
.L3018:
	movl	$2, %esi
	movq	%r14, %rdi
	movl	%edx, -360(%rbp)
	call	_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi@PLT
	movl	-360(%rbp), %edx
	movl	%edx, %ecx
	subl	%r13d, %ecx
	testl	$-2048, %ecx
	jne	.L2892
	testl	$-524288, %r13d
	je	.L3020
.L2892:
	movl	%r13d, %esi
	leaq	-296(%rbp), %rdi
	movl	%ecx, -372(%rbp)
	subl	%edx, %esi
	movl	%edx, -344(%rbp)
	salq	$32, %rsi
	movq	%rdi, -360(%rbp)
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movq	-360(%rbp), %rdi
	movq	%r13, %rsi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movl	-372(%rbp), %ecx
	movl	-344(%rbp), %edx
.L2893:
	movl	-272(%rbp), %eax
	movl	$1073741799, %esi
	subl	%ecx, %esi
	addl	%eax, %ecx
	cmpl	%esi, %eax
	movl	$2147483647, %eax
	cmovg	%eax, %ecx
	movl	%ecx, -272(%rbp)
	jmp	.L2891
	.p2align 4,,10
	.p2align 3
.L2850:
	call	_ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqTwoByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE
	movq	%rax, %r12
	jmp	.L2985
	.p2align 4,,10
	.p2align 3
.L3010:
	movq	%r15, %rdi
	movq	%rsi, -320(%rbp)
	movl	%r8d, -312(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	movl	-312(%rbp), %r8d
	movq	%rax, %r13
	jmp	.L2758
	.p2align 4,,10
	.p2align 3
.L3011:
	movq	%r15, %rdi
	movq	%rsi, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	jmp	.L2770
	.p2align 4,,10
	.p2align 3
.L2791:
	movq	41112(%r15), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L2793
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	jmp	.L2788
	.p2align 4,,10
	.p2align 3
.L2799:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3021
.L2801:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2853:
	movq	-320(%rbp), %rdi
	movl	%r8d, -328(%rbp)
	call	_ZN2v88internal17RegExpGlobalCache9FetchNextEv@PLT
	movl	-328(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L3022
	movq	(%r14), %rdx
	movl	(%rax), %edi
	movl	4(%rax), %eax
	movq	23(%rdx), %rsi
	testb	$1, %sil
	jne	.L2857
.L2860:
	movq	15(%rsi), %rdx
	sarq	$32, %rdx
	cmpl	$1, %edx
	jne	.L3023
	movl	$0, -360(%rbp)
.L2858:
	movq	(%r12), %rdx
	subl	%edi, %eax
	movl	11(%rdx), %ebx
	movl	%ebx, -352(%rbp)
	subl	%eax, %ebx
	movl	%ebx, -340(%rbp)
	jne	.L2861
.L2975:
	movq	128(%r15), %r12
	jmp	.L2854
	.p2align 4,,10
	.p2align 3
.L2823:
	movq	%rbx, %rdi
	movl	%r8d, -320(%rbp)
	call	_ZN2v88internal17RegExpGlobalCache9FetchNextEv@PLT
	movl	-320(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L3024
	movq	(%r14), %rdx
	movl	(%rax), %edi
	movl	4(%rax), %eax
	movq	23(%rdx), %rsi
	testb	$1, %sil
	jne	.L2827
.L2832:
	movq	15(%rsi), %rdx
	sarq	$32, %rdx
	cmpl	$1, %edx
	jne	.L3025
	movl	$0, -352(%rbp)
.L2828:
	movq	(%r12), %rdx
	subl	%edi, %eax
	movl	11(%rdx), %edx
	movl	%edx, -340(%rbp)
	subl	%eax, %edx
	movl	%edx, -336(%rbp)
	jne	.L2834
.L2972:
	movq	128(%r15), %r12
	jmp	.L2824
	.p2align 4,,10
	.p2align 3
.L3014:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L2908
	.p2align 4,,10
	.p2align 3
.L2968:
	movl	-256(%rbp), %eax
	movl	%ebx, -320(%rbp)
	movq	-384(%rbp), %r15
	movq	-392(%rbp), %r12
	movq	-400(%rbp), %rbx
	testl	%eax, %eax
	js	.L2977
	movl	-320(%rbp), %ecx
	cmpl	%ecx, -328(%rbp)
	jg	.L3026
.L2900:
	movq	-352(%rbp), %rdi
	call	_ZN2v88internal17RegExpGlobalCache19LastSuccessfulMatchEv@PLT
	movl	-336(%rbp), %ecx
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	-312(%rbp), %rsi
	movq	%rax, %r8
	call	_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi@PLT
	movq	%r14, %rdi
	call	_ZN2v88internal24ReplacementStringBuilder8ToStringEv@PLT
	testq	%rax, %rax
	je	.L2977
	movq	(%rax), %r12
	jmp	.L2888
.L3017:
	movl	-256(%rbp), %edx
	testl	%edx, %edx
	js	.L2977
	movq	(%r12), %r12
	jmp	.L2888
.L2793:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3027
.L2795:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2788
.L3015:
	movq	-312(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internalL39StringReplaceGlobalAtomRegExpWithStringINS0_16SeqOneByteStringEEENS0_6ObjectEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS6_INS0_8JSRegExpEEES8_NS6_INS0_15RegExpMatchInfoEEE
	movq	%rax, %r12
	jmp	.L2886
.L3016:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3028
.L2810:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2809
.L3024:
	movl	-192(%rbp), %r10d
	testl	%r10d, %r10d
	js	.L2971
	movq	(%r12), %r12
	jmp	.L2824
.L3022:
	movl	-128(%rbp), %edi
	testl	%edi, %edi
	js	.L2974
	movq	(%r12), %r12
	jmp	.L2854
.L3025:
	cmpl	$2, %edx
	jne	.L2833
	movq	79(%rsi), %rdx
	shrq	$32, %rdx
	movq	%rdx, -352(%rbp)
	jmp	.L2828
.L3023:
	cmpl	$2, %edx
	jne	.L2833
	movq	79(%rsi), %rdx
	shrq	$32, %rdx
	movq	%rdx, -360(%rbp)
	jmp	.L2858
.L3020:
	movl	%r13d, %esi
	leaq	-296(%rbp), %rdi
	movl	%edx, -344(%rbp)
	sall	$11, %esi
	movl	%ecx, -360(%rbp)
	orl	%ecx, %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movl	-360(%rbp), %ecx
	movl	-344(%rbp), %edx
	jmp	.L2893
.L3021:
	movq	%r15, %rdi
	movq	%rsi, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	jmp	.L2801
.L2827:
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L2833
	jmp	.L2832
.L2834:
	movl	%edx, %esi
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%rcx, -360(%rbp)
	movl	%r8d, -320(%rbp)
	xorl	%r14d, %r14d
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movl	-320(%rbp), %r8d
	movq	-360(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, -328(%rbp)
	je	.L2862
	movq	%r15, -360(%rbp)
	movq	%rcx, %rax
	movl	%r8d, %r15d
	jmp	.L2839
	.p2align 4,,10
	.p2align 3
.L2836:
	movq	%rbx, %rdi
	call	_ZN2v88internal17RegExpGlobalCache9FetchNextEv@PLT
	testq	%rax, %rax
	je	.L2966
.L2838:
	movl	%r13d, %r15d
.L2839:
	movl	(%rax), %ecx
	movl	4(%rax), %r13d
	cmpl	%r15d, %ecx
	jle	.L2836
	movq	-328(%rbp), %rax
	movq	(%r12), %rdi
	movl	%ecx, -320(%rbp)
	movq	(%rax), %rdx
	movslq	%r14d, %rax
	leaq	15(%rdx,%rax), %rsi
	movl	%r15d, %edx
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii@PLT
	movl	-320(%rbp), %ecx
	movq	%rbx, %rdi
	subl	%r15d, %ecx
	addl	%ecx, %r14d
	call	_ZN2v88internal17RegExpGlobalCache9FetchNextEv@PLT
	testq	%rax, %rax
	jne	.L2838
.L2966:
	movl	-192(%rbp), %r9d
	movq	-360(%rbp), %r15
	testl	%r9d, %r9d
	js	.L2971
	movq	%rbx, %rdi
	call	_ZN2v88internal17RegExpGlobalCache19LastSuccessfulMatchEv@PLT
	movl	-352(%rbp), %ecx
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	-312(%rbp), %rsi
	movq	%rax, %r8
	call	_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi@PLT
	cmpl	%r13d, -340(%rbp)
	jg	.L3029
	testl	%r14d, %r14d
	je	.L2972
.L2842:
	movl	-336(%rbp), %eax
	movq	-328(%rbp), %rcx
	leal	23(%r14), %r13d
	andl	$-8, %r13d
	addl	$23, %eax
	movq	(%rcx), %rdx
	andl	$-8, %eax
	subl	%r13d, %eax
	movl	%r14d, 11(%rdx)
	movl	%eax, %r12d
	jne	.L2843
	movq	(%rcx), %r12
	jmp	.L2824
.L2861:
	movl	%ebx, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rcx, -336(%rbp)
	movl	%r8d, -328(%rbp)
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2862
	movq	-336(%rbp), %rcx
	movq	%r15, -368(%rbp)
	xorl	%r13d, %r13d
	movl	-328(%rbp), %r8d
	movq	-320(%rbp), %r15
	movq	%rcx, %rax
	jmp	.L2866
	.p2align 4,,10
	.p2align 3
.L2863:
	movq	%r15, %rdi
	call	_ZN2v88internal17RegExpGlobalCache9FetchNextEv@PLT
	testq	%rax, %rax
	je	.L2967
.L2865:
	movl	%ebx, %r8d
.L2866:
	movl	(%rax), %ecx
	movl	4(%rax), %ebx
	cmpl	%r8d, %ecx
	jle	.L2863
	movq	(%r14), %rax
	movslq	%r13d, %rdx
	movq	(%r12), %rdi
	movl	%ecx, -336(%rbp)
	movl	%r8d, -328(%rbp)
	leaq	15(%rax,%rdx,2), %rsi
	movl	%r8d, %edx
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	movl	-328(%rbp), %r8d
	movl	-336(%rbp), %ecx
	movq	%r15, %rdi
	subl	%r8d, %ecx
	addl	%ecx, %r13d
	call	_ZN2v88internal17RegExpGlobalCache9FetchNextEv@PLT
	testq	%rax, %rax
	jne	.L2865
.L2967:
	movl	-128(%rbp), %esi
	movq	-368(%rbp), %r15
	testl	%esi, %esi
	js	.L2974
	movq	-320(%rbp), %rdi
	call	_ZN2v88internal17RegExpGlobalCache19LastSuccessfulMatchEv@PLT
	movl	-360(%rbp), %ecx
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	-312(%rbp), %rsi
	movq	%rax, %r8
	call	_ZN2v88internal6RegExp16SetLastMatchInfoEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEENS4_INS0_6StringEEEiPi@PLT
	cmpl	%ebx, -352(%rbp)
	jg	.L3030
	testl	%r13d, %r13d
	je	.L2975
.L2869:
	movl	-340(%rbp), %eax
	leal	23(%r13,%r13), %ebx
	movq	(%r14), %rdx
	andl	$-8, %ebx
	leal	23(%rax,%rax), %eax
	movl	%r13d, 11(%rdx)
	andl	$-8, %eax
	subl	%ebx, %eax
	movl	%eax, %r12d
	jne	.L2870
.L2871:
	movq	(%r14), %r12
	jmp	.L2854
.L2857:
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-37504(%rdx), %rsi
	je	.L2833
	jmp	.L2860
.L3026:
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi@PLT
	movl	-328(%rbp), %r13d
	movl	-320(%rbp), %eax
	subl	%eax, %r13d
	testl	$-2048, %r13d
	jne	.L2901
	testl	$-524288, %eax
	je	.L3031
.L2901:
	movl	-320(%rbp), %esi
	subl	-328(%rbp), %esi
	leaq	-296(%rbp), %rdi
	salq	$32, %rsi
	movq	%rdi, -328(%rbp)
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movq	-320(%rbp), %rsi
	movq	-328(%rbp), %rdi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
.L2902:
	movl	-272(%rbp), %edx
	movl	$1073741799, %eax
	subl	%r13d, %eax
	addl	%edx, %r13d
	cmpl	%eax, %edx
	movl	$2147483647, %eax
	cmovg	%eax, %r13d
	movl	%r13d, -272(%rbp)
	jmp	.L2900
.L3027:
	movq	%r15, %rdi
	movq	%rsi, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	jmp	.L2795
.L3028:
	movq	%r15, %rdi
	movq	%rsi, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	jmp	.L2810
.L2862:
	leaq	.LC37(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3031:
	movl	%eax, %esi
	leaq	-296(%rbp), %rdi
	sall	$11, %esi
	orl	%r13d, %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	jmp	.L2902
.L2995:
	call	__stack_chk_fail@PLT
.L3029:
	movq	-328(%rbp), %rax
	movq	(%r12), %rdi
	movl	-340(%rbp), %r12d
	movq	(%rax), %rdx
	movslq	%r14d, %rax
	movl	%r12d, %ecx
	leaq	15(%rdx,%rax), %rsi
	movl	%r13d, %edx
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii@PLT
	movl	%r12d, %eax
	subl	%r13d, %eax
	addl	%eax, %r14d
	jmp	.L2842
.L3030:
	movq	(%r14), %rdx
	movq	(%r12), %rdi
	movslq	%r13d, %rax
	movl	-352(%rbp), %r12d
	addq	%rax, %rax
	leaq	15(%rdx,%rax), %rsi
	movl	%ebx, %edx
	movl	%r12d, %ecx
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	movl	%r12d, %eax
	subl	%ebx, %eax
	addl	%eax, %r13d
	jmp	.L2869
.L2843:
	movq	-328(%rbp), %rax
	movq	(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap13IsLargeObjectENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L3032
.L2844:
	movq	-328(%rbp), %rax
	movq	(%rax), %r12
	jmp	.L2824
.L2870:
	movq	(%r14), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal4Heap13IsLargeObjectENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L2871
	movslq	%ebx, %rbx
	leaq	37592(%r15), %rdi
	movl	$1, %ecx
	movl	%r12d, %edx
	leaq	-1(%r13,%rbx), %rsi
	movl	$1, %r8d
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	jmp	.L2871
.L3032:
	movslq	%r13d, %r13
	leaq	37592(%r15), %rdi
	movl	$1, %ecx
	movl	%r12d, %edx
	leaq	-1(%r14,%r13), %rsi
	movl	$1, %r8d
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	jmp	.L2844
	.cfi_endproc
.LFE20250:
	.size	_ZN2v88internal12_GLOBAL__N_113RegExpReplaceEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEES8_, .-_ZN2v88internal12_GLOBAL__N_113RegExpReplaceEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEES8_
	.section	.rodata._ZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"V8.Runtime_Runtime_RegExpReplaceRT"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC40:
	.string	"!functional_replace"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC41:
	.string	"../deps/v8/src/runtime/runtime-regexp.cc:1766"
	.section	.text._ZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE.isra.0:
.LFB25935:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -320(%rbp)
	movq	$0, -288(%rbp)
	movaps	%xmm0, -304(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3235
.L3034:
	movq	_ZZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateEE29trace_event_unique_atomic1713(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3236
.L3036:
	movq	$0, -416(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3237
.L3038:
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	41096(%r15), %r14
	movq	%rax, -472(%rbp)
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L3042
.L3043:
	leaq	.LC20(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3236:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3238
.L3037:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateEE29trace_event_unique_atomic1713(%rip)
	jmp	.L3036
	.p2align 4,,10
	.p2align 3
.L3042:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3043
	movq	-8(%r12), %rax
	leaq	-8(%r12), %rbx
	testb	$1, %al
	jne	.L3239
.L3044:
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3237:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3240
.L3039:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3040
	movq	(%rdi), %rax
	call	*8(%rax)
.L3040:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3041
	movq	(%rdi), %rax
	call	*8(%rax)
.L3041:
	leaq	.LC39(%rip), %rax
	movq	%rbx, -408(%rbp)
	movq	%rax, -400(%rbp)
	leaq	-408(%rbp), %rax
	movq	%r13, -392(%rbp)
	movq	%rax, -416(%rbp)
	jmp	.L3038
	.p2align 4,,10
	.p2align 3
.L3239:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L3044
	leaq	-16(%r12), %rcx
	movq	%rax, %rsi
	movq	%rcx, -480(%rbp)
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L3047
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L3241
.L3047:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L3055
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L3242
.L3055:
	movq	-16(%r12), %rax
	testb	$1, %al
	jne	.L3243
.L3062:
	leaq	-16(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3234
.L3068:
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r13, -488(%rbp)
	call	_ZN2v88internal11RegExpUtils18IsUnmodifiedRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movb	%al, -489(%rbp)
	testb	%al, %al
	je	.L3066
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12_GLOBAL__N_113RegExpReplaceEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEES8_
	testq	%rax, %rax
	je	.L3234
	movq	(%rax), %r12
.L3069:
	movq	-472(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r14
	je	.L3171
	movq	%r14, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3171:
	leaq	-416(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-320(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3244
.L3033:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3245
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3249:
	.cfi_restore_state
	movb	$1, -489(%rbp)
	.p2align 4,,10
	.p2align 3
.L3066:
	movq	(%rbx), %rax
	leaq	2632(%r15), %rsi
	movl	$3, %edx
	movl	11(%rax), %eax
	movl	%eax, -496(%rbp)
	movq	2632(%r15), %rax
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L3072
	movl	11(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$3, %edx
.L3072:
	movabsq	$824633720832, %rax
	movl	%edx, -160(%rbp)
	movq	%rax, -148(%rbp)
	movq	2632(%r15), %rax
	movq	%r15, -136(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	jne	.L3073
	movq	%r15, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
.L3073:
	leaq	-160(%rbp), %r13
	movq	%rsi, -128(%rbp)
	movq	%r13, %rdi
	movq	$0, -120(%rbp)
	movq	%r12, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -156(%rbp)
	jne	.L3074
	movq	-136(%rbp), %rax
	addq	$88, %rax
.L3075:
	movq	(%rax), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movb	%al, -512(%rbp)
	testb	%al, %al
	je	.L3178
	movq	3528(%r15), %rax
	leaq	3528(%r15), %rsi
	movl	$3, %edx
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L3077
	movl	11(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$3, %edx
.L3077:
	movabsq	$824633720832, %rax
	movl	%edx, -160(%rbp)
	movq	%rax, -148(%rbp)
	movq	3528(%r15), %rax
	movq	%r15, -136(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L3246
.L3078:
	movq	%r13, %rdi
	movq	%rsi, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%r12, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -156(%rbp)
	je	.L3247
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L3080
	.p2align 4,,10
	.p2align 3
.L3234:
	movq	312(%r15), %r12
	jmp	.L3069
	.p2align 4,,10
	.p2align 3
.L3243:
	movq	-1(%rax), %rdx
	testb	$2, 13(%rdx)
	je	.L3248
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	$0, -488(%rbp)
	call	_ZN2v88internal11RegExpUtils18IsUnmodifiedRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L3249
	leaq	.LC40(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3248:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L3062
	leaq	-16(%r12), %r13
	jmp	.L3068
	.p2align 4,,10
	.p2align 3
.L3242:
	movq	(%rbx), %rax
	movq	41112(%r15), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3058
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
	jmp	.L3055
	.p2align 4,,10
	.p2align 3
.L3074:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L3075
	jmp	.L3234
	.p2align 4,,10
	.p2align 3
.L3241:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L3050
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L3050
	movq	%rbx, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	jmp	.L3055
	.p2align 4,,10
	.p2align 3
.L3050:
	movq	41112(%r15), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3052
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	jmp	.L3047
	.p2align 4,,10
	.p2align 3
.L3235:
	movq	40960(%rsi), %rax
	movl	$497, %edx
	leaq	-312(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -320(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3034
	.p2align 4,,10
	.p2align 3
.L3178:
	movb	$0, -536(%rbp)
.L3076:
	leaq	-224(%rbp), %rax
	movq	41136(%r15), %rsi
	leaq	.LC41(%rip), %rdx
	movq	%rax, %rdi
	movq	%rax, -520(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	-520(%rbp), %rax
	movq	$0, -376(%rbp)
	movq	$0, -368(%rbp)
	movq	%rax, -384(%rbp)
	leaq	88(%r15), %rax
	movq	%rax, -504(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, -544(%rbp)
	leaq	-384(%rbp), %rax
	movq	$0, -360(%rbp)
	movq	%rax, -552(%rbp)
.L3096:
	movq	-504(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	$0, -272(%rbp)
	call	_ZN2v88internal11RegExpUtils10RegExpExecEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L3233
	movq	%rax, -272(%rbp)
	movq	104(%r15), %rcx
	cmpq	%rcx, (%rax)
	je	.L3083
	movq	-368(%rbp), %rsi
	cmpq	-360(%rbp), %rsi
	je	.L3084
	movq	%rax, (%rsi)
	addq	$8, -368(%rbp)
.L3085:
	cmpb	$0, -512(%rbp)
	je	.L3083
	movq	-272(%rbp), %rcx
	movq	(%rcx), %rax
	testb	$1, %al
	jne	.L3086
.L3088:
	movq	%rcx, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rcx, -528(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-528(%rbp), %rcx
.L3087:
	movq	%rax, -96(%rbp)
	movq	%r13, %rdi
	movabsq	$824633720832, %rdx
	movabsq	$-4294967296, %rax
	movl	$3, -160(%rbp)
	movq	%rdx, -148(%rbp)
	movq	%r15, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -156(%rbp)
	jne	.L3089
	movq	-136(%rbp), %rax
	leaq	88(%rax), %rsi
.L3090:
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3091
.L3094:
	movq	%r15, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3233
.L3093:
	movq	(%rsi), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jne	.L3096
	movzbl	-536(%rbp), %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11RegExpUtils22SetAdvancedStringIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEEb@PLT
	testq	%rax, %rax
	jne	.L3096
	.p2align 4,,10
	.p2align 3
.L3233:
	movq	312(%r15), %r12
.L3097:
	movq	-520(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	jmp	.L3069
	.p2align 4,,10
	.p2align 3
.L3089:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L3090
	jmp	.L3233
	.p2align 4,,10
	.p2align 3
.L3058:
	movq	41088(%r15), %rbx
	cmpq	41096(%r15), %rbx
	je	.L3250
.L3060:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L3055
	.p2align 4,,10
	.p2align 3
.L3086:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3088
	movq	%rcx, %rax
	jmp	.L3087
	.p2align 4,,10
	.p2align 3
.L3244:
	leaq	-312(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3033
	.p2align 4,,10
	.p2align 3
.L3091:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L3094
	jmp	.L3093
	.p2align 4,,10
	.p2align 3
.L3240:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC39(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L3039
	.p2align 4,,10
	.p2align 3
.L3238:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3037
	.p2align 4,,10
	.p2align 3
.L3247:
	movq	-136(%rbp), %rax
	addq	$88, %rax
.L3080:
	movq	(%rax), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movb	%al, -536(%rbp)
	call	_ZN2v88internal11RegExpUtils12SetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEm@PLT
	testq	%rax, %rax
	jne	.L3076
	jmp	.L3234
	.p2align 4,,10
	.p2align 3
.L3052:
	movq	41088(%r15), %rbx
	cmpq	41096(%r15), %rbx
	je	.L3251
.L3054:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L3047
	.p2align 4,,10
	.p2align 3
.L3084:
	movq	-544(%rbp), %rdx
	movq	-552(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L3085
	.p2align 4,,10
	.p2align 3
.L3246:
	movq	%r15, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L3078
	.p2align 4,,10
	.p2align 3
.L3250:
	movq	%r15, %rdi
	movq	%rsi, -488(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-488(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L3060
	.p2align 4,,10
	.p2align 3
.L3083:
	leaq	-272(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -552(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	-368(%rbp), %rcx
	movq	-376(%rbp), %rax
	movl	$0, -544(%rbp)
	movq	%rcx, -568(%rbp)
	cmpq	%rax, %rcx
	je	.L3098
	leaq	2768(%r15), %rcx
	movq	%rbx, -560(%rbp)
	movq	%rcx, -528(%rbp)
	movq	%r14, -512(%rbp)
	movq	%rax, %r14
.L3167:
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	%r15, -448(%rbp)
	movq	-528(%rbp), %r12
	movq	%rax, -440(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -432(%rbp)
	movq	(%r14), %rbx
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L3099
.L3101:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rdx
.L3100:
	movq	2768(%r15), %rcx
	movl	$3, %eax
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L3102
	movl	11(%rcx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L3102:
	movl	%eax, -160(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -148(%rbp)
	movq	2768(%r15), %rax
	movq	%r15, -136(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L3252
.L3103:
	movq	%r13, %rdi
	movq	%r12, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	$-1, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -156(%rbp)
	jne	.L3104
	movq	-136(%rbp), %rax
	leaq	88(%rax), %rsi
.L3105:
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3107
	sarq	$32, %rax
	movl	$0, %edx
	movq	41112(%r15), %rdi
	cmovs	%rdx, %rax
	salq	$32, %rax
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L3108
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3109:
	testq	%rax, %rax
	je	.L3231
.L3111:
	movq	(%rax), %r12
	testb	$1, %r12b
	jne	.L3112
	sarq	$32, %r12
	movl	$0, %eax
	testq	%r12, %r12
	cmovle	%eax, %r12d
.L3113:
	movq	(%r14), %rbx
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L3116
.L3118:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
.L3117:
	movq	%rax, -96(%rbp)
	movq	%r13, %rdi
	movabsq	$824633720832, %rcx
	movabsq	$-4294967296, %rax
	movl	$3, -160(%rbp)
	movq	%rcx, -148(%rbp)
	movq	%r15, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -156(%rbp)
	jne	.L3119
	movq	-136(%rbp), %rax
	leaq	88(%rax), %rsi
.L3120:
	movq	%r15, %rdi
	call	_ZN2v88internal6Object8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	%rax, -576(%rbp)
	testq	%rax, %rax
	je	.L3231
	movq	(%rax), %rax
	movq	(%r14), %rsi
	leaq	2688(%r15), %rdx
	movq	%r15, %rdi
	movl	11(%rax), %eax
	movl	%eax, -588(%rbp)
	call	_ZN2v88internal6Object11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3231
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L3253
.L3124:
	movl	$0, -584(%rbp)
	sarq	$32, %rax
	testq	%rax, %rax
	jle	.L3127
.L3128:
	movl	-496(%rbp), %ecx
	cmpl	%eax, %ecx
	cmovbe	%ecx, %eax
	movl	%eax, -584(%rbp)
.L3127:
	movq	-520(%rbp), %rax
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -328(%rbp)
	testl	%r12d, %r12d
	je	.L3131
	xorl	%ebx, %ebx
.L3142:
	movq	$0, -456(%rbp)
	movq	(%r14), %rcx
	movq	(%rcx), %rax
	testb	$1, %al
	jne	.L3132
.L3134:
	movq	%rcx, %rsi
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	%rcx, -536(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-536(%rbp), %rcx
.L3133:
	movabsq	$824633720832, %rdx
	movq	%r13, %rdi
	movl	$3, -160(%rbp)
	movq	%rdx, -148(%rbp)
	movq	%r15, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -96(%rbp)
	movl	%ebx, -88(%rbp)
	movl	$-1, -84(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -156(%rbp)
	jne	.L3135
	movq	-136(%rbp), %rax
	leaq	88(%rax), %rsi
.L3136:
	movq	%rsi, -456(%rbp)
	movq	88(%r15), %rax
	cmpq	%rax, (%rsi)
	je	.L3138
	movq	%r15, %rdi
	call	_ZN2v88internal6Object8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3231
	movq	%rax, -456(%rbp)
.L3138:
	movq	-336(%rbp), %r8
	cmpq	-328(%rbp), %r8
	je	.L3140
	addl	$1, %ebx
	movq	%rsi, (%r8)
	addq	$8, -336(%rbp)
	cmpl	%r12d, %ebx
	jne	.L3142
.L3131:
	movq	(%r14), %rsi
	leaq	2648(%r15), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L3231
	movq	(%rax), %rax
	movq	88(%r15), %rcx
	cmpb	$0, -489(%rbp)
	movq	%rax, -600(%rbp)
	movq	%rcx, -608(%rbp)
	jne	.L3254
	movq	-608(%rbp), %rcx
	cmpq	%rcx, -600(%rbp)
	je	.L3161
	movq	%r10, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal6Object8ToObjectEPNS0_7IsolateENS0_6HandleIS1_EEPKc
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L3231
.L3161:
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_117VectorBackedMatchE(%rip), %rax
	movq	-560(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, -160(%rbp)
	movq	-576(%rbp), %rax
	movq	%r10, -536(%rbp)
	movq	%rax, -136(%rbp)
	movl	-584(%rbp), %eax
	movq	%r15, -152(%rbp)
	movl	%eax, -128(%rbp)
	leaq	-352(%rbp), %rax
	movq	$0, -144(%rbp)
	movq	%rax, -120(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	movq	-536(%rbp), %r10
	movq	%rax, -144(%rbp)
	movq	88(%r15), %rax
	movq	(%r10), %rdx
	cmpq	%rax, %rdx
	setne	-112(%rbp)
	je	.L3164
	movq	%r10, -104(%rbp)
.L3164:
	movq	-488(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3231
.L3160:
	movl	-544(%rbp), %ecx
	cmpl	%ecx, -584(%rbp)
	jnb	.L3255
.L3166:
	leaq	-448(%rbp), %rdi
	addq	$8, %r14
	call	_ZN2v88internal11HandleScopeD1Ev
	cmpq	%r14, -568(%rbp)
	jne	.L3167
	movq	-512(%rbp), %r14
	movq	-560(%rbp), %rbx
.L3098:
	movl	-544(%rbp), %ecx
	cmpl	%ecx, -496(%rbp)
	ja	.L3256
.L3169:
	movq	-552(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	je	.L3233
	movq	(%rax), %r12
	jmp	.L3097
	.p2align 4,,10
	.p2align 3
.L3119:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L3120
	.p2align 4,,10
	.p2align 3
.L3231:
	movq	-512(%rbp), %r14
	movq	312(%r15), %r12
.L3106:
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3097
	movq	-440(%rbp), %rdx
	subl	$1, 41104(%rdi)
	movq	-432(%rbp), %rax
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L3097
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L3097
.L3251:
	movq	%r15, %rdi
	movq	%rsi, -488(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-488(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L3054
.L3135:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L3136
	jmp	.L3231
.L3104:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L3105
	jmp	.L3231
.L3132:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3134
	movq	%rcx, %rax
	jmp	.L3133
.L3112:
	movsd	7(%r12), %xmm0
	xorl	%r12d, %r12d
	comisd	.LC21(%rip), %xmm0
	jb	.L3113
	movsd	.LC22(%rip), %xmm2
	movl	$-1, %r12d
	comisd	%xmm0, %xmm2
	jbe	.L3113
	cvttsd2siq	%xmm0, %r12
	jmp	.L3113
.L3099:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3101
	movq	%rbx, %rdx
	jmp	.L3100
.L3107:
	movq	%r15, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L3109
.L3108:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3257
.L3110:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%r12, (%rax)
	jmp	.L3111
.L3116:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3118
	movq	%rbx, %rax
	jmp	.L3117
.L3252:
	movq	-528(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rdx, -536(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-536(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L3103
.L3140:
	leaq	-456(%rbp), %rdx
	leaq	-352(%rbp), %rdi
	movq	%r8, %rsi
	addl	$1, %ebx
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	cmpl	%ebx, %r12d
	jne	.L3142
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L3253:
	movq	%r15, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L3231
	movq	(%rax), %rax
	testb	$1, %al
	je	.L3124
	movsd	7(%rax), %xmm0
	comisd	.LC21(%rip), %xmm0
	jb	.L3226
	movsd	.LC22(%rip), %xmm1
	orl	$-1, %eax
	comisd	%xmm0, %xmm1
	jbe	.L3128
	cvttsd2siq	%xmm0, %rax
	jmp	.L3128
.L3257:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L3110
.L3226:
	movl	$0, -584(%rbp)
	jmp	.L3127
.L3245:
	call	__stack_chk_fail@PLT
.L3254:
	cmpl	$65534, %r12d
	ja	.L3146
	leal	2(%r12), %ebx
	movl	%ebx, -536(%rbp)
	cmpq	%rcx, %rax
	je	.L3148
	leal	3(%r12), %eax
	movl	%eax, -536(%rbp)
.L3148:
	cmpl	$65534, -536(%rbp)
	ja	.L3146
	movl	-536(%rbp), %edi
	movq	%r10, -576(%rbp)
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	movq	-576(%rbp), %r10
	movq	%rax, %rbx
	xorl	%eax, %eax
	jmp	.L3152
.L3258:
	movq	-344(%rbp), %rdx
	movq	(%rdx,%rax,8), %rdx
	movq	%rdx, (%rbx,%rax,8)
	addq	$1, %rax
.L3152:
	cmpl	%eax, %r12d
	ja	.L3258
	movl	%r12d, %eax
	movq	-584(%rbp), %rsi
	movq	41112(%r15), %rdi
	leaq	(%rbx,%rax,8), %rax
	movq	%rax, -576(%rbp)
	leal	1(%r12), %eax
	salq	$32, %rsi
	movl	%eax, -592(%rbp)
	testq	%rdi, %rdi
	je	.L3153
	movq	%r10, -616(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-616(%rbp), %r10
.L3154:
	movq	-576(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-560(%rbp), %rcx
	movslq	-592(%rbp), %rax
	movq	%rcx, (%rbx,%rax,8)
	movq	-608(%rbp), %rcx
	cmpq	%rcx, -600(%rbp)
	je	.L3156
	leal	2(%r12), %eax
	cltq
	movq	%r10, (%rbx,%rax,8)
.L3156:
	movq	-480(%rbp), %rsi
	movl	-536(%rbp), %ecx
	movq	%rbx, %r8
	movq	%r15, %rdi
	movq	-504(%rbp), %rdx
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3229
	movq	%r15, %rdi
	call	_ZN2v88internal6Object8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3229
	movq	%rbx, %rdi
	call	_ZN2v88internal11DeleteArrayINS0_6HandleINS0_6ObjectEEEEEvPT_
	jmp	.L3160
.L3146:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$305, %esi
	movq	%r15, %rdi
	movq	-512(%rbp), %r14
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r12
	jmp	.L3106
.L3256:
	movl	-496(%rbp), %ecx
	movl	-544(%rbp), %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory12NewSubStringENS0_6HandleINS0_6StringEEEii
	movq	-552(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L3169
.L3255:
	movl	-584(%rbp), %ebx
	movl	-544(%rbp), %edx
	movq	%r15, %rdi
	movq	-560(%rbp), %rsi
	movl	%ebx, %ecx
	call	_ZN2v88internal7Factory12NewSubStringENS0_6HandleINS0_6StringEEEii
	movq	-552(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	-552(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movl	-588(%rbp), %eax
	addl	%ebx, %eax
	movl	%eax, -544(%rbp)
	jmp	.L3166
.L3153:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3259
.L3155:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L3154
.L3229:
	movq	%rbx, %rdi
	movq	-512(%rbp), %r14
	movq	312(%r15), %r12
	call	_ZN2v88internal11DeleteArrayINS0_6HandleINS0_6ObjectEEEEEvPT_
	jmp	.L3106
.L3259:
	movq	%r15, %rdi
	movq	%rsi, -624(%rbp)
	movq	%r10, -616(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-624(%rbp), %rsi
	movq	-616(%rbp), %r10
	jmp	.L3155
	.cfi_endproc
.LFE25935:
	.size	_ZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal23Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE:
.LFB20268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3438
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %r15
	movq	%rax, -376(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3263
.L3264:
	leaq	.LC20(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3263:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3264
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rbx
	testb	$1, %al
	jne	.L3439
.L3265:
	leaq	.LC11(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3439:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L3265
	leaq	-16(%rsi), %rcx
	movq	%rax, %rsi
	movq	%rcx, -384(%rbp)
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L3268
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L3440
.L3268:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L3276
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L3441
.L3276:
	movq	-16(%r13), %rax
	testb	$1, %al
	jne	.L3442
.L3283:
	leaq	-16(%r13), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3437
.L3289:
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r12, -392(%rbp)
	call	_ZN2v88internal11RegExpUtils18IsUnmodifiedRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movb	%al, -393(%rbp)
	testb	%al, %al
	je	.L3287
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12_GLOBAL__N_113RegExpReplaceEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEES8_
	testq	%rax, %rax
	je	.L3437
	movq	(%rax), %r12
.L3290:
	movq	-376(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	cmpq	41096(%r14), %r15
	je	.L3260
	movq	%r15, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3260:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3443
	addq	$504, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3447:
	.cfi_restore_state
	movb	$1, -393(%rbp)
	.p2align 4,,10
	.p2align 3
.L3287:
	movq	(%rbx), %rax
	leaq	2632(%r14), %rsi
	movl	$3, %edx
	movl	11(%rax), %eax
	movl	%eax, -400(%rbp)
	movq	2632(%r14), %rax
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L3293
	movl	11(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$3, %edx
.L3293:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	2632(%r14), %rax
	movq	%r14, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	jne	.L3294
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
.L3294:
	leaq	-144(%rbp), %r12
	movq	%rsi, -112(%rbp)
	movq	%r12, %rdi
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L3295
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L3296:
	movq	(%rax), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movb	%al, -416(%rbp)
	testb	%al, %al
	je	.L3396
	movq	3528(%r14), %rax
	leaq	3528(%r14), %rsi
	movl	$3, %edx
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L3298
	movl	11(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$3, %edx
.L3298:
	movabsq	$824633720832, %rax
	movl	%edx, -144(%rbp)
	movq	%rax, -132(%rbp)
	movq	3528(%r14), %rax
	movq	%r14, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L3444
.L3299:
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	je	.L3445
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L3301
	.p2align 4,,10
	.p2align 3
.L3437:
	movq	312(%r14), %r12
	jmp	.L3290
	.p2align 4,,10
	.p2align 3
.L3442:
	movq	-1(%rax), %rdx
	testb	$2, 13(%rdx)
	je	.L3446
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$0, -392(%rbp)
	call	_ZN2v88internal11RegExpUtils18IsUnmodifiedRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testb	%al, %al
	je	.L3447
	leaq	.LC40(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3446:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L3283
	leaq	-16(%r13), %r12
	jmp	.L3289
	.p2align 4,,10
	.p2align 3
.L3441:
	movq	(%rbx), %rax
	movq	41112(%r14), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3279
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
	jmp	.L3276
	.p2align 4,,10
	.p2align 3
.L3295:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L3296
	jmp	.L3437
	.p2align 4,,10
	.p2align 3
.L3440:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L3271
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L3271
	movq	%rbx, %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	jmp	.L3276
	.p2align 4,,10
	.p2align 3
.L3271:
	movq	41112(%r14), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3273
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
	jmp	.L3268
	.p2align 4,,10
	.p2align 3
.L3438:
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r12
	jmp	.L3260
	.p2align 4,,10
	.p2align 3
.L3396:
	movb	$0, -440(%rbp)
.L3297:
	leaq	-208(%rbp), %rax
	movq	41136(%r14), %rsi
	leaq	.LC41(%rip), %rdx
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movq	-424(%rbp), %rax
	movq	$0, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	%rax, -320(%rbp)
	leaq	88(%r14), %rax
	movq	%rax, -408(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, -448(%rbp)
	leaq	-320(%rbp), %rax
	movq	$0, -296(%rbp)
	movq	%rax, -456(%rbp)
.L3317:
	movq	-408(%rbp), %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	$0, -256(%rbp)
	call	_ZN2v88internal11RegExpUtils10RegExpExecEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L3436
	movq	%rax, -256(%rbp)
	movq	104(%r14), %rcx
	cmpq	%rcx, (%rax)
	je	.L3304
	movq	-304(%rbp), %rsi
	cmpq	-296(%rbp), %rsi
	je	.L3305
	movq	%rax, (%rsi)
	addq	$8, -304(%rbp)
.L3306:
	cmpb	$0, -416(%rbp)
	je	.L3304
	movq	-256(%rbp), %rcx
	movq	(%rcx), %rax
	testb	$1, %al
	jne	.L3307
.L3309:
	movq	%rcx, %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rcx, -432(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-432(%rbp), %rcx
.L3308:
	movabsq	$824633720832, %rdi
	movq	%rax, -80(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rdi, -132(%rbp)
	movq	%r12, %rdi
	movl	$3, -144(%rbp)
	movq	%r14, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L3310
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rsi
.L3311:
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3312
.L3315:
	movq	%r14, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3436
.L3314:
	movq	(%rsi), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jne	.L3317
	movzbl	-440(%rbp), %ecx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11RegExpUtils22SetAdvancedStringIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEEb@PLT
	testq	%rax, %rax
	jne	.L3317
	.p2align 4,,10
	.p2align 3
.L3436:
	movq	312(%r14), %r12
.L3318:
	movq	-424(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	jmp	.L3290
	.p2align 4,,10
	.p2align 3
.L3310:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L3311
	jmp	.L3436
	.p2align 4,,10
	.p2align 3
.L3279:
	movq	41088(%r14), %rbx
	cmpq	41096(%r14), %rbx
	je	.L3448
.L3281:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rbx)
	jmp	.L3276
	.p2align 4,,10
	.p2align 3
.L3307:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3309
	movq	%rcx, %rax
	jmp	.L3308
	.p2align 4,,10
	.p2align 3
.L3312:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L3315
	jmp	.L3314
	.p2align 4,,10
	.p2align 3
.L3445:
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L3301:
	movq	(%rax), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movb	%al, -440(%rbp)
	call	_ZN2v88internal11RegExpUtils12SetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEm@PLT
	testq	%rax, %rax
	jne	.L3297
	jmp	.L3437
	.p2align 4,,10
	.p2align 3
.L3273:
	movq	41088(%r14), %rbx
	cmpq	41096(%r14), %rbx
	je	.L3449
.L3275:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rbx)
	jmp	.L3268
	.p2align 4,,10
	.p2align 3
.L3305:
	movq	-448(%rbp), %rdx
	movq	-456(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L3306
	.p2align 4,,10
	.p2align 3
.L3444:
	movq	%r14, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L3299
	.p2align 4,,10
	.p2align 3
.L3448:
	movq	%r14, %rdi
	movq	%rsi, -392(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-392(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L3281
.L3304:
	leaq	-256(%rbp), %rax
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -456(%rbp)
	call	_ZN2v88internal24IncrementalStringBuilderC1EPNS0_7IsolateE@PLT
	movq	-304(%rbp), %rcx
	movq	-312(%rbp), %rax
	movl	$0, -448(%rbp)
	movq	%rcx, -480(%rbp)
	cmpq	%rax, %rcx
	je	.L3319
	leaq	2768(%r14), %rcx
	movq	%rbx, -464(%rbp)
	movq	%rcx, -432(%rbp)
	movq	%r15, -416(%rbp)
	movq	%r14, %r15
	movq	%rax, %r14
.L3388:
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	%r15, -352(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rax, -344(%rbp)
	movq	41096(%r15), %rax
	movq	%rax, -336(%rbp)
	movq	(%r14), %rbx
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L3320
.L3322:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%rcx, -440(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-440(%rbp), %rcx
	movq	%rax, %r13
.L3321:
	movq	2768(%r15), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L3323
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L3323:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	2768(%r15), %rax
	movq	%r15, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L3450
.L3324:
	movq	%r12, %rdi
	movq	%rcx, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L3325
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rsi
.L3326:
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L3328
	sarq	$32, %rax
	movl	$0, %edx
	movq	41112(%r15), %rdi
	cmovs	%rdx, %rax
	salq	$32, %rax
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L3329
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3330:
	testq	%rax, %rax
	je	.L3434
.L3332:
	movq	(%rax), %r13
	testb	$1, %r13b
	jne	.L3333
	sarq	$32, %r13
	movl	$0, %eax
	testq	%r13, %r13
	cmovle	%eax, %r13d
.L3334:
	movq	(%r14), %rbx
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L3337
.L3339:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
.L3338:
	movq	%rax, -80(%rbp)
	movq	%r12, %rdi
	movabsq	$824633720832, %rcx
	movabsq	$-4294967296, %rax
	movl	$3, -144(%rbp)
	movq	%rcx, -132(%rbp)
	movq	%r15, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L3340
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rsi
.L3341:
	movq	%r15, %rdi
	call	_ZN2v88internal6Object8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	%rax, -472(%rbp)
	testq	%rax, %rax
	je	.L3434
	movq	(%rax), %rax
	movq	(%r14), %rsi
	leaq	2688(%r15), %rdx
	movq	%r15, %rdi
	movl	11(%rax), %eax
	movl	%eax, -492(%rbp)
	call	_ZN2v88internal6Object11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3434
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L3451
.L3345:
	movl	$0, -488(%rbp)
	sarq	$32, %rax
	testq	%rax, %rax
	jle	.L3348
.L3349:
	movl	-400(%rbp), %ebx
	cmpl	%eax, %ebx
	cmovbe	%ebx, %eax
	movl	%eax, -488(%rbp)
.L3348:
	movq	-424(%rbp), %rax
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rax, -288(%rbp)
	movq	$0, -264(%rbp)
	testl	%r13d, %r13d
	je	.L3352
	xorl	%ebx, %ebx
.L3363:
	movq	$0, -360(%rbp)
	movq	(%r14), %rcx
	movq	(%rcx), %rax
	testb	$1, %al
	jne	.L3353
.L3355:
	movq	%rcx, %rsi
	movl	%ebx, %edx
	movq	%r15, %rdi
	movq	%rcx, -440(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-440(%rbp), %rcx
.L3354:
	movabsq	$824633720832, %rdi
	movq	%r15, -120(%rbp)
	movq	%rdi, -132(%rbp)
	movq	%r12, %rdi
	movl	$3, -144(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -80(%rbp)
	movl	%ebx, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L3356
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rsi
.L3357:
	movq	%rsi, -360(%rbp)
	movq	88(%r15), %rax
	cmpq	%rax, (%rsi)
	je	.L3359
	movq	%r15, %rdi
	call	_ZN2v88internal6Object8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3434
	movq	%rax, -360(%rbp)
.L3359:
	movq	-272(%rbp), %r8
	cmpq	-264(%rbp), %r8
	je	.L3361
	addl	$1, %ebx
	movq	%rsi, (%r8)
	addq	$8, -272(%rbp)
	cmpl	%ebx, %r13d
	jne	.L3363
.L3352:
	movq	(%r14), %rsi
	leaq	2648(%r15), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEE
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L3434
	movq	(%rax), %rax
	movq	88(%r15), %rbx
	cmpb	$0, -393(%rbp)
	movq	%rax, -504(%rbp)
	movq	%rbx, -512(%rbp)
	jne	.L3452
	movq	-512(%rbp), %rbx
	cmpq	%rbx, -504(%rbp)
	je	.L3382
	movq	%r10, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal6Object8ToObjectEPNS0_7IsolateENS0_6HandleIS1_EEPKc
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L3434
.L3382:
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_117VectorBackedMatchE(%rip), %rax
	movq	-464(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rax, -144(%rbp)
	movq	-472(%rbp), %rax
	movq	%r10, -440(%rbp)
	movq	%rax, -120(%rbp)
	movl	-488(%rbp), %eax
	movq	%r15, -136(%rbp)
	movl	%eax, -112(%rbp)
	leaq	-288(%rbp), %rax
	movq	$0, -128(%rbp)
	movq	%rax, -104(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	movq	-440(%rbp), %r10
	movq	%rax, -128(%rbp)
	movq	88(%r15), %rax
	movq	(%r10), %rdx
	cmpq	%rax, %rdx
	setne	-96(%rbp)
	je	.L3385
	movq	%r10, -88(%rbp)
.L3385:
	movq	-392(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3434
.L3381:
	movl	-448(%rbp), %ebx
	cmpl	%ebx, -488(%rbp)
	jnb	.L3453
.L3387:
	leaq	-352(%rbp), %rdi
	addq	$8, %r14
	call	_ZN2v88internal11HandleScopeD1Ev
	cmpq	%r14, -480(%rbp)
	jne	.L3388
	movq	%r15, %r14
	movq	-464(%rbp), %rbx
	movq	-416(%rbp), %r15
.L3319:
	movl	-448(%rbp), %ecx
	cmpl	%ecx, -400(%rbp)
	ja	.L3454
.L3390:
	movq	-456(%rbp), %rdi
	call	_ZN2v88internal24IncrementalStringBuilder6FinishEv@PLT
	testq	%rax, %rax
	je	.L3436
	movq	(%rax), %r12
	jmp	.L3318
	.p2align 4,,10
	.p2align 3
.L3340:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L3341
.L3434:
	movq	%r15, %r14
	movq	-416(%rbp), %r15
	movq	312(%r14), %r12
.L3327:
	movq	-352(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3318
	movq	-344(%rbp), %rdx
	subl	$1, 41104(%rdi)
	movq	-336(%rbp), %rax
	movq	%rdx, 41088(%rdi)
	cmpq	41096(%rdi), %rax
	je	.L3318
	movq	%rax, 41096(%rdi)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L3318
.L3449:
	movq	%r14, %rdi
	movq	%rsi, -392(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-392(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L3275
.L3356:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L3357
	jmp	.L3434
.L3325:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L3326
	jmp	.L3434
.L3353:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3355
	movq	%rcx, %rax
	jmp	.L3354
.L3333:
	movsd	7(%r13), %xmm0
	xorl	%r13d, %r13d
	comisd	.LC21(%rip), %xmm0
	jb	.L3334
	movsd	.LC22(%rip), %xmm2
	movl	$-1, %r13d
	comisd	%xmm0, %xmm2
	jbe	.L3334
	cvttsd2siq	%xmm0, %r13
	jmp	.L3334
.L3320:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3322
	movq	%rbx, %r13
	jmp	.L3321
.L3328:
	movq	%r15, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L3330
.L3329:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3455
.L3331:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%r13, (%rax)
	jmp	.L3332
.L3337:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3339
	movq	%rbx, %rax
	jmp	.L3338
.L3450:
	movq	-432(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rcx
	jmp	.L3324
.L3361:
	leaq	-360(%rbp), %rdx
	leaq	-288(%rbp), %rdi
	movq	%r8, %rsi
	addl	$1, %ebx
	call	_ZNSt6vectorIN2v88internal6HandleINS1_6ObjectEEENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	cmpl	%r13d, %ebx
	jne	.L3363
	jmp	.L3352
	.p2align 4,,10
	.p2align 3
.L3451:
	movq	%r15, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L3434
	movq	(%rax), %rax
	testb	$1, %al
	je	.L3345
	movsd	7(%rax), %xmm0
	comisd	.LC21(%rip), %xmm0
	jb	.L3429
	movsd	.LC22(%rip), %xmm1
	orl	$-1, %eax
	comisd	%xmm0, %xmm1
	jbe	.L3349
	cvttsd2siq	%xmm0, %rax
	jmp	.L3349
.L3455:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L3331
.L3429:
	movl	$0, -488(%rbp)
	jmp	.L3348
.L3443:
	call	__stack_chk_fail@PLT
.L3454:
	movl	-400(%rbp), %ecx
	movl	-448(%rbp), %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory12NewSubStringENS0_6HandleINS0_6StringEEEii
	movq	-456(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L3390
.L3453:
	movl	-488(%rbp), %ebx
	movl	-448(%rbp), %edx
	movq	%r15, %rdi
	movq	-464(%rbp), %rsi
	movl	%ebx, %ecx
	call	_ZN2v88internal7Factory12NewSubStringENS0_6HandleINS0_6StringEEEii
	movq	-456(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movq	-456(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal24IncrementalStringBuilder12AppendStringENS0_6HandleINS0_6StringEEE@PLT
	movl	-492(%rbp), %eax
	addl	%ebx, %eax
	movl	%eax, -448(%rbp)
	jmp	.L3387
.L3452:
	cmpl	$65534, %r13d
	ja	.L3367
	leal	2(%r13), %ecx
	movl	%ecx, -440(%rbp)
	cmpq	%rbx, %rax
	je	.L3369
	leal	3(%r13), %eax
	movl	%eax, -440(%rbp)
.L3369:
	cmpl	$65534, -440(%rbp)
	ja	.L3367
	movl	-440(%rbp), %edi
	movq	%r10, -472(%rbp)
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m
	movq	-472(%rbp), %r10
	movq	%rax, %rbx
	xorl	%eax, %eax
	jmp	.L3373
.L3456:
	movq	-280(%rbp), %rdx
	movq	(%rdx,%rax,8), %rdx
	movq	%rdx, (%rbx,%rax,8)
	addq	$1, %rax
.L3373:
	cmpl	%eax, %r13d
	ja	.L3456
	movl	%r13d, %eax
	movq	-488(%rbp), %rsi
	movq	41112(%r15), %rdi
	leaq	(%rbx,%rax,8), %rax
	movq	%rax, -520(%rbp)
	leal	1(%r13), %eax
	salq	$32, %rsi
	movl	%eax, -472(%rbp)
	testq	%rdi, %rdi
	je	.L3374
	movq	%r10, -528(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-528(%rbp), %r10
.L3375:
	movq	-520(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-464(%rbp), %rcx
	movslq	-472(%rbp), %rax
	movq	%rcx, (%rbx,%rax,8)
	movq	-512(%rbp), %rcx
	cmpq	%rcx, -504(%rbp)
	je	.L3377
	leal	2(%r13), %eax
	cltq
	movq	%r10, (%rbx,%rax,8)
.L3377:
	movq	-384(%rbp), %rsi
	movl	-440(%rbp), %ecx
	movq	%rbx, %r8
	movq	%r15, %rdi
	movq	-408(%rbp), %rdx
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3432
	movq	%r15, %rdi
	call	_ZN2v88internal6Object8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3432
	movq	%rbx, %rdi
	call	_ZN2v88internal11DeleteArrayINS0_6HandleINS0_6ObjectEEEEEvPT_
	jmp	.L3381
.L3367:
	movq	%r15, %r14
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$305, %esi
	movq	%r14, %rdi
	movq	-416(%rbp), %r15
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r12
	jmp	.L3327
.L3374:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3457
.L3376:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L3375
.L3432:
	movq	%r15, %r14
	movq	%rbx, %rdi
	movq	-416(%rbp), %r15
	movq	312(%r14), %r12
	call	_ZN2v88internal11DeleteArrayINS0_6HandleINS0_6ObjectEEEEEvPT_
	jmp	.L3327
.L3457:
	movq	%r15, %rdi
	movq	%rsi, -536(%rbp)
	movq	%r10, -528(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-536(%rbp), %rsi
	movq	-528(%rbp), %r10
	jmp	.L3376
	.cfi_endproc
.LFE20268:
	.size	_ZN2v88internal23Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_RegExpReplaceRTEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv:
.LFB25458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	movq	%rsi, %rcx
	movslq	%esi, %r11
	movl	%esi, %r10d
	movq	%rsi, -64(%rbp)
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	movl	%esi, %r8d
	subl	%r13d, %r10d
	leaq	0(,%r13,4), %rax
	movq	%rdx, %rbx
	leaq	0(,%r11,4), %rsi
	subq	%rax, %rbx
	subq	%rax, %rdi
	movl	%ecx, %eax
	leaq	(%rbx,%rsi), %r14
	addl	$1, %eax
	addq	%rdi, %rsi
	cmpl	%ecx, %r13d
	jge	.L3459
	movl	%ecx, -52(%rbp)
	subl	$1, %ecx
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L3483
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L3461:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L3461
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L3462
.L3460:
	movslq	%edx, %rcx
	movq	-64(%rbp), %r15
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L3462
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L3462
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L3462:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzbl	-1(%r9,%r11), %r15d
	movl	-64(%rbp), %esi
	movl	%r15d, %r11d
	.p2align 4,,10
	.p2align 3
.L3482:
	cmpl	%eax, %r8d
	jl	.L3464
	.p2align 4,,10
	.p2align 3
.L3468:
	movslq	%eax, %rdx
	cmpb	%r11b, -1(%r9,%rdx)
	je	.L3464
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L3489
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L3468
.L3464:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L3490
.L3469:
	cmpl	%esi, %r12d
	jge	.L3463
.L3492:
	movslq	%esi, %rax
	movzbl	-1(%r9,%rax), %r11d
	movl	%edx, %eax
	jmp	.L3482
	.p2align 4,,10
	.p2align 3
.L3489:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L3468
	jmp	.L3464
	.p2align 4,,10
	.p2align 3
.L3490:
	cmpl	%esi, %r12d
	jl	.L3475
	jmp	.L3470
	.p2align 4,,10
	.p2align 3
.L3491:
	cmpl	%r10d, (%r14)
	jne	.L3472
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L3472:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L3470
.L3475:
	movl	%ecx, %esi
	cmpb	%r15b, -1(%r9,%rcx)
	jne	.L3491
	cmpl	%r12d, %ecx
	jle	.L3458
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L3492
	.p2align 4,,10
	.p2align 3
.L3463:
	cmpl	-64(%rbp), %edx
	jge	.L3458
	.p2align 4,,10
	.p2align 3
.L3477:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L3479
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L3479:
	cmpl	%r13d, %edx
	je	.L3493
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L3477
.L3458:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3493:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L3477
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3470:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L3469
.L3459:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3483:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L3460
	.cfi_endproc
.LFE25458:
	.size	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi:
.LFB25149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %r13
	movq	%rdx, -72(%rbp)
	movq	(%rdi), %r10
	leal	-1(%rax), %edx
	movl	%eax, %r8d
	movl	%eax, -60(%rbp)
	leal	-2(%rax), %r15d
	movslq	%edx, %r9
	leaq	42372(%r10), %rdi
	movl	%edx, %ebx
	negl	%r8d
	movzbl	0(%r13,%r9), %r11d
	subl	42372(%r10,%r11,4), %ebx
	movl	%r14d, %r10d
	movq	%r11, %r9
	movl	$1, %r11d
	movl	%ebx, -56(%rbp)
	subl	%eax, %r10d
	jmp	.L3507
	.p2align 4,,10
	.p2align 3
.L3509:
	movl	%edx, %ebx
	subl	(%rdi,%rax,4), %ebx
	movl	%ebx, %eax
	addl	%ebx, %ecx
	movl	%r11d, %ebx
	subl	%eax, %ebx
	addl	%ebx, %r8d
.L3507:
	cmpl	%r10d, %ecx
	jg	.L3508
	leal	(%rdx,%rcx), %eax
	cltq
	movzbl	(%rsi,%rax), %eax
	cmpb	%r9b, %al
	jne	.L3509
	testl	%r15d, %r15d
	js	.L3504
	movslq	%ecx, %rbx
	movl	%edx, -64(%rbp)
	movslq	%r15d, %rax
	addq	%rsi, %rbx
	jmp	.L3499
	.p2align 4,,10
	.p2align 3
.L3510:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L3504
.L3499:
	movzbl	(%rbx,%rax), %edx
	movl	%eax, %r14d
	cmpb	%dl, 0(%r13,%rax)
	je	.L3510
	movl	-60(%rbp), %eax
	movl	-56(%rbp), %ebx
	movl	-64(%rbp), %edx
	subl	%r14d, %eax
	addl	%ebx, %ecx
	subl	%ebx, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L3507
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal12StringSearchIhhE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	leaq	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	-72(%rbp), %rdx
	movq	%rax, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhhE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.p2align 4,,10
	.p2align 3
.L3508:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3504:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25149:
	.size	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi:
.LFB24714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rsi
	movq	8(%rdi), %r13
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%esi, %eax
	subl	%esi, %ebx
	movq	%rsi, -80(%rbp)
	movl	%esi, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %ecx
	jg	.L3516
	movl	$-9, %edx
	movl	%ecx, %r9d
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L3514
	movzbl	0(%r13), %r15d
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	movl	%r15d, %r14d
	jmp	.L3518
	.p2align 4,,10
	.p2align 3
.L3542:
	subq	%r12, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r12, %rdx
	cmpb	%r14b, (%rdx)
	je	.L3517
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L3516
.L3518:
	movl	-52(%rbp), %edx
	movslq	%r9d, %rdi
	movl	%r15d, %esi
	addq	%r12, %rdi
	subl	%r9d, %edx
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L3542
.L3516:
	movl	$-1, %r9d
.L3511:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3517:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L3516
	movl	-56(%rbp), %esi
	movl	$1, %eax
	jmp	.L3521
	.p2align 4,,10
	.p2align 3
.L3543:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %esi
	jle	.L3520
.L3521:
	movzbl	(%rdx,%rax), %edi
	movl	%eax, %ecx
	cmpb	%dil, 0(%r13,%rax)
	je	.L3543
.L3520:
	cmpl	-56(%rbp), %ecx
	je	.L3511
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L3516
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L3518
.L3514:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	movl	32(%rax), %r10d
	leaq	42372(%rdx), %rsi
	testl	%r10d, %r10d
	je	.L3544
	leal	-1(%r10), %eax
	addq	$43396, %rdx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L3526:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L3526
.L3525:
	movl	-80(%rbp), %eax
	subl	$1, %eax
	cmpl	%r10d, %eax
	jle	.L3527
	movl	-80(%rbp), %edx
	movslq	%r10d, %rcx
	movq	-72(%rbp), %r8
	leaq	1(%rcx), %rax
	subl	$2, %edx
	subl	%r10d, %edx
	addq	%rax, %rdx
	jmp	.L3528
	.p2align 4,,10
	.p2align 3
.L3545:
	addq	$1, %rax
.L3528:
	movq	8(%r8), %rdi
	movzbl	(%rdi,%rcx), %edi
	movl	%ecx, (%rsi,%rdi,4)
	movq	%rax, %rcx
	cmpq	%rax, %rdx
	jne	.L3545
.L3527:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhhE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
.L3544:
	.cfi_restore_state
	leaq	42380(%rdx), %rdi
	movq	%rsi, %rcx
	movq	$-1, %rax
	movq	$-1, 42372(%rdx)
	movq	$-1, 43388(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L3525
	.cfi_endproc
.LFE24714:
	.size	_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIhhE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv:
.LFB25463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	leaq	0(,%r13,4), %rax
	movslq	%r15d, %r11
	movl	%r15d, %r10d
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	leaq	0(,%r11,4), %rsi
	subl	%r13d, %r10d
	movq	%rdx, %rbx
	subq	%rax, %rdi
	subq	%rax, %rbx
	leal	1(%r15), %eax
	leaq	(%rbx,%rsi), %r14
	addq	%rdi, %rsi
	cmpl	%r15d, %r13d
	jge	.L3547
	leal	-1(%r15), %ecx
	movl	%r15d, -52(%rbp)
	movl	%r15d, %r8d
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L3571
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L3549:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L3549
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L3550
.L3548:
	movslq	%edx, %rcx
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L3550
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L3550
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L3550:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzwl	-2(%r9,%r11,2), %ecx
	movl	%r15d, %esi
	movq	%r13, -64(%rbp)
	movl	%ecx, %r11d
	movl	%ecx, %r13d
	.p2align 4,,10
	.p2align 3
.L3576:
	cmpl	%eax, %r8d
	jl	.L3552
.L3556:
	movslq	%eax, %rdx
	cmpw	%r11w, -2(%r9,%rdx,2)
	je	.L3552
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L3578
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L3556
.L3552:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L3579
.L3557:
	cmpl	%esi, %r12d
	jge	.L3551
.L3581:
	movslq	%esi, %rax
	movzwl	-2(%r9,%rax,2), %r11d
	movl	%edx, %eax
	jmp	.L3576
	.p2align 4,,10
	.p2align 3
.L3578:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	jmp	.L3576
	.p2align 4,,10
	.p2align 3
.L3579:
	cmpl	%esi, %r12d
	jl	.L3563
	jmp	.L3558
	.p2align 4,,10
	.p2align 3
.L3580:
	cmpl	%r10d, (%r14)
	jne	.L3560
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L3560:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L3558
.L3563:
	movl	%ecx, %esi
	cmpw	%r13w, -2(%r9,%rcx,2)
	jne	.L3580
	cmpl	%r12d, %ecx
	jle	.L3546
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L3581
	.p2align 4,,10
	.p2align 3
.L3551:
	movq	-64(%rbp), %r13
	cmpl	%r15d, %edx
	jge	.L3546
	.p2align 4,,10
	.p2align 3
.L3565:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L3567
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L3567:
	cmpl	%r13d, %edx
	je	.L3582
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L3565
.L3546:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3582:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L3565
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3558:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L3557
.L3547:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3571:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L3548
	.cfi_endproc
.LFE25463:
	.size	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi:
.LFB25154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %r14
	movq	%rdx, -72(%rbp)
	movq	(%rdi), %r10
	leal	-1(%rax), %edx
	movl	%eax, %r8d
	movl	%eax, -64(%rbp)
	movslq	%edx, %r9
	movl	%edx, %ebx
	leaq	42372(%r10), %rdi
	negl	%r8d
	movzwl	(%r14,%r9,2), %r9d
	movzbl	%r9b, %r11d
	subl	42372(%r10,%r11,4), %ebx
	movl	%r15d, %r10d
	movl	$1, %r11d
	movl	%ebx, -60(%rbp)
	leal	-2(%rax), %ebx
	subl	%eax, %r10d
	movl	%ebx, -56(%rbp)
	jmp	.L3596
	.p2align 4,,10
	.p2align 3
.L3598:
	movl	%edx, %ebx
	subl	(%rdi,%rax,4), %ebx
	movl	%ebx, %eax
	addl	%ebx, %ecx
	movl	%r11d, %ebx
	subl	%eax, %ebx
	addl	%ebx, %r8d
.L3596:
	cmpl	%r10d, %ecx
	jg	.L3597
	leal	(%rdx,%rcx), %eax
	cltq
	movzbl	(%rsi,%rax), %ebx
	movq	%rbx, %rax
	cmpw	%bx, %r9w
	jne	.L3598
	movslq	-56(%rbp), %rax
	testl	%eax, %eax
	js	.L3593
	movslq	%ecx, %r13
	addq	%rsi, %r13
	jmp	.L3588
	.p2align 4,,10
	.p2align 3
.L3599:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L3593
.L3588:
	movzbl	0(%r13,%rax), %ebx
	movl	%eax, %r15d
	cmpw	%bx, (%r14,%rax,2)
	je	.L3599
	movl	-64(%rbp), %eax
	movl	-60(%rbp), %ebx
	subl	%r15d, %eax
	addl	%ebx, %ecx
	subl	%ebx, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L3596
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal12StringSearchIthE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	leaq	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi(%rip), %rax
	movq	-72(%rbp), %rdx
	movq	%rax, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIthE16BoyerMooreSearchEPS2_NS0_6VectorIKhEEi
	.p2align 4,,10
	.p2align 3
.L3597:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3593:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25154:
	.size	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi,"axG",@progbits,_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.type	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi, @function
_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi:
.LFB24718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rcx
	movq	8(%rdi), %r14
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%ecx, %eax
	subl	%ecx, %ebx
	movq	%rcx, -80(%rbp)
	movl	%ecx, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %r9d
	jg	.L3605
	movl	$-9, %edx
	movq	%rsi, %r13
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L3603
	movzwl	(%r14), %eax
	leal	1(%rbx), %ecx
	movl	%ecx, -52(%rbp)
	movzbl	%ah, %edx
	movl	%eax, %r15d
	cmpb	%dl, %al
	cmovbe	%edx, %eax
	movzbl	%al, %r12d
	jmp	.L3607
	.p2align 4,,10
	.p2align 3
.L3632:
	subq	%r13, %rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	addq	%r13, %rdx
	cmpb	%r15b, (%rdx)
	je	.L3606
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L3605
.L3607:
	movl	-52(%rbp), %edx
	movslq	%r9d, %rdi
	movl	%r12d, %esi
	addq	%r13, %rdi
	subl	%r9d, %edx
	movslq	%edx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L3632
.L3605:
	movl	$-1, %r9d
.L3600:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3606:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L3605
	movl	-56(%rbp), %edi
	movl	$1, %eax
	jmp	.L3610
	.p2align 4,,10
	.p2align 3
.L3633:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %edi
	jle	.L3609
.L3610:
	movzbl	(%rdx,%rax), %esi
	movl	%eax, %ecx
	cmpw	%si, (%r14,%rax,2)
	je	.L3633
.L3609:
	cmpl	-56(%rbp), %ecx
	je	.L3600
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L3605
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L3607
.L3603:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movl	32(%rax), %edx
	leaq	42372(%rcx), %rsi
	testl	%edx, %edx
	je	.L3634
	leal	-1(%rdx), %eax
	addq	$43396, %rcx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L3615:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L3615
.L3614:
	movl	-80(%rbp), %edi
	movslq	%edx, %rax
	movq	-72(%rbp), %r8
	addq	%rax, %rax
	subl	$1, %edi
	cmpl	%edx, %edi
	jle	.L3616
	.p2align 4,,10
	.p2align 3
.L3617:
	movq	8(%r8), %rcx
	movzbl	(%rcx,%rax), %ecx
	addq	$2, %rax
	movl	%edx, (%rsi,%rcx,4)
	addl	$1, %edx
	cmpl	%edi, %edx
	jne	.L3617
.L3616:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIthE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKhEEi
.L3634:
	.cfi_restore_state
	leaq	42380(%rcx), %rdi
	movq	$-1, %rax
	movq	$-1, 42372(%rcx)
	movq	$-1, 43388(%rcx)
	andq	$-8, %rdi
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L3614
	.cfi_endproc
.LFE24718:
	.size	_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi, .-_ZN2v88internal12StringSearchIthE13InitialSearchEPS2_NS0_6VectorIKhEEi
	.section	.text._ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv:
.LFB25469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rsi
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	movq	%rsi, %rcx
	movslq	%esi, %r11
	movl	%esi, %r10d
	movq	%rsi, -64(%rbp)
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	movl	%esi, %r8d
	subl	%r13d, %r10d
	leaq	0(,%r13,4), %rax
	movq	%rdx, %rbx
	leaq	0(,%r11,4), %rsi
	subq	%rax, %rbx
	subq	%rax, %rdi
	movl	%ecx, %eax
	leaq	(%rbx,%rsi), %r14
	addl	$1, %eax
	addq	%rdi, %rsi
	cmpl	%ecx, %r13d
	jge	.L3636
	movl	%ecx, -52(%rbp)
	subl	$1, %ecx
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L3660
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L3638:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L3638
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L3639
.L3637:
	movslq	%edx, %rcx
	movq	-64(%rbp), %r15
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L3639
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L3639
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L3639:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzbl	-1(%r9,%r11), %r15d
	movl	-64(%rbp), %esi
	movl	%r15d, %r11d
	.p2align 4,,10
	.p2align 3
.L3659:
	cmpl	%eax, %r8d
	jl	.L3641
	.p2align 4,,10
	.p2align 3
.L3645:
	movslq	%eax, %rdx
	cmpb	%r11b, -1(%r9,%rdx)
	je	.L3641
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L3666
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L3645
.L3641:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L3667
.L3646:
	cmpl	%esi, %r12d
	jge	.L3640
.L3669:
	movslq	%esi, %rax
	movzbl	-1(%r9,%rax), %r11d
	movl	%edx, %eax
	jmp	.L3659
	.p2align 4,,10
	.p2align 3
.L3666:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L3645
	jmp	.L3641
	.p2align 4,,10
	.p2align 3
.L3667:
	cmpl	%esi, %r12d
	jl	.L3652
	jmp	.L3647
	.p2align 4,,10
	.p2align 3
.L3668:
	cmpl	%r10d, (%r14)
	jne	.L3649
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L3649:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L3647
.L3652:
	movl	%ecx, %esi
	cmpb	%r15b, -1(%r9,%rcx)
	jne	.L3668
	cmpl	%r12d, %ecx
	jle	.L3635
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L3669
	.p2align 4,,10
	.p2align 3
.L3640:
	cmpl	-64(%rbp), %edx
	jge	.L3635
	.p2align 4,,10
	.p2align 3
.L3654:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L3656
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L3656:
	cmpl	%r13d, %edx
	je	.L3670
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L3654
.L3635:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3670:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L3654
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3647:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L3646
.L3636:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3660:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L3637
	.cfi_endproc
.LFE25469:
	.size	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi:
.LFB25159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %r13
	movq	%rdx, -88(%rbp)
	movq	(%rdi), %r10
	movq	%rdi, -80(%rbp)
	leal	-1(%rax), %edx
	movl	%eax, %r15d
	movl	%eax, %edi
	movl	%eax, -56(%rbp)
	movslq	%edx, %r8
	leaq	42372(%r10), %r11
	movl	%edx, %ebx
	negl	%edi
	movzbl	0(%r13,%r8), %r9d
	movl	%r14d, %r8d
	subl	%eax, %r8d
	subl	42372(%r10,%r9,4), %ebx
	movl	$1, %r10d
	movl	%ebx, -64(%rbp)
	leal	-2(%rax), %ebx
	movl	%r10d, %eax
	subl	%r15d, %eax
	movl	%ebx, -60(%rbp)
	movl	%eax, -68(%rbp)
.L3687:
	cmpl	%r8d, %ecx
	jle	.L3676
	jmp	.L3683
	.p2align 4,,10
	.p2align 3
.L3688:
	cmpw	$255, %bx
	ja	.L3674
	movl	%edx, %ebx
	subl	(%r11,%rax,4), %ebx
	movl	%ebx, %eax
	addl	%ebx, %ecx
	movl	%r10d, %ebx
	subl	%eax, %ebx
	addl	%ebx, %edi
	cmpl	%r8d, %ecx
	jg	.L3683
.L3676:
	leal	(%rdx,%rcx), %eax
	cltq
	movzwl	(%rsi,%rax,2), %ebx
	movq	%rbx, %rax
	cmpl	%ebx, %r9d
	jne	.L3688
	movslq	-60(%rbp), %rax
	testl	%eax, %eax
	js	.L3684
	movslq	%ecx, %rbx
	leaq	(%rsi,%rbx,2), %r15
	jmp	.L3678
	.p2align 4,,10
	.p2align 3
.L3689:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L3684
.L3678:
	movzbl	0(%r13,%rax), %r12d
	movzwl	(%r15,%rax,2), %ebx
	movl	%eax, %r14d
	cmpl	%ebx, %r12d
	je	.L3689
	movl	-56(%rbp), %eax
	movl	-64(%rbp), %ebx
	subl	%r14d, %eax
	addl	%ebx, %ecx
	subl	%ebx, %eax
	addl	%eax, %edi
	testl	%edi, %edi
	jle	.L3687
	movq	-80(%rbp), %rbx
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v88internal12StringSearchIhtE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%rbx, %rdi
	leaq	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	-88(%rbp), %rdx
	movq	%rax, 24(%rbx)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhtE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.p2align 4,,10
	.p2align 3
.L3683:
	.cfi_restore_state
	addq	$56, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3684:
	.cfi_restore_state
	addq	$56, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3674:
	.cfi_restore_state
	addl	-56(%rbp), %ecx
	addl	-68(%rbp), %edi
	jmp	.L3687
	.cfi_endproc
.LFE25159:
	.size	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi:
.LFB24722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rcx
	movq	8(%rdi), %r14
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%ecx, %eax
	subl	%ecx, %ebx
	movq	%rcx, -80(%rbp)
	movl	%ecx, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %r9d
	jg	.L3695
	movl	$-9, %edx
	movq	%rsi, %r12
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L3693
	movzbl	(%r14), %r15d
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	movl	%r15d, %r13d
	jmp	.L3697
	.p2align 4,,10
	.p2align 3
.L3721:
	andq	$-2, %rax
	subq	%r12, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	leaq	(%r12,%rdx,2), %rdx
	cmpw	%r13w, (%rdx)
	je	.L3696
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L3695
.L3697:
	movl	-52(%rbp), %edx
	movl	%r15d, %esi
	subl	%r9d, %edx
	movslq	%r9d, %r9
	movslq	%edx, %rdx
	leaq	(%r12,%r9,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L3721
.L3695:
	movl	$-1, %r9d
.L3690:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3696:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L3695
	movl	-56(%rbp), %r8d
	movl	$1, %eax
	jmp	.L3700
	.p2align 4,,10
	.p2align 3
.L3722:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %r8d
	jle	.L3699
.L3700:
	movzbl	(%r14,%rax), %edi
	movzwl	(%rdx,%rax,2), %esi
	movl	%eax, %ecx
	cmpl	%esi, %edi
	je	.L3722
.L3699:
	cmpl	-56(%rbp), %ecx
	je	.L3690
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L3695
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L3697
.L3693:
	movq	-72(%rbp), %rax
	movq	(%rax), %rdx
	movl	32(%rax), %r10d
	leaq	42372(%rdx), %rsi
	testl	%r10d, %r10d
	je	.L3723
	leal	-1(%r10), %eax
	addq	$43396, %rdx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L3705:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L3705
.L3704:
	movl	-80(%rbp), %eax
	subl	$1, %eax
	cmpl	%r10d, %eax
	jle	.L3706
	movl	-80(%rbp), %edx
	movslq	%r10d, %rcx
	movq	-72(%rbp), %r8
	leaq	1(%rcx), %rax
	subl	$2, %edx
	subl	%r10d, %edx
	addq	%rax, %rdx
	jmp	.L3707
	.p2align 4,,10
	.p2align 3
.L3724:
	addq	$1, %rax
.L3707:
	movq	8(%r8), %rdi
	movzbl	(%rdi,%rcx), %edi
	movl	%ecx, (%rsi,%rdi,4)
	movq	%rax, %rcx
	cmpq	%rdx, %rax
	jne	.L3724
.L3706:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r12, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIhtE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
.L3723:
	.cfi_restore_state
	leaq	42380(%rdx), %rdi
	movq	%rsi, %rcx
	movq	$-1, %rax
	movq	$-1, 42372(%rdx)
	movq	$-1, 43388(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L3704
	.cfi_endproc
.LFE24722:
	.size	_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIhtE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv,"axG",@progbits,_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv
	.type	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv, @function
_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv:
.LFB25474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movslq	32(%rdi), %r13
	movq	8(%rdi), %r9
	movq	(%rdi), %rdi
	leaq	0(,%r13,4), %rax
	movslq	%r15d, %r11
	movl	%r15d, %r10d
	leaq	43396(%rdi), %rdx
	addq	$44400, %rdi
	leaq	0(,%r11,4), %rsi
	subl	%r13d, %r10d
	movq	%rdx, %rbx
	subq	%rax, %rdi
	subq	%rax, %rbx
	leal	1(%r15), %eax
	leaq	(%rbx,%rsi), %r14
	addq	%rdi, %rsi
	cmpl	%r15d, %r13d
	jge	.L3726
	leal	-1(%r15), %ecx
	movl	%r15d, -52(%rbp)
	movl	%r15d, %r8d
	movq	%r13, %r12
	subl	%r13d, %ecx
	cmpl	$2, %ecx
	jbe	.L3750
	movl	%r10d, %ecx
	movd	%r10d, %xmm1
	shrl	$2, %ecx
	pshufd	$0, %xmm1, %xmm0
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L3728:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L3728
	movl	%r10d, %ecx
	andl	$-4, %ecx
	leal	(%rcx,%r12), %edx
	cmpl	%r10d, %ecx
	je	.L3729
.L3727:
	movslq	%edx, %rcx
	movl	%r10d, (%rbx,%rcx,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %r15d
	jle	.L3729
	movslq	%ecx, %rcx
	addl	$2, %edx
	movl	%r10d, (%rbx,%rcx,4)
	cmpl	%r15d, %edx
	jge	.L3729
	movslq	%edx, %rdx
	movl	%r10d, (%rbx,%rdx,4)
.L3729:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	movzwl	-2(%r9,%r11,2), %ecx
	movl	%r15d, %esi
	movq	%r13, -64(%rbp)
	movl	%ecx, %r11d
	movl	%ecx, %r13d
	.p2align 4,,10
	.p2align 3
.L3755:
	cmpl	%eax, %r8d
	jl	.L3731
.L3735:
	movslq	%eax, %rdx
	cmpw	%r11w, -2(%r9,%rdx,2)
	je	.L3731
	leaq	(%rbx,%rdx,4), %rcx
	cmpl	%r10d, (%rcx)
	je	.L3757
	movl	(%rdi,%rdx,4), %eax
	cmpl	%r8d, %eax
	jle	.L3735
.L3731:
	subl	$1, %esi
	leal	-1(%rax), %edx
	movslq	%esi, %rcx
	movl	%edx, (%rdi,%rcx,4)
	cmpl	%r8d, %edx
	je	.L3758
.L3736:
	cmpl	%esi, %r12d
	jge	.L3730
.L3760:
	movslq	%esi, %rax
	movzwl	-2(%r9,%rax,2), %r11d
	movl	%edx, %eax
	jmp	.L3755
	.p2align 4,,10
	.p2align 3
.L3757:
	subl	%esi, %eax
	movl	%eax, (%rcx)
	movl	(%rdi,%rdx,4), %eax
	jmp	.L3755
	.p2align 4,,10
	.p2align 3
.L3758:
	cmpl	%esi, %r12d
	jl	.L3742
	jmp	.L3737
	.p2align 4,,10
	.p2align 3
.L3759:
	cmpl	%r10d, (%r14)
	jne	.L3739
	movl	-52(%rbp), %edx
	subl	%ecx, %edx
	movl	%edx, (%r14)
.L3739:
	movl	%r8d, -4(%rdi,%rcx,4)
	subq	$1, %rcx
	subl	$1, %esi
	cmpl	%ecx, %r12d
	jge	.L3737
.L3742:
	movl	%ecx, %esi
	cmpw	%r13w, -2(%r9,%rcx,2)
	jne	.L3759
	cmpl	%r12d, %ecx
	jle	.L3725
	subl	$1, %esi
	leal	-2(%rax), %edx
	movslq	%esi, %rax
	movl	%edx, (%rdi,%rax,4)
	cmpl	%esi, %r12d
	jl	.L3760
	.p2align 4,,10
	.p2align 3
.L3730:
	movq	-64(%rbp), %r13
	cmpl	%r15d, %edx
	jge	.L3725
	.p2align 4,,10
	.p2align 3
.L3744:
	cmpl	%r10d, (%rbx,%r13,4)
	jne	.L3746
	movl	%edx, %eax
	subl	%r12d, %eax
	movl	%eax, (%rbx,%r13,4)
.L3746:
	cmpl	%r13d, %edx
	je	.L3761
	addq	$1, %r13
	cmpl	%r13d, %r8d
	jge	.L3744
.L3725:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3761:
	.cfi_restore_state
	movslq	%edx, %rdx
	addq	$1, %r13
	movl	(%rdi,%rdx,4), %edx
	cmpl	%r13d, %r8d
	jge	.L3744
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3737:
	.cfi_restore_state
	movl	%r8d, %edx
	jmp	.L3736
.L3726:
	movl	$1, (%r14)
	movl	%eax, (%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3750:
	.cfi_restore_state
	movl	%r13d, %edx
	jmp	.L3727
	.cfi_endproc
.LFE25474:
	.size	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv, .-_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv
	.section	.text._ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi:
.LFB25164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rax
	movq	8(%rdi), %rbx
	movq	%rdx, -72(%rbp)
	movq	(%rdi), %r10
	leal	-1(%rax), %edx
	movl	%eax, %r8d
	movl	%eax, -60(%rbp)
	movslq	%edx, %r9
	leaq	42372(%r10), %rdi
	movl	%edx, %r15d
	negl	%r8d
	movzwl	(%rbx,%r9,2), %r9d
	movzbl	%r9b, %r11d
	subl	42372(%r10,%r11,4), %r15d
	movl	%r14d, %r10d
	movl	$1, %r11d
	movl	%r15d, -56(%rbp)
	subl	%eax, %r10d
	leal	-2(%rax), %r15d
	jmp	.L3775
	.p2align 4,,10
	.p2align 3
.L3777:
	movzbl	%al, %eax
	movl	%edx, %r14d
	subl	(%rdi,%rax,4), %r14d
	movl	%r14d, %eax
	addl	%r14d, %ecx
	movl	%r11d, %r14d
	subl	%eax, %r14d
	addl	%r14d, %r8d
.L3775:
	cmpl	%r10d, %ecx
	jg	.L3776
	leal	(%rdx,%rcx), %eax
	cltq
	movzwl	(%rsi,%rax,2), %eax
	cmpw	%r9w, %ax
	jne	.L3777
	testl	%r15d, %r15d
	js	.L3772
	movslq	%ecx, %r13
	movl	%edx, -64(%rbp)
	movslq	%r15d, %rax
	leaq	(%rsi,%r13,2), %r14
	jmp	.L3767
	.p2align 4,,10
	.p2align 3
.L3778:
	subq	$1, %rax
	testl	%eax, %eax
	js	.L3772
.L3767:
	movzwl	(%r14,%rax,2), %edx
	movl	%eax, %r13d
	cmpw	%dx, (%rbx,%rax,2)
	je	.L3778
	movl	-60(%rbp), %eax
	movl	-56(%rbp), %r14d
	movl	-64(%rbp), %edx
	subl	%r13d, %eax
	addl	%r14d, %ecx
	subl	%r14d, %eax
	addl	%eax, %r8d
	testl	%r8d, %r8d
	jle	.L3775
	movq	%r12, %rdi
	movl	%ecx, -60(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal12StringSearchIttE23PopulateBoyerMooreTableEv
	movl	-60(%rbp), %ecx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	leaq	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi(%rip), %rax
	movq	-72(%rbp), %rdx
	movq	%rax, 24(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIttE16BoyerMooreSearchEPS2_NS0_6VectorIKtEEi
	.p2align 4,,10
	.p2align 3
.L3776:
	.cfi_restore_state
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3772:
	.cfi_restore_state
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25164:
	.size	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
	.section	.text._ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi,"axG",@progbits,_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.type	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi, @function
_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi:
.LFB24726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %rsi
	movq	8(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movl	%esi, %eax
	subl	%esi, %ebx
	movq	%rsi, -80(%rbp)
	movl	%esi, -56(%rbp)
	sall	$2, %eax
	cmpl	%ebx, %ecx
	jg	.L3784
	movl	$-9, %edx
	movl	%ecx, %r9d
	subl	%eax, %edx
	movl	%edx, -60(%rbp)
	testl	%edx, %edx
	jg	.L3782
	movzwl	(%r15), %r14d
	leal	1(%rbx), %eax
	movl	%eax, -52(%rbp)
	movl	%r14d, %eax
	movzbl	%ah, %eax
	cmpb	%al, %r14b
	cmova	%r14d, %eax
	movzbl	%al, %r12d
	jmp	.L3786
	.p2align 4,,10
	.p2align 3
.L3811:
	andq	$-2, %rax
	subq	%r13, %rax
	sarq	%rax
	movslq	%eax, %rdx
	movl	%eax, %r9d
	leaq	0(%r13,%rdx,2), %rdx
	cmpw	%r14w, (%rdx)
	je	.L3785
	leal	1(%rax), %r9d
	cmpl	%eax, %ebx
	jle	.L3784
.L3786:
	movl	-52(%rbp), %edx
	movl	%r12d, %esi
	subl	%r9d, %edx
	movslq	%r9d, %r9
	movslq	%edx, %rdx
	leaq	0(%r13,%r9,2), %rdi
	addq	%rdx, %rdx
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L3811
.L3784:
	movl	$-1, %r9d
.L3779:
	addq	$56, %rsp
	movl	%r9d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3785:
	.cfi_restore_state
	cmpl	$-1, %eax
	je	.L3784
	movl	-56(%rbp), %esi
	movl	$1, %eax
	jmp	.L3789
	.p2align 4,,10
	.p2align 3
.L3812:
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpl	%eax, %esi
	jle	.L3788
.L3789:
	movzwl	(%rdx,%rax,2), %edi
	movl	%eax, %ecx
	cmpw	%di, (%r15,%rax,2)
	je	.L3812
.L3788:
	cmpl	-56(%rbp), %ecx
	je	.L3779
	addl	$1, %r9d
	addl	-60(%rbp), %ecx
	cmpl	%r9d, %ebx
	jl	.L3784
	leal	1(%rcx), %eax
	movl	%eax, -60(%rbp)
	testl	%eax, %eax
	jle	.L3786
.L3782:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movl	32(%rax), %edx
	leaq	42372(%rcx), %rsi
	testl	%edx, %edx
	je	.L3813
	leal	-1(%rdx), %eax
	addq	$43396, %rcx
	movd	%eax, %xmm0
	movq	%rsi, %rax
	pshufd	$0, %xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L3794:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L3794
.L3793:
	movl	-80(%rbp), %edi
	movslq	%edx, %rax
	movq	-72(%rbp), %r8
	addq	%rax, %rax
	subl	$1, %edi
	cmpl	%edx, %edi
	jle	.L3795
	.p2align 4,,10
	.p2align 3
.L3796:
	movq	8(%r8), %rcx
	movzbl	(%rcx,%rax), %ecx
	addq	$2, %rax
	movl	%edx, (%rsi,%rcx,4)
	addl	$1, %edx
	cmpl	%edi, %edx
	jne	.L3796
.L3795:
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movl	%r9d, %ecx
	leaq	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi(%rip), %rbx
	movq	%rbx, 24(%rax)
	addq	$56, %rsp
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12StringSearchIttE24BoyerMooreHorspoolSearchEPS2_NS0_6VectorIKtEEi
.L3813:
	.cfi_restore_state
	leaq	42380(%rcx), %rdi
	movq	$-1, %rax
	movq	$-1, 42372(%rcx)
	movq	$-1, 43388(%rcx)
	andq	$-8, %rdi
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	addl	$1024, %ecx
	shrl	$3, %ecx
	rep stosq
	jmp	.L3793
	.cfi_endproc
.LFE24726:
	.size	_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi, .-_ZN2v88internal12StringSearchIttE13InitialSearchEPS2_NS0_6VectorIKtEEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii, @function
_GLOBAL__sub_I__ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii:
.LFB25854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25854:
	.size	_GLOBAL__sub_I__ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii, .-_GLOBAL__sub_I__ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19CompiledReplacement7CompileEPNS0_7IsolateENS0_6HandleINS0_8JSRegExpEEENS4_INS0_6StringEEEii
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchE, 88
_ZTVN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch8GetMatchEv
	.quad	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch9GetPrefixEv
	.quad	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch9GetSuffixEv
	.quad	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch12CaptureCountEv
	.quad	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch16HasNamedCapturesEv
	.quad	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch10GetCaptureEiPb
	.quad	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS4_5Match12CaptureStateE
	.quad	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_120MatchInfoBackedMatchD0Ev
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_117VectorBackedMatchE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_117VectorBackedMatchE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_117VectorBackedMatchE, 88
_ZTVN2v88internal12_GLOBAL__N_117VectorBackedMatchE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch8GetMatchEv
	.quad	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch9GetPrefixEv
	.quad	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch9GetSuffixEv
	.quad	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch12CaptureCountEv
	.quad	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch16HasNamedCapturesEv
	.quad	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch10GetCaptureEiPb
	.quad	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS4_5Match12CaptureStateE
	.quad	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_117VectorBackedMatchD0Ev
	.section	.bss._ZZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateEE29trace_event_unique_atomic1928,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateEE29trace_event_unique_atomic1928, @object
	.size	_ZZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateEE29trace_event_unique_atomic1928, 8
_ZZN2v88internalL22Stats_Runtime_IsRegExpEiPmPNS0_7IsolateEE29trace_event_unique_atomic1928:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateEE29trace_event_unique_atomic1912,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateEE29trace_event_unique_atomic1912, @object
	.size	_ZZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateEE29trace_event_unique_atomic1912, 8
_ZZN2v88internalL40Stats_Runtime_RegExpInitializeAndCompileEiPmPNS0_7IsolateEE29trace_event_unique_atomic1912:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateEE29trace_event_unique_atomic1713,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateEE29trace_event_unique_atomic1713, @object
	.size	_ZZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateEE29trace_event_unique_atomic1713, 8
_ZZN2v88internalL29Stats_Runtime_RegExpReplaceRTEiPmPNS0_7IsolateEE29trace_event_unique_atomic1713:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateEE29trace_event_unique_atomic1559,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateEE29trace_event_unique_atomic1559, @object
	.size	_ZZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateEE29trace_event_unique_atomic1559, 8
_ZZN2v88internalL25Stats_Runtime_RegExpSplitEiPmPNS0_7IsolateEE29trace_event_unique_atomic1559:
	.zero	8
	.section	.bss._ZZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1408,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1408, @object
	.size	_ZZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1408, 8
_ZZN2v88internalL54Stats_Runtime_StringReplaceNonGlobalRegExpWithFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1408:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1381,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1381, @object
	.size	_ZZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1381, 8
_ZZN2v88internalL32Stats_Runtime_RegExpExecMultipleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1381:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_RegExpExecEiPmPNS0_7IsolateEE28trace_event_unique_atomic882,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_RegExpExecEiPmPNS0_7IsolateEE28trace_event_unique_atomic882, @object
	.size	_ZZN2v88internalL24Stats_Runtime_RegExpExecEiPmPNS0_7IsolateEE28trace_event_unique_atomic882, 8
_ZZN2v88internalL24Stats_Runtime_RegExpExecEiPmPNS0_7IsolateEE28trace_event_unique_atomic882:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateEE28trace_event_unique_atomic801,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateEE28trace_event_unique_atomic801, @object
	.size	_ZZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateEE28trace_event_unique_atomic801, 8
_ZZN2v88internalL25Stats_Runtime_StringSplitEiPmPNS0_7IsolateEE28trace_event_unique_atomic801:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	4294967295
	.long	2146435071
	.align 8
.LC3:
	.long	4290772992
	.long	1105199103
	.align 8
.LC4:
	.long	0
	.long	-1042284544
	.align 8
.LC21:
	.long	0
	.long	1072693248
	.align 8
.LC22:
	.long	4292870144
	.long	1106247679
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
