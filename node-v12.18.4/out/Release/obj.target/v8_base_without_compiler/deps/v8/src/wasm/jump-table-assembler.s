	.file	"jump-table-assembler.cc"
	.text
	.section	.text._ZN2v88internal4wasm18JumpTableAssembler23EmitLazyCompileJumpSlotEjm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm18JumpTableAssembler23EmitLazyCompileJumpSlotEjm
	.type	_ZN2v88internal4wasm18JumpTableAssembler23EmitLazyCompileJumpSlotEjm, @function
_ZN2v88internal4wasm18JumpTableAssembler23EmitLazyCompileJumpSlotEjm:
.LFB19449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	call	_ZN2v88internal9Assembler11pushq_imm32Ei@PLT
	subq	32(%r12), %rbx
	movq	%r12, %rdi
	movl	$19, %edx
	leaq	-5(%rbx), %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler8near_jmpElNS0_9RelocInfo4ModeE@PLT
	.cfi_endproc
.LFE19449:
	.size	_ZN2v88internal4wasm18JumpTableAssembler23EmitLazyCompileJumpSlotEjm, .-_ZN2v88internal4wasm18JumpTableAssembler23EmitLazyCompileJumpSlotEjm
	.section	.text._ZN2v88internal4wasm18JumpTableAssembler19EmitRuntimeStubSlotEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm18JumpTableAssembler19EmitRuntimeStubSlotEm
	.type	_ZN2v88internal4wasm18JumpTableAssembler19EmitRuntimeStubSlotEm, @function
_ZN2v88internal4wasm18JumpTableAssembler19EmitRuntimeStubSlotEm:
.LFB19450:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal14MacroAssembler23JumpToInstructionStreamEm@PLT
	.cfi_endproc
.LFE19450:
	.size	_ZN2v88internal4wasm18JumpTableAssembler19EmitRuntimeStubSlotEm, .-_ZN2v88internal4wasm18JumpTableAssembler19EmitRuntimeStubSlotEm
	.section	.text._ZN2v88internal4wasm18JumpTableAssembler12EmitJumpSlotEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm18JumpTableAssembler12EmitJumpSlotEm
	.type	_ZN2v88internal4wasm18JumpTableAssembler12EmitJumpSlotEm, @function
_ZN2v88internal4wasm18JumpTableAssembler12EmitJumpSlotEm:
.LFB19451:
	.cfi_startproc
	endbr64
	subq	32(%rdi), %rsi
	movl	$19, %edx
	subq	$5, %rsi
	jmp	_ZN2v88internal9Assembler8near_jmpElNS0_9RelocInfo4ModeE@PLT
	.cfi_endproc
.LFE19451:
	.size	_ZN2v88internal4wasm18JumpTableAssembler12EmitJumpSlotEm, .-_ZN2v88internal4wasm18JumpTableAssembler12EmitJumpSlotEm
	.section	.text._ZN2v88internal4wasm18JumpTableAssembler8NopBytesEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm18JumpTableAssembler8NopBytesEi
	.type	_ZN2v88internal4wasm18JumpTableAssembler8NopBytesEi, @function
_ZN2v88internal4wasm18JumpTableAssembler8NopBytesEi:
.LFB19452:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal9Assembler3NopEi@PLT
	.cfi_endproc
.LFE19452:
	.size	_ZN2v88internal4wasm18JumpTableAssembler8NopBytesEi, .-_ZN2v88internal4wasm18JumpTableAssembler8NopBytesEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm18JumpTableAssembler23EmitLazyCompileJumpSlotEjm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm18JumpTableAssembler23EmitLazyCompileJumpSlotEjm, @function
_GLOBAL__sub_I__ZN2v88internal4wasm18JumpTableAssembler23EmitLazyCompileJumpSlotEjm:
.LFB23336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23336:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm18JumpTableAssembler23EmitLazyCompileJumpSlotEjm, .-_GLOBAL__sub_I__ZN2v88internal4wasm18JumpTableAssembler23EmitLazyCompileJumpSlotEjm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm18JumpTableAssembler23EmitLazyCompileJumpSlotEjm
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
