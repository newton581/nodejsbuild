	.file	"bytecode-array-random-iterator.cc"
	.text
	.section	.rodata._ZN2v88internal11interpreter27BytecodeArrayRandomIterator10InitializeEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal11interpreter27BytecodeArrayRandomIterator10InitializeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator10InitializeEv
	.type	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator10InitializeEv, @function
_ZN2v88internal11interpreter27BytecodeArrayRandomIterator10InitializeEv:
.LFB17810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	$2147483648, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movabsq	$4611686018427387900, %rbx
	subq	$40, %rsp
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L31:
	movl	%edx, (%r14)
	addq	$4, 40(%r12)
.L4:
	movl	8(%r12), %r14d
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter21BytecodeArrayAccessor21current_bytecode_sizeEv@PLT
	movq	%r12, %rdi
	leal	(%rax,%r14), %esi
	call	_ZN2v88internal11interpreter21BytecodeArrayAccessor9SetOffsetEi@PLT
.L15:
	movq	(%r12), %rdi
	movl	8(%r12), %r14d
	movq	(%rdi), %rax
	call	*(%rax)
	cmpl	%r14d, %eax
	jle	.L2
	movl	8(%r12), %edx
	movq	40(%r12), %r14
	cmpq	48(%r12), %r14
	jne	.L31
	movq	32(%r12), %r15
	movq	%r14, %rcx
	subq	%r15, %rcx
	movq	%rcx, %rax
	sarq	$2, %rax
	cmpq	$536870911, %rax
	je	.L32
	testq	%rax, %rax
	je	.L17
	leaq	(%rax,%rax), %rdi
	movq	%r13, %rsi
	movl	$2147483644, %r8d
	cmpq	%rdi, %rax
	jbe	.L33
.L6:
	movq	24(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r9
	subq	%rax, %r9
	cmpq	%r9, %rsi
	ja	.L34
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L9:
	leaq	(%rax,%r8), %rdi
	leaq	4(%rax), %rsi
.L7:
	movl	%edx, (%rax,%rcx)
	cmpq	%r15, %r14
	je	.L10
	leaq	-4(%r14), %r8
	leaq	15(%rax), %rdx
	subq	%r15, %r8
	subq	%r15, %rdx
	movq	%r8, %rcx
	shrq	$2, %rcx
	cmpq	$30, %rdx
	jbe	.L20
	testq	%rbx, %rcx
	je	.L20
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rsi
	shrq	$2, %rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L12:
	movdqu	(%r15,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L12
	movq	%rcx, %rsi
	andq	$-4, %rsi
	leaq	0(,%rsi,4), %rdx
	addq	%rdx, %r15
	addq	%rax, %rdx
	cmpq	%rsi, %rcx
	je	.L14
	movl	(%r15), %ecx
	movl	%ecx, (%rdx)
	leaq	4(%r15), %rcx
	cmpq	%rcx, %r14
	je	.L14
	movl	4(%r15), %ecx
	movl	%ecx, 4(%rdx)
	leaq	8(%r15), %rcx
	cmpq	%rcx, %r14
	je	.L14
	movl	8(%r15), %ecx
	movl	%ecx, 8(%rdx)
.L14:
	leaq	8(%rax,%r8), %rsi
.L10:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%rdi, 48(%r12)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 32(%r12)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L33:
	testq	%rdi, %rdi
	jne	.L35
	movl	$4, %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$0, 56(%r12)
	movq	32(%r12), %rax
	cmpq	40(%r12), %rax
	je	.L1
	movl	(%rax), %esi
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter21BytecodeArrayAccessor9SetOffsetEi@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	$8, %esi
	movl	$4, %r8d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L11:
	movl	(%r15,%rdx,4), %esi
	movl	%esi, (%rax,%rdx,4)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L11
	jmp	.L14
.L34:
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movl	%edx, -52(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-52(%rbp), %edx
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %rcx
	jmp	.L9
.L32:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L35:
	cmpq	$536870911, %rdi
	movl	$536870911, %r8d
	cmova	%r8, %rdi
	leaq	0(,%rdi,4), %r8
	leaq	7(%r8), %rsi
	andq	$-8, %rsi
	jmp	.L6
	.cfi_endproc
.LFE17810:
	.size	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator10InitializeEv, .-_ZN2v88internal11interpreter27BytecodeArrayRandomIterator10InitializeEv
	.section	.text._ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE
	.type	_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE, @function
_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE:
.LFB17805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	leaq	-32(%rbp), %rsi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEi@PLT
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L37
	movq	(%rdi), %rax
	call	*72(%rax)
.L37:
	movq	%rbx, 24(%r12)
	movq	%r12, %rdi
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	call	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator10InitializeEv
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L43
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L43:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17805:
	.size	_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE, .-_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE
	.globl	_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC1ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE
	.set	_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC1ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE,_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE
	.section	.text._ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneE
	.type	_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneE, @function
_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneE:
.LFB17808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	xorl	%edx, %edx
	call	_ZN2v88internal11interpreter21BytecodeArrayAccessorC2ENS0_6HandleINS0_13BytecodeArrayEEEi@PLT
	movq	%rbx, 24(%r12)
	movq	%r12, %rdi
	popq	%rbx
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator10InitializeEv
	.cfi_endproc
.LFE17808:
	.size	_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneE, .-_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneE
	.globl	_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC1ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneE
	.set	_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC1ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneE,_ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ENS0_6HandleINS0_13BytecodeArrayEEEPNS0_4ZoneE
	.section	.text._ZNK2v88internal11interpreter27BytecodeArrayRandomIterator7IsValidEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter27BytecodeArrayRandomIterator7IsValidEv
	.type	_ZNK2v88internal11interpreter27BytecodeArrayRandomIterator7IsValidEv, @function
_ZNK2v88internal11interpreter27BytecodeArrayRandomIterator7IsValidEv:
.LFB17811:
	.cfi_startproc
	endbr64
	movslq	56(%rdi), %rax
	xorl	%r8d, %r8d
	testl	%eax, %eax
	js	.L46
	movq	40(%rdi), %rdx
	subq	32(%rdi), %rdx
	sarq	$2, %rdx
	cmpq	%rdx, %rax
	setb	%r8b
.L46:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE17811:
	.size	_ZNK2v88internal11interpreter27BytecodeArrayRandomIterator7IsValidEv, .-_ZNK2v88internal11interpreter27BytecodeArrayRandomIterator7IsValidEv
	.section	.text._ZN2v88internal11interpreter27BytecodeArrayRandomIterator21UpdateOffsetFromIndexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator21UpdateOffsetFromIndexEv
	.type	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator21UpdateOffsetFromIndexEv, @function
_ZN2v88internal11interpreter27BytecodeArrayRandomIterator21UpdateOffsetFromIndexEv:
.LFB17812:
	.cfi_startproc
	endbr64
	movslq	56(%rdi), %rax
	testl	%eax, %eax
	js	.L49
	movq	32(%rdi), %rcx
	movq	40(%rdi), %rdx
	subq	%rcx, %rdx
	sarq	$2, %rdx
	cmpq	%rdx, %rax
	jnb	.L49
	movl	(%rcx,%rax,4), %esi
	jmp	_ZN2v88internal11interpreter21BytecodeArrayAccessor9SetOffsetEi@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	ret
	.cfi_endproc
.LFE17812:
	.size	_ZN2v88internal11interpreter27BytecodeArrayRandomIterator21UpdateOffsetFromIndexEv, .-_ZN2v88internal11interpreter27BytecodeArrayRandomIterator21UpdateOffsetFromIndexEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE:
.LFB21600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21600:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE, .-_GLOBAL__sub_I__ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter27BytecodeArrayRandomIteratorC2ESt10unique_ptrINS1_21AbstractBytecodeArrayESt14default_deleteIS4_EEPNS0_4ZoneE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
