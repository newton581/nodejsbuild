	.file	"runtime-strings.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch8GetMatchEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch8GetMatchEv, @function
_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch8GetMatchEv:
.LFB20129:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE20129:
	.size	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch8GetMatchEv, .-_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch8GetMatchEv
	.section	.text._ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch9GetPrefixEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch9GetPrefixEv, @function
_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch9GetPrefixEv:
.LFB20130:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE20130:
	.size	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch9GetPrefixEv, .-_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch9GetPrefixEv
	.section	.text._ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch9GetSuffixEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch9GetSuffixEv, @function
_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch9GetSuffixEv:
.LFB20131:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE20131:
	.size	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch9GetSuffixEv, .-_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch9GetSuffixEv
	.section	.text._ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch12CaptureCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch12CaptureCountEv, @function
_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch12CaptureCountEv:
.LFB20132:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE20132:
	.size	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch12CaptureCountEv, .-_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch12CaptureCountEv
	.section	.text._ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch16HasNamedCapturesEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch16HasNamedCapturesEv, @function
_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch16HasNamedCapturesEv:
.LFB20133:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE20133:
	.size	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch16HasNamedCapturesEv, .-_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch16HasNamedCapturesEv
	.section	.text._ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch10GetCaptureEiPb,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch10GetCaptureEiPb, @function
_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch10GetCaptureEiPb:
.LFB20134:
	.cfi_startproc
	endbr64
	movb	$0, (%rdx)
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE20134:
	.size	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch10GetCaptureEiPb, .-_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch10GetCaptureEiPb
	.section	.text._ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD2Ev, @function
_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD2Ev:
.LFB25268:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25268:
	.size	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD2Ev, .-_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD2Ev
	.set	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD1Ev,_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD2Ev
	.section	.rodata._ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS6_5Match12CaptureStateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS6_5Match12CaptureStateE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS6_5Match12CaptureStateE, @function
_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS6_5Match12CaptureStateE:
.LFB20135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20135:
	.size	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS6_5Match12CaptureStateE, .-_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS6_5Match12CaptureStateE
	.section	.text._ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD0Ev, @function
_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD0Ev:
.LFB25270:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25270:
	.size	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD0Ev, .-_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD0Ev
	.section	.text._ZN2v88internalL29CopyCachedOneByteCharsToArrayEPNS0_4HeapEPKhNS0_10FixedArrayEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29CopyCachedOneByteCharsToArrayEPNS0_4HeapEPKhNS0_10FixedArrayEi, @function
_ZN2v88internalL29CopyCachedOneByteCharsToArrayEPNS0_4HeapEPKhNS0_10FixedArrayEi:
.LFB20173:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	andq	$-262144, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	$4, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	-32976(%rdi), %rax
	movq	-37504(%rdi), %r9
	leaq	8(%rdx), %rdi
	movq	8(%rdx), %rdx
	movq	%rdi, -56(%rbp)
	testl	$262144, %edx
	jne	.L16
	xorl	%r14d, %r14d
	andl	$24, %edx
	sete	%r14b
	sall	$2, %r14d
.L16:
	testl	%ecx, %ecx
	jle	.L27
	leal	-1(%rcx), %r10d
	leaq	15(%r11), %rsi
	subq	$1, %rax
	movl	%ecx, -60(%rbp)
	movq	%r11, %rdi
	xorl	%ebx, %ebx
	movq	%r10, %r11
	movq	%rax, %r8
	movq	%r15, %r10
	movq	%rsi, %r13
	movq	%r9, %r15
	movl	%r14d, %ecx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L20:
	testb	%dl, %dl
	jne	.L19
.L34:
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L19
	movq	-56(%rbp), %rax
	testb	$24, (%rax)
	jne	.L19
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r10, -96(%rbp)
	movq	%r11, -88(%rbp)
	movq	%r8, -80(%rbp)
	movl	%ecx, -64(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %r10
	movq	-88(%rbp), %r11
	movq	-80(%rbp), %r8
	movl	-64(%rbp), %ecx
	movq	-72(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L19:
	leal	1(%r14), %esi
	leaq	1(%rbx), %rdx
	addq	$8, %r13
	cmpq	%rbx, %r11
	je	.L40
	movq	%rdx, %rbx
.L24:
	movzbl	(%r10,%rbx), %edx
	movl	%ebx, %r14d
	leaq	16(,%rdx,8), %rdx
	movq	(%rdx,%r8), %r12
	cmpq	%r12, %r15
	je	.L18
	movq	%r12, 0(%r13)
	testl	%ecx, %ecx
	je	.L19
	movq	%r12, %rdx
	notq	%rdx
	andl	$1, %edx
	cmpl	$4, %ecx
	jne	.L20
	testb	%dl, %dl
	jne	.L19
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$4, 10(%rdx)
	je	.L34
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r10, -96(%rbp)
	movq	%r11, -88(%rbp)
	movq	%r8, -80(%rbp)
	movl	%ecx, -64(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rdi
	movl	-64(%rbp), %ecx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r11
	movq	-96(%rbp), %r10
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L18:
	movl	-60(%rbp), %ecx
	movl	%ebx, %r8d
	movq	%r13, %rdi
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	subl	%ebx, %ecx
	movslq	%ecx, %rcx
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
.L15:
	addq	$56, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	movl	%esi, %r8d
	jmp	.L15
.L27:
	xorl	%r8d, %r8d
	jmp	.L15
	.cfi_endproc
.LFE20173:
	.size	_ZN2v88internalL29CopyCachedOneByteCharsToArrayEPNS0_4HeapEPKhNS0_10FixedArrayEi, .-_ZN2v88internalL29CopyCachedOneByteCharsToArrayEPNS0_4HeapEPKhNS0_10FixedArrayEi
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L47
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L50
.L41:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L41
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL27Stats_Runtime_StringIndexOfEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC2:
	.string	"V8.Runtime_Runtime_StringIndexOf"
	.section	.text._ZN2v88internalL27Stats_Runtime_StringIndexOfEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_StringIndexOfEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_StringIndexOfEiPmPNS0_7IsolateE.isra.0:
.LFB25350:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L81
.L52:
	movq	_ZZN2v88internalL27Stats_Runtime_StringIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic196(%rip), %rbx
	testq	%rbx, %rbx
	je	.L82
.L54:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L83
.L56:
	leaq	-16(%r13), %rcx
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L60
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L60:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L84
.L51:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L86
.L55:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_StringIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic196(%rip)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L83:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L87
.L57:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L58
	movq	(%rdi), %rax
	call	*8(%rax)
.L58:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L59
	movq	(%rdi), %rax
	call	*8(%rax)
.L59:
	leaq	.LC2(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L81:
	movq	40960(%rsi), %rax
	movl	$535, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L87:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L55
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25350:
	.size	_ZN2v88internalL27Stats_Runtime_StringIndexOfEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_StringIndexOfEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"V8.Runtime_Runtime_StringLessThan"
	.section	.rodata._ZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"args[0].IsString()"
.LC5:
	.string	"Check failed: %s."
.LC6:
	.string	"args[1].IsString()"
	.section	.text._ZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateE, @function
_ZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateE:
.LFB20178:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L125
.L89:
	movq	_ZZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateEE28trace_event_unique_atomic424(%rip), %rbx
	testq	%rbx, %rbx
	je	.L126
.L91:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L127
.L93:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L97
.L98:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L126:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L128
.L92:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateEE28trace_event_unique_atomic424(%rip)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L97:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L98
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	testb	$1, %al
	jne	.L129
.L99:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L127:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L130
.L94:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L95
	movq	(%rdi), %rax
	call	*8(%rax)
.L95:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L96
	movq	(%rdi), %rax
	call	*8(%rax)
.L96:
	leaq	.LC3(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L129:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L99
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	movl	$18, %edi
	movl	%eax, %esi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	testb	%al, %al
	je	.L101
	movq	112(%r12), %r13
.L102:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L105
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L105:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L131
.L88:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	movq	120(%r12), %r13
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L125:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$538, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L130:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L128:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L92
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20178:
	.size	_ZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateE, .-_ZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Stats_Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"V8.Runtime_Runtime_StringLessThanOrEqual"
	.section	.text._ZN2v88internalL35Stats_Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateE, @function
_ZN2v88internalL35Stats_Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateE:
.LFB20181:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L170
.L134:
	movq	_ZZN2v88internalL35Stats_Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic435(%rip), %rbx
	testq	%rbx, %rbx
	je	.L171
.L136:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L172
.L138:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L142
.L143:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L171:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L173
.L137:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic435(%rip)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L142:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L143
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	testb	$1, %al
	jne	.L174
.L144:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L175
.L139:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	movq	(%rdi), %rax
	call	*8(%rax)
.L140:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rax
	call	*8(%rax)
.L141:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L174:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L144
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	movl	$19, %edi
	movl	%eax, %esi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	testb	%al, %al
	je	.L146
	movq	112(%r12), %r13
.L147:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L150
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L150:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L176
.L133:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L177
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movq	120(%r12), %r13
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L170:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$539, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L176:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L175:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L173:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L137
.L177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20181:
	.size	_ZN2v88internalL35Stats_Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateE, .-_ZN2v88internalL35Stats_Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Stats_Runtime_StringGreaterThanEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"V8.Runtime_Runtime_StringGreaterThan"
	.section	.text._ZN2v88internalL31Stats_Runtime_StringGreaterThanEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_StringGreaterThanEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_StringGreaterThanEiPmPNS0_7IsolateE:
.LFB20184:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L215
.L179:
	movq	_ZZN2v88internalL31Stats_Runtime_StringGreaterThanEiPmPNS0_7IsolateEE28trace_event_unique_atomic446(%rip), %rbx
	testq	%rbx, %rbx
	je	.L216
.L181:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L217
.L183:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L187
.L188:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L218
.L182:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_StringGreaterThanEiPmPNS0_7IsolateEE28trace_event_unique_atomic446(%rip)
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L187:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L188
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	testb	$1, %al
	jne	.L219
.L189:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L217:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L220
.L184:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L185
	movq	(%rdi), %rax
	call	*8(%rax)
.L185:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L186
	movq	(%rdi), %rax
	call	*8(%rax)
.L186:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L219:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L189
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	movl	$20, %edi
	movl	%eax, %esi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	testb	%al, %al
	je	.L191
	movq	112(%r12), %r13
.L192:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L195
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L195:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L221
.L178:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L222
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movq	120(%r12), %r13
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L215:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$532, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L220:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L218:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L182
.L222:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20184:
	.size	_ZN2v88internalL31Stats_Runtime_StringGreaterThanEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_StringGreaterThanEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Stats_Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"V8.Runtime_Runtime_StringGreaterThanOrEqual"
	.section	.text._ZN2v88internalL38Stats_Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateE, @function
_ZN2v88internalL38Stats_Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateE:
.LFB20187:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L260
.L224:
	movq	_ZZN2v88internalL38Stats_Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic457(%rip), %rbx
	testq	%rbx, %rbx
	je	.L261
.L226:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L262
.L228:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L232
.L233:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L261:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L263
.L227:
	movq	%rbx, _ZZN2v88internalL38Stats_Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic457(%rip)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L232:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L233
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	testb	$1, %al
	jne	.L264
.L234:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L262:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L265
.L229:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L230
	movq	(%rdi), %rax
	call	*8(%rax)
.L230:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L231
	movq	(%rdi), %rax
	call	*8(%rax)
.L231:
	leaq	.LC9(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L264:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L234
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	movl	$21, %edi
	movl	%eax, %esi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	testb	%al, %al
	je	.L236
	movq	112(%r12), %r13
.L237:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L240
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L240:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L266
.L223:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L267
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movq	120(%r12), %r13
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L260:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$533, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L266:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L265:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L263:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L227
.L267:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20187:
	.size	_ZN2v88internalL38Stats_Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateE, .-_ZN2v88internalL38Stats_Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL25Stats_Runtime_StringEqualEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"V8.Runtime_Runtime_StringEqual"
	.section	.text._ZN2v88internalL25Stats_Runtime_StringEqualEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_StringEqualEiPmPNS0_7IsolateE, @function
_ZN2v88internalL25Stats_Runtime_StringEqualEiPmPNS0_7IsolateE:
.LFB20190:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L309
.L269:
	movq	_ZZN2v88internalL25Stats_Runtime_StringEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic468(%rip), %rbx
	testq	%rbx, %rbx
	je	.L310
.L271:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L311
.L273:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L277
.L278:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L310:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L312
.L272:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_StringEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic468(%rip)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L277:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L278
	movq	-8(%r13), %rdx
	leaq	-8(%r13), %r8
	testb	$1, %dl
	jne	.L313
.L279:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L311:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L314
.L274:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L275
	movq	(%rdi), %rax
	call	*8(%rax)
.L275:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L276
	movq	(%rdi), %rax
	call	*8(%rax)
.L276:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L313:
	movq	-1(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L279
	cmpq	-8(%r13), %rax
	je	.L283
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L285
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L284
.L285:
	movq	%r8, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	jne	.L283
.L284:
	movq	120(%r12), %r13
.L286:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L289
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L289:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L315
.L268:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L316
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movq	112(%r12), %r13
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L309:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$530, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L315:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L314:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L312:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L272
.L316:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20190:
	.size	_ZN2v88internalL25Stats_Runtime_StringEqualEiPmPNS0_7IsolateE, .-_ZN2v88internalL25Stats_Runtime_StringEqualEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Stats_Runtime_StringMaxLengthEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"V8.Runtime_Runtime_StringMaxLength"
	.section	.text._ZN2v88internalL29Stats_Runtime_StringMaxLengthEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_StringMaxLengthEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_StringMaxLengthEiPmPNS0_7IsolateE.isra.0:
.LFB25351:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L346
.L318:
	movq	_ZZN2v88internalL29Stats_Runtime_StringMaxLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic483(%rip), %rbx
	testq	%rbx, %rbx
	je	.L347
	movq	$0, -128(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L348
.L322:
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L349
.L326:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L350
	movabsq	$4611685911053205504, %rax
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L351
.L321:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_StringMaxLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic483(%rip)
	movq	$0, -128(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	je	.L322
.L348:
	pxor	%xmm0, %xmm0
	xorl	%r12d, %r12d
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L352
.L323:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L324
	movq	(%rdi), %rax
	call	*8(%rax)
.L324:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L325
	movq	(%rdi), %rax
	call	*8(%rax)
.L325:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -120(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-120(%rbp), %rax
	movq	%r12, -104(%rbp)
	movq	%rax, -128(%rbp)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L349:
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L346:
	movq	40960(%rdi), %rdi
	leaq	-88(%rbp), %rsi
	movl	$540, %edx
	addq	$23240, %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L352:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r12
	addq	$64, %rsp
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L351:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L321
.L350:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25351:
	.size	_ZN2v88internalL29Stats_Runtime_StringMaxLengthEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_StringMaxLengthEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_StringLastIndexOfEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"V8.Runtime_Runtime_StringLastIndexOf"
	.section	.text._ZN2v88internalL31Stats_Runtime_StringLastIndexOfEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_StringLastIndexOfEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_StringLastIndexOfEiPmPNS0_7IsolateE.isra.0:
.LFB25355:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L383
.L354:
	movq	_ZZN2v88internalL31Stats_Runtime_StringLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic217(%rip), %rbx
	testq	%rbx, %rbx
	je	.L384
.L356:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L385
.L358:
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	leaq	88(%r12), %rcx
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	call	_ZN2v88internal6String11LastIndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L362
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L362:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L386
.L353:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L388
.L357:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_StringLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic217(%rip)
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L385:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L389
.L359:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L360
	movq	(%rdi), %rax
	call	*8(%rax)
.L360:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L361
	movq	(%rdi), %rax
	call	*8(%rax)
.L361:
	leaq	.LC12(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L386:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L383:
	movq	40960(%rsi), %rax
	movl	$537, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L389:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L388:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L357
.L387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25355:
	.size	_ZN2v88internalL31Stats_Runtime_StringLastIndexOfEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_StringLastIndexOfEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL23Stats_Runtime_StringAddEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC13:
	.string	"V8.Runtime_Runtime_StringAdd"
	.section	.text._ZN2v88internalL23Stats_Runtime_StringAddEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL23Stats_Runtime_StringAddEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL23Stats_Runtime_StringAddEiPmPNS0_7IsolateE.isra.0:
.LFB25369:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L433
.L391:
	movq	_ZZN2v88internalL23Stats_Runtime_StringAddEiPmPNS0_7IsolateEE28trace_event_unique_atomic236(%rip), %rbx
	testq	%rbx, %rbx
	je	.L434
.L393:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L435
.L395:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L399
.L400:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L434:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L436
.L394:
	movq	%rbx, _ZZN2v88internalL23Stats_Runtime_StringAddEiPmPNS0_7IsolateEE28trace_event_unique_atomic236(%rip)
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L399:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L400
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	testb	$1, %al
	jne	.L437
.L401:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L435:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L438
.L396:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L397
	movq	(%rdi), %rax
	call	*8(%rax)
.L397:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L398
	movq	(%rdi), %rax
	call	*8(%rax)
.L398:
	leaq	.LC13(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L437:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L401
	movq	40960(%r12), %r15
	cmpb	$0, 7072(%r15)
	je	.L403
	movq	7064(%r15), %rax
.L404:
	testq	%rax, %rax
	je	.L405
	addl	$1, (%rax)
.L405:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
	testq	%rax, %rax
	je	.L439
	movq	(%rax), %r13
.L407:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L410
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L410:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L440
.L390:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L441
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movb	$1, 7072(%r15)
	leaq	7048(%r15), %rdi
	movq	%rdx, -168(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	-168(%rbp), %rdx
	movq	%rax, 7064(%r15)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L439:
	movq	312(%r12), %r13
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L433:
	movq	40960(%rsi), %rax
	movl	$527, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L440:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L438:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L436:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L394
.L441:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25369:
	.size	_ZN2v88internalL23Stats_Runtime_StringAddEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL23Stats_Runtime_StringAddEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"V8.Runtime_Runtime_GetSubstitution"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC15:
	.string	"args[2].IsSmi()"
.LC16:
	.string	"args[3].IsString()"
.LC17:
	.string	"args[4].IsSmi()"
	.section	.text._ZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateE.isra.0:
.LFB25370:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L487
.L443:
	movq	_ZZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateEE27trace_event_unique_atomic21(%rip), %rbx
	testq	%rbx, %rbx
	je	.L488
.L445:
	movq	$0, -192(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L489
.L447:
	movq	41088(%r12), %rax
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%r14), %rcx
	movq	%rax, -200(%rbp)
	testb	$1, %cl
	jne	.L451
.L452:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L488:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L490
.L446:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateEE27trace_event_unique_atomic21(%rip)
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L451:
	movq	-1(%rcx), %rax
	cmpw	$63, 11(%rax)
	ja	.L452
	movq	-8(%r14), %rax
	leaq	-8(%r14), %rsi
	testb	$1, %al
	jne	.L491
.L453:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L489:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L492
.L448:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L449
	movq	(%rdi), %rax
	call	*8(%rax)
.L449:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L450
	movq	(%rdi), %rax
	call	*8(%rax)
.L450:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -184(%rbp)
	movq	%rax, -176(%rbp)
	leaq	-184(%rbp), %rax
	movq	%r13, -168(%rbp)
	movq	%rax, -192(%rbp)
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L491:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L453
	movq	-16(%r14), %r8
	testb	$1, %r8b
	jne	.L493
	movq	-24(%r14), %rdx
	sarq	$32, %r8
	leaq	-24(%r14), %r15
	testb	$1, %dl
	jne	.L494
.L456:
	leaq	.LC16(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L494:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L456
	movq	-32(%r14), %rbx
	testb	$1, %bl
	jne	.L495
	movl	11(%rax), %r11d
	sarq	$32, %rbx
	movq	%rsi, %r9
	cmpl	%r11d, %r8d
	je	.L460
	movl	%r8d, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -216(%rbp)
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	(%r14), %rcx
	movq	-216(%rbp), %r8
	movq	%rax, %r9
	movq	-8(%r14), %rax
	movq	-208(%rbp), %rsi
	movl	11(%rax), %r11d
.L460:
	movl	%r8d, %edx
	addl	11(%rcx), %edx
	je	.L462
	movl	%r11d, %ecx
	movq	%r12, %rdi
	movq	%r9, -208(%rbp)
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	-208(%rbp), %r9
	movq	%rax, %rsi
.L462:
	movq	%rsi, -136(%rbp)
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	16+_ZTVZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEE11SimpleMatch(%rip), %rax
	leaq	-160(%rbp), %rsi
	movq	%r14, -152(%rbp)
	movq	%rax, -160(%rbp)
	movq	%r9, -144(%rbp)
	call	_ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi@PLT
	testq	%rax, %rax
	je	.L496
	movq	(%rax), %r15
.L464:
	subl	$1, 41104(%r12)
	movq	-200(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L467
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L467:
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L497
.L442:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L498
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movq	312(%r12), %r15
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L487:
	movq	40960(%rsi), %rax
	movl	$525, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L493:
	leaq	.LC15(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L492:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L490:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L495:
	leaq	.LC17(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L497:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L442
.L498:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25370:
	.size	_ZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"V8.Runtime_Runtime_StringIndexOfUnchecked"
	.section	.text._ZN2v88internalL36Stats_Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateE.isra.0:
.LFB25371:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L529
.L500:
	movq	_ZZN2v88internalL36Stats_Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic206(%rip), %rbx
	testq	%rbx, %rbx
	je	.L530
.L502:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L531
.L504:
	movl	$0, %eax
	leaq	-8(%r13), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movslq	-12(%r13), %rcx
	testq	%rcx, %rcx
	cmovs	%rax, %rcx
	movq	0(%r13), %rax
	movl	11(%rax), %eax
	cmpl	%ecx, %eax
	cmovl	%eax, %ecx
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	salq	$32, %r13
	cmpq	41096(%r12), %rbx
	je	.L508
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L508:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L532
.L499:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L533
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L530:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L534
.L503:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic206(%rip)
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L531:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L535
.L505:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L506
	movq	(%rdi), %rax
	call	*8(%rax)
.L506:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L507
	movq	(%rdi), %rax
	call	*8(%rax)
.L507:
	leaq	.LC18(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L532:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L529:
	movq	40960(%rsi), %rax
	movl	$536, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L535:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC18(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L534:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L503
.L533:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25371:
	.size	_ZN2v88internalL36Stats_Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"V8.Runtime_Runtime_StringCharCodeAt"
	.section	.rodata._ZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC20:
	.string	"args[1].IsNumber()"
	.section	.text._ZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateE.isra.0:
.LFB25372:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L620
.L537:
	movq	_ZZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateEE28trace_event_unique_atomic254(%rip), %rbx
	testq	%rbx, %rbx
	je	.L621
.L539:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L622
.L541:
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L545
.L546:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L621:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L623
.L540:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateEE28trace_event_unique_atomic254(%rip)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L545:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L546
	movq	-8(%r14), %rdx
	testb	$1, %dl
	je	.L616
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L624
	movq	7(%rdx), %rdx
	movsd	.LC22(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC21(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L552
	movsd	.LC23(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L625
.L552:
	movabsq	$9218868437227405312, %rcx
	testq	%rcx, %rdx
	je	.L594
	movq	%rdx, %rsi
	xorl	%r13d, %r13d
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L626
	cmpl	$31, %ecx
	jg	.L551
	movabsq	$4503599627370495, %r13
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %r13
	addq	%rsi, %r13
	salq	%cl, %r13
	movl	%r13d, %r13d
.L558:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %r13d
.L551:
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	ja	.L561
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L627
.L561:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L619
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L572
.L619:
	movq	(%r14), %rsi
.L569:
	cmpl	%r13d, 11(%rsi)
	ja	.L576
	movq	1088(%r12), %r13
.L577:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L590
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L590:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L628
.L536:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L629
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L576:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L578
	leaq	.L580(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateE.isra.0,"a",@progbits
	.align 4
	.align 4
.L580:
	.long	.L586-.L580
	.long	.L583-.L580
	.long	.L585-.L580
	.long	.L581-.L580
	.long	.L578-.L580
	.long	.L579-.L580
	.long	.L578-.L580
	.long	.L578-.L580
	.long	.L584-.L580
	.long	.L583-.L580
	.long	.L582-.L580
	.long	.L581-.L580
	.long	.L578-.L580
	.long	.L579-.L580
	.section	.text._ZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L616:
	shrq	$32, %rdx
	movq	%rdx, %r13
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L622:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L630
.L542:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L543
	movq	(%rdi), %rax
	call	*8(%rax)
.L543:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L544
	movq	(%rdi), %rax
	call	*8(%rax)
.L544:
	leaq	.LC19(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L572:
	movq	(%r14), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L573
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L625:
	comisd	.LC24(%rip), %xmm0
	jb	.L552
	cvttsd2sil	%xmm0, %r13d
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%r13d, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L552
	je	.L551
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L579:
	movq	%rsi, -168(%rbp)
	leaq	-168(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal10ThinString3GetEi@PLT
	movzwl	%ax, %r13d
	.p2align 4,,10
	.p2align 3
.L587:
	salq	$32, %r13
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L581:
	movq	%rsi, -168(%rbp)
	leaq	-168(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	movzwl	%ax, %r13d
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L583:
	movq	%rsi, -168(%rbp)
	leaq	-168(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal10ConsString3GetEi@PLT
	movzwl	%ax, %r13d
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L585:
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r13d, %rdx
	movzwl	(%rax,%rdx,2), %r13d
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L586:
	leal	16(%r13,%r13), %eax
	cltq
	movzwl	-1(%rsi,%rax), %r13d
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L582:
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r13d, %rdx
	movzbl	(%rax,%rdx), %r13d
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L584:
	leal	16(%r13), %edx
	movslq	%edx, %rdx
	movzbl	-1(%rsi,%rdx), %r13d
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L626:
	cmpl	$-52, %ecx
	jl	.L551
	movabsq	$4503599627370495, %r13
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %r13
	addq	%rcx, %r13
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %r13
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L627:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L564
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L564
	movq	%r14, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L564:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L566
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L620:
	movq	40960(%rsi), %rax
	movl	$529, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L624:
	leaq	.LC20(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L573:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L631
.L575:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L628:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L630:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC19(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L623:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L540
.L578:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L566:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L632
.L568:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L631:
	movq	%r12, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	jmp	.L575
.L632:
	movq	%r12, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L568
.L594:
	xorl	%r13d, %r13d
	jmp	.L551
.L629:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25372:
	.size	_ZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"V8.Runtime_Runtime_StringToArray"
	.section	.text._ZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateE.isra.0:
.LFB25373:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L740
.L634:
	movq	_ZZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic379(%rip), %rbx
	testq	%rbx, %rbx
	je	.L741
.L636:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L742
.L638:
	movq	41088(%r13), %rax
	addl	$1, 41104(%r13)
	movq	(%r14), %rdx
	movq	%rax, -240(%rbp)
	movq	41096(%r13), %rax
	movq	%rax, -232(%rbp)
	testb	$1, %dl
	jne	.L642
.L643:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L741:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L743
.L637:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic379(%rip)
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L642:
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L643
	movq	-8(%r14), %rax
	testb	$1, %al
	je	.L734
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L744
	movq	7(%rax), %rax
	movsd	.LC22(%rip), %xmm2
	movq	%rax, %xmm1
	andpd	.LC21(%rip), %xmm1
	movq	%rax, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L649
	movsd	.LC23(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L745
.L649:
	movabsq	$9218868437227405312, %rcx
	testq	%rcx, %rax
	je	.L702
	movq	%rax, %rsi
	xorl	%ebx, %ebx
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L746
	cmpl	$31, %ecx
	jg	.L648
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rsi
	andq	%rax, %rbx
	addq	%rsi, %rbx
	salq	%cl, %rbx
	movl	%ebx, %ebx
.L655:
	sarq	$63, %rax
	orl	$1, %eax
	imull	%eax, %ebx
.L648:
	movq	-1(%rdx), %rax
	movq	%rdx, %rsi
	cmpw	$63, 11(%rax)
	ja	.L658
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	je	.L747
.L658:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L739
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L669
.L739:
	movq	(%r14), %rsi
.L666:
	cmpl	%ebx, 11(%rsi)
	cmovbe	11(%rsi), %ebx
	movl	%ebx, -184(%rbp)
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L673
	movq	23(%rsi), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L673
.L674:
	movl	-184(%rbp), %esi
	xorl	%edx, %edx
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -192(%rbp)
.L677:
	movslq	%ebx, %r12
	leaq	.L684(%rip), %r15
	cmpl	%ebx, -184(%rbp)
	jle	.L695
	.p2align 4,,10
	.p2align 3
.L696:
	movq	(%r14), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L682
	movzwl	%ax, %eax
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateE.isra.0,"a",@progbits
	.align 4
	.align 4
.L684:
	.long	.L690-.L684
	.long	.L687-.L684
	.long	.L689-.L684
	.long	.L685-.L684
	.long	.L682-.L684
	.long	.L683-.L684
	.long	.L682-.L684
	.long	.L682-.L684
	.long	.L688-.L684
	.long	.L687-.L684
	.long	.L686-.L684
	.long	.L685-.L684
	.long	.L682-.L684
	.long	.L683-.L684
	.section	.text._ZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L683:
	movl	%ebx, %esi
	leaq	-168(%rbp), %rdi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
	movzwl	%ax, %esi
	.p2align 4,,10
	.p2align 3
.L691:
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	movq	-192(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	leaq	15(%rdi,%r12,8), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L698
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -200(%rbp)
	testl	$262144, %eax
	je	.L693
	movq	%rdx, -224(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-200(%rbp), %rcx
	movq	-224(%rbp), %rdx
	movq	-216(%rbp), %rsi
	movq	-208(%rbp), %rdi
	movq	8(%rcx), %rax
.L693:
	testb	$24, %al
	je	.L698
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L698
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L698:
	addl	$1, %ebx
	addq	$1, %r12
	cmpl	%ebx, -184(%rbp)
	jne	.L696
.L695:
	movq	-192(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r13, %rdi
	movq	(%rsi), %rax
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r12
	movq	-240(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-232(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L681
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L681:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L748
.L633:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L749
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L685:
	.cfi_restore_state
	movl	%ebx, %esi
	leaq	-168(%rbp), %rdi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	movzwl	%ax, %esi
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L687:
	movl	%ebx, %esi
	leaq	-168(%rbp), %rdi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	movzwl	%ax, %esi
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L689:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r12,2), %esi
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L690:
	leal	16(%rbx,%rbx), %eax
	cltq
	movzwl	-1(%rdx,%rax), %esi
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L686:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%r12), %esi
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L688:
	leal	16(%rbx), %eax
	cltq
	movzbl	-1(%rdx,%rax), %esi
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L673:
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	testb	$8, 11(%rax)
	je	.L674
	movl	-184(%rbp), %esi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE@PLT
	leaq	-169(%rbp), %rsi
	leaq	-168(%rbp), %rdi
	movq	%rax, -192(%rbp)
	movq	(%r14), %rax
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	shrq	$32, %rdx
	movq	%rax, %rsi
	cmpl	$1, %edx
	jne	.L750
	movq	-192(%rbp), %rax
	movl	-184(%rbp), %ecx
	leaq	37592(%r13), %rdi
	movq	(%rax), %rdx
	call	_ZN2v88internalL29CopyCachedOneByteCharsToArrayEPNS0_4HeapEPKhNS0_10FixedArrayEi
	movl	%eax, %ebx
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L734:
	shrq	$32, %rax
	movq	%rax, %rbx
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L742:
	pxor	%xmm0, %xmm0
	xorl	%r12d, %r12d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L751
.L639:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L640
	movq	(%rdi), %rax
	call	*8(%rax)
.L640:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L641
	movq	(%rdi), %rax
	call	*8(%rax)
.L641:
	leaq	.LC25(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L669:
	movq	(%r14), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L670
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L745:
	comisd	.LC24(%rip), %xmm0
	jb	.L649
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L649
	je	.L648
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L746:
	cmpl	$-52, %ecx
	jl	.L648
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rcx
	andq	%rax, %rbx
	addq	%rcx, %rbx
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rbx
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L747:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L661
	movq	23(%rdx), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	je	.L661
	movq	%r14, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L661:
	movq	41112(%r13), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L663
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L740:
	movq	40960(%rsi), %rax
	movl	$544, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L744:
	leaq	.LC20(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L670:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L752
.L672:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L748:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L633
.L750:
	movq	-192(%rbp), %rbx
	movq	88(%r13), %rax
	movslq	-184(%rbp), %rcx
	movq	(%rbx), %rbx
	movq	%rbx, -200(%rbp)
	leaq	15(%rbx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	xorl	%ebx, %ebx
	jmp	.L677
.L743:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L637
.L751:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC25(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r12
	addq	$64, %rsp
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L682:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L663:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L753
.L665:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L658
.L752:
	movq	%r13, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L672
.L753:
	movq	%r13, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L665
.L702:
	xorl	%ebx, %ebx
	jmp	.L648
.L749:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25373:
	.size	_ZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC26:
	.string	"V8.Runtime_Runtime_StringCompareSequence"
	.section	.rodata._ZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC27:
	.string	"args[2].IsNumber()"
	.section	.text._ZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateE.isra.0:
.LFB25374:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -224(%rbp)
	movq	$0, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L855
.L755:
	movq	_ZZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateEE28trace_event_unique_atomic488(%rip), %rbx
	testq	%rbx, %rbx
	je	.L856
.L757:
	movq	$0, -256(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L857
.L759:
	movq	41088(%r12), %rax
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	%rax, -264(%rbp)
	movq	(%r15), %rax
	testb	$1, %al
	jne	.L763
.L764:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L856:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L858
.L758:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateEE28trace_event_unique_atomic488(%rip)
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L763:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L764
	movq	-8(%r15), %rdx
	leaq	-8(%r15), %r13
	testb	$1, %dl
	jne	.L859
.L765:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L857:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L860
.L760:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L761
	movq	(%rdi), %rax
	call	*8(%rax)
.L761:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L762
	movq	(%rdi), %rax
	call	*8(%rax)
.L762:
	leaq	.LC26(%rip), %rax
	movq	%rbx, -248(%rbp)
	movq	%rax, -240(%rbp)
	leaq	-248(%rbp), %rax
	movq	%r13, -232(%rbp)
	movq	%rax, -256(%rbp)
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L859:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L765
	movq	-16(%r15), %rbx
	testb	$1, %bl
	je	.L852
	movq	-1(%rbx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L861
	movq	7(%rbx), %rdx
	movsd	.LC22(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC21(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L862
.L772:
	movabsq	$9218868437227405312, %rcx
	testq	%rcx, %rdx
	je	.L827
	movq	%rdx, %rsi
	xorl	%ebx, %ebx
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L863
	cmpl	$31, %ecx
	jg	.L771
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %rbx
	addq	%rsi, %rbx
	salq	%cl, %rbx
	movl	%ebx, %ecx
.L777:
	movq	%rdx, %rbx
	sarq	$63, %rbx
	orl	$1, %ebx
	imull	%ecx, %ebx
.L771:
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	ja	.L780
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L864
.L780:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L788
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L865
.L788:
	movq	%r15, %rdx
	leaq	-176(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	jbe	.L794
	movq	%rdx, %rsi
	movq	%r13, %r8
.L795:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L803
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L866
.L803:
	leaq	-128(%rbp), %rdi
	movq	%r8, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	0(%r13), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L815
	cmpb	$0, -144(%rbp)
	movzbl	-96(%rbp), %esi
	movq	-136(%rbp), %rcx
	movq	-88(%rbp), %rdx
	jne	.L811
	testb	%sil, %sil
	jne	.L812
	movslq	%ebx, %rbx
	leal	-1(%rax), %edi
	xorl	%eax, %eax
	leaq	(%rcx,%rbx,2), %rsi
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L867:
	leaq	1(%rax), %rcx
	cmpq	%rax, %rdi
	je	.L815
	movq	%rcx, %rax
.L814:
	movzwl	(%rsi,%rax,2), %ebx
	cmpw	%bx, (%rdx,%rax,2)
	je	.L867
.L813:
	movq	120(%r12), %r13
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L815:
	movq	112(%r12), %r13
.L810:
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movq	-264(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L823
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L823:
	leaq	-256(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L868
.L754:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L869
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L852:
	.cfi_restore_state
	shrq	$32, %rbx
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L794:
	movq	-1(%rdx), %rax
	movq	%rdx, %rsi
	movq	%r13, %r8
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L795
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L798
	movq	23(%rdx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L798
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r8
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L811:
	testb	%sil, %sil
	movslq	%ebx, %rbx
	leal	-1(%rax), %esi
	movl	$0, %eax
	jne	.L817
	addq	%rcx, %rbx
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L870:
	leaq	1(%rax), %rcx
	cmpq	%rax, %rsi
	je	.L815
	movq	%rcx, %rax
.L818:
	movzbl	(%rbx,%rax), %ecx
	cmpw	%cx, (%rdx,%rax,2)
	je	.L870
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L862:
	movsd	.LC23(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L772
	comisd	.LC24(%rip), %xmm0
	jb	.L772
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L772
	je	.L771
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L817:
	addq	%rcx, %rbx
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L819:
	leaq	1(%rax), %rcx
	cmpq	%rsi, %rax
	je	.L815
	movq	%rcx, %rax
.L820:
	movzbl	(%rdx,%rax), %ecx
	cmpb	%cl, (%rbx,%rax)
	je	.L819
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L812:
	movslq	%ebx, %rbx
	leal	-1(%rax), %r8d
	xorl	%eax, %eax
	leaq	(%rcx,%rbx,2), %rdi
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L871:
	leaq	1(%rax), %rcx
	cmpq	%r8, %rax
	je	.L815
	movq	%rcx, %rax
.L816:
	movzbl	(%rdx,%rax), %esi
	movzwl	(%rdi,%rax,2), %ecx
	cmpl	%ecx, %esi
	je	.L871
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L866:
	movq	(%r8), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L806
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L865:
	movq	(%r15), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L791
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L798:
	movq	41112(%r12), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L800
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L863:
	cmpl	$-52, %ecx
	jl	.L771
	movabsq	$4503599627370495, %rcx
	movabsq	$4503599627370496, %rbx
	andq	%rdx, %rcx
	addq	%rcx, %rbx
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rbx
	movq	%rbx, %rcx
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L864:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L783
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L783
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L783:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L785
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L855:
	movq	40960(%rsi), %rax
	movl	$542, %edx
	leaq	-216(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -224(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L861:
	leaq	.LC27(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L791:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L872
.L793:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L806:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L873
.L808:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L868:
	leaq	-216(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L754
.L860:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC26(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L760
.L858:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L758
.L785:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L874
.L787:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L780
.L800:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L875
.L802:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L795
.L873:
	movq	%r12, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L808
.L872:
	movq	%r12, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L793
.L874:
	movq	%r12, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L787
.L875:
	movq	%r12, %rdi
	movq	%rsi, -272(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L802
.L827:
	xorl	%ebx, %ebx
	jmp	.L771
.L869:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25374:
	.size	_ZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL24Stats_Runtime_StringTrimEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC28:
	.string	"V8.Runtime_Runtime_StringTrim"
.LC29:
	.string	"args[1].IsSmi()"
	.section	.text._ZN2v88internalL24Stats_Runtime_StringTrimEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_StringTrimEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_StringTrimEiPmPNS0_7IsolateE.isra.0:
.LFB25376:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L907
.L877:
	movq	_ZZN2v88internalL24Stats_Runtime_StringTrimEiPmPNS0_7IsolateEE28trace_event_unique_atomic142(%rip), %rbx
	testq	%rbx, %rbx
	je	.L908
.L879:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L909
.L881:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	-8(%r13), %rdx
	testb	$1, %dl
	jne	.L910
	movq	%r13, %rsi
	sarq	$32, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L886
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L886:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L911
.L876:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L912
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L913
.L882:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L883
	movq	(%rdi), %rax
	call	*8(%rax)
.L883:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L884
	movq	(%rdi), %rax
	call	*8(%rax)
.L884:
	leaq	.LC28(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L908:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L914
.L880:
	movq	%rbx, _ZZN2v88internalL24Stats_Runtime_StringTrimEiPmPNS0_7IsolateEE28trace_event_unique_atomic142(%rip)
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L910:
	leaq	.LC29(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L907:
	movq	40960(%rsi), %rax
	movl	$545, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L911:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L914:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L913:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC28(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L882
.L912:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25376:
	.size	_ZN2v88internalL24Stats_Runtime_StringTrimEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_StringTrimEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"V8.Runtime_Runtime_StringSubstring"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC31:
	.string	"args[1].ToInt32(&start)"
.LC32:
	.string	"args[2].ToInt32(&end)"
	.section	.text._ZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateE.isra.0:
.LFB25377:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L964
.L916:
	movq	_ZZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateEE28trace_event_unique_atomic223(%rip), %rbx
	testq	%rbx, %rbx
	je	.L965
.L918:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L966
.L920:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L924
.L925:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L965:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L967
.L919:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateEE28trace_event_unique_atomic223(%rip)
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L924:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L925
	movq	-8(%r13), %rax
	testb	$1, %al
	je	.L927
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L968
.L927:
	leaq	-168(%rbp), %r15
	leaq	-176(%rbp), %rsi
	movl	$0, -176(%rbp)
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L969
	movq	-16(%r13), %rax
	testb	$1, %al
	je	.L931
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L970
.L931:
	leaq	-172(%rbp), %rsi
	movq	%r15, %rdi
	movl	$0, -172(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L971
	movq	40960(%r12), %r15
	cmpb	$0, 7104(%r15)
	je	.L934
	movq	7096(%r15), %rax
.L935:
	testq	%rax, %rax
	je	.L936
	addl	$1, (%rax)
.L936:
	movl	-176(%rbp), %edx
	movl	-172(%rbp), %ecx
	testl	%edx, %edx
	jne	.L937
	movq	0(%r13), %r15
	cmpl	11(%r15), %ecx
	je	.L938
.L937:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	(%rax), %r15
.L938:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L941
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L941:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L972
.L915:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L973
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L966:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L974
.L921:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L922
	movq	(%rdi), %rax
	call	*8(%rax)
.L922:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L923
	movq	(%rdi), %rax
	call	*8(%rax)
.L923:
	leaq	.LC30(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L934:
	movb	$1, 7104(%r15)
	leaq	7080(%r15), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7096(%r15)
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L964:
	movq	40960(%rsi), %rax
	movl	$543, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L968:
	leaq	.LC20(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L969:
	leaq	.LC31(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L970:
	leaq	.LC27(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L967:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L974:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC30(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L971:
	leaq	.LC32(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L972:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L915
.L973:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25377:
	.size	_ZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_InternalizeStringEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	"V8.Runtime_Runtime_InternalizeString"
	.section	.text._ZN2v88internalL31Stats_Runtime_InternalizeStringEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_InternalizeStringEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_InternalizeStringEiPmPNS0_7IsolateE.isra.0:
.LFB25378:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1012
.L976:
	movq	_ZZN2v88internalL31Stats_Runtime_InternalizeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic247(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1013
.L978:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1014
.L980:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L984
.L985:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1013:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1015
.L979:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_InternalizeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic247(%rip)
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L984:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L985
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L1016
.L986:
	movq	0(%r13), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L989
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L989:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1017
.L975:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1018
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1014:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1019
.L981:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L982
	movq	(%rdi), %rax
	call	*8(%rax)
.L982:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L983
	movq	(%rdi), %rax
	call	*8(%rax)
.L983:
	leaq	.LC33(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r13
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	40960(%rsi), %rax
	movl	$526, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L1017:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1015:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1019:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC33(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L981
.L1018:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25378:
	.size	_ZN2v88internalL31Stats_Runtime_InternalizeStringEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_InternalizeStringEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL27Stats_Runtime_FlattenStringEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"V8.Runtime_Runtime_FlattenString"
	.section	.text._ZN2v88internalL27Stats_Runtime_FlattenStringEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_FlattenStringEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_FlattenStringEiPmPNS0_7IsolateE.isra.0:
.LFB25379:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1070
.L1021:
	movq	_ZZN2v88internalL27Stats_Runtime_FlattenStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic476(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1071
.L1023:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1072
.L1025:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L1029
.L1030:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1071:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1073
.L1024:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_FlattenStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic476(%rip)
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1029:
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L1030
	movq	-1(%rdx), %rax
	movq	%rdx, %rsi
	cmpw	$63, 11(%rax)
	ja	.L1032
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	je	.L1074
.L1032:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L1069
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L1069
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %r13
	testq	%rdi, %rdi
	je	.L1044
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1069:
	movq	0(%r13), %r13
.L1040:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1049
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1049:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1075
.L1020:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1076
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1072:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1077
.L1026:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1027
	movq	(%rdi), %rax
	call	*8(%rax)
.L1027:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1028
	movq	(%rdi), %rax
	call	*8(%rax)
.L1028:
	leaq	.LC34(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1078
.L1046:
	movq	%r13, (%rax)
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1074:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L1035
	movq	23(%rdx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L1035
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L1040
	.p2align 4,,10
	.p2align 3
.L1035:
	movq	41112(%r12), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1037
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1070:
	movq	40960(%rsi), %rax
	movl	$524, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1075:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1073:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1077:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC34(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1037:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L1079
.L1039:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1078:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1046
.L1079:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1039
.L1076:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25379:
	.size	_ZN2v88internalL27Stats_Runtime_FlattenStringEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_FlattenStringEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"V8.Runtime_Runtime_StringIncludes"
	.section	.rodata._ZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC36:
	.string	"String.prototype.includes"
.LC37:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateE.isra.0:
.LFB25399:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1144
.L1081:
	movq	_ZZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateEE28trace_event_unique_atomic153(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1145
.L1083:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1146
.L1085:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	cmpq	104(%r12), %rax
	jne	.L1147
.L1089:
	leaq	.LC36(%rip), %rax
	leaq	-192(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	movq	$25, -184(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$27, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1098
.L1143:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L1095:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1112
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1112:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1148
.L1080:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1149
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1147:
	.cfi_restore_state
	cmpq	88(%r12), %rax
	je	.L1089
	testb	$1, %al
	jne	.L1090
.L1093:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1142
.L1092:
	leaq	-8(%r13), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal11RegExpUtils8IsRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	-200(%rbp), %r8
	testb	%al, %al
	je	.L1142
	shrw	$8, %ax
	jne	.L1150
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L1099
.L1102:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1142
.L1101:
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rsi
	testb	$1, %al
	jne	.L1103
	movq	(%r15), %rdx
.L1104:
	sarq	$32, %rax
	movl	$0, %ecx
	testq	%rax, %rax
	cmovg	%eax, %ecx
.L1107:
	cmpl	%ecx, 11(%rdx)
	cmovbe	11(%rdx), %ecx
	movq	%r15, %rsi
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	cmpl	$-1, %eax
	setne	%sil
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r13
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1146:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1151
.L1086:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1087
	movq	(%rdi), %rax
	call	*8(%rax)
.L1087:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1088
	movq	(%rdi), %rax
	call	*8(%rax)
.L1088:
	leaq	.LC35(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1145:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1152
.L1084:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateEE28trace_event_unique_atomic153(%rip)
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	40960(%rsi), %rax
	movl	$534, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1148:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	312(%r12), %r13
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1098:
	leaq	.LC37(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1152:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1151:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC35(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1090:
	movq	-1(%rax), %rax
	movq	%r13, %r15
	cmpw	$63, 11(%rax)
	jbe	.L1092
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1150:
	leaq	.LC36(%rip), %rax
	xorl	%edx, %edx
	leaq	-176(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -176(%rbp)
	movq	$25, -168(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1098
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$53, %esi
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1102
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1103:
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-200(%rbp), %r8
	testq	%rax, %rax
	je	.L1142
	movq	(%rax), %rax
	movq	(%r15), %rdx
	testb	$1, %al
	je	.L1104
	movsd	7(%rax), %xmm0
	xorl	%ecx, %ecx
	comisd	.LC38(%rip), %xmm0
	jb	.L1107
	movsd	.LC39(%rip), %xmm1
	movl	$-1, %ecx
	comisd	%xmm0, %xmm1
	jbe	.L1107
	cvttsd2siq	%xmm0, %rcx
	jmp	.L1107
.L1149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25399:
	.size	_ZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC40:
	.string	"V8.Runtime_Runtime_StringBuilderConcat"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC41:
	.string	"args[0].IsJSArray()"
.LC42:
	.string	"args[2].IsString()"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC46:
	.string	"TryNumberToSize(array->length(), &actual_array_length)"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateE.isra.0.str1.1
.LC47:
	.string	"array_length >= 0"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC48:
	.string	"static_cast<size_t>(array_length) <= actual_array_length"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateE.isra.0.str1.1
.LC49:
	.string	"array->HasFastElements()"
	.section	.text._ZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateE.isra.0:
.LFB25410:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1226
.L1154:
	movq	_ZZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateEE28trace_event_unique_atomic273(%rip), %r13
	testq	%r13, %r13
	je	.L1227
.L1156:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1228
.L1158:
	movq	41088(%r12), %rax
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	%rax, -184(%rbp)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L1229
.L1162:
	leaq	.LC41(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1227:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1230
.L1157:
	movq	%r13, _ZZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateEE28trace_event_unique_atomic273(%rip)
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1229:
	movq	-1(%rax), %rax
	cmpw	$1061, 11(%rax)
	jne	.L1162
	movq	-8(%rbx), %rax
	leaq	-168(%rbp), %r14
	leaq	-172(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	jne	.L1163
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r15
.L1165:
	subl	$1, 41104(%r12)
	movq	-184(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1192
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1192:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1231
.L1153:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1232
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1163:
	.cfi_restore_state
	movq	-16(%rbx), %rax
	testb	$1, %al
	jne	.L1233
.L1166:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1228:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1234
.L1159:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1160
	movq	(%rdi), %rax
	call	*8(%rax)
.L1160:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1161
	movq	(%rdi), %rax
	call	*8(%rax)
.L1161:
	leaq	.LC40(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1233:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1166
	movq	(%rbx), %rdi
	movq	23(%rdi), %rax
	testb	$1, %al
	jne	.L1235
	sarq	$32, %rax
	js	.L1169
.L1170:
	movslq	-172(%rbp), %rdx
	testl	%edx, %edx
	js	.L1236
	cmpq	%rax, %rdx
	ja	.L1237
	movq	-1(%rdi), %rax
	cmpb	$47, 14(%rax)
	ja	.L1238
	call	_ZN2v88internal8JSObject16ValidateElementsES1_@PLT
	movq	(%rbx), %rdi
	movq	-1(%rdi), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	leal	-2(%rax), %edx
	cmpb	$1, %dl
	jbe	.L1178
	cmpb	$5, %al
	ja	.L1179
	testb	$1, %al
	jne	.L1239
.L1179:
	movq	%rbx, %rdi
	movl	$2, %esi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	(%rbx), %rdi
.L1178:
	movq	-16(%rbx), %rdx
	movl	11(%rdx), %r8d
	movq	-1(%rdi), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$2, %eax
	cmpb	$1, %al
	ja	.L1225
	movq	-1(%rdx), %rax
	movl	-172(%rbp), %edx
	movzwl	11(%rax), %eax
	shrw	$3, %ax
	andl	$1, %eax
	movb	%al, -168(%rbp)
	movq	15(%rdi), %rsi
	movslq	11(%rsi), %rax
	cmpl	%edx, %eax
	jge	.L1181
	movl	%eax, -172(%rbp)
	movl	%eax, %edx
.L1181:
	testl	%edx, %edx
	je	.L1224
	cmpl	$1, %edx
	je	.L1240
.L1183:
	movq	%r14, %rcx
	movl	%r8d, %edi
	call	_ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb@PLT
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L1225
	testl	%eax, %eax
	je	.L1224
	xorl	%edx, %edx
	cmpb	$0, -168(%rbp)
	movq	%r12, %rdi
	je	.L1187
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1223
	movq	(%rbx), %rax
	movq	-16(%rbx), %rdi
	movl	-172(%rbp), %ecx
	movq	15(%rax), %rdx
	movq	(%r14), %rax
	leaq	15(%rax), %rsi
	call	_ZN2v88internal25StringBuilderConcatHelperIhEEvNS0_6StringEPT_NS0_10FixedArrayEi@PLT
	movq	(%r14), %r15
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	128(%r12), %r15
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1235:
	movsd	7(%rax), %xmm0
	comisd	.LC43(%rip), %xmm0
	jb	.L1169
	movsd	.LC44(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L1241
.L1169:
	leaq	.LC46(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1225:
	movq	2680(%r12), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r15
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1226:
	movq	40960(%rsi), %rax
	movl	$528, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1241:
	movsd	.LC45(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jb	.L1242
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	btcq	$63, %rax
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1231:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1230:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1234:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC40(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1242:
	cvttsd2siq	%xmm0, %rax
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1239:
	movq	%rbx, %rdi
	movl	$3, %esi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	(%rbx), %rdi
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1236:
	leaq	.LC47(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1237:
	leaq	.LC48(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1240:
	movq	15(%rsi), %r15
	testb	$1, %r15b
	jne	.L1184
.L1222:
	movl	-172(%rbp), %edx
	jmp	.L1183
	.p2align 4,,10
	.p2align 3
.L1238:
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1187:
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1223
	movq	(%rbx), %rax
	movq	-16(%rbx), %rdi
	movl	-172(%rbp), %ecx
	movq	15(%rax), %rdx
	movq	(%r14), %rax
	leaq	15(%rax), %rsi
	call	_ZN2v88internal25StringBuilderConcatHelperItEEvNS0_6StringEPT_NS0_10FixedArrayEi@PLT
	movq	(%r14), %r15
	jmp	.L1165
.L1184:
	movq	-1(%r15), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1165
	jmp	.L1222
.L1223:
	movq	312(%r12), %r15
	jmp	.L1165
.L1232:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25410:
	.size	_ZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal23Runtime_GetSubstitutionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_GetSubstitutionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_GetSubstitutionEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_GetSubstitutionEiPmPNS0_7IsolateE:
.LFB20117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1264
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %r13
	movq	%rax, -104(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1246
.L1247:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1246:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1247
	movq	-8(%r14), %rcx
	leaq	-8(%rsi), %rsi
	testb	$1, %cl
	jne	.L1265
.L1248:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	-1(%rcx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1248
	movq	-16(%r14), %r9
	testb	$1, %r9b
	jne	.L1266
	movq	-24(%r14), %rdx
	sarq	$32, %r9
	leaq	-24(%r14), %r15
	testb	$1, %dl
	jne	.L1267
.L1251:
	leaq	.LC16(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1267:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1251
	movq	-32(%r14), %rbx
	testb	$1, %bl
	jne	.L1268
	movl	11(%rcx), %ecx
	sarq	$32, %rbx
	movq	%rsi, %r10
	cmpl	%ecx, %r9d
	je	.L1255
	movl	%r9d, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r9, -120(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %rsi
	movq	%rax, %r10
	movq	-8(%r14), %rax
	movl	11(%rax), %ecx
	movq	(%r14), %rax
.L1255:
	movl	%r9d, %edx
	addl	11(%rax), %edx
	je	.L1257
	movq	%r12, %rdi
	movq	%r10, -112(%rbp)
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	-112(%rbp), %r10
	movq	%rax, %rsi
.L1257:
	movq	%rsi, -72(%rbp)
	leaq	16+_ZTVZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEE11SimpleMatch(%rip), %rax
	movl	%ebx, %ecx
	movq	%r15, %rdx
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	%r14, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	_ZN2v88internal6String15GetSubstitutionEPNS0_7IsolateEPNS1_5MatchENS0_6HandleIS1_EEi@PLT
	testq	%rax, %rax
	je	.L1269
	movq	(%rax), %r15
.L1259:
	subl	$1, 41104(%r12)
	movq	-104(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1243
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1243:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1270
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1264:
	.cfi_restore_state
	movq	%rdx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r15
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1269:
	movq	312(%r12), %r15
	jmp	.L1259
	.p2align 4,,10
	.p2align 3
.L1266:
	leaq	.LC15(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1268:
	leaq	.LC17(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20117:
	.size	_ZN2v88internal23Runtime_GetSubstitutionEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_GetSubstitutionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30StringReplaceOneCharWithStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEES5_S5_Pbi,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30StringReplaceOneCharWithStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEES5_S5_Pbi
	.type	_ZN2v88internal30StringReplaceOneCharWithStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEES5_S5_Pbi, @function
_ZN2v88internal30StringReplaceOneCharWithStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEES5_S5_Pbi:
.LFB20136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$40, %rsp
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	37528(%r15), %rax
	jb	.L1296
	testl	%ebx, %ebx
	jne	.L1298
.L1296:
	xorl	%eax, %eax
.L1275:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1298:
	.cfi_restore_state
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L1276
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L1276
	movq	41112(%r15), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1277
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, %r11
.L1278:
	movq	41112(%r15), %rdi
	movq	23(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1280
	movq	%r11, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %r11
	movq	%rax, %r10
.L1281:
	subl	$1, %ebx
	movq	-56(%rbp), %rcx
	movq	%r11, %rsi
	movq	%r14, %r8
	movl	%ebx, %r9d
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%r10, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	_ZN2v88internal30StringReplaceOneCharWithStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEES5_S5_Pbi
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L1296
	cmpb	$0, (%r14)
	movq	%r10, %rdx
	jne	.L1297
	movq	-56(%rbp), %rcx
	movq	%r13, %rdx
	movl	%ebx, %r9d
	movq	%r14, %r8
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%r11, -64(%rbp)
	call	_ZN2v88internal30StringReplaceOneCharWithStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEES5_S5_Pbi
	movq	-64(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L1296
	cmpb	$0, (%r14)
	movq	%r12, %rax
	movq	%r11, %rsi
	je	.L1275
.L1297:
	addq	$40, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
	.p2align 4,,10
	.p2align 3
.L1276:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	movl	%eax, %ebx
	movq	%r12, %rax
	cmpl	$-1, %ebx
	je	.L1275
	movb	$1, (%r14)
	movq	(%r12), %rax
	movq	%r12, %rsi
	cmpl	11(%rax), %ebx
	je	.L1291
	movl	%ebx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %rsi
.L1291:
	movq	-56(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1296
	movq	(%r12), %rax
	addl	$1, %ebx
	movl	%ebx, %edx
	movl	11(%rax), %ecx
	je	.L1299
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %rdx
.L1294:
	movq	%r13, %rsi
	jmp	.L1297
	.p2align 4,,10
	.p2align 3
.L1299:
	movq	%r12, %rdx
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1277:
	movq	41088(%r15), %r11
	cmpq	41096(%r15), %r11
	je	.L1300
.L1279:
	leaq	8(%r11), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r11)
	jmp	.L1278
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	41088(%r15), %r10
	cmpq	41096(%r15), %r10
	je	.L1301
.L1282:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r10)
	jmp	.L1281
.L1300:
	movq	%r15, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r11
	jmp	.L1279
.L1301:
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %r11
	movq	%rax, %r10
	jmp	.L1282
	.cfi_endproc
.LFE20136:
	.size	_ZN2v88internal30StringReplaceOneCharWithStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEES5_S5_Pbi, .-_ZN2v88internal30StringReplaceOneCharWithStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEES5_S5_Pbi
	.section	.rodata._ZN2v88internalL44Stats_Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC50:
	.string	"V8.Runtime_Runtime_StringReplaceOneCharWithString"
	.section	.text._ZN2v88internalL44Stats_Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Stats_Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL44Stats_Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateE.isra.0:
.LFB25375:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1363
.L1303:
	movq	_ZZN2v88internalL44Stats_Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic112(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1364
.L1305:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1365
.L1307:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1311
.L1312:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1364:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1366
.L1306:
	movq	%rbx, _ZZN2v88internalL44Stats_Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic112(%rip)
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1311:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1312
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r15
	testb	$1, %al
	jne	.L1367
.L1313:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1365:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1368
.L1308:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1309
	movq	(%rdi), %rax
	call	*8(%rax)
.L1309:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1310
	movq	(%rdi), %rax
	call	*8(%rax)
.L1310:
	leaq	.LC50(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1367:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1313
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rcx
	testb	$1, %al
	jne	.L1369
.L1315:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1369:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1315
	leaq	-161(%rbp), %r8
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$4096, %r9d
	movq	%r8, -192(%rbp)
	movq	%rcx, -184(%rbp)
	movb	$0, -161(%rbp)
	call	_ZN2v88internal30StringReplaceOneCharWithStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEES5_S5_Pbi
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %r8
	testq	%rax, %rax
	je	.L1370
.L1336:
	movq	(%rax), %r13
.L1320:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1341
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1341:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1371
.L1302:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1372
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1370:
	.cfi_restore_state
	movq	12480(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1337
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	jbe	.L1373
.L1322:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1374
.L1330:
	movl	$4096, %r9d
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal30StringReplaceOneCharWithStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEES5_S5_Pbi
	testq	%rax, %rax
	jne	.L1336
	movq	12480(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1337
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	%rax, %r13
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1363:
	movq	40960(%rsi), %rax
	movl	$541, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1371:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1337:
	movq	312(%r12), %r13
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1368:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC50(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1366:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1373:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1322
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1325
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L1325
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -192(%rbp)
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %r8
	movq	%rax, %r13
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1374:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L1330
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1333
	movq	%r8, -192(%rbp)
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %r8
	movq	%rax, %r13
	jmp	.L1330
.L1325:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1327
	movq	%r8, -192(%rbp)
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %r8
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L1322
.L1333:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L1375
.L1335:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L1330
.L1327:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L1376
.L1329:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L1322
.L1375:
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L1335
.L1376:
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rcx, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L1329
.L1372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25375:
	.size	_ZN2v88internalL44Stats_Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL44Stats_Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal38Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateE:
.LFB20141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1414
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1380
.L1381:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1380:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1381
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r15
	testb	$1, %al
	jne	.L1415
.L1382:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1382
	movq	-16(%rsi), %rax
	leaq	-16(%rsi), %rcx
	testb	$1, %al
	jne	.L1416
.L1384:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1416:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1384
	leaq	-57(%rbp), %r8
	movl	$4096, %r9d
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movb	$0, -57(%rbp)
	call	_ZN2v88internal30StringReplaceOneCharWithStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEES5_S5_Pbi
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	testq	%rax, %rax
	je	.L1417
.L1405:
	movq	(%rax), %r13
.L1389:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1377
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1377:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1418
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1417:
	.cfi_restore_state
	movq	12480(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1406
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	jbe	.L1419
.L1391:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1420
.L1399:
	movl	$4096, %r9d
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal30StringReplaceOneCharWithStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEES5_S5_Pbi
	testq	%rax, %rax
	jne	.L1405
	movq	12480(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1406
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate13StackOverflowEv@PLT
	movq	%rax, %r13
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL44Stats_Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1406:
	movq	312(%r12), %r13
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1391
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1394
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L1394
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	%rax, %r13
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1420:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L1399
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1402
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	%rax, %r13
	jmp	.L1399
.L1394:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1396
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L1391
.L1402:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L1421
.L1404:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L1399
.L1396:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L1422
.L1398:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L1391
.L1421:
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L1404
.L1422:
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rcx
	movq	%rax, %r13
	jmp	.L1398
.L1418:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20141:
	.size	_ZN2v88internal38Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateE, .-_ZN2v88internal38Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Runtime_StringTrimEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_StringTrimEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_StringTrimEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_StringTrimEiPmPNS0_7IsolateE:
.LFB20144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1428
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	-8(%rsi), %rdx
	testb	$1, %dl
	jne	.L1429
	sarq	$32, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal6String4TrimEPNS0_7IsolateENS0_6HandleIS1_EENS1_8TrimModeE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1423
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1423:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1428:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL24Stats_Runtime_StringTrimEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1429:
	.cfi_restore_state
	leaq	.LC29(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20144:
	.size	_ZN2v88internal18Runtime_StringTrimEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_StringTrimEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_StringIncludesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_StringIncludesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_StringIncludesEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_StringIncludesEiPmPNS0_7IsolateE:
.LFB20147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r15d
	testl	%r15d, %r15d
	jne	.L1467
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rax
	cmpq	104(%rdx), %rax
	jne	.L1468
.L1433:
	leaq	.LC36(%rip), %rax
	leaq	-96(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	movq	$25, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$27, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1442
.L1466:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L1439:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1430
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rax
.L1430:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1469
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1468:
	.cfi_restore_state
	cmpq	88(%rdx), %rax
	je	.L1433
	testb	$1, %al
	jne	.L1434
.L1437:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1465
.L1436:
	leaq	-8(%r13), %r9
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	movq	%r9, %rsi
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal11RegExpUtils8IsRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %r8
	testb	%al, %al
	je	.L1465
	shrw	$8, %ax
	jne	.L1470
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L1443
.L1446:
	movq	%r9, %rsi
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-104(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L1465
.L1445:
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rsi
	testb	$1, %al
	jne	.L1447
	movq	(%r8), %rdx
.L1448:
	sarq	$32, %rax
	testq	%rax, %rax
	cmovg	%eax, %r15d
.L1451:
	cmpl	%r15d, 11(%rdx)
	movl	%r15d, %ecx
	cmovbe	11(%rdx), %ecx
	movq	%r8, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	cmpl	$-1, %eax
	setne	%sil
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %rax
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1467:
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateE.isra.0
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1465:
	movq	312(%r12), %rax
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1442:
	leaq	.LC37(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1470:
	leaq	.LC36(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$25, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1442
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$53, %esi
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1434:
	movq	-1(%rax), %rax
	movq	%rsi, %r8
	cmpw	$63, 11(%rax)
	jbe	.L1436
	jmp	.L1437
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1446
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1447:
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %r8
	testq	%rax, %rax
	je	.L1465
	movq	(%rax), %rax
	movq	(%r8), %rdx
	testb	$1, %al
	je	.L1448
	movsd	7(%rax), %xmm0
	comisd	.LC38(%rip), %xmm0
	jb	.L1451
	movsd	.LC39(%rip), %xmm1
	movl	$-1, %r15d
	comisd	%xmm0, %xmm1
	jbe	.L1451
	cvttsd2siq	%xmm0, %r15
	jmp	.L1451
.L1469:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20147:
	.size	_ZN2v88internal22Runtime_StringIncludesEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_StringIncludesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_StringIndexOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_StringIndexOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_StringIndexOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_StringIndexOfEiPmPNS0_7IsolateE:
.LFB20150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1475
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	leaq	-16(%rsi), %rcx
	movq	%r12, %rdi
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %rdx
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1473
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1473:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1475:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL27Stats_Runtime_StringIndexOfEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20150:
	.size	_ZN2v88internal21Runtime_StringIndexOfEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_StringIndexOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateE:
.LFB20153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1480
	addl	$1, 41104(%rdx)
	movslq	-12(%rsi), %rcx
	movl	$0, %eax
	movq	%r12, %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %rdx
	testq	%rcx, %rcx
	cmovs	%rax, %rcx
	movq	(%rsi), %rax
	movl	11(%rax), %eax
	cmpl	%ecx, %eax
	cmovl	%eax, %ecx
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	salq	$32, %r13
	cmpq	41096(%r12), %rbx
	je	.L1476
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1476:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1480:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Stats_Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20153:
	.size	_ZN2v88internal30Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_StringLastIndexOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_StringLastIndexOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_StringLastIndexOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_StringLastIndexOfEiPmPNS0_7IsolateE:
.LFB20156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1485
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	leaq	88(%rdx), %rcx
	movq	%r12, %rdi
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %rdx
	call	_ZN2v88internal6String11LastIndexOfEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L1483
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1483:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1485:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_StringLastIndexOfEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20156:
	.size	_ZN2v88internal25Runtime_StringLastIndexOfEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_StringLastIndexOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_StringSubstringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_StringSubstringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_StringSubstringEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_StringSubstringEiPmPNS0_7IsolateE:
.LFB20159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1511
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1489
.L1490:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1489:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1490
	movq	-8(%rsi), %rax
	testb	$1, %al
	je	.L1492
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1512
.L1492:
	leaq	-64(%rbp), %r15
	leaq	-72(%rbp), %rsi
	movl	$0, -72(%rbp)
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L1513
	movq	-16(%r13), %rax
	testb	$1, %al
	je	.L1496
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1514
.L1496:
	leaq	-68(%rbp), %rsi
	movq	%r15, %rdi
	movl	$0, -68(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L1515
	movq	40960(%r12), %r15
	cmpb	$0, 7104(%r15)
	je	.L1499
	movq	7096(%r15), %rax
.L1500:
	testq	%rax, %rax
	je	.L1501
	addl	$1, (%rax)
.L1501:
	movl	-72(%rbp), %edx
	movl	-68(%rbp), %ecx
	testl	%edx, %edx
	jne	.L1502
	movq	0(%r13), %r15
	cmpl	11(%r15), %ecx
	je	.L1503
.L1502:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	(%rax), %r15
.L1503:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1486
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1486:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1516
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1499:
	.cfi_restore_state
	movb	$1, 7104(%r15)
	leaq	7080(%r15), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7096(%r15)
	jmp	.L1500
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r15
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1512:
	leaq	.LC20(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1513:
	leaq	.LC31(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1514:
	leaq	.LC27(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1515:
	leaq	.LC32(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
.L1516:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20159:
	.size	_ZN2v88internal23Runtime_StringSubstringEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_StringSubstringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal17Runtime_StringAddEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Runtime_StringAddEiPmPNS0_7IsolateE
	.type	_ZN2v88internal17Runtime_StringAddEiPmPNS0_7IsolateE, @function
_ZN2v88internal17Runtime_StringAddEiPmPNS0_7IsolateE:
.LFB20162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1534
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1519
.L1520:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1519:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1520
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L1535
.L1521:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1535:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1521
	movq	40960(%r12), %r15
	cmpb	$0, 7072(%r15)
	je	.L1523
	movq	7064(%r15), %rax
.L1524:
	testq	%rax, %rax
	je	.L1525
	addl	$1, (%rax)
.L1525:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_@PLT
	testq	%rax, %rax
	je	.L1536
	movq	(%rax), %r13
.L1527:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1517
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1517:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1523:
	.cfi_restore_state
	movb	$1, 7072(%r15)
	leaq	7048(%r15), %rdi
	movq	%rdx, -56(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, 7064(%r15)
	jmp	.L1524
	.p2align 4,,10
	.p2align 3
.L1536:
	movq	312(%r12), %r13
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1534:
	addq	$24, %rsp
	movq	%r13, %rdi
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL23Stats_Runtime_StringAddEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20162:
	.size	_ZN2v88internal17Runtime_StringAddEiPmPNS0_7IsolateE, .-_ZN2v88internal17Runtime_StringAddEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_InternalizeStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_InternalizeStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_InternalizeStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_InternalizeStringEiPmPNS0_7IsolateE:
.LFB20165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1548
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1539
.L1540:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1539:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1540
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L1549
.L1541:
	movq	(%r8), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1537
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1537:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1549:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r8
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1548:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_InternalizeStringEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20165:
	.size	_ZN2v88internal25Runtime_InternalizeStringEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_InternalizeStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_StringCharCodeAtEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_StringCharCodeAtEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_StringCharCodeAtEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_StringCharCodeAtEiPmPNS0_7IsolateE:
.LFB20168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r13d
	testl	%r13d, %r13d
	jne	.L1610
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1553
.L1554:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1554
	movq	-8(%rsi), %rdx
	testb	$1, %dl
	je	.L1606
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L1611
	movq	7(%rdx), %rdx
	movsd	.LC22(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC21(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L1612
.L1560:
	movabsq	$9218868437227405312, %rcx
	testq	%rcx, %rdx
	je	.L1559
	movq	%rdx, %rsi
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L1613
	cmpl	$31, %ecx
	jg	.L1559
	movabsq	$4503599627370495, %r13
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %r13
	addq	%rsi, %r13
	salq	%cl, %r13
	movl	%r13d, %r13d
.L1566:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %r13d
.L1559:
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	ja	.L1569
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L1614
.L1569:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L1609
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L1580
.L1609:
	movq	(%rdi), %rsi
.L1577:
	cmpl	%r13d, 11(%rsi)
	ja	.L1584
	movq	1088(%r12), %r13
.L1585:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1550
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1550:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1615
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1584:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L1586
	leaq	.L1588(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal24Runtime_StringCharCodeAtEiPmPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L1588:
	.long	.L1594-.L1588
	.long	.L1591-.L1588
	.long	.L1593-.L1588
	.long	.L1589-.L1588
	.long	.L1586-.L1588
	.long	.L1587-.L1588
	.long	.L1586-.L1588
	.long	.L1586-.L1588
	.long	.L1592-.L1588
	.long	.L1591-.L1588
	.long	.L1590-.L1588
	.long	.L1589-.L1588
	.long	.L1586-.L1588
	.long	.L1587-.L1588
	.section	.text._ZN2v88internal24Runtime_StringCharCodeAtEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1606:
	shrq	$32, %rdx
	movq	%rdx, %r13
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1612:
	movsd	.LC23(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1560
	comisd	.LC24(%rip), %xmm0
	jb	.L1560
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1560
	jne	.L1560
	movl	%ecx, %r13d
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1580:
	movq	(%rdi), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1581
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1587:
	movq	%rsi, -48(%rbp)
	leaq	-48(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal10ThinString3GetEi@PLT
	movzwl	%ax, %r13d
	.p2align 4,,10
	.p2align 3
.L1595:
	salq	$32, %r13
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	%rsi, -48(%rbp)
	leaq	-48(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	movzwl	%ax, %r13d
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	%rsi, -48(%rbp)
	leaq	-48(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal10ConsString3GetEi@PLT
	movzwl	%ax, %r13d
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1594:
	leal	16(%r13,%r13), %eax
	cltq
	movzwl	-1(%rsi,%rax), %r13d
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1592:
	addl	$16, %r13d
	movslq	%r13d, %r13
	movzbl	-1(%rsi,%r13), %r13d
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1593:
	movq	15(%rsi), %rdi
	movslq	%r13d, %r13
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r13,2), %r13d
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1590:
	movq	15(%rsi), %rdi
	movslq	%r13d, %r13
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%r13), %r13d
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1613:
	cmpl	$-52, %ecx
	jl	.L1559
	movabsq	$4503599627370495, %r13
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %r13
	addq	%rcx, %r13
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %r13
	jmp	.L1566
	.p2align 4,,10
	.p2align 3
.L1614:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1572
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L1572
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1572:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1574
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdi
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1610:
	movq	%rdx, %rsi
	call	_ZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L1550
	.p2align 4,,10
	.p2align 3
.L1611:
	leaq	.LC20(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1581:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1616
.L1583:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1577
.L1586:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1574:
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L1617
.L1576:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1583
.L1617:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1576
.L1615:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20168:
	.size	_ZN2v88internal24Runtime_StringCharCodeAtEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_StringCharCodeAtEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_StringBuilderConcatEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_StringBuilderConcatEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_StringBuilderConcatEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_StringBuilderConcatEiPmPNS0_7IsolateE:
.LFB20171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1667
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1668
.L1621:
	leaq	.LC41(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1668:
	movq	-1(%rax), %rax
	cmpw	$1061, 11(%rax)
	jne	.L1621
	movq	-8(%rsi), %rax
	leaq	-64(%rbp), %r14
	leaq	-68(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	jne	.L1622
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L1624:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1618
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rax
.L1618:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1669
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1622:
	.cfi_restore_state
	movq	-16(%r13), %rax
	testb	$1, %al
	jne	.L1670
.L1625:
	leaq	.LC42(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1670:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1625
	movq	0(%r13), %rdi
	movq	23(%rdi), %rax
	testb	$1, %al
	jne	.L1671
	sarq	$32, %rax
	js	.L1628
.L1629:
	movslq	-68(%rbp), %rdx
	testl	%edx, %edx
	js	.L1672
	cmpq	%rax, %rdx
	ja	.L1673
	movq	-1(%rdi), %rax
	cmpb	$47, 14(%rax)
	ja	.L1674
	call	_ZN2v88internal8JSObject16ValidateElementsES1_@PLT
	movq	0(%r13), %rdi
	movq	-1(%rdi), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	leal	-2(%rax), %edx
	cmpb	$1, %dl
	jbe	.L1637
	cmpb	$5, %al
	ja	.L1638
	testb	$1, %al
	jne	.L1675
.L1638:
	movq	%r13, %rdi
	movl	$2, %esi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	0(%r13), %rdi
.L1637:
	movq	-16(%r13), %rdx
	movl	11(%rdx), %r8d
	movq	-1(%rdi), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$2, %eax
	cmpb	$1, %al
	ja	.L1664
	movq	-1(%rdx), %rax
	movl	-68(%rbp), %edx
	movzwl	11(%rax), %eax
	shrw	$3, %ax
	andl	$1, %eax
	movb	%al, -64(%rbp)
	movq	15(%rdi), %rsi
	movslq	11(%rsi), %rax
	cmpl	%edx, 11(%rsi)
	jge	.L1640
	movl	%eax, -68(%rbp)
	movl	%eax, %edx
.L1640:
	testl	%edx, %edx
	je	.L1666
	cmpl	$1, %edx
	je	.L1676
.L1642:
	movq	%r14, %rcx
	movl	%r8d, %edi
	call	_ZN2v88internal25StringBuilderConcatLengthEiNS0_10FixedArrayEiPb@PLT
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L1664
	testl	%eax, %eax
	je	.L1666
	xorl	%edx, %edx
	cmpb	$0, -64(%rbp)
	movq	%r12, %rdi
	je	.L1646
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1665
	movq	0(%r13), %rax
	movq	-16(%r13), %rdi
	movl	-68(%rbp), %ecx
	movq	15(%rax), %rdx
	movq	(%r14), %rax
	leaq	15(%rax), %rsi
	call	_ZN2v88internal25StringBuilderConcatHelperIhEEvNS0_6StringEPT_NS0_10FixedArrayEi@PLT
	movq	(%r14), %rax
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1666:
	movq	128(%r12), %rax
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1671:
	movsd	7(%rax), %xmm0
	comisd	.LC43(%rip), %xmm0
	jb	.L1628
	movsd	.LC44(%rip), %xmm1
	comisd	%xmm0, %xmm1
	ja	.L1677
.L1628:
	leaq	.LC46(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1664:
	movq	2680(%r12), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1667:
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateE.isra.0
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1677:
	movsd	.LC45(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jb	.L1678
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	btcq	$63, %rax
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1678:
	cvttsd2siq	%xmm0, %rax
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1675:
	movq	%r13, %rdi
	movl	$3, %esi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	0(%r13), %rdi
	jmp	.L1637
	.p2align 4,,10
	.p2align 3
.L1672:
	leaq	.LC47(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1673:
	leaq	.LC48(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1676:
	movq	15(%rsi), %rax
	testb	$1, %al
	jne	.L1643
.L1663:
	movl	-68(%rbp), %edx
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1674:
	leaq	.LC49(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1646:
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1665
	movq	0(%r13), %rax
	movq	-16(%r13), %rdi
	movl	-68(%rbp), %ecx
	movq	15(%rax), %rdx
	movq	(%r14), %rax
	leaq	15(%rax), %rsi
	call	_ZN2v88internal25StringBuilderConcatHelperItEEvNS0_6StringEPT_NS0_10FixedArrayEi@PLT
	movq	(%r14), %rax
	jmp	.L1624
.L1643:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1624
	jmp	.L1663
.L1665:
	movq	312(%r12), %rax
	jmp	.L1624
.L1669:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20171:
	.size	_ZN2v88internal27Runtime_StringBuilderConcatEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_StringBuilderConcatEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_StringToArrayEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_StringToArrayEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_StringToArrayEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_StringToArrayEiPmPNS0_7IsolateE:
.LFB20175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %ebx
	testl	%ebx, %ebx
	jne	.L1762
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -144(%rbp)
	movq	41096(%rdx), %rax
	movq	(%rsi), %rdx
	movq	%rax, -136(%rbp)
	testb	$1, %dl
	jne	.L1682
.L1683:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1682:
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	ja	.L1683
	movq	-8(%rsi), %rax
	testb	$1, %al
	je	.L1756
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L1763
	movq	7(%rax), %rsi
	movsd	.LC22(%rip), %xmm2
	movq	%rsi, %xmm1
	andpd	.LC21(%rip), %xmm1
	movq	%rsi, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L1764
.L1689:
	movabsq	$9218868437227405312, %rax
	testq	%rax, %rsi
	je	.L1688
	movq	%rsi, %rdi
	shrq	$52, %rdi
	andl	$2047, %edi
	movl	%edi, %ecx
	subl	$1075, %ecx
	js	.L1765
	cmpl	$31, %ecx
	jg	.L1688
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rdi
	andq	%rsi, %rax
	addq	%rdi, %rax
	salq	%cl, %rax
	movl	%eax, %eax
.L1695:
	movq	%rsi, %rbx
	sarq	$63, %rbx
	orl	$1, %ebx
	imull	%eax, %ebx
.L1688:
	movq	-1(%rdx), %rax
	movq	%rdx, %rsi
	cmpw	$63, 11(%rax)
	ja	.L1698
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	je	.L1766
.L1698:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L1761
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L1709
.L1761:
	movq	(%r14), %rsi
.L1706:
	cmpl	%ebx, 11(%rsi)
	cmovbe	11(%rsi), %ebx
	movl	%ebx, -88(%rbp)
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L1713
	movq	23(%rsi), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L1713
.L1714:
	movl	-88(%rbp), %esi
	xorl	%edx, %edx
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movq	%rax, -96(%rbp)
.L1717:
	movslq	%ebx, %r12
	leaq	.L1724(%rip), %r15
	cmpl	%ebx, -88(%rbp)
	jle	.L1735
	.p2align 4,,10
	.p2align 3
.L1736:
	movq	(%r14), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L1722
	movzwl	%ax, %eax
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal21Runtime_StringToArrayEiPmPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L1724:
	.long	.L1730-.L1724
	.long	.L1727-.L1724
	.long	.L1729-.L1724
	.long	.L1725-.L1724
	.long	.L1722-.L1724
	.long	.L1723-.L1724
	.long	.L1722-.L1724
	.long	.L1722-.L1724
	.long	.L1728-.L1724
	.long	.L1727-.L1724
	.long	.L1726-.L1724
	.long	.L1725-.L1724
	.long	.L1722-.L1724
	.long	.L1723-.L1724
	.section	.text._ZN2v88internal21Runtime_StringToArrayEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L1723:
	movl	%ebx, %esi
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
	movzwl	%ax, %esi
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	movq	-96(%rbp), %rcx
	movq	(%rax), %rdx
	movq	(%rcx), %rdi
	leaq	15(%rdi,%r12,8), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L1737
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -104(%rbp)
	testl	$262144, %eax
	je	.L1733
	movq	%rdx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	8(%rcx), %rax
.L1733:
	testb	$24, %al
	je	.L1737
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1737
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L1737:
	addl	$1, %ebx
	addq	$1, %r12
	cmpl	%ebx, -88(%rbp)
	jne	.L1736
.L1735:
	movq	-96(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r13, %rdi
	movq	(%rsi), %rax
	movslq	11(%rax), %rcx
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r12
	movq	-144(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-136(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L1679
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1679:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1767
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1725:
	.cfi_restore_state
	movl	%ebx, %esi
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	movzwl	%ax, %esi
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1727:
	movl	%ebx, %esi
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	movzwl	%ax, %esi
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1729:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r12,2), %esi
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1730:
	leal	16(%rbx,%rbx), %eax
	cltq
	movzwl	-1(%rdx,%rax), %esi
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1726:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzbl	(%rax,%r12), %esi
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1728:
	leal	16(%rbx), %eax
	cltq
	movzbl	-1(%rdx,%rax), %esi
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	testb	$8, 11(%rax)
	je	.L1714
	movl	-88(%rbp), %esi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE@PLT
	leaq	-65(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%rax, -96(%rbp)
	movq	(%r14), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String14GetFlatContentERKNS0_29PerThreadAssertScopeDebugOnlyILNS0_19PerThreadAssertTypeE0ELb0EEE@PLT
	shrq	$32, %rdx
	movq	%rax, %rsi
	cmpl	$1, %edx
	jne	.L1768
	movq	-96(%rbp), %rax
	movl	-88(%rbp), %ecx
	leaq	37592(%r13), %rdi
	movq	(%rax), %rdx
	call	_ZN2v88internalL29CopyCachedOneByteCharsToArrayEPNS0_4HeapEPKhNS0_10FixedArrayEi
	movl	%eax, %ebx
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1756:
	shrq	$32, %rax
	movq	%rax, %rbx
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1764:
	movsd	.LC23(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1689
	comisd	.LC24(%rip), %xmm0
	jb	.L1689
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1689
	jne	.L1689
	movl	%eax, %ebx
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1709:
	movq	(%r14), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1710
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1765:
	cmpl	$-52, %ecx
	jl	.L1688
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rcx
	andq	%rsi, %rax
	addq	%rcx, %rax
	movl	$1075, %ecx
	subl	%edi, %ecx
	shrq	%cl, %rax
	jmp	.L1695
	.p2align 4,,10
	.p2align 3
.L1766:
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L1701
	movq	23(%rdx), %rax
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	je	.L1701
	movq	%r14, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1701:
	movq	41112(%r13), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1703
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1762:
	movq	%rdx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r12
	jmp	.L1679
	.p2align 4,,10
	.p2align 3
.L1763:
	leaq	.LC20(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1710:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L1769
.L1712:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L1706
.L1768:
	movq	-96(%rbp), %rbx
	movq	88(%r13), %rax
	movslq	-88(%rbp), %rcx
	movq	(%rbx), %rbx
	movq	%rbx, -104(%rbp)
	leaq	15(%rbx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	xorl	%ebx, %ebx
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1722:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1703:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L1770
.L1705:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L1698
.L1769:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1712
.L1770:
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1705
.L1767:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20175:
	.size	_ZN2v88internal21Runtime_StringToArrayEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_StringToArrayEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_StringLessThanEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_StringLessThanEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_StringLessThanEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_StringLessThanEiPmPNS0_7IsolateE:
.LFB20179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1782
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1773
.L1774:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1773:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1774
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L1783
.L1775:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1783:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1775
	movq	%r12, %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	movl	$18, %edi
	movl	%eax, %esi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	testb	%al, %al
	je	.L1777
	movq	112(%r12), %r14
.L1778:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1771
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1771:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1777:
	.cfi_restore_state
	movq	120(%r12), %r14
	jmp	.L1778
	.p2align 4,,10
	.p2align 3
.L1782:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20179:
	.size	_ZN2v88internal22Runtime_StringLessThanEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_StringLessThanEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateE:
.LFB20182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1795
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1786
.L1787:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1786:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1787
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L1796
.L1788:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1796:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1788
	movq	%r12, %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	movl	$19, %edi
	movl	%eax, %esi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	testb	%al, %al
	je	.L1790
	movq	112(%r12), %r14
.L1791:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1784
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1784:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1790:
	.cfi_restore_state
	movq	120(%r12), %r14
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1795:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL35Stats_Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20182:
	.size	_ZN2v88internal29Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_StringGreaterThanEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_StringGreaterThanEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_StringGreaterThanEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_StringGreaterThanEiPmPNS0_7IsolateE:
.LFB20185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1808
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1799
.L1800:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1799:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1800
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L1809
.L1801:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1809:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1801
	movq	%r12, %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	movl	$20, %edi
	movl	%eax, %esi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	testb	%al, %al
	je	.L1803
	movq	112(%r12), %r14
.L1804:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1797
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1797:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1803:
	.cfi_restore_state
	movq	120(%r12), %r14
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1808:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_StringGreaterThanEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20185:
	.size	_ZN2v88internal25Runtime_StringGreaterThanEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_StringGreaterThanEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateE:
.LFB20188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1821
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1812
.L1813:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1812:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1813
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L1822
.L1814:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1822:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1814
	movq	%r12, %rdi
	call	_ZN2v88internal6String7CompareEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	movl	$21, %edi
	movl	%eax, %esi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	testb	%al, %al
	je	.L1816
	movq	112(%r12), %r14
.L1817:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1810
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1810:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1816:
	.cfi_restore_state
	movq	120(%r12), %r14
	jmp	.L1817
	.p2align 4,,10
	.p2align 3
.L1821:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL38Stats_Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20188:
	.size	_ZN2v88internal32Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Runtime_StringEqualEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_StringEqualEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_StringEqualEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_StringEqualEiPmPNS0_7IsolateE:
.LFB20191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1838
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1825
.L1826:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1825:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1826
	movq	-8(%rsi), %rdx
	leaq	-8(%rsi), %r8
	testb	$1, %dl
	jne	.L1839
.L1827:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1839:
	movq	-1(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L1827
	cmpq	-8(%rsi), %rax
	je	.L1831
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L1833
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	je	.L1832
.L1833:
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	jne	.L1831
.L1832:
	movq	120(%r12), %r14
.L1834:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1823
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1823:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1831:
	.cfi_restore_state
	movq	112(%r12), %r14
	jmp	.L1834
	.p2align 4,,10
	.p2align 3
.L1838:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL25Stats_Runtime_StringEqualEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20191:
	.size	_ZN2v88internal19Runtime_StringEqualEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_StringEqualEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_FlattenStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_FlattenStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_FlattenStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_FlattenStringEiPmPNS0_7IsolateE:
.LFB20194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1864
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L1842
.L1843:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1842:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1843
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	ja	.L1845
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L1865
.L1845:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L1863
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L1856
.L1863:
	movq	(%rdi), %r13
.L1853:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1840
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1840:
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1856:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %r13
	testq	%rdi, %rdi
	je	.L1857
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	jmp	.L1853
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1848
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L1848
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L1853
	.p2align 4,,10
	.p2align 3
.L1848:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1850
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdi
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1864:
	addq	$16, %rsp
	movq	%rdx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL27Stats_Runtime_FlattenStringEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L1857:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1866
.L1859:
	movq	%r13, (%rax)
	jmp	.L1853
	.p2align 4,,10
	.p2align 3
.L1850:
	movq	41088(%r12), %rdi
	cmpq	41096(%r12), %rdi
	je	.L1867
.L1852:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdi)
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1866:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1859
.L1867:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdi
	jmp	.L1852
	.cfi_endproc
.LFE20194:
	.size	_ZN2v88internal21Runtime_FlattenStringEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_FlattenStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_StringMaxLengthEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_StringMaxLengthEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_StringMaxLengthEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_StringMaxLengthEiPmPNS0_7IsolateE:
.LFB20197:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1870
	movabsq	$4611685911053205504, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1870:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL29Stats_Runtime_StringMaxLengthEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE20197:
	.size	_ZN2v88internal23Runtime_StringMaxLengthEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_StringMaxLengthEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_StringCompareSequenceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_StringCompareSequenceEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_StringCompareSequenceEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_StringCompareSequenceEiPmPNS0_7IsolateE:
.LFB20200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1948
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %r14
	testb	$1, %al
	jne	.L1874
.L1875:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1874:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1875
	movq	-8(%rsi), %rdx
	leaq	-8(%rsi), %r13
	testb	$1, %dl
	jne	.L1949
.L1876:
	leaq	.LC6(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1949:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1876
	movq	-16(%rsi), %rbx
	testb	$1, %bl
	je	.L1945
	movq	-1(%rbx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L1950
	movq	7(%rbx), %rdx
	movsd	.LC22(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC21(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L1883
	movsd	.LC23(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1883
	comisd	.LC24(%rip), %xmm0
	jb	.L1883
	cvttsd2sil	%xmm0, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1883
	je	.L1882
	.p2align 4,,10
	.p2align 3
.L1883:
	movabsq	$9218868437227405312, %rcx
	testq	%rcx, %rdx
	je	.L1935
	movq	%rdx, %rsi
	xorl	%ebx, %ebx
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L1951
	cmpl	$31, %ecx
	jg	.L1882
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %rbx
	addq	%rsi, %rbx
	salq	%cl, %rbx
	movl	%ebx, %ebx
.L1888:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %ebx
	jmp	.L1882
	.p2align 4,,10
	.p2align 3
.L1945:
	shrq	$32, %rbx
.L1882:
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	ja	.L1891
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L1952
.L1891:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L1899
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L1953
.L1899:
	movq	%r8, %rdx
	leaq	-160(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1905
	movq	%rdx, %rsi
	movq	%r13, %r8
.L1906:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L1914
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L1954
.L1914:
	leaq	-112(%rbp), %rdi
	movq	%r8, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal16FlatStringReaderC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	0(%r13), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	jle	.L1926
	cmpb	$0, -128(%rbp)
	movzbl	-80(%rbp), %esi
	movq	-72(%rbp), %rdx
	movq	-120(%rbp), %rcx
	jne	.L1922
	testb	%sil, %sil
	jne	.L1923
	movslq	%ebx, %rbx
	leal	-1(%rax), %edi
	xorl	%eax, %eax
	leaq	(%rcx,%rbx,2), %rsi
	jmp	.L1925
	.p2align 4,,10
	.p2align 3
.L1955:
	leaq	1(%rax), %rcx
	cmpq	%rax, %rdi
	je	.L1926
	movq	%rcx, %rax
.L1925:
	movzwl	(%rsi,%rax,2), %ebx
	cmpw	%bx, (%rdx,%rax,2)
	je	.L1955
.L1924:
	movq	120(%r12), %r13
	jmp	.L1921
	.p2align 4,,10
	.p2align 3
.L1926:
	movq	112(%r12), %r13
.L1921:
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movq	-152(%rbp), %rax
	movq	-144(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L1871
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1871:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1956
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1905:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	movq	%rdx, %rsi
	movq	%r13, %r8
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L1906
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L1909
	movq	23(%rdx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L1909
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r8
	jmp	.L1914
	.p2align 4,,10
	.p2align 3
.L1922:
	testb	%sil, %sil
	jne	.L1928
	movslq	%ebx, %rbx
	leal	-1(%rax), %edi
	xorl	%eax, %eax
	addq	%rcx, %rbx
	jmp	.L1929
	.p2align 4,,10
	.p2align 3
.L1957:
	leaq	1(%rax), %rcx
	cmpq	%rdi, %rax
	je	.L1926
	movq	%rcx, %rax
.L1929:
	movzbl	(%rbx,%rax), %esi
	movzwl	(%rdx,%rax,2), %ecx
	cmpl	%ecx, %esi
	je	.L1957
	jmp	.L1924
	.p2align 4,,10
	.p2align 3
.L1928:
	movslq	%ebx, %rbx
	leal	-1(%rax), %esi
	xorl	%eax, %eax
	addq	%rcx, %rbx
	jmp	.L1931
	.p2align 4,,10
	.p2align 3
.L1930:
	leaq	1(%rax), %rcx
	cmpq	%rax, %rsi
	je	.L1926
	movq	%rcx, %rax
.L1931:
	movzbl	(%rbx,%rax), %edi
	cmpb	%dil, (%rdx,%rax)
	je	.L1930
	jmp	.L1924
	.p2align 4,,10
	.p2align 3
.L1923:
	movslq	%ebx, %rbx
	leal	-1(%rax), %r8d
	xorl	%eax, %eax
	leaq	(%rcx,%rbx,2), %rdi
	jmp	.L1927
	.p2align 4,,10
	.p2align 3
.L1958:
	leaq	1(%rax), %rcx
	cmpq	%r8, %rax
	je	.L1926
	movq	%rcx, %rax
.L1927:
	movzbl	(%rdx,%rax), %esi
	movzwl	(%rdi,%rax,2), %ecx
	cmpl	%ecx, %esi
	je	.L1958
	jmp	.L1924
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	(%r8), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1902
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L1954:
	movq	(%r8), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1917
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	jmp	.L1914
	.p2align 4,,10
	.p2align 3
.L1951:
	cmpl	$-52, %ecx
	jl	.L1882
	movabsq	$4503599627370495, %rbx
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rbx
	addq	%rcx, %rbx
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rbx
	jmp	.L1888
	.p2align 4,,10
	.p2align 3
.L1909:
	movq	41112(%r12), %rdi
	movq	15(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L1911
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
	jmp	.L1906
	.p2align 4,,10
	.p2align 3
.L1952:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L1894
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L1894
	movq	%r8, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r8
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L1894:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1896
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
	jmp	.L1891
	.p2align 4,,10
	.p2align 3
.L1948:
	movq	%rdx, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L1950:
	leaq	.LC27(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1902:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L1959
.L1904:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L1917:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L1960
.L1919:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1914
.L1896:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L1961
.L1898:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1891
.L1911:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L1962
.L1913:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1906
.L1960:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1919
.L1959:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1904
.L1961:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1898
.L1962:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1913
.L1935:
	xorl	%ebx, %ebx
	jmp	.L1882
.L1956:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20200:
	.size	_ZN2v88internal29Runtime_StringCompareSequenceEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_StringCompareSequenceEiPmPNS0_7IsolateE
	.section	.rodata._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC51:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.type	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, @function
_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_:
.LFB23611:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L1977
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1973
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1978
.L1965:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1972:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1979
	testq	%r13, %r13
	jg	.L1968
	testq	%r9, %r9
	jne	.L1971
.L1969:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1979:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1968
.L1971:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1969
	.p2align 4,,10
	.p2align 3
.L1978:
	testq	%rsi, %rsi
	jne	.L1966
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1972
	.p2align 4,,10
	.p2align 3
.L1968:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1969
	jmp	.L1971
	.p2align 4,,10
	.p2align 3
.L1973:
	movl	$4, %r14d
	jmp	.L1965
.L1977:
	leaq	.LC51(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1966:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L1965
	.cfi_endproc
.LFE23611:
	.size	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_, .-_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	.section	.rodata._ZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC52:
	.string	"V8.Runtime_Runtime_StringEscapeQuotes"
	.section	.rodata._ZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC53:
	.string	"&quot;"
	.section	.text._ZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE.isra.0:
.LFB25380:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -176(%rbp)
	movq	$0, -144(%rbp)
	movaps	%xmm0, -160(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2044
.L1981:
	movq	_ZZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateEE28trace_event_unique_atomic511(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2045
.L1983:
	movq	$0, -208(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2046
.L1985:
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movq	%rax, -312(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -304(%rbp)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1989
.L1990:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2045:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2047
.L1984:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateEE28trace_event_unique_atomic511(%rip)
	jmp	.L1983
	.p2align 4,,10
	.p2align 3
.L1989:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1990
	movl	11(%rax), %eax
	movl	$34, %esi
	movq	%r12, %rdi
	movl	%eax, -276(%rbp)
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %rbx
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	movl	%eax, -260(%rbp)
	movl	%eax, %r14d
	cmpl	$-1, %eax
	je	.L2048
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	call	_Znwm@PLT
	leaq	4(%rax), %rdx
	movq	%rax, -240(%rbp)
	movl	%r14d, (%rax)
	movl	-260(%rbp), %eax
	movq	%rdx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	leal	1(%rax), %ecx
	cmpl	-276(%rbp), %ecx
	jge	.L1993
	leaq	-260(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L1994:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	movl	%eax, -260(%rbp)
	cmpl	$-1, %eax
	je	.L1993
	movq	-232(%rbp), %rsi
	cmpq	-224(%rbp), %rsi
	je	.L1995
	movl	%eax, (%rsi)
	movl	-260(%rbp), %eax
	addq	$4, -232(%rbp)
	leal	1(%rax), %ecx
	cmpl	-276(%rbp), %ecx
	jl	.L1994
.L1993:
	leaq	.LC53(%rip), %rax
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -256(%rbp)
	movq	$6, -248(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2008
	movq	-232(%rbp), %rax
	leaq	-128(%rbp), %r14
	subq	-240(%rbp), %rax
	movq	%r13, %rdx
	sarq	$2, %rax
	leaq	37592(%r12), %rsi
	movq	%r14, %rdi
	leal	1(%rax,%rax), %ecx
	call	_ZN2v88internal24ReplacementStringBuilderC1EPNS0_4HeapENS0_6HandleINS0_6StringEEEi@PLT
	movq	-232(%rbp), %rax
	movq	-240(%rbp), %rdx
	movq	%rax, -288(%rbp)
	cmpq	%rdx, %rax
	je	.L2016
	movq	%r12, -320(%rbp)
	movq	%rdx, %r15
	movl	$-1, %r13d
	movq	%rbx, %r12
	jmp	.L2003
	.p2align 4,,10
	.p2align 3
.L1999:
	movq	%r12, %rsi
	movq	%r14, %rdi
	addq	$4, %r15
	call	_ZN2v88internal24ReplacementStringBuilder9AddStringENS0_6HandleINS0_6StringEEE@PLT
	cmpq	%r15, -288(%rbp)
	je	.L2049
.L2003:
	movl	%r13d, %ebx
	movl	(%r15), %r13d
	addl	$1, %ebx
	cmpl	%ebx, %r13d
	jle	.L1999
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi@PLT
	movl	%r13d, %ecx
	subl	%ebx, %ecx
	testl	$-2048, %ecx
	jne	.L2000
	testl	$-524288, %ebx
	je	.L2050
.L2000:
	movl	%ebx, %esi
	leaq	-120(%rbp), %rdi
	movl	%ecx, -280(%rbp)
	subl	%r13d, %esi
	movq	%rdi, -296(%rbp)
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movq	-296(%rbp), %rdi
	movq	%rbx, %rsi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movl	-280(%rbp), %ecx
.L2001:
	movl	-96(%rbp), %esi
	movl	$1073741799, %edi
	subl	%ecx, %edi
	addl	%esi, %ecx
	cmpl	%edi, %esi
	movl	$2147483647, %esi
	cmovg	%esi, %ecx
	movl	%ecx, -96(%rbp)
	jmp	.L1999
	.p2align 4,,10
	.p2align 3
.L1995:
	leaq	-240(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movl	-260(%rbp), %eax
	leal	1(%rax), %ecx
	cmpl	%ecx, -276(%rbp)
	jg	.L1994
	jmp	.L1993
	.p2align 4,,10
	.p2align 3
.L2049:
	movq	-320(%rbp), %r12
.L1998:
	movl	-276(%rbp), %eax
	subl	$1, %eax
	cmpl	%r13d, %eax
	jg	.L2051
.L2004:
	movq	%r14, %rdi
	call	_ZN2v88internal24ReplacementStringBuilder8ToStringEv@PLT
	testq	%rax, %rax
	je	.L2008
	movq	-240(%rbp), %rdi
	movq	(%rax), %r13
	testq	%rdi, %rdi
	je	.L1992
	call	_ZdlPv@PLT
.L1992:
	subl	$1, 41104(%r12)
	movq	-312(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-304(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L2012
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2012:
	leaq	-208(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2052
.L1980:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2053
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2046:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2054
.L1986:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1987
	movq	(%rdi), %rax
	call	*8(%rax)
.L1987:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1988
	movq	(%rdi), %rax
	call	*8(%rax)
.L1988:
	leaq	.LC52(%rip), %rax
	movq	%rbx, -200(%rbp)
	movq	%rax, -192(%rbp)
	leaq	-200(%rbp), %rax
	movq	%r14, -184(%rbp)
	movq	%rax, -208(%rbp)
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L2050:
	sall	$11, %ebx
	leaq	-120(%rbp), %rdi
	movl	%ecx, -296(%rbp)
	movl	%ebx, %esi
	orl	%ecx, %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movl	-296(%rbp), %ecx
	jmp	.L2001
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	0(%r13), %r13
	jmp	.L1992
	.p2align 4,,10
	.p2align 3
.L2051:
	movl	$2, %esi
	movq	%r14, %rdi
	leal	1(%r13), %ebx
	call	_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi@PLT
	movl	-276(%rbp), %r13d
	subl	%ebx, %r13d
	testl	$-2048, %r13d
	jne	.L2005
	testl	$-524288, %ebx
	je	.L2055
.L2005:
	leaq	-120(%rbp), %r15
	movl	%ebx, %esi
	subl	-276(%rbp), %esi
	salq	$32, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
.L2006:
	movl	-96(%rbp), %edx
	movl	$1073741799, %eax
	subl	%r13d, %eax
	addl	%edx, %r13d
	cmpl	%eax, %edx
	movl	$2147483647, %eax
	cmovg	%eax, %r13d
	movl	%r13d, -96(%rbp)
	jmp	.L2004
	.p2align 4,,10
	.p2align 3
.L2008:
	leaq	.LC37(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2044:
	movq	40960(%rsi), %rax
	movl	$531, %edx
	leaq	-168(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -176(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1981
	.p2align 4,,10
	.p2align 3
.L2052:
	leaq	-168(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1980
	.p2align 4,,10
	.p2align 3
.L2054:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC52(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L2047:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L2016:
	movl	$-1, %r13d
	jmp	.L1998
.L2055:
	sall	$11, %ebx
	leaq	-120(%rbp), %rdi
	movl	%ebx, %esi
	orl	%r13d, %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	jmp	.L2006
.L2053:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25380:
	.size	_ZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal26Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE:
.LFB20203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2096
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -200(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -192(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2059
.L2060:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2059:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2060
	movl	11(%rax), %eax
	movl	$34, %esi
	movq	%r12, %rdi
	movl	%eax, -164(%rbp)
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %rbx
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	movl	%eax, -148(%rbp)
	movl	%eax, %r14d
	cmpl	$-1, %eax
	je	.L2097
	pxor	%xmm0, %xmm0
	movl	$4, %edi
	movq	$0, -112(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	_Znwm@PLT
	leaq	4(%rax), %rdx
	movq	%rax, -128(%rbp)
	movl	%r14d, (%rax)
	movl	-148(%rbp), %eax
	movq	%rdx, -112(%rbp)
	movq	%rdx, -120(%rbp)
	leal	1(%rax), %ecx
	cmpl	-164(%rbp), %ecx
	jge	.L2063
	leaq	-148(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L2064:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6String7IndexOfEPNS0_7IsolateENS0_6HandleIS1_EES5_i@PLT
	movl	%eax, -148(%rbp)
	cmpl	$-1, %eax
	je	.L2063
	movq	-120(%rbp), %rsi
	cmpq	-112(%rbp), %rsi
	je	.L2065
	movl	%eax, (%rsi)
	movl	-148(%rbp), %eax
	addq	$4, -120(%rbp)
	leal	1(%rax), %ecx
	cmpl	-164(%rbp), %ecx
	jl	.L2064
.L2063:
	leaq	.LC53(%rip), %rax
	leaq	-144(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	movq	$6, -136(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2078
	movq	-120(%rbp), %rax
	subq	-128(%rbp), %rax
	leaq	-96(%rbp), %r14
	movq	%r13, %rdx
	sarq	$2, %rax
	leaq	37592(%r12), %rsi
	movq	%r14, %rdi
	leal	1(%rax,%rax), %ecx
	call	_ZN2v88internal24ReplacementStringBuilderC1EPNS0_4HeapENS0_6HandleINS0_6StringEEEi@PLT
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rax, -176(%rbp)
	cmpq	%rdx, %rax
	je	.L2083
	movq	%r12, -208(%rbp)
	movq	%rdx, %r15
	movl	$-1, %r13d
	movq	%rbx, %r12
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2069:
	movq	%r12, %rsi
	movq	%r14, %rdi
	addq	$4, %r15
	call	_ZN2v88internal24ReplacementStringBuilder9AddStringENS0_6HandleINS0_6StringEEE@PLT
	cmpq	%r15, -176(%rbp)
	je	.L2098
.L2073:
	movl	%r13d, %ebx
	movl	(%r15), %r13d
	addl	$1, %ebx
	cmpl	%ebx, %r13d
	jle	.L2069
	movl	$2, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi@PLT
	movl	%r13d, %ecx
	subl	%ebx, %ecx
	testl	$-2048, %ecx
	jne	.L2070
	testl	$-524288, %ebx
	je	.L2099
.L2070:
	movl	%ebx, %esi
	leaq	-88(%rbp), %rdi
	movl	%ecx, -168(%rbp)
	subl	%r13d, %esi
	movq	%rdi, -184(%rbp)
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movq	-184(%rbp), %rdi
	movq	%rbx, %rsi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movl	-168(%rbp), %ecx
.L2071:
	movl	-64(%rbp), %esi
	movl	$1073741799, %edi
	subl	%ecx, %edi
	addl	%esi, %ecx
	cmpl	%edi, %esi
	movl	$2147483647, %esi
	cmovg	%esi, %ecx
	movl	%ecx, -64(%rbp)
	jmp	.L2069
	.p2align 4,,10
	.p2align 3
.L2065:
	leaq	-128(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIiSaIiEE17_M_realloc_insertIJRiEEEvN9__gnu_cxx17__normal_iteratorIPiS1_EEDpOT_
	movl	-148(%rbp), %eax
	leal	1(%rax), %ecx
	cmpl	%ecx, -164(%rbp)
	jg	.L2064
	jmp	.L2063
	.p2align 4,,10
	.p2align 3
.L2098:
	movq	-208(%rbp), %r12
.L2068:
	movl	-164(%rbp), %eax
	subl	$1, %eax
	cmpl	%r13d, %eax
	jg	.L2100
.L2074:
	movq	%r14, %rdi
	call	_ZN2v88internal24ReplacementStringBuilder8ToStringEv@PLT
	testq	%rax, %rax
	je	.L2078
	movq	-128(%rbp), %rdi
	movq	(%rax), %r13
	testq	%rdi, %rdi
	je	.L2062
	call	_ZdlPv@PLT
.L2062:
	subl	$1, 41104(%r12)
	movq	-200(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-192(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L2056
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2056:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2101
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2099:
	.cfi_restore_state
	sall	$11, %ebx
	leaq	-88(%rbp), %rdi
	movl	%ecx, -184(%rbp)
	movl	%ebx, %esi
	orl	%ecx, %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movl	-184(%rbp), %ecx
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2097:
	movq	0(%r13), %r13
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2100:
	movl	$2, %esi
	movq	%r14, %rdi
	leal	1(%r13), %ebx
	call	_ZN2v88internal24ReplacementStringBuilder14EnsureCapacityEi@PLT
	movl	-164(%rbp), %r13d
	subl	%ebx, %r13d
	testl	$-2048, %r13d
	jne	.L2075
	testl	$-524288, %ebx
	je	.L2102
.L2075:
	leaq	-88(%rbp), %r15
	movl	%ebx, %esi
	subl	-164(%rbp), %esi
	salq	$32, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
.L2076:
	movl	-64(%rbp), %edx
	movl	$1073741799, %eax
	subl	%r13d, %eax
	addl	%edx, %r13d
	cmpl	%eax, %edx
	movl	$2147483647, %eax
	cmovg	%eax, %r13d
	movl	%r13d, -64(%rbp)
	jmp	.L2074
	.p2align 4,,10
	.p2align 3
.L2078:
	leaq	.LC37(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2096:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L2056
	.p2align 4,,10
	.p2align 3
.L2083:
	movl	$-1, %r13d
	jmp	.L2068
.L2102:
	sall	$11, %ebx
	leaq	-88(%rbp), %rdi
	movl	%ebx, %esi
	orl	%r13d, %esi
	salq	$32, %rsi
	call	_ZN2v88internal17FixedArrayBuilder3AddENS0_3SmiE@PLT
	jmp	.L2076
.L2101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20203:
	.size	_ZN2v88internal26Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_StringEscapeQuotesEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal23Runtime_GetSubstitutionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal23Runtime_GetSubstitutionEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal23Runtime_GetSubstitutionEiPmPNS0_7IsolateE:
.LFB25324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25324:
	.size	_GLOBAL__sub_I__ZN2v88internal23Runtime_GetSubstitutionEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal23Runtime_GetSubstitutionEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal23Runtime_GetSubstitutionEiPmPNS0_7IsolateE
	.section	.data.rel.ro.local._ZTVZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEE11SimpleMatch,"aw"
	.align 8
	.type	_ZTVZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEE11SimpleMatch, @object
	.size	_ZTVZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEE11SimpleMatch, 88
_ZTVZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEE11SimpleMatch:
	.quad	0
	.quad	0
	.quad	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch8GetMatchEv
	.quad	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch9GetPrefixEv
	.quad	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch9GetSuffixEv
	.quad	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch12CaptureCountEv
	.quad	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch16HasNamedCapturesEv
	.quad	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch10GetCaptureEiPb
	.quad	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatch15GetNamedCaptureENS0_6HandleINS0_6StringEEEPNS6_5Match12CaptureStateE
	.quad	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD1Ev
	.quad	_ZZN2v88internalL33__RT_impl_Runtime_GetSubstitutionENS0_9ArgumentsEPNS0_7IsolateEEN11SimpleMatchD0Ev
	.section	.bss._ZZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateEE28trace_event_unique_atomic511,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateEE28trace_event_unique_atomic511, @object
	.size	_ZZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateEE28trace_event_unique_atomic511, 8
_ZZN2v88internalL32Stats_Runtime_StringEscapeQuotesEiPmPNS0_7IsolateEE28trace_event_unique_atomic511:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateEE28trace_event_unique_atomic488,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateEE28trace_event_unique_atomic488, @object
	.size	_ZZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateEE28trace_event_unique_atomic488, 8
_ZZN2v88internalL35Stats_Runtime_StringCompareSequenceEiPmPNS0_7IsolateEE28trace_event_unique_atomic488:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_StringMaxLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic483,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_StringMaxLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic483, @object
	.size	_ZZN2v88internalL29Stats_Runtime_StringMaxLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic483, 8
_ZZN2v88internalL29Stats_Runtime_StringMaxLengthEiPmPNS0_7IsolateEE28trace_event_unique_atomic483:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_FlattenStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic476,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_FlattenStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic476, @object
	.size	_ZZN2v88internalL27Stats_Runtime_FlattenStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic476, 8
_ZZN2v88internalL27Stats_Runtime_FlattenStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic476:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_StringEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic468,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_StringEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic468, @object
	.size	_ZZN2v88internalL25Stats_Runtime_StringEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic468, 8
_ZZN2v88internalL25Stats_Runtime_StringEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic468:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic457,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic457, @object
	.size	_ZZN2v88internalL38Stats_Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic457, 8
_ZZN2v88internalL38Stats_Runtime_StringGreaterThanOrEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic457:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_StringGreaterThanEiPmPNS0_7IsolateEE28trace_event_unique_atomic446,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_StringGreaterThanEiPmPNS0_7IsolateEE28trace_event_unique_atomic446, @object
	.size	_ZZN2v88internalL31Stats_Runtime_StringGreaterThanEiPmPNS0_7IsolateEE28trace_event_unique_atomic446, 8
_ZZN2v88internalL31Stats_Runtime_StringGreaterThanEiPmPNS0_7IsolateEE28trace_event_unique_atomic446:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic435,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic435, @object
	.size	_ZZN2v88internalL35Stats_Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic435, 8
_ZZN2v88internalL35Stats_Runtime_StringLessThanOrEqualEiPmPNS0_7IsolateEE28trace_event_unique_atomic435:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateEE28trace_event_unique_atomic424,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateEE28trace_event_unique_atomic424, @object
	.size	_ZZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateEE28trace_event_unique_atomic424, 8
_ZZN2v88internalL28Stats_Runtime_StringLessThanEiPmPNS0_7IsolateEE28trace_event_unique_atomic424:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic379,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic379, @object
	.size	_ZZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic379, 8
_ZZN2v88internalL27Stats_Runtime_StringToArrayEiPmPNS0_7IsolateEE28trace_event_unique_atomic379:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateEE28trace_event_unique_atomic273,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateEE28trace_event_unique_atomic273, @object
	.size	_ZZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateEE28trace_event_unique_atomic273, 8
_ZZN2v88internalL33Stats_Runtime_StringBuilderConcatEiPmPNS0_7IsolateEE28trace_event_unique_atomic273:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateEE28trace_event_unique_atomic254,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateEE28trace_event_unique_atomic254, @object
	.size	_ZZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateEE28trace_event_unique_atomic254, 8
_ZZN2v88internalL30Stats_Runtime_StringCharCodeAtEiPmPNS0_7IsolateEE28trace_event_unique_atomic254:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_InternalizeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic247,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_InternalizeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic247, @object
	.size	_ZZN2v88internalL31Stats_Runtime_InternalizeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic247, 8
_ZZN2v88internalL31Stats_Runtime_InternalizeStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic247:
	.zero	8
	.section	.bss._ZZN2v88internalL23Stats_Runtime_StringAddEiPmPNS0_7IsolateEE28trace_event_unique_atomic236,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL23Stats_Runtime_StringAddEiPmPNS0_7IsolateEE28trace_event_unique_atomic236, @object
	.size	_ZZN2v88internalL23Stats_Runtime_StringAddEiPmPNS0_7IsolateEE28trace_event_unique_atomic236, 8
_ZZN2v88internalL23Stats_Runtime_StringAddEiPmPNS0_7IsolateEE28trace_event_unique_atomic236:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateEE28trace_event_unique_atomic223,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateEE28trace_event_unique_atomic223, @object
	.size	_ZZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateEE28trace_event_unique_atomic223, 8
_ZZN2v88internalL29Stats_Runtime_StringSubstringEiPmPNS0_7IsolateEE28trace_event_unique_atomic223:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_StringLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic217,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_StringLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic217, @object
	.size	_ZZN2v88internalL31Stats_Runtime_StringLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic217, 8
_ZZN2v88internalL31Stats_Runtime_StringLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic217:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic206,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic206, @object
	.size	_ZZN2v88internalL36Stats_Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic206, 8
_ZZN2v88internalL36Stats_Runtime_StringIndexOfUncheckedEiPmPNS0_7IsolateEE28trace_event_unique_atomic206:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_StringIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic196,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_StringIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic196, @object
	.size	_ZZN2v88internalL27Stats_Runtime_StringIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic196, 8
_ZZN2v88internalL27Stats_Runtime_StringIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic196:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateEE28trace_event_unique_atomic153,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateEE28trace_event_unique_atomic153, @object
	.size	_ZZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateEE28trace_event_unique_atomic153, 8
_ZZN2v88internalL28Stats_Runtime_StringIncludesEiPmPNS0_7IsolateEE28trace_event_unique_atomic153:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_StringTrimEiPmPNS0_7IsolateEE28trace_event_unique_atomic142,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_StringTrimEiPmPNS0_7IsolateEE28trace_event_unique_atomic142, @object
	.size	_ZZN2v88internalL24Stats_Runtime_StringTrimEiPmPNS0_7IsolateEE28trace_event_unique_atomic142, 8
_ZZN2v88internalL24Stats_Runtime_StringTrimEiPmPNS0_7IsolateEE28trace_event_unique_atomic142:
	.zero	8
	.section	.bss._ZZN2v88internalL44Stats_Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic112,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Stats_Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic112, @object
	.size	_ZZN2v88internalL44Stats_Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic112, 8
_ZZN2v88internalL44Stats_Runtime_StringReplaceOneCharWithStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic112:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateEE27trace_event_unique_atomic21,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateEE27trace_event_unique_atomic21, @object
	.size	_ZZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateEE27trace_event_unique_atomic21, 8
_ZZN2v88internalL29Stats_Runtime_GetSubstitutionEiPmPNS0_7IsolateEE27trace_event_unique_atomic21:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC21:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC22:
	.long	4294967295
	.long	2146435071
	.align 8
.LC23:
	.long	4290772992
	.long	1105199103
	.align 8
.LC24:
	.long	0
	.long	-1042284544
	.align 8
.LC38:
	.long	0
	.long	1072693248
	.align 8
.LC39:
	.long	4292870144
	.long	1106247679
	.align 8
.LC43:
	.long	0
	.long	0
	.align 8
.LC44:
	.long	0
	.long	1139802112
	.align 8
.LC45:
	.long	0
	.long	1138753536
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
