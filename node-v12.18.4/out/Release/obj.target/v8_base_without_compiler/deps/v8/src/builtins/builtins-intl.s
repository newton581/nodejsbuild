	.file	"builtins-intl.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5836:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5836:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5837:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5837:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5839:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5839:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal12_GLOBAL__N_119CreateBoundFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS0_8Builtins4NameEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119CreateBoundFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS0_8Builtins4NameEi, @function
_ZN2v88internal12_GLOBAL__N_119CreateBoundFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS0_8Builtins4NameEi:
.LFB19278:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$40, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %r14
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L7:
	movq	%r12, %rdi
	movl	$5, %edx
	call	_ZN2v88internal7Factory17NewBuiltinContextENS0_6HandleINS0_13NativeContextEEEi@PLT
	movq	(%r15), %r15
	movq	(%rax), %rdi
	movq	%rax, %r14
	movq	%r15, 47(%rdi)
	leaq	47(%rdi), %rsi
	testb	$1, %r15b
	je	.L15
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L23
	testb	$24, %al
	je	.L15
.L25:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L24
	.p2align 4,,10
	.p2align 3
.L15:
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	128(%r12), %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory31NewSharedFunctionInfoForBuiltinENS0_11MaybeHandleINS0_6StringEEEiNS0_12FunctionKindE@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	movw	%bx, 41(%rax)
	movq	0(%r13), %rax
	movw	%bx, 39(%rax)
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1303(%rax), %r15
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L12
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L13:
	addq	$40, %rsp
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	movl	$1, %r8d
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L25
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L6:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L26
.L8:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L12:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L27
.L14:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L8
	.cfi_endproc
.LFE19278:
	.size	_ZN2v88internal12_GLOBAL__N_119CreateBoundFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS0_8Builtins4NameEi, .-_ZN2v88internal12_GLOBAL__N_119CreateBoundFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS0_8Builtins4NameEi
	.section	.text._ZN2v88internalL32Builtin_Impl_CollatorConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Builtin_Impl_CollatorConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL32Builtin_Impl_CollatorConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19396:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$54, %esi
	subq	$24, %rsp
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rax
	movq	41096(%rdx), %r13
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	leal	-8(,%r15,8), %eax
	movq	%rbx, %rcx
	movq	%rbx, %rsi
	movslq	%eax, %rdx
	subl	$8, %eax
	subq	%rdx, %rcx
	cltq
	subq	%rax, %rsi
	movq	%rcx, %rdx
	movq	88(%r12), %rax
	cmpq	%rax, (%rcx)
	cmove	%rsi, %rdx
	cmpl	$5, %r15d
	jg	.L30
	leaq	88(%r12), %r15
	movq	%r15, %r14
.L33:
	movq	%r12, %rdi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L41
.L34:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal10JSCollator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_@PLT
	testq	%rax, %rax
	je	.L41
	movq	(%rax), %r15
.L35:
	subl	$1, 41104(%r12)
	movq	-56(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L39
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L39:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	leaq	-8(%rbx), %r14
	cmpl	$6, %r15d
	je	.L42
	movq	%r12, %rdi
	leaq	-16(%rbx), %r15
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L34
.L41:
	movq	312(%r12), %r15
	jmp	.L35
.L42:
	leaq	88(%r12), %r15
	jmp	.L33
	.cfi_endproc
.LFE19396:
	.size	_ZN2v88internalL32Builtin_Impl_CollatorConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL32Builtin_Impl_CollatorConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_SegmentIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"%SegmentIteratorPrototype%.next"
	.section	.rodata._ZN2v88internalL41Builtin_Impl_SegmentIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"(location_) != nullptr"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL41Builtin_Impl_SegmentIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_SegmentIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_SegmentIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19417:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L44
.L47:
	leaq	.LC0(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$31, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L58
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L50:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L52
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L52:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L59
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1096, 11(%rax)
	jne	.L47
	movq	%rdx, %rdi
	call	_ZN2v88internal17JSSegmentIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L60
	movq	(%rax), %r13
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L58:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L60:
	movq	312(%r12), %r13
	jmp	.L50
.L59:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19417:
	.size	_ZN2v88internalL41Builtin_Impl_SegmentIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_SegmentIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"get %SegmentIteratorPrototype%.breakType"
	.section	.text._ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19411:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L62
.L65:
	leaq	.LC3(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$40, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L73
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L66:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L68
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L68:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L74
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1096, 11(%rdx)
	jne	.L65
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal17JSSegmentIterator9BreakTypeEv@PLT
	movq	(%rax), %r13
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L74:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19411:
	.size	_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_RelativeTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"Intl.RelativeTimeFormat.prototype.format"
	.section	.text._ZN2v88internalL46Builtin_Impl_RelativeTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_RelativeTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_RelativeTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19333:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L76
.L79:
	leaq	.LC4(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$40, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L94
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L82:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L88
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L88:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1095, 11(%rax)
	jne	.L79
	cmpl	$5, %edi
	jg	.L96
	leaq	88(%rdx), %rdx
	movq	%rdx, %rsi
.L85:
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal20JSRelativeTimeFormat6FormatEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS4_IS1_EE@PLT
	testq	%rax, %rax
	je	.L97
	movq	(%rax), %r13
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	leaq	-8(%rsi), %rsi
	cmpl	$6, %edi
	je	.L98
	leaq	-16(%r13), %rdx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L97:
	movq	312(%r12), %r13
	jmp	.L82
.L95:
	call	__stack_chk_fail@PLT
.L98:
	leaq	88(%rdx), %rdx
	jmp	.L85
	.cfi_endproc
.LFE19333:
	.size	_ZN2v88internalL46Builtin_Impl_RelativeTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_RelativeTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL49Builtin_Impl_NumberFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"Intl.NumberFormat.prototype.resolvedOptions"
	.section	.text._ZN2v88internalL49Builtin_Impl_NumberFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL49Builtin_Impl_NumberFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL49Builtin_Impl_NumberFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19287:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L100
.L103:
	leaq	.LC5(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$43, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L114
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L106:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L108
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L108:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L103
	movq	%rdx, %rdi
	call	_ZN2v88internal14JSNumberFormat18UnwrapNumberFormatEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L116
	movq	%r12, %rdi
	call	_ZN2v88internal14JSNumberFormat15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L114:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L116:
	movq	312(%r12), %r13
	jmp	.L106
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19287:
	.size	_ZN2v88internalL49Builtin_Impl_NumberFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL49Builtin_Impl_NumberFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL53Builtin_Impl_RelativeTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"Intl.RelativeTimeFormat.prototype.formatToParts"
	.section	.text._ZN2v88internalL53Builtin_Impl_RelativeTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL53Builtin_Impl_RelativeTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL53Builtin_Impl_RelativeTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19336:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L118
.L121:
	leaq	.LC6(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$47, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L136
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L124:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L130
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L130:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L137
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1095, 11(%rax)
	jne	.L121
	cmpl	$5, %edi
	jg	.L138
	leaq	88(%rdx), %rdx
	movq	%rdx, %rsi
.L127:
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal20JSRelativeTimeFormat13FormatToPartsEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_NS4_IS1_EE@PLT
	testq	%rax, %rax
	je	.L139
	movq	(%rax), %r13
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L136:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L138:
	leaq	-8(%rsi), %rsi
	cmpl	$6, %edi
	je	.L140
	leaq	-16(%r13), %rdx
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L139:
	movq	312(%r12), %r13
	jmp	.L124
.L137:
	call	__stack_chk_fail@PLT
.L140:
	leaq	88(%rdx), %rdx
	jmp	.L127
	.cfi_endproc
.LFE19336:
	.size	_ZN2v88internalL53Builtin_Impl_RelativeTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL53Builtin_Impl_RelativeTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL51Builtin_Impl_DateTimeFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"Intl.DateTimeFormat.prototype.resolvedOptions"
	.section	.text._ZN2v88internalL51Builtin_Impl_DateTimeFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL51Builtin_Impl_DateTimeFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL51Builtin_Impl_DateTimeFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19264:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L142
.L145:
	leaq	.LC7(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$45, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L158
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L148:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L151
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L151:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L145
	movq	%rdx, %rdi
	call	_ZN2v88internal16JSDateTimeFormat20UnwrapDateTimeFormatEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L157
	movq	%r12, %rdi
	call	_ZN2v88internal16JSDateTimeFormat15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L157
	movq	(%rax), %r13
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L157:
	movq	312(%r12), %r13
	jmp	.L148
.L159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19264:
	.size	_ZN2v88internalL51Builtin_Impl_DateTimeFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL51Builtin_Impl_DateTimeFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypePrecedingENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"%SegmentIteratorPrototype%.preceding"
	.section	.text._ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypePrecedingENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypePrecedingENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypePrecedingENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19420:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L161
.L164:
	leaq	.LC8(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$36, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L177
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L167:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L171
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L171:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L178
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1096, 11(%rax)
	jne	.L164
	cmpl	$6, %edi
	leaq	-8(%rsi), %rax
	leaq	88(%rdx), %rdx
	movq	%r12, %rdi
	cmovge	%rax, %rdx
	call	_ZN2v88internal17JSSegmentIterator9PrecedingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L169
	movq	312(%r12), %r13
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L177:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L169:
	movzbl	%ah, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r13
	jmp	.L167
.L178:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19420:
	.size	_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypePrecedingENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypePrecedingENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeFollowingENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"%SegmentIteratorPrototype%.following"
	.section	.text._ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeFollowingENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeFollowingENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeFollowingENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19414:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L180
.L183:
	leaq	.LC9(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$36, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L196
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L186:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L190
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L190:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1096, 11(%rax)
	jne	.L183
	cmpl	$6, %edi
	leaq	-8(%rsi), %rax
	leaq	88(%rdx), %rdx
	movq	%r12, %rdi
	cmovge	%rax, %rdx
	call	_ZN2v88internal17JSSegmentIterator9FollowingEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L188
	movq	312(%r12), %r13
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L196:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L188:
	movzbl	%ah, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r13
	jmp	.L186
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19414:
	.size	_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeFollowingENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeFollowingENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Builtin_Impl_SegmenterPrototypeSegmentENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"Intl.Segmenter.prototype.segment"
	.section	.text._ZN2v88internalL38Builtin_Impl_SegmenterPrototypeSegmentENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_SegmenterPrototypeSegmentENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_SegmenterPrototypeSegmentENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19435:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L199
.L202:
	leaq	.LC10(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$32, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L219
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L205:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L213
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L213:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L220
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1097, 11(%rdx)
	jne	.L202
	leaq	-8(%rsi), %rsi
	leaq	88(%r12), %r15
	cmpl	$6, %edi
	cmovge	%rsi, %r15
	movq	(%r15), %rdx
	testb	$1, %dl
	jne	.L221
.L210:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L218
	movq	0(%r13), %rax
.L209:
	movslq	43(%rax), %r13
	movq	31(%rax), %rax
	movq	7(%rax), %rax
	andl	$3, %r13d
	movq	24(%rax), %rax
	movq	(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal17JSSegmentIterator6CreateEPNS0_7IsolateEPN6icu_6713BreakIteratorENS0_11JSSegmenter11GranularityENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L218
	movq	(%rax), %r13
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L219:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L221:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L210
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L218:
	movq	312(%r12), %r13
	jmp	.L205
.L220:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19435:
	.size	_ZN2v88internalL38Builtin_Impl_SegmenterPrototypeSegmentENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_SegmenterPrototypeSegmentENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL45Builtin_Impl_V8BreakIteratorInternalAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_V8BreakIteratorInternalAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_V8BreakIteratorInternalAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19447:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	41112(%rdx), %rdi
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	movq	12464(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L223
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L224:
	movq	47(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L226
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
.L227:
	leaq	88(%r12), %rax
	leaq	-8(%rbx), %rsi
	cmpl	$5, %r15d
	cmovle	%rax, %rsi
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L231
.L234:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L241
.L233:
	movq	%rsi, %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal17JSV8BreakIterator9AdoptTextEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE@PLT
	movq	88(%r12), %r15
.L235:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L239
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L239:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L242
.L228:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%r13, %rax
	cmpq	%r14, %r13
	je	.L243
.L225:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L231:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L234
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L241:
	movq	312(%r12), %r15
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%rdx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L228
	.cfi_endproc
.LFE19447:
	.size	_ZN2v88internalL45Builtin_Impl_V8BreakIteratorInternalAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_V8BreakIteratorInternalAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_CollatorInternalCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"(icu_collator) != nullptr"
	.section	.text._ZN2v88internalL36Builtin_Impl_CollatorInternalCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_CollatorInternalCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_CollatorInternalCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19408:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	41112(%rdx), %rdi
	movq	%rsi, -56(%rbp)
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r15
	movq	12464(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L245
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L246:
	movq	47(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L248
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L249:
	cmpl	$5, %r13d
	jg	.L251
	leaq	88(%r12), %rdx
	movq	%rdx, %r13
.L254:
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L255
.L258:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L261
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L260
.L263:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L261
.L262:
	movq	(%rbx), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L270
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl14CompareStringsEPNS0_7IsolateERKN6icu_678CollatorENS0_6HandleINS0_6StringEEESA_@PLT
	movq	(%rax), %r13
.L259:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L268
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L268:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movq	-56(%rbp), %rax
	leaq	-8(%rax), %rdx
	cmpl	$6, %r13d
	je	.L271
	movq	%rax, %r13
	movq	(%rdx), %rax
	subq	$16, %r13
	testb	$1, %al
	je	.L258
.L255:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L258
	movq	0(%r13), %rax
	testb	$1, %al
	je	.L263
	.p2align 4,,10
	.p2align 3
.L260:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L263
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L248:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L272
.L250:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%r14, %rax
	cmpq	%r15, %r14
	je	.L273
.L247:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L261:
	movq	312(%r12), %r13
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L273:
	movq	%rdx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L270:
	leaq	.LC11(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L271:
	leaq	88(%r12), %r13
	jmp	.L254
	.cfi_endproc
.LFE19408:
	.size	_ZN2v88internalL36Builtin_Impl_CollatorInternalCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_CollatorInternalCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_11JSSegmenterEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0.str1.1,"aMS",@progbits,1
.LC12:
	.string	"Intl.Segmenter"
	.section	.text._ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_11JSSegmenterEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_11JSSegmenterEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_11JSSegmenterEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0:
.LFB24221:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movl	$61, %esi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	leal	-8(,%r13,8), %eax
	movq	%rbx, %rcx
	movslq	%eax, %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	movq	88(%r12), %rcx
	cmpq	%rcx, (%rdx)
	je	.L287
	subl	$8, %eax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	cltq
	subq	%rax, %rsi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L286
	cmpl	$5, %r13d
	jle	.L288
	leaq	-8(%rbx), %rdx
	cmpl	$6, %r13d
	je	.L289
	leaq	-16(%rbx), %rcx
.L282:
	movq	%r12, %rdi
	call	_ZN2v88internal11JSSegmenter3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_@PLT
	testq	%rax, %rax
	je	.L286
	movq	(%rax), %rax
.L277:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L290
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	leaq	88(%r12), %rcx
	movq	%rcx, %rdx
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L287:
	leaq	.LC12(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$14, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L291
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$40, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L286:
	movq	312(%r12), %rax
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L291:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L290:
	call	__stack_chk_fail@PLT
.L289:
	leaq	88(%r12), %rcx
	jmp	.L282
	.cfi_endproc
.LFE24221:
	.size	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_11JSSegmenterEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0, .-_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_11JSSegmenterEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_20JSRelativeTimeFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0.str1.1,"aMS",@progbits,1
.LC13:
	.string	"Intl.RelativeTimeFormat"
	.section	.text._ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_20JSRelativeTimeFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_20JSRelativeTimeFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_20JSRelativeTimeFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0:
.LFB24219:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movl	$58, %esi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	leal	-8(,%r13,8), %eax
	movq	%rbx, %rcx
	movslq	%eax, %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	movq	88(%r12), %rcx
	cmpq	%rcx, (%rdx)
	je	.L305
	subl	$8, %eax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	cltq
	subq	%rax, %rsi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L304
	cmpl	$5, %r13d
	jle	.L306
	leaq	-8(%rbx), %rdx
	cmpl	$6, %r13d
	je	.L307
	leaq	-16(%rbx), %rcx
.L300:
	movq	%r12, %rdi
	call	_ZN2v88internal20JSRelativeTimeFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_@PLT
	testq	%rax, %rax
	je	.L304
	movq	(%rax), %rax
.L295:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L308
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	leaq	88(%r12), %rcx
	movq	%rcx, %rdx
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L305:
	leaq	.LC13(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$23, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L309
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$40, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L304:
	movq	312(%r12), %rax
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L309:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L308:
	call	__stack_chk_fail@PLT
.L307:
	leaq	88(%r12), %rcx
	jmp	.L300
	.cfi_endproc
.LFE24219:
	.size	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_20JSRelativeTimeFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0, .-_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_20JSRelativeTimeFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_12JSListFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0.str1.1,"aMS",@progbits,1
.LC14:
	.string	"Intl.ListFormat"
	.section	.text._ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_12JSListFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_12JSListFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_12JSListFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0:
.LFB24218:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movl	$60, %esi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	leal	-8(,%r13,8), %eax
	movq	%rbx, %rcx
	movslq	%eax, %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	movq	88(%r12), %rcx
	cmpq	%rcx, (%rdx)
	je	.L323
	subl	$8, %eax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	cltq
	subq	%rax, %rsi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L322
	cmpl	$5, %r13d
	jle	.L324
	leaq	-8(%rbx), %rdx
	cmpl	$6, %r13d
	je	.L325
	leaq	-16(%rbx), %rcx
.L318:
	movq	%r12, %rdi
	call	_ZN2v88internal12JSListFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_@PLT
	testq	%rax, %rax
	je	.L322
	movq	(%rax), %rax
.L313:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L326
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	leaq	88(%r12), %rcx
	movq	%rcx, %rdx
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L323:
	leaq	.LC14(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$15, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L327
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$40, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L322:
	movq	312(%r12), %rax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L327:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L326:
	call	__stack_chk_fail@PLT
.L325:
	leaq	88(%r12), %rcx
	jmp	.L318
	.cfi_endproc
.LFE24218:
	.size	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_12JSListFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0, .-_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_12JSListFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0
	.section	.rodata._ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_13JSPluralRulesEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0.str1.1,"aMS",@progbits,1
.LC15:
	.string	"Intl.PluralRules"
	.section	.text._ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_13JSPluralRulesEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_13JSPluralRulesEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0, @function
_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_13JSPluralRulesEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0:
.LFB24220:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movl	$57, %esi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	leal	-8(,%r13,8), %eax
	movq	%rbx, %rcx
	movslq	%eax, %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	movq	88(%r12), %rcx
	cmpq	%rcx, (%rdx)
	je	.L341
	subl	$8, %eax
	movq	%rbx, %rsi
	movq	%r12, %rdi
	cltq
	subq	%rax, %rsi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L340
	cmpl	$5, %r13d
	jle	.L342
	leaq	-8(%rbx), %rdx
	cmpl	$6, %r13d
	je	.L343
	leaq	-16(%rbx), %rcx
.L336:
	movq	%r12, %rdi
	call	_ZN2v88internal13JSPluralRules3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_@PLT
	testq	%rax, %rax
	je	.L340
	movq	(%rax), %rax
.L331:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L344
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	leaq	88(%r12), %rcx
	movq	%rcx, %rdx
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L341:
	leaq	.LC15(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$16, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L345
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$40, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L340:
	movq	312(%r12), %rax
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L345:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L344:
	call	__stack_chk_fail@PLT
.L343:
	leaq	88(%r12), %rcx
	jmp	.L336
	.cfi_endproc
.LFE24220:
	.size	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_13JSPluralRulesEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0, .-_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_13JSPluralRulesEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0
	.section	.rodata._ZN2v88internalL45Builtin_Impl_NumberFormatInternalFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"(icu_localized_number_formatter) != nullptr"
	.section	.text._ZN2v88internalL45Builtin_Impl_NumberFormatInternalFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_NumberFormatInternalFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_NumberFormatInternalFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19293:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	41112(%rdx), %rdi
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	movq	12464(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L347
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L348:
	movq	47(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L350
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L351:
	cmpl	$5, %r15d
	leaq	88(%r12), %rax
	leaq	-8(%rbx), %r8
	cmovle	%rax, %r8
	movq	(%r8), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	cmpb	$0, _ZN2v88internal24FLAG_harmony_intl_bigintE(%rip)
	je	.L355
	testb	%dl, %dl
	je	.L376
.L363:
	movq	(%rcx), %rax
	movq	31(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L377
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14JSNumberFormat13FormatNumericEPNS0_7IsolateERKN6icu_676number24LocalizedNumberFormatterENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L373
	movq	(%rax), %r15
.L362:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L370
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L370:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	testb	%dl, %dl
	jne	.L363
	movq	-1(%rax), %rax
	cmpw	$65, 11(%rax)
	je	.L363
	movq	%rcx, -56(%rbp)
	xorl	%edx, %edx
.L375:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %r8
	jne	.L363
.L373:
	movq	312(%r12), %r15
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L376:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L363
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	je	.L363
	movq	%rcx, -56(%rbp)
	movl	$1, %edx
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L350:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L378
.L352:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L347:
	movq	%r13, %rax
	cmpq	%r14, %r13
	je	.L379
.L349:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L377:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%rdx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L378:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L352
	.cfi_endproc
.LFE19293:
	.size	_ZN2v88internalL45Builtin_Impl_NumberFormatInternalFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_NumberFormatInternalFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_StringPrototypeToUpperCaseIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"String.prototype.toUpperCase"
	.section	.text._ZN2v88internalL43Builtin_Impl_StringPrototypeToUpperCaseIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_StringPrototypeToUpperCaseIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_StringPrototypeToUpperCaseIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19249:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rdx
	cmpq	%rdx, 104(%r12)
	jne	.L410
.L381:
	leaq	.LC17(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$28, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L411
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$27, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L387:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L405
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L405:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L412
	addq	$48, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	cmpq	%rdx, 88(%r12)
	je	.L381
	testb	$1, %dl
	jne	.L382
.L385:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L409
.L384:
	movq	(%r8), %rax
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	ja	.L389
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L413
.L389:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L397
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L397
	movq	(%r8), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L400
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl14ConvertToUpperEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L409
	movq	(%rax), %r14
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L411:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L382:
	movq	-1(%rdx), %rax
	movq	%rsi, %r8
	cmpw	$63, 11(%rax)
	jbe	.L384
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L409:
	movq	312(%r12), %r14
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L413:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L392
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L392
	movq	%r8, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r8
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L400:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L414
.L402:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L392:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L394
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
	jmp	.L389
.L394:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L415
.L396:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L389
.L414:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L402
.L415:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L396
.L412:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19249:
	.size	_ZN2v88internalL43Builtin_Impl_StringPrototypeToUpperCaseIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_StringPrototypeToUpperCaseIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_StringPrototypeNormalizeIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC18:
	.string	"String.prototype.normalize"
	.section	.text._ZN2v88internalL41Builtin_Impl_StringPrototypeNormalizeIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_StringPrototypeNormalizeIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_StringPrototypeNormalizeIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19252:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$75, %esi
	subq	$40, %rsp
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	(%rbx), %rax
	cmpq	%rax, 104(%r12)
	jne	.L433
.L417:
	leaq	.LC18(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$26, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L434
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$27, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r15
.L423:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L428
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L428:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L435
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	cmpq	%rax, 88(%r12)
	je	.L417
	movq	%rbx, %rsi
	testb	$1, %al
	jne	.L418
.L421:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L432
.L420:
	subq	$8, %rbx
	leaq	88(%r12), %rdx
	cmpl	$5, %r15d
	movq	%r12, %rdi
	cmovg	%rbx, %rdx
	call	_ZN2v88internal4Intl9NormalizeEPNS0_7IsolateENS0_6HandleINS0_6StringEEENS4_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L432
	movq	(%rax), %r15
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L434:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L418:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L421
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L432:
	movq	312(%r12), %r15
	jmp	.L423
.L435:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19252:
	.size	_ZN2v88internalL41Builtin_Impl_StringPrototypeNormalizeIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_StringPrototypeNormalizeIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleUpperCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"String.prototype.toLocaleUpperCase"
	.section	.text._ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleUpperCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleUpperCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleUpperCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19381:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$63, %esi
	subq	$40, %rsp
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	(%rbx), %rax
	cmpq	%rax, 104(%r12)
	jne	.L453
.L437:
	leaq	.LC19(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$34, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L454
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$27, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r15
.L443:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L448
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L448:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L455
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore_state
	cmpq	%rax, 88(%r12)
	je	.L437
	movq	%rbx, %rsi
	testb	$1, %al
	jne	.L438
.L441:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L452
.L440:
	subq	$8, %rbx
	leaq	88(%r12), %rcx
	cmpl	$5, %r15d
	movq	%r12, %rdi
	cmovg	%rbx, %rcx
	movl	$1, %edx
	call	_ZN2v88internal4Intl23StringLocaleConvertCaseEPNS0_7IsolateENS0_6HandleINS0_6StringEEEbNS4_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L452
	movq	(%rax), %r15
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L454:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L438:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L441
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L452:
	movq	312(%r12), %r15
	jmp	.L443
.L455:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19381:
	.size	_ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleUpperCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleUpperCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleLowerCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"String.prototype.toLocaleLowerCase"
	.section	.text._ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleLowerCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleLowerCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleLowerCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19378:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rdx, %rdi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$64, %esi
	subq	$40, %rsp
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	(%rbx), %rax
	cmpq	%rax, 104(%r12)
	jne	.L473
.L457:
	leaq	.LC20(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$34, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L474
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$27, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r15
.L463:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L468
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L468:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L475
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore_state
	cmpq	%rax, 88(%r12)
	je	.L457
	movq	%rbx, %rsi
	testb	$1, %al
	jne	.L458
.L461:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L472
.L460:
	subq	$8, %rbx
	leaq	88(%r12), %rcx
	cmpl	$5, %r15d
	movq	%r12, %rdi
	cmovg	%rbx, %rcx
	xorl	%edx, %edx
	call	_ZN2v88internal4Intl23StringLocaleConvertCaseEPNS0_7IsolateENS0_6HandleINS0_6StringEEEbNS4_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L472
	movq	(%rax), %r15
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L474:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L458:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L461
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L472:
	movq	312(%r12), %r15
	jmp	.L463
.L475:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19378:
	.size	_ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleLowerCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleLowerCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal12_GLOBAL__N_112CreateLocaleEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEESA_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_112CreateLocaleEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEESA_, @function
_ZN2v88internal12_GLOBAL__N_112CreateLocaleEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEESA_:
.LFB19315:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	testq	%rax, %rax
	je	.L478
	movq	%rax, %r14
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L504
.L479:
	xorl	%edx, %edx
	movl	$72, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L478:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L480
	testb	$1, %al
	jne	.L505
.L485:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L478
.L489:
	movq	%rbx, %r13
.L487:
	movq	(%r15), %rdx
	movq	%r15, %rax
	cmpq	%rdx, 88(%r12)
	je	.L506
	testb	$1, %dl
	jne	.L492
.L494:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	testq	%rax, %rax
	je	.L478
.L493:
	movq	%rax, %rcx
.L491:
	addq	$8, %rsp
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8JSLocale3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6StringEEENS4_INS0_10JSReceiverEEE@PLT
	.p2align 4,,10
	.p2align 3
.L480:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L479
	testb	$1, %al
	je	.L485
.L505:
	movq	-1(%rax), %rdx
	cmpw	$1092, 11(%rdx)
	je	.L484
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L485
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L506:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	%rax, %rcx
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L492:
	movq	-1(%rdx), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L494
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L484:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r13
	jmp	.L487
	.cfi_endproc
.LFE19315:
	.size	_ZN2v88internal12_GLOBAL__N_112CreateLocaleEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEESA_, .-_ZN2v88internal12_GLOBAL__N_112CreateLocaleEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEESA_
	.section	.rodata._ZN2v88internalL36Builtin_Impl_LocalePrototypeMaximizeENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"Intl.Locale.prototype.maximize"
	.section	.text._ZN2v88internalL36Builtin_Impl_LocalePrototypeMaximizeENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_LocalePrototypeMaximizeENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_LocalePrototypeMaximizeENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19324:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L508
.L511:
	leaq	.LC21(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$30, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L528
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L517:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L522
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L522:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L529
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L511
	movq	12464(%rdx), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L512
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L513:
	movq	639(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L530
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L518:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	-88(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	(%rdx), %rsi
	call	_ZN2v88internal8JSLocale8MaximizeEPNS0_7IsolateENS0_6StringE@PLT
	movq	%r13, %r8
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112CreateLocaleEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEESA_
	testq	%rax, %rax
	je	.L531
	movq	(%rax), %r13
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L528:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L530:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L532
.L519:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L512:
	movq	%r14, %rax
	cmpq	41096(%rdx), %r14
	je	.L533
.L514:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L513
.L533:
	movq	%rdx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L514
.L532:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L531:
	movq	312(%r12), %r13
	jmp	.L517
.L529:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19324:
	.size	_ZN2v88internalL36Builtin_Impl_LocalePrototypeMaximizeENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_LocalePrototypeMaximizeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_LocalePrototypeMinimizeENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"Intl.Locale.prototype.minimize"
	.section	.text._ZN2v88internalL36Builtin_Impl_LocalePrototypeMinimizeENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_LocalePrototypeMinimizeENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_LocalePrototypeMinimizeENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19327:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L535
.L538:
	leaq	.LC22(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$30, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L555
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L544:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L549
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L549:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L556
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L538
	movq	12464(%rdx), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L539
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L540:
	movq	639(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L557
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L545:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	-88(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	(%rdx), %rsi
	call	_ZN2v88internal8JSLocale8MinimizeEPNS0_7IsolateENS0_6StringE@PLT
	movq	%r13, %r8
	movq	%r15, %rdx
	movq	%r15, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112CreateLocaleEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEESA_
	testq	%rax, %rax
	je	.L558
	movq	(%rax), %r13
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L555:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L557:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L559
.L546:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L539:
	movq	%r14, %rax
	cmpq	41096(%rdx), %r14
	je	.L560
.L541:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L540
.L560:
	movq	%rdx, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L541
.L559:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L558:
	movq	312(%r12), %r13
	jmp	.L544
.L556:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19327:
	.size	_ZN2v88internalL36Builtin_Impl_LocalePrototypeMinimizeENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_LocalePrototypeMinimizeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_PluralRulesPrototypeSelectENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"Intl.PluralRules.prototype.select"
	.section	.text._ZN2v88internalL39Builtin_Impl_PluralRulesPrototypeSelectENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_PluralRulesPrototypeSelectENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_PluralRulesPrototypeSelectENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19390:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L562
.L565:
	leaq	.LC23(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$33, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L585
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L568:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L579
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L579:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L586
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1094, 11(%rax)
	jne	.L565
	leaq	-8(%rsi), %rax
	cmpl	$6, %edi
	leaq	88(%rdx), %rsi
	cmovge	%rax, %rsi
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L587
.L571:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L576:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13JSPluralRules13ResolvePluralEPNS0_7IsolateENS0_6HandleIS1_EEd@PLT
	testq	%rax, %rax
	je	.L584
	movq	(%rax), %r13
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L585:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L587:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L572
	xorl	%edx, %edx
.L573:
	testb	%dl, %dl
	jne	.L571
	movsd	7(%rax), %xmm0
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L572:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L574
.L584:
	movq	312(%r12), %r13
	jmp	.L568
.L586:
	call	__stack_chk_fail@PLT
.L574:
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L573
	.cfi_endproc
.LFE19390:
	.size	_ZN2v88internalL39Builtin_Impl_PluralRulesPrototypeSelectENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_PluralRulesPrototypeSelectENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_NumberFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"Intl.NumberFormat"
.LC25:
	.string	"success.FromJust()"
	.section	.text._ZN2v88internalL36Builtin_Impl_NumberFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_NumberFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_NumberFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19284:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -104(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -112(%rbp)
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	631(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L589
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L590:
	movl	$55, %esi
	movq	%r15, %rdi
	movq	%r12, %rbx
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	leal	-8(,%r14,8), %eax
	movq	%r12, %rsi
	movslq	%eax, %rdx
	subl	$8, %eax
	cltq
	subq	%rdx, %rbx
	subq	%rax, %rsi
	movq	88(%r15), %rax
	cmpq	%rax, (%rbx)
	movq	%rsi, %rdx
	cmovne	%rbx, %rdx
	cmpl	$5, %r14d
	jg	.L593
	leaq	88(%r15), %rcx
	movq	%rcx, %r8
.L596:
	movq	%r15, %rdi
	movq	%r8, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L620
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal14JSNumberFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L620
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object10InstanceOfEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testq	%rax, %rax
	je	.L620
	movq	(%rax), %rax
	leaq	-96(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movq	88(%r15), %rcx
	cmpq	%rcx, (%rbx)
	jne	.L615
	testb	%al, %al
	jne	.L601
.L615:
	movq	(%r14), %r12
.L598:
	movq	-104(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-112(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L612
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L612:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L623
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	.cfi_restore_state
	leaq	-8(%r12), %r8
	cmpl	$6, %r14d
	je	.L624
	leaq	-16(%r12), %rcx
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L589:
	movq	-104(%rbp), %rax
	movq	%rax, %r13
	cmpq	41096(%rdx), %rax
	je	.L625
.L591:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L620:
	movq	312(%r15), %r12
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L601:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L626
.L603:
	leaq	.LC24(%rip), %rax
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	movq	$17, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L627
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r12
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L625:
	movq	%rdx, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L626:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L603
	movzbl	-96(%rbp), %eax
	movl	$1, %r8d
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	3864(%r15), %rdx
	movq	%r15, %rdi
	movq	$0, -80(%rbp)
	andl	$-64, %eax
	movq	$0, -72(%rbp)
	orl	$42, %eax
	movq	$0, -64(%rbp)
	movq	%r14, -88(%rbp)
	movb	%al, -96(%rbp)
	call	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L620
	shrw	$8, %ax
	je	.L628
	movq	(%r12), %r12
	jmp	.L598
.L627:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L628:
	leaq	.LC25(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L623:
	call	__stack_chk_fail@PLT
.L624:
	leaq	88(%r15), %rcx
	jmp	.L596
	.cfi_endproc
.LFE19284:
	.size	_ZN2v88internalL36Builtin_Impl_NumberFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_NumberFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Builtin_Impl_DateTimeFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC26:
	.string	"Intl.DateTimeFormat"
	.section	.text._ZN2v88internalL38Builtin_Impl_DateTimeFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_DateTimeFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_DateTimeFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19296:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -104(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -112(%rbp)
	movq	12464(%rdx), %rax
	movq	39(%rax), %rax
	movq	623(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L630
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L631:
	movl	$56, %esi
	movq	%r15, %rdi
	movq	%r12, %rbx
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	leal	-8(,%r14,8), %eax
	movq	%r12, %rsi
	movslq	%eax, %rdx
	subl	$8, %eax
	cltq
	subq	%rdx, %rbx
	subq	%rax, %rsi
	movq	88(%r15), %rax
	cmpq	%rax, (%rbx)
	movq	%rsi, %rdx
	cmovne	%rbx, %rdx
	cmpl	$5, %r14d
	jg	.L634
	leaq	88(%r15), %rcx
	movq	%rcx, %r8
.L637:
	movq	%r15, %rdi
	movq	%r8, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L661
	movq	%r8, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal16JSDateTimeFormat3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L661
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object10InstanceOfEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testq	%rax, %rax
	je	.L661
	movq	(%rax), %rax
	leaq	-96(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movq	88(%r15), %rcx
	cmpq	%rcx, (%rbx)
	jne	.L656
	testb	%al, %al
	jne	.L642
.L656:
	movq	(%r14), %r12
.L639:
	movq	-104(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-112(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L653
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L653:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L664
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L634:
	.cfi_restore_state
	leaq	-8(%r12), %r8
	cmpl	$6, %r14d
	je	.L665
	leaq	-16(%r12), %rcx
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L630:
	movq	-104(%rbp), %rax
	movq	%rax, %r13
	cmpq	41096(%rdx), %rax
	je	.L666
.L632:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L661:
	movq	312(%r15), %r12
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L642:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L667
.L644:
	leaq	.LC26(%rip), %rax
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%rax, -96(%rbp)
	movq	$19, -88(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L668
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r12
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L666:
	movq	%rdx, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L667:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L644
	movzbl	-96(%rbp), %eax
	movl	$1, %r8d
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	3864(%r15), %rdx
	movq	%r15, %rdi
	movq	$0, -80(%rbp)
	andl	$-64, %eax
	movq	$0, -72(%rbp)
	orl	$42, %eax
	movq	$0, -64(%rbp)
	movq	%r14, -88(%rbp)
	movb	%al, -96(%rbp)
	call	_ZN2v88internal10JSReceiver17DefineOwnPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPNS0_18PropertyDescriptorENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	je	.L661
	shrw	$8, %ax
	je	.L669
	movq	(%r12), %r12
	jmp	.L639
.L668:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L669:
	leaq	.LC25(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L664:
	call	__stack_chk_fail@PLT
.L665:
	leaq	88(%r15), %rcx
	jmp	.L637
	.cfi_endproc
.LFE19296:
	.size	_ZN2v88internalL38Builtin_Impl_DateTimeFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_DateTimeFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"get Intl.v8BreakIterator.prototype.breakType"
	.section	.text._ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19468:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L671
.L674:
	leaq	.LC27(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$44, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L698
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L677:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L685
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L685:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L699
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1088, 11(%rdx)
	jne	.L674
	movq	41112(%r12), %rdi
	movq	79(%rax), %rsi
	testq	%rdi, %rdi
	je	.L700
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L678:
	cmpq	%rsi, 88(%r12)
	je	.L680
	movq	%rsi, %r13
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L698:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L700:
	movq	%r14, %rax
	cmpq	41096(%r12), %r14
	je	.L701
.L679:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L678
.L701:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	$1059, %edx
	call	_ZN2v88internal12_GLOBAL__N_119CreateBoundFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS0_8Builtins4NameEi
	movq	0(%r13), %rdi
	movq	(%rax), %r13
	movq	%rax, %r15
	leaq	79(%rdi), %rsi
	movq	%r13, 79(%rdi)
	testb	$1, %r13b
	je	.L686
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L682
	movq	%r13, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
.L682:
	testb	$24, %al
	je	.L686
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L686
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L686:
	movq	(%r15), %r13
	jmp	.L677
.L699:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19468:
	.size	_ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL49Builtin_Impl_DateTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC28:
	.string	"Intl.DateTimeFormat.prototype.formatToParts"
	.section	.text._ZN2v88internalL49Builtin_Impl_DateTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL49Builtin_Impl_DateTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL49Builtin_Impl_DateTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19270:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L703
.L706:
	leaq	.LC28(%rip), %rax
	movq	$43, -72(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
.L738:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L704
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L736:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L709:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L723
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L723:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L739
	addq	$48, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L703:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L706
	movq	-1(%rax), %rax
	cmpw	$1090, 11(%rax)
	je	.L732
	leaq	.LC28(%rip), %rax
	movq	$43, -56(%rbp)
	leaq	-64(%rbp), %rsi
	movq	%rax, -64(%rbp)
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L704:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L732:
	leaq	88(%r12), %rax
	leaq	-8(%rsi), %rsi
	cmpl	$5, %edi
	cmovg	%rsi, %rax
	movq	(%rax), %rax
	cmpq	%rax, 88(%r12)
	je	.L740
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L741
.L715:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L719:
	call	_ZN2v88internal9DateCache8TimeClipEd@PLT
	ucomisd	%xmm0, %xmm0
	jp	.L742
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal16JSDateTimeFormat13FormatToPartsEPNS0_7IsolateENS0_6HandleIS1_EEd@PLT
	testq	%rax, %rax
	je	.L735
	movq	(%rax), %r13
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L741:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L716
	xorl	%edx, %edx
.L713:
	testb	%dl, %dl
	jne	.L715
	movsd	7(%rax), %xmm0
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L740:
	movq	%r12, %rdi
	call	_ZN2v88internal6JSDate16CurrentTimeValueEPNS0_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L735:
	movq	312(%r12), %r13
	jmp	.L709
.L716:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	je	.L735
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L742:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$202, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L736
.L739:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19270:
	.size	_ZN2v88internalL49Builtin_Impl_DateTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL49Builtin_Impl_DateTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_NumberFormatPrototypeFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"get Intl.NumberFormat.prototype.format"
	.section	.text._ZN2v88internalL46Builtin_Impl_NumberFormatPrototypeFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_NumberFormatPrototypeFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_NumberFormatPrototypeFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19290:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L744
.L747:
	leaq	.LC29(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$38, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L772
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L750:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L759
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L759:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L773
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L744:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L747
	movq	%rdx, %rdi
	call	_ZN2v88internal14JSNumberFormat18UnwrapNumberFormatEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L774
	movq	(%rax), %rax
	movq	41112(%r12), %rdi
	movq	39(%rax), %r13
	testq	%rdi, %rdi
	je	.L751
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L752:
	cmpq	%r13, 88(%r12)
	jne	.L750
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$1028, %edx
	call	_ZN2v88internal12_GLOBAL__N_119CreateBoundFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS0_8Builtins4NameEi
	movq	(%r15), %rdi
	movq	(%rax), %r15
	movq	%rax, %r13
	leaq	39(%rdi), %rsi
	movq	%r15, 39(%rdi)
	testb	$1, %r15b
	je	.L760
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L756
	movq	%r15, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
.L756:
	testb	$24, %al
	je	.L760
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L760
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L760:
	movq	0(%r13), %r13
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L772:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L751:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L775
.L753:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r13, (%rax)
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L774:
	movq	312(%r12), %r13
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L775:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L753
.L773:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19290:
	.size	_ZN2v88internalL46Builtin_Impl_NumberFormatPrototypeFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_NumberFormatPrototypeFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_DateTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"get Intl.DateTimeFormat.prototype.format"
	.section	.text._ZN2v88internalL42Builtin_Impl_DateTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_DateTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_DateTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19299:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L777
.L780:
	leaq	.LC30(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$40, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L805
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L783:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L792
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L792:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L806
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L777:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L780
	movq	%rdx, %rdi
	call	_ZN2v88internal16JSDateTimeFormat20UnwrapDateTimeFormatEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L807
	movq	(%rax), %rax
	movq	41112(%r12), %rdi
	movq	47(%rax), %r13
	testq	%rdi, %rdi
	je	.L784
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L785:
	cmpq	%r13, 88(%r12)
	jne	.L783
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$1000, %edx
	call	_ZN2v88internal12_GLOBAL__N_119CreateBoundFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS0_8Builtins4NameEi
	movq	(%r15), %rdi
	movq	(%rax), %r15
	movq	%rax, %r13
	leaq	47(%rdi), %rsi
	movq	%r15, 47(%rdi)
	testb	$1, %r15b
	je	.L793
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L789
	movq	%r15, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
.L789:
	testb	$24, %al
	je	.L793
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L793
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L793:
	movq	0(%r13), %r13
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L805:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L784:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L808
.L786:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r13, (%rax)
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L807:
	movq	312(%r12), %r13
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L808:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L786
.L806:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19299:
	.size	_ZN2v88internalL42Builtin_Impl_DateTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_DateTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_NumberFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC31:
	.string	"Intl.NumberFormat.prototype.formatToParts"
	.section	.text._ZN2v88internalL47Builtin_Impl_NumberFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_NumberFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_NumberFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19261:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L810
.L813:
	leaq	.LC31(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$41, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L840
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L816:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L831
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L831:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L841
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L810:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1093, 11(%rax)
	jne	.L813
	cmpl	$5, %edi
	jle	.L842
	movq	-8(%r13), %rdx
	leaq	-8(%rsi), %rsi
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	cmpb	$0, _ZN2v88internal24FLAG_harmony_intl_bigintE(%rip)
	je	.L817
	testb	%al, %al
	jne	.L828
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L828
	movq	-1(%rdx), %rax
	movl	$1, %edx
	cmpw	$66, 11(%rax)
	je	.L828
.L839:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L828
.L837:
	movq	312(%r12), %r13
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L840:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L842:
	leaq	1088(%rdx), %rdx
.L824:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14JSNumberFormat13FormatToPartsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L837
	movq	(%rax), %r13
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L817:
	testb	%al, %al
	jne	.L828
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L843
	.p2align 4,,10
	.p2align 3
.L828:
	movq	%rsi, %rdx
	jmp	.L824
.L843:
	xorl	%edx, %edx
	jmp	.L839
.L841:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19261:
	.size	_ZN2v88internalL47Builtin_Impl_NumberFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_NumberFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_V8BreakIteratorPrototypeFirstENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"get Intl.v8BreakIterator.prototype.first"
	.section	.text._ZN2v88internalL42Builtin_Impl_V8BreakIteratorPrototypeFirstENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_V8BreakIteratorPrototypeFirstENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_V8BreakIteratorPrototypeFirstENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19450:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L845
.L848:
	leaq	.LC32(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$40, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L872
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L851:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L859
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L859:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L873
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L845:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1088, 11(%rdx)
	jne	.L848
	movq	41112(%r12), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L874
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L852:
	cmpq	%rsi, 88(%r12)
	je	.L854
	movq	%rsi, %r13
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L872:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L874:
	movq	%r14, %rax
	cmpq	41096(%r12), %r14
	je	.L875
.L853:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L852
.L875:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L854:
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	$1061, %edx
	call	_ZN2v88internal12_GLOBAL__N_119CreateBoundFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS0_8Builtins4NameEi
	movq	0(%r13), %rdi
	movq	(%rax), %r13
	movq	%rax, %r15
	leaq	55(%rdi), %rsi
	movq	%r13, 55(%rdi)
	testb	$1, %r13b
	je	.L860
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L856
	movq	%r13, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
.L856:
	testb	$24, %al
	je	.L860
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L860
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L860:
	movq	(%r15), %r13
	jmp	.L851
.L873:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19450:
	.size	_ZN2v88internalL42Builtin_Impl_V8BreakIteratorPrototypeFirstENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_V8BreakIteratorPrototypeFirstENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_V8BreakIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC33:
	.string	"get Intl.v8BreakIterator.prototype.next"
	.section	.text._ZN2v88internalL41Builtin_Impl_V8BreakIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_V8BreakIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_V8BreakIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19456:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L877
.L880:
	leaq	.LC33(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$39, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L904
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L883:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L891
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L891:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L905
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L877:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1088, 11(%rdx)
	jne	.L880
	movq	41112(%r12), %rdi
	movq	63(%rax), %rsi
	testq	%rdi, %rdi
	je	.L906
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L884:
	cmpq	%rsi, 88(%r12)
	je	.L886
	movq	%rsi, %r13
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L904:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L906:
	movq	%r14, %rax
	cmpq	41096(%r12), %r14
	je	.L907
.L885:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L884
.L907:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L886:
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	$1062, %edx
	call	_ZN2v88internal12_GLOBAL__N_119CreateBoundFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS0_8Builtins4NameEi
	movq	0(%r13), %rdi
	movq	(%rax), %r13
	movq	%rax, %r15
	leaq	63(%rdi), %rsi
	movq	%r13, 63(%rdi)
	testb	$1, %r13b
	je	.L892
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L888
	movq	%r13, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
.L888:
	testb	$24, %al
	je	.L892
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L892
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L892:
	movq	(%r15), %r13
	jmp	.L883
.L905:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19456:
	.size	_ZN2v88internalL41Builtin_Impl_V8BreakIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_V8BreakIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"get Intl.v8BreakIterator.prototype.adoptText"
	.section	.text._ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19444:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L909
.L912:
	leaq	.LC34(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$44, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L936
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L915:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L923
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L923:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L937
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1088, 11(%rdx)
	jne	.L912
	movq	41112(%r12), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L938
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L916:
	cmpq	%rsi, 88(%r12)
	je	.L918
	movq	%rsi, %r13
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L936:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L938:
	movq	%r14, %rax
	cmpq	41096(%r12), %r14
	je	.L939
.L917:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L916
.L939:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L918:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$1058, %edx
	call	_ZN2v88internal12_GLOBAL__N_119CreateBoundFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS0_8Builtins4NameEi
	movq	0(%r13), %rdi
	movq	(%rax), %r13
	movq	%rax, %r15
	leaq	47(%rdi), %rsi
	movq	%r13, 47(%rdi)
	testb	$1, %r13b
	je	.L924
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L920
	movq	%r13, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
.L920:
	testb	$24, %al
	je	.L924
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L924
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L924:
	movq	(%r15), %r13
	jmp	.L915
.L937:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19444:
	.size	_ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Builtin_Impl_CollatorPrototypeCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"get Intl.Collator.prototype.compare"
	.section	.text._ZN2v88internalL37Builtin_Impl_CollatorPrototypeCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Builtin_Impl_CollatorPrototypeCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL37Builtin_Impl_CollatorPrototypeCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19405:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L941
.L944:
	leaq	.LC35(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$35, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L968
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L947:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L955
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L955:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L969
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L941:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1089, 11(%rdx)
	jne	.L944
	movq	41112(%r12), %rdi
	movq	31(%rax), %rsi
	testq	%rdi, %rdi
	je	.L970
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L948:
	cmpq	%rsi, 88(%r12)
	je	.L950
	movq	%rsi, %r13
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L968:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L970:
	movq	%r14, %rax
	cmpq	41096(%r12), %r14
	je	.L971
.L949:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L948
.L971:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L950:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$2, %ecx
	movl	$992, %edx
	call	_ZN2v88internal12_GLOBAL__N_119CreateBoundFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS0_8Builtins4NameEi
	movq	0(%r13), %rdi
	movq	(%rax), %r13
	movq	%rax, %r15
	leaq	31(%rdi), %rsi
	movq	%r13, 31(%rdi)
	testb	$1, %r13b
	je	.L956
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L952
	movq	%r13, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
.L952:
	testb	$24, %al
	je	.L956
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L956
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L956:
	movq	(%r15), %r13
	jmp	.L947
.L969:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19405:
	.size	_ZN2v88internalL37Builtin_Impl_CollatorPrototypeCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL37Builtin_Impl_CollatorPrototypeCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Builtin_Impl_V8BreakIteratorPrototypeCurrentENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"get Intl.v8BreakIterator.prototype.current"
	.section	.text._ZN2v88internalL44Builtin_Impl_V8BreakIteratorPrototypeCurrentENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_V8BreakIteratorPrototypeCurrentENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_V8BreakIteratorPrototypeCurrentENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB19462:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L973
.L976:
	leaq	.LC36(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$42, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1000
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L979:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L987
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L987:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1001
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L973:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1088, 11(%rdx)
	jne	.L976
	movq	41112(%r12), %rdi
	movq	71(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1002
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L980:
	cmpq	%rsi, 88(%r12)
	je	.L982
	movq	%rsi, %r13
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1000:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	%r14, %rax
	cmpq	41096(%r12), %r14
	je	.L1003
.L981:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L980
.L1003:
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L982:
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	$1060, %edx
	call	_ZN2v88internal12_GLOBAL__N_119CreateBoundFunctionEPNS0_7IsolateENS0_6HandleINS0_8JSObjectEEENS0_8Builtins4NameEi
	movq	0(%r13), %rdi
	movq	(%rax), %r13
	movq	%rax, %r15
	leaq	71(%rdi), %rsi
	movq	%r13, 71(%rdi)
	testb	$1, %r13b
	je	.L988
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	je	.L984
	movq	%r13, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	8(%rcx), %rax
.L984:
	testb	$24, %al
	je	.L988
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L988
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L988:
	movq	(%r15), %r13
	jmp	.L979
.L1001:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19462:
	.size	_ZN2v88internalL44Builtin_Impl_V8BreakIteratorPrototypeCurrentENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_V8BreakIteratorPrototypeCurrentENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8928:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L1010
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L1013
.L1004:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1010:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L1013:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1004
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8928:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC38:
	.string	"V8.Builtin_StringPrototypeNormalizeIntl"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE:
.LFB19250:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1043
.L1015:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic46(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1044
.L1017:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1045
.L1019:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL41Builtin_Impl_StringPrototypeNormalizeIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1046
.L1023:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1047
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1044:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1048
.L1018:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic46(%rip)
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1045:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1049
.L1020:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1021
	movq	(%rdi), %rax
	call	*8(%rax)
.L1021:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1022
	movq	(%rdi), %rax
	call	*8(%rax)
.L1022:
	leaq	.LC38(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1046:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$918, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1049:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC38(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1048:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1018
.L1047:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19250:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"V8.Builtin_IntlGetCanonicalLocales"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE:
.LFB19303:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1085
.L1051:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_IntlGetCanonicalLocalesEiPmPNS0_7IsolateEE28trace_event_unique_atomic546(%rip), %r13
	testq	%r13, %r13
	je	.L1086
.L1053:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1087
.L1055:
	addl	$1, 41104(%r12)
	leaq	88(%r12), %rax
	cmpl	$5, %r14d
	movq	%r12, %rdi
	leaq	-8(%rbx), %rsi
	movq	41088(%r12), %r13
	movq	41096(%r12), %r15
	cmovle	%rax, %rsi
	call	_ZN2v88internal4Intl19GetCanonicalLocalesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L1088
	movq	(%rax), %r14
.L1062:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L1065
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1065:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1089
.L1050:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1090
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1087:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1091
.L1056:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1057
	movq	(%rdi), %rax
	call	*8(%rax)
.L1057:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1058
	movq	(%rdi), %rax
	call	*8(%rax)
.L1058:
	leaq	.LC39(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1086:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1092
.L1054:
	movq	%r13, _ZZN2v88internalL42Builtin_Impl_Stats_IntlGetCanonicalLocalesEiPmPNS0_7IsolateEE28trace_event_unique_atomic546(%rip)
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1088:
	movq	312(%r12), %r14
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1089:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1050
	.p2align 4,,10
	.p2align 3
.L1085:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$876, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1092:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1091:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC39(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1056
.L1090:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19303:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC40:
	.string	"V8.Builtin_StringPrototypeToLocaleLowerCase"
	.section	.text._ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE, @function
_ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE:
.LFB19376:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1122
.L1094:
	movq	_ZZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateEE28trace_event_unique_atomic811(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1123
.L1096:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1124
.L1098:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleLowerCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1125
.L1102:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1126
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1123:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1127
.L1097:
	movq	%rbx, _ZZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateEE28trace_event_unique_atomic811(%rip)
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1124:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1128
.L1099:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1100
	movq	(%rdi), %rax
	call	*8(%rax)
.L1100:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1101
	movq	(%rdi), %rax
	call	*8(%rax)
.L1101:
	leaq	.LC40(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1125:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$919, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1128:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC40(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1127:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1097
.L1126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19376:
	.size	_ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE, .-_ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC41:
	.string	"V8.Builtin_StringPrototypeToLocaleUpperCase"
	.section	.text._ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE, @function
_ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE:
.LFB19379:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1158
.L1130:
	movq	_ZZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateEE28trace_event_unique_atomic823(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1159
.L1132:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1160
.L1134:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleUpperCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1161
.L1138:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1162
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1159:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1163
.L1133:
	movq	%rbx, _ZZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateEE28trace_event_unique_atomic823(%rip)
	jmp	.L1132
	.p2align 4,,10
	.p2align 3
.L1160:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1164
.L1135:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1136
	movq	(%rdi), %rax
	call	*8(%rax)
.L1136:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1137
	movq	(%rdi), %rax
	call	*8(%rax)
.L1137:
	leaq	.LC41(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1161:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1158:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$920, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1164:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC41(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1163:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1133
.L1162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19379:
	.size	_ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE, .-_ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC42:
	.string	"V8.Builtin_V8BreakIteratorSupportedLocalesOf"
	.align 8
.LC43:
	.string	"Intl.v8BreakIterator.supportedLocalesOf"
	.section	.text._ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19253:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1202
.L1166:
	movq	_ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateEE27trace_event_unique_atomic57(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1203
.L1168:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1204
.L1170:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	cmpl	$5, %r15d
	jg	.L1174
	leaq	88(%r12), %r14
	movq	%r14, %rcx
.L1177:
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11Ev@PLT
	movq	-168(%rbp), %rcx
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC43(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L1205
	movq	(%rax), %r14
.L1179:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1182
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1182:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1206
.L1165:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1207
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1174:
	.cfi_restore_state
	leaq	-8(%r14), %rcx
	cmpl	$6, %r15d
	je	.L1208
	subq	$16, %r14
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1204:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1209
.L1171:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1172
	movq	(%rdi), %rax
	call	*8(%rax)
.L1172:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1173
	movq	(%rdi), %rax
	call	*8(%rax)
.L1173:
	leaq	.LC42(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1203:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1210
.L1169:
	movq	%rbx, _ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateEE27trace_event_unique_atomic57(%rip)
	jmp	.L1168
	.p2align 4,,10
	.p2align 3
.L1205:
	movq	312(%r12), %r14
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1206:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1202:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$934, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1210:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1209:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC42(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1171
.L1207:
	call	__stack_chk_fail@PLT
.L1208:
	leaq	88(%r12), %r14
	jmp	.L1177
	.cfi_endproc
.LFE19253:
	.size	_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL49Builtin_Impl_Stats_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC44:
	.string	"V8.Builtin_NumberFormatSupportedLocalesOf"
	.align 8
.LC45:
	.string	"Intl.NumberFormat.supportedLocalesOf"
	.section	.text._ZN2v88internalL49Builtin_Impl_Stats_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL49Builtin_Impl_Stats_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internalL49Builtin_Impl_Stats_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19256:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1248
.L1212:
	movq	_ZZN2v88internalL49Builtin_Impl_Stats_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateEE27trace_event_unique_atomic68(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1249
.L1214:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1250
.L1216:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	cmpl	$5, %r15d
	jg	.L1220
	leaq	88(%r12), %r14
	movq	%r14, %rcx
.L1223:
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal14JSNumberFormat19GetAvailableLocalesB5cxx11Ev@PLT
	movq	-168(%rbp), %rcx
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC45(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L1251
	movq	(%rax), %r14
.L1225:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1228
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1228:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1252
.L1211:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1253
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1220:
	.cfi_restore_state
	leaq	-8(%r14), %rcx
	cmpl	$6, %r15d
	je	.L1254
	subq	$16, %r14
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1250:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1255
.L1217:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1218
	movq	(%rdi), %rax
	call	*8(%rax)
.L1218:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1219
	movq	(%rdi), %rax
	call	*8(%rax)
.L1219:
	leaq	.LC44(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1249:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1256
.L1215:
	movq	%rbx, _ZZN2v88internalL49Builtin_Impl_Stats_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateEE27trace_event_unique_atomic68(%rip)
	jmp	.L1214
	.p2align 4,,10
	.p2align 3
.L1251:
	movq	312(%r12), %r14
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1252:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$899, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1256:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1255:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC44(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1217
.L1253:
	call	__stack_chk_fail@PLT
.L1254:
	leaq	88(%r12), %r14
	jmp	.L1223
	.cfi_endproc
.LFE19256:
	.size	_ZN2v88internalL49Builtin_Impl_Stats_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internalL49Builtin_Impl_Stats_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL51Builtin_Impl_Stats_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC46:
	.string	"V8.Builtin_DateTimeFormatSupportedLocalesOf"
	.align 8
.LC47:
	.string	"Intl.DateTimeFormat.supportedLocalesOf"
	.section	.text._ZN2v88internalL51Builtin_Impl_Stats_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL51Builtin_Impl_Stats_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internalL51Builtin_Impl_Stats_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19265:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1294
.L1258:
	movq	_ZZN2v88internalL51Builtin_Impl_Stats_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic116(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1295
.L1260:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1296
.L1262:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	cmpl	$5, %r15d
	jg	.L1266
	leaq	88(%r12), %r14
	movq	%r14, %rcx
.L1269:
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal16JSDateTimeFormat19GetAvailableLocalesB5cxx11Ev@PLT
	movq	-168(%rbp), %rcx
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC47(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L1297
	movq	(%rax), %r14
.L1271:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1274
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1274:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1298
.L1257:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1299
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1266:
	.cfi_restore_state
	leaq	-8(%r14), %rcx
	cmpl	$6, %r15d
	je	.L1300
	subq	$16, %r14
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1296:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1301
.L1263:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1264
	movq	(%rdi), %rax
	call	*8(%rax)
.L1264:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1265
	movq	(%rdi), %rax
	call	*8(%rax)
.L1265:
	leaq	.LC46(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1262
	.p2align 4,,10
	.p2align 3
.L1295:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1302
.L1261:
	movq	%rbx, _ZZN2v88internalL51Builtin_Impl_Stats_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic116(%rip)
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1297:
	movq	312(%r12), %r14
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1298:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1294:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$875, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1258
	.p2align 4,,10
	.p2align 3
.L1302:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1301:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC46(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1263
.L1299:
	call	__stack_chk_fail@PLT
.L1300:
	leaq	88(%r12), %r14
	jmp	.L1269
	.cfi_endproc
.LFE19265:
	.size	_ZN2v88internalL51Builtin_Impl_Stats_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internalL51Builtin_Impl_Stats_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC48:
	.string	"V8.Builtin_ListFormatSupportedLocalesOf"
	.align 8
.LC49:
	.string	"Intl.ListFormat.supportedLocalesOf"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19312:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1340
.L1304:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic569(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1341
.L1306:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1342
.L1308:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	cmpl	$5, %r15d
	jg	.L1312
	leaq	88(%r12), %r14
	movq	%r14, %rcx
.L1315:
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11Ev@PLT
	movq	-168(%rbp), %rcx
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC49(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L1343
	movq	(%rax), %r14
.L1317:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1320
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1320:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1344
.L1303:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1345
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1312:
	.cfi_restore_state
	leaq	-8(%r14), %rcx
	cmpl	$6, %r15d
	je	.L1346
	subq	$16, %r14
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1342:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1347
.L1309:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1310
	movq	(%rdi), %rax
	call	*8(%rax)
.L1310:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1311
	movq	(%rdi), %rax
	call	*8(%rax)
.L1311:
	leaq	.LC48(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1341:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1348
.L1307:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic569(%rip)
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1343:
	movq	312(%r12), %r14
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1344:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1340:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$879, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1348:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1347:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC48(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1309
.L1345:
	call	__stack_chk_fail@PLT
.L1346:
	leaq	88(%r12), %r14
	jmp	.L1315
	.cfi_endproc
.LFE19312:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL55Builtin_Impl_Stats_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC50:
	.string	"V8.Builtin_RelativeTimeFormatSupportedLocalesOf"
	.align 8
.LC51:
	.string	"Intl.RelativeTimeFormat.supportedLocalesOf"
	.section	.text._ZN2v88internalL55Builtin_Impl_Stats_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL55Builtin_Impl_Stats_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internalL55Builtin_Impl_Stats_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19328:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1386
.L1350:
	movq	_ZZN2v88internalL55Builtin_Impl_Stats_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic674(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1387
.L1352:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1388
.L1354:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	cmpl	$5, %r15d
	jg	.L1358
	leaq	88(%r12), %r14
	movq	%r14, %rcx
.L1361:
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal20JSRelativeTimeFormat19GetAvailableLocalesB5cxx11Ev@PLT
	movq	-168(%rbp), %rcx
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC51(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L1389
	movq	(%rax), %r14
.L1363:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1366
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1366:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1390
.L1349:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1391
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1358:
	.cfi_restore_state
	leaq	-8(%r14), %rcx
	cmpl	$6, %r15d
	je	.L1392
	subq	$16, %r14
	jmp	.L1361
	.p2align 4,,10
	.p2align 3
.L1388:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1393
.L1355:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1356
	movq	(%rdi), %rax
	call	*8(%rax)
.L1356:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1357
	movq	(%rdi), %rax
	call	*8(%rax)
.L1357:
	leaq	.LC50(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1387:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1394
.L1353:
	movq	%rbx, _ZZN2v88internalL55Builtin_Impl_Stats_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic674(%rip)
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	312(%r12), %r14
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1390:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1386:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$908, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1394:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1393:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC50(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1355
.L1391:
	call	__stack_chk_fail@PLT
.L1392:
	leaq	88(%r12), %r14
	jmp	.L1361
	.cfi_endproc
.LFE19328:
	.size	_ZN2v88internalL55Builtin_Impl_Stats_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internalL55Builtin_Impl_Stats_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL48Builtin_Impl_Stats_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC52:
	.string	"V8.Builtin_PluralRulesSupportedLocalesOf"
	.align 8
.LC53:
	.string	"Intl.PluralRules.supportedLocalesOf"
	.section	.text._ZN2v88internalL48Builtin_Impl_Stats_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Builtin_Impl_Stats_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internalL48Builtin_Impl_Stats_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19391:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1432
.L1396:
	movq	_ZZN2v88internalL48Builtin_Impl_Stats_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic871(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1433
.L1398:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1434
.L1400:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	cmpl	$5, %r15d
	jg	.L1404
	leaq	88(%r12), %r14
	movq	%r14, %rcx
.L1407:
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11Ev@PLT
	movq	-168(%rbp), %rcx
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC53(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L1435
	movq	(%rax), %r14
.L1409:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1412
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1412:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1436
.L1395:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1437
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1404:
	.cfi_restore_state
	leaq	-8(%r14), %rcx
	cmpl	$6, %r15d
	je	.L1438
	subq	$16, %r14
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1434:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1439
.L1401:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1402
	movq	(%rdi), %rax
	call	*8(%rax)
.L1402:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1403
	movq	(%rdi), %rax
	call	*8(%rax)
.L1403:
	leaq	.LC52(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1433:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1440
.L1399:
	movq	%rbx, _ZZN2v88internalL48Builtin_Impl_Stats_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic871(%rip)
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1435:
	movq	312(%r12), %r14
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1436:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$903, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1396
	.p2align 4,,10
	.p2align 3
.L1440:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1439:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC52(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1401
.L1437:
	call	__stack_chk_fail@PLT
.L1438:
	leaq	88(%r12), %r14
	jmp	.L1407
	.cfi_endproc
.LFE19391:
	.size	_ZN2v88internalL48Builtin_Impl_Stats_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internalL48Builtin_Impl_Stats_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_Stats_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC54:
	.string	"V8.Builtin_CollatorSupportedLocalesOf"
	.align 8
.LC55:
	.string	"Intl.Collator.supportedLocalesOf"
	.section	.text._ZN2v88internalL45Builtin_Impl_Stats_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_Stats_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_Stats_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19400:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1478
.L1442:
	movq	_ZZN2v88internalL45Builtin_Impl_Stats_CollatorSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic897(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1479
.L1444:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1480
.L1446:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	cmpl	$5, %r15d
	jg	.L1450
	leaq	88(%r12), %r14
	movq	%r14, %rcx
.L1453:
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11Ev@PLT
	movq	-168(%rbp), %rcx
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC55(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L1481
	movq	(%rax), %r14
.L1455:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1458
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1458:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1482
.L1441:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1483
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1450:
	.cfi_restore_state
	leaq	-8(%r14), %rcx
	cmpl	$6, %r15d
	je	.L1484
	subq	$16, %r14
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1480:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1485
.L1447:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1448
	movq	(%rdi), %rax
	call	*8(%rax)
.L1448:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1449
	movq	(%rdi), %rax
	call	*8(%rax)
.L1449:
	leaq	.LC54(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1479:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1486
.L1445:
	movq	%rbx, _ZZN2v88internalL45Builtin_Impl_Stats_CollatorSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic897(%rip)
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1481:
	movq	312(%r12), %r14
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1482:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1478:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$863, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1486:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1485:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC54(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1447
.L1483:
	call	__stack_chk_fail@PLT
.L1484:
	leaq	88(%r12), %r14
	jmp	.L1453
	.cfi_endproc
.LFE19400:
	.size	_ZN2v88internalL45Builtin_Impl_Stats_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_Stats_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_Stats_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC56:
	.string	"V8.Builtin_SegmenterSupportedLocalesOf"
	.align 8
.LC57:
	.string	"Intl.Segmenter.supportedLocalesOf"
	.section	.text._ZN2v88internalL46Builtin_Impl_Stats_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_Stats_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_Stats_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19427:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1524
.L1488:
	movq	_ZZN2v88internalL46Builtin_Impl_Stats_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateEE29trace_event_unique_atomic1032(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1525
.L1490:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1526
.L1492:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	cmpl	$5, %r15d
	jg	.L1496
	leaq	88(%r12), %r14
	movq	%r14, %rcx
.L1499:
	movq	%rcx, -168(%rbp)
	call	_ZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11Ev@PLT
	movq	-168(%rbp), %rcx
	movq	%r14, %r8
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC57(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L1527
	movq	(%rax), %r14
.L1501:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L1504
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1504:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1528
.L1487:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1529
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1496:
	.cfi_restore_state
	leaq	-8(%r14), %rcx
	cmpl	$6, %r15d
	je	.L1530
	subq	$16, %r14
	jmp	.L1499
	.p2align 4,,10
	.p2align 3
.L1526:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1531
.L1493:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1494
	movq	(%rdi), %rax
	call	*8(%rax)
.L1494:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1495
	movq	(%rdi), %rax
	call	*8(%rax)
.L1495:
	leaq	.LC56(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1492
	.p2align 4,,10
	.p2align 3
.L1525:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1532
.L1491:
	movq	%rbx, _ZZN2v88internalL46Builtin_Impl_Stats_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateEE29trace_event_unique_atomic1032(%rip)
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1527:
	movq	312(%r12), %r14
	jmp	.L1501
	.p2align 4,,10
	.p2align 3
.L1528:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1487
	.p2align 4,,10
	.p2align 3
.L1524:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$912, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1532:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1491
	.p2align 4,,10
	.p2align 3
.L1531:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC56(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1493
.L1529:
	call	__stack_chk_fail@PLT
.L1530:
	leaq	88(%r12), %r14
	jmp	.L1499
	.cfi_endproc
.LFE19427:
	.size	_ZN2v88internalL46Builtin_Impl_Stats_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_Stats_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL53Builtin_Impl_Stats_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC58:
	.string	"V8.Builtin_NumberFormatPrototypeFormatToParts"
	.section	.text._ZN2v88internalL53Builtin_Impl_Stats_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL53Builtin_Impl_Stats_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL53Builtin_Impl_Stats_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE:
.LFB19259:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1562
.L1534:
	movq	_ZZN2v88internalL53Builtin_Impl_Stats_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE27trace_event_unique_atomic79(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1563
.L1536:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1564
.L1538:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL47Builtin_Impl_NumberFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1565
.L1542:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1566
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1563:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1567
.L1537:
	movq	%rbx, _ZZN2v88internalL53Builtin_Impl_Stats_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE27trace_event_unique_atomic79(%rip)
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L1564:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1568
.L1539:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1540
	movq	(%rdi), %rax
	call	*8(%rax)
.L1540:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1541
	movq	(%rdi), %rax
	call	*8(%rax)
.L1541:
	leaq	.LC58(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1538
	.p2align 4,,10
	.p2align 3
.L1565:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1542
	.p2align 4,,10
	.p2align 3
.L1562:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$897, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1534
	.p2align 4,,10
	.p2align 3
.L1568:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC58(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1539
	.p2align 4,,10
	.p2align 3
.L1567:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1537
.L1566:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19259:
	.size	_ZN2v88internalL53Builtin_Impl_Stats_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE, .-_ZN2v88internalL53Builtin_Impl_Stats_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL57Builtin_Impl_Stats_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC59:
	.string	"V8.Builtin_DateTimeFormatPrototypeResolvedOptions"
	.section	.text._ZN2v88internalL57Builtin_Impl_Stats_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL57Builtin_Impl_Stats_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL57Builtin_Impl_Stats_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19262:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1598
.L1570:
	movq	_ZZN2v88internalL57Builtin_Impl_Stats_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic101(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1599
.L1572:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1600
.L1574:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL51Builtin_Impl_DateTimeFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1601
.L1578:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1602
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1599:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1603
.L1573:
	movq	%rbx, _ZZN2v88internalL57Builtin_Impl_Stats_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic101(%rip)
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1600:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1604
.L1575:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1576
	movq	(%rdi), %rax
	call	*8(%rax)
.L1576:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1577
	movq	(%rdi), %rax
	call	*8(%rax)
.L1577:
	leaq	.LC59(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1574
	.p2align 4,,10
	.p2align 3
.L1601:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1598:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$874, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1570
	.p2align 4,,10
	.p2align 3
.L1604:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC59(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1603:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1573
.L1602:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19262:
	.size	_ZN2v88internalL57Builtin_Impl_Stats_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internalL57Builtin_Impl_Stats_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC60:
	.string	"V8.Builtin_DateTimeFormatInternalFormat"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE:
.LFB19300:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1647
.L1606:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_DateTimeFormatInternalFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic528(%rip), %r13
	testq	%r13, %r13
	je	.L1648
.L1608:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1649
.L1610:
	movq	41112(%r12), %rdi
	movq	41088(%r12), %r13
	movq	41096(%r12), %r14
	movq	12464(%r12), %rsi
	addl	$1, 41104(%r12)
	testq	%rdi, %rdi
	je	.L1614
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1615:
	movq	47(%rsi), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1617
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1618:
	subq	$8, %rbx
	leaq	88(%r12), %rdx
	cmpl	$5, %r15d
	movq	%r12, %rdi
	cmovg	%rbx, %rdx
	call	_ZN2v88internal16JSDateTimeFormat14DateTimeFormatEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L1650
	movq	(%rax), %r15
.L1623:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1626
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1626:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1651
.L1605:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1652
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1617:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1653
.L1619:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L1618
	.p2align 4,,10
	.p2align 3
.L1614:
	movq	%r13, %rax
	cmpq	%r14, %r13
	je	.L1654
.L1616:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1649:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1655
.L1611:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1612
	movq	(%rdi), %rax
	call	*8(%rax)
.L1612:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1613
	movq	(%rdi), %rax
	call	*8(%rax)
.L1613:
	leaq	.LC60(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1610
	.p2align 4,,10
	.p2align 3
.L1648:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1656
.L1609:
	movq	%r13, _ZZN2v88internalL47Builtin_Impl_Stats_DateTimeFormatInternalFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic528(%rip)
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1650:
	movq	312(%r12), %r15
	jmp	.L1623
	.p2align 4,,10
	.p2align 3
.L1651:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1647:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$869, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1654:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	jmp	.L1616
	.p2align 4,,10
	.p2align 3
.L1653:
	movq	%r12, %rdi
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L1619
	.p2align 4,,10
	.p2align 3
.L1656:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1609
	.p2align 4,,10
	.p2align 3
.L1655:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC60(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1611
.L1652:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19300:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC61:
	.string	"V8.Builtin_V8BreakIteratorInternalAdoptText"
	.section	.text._ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE, @function
_ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE:
.LFB19445:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1686
.L1658:
	movq	_ZZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1100(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1687
.L1660:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1688
.L1662:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL45Builtin_Impl_V8BreakIteratorInternalAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1689
.L1666:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1690
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1687:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1691
.L1661:
	movq	%rbx, _ZZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1100(%rip)
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1688:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1692
.L1663:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1664
	movq	(%rdi), %rax
	call	*8(%rax)
.L1664:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1665
	movq	(%rdi), %rax
	call	*8(%rax)
.L1665:
	leaq	.LC61(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L1662
	.p2align 4,,10
	.p2align 3
.L1689:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1666
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$923, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1692:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC61(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L1691:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1661
.L1690:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19445:
	.size	_ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE, .-_ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC62:
	.string	"V8.Builtin_V8BreakIteratorInternalFirst"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE:
.LFB19451:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1731
.L1694:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateEE29trace_event_unique_atomic1136(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1732
.L1696:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1733
.L1698:
	movq	41112(%r12), %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	movq	12464(%r12), %rsi
	addl	$1, 41104(%r12)
	testq	%rdi, %rdi
	je	.L1702
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1703:
	movq	47(%rsi), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1705
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1706:
	movq	%r12, %rdi
	call	_ZN2v88internal17JSV8BreakIterator5FirstEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L1710
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1710:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1734
.L1693:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1735
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1705:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1736
.L1707:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1706
	.p2align 4,,10
	.p2align 3
.L1702:
	movq	%rbx, %rax
	cmpq	%r13, %rbx
	je	.L1737
.L1704:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1703
	.p2align 4,,10
	.p2align 3
.L1733:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1738
.L1699:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1700
	movq	(%rdi), %rax
	call	*8(%rax)
.L1700:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1701
	movq	(%rdi), %rax
	call	*8(%rax)
.L1701:
	leaq	.LC62(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1732:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1739
.L1697:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateEE29trace_event_unique_atomic1136(%rip)
	jmp	.L1696
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$926, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1694
	.p2align 4,,10
	.p2align 3
.L1734:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1737:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1736:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1739:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1697
	.p2align 4,,10
	.p2align 3
.L1738:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC62(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1699
.L1735:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19451:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_Stats_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC63:
	.string	"V8.Builtin_V8BreakIteratorInternalNext"
	.section	.text._ZN2v88internalL46Builtin_Impl_Stats_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_Stats_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_Stats_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE:
.LFB19457:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1778
.L1741:
	movq	_ZZN2v88internalL46Builtin_Impl_Stats_V8BreakIteratorInternalNextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1166(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1779
.L1743:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1780
.L1745:
	movq	41112(%r12), %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	movq	12464(%r12), %rsi
	addl	$1, 41104(%r12)
	testq	%rdi, %rdi
	je	.L1749
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1750:
	movq	47(%rsi), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1752
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1753:
	movq	%r12, %rdi
	call	_ZN2v88internal17JSV8BreakIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L1757
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1757:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1781
.L1740:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1782
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1752:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1783
.L1754:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1749:
	movq	%rbx, %rax
	cmpq	%r13, %rbx
	je	.L1784
.L1751:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1750
	.p2align 4,,10
	.p2align 3
.L1780:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1785
.L1746:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1747
	movq	(%rdi), %rax
	call	*8(%rax)
.L1747:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1748
	movq	(%rdi), %rax
	call	*8(%rax)
.L1748:
	leaq	.LC63(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1745
	.p2align 4,,10
	.p2align 3
.L1779:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1786
.L1744:
	movq	%rbx, _ZZN2v88internalL46Builtin_Impl_Stats_V8BreakIteratorInternalNextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1166(%rip)
	jmp	.L1743
	.p2align 4,,10
	.p2align 3
.L1778:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$927, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1781:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1740
	.p2align 4,,10
	.p2align 3
.L1784:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L1751
	.p2align 4,,10
	.p2align 3
.L1783:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1754
	.p2align 4,,10
	.p2align 3
.L1786:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1744
	.p2align 4,,10
	.p2align 3
.L1785:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC63(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1746
.L1782:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19457:
	.size	_ZN2v88internalL46Builtin_Impl_Stats_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_Stats_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL49Builtin_Impl_Stats_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC64:
	.string	"V8.Builtin_V8BreakIteratorInternalCurrent"
	.section	.text._ZN2v88internalL49Builtin_Impl_Stats_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL49Builtin_Impl_Stats_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE, @function
_ZN2v88internalL49Builtin_Impl_Stats_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE:
.LFB19463:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1825
.L1788:
	movq	_ZZN2v88internalL49Builtin_Impl_Stats_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1195(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1826
.L1790:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1827
.L1792:
	movq	41112(%r12), %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	movq	12464(%r12), %rsi
	addl	$1, 41104(%r12)
	testq	%rdi, %rdi
	je	.L1796
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1797:
	movq	47(%rsi), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1799
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1800:
	movq	%r12, %rdi
	call	_ZN2v88internal17JSV8BreakIterator7CurrentEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L1804
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1804:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1828
.L1787:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1829
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1799:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1830
.L1801:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1796:
	movq	%rbx, %rax
	cmpq	%r13, %rbx
	je	.L1831
.L1798:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1797
	.p2align 4,,10
	.p2align 3
.L1827:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1832
.L1793:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1794
	movq	(%rdi), %rax
	call	*8(%rax)
.L1794:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1795
	movq	(%rdi), %rax
	call	*8(%rax)
.L1795:
	leaq	.LC64(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1792
	.p2align 4,,10
	.p2align 3
.L1826:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1833
.L1791:
	movq	%rbx, _ZZN2v88internalL49Builtin_Impl_Stats_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1195(%rip)
	jmp	.L1790
	.p2align 4,,10
	.p2align 3
.L1825:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$925, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1788
	.p2align 4,,10
	.p2align 3
.L1828:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1831:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1830:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1801
	.p2align 4,,10
	.p2align 3
.L1833:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1832:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC64(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1793
.L1829:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19463:
	.size	_ZN2v88internalL49Builtin_Impl_Stats_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE, .-_ZN2v88internalL49Builtin_Impl_Stats_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC65:
	.string	"V8.Builtin_V8BreakIteratorInternalBreakType"
	.section	.text._ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE:
.LFB19469:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1872
.L1835:
	movq	_ZZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1224(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1873
.L1837:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1874
.L1839:
	movq	41112(%r12), %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	movq	12464(%r12), %rsi
	addl	$1, 41104(%r12)
	testq	%rdi, %rdi
	je	.L1843
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1844:
	movq	47(%rsi), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1846
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1847:
	movq	%r12, %rdi
	call	_ZN2v88internal17JSV8BreakIterator9BreakTypeEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %r13
	je	.L1851
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1851:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1875
.L1834:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1876
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1846:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1877
.L1848:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L1847
	.p2align 4,,10
	.p2align 3
.L1843:
	movq	%rbx, %rax
	cmpq	%r13, %rbx
	je	.L1878
.L1845:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1874:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1879
.L1840:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1841
	movq	(%rdi), %rax
	call	*8(%rax)
.L1841:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1842
	movq	(%rdi), %rax
	call	*8(%rax)
.L1842:
	leaq	.LC65(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1873:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1880
.L1838:
	movq	%rbx, _ZZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1224(%rip)
	jmp	.L1837
	.p2align 4,,10
	.p2align 3
.L1872:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$924, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1835
	.p2align 4,,10
	.p2align 3
.L1875:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1834
	.p2align 4,,10
	.p2align 3
.L1878:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1877:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1848
	.p2align 4,,10
	.p2align 3
.L1880:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1838
	.p2align 4,,10
	.p2align 3
.L1879:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC65(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1840
.L1876:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19469:
	.size	_ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE, .-_ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeLanguageEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC66:
	.string	"V8.Builtin_LocalePrototypeLanguage"
	.align 8
.LC67:
	.string	"Intl.Locale.prototype.language"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeLanguageEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeLanguageEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeLanguageEiPmPNS0_7IsolateE:
.LFB19337:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1918
.L1882:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeLanguageEiPmPNS0_7IsolateEE28trace_event_unique_atomic718(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1919
.L1884:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1920
.L1886:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1890
.L1893:
	leaq	.LC67(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$30, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1921
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L1894:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1897
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1897:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1922
.L1881:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1923
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1890:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L1893
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale8LanguageEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L1894
	.p2align 4,,10
	.p2align 3
.L1920:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1924
.L1887:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1888
	movq	(%rdi), %rax
	call	*8(%rax)
.L1888:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1889
	movq	(%rdi), %rax
	call	*8(%rax)
.L1889:
	leaq	.LC66(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1919:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1925
.L1885:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeLanguageEiPmPNS0_7IsolateEE28trace_event_unique_atomic718(%rip)
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1918:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$886, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1882
	.p2align 4,,10
	.p2align 3
.L1922:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1881
	.p2align 4,,10
	.p2align 3
.L1921:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1925:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1885
	.p2align 4,,10
	.p2align 3
.L1924:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC66(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1887
.L1923:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19337:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeLanguageEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeLanguageEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC68:
	.string	"V8.Builtin_LocalePrototypeScript"
	.section	.rodata._ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC69:
	.string	"Intl.Locale.prototype.script"
	.section	.text._ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateE:
.LFB19340:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1963
.L1927:
	movq	_ZZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateEE28trace_event_unique_atomic726(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1964
.L1929:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1965
.L1931:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1935
.L1938:
	leaq	.LC69(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$28, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1966
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L1939:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1942
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1942:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1967
.L1926:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1968
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1935:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L1938
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale6ScriptEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L1939
	.p2align 4,,10
	.p2align 3
.L1965:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1969
.L1932:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1933
	movq	(%rdi), %rax
	call	*8(%rax)
.L1933:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1934
	movq	(%rdi), %rax
	call	*8(%rax)
.L1934:
	leaq	.LC68(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1931
	.p2align 4,,10
	.p2align 3
.L1964:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1970
.L1930:
	movq	%rbx, _ZZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateEE28trace_event_unique_atomic726(%rip)
	jmp	.L1929
	.p2align 4,,10
	.p2align 3
.L1963:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$892, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1927
	.p2align 4,,10
	.p2align 3
.L1967:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1926
	.p2align 4,,10
	.p2align 3
.L1966:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1970:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1930
	.p2align 4,,10
	.p2align 3
.L1969:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC68(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1932
.L1968:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19340:
	.size	_ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC70:
	.string	"V8.Builtin_LocalePrototypeRegion"
	.section	.rodata._ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC71:
	.string	"Intl.Locale.prototype.region"
	.section	.text._ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateE:
.LFB19343:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2008
.L1972:
	movq	_ZZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateEE28trace_event_unique_atomic733(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2009
.L1974:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2010
.L1976:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1980
.L1983:
	leaq	.LC71(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$28, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2011
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L1984:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1987
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1987:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2012
.L1971:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2013
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1980:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L1983
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale6RegionEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L1984
	.p2align 4,,10
	.p2align 3
.L2010:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2014
.L1977:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1978
	movq	(%rdi), %rax
	call	*8(%rax)
.L1978:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1979
	movq	(%rdi), %rax
	call	*8(%rax)
.L1979:
	leaq	.LC70(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1976
	.p2align 4,,10
	.p2align 3
.L2009:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2015
.L1975:
	movq	%rbx, _ZZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateEE28trace_event_unique_atomic733(%rip)
	jmp	.L1974
	.p2align 4,,10
	.p2align 3
.L2008:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$891, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1972
	.p2align 4,,10
	.p2align 3
.L2012:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1971
	.p2align 4,,10
	.p2align 3
.L2011:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2015:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L2014:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC70(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1977
.L2013:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19343:
	.size	_ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeBaseNameEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC72:
	.string	"V8.Builtin_LocalePrototypeBaseName"
	.align 8
.LC73:
	.string	"Intl.Locale.prototype.baseName"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeBaseNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeBaseNameEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeBaseNameEiPmPNS0_7IsolateE:
.LFB19346:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2053
.L2017:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeBaseNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic740(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2054
.L2019:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2055
.L2021:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2025
.L2028:
	leaq	.LC73(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$30, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2056
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2029:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2032
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2032:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2057
.L2016:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2058
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2025:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L2028
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale8BaseNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L2029
	.p2align 4,,10
	.p2align 3
.L2055:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2059
.L2022:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2023
	movq	(%rdi), %rax
	call	*8(%rax)
.L2023:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2024
	movq	(%rdi), %rax
	call	*8(%rax)
.L2024:
	leaq	.LC72(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2021
	.p2align 4,,10
	.p2align 3
.L2054:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2060
.L2020:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeBaseNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic740(%rip)
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2053:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$881, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2017
	.p2align 4,,10
	.p2align 3
.L2057:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2016
	.p2align 4,,10
	.p2align 3
.L2056:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2060:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2059:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC72(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2022
.L2058:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19346:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeBaseNameEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeBaseNameEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeCalendarEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC74:
	.string	"V8.Builtin_LocalePrototypeCalendar"
	.align 8
.LC75:
	.string	"Intl.Locale.prototype.calendar"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeCalendarEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeCalendarEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeCalendarEiPmPNS0_7IsolateE:
.LFB19349:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2098
.L2062:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeCalendarEiPmPNS0_7IsolateEE28trace_event_unique_atomic747(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2099
.L2064:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2100
.L2066:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2070
.L2073:
	leaq	.LC75(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$30, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2101
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2074:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2077
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2077:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2102
.L2061:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2103
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2070:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L2073
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale8CalendarEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L2074
	.p2align 4,,10
	.p2align 3
.L2100:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2104
.L2067:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2068
	movq	(%rdi), %rax
	call	*8(%rax)
.L2068:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2069
	movq	(%rdi), %rax
	call	*8(%rax)
.L2069:
	leaq	.LC74(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2066
	.p2align 4,,10
	.p2align 3
.L2099:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2105
.L2065:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeCalendarEiPmPNS0_7IsolateEE28trace_event_unique_atomic747(%rip)
	jmp	.L2064
	.p2align 4,,10
	.p2align 3
.L2098:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$882, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2102:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2061
	.p2align 4,,10
	.p2align 3
.L2101:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2105:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2065
	.p2align 4,,10
	.p2align 3
.L2104:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC74(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2067
.L2103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19349:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeCalendarEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeCalendarEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC76:
	.string	"V8.Builtin_LocalePrototypeCaseFirst"
	.align 8
.LC77:
	.string	"Intl.Locale.prototype.caseFirst"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE:
.LFB19352:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2143
.L2107:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCaseFirstEiPmPNS0_7IsolateEE28trace_event_unique_atomic754(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2144
.L2109:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2145
.L2111:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2115
.L2118:
	leaq	.LC77(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$31, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2146
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2119:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2122
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2122:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2147
.L2106:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2148
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2115:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L2118
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale9CaseFirstEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L2119
	.p2align 4,,10
	.p2align 3
.L2145:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2149
.L2112:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2113
	movq	(%rdi), %rax
	call	*8(%rax)
.L2113:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2114
	movq	(%rdi), %rax
	call	*8(%rax)
.L2114:
	leaq	.LC76(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2111
	.p2align 4,,10
	.p2align 3
.L2144:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2150
.L2110:
	movq	%rbx, _ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCaseFirstEiPmPNS0_7IsolateEE28trace_event_unique_atomic754(%rip)
	jmp	.L2109
	.p2align 4,,10
	.p2align 3
.L2143:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$883, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2107
	.p2align 4,,10
	.p2align 3
.L2147:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2106
	.p2align 4,,10
	.p2align 3
.L2146:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2150:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2110
	.p2align 4,,10
	.p2align 3
.L2149:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC76(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2112
.L2148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19352:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCollationEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC78:
	.string	"V8.Builtin_LocalePrototypeCollation"
	.align 8
.LC79:
	.string	"Intl.Locale.prototype.collation"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCollationEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCollationEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCollationEiPmPNS0_7IsolateE:
.LFB19355:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2188
.L2152:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCollationEiPmPNS0_7IsolateEE28trace_event_unique_atomic761(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2189
.L2154:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2190
.L2156:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2160
.L2163:
	leaq	.LC79(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$31, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2191
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2164:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2167
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2167:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2192
.L2151:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2193
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2160:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L2163
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale9CollationEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L2164
	.p2align 4,,10
	.p2align 3
.L2190:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2194
.L2157:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2158
	movq	(%rdi), %rax
	call	*8(%rax)
.L2158:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2159
	movq	(%rdi), %rax
	call	*8(%rax)
.L2159:
	leaq	.LC78(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2156
	.p2align 4,,10
	.p2align 3
.L2189:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2195
.L2155:
	movq	%rbx, _ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCollationEiPmPNS0_7IsolateEE28trace_event_unique_atomic761(%rip)
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2188:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$884, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2152
	.p2align 4,,10
	.p2align 3
.L2192:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2151
	.p2align 4,,10
	.p2align 3
.L2191:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2195:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2155
	.p2align 4,,10
	.p2align 3
.L2194:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC78(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2157
.L2193:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19355:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCollationEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCollationEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeHourCycleEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC80:
	.string	"V8.Builtin_LocalePrototypeHourCycle"
	.align 8
.LC81:
	.string	"Intl.Locale.prototype.hourCycle"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeHourCycleEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeHourCycleEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeHourCycleEiPmPNS0_7IsolateE:
.LFB19358:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2233
.L2197:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeHourCycleEiPmPNS0_7IsolateEE28trace_event_unique_atomic768(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2234
.L2199:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2235
.L2201:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2205
.L2208:
	leaq	.LC81(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$31, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2236
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2209:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2212
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2212:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2237
.L2196:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2238
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2205:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L2208
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale9HourCycleEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L2209
	.p2align 4,,10
	.p2align 3
.L2235:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2239
.L2202:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2203
	movq	(%rdi), %rax
	call	*8(%rax)
.L2203:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2204
	movq	(%rdi), %rax
	call	*8(%rax)
.L2204:
	leaq	.LC80(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2234:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2240
.L2200:
	movq	%rbx, _ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeHourCycleEiPmPNS0_7IsolateEE28trace_event_unique_atomic768(%rip)
	jmp	.L2199
	.p2align 4,,10
	.p2align 3
.L2233:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$885, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2197
	.p2align 4,,10
	.p2align 3
.L2237:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2196
	.p2align 4,,10
	.p2align 3
.L2236:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2240:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2200
	.p2align 4,,10
	.p2align 3
.L2239:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC80(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2202
.L2238:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19358:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeHourCycleEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeHourCycleEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC82:
	.string	"V8.Builtin_LocalePrototypeNumeric"
	.section	.rodata._ZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC83:
	.string	"Intl.Locale.prototype.numeric"
	.section	.text._ZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateE:
.LFB19361:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2278
.L2242:
	movq	_ZZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateEE28trace_event_unique_atomic775(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2279
.L2244:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2280
.L2246:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2250
.L2253:
	leaq	.LC83(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$29, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2281
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2254:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2257
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2257:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2282
.L2241:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2283
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2250:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L2253
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale7NumericEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L2254
	.p2align 4,,10
	.p2align 3
.L2280:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2284
.L2247:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2248
	movq	(%rdi), %rax
	call	*8(%rax)
.L2248:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2249
	movq	(%rdi), %rax
	call	*8(%rax)
.L2249:
	leaq	.LC82(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2246
	.p2align 4,,10
	.p2align 3
.L2279:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2285
.L2245:
	movq	%rbx, _ZZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateEE28trace_event_unique_atomic775(%rip)
	jmp	.L2244
	.p2align 4,,10
	.p2align 3
.L2278:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$889, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2242
	.p2align 4,,10
	.p2align 3
.L2282:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2241
	.p2align 4,,10
	.p2align 3
.L2281:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2285:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2245
	.p2align 4,,10
	.p2align 3
.L2284:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC82(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2247
.L2283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19361:
	.size	_ZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL49Builtin_Impl_Stats_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC84:
	.string	"V8.Builtin_LocalePrototypeNumberingSystem"
	.align 8
.LC85:
	.string	"Intl.Locale.prototype.numberingSystem"
	.section	.text._ZN2v88internalL49Builtin_Impl_Stats_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL49Builtin_Impl_Stats_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE, @function
_ZN2v88internalL49Builtin_Impl_Stats_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE:
.LFB19364:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2323
.L2287:
	movq	_ZZN2v88internalL49Builtin_Impl_Stats_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateEE28trace_event_unique_atomic782(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2324
.L2289:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2325
.L2291:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2295
.L2298:
	leaq	.LC85(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$37, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2326
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2299:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2302
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2302:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2327
.L2286:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2328
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2295:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L2298
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale15NumberingSystemEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L2299
	.p2align 4,,10
	.p2align 3
.L2325:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2329
.L2292:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2293
	movq	(%rdi), %rax
	call	*8(%rax)
.L2293:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2294
	movq	(%rdi), %rax
	call	*8(%rax)
.L2294:
	leaq	.LC84(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2324:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2330
.L2290:
	movq	%rbx, _ZZN2v88internalL49Builtin_Impl_Stats_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateEE28trace_event_unique_atomic782(%rip)
	jmp	.L2289
	.p2align 4,,10
	.p2align 3
.L2323:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$890, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2287
	.p2align 4,,10
	.p2align 3
.L2327:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2286
	.p2align 4,,10
	.p2align 3
.L2326:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2330:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2290
	.p2align 4,,10
	.p2align 3
.L2329:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC84(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2292
.L2328:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19364:
	.size	_ZN2v88internalL49Builtin_Impl_Stats_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE, .-_ZN2v88internalL49Builtin_Impl_Stats_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeToStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC86:
	.string	"V8.Builtin_LocalePrototypeToString"
	.align 8
.LC87:
	.string	"Intl.Locale.prototype.toString"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeToStringEiPmPNS0_7IsolateE:
.LFB19367:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2368
.L2332:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic789(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2369
.L2334:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2370
.L2336:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2340
.L2343:
	leaq	.LC87(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$30, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2371
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2344:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2347
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2347:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2372
.L2331:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2373
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2340:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L2343
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSLocale8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2370:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2374
.L2337:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2338
	movq	(%rdi), %rax
	call	*8(%rax)
.L2338:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2339
	movq	(%rdi), %rax
	call	*8(%rax)
.L2339:
	leaq	.LC86(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2336
	.p2align 4,,10
	.p2align 3
.L2369:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2375
.L2335:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic789(%rip)
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L2368:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$893, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2332
	.p2align 4,,10
	.p2align 3
.L2372:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2371:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2375:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2335
	.p2align 4,,10
	.p2align 3
.L2374:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC86(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2337
.L2373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19367:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeToStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_Stats_LocaleConstructorEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC88:
	.string	"V8.Builtin_LocaleConstructor"
.LC89:
	.string	"Intl.Locale"
	.section	.text._ZN2v88internalL36Builtin_Impl_Stats_LocaleConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_Stats_LocaleConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_Stats_LocaleConstructorEiPmPNS0_7IsolateE:
.LFB19319:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2415
.L2377:
	movq	_ZZN2v88internalL36Builtin_Impl_Stats_LocaleConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic628(%rip), %r14
	testq	%r14, %r14
	je	.L2416
.L2379:
	movq	$0, -160(%rbp)
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L2417
.L2381:
	movl	$59, %esi
	movq	%r12, %rdi
	addl	$1, 41104(%r12)
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	leal	-8(,%r13,8), %eax
	movq	%rbx, %rcx
	movslq	%eax, %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	movq	88(%r12), %rcx
	cmpq	%rcx, (%rdx)
	je	.L2418
	subl	$8, %eax
	movq	%rbx, %rsi
	cltq
	subq	%rax, %rsi
	cmpl	$5, %r13d
	jle	.L2419
	leaq	-8(%rbx), %rcx
	cmpl	$6, %r13d
	je	.L2420
	leaq	-16(%rbx), %r8
.L2391:
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112CreateLocaleEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEESA_
	testq	%rax, %rax
	je	.L2421
.L2392:
	movq	(%rax), %r13
.L2387:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L2395
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2395:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2422
.L2376:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2423
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2419:
	.cfi_restore_state
	leaq	88(%r12), %r8
	movq	%r12, %rdi
	movq	%r8, %rcx
	call	_ZN2v88internal12_GLOBAL__N_112CreateLocaleEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEESA_
	testq	%rax, %rax
	jne	.L2392
.L2421:
	movq	312(%r12), %r13
	jmp	.L2387
	.p2align 4,,10
	.p2align 3
.L2417:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2424
.L2382:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2383
	movq	(%rdi), %rax
	call	*8(%rax)
.L2383:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2384
	movq	(%rdi), %rax
	call	*8(%rax)
.L2384:
	leaq	.LC88(%rip), %rax
	movq	%r14, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2416:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2425
.L2380:
	movq	%r14, _ZZN2v88internalL36Builtin_Impl_Stats_LocaleConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic628(%rip)
	jmp	.L2379
	.p2align 4,,10
	.p2align 3
.L2418:
	leaq	.LC89(%rip), %rax
	xorl	%edx, %edx
	leaq	-176(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -176(%rbp)
	movq	$11, -168(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2426
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
	jmp	.L2387
	.p2align 4,,10
	.p2align 3
.L2415:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$880, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2377
	.p2align 4,,10
	.p2align 3
.L2422:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2425:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2424:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC88(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r14, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2382
	.p2align 4,,10
	.p2align 3
.L2426:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2423:
	call	__stack_chk_fail@PLT
.L2420:
	leaq	88(%r12), %r8
	jmp	.L2391
	.cfi_endproc
.LFE19319:
	.size	_ZN2v88internalL36Builtin_Impl_Stats_LocaleConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_Stats_LocaleConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMaximizeEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC90:
	.string	"V8.Builtin_LocalePrototypeMaximize"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMaximizeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMaximizeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMaximizeEiPmPNS0_7IsolateE:
.LFB19322:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2456
.L2428:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMaximizeEiPmPNS0_7IsolateEE28trace_event_unique_atomic650(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2457
.L2430:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2458
.L2432:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_LocalePrototypeMaximizeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2459
.L2436:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2460
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2457:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2461
.L2431:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMaximizeEiPmPNS0_7IsolateEE28trace_event_unique_atomic650(%rip)
	jmp	.L2430
	.p2align 4,,10
	.p2align 3
.L2458:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2462
.L2433:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2434
	movq	(%rdi), %rax
	call	*8(%rax)
.L2434:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2435
	movq	(%rdi), %rax
	call	*8(%rax)
.L2435:
	leaq	.LC90(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2432
	.p2align 4,,10
	.p2align 3
.L2459:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2436
	.p2align 4,,10
	.p2align 3
.L2456:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$887, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2462:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC90(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2461:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2431
.L2460:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19322:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMaximizeEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMaximizeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMinimizeEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC91:
	.string	"V8.Builtin_LocalePrototypeMinimize"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMinimizeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMinimizeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMinimizeEiPmPNS0_7IsolateE:
.LFB19325:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2492
.L2464:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMinimizeEiPmPNS0_7IsolateEE28trace_event_unique_atomic662(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2493
.L2466:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2494
.L2468:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_LocalePrototypeMinimizeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2495
.L2472:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2496
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2493:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2497
.L2467:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMinimizeEiPmPNS0_7IsolateEE28trace_event_unique_atomic662(%rip)
	jmp	.L2466
	.p2align 4,,10
	.p2align 3
.L2494:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2498
.L2469:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2470
	movq	(%rdi), %rax
	call	*8(%rax)
.L2470:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2471
	movq	(%rdi), %rax
	call	*8(%rax)
.L2471:
	leaq	.LC91(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2468
	.p2align 4,,10
	.p2align 3
.L2495:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2492:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$888, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2498:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC91(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L2497:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2467
.L2496:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19325:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMinimizeEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMinimizeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL52Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC92:
	.string	"V8.Builtin_RelativeTimeFormatPrototypeFormat"
	.section	.text._ZN2v88internalL52Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL52Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE, @function
_ZN2v88internalL52Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE:
.LFB19331:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2528
.L2500:
	movq	_ZZN2v88internalL52Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic686(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2529
.L2502:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2530
.L2504:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL46Builtin_Impl_RelativeTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2531
.L2508:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2532
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2529:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2533
.L2503:
	movq	%rbx, _ZZN2v88internalL52Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic686(%rip)
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2530:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2534
.L2505:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2506
	movq	(%rdi), %rax
	call	*8(%rax)
.L2506:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2507
	movq	(%rdi), %rax
	call	*8(%rax)
.L2507:
	leaq	.LC92(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2531:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2508
	.p2align 4,,10
	.p2align 3
.L2528:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$905, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2500
	.p2align 4,,10
	.p2align 3
.L2534:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC92(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2505
	.p2align 4,,10
	.p2align 3
.L2533:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2503
.L2532:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19331:
	.size	_ZN2v88internalL52Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE, .-_ZN2v88internalL52Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL59Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC93:
	.string	"V8.Builtin_RelativeTimeFormatPrototypeFormatToParts"
	.section	.text._ZN2v88internalL59Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL59Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL59Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE:
.LFB19334:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2564
.L2536:
	movq	_ZZN2v88internalL59Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic702(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2565
.L2538:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2566
.L2540:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL53Builtin_Impl_RelativeTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2567
.L2544:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2568
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2565:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2569
.L2539:
	movq	%rbx, _ZZN2v88internalL59Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic702(%rip)
	jmp	.L2538
	.p2align 4,,10
	.p2align 3
.L2566:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2570
.L2541:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2542
	movq	(%rdi), %rax
	call	*8(%rax)
.L2542:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2543
	movq	(%rdi), %rax
	call	*8(%rax)
.L2543:
	leaq	.LC93(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2540
	.p2align 4,,10
	.p2align 3
.L2567:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2544
	.p2align 4,,10
	.p2align 3
.L2564:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$906, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2536
	.p2align 4,,10
	.p2align 3
.L2570:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC93(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2541
	.p2align 4,,10
	.p2align 3
.L2569:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2539
.L2568:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19334:
	.size	_ZN2v88internalL59Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE, .-_ZN2v88internalL59Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_Stats_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC94:
	.string	"V8.Builtin_PluralRulesPrototypeSelect"
	.section	.text._ZN2v88internalL45Builtin_Impl_Stats_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_Stats_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_Stats_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE:
.LFB19388:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2600
.L2572:
	movq	_ZZN2v88internalL45Builtin_Impl_Stats_PluralRulesPrototypeSelectEiPmPNS0_7IsolateEE28trace_event_unique_atomic850(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2601
.L2574:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2602
.L2576:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL39Builtin_Impl_PluralRulesPrototypeSelectENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2603
.L2580:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2604
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2601:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2605
.L2575:
	movq	%rbx, _ZZN2v88internalL45Builtin_Impl_Stats_PluralRulesPrototypeSelectEiPmPNS0_7IsolateEE28trace_event_unique_atomic850(%rip)
	jmp	.L2574
	.p2align 4,,10
	.p2align 3
.L2602:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2606
.L2577:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2578
	movq	(%rdi), %rax
	call	*8(%rax)
.L2578:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2579
	movq	(%rdi), %rax
	call	*8(%rax)
.L2579:
	leaq	.LC94(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2576
	.p2align 4,,10
	.p2align 3
.L2603:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2600:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$902, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L2606:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC94(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2577
	.p2align 4,,10
	.p2align 3
.L2605:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2575
.L2604:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19388:
	.size	_ZN2v88internalL45Builtin_Impl_Stats_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_Stats_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC95:
	.string	"V8.Builtin_SegmentIteratorPrototypeNext"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE:
.LFB19415:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2636
.L2608:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateEE28trace_event_unique_atomic992(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2637
.L2610:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2638
.L2612:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL41Builtin_Impl_SegmentIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2639
.L2616:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2640
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2637:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2641
.L2611:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateEE28trace_event_unique_atomic992(%rip)
	jmp	.L2610
	.p2align 4,,10
	.p2align 3
.L2638:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2642
.L2613:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2614
	movq	(%rdi), %rax
	call	*8(%rax)
.L2614:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2615
	movq	(%rdi), %rax
	call	*8(%rax)
.L2615:
	leaq	.LC95(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2612
	.p2align 4,,10
	.p2align 3
.L2639:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2616
	.p2align 4,,10
	.p2align 3
.L2636:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$917, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2608
	.p2align 4,,10
	.p2align 3
.L2642:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC95(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2613
	.p2align 4,,10
	.p2align 3
.L2641:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2611
.L2640:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19415:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL48Builtin_Impl_Stats_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC96:
	.string	"V8.Builtin_SegmentIteratorPrototypeIndex"
	.align 8
.LC97:
	.string	"get %SegmentIteratorPrototype%.index"
	.section	.text._ZN2v88internalL48Builtin_Impl_Stats_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Builtin_Impl_Stats_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE, @function
_ZN2v88internalL48Builtin_Impl_Stats_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE:
.LFB19421:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2680
.L2644:
	movq	_ZZN2v88internalL48Builtin_Impl_Stats_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateEE29trace_event_unique_atomic1016(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2681
.L2646:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2682
.L2648:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2652
.L2655:
	leaq	.LC97(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$36, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2683
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2656:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2659
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2659:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2684
.L2643:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2685
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2652:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1096, 11(%rax)
	jne	.L2655
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17JSSegmentIterator5IndexEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L2656
	.p2align 4,,10
	.p2align 3
.L2682:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2686
.L2649:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2650
	movq	(%rdi), %rax
	call	*8(%rax)
.L2650:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2651
	movq	(%rdi), %rax
	call	*8(%rax)
.L2651:
	leaq	.LC96(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2648
	.p2align 4,,10
	.p2align 3
.L2681:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2687
.L2647:
	movq	%rbx, _ZZN2v88internalL48Builtin_Impl_Stats_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateEE29trace_event_unique_atomic1016(%rip)
	jmp	.L2646
	.p2align 4,,10
	.p2align 3
.L2680:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$916, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2644
	.p2align 4,,10
	.p2align 3
.L2684:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2643
	.p2align 4,,10
	.p2align 3
.L2683:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2687:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2647
	.p2align 4,,10
	.p2align 3
.L2686:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC96(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2649
.L2685:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19421:
	.size	_ZN2v88internalL48Builtin_Impl_Stats_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE, .-_ZN2v88internalL48Builtin_Impl_Stats_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC98:
	.string	"V8.Builtin_SegmentIteratorPrototypeBreakType"
	.section	.text._ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE:
.LFB19409:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2717
.L2689:
	movq	_ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateEE28trace_event_unique_atomic969(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2718
.L2691:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2719
.L2693:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2720
.L2697:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2721
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2718:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2722
.L2692:
	movq	%rbx, _ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateEE28trace_event_unique_atomic969(%rip)
	jmp	.L2691
	.p2align 4,,10
	.p2align 3
.L2719:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2723
.L2694:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2695
	movq	(%rdi), %rax
	call	*8(%rax)
.L2695:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2696
	movq	(%rdi), %rax
	call	*8(%rax)
.L2696:
	leaq	.LC98(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2693
	.p2align 4,,10
	.p2align 3
.L2720:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2717:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$913, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2689
	.p2align 4,,10
	.p2align 3
.L2723:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC98(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2694
	.p2align 4,,10
	.p2align 3
.L2722:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2692
.L2721:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19409:
	.size	_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE, .-_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_NumberFormatConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC99:
	.string	"V8.Builtin_NumberFormatConstructor"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_NumberFormatConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_NumberFormatConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_NumberFormatConstructorEiPmPNS0_7IsolateE:
.LFB19282:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2753
.L2725:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_NumberFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic393(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2754
.L2727:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2755
.L2729:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_NumberFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2756
.L2733:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2757
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2754:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2758
.L2728:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_NumberFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic393(%rip)
	jmp	.L2727
	.p2align 4,,10
	.p2align 3
.L2755:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2759
.L2730:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2731
	movq	(%rdi), %rax
	call	*8(%rax)
.L2731:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2732
	movq	(%rdi), %rax
	call	*8(%rax)
.L2732:
	leaq	.LC99(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2729
	.p2align 4,,10
	.p2align 3
.L2756:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2733
	.p2align 4,,10
	.p2align 3
.L2753:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$894, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2725
	.p2align 4,,10
	.p2align 3
.L2759:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC99(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2730
	.p2align 4,,10
	.p2align 3
.L2758:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2728
.L2757:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19282:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_NumberFormatConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_NumberFormatConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Builtin_Impl_Stats_DateTimeFormatConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC100:
	.string	"V8.Builtin_DateTimeFormatConstructor"
	.section	.text._ZN2v88internalL44Builtin_Impl_Stats_DateTimeFormatConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_Stats_DateTimeFormatConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_Stats_DateTimeFormatConstructorEiPmPNS0_7IsolateE:
.LFB19294:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2789
.L2761:
	movq	_ZZN2v88internalL44Builtin_Impl_Stats_DateTimeFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic487(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2790
.L2763:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2791
.L2765:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL38Builtin_Impl_DateTimeFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2792
.L2769:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2793
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2790:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2794
.L2764:
	movq	%rbx, _ZZN2v88internalL44Builtin_Impl_Stats_DateTimeFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic487(%rip)
	jmp	.L2763
	.p2align 4,,10
	.p2align 3
.L2791:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2795
.L2766:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2767
	movq	(%rdi), %rax
	call	*8(%rax)
.L2767:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2768
	movq	(%rdi), %rax
	call	*8(%rax)
.L2768:
	leaq	.LC100(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2765
	.p2align 4,,10
	.p2align 3
.L2792:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2769
	.p2align 4,,10
	.p2align 3
.L2789:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$868, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L2795:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC100(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2766
	.p2align 4,,10
	.p2align 3
.L2794:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2764
.L2793:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19294:
	.size	_ZN2v88internalL44Builtin_Impl_Stats_DateTimeFormatConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_Stats_DateTimeFormatConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC101:
	.string	"V8.Builtin_SegmentIteratorPrototypeFollowing"
	.section	.text._ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE, @function
_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE:
.LFB19412:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2825
.L2797:
	movq	_ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateEE28trace_event_unique_atomic978(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2826
.L2799:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2827
.L2801:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeFollowingENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2828
.L2805:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2829
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2826:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2830
.L2800:
	movq	%rbx, _ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateEE28trace_event_unique_atomic978(%rip)
	jmp	.L2799
	.p2align 4,,10
	.p2align 3
.L2827:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2831
.L2802:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2803
	movq	(%rdi), %rax
	call	*8(%rax)
.L2803:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2804
	movq	(%rdi), %rax
	call	*8(%rax)
.L2804:
	leaq	.LC101(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2801
	.p2align 4,,10
	.p2align 3
.L2828:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2805
	.p2align 4,,10
	.p2align 3
.L2825:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$914, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2797
	.p2align 4,,10
	.p2align 3
.L2831:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC101(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2802
	.p2align 4,,10
	.p2align 3
.L2830:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2800
.L2829:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19412:
	.size	_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE, .-_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC102:
	.string	"V8.Builtin_SegmentIteratorPrototypePreceding"
	.section	.text._ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE, @function
_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE:
.LFB19418:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2861
.L2833:
	movq	_ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1002(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2862
.L2835:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2863
.L2837:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypePrecedingENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2864
.L2841:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2865
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2862:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2866
.L2836:
	movq	%rbx, _ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1002(%rip)
	jmp	.L2835
	.p2align 4,,10
	.p2align 3
.L2863:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2867
.L2838:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2839
	movq	(%rdi), %rax
	call	*8(%rax)
.L2839:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2840
	movq	(%rdi), %rax
	call	*8(%rax)
.L2840:
	leaq	.LC102(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2837
	.p2align 4,,10
	.p2align 3
.L2864:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2841
	.p2align 4,,10
	.p2align 3
.L2861:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$915, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2833
	.p2align 4,,10
	.p2align 3
.L2867:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC102(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2838
	.p2align 4,,10
	.p2align 3
.L2866:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2836
.L2865:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19418:
	.size	_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE, .-_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL55Builtin_Impl_Stats_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC103:
	.string	"V8.Builtin_DateTimeFormatPrototypeFormatToParts"
	.section	.text._ZN2v88internalL55Builtin_Impl_Stats_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL55Builtin_Impl_Stats_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL55Builtin_Impl_Stats_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE:
.LFB19268:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2897
.L2869:
	movq	_ZZN2v88internalL55Builtin_Impl_Stats_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic127(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2898
.L2871:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2899
.L2873:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL49Builtin_Impl_DateTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2900
.L2877:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2901
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2898:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2902
.L2872:
	movq	%rbx, _ZZN2v88internalL55Builtin_Impl_Stats_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic127(%rip)
	jmp	.L2871
	.p2align 4,,10
	.p2align 3
.L2899:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2903
.L2874:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2875
	movq	(%rdi), %rax
	call	*8(%rax)
.L2875:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2876
	movq	(%rdi), %rax
	call	*8(%rax)
.L2876:
	leaq	.LC103(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2873
	.p2align 4,,10
	.p2align 3
.L2900:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2877
	.p2align 4,,10
	.p2align 3
.L2897:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$873, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2869
	.p2align 4,,10
	.p2align 3
.L2903:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC103(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2874
	.p2align 4,,10
	.p2align 3
.L2902:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2872
.L2901:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19268:
	.size	_ZN2v88internalL55Builtin_Impl_Stats_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE, .-_ZN2v88internalL55Builtin_Impl_Stats_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL55Builtin_Impl_Stats_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC104:
	.string	"V8.Builtin_NumberFormatPrototypeResolvedOptions"
	.section	.text._ZN2v88internalL55Builtin_Impl_Stats_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL55Builtin_Impl_Stats_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL55Builtin_Impl_Stats_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19285:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2933
.L2905:
	movq	_ZZN2v88internalL55Builtin_Impl_Stats_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic401(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2934
.L2907:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2935
.L2909:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL49Builtin_Impl_NumberFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2936
.L2913:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2937
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2934:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2938
.L2908:
	movq	%rbx, _ZZN2v88internalL55Builtin_Impl_Stats_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic401(%rip)
	jmp	.L2907
	.p2align 4,,10
	.p2align 3
.L2935:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2939
.L2910:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2911
	movq	(%rdi), %rax
	call	*8(%rax)
.L2911:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2912
	movq	(%rdi), %rax
	call	*8(%rax)
.L2912:
	leaq	.LC104(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2909
	.p2align 4,,10
	.p2align 3
.L2936:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2913
	.p2align 4,,10
	.p2align 3
.L2933:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$898, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2905
	.p2align 4,,10
	.p2align 3
.L2939:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC104(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L2910
	.p2align 4,,10
	.p2align 3
.L2938:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2908
.L2937:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19285:
	.size	_ZN2v88internalL55Builtin_Impl_Stats_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internalL55Builtin_Impl_Stats_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL53Builtin_Impl_Stats_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC105:
	.string	"V8.Builtin_ListFormatPrototypeResolvedOptions"
	.align 8
.LC106:
	.string	"Intl.ListFormat.prototype.resolvedOptions"
	.section	.text._ZN2v88internalL53Builtin_Impl_Stats_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL53Builtin_Impl_Stats_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL53Builtin_Impl_Stats_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19309:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2977
.L2941:
	movq	_ZZN2v88internalL53Builtin_Impl_Stats_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic562(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2978
.L2943:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2979
.L2945:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2949
.L2952:
	leaq	.LC106(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$41, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2980
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2953:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2956
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2956:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2981
.L2940:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2982
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2949:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1091, 11(%rax)
	jne	.L2952
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12JSListFormat15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L2953
	.p2align 4,,10
	.p2align 3
.L2979:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2983
.L2946:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2947
	movq	(%rdi), %rax
	call	*8(%rax)
.L2947:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2948
	movq	(%rdi), %rax
	call	*8(%rax)
.L2948:
	leaq	.LC105(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2945
	.p2align 4,,10
	.p2align 3
.L2978:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2984
.L2944:
	movq	%rbx, _ZZN2v88internalL53Builtin_Impl_Stats_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic562(%rip)
	jmp	.L2943
	.p2align 4,,10
	.p2align 3
.L2977:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$878, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2941
	.p2align 4,,10
	.p2align 3
.L2981:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2940
	.p2align 4,,10
	.p2align 3
.L2980:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2984:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2944
	.p2align 4,,10
	.p2align 3
.L2983:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC105(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2946
.L2982:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19309:
	.size	_ZN2v88internalL53Builtin_Impl_Stats_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internalL53Builtin_Impl_Stats_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL61Builtin_Impl_Stats_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC107:
	.string	"V8.Builtin_RelativeTimeFormatPrototypeResolvedOptions"
	.align 8
.LC108:
	.string	"Intl.RelativeTimeFormat.prototype.resolvedOptions"
	.section	.text._ZN2v88internalL61Builtin_Impl_Stats_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL61Builtin_Impl_Stats_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL61Builtin_Impl_Stats_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19373:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3022
.L2986:
	movq	_ZZN2v88internalL61Builtin_Impl_Stats_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic804(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3023
.L2988:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3024
.L2990:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2994
.L2997:
	leaq	.LC108(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$49, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3025
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L2998:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3001
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3001:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3026
.L2985:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3027
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2994:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1095, 11(%rax)
	jne	.L2997
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal20JSRelativeTimeFormat15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L2998
	.p2align 4,,10
	.p2align 3
.L3024:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3028
.L2991:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2992
	movq	(%rdi), %rax
	call	*8(%rax)
.L2992:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2993
	movq	(%rdi), %rax
	call	*8(%rax)
.L2993:
	leaq	.LC107(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2990
	.p2align 4,,10
	.p2align 3
.L3023:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3029
.L2989:
	movq	%rbx, _ZZN2v88internalL61Builtin_Impl_Stats_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic804(%rip)
	jmp	.L2988
	.p2align 4,,10
	.p2align 3
.L3022:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$907, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2986
	.p2align 4,,10
	.p2align 3
.L3026:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2985
	.p2align 4,,10
	.p2align 3
.L3025:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3029:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2989
	.p2align 4,,10
	.p2align 3
.L3028:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC107(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2991
.L3027:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19373:
	.size	_ZN2v88internalL61Builtin_Impl_Stats_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internalL61Builtin_Impl_Stats_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL54Builtin_Impl_Stats_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC109:
	.string	"V8.Builtin_PluralRulesPrototypeResolvedOptions"
	.align 8
.LC110:
	.string	"Intl.PluralRules.prototype.resolvedOptions"
	.section	.text._ZN2v88internalL54Builtin_Impl_Stats_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL54Builtin_Impl_Stats_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL54Builtin_Impl_Stats_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19385:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3067
.L3031:
	movq	_ZZN2v88internalL54Builtin_Impl_Stats_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic843(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3068
.L3033:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3069
.L3035:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3039
.L3042:
	leaq	.LC110(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$42, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3070
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L3043:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3046
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3046:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3071
.L3030:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3072
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3039:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1094, 11(%rax)
	jne	.L3042
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13JSPluralRules15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L3043
	.p2align 4,,10
	.p2align 3
.L3069:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3073
.L3036:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3037
	movq	(%rdi), %rax
	call	*8(%rax)
.L3037:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3038
	movq	(%rdi), %rax
	call	*8(%rax)
.L3038:
	leaq	.LC109(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3035
	.p2align 4,,10
	.p2align 3
.L3068:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3074
.L3034:
	movq	%rbx, _ZZN2v88internalL54Builtin_Impl_Stats_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic843(%rip)
	jmp	.L3033
	.p2align 4,,10
	.p2align 3
.L3067:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$901, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3031
	.p2align 4,,10
	.p2align 3
.L3071:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3030
	.p2align 4,,10
	.p2align 3
.L3070:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3074:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3034
	.p2align 4,,10
	.p2align 3
.L3073:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC109(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3036
.L3072:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19385:
	.size	_ZN2v88internalL54Builtin_Impl_Stats_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internalL54Builtin_Impl_Stats_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL51Builtin_Impl_Stats_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC111:
	.string	"V8.Builtin_CollatorPrototypeResolvedOptions"
	.align 8
.LC112:
	.string	"Intl.Collator.prototype.resolvedOptions"
	.section	.text._ZN2v88internalL51Builtin_Impl_Stats_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL51Builtin_Impl_Stats_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL51Builtin_Impl_Stats_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19397:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3112
.L3076:
	movq	_ZZN2v88internalL51Builtin_Impl_Stats_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic890(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3113
.L3078:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3114
.L3080:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3084
.L3087:
	leaq	.LC112(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$39, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3115
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L3088:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3091
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3091:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3116
.L3075:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3117
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3084:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1089, 11(%rax)
	jne	.L3087
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L3088
	.p2align 4,,10
	.p2align 3
.L3114:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3118
.L3081:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3082
	movq	(%rdi), %rax
	call	*8(%rax)
.L3082:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3083
	movq	(%rdi), %rax
	call	*8(%rax)
.L3083:
	leaq	.LC111(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3080
	.p2align 4,,10
	.p2align 3
.L3113:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3119
.L3079:
	movq	%rbx, _ZZN2v88internalL51Builtin_Impl_Stats_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic890(%rip)
	jmp	.L3078
	.p2align 4,,10
	.p2align 3
.L3112:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$864, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3076
	.p2align 4,,10
	.p2align 3
.L3116:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3075
	.p2align 4,,10
	.p2align 3
.L3115:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3119:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3079
	.p2align 4,,10
	.p2align 3
.L3118:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC111(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3081
.L3117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19397:
	.size	_ZN2v88internalL51Builtin_Impl_Stats_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internalL51Builtin_Impl_Stats_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL52Builtin_Impl_Stats_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC113:
	.string	"V8.Builtin_SegmenterPrototypeResolvedOptions"
	.align 8
.LC114:
	.string	"Intl.Segmenter.prototype.resolvedOptions"
	.section	.text._ZN2v88internalL52Builtin_Impl_Stats_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL52Builtin_Impl_Stats_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL52Builtin_Impl_Stats_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19430:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3157
.L3121:
	movq	_ZZN2v88internalL52Builtin_Impl_Stats_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1043(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3158
.L3123:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3159
.L3125:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3129
.L3132:
	leaq	.LC114(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$40, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3160
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L3133:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3136
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3136:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3161
.L3120:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3162
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3129:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1097, 11(%rax)
	jne	.L3132
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11JSSegmenter15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L3133
	.p2align 4,,10
	.p2align 3
.L3159:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3163
.L3126:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3127
	movq	(%rdi), %rax
	call	*8(%rax)
.L3127:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3128
	movq	(%rdi), %rax
	call	*8(%rax)
.L3128:
	leaq	.LC113(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3125
	.p2align 4,,10
	.p2align 3
.L3158:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3164
.L3124:
	movq	%rbx, _ZZN2v88internalL52Builtin_Impl_Stats_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1043(%rip)
	jmp	.L3123
	.p2align 4,,10
	.p2align 3
.L3157:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$910, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3121
	.p2align 4,,10
	.p2align 3
.L3161:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L3160:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3164:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3124
	.p2align 4,,10
	.p2align 3
.L3163:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC113(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3126
.L3162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19430:
	.size	_ZN2v88internalL52Builtin_Impl_Stats_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internalL52Builtin_Impl_Stats_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL58Builtin_Impl_Stats_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC115:
	.string	"V8.Builtin_V8BreakIteratorPrototypeResolvedOptions"
	.align 8
.LC116:
	.string	"Intl.v8BreakIterator.prototype.resolvedOptions"
	.section	.text._ZN2v88internalL58Builtin_Impl_Stats_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL58Builtin_Impl_Stats_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL58Builtin_Impl_Stats_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19439:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3202
.L3166:
	movq	_ZZN2v88internalL58Builtin_Impl_Stats_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1075(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3203
.L3168:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3204
.L3170:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3174
.L3177:
	leaq	.LC116(%rip), %rax
	xorl	%edx, %edx
	leaq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$46, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3205
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L3178:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3181
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3181:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3206
.L3165:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3207
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3174:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1088, 11(%rax)
	jne	.L3177
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17JSV8BreakIterator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	jmp	.L3178
	.p2align 4,,10
	.p2align 3
.L3204:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3208
.L3171:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3172
	movq	(%rdi), %rax
	call	*8(%rax)
.L3172:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3173
	movq	(%rdi), %rax
	call	*8(%rax)
.L3173:
	leaq	.LC115(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3170
	.p2align 4,,10
	.p2align 3
.L3203:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3209
.L3169:
	movq	%rbx, _ZZN2v88internalL58Builtin_Impl_Stats_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1075(%rip)
	jmp	.L3168
	.p2align 4,,10
	.p2align 3
.L3202:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$933, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3166
	.p2align 4,,10
	.p2align 3
.L3206:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3165
	.p2align 4,,10
	.p2align 3
.L3205:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3209:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3169
	.p2align 4,,10
	.p2align 3
.L3208:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC115(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3171
.L3207:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19439:
	.size	_ZN2v88internalL58Builtin_Impl_Stats_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internalL58Builtin_Impl_Stats_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_Stats_CollatorPrototypeCompareEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC117:
	.string	"V8.Builtin_CollatorPrototypeCompare"
	.section	.text._ZN2v88internalL43Builtin_Impl_Stats_CollatorPrototypeCompareEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_Stats_CollatorPrototypeCompareEiPmPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_Stats_CollatorPrototypeCompareEiPmPNS0_7IsolateE:
.LFB19403:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3239
.L3211:
	movq	_ZZN2v88internalL43Builtin_Impl_Stats_CollatorPrototypeCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic908(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3240
.L3213:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3241
.L3215:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL37Builtin_Impl_CollatorPrototypeCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3242
.L3219:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3243
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3240:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3244
.L3214:
	movq	%rbx, _ZZN2v88internalL43Builtin_Impl_Stats_CollatorPrototypeCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic908(%rip)
	jmp	.L3213
	.p2align 4,,10
	.p2align 3
.L3241:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3245
.L3216:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3217
	movq	(%rdi), %rax
	call	*8(%rax)
.L3217:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3218
	movq	(%rdi), %rax
	call	*8(%rax)
.L3218:
	leaq	.LC117(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3215
	.p2align 4,,10
	.p2align 3
.L3242:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3219
	.p2align 4,,10
	.p2align 3
.L3239:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$862, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3211
	.p2align 4,,10
	.p2align 3
.L3245:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC117(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3216
	.p2align 4,,10
	.p2align 3
.L3244:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3214
.L3243:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19403:
	.size	_ZN2v88internalL43Builtin_Impl_Stats_CollatorPrototypeCompareEiPmPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_Stats_CollatorPrototypeCompareEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL48Builtin_Impl_Stats_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC118:
	.string	"V8.Builtin_DateTimeFormatPrototypeFormat"
	.section	.text._ZN2v88internalL48Builtin_Impl_Stats_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Builtin_Impl_Stats_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE, @function
_ZN2v88internalL48Builtin_Impl_Stats_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE:
.LFB19297:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3275
.L3247:
	movq	_ZZN2v88internalL48Builtin_Impl_Stats_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic495(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3276
.L3249:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3277
.L3251:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL42Builtin_Impl_DateTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3278
.L3255:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3279
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3276:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3280
.L3250:
	movq	%rbx, _ZZN2v88internalL48Builtin_Impl_Stats_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic495(%rip)
	jmp	.L3249
	.p2align 4,,10
	.p2align 3
.L3277:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3281
.L3252:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3253
	movq	(%rdi), %rax
	call	*8(%rax)
.L3253:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3254
	movq	(%rdi), %rax
	call	*8(%rax)
.L3254:
	leaq	.LC118(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3251
	.p2align 4,,10
	.p2align 3
.L3278:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3255
	.p2align 4,,10
	.p2align 3
.L3275:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$870, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3247
	.p2align 4,,10
	.p2align 3
.L3281:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC118(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3252
	.p2align 4,,10
	.p2align 3
.L3280:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3250
.L3279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19297:
	.size	_ZN2v88internalL48Builtin_Impl_Stats_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE, .-_ZN2v88internalL48Builtin_Impl_Stats_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC119:
	.string	"V8.Builtin_V8BreakIteratorPrototypeAdoptText"
	.section	.text._ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE, @function
_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE:
.LFB19442:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3311
.L3283:
	movq	_ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1082(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3312
.L3285:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3313
.L3287:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3314
.L3291:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3315
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3312:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3316
.L3286:
	movq	%rbx, _ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1082(%rip)
	jmp	.L3285
	.p2align 4,,10
	.p2align 3
.L3313:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3317
.L3288:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3289
	movq	(%rdi), %rax
	call	*8(%rax)
.L3289:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3290
	movq	(%rdi), %rax
	call	*8(%rax)
.L3290:
	leaq	.LC119(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3287
	.p2align 4,,10
	.p2align 3
.L3314:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3291
	.p2align 4,,10
	.p2align 3
.L3311:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$928, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3283
	.p2align 4,,10
	.p2align 3
.L3317:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC119(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3288
	.p2align 4,,10
	.p2align 3
.L3316:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3286
.L3315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19442:
	.size	_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE, .-_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL52Builtin_Impl_Stats_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC120:
	.string	"V8.Builtin_NumberFormatPrototypeFormatNumber"
	.section	.text._ZN2v88internalL52Builtin_Impl_Stats_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL52Builtin_Impl_Stats_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internalL52Builtin_Impl_Stats_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE:
.LFB19288:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3347
.L3319:
	movq	_ZZN2v88internalL52Builtin_Impl_Stats_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic418(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3348
.L3321:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3349
.L3323:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL46Builtin_Impl_NumberFormatPrototypeFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3350
.L3327:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3351
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3348:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3352
.L3322:
	movq	%rbx, _ZZN2v88internalL52Builtin_Impl_Stats_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic418(%rip)
	jmp	.L3321
	.p2align 4,,10
	.p2align 3
.L3349:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3353
.L3324:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3325
	movq	(%rdi), %rax
	call	*8(%rax)
.L3325:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3326
	movq	(%rdi), %rax
	call	*8(%rax)
.L3326:
	leaq	.LC120(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3323
	.p2align 4,,10
	.p2align 3
.L3350:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3327
	.p2align 4,,10
	.p2align 3
.L3347:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$896, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3319
	.p2align 4,,10
	.p2align 3
.L3353:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC120(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3324
	.p2align 4,,10
	.p2align 3
.L3352:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3322
.L3351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19288:
	.size	_ZN2v88internalL52Builtin_Impl_Stats_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE, .-_ZN2v88internalL52Builtin_Impl_Stats_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL48Builtin_Impl_Stats_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC121:
	.string	"V8.Builtin_V8BreakIteratorPrototypeFirst"
	.section	.text._ZN2v88internalL48Builtin_Impl_Stats_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Builtin_Impl_Stats_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE, @function
_ZN2v88internalL48Builtin_Impl_Stats_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE:
.LFB19448:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3383
.L3355:
	movq	_ZZN2v88internalL48Builtin_Impl_Stats_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateEE29trace_event_unique_atomic1118(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3384
.L3357:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3385
.L3359:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL42Builtin_Impl_V8BreakIteratorPrototypeFirstENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3386
.L3363:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3387
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3384:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3388
.L3358:
	movq	%rbx, _ZZN2v88internalL48Builtin_Impl_Stats_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateEE29trace_event_unique_atomic1118(%rip)
	jmp	.L3357
	.p2align 4,,10
	.p2align 3
.L3385:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3389
.L3360:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3361
	movq	(%rdi), %rax
	call	*8(%rax)
.L3361:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3362
	movq	(%rdi), %rax
	call	*8(%rax)
.L3362:
	leaq	.LC121(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3359
	.p2align 4,,10
	.p2align 3
.L3386:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3363
	.p2align 4,,10
	.p2align 3
.L3383:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$931, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3355
	.p2align 4,,10
	.p2align 3
.L3389:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC121(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3360
	.p2align 4,,10
	.p2align 3
.L3388:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3358
.L3387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19448:
	.size	_ZN2v88internalL48Builtin_Impl_Stats_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE, .-_ZN2v88internalL48Builtin_Impl_Stats_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC122:
	.string	"V8.Builtin_V8BreakIteratorPrototypeNext"
	.section	.text._ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE:
.LFB19454:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3419
.L3391:
	movq	_ZZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1148(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3420
.L3393:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3421
.L3395:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL41Builtin_Impl_V8BreakIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3422
.L3399:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3423
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3420:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3424
.L3394:
	movq	%rbx, _ZZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1148(%rip)
	jmp	.L3393
	.p2align 4,,10
	.p2align 3
.L3421:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3425
.L3396:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3397
	movq	(%rdi), %rax
	call	*8(%rax)
.L3397:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3398
	movq	(%rdi), %rax
	call	*8(%rax)
.L3398:
	leaq	.LC122(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3395
	.p2align 4,,10
	.p2align 3
.L3422:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3399
	.p2align 4,,10
	.p2align 3
.L3419:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$932, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3391
	.p2align 4,,10
	.p2align 3
.L3425:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC122(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3396
	.p2align 4,,10
	.p2align 3
.L3424:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3394
.L3423:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19454:
	.size	_ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL50Builtin_Impl_Stats_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC123:
	.string	"V8.Builtin_V8BreakIteratorPrototypeCurrent"
	.section	.text._ZN2v88internalL50Builtin_Impl_Stats_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL50Builtin_Impl_Stats_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE, @function
_ZN2v88internalL50Builtin_Impl_Stats_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE:
.LFB19460:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3455
.L3427:
	movq	_ZZN2v88internalL50Builtin_Impl_Stats_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1177(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3456
.L3429:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3457
.L3431:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL44Builtin_Impl_V8BreakIteratorPrototypeCurrentENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3458
.L3435:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3459
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3456:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3460
.L3430:
	movq	%rbx, _ZZN2v88internalL50Builtin_Impl_Stats_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1177(%rip)
	jmp	.L3429
	.p2align 4,,10
	.p2align 3
.L3457:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3461
.L3432:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3433
	movq	(%rdi), %rax
	call	*8(%rax)
.L3433:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3434
	movq	(%rdi), %rax
	call	*8(%rax)
.L3434:
	leaq	.LC123(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3431
	.p2align 4,,10
	.p2align 3
.L3458:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3435
	.p2align 4,,10
	.p2align 3
.L3455:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$930, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3427
	.p2align 4,,10
	.p2align 3
.L3461:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC123(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3432
	.p2align 4,,10
	.p2align 3
.L3460:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3430
.L3459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19460:
	.size	_ZN2v88internalL50Builtin_Impl_Stats_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE, .-_ZN2v88internalL50Builtin_Impl_Stats_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Builtin_Impl_Stats_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC124:
	.string	"V8.Builtin_SegmenterPrototypeSegment"
	.section	.text._ZN2v88internalL44Builtin_Impl_Stats_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_Stats_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_Stats_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE:
.LFB19433:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3491
.L3463:
	movq	_ZZN2v88internalL44Builtin_Impl_Stats_SegmenterPrototypeSegmentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1051(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3492
.L3465:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3493
.L3467:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL38Builtin_Impl_SegmenterPrototypeSegmentENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3494
.L3471:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3495
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3492:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3496
.L3466:
	movq	%rbx, _ZZN2v88internalL44Builtin_Impl_Stats_SegmenterPrototypeSegmentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1051(%rip)
	jmp	.L3465
	.p2align 4,,10
	.p2align 3
.L3493:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3497
.L3468:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3469
	movq	(%rdi), %rax
	call	*8(%rax)
.L3469:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3470
	movq	(%rdi), %rax
	call	*8(%rax)
.L3470:
	leaq	.LC124(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3467
	.p2align 4,,10
	.p2align 3
.L3494:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3471
	.p2align 4,,10
	.p2align 3
.L3491:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$911, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3463
	.p2align 4,,10
	.p2align 3
.L3497:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC124(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3468
	.p2align 4,,10
	.p2align 3
.L3496:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3466
.L3495:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19433:
	.size	_ZN2v88internalL44Builtin_Impl_Stats_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_Stats_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC125:
	.string	"V8.Builtin_V8BreakIteratorPrototypeBreakType"
	.section	.text._ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE:
.LFB19466:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3527
.L3499:
	movq	_ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1206(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3528
.L3501:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3529
.L3503:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3530
.L3507:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3531
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3528:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3532
.L3502:
	movq	%rbx, _ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1206(%rip)
	jmp	.L3501
	.p2align 4,,10
	.p2align 3
.L3529:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3533
.L3504:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3505
	movq	(%rdi), %rax
	call	*8(%rax)
.L3505:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3506
	movq	(%rdi), %rax
	call	*8(%rax)
.L3506:
	leaq	.LC125(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3503
	.p2align 4,,10
	.p2align 3
.L3530:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3507
	.p2align 4,,10
	.p2align 3
.L3527:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$929, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3499
	.p2align 4,,10
	.p2align 3
.L3533:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC125(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3504
	.p2align 4,,10
	.p2align 3
.L3532:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3502
.L3531:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19466:
	.size	_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE, .-_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_CollatorInternalCompareEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC126:
	.string	"V8.Builtin_CollatorInternalCompare"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_CollatorInternalCompareEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_CollatorInternalCompareEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_CollatorInternalCompareEiPmPNS0_7IsolateE:
.LFB19406:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3563
.L3535:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_CollatorInternalCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic936(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3564
.L3537:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3565
.L3539:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_CollatorInternalCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3566
.L3543:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3567
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3564:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3568
.L3538:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_CollatorInternalCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic936(%rip)
	jmp	.L3537
	.p2align 4,,10
	.p2align 3
.L3565:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3569
.L3540:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3541
	movq	(%rdi), %rax
	call	*8(%rax)
.L3541:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3542
	movq	(%rdi), %rax
	call	*8(%rax)
.L3542:
	leaq	.LC126(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3539
	.p2align 4,,10
	.p2align 3
.L3566:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3543
	.p2align 4,,10
	.p2align 3
.L3563:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$861, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3535
	.p2align 4,,10
	.p2align 3
.L3569:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC126(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3540
	.p2align 4,,10
	.p2align 3
.L3568:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3538
.L3567:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19406:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_CollatorInternalCompareEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_CollatorInternalCompareEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL51Builtin_Impl_Stats_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC127:
	.string	"V8.Builtin_NumberFormatInternalFormatNumber"
	.section	.text._ZN2v88internalL51Builtin_Impl_Stats_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL51Builtin_Impl_Stats_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internalL51Builtin_Impl_Stats_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE:
.LFB19291:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3599
.L3571:
	movq	_ZZN2v88internalL51Builtin_Impl_Stats_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic451(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3600
.L3573:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3601
.L3575:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL45Builtin_Impl_NumberFormatInternalFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3602
.L3579:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3603
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3600:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3604
.L3574:
	movq	%rbx, _ZZN2v88internalL51Builtin_Impl_Stats_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic451(%rip)
	jmp	.L3573
	.p2align 4,,10
	.p2align 3
.L3601:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3605
.L3576:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3577
	movq	(%rdi), %rax
	call	*8(%rax)
.L3577:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3578
	movq	(%rdi), %rax
	call	*8(%rax)
.L3578:
	leaq	.LC127(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3575
	.p2align 4,,10
	.p2align 3
.L3602:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3579
	.p2align 4,,10
	.p2align 3
.L3599:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$895, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3571
	.p2align 4,,10
	.p2align 3
.L3605:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC127(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3576
	.p2align 4,,10
	.p2align 3
.L3604:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3574
.L3603:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19291:
	.size	_ZN2v88internalL51Builtin_Impl_Stats_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE, .-_ZN2v88internalL51Builtin_Impl_Stats_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_Stats_ListFormatConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC128:
	.string	"V8.Builtin_ListFormatConstructor"
	.section	.text._ZN2v88internalL40Builtin_Impl_Stats_ListFormatConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_Stats_ListFormatConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_Stats_ListFormatConstructorEiPmPNS0_7IsolateE:
.LFB19306:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3637
.L3607:
	movq	_ZZN2v88internalL40Builtin_Impl_Stats_ListFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic554(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3638
.L3609:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3639
.L3611:
	addl	$1, 41104(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	call	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_12JSListFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	%rbx, 41096(%r12)
	je	.L3617
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3617:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3640
.L3606:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3641
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3639:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3642
.L3612:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3613
	movq	(%rdi), %rax
	call	*8(%rax)
.L3613:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3614
	movq	(%rdi), %rax
	call	*8(%rax)
.L3614:
	leaq	.LC128(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3611
	.p2align 4,,10
	.p2align 3
.L3638:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3643
.L3610:
	movq	%rbx, _ZZN2v88internalL40Builtin_Impl_Stats_ListFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic554(%rip)
	jmp	.L3609
	.p2align 4,,10
	.p2align 3
.L3637:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$877, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3607
	.p2align 4,,10
	.p2align 3
.L3640:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3606
	.p2align 4,,10
	.p2align 3
.L3643:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3610
	.p2align 4,,10
	.p2align 3
.L3642:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC128(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3612
.L3641:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19306:
	.size	_ZN2v88internalL40Builtin_Impl_Stats_ListFormatConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_Stats_ListFormatConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL48Builtin_Impl_Stats_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC129:
	.string	"V8.Builtin_RelativeTimeFormatConstructor"
	.section	.text._ZN2v88internalL48Builtin_Impl_Stats_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Builtin_Impl_Stats_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL48Builtin_Impl_Stats_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE:
.LFB19370:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3675
.L3645:
	movq	_ZZN2v88internalL48Builtin_Impl_Stats_RelativeTimeFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic796(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3676
.L3647:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3677
.L3649:
	addl	$1, 41104(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	call	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_20JSRelativeTimeFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	%rbx, 41096(%r12)
	je	.L3655
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3655:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3678
.L3644:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3679
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3677:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3680
.L3650:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3651
	movq	(%rdi), %rax
	call	*8(%rax)
.L3651:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3652
	movq	(%rdi), %rax
	call	*8(%rax)
.L3652:
	leaq	.LC129(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3649
	.p2align 4,,10
	.p2align 3
.L3676:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3681
.L3648:
	movq	%rbx, _ZZN2v88internalL48Builtin_Impl_Stats_RelativeTimeFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic796(%rip)
	jmp	.L3647
	.p2align 4,,10
	.p2align 3
.L3675:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$904, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3645
	.p2align 4,,10
	.p2align 3
.L3678:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3644
	.p2align 4,,10
	.p2align 3
.L3681:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3648
	.p2align 4,,10
	.p2align 3
.L3680:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC129(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3650
.L3679:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19370:
	.size	_ZN2v88internalL48Builtin_Impl_Stats_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL48Builtin_Impl_Stats_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_Stats_PluralRulesConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC130:
	.string	"V8.Builtin_PluralRulesConstructor"
	.section	.text._ZN2v88internalL41Builtin_Impl_Stats_PluralRulesConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_Stats_PluralRulesConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_Stats_PluralRulesConstructorEiPmPNS0_7IsolateE:
.LFB19382:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3713
.L3683:
	movq	_ZZN2v88internalL41Builtin_Impl_Stats_PluralRulesConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic835(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3714
.L3685:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3715
.L3687:
	addl	$1, 41104(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	call	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_13JSPluralRulesEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	%rbx, 41096(%r12)
	je	.L3693
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3693:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3716
.L3682:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3717
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3715:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3718
.L3688:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3689
	movq	(%rdi), %rax
	call	*8(%rax)
.L3689:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3690
	movq	(%rdi), %rax
	call	*8(%rax)
.L3690:
	leaq	.LC130(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3687
	.p2align 4,,10
	.p2align 3
.L3714:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3719
.L3686:
	movq	%rbx, _ZZN2v88internalL41Builtin_Impl_Stats_PluralRulesConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic835(%rip)
	jmp	.L3685
	.p2align 4,,10
	.p2align 3
.L3713:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$900, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3683
	.p2align 4,,10
	.p2align 3
.L3716:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3682
	.p2align 4,,10
	.p2align 3
.L3719:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3686
	.p2align 4,,10
	.p2align 3
.L3718:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC130(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3688
.L3717:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19382:
	.size	_ZN2v88internalL41Builtin_Impl_Stats_PluralRulesConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_Stats_PluralRulesConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL38Builtin_Impl_Stats_CollatorConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC131:
	.string	"V8.Builtin_CollatorConstructor"
	.section	.text._ZN2v88internalL38Builtin_Impl_Stats_CollatorConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_Stats_CollatorConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_Stats_CollatorConstructorEiPmPNS0_7IsolateE:
.LFB19394:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3749
.L3721:
	movq	_ZZN2v88internalL38Builtin_Impl_Stats_CollatorConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic882(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3750
.L3723:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3751
.L3725:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL32Builtin_Impl_CollatorConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3752
.L3729:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3753
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3750:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3754
.L3724:
	movq	%rbx, _ZZN2v88internalL38Builtin_Impl_Stats_CollatorConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic882(%rip)
	jmp	.L3723
	.p2align 4,,10
	.p2align 3
.L3751:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3755
.L3726:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3727
	movq	(%rdi), %rax
	call	*8(%rax)
.L3727:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3728
	movq	(%rdi), %rax
	call	*8(%rax)
.L3728:
	leaq	.LC131(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3725
	.p2align 4,,10
	.p2align 3
.L3752:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3729
	.p2align 4,,10
	.p2align 3
.L3749:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$860, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3721
	.p2align 4,,10
	.p2align 3
.L3755:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC131(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3726
	.p2align 4,,10
	.p2align 3
.L3754:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3724
.L3753:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19394:
	.size	_ZN2v88internalL38Builtin_Impl_Stats_CollatorConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_Stats_CollatorConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_Stats_SegmenterConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC132:
	.string	"V8.Builtin_SegmenterConstructor"
	.section	.text._ZN2v88internalL39Builtin_Impl_Stats_SegmenterConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_Stats_SegmenterConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_Stats_SegmenterConstructorEiPmPNS0_7IsolateE:
.LFB19424:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3787
.L3757:
	movq	_ZZN2v88internalL39Builtin_Impl_Stats_SegmenterConstructorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1024(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3788
.L3759:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3789
.L3761:
	addl	$1, 41104(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	call	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_11JSSegmenterEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	%rbx, 41096(%r12)
	je	.L3767
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3767:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3790
.L3756:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3791
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3789:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3792
.L3762:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3763
	movq	(%rdi), %rax
	call	*8(%rax)
.L3763:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3764
	movq	(%rdi), %rax
	call	*8(%rax)
.L3764:
	leaq	.LC132(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3761
	.p2align 4,,10
	.p2align 3
.L3788:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3793
.L3760:
	movq	%rbx, _ZZN2v88internalL39Builtin_Impl_Stats_SegmenterConstructorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1024(%rip)
	jmp	.L3759
	.p2align 4,,10
	.p2align 3
.L3787:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$909, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3757
	.p2align 4,,10
	.p2align 3
.L3790:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3756
	.p2align 4,,10
	.p2align 3
.L3793:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3760
	.p2align 4,,10
	.p2align 3
.L3792:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC132(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3762
.L3791:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19424:
	.size	_ZN2v88internalL39Builtin_Impl_Stats_SegmenterConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_Stats_SegmenterConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_Stats_V8BreakIteratorConstructorEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC133:
	.string	"V8.Builtin_V8BreakIteratorConstructor"
	.section	.text._ZN2v88internalL45Builtin_Impl_Stats_V8BreakIteratorConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_Stats_V8BreakIteratorConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_Stats_V8BreakIteratorConstructorEiPmPNS0_7IsolateE:
.LFB19436:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3834
.L3795:
	movq	_ZZN2v88internalL45Builtin_Impl_Stats_V8BreakIteratorConstructorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1069(%rip), %r13
	testq	%r13, %r13
	je	.L3835
.L3797:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L3836
.L3799:
	movq	41088(%r12), %rax
	movq	%rbx, %rcx
	movq	%rbx, %rsi
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	movq	%rax, -168(%rbp)
	leal	-8(,%r15,8), %eax
	movslq	%eax, %rdx
	subl	$8, %eax
	subq	%rdx, %rcx
	cltq
	subq	%rax, %rsi
	movq	%rcx, %rdx
	movq	88(%r12), %rax
	cmpq	%rax, (%rcx)
	cmove	%rsi, %rdx
	cmpl	$5, %r15d
	jg	.L3804
	leaq	88(%r12), %r15
	movq	%r15, %r14
.L3807:
	movq	%r12, %rdi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3833
.L3808:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_@PLT
	testq	%rax, %rax
	je	.L3833
	movq	(%rax), %r15
.L3809:
	subl	$1, 41104(%r12)
	movq	-168(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L3813
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3813:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3837
.L3794:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3838
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3804:
	.cfi_restore_state
	leaq	-8(%rbx), %r14
	cmpl	$6, %r15d
	je	.L3839
	movq	%r12, %rdi
	leaq	-16(%rbx), %r15
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L3808
.L3833:
	movq	312(%r12), %r15
	jmp	.L3809
	.p2align 4,,10
	.p2align 3
.L3836:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3840
.L3800:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3801
	movq	(%rdi), %rax
	call	*8(%rax)
.L3801:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3802
	movq	(%rdi), %rax
	call	*8(%rax)
.L3802:
	leaq	.LC133(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3799
	.p2align 4,,10
	.p2align 3
.L3835:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3841
.L3798:
	movq	%r13, _ZZN2v88internalL45Builtin_Impl_Stats_V8BreakIteratorConstructorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1069(%rip)
	jmp	.L3797
	.p2align 4,,10
	.p2align 3
.L3834:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$922, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3795
	.p2align 4,,10
	.p2align 3
.L3837:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3794
	.p2align 4,,10
	.p2align 3
.L3841:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L3798
	.p2align 4,,10
	.p2align 3
.L3840:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC133(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3800
.L3838:
	call	__stack_chk_fail@PLT
.L3839:
	leaq	88(%r12), %r15
	jmp	.L3807
	.cfi_endproc
.LFE19436:
	.size	_ZN2v88internalL45Builtin_Impl_Stats_V8BreakIteratorConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_Stats_V8BreakIteratorConstructorEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL49Builtin_Impl_Stats_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC134:
	.string	"V8.Builtin_StringPrototypeToUpperCaseIntl"
	.section	.text._ZN2v88internalL49Builtin_Impl_Stats_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL49Builtin_Impl_Stats_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE, @function
_ZN2v88internalL49Builtin_Impl_Stats_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE:
.LFB19247:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3871
.L3843:
	movq	_ZZN2v88internalL49Builtin_Impl_Stats_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic39(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3872
.L3845:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3873
.L3847:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL43Builtin_Impl_StringPrototypeToUpperCaseIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3874
.L3851:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3875
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3872:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3876
.L3846:
	movq	%rbx, _ZZN2v88internalL49Builtin_Impl_Stats_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic39(%rip)
	jmp	.L3845
	.p2align 4,,10
	.p2align 3
.L3873:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3877
.L3848:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3849
	movq	(%rdi), %rax
	call	*8(%rax)
.L3849:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3850
	movq	(%rdi), %rax
	call	*8(%rax)
.L3850:
	leaq	.LC134(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3847
	.p2align 4,,10
	.p2align 3
.L3874:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3851
	.p2align 4,,10
	.p2align 3
.L3871:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$921, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3843
	.p2align 4,,10
	.p2align 3
.L3877:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC134(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3848
	.p2align 4,,10
	.p2align 3
.L3876:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3846
.L3875:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19247:
	.size	_ZN2v88internalL49Builtin_Impl_Stats_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE, .-_ZN2v88internalL49Builtin_Impl_Stats_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Builtin_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Builtin_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Builtin_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Builtin_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE:
.LFB19248:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3882
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL43Builtin_Impl_StringPrototypeToUpperCaseIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3882:
	.cfi_restore 6
	jmp	_ZN2v88internalL49Builtin_Impl_Stats_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19248:
	.size	_ZN2v88internal38Builtin_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE, .-_ZN2v88internal38Builtin_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE:
.LFB19251:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3887
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL41Builtin_Impl_StringPrototypeNormalizeIntlENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3887:
	.cfi_restore 6
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19251:
	.size	_ZN2v88internal36Builtin_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Builtin_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Builtin_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Builtin_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Builtin_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3899
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	cmpl	$5, %edi
	jle	.L3900
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L3901
	leaq	-16(%rsi), %rbx
.L3893:
	call	_ZN2v88internal17JSV8BreakIterator19GetAvailableLocalesB5cxx11Ev@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC43(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L3902
	movq	(%rax), %r15
.L3895:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L3888
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3888:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3900:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	movq	%rbx, %r15
	jmp	.L3893
	.p2align 4,,10
	.p2align 3
.L3902:
	movq	312(%r12), %r15
	jmp	.L3895
	.p2align 4,,10
	.p2align 3
.L3899:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE
.L3901:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	jmp	.L3893
	.cfi_endproc
.LFE19254:
	.size	_ZN2v88internal41Builtin_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internal41Builtin_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Builtin_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Builtin_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Builtin_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Builtin_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3914
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	cmpl	$5, %edi
	jle	.L3915
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L3916
	leaq	-16(%rsi), %rbx
.L3908:
	call	_ZN2v88internal14JSNumberFormat19GetAvailableLocalesB5cxx11Ev@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC45(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L3917
	movq	(%rax), %r15
.L3910:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L3903
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3903:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3915:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	movq	%rbx, %r15
	jmp	.L3908
	.p2align 4,,10
	.p2align 3
.L3917:
	movq	312(%r12), %r15
	jmp	.L3910
	.p2align 4,,10
	.p2align 3
.L3914:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL49Builtin_Impl_Stats_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE
.L3916:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	jmp	.L3908
	.cfi_endproc
.LFE19257:
	.size	_ZN2v88internal38Builtin_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internal38Builtin_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal42Builtin_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal42Builtin_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal42Builtin_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE, @function
_ZN2v88internal42Builtin_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE:
.LFB19260:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3922
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL47Builtin_Impl_NumberFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3922:
	.cfi_restore 6
	jmp	_ZN2v88internalL53Builtin_Impl_Stats_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19260:
	.size	_ZN2v88internal42Builtin_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE, .-_ZN2v88internal42Builtin_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal46Builtin_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal46Builtin_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal46Builtin_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internal46Builtin_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19263:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3927
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL51Builtin_Impl_DateTimeFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3927:
	.cfi_restore 6
	jmp	_ZN2v88internalL57Builtin_Impl_Stats_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19263:
	.size	_ZN2v88internal46Builtin_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internal46Builtin_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Builtin_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Builtin_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Builtin_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Builtin_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3939
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	cmpl	$5, %edi
	jle	.L3940
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L3941
	leaq	-16(%rsi), %rbx
.L3933:
	call	_ZN2v88internal16JSDateTimeFormat19GetAvailableLocalesB5cxx11Ev@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC47(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L3942
	movq	(%rax), %r15
.L3935:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L3928
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3928:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3940:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	movq	%rbx, %r15
	jmp	.L3933
	.p2align 4,,10
	.p2align 3
.L3942:
	movq	312(%r12), %r15
	jmp	.L3935
	.p2align 4,,10
	.p2align 3
.L3939:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL51Builtin_Impl_Stats_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE
.L3941:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	jmp	.L3933
	.cfi_endproc
.LFE19266:
	.size	_ZN2v88internal40Builtin_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internal40Builtin_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal44Builtin_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal44Builtin_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal44Builtin_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE, @function
_ZN2v88internal44Builtin_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE:
.LFB19269:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3947
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL49Builtin_Impl_DateTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3947:
	.cfi_restore 6
	jmp	_ZN2v88internalL55Builtin_Impl_Stats_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19269:
	.size	_ZN2v88internal44Builtin_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE, .-_ZN2v88internal44Builtin_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_NumberFormatConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_NumberFormatConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_NumberFormatConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_NumberFormatConstructorEiPmPNS0_7IsolateE:
.LFB19283:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3952
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_NumberFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3952:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_NumberFormatConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19283:
	.size	_ZN2v88internal31Builtin_NumberFormatConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_NumberFormatConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal44Builtin_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal44Builtin_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal44Builtin_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internal44Builtin_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19286:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3957
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL49Builtin_Impl_NumberFormatPrototypeResolvedOptionsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3957:
	.cfi_restore 6
	jmp	_ZN2v88internalL55Builtin_Impl_Stats_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19286:
	.size	_ZN2v88internal44Builtin_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internal44Builtin_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Builtin_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Builtin_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Builtin_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Builtin_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE:
.LFB19289:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3962
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL46Builtin_Impl_NumberFormatPrototypeFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3962:
	.cfi_restore 6
	jmp	_ZN2v88internalL52Builtin_Impl_Stats_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19289:
	.size	_ZN2v88internal41Builtin_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE, .-_ZN2v88internal41Builtin_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Builtin_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Builtin_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Builtin_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Builtin_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE:
.LFB19292:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3967
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL45Builtin_Impl_NumberFormatInternalFormatNumberENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3967:
	.cfi_restore 6
	jmp	_ZN2v88internalL51Builtin_Impl_Stats_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19292:
	.size	_ZN2v88internal40Builtin_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE, .-_ZN2v88internal40Builtin_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Builtin_DateTimeFormatConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Builtin_DateTimeFormatConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Builtin_DateTimeFormatConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Builtin_DateTimeFormatConstructorEiPmPNS0_7IsolateE:
.LFB19295:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3972
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL38Builtin_Impl_DateTimeFormatConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3972:
	.cfi_restore 6
	jmp	_ZN2v88internalL44Builtin_Impl_Stats_DateTimeFormatConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19295:
	.size	_ZN2v88internal33Builtin_DateTimeFormatConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal33Builtin_DateTimeFormatConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Builtin_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Builtin_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Builtin_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Builtin_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE:
.LFB19298:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3977
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL42Builtin_Impl_DateTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3977:
	.cfi_restore 6
	jmp	_ZN2v88internalL48Builtin_Impl_Stats_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19298:
	.size	_ZN2v88internal37Builtin_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE, .-_ZN2v88internal37Builtin_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE:
.LFB19301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3994
	movq	41112(%rdx), %rdi
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	movq	12464(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L3980
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3981:
	movq	47(%rsi), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3983
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3984:
	subq	$8, %r13
	leaq	88(%r12), %rdx
	cmpl	$5, %r15d
	movq	%r12, %rdi
	cmovg	%r13, %rdx
	call	_ZN2v88internal16JSDateTimeFormat14DateTimeFormatEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L3995
	movq	(%rax), %r13
.L3989:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L3978
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3978:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3980:
	.cfi_restore_state
	movq	%rbx, %rax
	cmpq	%r14, %rbx
	je	.L3996
.L3982:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3981
	.p2align 4,,10
	.p2align 3
.L3983:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3997
.L3985:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L3984
	.p2align 4,,10
	.p2align 3
.L3995:
	movq	312(%r12), %r13
	jmp	.L3989
	.p2align 4,,10
	.p2align 3
.L3994:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L3997:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L3985
	.p2align 4,,10
	.p2align 3
.L3996:
	movq	%rdx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L3982
	.cfi_endproc
.LFE19301:
	.size	_ZN2v88internal36Builtin_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_DateTimeFormatInternalFormatEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE:
.LFB19304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4007
	addl	$1, 41104(%rdx)
	subq	$8, %rsi
	leaq	88(%rdx), %rax
	cmpl	$5, %edi
	cmovle	%rax, %rsi
	movq	%rdx, %rdi
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	call	_ZN2v88internal4Intl19GetCanonicalLocalesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L4008
	movq	(%rax), %r14
.L4003:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L3998
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3998:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4008:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L4003
	.p2align 4,,10
	.p2align 3
.L4007:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19304:
	.size	_ZN2v88internal31Builtin_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_IntlGetCanonicalLocalesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Builtin_ListFormatConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Builtin_ListFormatConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Builtin_ListFormatConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Builtin_ListFormatConstructorEiPmPNS0_7IsolateE:
.LFB19307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4013
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movslq	%edi, %rdi
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_12JSListFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L4011
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4011:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4013:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Builtin_Impl_Stats_ListFormatConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19307:
	.size	_ZN2v88internal29Builtin_ListFormatConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal29Builtin_ListFormatConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal42Builtin_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal42Builtin_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal42Builtin_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internal42Builtin_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4027
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4017
.L4020:
	leaq	.LC106(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$41, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4028
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4021:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4014
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4014:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4029
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4017:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1091, 11(%rax)
	jne	.L4020
	movq	%rdx, %rdi
	call	_ZN2v88internal12JSListFormat15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4021
	.p2align 4,,10
	.p2align 3
.L4027:
	call	_ZN2v88internalL53Builtin_Impl_Stats_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	jmp	.L4014
	.p2align 4,,10
	.p2align 3
.L4028:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4029:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19310:
	.size	_ZN2v88internal42Builtin_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internal42Builtin_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4041
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	cmpl	$5, %edi
	jle	.L4042
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L4043
	leaq	-16(%rsi), %rbx
.L4035:
	call	_ZN2v88internal12JSListFormat19GetAvailableLocalesB5cxx11Ev@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC49(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L4044
	movq	(%rax), %r15
.L4037:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L4030
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4030:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4042:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	movq	%rbx, %r15
	jmp	.L4035
	.p2align 4,,10
	.p2align 3
.L4044:
	movq	312(%r12), %r15
	jmp	.L4037
	.p2align 4,,10
	.p2align 3
.L4041:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE
.L4043:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	jmp	.L4035
	.cfi_endproc
.LFE19313:
	.size	_ZN2v88internal36Builtin_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Builtin_LocaleConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Builtin_LocaleConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Builtin_LocaleConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Builtin_LocaleConstructorEiPmPNS0_7IsolateE:
.LFB19320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4060
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movl	$59, %esi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	leal	-8(,%r15,8), %eax
	movq	%r13, %rcx
	movslq	%eax, %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	movq	88(%r12), %rcx
	cmpq	%rcx, (%rdx)
	je	.L4061
	subl	$8, %eax
	movq	%r13, %rsi
	cltq
	subq	%rax, %rsi
	cmpl	$5, %r15d
	jle	.L4062
	leaq	-8(%r13), %rcx
	cmpl	$6, %r15d
	je	.L4063
	leaq	-16(%r13), %r8
.L4054:
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112CreateLocaleEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEESA_
	testq	%rax, %rax
	je	.L4064
.L4055:
	movq	(%rax), %r13
.L4050:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4045
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4045:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4065
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4062:
	.cfi_restore_state
	leaq	88(%r12), %r8
	movq	%r12, %rdi
	movq	%r8, %rcx
	call	_ZN2v88internal12_GLOBAL__N_112CreateLocaleEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_10JSReceiverEEENS4_INS0_6ObjectEEESA_
	testq	%rax, %rax
	jne	.L4055
.L4064:
	movq	312(%r12), %r13
	jmp	.L4050
	.p2align 4,,10
	.p2align 3
.L4061:
	leaq	.LC89(%rip), %rax
	xorl	%edx, %edx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$11, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4066
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
	jmp	.L4050
	.p2align 4,,10
	.p2align 3
.L4060:
	call	_ZN2v88internalL36Builtin_Impl_Stats_LocaleConstructorEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L4045
	.p2align 4,,10
	.p2align 3
.L4066:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4065:
	call	__stack_chk_fail@PLT
.L4063:
	leaq	88(%r12), %r8
	jmp	.L4054
	.cfi_endproc
.LFE19320:
	.size	_ZN2v88internal25Builtin_LocaleConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal25Builtin_LocaleConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_LocalePrototypeMaximizeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_LocalePrototypeMaximizeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_LocalePrototypeMaximizeEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_LocalePrototypeMaximizeEiPmPNS0_7IsolateE:
.LFB19323:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4071
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_LocalePrototypeMaximizeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4071:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMaximizeEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19323:
	.size	_ZN2v88internal31Builtin_LocalePrototypeMaximizeEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_LocalePrototypeMaximizeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_LocalePrototypeMinimizeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_LocalePrototypeMinimizeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_LocalePrototypeMinimizeEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_LocalePrototypeMinimizeEiPmPNS0_7IsolateE:
.LFB19326:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4076
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_LocalePrototypeMinimizeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4076:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMinimizeEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19326:
	.size	_ZN2v88internal31Builtin_LocalePrototypeMinimizeEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_LocalePrototypeMinimizeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal44Builtin_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal44Builtin_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal44Builtin_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal44Builtin_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4088
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	cmpl	$5, %edi
	jle	.L4089
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L4090
	leaq	-16(%rsi), %rbx
.L4082:
	call	_ZN2v88internal20JSRelativeTimeFormat19GetAvailableLocalesB5cxx11Ev@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC51(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L4091
	movq	(%rax), %r15
.L4084:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L4077
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4077:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4089:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	movq	%rbx, %r15
	jmp	.L4082
	.p2align 4,,10
	.p2align 3
.L4091:
	movq	312(%r12), %r15
	jmp	.L4084
	.p2align 4,,10
	.p2align 3
.L4088:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL55Builtin_Impl_Stats_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE
.L4090:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	jmp	.L4082
	.cfi_endproc
.LFE19329:
	.size	_ZN2v88internal44Builtin_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internal44Builtin_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Builtin_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Builtin_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Builtin_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Builtin_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE:
.LFB19332:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4096
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL46Builtin_Impl_RelativeTimeFormatPrototypeFormatENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4096:
	.cfi_restore 6
	jmp	_ZN2v88internalL52Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19332:
	.size	_ZN2v88internal41Builtin_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE, .-_ZN2v88internal41Builtin_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal48Builtin_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal48Builtin_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal48Builtin_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE, @function
_ZN2v88internal48Builtin_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE:
.LFB19335:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4101
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL53Builtin_Impl_RelativeTimeFormatPrototypeFormatToPartsENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4101:
	.cfi_restore 6
	jmp	_ZN2v88internalL59Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19335:
	.size	_ZN2v88internal48Builtin_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE, .-_ZN2v88internal48Builtin_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_LocalePrototypeLanguageEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_LocalePrototypeLanguageEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_LocalePrototypeLanguageEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_LocalePrototypeLanguageEiPmPNS0_7IsolateE:
.LFB19338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4115
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4105
.L4108:
	leaq	.LC67(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$30, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4116
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4109:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4102
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4102:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4117
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4105:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L4108
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSLocale8LanguageEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4109
	.p2align 4,,10
	.p2align 3
.L4115:
	call	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeLanguageEiPmPNS0_7IsolateE
	jmp	.L4102
	.p2align 4,,10
	.p2align 3
.L4116:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19338:
	.size	_ZN2v88internal31Builtin_LocalePrototypeLanguageEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_LocalePrototypeLanguageEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Builtin_LocalePrototypeScriptEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Builtin_LocalePrototypeScriptEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Builtin_LocalePrototypeScriptEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Builtin_LocalePrototypeScriptEiPmPNS0_7IsolateE:
.LFB19341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4131
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4121
.L4124:
	leaq	.LC69(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$28, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4132
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4125:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4118
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4118:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4133
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4121:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L4124
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSLocale6ScriptEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4125
	.p2align 4,,10
	.p2align 3
.L4131:
	call	_ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateE
	jmp	.L4118
	.p2align 4,,10
	.p2align 3
.L4132:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4133:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19341:
	.size	_ZN2v88internal29Builtin_LocalePrototypeScriptEiPmPNS0_7IsolateE, .-_ZN2v88internal29Builtin_LocalePrototypeScriptEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Builtin_LocalePrototypeRegionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Builtin_LocalePrototypeRegionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Builtin_LocalePrototypeRegionEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Builtin_LocalePrototypeRegionEiPmPNS0_7IsolateE:
.LFB19344:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4147
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4137
.L4140:
	leaq	.LC71(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$28, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4148
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4141:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4134
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4134:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4149
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4137:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L4140
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSLocale6RegionEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4141
	.p2align 4,,10
	.p2align 3
.L4147:
	call	_ZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateE
	jmp	.L4134
	.p2align 4,,10
	.p2align 3
.L4148:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19344:
	.size	_ZN2v88internal29Builtin_LocalePrototypeRegionEiPmPNS0_7IsolateE, .-_ZN2v88internal29Builtin_LocalePrototypeRegionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_LocalePrototypeBaseNameEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_LocalePrototypeBaseNameEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_LocalePrototypeBaseNameEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_LocalePrototypeBaseNameEiPmPNS0_7IsolateE:
.LFB19347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4163
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4153
.L4156:
	leaq	.LC73(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$30, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4164
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4157:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4150
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4150:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4165
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4153:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L4156
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSLocale8BaseNameEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4157
	.p2align 4,,10
	.p2align 3
.L4163:
	call	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeBaseNameEiPmPNS0_7IsolateE
	jmp	.L4150
	.p2align 4,,10
	.p2align 3
.L4164:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19347:
	.size	_ZN2v88internal31Builtin_LocalePrototypeBaseNameEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_LocalePrototypeBaseNameEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_LocalePrototypeCalendarEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_LocalePrototypeCalendarEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_LocalePrototypeCalendarEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_LocalePrototypeCalendarEiPmPNS0_7IsolateE:
.LFB19350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4179
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4169
.L4172:
	leaq	.LC75(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$30, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4180
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4173:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4166
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4166:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4181
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4169:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L4172
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSLocale8CalendarEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4173
	.p2align 4,,10
	.p2align 3
.L4179:
	call	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeCalendarEiPmPNS0_7IsolateE
	jmp	.L4166
	.p2align 4,,10
	.p2align 3
.L4180:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4181:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19350:
	.size	_ZN2v88internal31Builtin_LocalePrototypeCalendarEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_LocalePrototypeCalendarEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE:
.LFB19353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4195
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4185
.L4188:
	leaq	.LC77(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$31, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4196
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4189:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4182
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4182:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4197
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4185:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L4188
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSLocale9CaseFirstEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4189
	.p2align 4,,10
	.p2align 3
.L4195:
	call	_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE
	jmp	.L4182
	.p2align 4,,10
	.p2align 3
.L4196:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19353:
	.size	_ZN2v88internal32Builtin_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_LocalePrototypeCaseFirstEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_LocalePrototypeCollationEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_LocalePrototypeCollationEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_LocalePrototypeCollationEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_LocalePrototypeCollationEiPmPNS0_7IsolateE:
.LFB19356:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4211
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4201
.L4204:
	leaq	.LC79(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$31, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4212
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4205:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4198
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4198:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4213
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4201:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L4204
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSLocale9CollationEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4205
	.p2align 4,,10
	.p2align 3
.L4211:
	call	_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCollationEiPmPNS0_7IsolateE
	jmp	.L4198
	.p2align 4,,10
	.p2align 3
.L4212:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4213:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19356:
	.size	_ZN2v88internal32Builtin_LocalePrototypeCollationEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_LocalePrototypeCollationEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_LocalePrototypeHourCycleEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_LocalePrototypeHourCycleEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_LocalePrototypeHourCycleEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_LocalePrototypeHourCycleEiPmPNS0_7IsolateE:
.LFB19359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4227
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4217
.L4220:
	leaq	.LC81(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$31, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4228
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4221:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4214
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4214:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4229
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4217:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L4220
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSLocale9HourCycleEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4221
	.p2align 4,,10
	.p2align 3
.L4227:
	call	_ZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeHourCycleEiPmPNS0_7IsolateE
	jmp	.L4214
	.p2align 4,,10
	.p2align 3
.L4228:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4229:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19359:
	.size	_ZN2v88internal32Builtin_LocalePrototypeHourCycleEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_LocalePrototypeHourCycleEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Builtin_LocalePrototypeNumericEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Builtin_LocalePrototypeNumericEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Builtin_LocalePrototypeNumericEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Builtin_LocalePrototypeNumericEiPmPNS0_7IsolateE:
.LFB19362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4243
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4233
.L4236:
	leaq	.LC83(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$29, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4244
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4237:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4230
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4230:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4245
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4233:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L4236
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSLocale7NumericEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4237
	.p2align 4,,10
	.p2align 3
.L4243:
	call	_ZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateE
	jmp	.L4230
	.p2align 4,,10
	.p2align 3
.L4244:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4245:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19362:
	.size	_ZN2v88internal30Builtin_LocalePrototypeNumericEiPmPNS0_7IsolateE, .-_ZN2v88internal30Builtin_LocalePrototypeNumericEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Builtin_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Builtin_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Builtin_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Builtin_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE:
.LFB19365:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4259
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4249
.L4252:
	leaq	.LC85(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$37, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4260
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4253:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4246
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4246:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4261
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4249:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L4252
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSLocale15NumberingSystemEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4253
	.p2align 4,,10
	.p2align 3
.L4259:
	call	_ZN2v88internalL49Builtin_Impl_Stats_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE
	jmp	.L4246
	.p2align 4,,10
	.p2align 3
.L4260:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4261:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19365:
	.size	_ZN2v88internal38Builtin_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE, .-_ZN2v88internal38Builtin_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_LocalePrototypeToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_LocalePrototypeToStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_LocalePrototypeToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_LocalePrototypeToStringEiPmPNS0_7IsolateE:
.LFB19368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4275
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4265
.L4268:
	leaq	.LC87(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$30, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4276
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4269:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4262
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4262:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4277
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4265:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1092, 11(%rax)
	jne	.L4268
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSLocale8ToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4269
	.p2align 4,,10
	.p2align 3
.L4275:
	call	_ZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeToStringEiPmPNS0_7IsolateE
	jmp	.L4262
	.p2align 4,,10
	.p2align 3
.L4276:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19368:
	.size	_ZN2v88internal31Builtin_LocalePrototypeToStringEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_LocalePrototypeToStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Builtin_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Builtin_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Builtin_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Builtin_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE:
.LFB19371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4282
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movslq	%edi, %rdi
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_20JSRelativeTimeFormatEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L4280
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4280:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4282:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL48Builtin_Impl_Stats_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19371:
	.size	_ZN2v88internal37Builtin_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal37Builtin_RelativeTimeFormatConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal50Builtin_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal50Builtin_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal50Builtin_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internal50Builtin_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4296
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4286
.L4289:
	leaq	.LC108(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$49, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4297
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4290:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4283
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4283:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4298
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4286:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1095, 11(%rax)
	jne	.L4289
	movq	%rdx, %rdi
	call	_ZN2v88internal20JSRelativeTimeFormat15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4290
	.p2align 4,,10
	.p2align 3
.L4296:
	call	_ZN2v88internalL61Builtin_Impl_Stats_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	jmp	.L4283
	.p2align 4,,10
	.p2align 3
.L4297:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19374:
	.size	_ZN2v88internal50Builtin_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internal50Builtin_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Builtin_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Builtin_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Builtin_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Builtin_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE:
.LFB19377:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4303
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleLowerCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4303:
	.cfi_restore 6
	jmp	_ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19377:
	.size	_ZN2v88internal40Builtin_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE, .-_ZN2v88internal40Builtin_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Builtin_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Builtin_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Builtin_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Builtin_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE:
.LFB19380:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4308
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL45Builtin_Impl_StringPrototypeToLocaleUpperCaseENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4308:
	.cfi_restore 6
	jmp	_ZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19380:
	.size	_ZN2v88internal40Builtin_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE, .-_ZN2v88internal40Builtin_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Builtin_PluralRulesConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Builtin_PluralRulesConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Builtin_PluralRulesConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Builtin_PluralRulesConstructorEiPmPNS0_7IsolateE:
.LFB19383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4313
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movslq	%edi, %rdi
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_13JSPluralRulesEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L4311
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4311:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4313:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL41Builtin_Impl_Stats_PluralRulesConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19383:
	.size	_ZN2v88internal30Builtin_PluralRulesConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal30Builtin_PluralRulesConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal43Builtin_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal43Builtin_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal43Builtin_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internal43Builtin_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4327
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4317
.L4320:
	leaq	.LC110(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$42, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4328
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4321:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4314
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4314:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4329
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4317:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1094, 11(%rax)
	jne	.L4320
	movq	%rdx, %rdi
	call	_ZN2v88internal13JSPluralRules15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4321
	.p2align 4,,10
	.p2align 3
.L4327:
	call	_ZN2v88internalL54Builtin_Impl_Stats_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	jmp	.L4314
	.p2align 4,,10
	.p2align 3
.L4328:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4329:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19386:
	.size	_ZN2v88internal43Builtin_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internal43Builtin_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Builtin_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Builtin_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Builtin_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Builtin_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE:
.LFB19389:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4334
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL39Builtin_Impl_PluralRulesPrototypeSelectENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4334:
	.cfi_restore 6
	jmp	_ZN2v88internalL45Builtin_Impl_Stats_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19389:
	.size	_ZN2v88internal34Builtin_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE, .-_ZN2v88internal34Builtin_PluralRulesPrototypeSelectEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Builtin_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Builtin_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Builtin_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Builtin_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4346
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	cmpl	$5, %edi
	jle	.L4347
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L4348
	leaq	-16(%rsi), %rbx
.L4340:
	call	_ZN2v88internal13JSPluralRules19GetAvailableLocalesB5cxx11Ev@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC53(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L4349
	movq	(%rax), %r15
.L4342:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L4335
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4335:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4347:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	movq	%rbx, %r15
	jmp	.L4340
	.p2align 4,,10
	.p2align 3
.L4349:
	movq	312(%r12), %r15
	jmp	.L4342
	.p2align 4,,10
	.p2align 3
.L4346:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL48Builtin_Impl_Stats_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE
.L4348:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	jmp	.L4340
	.cfi_endproc
.LFE19392:
	.size	_ZN2v88internal37Builtin_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internal37Builtin_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Builtin_CollatorConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Builtin_CollatorConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Builtin_CollatorConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Builtin_CollatorConstructorEiPmPNS0_7IsolateE:
.LFB19395:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4354
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL32Builtin_Impl_CollatorConstructorENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4354:
	.cfi_restore 6
	jmp	_ZN2v88internalL38Builtin_Impl_Stats_CollatorConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19395:
	.size	_ZN2v88internal27Builtin_CollatorConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal27Builtin_CollatorConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Builtin_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Builtin_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Builtin_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Builtin_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4368
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4358
.L4361:
	leaq	.LC112(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$39, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4369
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4362:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4355
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4355:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4370
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4358:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1089, 11(%rax)
	jne	.L4361
	movq	%rdx, %rdi
	call	_ZN2v88internal10JSCollator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4362
	.p2align 4,,10
	.p2align 3
.L4368:
	call	_ZN2v88internalL51Builtin_Impl_Stats_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	jmp	.L4355
	.p2align 4,,10
	.p2align 3
.L4369:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4370:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19398:
	.size	_ZN2v88internal40Builtin_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internal40Builtin_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Builtin_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Builtin_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Builtin_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Builtin_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4382
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	cmpl	$5, %edi
	jle	.L4383
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L4384
	leaq	-16(%rsi), %rbx
.L4376:
	call	_ZN2v88internal10JSCollator19GetAvailableLocalesB5cxx11Ev@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC55(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L4385
	movq	(%rax), %r15
.L4378:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L4371
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4371:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4383:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	movq	%rbx, %r15
	jmp	.L4376
	.p2align 4,,10
	.p2align 3
.L4385:
	movq	312(%r12), %r15
	jmp	.L4378
	.p2align 4,,10
	.p2align 3
.L4382:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL45Builtin_Impl_Stats_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE
.L4384:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	jmp	.L4376
	.cfi_endproc
.LFE19401:
	.size	_ZN2v88internal34Builtin_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internal34Builtin_CollatorSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Builtin_CollatorPrototypeCompareEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Builtin_CollatorPrototypeCompareEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Builtin_CollatorPrototypeCompareEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Builtin_CollatorPrototypeCompareEiPmPNS0_7IsolateE:
.LFB19404:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4390
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL37Builtin_Impl_CollatorPrototypeCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4390:
	.cfi_restore 6
	jmp	_ZN2v88internalL43Builtin_Impl_Stats_CollatorPrototypeCompareEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19404:
	.size	_ZN2v88internal32Builtin_CollatorPrototypeCompareEiPmPNS0_7IsolateE, .-_ZN2v88internal32Builtin_CollatorPrototypeCompareEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_CollatorInternalCompareEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_CollatorInternalCompareEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_CollatorInternalCompareEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_CollatorInternalCompareEiPmPNS0_7IsolateE:
.LFB19407:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4395
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_CollatorInternalCompareENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4395:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_CollatorInternalCompareEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19407:
	.size	_ZN2v88internal31Builtin_CollatorInternalCompareEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_CollatorInternalCompareEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Builtin_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Builtin_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Builtin_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Builtin_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE:
.LFB19410:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4400
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4400:
	.cfi_restore 6
	jmp	_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19410:
	.size	_ZN2v88internal41Builtin_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE, .-_ZN2v88internal41Builtin_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Builtin_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Builtin_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Builtin_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Builtin_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE:
.LFB19413:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4405
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypeFollowingENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4405:
	.cfi_restore 6
	jmp	_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19413:
	.size	_ZN2v88internal41Builtin_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE, .-_ZN2v88internal41Builtin_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE:
.LFB19416:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4410
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL41Builtin_Impl_SegmentIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4410:
	.cfi_restore 6
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19416:
	.size	_ZN2v88internal36Builtin_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Builtin_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Builtin_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Builtin_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Builtin_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE:
.LFB19419:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4415
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL46Builtin_Impl_SegmentIteratorPrototypePrecedingENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4415:
	.cfi_restore 6
	jmp	_ZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19419:
	.size	_ZN2v88internal41Builtin_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE, .-_ZN2v88internal41Builtin_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Builtin_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Builtin_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Builtin_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Builtin_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE:
.LFB19422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4429
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4419
.L4422:
	leaq	.LC97(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$36, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4430
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4423:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4416
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4416:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4431
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4419:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1096, 11(%rax)
	jne	.L4422
	movq	%rdx, %rdi
	call	_ZN2v88internal17JSSegmentIterator5IndexEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4423
	.p2align 4,,10
	.p2align 3
.L4429:
	call	_ZN2v88internalL48Builtin_Impl_Stats_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE
	jmp	.L4416
	.p2align 4,,10
	.p2align 3
.L4430:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4431:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19422:
	.size	_ZN2v88internal37Builtin_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE, .-_ZN2v88internal37Builtin_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Builtin_SegmenterConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Builtin_SegmenterConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Builtin_SegmenterConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Builtin_SegmenterConstructorEiPmPNS0_7IsolateE:
.LFB19425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4436
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movslq	%edi, %rdi
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal12_GLOBAL__N_123DisallowCallConstructorINS0_11JSSegmenterEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateENS_7Isolate17UseCounterFeatureEPKc.constprop.0
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L4434
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4434:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4436:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Builtin_Impl_Stats_SegmenterConstructorEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19425:
	.size	_ZN2v88internal28Builtin_SegmenterConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal28Builtin_SegmenterConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Builtin_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Builtin_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Builtin_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Builtin_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE:
.LFB19428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4448
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %r14
	cmpl	$5, %edi
	jle	.L4449
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L4450
	leaq	-16(%rsi), %rbx
.L4442:
	call	_ZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11Ev@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	leaq	.LC57(%rip), %rsi
	call	_ZN2v88internal4Intl18SupportedLocalesOfEPNS0_7IsolateEPKcRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISC_ESaISC_EENS0_6HandleINS0_6ObjectEEESL_@PLT
	testq	%rax, %rax
	je	.L4451
	movq	(%rax), %r15
.L4444:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L4437
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4437:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4449:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	movq	%rbx, %r15
	jmp	.L4442
	.p2align 4,,10
	.p2align 3
.L4451:
	movq	312(%r12), %r15
	jmp	.L4444
	.p2align 4,,10
	.p2align 3
.L4448:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL46Builtin_Impl_Stats_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE
.L4450:
	.cfi_restore_state
	leaq	88(%rdx), %rbx
	jmp	.L4442
	.cfi_endproc
.LFE19428:
	.size	_ZN2v88internal35Builtin_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE, .-_ZN2v88internal35Builtin_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Builtin_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Builtin_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Builtin_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Builtin_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4465
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4455
.L4458:
	leaq	.LC114(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$40, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4466
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4459:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4452
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4452:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4467
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4455:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1097, 11(%rax)
	jne	.L4458
	movq	%rdx, %rdi
	call	_ZN2v88internal11JSSegmenter15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4459
	.p2align 4,,10
	.p2align 3
.L4465:
	call	_ZN2v88internalL52Builtin_Impl_Stats_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	jmp	.L4452
	.p2align 4,,10
	.p2align 3
.L4466:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4467:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19431:
	.size	_ZN2v88internal41Builtin_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internal41Builtin_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Builtin_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Builtin_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Builtin_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Builtin_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE:
.LFB19434:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4472
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL38Builtin_Impl_SegmenterPrototypeSegmentENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4472:
	.cfi_restore 6
	jmp	_ZN2v88internalL44Builtin_Impl_Stats_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19434:
	.size	_ZN2v88internal33Builtin_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE, .-_ZN2v88internal33Builtin_SegmenterPrototypeSegmentEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Builtin_V8BreakIteratorConstructorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Builtin_V8BreakIteratorConstructorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Builtin_V8BreakIteratorConstructorEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Builtin_V8BreakIteratorConstructorEiPmPNS0_7IsolateE:
.LFB19437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4487
	leal	-8(,%rdi,8), %eax
	movq	%rsi, %rcx
	movq	%rsi, %r8
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movslq	%eax, %rdx
	subl	$8, %eax
	subq	%rdx, %rcx
	cltq
	subq	%rax, %r8
	movq	%rcx, %rdx
	movq	88(%r12), %rax
	cmpq	%rax, (%rcx)
	cmove	%r8, %rdx
	cmpl	$5, %edi
	jle	.L4488
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L4489
	leaq	-16(%rsi), %r14
.L4479:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSFunction13GetDerivedMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L4486
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal17JSV8BreakIterator3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_@PLT
	testq	%rax, %rax
	je	.L4486
	movq	(%rax), %r14
.L4481:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4473
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4473:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4488:
	.cfi_restore_state
	leaq	88(%r12), %r14
	movq	%r14, %r15
	jmp	.L4479
	.p2align 4,,10
	.p2align 3
.L4486:
	movq	312(%r12), %r14
	jmp	.L4481
	.p2align 4,,10
	.p2align 3
.L4487:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL45Builtin_Impl_Stats_V8BreakIteratorConstructorEiPmPNS0_7IsolateE
.L4489:
	.cfi_restore_state
	leaq	88(%r12), %r14
	jmp	.L4479
	.cfi_endproc
.LFE19437:
	.size	_ZN2v88internal34Builtin_V8BreakIteratorConstructorEiPmPNS0_7IsolateE, .-_ZN2v88internal34Builtin_V8BreakIteratorConstructorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal47Builtin_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal47Builtin_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal47Builtin_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE, @function
_ZN2v88internal47Builtin_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE:
.LFB19440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4503
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4493
.L4496:
	leaq	.LC116(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$46, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4504
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4497:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4490
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rax
.L4490:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4505
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4493:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1088, 11(%rax)
	jne	.L4496
	movq	%rdx, %rdi
	call	_ZN2v88internal17JSV8BreakIterator15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rax
	jmp	.L4497
	.p2align 4,,10
	.p2align 3
.L4503:
	call	_ZN2v88internalL58Builtin_Impl_Stats_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	jmp	.L4490
	.p2align 4,,10
	.p2align 3
.L4504:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L4505:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19440:
	.size	_ZN2v88internal47Builtin_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE, .-_ZN2v88internal47Builtin_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Builtin_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Builtin_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Builtin_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Builtin_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE:
.LFB19443:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4510
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4510:
	.cfi_restore 6
	jmp	_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19443:
	.size	_ZN2v88internal41Builtin_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE, .-_ZN2v88internal41Builtin_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Builtin_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Builtin_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Builtin_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Builtin_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE:
.LFB19446:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4515
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL45Builtin_Impl_V8BreakIteratorInternalAdoptTextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4515:
	.cfi_restore 6
	jmp	_ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19446:
	.size	_ZN2v88internal40Builtin_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE, .-_ZN2v88internal40Builtin_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Builtin_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Builtin_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Builtin_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Builtin_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE:
.LFB19449:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4520
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL42Builtin_Impl_V8BreakIteratorPrototypeFirstENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4520:
	.cfi_restore 6
	jmp	_ZN2v88internalL48Builtin_Impl_Stats_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19449:
	.size	_ZN2v88internal37Builtin_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE, .-_ZN2v88internal37Builtin_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE:
.LFB19452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4533
	movq	41112(%rdx), %rdi
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	12464(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L4523
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L4524:
	movq	47(%rsi), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4526
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4527:
	movq	%r12, %rdi
	call	_ZN2v88internal17JSV8BreakIterator5FirstEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L4521
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4521:
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4523:
	.cfi_restore_state
	movq	%rbx, %rax
	cmpq	%r13, %rbx
	je	.L4534
.L4525:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L4524
	.p2align 4,,10
	.p2align 3
.L4526:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L4535
.L4528:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L4527
	.p2align 4,,10
	.p2align 3
.L4533:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L4535:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4528
	.p2align 4,,10
	.p2align 3
.L4534:
	movq	%rdx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L4525
	.cfi_endproc
.LFE19452:
	.size	_ZN2v88internal36Builtin_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Builtin_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Builtin_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Builtin_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Builtin_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE:
.LFB19455:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4540
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL41Builtin_Impl_V8BreakIteratorPrototypeNextENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4540:
	.cfi_restore 6
	jmp	_ZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19455:
	.size	_ZN2v88internal36Builtin_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE, .-_ZN2v88internal36Builtin_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Builtin_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Builtin_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Builtin_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Builtin_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE:
.LFB19458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4553
	movq	41112(%rdx), %rdi
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	12464(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L4543
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L4544:
	movq	47(%rsi), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4546
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4547:
	movq	%r12, %rdi
	call	_ZN2v88internal17JSV8BreakIterator4NextEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L4541
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4541:
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4543:
	.cfi_restore_state
	movq	%rbx, %rax
	cmpq	%r13, %rbx
	je	.L4554
.L4545:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L4544
	.p2align 4,,10
	.p2align 3
.L4546:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L4555
.L4548:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L4547
	.p2align 4,,10
	.p2align 3
.L4553:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL46Builtin_Impl_Stats_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L4555:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4548
	.p2align 4,,10
	.p2align 3
.L4554:
	movq	%rdx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L4545
	.cfi_endproc
.LFE19458:
	.size	_ZN2v88internal35Builtin_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE, .-_ZN2v88internal35Builtin_V8BreakIteratorInternalNextEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal39Builtin_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Builtin_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE
	.type	_ZN2v88internal39Builtin_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE, @function
_ZN2v88internal39Builtin_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE:
.LFB19461:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4560
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL44Builtin_Impl_V8BreakIteratorPrototypeCurrentENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4560:
	.cfi_restore 6
	jmp	_ZN2v88internalL50Builtin_Impl_Stats_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19461:
	.size	_ZN2v88internal39Builtin_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE, .-_ZN2v88internal39Builtin_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Builtin_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Builtin_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Builtin_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Builtin_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE:
.LFB19464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4573
	movq	41112(%rdx), %rdi
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	12464(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L4563
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L4564:
	movq	47(%rsi), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4566
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4567:
	movq	%r12, %rdi
	call	_ZN2v88internal17JSV8BreakIterator7CurrentEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L4561
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4561:
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4563:
	.cfi_restore_state
	movq	%rbx, %rax
	cmpq	%r13, %rbx
	je	.L4574
.L4565:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L4564
	.p2align 4,,10
	.p2align 3
.L4566:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L4575
.L4568:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L4567
	.p2align 4,,10
	.p2align 3
.L4573:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL49Builtin_Impl_Stats_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L4575:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4568
	.p2align 4,,10
	.p2align 3
.L4574:
	movq	%rdx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L4565
	.cfi_endproc
.LFE19464:
	.size	_ZN2v88internal38Builtin_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE, .-_ZN2v88internal38Builtin_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Builtin_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Builtin_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Builtin_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Builtin_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE:
.LFB19467:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4580
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL46Builtin_Impl_V8BreakIteratorPrototypeBreakTypeENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4580:
	.cfi_restore 6
	jmp	_ZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19467:
	.size	_ZN2v88internal41Builtin_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE, .-_ZN2v88internal41Builtin_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal40Builtin_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal40Builtin_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal40Builtin_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE, @function
_ZN2v88internal40Builtin_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE:
.LFB19470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4593
	movq	41112(%rdx), %rdi
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	12464(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L4583
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L4584:
	movq	47(%rsi), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4586
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4587:
	movq	%r12, %rdi
	call	_ZN2v88internal17JSV8BreakIterator9BreakTypeEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r14
	cmpq	41096(%r12), %r13
	je	.L4581
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4581:
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4583:
	.cfi_restore_state
	movq	%rbx, %rax
	cmpq	%r13, %rbx
	je	.L4594
.L4585:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L4584
	.p2align 4,,10
	.p2align 3
.L4586:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L4595
.L4588:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L4587
	.p2align 4,,10
	.p2align 3
.L4593:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L4595:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4588
	.p2align 4,,10
	.p2align 3
.L4594:
	movq	%rdx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L4585
	.cfi_endproc
.LFE19470:
	.size	_ZN2v88internal40Builtin_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE, .-_ZN2v88internal40Builtin_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19DateTimeFormatRangeINS0_6StringEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE,"axG",@progbits,_ZN2v88internal19DateTimeFormatRangeINS0_6StringEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE,comdat
	.p2align 4
	.weak	_ZN2v88internal19DateTimeFormatRangeINS0_6StringEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE
	.type	_ZN2v88internal19DateTimeFormatRangeINS0_6StringEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE, @function
_ZN2v88internal19DateTimeFormatRangeINS0_6StringEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE:
.LFB21679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L4597
.L4600:
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	%rbx, -80(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%rax, -72(%rbp)
.L4643:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4598
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4603:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L4644
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4597:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L4600
	movq	-1(%rax), %rax
	cmpw	$1090, 11(%rax)
	je	.L4636
	movq	%rcx, %rdi
	call	strlen@PLT
	movq	%rbx, -64(%rbp)
	leaq	-64(%rbp), %rsi
	movq	%rax, -56(%rbp)
	jmp	.L4643
	.p2align 4,,10
	.p2align 3
.L4598:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4636:
	cmpl	$5, %edi
	jle	.L4645
	leaq	-8(%rsi), %rsi
	cmpl	$6, %edi
	je	.L4646
	leaq	-16(%r13), %r14
.L4607:
	movq	(%rsi), %rax
	movq	88(%r12), %rcx
	cmpq	%rax, %rcx
	je	.L4641
	movq	(%r14), %rdx
	movq	%rdx, %rdi
	cmpq	%rdx, %rcx
	je	.L4641
	testb	$1, %al
	jne	.L4647
.L4610:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L4616:
	testb	$1, %dl
	jne	.L4648
.L4618:
	sarq	$32, %rdi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
.L4623:
	comisd	%xmm1, %xmm0
	ja	.L4641
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%r8
	testq	%rax, %rax
	je	.L4642
	movq	(%rax), %rax
	jmp	.L4603
	.p2align 4,,10
	.p2align 3
.L4645:
	leaq	88(%r12), %rsi
	movq	%rsi, %r14
	jmp	.L4607
	.p2align 4,,10
	.p2align 3
.L4641:
	xorl	%edx, %edx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$202, %esi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L4603
	.p2align 4,,10
	.p2align 3
.L4647:
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L4611
	xorl	%ecx, %ecx
.L4612:
	testb	%cl, %cl
	jne	.L4610
	movsd	7(%rax), %xmm0
	jmp	.L4616
	.p2align 4,,10
	.p2align 3
.L4648:
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L4619
	movq	%rdx, %rdi
	xorl	%eax, %eax
.L4620:
	testb	%al, %al
	jne	.L4618
	movsd	7(%rdi), %xmm1
	jmp	.L4623
	.p2align 4,,10
	.p2align 3
.L4642:
	movq	312(%r12), %rax
	jmp	.L4603
.L4611:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	-88(%rbp), %r8
	testq	%rax, %rax
	je	.L4642
	movq	(%rax), %rax
	movq	(%r14), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rdi
	notq	%rcx
	andl	$1, %ecx
	jmp	.L4612
	.p2align 4,,10
	.p2align 3
.L4619:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -96(%rbp)
	movsd	%xmm0, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-88(%rbp), %xmm0
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	je	.L4642
	movq	(%rax), %rdi
	movq	%rdi, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L4620
.L4644:
	call	__stack_chk_fail@PLT
.L4646:
	leaq	88(%r12), %r14
	jmp	.L4607
	.cfi_endproc
.LFE21679:
	.size	_ZN2v88internal19DateTimeFormatRangeINS0_6StringEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE, .-_ZN2v88internal19DateTimeFormatRangeINS0_6StringEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE
	.section	.rodata._ZN2v88internalL53Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC135:
	.string	"V8.Builtin_DateTimeFormatPrototypeFormatRange"
	.align 8
.LC136:
	.string	"Intl.DateTimeFormat.prototype.formatRange"
	.section	.text._ZN2v88internalL53Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL53Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL53Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE:
.LFB19272:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4680
.L4650:
	movq	_ZZN2v88internalL53Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic212(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4681
.L4652:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L4682
.L4654:
	movq	_ZN2v88internal16JSDateTimeFormat11FormatRangeEPNS0_7IsolateENS0_6HandleIS1_EEdd@GOTPCREL(%rip), %r8
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	addl	$1, 41104(%r12)
	movq	41088(%r12), %r15
	leaq	.LC136(%rip), %rcx
	movq	41096(%r12), %rbx
	call	_ZN2v88internal19DateTimeFormatRangeINS0_6StringEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	%rbx, 41096(%r12)
	je	.L4660
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4660:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4683
.L4649:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4684
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4682:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4685
.L4655:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4656
	movq	(%rdi), %rax
	call	*8(%rax)
.L4656:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4657
	movq	(%rdi), %rax
	call	*8(%rax)
.L4657:
	leaq	.LC135(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L4654
	.p2align 4,,10
	.p2align 3
.L4681:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4686
.L4653:
	movq	%rbx, _ZZN2v88internalL53Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic212(%rip)
	jmp	.L4652
	.p2align 4,,10
	.p2align 3
.L4680:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$871, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4650
	.p2align 4,,10
	.p2align 3
.L4683:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4649
	.p2align 4,,10
	.p2align 3
.L4686:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L4653
	.p2align 4,,10
	.p2align 3
.L4685:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC135(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L4655
.L4684:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19272:
	.size	_ZN2v88internalL53Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE, .-_ZN2v88internalL53Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal42Builtin_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal42Builtin_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal42Builtin_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE, @function
_ZN2v88internal42Builtin_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE:
.LFB19273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4739
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4690
.L4693:
	leaq	.LC136(%rip), %rax
	movq	$41, -88(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%rax, -96(%rbp)
.L4738:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4691
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4696:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4687
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rax
.L4687:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4740
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4690:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L4693
	movq	-1(%rax), %rax
	cmpw	$1090, 11(%rax)
	je	.L4731
	leaq	.LC136(%rip), %rax
	movq	$41, -72(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%rax, -80(%rbp)
	jmp	.L4738
	.p2align 4,,10
	.p2align 3
.L4739:
	call	_ZN2v88internalL53Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE
	jmp	.L4687
	.p2align 4,,10
	.p2align 3
.L4691:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4731:
	cmpl	$5, %edi
	jle	.L4741
	leaq	-8(%rsi), %rsi
	cmpl	$6, %edi
	je	.L4742
	leaq	-16(%r13), %r15
.L4700:
	movq	(%rsi), %rax
	movq	88(%r12), %rcx
	cmpq	%rcx, %rax
	je	.L4737
	movq	(%r15), %rdx
	movq	%rdx, %rdi
	cmpq	%rdx, %rcx
	je	.L4737
	testb	$1, %al
	jne	.L4743
.L4703:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L4709:
	testb	$1, %dl
	jne	.L4744
.L4711:
	sarq	$32, %rdi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
.L4716:
	comisd	%xmm1, %xmm0
	ja	.L4737
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal16JSDateTimeFormat11FormatRangeEPNS0_7IsolateENS0_6HandleIS1_EEdd@PLT
	testq	%rax, %rax
	je	.L4735
	movq	(%rax), %rax
	jmp	.L4696
	.p2align 4,,10
	.p2align 3
.L4741:
	leaq	88(%r12), %rsi
	movq	%rsi, %r15
	jmp	.L4700
	.p2align 4,,10
	.p2align 3
.L4737:
	xorl	%edx, %edx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$202, %esi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L4696
	.p2align 4,,10
	.p2align 3
.L4743:
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L4704
	xorl	%ecx, %ecx
.L4705:
	testb	%cl, %cl
	jne	.L4703
	movsd	7(%rax), %xmm0
	jmp	.L4709
	.p2align 4,,10
	.p2align 3
.L4744:
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L4712
	movq	%rdx, %rdi
	xorl	%eax, %eax
.L4713:
	testb	%al, %al
	jne	.L4711
	movsd	7(%rdi), %xmm1
	jmp	.L4716
	.p2align 4,,10
	.p2align 3
.L4735:
	movq	312(%r12), %rax
	jmp	.L4696
.L4704:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	je	.L4735
	movq	(%rax), %rax
	movq	(%r15), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rdi
	notq	%rcx
	andl	$1, %ecx
	jmp	.L4705
	.p2align 4,,10
	.p2align 3
.L4712:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movsd	%xmm0, -104(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-104(%rbp), %xmm0
	testq	%rax, %rax
	je	.L4735
	movq	(%rax), %rdi
	movq	%rdi, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L4713
.L4740:
	call	__stack_chk_fail@PLT
.L4742:
	leaq	88(%r12), %r15
	jmp	.L4700
	.cfi_endproc
.LFE19273:
	.size	_ZN2v88internal42Builtin_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE, .-_ZN2v88internal42Builtin_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19DateTimeFormatRangeINS0_7JSArrayEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE,"axG",@progbits,_ZN2v88internal19DateTimeFormatRangeINS0_7JSArrayEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE,comdat
	.p2align 4
	.weak	_ZN2v88internal19DateTimeFormatRangeINS0_7JSArrayEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE
	.type	_ZN2v88internal19DateTimeFormatRangeINS0_7JSArrayEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE, @function
_ZN2v88internal19DateTimeFormatRangeINS0_7JSArrayEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE:
.LFB21680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L4746
.L4749:
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	%rbx, -80(%rbp)
	leaq	-80(%rbp), %rsi
	movq	%rax, -72(%rbp)
.L4792:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4747
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4752:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L4793
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4746:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L4749
	movq	-1(%rax), %rax
	cmpw	$1090, 11(%rax)
	je	.L4785
	movq	%rcx, %rdi
	call	strlen@PLT
	movq	%rbx, -64(%rbp)
	leaq	-64(%rbp), %rsi
	movq	%rax, -56(%rbp)
	jmp	.L4792
	.p2align 4,,10
	.p2align 3
.L4747:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4785:
	cmpl	$5, %edi
	jle	.L4794
	leaq	-8(%rsi), %rsi
	cmpl	$6, %edi
	je	.L4795
	leaq	-16(%r13), %r14
.L4756:
	movq	(%rsi), %rax
	movq	88(%r12), %rcx
	cmpq	%rax, %rcx
	je	.L4790
	movq	(%r14), %rdx
	movq	%rdx, %rdi
	cmpq	%rdx, %rcx
	je	.L4790
	testb	$1, %al
	jne	.L4796
.L4759:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L4765:
	testb	$1, %dl
	jne	.L4797
.L4767:
	sarq	$32, %rdi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
.L4772:
	comisd	%xmm1, %xmm0
	ja	.L4790
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%r8
	testq	%rax, %rax
	je	.L4791
	movq	(%rax), %rax
	jmp	.L4752
	.p2align 4,,10
	.p2align 3
.L4794:
	leaq	88(%r12), %rsi
	movq	%rsi, %r14
	jmp	.L4756
	.p2align 4,,10
	.p2align 3
.L4790:
	xorl	%edx, %edx
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$202, %esi
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L4752
	.p2align 4,,10
	.p2align 3
.L4796:
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L4760
	xorl	%ecx, %ecx
.L4761:
	testb	%cl, %cl
	jne	.L4759
	movsd	7(%rax), %xmm0
	jmp	.L4765
	.p2align 4,,10
	.p2align 3
.L4797:
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	jne	.L4768
	movq	%rdx, %rdi
	xorl	%eax, %eax
.L4769:
	testb	%al, %al
	jne	.L4767
	movsd	7(%rdi), %xmm1
	jmp	.L4772
	.p2align 4,,10
	.p2align 3
.L4791:
	movq	312(%r12), %rax
	jmp	.L4752
.L4760:
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movq	-88(%rbp), %r8
	testq	%rax, %rax
	je	.L4791
	movq	(%rax), %rax
	movq	(%r14), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rdi
	notq	%rcx
	andl	$1, %ecx
	jmp	.L4761
	.p2align 4,,10
	.p2align 3
.L4768:
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -96(%rbp)
	movsd	%xmm0, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movsd	-88(%rbp), %xmm0
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	je	.L4791
	movq	(%rax), %rdi
	movq	%rdi, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L4769
.L4793:
	call	__stack_chk_fail@PLT
.L4795:
	leaq	88(%r12), %r14
	jmp	.L4756
	.cfi_endproc
.LFE21680:
	.size	_ZN2v88internal19DateTimeFormatRangeINS0_7JSArrayEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE, .-_ZN2v88internal19DateTimeFormatRangeINS0_7JSArrayEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE
	.section	.rodata._ZN2v88internalL60Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC137:
	.string	"V8.Builtin_DateTimeFormatPrototypeFormatRangeToParts"
	.align 8
.LC138:
	.string	"Intl.DateTimeFormat.prototype.formatRangeToParts"
	.section	.text._ZN2v88internalL60Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL60Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL60Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE:
.LFB19275:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4829
.L4799:
	movq	_ZZN2v88internalL60Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic219(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4830
.L4801:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L4831
.L4803:
	movq	_ZN2v88internal16JSDateTimeFormat18FormatRangeToPartsEPNS0_7IsolateENS0_6HandleIS1_EEdd@GOTPCREL(%rip), %r8
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	addl	$1, 41104(%r12)
	movq	41088(%r12), %r15
	leaq	.LC138(%rip), %rcx
	movq	41096(%r12), %rbx
	call	_ZN2v88internal19DateTimeFormatRangeINS0_7JSArrayEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	%rbx, 41096(%r12)
	je	.L4809
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4809:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4832
.L4798:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4833
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4831:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4834
.L4804:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4805
	movq	(%rdi), %rax
	call	*8(%rax)
.L4805:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4806
	movq	(%rdi), %rax
	call	*8(%rax)
.L4806:
	leaq	.LC137(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L4803
	.p2align 4,,10
	.p2align 3
.L4830:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4835
.L4802:
	movq	%rbx, _ZZN2v88internalL60Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic219(%rip)
	jmp	.L4801
	.p2align 4,,10
	.p2align 3
.L4829:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$872, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4799
	.p2align 4,,10
	.p2align 3
.L4832:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4798
	.p2align 4,,10
	.p2align 3
.L4835:
	leaq	.LC37(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L4802
	.p2align 4,,10
	.p2align 3
.L4834:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC137(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L4804
.L4833:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19275:
	.size	_ZN2v88internalL60Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE, .-_ZN2v88internalL60Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal49Builtin_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal49Builtin_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal49Builtin_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE, @function
_ZN2v88internal49Builtin_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE:
.LFB19276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4840
	addl	$1, 41104(%rdx)
	movq	_ZN2v88internal16JSDateTimeFormat18FormatRangeToPartsEPNS0_7IsolateENS0_6HandleIS1_EEdd@GOTPCREL(%rip), %r8
	movslq	%edi, %rdi
	leaq	.LC138(%rip), %rcx
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal19DateTimeFormatRangeINS0_7JSArrayEEENS0_6ObjectENS0_16BuiltinArgumentsEPNS0_7IsolateEPKcPFNS0_11MaybeHandleIT_EES6_NS0_6HandleINS0_16JSDateTimeFormatEEEddE
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	cmpq	41096(%r12), %rbx
	je	.L4838
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4838:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4840:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL60Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19276:
	.size	_ZN2v88internal49Builtin_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE, .-_ZN2v88internal49Builtin_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal38Builtin_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal38Builtin_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal38Builtin_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE:
.LFB24141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24141:
	.size	_GLOBAL__sub_I__ZN2v88internal38Builtin_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal38Builtin_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal38Builtin_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1224,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1224, @object
	.size	_ZZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1224, 8
_ZZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalBreakTypeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1224:
	.zero	8
	.section	.bss._ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1206,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1206, @object
	.size	_ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1206, 8
_ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeBreakTypeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1206:
	.zero	8
	.section	.bss._ZZN2v88internalL49Builtin_Impl_Stats_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1195,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL49Builtin_Impl_Stats_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1195, @object
	.size	_ZZN2v88internalL49Builtin_Impl_Stats_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1195, 8
_ZZN2v88internalL49Builtin_Impl_Stats_V8BreakIteratorInternalCurrentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1195:
	.zero	8
	.section	.bss._ZZN2v88internalL50Builtin_Impl_Stats_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1177,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL50Builtin_Impl_Stats_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1177, @object
	.size	_ZZN2v88internalL50Builtin_Impl_Stats_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1177, 8
_ZZN2v88internalL50Builtin_Impl_Stats_V8BreakIteratorPrototypeCurrentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1177:
	.zero	8
	.section	.bss._ZZN2v88internalL46Builtin_Impl_Stats_V8BreakIteratorInternalNextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1166,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL46Builtin_Impl_Stats_V8BreakIteratorInternalNextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1166, @object
	.size	_ZZN2v88internalL46Builtin_Impl_Stats_V8BreakIteratorInternalNextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1166, 8
_ZZN2v88internalL46Builtin_Impl_Stats_V8BreakIteratorInternalNextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1166:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1148,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1148, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1148, 8
_ZZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorPrototypeNextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1148:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateEE29trace_event_unique_atomic1136,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateEE29trace_event_unique_atomic1136, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateEE29trace_event_unique_atomic1136, 8
_ZZN2v88internalL47Builtin_Impl_Stats_V8BreakIteratorInternalFirstEiPmPNS0_7IsolateEE29trace_event_unique_atomic1136:
	.zero	8
	.section	.bss._ZZN2v88internalL48Builtin_Impl_Stats_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateEE29trace_event_unique_atomic1118,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL48Builtin_Impl_Stats_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateEE29trace_event_unique_atomic1118, @object
	.size	_ZZN2v88internalL48Builtin_Impl_Stats_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateEE29trace_event_unique_atomic1118, 8
_ZZN2v88internalL48Builtin_Impl_Stats_V8BreakIteratorPrototypeFirstEiPmPNS0_7IsolateEE29trace_event_unique_atomic1118:
	.zero	8
	.section	.bss._ZZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1100,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1100, @object
	.size	_ZZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1100, 8
_ZZN2v88internalL51Builtin_Impl_Stats_V8BreakIteratorInternalAdoptTextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1100:
	.zero	8
	.section	.bss._ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1082,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1082, @object
	.size	_ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1082, 8
_ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorPrototypeAdoptTextEiPmPNS0_7IsolateEE29trace_event_unique_atomic1082:
	.zero	8
	.section	.bss._ZZN2v88internalL58Builtin_Impl_Stats_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1075,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL58Builtin_Impl_Stats_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1075, @object
	.size	_ZZN2v88internalL58Builtin_Impl_Stats_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1075, 8
_ZZN2v88internalL58Builtin_Impl_Stats_V8BreakIteratorPrototypeResolvedOptionsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1075:
	.zero	8
	.section	.bss._ZZN2v88internalL45Builtin_Impl_Stats_V8BreakIteratorConstructorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1069,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Builtin_Impl_Stats_V8BreakIteratorConstructorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1069, @object
	.size	_ZZN2v88internalL45Builtin_Impl_Stats_V8BreakIteratorConstructorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1069, 8
_ZZN2v88internalL45Builtin_Impl_Stats_V8BreakIteratorConstructorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1069:
	.zero	8
	.section	.bss._ZZN2v88internalL44Builtin_Impl_Stats_SegmenterPrototypeSegmentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1051,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Builtin_Impl_Stats_SegmenterPrototypeSegmentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1051, @object
	.size	_ZZN2v88internalL44Builtin_Impl_Stats_SegmenterPrototypeSegmentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1051, 8
_ZZN2v88internalL44Builtin_Impl_Stats_SegmenterPrototypeSegmentEiPmPNS0_7IsolateEE29trace_event_unique_atomic1051:
	.zero	8
	.section	.bss._ZZN2v88internalL52Builtin_Impl_Stats_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1043,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL52Builtin_Impl_Stats_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1043, @object
	.size	_ZZN2v88internalL52Builtin_Impl_Stats_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1043, 8
_ZZN2v88internalL52Builtin_Impl_Stats_SegmenterPrototypeResolvedOptionsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1043:
	.zero	8
	.section	.bss._ZZN2v88internalL46Builtin_Impl_Stats_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateEE29trace_event_unique_atomic1032,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL46Builtin_Impl_Stats_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateEE29trace_event_unique_atomic1032, @object
	.size	_ZZN2v88internalL46Builtin_Impl_Stats_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateEE29trace_event_unique_atomic1032, 8
_ZZN2v88internalL46Builtin_Impl_Stats_SegmenterSupportedLocalesOfEiPmPNS0_7IsolateEE29trace_event_unique_atomic1032:
	.zero	8
	.section	.bss._ZZN2v88internalL39Builtin_Impl_Stats_SegmenterConstructorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1024,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Builtin_Impl_Stats_SegmenterConstructorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1024, @object
	.size	_ZZN2v88internalL39Builtin_Impl_Stats_SegmenterConstructorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1024, 8
_ZZN2v88internalL39Builtin_Impl_Stats_SegmenterConstructorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1024:
	.zero	8
	.section	.bss._ZZN2v88internalL48Builtin_Impl_Stats_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateEE29trace_event_unique_atomic1016,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL48Builtin_Impl_Stats_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateEE29trace_event_unique_atomic1016, @object
	.size	_ZZN2v88internalL48Builtin_Impl_Stats_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateEE29trace_event_unique_atomic1016, 8
_ZZN2v88internalL48Builtin_Impl_Stats_SegmentIteratorPrototypeIndexEiPmPNS0_7IsolateEE29trace_event_unique_atomic1016:
	.zero	8
	.section	.bss._ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1002,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1002, @object
	.size	_ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1002, 8
_ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypePrecedingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1002:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateEE28trace_event_unique_atomic992,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateEE28trace_event_unique_atomic992, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateEE28trace_event_unique_atomic992, 8
_ZZN2v88internalL47Builtin_Impl_Stats_SegmentIteratorPrototypeNextEiPmPNS0_7IsolateEE28trace_event_unique_atomic992:
	.zero	8
	.section	.bss._ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateEE28trace_event_unique_atomic978,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateEE28trace_event_unique_atomic978, @object
	.size	_ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateEE28trace_event_unique_atomic978, 8
_ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeFollowingEiPmPNS0_7IsolateEE28trace_event_unique_atomic978:
	.zero	8
	.section	.bss._ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateEE28trace_event_unique_atomic969,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateEE28trace_event_unique_atomic969, @object
	.size	_ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateEE28trace_event_unique_atomic969, 8
_ZZN2v88internalL52Builtin_Impl_Stats_SegmentIteratorPrototypeBreakTypeEiPmPNS0_7IsolateEE28trace_event_unique_atomic969:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_CollatorInternalCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic936,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_CollatorInternalCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic936, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_CollatorInternalCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic936, 8
_ZZN2v88internalL42Builtin_Impl_Stats_CollatorInternalCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic936:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_CollatorPrototypeCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic908,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_CollatorPrototypeCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic908, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_CollatorPrototypeCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic908, 8
_ZZN2v88internalL43Builtin_Impl_Stats_CollatorPrototypeCompareEiPmPNS0_7IsolateEE28trace_event_unique_atomic908:
	.zero	8
	.section	.bss._ZZN2v88internalL45Builtin_Impl_Stats_CollatorSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic897,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Builtin_Impl_Stats_CollatorSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic897, @object
	.size	_ZZN2v88internalL45Builtin_Impl_Stats_CollatorSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic897, 8
_ZZN2v88internalL45Builtin_Impl_Stats_CollatorSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic897:
	.zero	8
	.section	.bss._ZZN2v88internalL51Builtin_Impl_Stats_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic890,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL51Builtin_Impl_Stats_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic890, @object
	.size	_ZZN2v88internalL51Builtin_Impl_Stats_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic890, 8
_ZZN2v88internalL51Builtin_Impl_Stats_CollatorPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic890:
	.zero	8
	.section	.bss._ZZN2v88internalL38Builtin_Impl_Stats_CollatorConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic882,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Builtin_Impl_Stats_CollatorConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic882, @object
	.size	_ZZN2v88internalL38Builtin_Impl_Stats_CollatorConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic882, 8
_ZZN2v88internalL38Builtin_Impl_Stats_CollatorConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic882:
	.zero	8
	.section	.bss._ZZN2v88internalL48Builtin_Impl_Stats_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic871,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL48Builtin_Impl_Stats_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic871, @object
	.size	_ZZN2v88internalL48Builtin_Impl_Stats_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic871, 8
_ZZN2v88internalL48Builtin_Impl_Stats_PluralRulesSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic871:
	.zero	8
	.section	.bss._ZZN2v88internalL45Builtin_Impl_Stats_PluralRulesPrototypeSelectEiPmPNS0_7IsolateEE28trace_event_unique_atomic850,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Builtin_Impl_Stats_PluralRulesPrototypeSelectEiPmPNS0_7IsolateEE28trace_event_unique_atomic850, @object
	.size	_ZZN2v88internalL45Builtin_Impl_Stats_PluralRulesPrototypeSelectEiPmPNS0_7IsolateEE28trace_event_unique_atomic850, 8
_ZZN2v88internalL45Builtin_Impl_Stats_PluralRulesPrototypeSelectEiPmPNS0_7IsolateEE28trace_event_unique_atomic850:
	.zero	8
	.section	.bss._ZZN2v88internalL54Builtin_Impl_Stats_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic843,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL54Builtin_Impl_Stats_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic843, @object
	.size	_ZZN2v88internalL54Builtin_Impl_Stats_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic843, 8
_ZZN2v88internalL54Builtin_Impl_Stats_PluralRulesPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic843:
	.zero	8
	.section	.bss._ZZN2v88internalL41Builtin_Impl_Stats_PluralRulesConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic835,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Builtin_Impl_Stats_PluralRulesConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic835, @object
	.size	_ZZN2v88internalL41Builtin_Impl_Stats_PluralRulesConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic835, 8
_ZZN2v88internalL41Builtin_Impl_Stats_PluralRulesConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic835:
	.zero	8
	.section	.bss._ZZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateEE28trace_event_unique_atomic823,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateEE28trace_event_unique_atomic823, @object
	.size	_ZZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateEE28trace_event_unique_atomic823, 8
_ZZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleUpperCaseEiPmPNS0_7IsolateEE28trace_event_unique_atomic823:
	.zero	8
	.section	.bss._ZZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateEE28trace_event_unique_atomic811,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateEE28trace_event_unique_atomic811, @object
	.size	_ZZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateEE28trace_event_unique_atomic811, 8
_ZZN2v88internalL51Builtin_Impl_Stats_StringPrototypeToLocaleLowerCaseEiPmPNS0_7IsolateEE28trace_event_unique_atomic811:
	.zero	8
	.section	.bss._ZZN2v88internalL61Builtin_Impl_Stats_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic804,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL61Builtin_Impl_Stats_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic804, @object
	.size	_ZZN2v88internalL61Builtin_Impl_Stats_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic804, 8
_ZZN2v88internalL61Builtin_Impl_Stats_RelativeTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic804:
	.zero	8
	.section	.bss._ZZN2v88internalL48Builtin_Impl_Stats_RelativeTimeFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic796,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL48Builtin_Impl_Stats_RelativeTimeFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic796, @object
	.size	_ZZN2v88internalL48Builtin_Impl_Stats_RelativeTimeFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic796, 8
_ZZN2v88internalL48Builtin_Impl_Stats_RelativeTimeFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic796:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic789,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic789, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic789, 8
_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeToStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic789:
	.zero	8
	.section	.bss._ZZN2v88internalL49Builtin_Impl_Stats_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateEE28trace_event_unique_atomic782,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL49Builtin_Impl_Stats_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateEE28trace_event_unique_atomic782, @object
	.size	_ZZN2v88internalL49Builtin_Impl_Stats_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateEE28trace_event_unique_atomic782, 8
_ZZN2v88internalL49Builtin_Impl_Stats_LocalePrototypeNumberingSystemEiPmPNS0_7IsolateEE28trace_event_unique_atomic782:
	.zero	8
	.section	.bss._ZZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateEE28trace_event_unique_atomic775,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateEE28trace_event_unique_atomic775, @object
	.size	_ZZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateEE28trace_event_unique_atomic775, 8
_ZZN2v88internalL41Builtin_Impl_Stats_LocalePrototypeNumericEiPmPNS0_7IsolateEE28trace_event_unique_atomic775:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeHourCycleEiPmPNS0_7IsolateEE28trace_event_unique_atomic768,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeHourCycleEiPmPNS0_7IsolateEE28trace_event_unique_atomic768, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeHourCycleEiPmPNS0_7IsolateEE28trace_event_unique_atomic768, 8
_ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeHourCycleEiPmPNS0_7IsolateEE28trace_event_unique_atomic768:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCollationEiPmPNS0_7IsolateEE28trace_event_unique_atomic761,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCollationEiPmPNS0_7IsolateEE28trace_event_unique_atomic761, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCollationEiPmPNS0_7IsolateEE28trace_event_unique_atomic761, 8
_ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCollationEiPmPNS0_7IsolateEE28trace_event_unique_atomic761:
	.zero	8
	.section	.bss._ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCaseFirstEiPmPNS0_7IsolateEE28trace_event_unique_atomic754,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCaseFirstEiPmPNS0_7IsolateEE28trace_event_unique_atomic754, @object
	.size	_ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCaseFirstEiPmPNS0_7IsolateEE28trace_event_unique_atomic754, 8
_ZZN2v88internalL43Builtin_Impl_Stats_LocalePrototypeCaseFirstEiPmPNS0_7IsolateEE28trace_event_unique_atomic754:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeCalendarEiPmPNS0_7IsolateEE28trace_event_unique_atomic747,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeCalendarEiPmPNS0_7IsolateEE28trace_event_unique_atomic747, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeCalendarEiPmPNS0_7IsolateEE28trace_event_unique_atomic747, 8
_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeCalendarEiPmPNS0_7IsolateEE28trace_event_unique_atomic747:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeBaseNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic740,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeBaseNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic740, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeBaseNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic740, 8
_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeBaseNameEiPmPNS0_7IsolateEE28trace_event_unique_atomic740:
	.zero	8
	.section	.bss._ZZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateEE28trace_event_unique_atomic733,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateEE28trace_event_unique_atomic733, @object
	.size	_ZZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateEE28trace_event_unique_atomic733, 8
_ZZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeRegionEiPmPNS0_7IsolateEE28trace_event_unique_atomic733:
	.zero	8
	.section	.bss._ZZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateEE28trace_event_unique_atomic726,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateEE28trace_event_unique_atomic726, @object
	.size	_ZZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateEE28trace_event_unique_atomic726, 8
_ZZN2v88internalL40Builtin_Impl_Stats_LocalePrototypeScriptEiPmPNS0_7IsolateEE28trace_event_unique_atomic726:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeLanguageEiPmPNS0_7IsolateEE28trace_event_unique_atomic718,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeLanguageEiPmPNS0_7IsolateEE28trace_event_unique_atomic718, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeLanguageEiPmPNS0_7IsolateEE28trace_event_unique_atomic718, 8
_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeLanguageEiPmPNS0_7IsolateEE28trace_event_unique_atomic718:
	.zero	8
	.section	.bss._ZZN2v88internalL59Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic702,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL59Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic702, @object
	.size	_ZZN2v88internalL59Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic702, 8
_ZZN2v88internalL59Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic702:
	.zero	8
	.section	.bss._ZZN2v88internalL52Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic686,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL52Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic686, @object
	.size	_ZZN2v88internalL52Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic686, 8
_ZZN2v88internalL52Builtin_Impl_Stats_RelativeTimeFormatPrototypeFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic686:
	.zero	8
	.section	.bss._ZZN2v88internalL55Builtin_Impl_Stats_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic674,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL55Builtin_Impl_Stats_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic674, @object
	.size	_ZZN2v88internalL55Builtin_Impl_Stats_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic674, 8
_ZZN2v88internalL55Builtin_Impl_Stats_RelativeTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic674:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMinimizeEiPmPNS0_7IsolateEE28trace_event_unique_atomic662,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMinimizeEiPmPNS0_7IsolateEE28trace_event_unique_atomic662, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMinimizeEiPmPNS0_7IsolateEE28trace_event_unique_atomic662, 8
_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMinimizeEiPmPNS0_7IsolateEE28trace_event_unique_atomic662:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMaximizeEiPmPNS0_7IsolateEE28trace_event_unique_atomic650,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMaximizeEiPmPNS0_7IsolateEE28trace_event_unique_atomic650, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMaximizeEiPmPNS0_7IsolateEE28trace_event_unique_atomic650, 8
_ZZN2v88internalL42Builtin_Impl_Stats_LocalePrototypeMaximizeEiPmPNS0_7IsolateEE28trace_event_unique_atomic650:
	.zero	8
	.section	.bss._ZZN2v88internalL36Builtin_Impl_Stats_LocaleConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic628,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Builtin_Impl_Stats_LocaleConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic628, @object
	.size	_ZZN2v88internalL36Builtin_Impl_Stats_LocaleConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic628, 8
_ZZN2v88internalL36Builtin_Impl_Stats_LocaleConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic628:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic569,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic569, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic569, 8
_ZZN2v88internalL47Builtin_Impl_Stats_ListFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic569:
	.zero	8
	.section	.bss._ZZN2v88internalL53Builtin_Impl_Stats_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic562,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL53Builtin_Impl_Stats_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic562, @object
	.size	_ZZN2v88internalL53Builtin_Impl_Stats_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic562, 8
_ZZN2v88internalL53Builtin_Impl_Stats_ListFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic562:
	.zero	8
	.section	.bss._ZZN2v88internalL40Builtin_Impl_Stats_ListFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic554,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Builtin_Impl_Stats_ListFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic554, @object
	.size	_ZZN2v88internalL40Builtin_Impl_Stats_ListFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic554, 8
_ZZN2v88internalL40Builtin_Impl_Stats_ListFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic554:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_IntlGetCanonicalLocalesEiPmPNS0_7IsolateEE28trace_event_unique_atomic546,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_IntlGetCanonicalLocalesEiPmPNS0_7IsolateEE28trace_event_unique_atomic546, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_IntlGetCanonicalLocalesEiPmPNS0_7IsolateEE28trace_event_unique_atomic546, 8
_ZZN2v88internalL42Builtin_Impl_Stats_IntlGetCanonicalLocalesEiPmPNS0_7IsolateEE28trace_event_unique_atomic546:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_DateTimeFormatInternalFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic528,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_DateTimeFormatInternalFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic528, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_DateTimeFormatInternalFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic528, 8
_ZZN2v88internalL47Builtin_Impl_Stats_DateTimeFormatInternalFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic528:
	.zero	8
	.section	.bss._ZZN2v88internalL48Builtin_Impl_Stats_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic495,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL48Builtin_Impl_Stats_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic495, @object
	.size	_ZZN2v88internalL48Builtin_Impl_Stats_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic495, 8
_ZZN2v88internalL48Builtin_Impl_Stats_DateTimeFormatPrototypeFormatEiPmPNS0_7IsolateEE28trace_event_unique_atomic495:
	.zero	8
	.section	.bss._ZZN2v88internalL44Builtin_Impl_Stats_DateTimeFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic487,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Builtin_Impl_Stats_DateTimeFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic487, @object
	.size	_ZZN2v88internalL44Builtin_Impl_Stats_DateTimeFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic487, 8
_ZZN2v88internalL44Builtin_Impl_Stats_DateTimeFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic487:
	.zero	8
	.section	.bss._ZZN2v88internalL51Builtin_Impl_Stats_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic451,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL51Builtin_Impl_Stats_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic451, @object
	.size	_ZZN2v88internalL51Builtin_Impl_Stats_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic451, 8
_ZZN2v88internalL51Builtin_Impl_Stats_NumberFormatInternalFormatNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic451:
	.zero	8
	.section	.bss._ZZN2v88internalL52Builtin_Impl_Stats_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic418,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL52Builtin_Impl_Stats_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic418, @object
	.size	_ZZN2v88internalL52Builtin_Impl_Stats_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic418, 8
_ZZN2v88internalL52Builtin_Impl_Stats_NumberFormatPrototypeFormatNumberEiPmPNS0_7IsolateEE28trace_event_unique_atomic418:
	.zero	8
	.section	.bss._ZZN2v88internalL55Builtin_Impl_Stats_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic401,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL55Builtin_Impl_Stats_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic401, @object
	.size	_ZZN2v88internalL55Builtin_Impl_Stats_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic401, 8
_ZZN2v88internalL55Builtin_Impl_Stats_NumberFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic401:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_NumberFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic393,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_NumberFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic393, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_NumberFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic393, 8
_ZZN2v88internalL42Builtin_Impl_Stats_NumberFormatConstructorEiPmPNS0_7IsolateEE28trace_event_unique_atomic393:
	.zero	8
	.section	.bss._ZZN2v88internalL60Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic219,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL60Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic219, @object
	.size	_ZZN2v88internalL60Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic219, 8
_ZZN2v88internalL60Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic219:
	.zero	8
	.section	.bss._ZZN2v88internalL53Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic212,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL53Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic212, @object
	.size	_ZZN2v88internalL53Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic212, 8
_ZZN2v88internalL53Builtin_Impl_Stats_DateTimeFormatPrototypeFormatRangeEiPmPNS0_7IsolateEE28trace_event_unique_atomic212:
	.zero	8
	.section	.bss._ZZN2v88internalL55Builtin_Impl_Stats_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic127,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL55Builtin_Impl_Stats_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic127, @object
	.size	_ZZN2v88internalL55Builtin_Impl_Stats_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic127, 8
_ZZN2v88internalL55Builtin_Impl_Stats_DateTimeFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE28trace_event_unique_atomic127:
	.zero	8
	.section	.bss._ZZN2v88internalL51Builtin_Impl_Stats_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic116,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL51Builtin_Impl_Stats_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic116, @object
	.size	_ZZN2v88internalL51Builtin_Impl_Stats_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic116, 8
_ZZN2v88internalL51Builtin_Impl_Stats_DateTimeFormatSupportedLocalesOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic116:
	.zero	8
	.section	.bss._ZZN2v88internalL57Builtin_Impl_Stats_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic101,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL57Builtin_Impl_Stats_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic101, @object
	.size	_ZZN2v88internalL57Builtin_Impl_Stats_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic101, 8
_ZZN2v88internalL57Builtin_Impl_Stats_DateTimeFormatPrototypeResolvedOptionsEiPmPNS0_7IsolateEE28trace_event_unique_atomic101:
	.zero	8
	.section	.bss._ZZN2v88internalL53Builtin_Impl_Stats_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE27trace_event_unique_atomic79,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL53Builtin_Impl_Stats_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE27trace_event_unique_atomic79, @object
	.size	_ZZN2v88internalL53Builtin_Impl_Stats_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE27trace_event_unique_atomic79, 8
_ZZN2v88internalL53Builtin_Impl_Stats_NumberFormatPrototypeFormatToPartsEiPmPNS0_7IsolateEE27trace_event_unique_atomic79:
	.zero	8
	.section	.bss._ZZN2v88internalL49Builtin_Impl_Stats_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateEE27trace_event_unique_atomic68,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL49Builtin_Impl_Stats_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateEE27trace_event_unique_atomic68, @object
	.size	_ZZN2v88internalL49Builtin_Impl_Stats_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateEE27trace_event_unique_atomic68, 8
_ZZN2v88internalL49Builtin_Impl_Stats_NumberFormatSupportedLocalesOfEiPmPNS0_7IsolateEE27trace_event_unique_atomic68:
	.zero	8
	.section	.bss._ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateEE27trace_event_unique_atomic57,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateEE27trace_event_unique_atomic57, @object
	.size	_ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateEE27trace_event_unique_atomic57, 8
_ZZN2v88internalL52Builtin_Impl_Stats_V8BreakIteratorSupportedLocalesOfEiPmPNS0_7IsolateEE27trace_event_unique_atomic57:
	.zero	8
	.section	.bss._ZZN2v88internalL47Builtin_Impl_Stats_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic46,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Builtin_Impl_Stats_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic46, @object
	.size	_ZZN2v88internalL47Builtin_Impl_Stats_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic46, 8
_ZZN2v88internalL47Builtin_Impl_Stats_StringPrototypeNormalizeIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic46:
	.zero	8
	.section	.bss._ZZN2v88internalL49Builtin_Impl_Stats_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic39,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL49Builtin_Impl_Stats_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic39, @object
	.size	_ZZN2v88internalL49Builtin_Impl_Stats_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic39, 8
_ZZN2v88internalL49Builtin_Impl_Stats_StringPrototypeToUpperCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic39:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
