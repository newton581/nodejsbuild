	.file	"gc-idle-time-handler.cc"
	.text
	.section	.rodata._ZN2v88internal19GCIdleTimeHeapState5PrintEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"contexts_disposed=%d "
.LC1:
	.string	"contexts_disposal_rate=%f "
.LC2:
	.string	"size_of_objects=%zu "
	.section	.rodata._ZN2v88internal19GCIdleTimeHeapState5PrintEv.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"incremental_marking_stopped=%d "
	.section	.text._ZN2v88internal19GCIdleTimeHeapState5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19GCIdleTimeHeapState5PrintEv
	.type	_ZN2v88internal19GCIdleTimeHeapState5PrintEv, @function
_ZN2v88internal19GCIdleTimeHeapState5PrintEv:
.LFB9181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %esi
	leaq	.LC0(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movsd	8(%rbx), %xmm0
	movl	$1, %eax
	leaq	.LC1(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	16(%rbx), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movzbl	24(%rbx), %esi
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	leaq	.LC3(%rip), %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6PrintFEPKcz@PLT
	.cfi_endproc
.LFE9181:
	.size	_ZN2v88internal19GCIdleTimeHeapState5PrintEv, .-_ZN2v88internal19GCIdleTimeHeapState5PrintEv
	.section	.text._ZN2v88internal17GCIdleTimeHandler23EstimateMarkingStepSizeEdd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17GCIdleTimeHandler23EstimateMarkingStepSizeEdd
	.type	_ZN2v88internal17GCIdleTimeHandler23EstimateMarkingStepSizeEdd, @function
_ZN2v88internal17GCIdleTimeHandler23EstimateMarkingStepSizeEdd:
.LFB9182:
	.cfi_startproc
	endbr64
	ucomisd	.LC5(%rip), %xmm1
	jp	.L5
	jne	.L5
	movsd	.LC4(%rip), %xmm1
.L5:
	mulsd	%xmm0, %xmm1
	movl	$734003200, %eax
	comisd	.LC6(%rip), %xmm1
	jnb	.L4
	mulsd	.LC7(%rip), %xmm1
	movsd	.LC8(%rip), %xmm0
	comisd	%xmm0, %xmm1
	jb	.L12
	subsd	%xmm0, %xmm1
	cvttsd2siq	%xmm1, %rax
	btcq	$63, %rax
.L4:
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	cvttsd2siq	%xmm1, %rax
	ret
	.cfi_endproc
.LFE9182:
	.size	_ZN2v88internal17GCIdleTimeHandler23EstimateMarkingStepSizeEdd, .-_ZN2v88internal17GCIdleTimeHandler23EstimateMarkingStepSizeEdd
	.section	.text._ZN2v88internal17GCIdleTimeHandler39EstimateFinalIncrementalMarkCompactTimeEmd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17GCIdleTimeHandler39EstimateFinalIncrementalMarkCompactTimeEmd
	.type	_ZN2v88internal17GCIdleTimeHandler39EstimateFinalIncrementalMarkCompactTimeEmd, @function
_ZN2v88internal17GCIdleTimeHandler39EstimateFinalIncrementalMarkCompactTimeEmd:
.LFB9183:
	.cfi_startproc
	endbr64
	ucomisd	.LC5(%rip), %xmm0
	movapd	%xmm0, %xmm1
	jp	.L14
	jne	.L14
	movsd	.LC9(%rip), %xmm1
.L14:
	testq	%rdi, %rdi
	js	.L16
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdi, %xmm0
	divsd	%xmm1, %xmm0
	minsd	.LC10(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rdi, %rax
	andl	$1, %edi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm1, %xmm0
	minsd	.LC10(%rip), %xmm0
	ret
	.cfi_endproc
.LFE9183:
	.size	_ZN2v88internal17GCIdleTimeHandler39EstimateFinalIncrementalMarkCompactTimeEmd, .-_ZN2v88internal17GCIdleTimeHandler39EstimateFinalIncrementalMarkCompactTimeEmd
	.section	.text._ZN2v88internal17GCIdleTimeHandler34ShouldDoContextDisposalMarkCompactEidm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17GCIdleTimeHandler34ShouldDoContextDisposalMarkCompactEidm
	.type	_ZN2v88internal17GCIdleTimeHandler34ShouldDoContextDisposalMarkCompactEidm, @function
_ZN2v88internal17GCIdleTimeHandler34ShouldDoContextDisposalMarkCompactEidm:
.LFB9184:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	testl	%edi, %edi
	jle	.L20
	comisd	.LC5(%rip), %xmm0
	jbe	.L20
	movsd	.LC11(%rip), %xmm1
	cmpq	$104857600, %rsi
	setbe	%al
	comisd	%xmm0, %xmm1
	seta	%dl
	andl	%edx, %eax
.L20:
	ret
	.cfi_endproc
.LFE9184:
	.size	_ZN2v88internal17GCIdleTimeHandler34ShouldDoContextDisposalMarkCompactEidm, .-_ZN2v88internal17GCIdleTimeHandler34ShouldDoContextDisposalMarkCompactEidm
	.section	.text._ZN2v88internal17GCIdleTimeHandler35ShouldDoFinalIncrementalMarkCompactEdmd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17GCIdleTimeHandler35ShouldDoFinalIncrementalMarkCompactEdmd
	.type	_ZN2v88internal17GCIdleTimeHandler35ShouldDoFinalIncrementalMarkCompactEdmd, @function
_ZN2v88internal17GCIdleTimeHandler35ShouldDoFinalIncrementalMarkCompactEdmd:
.LFB9185:
	.cfi_startproc
	endbr64
	ucomisd	.LC5(%rip), %xmm1
	movapd	%xmm1, %xmm2
	jp	.L27
	jne	.L27
	movsd	.LC9(%rip), %xmm2
.L27:
	testq	%rdi, %rdi
	js	.L29
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdi, %xmm1
.L30:
	divsd	%xmm2, %xmm1
	minsd	.LC10(%rip), %xmm1
	comisd	%xmm1, %xmm0
	setnb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rdi, %rax
	andl	$1, %edi
	pxor	%xmm1, %xmm1
	shrq	%rax
	orq	%rdi, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L30
	.cfi_endproc
.LFE9185:
	.size	_ZN2v88internal17GCIdleTimeHandler35ShouldDoFinalIncrementalMarkCompactEdmd, .-_ZN2v88internal17GCIdleTimeHandler35ShouldDoFinalIncrementalMarkCompactEdmd
	.section	.text._ZN2v88internal17GCIdleTimeHandler34ShouldDoOverApproximateWeakClosureEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17GCIdleTimeHandler34ShouldDoOverApproximateWeakClosureEd
	.type	_ZN2v88internal17GCIdleTimeHandler34ShouldDoOverApproximateWeakClosureEd, @function
_ZN2v88internal17GCIdleTimeHandler34ShouldDoOverApproximateWeakClosureEd:
.LFB9186:
	.cfi_startproc
	endbr64
	comisd	.LC12(%rip), %xmm0
	setnb	%al
	ret
	.cfi_endproc
.LFE9186:
	.size	_ZN2v88internal17GCIdleTimeHandler34ShouldDoOverApproximateWeakClosureEd, .-_ZN2v88internal17GCIdleTimeHandler34ShouldDoOverApproximateWeakClosureEd
	.section	.text._ZN2v88internal17GCIdleTimeHandler7ComputeEdNS0_19GCIdleTimeHeapStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17GCIdleTimeHandler7ComputeEdNS0_19GCIdleTimeHeapStateE
	.type	_ZN2v88internal17GCIdleTimeHandler7ComputeEdNS0_19GCIdleTimeHeapStateE, @function
_ZN2v88internal17GCIdleTimeHandler7ComputeEdNS0_19GCIdleTimeHeapStateE:
.LFB9187:
	.cfi_startproc
	endbr64
	cvttsd2sil	%xmm0, %eax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%eax, %eax
	movzbl	40(%rbp), %edx
	movl	$0, %eax
	jle	.L52
	cmpb	$0, _ZN2v88internal24FLAG_incremental_markingE(%rip)
	je	.L34
	movl	%edx, %eax
	xorl	$1, %eax
.L34:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	testb	%dl, %dl
	je	.L34
	movl	16(%rbp), %ecx
	movq	32(%rbp), %rdx
	movsd	24(%rbp), %xmm0
	testl	%ecx, %ecx
	jle	.L34
	comisd	.LC5(%rip), %xmm0
	jbe	.L34
	cmpq	$104857600, %rdx
	ja	.L41
	movsd	.LC11(%rip), %xmm1
	movl	$2, %eax
	comisd	%xmm0, %xmm1
	ja	.L34
.L41:
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9187:
	.size	_ZN2v88internal17GCIdleTimeHandler7ComputeEdNS0_19GCIdleTimeHeapStateE, .-_ZN2v88internal17GCIdleTimeHandler7ComputeEdNS0_19GCIdleTimeHeapStateE
	.section	.text._ZN2v88internal17GCIdleTimeHandler7EnabledEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17GCIdleTimeHandler7EnabledEv
	.type	_ZN2v88internal17GCIdleTimeHandler7EnabledEv, @function
_ZN2v88internal17GCIdleTimeHandler7EnabledEv:
.LFB9188:
	.cfi_startproc
	endbr64
	movzbl	_ZN2v88internal24FLAG_incremental_markingE(%rip), %eax
	ret
	.cfi_endproc
.LFE9188:
	.size	_ZN2v88internal17GCIdleTimeHandler7EnabledEv, .-_ZN2v88internal17GCIdleTimeHandler7EnabledEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal17GCIdleTimeHandler22kConservativeTimeRatioE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal17GCIdleTimeHandler22kConservativeTimeRatioE, @function
_GLOBAL__sub_I__ZN2v88internal17GCIdleTimeHandler22kConservativeTimeRatioE:
.LFB10494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10494:
	.size	_GLOBAL__sub_I__ZN2v88internal17GCIdleTimeHandler22kConservativeTimeRatioE, .-_GLOBAL__sub_I__ZN2v88internal17GCIdleTimeHandler22kConservativeTimeRatioE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal17GCIdleTimeHandler22kConservativeTimeRatioE
	.globl	_ZN2v88internal17GCIdleTimeHandler43kMinTimeForOverApproximatingWeakClosureInMsE
	.section	.rodata._ZN2v88internal17GCIdleTimeHandler43kMinTimeForOverApproximatingWeakClosureInMsE,"a"
	.align 8
	.type	_ZN2v88internal17GCIdleTimeHandler43kMinTimeForOverApproximatingWeakClosureInMsE, @object
	.size	_ZN2v88internal17GCIdleTimeHandler43kMinTimeForOverApproximatingWeakClosureInMsE, 8
_ZN2v88internal17GCIdleTimeHandler43kMinTimeForOverApproximatingWeakClosureInMsE:
	.quad	1
	.globl	_ZN2v88internal17GCIdleTimeHandler24kHighContextDisposalRateE
	.section	.rodata._ZN2v88internal17GCIdleTimeHandler24kHighContextDisposalRateE,"a"
	.align 8
	.type	_ZN2v88internal17GCIdleTimeHandler24kHighContextDisposalRateE, @object
	.size	_ZN2v88internal17GCIdleTimeHandler24kHighContextDisposalRateE, 8
_ZN2v88internal17GCIdleTimeHandler24kHighContextDisposalRateE:
	.long	0
	.long	1079574528
	.globl	_ZN2v88internal17GCIdleTimeHandler39kMaxFinalIncrementalMarkCompactTimeInMsE
	.section	.rodata._ZN2v88internal17GCIdleTimeHandler39kMaxFinalIncrementalMarkCompactTimeInMsE,"a"
	.align 8
	.type	_ZN2v88internal17GCIdleTimeHandler39kMaxFinalIncrementalMarkCompactTimeInMsE, @object
	.size	_ZN2v88internal17GCIdleTimeHandler39kMaxFinalIncrementalMarkCompactTimeInMsE, 8
_ZN2v88internal17GCIdleTimeHandler39kMaxFinalIncrementalMarkCompactTimeInMsE:
	.quad	1000
	.globl	_ZN2v88internal17GCIdleTimeHandler22kConservativeTimeRatioE
	.section	.rodata._ZN2v88internal17GCIdleTimeHandler22kConservativeTimeRatioE,"a"
	.align 8
	.type	_ZN2v88internal17GCIdleTimeHandler22kConservativeTimeRatioE, @object
	.size	_ZN2v88internal17GCIdleTimeHandler22kConservativeTimeRatioE, 8
_ZN2v88internal17GCIdleTimeHandler22kConservativeTimeRatioE:
	.long	3435973837
	.long	1072483532
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	0
	.long	1090060288
	.align 8
.LC5:
	.long	0
	.long	0
	.align 8
.LC6:
	.long	0
	.long	1103486976
	.align 8
.LC7:
	.long	3435973837
	.long	1072483532
	.align 8
.LC8:
	.long	0
	.long	1138753536
	.align 8
.LC9:
	.long	0
	.long	1094713344
	.align 8
.LC10:
	.long	0
	.long	1083129856
	.align 8
.LC11:
	.long	0
	.long	1079574528
	.align 8
.LC12:
	.long	0
	.long	1072693248
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
