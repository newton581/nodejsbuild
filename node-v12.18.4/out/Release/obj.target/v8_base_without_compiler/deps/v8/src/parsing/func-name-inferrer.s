	.file	"func-name-inferrer.cc"
	.text
	.section	.text._ZN2v88internal16FuncNameInferrer17MakeNameFromStackEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal16FuncNameInferrer17MakeNameFromStackEv.part.0, @function
_ZN2v88internal16FuncNameInferrer17MakeNameFromStackEv.part.0:
.LFB23522:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	call	_ZN2v88internal15AstValueFactory13NewConsStringEv@PLT
	movq	8(%r15), %rbx
	movq	%rax, %r13
	movq	16(%r15), %rax
	cmpq	%rax, %rbx
	jne	.L2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%r15), %rdx
	movq	1096(%rdx), %r14
	movq	56(%rdx), %rdx
	movq	200(%rdx), %r12
	movl	16(%r12), %edi
	testl	%edi, %edi
	je	.L7
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L22
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
.L9:
	movdqu	8(%r13), %xmm1
	movq	%r12, %xmm0
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm1, (%rax)
	movups	%xmm0, 8(%r13)
	movq	-8(%rbx), %r12
	andq	$-4, %r12
	movl	16(%r12), %esi
	testl	%esi, %esi
	je	.L20
.L10:
	movq	16(%r14), %rax
	movq	24(%r14), %rdx
	subq	%rax, %rdx
	cmpq	$15, %rdx
	jbe	.L23
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
.L13:
	movdqu	8(%r13), %xmm3
	movups	%xmm3, (%rax)
	movq	%rax, 16(%r13)
.L14:
	movq	%r12, 8(%r13)
.L20:
	movq	16(%r15), %rax
.L11:
	cmpq	%rax, %rbx
	je	.L1
.L2:
	addq	$8, %rbx
	cmpq	%rax, %rbx
	je	.L4
	movzbl	-8(%rbx), %edx
	andl	$3, %edx
	cmpb	$2, %dl
	je	.L24
.L4:
	cmpq	$0, 8(%r13)
	jne	.L25
	movq	-8(%rbx), %r12
	andq	$-4, %r12
	movl	16(%r12), %edx
	testl	%edx, %edx
	jne	.L14
	cmpq	%rax, %rbx
	jne	.L2
.L1:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movzbl	(%rbx), %edx
	andl	$3, %edx
	cmpb	$2, %dl
	jne	.L4
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L7:
	movq	-8(%rbx), %r12
	andq	$-4, %r12
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jne	.L10
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$16, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L9
	.cfi_endproc
.LFE23522:
	.size	_ZN2v88internal16FuncNameInferrer17MakeNameFromStackEv.part.0, .-_ZN2v88internal16FuncNameInferrer17MakeNameFromStackEv.part.0
	.section	.text._ZN2v88internal16FuncNameInferrerC2EPNS0_15AstValueFactoryE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FuncNameInferrerC2EPNS0_15AstValueFactoryE
	.type	_ZN2v88internal16FuncNameInferrerC2EPNS0_15AstValueFactoryE, @function
_ZN2v88internal16FuncNameInferrerC2EPNS0_15AstValueFactoryE:
.LFB19101:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	ret
	.cfi_endproc
.LFE19101:
	.size	_ZN2v88internal16FuncNameInferrerC2EPNS0_15AstValueFactoryE, .-_ZN2v88internal16FuncNameInferrerC2EPNS0_15AstValueFactoryE
	.globl	_ZN2v88internal16FuncNameInferrerC1EPNS0_15AstValueFactoryE
	.set	_ZN2v88internal16FuncNameInferrerC1EPNS0_15AstValueFactoryE,_ZN2v88internal16FuncNameInferrerC2EPNS0_15AstValueFactoryE
	.section	.rodata._ZN2v88internal16FuncNameInferrer15PushLiteralNameEPKNS0_12AstRawStringE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal16FuncNameInferrer15PushLiteralNameEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FuncNameInferrer15PushLiteralNameEPKNS0_12AstRawStringE
	.type	_ZN2v88internal16FuncNameInferrer15PushLiteralNameEPKNS0_12AstRawStringE, @function
_ZN2v88internal16FuncNameInferrer15PushLiteralNameEPKNS0_12AstRawStringE:
.LFB19104:
	.cfi_startproc
	endbr64
	cmpq	$0, 56(%rdi)
	je	.L55
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	cmpq	416(%rax), %rsi
	je	.L27
	movq	16(%rdi), %r12
	orq	$1, %rsi
	cmpq	24(%rdi), %r12
	je	.L29
	movq	%rsi, (%r12)
	addq	$8, 16(%rdi)
.L27:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	8(%rdi), %r14
	movq	%r12, %rdx
	movabsq	$1152921504606846975, %rcx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L58
	testq	%rax, %rax
	je	.L40
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L59
.L32:
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L33:
	movq	%rsi, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L34
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L43
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L43
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L36:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L36
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L38
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L38:
	leaq	16(%r13,%rsi), %rax
.L34:
	testq	%r14, %r14
	je	.L39
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L39:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L59:
	testq	%rdi, %rdi
	jne	.L60
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$8, %r15d
	jmp	.L32
.L43:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L35
	jmp	.L38
.L58:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L60:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L32
	.cfi_endproc
.LFE19104:
	.size	_ZN2v88internal16FuncNameInferrer15PushLiteralNameEPKNS0_12AstRawStringE, .-_ZN2v88internal16FuncNameInferrer15PushLiteralNameEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal16FuncNameInferrer16PushVariableNameEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FuncNameInferrer16PushVariableNameEPKNS0_12AstRawStringE
	.type	_ZN2v88internal16FuncNameInferrer16PushVariableNameEPKNS0_12AstRawStringE, @function
_ZN2v88internal16FuncNameInferrer16PushVariableNameEPKNS0_12AstRawStringE:
.LFB19105:
	.cfi_startproc
	endbr64
	cmpq	$0, 56(%rdi)
	je	.L89
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	cmpq	248(%rax), %rsi
	je	.L61
	movq	16(%rdi), %r12
	orq	$2, %rsi
	cmpq	24(%rdi), %r12
	je	.L63
	movq	%rsi, (%r12)
	addq	$8, 16(%rdi)
.L61:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	8(%rdi), %r14
	movq	%r12, %rdx
	movabsq	$1152921504606846975, %rcx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L92
	testq	%rax, %rax
	je	.L74
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L93
.L66:
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L67:
	movq	%rsi, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L68
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L77
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L77
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L70:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L70
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L72
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L72:
	leaq	16(%r13,%rsi), %rax
.L68:
	testq	%r14, %r14
	je	.L73
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L73:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L93:
	testq	%rdi, %rdi
	jne	.L94
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$8, %r15d
	jmp	.L66
.L77:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L69:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L69
	jmp	.L72
.L92:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L94:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L66
	.cfi_endproc
.LFE19105:
	.size	_ZN2v88internal16FuncNameInferrer16PushVariableNameEPKNS0_12AstRawStringE, .-_ZN2v88internal16FuncNameInferrer16PushVariableNameEPKNS0_12AstRawStringE
	.section	.rodata._ZN2v88internal16FuncNameInferrer25RemoveAsyncKeywordFromEndEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"names_stack_.size() > 0"
.LC2:
	.string	"Check failed: %s."
.LC3:
	.string	"async"
	.section	.rodata._ZN2v88internal16FuncNameInferrer25RemoveAsyncKeywordFromEndEv.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"names_stack_.back().name()->IsOneByteEqualTo(\"async\")"
	.section	.text._ZN2v88internal16FuncNameInferrer25RemoveAsyncKeywordFromEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FuncNameInferrer25RemoveAsyncKeywordFromEndEv
	.type	_ZN2v88internal16FuncNameInferrer25RemoveAsyncKeywordFromEndEv, @function
_ZN2v88internal16FuncNameInferrer25RemoveAsyncKeywordFromEndEv:
.LFB19106:
	.cfi_startproc
	endbr64
	cmpq	$0, 56(%rdi)
	je	.L100
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	cmpq	%rax, 8(%rdi)
	je	.L103
	movq	-8(%rax), %rdi
	leaq	.LC3(%rip), %rsi
	andq	$-4, %rdi
	call	_ZNK2v88internal12AstRawString16IsOneByteEqualToEPKc@PLT
	testb	%al, %al
	je	.L104
	subq	$8, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19106:
	.size	_ZN2v88internal16FuncNameInferrer25RemoveAsyncKeywordFromEndEv, .-_ZN2v88internal16FuncNameInferrer25RemoveAsyncKeywordFromEndEv
	.section	.text._ZN2v88internal16FuncNameInferrer17MakeNameFromStackEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FuncNameInferrer17MakeNameFromStackEv
	.type	_ZN2v88internal16FuncNameInferrer17MakeNameFromStackEv, @function
_ZN2v88internal16FuncNameInferrer17MakeNameFromStackEv:
.LFB19107:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	cmpq	%rax, 8(%rdi)
	je	.L109
	jmp	_ZN2v88internal16FuncNameInferrer17MakeNameFromStackEv.part.0
	.p2align 4,,10
	.p2align 3
.L109:
	movq	(%rdi), %rax
	movq	64(%rax), %rax
	ret
	.cfi_endproc
.LFE19107:
	.size	_ZN2v88internal16FuncNameInferrer17MakeNameFromStackEv, .-_ZN2v88internal16FuncNameInferrer17MakeNameFromStackEv
	.section	.text._ZNSt6vectorIN2v88internal16FuncNameInferrer4NameESaIS3_EE12emplace_backIJS3_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal16FuncNameInferrer4NameESaIS3_EE12emplace_backIJS3_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal16FuncNameInferrer4NameESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal16FuncNameInferrer4NameESaIS3_EE12emplace_backIJS3_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal16FuncNameInferrer4NameESaIS3_EE12emplace_backIJS3_EEEvDpOT_:
.LFB22314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L111
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L137
	testq	%rax, %rax
	je	.L122
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L138
.L114:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L115:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L116
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L125
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L125
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L118:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L118
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L120
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L120:
	leaq	16(%r13,%rsi), %rax
.L116:
	testq	%r14, %r14
	je	.L121
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L121:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L139
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$8, %r15d
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L125:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L117:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L117
	jmp	.L120
.L137:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L139:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L114
	.cfi_endproc
.LFE22314:
	.size	_ZNSt6vectorIN2v88internal16FuncNameInferrer4NameESaIS3_EE12emplace_backIJS3_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal16FuncNameInferrer4NameESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	.section	.text._ZN2v88internal16FuncNameInferrer17PushEnclosingNameEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FuncNameInferrer17PushEnclosingNameEPKNS0_12AstRawStringE
	.type	_ZN2v88internal16FuncNameInferrer17PushEnclosingNameEPKNS0_12AstRawStringE, @function
_ZN2v88internal16FuncNameInferrer17PushEnclosingNameEPKNS0_12AstRawStringE:
.LFB19103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	16(%rsi), %eax
	testl	%eax, %eax
	jne	.L149
.L140:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L150
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	%rsi, %rbx
	call	_ZNK2v88internal12AstRawString14FirstCharacterEv@PLT
	movzwl	%ax, %edi
	call	_ZN7unibrow9Uppercase2IsEj@PLT
	testb	%al, %al
	je	.L140
	leaq	-32(%rbp), %rsi
	leaq	8(%r12), %rdi
	movq	%rbx, -32(%rbp)
	call	_ZNSt6vectorIN2v88internal16FuncNameInferrer4NameESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	jmp	.L140
.L150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19103:
	.size	_ZN2v88internal16FuncNameInferrer17PushEnclosingNameEPKNS0_12AstRawStringE, .-_ZN2v88internal16FuncNameInferrer17PushEnclosingNameEPKNS0_12AstRawStringE
	.section	.rodata._ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC5:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_default_appendEm
	.type	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_default_appendEm, @function
_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_default_appendEm:
.LFB22329:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L170
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%r12), %rax
	movq	%rcx, %rsi
	subq	(%rdi), %rsi
	subq	%rcx, %rax
	movq	%rdx, %rdi
	movq	%rsi, %r13
	sarq	$3, %rax
	sarq	$3, %r13
	subq	%r13, %rdi
	cmpq	%rbx, %rax
	jb	.L153
	salq	$3, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L173
	cmpq	%rbx, %r13
	movq	%rbx, %r14
	movq	%rsi, -56(%rbp)
	cmovnb	%r13, %r14
	addq	%r13, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$3, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	leaq	0(,%rbx,8), %rdx
	movq	%rax, %r15
	leaq	(%rax,%rsi), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L174
	testq	%r8, %r8
	jne	.L157
.L158:
	addq	%r13, %rbx
	addq	%r15, %r14
	movq	%r15, (%r12)
	leaq	(%r15,%rbx,8), %rax
	movq	%r14, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L157:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L158
.L173:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22329:
	.size	_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_default_appendEm, .-_ZNSt6vectorIPN2v88internal15FunctionLiteralESaIS3_EE17_M_default_appendEm
	.section	.text._ZN2v88internal16FuncNameInferrer19InferFunctionsNamesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16FuncNameInferrer19InferFunctionsNamesEv
	.type	_ZN2v88internal16FuncNameInferrer19InferFunctionsNamesEv, @function
_ZN2v88internal16FuncNameInferrer19InferFunctionsNamesEv:
.LFB19108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %rax
	cmpq	%rax, 8(%rdi)
	je	.L182
	call	_ZN2v88internal16FuncNameInferrer17MakeNameFromStackEv.part.0
	movq	%rax, %r12
.L177:
	movq	32(%r13), %rbx
	movq	40(%r13), %r14
	cmpq	%r14, %rbx
	je	.L175
	.p2align 4,,10
	.p2align 3
.L179:
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	addq	$8, %rbx
	call	_ZN2v88internal15FunctionLiteral21set_raw_inferred_nameEPKNS0_13AstConsStringE@PLT
	cmpq	%rbx, %r14
	jne	.L179
	movq	32(%r13), %rax
	cmpq	%rax, 40(%r13)
	je	.L175
	movq	%rax, 40(%r13)
.L175:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	64(%rax), %r12
	jmp	.L177
	.cfi_endproc
.LFE19108:
	.size	_ZN2v88internal16FuncNameInferrer19InferFunctionsNamesEv, .-_ZN2v88internal16FuncNameInferrer19InferFunctionsNamesEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16FuncNameInferrerC2EPNS0_15AstValueFactoryE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16FuncNameInferrerC2EPNS0_15AstValueFactoryE, @function
_GLOBAL__sub_I__ZN2v88internal16FuncNameInferrerC2EPNS0_15AstValueFactoryE:
.LFB23502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23502:
	.size	_GLOBAL__sub_I__ZN2v88internal16FuncNameInferrerC2EPNS0_15AstValueFactoryE, .-_GLOBAL__sub_I__ZN2v88internal16FuncNameInferrerC2EPNS0_15AstValueFactoryE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16FuncNameInferrerC2EPNS0_15AstValueFactoryE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
