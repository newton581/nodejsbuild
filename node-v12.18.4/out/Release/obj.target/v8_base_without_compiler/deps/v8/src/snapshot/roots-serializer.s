	.file	"roots-serializer.cc"
	.text
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB6294:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE6294:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.rodata._ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Out of memory: HashMap::Initialize"
	.section	.text._ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE
	.type	_ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE, @function
_ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE:
.LFB18261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movzwl	%dx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal10SerializerC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal15RootsSerializerE(%rip), %rax
	pxor	%xmm0, %xmm0
	movw	%r13w, 360(%rbx)
	movq	%rax, (%rbx)
	movl	$192, %edi
	movups	%xmm0, 368(%rbx)
	movups	%xmm0, 384(%rbx)
	movups	%xmm0, 400(%rbx)
	movups	%xmm0, 416(%rbx)
	movups	%xmm0, 432(%rbx)
	call	malloc@PLT
	movq	%rax, 456(%rbx)
	testq	%rax, %rax
	je	.L12
	movl	$8, 464(%rbx)
	movl	$24, %edx
	movb	$0, 16(%rax)
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L5:
	movq	456(%rbx), %rcx
	addq	$1, %rax
	movb	$0, 16(%rcx,%rdx)
	movl	464(%rbx), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L5
	movb	$1, 488(%rbx)
	xorl	%eax, %eax
	movl	$1, %esi
	movl	$0, 468(%rbx)
	movl	$0, 480(%rbx)
	testq	%r12, %r12
	je	.L3
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rax, %rdx
	movq	%rax, %rcx
	movq	%rsi, %rdi
	addq	$1, %rax
	shrq	$6, %rdx
	andl	$63, %ecx
	salq	%cl, %rdi
	orq	%rdi, 368(%rbx,%rdx,8)
	cmpq	%r12, %rax
	jne	.L6
.L3:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L12:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18261:
	.size	_ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE, .-_ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE
	.globl	_ZN2v88internal15RootsSerializerC1EPNS0_7IsolateENS0_9RootIndexE
	.set	_ZN2v88internal15RootsSerializerC1EPNS0_7IsolateENS0_9RootIndexE,_ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE
	.section	.text._ZN2v88internal15RootsSerializer18CheckRehashabilityENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RootsSerializer18CheckRehashabilityENS0_10HeapObjectE
	.type	_ZN2v88internal15RootsSerializer18CheckRehashabilityENS0_10HeapObjectE, @function
_ZN2v88internal15RootsSerializer18CheckRehashabilityENS0_10HeapObjectE:
.LFB18266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpb	$0, 488(%rdi)
	movq	%rsi, -24(%rbp)
	jne	.L21
.L13:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	leaq	-24(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZNK2v88internal10HeapObject14NeedsRehashingEv@PLT
	testb	%al, %al
	je	.L13
	movq	%r12, %rdi
	call	_ZNK2v88internal10HeapObject13CanBeRehashedEv@PLT
	testb	%al, %al
	jne	.L13
	movb	$0, 488(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18266:
	.size	_ZN2v88internal15RootsSerializer18CheckRehashabilityENS0_10HeapObjectE, .-_ZN2v88internal15RootsSerializer18CheckRehashabilityENS0_10HeapObjectE
	.section	.rodata._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.type	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, @function
_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_:
.LFB20514:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L36
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L31
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L37
.L33:
	movq	%rcx, %r14
.L24:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L30:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L38
	testq	%rsi, %rsi
	jg	.L26
	testq	%r15, %r15
	jne	.L29
.L27:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L26
.L29:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L37:
	testq	%r14, %r14
	js	.L33
	jne	.L24
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L27
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$1, %r14d
	jmp	.L24
.L36:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20514:
	.size	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_, .-_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	.section	.text._ZN2v88internal15RootsSerializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RootsSerializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.type	_ZN2v88internal15RootsSerializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE, @function
_ZN2v88internal15RootsSerializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE:
.LFB18264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	88(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movb	$26, -9(%rbp)
	cmpq	96(%rdi), %rsi
	je	.L40
	movb	$26, (%rsi)
	addq	$1, 88(%rdi)
.L39:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	leaq	-9(%rbp), %rdx
	addq	$80, %rdi
	call	_ZNSt6vectorIhSaIhEE17_M_realloc_insertIJRKhEEEvN9__gnu_cxx17__normal_iteratorIPhS1_EEDpOT_
	jmp	.L39
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18264:
	.size	_ZN2v88internal15RootsSerializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE, .-_ZN2v88internal15RootsSerializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.section	.text._ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_,"axG",@progbits,_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_
	.type	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_, @function
_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_:
.LFB22175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	(%rdi), %r13
	movl	12(%rdi), %r15d
	addl	%eax, %eax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, %rbx
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L70
	movl	%ebx, 8(%r12)
	testl	%ebx, %ebx
	je	.L47
	movb	$0, 16(%rax)
	movl	$24, %edx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L48:
	movq	(%r12), %rcx
	addq	$1, %rax
	movb	$0, 16(%rcx,%rdx)
	movl	8(%r12), %ecx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	ja	.L48
.L47:
	movl	$0, 12(%r12)
	movq	%r13, %r14
	testl	%r15d, %r15d
	je	.L54
.L49:
	cmpb	$0, 16(%r14)
	jne	.L71
.L50:
	addq	$24, %r14
	cmpb	$0, 16(%r14)
	je	.L50
.L71:
	movl	8(%r12), %eax
	movl	12(%r14), %ebx
	movq	(%r12), %rdi
	movq	(%r14), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	jne	.L52
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L51
.L52:
	cmpq	%rsi, (%rdx)
	jne	.L72
.L51:
	movl	8(%r14), %eax
	movq	%rsi, (%rdx)
	movl	%ebx, 12(%rdx)
	movl	%eax, 8(%rdx)
	movb	$1, 16(%rdx)
	movl	12(%r12), %eax
	addl	$1, %eax
	movl	%eax, %edx
	movl	%eax, 12(%r12)
	shrl	$2, %edx
	addl	%edx, %eax
	cmpl	8(%r12), %eax
	jnb	.L53
.L56:
	addq	$24, %r14
	subl	$1, %r15d
	jne	.L49
.L54:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_
	movl	8(%r12), %eax
	movq	(%r12), %rsi
	leal	-1(%rax), %ecx
	movl	%ebx, %eax
	andl	%ecx, %eax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L56
	movq	(%r14), %rdi
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L73:
	addq	$1, %rax
	andq	%rcx, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rsi,%rdx,8), %rdx
	cmpb	$0, 16(%rdx)
	je	.L56
.L57:
	cmpq	%rdi, (%rdx)
	jne	.L73
	jmp	.L56
.L70:
	leaq	.LC0(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22175:
	.size	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_, .-_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_
	.section	.text._ZN2v88internal15RootsSerializer22SerializeInObjectCacheENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RootsSerializer22SerializeInObjectCacheENS0_10HeapObjectE
	.type	_ZN2v88internal15RootsSerializer22SerializeInObjectCacheENS0_10HeapObjectE, @function
_ZN2v88internal15RootsSerializer22SerializeInObjectCacheENS0_10HeapObjectE:
.LFB18263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movl	464(%rdi), %eax
	movq	456(%rdi), %r9
	leal	-1(%rax), %edi
	movl	%edi, %edx
	andl	%esi, %edx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%r9,%rax,8), %rax
	cmpb	$0, 16(%rax)
	je	.L75
	movq	%rax, %r8
	movq	%rdx, %rcx
	movl	%edi, %r10d
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L89:
	addq	$1, %rcx
	andq	%r10, %rcx
	leaq	(%rcx,%rcx,2), %r8
	leaq	(%r9,%r8,8), %r8
	cmpb	$0, 16(%r8)
	je	.L75
.L77:
	cmpq	%rsi, (%r8)
	jne	.L89
	movl	8(%r8), %r13d
	addq	$16, %rsp
	popq	%r12
	movl	%r13d, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movl	480(%r12), %r13d
	leal	1(%r13), %ecx
	movl	%ecx, 480(%r12)
	cmpb	$0, 16(%rax)
	jne	.L79
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L90:
	addq	$1, %rdx
	andq	%rdi, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%r9,%rax,8), %rax
	cmpb	$0, 16(%rax)
	je	.L80
.L79:
	cmpq	(%rax), %rsi
	jne	.L90
.L78:
	movl	%r13d, 8(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	%rsi, (%rax)
	movl	$0, 8(%rax)
	movl	%esi, 12(%rax)
	movb	$1, 16(%rax)
	movl	468(%r12), %ecx
	leal	1(%rcx), %edx
	movl	%edx, %ecx
	movl	%edx, 468(%r12)
	shrl	$2, %ecx
	addl	%ecx, %edx
	cmpl	464(%r12), %edx
	jb	.L78
	leaq	456(%r12), %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v84base19TemplateHashMapImplImjNS0_18KeyEqualityMatcherIlEENS0_23DefaultAllocationPolicyEE6ResizeES4_
	movl	464(%r12), %eax
	movq	-24(%rbp), %rsi
	movq	456(%r12), %rdi
	leal	-1(%rax), %ecx
	movl	%ecx, %edx
	andl	%esi, %edx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdi,%rax,8), %rax
	cmpb	$0, 16(%rax)
	jne	.L81
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L91:
	addq	$1, %rdx
	andq	%rcx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rdi,%rax,8), %rax
	cmpb	$0, 16(%rax)
	je	.L78
.L81:
	cmpq	(%rax), %rsi
	jne	.L91
	jmp	.L78
	.cfi_endproc
.LFE18263:
	.size	_ZN2v88internal15RootsSerializer22SerializeInObjectCacheENS0_10HeapObjectE, .-_ZN2v88internal15RootsSerializer22SerializeInObjectCacheENS0_10HeapObjectE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE, @function
_GLOBAL__sub_I__ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE:
.LFB22374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22374:
	.size	_GLOBAL__sub_I__ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE, .-_GLOBAL__sub_I__ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal15RootsSerializerC2EPNS0_7IsolateENS0_9RootIndexE
	.section	.rodata._ZN2v88internal15RootsSerializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"bitset::set"
	.section	.rodata._ZN2v88internal15RootsSerializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"%s: __position (which is %zu) >= _Nb (which is %zu)"
	.section	.text._ZN2v88internal15RootsSerializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15RootsSerializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.type	_ZN2v88internal15RootsSerializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, @function
_ZN2v88internal15RootsSerializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_:
.LFB18265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	104(%rdi), %rax
	movzwl	360(%rdi), %edi
	leaq	56(%rax,%rdi,8), %rbx
	cmpq	%rbx, %rcx
	jne	.L95
	movq	$-56, %r13
	subq	%rax, %r13
	cmpq	%r8, %rbx
	jnb	.L94
	.p2align 4,,10
	.p2align 3
.L99:
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal10Serializer19SerializeRootObjectENS0_6ObjectE@PLT
	leaq	0(%r13,%rbx), %rax
	movq	%rax, %rdx
	shrq	$3, %rdx
	cmpq	$4799, %rax
	ja	.L104
	andl	$63, %edx
	shrq	$9, %rax
	movl	$1, %esi
	addq	$8, %rbx
	movl	%edx, %ecx
	salq	%cl, %rsi
	orq	%rsi, 368(%r14,%rax,8)
	cmpq	%rbx, %r12
	ja	.L99
.L94:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10Serializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_@PLT
.L104:
	.cfi_restore_state
	movl	$600, %ecx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE18265:
	.size	_ZN2v88internal15RootsSerializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_, .-_ZN2v88internal15RootsSerializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.weak	_ZTVN2v88internal15RootsSerializerE
	.section	.data.rel.ro._ZTVN2v88internal15RootsSerializerE,"awG",@progbits,_ZTVN2v88internal15RootsSerializerE,comdat
	.align 8
	.type	_ZTVN2v88internal15RootsSerializerE, @object
	.size	_ZTVN2v88internal15RootsSerializerE, 72
_ZTVN2v88internal15RootsSerializerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15RootsSerializer17VisitRootPointersENS0_4RootEPKcNS0_14FullObjectSlotES5_
	.quad	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.quad	_ZN2v88internal15RootsSerializer11SynchronizeENS0_22VisitorSynchronization7SyncTagE
	.quad	__cxa_pure_virtual
	.quad	_ZN2v88internal10Serializer14MustBeDeferredENS0_10HeapObjectE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
