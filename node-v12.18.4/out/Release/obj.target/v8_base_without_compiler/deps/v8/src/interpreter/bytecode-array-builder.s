	.file	"bytecode-array-builder.cc"
	.text
	.section	.text._ZN2v88internal11interpreter22RegisterTransferWriterD2Ev,"axG",@progbits,_ZN2v88internal11interpreter22RegisterTransferWriterD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter22RegisterTransferWriterD2Ev
	.type	_ZN2v88internal11interpreter22RegisterTransferWriterD2Ev, @function
_ZN2v88internal11interpreter22RegisterTransferWriterD2Ev:
.LFB25570:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25570:
	.size	_ZN2v88internal11interpreter22RegisterTransferWriterD2Ev, .-_ZN2v88internal11interpreter22RegisterTransferWriterD2Ev
	.weak	_ZN2v88internal11interpreter22RegisterTransferWriterD1Ev
	.set	_ZN2v88internal11interpreter22RegisterTransferWriterD1Ev,_ZN2v88internal11interpreter22RegisterTransferWriterD2Ev
	.section	.rodata._ZN2v88internal11interpreter22RegisterTransferWriterD0Ev.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal11interpreter22RegisterTransferWriterD0Ev,"axG",@progbits,_ZN2v88internal11interpreter22RegisterTransferWriterD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter22RegisterTransferWriterD0Ev
	.type	_ZN2v88internal11interpreter22RegisterTransferWriterD0Ev, @function
_ZN2v88internal11interpreter22RegisterTransferWriterD0Ev:
.LFB25572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE25572:
	.size	_ZN2v88internal11interpreter22RegisterTransferWriterD0Ev, .-_ZN2v88internal11interpreter22RegisterTransferWriterD0Ev
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0:
.LFB25640:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L5
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r12), %esi
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	ret
	.cfi_endproc
.LFE25640:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE83ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE84ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE70ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE71ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE56ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE60ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE57ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE54ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE69ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE68ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE67ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE65ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE64ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE63ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE62ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE59ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE58ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE66ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE55ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE61ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE111ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE110ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE108ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE107ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE106ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE105ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE104ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE103ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE53ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE72ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE81ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE79ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE78ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE117ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE77ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE76ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE75ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE74ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE73ELNS1_14AccumulatorUseE3EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE18ELNS1_14AccumulatorUseE2EEEvv.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE18ELNS1_14AccumulatorUseE2EEEvv.isra.0, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE18ELNS1_14AccumulatorUseE2EEEvv.isra.0:
.LFB25686:
	.cfi_startproc
	testq	%rdi, %rdi
	je	.L10
	movl	8(%rdi), %esi
	jmp	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	ret
	.cfi_endproc
.LFE25686:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE18ELNS1_14AccumulatorUseE2EEEvv.isra.0, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE18ELNS1_14AccumulatorUseE2EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE26ELNS1_14AccumulatorUseE2EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE18ELNS1_14AccumulatorUseE2EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE27ELNS1_14AccumulatorUseE2EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE18ELNS1_14AccumulatorUseE2EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE88ELNS1_14AccumulatorUseE2EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE18ELNS1_14AccumulatorUseE2EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE90ELNS1_14AccumulatorUseE2EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE18ELNS1_14AccumulatorUseE2EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE94ELNS1_14AccumulatorUseE2EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE18ELNS1_14AccumulatorUseE2EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE93ELNS1_14AccumulatorUseE2EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE18ELNS1_14AccumulatorUseE2EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE92ELNS1_14AccumulatorUseE2EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE18ELNS1_14AccumulatorUseE2EEEvv.isra.0
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE89ELNS1_14AccumulatorUseE2EEEvv.isra.0,_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE18ELNS1_14AccumulatorUseE2EEEvv.isra.0
	.section	.text._ZN2v88internal11interpreter22RegisterTransferWriter8EmitLdarENS1_8RegisterE,"axG",@progbits,_ZN2v88internal11interpreter22RegisterTransferWriter8EmitLdarENS1_8RegisterE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter22RegisterTransferWriter8EmitLdarENS1_8RegisterE
	.type	_ZN2v88internal11interpreter22RegisterTransferWriter8EmitLdarENS1_8RegisterE, @function
_ZN2v88internal11interpreter22RegisterTransferWriter8EmitLdarENS1_8RegisterE:
.LFB19523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$-5, %eax
	subl	%esi, %eax
	leal	128(%rax), %ecx
	cmpl	$255, %ecx
	jbe	.L13
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L13:
	movl	%eax, -44(%rbp)
	movzbl	480(%rdi), %eax
	movb	$37, -48(%rbp)
	movb	%dl, -20(%rbp)
	movb	$0, -16(%rbp)
	movl	$-1, -12(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movl	$1, -24(%rbp)
	testb	%al, %al
	je	.L14
	movl	484(%rdi), %edx
	movb	%al, -16(%rbp)
	movb	$0, 480(%rdi)
	movl	%edx, -12(%rbp)
	movl	$-1, 484(%rdi)
.L14:
	addq	$320, %rdi
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L23:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19523:
	.size	_ZN2v88internal11interpreter22RegisterTransferWriter8EmitLdarENS1_8RegisterE, .-_ZN2v88internal11interpreter22RegisterTransferWriter8EmitLdarENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter22RegisterTransferWriter8EmitStarENS1_8RegisterE,"axG",@progbits,_ZN2v88internal11interpreter22RegisterTransferWriter8EmitStarENS1_8RegisterE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter22RegisterTransferWriter8EmitStarENS1_8RegisterE
	.type	_ZN2v88internal11interpreter22RegisterTransferWriter8EmitStarENS1_8RegisterE, @function
_ZN2v88internal11interpreter22RegisterTransferWriter8EmitStarENS1_8RegisterE:
.LFB19524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$-5, %eax
	subl	%esi, %eax
	leal	128(%rax), %ecx
	cmpl	$255, %ecx
	jbe	.L25
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L25:
	movl	%eax, -44(%rbp)
	movzbl	480(%rdi), %eax
	movb	$38, -48(%rbp)
	movb	%dl, -20(%rbp)
	movb	$0, -16(%rbp)
	movl	$-1, -12(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movl	$1, -24(%rbp)
	testb	%al, %al
	je	.L26
	movl	484(%rdi), %edx
	movb	%al, -16(%rbp)
	movb	$0, 480(%rdi)
	movl	%edx, -12(%rbp)
	movl	$-1, 484(%rdi)
.L26:
	addq	$320, %rdi
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L35
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L35:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19524:
	.size	_ZN2v88internal11interpreter22RegisterTransferWriter8EmitStarENS1_8RegisterE, .-_ZN2v88internal11interpreter22RegisterTransferWriter8EmitStarENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter22RegisterTransferWriter7EmitMovENS1_8RegisterES3_,"axG",@progbits,_ZN2v88internal11interpreter22RegisterTransferWriter7EmitMovENS1_8RegisterES3_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11interpreter22RegisterTransferWriter7EmitMovENS1_8RegisterES3_
	.type	_ZN2v88internal11interpreter22RegisterTransferWriter7EmitMovENS1_8RegisterES3_, @function
_ZN2v88internal11interpreter22RegisterTransferWriter7EmitMovENS1_8RegisterES3_:
.LFB19525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$-5, %eax
	movl	%eax, %ecx
	subl	%edx, %eax
	subl	%esi, %ecx
	leal	128(%rcx), %edx
	movl	%ecx, %esi
	cmpl	$255, %edx
	jbe	.L42
	leal	32768(%rcx), %edx
	cmpl	$65536, %edx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L37:
	leal	128(%rax), %r8d
	cmpl	$255, %r8d
	jbe	.L38
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	movl	$4, %edx
	cmovb	%ecx, %edx
.L38:
	movl	%eax, -40(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -28(%rbp)
	movzbl	480(%rdi), %eax
	movb	$39, -48(%rbp)
	movb	%dl, -20(%rbp)
	movb	$0, -16(%rbp)
	movl	$-1, -12(%rbp)
	movl	%esi, -44(%rbp)
	movq	$0, -36(%rbp)
	testb	%al, %al
	je	.L40
	movl	484(%rdi), %edx
	movb	%al, -16(%rbp)
	movb	$0, 480(%rdi)
	movl	%edx, -12(%rbp)
	movl	$-1, 484(%rdi)
.L40:
	addq	$320, %rdi
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movl	$2, %ecx
	movl	$1, %edx
	jmp	.L37
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19525:
	.size	_ZN2v88internal11interpreter22RegisterTransferWriter7EmitMovENS1_8RegisterES3_, .-_ZN2v88internal11interpreter22RegisterTransferWriter7EmitMovENS1_8RegisterES3_
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0:
.LFB25638:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L51
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L51:
	movzbl	472(%rbx), %eax
	testb	%al, %al
	je	.L52
	cmpb	$2, %al
	je	.L53
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L52
	movl	476(%rbx), %edx
	pxor	%xmm0, %xmm0
	movb	$-102, -80(%rbp)
	movabsq	$4294967296, %rcx
	movb	$0, 472(%rbx)
	movl	%edx, -44(%rbp)
	movzbl	480(%rbx), %edx
	movl	$-1, 476(%rbx)
	movb	%al, -48(%rbp)
	movq	%rcx, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L55
	cmpb	$1, %al
	jne	.L56
	cmpb	$2, %dl
	jne	.L56
	movb	$2, -48(%rbp)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L52:
	movabsq	$4294967296, %rax
	pxor	%xmm0, %xmm0
	movb	$-102, -80(%rbp)
	movq	%rax, -60(%rbp)
	movzbl	480(%rbx), %eax
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%al, %al
	je	.L55
	movl	484(%rbx), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L56:
	movb	$0, 480(%rbx)
	movl	$-1, 484(%rbx)
.L55:
	leaq	-80(%rbp), %rsi
	leaq	320(%rbx), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movl	476(%rbx), %eax
	pxor	%xmm0, %xmm0
	movb	$-102, -80(%rbp)
	cmpb	$0, 480(%rbx)
	movb	$0, 472(%rbx)
	movl	%eax, -44(%rbp)
	movabsq	$4294967296, %rax
	movl	$-1, 476(%rbx)
	movb	$2, -48(%rbp)
	movq	%rax, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	jne	.L56
	jmp	.L55
.L78:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25638:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0:
.LFB25637:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L80
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L80:
	movzbl	472(%rbx), %eax
	testb	%al, %al
	je	.L81
	cmpb	$2, %al
	je	.L82
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L81
	movl	476(%rbx), %edx
	pxor	%xmm0, %xmm0
	movb	$-103, -80(%rbp)
	movabsq	$4294967296, %rcx
	movb	$0, 472(%rbx)
	movl	%edx, -44(%rbp)
	movzbl	480(%rbx), %edx
	movl	$-1, 476(%rbx)
	movb	%al, -48(%rbp)
	movq	%rcx, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L84
	cmpb	$1, %al
	jne	.L85
	cmpb	$2, %dl
	jne	.L85
	movb	$2, -48(%rbp)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L81:
	movabsq	$4294967296, %rax
	pxor	%xmm0, %xmm0
	movb	$-103, -80(%rbp)
	movq	%rax, -60(%rbp)
	movzbl	480(%rbx), %eax
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%al, %al
	je	.L84
	movl	484(%rbx), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L85:
	movb	$0, 480(%rbx)
	movl	$-1, 484(%rbx)
.L84:
	leaq	-80(%rbp), %rsi
	leaq	320(%rbx), %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	movl	476(%rbx), %eax
	pxor	%xmm0, %xmm0
	movb	$-103, -80(%rbp)
	cmpb	$0, 480(%rbx)
	movb	$0, 472(%rbx)
	movl	%eax, -44(%rbp)
	movabsq	$4294967296, %rax
	movl	$-1, 476(%rbx)
	movb	$2, -48(%rbp)
	movq	%rax, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	jne	.L85
	jmp	.L84
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25637:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0
	.section	.text._ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE,"axG",@progbits,_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE,comdat
	.p2align 4
	.weak	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	.type	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE, @function
_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE:
.LFB10311:
	.cfi_startproc
	endbr64
	cmpb	$39, %dil
	jbe	.L109
	leal	-112(%rdi), %edx
	movl	$1, %eax
	cmpb	$3, %dl
	ja	.L111
.L108:
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	movabsq	$962337437696, %rdx
	movl	$1, %eax
	btq	%rdi, %rdx
	jc	.L108
.L111:
	leal	118(%rdi), %eax
	cmpb	$22, %al
	ja	.L112
	addl	$107, %edi
	cmpb	$3, %dil
	seta	%al
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	cmpb	$-95, %dil
	sete	%al
	cmpb	$-81, %dil
	sete	%dl
	orl	%edx, %eax
	ret
	.cfi_endproc
.LFE10311:
	.size	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE, .-_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilderC2EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilderC2EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilderC2EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilderC2EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE:
.LFB19557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movd	%ecx, %xmm0
	movd	%edx, %xmm2
	movd	%ecx, %xmm1
	punpckldq	%xmm0, %xmm2
	punpckldq	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	punpcklqdq	%xmm1, %xmm0
	movq	%rsi, %xmm1
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	24(%rdi), %r14
	movl	%r9d, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%r8, -40(%rbp)
	movb	$0, 16(%rdi)
	movl	%edx, -44(%rbp)
	movhps	-40(%rbp), %xmm1
	movaps	%xmm0, -64(%rbp)
	movups	%xmm1, (%rdi)
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter20ConstantArrayBuilderC1EPNS0_4ZoneE@PLT
	leaq	264(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal11interpreter19HandlerTableBuilderC1EPNS0_4ZoneE@PLT
	movdqa	-64(%rbp), %xmm0
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movq	$0, 312(%rbx)
	leaq	320(%rbx), %rdi
	movq	%r12, %rsi
	movups	%xmm0, 296(%rbx)
	call	_ZN2v88internal11interpreter19BytecodeArrayWriterC1EPNS0_4ZoneEPNS1_20ConstantArrayBuilderENS0_26SourcePositionTableBuilder13RecordingModeE@PLT
	cmpb	$0, _ZN2v88internal17FLAG_ignition_reoE(%rip)
	movq	$0, 464(%rbx)
	movb	$0, 472(%rbx)
	movl	$-1, 476(%rbx)
	movb	$0, 480(%rbx)
	movl	$-1, 484(%rbx)
	jne	.L123
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movq	16(%r12), %r9
	movq	24(%r12), %rax
	movl	300(%rbx), %ecx
	subq	%r9, %rax
	cmpq	$15, %rax
	jbe	.L124
	leaq	16(%r9), %rax
	movq	%rax, 16(%r12)
.L119:
	leaq	16+_ZTVN2v88internal11interpreter22RegisterTransferWriterE(%rip), %rax
	movq	%rbx, 8(%r9)
	movq	%rax, (%r9)
	movq	16(%r12), %r13
	movq	24(%r12), %rax
	subq	%r13, %rax
	cmpq	$199, %rax
	jbe	.L125
	leaq	200(%r13), %rax
	movq	%rax, 16(%r12)
.L121:
	movl	-44(%rbp), %r8d
	leaq	304(%rbx), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizerC1EPNS0_4ZoneEPNS1_25BytecodeRegisterAllocatorEiiPNS2_14BytecodeWriterE@PLT
	movq	%r13, 464(%rbx)
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movl	$16, %esi
	movq	%r12, %rdi
	movl	%ecx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-40(%rbp), %ecx
	movq	%rax, %r9
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L125:
	movl	$200, %esi
	movq	%r12, %rdi
	movq	%r9, -64(%rbp)
	movl	%ecx, -40(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movl	-40(%rbp), %ecx
	movq	-64(%rbp), %r9
	movq	%rax, %r13
	jmp	.L121
	.cfi_endproc
.LFE19557:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilderC2EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilderC2EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilderC1EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE
	.set	_ZN2v88internal11interpreter20BytecodeArrayBuilderC1EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE,_ZN2v88internal11interpreter20BytecodeArrayBuilderC2EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE
	.section	.text._ZNK2v88internal11interpreter20BytecodeArrayBuilder9ParameterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter20BytecodeArrayBuilder9ParameterEi
	.type	_ZNK2v88internal11interpreter20BytecodeArrayBuilder9ParameterEi, @function
_ZNK2v88internal11interpreter20BytecodeArrayBuilder9ParameterEi:
.LFB19559:
	.cfi_startproc
	endbr64
	movl	296(%rdi), %r8d
	leal	1(%rsi), %edi
	movl	%r8d, %esi
	jmp	_ZN2v88internal11interpreter8Register18FromParameterIndexEii@PLT
	.cfi_endproc
.LFE19559:
	.size	_ZNK2v88internal11interpreter20BytecodeArrayBuilder9ParameterEi, .-_ZNK2v88internal11interpreter20BytecodeArrayBuilder9ParameterEi
	.section	.text._ZNK2v88internal11interpreter20BytecodeArrayBuilder8ReceiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter20BytecodeArrayBuilder8ReceiverEv
	.type	_ZNK2v88internal11interpreter20BytecodeArrayBuilder8ReceiverEv, @function
_ZNK2v88internal11interpreter20BytecodeArrayBuilder8ReceiverEv:
.LFB19560:
	.cfi_startproc
	endbr64
	movl	296(%rdi), %esi
	xorl	%edi, %edi
	jmp	_ZN2v88internal11interpreter8Register18FromParameterIndexEii@PLT
	.cfi_endproc
.LFE19560:
	.size	_ZNK2v88internal11interpreter20BytecodeArrayBuilder8ReceiverEv, .-_ZNK2v88internal11interpreter20BytecodeArrayBuilder8ReceiverEv
	.section	.rodata._ZNK2v88internal11interpreter20BytecodeArrayBuilder5LocalEi.str1.1,"aMS",@progbits,1
.LC1:
	.string	"index < locals_count()"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZNK2v88internal11interpreter20BytecodeArrayBuilder5LocalEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter20BytecodeArrayBuilder5LocalEi
	.type	_ZNK2v88internal11interpreter20BytecodeArrayBuilder5LocalEi, @function
_ZNK2v88internal11interpreter20BytecodeArrayBuilder5LocalEi:
.LFB19561:
	.cfi_startproc
	endbr64
	cmpl	300(%rdi), %esi
	jge	.L133
	movl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19561:
	.size	_ZNK2v88internal11interpreter20BytecodeArrayBuilder5LocalEi, .-_ZNK2v88internal11interpreter20BytecodeArrayBuilder5LocalEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder15ToBytecodeArrayEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder15ToBytecodeArrayEPNS0_7IsolateE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder15ToBytecodeArrayEPNS0_7IsolateE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder15ToBytecodeArrayEPNS0_7IsolateE:
.LFB19562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movb	$1, 16(%rdi)
	movq	464(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L138
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	464(%rbx), %rax
	movl	28(%rax), %r13d
	addl	$1, %r13d
.L136:
	leaq	264(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal11interpreter19HandlerTableBuilder14ToHandlerTableEPNS0_7IsolateE@PLT
	movl	296(%rbx), %ecx
	addq	$8, %rsp
	movl	%r13d, %edx
	leaq	320(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	movq	%rax, %r8
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter15ToBytecodeArrayEPNS0_7IsolateEiiNS0_6HandleINS0_9ByteArrayEEE@PLT
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movl	308(%rbx), %r13d
	jmp	.L136
	.cfi_endproc
.LFE19562:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder15ToBytecodeArrayEPNS0_7IsolateE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder15ToBytecodeArrayEPNS0_7IsolateE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder21ToSourcePositionTableEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder21ToSourcePositionTableEPNS0_7IsolateE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder21ToSourcePositionTableEPNS0_7IsolateE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder21ToSourcePositionTableEPNS0_7IsolateE:
.LFB19563:
	.cfi_startproc
	endbr64
	addq	$320, %rdi
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter21ToSourcePositionTableEPNS0_7IsolateE@PLT
	.cfi_endproc
.LFE19563:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder21ToSourcePositionTableEPNS0_7IsolateE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder21ToSourcePositionTableEPNS0_7IsolateE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder21SetDeferredSourceInfoENS1_18BytecodeSourceInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder21SetDeferredSourceInfoENS1_18BytecodeSourceInfoE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder21SetDeferredSourceInfoENS1_18BytecodeSourceInfoE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder21SetDeferredSourceInfoENS1_18BytecodeSourceInfoE:
.LFB19565:
	.cfi_startproc
	endbr64
	testb	%sil, %sil
	je	.L140
	movq	%rsi, 480(%rdi)
.L140:
	ret
	.cfi_endproc
.LFE19565:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder21SetDeferredSourceInfoENS1_18BytecodeSourceInfoE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder21SetDeferredSourceInfoENS1_18BytecodeSourceInfoE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder30AttachOrEmitDeferredSourceInfoEPNS1_12BytecodeNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder30AttachOrEmitDeferredSourceInfoEPNS1_12BytecodeNodeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder30AttachOrEmitDeferredSourceInfoEPNS1_12BytecodeNodeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder30AttachOrEmitDeferredSourceInfoEPNS1_12BytecodeNodeE:
.LFB19566:
	.cfi_startproc
	endbr64
	movzbl	480(%rdi), %eax
	testb	%al, %al
	je	.L142
	movzbl	32(%rsi), %edx
	testb	%dl, %dl
	jne	.L144
	movl	484(%rdi), %edx
	movb	%al, 32(%rsi)
	movl	%edx, 36(%rsi)
.L145:
	movb	$0, 480(%rdi)
	movl	$-1, 484(%rdi)
.L142:
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	cmpb	$2, %al
	jne	.L145
	cmpb	$1, %dl
	jne	.L145
	movb	$2, 32(%rsi)
	jmp	.L145
	.cfi_endproc
.LFE19566:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder30AttachOrEmitDeferredSourceInfoEPNS1_12BytecodeNodeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder30AttachOrEmitDeferredSourceInfoEPNS1_12BytecodeNodeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE:
.LFB19567:
	.cfi_startproc
	endbr64
	movzbl	480(%rdi), %eax
	testb	%al, %al
	je	.L156
	movzbl	32(%rsi), %edx
	testb	%dl, %dl
	jne	.L157
	movl	484(%rdi), %edx
	movb	%al, 32(%rsi)
	movl	%edx, 36(%rsi)
.L158:
	movb	$0, 480(%rdi)
	movl	$-1, 484(%rdi)
.L156:
	addq	$320, %rdi
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	.p2align 4,,10
	.p2align 3
.L157:
	cmpb	$1, %dl
	jne	.L158
	cmpb	$2, %al
	jne	.L158
	movb	$2, 32(%rsi)
	jmp	.L158
	.cfi_endproc
.LFE19567:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE:
.LFB19568:
	.cfi_startproc
	endbr64
	movzbl	480(%rdi), %eax
	testb	%al, %al
	je	.L169
	movzbl	32(%rsi), %ecx
	testb	%cl, %cl
	jne	.L170
	movl	484(%rdi), %ecx
	movb	%al, 32(%rsi)
	movl	%ecx, 36(%rsi)
.L171:
	movb	$0, 480(%rdi)
	movl	$-1, 484(%rdi)
.L169:
	addq	$320, %rdi
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	cmpb	$1, %cl
	jne	.L171
	cmpb	$2, %al
	jne	.L171
	movb	$2, 32(%rsi)
	jmp	.L171
	.cfi_endproc
.LFE19568:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE:
.LFB19569:
	.cfi_startproc
	endbr64
	movzbl	480(%rdi), %eax
	testb	%al, %al
	je	.L182
	movzbl	32(%rsi), %ecx
	testb	%cl, %cl
	jne	.L183
	movl	484(%rdi), %ecx
	movb	%al, 32(%rsi)
	movl	%ecx, 36(%rsi)
.L184:
	movb	$0, 480(%rdi)
	movl	$-1, 484(%rdi)
.L182:
	addq	$320, %rdi
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE@PLT
	.p2align 4,,10
	.p2align 3
.L183:
	cmpb	$1, %cl
	jne	.L184
	cmpb	$2, %al
	jne	.L184
	movb	$2, 32(%rsi)
	jmp	.L184
	.cfi_endproc
.LFE19569:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE:
.LFB19570:
	.cfi_startproc
	endbr64
	movzbl	480(%rdi), %eax
	testb	%al, %al
	je	.L195
	movzbl	32(%rsi), %ecx
	testb	%cl, %cl
	jne	.L196
	movl	484(%rdi), %ecx
	movb	%al, 32(%rsi)
	movl	%ecx, 36(%rsi)
.L197:
	movb	$0, 480(%rdi)
	movl	$-1, 484(%rdi)
.L195:
	addq	$320, %rdi
	jmp	_ZN2v88internal11interpreter19BytecodeArrayWriter11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE@PLT
	.p2align 4,,10
	.p2align 3
.L196:
	cmpb	$1, %cl
	jne	.L197
	cmpb	$2, %al
	jne	.L197
	movb	$2, 32(%rsi)
	jmp	.L197
	.cfi_endproc
.LFE19570:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder13OutputLdarRawENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder13OutputLdarRawENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder13OutputLdarRawENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder13OutputLdarRawENS1_8RegisterE:
.LFB19571:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$-5, %eax
	subl	%esi, %eax
	leal	128(%rax), %ecx
	cmpl	$255, %ecx
	jbe	.L208
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L208:
	movl	%eax, -44(%rbp)
	movzbl	480(%rdi), %eax
	movb	$37, -48(%rbp)
	movb	%dl, -20(%rbp)
	movb	$0, -16(%rbp)
	movl	$-1, -12(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movl	$1, -24(%rbp)
	testb	%al, %al
	je	.L209
	movl	484(%rdi), %edx
	movb	%al, -16(%rbp)
	movb	$0, 480(%rdi)
	movl	%edx, -12(%rbp)
	movl	$-1, 484(%rdi)
.L209:
	addq	$320, %rdi
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L218:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19571:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder13OutputLdarRawENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder13OutputLdarRawENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder13OutputStarRawENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder13OutputStarRawENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder13OutputStarRawENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder13OutputStarRawENS1_8RegisterE:
.LFB19572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$-5, %eax
	subl	%esi, %eax
	leal	128(%rax), %ecx
	cmpl	$255, %ecx
	jbe	.L220
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L220:
	movl	%eax, -44(%rbp)
	movzbl	480(%rdi), %eax
	movb	$38, -48(%rbp)
	movb	%dl, -20(%rbp)
	movb	$0, -16(%rbp)
	movl	$-1, -12(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -32(%rbp)
	movl	$1, -24(%rbp)
	testb	%al, %al
	je	.L221
	movl	484(%rdi), %edx
	movb	%al, -16(%rbp)
	movb	$0, 480(%rdi)
	movl	%edx, -12(%rbp)
	movl	$-1, 484(%rdi)
.L221:
	addq	$320, %rdi
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L230:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19572:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder13OutputStarRawENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder13OutputStarRawENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder12OutputMovRawENS1_8RegisterES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder12OutputMovRawENS1_8RegisterES3_
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder12OutputMovRawENS1_8RegisterES3_, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder12OutputMovRawENS1_8RegisterES3_:
.LFB19573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$-5, %eax
	movl	%eax, %ecx
	subl	%edx, %eax
	subl	%esi, %ecx
	leal	128(%rcx), %edx
	movl	%ecx, %esi
	cmpl	$255, %edx
	jbe	.L237
	leal	32768(%rcx), %edx
	cmpl	$65536, %edx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L232:
	leal	128(%rax), %r8d
	cmpl	$255, %r8d
	jbe	.L233
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	movl	$4, %edx
	cmovb	%ecx, %edx
.L233:
	movl	%eax, -40(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -28(%rbp)
	movzbl	480(%rdi), %eax
	movb	$39, -48(%rbp)
	movb	%dl, -20(%rbp)
	movb	$0, -16(%rbp)
	movl	$-1, -12(%rbp)
	movl	%esi, -44(%rbp)
	movq	$0, -36(%rbp)
	testb	%al, %al
	je	.L235
	movl	484(%rdi), %edx
	movb	%al, -16(%rbp)
	movb	$0, 480(%rdi)
	movl	%edx, -12(%rbp)
	movl	$-1, 484(%rdi)
.L235:
	addq	$320, %rdi
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L244
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movl	$2, %ecx
	movl	$1, %edx
	jmp	.L232
.L244:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19573:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder12OutputMovRawENS1_8RegisterES3_, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder12OutputMovRawENS1_8RegisterES3_
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi:
.LFB20137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$34, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$11, %sil
	ja	.L246
	movl	%ecx, %ebx
	movzbl	%sil, %esi
	leaq	.L248(%rip), %rcx
	movl	%edx, %r13d
	movl	%edx, %r14d
	movslq	(%rcx,%rsi,4), %rdx
	movq	%rdi, %r12
	movq	464(%rdi), %rdi
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi,"a",@progbits
	.align 4
	.align 4
.L248:
	.long	.L259-.L248
	.long	.L258-.L248
	.long	.L257-.L248
	.long	.L256-.L248
	.long	.L255-.L248
	.long	.L254-.L248
	.long	.L253-.L248
	.long	.L252-.L248
	.long	.L251-.L248
	.long	.L250-.L248
	.long	.L249-.L248
	.long	.L247-.L248
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi
	.p2align 4,,10
	.p2align 3
.L249:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE52ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L260
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L260:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L480
.L261:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L336
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L263:
	cmpl	$255, %ebx
	jbe	.L264
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L264:
	movb	$52, -96(%rbp)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L247:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE53ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L267
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L267:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L481
.L268:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L341
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L270:
	cmpl	$255, %ebx
	jbe	.L271
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L271:
	movb	$53, -96(%rbp)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L259:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE58ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L297
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L297:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L482
.L298:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L366
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L300:
	cmpl	$255, %ebx
	jbe	.L301
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L301:
	movb	$58, -96(%rbp)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L258:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE59ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L303
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L303:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L483
.L304:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L371
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L306:
	cmpl	$255, %ebx
	jbe	.L307
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L307:
	movb	$59, -96(%rbp)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L257:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE60ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L309
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L309:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L484
.L310:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L376
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L312:
	cmpl	$255, %ebx
	jbe	.L313
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L313:
	movb	$60, -96(%rbp)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L253:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE54ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L273
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L273:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L485
.L274:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L346
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L276:
	cmpl	$255, %ebx
	jbe	.L277
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L277:
	movb	$54, -96(%rbp)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L255:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE62ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L321
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L321:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L486
.L322:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L386
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L324:
	cmpl	$255, %ebx
	jbe	.L325
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L325:
	movb	$62, -96(%rbp)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L251:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE56ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L285
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L285:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L487
.L286:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L356
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L288:
	cmpl	$255, %ebx
	jbe	.L289
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L289:
	movb	$56, -96(%rbp)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L256:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE61ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L315
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L315:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L488
.L316:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L381
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L318:
	cmpl	$255, %ebx
	jbe	.L319
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L319:
	movb	$61, -96(%rbp)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L252:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE55ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L279
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L279:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L489
.L280:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L351
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L282:
	cmpl	$255, %ebx
	jbe	.L283
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L283:
	movb	$55, -96(%rbp)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L254:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE63ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L327
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L327:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L490
.L328:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L391
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L330:
	cmpl	$255, %ebx
	jbe	.L331
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L331:
	movb	$63, -96(%rbp)
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L250:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE57ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L291
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L291:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L491
.L292:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L361
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L294:
	cmpl	$255, %ebx
	jbe	.L295
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L295:
	movb	$57, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L479:
	movb	%al, -68(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$8589934592, %rax
	movq	%rax, -76(%rbp)
	movb	%r14b, -64(%rbp)
	movl	%r15d, -60(%rbp)
	movl	%r13d, -92(%rbp)
	movl	%ebx, -88(%rbp)
	movq	$0, -84(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L492
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	cmpb	$2, %r14b
	je	.L329
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L329
	movl	$63, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L329
	xorl	%r14d, %r14d
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L486:
	cmpb	$2, %r14b
	je	.L323
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L323
	movl	$62, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L323
	xorl	%r14d, %r14d
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L488:
	cmpb	$2, %r14b
	je	.L317
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L317
	movl	$61, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L317
	xorl	%r14d, %r14d
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L484:
	cmpb	$2, %r14b
	je	.L311
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L311
	movl	$60, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L311
	xorl	%r14d, %r14d
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L489:
	cmpb	$2, %r14b
	je	.L281
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L281
	movl	$55, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L281
	xorl	%r14d, %r14d
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L482:
	cmpb	$2, %r14b
	je	.L299
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L299
	movl	$58, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L299
	xorl	%r14d, %r14d
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L487:
	cmpb	$2, %r14b
	je	.L287
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L287
	movl	$56, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L287
	xorl	%r14d, %r14d
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L480:
	cmpb	$2, %r14b
	je	.L262
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L262
	movl	$52, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L262
	xorl	%r14d, %r14d
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L485:
	cmpb	$2, %r14b
	je	.L275
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L275
	movl	$54, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L275
	xorl	%r14d, %r14d
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L481:
	cmpb	$2, %r14b
	je	.L269
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L269
	movl	$53, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L269
	xorl	%r14d, %r14d
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L483:
	cmpb	$2, %r14b
	je	.L305
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L305
	movl	$59, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L305
	xorl	%r14d, %r14d
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L491:
	cmpb	$2, %r14b
	je	.L293
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L293
	movl	$57, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L293
	xorl	%r14d, %r14d
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L329:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L323:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L317:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L293:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L311:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L262:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L299:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L269:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L281:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L275:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L287:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L305:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L361:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L341:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L336:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L371:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L366:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L351:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L381:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L391:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L346:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L356:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L386:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L376:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L312
.L246:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L492:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20137:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder15BinaryOperationENS0_5Token5ValueENS1_8RegisterEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder25BinaryOperationSmiLiteralENS0_5Token5ValueENS0_3SmiEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder25BinaryOperationSmiLiteralENS0_5Token5ValueENS0_3SmiEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder25BinaryOperationSmiLiteralENS0_5Token5ValueENS0_3SmiEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder25BinaryOperationSmiLiteralENS0_5Token5ValueENS0_3SmiEi:
.LFB20138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$34, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$11, %sil
	ja	.L494
	movq	%rdx, %rbx
	movzbl	%sil, %esi
	movq	%rdi, %r12
	movl	%ecx, %r13d
	leaq	.L496(%rip), %rdx
	movq	464(%rdi), %rdi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter20BytecodeArrayBuilder25BinaryOperationSmiLiteralENS0_5Token5ValueENS0_3SmiEi,"a",@progbits
	.align 4
	.align 4
.L496:
	.long	.L507-.L496
	.long	.L506-.L496
	.long	.L505-.L496
	.long	.L504-.L496
	.long	.L503-.L496
	.long	.L502-.L496
	.long	.L501-.L496
	.long	.L500-.L496
	.long	.L499-.L496
	.long	.L498-.L496
	.long	.L497-.L496
	.long	.L495-.L496
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder25BinaryOperationSmiLiteralENS0_5Token5ValueENS0_3SmiEi
	.p2align 4,,10
	.p2align 3
.L497:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE64ELNS1_14AccumulatorUseE3EEEvv.isra.0
	sarq	$32, %rbx
	movl	$-1, %r15d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L680
.L508:
	leal	128(%rbx), %eax
	cmpl	$255, %eax
	jbe	.L572
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L510:
	cmpl	$255, %r13d
	jbe	.L511
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L511:
	movb	$64, -96(%rbp)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L495:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE65ELNS1_14AccumulatorUseE3EEEvv.isra.0
	sarq	$32, %rbx
	movl	$-1, %r15d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L681
.L514:
	leal	128(%rbx), %eax
	cmpl	$255, %eax
	jbe	.L577
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L516:
	cmpl	$255, %r13d
	jbe	.L517
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L517:
	movb	$65, -96(%rbp)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L507:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE70ELNS1_14AccumulatorUseE3EEEvv.isra.0
	sarq	$32, %rbx
	movl	$-1, %r15d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L682
.L539:
	leal	128(%rbx), %eax
	cmpl	$255, %eax
	jbe	.L602
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L541:
	cmpl	$255, %r13d
	jbe	.L542
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L542:
	movb	$70, -96(%rbp)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L506:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE71ELNS1_14AccumulatorUseE3EEEvv.isra.0
	sarq	$32, %rbx
	movl	$-1, %r15d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L683
.L544:
	leal	128(%rbx), %eax
	cmpl	$255, %eax
	jbe	.L607
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L546:
	cmpl	$255, %r13d
	jbe	.L547
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L547:
	movb	$71, -96(%rbp)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L505:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE72ELNS1_14AccumulatorUseE3EEEvv.isra.0
	sarq	$32, %rbx
	movl	$-1, %r15d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L684
.L549:
	leal	128(%rbx), %eax
	cmpl	$255, %eax
	jbe	.L612
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L551:
	cmpl	$255, %r13d
	jbe	.L552
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L552:
	movb	$72, -96(%rbp)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L501:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE66ELNS1_14AccumulatorUseE3EEEvv.isra.0
	sarq	$32, %rbx
	movl	$-1, %r15d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L685
.L519:
	leal	128(%rbx), %eax
	cmpl	$255, %eax
	jbe	.L582
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L521:
	cmpl	$255, %r13d
	jbe	.L522
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L522:
	movb	$66, -96(%rbp)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L503:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE74ELNS1_14AccumulatorUseE3EEEvv.isra.0
	sarq	$32, %rbx
	movl	$-1, %r15d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L686
.L559:
	leal	128(%rbx), %eax
	cmpl	$255, %eax
	jbe	.L622
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L561:
	cmpl	$255, %r13d
	jbe	.L562
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L562:
	movb	$74, -96(%rbp)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L499:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE68ELNS1_14AccumulatorUseE3EEEvv.isra.0
	sarq	$32, %rbx
	movl	$-1, %r15d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L687
.L529:
	leal	128(%rbx), %eax
	cmpl	$255, %eax
	jbe	.L592
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L531:
	cmpl	$255, %r13d
	jbe	.L532
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L532:
	movb	$68, -96(%rbp)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L504:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE73ELNS1_14AccumulatorUseE3EEEvv.isra.0
	sarq	$32, %rbx
	movl	$-1, %r15d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L688
.L554:
	leal	128(%rbx), %eax
	cmpl	$255, %eax
	jbe	.L617
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L556:
	cmpl	$255, %r13d
	jbe	.L557
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L557:
	movb	$73, -96(%rbp)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L500:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE67ELNS1_14AccumulatorUseE3EEEvv.isra.0
	sarq	$32, %rbx
	movl	$-1, %r15d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L689
.L524:
	leal	128(%rbx), %eax
	cmpl	$255, %eax
	jbe	.L587
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L526:
	cmpl	$255, %r13d
	jbe	.L527
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L527:
	movb	$67, -96(%rbp)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L502:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE75ELNS1_14AccumulatorUseE3EEEvv.isra.0
	sarq	$32, %rbx
	movl	$-1, %r15d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L690
.L564:
	leal	128(%rbx), %eax
	cmpl	$255, %eax
	jbe	.L627
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L566:
	cmpl	$255, %r13d
	jbe	.L567
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%edx, %eax
.L567:
	movb	$75, -96(%rbp)
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L498:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE69ELNS1_14AccumulatorUseE3EEEvv.isra.0
	sarq	$32, %rbx
	movl	$-1, %r15d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	jne	.L691
.L534:
	leal	128(%rbx), %eax
	cmpl	$255, %eax
	jbe	.L597
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L536:
	cmpl	$255, %r13d
	jbe	.L537
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L537:
	movb	$69, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L679:
	movb	%al, -68(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$8589934592, %rax
	movq	%rax, -76(%rbp)
	movb	%r14b, -64(%rbp)
	movl	%r15d, -60(%rbp)
	movl	%ebx, -92(%rbp)
	movl	%r13d, -88(%rbp)
	movq	$0, -84(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L692
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L691:
	.cfi_restore_state
	cmpb	$2, %r14b
	je	.L535
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L535
	movl	$69, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L535
	xorl	%r14d, %r14d
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L690:
	cmpb	$2, %r14b
	je	.L565
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L565
	movl	$75, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L565
	xorl	%r14d, %r14d
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L689:
	cmpb	$2, %r14b
	je	.L525
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L525
	movl	$67, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L525
	xorl	%r14d, %r14d
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L685:
	cmpb	$2, %r14b
	je	.L520
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L520
	movl	$66, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L520
	xorl	%r14d, %r14d
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L687:
	cmpb	$2, %r14b
	je	.L530
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L530
	movl	$68, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L530
	xorl	%r14d, %r14d
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L683:
	cmpb	$2, %r14b
	je	.L545
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L545
	movl	$71, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L545
	xorl	%r14d, %r14d
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L688:
	cmpb	$2, %r14b
	je	.L555
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L555
	movl	$73, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L555
	xorl	%r14d, %r14d
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L684:
	cmpb	$2, %r14b
	je	.L550
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L550
	movl	$72, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L550
	xorl	%r14d, %r14d
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L686:
	cmpb	$2, %r14b
	je	.L560
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L560
	movl	$74, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L560
	xorl	%r14d, %r14d
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L682:
	cmpb	$2, %r14b
	je	.L540
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L540
	movl	$70, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L540
	xorl	%r14d, %r14d
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L681:
	cmpb	$2, %r14b
	je	.L515
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L515
	movl	$65, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L515
	xorl	%r14d, %r14d
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L680:
	cmpb	$2, %r14b
	je	.L509
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L509
	movl	$64, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L509
	xorl	%r14d, %r14d
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L535:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L509:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L565:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L545:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L520:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L550:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L525:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L555:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L530:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L560:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L540:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L515:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L627:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L622:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L617:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L612:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L587:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L582:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L592:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L607:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L602:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L577:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L572:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L597:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L536
.L494:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L692:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20138:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder25BinaryOperationSmiLiteralENS0_5Token5ValueENS0_3SmiEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder25BinaryOperationSmiLiteralENS0_5Token5ValueENS0_3SmiEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder14UnaryOperationENS0_5Token5ValueEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder14UnaryOperationENS0_5Token5ValueEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder14UnaryOperationENS0_5Token5ValueEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder14UnaryOperationENS0_5Token5ValueEi:
.LFB20139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$44, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$8, %sil
	ja	.L694
	movl	%edx, %ebx
	movzbl	%sil, %esi
	leaq	.L696(%rip), %rdx
	movq	%rdi, %r12
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter20BytecodeArrayBuilder14UnaryOperationENS0_5Token5ValueEi,"a",@progbits
	.align 4
	.align 4
.L696:
	.long	.L700-.L696
	.long	.L699-.L696
	.long	.L694-.L696
	.long	.L698-.L696
	.long	.L694-.L696
	.long	.L694-.L696
	.long	.L694-.L696
	.long	.L697-.L696
	.long	.L695-.L696
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder14UnaryOperationENS0_5Token5ValueEi
	.p2align 4,,10
	.p2align 3
.L700:
	movq	464(%rdi), %rdi
	movl	$-1, %r14d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE117ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movzbl	472(%r12), %r13d
	testb	%r13b, %r13b
	je	.L708
	cmpb	$2, %r13b
	je	.L709
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L760
.L709:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L708:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L710
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L710:
	movb	$117, -80(%rbp)
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L699:
	movq	464(%rdi), %rdi
	movl	$-1, %r14d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE78ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movzbl	472(%r12), %r13d
	testb	%r13b, %r13b
	je	.L711
	cmpb	$2, %r13b
	je	.L712
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L761
.L712:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L711:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L713
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L713:
	movb	$78, -80(%rbp)
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L698:
	movq	464(%rdi), %rdi
	movl	$-1, %r14d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE79ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movzbl	472(%r12), %r13d
	testb	%r13b, %r13b
	je	.L714
	cmpb	$2, %r13b
	je	.L715
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L762
.L715:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L714:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L716
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L716:
	movb	$79, -80(%rbp)
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L697:
	movq	464(%rdi), %rdi
	movl	$-1, %r14d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE76ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movzbl	472(%r12), %r13d
	testb	%r13b, %r13b
	je	.L701
	cmpb	$2, %r13b
	je	.L702
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L763
.L702:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L701:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L703
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L703:
	movb	$76, -80(%rbp)
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L695:
	movq	464(%rdi), %rdi
	movl	$-1, %r14d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE77ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movzbl	472(%r12), %r13d
	testb	%r13b, %r13b
	je	.L705
	cmpb	$2, %r13b
	je	.L706
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L764
.L706:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L705:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L707
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L707:
	movb	$77, -80(%rbp)
.L759:
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movb	%al, -52(%rbp)
	movb	%r13b, -48(%rbp)
	movl	%r14d, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$1, -56(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L765
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L694:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L764:
	movl	$77, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L706
	xorl	%r13d, %r13d
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L763:
	movl	$76, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L702
	xorl	%r13d, %r13d
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L762:
	movl	$79, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L715
	xorl	%r13d, %r13d
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L761:
	movl	$78, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L712
	xorl	%r13d, %r13d
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L760:
	movl	$117, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L709
	xorl	%r13d, %r13d
	jmp	.L708
.L765:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20139:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder14UnaryOperationENS0_5Token5ValueEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder14UnaryOperationENS0_5Token5ValueEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder10LogicalNotENS2_13ToBooleanModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder10LogicalNotENS2_13ToBooleanModeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder10LogicalNotENS2_13ToBooleanModeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder10LogicalNotENS2_13ToBooleanModeE:
.LFB20140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %esi
	je	.L800
	testq	%r13, %r13
	je	.L771
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L771:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	jne	.L801
	pxor	%xmm0, %xmm0
	movb	$80, -80(%rbp)
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movq	$0, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L774
	movl	484(%r12), %eax
	movb	%dl, -48(%rbp)
	movl	%eax, -44(%rbp)
.L775:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L774:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
.L770:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L802
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$80, -80(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	%al, -48(%rbp)
	movl	%ecx, -44(%rbp)
	movq	$0, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L774
	cmpb	$1, %al
	jne	.L775
	cmpb	$2, %dl
	jne	.L775
	movb	$2, -48(%rbp)
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L800:
	movq	%r13, %rdi
	movl	$-1, %ebx
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE81ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movzbl	472(%r12), %r13d
	testb	%r13b, %r13b
	je	.L768
	cmpb	$2, %r13b
	je	.L769
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L769
	movl	$81, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	jne	.L780
.L769:
	movl	476(%r12), %ebx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L768:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movb	$81, -80(%rbp)
	movb	%r13b, -48(%rbp)
	movl	%ebx, -44(%rbp)
	movq	$0, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L780:
	xorl	%r13d, %r13d
	jmp	.L768
.L802:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20140:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder10LogicalNotENS2_13ToBooleanModeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder10LogicalNotENS2_13ToBooleanModeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder6TypeOfEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder6TypeOfEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder6TypeOfEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder6TypeOfEv:
.LFB20141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L804
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L804:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	je	.L805
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$82, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L807
	cmpb	$1, %al
	jne	.L808
	cmpb	$2, %dl
	jne	.L808
	movb	$2, -32(%rbp)
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L805:
	pxor	%xmm0, %xmm0
	movb	$82, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L807
	movl	484(%r12), %eax
	movb	%dl, -32(%rbp)
	movl	%eax, -28(%rbp)
.L808:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L807:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L827
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L827:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20141:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder6TypeOfEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder6TypeOfEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder19GetSuperConstructorENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder19GetSuperConstructorENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder19GetSuperConstructorENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder19GetSuperConstructorENS1_8RegisterE:
.LFB20142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L829
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L829
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L829:
	movzbl	472(%r12), %ecx
	movl	$-5, %eax
	movl	$-1, %esi
	subl	%ebx, %eax
	testb	%cl, %cl
	je	.L830
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L830:
	leal	128(%rax), %edi
	movl	$1, %edx
	cmpl	$255, %edi
	jbe	.L831
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L831:
	movl	%eax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$85, -64(%rbp)
	movb	%dl, -36(%rbp)
	movb	%cl, -32(%rbp)
	movl	%esi, -28(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L832
	testb	%cl, %cl
	jne	.L833
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L834:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L832:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L856
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L833:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L834
	cmpb	$1, %cl
	jne	.L834
	movb	$2, -32(%rbp)
	jmp	.L834
.L856:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20142:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder19GetSuperConstructorENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder19GetSuperConstructorENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareOperationENS0_5Token5ValueENS1_8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareOperationENS0_5Token5ValueENS1_8RegisterEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareOperationENS0_5Token5ValueENS1_8RegisterEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareOperationENS0_5Token5ValueENS1_8RegisterEi:
.LFB20143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$53, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$9, %sil
	ja	.L858
	movl	%ecx, %ebx
	movzbl	%sil, %esi
	leaq	.L860(%rip), %rcx
	movl	%edx, %r13d
	movl	%edx, %r14d
	movslq	(%rcx,%rsi,4), %rdx
	movq	%rdi, %r12
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareOperationENS0_5Token5ValueENS1_8RegisterEi,"a",@progbits
	.align 4
	.align 4
.L860:
	.long	.L867-.L860
	.long	.L866-.L860
	.long	.L858-.L860
	.long	.L858-.L860
	.long	.L865-.L860
	.long	.L864-.L860
	.long	.L863-.L860
	.long	.L862-.L860
	.long	.L861-.L860
	.long	.L859-.L860
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareOperationENS0_5Token5ValueENS1_8RegisterEi
	.p2align 4,,10
	.p2align 3
.L859:
	movq	464(%rdi), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE111ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L911
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L911:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	je	.L912
	cmpb	$2, %r14b
	je	.L913
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L913
	movl	$111, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L913
	xorl	%r14d, %r14d
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L867:
	movq	464(%rdi), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE103ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L868
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L868:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	je	.L869
	cmpb	$2, %r14b
	je	.L870
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L870
	movl	$103, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L870
	xorl	%r14d, %r14d
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L864:
	movq	464(%rdi), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE106ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L887
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L887:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	je	.L888
	cmpb	$2, %r14b
	je	.L889
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L889
	movl	$106, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L889
	xorl	%r14d, %r14d
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L863:
	movq	464(%rdi), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE107ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L893
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L893:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	je	.L894
	cmpb	$2, %r14b
	je	.L895
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L895
	movl	$107, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L895
	xorl	%r14d, %r14d
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L866:
	movq	464(%rdi), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE104ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L875
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L875:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	je	.L876
	cmpb	$2, %r14b
	je	.L877
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L877
	movl	$104, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L877
	xorl	%r14d, %r14d
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L865:
	movq	464(%rdi), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE105ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L881
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L881:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	je	.L882
	cmpb	$2, %r14b
	je	.L883
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L883
	movl	$105, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L883
	xorl	%r14d, %r14d
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L862:
	movq	464(%rdi), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE108ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L899
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L899:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	je	.L900
	cmpb	$2, %r14b
	je	.L901
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L901
	movl	$108, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L901
	xorl	%r14d, %r14d
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L861:
	movq	464(%rdi), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE110ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L905
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r14d
.L905:
	movl	$-5, %r13d
	movl	$-1, %r15d
	subl	%r14d, %r13d
	movzbl	472(%r12), %r14d
	testb	%r14b, %r14b
	je	.L906
	cmpb	$2, %r14b
	je	.L907
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L907
	movl	$110, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L907
	xorl	%r14d, %r14d
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L907:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L906:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L950
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L908:
	cmpl	$255, %ebx
	jbe	.L909
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L909:
	movb	$110, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L1015:
	movb	%al, -68(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$8589934592, %rax
	movq	%rax, -76(%rbp)
	movb	%r14b, -64(%rbp)
	movl	%r15d, -60(%rbp)
	movl	%r13d, -92(%rbp)
	movl	%ebx, -88(%rbp)
	movq	$0, -84(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1016
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L901:
	.cfi_restore_state
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L900:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L945
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L902:
	cmpl	$255, %ebx
	jbe	.L903
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L903:
	movb	$108, -96(%rbp)
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L889:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L888:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L935
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L890:
	cmpl	$255, %ebx
	jbe	.L891
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L891:
	movb	$106, -96(%rbp)
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L877:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L876:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L925
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L878:
	cmpl	$255, %ebx
	jbe	.L879
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L879:
	movb	$104, -96(%rbp)
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L913:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L912:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L955
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L914:
	cmpl	$255, %ebx
	jbe	.L915
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L915:
	movb	$111, -96(%rbp)
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L895:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L894:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L940
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L896:
	cmpl	$255, %ebx
	jbe	.L897
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L897:
	movb	$107, -96(%rbp)
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L870:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L869:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L920
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L871:
	cmpl	$255, %ebx
	jbe	.L872
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L872:
	movb	$103, -96(%rbp)
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L883:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L882:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L930
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L884:
	cmpl	$255, %ebx
	jbe	.L885
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L885:
	movb	$105, -96(%rbp)
	jmp	.L1015
.L858:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L930:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L920:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L940:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L955:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L925:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L935:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L945:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L950:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L908
.L1016:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20143:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareOperationENS0_5Token5ValueENS1_8RegisterEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareOperationENS0_5Token5ValueENS1_8RegisterEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareReferenceENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareReferenceENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareReferenceENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareReferenceENS1_8RegisterE:
.LFB20144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L1018
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r14), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1018
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L1018:
	movzbl	472(%r12), %ecx
	movl	$-5, %eax
	movl	$-1, %esi
	subl	%r13d, %eax
	testb	%cl, %cl
	je	.L1019
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1019:
	leal	128(%rax), %edi
	movl	$1, %edx
	cmpl	$255, %edi
	jbe	.L1020
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L1020:
	movl	%eax, -76(%rbp)
	movzbl	480(%r12), %eax
	movb	$109, -80(%rbp)
	movb	%dl, -52(%rbp)
	movb	%cl, -48(%rbp)
	movl	%esi, -44(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$1, -56(%rbp)
	testb	%al, %al
	je	.L1021
	testb	%cl, %cl
	jne	.L1022
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L1023:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1021:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1045
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1022:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L1023
	cmpb	$1, %cl
	jne	.L1023
	movb	$2, -48(%rbp)
	jmp	.L1023
.L1045:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20144:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareReferenceENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareReferenceENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder19CompareUndetectableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CompareUndetectableEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CompareUndetectableEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder19CompareUndetectableEv:
.LFB20145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L1047
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1047:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L1048
	cmpb	$2, %al
	je	.L1049
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1048
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	$112, -64(%rbp)
	movb	$0, 472(%r12)
	movl	%edx, -28(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L1051
	cmpb	$1, %al
	jne	.L1052
	cmpb	$2, %dl
	jne	.L1052
	movb	$2, -32(%rbp)
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1048:
	movzbl	480(%r12), %eax
	pxor	%xmm0, %xmm0
	movb	$112, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%al, %al
	je	.L1051
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1052:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1051:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1074
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1049:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	$112, -64(%rbp)
	movb	$2, -32(%rbp)
	movl	%eax, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	jne	.L1052
	jmp	.L1051
.L1074:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20145:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CompareUndetectableEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder19CompareUndetectableEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareUndefinedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareUndefinedEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareUndefinedEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareUndefinedEv:
.LFB20146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L1076
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1076:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L1077
	cmpb	$2, %al
	je	.L1078
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1077
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	$114, -64(%rbp)
	movb	$0, 472(%r12)
	movl	%edx, -28(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L1080
	cmpb	$1, %al
	jne	.L1081
	cmpb	$2, %dl
	jne	.L1081
	movb	$2, -32(%rbp)
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1077:
	movzbl	480(%r12), %eax
	pxor	%xmm0, %xmm0
	movb	$114, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%al, %al
	je	.L1080
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1081:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1080:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1103
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1078:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	$114, -64(%rbp)
	movb	$2, -32(%rbp)
	movl	%eax, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	jne	.L1081
	jmp	.L1080
.L1103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20146:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareUndefinedEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder16CompareUndefinedEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11CompareNullEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CompareNullEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CompareNullEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11CompareNullEv:
.LFB20147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L1105
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1105:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L1106
	cmpb	$2, %al
	je	.L1107
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1106
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	$113, -64(%rbp)
	movb	$0, 472(%r12)
	movl	%edx, -28(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L1109
	cmpb	$1, %al
	jne	.L1110
	cmpb	$2, %dl
	jne	.L1110
	movb	$2, -32(%rbp)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1106:
	movzbl	480(%r12), %eax
	pxor	%xmm0, %xmm0
	movb	$113, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%al, %al
	je	.L1109
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1110:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1109:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1132
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1107:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	$113, -64(%rbp)
	movb	$2, -32(%rbp)
	movl	%eax, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	jne	.L1110
	jmp	.L1109
.L1132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20147:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CompareNullEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11CompareNullEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder10CompareNilENS0_5Token5ValueENS2_8NilValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder10CompareNilENS0_5Token5ValueENS2_8NilValueE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder10CompareNilENS0_5Token5ValueENS2_8NilValueE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder10CompareNilENS0_5Token5ValueENS2_8NilValueE:
.LFB20148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$53, %sil
	je	.L1192
	movq	464(%rdi), %r13
	cmpl	$1, %edx
	je	.L1193
	testq	%r13, %r13
	je	.L1143
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1143:
	movzbl	472(%r12), %eax
	testb	%al, %al
	jne	.L1194
.L1144:
	movb	$113, -64(%rbp)
.L1191:
	movzbl	480(%r12), %eax
	pxor	%xmm0, %xmm0
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%al, %al
	je	.L1147
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1148:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1147:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	%r12, %rax
.L1133:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1195
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1194:
	.cfi_restore_state
	cmpb	$2, %al
	je	.L1145
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1144
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	$113, -64(%rbp)
	movb	$0, 472(%r12)
	movl	%edx, -28(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L1147
	cmpb	$2, %dl
	jne	.L1148
	cmpb	$1, %al
	jne	.L1148
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1193:
	testq	%r13, %r13
	je	.L1137
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1137:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L1138
	cmpb	$2, %al
	je	.L1139
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1138
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	$114, -64(%rbp)
	movb	$0, 472(%r12)
	movl	%edx, -28(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L1147
	cmpb	$1, %al
	jne	.L1148
	cmpb	$2, %dl
	jne	.L1148
.L1187:
	movb	$2, -32(%rbp)
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1192:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CompareUndetectableEv
	jmp	.L1133
	.p2align 4,,10
	.p2align 3
.L1138:
	movb	$114, -64(%rbp)
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1145:
	movl	476(%r12), %eax
	movb	$113, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1189:
	cmpb	$0, 480(%r12)
	pxor	%xmm0, %xmm0
	movb	$2, -32(%rbp)
	movl	%eax, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	jne	.L1148
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L1139:
	movl	476(%r12), %eax
	movb	$114, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L1189
.L1195:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20148:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder10CompareNilENS0_5Token5ValueENS2_8NilValueE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder10CompareNilENS0_5Token5ValueENS2_8NilValueE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder13CompareTypeOfENS1_15TestTypeOfFlags11LiteralFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CompareTypeOfENS1_15TestTypeOfFlags11LiteralFlagE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CompareTypeOfENS1_15TestTypeOfFlags11LiteralFlagE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder13CompareTypeOfENS1_15TestTypeOfFlags11LiteralFlagE:
.LFB20149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	%esi, %edi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter15TestTypeOfFlags6EncodeENS2_11LiteralFlagE@PLT
	movq	464(%r12), %r13
	movzbl	%al, %ebx
	testq	%r13, %r13
	je	.L1197
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1197:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L1198
	cmpb	$2, %al
	je	.L1199
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1198
.L1199:
	movl	476(%r12), %edx
	movb	$115, -80(%rbp)
	movb	$0, 472(%r12)
	movl	%edx, -44(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	%al, -48(%rbp)
	movl	%ebx, -76(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$1, -56(%rbp)
	movb	$1, -52(%rbp)
	testb	%dl, %dl
	je	.L1201
	cmpb	$1, %al
	jne	.L1202
	cmpb	$2, %dl
	jne	.L1202
	movb	$2, -48(%rbp)
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1198:
	movzbl	480(%r12), %eax
	movb	$115, -80(%rbp)
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$1, -56(%rbp)
	movb	$1, -52(%rbp)
	testb	%al, %al
	je	.L1201
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L1202:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1201:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1227
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1227:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20149:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CompareTypeOfENS1_15TestTypeOfFlags11LiteralFlagE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder13CompareTypeOfENS1_15TestTypeOfFlags11LiteralFlagE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadConstantPoolEntryEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadConstantPoolEntryEm
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadConstantPoolEntryEm, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadConstantPoolEntryEm:
.LFB20150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1229
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1229:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L1230
	cmpb	$2, %dl
	je	.L1231
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1238
.L1231:
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1230:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L1232
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1232:
	movb	%al, -36(%rbp)
	movzbl	480(%r12), %eax
	movb	$18, -64(%rbp)
	movb	%dl, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movl	%ebx, -60(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L1233
	testb	%dl, %dl
	jne	.L1234
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1235:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1233:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1258
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1234:
	.cfi_restore_state
	cmpb	$1, %dl
	jne	.L1235
	cmpb	$2, %al
	jne	.L1235
	movb	$2, -32(%rbp)
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1238:
	xorl	%edx, %edx
	jmp	.L1230
.L1258:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20150:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadConstantPoolEntryEm, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadConstantPoolEntryEm
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE:
.LFB20151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	sarq	$32, %rbx
	jne	.L1260
	testq	%rdi, %rdi
	je	.L1261
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1261:
	movzbl	472(%r12), %ebx
	movl	$-1, %r13d
	testb	%bl, %bl
	jne	.L1291
.L1262:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movb	$11, -80(%rbp)
	movb	%bl, -48(%rbp)
	movl	%r13d, -44(%rbp)
	movq	$0, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
.L1264:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1292
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1260:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L1265
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1265:
	movzbl	472(%r12), %r13d
	movl	$-1, %r14d
	testb	%r13b, %r13b
	je	.L1266
	cmpb	$2, %r13b
	je	.L1267
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L1267
	movl	$12, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	jne	.L1273
.L1267:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1266:
	leal	128(%rbx), %edx
	movl	$1, %eax
	cmpl	$255, %edx
	jbe	.L1268
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1268:
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movb	$12, -80(%rbp)
	movb	%al, -52(%rbp)
	movb	%r13b, -48(%rbp)
	movl	%r14d, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$1, -56(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1291:
	cmpb	$2, %bl
	je	.L1263
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L1263
	movl	$11, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	jne	.L1271
.L1263:
	movl	476(%r12), %r13d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L1262
	.p2align 4,,10
	.p2align 3
.L1271:
	xorl	%ebx, %ebx
	jmp	.L1262
	.p2align 4,,10
	.p2align 3
.L1273:
	xorl	%r13d, %r13d
	jmp	.L1266
.L1292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20151:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_3SmiE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEd
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEd, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEd:
.LFB20152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEd@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1294
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1294:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L1295
	cmpb	$2, %dl
	je	.L1296
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1303
.L1296:
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1295:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L1297
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1297:
	movb	%al, -36(%rbp)
	movzbl	480(%r12), %eax
	movb	$18, -64(%rbp)
	movb	%dl, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movl	%ebx, -60(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L1298
	testb	%dl, %dl
	jne	.L1299
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1300:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1298:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1323
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1299:
	.cfi_restore_state
	cmpb	$1, %dl
	jne	.L1300
	cmpb	$2, %al
	jne	.L1300
	movb	$2, -32(%rbp)
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1303:
	xorl	%edx, %edx
	jmp	.L1295
.L1323:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20152:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEd, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEd
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE:
.LFB20153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1325
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1325:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L1326
	cmpb	$2, %dl
	je	.L1327
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1334
.L1327:
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1326:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L1328
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1328:
	movb	%al, -36(%rbp)
	movzbl	480(%r12), %eax
	movb	$18, -64(%rbp)
	movb	%dl, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movl	%ebx, -60(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L1329
	testb	%dl, %dl
	jne	.L1330
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1331:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1329:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1354
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1330:
	.cfi_restore_state
	cmpb	$1, %dl
	jne	.L1331
	cmpb	$2, %al
	jne	.L1331
	movb	$2, -32(%rbp)
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1334:
	xorl	%edx, %edx
	jmp	.L1326
.L1354:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20153:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_5ScopeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_5ScopeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_5ScopeE:
.LFB20154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_5ScopeE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1356
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1356:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L1357
	cmpb	$2, %dl
	je	.L1358
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1365
.L1358:
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1357:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L1359
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1359:
	movb	%al, -36(%rbp)
	movzbl	480(%r12), %eax
	movb	$18, -64(%rbp)
	movb	%dl, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movl	%ebx, -60(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L1360
	testb	%dl, %dl
	jne	.L1361
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1362:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1360:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1385
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1361:
	.cfi_restore_state
	cmpb	$1, %dl
	jne	.L1362
	cmpb	$2, %al
	jne	.L1362
	movb	$2, -32(%rbp)
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1365:
	xorl	%edx, %edx
	jmp	.L1357
.L1385:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20154:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_5ScopeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralEPKNS0_5ScopeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstBigIntE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstBigIntE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstBigIntE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstBigIntE:
.LFB20155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_9AstBigIntE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1387
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1387:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L1388
	cmpb	$2, %dl
	je	.L1389
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1396
.L1389:
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1388:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L1390
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1390:
	movb	%al, -36(%rbp)
	movzbl	480(%r12), %eax
	movb	$18, -64(%rbp)
	movb	%dl, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movl	%ebx, -60(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L1391
	testb	%dl, %dl
	jne	.L1392
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1393:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1391:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1416
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1392:
	.cfi_restore_state
	cmpb	$1, %dl
	jne	.L1393
	cmpb	$2, %al
	jne	.L1393
	movb	$2, -32(%rbp)
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1396:
	xorl	%edx, %edx
	jmp	.L1388
.L1416:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20155:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstBigIntE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstBigIntE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstSymbolE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstSymbolE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstSymbolE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstSymbolE:
.LFB20156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	%sil, %sil
	je	.L1448
.L1418:
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1419
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1419:
	movzbl	472(%r12), %ecx
	movl	%ebx, %eax
	movl	$-1, %esi
	testb	%cl, %cl
	je	.L1420
	cmpb	$2, %cl
	je	.L1421
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1428
.L1421:
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1420:
	movl	$1, %edx
	cmpl	$255, %eax
	jbe	.L1422
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L1422:
	movl	%eax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$18, -64(%rbp)
	movb	%dl, -36(%rbp)
	movb	%cl, -32(%rbp)
	movl	%esi, -28(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L1423
	testb	%cl, %cl
	jne	.L1424
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1425:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1423:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1449
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1424:
	.cfi_restore_state
	cmpb	$1, %cl
	jne	.L1425
	cmpb	$2, %al
	jne	.L1425
	movb	$2, -32(%rbp)
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1448:
	leaq	24(%rdi), %rdi
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder22InsertHomeObjectSymbolEv@PLT
	movq	%rax, %rbx
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1428:
	xorl	%ecx, %ecx
	jmp	.L1420
.L1449:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20156:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstSymbolE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadLiteralENS0_9AstSymbolE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv:
.LFB20157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1451
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1451:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L1452
	cmpb	$2, %al
	je	.L1453
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1452
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	$13, -64(%rbp)
	movb	$0, 472(%r12)
	movl	%edx, -28(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L1455
	cmpb	$1, %al
	jne	.L1456
	cmpb	$2, %dl
	jne	.L1456
	movb	$2, -32(%rbp)
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1452:
	movzbl	480(%r12), %eax
	pxor	%xmm0, %xmm0
	movb	$13, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%al, %al
	je	.L1455
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1456:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1455:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1478
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1453:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	$13, -64(%rbp)
	movb	$2, -32(%rbp)
	movl	%eax, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	jne	.L1456
	jmp	.L1455
.L1478:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20157:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder13LoadUndefinedEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadNullEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadNullEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadNullEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadNullEv:
.LFB20158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1480
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1480:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L1481
	cmpb	$2, %al
	je	.L1482
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1481
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	$14, -64(%rbp)
	movb	$0, 472(%r12)
	movl	%edx, -28(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L1484
	cmpb	$1, %al
	jne	.L1485
	cmpb	$2, %dl
	jne	.L1485
	movb	$2, -32(%rbp)
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1481:
	movzbl	480(%r12), %eax
	pxor	%xmm0, %xmm0
	movb	$14, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%al, %al
	je	.L1484
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1485:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1484:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1507
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1482:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	$14, -64(%rbp)
	movb	$2, -32(%rbp)
	movl	%eax, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	jne	.L1485
	jmp	.L1484
.L1507:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20158:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadNullEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadNullEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv:
.LFB20159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1509
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1509:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L1510
	cmpb	$2, %al
	je	.L1511
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1510
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	$15, -64(%rbp)
	movb	$0, 472(%r12)
	movl	%edx, -28(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L1513
	cmpb	$1, %al
	jne	.L1514
	cmpb	$2, %dl
	jne	.L1514
	movb	$2, -32(%rbp)
	jmp	.L1514
	.p2align 4,,10
	.p2align 3
.L1510:
	movzbl	480(%r12), %eax
	pxor	%xmm0, %xmm0
	movb	$15, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%al, %al
	je	.L1513
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1514:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1513:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1536
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1511:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	$15, -64(%rbp)
	movb	$2, -32(%rbp)
	movl	%eax, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	jne	.L1514
	jmp	.L1513
.L1536:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20159:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadTheHoleEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadTrueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadTrueEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadTrueEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadTrueEv:
.LFB20160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1538
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1538:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L1539
	cmpb	$2, %al
	je	.L1540
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1539
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	$16, -64(%rbp)
	movb	$0, 472(%r12)
	movl	%edx, -28(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L1542
	cmpb	$1, %al
	jne	.L1543
	cmpb	$2, %dl
	jne	.L1543
	movb	$2, -32(%rbp)
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1539:
	movzbl	480(%r12), %eax
	pxor	%xmm0, %xmm0
	movb	$16, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%al, %al
	je	.L1542
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1543:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1542:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1565
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1540:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	$16, -64(%rbp)
	movb	$2, -32(%rbp)
	movl	%eax, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	jne	.L1543
	jmp	.L1542
.L1565:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20160:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadTrueEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder8LoadTrueEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder9LoadFalseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder9LoadFalseEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder9LoadFalseEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder9LoadFalseEv:
.LFB20161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1567
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1567:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L1568
	cmpb	$2, %al
	je	.L1569
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1568
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	$17, -64(%rbp)
	movb	$0, 472(%r12)
	movl	%edx, -28(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L1571
	cmpb	$1, %al
	jne	.L1572
	cmpb	$2, %dl
	jne	.L1572
	movb	$2, -32(%rbp)
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1568:
	movzbl	480(%r12), %eax
	pxor	%xmm0, %xmm0
	movb	$17, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%al, %al
	je	.L1571
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1572:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1571:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1594
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1569:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	$17, -64(%rbp)
	movb	$2, -32(%rbp)
	movl	%eax, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	jne	.L1572
	jmp	.L1571
.L1594:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20161:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder9LoadFalseEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder9LoadFalseEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadBooleanEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadBooleanEb
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadBooleanEb, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadBooleanEb:
.LFB20162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	%sil, %sil
	je	.L1596
	testq	%rdi, %rdi
	je	.L1597
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1597:
	movzbl	472(%r12), %eax
	testb	%al, %al
	jne	.L1654
.L1598:
	movb	$16, -64(%rbp)
.L1653:
	movzbl	480(%r12), %eax
	pxor	%xmm0, %xmm0
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%al, %al
	je	.L1608
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L1609:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1608:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1655
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1596:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L1604
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1604:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L1605
	cmpb	$2, %al
	je	.L1606
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1605
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	$17, -64(%rbp)
	movb	$0, 472(%r12)
	movl	%edx, -28(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L1608
	cmpb	$2, %dl
	jne	.L1609
	cmpb	$1, %al
	jne	.L1609
.L1649:
	movb	$2, -32(%rbp)
	jmp	.L1609
	.p2align 4,,10
	.p2align 3
.L1605:
	movb	$17, -64(%rbp)
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1654:
	cmpb	$2, %al
	je	.L1599
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1598
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	$16, -64(%rbp)
	movb	$0, 472(%r12)
	movl	%edx, -28(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L1608
	cmpb	$1, %al
	jne	.L1609
	cmpb	$2, %dl
	jne	.L1609
	jmp	.L1649
	.p2align 4,,10
	.p2align 3
.L1606:
	movl	476(%r12), %eax
	movb	$17, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1651:
	cmpb	$0, 480(%r12)
	pxor	%xmm0, %xmm0
	movb	$2, -32(%rbp)
	movl	%eax, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	jne	.L1609
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1599:
	movl	476(%r12), %eax
	movb	$16, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L1651
.L1655:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20162:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadBooleanEb, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11LoadBooleanEb
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE:
.LFB20163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	464(%rdi), %rdi
	movzbl	472(%r12), %ebx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1657
	testb	%bl, %bl
	jne	.L1681
.L1658:
	movq	40(%rdi), %rax
	addl	64(%rdi), %esi
	movslq	%esi, %rsi
	movq	16(%rdi), %rdx
	movq	(%rax,%rsi,8), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16RegisterTransferEPNS2_12RegisterInfoES4_@PLT
.L1660:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1682
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1681:
	.cfi_restore_state
	cmpb	$2, %bl
	je	.L1659
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1658
.L1659:
	movl	476(%r12), %eax
	movb	%bl, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movl	%eax, 484(%r12)
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1657:
	movl	$-5, %r13d
	movl	$-1, %r14d
	subl	%esi, %r13d
	testb	%bl, %bl
	je	.L1661
	cmpb	$2, %bl
	je	.L1662
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1683
.L1662:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1661:
	leal	128(%r13), %edx
	movl	$1, %eax
	cmpl	$255, %edx
	jbe	.L1663
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1663:
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movb	$37, -80(%rbp)
	movb	%al, -52(%rbp)
	movb	%bl, -48(%rbp)
	movl	%r14d, -44(%rbp)
	movl	%r13d, -76(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$1, -56(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1683:
	movl	$37, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L1662
	xorl	%ebx, %ebx
	jmp	.L1661
.L1682:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20163:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadAccumulatorWithRegisterENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE:
.LFB20164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	464(%rdi), %rdi
	movzbl	472(%r12), %ebx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1685
	testb	%bl, %bl
	jne	.L1709
.L1686:
	movq	40(%rdi), %rax
	addl	64(%rdi), %esi
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %rdx
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16RegisterTransferEPNS2_12RegisterInfoES4_@PLT
.L1688:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1710
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1709:
	.cfi_restore_state
	cmpb	$2, %bl
	je	.L1687
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1686
.L1687:
	movl	476(%r12), %eax
	movb	%bl, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movl	%eax, 484(%r12)
	jmp	.L1686
	.p2align 4,,10
	.p2align 3
.L1685:
	movl	$-5, %r13d
	movl	$-1, %r14d
	subl	%esi, %r13d
	testb	%bl, %bl
	je	.L1689
	cmpb	$2, %bl
	je	.L1690
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1711
.L1690:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1689:
	leal	128(%r13), %edx
	movl	$1, %eax
	cmpl	$255, %edx
	jbe	.L1691
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1691:
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movb	$38, -80(%rbp)
	movb	%al, -52(%rbp)
	movb	%bl, -48(%rbp)
	movl	%r14d, -44(%rbp)
	movl	%r13d, -76(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$1, -56(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1711:
	movl	$38, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L1690
	xorl	%ebx, %ebx
	jmp	.L1689
.L1710:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20164:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreAccumulatorInRegisterENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_:
.LFB20165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	464(%rdi), %rdi
	movzbl	472(%r12), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1713
	testb	%bl, %bl
	jne	.L1740
.L1714:
	movl	64(%rdi), %eax
	movq	40(%rdi), %rcx
	addl	%eax, %edx
	addl	%esi, %eax
	movslq	%edx, %rdx
	cltq
	movq	(%rcx,%rdx,8), %rdx
	movq	(%rcx,%rax,8), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16RegisterTransferEPNS2_12RegisterInfoES4_@PLT
.L1716:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1741
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1740:
	.cfi_restore_state
	cmpb	$2, %bl
	je	.L1715
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1714
.L1715:
	movl	476(%r12), %eax
	movb	%bl, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movl	%eax, 484(%r12)
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1713:
	movl	$-5, %r13d
	movl	$-1, %r15d
	movl	%r13d, %r14d
	subl	%esi, %r13d
	subl	%edx, %r14d
	testb	%bl, %bl
	je	.L1717
	cmpb	$2, %bl
	je	.L1718
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L1742
.L1718:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1717:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L1725
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1719:
	leal	128(%r14), %ecx
	cmpl	$255, %ecx
	jbe	.L1720
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%edx, %eax
.L1720:
	movb	%al, -68(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$8589934592, %rax
	movb	$39, -96(%rbp)
	movb	%bl, -64(%rbp)
	movl	%r15d, -60(%rbp)
	movl	%r13d, -92(%rbp)
	movl	%r14d, -88(%rbp)
	movq	$0, -84(%rbp)
	movq	%rax, -76(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L1725:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L1719
	.p2align 4,,10
	.p2align 3
.L1742:
	movl	$39, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L1718
	xorl	%ebx, %ebx
	jmp	.L1717
.L1741:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20165:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder12MoveRegisterENS1_8RegisterES3_
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder10LoadGlobalEPKNS0_12AstRawStringEiNS0_10TypeofModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder10LoadGlobalEPKNS0_12AstRawStringEiNS0_10TypeofModeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder10LoadGlobalEPKNS0_12AstRawStringEiNS0_10TypeofModeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder10LoadGlobalEPKNS0_12AstRawStringEiNS0_10TypeofModeE:
.LFB20166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testl	%r14d, %r14d
	jne	.L1744
	testq	%rdi, %rdi
	je	.L1745
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1745:
	movzbl	472(%r12), %r14d
	movl	$-1, %r15d
	testb	%r14b, %r14b
	je	.L1746
	cmpb	$2, %r14b
	je	.L1747
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L1747
	movl	$20, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	jne	.L1760
.L1747:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1746:
	cmpl	$255, %ebx
	jbe	.L1761
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1748:
	cmpl	$255, %r13d
	jbe	.L1749
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%edx, %eax
.L1749:
	movb	$20, -96(%rbp)
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1744:
	testq	%rdi, %rdi
	je	.L1752
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1752:
	movzbl	472(%r12), %r14d
	movl	$-1, %r15d
	testb	%r14b, %r14b
	je	.L1753
	cmpb	$2, %r14b
	je	.L1754
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L1754
	movl	$19, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	jne	.L1765
.L1754:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1753:
	cmpl	$255, %ebx
	jbe	.L1766
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1755:
	cmpl	$255, %r13d
	jbe	.L1756
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%edx, %eax
.L1756:
	movb	$19, -96(%rbp)
.L1784:
	movb	%al, -68(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$8589934592, %rax
	movq	%rax, -76(%rbp)
	movb	%r14b, -64(%rbp)
	movl	%r15d, -60(%rbp)
	movl	%ebx, -92(%rbp)
	movl	%r13d, -88(%rbp)
	movq	$0, -84(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1785
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1766:
	.cfi_restore_state
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L1755
	.p2align 4,,10
	.p2align 3
.L1761:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L1748
	.p2align 4,,10
	.p2align 3
.L1765:
	xorl	%r14d, %r14d
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1760:
	xorl	%r14d, %r14d
	jmp	.L1746
.L1785:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20166:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder10LoadGlobalEPKNS0_12AstRawStringEiNS0_10TypeofModeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder10LoadGlobalEPKNS0_12AstRawStringEiNS0_10TypeofModeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11StoreGlobalEPKNS0_12AstRawStringEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11StoreGlobalEPKNS0_12AstRawStringEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11StoreGlobalEPKNS0_12AstRawStringEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11StoreGlobalEPKNS0_12AstRawStringEi:
.LFB20167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L1787
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L1787:
	movzbl	472(%r12), %edx
	movl	$-1, %esi
	testb	%dl, %dl
	je	.L1788
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1788:
	cmpl	$255, %ebx
	jbe	.L1797
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
.L1789:
	cmpl	$255, %r13d
	jbe	.L1790
	cmpl	$65536, %r13d
	movl	$4, %ecx
	cmovb	%eax, %ecx
.L1790:
	movabsq	$8589934592, %rax
	movb	$21, -80(%rbp)
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	%cl, -52(%rbp)
	movb	%dl, -48(%rbp)
	movl	%esi, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movl	%r13d, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L1792
	testb	%dl, %dl
	jne	.L1793
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L1794:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L1792:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1814
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1793:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L1794
	cmpb	$1, %dl
	jne	.L1794
	movb	$2, -48(%rbp)
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L1797:
	movl	$2, %eax
	movl	$1, %ecx
	jmp	.L1789
.L1814:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20167:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11StoreGlobalEPKNS0_12AstRawStringEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11StoreGlobalEPKNS0_12AstRawStringEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder15LoadContextSlotENS1_8RegisterEiiNS2_21ContextSlotMutabilityE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder15LoadContextSlotENS1_8RegisterEiiNS2_21ContextSlotMutabilityE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder15LoadContextSlotENS1_8RegisterEiiNS2_21ContextSlotMutabilityE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder15LoadContextSlotENS1_8RegisterEiiNS2_21ContextSlotMutabilityE:
.LFB20168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-100(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$72, %rsp
	movl	%esi, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	testl	%ebx, %ebx
	jne	.L1816
	testb	%al, %al
	jne	.L1900
.L1816:
	movl	-100(%rbp), %r15d
	movq	464(%r12), %rdi
	testl	%r14d, %r14d
	jne	.L1901
	testq	%rdi, %rdi
	je	.L1829
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1829
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r15d
.L1829:
	movl	$-5, %r14d
	movl	$-1, %ecx
	subl	%r15d, %r14d
	movzbl	472(%r12), %r15d
	testb	%r15b, %r15b
	jne	.L1902
.L1830:
	leal	128(%r14), %eax
	cmpl	$255, %eax
	jbe	.L1857
.L1906:
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1832:
	cmpl	$255, %r13d
	jbe	.L1833
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%edx, %eax
.L1833:
	cmpl	$255, %ebx
	jbe	.L1835
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L1835:
	movb	$25, -96(%rbp)
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L1901:
	testq	%rdi, %rdi
	je	.L1838
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1838
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r15d
.L1838:
	movl	$-5, %r14d
	movl	$-1, %ecx
	subl	%r15d, %r14d
	movzbl	472(%r12), %r15d
	testb	%r15b, %r15b
	jne	.L1903
.L1839:
	leal	128(%r14), %eax
	cmpl	$255, %eax
	jbe	.L1862
.L1907:
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1841:
	cmpl	$255, %r13d
	jbe	.L1842
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%edx, %eax
.L1842:
	cmpl	$255, %ebx
	jbe	.L1844
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L1844:
	movb	$24, -96(%rbp)
.L1899:
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movb	%al, -68(%rbp)
	movb	%r15b, -64(%rbp)
	movl	%ecx, -60(%rbp)
	movl	%r14d, -92(%rbp)
	movl	%r13d, -88(%rbp)
	movl	%ebx, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
.L1824:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1904
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1900:
	.cfi_restore_state
	movq	464(%r12), %rdi
	testl	%r14d, %r14d
	jne	.L1905
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE27ELNS1_14AccumulatorUseE2EEEvv.isra.0
	movl	$-1, %r14d
	movzbl	472(%r12), %ebx
	testb	%bl, %bl
	je	.L1821
	cmpb	$2, %bl
	je	.L1822
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L1822
	movl	$27, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L1822
	xorl	%ebx, %ebx
	jmp	.L1821
	.p2align 4,,10
	.p2align 3
.L1902:
	movl	%ecx, -104(%rbp)
	cmpb	$2, %r15b
	je	.L1831
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L1831
	movl	$25, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movl	$-1, %ecx
	testb	%al, %al
	jne	.L1856
.L1831:
	leal	128(%r14), %eax
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	cmpl	$255, %eax
	ja	.L1906
.L1857:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L1832
	.p2align 4,,10
	.p2align 3
.L1903:
	movl	%ecx, -104(%rbp)
	cmpb	$2, %r15b
	je	.L1840
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L1840
	movl	$24, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movl	$-1, %ecx
	testb	%al, %al
	jne	.L1861
.L1840:
	leal	128(%r14), %eax
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	cmpl	$255, %eax
	ja	.L1907
.L1862:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L1841
	.p2align 4,,10
	.p2align 3
.L1905:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE26ELNS1_14AccumulatorUseE2EEEvv.isra.0
	movl	$-1, %r14d
	movzbl	472(%r12), %ebx
	testb	%bl, %bl
	je	.L1825
	cmpb	$2, %bl
	je	.L1826
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L1826
	movl	$26, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L1826
	xorl	%ebx, %ebx
	jmp	.L1825
	.p2align 4,,10
	.p2align 3
.L1822:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1821:
	movl	$1, %eax
	cmpl	$255, %r13d
	jbe	.L1823
	cmpl	$65536, %r13d
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1823:
	movb	$27, -96(%rbp)
.L1898:
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movb	%al, -68(%rbp)
	movb	%bl, -64(%rbp)
	movl	%r14d, -60(%rbp)
	movl	%r13d, -92(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movl	$1, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	jmp	.L1824
	.p2align 4,,10
	.p2align 3
.L1826:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1825:
	movl	$1, %eax
	cmpl	$255, %r13d
	jbe	.L1827
	cmpl	$65536, %r13d
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1827:
	movb	$26, -96(%rbp)
	jmp	.L1898
	.p2align 4,,10
	.p2align 3
.L1861:
	xorl	%r15d, %r15d
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1856:
	xorl	%r15d, %r15d
	jmp	.L1830
.L1904:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20168:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder15LoadContextSlotENS1_8RegisterEiiNS2_21ContextSlotMutabilityE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder15LoadContextSlotENS1_8RegisterEiiNS2_21ContextSlotMutabilityE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder16StoreContextSlotENS1_8RegisterEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder16StoreContextSlotENS1_8RegisterEii
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder16StoreContextSlotENS1_8RegisterEii, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder16StoreContextSlotENS1_8RegisterEii:
.LFB20169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-100(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$72, %rsp
	movl	%esi, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	testl	%ebx, %ebx
	jne	.L1909
	testb	%al, %al
	jne	.L1954
.L1909:
	movq	464(%r12), %rdi
	movl	-100(%rbp), %r15d
	testq	%rdi, %rdi
	je	.L1915
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1915
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r15d
.L1915:
	movl	$-5, %r14d
	movl	$-1, %ecx
	subl	%r15d, %r14d
	movzbl	472(%r12), %r15d
	testb	%r15b, %r15b
	jne	.L1955
.L1919:
	leal	128(%r14), %eax
	cmpl	$255, %eax
	jbe	.L1932
.L1957:
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1921:
	cmpl	$255, %r13d
	jbe	.L1922
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%edx, %eax
.L1922:
	cmpl	$255, %ebx
	jbe	.L1924
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L1924:
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movb	$28, -96(%rbp)
	movb	%al, -68(%rbp)
	movb	%r15b, -64(%rbp)
	movl	%ecx, -60(%rbp)
	movl	%r14d, -92(%rbp)
	movl	%r13d, -88(%rbp)
	movl	%ebx, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
.L1918:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1956
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1955:
	.cfi_restore_state
	movl	%ecx, -104(%rbp)
	cmpb	$2, %r15b
	je	.L1920
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L1920
	movl	$28, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movl	$-1, %ecx
	testb	%al, %al
	je	.L1920
	xorl	%r15d, %r15d
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L1954:
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1910
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L1910:
	movzbl	472(%r12), %ebx
	movl	$-1, %edx
	testb	%bl, %bl
	je	.L1912
	cmpb	$2, %bl
	je	.L1916
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L1916
	movl	$29, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L1916
	movl	$-1, %edx
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1912:
	movl	$1, %eax
	cmpl	$255, %r13d
	jbe	.L1917
	cmpl	$65536, %r13d
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1917:
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movb	$29, -96(%rbp)
	movb	%al, -68(%rbp)
	movb	%bl, -64(%rbp)
	movl	%edx, -60(%rbp)
	movl	%r13d, -92(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movl	$1, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	jmp	.L1918
	.p2align 4,,10
	.p2align 3
.L1920:
	leal	128(%r14), %eax
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	cmpl	$255, %eax
	ja	.L1957
.L1932:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L1921
	.p2align 4,,10
	.p2align 3
.L1916:
	movl	476(%r12), %edx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L1912
.L1956:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20169:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder16StoreContextSlotENS1_8RegisterEii, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder16StoreContextSlotENS1_8RegisterEii
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder14LoadLookupSlotEPKNS0_12AstRawStringENS0_10TypeofModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder14LoadLookupSlotEPKNS0_12AstRawStringENS0_10TypeofModeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder14LoadLookupSlotEPKNS0_12AstRawStringENS0_10TypeofModeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder14LoadLookupSlotEPKNS0_12AstRawStringENS0_10TypeofModeE:
.LFB20170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testl	%r13d, %r13d
	jne	.L1959
	testq	%rdi, %rdi
	je	.L1960
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1960:
	movzbl	472(%r12), %r13d
	movl	$-1, %r14d
	testb	%r13b, %r13b
	je	.L1961
	cmpb	$2, %r13b
	je	.L1962
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L1962
	movl	$33, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	jne	.L1971
.L1962:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1961:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L1963
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1963:
	movb	$33, -80(%rbp)
.L1993:
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movb	%al, -52(%rbp)
	movb	%r13b, -48(%rbp)
	movl	%r14d, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$1, -56(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1994
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1959:
	.cfi_restore_state
	testq	%rdi, %rdi
	je	.L1965
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1965:
	movzbl	472(%r12), %r13d
	movl	$-1, %r14d
	testb	%r13b, %r13b
	je	.L1966
	cmpb	$2, %r13b
	je	.L1967
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L1967
	movl	$30, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	jne	.L1975
.L1967:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1966:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L1968
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L1968:
	movb	$30, -80(%rbp)
	jmp	.L1993
	.p2align 4,,10
	.p2align 3
.L1971:
	xorl	%r13d, %r13d
	jmp	.L1961
	.p2align 4,,10
	.p2align 3
.L1975:
	xorl	%r13d, %r13d
	jmp	.L1966
.L1994:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20170:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder14LoadLookupSlotEPKNS0_12AstRawStringENS0_10TypeofModeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder14LoadLookupSlotEPKNS0_12AstRawStringENS0_10TypeofModeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadLookupContextSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadLookupContextSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadLookupContextSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadLookupContextSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii:
.LFB20171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testl	%r15d, %r15d
	jne	.L1996
	testq	%rdi, %rdi
	je	.L1997
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L1997:
	movzbl	472(%r12), %r15d
	movl	$-1, %ecx
	testb	%r15b, %r15b
	je	.L1998
	movl	%ecx, -100(%rbp)
	cmpb	$2, %r15b
	je	.L1999
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L1999
	movl	$34, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movl	$-1, %ecx
	testb	%al, %al
	jne	.L2016
.L1999:
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L1998:
	cmpl	$255, %ebx
	jbe	.L2017
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2000:
	cmpl	$255, %r14d
	jbe	.L2001
	cmpl	$65536, %r14d
	movl	$4, %eax
	cmovb	%edx, %eax
.L2001:
	cmpl	$255, %r13d
	jbe	.L2003
	cmpl	$65536, %r13d
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L2003:
	movb	$34, -96(%rbp)
	jmp	.L2042
	.p2align 4,,10
	.p2align 3
.L1996:
	testq	%rdi, %rdi
	je	.L2006
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2006:
	movzbl	472(%r12), %r15d
	movl	$-1, %ecx
	testb	%r15b, %r15b
	je	.L2007
	movl	%ecx, -100(%rbp)
	cmpb	$2, %r15b
	je	.L2008
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L2008
	movl	$31, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movl	$-1, %ecx
	testb	%al, %al
	jne	.L2022
.L2008:
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2007:
	cmpl	$255, %ebx
	jbe	.L2023
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2009:
	cmpl	$255, %r14d
	jbe	.L2010
	cmpl	$65536, %r14d
	movl	$4, %eax
	cmovb	%edx, %eax
.L2010:
	cmpl	$255, %r13d
	jbe	.L2012
	cmpl	$65536, %r13d
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L2012:
	movb	$31, -96(%rbp)
.L2042:
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movb	%al, -68(%rbp)
	movb	%r15b, -64(%rbp)
	movl	%ecx, -60(%rbp)
	movl	%ebx, -92(%rbp)
	movl	%r14d, -88(%rbp)
	movl	%r13d, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2043
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2023:
	.cfi_restore_state
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L2009
	.p2align 4,,10
	.p2align 3
.L2017:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L2000
	.p2align 4,,10
	.p2align 3
.L2022:
	xorl	%r15d, %r15d
	jmp	.L2007
	.p2align 4,,10
	.p2align 3
.L2016:
	xorl	%r15d, %r15d
	jmp	.L1998
.L2043:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20171:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadLookupContextSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder21LoadLookupContextSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder20LoadLookupGlobalSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder20LoadLookupGlobalSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder20LoadLookupGlobalSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder20LoadLookupGlobalSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii:
.LFB20172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testl	%r15d, %r15d
	jne	.L2045
	testq	%rdi, %rdi
	je	.L2046
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2046:
	movzbl	472(%r12), %r15d
	movl	$-1, %ecx
	testb	%r15b, %r15b
	je	.L2047
	movl	%ecx, -100(%rbp)
	cmpb	$2, %r15b
	je	.L2048
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L2048
	movl	$35, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movl	$-1, %ecx
	testb	%al, %al
	jne	.L2065
.L2048:
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2047:
	cmpl	$255, %ebx
	jbe	.L2066
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2049:
	cmpl	$255, %r14d
	jbe	.L2050
	cmpl	$65536, %r14d
	movl	$4, %eax
	cmovb	%edx, %eax
.L2050:
	cmpl	$255, %r13d
	jbe	.L2052
	cmpl	$65536, %r13d
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L2052:
	movb	$35, -96(%rbp)
	jmp	.L2091
	.p2align 4,,10
	.p2align 3
.L2045:
	testq	%rdi, %rdi
	je	.L2055
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2055:
	movzbl	472(%r12), %r15d
	movl	$-1, %ecx
	testb	%r15b, %r15b
	je	.L2056
	movl	%ecx, -100(%rbp)
	cmpb	$2, %r15b
	je	.L2057
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L2057
	movl	$32, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movl	$-1, %ecx
	testb	%al, %al
	jne	.L2071
.L2057:
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2056:
	cmpl	$255, %ebx
	jbe	.L2072
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2058:
	cmpl	$255, %r14d
	jbe	.L2059
	cmpl	$65536, %r14d
	movl	$4, %eax
	cmovb	%edx, %eax
.L2059:
	cmpl	$255, %r13d
	jbe	.L2061
	cmpl	$65536, %r13d
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L2061:
	movb	$32, -96(%rbp)
.L2091:
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movb	%al, -68(%rbp)
	movb	%r15b, -64(%rbp)
	movl	%ecx, -60(%rbp)
	movl	%ebx, -92(%rbp)
	movl	%r14d, -88(%rbp)
	movl	%r13d, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2092
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2072:
	.cfi_restore_state
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L2058
	.p2align 4,,10
	.p2align 3
.L2066:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L2049
	.p2align 4,,10
	.p2align 3
.L2071:
	xorl	%r15d, %r15d
	jmp	.L2056
	.p2align 4,,10
	.p2align 3
.L2065:
	xorl	%r15d, %r15d
	jmp	.L2047
.L2092:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20172:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder20LoadLookupGlobalSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder20LoadLookupGlobalSlotEPKNS0_12AstRawStringENS0_10TypeofModeEii
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder15StoreLookupSlotEPKNS0_12AstRawStringENS0_12LanguageModeENS0_18LookupHoistingModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder15StoreLookupSlotEPKNS0_12AstRawStringENS0_12LanguageModeENS0_18LookupHoistingModeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder15StoreLookupSlotEPKNS0_12AstRawStringENS0_12LanguageModeENS0_18LookupHoistingModeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder15StoreLookupSlotEPKNS0_12AstRawStringENS0_12LanguageModeENS0_18LookupHoistingModeE:
.LFB20173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movl	%r14d, %esi
	movl	%ebx, %edi
	movq	%rax, %r13
	call	_ZN2v88internal11interpreter20StoreLookupSlotFlags6EncodeENS0_12LanguageModeENS0_18LookupHoistingModeE@PLT
	movq	464(%r12), %r14
	movzbl	%al, %ebx
	testq	%r14, %r14
	je	.L2094
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r14), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2094:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L2095
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2095:
	movl	$1, %eax
	cmpl	$255, %r13d
	jbe	.L2096
	cmpl	$65536, %r13d
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2096:
	movb	%al, -52(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$36, -80(%rbp)
	movb	%dl, -48(%rbp)
	movl	%ecx, -44(%rbp)
	movl	%r13d, -76(%rbp)
	movl	%ebx, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L2097
	testb	%dl, %dl
	jne	.L2098
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2099:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2097:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2118
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2098:
	.cfi_restore_state
	cmpb	$1, %dl
	jne	.L2099
	cmpb	$2, %al
	jne	.L2099
	movb	$2, -48(%rbp)
	jmp	.L2099
.L2118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20173:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder15StoreLookupSlotEPKNS0_12AstRawStringENS0_12LanguageModeENS0_18LookupHoistingModeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder15StoreLookupSlotEPKNS0_12AstRawStringENS0_12LanguageModeENS0_18LookupHoistingModeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi:
.LFB20174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%ecx, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movq	464(%r12), %rdi
	movl	%eax, %r14d
	testq	%rdi, %rdi
	je	.L2121
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2121
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2121:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L2122
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2122:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L2133
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2123:
	cmpl	$255, %r14d
	jbe	.L2124
	cmpl	$65536, %r14d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L2124:
	cmpl	$255, %ebx
	jbe	.L2126
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L2126:
	movb	%al, -52(%rbp)
	movzbl	480(%r12), %eax
	movb	$40, -80(%rbp)
	movb	%sil, -48(%rbp)
	movl	%edi, -44(%rbp)
	movl	%edx, -76(%rbp)
	movl	%r14d, -72(%rbp)
	movl	%ebx, -68(%rbp)
	movq	$0, -64(%rbp)
	movl	$3, -56(%rbp)
	testb	%al, %al
	je	.L2128
	testb	%sil, %sil
	jne	.L2129
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2130:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2128:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2152
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2129:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2130
	cmpb	$1, %sil
	jne	.L2130
	movb	$2, -48(%rbp)
	jmp	.L2130
	.p2align 4,,10
	.p2align 3
.L2133:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L2123
.L2152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20174:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringE:
.LFB20175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movq	464(%r12), %rdi
	movl	%eax, %ebx
	testq	%rdi, %rdi
	je	.L2155
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2155
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2155:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L2156
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2156:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L2165
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2157:
	cmpl	$255, %ebx
	jbe	.L2158
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%ecx, %eax
.L2158:
	movb	%al, -52(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$41, -80(%rbp)
	movb	%sil, -48(%rbp)
	movl	%edi, -44(%rbp)
	movl	%edx, -76(%rbp)
	movl	%ebx, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L2160
	testb	%sil, %sil
	jne	.L2161
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2162:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2160:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2183
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2161:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2162
	cmpb	$1, %sil
	jne	.L2162
	movb	$2, -48(%rbp)
	jmp	.L2162
	.p2align 4,,10
	.p2align 3
.L2165:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L2157
.L2183:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20175:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder27LoadNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadKeyedPropertyENS1_8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadKeyedPropertyENS1_8RegisterEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadKeyedPropertyENS1_8RegisterEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadKeyedPropertyENS1_8RegisterEi:
.LFB20176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	464(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L2186
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r14), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2186
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2186:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L2187
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2187:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L2196
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2188:
	cmpl	$255, %ebx
	jbe	.L2189
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%ecx, %eax
.L2189:
	movb	%al, -52(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$42, -80(%rbp)
	movb	%sil, -48(%rbp)
	movl	%edi, -44(%rbp)
	movl	%edx, -76(%rbp)
	movl	%ebx, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L2191
	testb	%sil, %sil
	jne	.L2192
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2193:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2191:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2214
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2192:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2193
	cmpb	$1, %sil
	jne	.L2193
	movb	$2, -48(%rbp)
	jmp	.L2193
	.p2align 4,,10
	.p2align 3
.L2196:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L2188
.L2214:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20176:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadKeyedPropertyENS1_8RegisterEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder17LoadKeyedPropertyENS1_8RegisterEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11GetIteratorENS1_8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11GetIteratorENS1_8RegisterEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11GetIteratorENS1_8RegisterEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11GetIteratorENS1_8RegisterEi:
.LFB20177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2217
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2217
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2217:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L2218
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2218:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L2227
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2219:
	cmpl	$255, %ebx
	jbe	.L2220
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%ecx, %eax
.L2220:
	movb	%al, -52(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$-78, -80(%rbp)
	movb	%sil, -48(%rbp)
	movl	%edi, -44(%rbp)
	movl	%edx, -76(%rbp)
	movl	%ebx, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L2222
	testb	%sil, %sil
	jne	.L2223
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2224:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2222:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2245
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2223:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2224
	cmpb	$1, %sil
	jne	.L2224
	movb	$2, -48(%rbp)
	jmp	.L2224
	.p2align 4,,10
	.p2align 3
.L2227:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L2219
.L2245:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20177:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11GetIteratorENS1_8RegisterEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11GetIteratorENS1_8RegisterEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder25LoadAsyncIteratorPropertyENS1_8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder25LoadAsyncIteratorPropertyENS1_8RegisterEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder25LoadAsyncIteratorPropertyENS1_8RegisterEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder25LoadAsyncIteratorPropertyENS1_8RegisterEi:
.LFB20178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder25InsertAsyncIteratorSymbolEv@PLT
	movq	464(%r12), %rdi
	movl	%eax, %r14d
	testq	%rdi, %rdi
	je	.L2248
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2248
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2248:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L2249
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2249:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L2260
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2250:
	cmpl	$255, %r14d
	jbe	.L2251
	cmpl	$65536, %r14d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L2251:
	cmpl	$255, %ebx
	jbe	.L2253
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L2253:
	movb	%al, -52(%rbp)
	movzbl	480(%r12), %eax
	movb	$40, -80(%rbp)
	movb	%sil, -48(%rbp)
	movl	%edi, -44(%rbp)
	movl	%edx, -76(%rbp)
	movl	%r14d, -72(%rbp)
	movl	%ebx, -68(%rbp)
	movq	$0, -64(%rbp)
	movl	$3, -56(%rbp)
	testb	%al, %al
	je	.L2255
	testb	%sil, %sil
	jne	.L2256
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2257:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2255:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2279
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2256:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2257
	cmpb	$1, %sil
	jne	.L2257
	movb	$2, -48(%rbp)
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2260:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L2250
.L2279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20178:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder25LoadAsyncIteratorPropertyENS1_8RegisterEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder25LoadAsyncIteratorPropertyENS1_8RegisterEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreDataPropertyInLiteralENS1_8RegisterES3_NS_4base5FlagsINS0_25DataPropertyInLiteralFlagEiEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreDataPropertyInLiteralENS1_8RegisterES3_NS_4base5FlagsINS0_25DataPropertyInLiteralFlagEiEEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreDataPropertyInLiteralENS1_8RegisterES3_NS_4base5FlagsINS0_25DataPropertyInLiteralFlagEiEEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreDataPropertyInLiteralENS1_8RegisterES3_NS_4base5FlagsINS0_25DataPropertyInLiteralFlagEiEEi:
.LFB20179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$72, %rsp
	movl	%ecx, -100(%rbp)
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2282
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2282
	movl	%r15d, %esi
	movl	$-5, %r14d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	subl	%eax, %r14d
	testq	%rdi, %rdi
	je	.L2283
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2283:
	movzbl	472(%r12), %esi
	movl	$-5, %eax
	movl	$-1, %edi
	subl	%r13d, %eax
	testb	%sil, %sil
	je	.L2284
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2284:
	leal	128(%rax), %edx
	leal	128(%r14), %ecx
	cmpl	$255, %edx
	jbe	.L2324
	leal	32768(%rax), %edx
	cmpl	$65535, %edx
	jbe	.L2288
	movl	$4, %edx
.L2290:
	cmpl	$255, %ebx
	jbe	.L2291
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %dl
	cmovb	%ecx, %edx
.L2291:
	movd	-100(%rbp), %xmm1
	movd	%eax, %xmm0
	movd	%ebx, %xmm2
	movb	%dl, -68(%rbp)
	movabsq	$17179869184, %rax
	movd	%r14d, %xmm3
	movb	$50, -96(%rbp)
	movq	%rax, -76(%rbp)
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	movzbl	480(%r12), %eax
	punpcklqdq	%xmm1, %xmm0
	movb	%sil, -64(%rbp)
	movl	%edi, -60(%rbp)
	movups	%xmm0, -92(%rbp)
	testb	%al, %al
	je	.L2293
	testb	%sil, %sil
	jne	.L2294
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L2295:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2293:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2325
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2288:
	.cfi_restore_state
	movl	$2, %edx
	cmpl	$255, %ecx
	jbe	.L2290
.L2321:
	leal	32768(%r14), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	jmp	.L2290
	.p2align 4,,10
	.p2align 3
.L2282:
	movl	$-5, %r14d
	subl	%r15d, %r14d
	jmp	.L2283
	.p2align 4,,10
	.p2align 3
.L2324:
	movl	$1, %edx
	cmpl	$255, %ecx
	ja	.L2321
	jmp	.L2290
	.p2align 4,,10
	.p2align 3
.L2294:
	cmpb	$2, %al
	jne	.L2295
	cmpb	$1, %sil
	jne	.L2295
	movb	$2, -64(%rbp)
	jmp	.L2295
.L2325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20179:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreDataPropertyInLiteralENS1_8RegisterES3_NS_4base5FlagsINS0_25DataPropertyInLiteralFlagEiEEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder26StoreDataPropertyInLiteralENS1_8RegisterES3_NS_4base5FlagsINS0_25DataPropertyInLiteralFlagEiEEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder18CollectTypeProfileEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CollectTypeProfileEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CollectTypeProfileEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder18CollectTypeProfileEi:
.LFB20180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2327
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L2327:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L2328
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2328:
	leal	128(%rbx), %esi
	movl	$1, %eax
	cmpl	$255, %esi
	jbe	.L2329
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2329:
	movb	%al, -36(%rbp)
	movzbl	480(%r12), %eax
	movb	$51, -64(%rbp)
	movb	%dl, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movl	%ebx, -60(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L2330
	testb	%dl, %dl
	jne	.L2331
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L2332:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2330:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2351
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2331:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2332
	cmpb	$1, %dl
	jne	.L2332
	movb	$2, -32(%rbp)
	jmp	.L2332
.L2351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20180:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CollectTypeProfileEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder18CollectTypeProfileEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEmiNS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEmiNS0_12LanguageModeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEmiNS0_12LanguageModeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEmiNS0_12LanguageModeE:
.LFB20181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movq	464(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L2354
	movq	16(%r15), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r15), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2354
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2354:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L2355
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2355:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L2366
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2356:
	cmpl	$255, %r14d
	jbe	.L2357
	cmpl	$65536, %r14d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L2357:
	cmpl	$255, %ebx
	jbe	.L2359
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L2359:
	movb	%al, -68(%rbp)
	movzbl	480(%r12), %eax
	movb	$45, -96(%rbp)
	movb	%sil, -64(%rbp)
	movl	%edi, -60(%rbp)
	movl	%edx, -92(%rbp)
	movl	%r14d, -88(%rbp)
	movl	%ebx, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	testb	%al, %al
	je	.L2361
	testb	%sil, %sil
	jne	.L2362
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L2363:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2361:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2385
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2362:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2363
	cmpb	$1, %sil
	jne	.L2363
	movb	$2, -64(%rbp)
	jmp	.L2363
	.p2align 4,,10
	.p2align 3
.L2366:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L2356
.L2385:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20181:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEmiNS0_12LanguageModeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEmiNS0_12LanguageModeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEiNS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEiNS0_12LanguageModeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEiNS0_12LanguageModeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEiNS0_12LanguageModeE:
.LFB20182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%r8d, %ebx
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movl	%ebx, %r8d
	movl	%r14d, %ecx
	popq	%rbx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEmiNS0_12LanguageModeE
	.cfi_endproc
.LFE20182:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEiNS0_12LanguageModeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEPKNS0_12AstRawStringEiNS0_12LanguageModeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder28StoreNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringENS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder28StoreNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringENS0_12LanguageModeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder28StoreNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringENS0_12LanguageModeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder28StoreNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringENS0_12LanguageModeE:
.LFB20183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movq	464(%r12), %r15
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.L2389
	movq	16(%r15), %rsi
	movq	%r15, %rdi
	movzbl	%r14b, %r14d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r15), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2390
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2390:
	movzbl	472(%r12), %esi
	movl	$-5, %eax
	movl	$-1, %edi
	subl	%r13d, %eax
	testb	%sil, %sil
	je	.L2391
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2391:
	leal	128(%rax), %edx
	cmpl	$255, %edx
	jbe	.L2424
	leal	32768(%rax), %edx
	cmpl	$65535, %edx
	jbe	.L2395
	movl	$4, %edx
.L2397:
	movl	%eax, -92(%rbp)
	movzbl	480(%r12), %eax
	movb	$46, -96(%rbp)
	movb	%dl, -68(%rbp)
	movb	%sil, -64(%rbp)
	movl	%edi, -60(%rbp)
	movl	%ebx, -88(%rbp)
	movl	%r14d, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	testb	%al, %al
	je	.L2398
	testb	%sil, %sil
	jne	.L2399
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L2400:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2398:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2425
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2395:
	.cfi_restore_state
	movl	$2, %edx
	cmpl	$255, %ebx
	jbe	.L2397
.L2421:
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	jmp	.L2397
	.p2align 4,,10
	.p2align 3
.L2389:
	movzbl	%r14b, %r14d
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2424:
	movl	$1, %edx
	cmpl	$255, %ebx
	ja	.L2421
	jmp	.L2397
	.p2align 4,,10
	.p2align 3
.L2399:
	cmpb	$2, %al
	jne	.L2400
	cmpb	$1, %sil
	jne	.L2400
	movb	$2, -64(%rbp)
	jmp	.L2400
.L2425:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20183:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder28StoreNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringENS0_12LanguageModeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder28StoreNamedPropertyNoFeedbackENS1_8RegisterEPKNS0_12AstRawStringENS0_12LanguageModeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder21StoreNamedOwnPropertyENS1_8RegisterEPKNS0_12AstRawStringEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder21StoreNamedOwnPropertyENS1_8RegisterEPKNS0_12AstRawStringEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder21StoreNamedOwnPropertyENS1_8RegisterEPKNS0_12AstRawStringEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder21StoreNamedOwnPropertyENS1_8RegisterEPKNS0_12AstRawStringEi:
.LFB20184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movq	464(%r12), %r15
	movl	%eax, %r14d
	testq	%r15, %r15
	je	.L2428
	movq	16(%r15), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r15), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2428
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2428:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L2429
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2429:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L2440
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2430:
	cmpl	$255, %r14d
	jbe	.L2431
	cmpl	$65536, %r14d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L2431:
	cmpl	$255, %ebx
	jbe	.L2433
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L2433:
	movb	%al, -68(%rbp)
	movzbl	480(%r12), %eax
	movb	$47, -96(%rbp)
	movb	%sil, -64(%rbp)
	movl	%edi, -60(%rbp)
	movl	%edx, -92(%rbp)
	movl	%r14d, -88(%rbp)
	movl	%ebx, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	testb	%al, %al
	je	.L2435
	testb	%sil, %sil
	jne	.L2436
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L2437:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2435:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2459
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2436:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2437
	cmpb	$1, %sil
	jne	.L2437
	movb	$2, -64(%rbp)
	jmp	.L2437
	.p2align 4,,10
	.p2align 3
.L2440:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L2430
.L2459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20184:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder21StoreNamedOwnPropertyENS1_8RegisterEPKNS0_12AstRawStringEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder21StoreNamedOwnPropertyENS1_8RegisterEPKNS0_12AstRawStringEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreKeyedPropertyENS1_8RegisterES3_iNS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreKeyedPropertyENS1_8RegisterES3_iNS0_12LanguageModeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreKeyedPropertyENS1_8RegisterES3_iNS0_12LanguageModeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreKeyedPropertyENS1_8RegisterES3_iNS0_12LanguageModeE:
.LFB20185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movq	464(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L2462
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r14), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2462
	movl	%r15d, %esi
	movl	$-5, %r14d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	subl	%eax, %r14d
	testq	%rdi, %rdi
	je	.L2463
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2463:
	movzbl	472(%r12), %r8d
	movl	$-5, %edx
	movl	$-1, %esi
	subl	%r13d, %edx
	testb	%r8b, %r8b
	je	.L2464
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2464:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L2475
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2465:
	leal	128(%r14), %edi
	cmpl	$255, %edi
	jbe	.L2466
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%ecx, %eax
.L2466:
	cmpl	$255, %ebx
	jbe	.L2468
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L2468:
	movb	%al, -68(%rbp)
	movzbl	480(%r12), %eax
	movb	$48, -96(%rbp)
	movb	%r8b, -64(%rbp)
	movl	%esi, -60(%rbp)
	movl	%edx, -92(%rbp)
	movl	%r14d, -88(%rbp)
	movl	%ebx, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	testb	%al, %al
	je	.L2470
	testb	%r8b, %r8b
	jne	.L2471
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L2472:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2470:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2498
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2471:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2472
	cmpb	$1, %r8b
	jne	.L2472
	movb	$2, -64(%rbp)
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2462:
	movl	$-5, %r14d
	subl	%r15d, %r14d
	jmp	.L2463
	.p2align 4,,10
	.p2align 3
.L2475:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L2465
.L2498:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20185:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreKeyedPropertyENS1_8RegisterES3_iNS0_12LanguageModeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreKeyedPropertyENS1_8RegisterES3_iNS0_12LanguageModeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreInArrayLiteralENS1_8RegisterES3_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreInArrayLiteralENS1_8RegisterES3_i
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreInArrayLiteralENS1_8RegisterES3_i, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreInArrayLiteralENS1_8RegisterES3_i:
.LFB20186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movq	464(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L2501
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r14), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2501
	movl	%r15d, %esi
	movl	$-5, %r14d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	subl	%eax, %r14d
	testq	%rdi, %rdi
	je	.L2502
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2502:
	movzbl	472(%r12), %r8d
	movl	$-5, %edx
	movl	$-1, %esi
	subl	%r13d, %edx
	testb	%r8b, %r8b
	je	.L2503
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2503:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L2514
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2504:
	leal	128(%r14), %edi
	cmpl	$255, %edi
	jbe	.L2505
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%ecx, %eax
.L2505:
	cmpl	$255, %ebx
	jbe	.L2507
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L2507:
	movb	%al, -68(%rbp)
	movzbl	480(%r12), %eax
	movb	$49, -96(%rbp)
	movb	%r8b, -64(%rbp)
	movl	%esi, -60(%rbp)
	movl	%edx, -92(%rbp)
	movl	%r14d, -88(%rbp)
	movl	%ebx, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	testb	%al, %al
	je	.L2509
	testb	%r8b, %r8b
	jne	.L2510
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L2511:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2509:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2537
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2510:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2511
	cmpb	$1, %r8b
	jne	.L2511
	movb	$2, -64(%rbp)
	jmp	.L2511
	.p2align 4,,10
	.p2align 3
.L2501:
	movl	$-5, %r14d
	subl	%r15d, %r14d
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2514:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L2504
.L2537:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20186:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreInArrayLiteralENS1_8RegisterES3_i, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreInArrayLiteralENS1_8RegisterES3_i
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder23StoreHomeObjectPropertyENS1_8RegisterEiNS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder23StoreHomeObjectPropertyENS1_8RegisterEiNS0_12LanguageModeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder23StoreHomeObjectPropertyENS1_8RegisterEiNS0_12LanguageModeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder23StoreHomeObjectPropertyENS1_8RegisterEiNS0_12LanguageModeE:
.LFB20187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%ecx, %ebx
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder22InsertHomeObjectSymbolEv@PLT
	movl	%ebx, %r8d
	movl	%r14d, %ecx
	popq	%rbx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEmiNS0_12LanguageModeE
	.cfi_endproc
.LFE20187:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder23StoreHomeObjectPropertyENS1_8RegisterEiNS0_12LanguageModeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder23StoreHomeObjectPropertyENS1_8RegisterEiNS0_12LanguageModeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder27StoreClassFieldsInitializerENS1_8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder27StoreClassFieldsInitializerENS1_8RegisterEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder27StoreClassFieldsInitializerENS1_8RegisterEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder27StoreClassFieldsInitializerENS1_8RegisterEi:
.LFB20188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder23InsertClassFieldsSymbolEv@PLT
	addq	$8, %rsp
	movl	%r14d, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	movl	$1, %r8d
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder18StoreNamedPropertyENS1_8RegisterEmiNS0_12LanguageModeE
	.cfi_endproc
.LFE20188:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder27StoreClassFieldsInitializerENS1_8RegisterEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder27StoreClassFieldsInitializerENS1_8RegisterEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder26LoadClassFieldsInitializerENS1_8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder26LoadClassFieldsInitializerENS1_8RegisterEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder26LoadClassFieldsInitializerENS1_8RegisterEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder26LoadClassFieldsInitializerENS1_8RegisterEi:
.LFB20189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder23InsertClassFieldsSymbolEv@PLT
	movq	464(%r12), %rdi
	movl	%eax, %r14d
	testq	%rdi, %rdi
	je	.L2544
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2544
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2544:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L2545
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2545:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L2556
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2546:
	cmpl	$255, %r14d
	jbe	.L2547
	cmpl	$65536, %r14d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L2547:
	cmpl	$255, %ebx
	jbe	.L2549
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L2549:
	movb	%al, -52(%rbp)
	movzbl	480(%r12), %eax
	movb	$40, -80(%rbp)
	movb	%sil, -48(%rbp)
	movl	%edi, -44(%rbp)
	movl	%edx, -76(%rbp)
	movl	%r14d, -72(%rbp)
	movl	%ebx, -68(%rbp)
	movq	$0, -64(%rbp)
	movl	$3, -56(%rbp)
	testb	%al, %al
	je	.L2551
	testb	%sil, %sil
	jne	.L2552
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2553:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2551:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2575
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2552:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2553
	cmpb	$1, %sil
	jne	.L2553
	movb	$2, -48(%rbp)
	jmp	.L2553
	.p2align 4,,10
	.p2align 3
.L2556:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L2546
.L2575:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20189:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder26LoadClassFieldsInitializerENS1_8RegisterEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder26LoadClassFieldsInitializerENS1_8RegisterEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder13CreateClosureEmii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CreateClosureEmii
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CreateClosureEmii, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder13CreateClosureEmii:
.LFB20190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2577
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2577:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L2578
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2578:
	cmpl	$255, %ebx
	jbe	.L2610
	movl	$4, %eax
	cmpl	$65535, %ebx
	jbe	.L2611
.L2584:
	movb	%al, -52(%rbp)
	movzbl	480(%r12), %eax
	movb	$-127, -80(%rbp)
	movb	%dl, -48(%rbp)
	movl	%ecx, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movl	%r13d, -72(%rbp)
	movl	%r14d, -68(%rbp)
	movq	$0, -64(%rbp)
	movl	$3, -56(%rbp)
	testb	%al, %al
	je	.L2585
	testb	%dl, %dl
	jne	.L2586
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2587:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2585:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2612
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2586:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2587
	cmpb	$1, %dl
	jne	.L2587
	movb	$2, -48(%rbp)
	jmp	.L2587
	.p2align 4,,10
	.p2align 3
.L2611:
	movl	$2, %eax
	cmpl	$255, %r13d
	jbe	.L2584
.L2608:
	cmpl	$65536, %r13d
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	jmp	.L2584
	.p2align 4,,10
	.p2align 3
.L2610:
	movl	$1, %eax
	cmpl	$255, %r13d
	ja	.L2608
	jmp	.L2584
.L2612:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20190:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CreateClosureEmii, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder13CreateClosureEmii
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateBlockContextEPKNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateBlockContextEPKNS0_5ScopeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateBlockContextEPKNS0_5ScopeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateBlockContextEPKNS0_5ScopeE:
.LFB20191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_5ScopeE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2614
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2614:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L2615
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2615:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L2616
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2616:
	movb	%al, -36(%rbp)
	movzbl	480(%r12), %eax
	movb	$-126, -64(%rbp)
	movb	%dl, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movl	%ebx, -60(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L2617
	testb	%dl, %dl
	jne	.L2618
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L2619:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2617:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2638
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2618:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2619
	cmpb	$1, %dl
	jne	.L2619
	movb	$2, -32(%rbp)
	jmp	.L2619
.L2638:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20191:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateBlockContextEPKNS0_5ScopeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateBlockContextEPKNS0_5ScopeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateCatchContextENS1_8RegisterEPKNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateCatchContextENS1_8RegisterEPKNS0_5ScopeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateCatchContextENS1_8RegisterEPKNS0_5ScopeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateCatchContextENS1_8RegisterEPKNS0_5ScopeE:
.LFB20192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_5ScopeE@PLT
	movq	464(%r12), %rdi
	movl	%eax, %ebx
	testq	%rdi, %rdi
	je	.L2641
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2641
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2641:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L2642
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2642:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L2651
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2643:
	cmpl	$255, %ebx
	jbe	.L2644
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%ecx, %eax
.L2644:
	movb	%al, -52(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$-125, -80(%rbp)
	movb	%sil, -48(%rbp)
	movl	%edi, -44(%rbp)
	movl	%edx, -76(%rbp)
	movl	%ebx, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L2646
	testb	%sil, %sil
	jne	.L2647
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2648:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2646:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2669
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2647:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2648
	cmpb	$1, %sil
	jne	.L2648
	movb	$2, -48(%rbp)
	jmp	.L2648
	.p2align 4,,10
	.p2align 3
.L2651:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L2643
.L2669:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20192:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateCatchContextENS1_8RegisterEPKNS0_5ScopeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateCatchContextENS1_8RegisterEPKNS0_5ScopeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder21CreateFunctionContextEPKNS0_5ScopeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder21CreateFunctionContextEPKNS0_5ScopeEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder21CreateFunctionContextEPKNS0_5ScopeEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder21CreateFunctionContextEPKNS0_5ScopeEi:
.LFB20193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_5ScopeE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2671
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2671:
	movzbl	472(%r12), %edx
	movl	$-1, %esi
	testb	%dl, %dl
	je	.L2672
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2672:
	cmpl	$255, %ebx
	jbe	.L2681
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
.L2673:
	cmpl	$255, %r13d
	jbe	.L2674
	cmpl	$65536, %r13d
	movl	$4, %ecx
	cmovb	%eax, %ecx
.L2674:
	movabsq	$8589934592, %rax
	movb	$-124, -80(%rbp)
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	%cl, -52(%rbp)
	movb	%dl, -48(%rbp)
	movl	%esi, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movl	%r13d, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L2676
	testb	%dl, %dl
	jne	.L2677
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2678:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2676:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2698
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2677:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2678
	cmpb	$1, %dl
	jne	.L2678
	movb	$2, -48(%rbp)
	jmp	.L2678
	.p2align 4,,10
	.p2align 3
.L2681:
	movl	$2, %eax
	movl	$1, %ecx
	jmp	.L2673
.L2698:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20193:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder21CreateFunctionContextEPKNS0_5ScopeEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder21CreateFunctionContextEPKNS0_5ScopeEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateEvalContextEPKNS0_5ScopeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateEvalContextEPKNS0_5ScopeEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateEvalContextEPKNS0_5ScopeEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateEvalContextEPKNS0_5ScopeEi:
.LFB20194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_5ScopeE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2700
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2700:
	movzbl	472(%r12), %edx
	movl	$-1, %esi
	testb	%dl, %dl
	je	.L2701
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2701:
	cmpl	$255, %ebx
	jbe	.L2710
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
.L2702:
	cmpl	$255, %r13d
	jbe	.L2703
	cmpl	$65536, %r13d
	movl	$4, %ecx
	cmovb	%eax, %ecx
.L2703:
	movabsq	$8589934592, %rax
	movb	$-123, -80(%rbp)
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	%cl, -52(%rbp)
	movb	%dl, -48(%rbp)
	movl	%esi, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movl	%r13d, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L2705
	testb	%dl, %dl
	jne	.L2706
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2707:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2705:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2727
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2706:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2707
	cmpb	$1, %dl
	jne	.L2707
	movb	$2, -48(%rbp)
	jmp	.L2707
	.p2align 4,,10
	.p2align 3
.L2710:
	movl	$2, %eax
	movl	$1, %ecx
	jmp	.L2702
.L2727:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20194:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateEvalContextEPKNS0_5ScopeEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateEvalContextEPKNS0_5ScopeEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateWithContextENS1_8RegisterEPKNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateWithContextENS1_8RegisterEPKNS0_5ScopeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateWithContextENS1_8RegisterEPKNS0_5ScopeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateWithContextENS1_8RegisterEPKNS0_5ScopeE:
.LFB20195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_5ScopeE@PLT
	movq	464(%r12), %rdi
	movl	%eax, %ebx
	testq	%rdi, %rdi
	je	.L2730
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2730
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2730:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L2731
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2731:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L2740
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2732:
	cmpl	$255, %ebx
	jbe	.L2733
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%ecx, %eax
.L2733:
	movb	%al, -52(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$-122, -80(%rbp)
	movb	%sil, -48(%rbp)
	movl	%edi, -44(%rbp)
	movl	%edx, -76(%rbp)
	movl	%ebx, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L2735
	testb	%sil, %sil
	jne	.L2736
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2737:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2735:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2758
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2736:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2737
	cmpb	$1, %sil
	jne	.L2737
	movb	$2, -48(%rbp)
	jmp	.L2737
	.p2align 4,,10
	.p2align 3
.L2740:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L2732
.L2758:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20195:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateWithContextENS1_8RegisterEPKNS0_5ScopeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder17CreateWithContextENS1_8RegisterEPKNS0_5ScopeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE:
.LFB20196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$1, %sil
	je	.L2760
	movl	%esi, %ebx
	cmpb	$2, %sil
	je	.L2761
	testb	%sil, %sil
	je	.L2804
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2760:
	movq	464(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2767
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2767:
	movzbl	472(%r12), %ebx
	movl	$-1, %r13d
	testb	%bl, %bl
	jne	.L2805
.L2768:
	movb	$-120, -80(%rbp)
.L2802:
	movb	%bl, -48(%rbp)
	movl	%r13d, -44(%rbp)
.L2803:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movb	$1, -52(%rbp)
	movq	$0, -60(%rbp)
	movups	%xmm0, -76(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2806
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2804:
	.cfi_restore_state
	movq	464(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2763
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2763:
	movzbl	472(%r12), %r13d
	movl	$-1, %r14d
	testb	%r13b, %r13b
	je	.L2764
	cmpb	$2, %r13b
	je	.L2765
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L2765
	movl	$-121, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	jne	.L2775
.L2765:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2764:
	movb	$-121, -80(%rbp)
	movb	%r13b, -48(%rbp)
	movl	%r14d, -44(%rbp)
	jmp	.L2803
	.p2align 4,,10
	.p2align 3
.L2761:
	movq	464(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2770
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2770:
	movzbl	472(%r12), %ebx
	movl	$-1, %r13d
	testb	%bl, %bl
	je	.L2771
	cmpb	$2, %bl
	je	.L2772
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L2772
	movl	$-119, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	jne	.L2779
.L2772:
	movl	476(%r12), %r13d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2771:
	movb	$-119, -80(%rbp)
	jmp	.L2802
	.p2align 4,,10
	.p2align 3
.L2805:
	cmpb	$2, %bl
	je	.L2769
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L2769
	movl	$-120, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	jne	.L2777
.L2769:
	movl	476(%r12), %r13d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L2768
	.p2align 4,,10
	.p2align 3
.L2777:
	xorl	%ebx, %ebx
	jmp	.L2768
	.p2align 4,,10
	.p2align 3
.L2779:
	xorl	%ebx, %ebx
	jmp	.L2771
	.p2align 4,,10
	.p2align 3
.L2775:
	movl	%ebx, %r13d
	jmp	.L2764
.L2806:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20196:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder15CreateArgumentsENS0_19CreateArgumentsTypeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateRegExpLiteralEPKNS0_12AstRawStringEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateRegExpLiteralEPKNS0_12AstRawStringEii
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateRegExpLiteralEPKNS0_12AstRawStringEii, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateRegExpLiteralEPKNS0_12AstRawStringEii:
.LFB20197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L2808
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2808:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L2809
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2809:
	cmpl	$255, %ebx
	jbe	.L2841
	movl	$4, %eax
	cmpl	$65535, %ebx
	jbe	.L2842
.L2815:
	movb	%al, -52(%rbp)
	movzbl	480(%r12), %eax
	movb	$121, -80(%rbp)
	movb	%dl, -48(%rbp)
	movl	%ecx, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movl	%r13d, -72(%rbp)
	movl	%r14d, -68(%rbp)
	movq	$0, -64(%rbp)
	movl	$3, -56(%rbp)
	testb	%al, %al
	je	.L2816
	testb	%dl, %dl
	jne	.L2817
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2818:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2816:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2843
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2817:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2818
	cmpb	$1, %dl
	jne	.L2818
	movb	$2, -48(%rbp)
	jmp	.L2818
	.p2align 4,,10
	.p2align 3
.L2842:
	movl	$2, %eax
	cmpl	$255, %r13d
	jbe	.L2815
.L2839:
	cmpl	$65536, %r13d
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	jmp	.L2815
	.p2align 4,,10
	.p2align 3
.L2841:
	movl	$1, %eax
	cmpl	$255, %r13d
	ja	.L2839
	jmp	.L2815
.L2843:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20197:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateRegExpLiteralEPKNS0_12AstRawStringEii, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateRegExpLiteralEPKNS0_12AstRawStringEii
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateEmptyArrayLiteralEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateEmptyArrayLiteralEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateEmptyArrayLiteralEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateEmptyArrayLiteralEi:
.LFB20198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2845
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2845:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L2846
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2846:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L2847
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2847:
	movb	%al, -36(%rbp)
	movzbl	480(%r12), %eax
	movb	$124, -64(%rbp)
	movb	%dl, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movl	%ebx, -60(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L2848
	testb	%dl, %dl
	jne	.L2849
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L2850:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2848:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2869
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2849:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2850
	cmpb	$1, %dl
	jne	.L2850
	movb	$2, -32(%rbp)
	jmp	.L2850
.L2869:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20198:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateEmptyArrayLiteralEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateEmptyArrayLiteralEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateArrayLiteralEmii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateArrayLiteralEmii
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateArrayLiteralEmii, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateArrayLiteralEmii:
.LFB20199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2871
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2871:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L2872
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2872:
	cmpl	$255, %ebx
	jbe	.L2904
	movl	$4, %eax
	cmpl	$65535, %ebx
	jbe	.L2905
.L2878:
	movb	%al, -52(%rbp)
	movzbl	480(%r12), %eax
	movb	$122, -80(%rbp)
	movb	%dl, -48(%rbp)
	movl	%ecx, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movl	%r13d, -72(%rbp)
	movl	%r14d, -68(%rbp)
	movq	$0, -64(%rbp)
	movl	$3, -56(%rbp)
	testb	%al, %al
	je	.L2879
	testb	%dl, %dl
	jne	.L2880
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2881:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2879:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2906
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2880:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2881
	cmpb	$1, %dl
	jne	.L2881
	movb	$2, -48(%rbp)
	jmp	.L2881
	.p2align 4,,10
	.p2align 3
.L2905:
	movl	$2, %eax
	cmpl	$255, %r13d
	jbe	.L2878
.L2902:
	cmpl	$65536, %r13d
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	jmp	.L2878
	.p2align 4,,10
	.p2align 3
.L2904:
	movl	$1, %eax
	cmpl	$255, %r13d
	ja	.L2902
	jmp	.L2878
.L2906:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20199:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateArrayLiteralEmii, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder18CreateArrayLiteralEmii
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateArrayFromIterableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateArrayFromIterableEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateArrayFromIterableEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateArrayFromIterableEv:
.LFB20200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L2908
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2908:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	je	.L2909
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$123, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L2911
	cmpb	$1, %al
	jne	.L2912
	cmpb	$2, %dl
	jne	.L2912
	movb	$2, -32(%rbp)
	jmp	.L2912
	.p2align 4,,10
	.p2align 3
.L2909:
	pxor	%xmm0, %xmm0
	movb	$123, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L2911
	movl	484(%r12), %eax
	movb	%dl, -32(%rbp)
	movl	%eax, -28(%rbp)
.L2912:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2911:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2931
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2931:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20200:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateArrayFromIterableEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder23CreateArrayFromIterableEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateObjectLiteralEmii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateObjectLiteralEmii
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateObjectLiteralEmii, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateObjectLiteralEmii:
.LFB20201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2933
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2933:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L2934
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2934:
	cmpl	$255, %ebx
	jbe	.L2966
	movl	$4, %eax
	cmpl	$65535, %ebx
	jbe	.L2967
.L2940:
	movb	%al, -52(%rbp)
	movzbl	480(%r12), %eax
	movb	$125, -80(%rbp)
	movb	%dl, -48(%rbp)
	movl	%ecx, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movl	%r13d, -72(%rbp)
	movl	%r14d, -68(%rbp)
	movq	$0, -64(%rbp)
	movl	$3, -56(%rbp)
	testb	%al, %al
	je	.L2941
	testb	%dl, %dl
	jne	.L2942
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L2943:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2941:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2968
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2942:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L2943
	cmpb	$1, %dl
	jne	.L2943
	movb	$2, -48(%rbp)
	jmp	.L2943
	.p2align 4,,10
	.p2align 3
.L2967:
	movl	$2, %eax
	cmpl	$255, %r13d
	jbe	.L2940
.L2964:
	cmpl	$65536, %r13d
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	jmp	.L2940
	.p2align 4,,10
	.p2align 3
.L2966:
	movl	$1, %eax
	cmpl	$255, %r13d
	ja	.L2964
	jmp	.L2940
.L2968:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20201:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateObjectLiteralEmii, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder19CreateObjectLiteralEmii
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder24CreateEmptyObjectLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder24CreateEmptyObjectLiteralEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder24CreateEmptyObjectLiteralEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder24CreateEmptyObjectLiteralEv:
.LFB20202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2970
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L2970:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	je	.L2971
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$126, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L2973
	cmpb	$1, %al
	jne	.L2974
	cmpb	$2, %dl
	jne	.L2974
	movb	$2, -32(%rbp)
	jmp	.L2974
	.p2align 4,,10
	.p2align 3
.L2971:
	pxor	%xmm0, %xmm0
	movb	$126, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L2973
	movl	484(%r12), %eax
	movb	%dl, -32(%rbp)
	movl	%eax, -28(%rbp)
.L2974:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L2973:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2993
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2993:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20202:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder24CreateEmptyObjectLiteralEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder24CreateEmptyObjectLiteralEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11CloneObjectENS1_8RegisterEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CloneObjectENS1_8RegisterEii
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CloneObjectENS1_8RegisterEii, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11CloneObjectENS1_8RegisterEii:
.LFB20203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2996
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2996
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L2996:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L2997
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L2997:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L3006
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L2998:
	cmpl	$255, %r14d
	jbe	.L2999
	cmpl	$65536, %r14d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L2999:
	movb	%al, -52(%rbp)
	movzbl	480(%r12), %eax
	movb	$127, -80(%rbp)
	movb	%sil, -48(%rbp)
	movl	%edi, -44(%rbp)
	movl	%edx, -76(%rbp)
	movl	%ebx, -72(%rbp)
	movl	%r14d, -68(%rbp)
	movq	$0, -64(%rbp)
	movl	$3, -56(%rbp)
	testb	%al, %al
	je	.L3001
	testb	%sil, %sil
	jne	.L3002
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3003:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3001:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3024
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3002:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L3003
	cmpb	$1, %sil
	jne	.L3003
	movb	$2, -48(%rbp)
	jmp	.L3003
	.p2align 4,,10
	.p2align 3
.L3006:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L2998
.L3024:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20203:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CloneObjectENS1_8RegisterEii, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11CloneObjectENS1_8RegisterEii
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder17GetTemplateObjectEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder17GetTemplateObjectEmi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder17GetTemplateObjectEmi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder17GetTemplateObjectEmi:
.LFB20204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3026
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L3026:
	movzbl	472(%r12), %ecx
	movl	$-1, %esi
	testb	%cl, %cl
	je	.L3027
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L3027:
	cmpl	$255, %ebx
	jbe	.L3036
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L3028:
	cmpl	$255, %r13d
	jbe	.L3029
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%edx, %eax
.L3029:
	movb	%al, -52(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$-128, -80(%rbp)
	movb	%cl, -48(%rbp)
	movl	%esi, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movl	%r13d, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L3031
	testb	%cl, %cl
	jne	.L3032
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3033:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3031:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3053
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3032:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L3033
	cmpb	$1, %cl
	jne	.L3033
	movb	$2, -48(%rbp)
	jmp	.L3033
	.p2align 4,,10
	.p2align 3
.L3036:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L3028
.L3053:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20204:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder17GetTemplateObjectEmi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder17GetTemplateObjectEmi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11PushContextENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11PushContextENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11PushContextENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11PushContextENS1_8RegisterE:
.LFB20205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3055
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3055
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L3055:
	movzbl	472(%r12), %ecx
	movl	$-5, %eax
	movl	$-1, %esi
	subl	%ebx, %eax
	testb	%cl, %cl
	je	.L3056
	cmpb	$2, %cl
	je	.L3057
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L3064
.L3057:
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L3056:
	leal	128(%rax), %edi
	movl	$1, %edx
	cmpl	$255, %edi
	jbe	.L3058
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L3058:
	movl	%eax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$22, -64(%rbp)
	movb	%dl, -36(%rbp)
	movb	%cl, -32(%rbp)
	movl	%esi, -28(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L3059
	testb	%cl, %cl
	jne	.L3060
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L3061:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3059:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3087
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3060:
	.cfi_restore_state
	cmpb	$1, %cl
	jne	.L3061
	cmpb	$2, %al
	jne	.L3061
	movb	$2, -32(%rbp)
	jmp	.L3061
	.p2align 4,,10
	.p2align 3
.L3064:
	xorl	%ecx, %ecx
	jmp	.L3056
.L3087:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20205:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11PushContextENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11PushContextENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE:
.LFB20206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3089
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %esi
.L3089:
	movzbl	472(%r12), %ecx
	movl	$-5, %eax
	subl	%esi, %eax
	movl	$-1, %esi
	testb	%cl, %cl
	je	.L3090
	cmpb	$2, %cl
	je	.L3091
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L3098
.L3091:
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L3090:
	leal	128(%rax), %edi
	movl	$1, %edx
	cmpl	$255, %edi
	jbe	.L3092
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L3092:
	movl	%eax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$23, -64(%rbp)
	movb	%dl, -36(%rbp)
	movb	%cl, -32(%rbp)
	movl	%esi, -28(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L3093
	testb	%cl, %cl
	jne	.L3094
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L3095:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3093:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3118
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3094:
	.cfi_restore_state
	cmpb	$1, %cl
	jne	.L3095
	cmpb	$2, %al
	jne	.L3095
	movb	$2, -32(%rbp)
	jmp	.L3095
	.p2align 4,,10
	.p2align 3
.L3098:
	xorl	%ecx, %ecx
	jmp	.L3090
.L3118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20206:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder10PopContextENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder8ToObjectENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToObjectENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToObjectENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToObjectENS1_8RegisterE:
.LFB20207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3120
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3120
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L3120:
	movzbl	472(%r12), %ecx
	movl	$-5, %eax
	movl	$-1, %esi
	subl	%ebx, %eax
	testb	%cl, %cl
	je	.L3121
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L3121:
	leal	128(%rax), %edi
	movl	$1, %edx
	cmpl	$255, %edi
	jbe	.L3122
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L3122:
	movl	%eax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$119, -64(%rbp)
	movb	%dl, -36(%rbp)
	movb	%cl, -32(%rbp)
	movl	%esi, -28(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L3123
	testb	%cl, %cl
	jne	.L3124
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L3125:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3123:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3147
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3124:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L3125
	cmpb	$1, %cl
	jne	.L3125
	movb	$2, -32(%rbp)
	jmp	.L3125
.L3147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20207:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToObjectENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToObjectENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder6ToNameENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder6ToNameENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder6ToNameENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder6ToNameENS1_8RegisterE:
.LFB20208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3149
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3149
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L3149:
	movzbl	472(%r12), %ecx
	movl	$-5, %eax
	movl	$-1, %esi
	subl	%ebx, %eax
	testb	%cl, %cl
	je	.L3150
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L3150:
	leal	128(%rax), %edi
	movl	$1, %edx
	cmpl	$255, %edi
	jbe	.L3151
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L3151:
	movl	%eax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$116, -64(%rbp)
	movb	%dl, -36(%rbp)
	movb	%cl, -32(%rbp)
	movl	%esi, -28(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L3152
	testb	%cl, %cl
	jne	.L3153
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L3154:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3152:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3176
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3153:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L3154
	cmpb	$1, %cl
	jne	.L3154
	movb	$2, -32(%rbp)
	jmp	.L3154
.L3176:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20208:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder6ToNameENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder6ToNameENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder8ToStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToStringEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToStringEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToStringEv:
.LFB20209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L3178
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L3178:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	je	.L3179
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$120, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3181
	cmpb	$1, %al
	jne	.L3182
	cmpb	$2, %dl
	jne	.L3182
	movb	$2, -32(%rbp)
	jmp	.L3182
	.p2align 4,,10
	.p2align 3
.L3179:
	pxor	%xmm0, %xmm0
	movb	$120, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3181
	movl	484(%r12), %eax
	movb	%dl, -32(%rbp)
	movl	%eax, -28(%rbp)
.L3182:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3181:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3201
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3201:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20209:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToStringEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToStringEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder8ToNumberEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToNumberEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToNumberEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToNumberEi:
.LFB20210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$56, %rsp
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L3203
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L3203:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L3204
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L3204:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L3205
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L3205:
	movb	%al, -52(%rbp)
	movzbl	480(%r12), %eax
	movb	$117, -80(%rbp)
	movb	%dl, -48(%rbp)
	movl	%ecx, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$1, -56(%rbp)
	testb	%al, %al
	je	.L3206
	testb	%dl, %dl
	jne	.L3207
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3208:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3206:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3227
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3207:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L3208
	cmpb	$1, %dl
	jne	.L3208
	movb	$2, -48(%rbp)
	jmp	.L3208
.L3227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20210:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToNumberEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder8ToNumberEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder9ToNumericEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ToNumericEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ToNumericEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder9ToNumericEi:
.LFB20211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$56, %rsp
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L3229
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L3229:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L3230
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L3230:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L3231
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L3231:
	movb	%al, -52(%rbp)
	movzbl	480(%r12), %eax
	movb	$118, -80(%rbp)
	movb	%dl, -48(%rbp)
	movl	%ecx, -44(%rbp)
	movl	%ebx, -76(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$1, -56(%rbp)
	testb	%al, %al
	je	.L3232
	testb	%dl, %dl
	jne	.L3233
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3234:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3232:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3253
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3233:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L3234
	cmpb	$1, %dl
	jne	.L3234
	movb	$2, -48(%rbp)
	jmp	.L3234
.L3253:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20211:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ToNumericEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder9ToNumericEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE:
.LFB20212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	cmpq	$-1, 8(%rsi)
	movq	%rdi, %r12
	je	.L3255
	movq	464(%rdi), %rdi
	movq	%rsi, %r13
	testq	%rdi, %rdi
	je	.L3256
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
.L3256:
	leaq	320(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9BindLabelEPNS1_13BytecodeLabelE@PLT
.L3255:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20212:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_18BytecodeLoopHeaderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_18BytecodeLoopHeaderE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_18BytecodeLoopHeaderE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_18BytecodeLoopHeaderE:
.LFB20213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	464(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3262
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
.L3262:
	leaq	320(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter14BindLoopHeaderEPNS1_18BytecodeLoopHeaderE@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20213:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_18BytecodeLoopHeaderE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_18BytecodeLoopHeaderE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi:
.LFB20214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	464(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3268
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
.L3268:
	leaq	320(%r12), %rdi
	movl	%r14d, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter18BindJumpTableEntryEPNS1_17BytecodeJumpTableEi@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20214:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_17BytecodeJumpTableEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11MarkHandlerEiNS0_12HandlerTable15CatchPredictionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11MarkHandlerEiNS0_12HandlerTable15CatchPredictionE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11MarkHandlerEiNS0_12HandlerTable15CatchPredictionE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11MarkHandlerEiNS0_12HandlerTable15CatchPredictionE:
.LFB20215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	264(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	%esi, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movq	%r15, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	320(%rdi), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter17BindHandlerTargetEPNS1_19HandlerTableBuilderEi@PLT
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter19HandlerTableBuilder13SetPredictionEiNS0_12HandlerTable15CatchPredictionE@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20215:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11MarkHandlerEiNS0_12HandlerTable15CatchPredictionE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11MarkHandlerEiNS0_12HandlerTable15CatchPredictionE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder12MarkTryBeginEiNS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MarkTryBeginEiNS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MarkTryBeginEiNS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder12MarkTryBeginEiNS1_8RegisterE:
.LFB20216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	464(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3276
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
.L3276:
	leaq	264(%r12), %r15
	leaq	320(%r12), %rdi
	movl	%r13d, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter18BindTryRegionStartEPNS1_19HandlerTableBuilderEi@PLT
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter19HandlerTableBuilder18SetContextRegisterEiNS1_8RegisterE@PLT
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20216:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MarkTryBeginEiNS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder12MarkTryBeginEiNS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder10MarkTryEndEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder10MarkTryEndEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder10MarkTryEndEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder10MarkTryEndEi:
.LFB20217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	leaq	264(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	320(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter16BindTryRegionEndEPNS1_19HandlerTableBuilderEi@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20217:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder10MarkTryEndEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder10MarkTryEndEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE:
.LFB20218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3284
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
.L3284:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L3285
	cmpb	$2, %al
	je	.L3286
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L3285
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	%al, -32(%rbp)
	movabsq	$4294967296, %rcx
	movb	$0, 472(%r12)
	movl	%edx, -28(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	$-117, -64(%rbp)
	movq	%rcx, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3288
	cmpb	$1, %al
	jne	.L3289
	cmpb	$2, %dl
	jne	.L3289
	movb	$2, -32(%rbp)
	jmp	.L3289
	.p2align 4,,10
	.p2align 3
.L3285:
	movabsq	$4294967296, %rax
	pxor	%xmm0, %xmm0
	movb	$-117, -64(%rbp)
	movq	%rax, -44(%rbp)
	movzbl	480(%r12), %eax
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%al, %al
	je	.L3288
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L3289:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3288:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3311
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3286:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movl	%eax, -28(%rbp)
	movabsq	$4294967296, %rax
	movb	$-117, -64(%rbp)
	movb	$2, -32(%rbp)
	movq	%rax, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	jne	.L3289
	jmp	.L3288
.L3311:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20218:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE:
.LFB20219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %esi
	je	.L3338
	movq	464(%rdi), %r14
	testq	%r14, %r14
	je	.L3315
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3315:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	jne	.L3339
	pxor	%xmm0, %xmm0
	movb	$-105, -80(%rbp)
	movabsq	$4294967296, %rax
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movq	%rax, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L3318
	movl	484(%r12), %eax
	movb	%dl, -48(%rbp)
	movl	%eax, -44(%rbp)
.L3319:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3318:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
.L3314:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3340
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3339:
	.cfi_restore_state
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$-105, -80(%rbp)
	movb	$0, 472(%r12)
	movl	%ecx, -44(%rbp)
	movabsq	$4294967296, %rcx
	movl	$-1, 476(%r12)
	movb	%al, -48(%rbp)
	movq	%rcx, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L3318
	cmpb	$1, %al
	jne	.L3319
	cmpb	$2, %dl
	jne	.L3319
	movb	$2, -48(%rbp)
	jmp	.L3319
	.p2align 4,,10
	.p2align 3
.L3338:
	movq	%rdx, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0
	jmp	.L3314
.L3340:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20219:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE:
.LFB20220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %esi
	je	.L3367
	movq	464(%rdi), %r14
	testq	%r14, %r14
	je	.L3344
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3344:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	jne	.L3368
	pxor	%xmm0, %xmm0
	movb	$-104, -80(%rbp)
	movabsq	$4294967296, %rax
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movq	%rax, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L3347
	movl	484(%r12), %eax
	movb	%dl, -48(%rbp)
	movl	%eax, -44(%rbp)
.L3348:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3347:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
.L3343:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3369
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3368:
	.cfi_restore_state
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$-104, -80(%rbp)
	movb	$0, 472(%r12)
	movl	%ecx, -44(%rbp)
	movabsq	$4294967296, %rcx
	movl	$-1, 476(%r12)
	movb	%al, -48(%rbp)
	movq	%rcx, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L3347
	cmpb	$1, %al
	jne	.L3348
	cmpb	$2, %dl
	jne	.L3348
	movb	$2, -48(%rbp)
	jmp	.L3348
	.p2align 4,,10
	.p2align 3
.L3367:
	movq	%rdx, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0
	jmp	.L3343
.L3369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20220:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfNullEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfNullEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfNullEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfNullEPNS1_13BytecodeLabelE:
.LFB20221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L3371
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3371:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L3372
	cmpb	$2, %al
	je	.L3373
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L3372
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	%al, -48(%rbp)
	movabsq	$4294967296, %rcx
	movb	$0, 472(%r12)
	movl	%edx, -44(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	$-101, -80(%rbp)
	movq	%rcx, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L3375
	cmpb	$1, %al
	jne	.L3376
	cmpb	$2, %dl
	jne	.L3376
	movb	$2, -48(%rbp)
	jmp	.L3376
	.p2align 4,,10
	.p2align 3
.L3372:
	movabsq	$4294967296, %rax
	pxor	%xmm0, %xmm0
	movb	$-101, -80(%rbp)
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%al, %al
	je	.L3375
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3376:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3375:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3398
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3373:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movl	%eax, -44(%rbp)
	movabsq	$4294967296, %rax
	movb	$-101, -80(%rbp)
	movb	$2, -48(%rbp)
	movq	%rax, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	jne	.L3376
	jmp	.L3375
.L3398:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20221:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfNullEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfNullEPNS1_13BytecodeLabelE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder13JumpIfNotNullEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder13JumpIfNotNullEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder13JumpIfNotNullEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder13JumpIfNotNullEPNS1_13BytecodeLabelE:
.LFB20222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L3400
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3400:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L3401
	cmpb	$2, %al
	je	.L3402
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L3401
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	%al, -48(%rbp)
	movabsq	$4294967296, %rcx
	movb	$0, 472(%r12)
	movl	%edx, -44(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	$-100, -80(%rbp)
	movq	%rcx, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L3404
	cmpb	$1, %al
	jne	.L3405
	cmpb	$2, %dl
	jne	.L3405
	movb	$2, -48(%rbp)
	jmp	.L3405
	.p2align 4,,10
	.p2align 3
.L3401:
	movabsq	$4294967296, %rax
	pxor	%xmm0, %xmm0
	movb	$-100, -80(%rbp)
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%al, %al
	je	.L3404
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3405:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3404:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3427
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3402:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movl	%eax, -44(%rbp)
	movabsq	$4294967296, %rax
	movb	$-100, -80(%rbp)
	movb	$2, -48(%rbp)
	movq	%rax, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	jne	.L3405
	jmp	.L3404
.L3427:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20222:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder13JumpIfNotNullEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder13JumpIfNotNullEPNS1_13BytecodeLabelE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder15JumpIfUndefinedEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder15JumpIfUndefinedEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder15JumpIfUndefinedEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder15JumpIfUndefinedEPNS1_13BytecodeLabelE:
.LFB20223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L3429
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3429:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L3430
	cmpb	$2, %al
	je	.L3431
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L3430
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	%al, -48(%rbp)
	movabsq	$4294967296, %rcx
	movb	$0, 472(%r12)
	movl	%edx, -44(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	$-99, -80(%rbp)
	movq	%rcx, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L3433
	cmpb	$1, %al
	jne	.L3434
	cmpb	$2, %dl
	jne	.L3434
	movb	$2, -48(%rbp)
	jmp	.L3434
	.p2align 4,,10
	.p2align 3
.L3430:
	movabsq	$4294967296, %rax
	pxor	%xmm0, %xmm0
	movb	$-99, -80(%rbp)
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%al, %al
	je	.L3433
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3434:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3433:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3456
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3431:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movl	%eax, -44(%rbp)
	movabsq	$4294967296, %rax
	movb	$-99, -80(%rbp)
	movb	$2, -48(%rbp)
	movq	%rax, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	jne	.L3434
	jmp	.L3433
.L3456:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20223:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder15JumpIfUndefinedEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder15JumpIfUndefinedEPNS1_13BytecodeLabelE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE:
.LFB20224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L3458
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3458:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L3459
	cmpb	$2, %al
	je	.L3460
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L3459
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	%al, -48(%rbp)
	movabsq	$4294967296, %rcx
	movb	$0, 472(%r12)
	movl	%edx, -44(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	$-97, -80(%rbp)
	movq	%rcx, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L3462
	cmpb	$1, %al
	jne	.L3463
	cmpb	$2, %dl
	jne	.L3463
	movb	$2, -48(%rbp)
	jmp	.L3463
	.p2align 4,,10
	.p2align 3
.L3459:
	movabsq	$4294967296, %rax
	pxor	%xmm0, %xmm0
	movb	$-97, -80(%rbp)
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%al, %al
	je	.L3462
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3463:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3462:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3485
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3460:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movl	%eax, -44(%rbp)
	movabsq	$4294967296, %rax
	movb	$-97, -80(%rbp)
	movb	$2, -48(%rbp)
	movq	%rax, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	jne	.L3463
	jmp	.L3462
.L3485:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20224:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder21JumpIfUndefinedOrNullEPNS1_13BytecodeLabelE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder18JumpIfNotUndefinedEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder18JumpIfNotUndefinedEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder18JumpIfNotUndefinedEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder18JumpIfNotUndefinedEPNS1_13BytecodeLabelE:
.LFB20225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L3487
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3487:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L3488
	cmpb	$2, %al
	je	.L3489
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L3488
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	%al, -48(%rbp)
	movabsq	$4294967296, %rcx
	movb	$0, 472(%r12)
	movl	%edx, -44(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	$-98, -80(%rbp)
	movq	%rcx, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L3491
	cmpb	$1, %al
	jne	.L3492
	cmpb	$2, %dl
	jne	.L3492
	movb	$2, -48(%rbp)
	jmp	.L3492
	.p2align 4,,10
	.p2align 3
.L3488:
	movabsq	$4294967296, %rax
	pxor	%xmm0, %xmm0
	movb	$-98, -80(%rbp)
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%al, %al
	je	.L3491
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3492:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3491:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3514
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3489:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movl	%eax, -44(%rbp)
	movabsq	$4294967296, %rax
	movb	$-98, -80(%rbp)
	movb	$2, -48(%rbp)
	movq	%rax, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	jne	.L3492
	jmp	.L3491
.L3514:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20225:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder18JumpIfNotUndefinedEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder18JumpIfNotUndefinedEPNS1_13BytecodeLabelE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder9JumpIfNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder9JumpIfNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder9JumpIfNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder9JumpIfNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE:
.LFB20226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$53, %dl
	je	.L3545
	cmpl	$1, %ecx
	je	.L3546
	movq	464(%rdi), %r14
	testq	%r14, %r14
	je	.L3519
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3519:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L3520
	cmpb	$2, %al
	je	.L3521
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L3520
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	%al, -48(%rbp)
	movabsq	$4294967296, %rcx
	movb	$0, 472(%r12)
	movl	%edx, -44(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	$-101, -80(%rbp)
	movq	%rcx, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L3523
	cmpb	$1, %al
	jne	.L3524
	cmpb	$2, %dl
	jne	.L3524
	movb	$2, -48(%rbp)
	jmp	.L3524
	.p2align 4,,10
	.p2align 3
.L3520:
	movabsq	$4294967296, %rax
	pxor	%xmm0, %xmm0
	movb	$-101, -80(%rbp)
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%al, %al
	je	.L3523
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3524:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3523:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
	movq	%r12, %rax
.L3515:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L3547
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3546:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15JumpIfUndefinedEPNS1_13BytecodeLabelE
	jmp	.L3515
	.p2align 4,,10
	.p2align 3
.L3545:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CompareUndetectableEv
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0
	movq	-88(%rbp), %rax
	jmp	.L3515
	.p2align 4,,10
	.p2align 3
.L3521:
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movl	%eax, -44(%rbp)
	movabsq	$4294967296, %rax
	movb	$-101, -80(%rbp)
	movb	$2, -48(%rbp)
	movq	%rax, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	jne	.L3524
	jmp	.L3523
.L3547:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20226:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder9JumpIfNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder9JumpIfNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder12JumpIfNotNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder12JumpIfNotNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder12JumpIfNotNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder12JumpIfNotNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE:
.LFB20227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$53, %dl
	je	.L3578
	cmpl	$1, %ecx
	je	.L3579
	movq	464(%rdi), %r14
	testq	%r14, %r14
	je	.L3552
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3552:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L3553
	cmpb	$2, %al
	je	.L3554
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L3553
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	%al, -48(%rbp)
	movabsq	$4294967296, %rcx
	movb	$0, 472(%r12)
	movl	%edx, -44(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	$-100, -80(%rbp)
	movq	%rcx, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L3556
	cmpb	$1, %al
	jne	.L3557
	cmpb	$2, %dl
	jne	.L3557
	movb	$2, -48(%rbp)
	jmp	.L3557
	.p2align 4,,10
	.p2align 3
.L3553:
	movabsq	$4294967296, %rax
	pxor	%xmm0, %xmm0
	movb	$-100, -80(%rbp)
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%al, %al
	je	.L3556
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3557:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3556:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
	movq	%r12, %rax
.L3548:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L3580
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3579:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder18JumpIfNotUndefinedEPNS1_13BytecodeLabelE
	jmp	.L3548
	.p2align 4,,10
	.p2align 3
.L3578:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder19CompareUndetectableEv
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE.part.0
	movq	-88(%rbp), %rax
	jmp	.L3548
	.p2align 4,,10
	.p2align 3
.L3554:
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movl	%eax, -44(%rbp)
	movabsq	$4294967296, %rax
	movb	$-100, -80(%rbp)
	movb	$2, -48(%rbp)
	movq	%rax, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	jne	.L3557
	jmp	.L3556
.L3580:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20227:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder12JumpIfNotNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder12JumpIfNotNilEPNS1_13BytecodeLabelENS0_5Token5ValueENS2_8NilValueE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder16JumpIfJSReceiverEPNS1_13BytecodeLabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder16JumpIfJSReceiverEPNS1_13BytecodeLabelE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder16JumpIfJSReceiverEPNS1_13BytecodeLabelE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder16JumpIfJSReceiverEPNS1_13BytecodeLabelE:
.LFB20228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L3582
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r14), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3582:
	movzbl	472(%r12), %eax
	testb	%al, %al
	je	.L3583
	cmpb	$2, %al
	je	.L3584
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L3583
	movl	476(%r12), %edx
	pxor	%xmm0, %xmm0
	movb	%al, -48(%rbp)
	movabsq	$4294967296, %rcx
	movb	$0, 472(%r12)
	movl	%edx, -44(%rbp)
	movzbl	480(%r12), %edx
	movl	$-1, 476(%r12)
	movb	$-96, -80(%rbp)
	movq	%rcx, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%dl, %dl
	je	.L3586
	cmpb	$1, %al
	jne	.L3587
	cmpb	$2, %dl
	jne	.L3587
	movb	$2, -48(%rbp)
	jmp	.L3587
	.p2align 4,,10
	.p2align 3
.L3583:
	movabsq	$4294967296, %rax
	pxor	%xmm0, %xmm0
	movb	$-96, -80(%rbp)
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$0, -48(%rbp)
	movl	$-1, -44(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	testb	%al, %al
	je	.L3586
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3587:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3586:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter9WriteJumpEPNS1_12BytecodeNodeEPNS1_13BytecodeLabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3609
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3584:
	.cfi_restore_state
	movl	476(%r12), %eax
	pxor	%xmm0, %xmm0
	cmpb	$0, 480(%r12)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movl	%eax, -44(%rbp)
	movabsq	$4294967296, %rax
	movb	$-96, -80(%rbp)
	movb	$2, -48(%rbp)
	movq	%rax, -60(%rbp)
	movb	$1, -52(%rbp)
	movups	%xmm0, -76(%rbp)
	jne	.L3587
	jmp	.L3586
.L3609:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20228:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder16JumpIfJSReceiverEPNS1_13BytecodeLabelE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder16JumpIfJSReceiverEPNS1_13BytecodeLabelE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder8JumpLoopEPNS1_18BytecodeLoopHeaderEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder8JumpLoopEPNS1_18BytecodeLoopHeaderEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder8JumpLoopEPNS1_18BytecodeLoopHeaderEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder8JumpLoopEPNS1_18BytecodeLoopHeaderEi:
.LFB20229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3611
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
.L3611:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L3612
	cmpb	$2, %dl
	je	.L3613
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L3620
.L3613:
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L3612:
	leal	128(%rbx), %esi
	movl	$1, %eax
	cmpl	$255, %esi
	jbe	.L3614
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L3614:
	movb	%al, -52(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$-118, -80(%rbp)
	movb	%dl, -48(%rbp)
	movl	%ecx, -44(%rbp)
	movl	$0, -76(%rbp)
	movl	%ebx, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L3615
	testb	%dl, %dl
	jne	.L3616
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3617:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3615:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter13WriteJumpLoopEPNS1_12BytecodeNodeEPNS1_18BytecodeLoopHeaderE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3640
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3616:
	.cfi_restore_state
	cmpb	$1, %dl
	jne	.L3617
	cmpb	$2, %al
	jne	.L3617
	movb	$2, -48(%rbp)
	jmp	.L3617
	.p2align 4,,10
	.p2align 3
.L3620:
	xorl	%edx, %edx
	jmp	.L3612
.L3640:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20229:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder8JumpLoopEPNS1_18BytecodeLoopHeaderEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder8JumpLoopEPNS1_18BytecodeLoopHeaderEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder21SwitchOnSmiNoFeedbackEPNS1_17BytecodeJumpTableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder21SwitchOnSmiNoFeedbackEPNS1_17BytecodeJumpTableE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder21SwitchOnSmiNoFeedbackEPNS1_17BytecodeJumpTableE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder21SwitchOnSmiNoFeedbackEPNS1_17BytecodeJumpTableE:
.LFB20230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	464(%rdi), %r15
	movl	20(%rsi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %r8
	movl	16(%rsi), %r9d
	movq	(%rsi), %rbx
	testq	%r15, %r15
	je	.L3642
	movq	%r15, %rdi
	movq	%r8, -112(%rbp)
	movl	%r9d, -100(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r15), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movq	-112(%rbp), %r8
	movl	-100(%rbp), %r9d
.L3642:
	movzbl	472(%r12), %ecx
	movl	$-1, %esi
	testb	%cl, %cl
	je	.L3643
	cmpb	$2, %cl
	je	.L3644
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L3655
.L3644:
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L3643:
	cmpl	$255, %ebx
	jbe	.L3656
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L3645:
	cmpl	$255, %r9d
	jbe	.L3646
	cmpl	$65536, %r9d
	movl	$4, %eax
	cmovb	%edx, %eax
.L3646:
	leal	128(%r14), %edx
	cmpl	$255, %edx
	jbe	.L3648
	addl	$32768, %r14d
	cmpl	$65536, %r14d
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L3648:
	movb	%al, -68(%rbp)
	movzbl	480(%r12), %eax
	movb	$-95, -96(%rbp)
	movb	%cl, -64(%rbp)
	movl	%esi, -60(%rbp)
	movl	%ebx, -92(%rbp)
	movq	%r8, -88(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	testb	%al, %al
	je	.L3650
	testb	%cl, %cl
	jne	.L3651
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L3652:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3650:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r13, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3677
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3651:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L3652
	cmpb	$1, %cl
	jne	.L3652
	movb	$2, -64(%rbp)
	jmp	.L3652
	.p2align 4,,10
	.p2align 3
.L3656:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L3645
	.p2align 4,,10
	.p2align 3
.L3655:
	xorl	%ecx, %ecx
	jmp	.L3643
.L3677:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20230:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder21SwitchOnSmiNoFeedbackEPNS1_17BytecodeJumpTableE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder21SwitchOnSmiNoFeedbackEPNS1_17BytecodeJumpTableE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder10StackCheckEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder10StackCheckEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder10StackCheckEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder10StackCheckEi:
.LFB20231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movzbl	480(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	cmpl	$-1, %esi
	je	.L3701
.L3679:
	pxor	%xmm0, %xmm0
	movb	$-89, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movl	%esi, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3682
	cmpb	$1, %al
	jne	.L3683
	cmpb	$2, %dl
	jne	.L3683
	movb	$2, -32(%rbp)
	jmp	.L3683
	.p2align 4,,10
	.p2align 3
.L3701:
	movzbl	472(%rdi), %eax
	testb	%al, %al
	jne	.L3702
	pxor	%xmm0, %xmm0
	movb	$-89, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3682
	movl	484(%r12), %eax
	movb	%dl, -32(%rbp)
	movl	%eax, -28(%rbp)
.L3683:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3682:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3703
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3703:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
.L3702:
	movl	476(%rdi), %esi
	jmp	.L3679
	.cfi_endproc
.LFE20231:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder10StackCheckEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder10StackCheckEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder17SetPendingMessageEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder17SetPendingMessageEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder17SetPendingMessageEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder17SetPendingMessageEv:
.LFB20232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L3705
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L3705:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	je	.L3706
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$-88, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3708
	cmpb	$1, %al
	jne	.L3709
	cmpb	$2, %dl
	jne	.L3709
	movb	$2, -32(%rbp)
	jmp	.L3709
	.p2align 4,,10
	.p2align 3
.L3706:
	pxor	%xmm0, %xmm0
	movb	$-88, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3708
	movl	484(%r12), %eax
	movb	%dl, -32(%rbp)
	movl	%eax, -28(%rbp)
.L3709:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3708:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3728
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3728:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20232:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder17SetPendingMessageEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder17SetPendingMessageEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder5ThrowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder5ThrowEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder5ThrowEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder5ThrowEv:
.LFB20233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3730
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3730:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	je	.L3731
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$-87, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3733
	cmpb	$1, %al
	jne	.L3734
	cmpb	$2, %dl
	jne	.L3734
	movb	$2, -32(%rbp)
	jmp	.L3734
	.p2align 4,,10
	.p2align 3
.L3731:
	pxor	%xmm0, %xmm0
	movb	$-87, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3733
	movl	484(%r12), %eax
	movb	%dl, -32(%rbp)
	movl	%eax, -28(%rbp)
.L3734:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3733:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3753
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3753:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20233:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder5ThrowEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder5ThrowEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder7ReThrowEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder7ReThrowEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder7ReThrowEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder7ReThrowEv:
.LFB20234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3755
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3755:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	je	.L3756
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$-86, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3758
	cmpb	$1, %al
	jne	.L3759
	cmpb	$2, %dl
	jne	.L3759
	movb	$2, -32(%rbp)
	jmp	.L3759
	.p2align 4,,10
	.p2align 3
.L3756:
	pxor	%xmm0, %xmm0
	movb	$-86, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3758
	movl	484(%r12), %eax
	movb	%dl, -32(%rbp)
	movl	%eax, -28(%rbp)
.L3759:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3758:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3778
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3778:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20234:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder7ReThrowEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder7ReThrowEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder5AbortENS0_11AbortReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder5AbortENS0_11AbortReasonE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder5AbortENS0_11AbortReasonE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder5AbortENS0_11AbortReasonE:
.LFB20235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%sil, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movzbl	480(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	472(%rdi), %eax
	testb	%al, %al
	je	.L3780
	movl	476(%rdi), %ecx
	movb	$0, 472(%rdi)
	movl	$-1, 476(%rdi)
	movb	$-75, -64(%rbp)
	movb	%al, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movl	%esi, -60(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	movb	$1, -36(%rbp)
	testb	%dl, %dl
	je	.L3782
	cmpb	$1, %al
	jne	.L3783
	cmpb	$2, %dl
	jne	.L3783
	movb	$2, -32(%rbp)
	jmp	.L3783
	.p2align 4,,10
	.p2align 3
.L3780:
	movb	$-75, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movl	%esi, -60(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	movb	$1, -36(%rbp)
	testb	%dl, %dl
	je	.L3782
	movl	484(%r12), %eax
	movb	%dl, -32(%rbp)
	movl	%eax, -28(%rbp)
.L3783:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3782:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3799
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3799:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20235:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder5AbortENS0_11AbortReasonE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder5AbortENS0_11AbortReasonE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder6ReturnEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder6ReturnEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder6ReturnEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder6ReturnEv:
.LFB20236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3801
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3801:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	je	.L3802
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$-85, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3804
	cmpb	$1, %al
	jne	.L3805
	cmpb	$2, %dl
	jne	.L3805
	movb	$2, -32(%rbp)
	jmp	.L3805
	.p2align 4,,10
	.p2align 3
.L3802:
	pxor	%xmm0, %xmm0
	movb	$-85, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3804
	movl	484(%r12), %eax
	movb	%dl, -32(%rbp)
	movl	%eax, -28(%rbp)
.L3805:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3804:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3824
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3824:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20236:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder6ReturnEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder6ReturnEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowReferenceErrorIfHoleEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowReferenceErrorIfHoleEPKNS0_12AstRawStringE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowReferenceErrorIfHoleEPKNS0_12AstRawStringE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowReferenceErrorIfHoleEPKNS0_12AstRawStringE:
.LFB20237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L3826
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3826:
	movzbl	472(%r12), %edx
	movl	$-1, %ecx
	testb	%dl, %dl
	je	.L3827
	movl	476(%r12), %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L3827:
	movl	$1, %eax
	cmpl	$255, %ebx
	jbe	.L3828
	cmpl	$65536, %ebx
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L3828:
	movb	%al, -36(%rbp)
	movzbl	480(%r12), %eax
	movb	$-84, -64(%rbp)
	movb	%dl, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movl	%ebx, -60(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L3829
	testb	%dl, %dl
	jne	.L3830
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L3831:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3829:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3850
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3830:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L3831
	cmpb	$1, %dl
	jne	.L3831
	movb	$2, -32(%rbp)
	jmp	.L3831
.L3850:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20237:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowReferenceErrorIfHoleEPKNS0_12AstRawStringE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowReferenceErrorIfHoleEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowSuperNotCalledIfHoleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowSuperNotCalledIfHoleEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowSuperNotCalledIfHoleEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowSuperNotCalledIfHoleEv:
.LFB20238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3852
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3852:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	je	.L3853
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$-83, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3855
	cmpb	$1, %al
	jne	.L3856
	cmpb	$2, %dl
	jne	.L3856
	movb	$2, -32(%rbp)
	jmp	.L3856
	.p2align 4,,10
	.p2align 3
.L3853:
	pxor	%xmm0, %xmm0
	movb	$-83, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3855
	movl	484(%r12), %eax
	movb	%dl, -32(%rbp)
	movl	%eax, -28(%rbp)
.L3856:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3855:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3875
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3875:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20238:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowSuperNotCalledIfHoleEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder25ThrowSuperNotCalledIfHoleEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder32ThrowSuperAlreadyCalledIfNotHoleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder32ThrowSuperAlreadyCalledIfNotHoleEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder32ThrowSuperAlreadyCalledIfNotHoleEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder32ThrowSuperAlreadyCalledIfNotHoleEv:
.LFB20239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3877
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L3877:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	je	.L3878
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$-82, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3880
	cmpb	$1, %al
	jne	.L3881
	cmpb	$2, %dl
	jne	.L3881
	movb	$2, -32(%rbp)
	jmp	.L3881
	.p2align 4,,10
	.p2align 3
.L3878:
	pxor	%xmm0, %xmm0
	movb	$-82, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3880
	movl	484(%r12), %eax
	movb	%dl, -32(%rbp)
	movl	%eax, -28(%rbp)
.L3881:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3880:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3900
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3900:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20239:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder32ThrowSuperAlreadyCalledIfNotHoleEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder32ThrowSuperAlreadyCalledIfNotHoleEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder8DebuggerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder8DebuggerEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder8DebuggerEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder8DebuggerEv:
.LFB20240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3902
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
.L3902:
	movzbl	472(%r12), %eax
	movzbl	480(%r12), %edx
	testb	%al, %al
	je	.L3903
	movl	476(%r12), %ecx
	pxor	%xmm0, %xmm0
	movb	$-77, -64(%rbp)
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	movb	%al, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3905
	cmpb	$1, %al
	jne	.L3906
	cmpb	$2, %dl
	jne	.L3906
	movb	$2, -32(%rbp)
	jmp	.L3906
	.p2align 4,,10
	.p2align 3
.L3903:
	pxor	%xmm0, %xmm0
	movb	$-77, -64(%rbp)
	movb	$0, -32(%rbp)
	movl	$-1, -28(%rbp)
	movq	$0, -44(%rbp)
	movb	$1, -36(%rbp)
	movups	%xmm0, -60(%rbp)
	testb	%dl, %dl
	je	.L3905
	movl	484(%r12), %eax
	movb	%dl, -32(%rbp)
	movl	%eax, -28(%rbp)
.L3906:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3905:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3925
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3925:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20240:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder8DebuggerEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder8DebuggerEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi:
.LFB20241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movzbl	472(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	%dl, %dl
	je	.L3927
	movl	476(%rdi), %ecx
	movb	$0, 472(%rdi)
	movl	$-1, 476(%rdi)
.L3927:
	movl	$1, %eax
	cmpl	$255, %esi
	jbe	.L3928
	cmpl	$65536, %esi
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L3928:
	movb	%al, -36(%rbp)
	movzbl	480(%r12), %eax
	movb	$-76, -64(%rbp)
	movb	%dl, -32(%rbp)
	movl	%ecx, -28(%rbp)
	movl	%esi, -60(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L3929
	testb	%dl, %dl
	jne	.L3930
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L3931:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3929:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3948
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3930:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L3931
	cmpb	$1, %dl
	jne	.L3931
	movb	$2, -32(%rbp)
	jmp	.L3931
.L3948:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20241:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder14ForInEnumerateENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder14ForInEnumerateENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder14ForInEnumerateENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder14ForInEnumerateENS1_8RegisterE:
.LFB20242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3950
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3950
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L3950:
	movzbl	472(%r12), %ecx
	movl	$-5, %eax
	movl	$-1, %esi
	subl	%r13d, %eax
	testb	%cl, %cl
	je	.L3951
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L3951:
	leal	128(%rax), %edi
	movl	$1, %edx
	cmpl	$255, %edi
	jbe	.L3952
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L3952:
	movl	%eax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$-94, -64(%rbp)
	movb	%dl, -36(%rbp)
	movb	%cl, -32(%rbp)
	movl	%esi, -28(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L3953
	testb	%cl, %cl
	jne	.L3954
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L3955:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3953:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3977
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3954:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L3955
	cmpb	$1, %cl
	jne	.L3955
	movb	$2, -32(%rbp)
	jmp	.L3955
.L3977:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20242:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder14ForInEnumerateENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder14ForInEnumerateENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder12ForInPrepareENS1_12RegisterListEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder12ForInPrepareENS1_12RegisterListEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder12ForInPrepareENS1_12RegisterListEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder12ForInPrepareENS1_12RegisterListEi:
.LFB20243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	sarq	$32, %r14
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3980
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3980
	movq	%r14, %rax
	movl	%r13d, %esi
	salq	$32, %rax
	orq	%rax, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25PrepareOutputRegisterListENS1_12RegisterListE@PLT
.L3980:
	movzbl	472(%r12), %esi
	testl	%r14d, %r14d
	je	.L3981
	movl	$-5, %ecx
	movl	$-1, %edi
	subl	%r13d, %ecx
	leal	128(%rcx), %eax
	testb	%sil, %sil
	je	.L3982
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L3982:
	cmpl	$255, %eax
	jbe	.L3990
	leal	32768(%rcx), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L3984:
	cmpl	$255, %ebx
	jbe	.L3985
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L3985:
	movb	%al, -52(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$-93, -80(%rbp)
	movb	%sil, -48(%rbp)
	movl	%edi, -44(%rbp)
	movl	%ecx, -76(%rbp)
	movl	%ebx, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L3987
	testb	%sil, %sil
	jne	.L3988
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L3989:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L3987:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4012
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3981:
	.cfi_restore_state
	testb	%sil, %sil
	jne	.L4013
	movl	$-5, %ecx
	movl	$-1, %edi
.L3990:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L3984
	.p2align 4,,10
	.p2align 3
.L3988:
	cmpb	$2, %al
	jne	.L3989
	cmpb	$1, %sil
	jne	.L3989
	movb	$2, -48(%rbp)
	jmp	.L3989
	.p2align 4,,10
	.p2align 3
.L4013:
	movl	476(%r12), %edi
	movl	$-5, %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L3990
.L4012:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20243:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder12ForInPrepareENS1_12RegisterListEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder12ForInPrepareENS1_12RegisterListEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder13ForInContinueENS1_8RegisterES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder13ForInContinueENS1_8RegisterES3_
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder13ForInContinueENS1_8RegisterES3_, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder13ForInContinueENS1_8RegisterES3_:
.LFB20244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L4015
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4015
	movl	%r14d, %esi
	movl	$-5, %ebx
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	subl	%eax, %ebx
	testq	%rdi, %rdi
	je	.L4016
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L4016:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L4017
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4017:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L4026
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4018:
	leal	128(%rbx), %r8d
	cmpl	$255, %r8d
	jbe	.L4019
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%ecx, %eax
.L4019:
	movb	%al, -52(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$-92, -80(%rbp)
	movb	%sil, -48(%rbp)
	movl	%edi, -44(%rbp)
	movl	%edx, -76(%rbp)
	movl	%ebx, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L4021
	testb	%sil, %sil
	jne	.L4022
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L4023:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4021:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4050
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4022:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L4023
	cmpb	$1, %sil
	jne	.L4023
	movb	$2, -48(%rbp)
	jmp	.L4023
	.p2align 4,,10
	.p2align 3
.L4015:
	movl	$-5, %ebx
	subl	%r14d, %ebx
	jmp	.L4016
	.p2align 4,,10
	.p2align 3
.L4026:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L4018
.L4050:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20244:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder13ForInContinueENS1_8RegisterES3_, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder13ForInContinueENS1_8RegisterES3_
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInNextENS1_8RegisterES3_NS1_12RegisterListEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInNextENS1_8RegisterES3_NS1_12RegisterListEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInNextENS1_8RegisterES3_NS1_12RegisterListEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInNextENS1_8RegisterES3_NS1_12RegisterListEi:
.LFB20245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r10d
	movl	%ecx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	sarq	$32, %r15
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	%r8d, -100(%rbp)
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L4053
	movl	8(%rdi), %esi
	movl	%r10d, -108(%rbp)
	movq	%rcx, %rbx
	movl	%ecx, -104(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	movl	-104(%rbp), %edx
	movl	-108(%rbp), %r10d
	testq	%rdi, %rdi
	je	.L4053
	salq	$32, %r15
	movl	%ebx, %esi
	movl	%r10d, -104(%rbp)
	orq	%r15, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movq	464(%r12), %rdi
	movl	-104(%rbp), %r10d
	movl	%eax, %edx
	sarq	$32, %rax
	testl	%eax, %eax
	je	.L4070
.L4068:
	movl	$-5, %ebx
	subl	%edx, %ebx
	leal	128(%rbx), %r15d
	testq	%rdi, %rdi
	je	.L4055
.L4101:
	movl	%r10d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	movl	$-5, %ecx
	subl	%eax, %ecx
	testq	%rdi, %rdi
	je	.L4056
	movl	%r13d, %esi
	movl	%ecx, -104(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	-104(%rbp), %ecx
	movl	%eax, %r14d
.L4056:
	movzbl	472(%r12), %edi
	movl	$-5, %edx
	movl	$-1, %r8d
	subl	%r14d, %edx
	testb	%dil, %dil
	je	.L4057
	movl	476(%r12), %r8d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4057:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L4072
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%esi, %esi
	andl	$-2, %esi
	addl	$4, %esi
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4058:
	leal	128(%rcx), %r10d
	cmpl	$255, %r10d
	jbe	.L4059
	leal	32768(%rcx), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%esi, %eax
.L4059:
	cmpl	$255, %r15d
	jbe	.L4061
	leal	32768(%rbx), %esi
	cmpl	$65536, %esi
	sbbl	%esi, %esi
	andl	$-2, %esi
	addl	$4, %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
.L4061:
	movl	-100(%rbp), %esi
	cmpl	$255, %esi
	jbe	.L4063
	cmpl	$65536, %esi
	sbbl	%esi, %esi
	andl	$-2, %esi
	addl	$4, %esi
	cmpb	%sil, %al
	cmovb	%esi, %eax
.L4063:
	movd	-100(%rbp), %xmm2
	movb	%al, -68(%rbp)
	movd	%ebx, %xmm1
	movd	%edx, %xmm0
	movabsq	$17179869184, %rax
	movd	%ecx, %xmm3
	movb	$-91, -96(%rbp)
	movq	%rax, -76(%rbp)
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	movzbl	480(%r12), %eax
	punpcklqdq	%xmm1, %xmm0
	movb	%dil, -64(%rbp)
	movl	%r8d, -60(%rbp)
	movups	%xmm0, -92(%rbp)
	testb	%al, %al
	je	.L4065
	testb	%dil, %dil
	jne	.L4066
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L4067:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4065:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4100
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4066:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L4067
	cmpb	$1, %dil
	jne	.L4067
	movb	$2, -64(%rbp)
	jmp	.L4067
	.p2align 4,,10
	.p2align 3
.L4070:
	movl	$-5, %ebx
	movl	$123, %r15d
	testq	%rdi, %rdi
	jne	.L4101
.L4055:
	movl	$-5, %ecx
	subl	%r10d, %ecx
	jmp	.L4056
	.p2align 4,,10
	.p2align 3
.L4053:
	xorl	%edi, %edi
	testl	%r15d, %r15d
	jne	.L4068
	movl	$-5, %ecx
	movl	$-5, %ebx
	movl	$123, %r15d
	subl	%r10d, %ecx
	jmp	.L4056
	.p2align 4,,10
	.p2align 3
.L4072:
	movl	$2, %esi
	movl	$1, %eax
	jmp	.L4058
.L4100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20245:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInNextENS1_8RegisterES3_NS1_12RegisterListEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInNextENS1_8RegisterES3_NS1_12RegisterListEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInStepENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInStepENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInStepENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInStepENS1_8RegisterE:
.LFB20246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L4103
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4103
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L4103:
	movzbl	472(%r12), %ecx
	movl	$-5, %eax
	movl	$-1, %esi
	subl	%r13d, %eax
	testb	%cl, %cl
	je	.L4104
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4104:
	leal	128(%rax), %edi
	movl	$1, %edx
	cmpl	$255, %edi
	jbe	.L4105
	leal	32768(%rax), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
.L4105:
	movl	%eax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$-90, -64(%rbp)
	movb	%dl, -36(%rbp)
	movb	%cl, -32(%rbp)
	movl	%esi, -28(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
	movl	$1, -40(%rbp)
	testb	%al, %al
	je	.L4106
	testb	%cl, %cl
	jne	.L4107
	movl	484(%r12), %edx
	movb	%al, -32(%rbp)
	movl	%edx, -28(%rbp)
.L4108:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4106:
	leaq	-64(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4130
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4107:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L4108
	cmpb	$1, %cl
	jne	.L4108
	movb	$2, -32(%rbp)
	jmp	.L4108
.L4130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20246:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInStepENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder9ForInStepENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreModuleVariableEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreModuleVariableEii
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreModuleVariableEii, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreModuleVariableEii:
.LFB20247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L4132
	movq	16(%rdi), %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
.L4132:
	movzbl	472(%r12), %ecx
	movl	$-1, %esi
	testb	%cl, %cl
	je	.L4133
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4133:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L4142
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4134:
	cmpl	$255, %ebx
	jbe	.L4135
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L4135:
	movb	%al, -52(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$44, -80(%rbp)
	movb	%cl, -48(%rbp)
	movl	%esi, -44(%rbp)
	movl	%r13d, -76(%rbp)
	movl	%ebx, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L4137
	testb	%cl, %cl
	jne	.L4138
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L4139:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4137:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4159
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4138:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L4139
	cmpb	$1, %cl
	jne	.L4139
	movb	$2, -48(%rbp)
	jmp	.L4139
	.p2align 4,,10
	.p2align 3
.L4142:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L4134
.L4159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20247:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreModuleVariableEii, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder19StoreModuleVariableEii
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder18LoadModuleVariableEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder18LoadModuleVariableEii
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder18LoadModuleVariableEii, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder18LoadModuleVariableEii:
.LFB20248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L4161
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L4161:
	movzbl	472(%r12), %ecx
	movl	$-1, %esi
	testb	%cl, %cl
	je	.L4162
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4162:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L4171
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4163:
	cmpl	$255, %ebx
	jbe	.L4164
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L4164:
	movb	%al, -52(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -60(%rbp)
	movzbl	480(%r12), %eax
	movb	$43, -80(%rbp)
	movb	%cl, -48(%rbp)
	movl	%esi, -44(%rbp)
	movl	%r13d, -76(%rbp)
	movl	%ebx, -72(%rbp)
	movq	$0, -68(%rbp)
	testb	%al, %al
	je	.L4166
	testb	%cl, %cl
	jne	.L4167
	movl	484(%r12), %edx
	movb	%al, -48(%rbp)
	movl	%edx, -44(%rbp)
.L4168:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4166:
	leaq	-80(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4188
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4167:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L4168
	cmpb	$1, %cl
	jne	.L4168
	movb	$2, -48(%rbp)
	jmp	.L4168
	.p2align 4,,10
	.p2align 3
.L4171:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L4163
.L4188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20248:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder18LoadModuleVariableEii, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder18LoadModuleVariableEii
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder16SuspendGeneratorENS1_8RegisterENS1_12RegisterListEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder16SuspendGeneratorENS1_8RegisterENS1_12RegisterListEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder16SuspendGeneratorENS1_8RegisterENS1_12RegisterListEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder16SuspendGeneratorENS1_8RegisterENS1_12RegisterListEi:
.LFB20249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	sarq	$32, %rdx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$72, %rsp
	movl	%ecx, -100(%rbp)
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L4190
	movq	%r13, %rdi
	movq	%rdx, -112(%rbp)
	movl	%esi, %r15d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movq	-112(%rbp), %rdx
	movq	464(%r12), %rdi
	movl	%edx, %r13d
	testq	%rdi, %rdi
	je	.L4191
	salq	$32, %rdx
	movl	%r14d, %esi
	orq	%rdx, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rdx
	sarq	$32, %rdx
	testl	%edx, %edx
	je	.L4232
	movl	$-5, %r14d
	subl	%eax, %r14d
	leal	128(%r14), %r8d
.L4193:
	testq	%rdi, %rdi
	je	.L4194
	movl	%r15d, %esi
	movl	%r8d, -112(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	-112(%rbp), %r8d
	movl	%eax, %ebx
.L4194:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%ebx, %edx
	testb	%sil, %sil
	je	.L4195
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4195:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L4208
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4196:
	cmpl	$255, %r8d
	jbe	.L4197
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%ecx, %eax
.L4197:
	cmpl	$255, %r13d
	jbe	.L4199
	cmpl	$65536, %r13d
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L4199:
	movl	-100(%rbp), %ebx
	cmpl	$255, %ebx
	jbe	.L4201
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L4201:
	movd	-100(%rbp), %xmm2
	movb	%al, -68(%rbp)
	movd	%r13d, %xmm1
	movd	%edx, %xmm0
	movabsq	$17179869184, %rax
	movd	%r14d, %xmm3
	movb	$-80, -96(%rbp)
	movq	%rax, -76(%rbp)
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	movzbl	480(%r12), %eax
	punpcklqdq	%xmm1, %xmm0
	movb	%sil, -64(%rbp)
	movl	%edi, -60(%rbp)
	movups	%xmm0, -92(%rbp)
	testb	%al, %al
	je	.L4203
	testb	%sil, %sil
	jne	.L4204
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L4205:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4203:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4233
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4232:
	.cfi_restore_state
	movl	$123, %r8d
	movl	$-5, %r14d
	jmp	.L4193
	.p2align 4,,10
	.p2align 3
.L4190:
	movl	%edx, %r13d
.L4191:
	testl	%edx, %edx
	je	.L4213
	movl	$-5, %eax
	subl	%r14d, %eax
	movl	%eax, %r14d
	leal	128(%rax), %r8d
	jmp	.L4194
	.p2align 4,,10
	.p2align 3
.L4208:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L4196
	.p2align 4,,10
	.p2align 3
.L4204:
	cmpb	$2, %al
	jne	.L4205
	cmpb	$1, %sil
	jne	.L4205
	movb	$2, -64(%rbp)
	jmp	.L4205
	.p2align 4,,10
	.p2align 3
.L4213:
	movl	$123, %r8d
	movl	$-5, %r14d
	jmp	.L4194
.L4233:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20249:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder16SuspendGeneratorENS1_8RegisterENS1_12RegisterListEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder16SuspendGeneratorENS1_8RegisterENS1_12RegisterListEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder22SwitchOnGeneratorStateENS1_8RegisterEPNS1_17BytecodeJumpTableE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder22SwitchOnGeneratorStateENS1_8RegisterEPNS1_17BytecodeJumpTableE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder22SwitchOnGeneratorStateENS1_8RegisterEPNS1_17BytecodeJumpTableE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder22SwitchOnGeneratorStateENS1_8RegisterEPNS1_17BytecodeJumpTableE:
.LFB20250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	464(%rdi), %rdi
	movl	16(%rdx), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rdx), %r15d
	testq	%rdi, %rdi
	je	.L4236
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4236
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L4236:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L4237
	cmpb	$2, %sil
	je	.L4238
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L4249
.L4238:
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4237:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L4250
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4239:
	cmpl	$255, %r15d
	jbe	.L4240
	cmpl	$65536, %r15d
	movl	$4, %eax
	cmovb	%ecx, %eax
.L4240:
	cmpl	$255, %ebx
	jbe	.L4242
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L4242:
	movb	%al, -68(%rbp)
	movzbl	480(%r12), %eax
	movb	$-81, -96(%rbp)
	movb	%sil, -64(%rbp)
	movl	%edi, -60(%rbp)
	movl	%edx, -92(%rbp)
	movl	%r15d, -88(%rbp)
	movl	%ebx, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	testb	%al, %al
	je	.L4244
	testb	%sil, %sil
	jne	.L4245
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L4246:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4244:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter11WriteSwitchEPNS1_12BytecodeNodeEPNS1_17BytecodeJumpTableE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4272
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4245:
	.cfi_restore_state
	cmpb	$2, %al
	jne	.L4246
	cmpb	$1, %sil
	jne	.L4246
	movb	$2, -64(%rbp)
	jmp	.L4246
	.p2align 4,,10
	.p2align 3
.L4250:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L4239
	.p2align 4,,10
	.p2align 3
.L4249:
	xorl	%esi, %esi
	jmp	.L4237
.L4272:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20250:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder22SwitchOnGeneratorStateENS1_8RegisterEPNS1_17BytecodeJumpTableE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder22SwitchOnGeneratorStateENS1_8RegisterEPNS1_17BytecodeJumpTableE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder15ResumeGeneratorENS1_8RegisterENS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder15ResumeGeneratorENS1_8RegisterENS1_12RegisterListE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder15ResumeGeneratorENS1_8RegisterENS1_12RegisterListE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder15ResumeGeneratorENS1_8RegisterENS1_12RegisterListE:
.LFB20251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	sarq	$32, %r14
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, %eax
	testq	%r13, %r13
	je	.L4274
	movq	%r13, %rdi
	movl	%esi, -100(%rbp)
	movl	%esi, %r15d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer5FlushEv@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	movl	%r14d, %r13d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	movl	-100(%rbp), %eax
	testq	%rdi, %rdi
	je	.L4275
	movq	%r14, %rdx
	movl	%ebx, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25PrepareOutputRegisterListENS1_12RegisterListE@PLT
	testl	%r14d, %r14d
	movq	464(%r12), %rdi
	movl	-100(%rbp), %eax
	je	.L4313
	movl	$-5, %edx
	subl	%ebx, %edx
	movl	%edx, %ebx
	leal	128(%rdx), %r14d
.L4277:
	testq	%rdi, %rdi
	je	.L4278
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
.L4278:
	movzbl	472(%r12), %edi
	movl	$-5, %edx
	movl	$-1, %esi
	subl	%eax, %edx
	testb	%dil, %dil
	je	.L4279
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4279:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L4290
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4280:
	cmpl	$255, %r14d
	jbe	.L4281
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%ecx, %eax
.L4281:
	cmpl	$255, %r13d
	jbe	.L4283
	cmpl	$65536, %r13d
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L4283:
	movb	%al, -68(%rbp)
	movzbl	480(%r12), %eax
	movb	$-79, -96(%rbp)
	movb	%dil, -64(%rbp)
	movl	%esi, -60(%rbp)
	movl	%edx, -92(%rbp)
	movl	%ebx, -88(%rbp)
	movl	%r13d, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	testb	%al, %al
	je	.L4285
	testb	%dil, %dil
	jne	.L4286
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L4287:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4285:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4314
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4313:
	.cfi_restore_state
	movl	$123, %r14d
	movl	$-5, %ebx
	jmp	.L4277
	.p2align 4,,10
	.p2align 3
.L4274:
	movl	%r14d, %r13d
.L4275:
	testl	%r14d, %r14d
	je	.L4294
	movl	$-5, %edx
	subl	%ebx, %edx
	movl	%edx, %ebx
	leal	128(%rdx), %r14d
	jmp	.L4278
	.p2align 4,,10
	.p2align 3
.L4290:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L4280
	.p2align 4,,10
	.p2align 3
.L4286:
	cmpb	$2, %al
	jne	.L4287
	cmpb	$1, %dil
	jne	.L4287
	movb	$2, -64(%rbp)
	jmp	.L4287
	.p2align 4,,10
	.p2align 3
.L4294:
	movl	$123, %r14d
	movl	$-5, %ebx
	jmp	.L4278
.L4314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20251:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder15ResumeGeneratorENS1_8RegisterENS1_12RegisterListE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder15ResumeGeneratorENS1_8RegisterENS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi:
.LFB20252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	sarq	$32, %r14
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movl	%ecx, -100(%rbp)
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %r14d
	je	.L4441
	cmpl	$2, %r14d
	je	.L4442
	cmpl	$3, %r14d
	je	.L4443
	testq	%rdi, %rdi
	je	.L4356
	movl	%esi, %r15d
	movl	8(%rdi), %esi
	movq	%rdx, -112(%rbp)
	movl	%r14d, %r13d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%rbx), %rdi
	movq	-112(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L4357
	movl	%edx, %edx
	salq	$32, %r14
	movq	%rdx, %rsi
	orq	%r14, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movq	464(%rbx), %rdi
	movq	%rax, %rdx
	sarq	$32, %rdx
	testl	%edx, %edx
	je	.L4444
	movl	$-5, %r14d
	subl	%eax, %r14d
	leal	128(%r14), %edx
.L4359:
	testq	%rdi, %rdi
	je	.L4360
	movl	%r15d, %esi
	movl	%edx, -112(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	-112(%rbp), %edx
	movl	%eax, %r12d
.L4360:
	movl	$-5, %eax
	movzbl	472(%rbx), %r15d
	movl	$-1, %ecx
	subl	%r12d, %eax
	movl	%eax, %r12d
	testb	%r15b, %r15b
	je	.L4361
	movl	%ecx, -112(%rbp)
	cmpb	$2, %r15b
	je	.L4362
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L4362
	movl	$87, %edi
	movl	%edx, -104(%rbp)
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movl	-104(%rbp), %edx
	movl	$-1, %ecx
	testb	%al, %al
	jne	.L4392
.L4362:
	movl	476(%rbx), %ecx
	movb	$0, 472(%rbx)
	movl	$-1, 476(%rbx)
.L4361:
	leal	128(%r12), %eax
	cmpl	$255, %eax
	jbe	.L4393
	leal	32768(%r12), %eax
	cmpl	$65536, %eax
	sbbl	%esi, %esi
	andl	$-2, %esi
	addl	$4, %esi
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4363:
	cmpl	$255, %edx
	jbe	.L4364
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%esi, %eax
.L4364:
	cmpl	$255, %r13d
	jbe	.L4366
	cmpl	$65536, %r13d
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4366:
	movl	-100(%rbp), %esi
	cmpl	$255, %esi
	jbe	.L4368
	cmpl	$65536, %esi
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4368:
	movd	-100(%rbp), %xmm2
	movb	$87, -96(%rbp)
	movd	%r13d, %xmm1
	movd	%r12d, %xmm0
	movb	%al, -68(%rbp)
	movd	%r14d, %xmm3
	movb	%r15b, -64(%rbp)
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	movl	%ecx, -60(%rbp)
.L4440:
	punpcklqdq	%xmm1, %xmm0
	leaq	-96(%rbp), %rsi
	movq	%rbx, %rdi
	movabsq	$17179869184, %rax
	movq	%rax, -76(%rbp)
	movups	%xmm0, -92(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	jmp	.L4326
	.p2align 4,,10
	.p2align 3
.L4443:
	movq	%rdx, -112(%rbp)
	leal	2(%rdx), %r13d
	leal	1(%rdx), %r14d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE90ELNS1_14AccumulatorUseE2EEEvv.isra.0
	movq	464(%rbx), %rdi
	movq	-112(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L4341
	movl	%r13d, %esi
	movl	$-5, %r13d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movq	464(%rbx), %rdi
	movl	%r13d, %r15d
	movq	-112(%rbp), %rdx
	subl	%eax, %r15d
	testq	%rdi, %rdi
	je	.L4342
	movl	%r14d, %esi
	movl	%r13d, %r14d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movq	464(%rbx), %rdi
	movq	-112(%rbp), %rdx
	subl	%eax, %r14d
	testq	%rdi, %rdi
	je	.L4343
	movl	%edx, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movq	464(%rbx), %rdi
	subl	%eax, %r13d
	testq	%rdi, %rdi
	je	.L4344
	movl	%r12d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r12d
.L4344:
	movl	$-5, %eax
	movzbl	472(%rbx), %ecx
	movl	$-1, %esi
	subl	%r12d, %eax
	movl	%eax, %r12d
	testb	%cl, %cl
	je	.L4345
	movl	%esi, -112(%rbp)
	cmpb	$2, %cl
	je	.L4346
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L4445
.L4346:
	movl	476(%rbx), %esi
	movb	$0, 472(%rbx)
	movl	$-1, 476(%rbx)
.L4345:
	leal	128(%r12), %eax
	cmpl	$255, %eax
	jbe	.L4385
	leal	32768(%r12), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4347:
	leal	128(%r13), %edi
	cmpl	$255, %edi
	jbe	.L4348
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%edx, %eax
.L4348:
	leal	128(%r14), %edx
	cmpl	$255, %edx
	jbe	.L4350
	leal	32768(%r14), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4350:
	leal	128(%r15), %edx
	cmpl	$255, %edx
	jbe	.L4352
	leal	32768(%r15), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4352:
	movl	-100(%rbp), %edi
	cmpl	$255, %edi
	jbe	.L4354
	cmpl	$65536, %edi
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4354:
	movd	%r14d, %xmm1
	movd	%r15d, %xmm4
	movb	%al, -68(%rbp)
	movl	-100(%rbp), %eax
	movd	%r12d, %xmm0
	movd	%r13d, %xmm5
	movl	%esi, -60(%rbp)
	movq	%rbx, %rdi
	punpckldq	%xmm4, %xmm1
	punpckldq	%xmm5, %xmm0
	leaq	-96(%rbp), %rsi
	movb	$90, -96(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movl	$5, -72(%rbp)
	movb	%cl, -64(%rbp)
	movl	%eax, -76(%rbp)
	movups	%xmm0, -92(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
.L4326:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4446
	addq	$72, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4441:
	.cfi_restore_state
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE88ELNS1_14AccumulatorUseE2EEEvv.isra.0
	movq	464(%rbx), %rdi
	movq	-112(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L4317
	movl	%edx, %esi
	movl	$-5, %r13d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movq	464(%rbx), %rdi
	subl	%eax, %r13d
	testq	%rdi, %rdi
	je	.L4318
	movl	%r12d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r12d
.L4318:
	movl	$-5, %eax
	movzbl	472(%rbx), %r14d
	movl	$-1, %r15d
	subl	%r12d, %eax
	movl	%eax, %r12d
	testb	%r14b, %r14b
	je	.L4319
	cmpb	$2, %r14b
	je	.L4320
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L4447
.L4320:
	movl	476(%rbx), %r15d
	movb	$0, 472(%rbx)
	movl	$-1, 476(%rbx)
.L4319:
	leal	128(%r12), %eax
	cmpl	$255, %eax
	jbe	.L4373
	leal	32768(%r12), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4321:
	leal	128(%r13), %ecx
	cmpl	$255, %ecx
	jbe	.L4322
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%edx, %eax
.L4322:
	movl	-100(%rbp), %ecx
	cmpl	$255, %ecx
	jbe	.L4324
	cmpl	$65536, %ecx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4324:
	movb	%al, -68(%rbp)
	movl	-100(%rbp), %eax
	leaq	-96(%rbp), %rsi
	movq	%rbx, %rdi
	movb	$88, -96(%rbp)
	movb	%r14b, -64(%rbp)
	movl	%r15d, -60(%rbp)
	movl	%r12d, -92(%rbp)
	movl	%r13d, -88(%rbp)
	movl	%eax, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	jmp	.L4326
	.p2align 4,,10
	.p2align 3
.L4444:
	movl	$123, %edx
	movl	$-5, %r14d
	jmp	.L4359
	.p2align 4,,10
	.p2align 3
.L4341:
	movl	$-5, %r15d
	subl	%r13d, %r15d
.L4342:
	movl	$-5, %eax
	subl	%r14d, %eax
	movl	%eax, %r14d
.L4343:
	movl	$-5, %r13d
	subl	%edx, %r13d
	jmp	.L4344
	.p2align 4,,10
	.p2align 3
.L4317:
	movl	$-5, %r13d
	subl	%edx, %r13d
	jmp	.L4318
	.p2align 4,,10
	.p2align 3
.L4356:
	movl	%r14d, %r13d
.L4357:
	testl	%r14d, %r14d
	je	.L4397
	movl	$-5, %r14d
	subl	%edx, %r14d
	leal	128(%r14), %edx
	jmp	.L4360
	.p2align 4,,10
	.p2align 3
.L4442:
	movq	%rdx, -112(%rbp)
	leal	1(%rdx), %r14d
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE89ELNS1_14AccumulatorUseE2EEEvv.isra.0
	movq	464(%rbx), %rdi
	movq	-112(%rbp), %rdx
	testq	%rdi, %rdi
	je	.L4328
	movl	%r14d, %esi
	movl	$-5, %r14d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movq	464(%rbx), %rdi
	movl	%r14d, %r13d
	movq	-112(%rbp), %rdx
	subl	%eax, %r13d
	testq	%rdi, %rdi
	je	.L4329
	movl	%edx, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movq	464(%rbx), %rdi
	subl	%eax, %r14d
	testq	%rdi, %rdi
	je	.L4330
	movl	%r12d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r12d
.L4330:
	movl	$-5, %eax
	movzbl	472(%rbx), %r15d
	movl	$-1, %ecx
	subl	%r12d, %eax
	movl	%eax, %r12d
	testb	%r15b, %r15b
	je	.L4331
	movl	%ecx, -112(%rbp)
	cmpb	$2, %r15b
	je	.L4332
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L4448
.L4332:
	movl	476(%rbx), %ecx
	movb	$0, 472(%rbx)
	movl	$-1, 476(%rbx)
.L4331:
	leal	128(%r12), %eax
	cmpl	$255, %eax
	jbe	.L4379
	leal	32768(%r12), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4333:
	leal	128(%r14), %esi
	cmpl	$255, %esi
	jbe	.L4334
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%edx, %eax
.L4334:
	leal	128(%r13), %edx
	cmpl	$255, %edx
	jbe	.L4336
	leal	32768(%r13), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4336:
	movl	-100(%rbp), %esi
	cmpl	$255, %esi
	jbe	.L4338
	cmpl	$65536, %esi
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4338:
	movd	-100(%rbp), %xmm6
	movd	%r13d, %xmm1
	movd	%r12d, %xmm0
	movb	$89, -96(%rbp)
	movd	%r14d, %xmm7
	movb	%al, -68(%rbp)
	movb	%r15b, -64(%rbp)
	punpckldq	%xmm6, %xmm1
	punpckldq	%xmm7, %xmm0
	movl	%ecx, -60(%rbp)
	jmp	.L4440
	.p2align 4,,10
	.p2align 3
.L4393:
	movl	$2, %esi
	movl	$1, %eax
	jmp	.L4363
	.p2align 4,,10
	.p2align 3
.L4397:
	movl	$123, %edx
	movl	$-5, %r14d
	jmp	.L4360
	.p2align 4,,10
	.p2align 3
.L4328:
	movl	$-5, %r13d
	subl	%r14d, %r13d
.L4329:
	movl	$-5, %r14d
	subl	%edx, %r14d
	jmp	.L4330
	.p2align 4,,10
	.p2align 3
.L4385:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L4347
	.p2align 4,,10
	.p2align 3
.L4373:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L4321
	.p2align 4,,10
	.p2align 3
.L4379:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L4333
	.p2align 4,,10
	.p2align 3
.L4445:
	movl	$90, %edi
	movb	%cl, -104(%rbp)
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movzbl	-104(%rbp), %ecx
	movl	$-1, %esi
	testb	%al, %al
	je	.L4346
	xorl	%ecx, %ecx
	jmp	.L4345
	.p2align 4,,10
	.p2align 3
.L4392:
	xorl	%r15d, %r15d
	jmp	.L4361
	.p2align 4,,10
	.p2align 3
.L4447:
	movl	$88, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	je	.L4320
	xorl	%r14d, %r14d
	jmp	.L4319
	.p2align 4,,10
	.p2align 3
.L4448:
	movl	$89, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movl	$-1, %ecx
	testb	%al, %al
	je	.L4332
	xorl	%r15d, %r15d
	jmp	.L4331
.L4446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20252:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder12CallPropertyENS1_8RegisterENS1_12RegisterListEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder21CallUndefinedReceiverENS1_8RegisterENS1_12RegisterListEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder21CallUndefinedReceiverENS1_8RegisterENS1_12RegisterListEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder21CallUndefinedReceiverENS1_8RegisterENS1_12RegisterListEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder21CallUndefinedReceiverENS1_8RegisterENS1_12RegisterListEi:
.LFB20253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	sarq	$32, %rdx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$72, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L4554
	cmpl	$1, %edx
	je	.L4555
	cmpl	$2, %edx
	je	.L4556
	testq	%rdi, %rdi
	je	.L4481
	movl	%esi, %r14d
	movl	8(%rdi), %esi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	-104(%rbp), %rdx
	movq	464(%r12), %rdi
	movl	%edx, -104(%rbp)
	testq	%rdi, %rdi
	je	.L4552
	salq	$32, %rdx
	movl	%r13d, %esi
	orq	%rdx, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movq	464(%r12), %rdi
	movl	$-5, %ecx
	movq	%rax, %rdx
	sarq	$32, %rdx
	testl	%edx, %edx
	jne	.L4484
	movl	$123, %edx
.L4485:
	testq	%rdi, %rdi
	je	.L4483
	movl	%r14d, %esi
	movl	%edx, -112(%rbp)
	movl	%ecx, -108(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	-112(%rbp), %edx
	movl	-108(%rbp), %ecx
	movl	%eax, %r15d
.L4483:
	movzbl	472(%r12), %r14d
	movl	$-5, %r13d
	subl	%r15d, %r13d
	movl	$-1, %r15d
	testb	%r14b, %r14b
	je	.L4486
	cmpb	$2, %r14b
	je	.L4487
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L4487
	movl	$91, %edi
	movl	%edx, -112(%rbp)
	movl	%ecx, -108(%rbp)
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movl	-108(%rbp), %ecx
	movl	-112(%rbp), %edx
	testb	%al, %al
	jne	.L4513
.L4487:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4486:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L4514
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edi, %edi
	andl	$-2, %edi
	addl	$4, %edi
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4488:
	cmpl	$255, %edx
	jbe	.L4489
	leal	32768(%rcx), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%edi, %eax
.L4489:
	movl	-104(%rbp), %esi
	cmpl	$255, %esi
	jbe	.L4491
	cmpl	$65536, %esi
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4491:
	cmpl	$255, %ebx
	jbe	.L4493
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4493:
	movd	-104(%rbp), %xmm1
	movd	%ebx, %xmm2
	movd	%ecx, %xmm3
	movb	%al, -68(%rbp)
	movd	%r13d, %xmm0
	movb	$91, -96(%rbp)
	movb	%r14b, -64(%rbp)
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	movl	%r15d, -60(%rbp)
	jmp	.L4553
	.p2align 4,,10
	.p2align 3
.L4554:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE92ELNS1_14AccumulatorUseE2EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4451
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r15d
.L4451:
	movzbl	472(%r12), %r14d
	movl	$-5, %r13d
	subl	%r15d, %r13d
	movl	$-1, %r15d
	testb	%r14b, %r14b
	jne	.L4557
.L4452:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L4498
.L4560:
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4454:
	cmpl	$255, %ebx
	jbe	.L4455
	cmpl	$65536, %ebx
	movl	$4, %eax
	cmovb	%edx, %eax
.L4455:
	movb	%al, -68(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$8589934592, %rax
	movb	$92, -96(%rbp)
	movb	%r14b, -64(%rbp)
	movl	%r15d, -60(%rbp)
	movl	%r13d, -92(%rbp)
	movl	%ebx, -88(%rbp)
	movq	$0, -84(%rbp)
	movq	%rax, -76(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
.L4457:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4558
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4556:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE94ELNS1_14AccumulatorUseE2EEEvv.isra.0
	movq	464(%r12), %rdi
	leal	1(%r13), %r14d
	testq	%rdi, %rdi
	je	.L4469
	movl	%r14d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	$-5, %edx
	movq	464(%r12), %rdi
	movl	%edx, %r14d
	subl	%eax, %r14d
	testq	%rdi, %rdi
	je	.L4470
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	$-5, %edx
	movq	464(%r12), %rdi
	subl	%eax, %edx
	movl	%edx, %r8d
	testq	%rdi, %rdi
	je	.L4471
	movl	%r15d, %esi
	movl	%edx, -104(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	-104(%rbp), %r8d
	movl	%eax, %r15d
.L4471:
	movzbl	472(%r12), %ecx
	movl	$-5, %r13d
	subl	%r15d, %r13d
	movl	$-1, %r15d
	testb	%cl, %cl
	je	.L4472
	cmpb	$2, %cl
	je	.L4473
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	jne	.L4559
.L4473:
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4472:
	leal	128(%r13), %eax
	cmpl	$255, %eax
	jbe	.L4508
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4474:
	leal	128(%r8), %edi
	cmpl	$255, %edi
	jbe	.L4475
	leal	32768(%r8), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%edx, %eax
.L4475:
	leal	128(%r14), %edx
	cmpl	$255, %edx
	jbe	.L4477
	leal	32768(%r14), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4477:
	cmpl	$255, %ebx
	jbe	.L4479
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4479:
	movb	$94, -96(%rbp)
	movd	%r14d, %xmm1
	movd	%ebx, %xmm4
	movb	%al, -68(%rbp)
	movd	%r13d, %xmm0
	movd	%r8d, %xmm5
	punpckldq	%xmm4, %xmm1
	movb	%cl, -64(%rbp)
	punpckldq	%xmm5, %xmm0
	movl	%r15d, -60(%rbp)
.L4553:
	punpcklqdq	%xmm1, %xmm0
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movabsq	$17179869184, %rax
	movq	%rax, -76(%rbp)
	movups	%xmm0, -92(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	jmp	.L4457
	.p2align 4,,10
	.p2align 3
.L4557:
	cmpb	$2, %r14b
	je	.L4453
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L4453
	movl	$92, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	jne	.L4497
.L4453:
	leal	128(%r13), %eax
	movl	476(%r12), %r15d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	cmpl	$255, %eax
	ja	.L4560
.L4498:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L4454
	.p2align 4,,10
	.p2align 3
.L4481:
	movl	%edx, -104(%rbp)
.L4552:
	movl	$-5, %ecx
	subl	%r13d, %ecx
	leal	128(%rcx), %edx
	jmp	.L4483
	.p2align 4,,10
	.p2align 3
.L4555:
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE93ELNS1_14AccumulatorUseE2EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4459
	movl	%r13d, %esi
	movl	$-5, %r13d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	subl	%eax, %r13d
	testq	%rdi, %rdi
	je	.L4460
	movl	%r15d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r15d
.L4460:
	movl	$-5, %r14d
	movl	$-1, %esi
	subl	%r15d, %r14d
	movzbl	472(%r12), %r15d
	testb	%r15b, %r15b
	je	.L4461
	movl	%esi, -104(%rbp)
	cmpb	$2, %r15b
	je	.L4462
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L4462
	movl	$93, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movl	$-1, %esi
	testb	%al, %al
	je	.L4462
	xorl	%r15d, %r15d
	jmp	.L4461
	.p2align 4,,10
	.p2align 3
.L4462:
	movl	476(%r12), %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4461:
	leal	128(%r14), %eax
	cmpl	$255, %eax
	jbe	.L4503
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4463:
	leal	128(%r13), %ecx
	cmpl	$255, %ecx
	jbe	.L4464
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%edx, %eax
.L4464:
	cmpl	$255, %ebx
	jbe	.L4466
	cmpl	$65536, %ebx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4466:
	movl	%esi, -60(%rbp)
	movq	%r12, %rdi
	leaq	-96(%rbp), %rsi
	movb	$93, -96(%rbp)
	movb	%al, -68(%rbp)
	movb	%r15b, -64(%rbp)
	movl	%r14d, -92(%rbp)
	movl	%r13d, -88(%rbp)
	movl	%ebx, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	jmp	.L4457
	.p2align 4,,10
	.p2align 3
.L4514:
	movl	$2, %edi
	movl	$1, %eax
	jmp	.L4488
	.p2align 4,,10
	.p2align 3
.L4469:
	movl	$-5, %eax
	subl	%r14d, %eax
	movl	%eax, %r14d
.L4470:
	movl	$-5, %eax
	subl	%r13d, %eax
	movl	%eax, %r8d
	jmp	.L4471
	.p2align 4,,10
	.p2align 3
.L4459:
	movl	$-5, %eax
	subl	%r13d, %eax
	movl	%eax, %r13d
	jmp	.L4460
	.p2align 4,,10
	.p2align 3
.L4508:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L4474
	.p2align 4,,10
	.p2align 3
.L4559:
	movl	$94, %edi
	movl	%r8d, -108(%rbp)
	movb	%cl, -104(%rbp)
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movzbl	-104(%rbp), %ecx
	movl	-108(%rbp), %r8d
	testb	%al, %al
	je	.L4473
	xorl	%ecx, %ecx
	jmp	.L4472
	.p2align 4,,10
	.p2align 3
.L4513:
	xorl	%r14d, %r14d
	jmp	.L4486
	.p2align 4,,10
	.p2align 3
.L4497:
	xorl	%r14d, %r14d
	jmp	.L4452
	.p2align 4,,10
	.p2align 3
.L4503:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L4463
	.p2align 4,,10
	.p2align 3
.L4484:
	subl	%eax, %ecx
	leal	128(%rcx), %edx
	jmp	.L4485
.L4558:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20253:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder21CallUndefinedReceiverENS1_8RegisterENS1_12RegisterListEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder21CallUndefinedReceiverENS1_8RegisterENS1_12RegisterListEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder15CallAnyReceiverENS1_8RegisterENS1_12RegisterListEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder15CallAnyReceiverENS1_8RegisterENS1_12RegisterListEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder15CallAnyReceiverENS1_8RegisterENS1_12RegisterListEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder15CallAnyReceiverENS1_8RegisterENS1_12RegisterListEi:
.LFB20254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	sarq	$32, %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$72, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L4562
	movl	%esi, %r15d
	movl	8(%rdi), %esi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	-104(%rbp), %rdx
	movq	464(%r12), %rdi
	movl	%edx, -104(%rbp)
	testq	%rdi, %rdi
	je	.L4563
	salq	$32, %rdx
	movl	%r14d, %esi
	orq	%rdx, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rdx
	sarq	$32, %rdx
	testl	%edx, %edx
	je	.L4604
	movl	$-5, %r14d
	subl	%eax, %r14d
	leal	128(%r14), %r8d
.L4565:
	testq	%rdi, %rdi
	je	.L4566
	movl	%r15d, %esi
	movl	%r8d, -108(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	-108(%rbp), %r8d
	movl	%eax, %r13d
.L4566:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L4567
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4567:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L4580
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4568:
	cmpl	$255, %r8d
	jbe	.L4569
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%ecx, %eax
.L4569:
	movl	-104(%rbp), %ecx
	cmpl	$255, %ecx
	jbe	.L4571
	cmpl	$65536, %ecx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L4571:
	cmpl	$255, %ebx
	jbe	.L4573
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L4573:
	movd	-104(%rbp), %xmm1
	movb	%al, -68(%rbp)
	movd	%ebx, %xmm2
	movd	%edx, %xmm0
	movabsq	$17179869184, %rax
	movd	%r14d, %xmm3
	movb	$86, -96(%rbp)
	movq	%rax, -76(%rbp)
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	movzbl	480(%r12), %eax
	punpcklqdq	%xmm1, %xmm0
	movb	%sil, -64(%rbp)
	movl	%edi, -60(%rbp)
	movups	%xmm0, -92(%rbp)
	testb	%al, %al
	je	.L4575
	testb	%sil, %sil
	jne	.L4576
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L4577:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4575:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4605
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4604:
	.cfi_restore_state
	movl	$123, %r8d
	movl	$-5, %r14d
	jmp	.L4565
	.p2align 4,,10
	.p2align 3
.L4562:
	movl	%edx, -104(%rbp)
.L4563:
	testl	%edx, %edx
	je	.L4585
	movl	$-5, %eax
	subl	%r14d, %eax
	movl	%eax, %r14d
	leal	128(%rax), %r8d
	jmp	.L4566
	.p2align 4,,10
	.p2align 3
.L4580:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L4568
	.p2align 4,,10
	.p2align 3
.L4576:
	cmpb	$2, %al
	jne	.L4577
	cmpb	$1, %sil
	jne	.L4577
	movb	$2, -64(%rbp)
	jmp	.L4577
	.p2align 4,,10
	.p2align 3
.L4585:
	movl	$123, %r8d
	movl	$-5, %r14d
	jmp	.L4566
.L4605:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20254:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder15CallAnyReceiverENS1_8RegisterENS1_12RegisterListEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder15CallAnyReceiverENS1_8RegisterENS1_12RegisterListEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder14CallNoFeedbackENS1_8RegisterENS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder14CallNoFeedbackENS1_8RegisterENS1_12RegisterListE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder14CallNoFeedbackENS1_8RegisterENS1_12RegisterListE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder14CallNoFeedbackENS1_8RegisterENS1_12RegisterListE:
.LFB20255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	sarq	$32, %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L4607
	movl	%esi, -100(%rbp)
	movl	%esi, %r13d
	movl	8(%rdi), %esi
	movl	%r15d, %r14d
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	movl	-100(%rbp), %ecx
	testq	%rdi, %rdi
	je	.L4608
	salq	$32, %r15
	movl	%ebx, %esi
	orq	%r15, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movq	464(%r12), %rdi
	movl	-100(%rbp), %ecx
	movq	%rax, %rdx
	sarq	$32, %rdx
	testl	%edx, %edx
	je	.L4646
	movl	$-5, %ebx
	subl	%eax, %ebx
	leal	128(%rbx), %r15d
.L4610:
	testq	%rdi, %rdi
	je	.L4611
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %ecx
.L4611:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%ecx, %edx
	testb	%sil, %sil
	je	.L4612
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4612:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L4623
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4613:
	cmpl	$255, %r15d
	jbe	.L4614
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%ecx, %eax
.L4614:
	cmpl	$255, %r14d
	jbe	.L4616
	cmpl	$65536, %r14d
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L4616:
	movb	%al, -68(%rbp)
	movzbl	480(%r12), %eax
	movb	$95, -96(%rbp)
	movb	%sil, -64(%rbp)
	movl	%edi, -60(%rbp)
	movl	%edx, -92(%rbp)
	movl	%ebx, -88(%rbp)
	movl	%r14d, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	testb	%al, %al
	je	.L4618
	testb	%sil, %sil
	jne	.L4619
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L4620:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4618:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4647
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4646:
	.cfi_restore_state
	movl	$123, %r15d
	movl	$-5, %ebx
	jmp	.L4610
	.p2align 4,,10
	.p2align 3
.L4607:
	movl	%r15d, %r14d
.L4608:
	testl	%r15d, %r15d
	je	.L4627
	movl	$-5, %eax
	subl	%ebx, %eax
	movl	%eax, %ebx
	leal	128(%rax), %r15d
	jmp	.L4611
	.p2align 4,,10
	.p2align 3
.L4623:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L4613
	.p2align 4,,10
	.p2align 3
.L4619:
	cmpb	$2, %al
	jne	.L4620
	cmpb	$1, %sil
	jne	.L4620
	movb	$2, -64(%rbp)
	jmp	.L4620
	.p2align 4,,10
	.p2align 3
.L4627:
	movl	$123, %r15d
	movl	$-5, %ebx
	jmp	.L4611
.L4647:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20255:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder14CallNoFeedbackENS1_8RegisterENS1_12RegisterListE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder14CallNoFeedbackENS1_8RegisterENS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder14CallWithSpreadENS1_8RegisterENS1_12RegisterListEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder14CallWithSpreadENS1_8RegisterENS1_12RegisterListEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder14CallWithSpreadENS1_8RegisterENS1_12RegisterListEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder14CallWithSpreadENS1_8RegisterENS1_12RegisterListEi:
.LFB20256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	sarq	$32, %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$72, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L4649
	movl	%esi, %r15d
	movl	8(%rdi), %esi
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	-104(%rbp), %rdx
	movq	464(%r12), %rdi
	movl	%edx, -104(%rbp)
	testq	%rdi, %rdi
	je	.L4650
	salq	$32, %rdx
	movl	%r14d, %esi
	orq	%rdx, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rdx
	sarq	$32, %rdx
	testl	%edx, %edx
	je	.L4691
	movl	$-5, %r14d
	subl	%eax, %r14d
	leal	128(%r14), %r8d
.L4652:
	testq	%rdi, %rdi
	je	.L4653
	movl	%r15d, %esi
	movl	%r8d, -108(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	-108(%rbp), %r8d
	movl	%eax, %r13d
.L4653:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%r13d, %edx
	testb	%sil, %sil
	je	.L4654
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4654:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L4667
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4655:
	cmpl	$255, %r8d
	jbe	.L4656
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%ecx, %eax
.L4656:
	movl	-104(%rbp), %ecx
	cmpl	$255, %ecx
	jbe	.L4658
	cmpl	$65536, %ecx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L4658:
	cmpl	$255, %ebx
	jbe	.L4660
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L4660:
	movd	-104(%rbp), %xmm1
	movb	%al, -68(%rbp)
	movd	%ebx, %xmm2
	movd	%edx, %xmm0
	movabsq	$17179869184, %rax
	movd	%r14d, %xmm3
	movb	$96, -96(%rbp)
	movq	%rax, -76(%rbp)
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	movzbl	480(%r12), %eax
	punpcklqdq	%xmm1, %xmm0
	movb	%sil, -64(%rbp)
	movl	%edi, -60(%rbp)
	movups	%xmm0, -92(%rbp)
	testb	%al, %al
	je	.L4662
	testb	%sil, %sil
	jne	.L4663
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L4664:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4662:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4692
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4691:
	.cfi_restore_state
	movl	$123, %r8d
	movl	$-5, %r14d
	jmp	.L4652
	.p2align 4,,10
	.p2align 3
.L4649:
	movl	%edx, -104(%rbp)
.L4650:
	testl	%edx, %edx
	je	.L4672
	movl	$-5, %eax
	subl	%r14d, %eax
	movl	%eax, %r14d
	leal	128(%rax), %r8d
	jmp	.L4653
	.p2align 4,,10
	.p2align 3
.L4667:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L4655
	.p2align 4,,10
	.p2align 3
.L4663:
	cmpb	$2, %al
	jne	.L4664
	cmpb	$1, %sil
	jne	.L4664
	movb	$2, -64(%rbp)
	jmp	.L4664
	.p2align 4,,10
	.p2align 3
.L4672:
	movl	$123, %r8d
	movl	$-5, %r14d
	jmp	.L4653
.L4692:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20256:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder14CallWithSpreadENS1_8RegisterENS1_12RegisterListEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder14CallWithSpreadENS1_8RegisterENS1_12RegisterListEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder9ConstructENS1_8RegisterENS1_12RegisterListEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ConstructENS1_8RegisterENS1_12RegisterListEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ConstructENS1_8RegisterENS1_12RegisterListEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder9ConstructENS1_8RegisterENS1_12RegisterListEi:
.LFB20257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	sarq	$32, %rdx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$72, %rsp
	movl	%ecx, -100(%rbp)
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L4694
	movl	%esi, %r15d
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	-112(%rbp), %rdx
	movq	464(%r12), %rdi
	movl	%edx, %r13d
	testq	%rdi, %rdi
	je	.L4695
	salq	$32, %rdx
	movl	%r14d, %esi
	orq	%rdx, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rdx
	sarq	$32, %rdx
	testl	%edx, %edx
	je	.L4736
	movl	$-5, %r14d
	subl	%eax, %r14d
	leal	128(%r14), %r8d
.L4697:
	testq	%rdi, %rdi
	je	.L4698
	movl	%r15d, %esi
	movl	%r8d, -112(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	-112(%rbp), %r8d
	movl	%eax, %ebx
.L4698:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%ebx, %edx
	testb	%sil, %sil
	je	.L4699
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4699:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L4712
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4700:
	cmpl	$255, %r8d
	jbe	.L4701
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%ecx, %eax
.L4701:
	cmpl	$255, %r13d
	jbe	.L4703
	cmpl	$65536, %r13d
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L4703:
	movl	-100(%rbp), %ebx
	cmpl	$255, %ebx
	jbe	.L4705
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L4705:
	movd	-100(%rbp), %xmm2
	movb	%al, -68(%rbp)
	movd	%r13d, %xmm1
	movd	%edx, %xmm0
	movabsq	$17179869184, %rax
	movd	%r14d, %xmm3
	movb	$101, -96(%rbp)
	movq	%rax, -76(%rbp)
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	movzbl	480(%r12), %eax
	punpcklqdq	%xmm1, %xmm0
	movb	%sil, -64(%rbp)
	movl	%edi, -60(%rbp)
	movups	%xmm0, -92(%rbp)
	testb	%al, %al
	je	.L4707
	testb	%sil, %sil
	jne	.L4708
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L4709:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4707:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4737
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4736:
	.cfi_restore_state
	movl	$123, %r8d
	movl	$-5, %r14d
	jmp	.L4697
	.p2align 4,,10
	.p2align 3
.L4694:
	movl	%edx, %r13d
.L4695:
	testl	%edx, %edx
	je	.L4717
	movl	$-5, %eax
	subl	%r14d, %eax
	movl	%eax, %r14d
	leal	128(%rax), %r8d
	jmp	.L4698
	.p2align 4,,10
	.p2align 3
.L4712:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L4700
	.p2align 4,,10
	.p2align 3
.L4708:
	cmpb	$2, %al
	jne	.L4709
	cmpb	$1, %sil
	jne	.L4709
	movb	$2, -64(%rbp)
	jmp	.L4709
	.p2align 4,,10
	.p2align 3
.L4717:
	movl	$123, %r8d
	movl	$-5, %r14d
	jmp	.L4698
.L4737:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20257:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder9ConstructENS1_8RegisterENS1_12RegisterListEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder9ConstructENS1_8RegisterENS1_12RegisterListEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder19ConstructWithSpreadENS1_8RegisterENS1_12RegisterListEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder19ConstructWithSpreadENS1_8RegisterENS1_12RegisterListEi
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder19ConstructWithSpreadENS1_8RegisterENS1_12RegisterListEi, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder19ConstructWithSpreadENS1_8RegisterENS1_12RegisterListEi:
.LFB20258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	sarq	$32, %rdx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$72, %rsp
	movl	%ecx, -100(%rbp)
	movq	464(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L4739
	movl	%esi, %r15d
	movq	16(%r13), %rsi
	movq	%r13, %rdi
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer11MaterializeEPNS2_12RegisterInfoE@PLT
	movl	8(%r13), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	-112(%rbp), %rdx
	movq	464(%r12), %rdi
	movl	%edx, %r13d
	testq	%rdi, %rdi
	je	.L4740
	salq	$32, %rdx
	movl	%r14d, %esi
	orq	%rdx, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movq	464(%r12), %rdi
	movq	%rax, %rdx
	sarq	$32, %rdx
	testl	%edx, %edx
	je	.L4781
	movl	$-5, %r14d
	subl	%eax, %r14d
	leal	128(%r14), %r8d
.L4742:
	testq	%rdi, %rdi
	je	.L4743
	movl	%r15d, %esi
	movl	%r8d, -112(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	-112(%rbp), %r8d
	movl	%eax, %ebx
.L4743:
	movzbl	472(%r12), %esi
	movl	$-5, %edx
	movl	$-1, %edi
	subl	%ebx, %edx
	testb	%sil, %sil
	je	.L4744
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4744:
	leal	128(%rdx), %eax
	cmpl	$255, %eax
	jbe	.L4757
	leal	32768(%rdx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4745:
	cmpl	$255, %r8d
	jbe	.L4746
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%ecx, %eax
.L4746:
	cmpl	$255, %r13d
	jbe	.L4748
	cmpl	$65536, %r13d
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L4748:
	movl	-100(%rbp), %ebx
	cmpl	$255, %ebx
	jbe	.L4750
	cmpl	$65536, %ebx
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpb	%cl, %al
	cmovb	%ecx, %eax
.L4750:
	movd	-100(%rbp), %xmm2
	movb	%al, -68(%rbp)
	movd	%r13d, %xmm1
	movd	%edx, %xmm0
	movabsq	$17179869184, %rax
	movd	%r14d, %xmm3
	movb	$102, -96(%rbp)
	movq	%rax, -76(%rbp)
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	movzbl	480(%r12), %eax
	punpcklqdq	%xmm1, %xmm0
	movb	%sil, -64(%rbp)
	movl	%edi, -60(%rbp)
	movups	%xmm0, -92(%rbp)
	testb	%al, %al
	je	.L4752
	testb	%sil, %sil
	jne	.L4753
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L4754:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4752:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4782
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4781:
	.cfi_restore_state
	movl	$123, %r8d
	movl	$-5, %r14d
	jmp	.L4742
	.p2align 4,,10
	.p2align 3
.L4739:
	movl	%edx, %r13d
.L4740:
	testl	%edx, %edx
	je	.L4762
	movl	$-5, %eax
	subl	%r14d, %eax
	movl	%eax, %r14d
	leal	128(%rax), %r8d
	jmp	.L4743
	.p2align 4,,10
	.p2align 3
.L4757:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L4745
	.p2align 4,,10
	.p2align 3
.L4753:
	cmpb	$2, %al
	jne	.L4754
	cmpb	$1, %sil
	jne	.L4754
	movb	$2, -64(%rbp)
	jmp	.L4754
	.p2align 4,,10
	.p2align 3
.L4762:
	movl	$123, %r8d
	movl	$-5, %r14d
	jmp	.L4743
.L4782:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20258:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder19ConstructWithSpreadENS1_8RegisterENS1_12RegisterListEi, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder19ConstructWithSpreadENS1_8RegisterENS1_12RegisterListEi
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE:
.LFB20259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	%esi, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	sarq	$32, %r13
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r13d, %r12d
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, -100(%rbp)
	call	_ZN2v88internal11interpreter16IntrinsicsHelper11IsSupportedENS0_7Runtime10FunctionIdE@PLT
	testb	%al, %al
	jne	.L4835
	movq	464(%r14), %rdi
	movl	%r13d, %edx
	testq	%rdi, %rdi
	je	.L4796
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r14), %rdi
	movl	%r13d, %edx
	testq	%rdi, %rdi
	je	.L4796
	movl	%r13d, -104(%rbp)
	movl	%ebx, %esi
	salq	$32, %r13
	orq	%r13, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movl	-104(%rbp), %edx
	movl	%eax, -100(%rbp)
	movq	%rax, %r12
	shrq	$32, %r12
.L4796:
	movzbl	472(%r14), %r13d
	testl	%r12d, %r12d
	jne	.L4836
	testb	%r13b, %r13b
	je	.L4837
	movl	$123, %r12d
	movl	$-5, %ebx
	cmpb	$2, %r13b
	je	.L4799
.L4839:
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L4799
	movl	$97, %edi
	movl	%edx, -100(%rbp)
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movl	-100(%rbp), %edx
	testb	%al, %al
	je	.L4799
	movl	$-1, %esi
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L4798:
	cmpl	$255, %r12d
	jbe	.L4800
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%ecx, %ecx
	andl	$-2, %ecx
	addl	$4, %ecx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4801:
	cmpl	$255, %edx
	jbe	.L4802
	cmpl	$65536, %edx
	movl	$4, %eax
	cmovb	%ecx, %eax
.L4802:
	movb	$97, -96(%rbp)
	movb	%al, -68(%rbp)
	movb	%r13b, -64(%rbp)
	movl	%esi, -60(%rbp)
	movl	%r15d, -92(%rbp)
	movl	%ebx, -88(%rbp)
	movl	%edx, -84(%rbp)
.L4834:
	leaq	-96(%rbp), %rsi
	movq	%r14, %rdi
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4838
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4836:
	.cfi_restore_state
	movl	$-5, %ebx
	subl	-100(%rbp), %ebx
	movl	$-1, %esi
	leal	128(%rbx), %r12d
	testb	%r13b, %r13b
	je	.L4798
	cmpb	$2, %r13b
	jne	.L4839
	.p2align 4,,10
	.p2align 3
.L4799:
	movl	476(%r14), %esi
	movb	$0, 472(%r14)
	movl	$-1, 476(%r14)
	jmp	.L4798
	.p2align 4,,10
	.p2align 3
.L4835:
	movl	%r15d, %edi
	call	_ZN2v88internal11interpreter16IntrinsicsHelper13FromRuntimeIdENS0_7Runtime10FunctionIdE@PLT
	movq	464(%r14), %rdi
	movl	%r13d, %ecx
	movl	%eax, %r15d
	testq	%rdi, %rdi
	je	.L4786
	movl	8(%rdi), %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r14), %rdi
	movl	%r13d, %ecx
	testq	%rdi, %rdi
	je	.L4786
	movl	%r13d, -104(%rbp)
	movl	%ebx, %esi
	salq	$32, %r13
	orq	%r13, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movl	-104(%rbp), %ecx
	movl	%eax, -100(%rbp)
	movq	%rax, %r12
	shrq	$32, %r12
.L4786:
	movzbl	472(%r14), %r13d
	testl	%r12d, %r12d
	jne	.L4840
	testb	%r13b, %r13b
	jne	.L4816
	movl	$-5, %ebx
	movl	$-1, %esi
.L4805:
	movl	$2, %edx
	movl	$1, %eax
.L4791:
	cmpl	$255, %ecx
	jbe	.L4792
	cmpl	$65536, %ecx
	movl	$4, %eax
	cmovb	%edx, %eax
.L4792:
	movb	$100, -96(%rbp)
	movb	%al, -68(%rbp)
	movb	%r13b, -64(%rbp)
	movl	%esi, -60(%rbp)
	movl	%r15d, -92(%rbp)
	movl	%ebx, -88(%rbp)
	movl	%ecx, -84(%rbp)
	jmp	.L4834
	.p2align 4,,10
	.p2align 3
.L4837:
	movl	$-5, %ebx
	movl	$-1, %esi
.L4800:
	movl	$2, %ecx
	movl	$1, %eax
	jmp	.L4801
	.p2align 4,,10
	.p2align 3
.L4840:
	movl	$-5, %ebx
	subl	-100(%rbp), %ebx
	movl	$-1, %esi
	leal	128(%rbx), %r12d
	testb	%r13b, %r13b
	je	.L4788
.L4804:
	cmpb	$2, %r13b
	je	.L4789
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L4789
	movl	$100, %edi
	movl	%ecx, -100(%rbp)
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	movl	-100(%rbp), %ecx
	testb	%al, %al
	jne	.L4809
.L4789:
	movl	476(%r14), %esi
	movb	$0, 472(%r14)
	movl	$-1, 476(%r14)
.L4788:
	cmpl	$255, %r12d
	jbe	.L4805
	leal	32768(%rbx), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	jmp	.L4791
	.p2align 4,,10
	.p2align 3
.L4816:
	movl	$123, %r12d
	movl	$-5, %ebx
	jmp	.L4804
	.p2align 4,,10
	.p2align 3
.L4809:
	movl	$-1, %esi
	xorl	%r13d, %r13d
	jmp	.L4788
.L4838:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20259:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE:
.LFB20260:
	.cfi_startproc
	endbr64
	movl	%edx, %edx
	btsq	$32, %rdx
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE
	.cfi_endproc
.LFE20260:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE:
.LFB20261:
	.cfi_startproc
	endbr64
	movl	$2147483647, %edx
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdENS1_12RegisterListE
	.cfi_endproc
.LFE20261:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder11CallRuntimeENS0_7Runtime10FunctionIdE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder18CallRuntimeForPairENS0_7Runtime10FunctionIdENS1_12RegisterListES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CallRuntimeForPairENS0_7Runtime10FunctionIdENS1_12RegisterListES5_
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CallRuntimeForPairENS0_7Runtime10FunctionIdENS1_12RegisterListES5_, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder18CallRuntimeForPairENS0_7Runtime10FunctionIdENS1_12RegisterListES5_:
.LFB20262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	sarq	$32, %rdx
	pushq	%r13
	movl	%edx, %r15d
	movl	%r14d, %r9d
	.cfi_offset 13, -40
	movslq	%edx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$88, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzwl	%si, %eax
	movq	%rdx, -112(%rbp)
	movl	%eax, -100(%rbp)
	movq	%rcx, %rax
	sarq	$32, %rax
	testq	%rdi, %rdi
	je	.L4844
	movq	%rax, %r8
	movl	%ecx, %esi
	movq	%rax, -120(%rbp)
	salq	$32, %r8
	movl	%r14d, -104(%rbp)
	orq	%r8, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25PrepareOutputRegisterListENS1_12RegisterListE@PLT
	movq	-120(%rbp), %rax
	movl	-104(%rbp), %r9d
	movq	464(%r12), %rdi
	movq	-112(%rbp), %rdx
	testl	%eax, %eax
	je	.L4882
	movl	$-5, %eax
	subl	%ebx, %eax
	movl	%eax, %ecx
	leal	128(%rax), %ebx
.L4846:
	testq	%rdi, %rdi
	je	.L4847
	salq	$32, %rdx
	movl	%r14d, %esi
	movl	%ecx, -112(%rbp)
	orq	%rdx, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movl	-112(%rbp), %ecx
	movq	%rax, %r15
	movl	%eax, %r9d
	shrq	$32, %r15
.L4847:
	movzbl	472(%r12), %edi
	testl	%r15d, %r15d
	je	.L4848
	movl	$-5, %esi
	subl	%r9d, %esi
	movl	$-1, %r9d
	leal	128(%rsi), %eax
	testb	%dil, %dil
	je	.L4849
	movl	476(%r12), %r9d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4849:
	cmpl	$255, %eax
	jbe	.L4859
	leal	32768(%rsi), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4851:
	cmpl	$255, %r13d
	jbe	.L4852
	cmpl	$65536, %r13d
	movl	$4, %eax
	cmovb	%edx, %eax
.L4852:
	cmpl	$255, %ebx
	jbe	.L4854
	leal	32768(%rcx), %edx
	cmpl	$65536, %edx
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpb	%dl, %al
	cmovb	%edx, %eax
.L4854:
	movd	-100(%rbp), %xmm0
	movb	%al, -68(%rbp)
	movd	%ecx, %xmm2
	movd	%esi, %xmm3
	movabsq	$17179869184, %rax
	movd	%r13d, %xmm1
	movb	$98, -96(%rbp)
	movq	%rax, -76(%rbp)
	punpckldq	%xmm2, %xmm1
	punpckldq	%xmm3, %xmm0
	movzbl	480(%r12), %eax
	punpcklqdq	%xmm1, %xmm0
	movb	%dil, -64(%rbp)
	movl	%r9d, -60(%rbp)
	movups	%xmm0, -92(%rbp)
	testb	%al, %al
	je	.L4856
	testb	%dil, %dil
	jne	.L4857
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L4858:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4856:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4883
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4882:
	.cfi_restore_state
	movl	$123, %ebx
	movl	$-5, %ecx
	jmp	.L4846
	.p2align 4,,10
	.p2align 3
.L4848:
	testb	%dil, %dil
	je	.L4865
	movl	476(%r12), %r9d
	movl	$-5, %esi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4859:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L4851
	.p2align 4,,10
	.p2align 3
.L4844:
	testl	%eax, %eax
	je	.L4866
	movl	$-5, %eax
	subl	%ecx, %eax
	movl	%eax, %ecx
	leal	128(%rax), %ebx
	jmp	.L4847
	.p2align 4,,10
	.p2align 3
.L4857:
	cmpb	$2, %al
	jne	.L4858
	cmpb	$1, %dil
	jne	.L4858
	movb	$2, -64(%rbp)
	jmp	.L4858
	.p2align 4,,10
	.p2align 3
.L4865:
	movl	$-5, %esi
	movl	$-1, %r9d
	jmp	.L4859
	.p2align 4,,10
	.p2align 3
.L4866:
	movl	$123, %ebx
	movl	$-5, %ecx
	jmp	.L4847
.L4883:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20262:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CallRuntimeForPairENS0_7Runtime10FunctionIdENS1_12RegisterListES5_, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder18CallRuntimeForPairENS0_7Runtime10FunctionIdENS1_12RegisterListES5_
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder18CallRuntimeForPairENS0_7Runtime10FunctionIdENS1_8RegisterENS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CallRuntimeForPairENS0_7Runtime10FunctionIdENS1_8RegisterENS1_12RegisterListE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CallRuntimeForPairENS0_7Runtime10FunctionIdENS1_8RegisterENS1_12RegisterListE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder18CallRuntimeForPairENS0_7Runtime10FunctionIdENS1_8RegisterENS1_12RegisterListE:
.LFB20263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	sarq	$32, %r15
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movzwl	%si, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%edx, %eax
	testq	%rdi, %rdi
	je	.L4885
	movl	%edx, -100(%rbp)
	movl	%edx, %ebx
	movq	%r15, %rdx
	movl	%ecx, %esi
	salq	$32, %rdx
	orq	%rdx, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25PrepareOutputRegisterListENS1_12RegisterListE@PLT
	testl	%r15d, %r15d
	movq	464(%r12), %rdi
	movl	-100(%rbp), %eax
	je	.L4900
	movl	$-5, %edx
	subl	%r14d, %edx
	movl	%edx, %r14d
	leal	128(%rdx), %r15d
.L4886:
	testq	%rdi, %rdi
	je	.L4887
	movl	%ebx, %esi
	btsq	$32, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movq	%rax, %rdx
	sarq	$32, %rdx
	testl	%edx, %edx
	jne	.L4887
	movzbl	472(%r12), %esi
	testb	%sil, %sil
	je	.L4904
	movl	476(%r12), %edi
	movl	$-5, %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4897:
	movl	$2, %edx
	movl	$1, %eax
.L4891:
	cmpl	$255, %r15d
	jbe	.L4892
	leal	32768(%r14), %eax
	cmpl	$65536, %eax
	movl	$4, %eax
	cmovb	%edx, %eax
.L4892:
	movb	%al, -68(%rbp)
	movabsq	$17179869184, %rax
	movq	%rax, -76(%rbp)
	movzbl	480(%r12), %eax
	movb	$98, -96(%rbp)
	movb	%sil, -64(%rbp)
	movl	%edi, -60(%rbp)
	movl	%r13d, -92(%rbp)
	movl	%ecx, -88(%rbp)
	movl	$1, -84(%rbp)
	movl	%r14d, -80(%rbp)
	testb	%al, %al
	je	.L4894
	testb	%sil, %sil
	jne	.L4895
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L4896:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4894:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4920
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4900:
	.cfi_restore_state
	movl	$123, %r15d
	movl	$-5, %r14d
	jmp	.L4886
	.p2align 4,,10
	.p2align 3
.L4885:
	testl	%r15d, %r15d
	je	.L4898
	movl	$-5, %edx
	subl	%ecx, %edx
	movl	%edx, %r14d
	leal	128(%rdx), %r15d
.L4887:
	movl	$-5, %ecx
	movl	$-1, %edi
	movzbl	472(%r12), %esi
	subl	%eax, %ecx
	leal	128(%rcx), %eax
	testb	%sil, %sil
	je	.L4889
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4889:
	cmpl	$255, %eax
	jbe	.L4897
	leal	32768(%rcx), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
	jmp	.L4891
	.p2align 4,,10
	.p2align 3
.L4895:
	cmpb	$2, %al
	jne	.L4896
	cmpb	$1, %sil
	jne	.L4896
	movb	$2, -64(%rbp)
	jmp	.L4896
	.p2align 4,,10
	.p2align 3
.L4904:
	movl	$-5, %ecx
	movl	$-1, %edi
	jmp	.L4897
	.p2align 4,,10
	.p2align 3
.L4898:
	movl	$-5, %r14d
	movl	$123, %r15d
	jmp	.L4887
.L4920:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20263:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder18CallRuntimeForPairENS0_7Runtime10FunctionIdENS1_8RegisterENS1_12RegisterListE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder18CallRuntimeForPairENS0_7Runtime10FunctionIdENS1_8RegisterENS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder13CallJSRuntimeEiNS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CallJSRuntimeEiNS1_12RegisterListE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CallJSRuntimeEiNS1_12RegisterListE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder13CallJSRuntimeEiNS1_12RegisterListE:
.LFB20264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	sarq	$32, %rdx
	movl	%edx, %r14d
	movl	%edx, %r15d
	subq	$72, %rsp
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -104(%rbp)
	movl	%ebx, %eax
	testq	%rdi, %rdi
	je	.L4923
	movl	8(%rdi), %esi
	movl	%ebx, -108(%rbp)
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
	movq	464(%r12), %rdi
	movq	-104(%rbp), %rdx
	movl	-108(%rbp), %eax
	testq	%rdi, %rdi
	movl	%edx, %r15d
	je	.L4923
	salq	$32, %rdx
	movl	%ebx, %esi
	orq	%rdx, %rsi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movq	%rax, %r14
	shrq	$32, %r14
.L4923:
	movzbl	472(%r12), %esi
	testl	%r14d, %r14d
	je	.L4924
	movl	$-5, %ecx
	movl	$-1, %edi
	subl	%eax, %ecx
	leal	128(%rcx), %eax
	testb	%sil, %sil
	je	.L4925
	movl	476(%r12), %edi
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4925:
	cmpl	$255, %eax
	jbe	.L4933
	leal	32768(%rcx), %eax
	cmpl	$65536, %eax
	sbbl	%edx, %edx
	andl	$-2, %edx
	addl	$4, %edx
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4927:
	cmpl	$255, %r15d
	jbe	.L4928
	cmpl	$65536, %r15d
	movl	$4, %eax
	cmovb	%edx, %eax
.L4928:
	movb	%al, -68(%rbp)
	movzbl	480(%r12), %eax
	movb	$99, -96(%rbp)
	movb	%sil, -64(%rbp)
	movl	%edi, -60(%rbp)
	movl	%r13d, -92(%rbp)
	movl	%ecx, -88(%rbp)
	movl	%r15d, -84(%rbp)
	movq	$0, -80(%rbp)
	movl	$3, -72(%rbp)
	testb	%al, %al
	je	.L4930
	testb	%sil, %sil
	jne	.L4931
	movl	484(%r12), %edx
	movb	%al, -64(%rbp)
	movl	%edx, -60(%rbp)
.L4932:
	movb	$0, 480(%r12)
	movl	$-1, 484(%r12)
.L4930:
	leaq	-96(%rbp), %rsi
	leaq	320(%r12), %rdi
	call	_ZN2v88internal11interpreter19BytecodeArrayWriter5WriteEPNS1_12BytecodeNodeE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4954
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4924:
	.cfi_restore_state
	testb	%sil, %sil
	jne	.L4955
	movl	$-5, %ecx
	movl	$-1, %edi
.L4933:
	movl	$2, %edx
	movl	$1, %eax
	jmp	.L4927
	.p2align 4,,10
	.p2align 3
.L4931:
	cmpb	$2, %al
	jne	.L4932
	cmpb	$1, %sil
	jne	.L4932
	movb	$2, -64(%rbp)
	jmp	.L4932
	.p2align 4,,10
	.p2align 3
.L4955:
	movl	476(%r12), %edi
	movl	$-5, %ecx
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
	jmp	.L4933
.L4954:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20264:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder13CallJSRuntimeEiNS1_12RegisterListE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder13CallJSRuntimeEiNS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder6DeleteENS1_8RegisterENS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder6DeleteENS1_8RegisterENS0_12LanguageModeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder6DeleteENS1_8RegisterENS0_12LanguageModeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder6DeleteENS1_8RegisterENS0_12LanguageModeE:
.LFB20265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	464(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L4957
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE84ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4958
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L4958:
	movl	$-5, %eax
	movl	$-1, %r14d
	movzbl	472(%r12), %ebx
	subl	%r13d, %eax
	movl	%eax, %r13d
	testb	%bl, %bl
	je	.L4959
	cmpb	$2, %bl
	je	.L4960
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L4960
	movl	$84, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	jne	.L4969
.L4960:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4959:
	leal	128(%r13), %edx
	movl	$1, %eax
	cmpl	$255, %edx
	jbe	.L4961
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4961:
	movb	$84, -80(%rbp)
.L4991:
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movb	%al, -52(%rbp)
	movb	%bl, -48(%rbp)
	movl	%r14d, -44(%rbp)
	movl	%r13d, -76(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$1, -56(%rbp)
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder5WriteEPNS1_12BytecodeNodeE
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4992
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4957:
	.cfi_restore_state
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder23PrepareToOutputBytecodeILNS1_8BytecodeE83ELNS1_14AccumulatorUseE3EEEvv.isra.0
	movq	464(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4963
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	movl	%eax, %r13d
.L4963:
	movl	$-5, %eax
	movl	$-1, %r14d
	movzbl	472(%r12), %ebx
	subl	%r13d, %eax
	movl	%eax, %r13d
	testb	%bl, %bl
	je	.L4964
	cmpb	$2, %bl
	je	.L4965
	cmpb	$0, _ZN2v88internal41FLAG_ignition_filter_expression_positionsE(%rip)
	je	.L4965
	movl	$83, %edi
	call	_ZN2v88internal11interpreter9Bytecodes28IsWithoutExternalSideEffectsENS1_8BytecodeE
	testb	%al, %al
	jne	.L4973
.L4965:
	movl	476(%r12), %r14d
	movb	$0, 472(%r12)
	movl	$-1, 476(%r12)
.L4964:
	leal	128(%r13), %edx
	movl	$1, %eax
	cmpl	$255, %edx
	jbe	.L4966
	leal	32768(%r13), %eax
	cmpl	$65536, %eax
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$4, %eax
.L4966:
	movb	$83, -80(%rbp)
	jmp	.L4991
	.p2align 4,,10
	.p2align 3
.L4969:
	xorl	%ebx, %ebx
	jmp	.L4959
	.p2align 4,,10
	.p2align 3
.L4973:
	xorl	%ebx, %ebx
	jmp	.L4964
.L4992:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20265:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder6DeleteENS1_8RegisterENS0_12LanguageModeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder6DeleteENS1_8RegisterENS0_12LanguageModeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEPKNS0_12AstRawStringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEPKNS0_12AstRawStringE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEPKNS0_12AstRawStringE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEPKNS0_12AstRawStringE:
.LFB20266:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_12AstRawStringE@PLT
	.cfi_endproc
.LFE20266:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEPKNS0_12AstRawStringE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEPKNS0_12AstRawStringE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryENS0_9AstBigIntE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryENS0_9AstBigIntE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryENS0_9AstBigIntE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryENS0_9AstBigIntE:
.LFB20267:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertENS0_9AstBigIntE@PLT
	.cfi_endproc
.LFE20267:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryENS0_9AstBigIntE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryENS0_9AstBigIntE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEPKNS0_5ScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEPKNS0_5ScopeE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEPKNS0_5ScopeE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEPKNS0_5ScopeE:
.LFB20268:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEPKNS0_5ScopeE@PLT
	.cfi_endproc
.LFE20268:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEPKNS0_5ScopeE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEPKNS0_5ScopeE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEd
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEd, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEd:
.LFB20269:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder6InsertEd@PLT
	.cfi_endproc
.LFE20269:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEd, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder20GetConstantPoolEntryEd
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder36AsyncIteratorSymbolConstantPoolEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder36AsyncIteratorSymbolConstantPoolEntryEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder36AsyncIteratorSymbolConstantPoolEntryEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder36AsyncIteratorSymbolConstantPoolEntryEv:
.LFB20270:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder25InsertAsyncIteratorSymbolEv@PLT
	.cfi_endproc
.LFE20270:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder36AsyncIteratorSymbolConstantPoolEntryEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder36AsyncIteratorSymbolConstantPoolEntryEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder34ClassFieldsSymbolConstantPoolEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder34ClassFieldsSymbolConstantPoolEntryEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder34ClassFieldsSymbolConstantPoolEntryEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder34ClassFieldsSymbolConstantPoolEntryEv:
.LFB20271:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder23InsertClassFieldsSymbolEv@PLT
	.cfi_endproc
.LFE20271:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder34ClassFieldsSymbolConstantPoolEntryEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder34ClassFieldsSymbolConstantPoolEntryEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder50EmptyObjectBoilerplateDescriptionConstantPoolEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder50EmptyObjectBoilerplateDescriptionConstantPoolEntryEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder50EmptyObjectBoilerplateDescriptionConstantPoolEntryEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder50EmptyObjectBoilerplateDescriptionConstantPoolEntryEv:
.LFB20272:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder39InsertEmptyObjectBoilerplateDescriptionEv@PLT
	.cfi_endproc
.LFE20272:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder50EmptyObjectBoilerplateDescriptionConstantPoolEntryEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder50EmptyObjectBoilerplateDescriptionConstantPoolEntryEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder49EmptyArrayBoilerplateDescriptionConstantPoolEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder49EmptyArrayBoilerplateDescriptionConstantPoolEntryEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder49EmptyArrayBoilerplateDescriptionConstantPoolEntryEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder49EmptyArrayBoilerplateDescriptionConstantPoolEntryEv:
.LFB20273:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder38InsertEmptyArrayBoilerplateDescriptionEv@PLT
	.cfi_endproc
.LFE20273:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder49EmptyArrayBoilerplateDescriptionConstantPoolEntryEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder49EmptyArrayBoilerplateDescriptionConstantPoolEntryEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder32EmptyFixedArrayConstantPoolEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder32EmptyFixedArrayConstantPoolEntryEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder32EmptyFixedArrayConstantPoolEntryEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder32EmptyFixedArrayConstantPoolEntryEv:
.LFB20274:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder21InsertEmptyFixedArrayEv@PLT
	.cfi_endproc
.LFE20274:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder32EmptyFixedArrayConstantPoolEntryEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder32EmptyFixedArrayConstantPoolEntryEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder33HomeObjectSymbolConstantPoolEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder33HomeObjectSymbolConstantPoolEntryEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder33HomeObjectSymbolConstantPoolEntryEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder33HomeObjectSymbolConstantPoolEntryEv:
.LFB20275:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder22InsertHomeObjectSymbolEv@PLT
	.cfi_endproc
.LFE20275:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder33HomeObjectSymbolConstantPoolEntryEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder33HomeObjectSymbolConstantPoolEntryEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder31IteratorSymbolConstantPoolEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder31IteratorSymbolConstantPoolEntryEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder31IteratorSymbolConstantPoolEntryEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder31IteratorSymbolConstantPoolEntryEv:
.LFB20276:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder20InsertIteratorSymbolEv@PLT
	.cfi_endproc
.LFE20276:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder31IteratorSymbolConstantPoolEntryEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder31IteratorSymbolConstantPoolEntryEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder44InterpreterTrampolineSymbolConstantPoolEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder44InterpreterTrampolineSymbolConstantPoolEntryEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder44InterpreterTrampolineSymbolConstantPoolEntryEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder44InterpreterTrampolineSymbolConstantPoolEntryEv:
.LFB20277:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder33InsertInterpreterTrampolineSymbolEv@PLT
	.cfi_endproc
.LFE20277:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder44InterpreterTrampolineSymbolConstantPoolEntryEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder44InterpreterTrampolineSymbolConstantPoolEntryEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder20NaNConstantPoolEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder20NaNConstantPoolEntryEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder20NaNConstantPoolEntryEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder20NaNConstantPoolEntryEv:
.LFB20278:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder9InsertNaNEv@PLT
	.cfi_endproc
.LFE20278:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder20NaNConstantPoolEntryEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder20NaNConstantPoolEntryEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder17AllocateJumpTableEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder17AllocateJumpTableEii
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder17AllocateJumpTableEii, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder17AllocateJumpTableEii:
.LFB20279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$24, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_ZN2v88internal11interpreter20ConstantArrayBuilder15InsertJumpTableEm@PLT
	movq	(%r12), %rdi
	movq	%rax, %r14
	movq	24(%rdi), %rdx
	movq	16(%rdi), %rax
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L5010
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rdi)
.L5008:
	movq	%r14, (%rax)
	movq	$-1, 8(%rax)
	movl	%ebx, 16(%rax)
	movl	%r13d, 20(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5010:
	.cfi_restore_state
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L5008
	.cfi_endproc
.LFE20279:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder17AllocateJumpTableEii, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder17AllocateJumpTableEii
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv:
.LFB20280:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder14InsertDeferredEv@PLT
	.cfi_endproc
.LFE20280:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder33AllocateDeferredConstantPoolEntryEv
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder28SetDeferredConstantPoolEntryEmNS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder28SetDeferredConstantPoolEntryEmNS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder28SetDeferredConstantPoolEntryEmNS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder28SetDeferredConstantPoolEntryEmNS0_6HandleINS0_6ObjectEEE:
.LFB20281:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN2v88internal11interpreter20ConstantArrayBuilder13SetDeferredAtEmNS0_6HandleINS0_6ObjectEEE@PLT
	.cfi_endproc
.LFE20281:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder28SetDeferredConstantPoolEntryEmNS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder28SetDeferredConstantPoolEntryEmNS0_6HandleINS0_6ObjectEEE
	.section	.text._ZNK2v88internal11interpreter20BytecodeArrayBuilder15RegisterIsValidENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter20BytecodeArrayBuilder15RegisterIsValidENS1_8RegisterE
	.type	_ZNK2v88internal11interpreter20BytecodeArrayBuilder15RegisterIsValidENS1_8RegisterE, @function
_ZNK2v88internal11interpreter20BytecodeArrayBuilder15RegisterIsValidENS1_8RegisterE:
.LFB20282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	%esi, -20(%rbp)
	cmpl	$2147483647, %esi
	je	.L5019
	leaq	-20(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	testb	%al, %al
	je	.L5016
.L5017:
	movl	$1, %eax
.L5013:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5016:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter8Register19is_function_closureEv@PLT
	testb	%al, %al
	jne	.L5017
	movl	-20(%rbp), %eax
	testl	%eax, %eax
	js	.L5021
	cmpl	%eax, 300(%rbx)
	jg	.L5017
	cmpl	304(%rbx), %eax
	setl	%al
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5021:
	.cfi_restore_state
	movl	296(%rbx), %esi
	movq	%r12, %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	testl	%eax, %eax
	jns	.L5022
.L5019:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5022:
	.cfi_restore_state
	cmpl	296(%rbx), %eax
	setl	%al
	jmp	.L5013
	.cfi_endproc
.LFE20282:
	.size	_ZNK2v88internal11interpreter20BytecodeArrayBuilder15RegisterIsValidENS1_8RegisterE, .-_ZNK2v88internal11interpreter20BytecodeArrayBuilder15RegisterIsValidENS1_8RegisterE
	.section	.text._ZNK2v88internal11interpreter20BytecodeArrayBuilder19RegisterListIsValidENS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter20BytecodeArrayBuilder19RegisterListIsValidENS1_12RegisterListE
	.type	_ZNK2v88internal11interpreter20BytecodeArrayBuilder19RegisterListIsValidENS1_12RegisterListE, @function
_ZNK2v88internal11interpreter20BytecodeArrayBuilder19RegisterListIsValidENS1_12RegisterListE:
.LFB20283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	sarq	$32, %r12
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r12d, %r12d
	jle	.L5035
	movl	%esi, -44(%rbp)
	cmpl	$2147483647, %esi
	je	.L5033
	movq	%rdi, %r14
	leal	1(%rsi), %ebx
	leaq	-44(%rbp), %r13
	addl	%esi, %r12d
	.p2align 4,,10
	.p2align 3
.L5028:
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter8Register18is_current_contextEv@PLT
	testb	%al, %al
	jne	.L5030
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter8Register19is_function_closureEv@PLT
	testb	%al, %al
	jne	.L5030
	movl	-44(%rbp), %edx
	testl	%edx, %edx
	js	.L5040
	cmpl	300(%r14), %edx
	jl	.L5030
	cmpl	304(%r14), %edx
	jge	.L5023
	.p2align 4,,10
	.p2align 3
.L5030:
	cmpl	%ebx, %r12d
	je	.L5035
	movl	%ebx, -44(%rbp)
	addl	$1, %ebx
	cmpl	$-2147483648, %ebx
	jne	.L5028
.L5033:
	xorl	%eax, %eax
.L5023:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5041
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5035:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L5023
	.p2align 4,,10
	.p2align 3
.L5040:
	movl	296(%r14), %esi
	movq	%r13, %rdi
	call	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi@PLT
	testl	%eax, %eax
	js	.L5033
	cmpl	296(%r14), %eax
	jl	.L5030
	jmp	.L5033
.L5041:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20283:
	.size	_ZNK2v88internal11interpreter20BytecodeArrayBuilder19RegisterListIsValidENS1_12RegisterListE, .-_ZNK2v88internal11interpreter20BytecodeArrayBuilder19RegisterListIsValidENS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder23GetInputRegisterOperandENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder23GetInputRegisterOperandENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder23GetInputRegisterOperandENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder23GetInputRegisterOperandENS1_8RegisterE:
.LFB20285:
	.cfi_startproc
	endbr64
	movq	464(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5048
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer16GetInputRegisterENS1_8RegisterE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%eax, %esi
	movl	$-5, %eax
	subl	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5048:
	.cfi_restore 6
	movl	$-5, %eax
	subl	%esi, %eax
	ret
	.cfi_endproc
.LFE20285:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder23GetInputRegisterOperandENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder23GetInputRegisterOperandENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder24GetOutputRegisterOperandENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder24GetOutputRegisterOperandENS1_8RegisterE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder24GetOutputRegisterOperandENS1_8RegisterE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder24GetOutputRegisterOperandENS1_8RegisterE:
.LFB20286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	464(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5052
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer21PrepareOutputRegisterENS1_8RegisterE@PLT
.L5052:
	addq	$8, %rsp
	movl	$-5, %eax
	subl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20286:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder24GetOutputRegisterOperandENS1_8RegisterE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder24GetOutputRegisterOperandENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder27GetInputRegisterListOperandENS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder27GetInputRegisterListOperandENS1_12RegisterListE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder27GetInputRegisterListOperandENS1_12RegisterListE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder27GetInputRegisterListOperandENS1_12RegisterListE:
.LFB20287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	464(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L5058
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer20GetInputRegisterListENS1_12RegisterListE@PLT
	movq	%rax, %rsi
.L5058:
	movl	$-5, %edx
	movq	%rsi, %rcx
	popq	%rbp
	.cfi_def_cfa 7, 8
	sarq	$32, %rcx
	movl	%edx, %eax
	subl	%esi, %eax
	testl	%ecx, %ecx
	cmove	%edx, %eax
	ret
	.cfi_endproc
.LFE20287:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder27GetInputRegisterListOperandENS1_12RegisterListE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder27GetInputRegisterListOperandENS1_12RegisterListE
	.section	.text._ZN2v88internal11interpreter20BytecodeArrayBuilder28GetOutputRegisterListOperandENS1_12RegisterListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter20BytecodeArrayBuilder28GetOutputRegisterListOperandENS1_12RegisterListE
	.type	_ZN2v88internal11interpreter20BytecodeArrayBuilder28GetOutputRegisterListOperandENS1_12RegisterListE, @function
_ZN2v88internal11interpreter20BytecodeArrayBuilder28GetOutputRegisterListOperandENS1_12RegisterListE:
.LFB20288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	464(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5066
	call	_ZN2v88internal11interpreter25BytecodeRegisterOptimizer25PrepareOutputRegisterListENS1_12RegisterListE@PLT
.L5066:
	movq	%rbx, %rcx
	movl	$-5, %edx
	sarq	$32, %rcx
	movl	%edx, %eax
	subl	%ebx, %eax
	testl	%ecx, %ecx
	cmove	%edx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20288:
	.size	_ZN2v88internal11interpreter20BytecodeArrayBuilder28GetOutputRegisterListOperandENS1_12RegisterListE, .-_ZN2v88internal11interpreter20BytecodeArrayBuilder28GetOutputRegisterListOperandENS1_12RegisterListE
	.section	.rodata._ZN2v88internal11interpreterlsERSoRKNS1_20BytecodeArrayBuilder13ToBooleanModeE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"AlreadyBoolean"
.LC4:
	.string	"ConvertToBoolean"
	.section	.text._ZN2v88internal11interpreterlsERSoRKNS1_20BytecodeArrayBuilder13ToBooleanModeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal11interpreterlsERSoRKNS1_20BytecodeArrayBuilder13ToBooleanModeE
	.type	_ZN2v88internal11interpreterlsERSoRKNS1_20BytecodeArrayBuilder13ToBooleanModeE, @function
_ZN2v88internal11interpreterlsERSoRKNS1_20BytecodeArrayBuilder13ToBooleanModeE:
.LFB20289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	(%rsi), %eax
	testl	%eax, %eax
	je	.L5074
	cmpl	$1, %eax
	jne	.L5075
	movl	$14, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5074:
	.cfi_restore_state
	movl	$16, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5075:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20289:
	.size	_ZN2v88internal11interpreterlsERSoRKNS1_20BytecodeArrayBuilder13ToBooleanModeE, .-_ZN2v88internal11interpreterlsERSoRKNS1_20BytecodeArrayBuilder13ToBooleanModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter20BytecodeArrayBuilderC2EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter20BytecodeArrayBuilderC2EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter20BytecodeArrayBuilderC2EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE:
.LFB25602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25602:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter20BytecodeArrayBuilderC2EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE, .-_GLOBAL__sub_I__ZN2v88internal11interpreter20BytecodeArrayBuilderC2EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter20BytecodeArrayBuilderC2EPNS0_4ZoneEiiPNS0_18FeedbackVectorSpecENS0_26SourcePositionTableBuilder13RecordingModeE
	.weak	_ZTVN2v88internal11interpreter22RegisterTransferWriterE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter22RegisterTransferWriterE,"awG",@progbits,_ZTVN2v88internal11interpreter22RegisterTransferWriterE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter22RegisterTransferWriterE, @object
	.size	_ZTVN2v88internal11interpreter22RegisterTransferWriterE, 56
_ZTVN2v88internal11interpreter22RegisterTransferWriterE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter22RegisterTransferWriterD1Ev
	.quad	_ZN2v88internal11interpreter22RegisterTransferWriterD0Ev
	.quad	_ZN2v88internal11interpreter22RegisterTransferWriter8EmitLdarENS1_8RegisterE
	.quad	_ZN2v88internal11interpreter22RegisterTransferWriter8EmitStarENS1_8RegisterE
	.quad	_ZN2v88internal11interpreter22RegisterTransferWriter7EmitMovENS1_8RegisterES3_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
