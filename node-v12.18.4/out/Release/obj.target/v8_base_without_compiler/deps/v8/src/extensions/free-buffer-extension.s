	.file	"free-buffer-extension.cc"
	.text
	.section	.text._ZN2v89ExtensionD2Ev,"axG",@progbits,_ZN2v89ExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v89ExtensionD2Ev
	.type	_ZN2v89ExtensionD2Ev, @function
_ZN2v89ExtensionD2Ev:
.LFB2485:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.cfi_endproc
.LFE2485:
	.size	_ZN2v89ExtensionD2Ev, .-_ZN2v89ExtensionD2Ev
	.weak	_ZN2v89ExtensionD1Ev
	.set	_ZN2v89ExtensionD1Ev,_ZN2v89ExtensionD2Ev
	.section	.text._ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,"axG",@progbits,_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.type	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, @function
_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE:
.LFB2488:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2488:
	.size	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, .-_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.text._ZN2v88internal19FreeBufferExtensionD2Ev,"axG",@progbits,_ZN2v88internal19FreeBufferExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19FreeBufferExtensionD2Ev
	.type	_ZN2v88internal19FreeBufferExtensionD2Ev, @function
_ZN2v88internal19FreeBufferExtensionD2Ev:
.LFB9854:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L5:
	ret
	.cfi_endproc
.LFE9854:
	.size	_ZN2v88internal19FreeBufferExtensionD2Ev, .-_ZN2v88internal19FreeBufferExtensionD2Ev
	.weak	_ZN2v88internal19FreeBufferExtensionD1Ev
	.set	_ZN2v88internal19FreeBufferExtensionD1Ev,_ZN2v88internal19FreeBufferExtensionD2Ev
	.section	.text._ZN2v88internal19FreeBufferExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19FreeBufferExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.type	_ZN2v88internal19FreeBufferExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, @function
_ZN2v88internal19FreeBufferExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE:
.LFB8609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	_ZN2v88internal19FreeBufferExtension10FreeBufferERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8609:
	.size	_ZN2v88internal19FreeBufferExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, .-_ZN2v88internal19FreeBufferExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.text._ZN2v88internal19FreeBufferExtension10FreeBufferERKNS_20FunctionCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19FreeBufferExtension10FreeBufferERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal19FreeBufferExtension10FreeBufferERKNS_20FunctionCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal19FreeBufferExtension10FreeBufferERKNS_20FunctionCallbackInfoINS_5ValueEEE:
.LFB8610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jg	.L10
	movq	(%rdi), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L11:
	leaq	-80(%rbp), %rdi
	call	_ZN2v811ArrayBuffer11ExternalizeEv@PLT
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	movq	45544(%rax), %rdi
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	movq	(%rdi), %rax
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	32(%rax), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movq	8(%rdi), %rsi
	jmp	.L11
.L15:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8610:
	.size	_ZN2v88internal19FreeBufferExtension10FreeBufferERKNS_20FunctionCallbackInfoINS_5ValueEEE, .-_ZN2v88internal19FreeBufferExtension10FreeBufferERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal19FreeBufferExtensionD0Ev,"axG",@progbits,_ZN2v88internal19FreeBufferExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19FreeBufferExtensionD0Ev
	.type	_ZN2v88internal19FreeBufferExtensionD0Ev, @function
_ZN2v88internal19FreeBufferExtensionD0Ev:
.LFB9856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L17
	movq	(%rdi), %rax
	call	*8(%rax)
.L17:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9856:
	.size	_ZN2v88internal19FreeBufferExtensionD0Ev, .-_ZN2v88internal19FreeBufferExtensionD0Ev
	.section	.text._ZN2v89ExtensionD0Ev,"axG",@progbits,_ZN2v89ExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v89ExtensionD0Ev
	.type	_ZN2v89ExtensionD0Ev, @function
_ZN2v89ExtensionD0Ev:
.LFB2487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE2487:
	.size	_ZN2v89ExtensionD0Ev, .-_ZN2v89ExtensionD0Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal19FreeBufferExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal19FreeBufferExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, @function
_GLOBAL__sub_I__ZN2v88internal19FreeBufferExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE:
.LFB9874:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE9874:
	.size	_GLOBAL__sub_I__ZN2v88internal19FreeBufferExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, .-_GLOBAL__sub_I__ZN2v88internal19FreeBufferExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal19FreeBufferExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.weak	_ZTVN2v89ExtensionE
	.section	.data.rel.ro.local._ZTVN2v89ExtensionE,"awG",@progbits,_ZTVN2v89ExtensionE,comdat
	.align 8
	.type	_ZTVN2v89ExtensionE, @object
	.size	_ZTVN2v89ExtensionE, 40
_ZTVN2v89ExtensionE:
	.quad	0
	.quad	0
	.quad	_ZN2v89ExtensionD1Ev
	.quad	_ZN2v89ExtensionD0Ev
	.quad	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.weak	_ZTVN2v88internal19FreeBufferExtensionE
	.section	.data.rel.ro.local._ZTVN2v88internal19FreeBufferExtensionE,"awG",@progbits,_ZTVN2v88internal19FreeBufferExtensionE,comdat
	.align 8
	.type	_ZTVN2v88internal19FreeBufferExtensionE, @object
	.size	_ZTVN2v88internal19FreeBufferExtensionE, 40
_ZTVN2v88internal19FreeBufferExtensionE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19FreeBufferExtensionD1Ev
	.quad	_ZN2v88internal19FreeBufferExtensionD0Ev
	.quad	_ZN2v88internal19FreeBufferExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
