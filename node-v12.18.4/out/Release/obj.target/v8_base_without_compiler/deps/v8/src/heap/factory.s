	.file	"factory.cc"
	.text
	.section	.text._ZNK2v86String26ExternalStringResourceBase11IsCacheableEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.type	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv, @function
_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv:
.LFB3648:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE3648:
	.size	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv, .-_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5716:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5716:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5717:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5717:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v88internal10PagedSpace24SupportsInlineAllocationEv,"axG",@progbits,_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.type	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv, @function
_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv:
.LFB19615:
	.cfi_startproc
	endbr64
	cmpl	$2, 72(%rdi)
	je	.L13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*152(%rax)
	popq	%rbp
	.cfi_def_cfa 7, 8
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE19615:
	.size	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv, .-_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv
	.section	.text._ZNK2v88internal29NativesExternalStringResource4dataEv,"axG",@progbits,_ZNK2v88internal29NativesExternalStringResource4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal29NativesExternalStringResource4dataEv
	.type	_ZNK2v88internal29NativesExternalStringResource4dataEv, @function
_ZNK2v88internal29NativesExternalStringResource4dataEv:
.LFB23114:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE23114:
	.size	_ZNK2v88internal29NativesExternalStringResource4dataEv, .-_ZNK2v88internal29NativesExternalStringResource4dataEv
	.section	.text._ZNK2v88internal29NativesExternalStringResource6lengthEv,"axG",@progbits,_ZNK2v88internal29NativesExternalStringResource6lengthEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal29NativesExternalStringResource6lengthEv
	.type	_ZNK2v88internal29NativesExternalStringResource6lengthEv, @function
_ZNK2v88internal29NativesExternalStringResource6lengthEv:
.LFB23115:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE23115:
	.size	_ZNK2v88internal29NativesExternalStringResource6lengthEv, .-_ZNK2v88internal29NativesExternalStringResource6lengthEv
	.section	.text._ZN2v88internal19SequentialStringKeyIhED2Ev,"axG",@progbits,_ZN2v88internal19SequentialStringKeyIhED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19SequentialStringKeyIhED2Ev
	.type	_ZN2v88internal19SequentialStringKeyIhED2Ev, @function
_ZN2v88internal19SequentialStringKeyIhED2Ev:
.LFB27176:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27176:
	.size	_ZN2v88internal19SequentialStringKeyIhED2Ev, .-_ZN2v88internal19SequentialStringKeyIhED2Ev
	.weak	_ZN2v88internal19SequentialStringKeyIhED1Ev
	.set	_ZN2v88internal19SequentialStringKeyIhED1Ev,_ZN2v88internal19SequentialStringKeyIhED2Ev
	.section	.text._ZN2v88internal19SequentialStringKeyItED2Ev,"axG",@progbits,_ZN2v88internal19SequentialStringKeyItED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19SequentialStringKeyItED2Ev
	.type	_ZN2v88internal19SequentialStringKeyItED2Ev, @function
_ZN2v88internal19SequentialStringKeyItED2Ev:
.LFB27203:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27203:
	.size	_ZN2v88internal19SequentialStringKeyItED2Ev, .-_ZN2v88internal19SequentialStringKeyItED2Ev
	.weak	_ZN2v88internal19SequentialStringKeyItED1Ev
	.set	_ZN2v88internal19SequentialStringKeyItED1Ev,_ZN2v88internal19SequentialStringKeyItED2Ev
	.section	.text._ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED2Ev,"axG",@progbits,_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED2Ev
	.type	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED2Ev, @function
_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED2Ev:
.LFB27208:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27208:
	.size	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED2Ev, .-_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED2Ev
	.weak	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED1Ev
	.set	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED1Ev,_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED2Ev
	.section	.text._ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED2Ev,"axG",@progbits,_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED2Ev
	.type	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED2Ev, @function
_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED2Ev:
.LFB27213:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE27213:
	.size	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED2Ev, .-_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED2Ev
	.weak	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED1Ev
	.set	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED1Ev,_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED2Ev
	.section	.text._ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_14UncompiledData10InitializeES5_NS1_6StringEiiSt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SE_,"axG",@progbits,_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_14UncompiledData10InitializeES5_NS1_6StringEiiSt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SE_,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_14UncompiledData10InitializeES5_NS1_6StringEiiSt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SE_
	.type	_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_14UncompiledData10InitializeES5_NS1_6StringEiiSt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SE_, @function
_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_14UncompiledData10InitializeES5_NS1_6StringEiiSt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SE_:
.LFB29660:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE29660:
	.size	_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_14UncompiledData10InitializeES5_NS1_6StringEiiSt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SE_, .-_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_14UncompiledData10InitializeES5_NS1_6StringEiiSt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SE_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal14UncompiledData10InitializeES3_NS2_6StringEiiSt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES6_EEEd_UlS6_S7_S6_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v88internal14UncompiledData10InitializeES3_NS2_6StringEiiSt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES6_EEEd_UlS6_S7_S6_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v88internal14UncompiledData10InitializeES3_NS2_6StringEiiSt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES6_EEEd_UlS6_S7_S6_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal14UncompiledData10InitializeES3_NS2_6StringEiiSt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES6_EEEd_UlS6_S7_S6_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal14UncompiledData10InitializeES3_NS2_6StringEiiSt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES6_EEEd_UlS6_S7_S6_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation:
.LFB29661:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L23
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE29661:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal14UncompiledData10InitializeES3_NS2_6StringEiiSt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES6_EEEd_UlS6_S7_S6_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal14UncompiledData10InitializeES3_NS2_6StringEiiSt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES6_EEEd_UlS6_S7_S6_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_30UncompiledDataWithPreparseData10InitializeES5_NS1_6StringEiiNS1_12PreparseDataESt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SF_,"axG",@progbits,_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_30UncompiledDataWithPreparseData10InitializeES5_NS1_6StringEiiNS1_12PreparseDataESt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SF_,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_30UncompiledDataWithPreparseData10InitializeES5_NS1_6StringEiiNS1_12PreparseDataESt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SF_
	.type	_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_30UncompiledDataWithPreparseData10InitializeES5_NS1_6StringEiiNS1_12PreparseDataESt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SF_, @function
_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_30UncompiledDataWithPreparseData10InitializeES5_NS1_6StringEiiNS1_12PreparseDataESt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SF_:
.LFB29665:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE29665:
	.size	_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_30UncompiledDataWithPreparseData10InitializeES5_NS1_6StringEiiNS1_12PreparseDataESt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SF_, .-_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_30UncompiledDataWithPreparseData10InitializeES5_NS1_6StringEiiNS1_12PreparseDataESt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SF_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v88internal30UncompiledDataWithPreparseData10InitializeES3_NS2_6StringEiiNS2_12PreparseDataESt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES7_EEEd_UlS7_S8_S7_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v88internal30UncompiledDataWithPreparseData10InitializeES3_NS2_6StringEiiNS2_12PreparseDataESt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES7_EEEd_UlS7_S8_S7_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v88internal30UncompiledDataWithPreparseData10InitializeES3_NS2_6StringEiiNS2_12PreparseDataESt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES7_EEEd_UlS7_S8_S7_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v88internal30UncompiledDataWithPreparseData10InitializeES3_NS2_6StringEiiNS2_12PreparseDataESt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES7_EEEd_UlS7_S8_S7_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v88internal30UncompiledDataWithPreparseData10InitializeES3_NS2_6StringEiiNS2_12PreparseDataESt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES7_EEEd_UlS7_S8_S7_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation:
.LFB29666:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L27
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE29666:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v88internal30UncompiledDataWithPreparseData10InitializeES3_NS2_6StringEiiNS2_12PreparseDataESt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES7_EEEd_UlS7_S8_S7_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v88internal30UncompiledDataWithPreparseData10InitializeES3_NS2_6StringEiiNS2_12PreparseDataESt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES7_EEEd_UlS7_S8_S7_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation
	.section	.text._ZN2v88internal19SequentialStringKeyIhED0Ev,"axG",@progbits,_ZN2v88internal19SequentialStringKeyIhED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19SequentialStringKeyIhED0Ev
	.type	_ZN2v88internal19SequentialStringKeyIhED0Ev, @function
_ZN2v88internal19SequentialStringKeyIhED0Ev:
.LFB27178:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27178:
	.size	_ZN2v88internal19SequentialStringKeyIhED0Ev, .-_ZN2v88internal19SequentialStringKeyIhED0Ev
	.section	.text._ZN2v88internal19SequentialStringKeyItED0Ev,"axG",@progbits,_ZN2v88internal19SequentialStringKeyItED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19SequentialStringKeyItED0Ev
	.type	_ZN2v88internal19SequentialStringKeyItED0Ev, @function
_ZN2v88internal19SequentialStringKeyItED0Ev:
.LFB27205:
	.cfi_startproc
	endbr64
	movl	$40, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27205:
	.size	_ZN2v88internal19SequentialStringKeyItED0Ev, .-_ZN2v88internal19SequentialStringKeyItED0Ev
	.section	.text._ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED0Ev,"axG",@progbits,_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED0Ev
	.type	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED0Ev, @function
_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED0Ev:
.LFB27210:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27210:
	.size	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED0Ev, .-_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED0Ev
	.section	.text._ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED0Ev,"axG",@progbits,_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED0Ev
	.type	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED0Ev, @function
_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED0Ev:
.LFB27215:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE27215:
	.size	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED0Ev, .-_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED0Ev
	.section	.text._ZN2v88internal11TaggedFieldINS0_6ObjectELi0EE12Relaxed_LoadEPNS0_7IsolateENS0_10HeapObjectEi.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal11TaggedFieldINS0_6ObjectELi0EE12Relaxed_LoadEPNS0_7IsolateENS0_10HeapObjectEi.isra.0, @function
_ZN2v88internal11TaggedFieldINS0_6ObjectELi0EE12Relaxed_LoadEPNS0_7IsolateENS0_10HeapObjectEi.isra.0:
.LFB33038:
	.cfi_startproc
	movslq	%esi, %rsi
	movq	-1(%rdi,%rsi), %rax
	ret
	.cfi_endproc
.LFE33038:
	.size	_ZN2v88internal11TaggedFieldINS0_6ObjectELi0EE12Relaxed_LoadEPNS0_7IsolateENS0_10HeapObjectEi.isra.0, .-_ZN2v88internal11TaggedFieldINS0_6ObjectELi0EE12Relaxed_LoadEPNS0_7IsolateENS0_10HeapObjectEi.isra.0
	.section	.text._ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE7IsMatchENS0_6StringE,"axG",@progbits,_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE7IsMatchENS0_6StringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE7IsMatchENS0_6StringE
	.type	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE7IsMatchENS0_6StringE, @function
_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE7IsMatchENS0_6StringE:
.LFB32783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	15(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	-1(%rsi), %rdx
	testb	$8, 11(%rdx)
	movq	-1(%rsi), %rdx
	movzwl	11(%rdx), %edx
	je	.L34
	andl	$7, %edx
	cmpw	$2, %dx
	je	.L51
.L35:
	movq	16(%rbx), %rdx
	movslq	24(%rbx), %rcx
	movq	(%rdx), %rdx
	leaq	15(%rdx,%rcx,2), %rdx
	movslq	12(%rbx), %rcx
	leaq	(%rdx,%rcx,2), %rsi
	cmpq	%rsi, %rdx
	jb	.L38
.L45:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	addq	$2, %rdx
	addq	$1, %rax
	cmpq	%rdx, %rsi
	jbe	.L45
.L38:
	movzbl	(%rax), %ecx
	cmpw	%cx, (%rdx)
	je	.L52
.L46:
	xorl	%eax, %eax
.L55:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	andl	$7, %edx
	cmpw	$2, %dx
	je	.L53
.L40:
	movq	16(%rbx), %rdx
	movslq	24(%rbx), %rcx
	movq	(%rdx), %rdx
	leaq	15(%rdx,%rcx,2), %rdx
	movslq	12(%rbx), %rcx
	leaq	(%rdx,%rcx,2), %rcx
	cmpq	%rcx, %rdx
	jnb	.L45
	movzwl	(%rax), %ebx
	cmpw	%bx, (%rdx)
	jne	.L46
	.p2align 4,,10
	.p2align 3
.L54:
	addq	$2, %rdx
	addq	$2, %rax
	cmpq	%rdx, %rcx
	jbe	.L45
	movzwl	(%rax), %ebx
	cmpw	%bx, (%rdx)
	je	.L54
	xorl	%eax, %eax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L51:
	movq	15(%rsi), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L36
	movq	8(%rdi), %rax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L53:
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	jmp	.L40
.L36:
	call	*%rax
	jmp	.L35
	.cfi_endproc
.LFE32783:
	.size	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE7IsMatchENS0_6StringE, .-_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE7IsMatchENS0_6StringE
	.section	.rodata._ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE.part.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"invalid array length"
	.section	.text._ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE.part.0, @function
_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE.part.0:
.LFB33035:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	37592(%rdi), %r15
	pushq	%r14
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	cmpl	$134217726, %esi
	ja	.L62
.L57:
	movq	488(%r12), %r14
	leal	16(,%rbx,8), %esi
	movq	%r15, %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r14, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L59:
	salq	$32, %rbx
	movq	%rbx, 7(%rsi)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L63
.L60:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movl	%edx, -40(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-40(%rbp), %edx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L60
	.cfi_endproc
.LFE33035:
	.size	_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE.part.0, .-_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE.part.0
	.section	.text._ZN2v88internal19SequentialStringKeyIhE7IsMatchENS0_6StringE,"axG",@progbits,_ZN2v88internal19SequentialStringKeyIhE7IsMatchENS0_6StringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19SequentialStringKeyIhE7IsMatchENS0_6StringE
	.type	_ZN2v88internal19SequentialStringKeyIhE7IsMatchENS0_6StringE, @function
_ZN2v88internal19SequentialStringKeyIhE7IsMatchENS0_6StringE:
.LFB32789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	15(%rsi), %rdi
	subq	$8, %rsp
	movq	-1(%rsi), %rax
	testb	$8, 11(%rax)
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	je	.L65
	andl	$7, %eax
	cmpw	$2, %ax
	je	.L76
.L66:
	movslq	24(%rbx), %rdx
	movq	16(%rbx), %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	andl	$7, %eax
	cmpw	$2, %ax
	je	.L77
.L69:
	movslq	24(%rbx), %rdx
	movq	16(%rbx), %rax
	leaq	(%rdi,%rdx,2), %rcx
	cmpq	%rdi, %rcx
	ja	.L71
.L72:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	addq	$2, %rdi
	addq	$1, %rax
	cmpq	%rdi, %rcx
	jbe	.L72
.L71:
	movzbl	(%rax), %edx
	cmpw	%dx, (%rdi)
	je	.L78
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	%rax, %rdi
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L76:
	movq	15(%rsi), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L67
	movq	8(%rdi), %rdi
	jmp	.L66
.L67:
	call	*%rax
	movq	%rax, %rdi
	jmp	.L66
	.cfi_endproc
.LFE32789:
	.size	_ZN2v88internal19SequentialStringKeyIhE7IsMatchENS0_6StringE, .-_ZN2v88internal19SequentialStringKeyIhE7IsMatchENS0_6StringE
	.section	.text._ZN2v88internal19SequentialStringKeyItE7IsMatchENS0_6StringE,"axG",@progbits,_ZN2v88internal19SequentialStringKeyItE7IsMatchENS0_6StringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19SequentialStringKeyItE7IsMatchENS0_6StringE
	.type	_ZN2v88internal19SequentialStringKeyItE7IsMatchENS0_6StringE, @function
_ZN2v88internal19SequentialStringKeyItE7IsMatchENS0_6StringE:
.LFB32787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	15(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	-1(%rsi), %rdx
	testb	$8, 11(%rdx)
	movq	-1(%rsi), %rdx
	movzwl	11(%rdx), %edx
	je	.L80
	andl	$7, %edx
	cmpw	$2, %dx
	je	.L97
.L81:
	movslq	24(%rbx), %rdi
	movq	16(%rbx), %r8
	leaq	(%rax,%rdi), %rdx
	cmpq	%rdx, %rax
	jnb	.L91
	xorl	%edx, %edx
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L98:
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	je	.L91
.L84:
	movzbl	(%rax,%rdx), %esi
	movzwl	(%r8,%rdx,2), %ecx
	cmpl	%ecx, %esi
	je	.L98
	xorl	%eax, %eax
.L100:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	andl	$7, %edx
	cmpw	$2, %dx
	jne	.L86
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
.L86:
	movslq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	leaq	(%rax,%rcx,2), %rcx
	cmpq	%rax, %rcx
	ja	.L88
.L91:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	addq	$2, %rax
	addq	$2, %rdx
	cmpq	%rax, %rcx
	jbe	.L91
.L88:
	movzwl	(%rdx), %ebx
	cmpw	%bx, (%rax)
	je	.L99
	xorl	%eax, %eax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L97:
	movq	15(%rsi), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L82
	movq	8(%rdi), %rax
	jmp	.L81
.L82:
	call	*%rax
	jmp	.L81
	.cfi_endproc
.LFE32787:
	.size	_ZN2v88internal19SequentialStringKeyItE7IsMatchENS0_6StringE, .-_ZN2v88internal19SequentialStringKeyItE7IsMatchENS0_6StringE
	.section	.text._ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE7IsMatchENS0_6StringE,"axG",@progbits,_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE7IsMatchENS0_6StringE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE7IsMatchENS0_6StringE
	.type	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE7IsMatchENS0_6StringE, @function
_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE7IsMatchENS0_6StringE:
.LFB32785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	15(%rsi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	-1(%rsi), %rax
	testb	$8, 11(%rax)
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	je	.L102
	andl	$7, %eax
	cmpw	$2, %ax
	je	.L113
.L103:
	movq	16(%rbx), %rax
	movslq	12(%rbx), %rdx
	movq	%r8, %rsi
	movq	(%rax), %rcx
	movslq	24(%rbx), %rax
	leaq	15(%rcx,%rax), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	andl	$7, %eax
	cmpw	$2, %ax
	je	.L114
.L106:
	movq	16(%rbx), %rax
	movslq	12(%rbx), %rdi
	movq	(%rax), %rdx
	movslq	24(%rbx), %rax
	leaq	15(%rdx,%rax), %rax
	leaq	(%rax,%rdi), %rdx
	cmpq	%rdx, %rax
	jnb	.L109
	xorl	%edx, %edx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L115:
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	je	.L109
.L108:
	movzbl	(%rax,%rdx), %esi
	movzwl	(%r8,%rdx,2), %ecx
	cmpl	%ecx, %esi
	je	.L115
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	%rax, %r8
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L113:
	movq	15(%rsi), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L104
	movq	8(%rdi), %r8
	jmp	.L103
.L104:
	call	*%rax
	movq	%rax, %r8
	jmp	.L103
	.cfi_endproc
.LFE32785:
	.size	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE7IsMatchENS0_6StringE, .-_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE7IsMatchENS0_6StringE
	.section	.text._ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE.part.0, @function
_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE.part.0:
.LFB33032:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$37592, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	-37504(%rdi), %r13
	cmpl	$134217726, %esi
	ja	.L128
.L117:
	leal	16(,%rbx,8), %r14d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	%r14d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r14d
	jle	.L121
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L129
.L121:
	movq	152(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L130
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L123:
	movslq	%ebx, %rcx
	salq	$32, %rbx
	movq	%rbx, 7(%rsi)
	movq	(%r8), %rax
	leaq	15(%rax), %rdi
	movq	%r13, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$16, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L131
.L124:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L128:
	leaq	.LC0(%rip), %rsi
	movl	%edx, -44(%rbp)
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-44(%rbp), %edx
	movq	-40(%rbp), %rdi
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L121
.L122:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L132
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L124
	.cfi_endproc
.LFE33032:
	.size	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE.part.0, .-_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE.part.0
	.set	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE.part.0,_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE.part.0
	.section	.text._ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE.part.0, @function
_ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE.part.0:
.LFB33037:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	37592(%rdi), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	cmpl	$134217725, %esi
	ja	.L145
.L134:
	leal	24(,%rbx,8), %r13d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r15, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r13d
	jle	.L138
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L146
.L138:
	movq	704(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L147
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L140:
	movq	$0, 15(%rsi)
	salq	$32, %rbx
	movq	(%rax), %rdx
	movq	%rbx, 7(%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L148
.L141:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movl	%edx, -40(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-40(%rbp), %edx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L146:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L138
.L139:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L149
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L141
	.cfi_endproc
.LFE33037:
	.size	_ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE.part.0, .-_ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE.part.0
	.section	.text._ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE.constprop.0, @function
_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE.constprop.0:
.LFB33158:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$37592, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	cmpl	$134217726, %esi
	ja	.L162
.L151:
	leal	16(,%rbx,8), %r14d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	%r15d, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r14d
	jle	.L155
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L163
.L155:
	movq	152(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L164
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L157:
	movslq	%ebx, %rcx
	salq	$32, %rbx
	movq	%rbx, 7(%rsi)
	movq	(%r8), %rax
	leaq	15(%rax), %rdi
	movq	%r13, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$24, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L165
.L158:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L162:
	leaq	.LC0(%rip), %rsi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movq	-56(%rbp), %rdi
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L155
.L156:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L166
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L158
	.cfi_endproc
.LFE33158:
	.size	_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE.constprop.0, .-_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE.constprop.0
	.section	.text._ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0, @function
_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0:
.LFB33154:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	addq	$37592, %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rsi), %rax
	movq	%rsi, %rbx
	movzbl	7(%rax), %esi
	sall	$3, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	(%rbx), %rdx
	movq	%rax, %r12
	movq	%rdx, -1(%rax)
	testb	$1, %dl
	je	.L168
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L174
.L168:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE33154:
	.size	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0, .-_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	.section	.rodata._ZN2v88internal19SequentialStringKeyItE8AsHandleEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"String::kMaxLength >= length"
.LC2:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal19SequentialStringKeyItE8AsHandleEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal19SequentialStringKeyItE8AsHandleEPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19SequentialStringKeyItE8AsHandleEPNS0_7IsolateE
	.type	_ZN2v88internal19SequentialStringKeyItE8AsHandleEPNS0_7IsolateE, @function
_ZN2v88internal19SequentialStringKeyItE8AsHandleEPNS0_7IsolateE:
.LFB32786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movslq	24(%rdi), %r9
	movl	8(%rdi), %eax
	movq	16(%rdi), %r14
	btrq	$63, %r9
	movl	%eax, -52(%rbp)
	cmpl	$1073741799, %r9d
	jg	.L181
	leal	16(%r9,%r9), %r13d
	movq	%rsi, %rbx
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	824(%rsi), %r12
	leal	7(%r13), %esi
	movl	$1, %edx
	movq	%r9, %r15
	andl	$-8, %esi
	leaq	37592(%rbx), %rdi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L177
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L178:
	movl	%r15d, 11(%rsi)
	movl	-52(%rbp), %ecx
	leal	-16(%r13), %edx
	movq	%r14, %rsi
	movq	(%r12), %rax
	movslq	%edx, %rdx
	movl	%ecx, 7(%rax)
	movq	(%r12), %rax
	leaq	15(%rax), %rdi
	call	memcpy@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L182
.L179:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L181:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L182:
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L179
	.cfi_endproc
.LFE32786:
	.size	_ZN2v88internal19SequentialStringKeyItE8AsHandleEPNS0_7IsolateE, .-_ZN2v88internal19SequentialStringKeyItE8AsHandleEPNS0_7IsolateE
	.section	.text._ZN2v88internal19SequentialStringKeyIhE8AsHandleEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal19SequentialStringKeyIhE8AsHandleEPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19SequentialStringKeyIhE8AsHandleEPNS0_7IsolateE
	.type	_ZN2v88internal19SequentialStringKeyIhE8AsHandleEPNS0_7IsolateE, @function
_ZN2v88internal19SequentialStringKeyIhE8AsHandleEPNS0_7IsolateE:
.LFB32788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	8(%rdi), %eax
	movq	24(%rdi), %r15
	movq	16(%rdi), %r13
	movl	%eax, -52(%rbp)
	cmpl	$1073741799, %r15d
	jg	.L191
	movq	%rsi, %rbx
	movslq	%r15d, %r14
	movl	$1, %ecx
	movq	192(%rsi), %r12
	movq	37896(%rbx), %rax
	leal	23(%r14), %esi
	leaq	37592(%rbx), %rdi
	andl	$-8, %esi
	cmpb	$1, 232(%rax)
	sbbl	%edx, %edx
	xorl	%r8d, %r8d
	andl	$3, %edx
	addl	$1, %edx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L186
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L187:
	movl	%r15d, 11(%rsi)
	movl	-52(%rbp), %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	(%r12), %rax
	movl	%ecx, 7(%rax)
	movq	(%r12), %rax
	leaq	15(%rax), %rdi
	call	memcpy@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L192
.L188:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L191:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L188
	.cfi_endproc
.LFE32788:
	.size	_ZN2v88internal19SequentialStringKeyIhE8AsHandleEPNS0_7IsolateE, .-_ZN2v88internal19SequentialStringKeyIhE8AsHandleEPNS0_7IsolateE
	.section	.text._ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_,"axG",@progbits,_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_,comdat
	.p2align 4
	.weak	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	.type	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_, @function
_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_:
.LFB11845:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L193
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L198
.L193:
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE11845:
	.size	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_, .-_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	.section	.text._ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE,"axG",@progbits,_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE,comdat
	.p2align 4
	.weak	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	.type	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE, @function
_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE:
.LFB11856:
	.cfi_startproc
	endbr64
	testb	$1, %dl
	je	.L199
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L205
.L199:
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	jmp	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE11856:
	.size	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE, .-_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	.section	.text._ZN2v88internal10FixedArray3setEiNS0_6ObjectE,"axG",@progbits,_ZN2v88internal10FixedArray3setEiNS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	.type	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE, @function
_ZN2v88internal10FixedArray3setEiNS0_6ObjectE:
.LFB12100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leal	16(,%rsi,8), %ebx
	movq	(%rdi), %rax
	movslq	%ebx, %rbx
	movq	%rdx, -1(%rbx,%rax)
	testb	$1, %dl
	je	.L206
	movq	%rdx, %r14
	movq	%rdi, %r13
	movq	(%rdi), %rdi
	movq	%rdx, %r12
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testl	$262144, %eax
	jne	.L218
	testb	$24, %al
	je	.L206
.L220:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L219
.L206:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testb	$24, %al
	jne	.L220
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L219:
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE12100:
	.size	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE, .-_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	.section	.rodata._ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE,"axG",@progbits,_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	.type	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE, @function
_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE:
.LFB12907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movswq	9(%rax), %rdx
	movl	%edx, -76(%rbp)
	movq	%rdx, %r12
	addl	$1, %edx
	movw	%dx, 9(%rax)
	movq	(%rsi), %rax
	movq	(%rax), %rdx
	movq	16(%rsi), %rax
	movl	8(%rsi), %esi
	testl	%esi, %esi
	jne	.L222
	testq	%rax, %rax
	je	.L225
	movq	(%rax), %rax
	orq	$2, %rax
	movq	%rax, %r13
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L222:
	testq	%rax, %rax
	je	.L225
	movq	(%rax), %r13
.L224:
	movswl	%r12w, %eax
	movq	(%rcx), %rsi
	movl	24(%rbx), %r14d
	leal	3(%rax,%rax,2), %r8d
	sall	$3, %r8d
	movslq	%r8d, %r15
	movq	%rdx, -1(%r15,%rsi)
	movq	(%rcx), %rdi
	testb	$1, %dl
	je	.L242
	movq	%rdx, %r9
	leaq	-1(%r15,%rdi), %r10
	andq	$-262144, %r9
	movq	8(%r9), %rsi
	movq	%r9, -72(%rbp)
	testl	$262144, %esi
	je	.L227
	movq	%r10, %rsi
	movl	%r8d, -80(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	-72(%rbp), %r9
	movl	-80(%rbp), %r8d
	movq	-88(%rbp), %rdx
	movq	(%rcx), %rdi
	movq	8(%r9), %rsi
	leaq	-1(%r15,%rdi), %r10
.L227:
	andl	$24, %esi
	je	.L242
	movq	%rdi, %rsi
	andq	$-262144, %rsi
	testb	$24, 8(%rsi)
	je	.L260
	.p2align 4,,10
	.p2align 3
.L242:
	addl	%r14d, %r14d
	sarl	%r14d
	salq	$32, %r14
	movq	%r14, 7(%r15,%rdi)
	movq	(%rcx), %rdx
	movq	%r13, 15(%r15,%rdx)
	testb	$1, %r13b
	je	.L241
	cmpl	$3, %r13d
	je	.L241
	movq	%r13, %r14
	movq	(%rcx), %rdi
	addl	$16, %r8d
	movq	%r13, %rdx
	andq	$-262144, %r14
	movslq	%r8d, %r8
	andq	$-3, %rdx
	movq	8(%r14), %rax
	movq	%r8, -72(%rbp)
	leaq	-1(%rdi,%r8), %rsi
	testl	$262144, %eax
	jne	.L261
	testb	$24, %al
	je	.L241
.L266:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L241
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L241:
	movq	(%rbx), %rax
	movq	(%rax), %rax
	movl	7(%rax), %ebx
	testb	$1, %bl
	jne	.L232
	shrl	$2, %ebx
.L233:
	movswl	%r12w, %eax
	movq	(%rcx), %rsi
	testl	%eax, %eax
	jle	.L234
	leal	(%rax,%rax,2), %edx
	subl	$1, %eax
	leaq	-64(%rbp), %r14
	subq	%rax, %r12
	sall	$3, %edx
	leaq	(%r12,%r12,2), %r12
	movslq	%edx, %r13
	salq	$3, %r12
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L263:
	shrl	$2, %eax
	leaq	24(%r13), %r15
	leaq	7(%rsi), %rdi
	cmpl	%eax, %ebx
	jnb	.L236
.L264:
	movq	7(%r13,%rsi), %rdi
	movq	7(%r15,%rsi), %rax
	sarq	$32, %rdi
	sarq	$32, %rax
	andl	$-523777, %eax
	andl	$523776, %edi
	orl	%edi, %eax
	addl	%eax, %eax
	sarl	%eax
	salq	$32, %rax
	movq	%rax, 7(%r15,%rsi)
	movq	(%rcx), %rsi
	cmpq	%r12, %r13
	je	.L262
	subq	$24, %r13
.L235:
	movq	7(%r13,%rsi), %rax
	shrq	$41, %rax
	andl	$1023, %eax
	leal	3(%rax,%rax,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rax,%rsi), %rdi
	movl	7(%rdi), %eax
	testb	$1, %al
	je	.L263
	movq	%rdi, -64(%rbp)
	movq	%r14, %rdi
	leaq	24(%r13), %r15
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-72(%rbp), %rcx
	movq	(%rcx), %rsi
	leaq	7(%rsi), %rdi
	cmpl	%eax, %ebx
	jb	.L264
.L236:
	movq	(%r15,%rdi), %rax
	movl	-76(%rbp), %r13d
	sarq	$32, %rax
	sall	$9, %r13d
	andl	$-523777, %eax
	orl	%r13d, %eax
	addl	%eax, %eax
	sarl	%eax
	salq	$32, %rax
	movq	%rax, 7(%r15,%rsi)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L265
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	leaq	-64(%rbp), %rdi
	movq	%rcx, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-72(%rbp), %rcx
	movl	%eax, %ebx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L262:
	leaq	7(%rsi), %rdi
	movq	%r13, %r15
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-96(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	8(%r14), %rax
	movq	-88(%rbp), %rdx
	movq	(%rcx), %rdi
	leaq	-1(%rdi,%r8), %rsi
	testb	$24, %al
	jne	.L266
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%r10, %rsi
	movl	%r8d, -88(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movl	-88(%rbp), %r8d
	movq	(%rcx), %rdi
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L234:
	leaq	7(%rsi), %rdi
	jmp	.L236
.L265:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12907:
	.size	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE, .-_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	.section	.rodata._ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"static_cast<unsigned>(number) <= static_cast<unsigned>(kMaxNumberOfDescriptors)"
	.align 8
.LC5:
	.string	"static_cast<unsigned>(value) <= 255"
	.section	.text._ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE,"axG",@progbits,_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	.type	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE, @function
_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE:
.LFB17000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%r14, %rsi
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	leaq	-48(%rbp), %rdi
	movq	39(%rax), %rdx
	movq	%rdx, -48(%rbp)
	movl	15(%rax), %ebx
	call	_ZN2v88internal15DescriptorArray6AppendEPNS0_10DescriptorE
	shrl	$10, %ebx
	andl	$1023, %ebx
	leal	1(%rbx), %ecx
	cmpl	$1020, %ecx
	jg	.L288
	movq	0(%r13), %rdx
	movl	%ecx, %esi
	sall	$10, %esi
	movl	15(%rdx), %eax
	andl	$-1047553, %eax
	orl	%esi, %eax
	movl	%eax, 15(%rdx)
	movq	-48(%rbp), %rdx
	movq	0(%r13), %rsi
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L289
.L269:
	movq	(%r14), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	je	.L290
.L271:
	testb	$2, 24(%r14)
	je	.L291
.L267:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L292
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	leaq	37592(%r12), %rdi
	call	_ZN2v88internal41Heap_MarkingBarrierForDescriptorArraySlowEPNS0_4HeapENS0_10HeapObjectES3_i@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L290:
	testb	$8, 11(%rax)
	je	.L271
	movq	0(%r13), %rdx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	testb	$2, 24(%r14)
	jne	.L267
.L291:
	movq	0(%r13), %rdx
	movzbl	9(%rdx), %eax
	cmpl	$2, %eax
	jle	.L274
	movzbl	7(%rdx), %ecx
	cmpl	%ecx, %eax
	je	.L293
	leal	1(%rax), %ecx
	cmpl	$255, %eax
	je	.L294
	movb	%cl, 9(%rdx)
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L274:
	leal	-1(%rax), %ecx
	testl	%eax, %eax
	movl	$2, %eax
	cmovne	%ecx, %eax
	movb	%al, 9(%rdx)
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L288:
	leaq	.LC4(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L293:
	movb	$2, 9(%rdx)
	jmp	.L267
.L294:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17000:
	.size	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE, .-_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	.section	.text._ZN2v88internal4Name6EqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_,"axG",@progbits,_ZN2v88internal4Name6EqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4Name6EqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.type	_ZN2v88internal4Name6EqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_, @function
_ZN2v88internal4Name6EqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_:
.LFB17053:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rsi
	je	.L303
	movq	(%rsi), %rax
	testq	%rdx, %rdx
	je	.L297
	testq	%rsi, %rsi
	je	.L297
	cmpq	%rax, (%rdx)
	je	.L303
.L297:
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$65504, %ecx
	jne	.L301
	movq	(%rdx), %rcx
	movq	-1(%rcx), %rcx
	movzwl	11(%rcx), %ecx
	andl	$65504, %ecx
	je	.L299
.L301:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L299
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L299
	jmp	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	.p2align 4,,10
	.p2align 3
.L303:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE17053:
	.size	_ZN2v88internal4Name6EqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_, .-_ZN2v88internal4Name6EqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_
	.section	.text._ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE,"axG",@progbits,_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	.type	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE, @function
_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE:
.LFB17216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -32
	movq	(%rsi), %rcx
	movq	-1(%rcx), %rdi
	movq	%rcx, %r12
	cmpw	$63, 11(%rdi)
	jbe	.L324
.L310:
	movq	-1(%r12), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L325
.L319:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movq	-1(%r12), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L319
	movq	(%rax), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L320
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	movq	-1(%rcx), %rdi
	movzwl	11(%rdi), %edi
	andl	$7, %edi
	cmpw	$1, %di
	jne	.L310
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$1, %ax
	jne	.L313
	movq	23(%rcx), %rax
	movl	11(%rax), %eax
	testl	%eax, %eax
	je	.L313
	addq	$16, %rsp
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L326
.L322:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movq	41112(%r13), %rdi
	movq	15(%rcx), %r12
	testq	%rdi, %rdi
	je	.L315
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L315:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L327
.L317:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%r12, (%rax)
	jmp	.L310
.L327:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L326:
	movq	%r13, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L322
	.cfi_endproc
.LFE17216:
	.size	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE, .-_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Isolate14native_contextEv,"axG",@progbits,_ZN2v88internal7Isolate14native_contextEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Isolate14native_contextEv
	.type	_ZN2v88internal7Isolate14native_contextEv, @function
_ZN2v88internal7Isolate14native_contextEv:
.LFB18785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L329
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L333
.L331:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L331
	.cfi_endproc
.LFE18785:
	.size	_ZN2v88internal7Isolate14native_contextEv, .-_ZN2v88internal7Isolate14native_contextEv
	.section	.rodata._ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"!object.IsSmi()"
	.section	.text._ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE,"axG",@progbits,_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE
	.type	_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE, @function
_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE:
.LFB20088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	120(%rdi), %rbx
	movq	104(%rdi), %rax
	testq	%rbx, %rbx
	je	.L335
	cmpq	%rax, %rbx
	ja	.L361
.L336:
	movq	%rax, %rcx
	subq	%rbx, %rcx
	movq	%rcx, %rbx
.L335:
	movslq	%r14d, %r13
	leaq	0(%r13,%rax), %rdx
	cmpq	112(%r12), %rdx
	jbe	.L340
	movq	(%r12), %rax
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*184(%rax)
	testb	%al, %al
	jne	.L362
	movslq	72(%r12), %rax
	salq	$32, %rax
	movq	%rax, %r13
	notq	%rax
	testb	$1, %al
	jne	.L345
.L366:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*152(%rax)
	testb	%al, %al
	je	.L363
.L345:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movq	104(%r12), %rax
	leaq	0(%r13,%rax), %rdx
.L340:
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	movq	%rdx, 104(%r12)
	leaq	1(%rax), %r13
	jne	.L364
.L343:
	movq	%r13, %rax
	notq	%rax
	testb	$1, %r13b
	je	.L365
	testb	$1, %al
	jne	.L345
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L361:
	movq	(%rdi), %rdx
	leaq	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv(%rip), %rsi
	movq	128(%rdx), %rcx
	cmpq	%rsi, %rcx
	jne	.L337
	cmpl	$2, 72(%rdi)
	jne	.L336
	call	*152(%rdx)
	testb	%al, %al
	jne	.L360
.L338:
	movq	104(%r12), %rbx
	movq	%rbx, 120(%r12)
	movq	%rbx, %rax
.L339:
	testq	%rbx, %rbx
	jne	.L336
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L364:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L363:
	movq	%r12, %rdi
	leaq	-1(%r13), %rdx
	leal	(%r14,%rbx), %esi
	movl	%r14d, %ecx
	call	_ZN2v88internal5Space14AllocationStepEimi@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*48(%rax)
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L365:
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L337:
	call	*%rcx
	testb	%al, %al
	jne	.L338
.L360:
	movq	120(%r12), %rbx
	movq	104(%r12), %rax
	jmp	.L339
	.cfi_endproc
.LFE20088:
	.size	_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE, .-_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE
	.section	.text._ZN2v88internal7Factory11CodeBuilderC2EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory11CodeBuilderC2EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE
	.type	_ZN2v88internal7Factory11CodeBuilderC2EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE, @function
_ZN2v88internal7Factory11CodeBuilderC2EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE:
.LFB24000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4294967295, %eax
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	%rax, 32(%rbx)
	leaq	976(%rsi), %rax
	movl	%ecx, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	%rax, 40(%rbx)
	movups	%xmm0, (%rbx)
	call	_ZN2v88internal18DeoptimizationData5EmptyEPNS0_7IsolateE@PLT
	movb	$0, 58(%rbx)
	movq	%rax, 48(%rbx)
	movl	$256, %eax
	movw	%ax, 56(%rbx)
	movl	$0, 60(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24000:
	.size	_ZN2v88internal7Factory11CodeBuilderC2EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE, .-_ZN2v88internal7Factory11CodeBuilderC2EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE
	.globl	_ZN2v88internal7Factory11CodeBuilderC1EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE
	.set	_ZN2v88internal7Factory11CodeBuilderC1EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE,_ZN2v88internal7Factory11CodeBuilderC2EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE
	.section	.text._ZN2v88internal7Factory26AllocateRawWithImmortalMapEiNS0_14AllocationTypeENS0_3MapENS0_19AllocationAlignmentE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory26AllocateRawWithImmortalMapEiNS0_14AllocationTypeENS0_3MapENS0_19AllocationAlignmentE
	.type	_ZN2v88internal7Factory26AllocateRawWithImmortalMapEiNS0_14AllocationTypeENS0_3MapENS0_19AllocationAlignmentE, @function
_ZN2v88internal7Factory26AllocateRawWithImmortalMapEiNS0_14AllocationTypeENS0_3MapENS0_19AllocationAlignmentE:
.LFB24009:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$37592, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rcx, %rbx
	movl	$1, %ecx
	subq	$8, %rsp
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rbx, -1(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24009:
	.size	_ZN2v88internal7Factory26AllocateRawWithImmortalMapEiNS0_14AllocationTypeENS0_3MapENS0_19AllocationAlignmentE, .-_ZN2v88internal7Factory26AllocateRawWithImmortalMapEiNS0_14AllocationTypeENS0_3MapENS0_19AllocationAlignmentE
	.section	.text._ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	.type	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE, @function
_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE:
.LFB24010:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	subq	$16, %rsp
	movq	(%rsi), %rax
	movzbl	7(%rax), %esi
	sall	$3, %esi
	testq	%rcx, %rcx
	movl	$1, %ecx
	leal	16(%rsi), %eax
	cmovne	%eax, %esi
	addq	$37592, %rdi
	xorl	%r8d, %r8d
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	testb	%r13b, %r13b
	je	.L373
	movq	(%r12), %rdx
	movq	%rdx, -1(%rax)
	testb	$1, %dl
	je	.L380
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	testb	$4, 10(%rcx)
	jne	.L393
	.p2align 4,,10
	.p2align 3
.L380:
	testq	%rbx, %rbx
	je	.L384
	movq	(%r12), %rdx
	movzbl	7(%rdx), %edx
	movq	(%rbx), %rcx
	movq	3992(%r14), %rsi
	salq	$3, %rdx
	andl	$2040, %edx
	addq	%rax, %rdx
	movq	%rsi, -1(%rdx)
	movq	%rcx, 7(%rdx)
	cmpb	$0, _ZN2v88internal32FLAG_allocation_site_pretenuringE(%rip)
	je	.L384
	addl	$1, 35(%rcx)
.L384:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L373:
	.cfi_restore_state
	movq	(%r12), %rdx
	movq	%rdx, -1(%rax)
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%rax, %rdi
	xorl	%esi, %esi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-40(%rbp), %rax
	jmp	.L380
	.cfi_endproc
.LFE24010:
	.size	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE, .-_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	.section	.text._ZN2v88internal7Factory27InitializeAllocationMementoENS0_17AllocationMementoENS0_14AllocationSiteE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory27InitializeAllocationMementoENS0_17AllocationMementoENS0_14AllocationSiteE
	.type	_ZN2v88internal7Factory27InitializeAllocationMementoENS0_17AllocationMementoENS0_14AllocationSiteE, @function
_ZN2v88internal7Factory27InitializeAllocationMementoENS0_17AllocationMementoENS0_14AllocationSiteE:
.LFB24011:
	.cfi_startproc
	endbr64
	movq	3992(%rdi), %rax
	movq	%rax, -1(%rsi)
	movq	%rdx, 7(%rsi)
	cmpb	$0, _ZN2v88internal32FLAG_allocation_site_pretenuringE(%rip)
	je	.L394
	addl	$1, 35(%rdx)
.L394:
	ret
	.cfi_endproc
.LFE24011:
	.size	_ZN2v88internal7Factory27InitializeAllocationMementoENS0_17AllocationMementoENS0_14AllocationSiteE, .-_ZN2v88internal7Factory27InitializeAllocationMementoENS0_17AllocationMementoENS0_14AllocationSiteE
	.section	.text._ZN2v88internal7Factory16AllocateRawArrayEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory16AllocateRawArrayEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory16AllocateRawArrayEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory16AllocateRawArrayEiNS0_14AllocationTypeE:
.LFB24012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	addq	$37592, %rdi
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %r8
	cmpl	$131072, %ebx
	jle	.L397
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L401
.L397:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L402:
	movq	%rdx, %rsi
	movq	%rdx, %rax
	orq	$256, %rsi
	lock cmpxchgq	%rsi, (%rcx)
	cmpq	%rax, %rdx
	je	.L397
.L398:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L402
	jmp	.L397
	.cfi_endproc
.LFE24012:
	.size	_ZN2v88internal7Factory16AllocateRawArrayEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory16AllocateRawArrayEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory21AllocateRawFixedArrayEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory21AllocateRawFixedArrayEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory21AllocateRawFixedArrayEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory21AllocateRawFixedArrayEiNS0_14AllocationTypeE:
.LFB24013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	37592(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$16, %rsp
	cmpl	$134217726, %esi
	ja	.L411
.L404:
	leal	16(,%rbx,8), %ebx
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %r8
	cmpl	$131072, %ebx
	jle	.L408
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L412
.L408:
	addq	$16, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movl	%edx, -20(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-20(%rbp), %edx
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L412:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%rdx, %rsi
	movq	%rdx, %rax
	orq	$256, %rsi
	lock cmpxchgq	%rsi, (%rcx)
	cmpq	%rax, %rdx
	je	.L408
.L407:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L413
	jmp	.L408
	.cfi_endproc
.LFE24013:
	.size	_ZN2v88internal7Factory21AllocateRawFixedArrayEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory21AllocateRawFixedArrayEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory24AllocateRawWeakArrayListEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory24AllocateRawWeakArrayListEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory24AllocateRawWeakArrayListEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory24AllocateRawWeakArrayListEiNS0_14AllocationTypeE:
.LFB24014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	37592(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	subq	$16, %rsp
	cmpl	$134217725, %esi
	ja	.L422
.L415:
	leal	24(,%rbx,8), %ebx
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %r8
	cmpl	$131072, %ebx
	jle	.L419
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L423
.L419:
	addq	$16, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	movl	%edx, -20(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-20(%rbp), %edx
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L423:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rdx, %rsi
	movq	%rdx, %rax
	orq	$256, %rsi
	lock cmpxchgq	%rsi, (%rcx)
	cmpq	%rax, %rdx
	je	.L419
.L418:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L424
	jmp	.L419
	.cfi_endproc
.LFE24014:
	.size	_ZN2v88internal7Factory24AllocateRawWeakArrayListEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory24AllocateRawWeakArrayListEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE:
.LFB24015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$37592, %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	(%rsi), %rax
	movzbl	7(%rax), %esi
	sall	$3, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	testb	%bl, %bl
	je	.L426
	movq	(%r12), %rdx
	movq	%rdx, -1(%rax)
	testb	$1, %dl
	je	.L433
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	testb	$4, 10(%rcx)
	jne	.L439
.L433:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	movq	(%r12), %rdx
	movq	%rdx, -1(%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	movq	%rax, %rdi
	xorl	%esi, %esi
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-24(%rbp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24015:
	.size	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory15NewFillerObjectEibNS0_14AllocationTypeENS0_16AllocationOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory15NewFillerObjectEibNS0_14AllocationTypeENS0_16AllocationOriginE
	.type	_ZN2v88internal7Factory15NewFillerObjectEibNS0_14AllocationTypeENS0_16AllocationOriginE, @function
_ZN2v88internal7Factory15NewFillerObjectEibNS0_14AllocationTypeENS0_16AllocationOriginE:
.LFB24016:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %eax
	movl	%r8d, %ecx
	movzbl	%dl, %r8d
	movl	%eax, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	37592(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r14, %rdi
	movl	$1, %ecx
	movl	%r12d, %edx
	leaq	-1(%rax), %rsi
	movl	$1, %r8d
	movq	%rax, %r13
	call	_ZN2v88internal4Heap20CreateFillerObjectAtEmiNS0_18ClearRecordedSlotsENS0_20ClearFreedMemoryModeE@PLT
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L441
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L445
.L443:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L443
	.cfi_endproc
.LFE24016:
	.size	_ZN2v88internal7Factory15NewFillerObjectEibNS0_14AllocationTypeENS0_16AllocationOriginE, .-_ZN2v88internal7Factory15NewFillerObjectEibNS0_14AllocationTypeENS0_16AllocationOriginE
	.section	.text._ZN2v88internal7Factory10NewOddballENS0_6HandleINS0_3MapEEEPKcNS2_INS0_6ObjectEEES6_hNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory10NewOddballENS0_6HandleINS0_3MapEEEPKcNS2_INS0_6ObjectEEES6_hNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory10NewOddballENS0_6HandleINS0_3MapEEEPKcNS2_INS0_6ObjectEEES6_hNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory10NewOddballENS0_6HandleINS0_3MapEEEPKcNS2_INS0_6ObjectEEES6_hNS0_14AllocationTypeE:
.LFB24023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movzbl	16(%rbp), %edx
	movl	%r9d, -52(%rbp)
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L447
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L448:
	movzbl	-52(%rbp), %r9d
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Oddball10InitializeEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEES7_h@PLT
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L447:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L451
.L449:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L449
	.cfi_endproc
.LFE24023:
	.size	_ZN2v88internal7Factory10NewOddballENS0_6HandleINS0_3MapEEEPKcNS2_INS0_6ObjectEEES6_hNS0_14AllocationTypeE, .-_ZN2v88internal7Factory10NewOddballENS0_6HandleINS0_3MapEEEPKcNS2_INS0_6ObjectEEES6_hNS0_14AllocationTypeE
	.section	.rodata._ZN2v88internal7Factory22NewSelfReferenceMarkerENS0_14AllocationTypeE.str1.1,"aMS",@progbits,1
.LC7:
	.string	"undefined"
.LC8:
	.string	"self_reference_marker"
	.section	.text._ZN2v88internal7Factory22NewSelfReferenceMarkerENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory22NewSelfReferenceMarkerENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory22NewSelfReferenceMarkerENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory22NewSelfReferenceMarkerENS0_14AllocationTypeE:
.LFB24024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L453
	movabsq	$-4294967296, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L454:
	leaq	952(%r12), %rsi
	movq	%r12, %rdi
	movl	%r13d, %edx
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L456
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L457:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$10, %r9d
	leaq	.LC7(%rip), %r8
	leaq	.LC8(%rip), %rdx
	call	_ZN2v88internal7Oddball10InitializeEPNS0_7IsolateENS0_6HandleIS1_EEPKcNS4_INS0_6ObjectEEES7_h@PLT
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore_state
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L460
.L455:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movabsq	$-4294967296, %rax
	movq	%rax, (%r14)
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L456:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L461
.L458:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L461:
	movq	%r12, %rdi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L458
	.cfi_endproc
.LFE24024:
	.size	_ZN2v88internal7Factory22NewSelfReferenceMarkerENS0_14AllocationTypeE, .-_ZN2v88internal7Factory22NewSelfReferenceMarkerENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory16NewPropertyArrayEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory16NewPropertyArrayEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory16NewPropertyArrayEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory16NewPropertyArrayEiNS0_14AllocationTypeE:
.LFB24025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	968(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	testl	%esi, %esi
	jne	.L476
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	movl	%esi, %ebx
	leaq	37592(%rdi), %r14
	cmpl	$134217726, %esi
	ja	.L477
.L465:
	leal	16(,%rbx,8), %r13d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r14, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r13d
	jle	.L469
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	je	.L469
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L478:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L469
.L470:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L478
	.p2align 4,,10
	.p2align 3
.L469:
	movq	584(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L479
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L471:
	movslq	%ebx, %rcx
	salq	$32, %rbx
	movq	%rbx, 7(%rsi)
	movq	(%rdx), %rbx
	movq	88(%r12), %rax
	leaq	15(%rbx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$16, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L480
.L472:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L477:
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	movl	%edx, -40(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-40(%rbp), %edx
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L472
	.cfi_endproc
.LFE24025:
	.size	_ZN2v88internal7Factory16NewPropertyArrayEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory16NewPropertyArrayEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE:
.LFB24026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movzwl	%si, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	$37592, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	movl	%r8d, %edx
	subq	$24, %rsp
	cmpl	$134217726, %ebx
	ja	.L493
.L482:
	leal	16(,%rbx,8), %r15d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	%r15d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r15d
	jle	.L486
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L494
.L486:
	movq	56(%r12,%r14,8), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L495
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L488:
	movslq	%ebx, %rcx
	salq	$32, %rbx
	movq	%rbx, 7(%rsi)
	movq	(%r8), %rax
	leaq	15(%rax), %rdi
	movq	%r13, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$24, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L496
.L489:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L493:
	leaq	.LC0(%rip), %rsi
	movq	%rdi, -56(%rbp)
	movl	%r8d, -60(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rdi
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L494:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L497:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L486
.L487:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L497
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L496:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L489
	.cfi_endproc
.LFE24026:
	.size	_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE, .-_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory20NewFixedArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EENS0_9RootIndexEiNS0_14AllocationTypeE,"axG",@progbits,_ZN2v88internal7Factory20NewFixedArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EENS0_9RootIndexEiNS0_14AllocationTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory20NewFixedArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EENS0_9RootIndexEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory20NewFixedArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EENS0_9RootIndexEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory20NewFixedArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EENS0_9RootIndexEiNS0_14AllocationTypeE:
.LFB27124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r8d
	movq	88(%rdi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE27124:
	.size	_ZN2v88internal7Factory20NewFixedArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EENS0_9RootIndexEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory20NewFixedArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EENS0_9RootIndexEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE:
.LFB24029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	288(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testl	%esi, %esi
	jne	.L514
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L514:
	.cfi_restore_state
	movq	88(%rdi), %r13
	movl	%esi, %ebx
	leaq	37592(%rdi), %r15
	cmpl	$134217726, %esi
	ja	.L515
.L503:
	leal	16(,%rbx,8), %r14d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r15, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r14d
	jle	.L507
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	je	.L507
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L516:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L507
.L508:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L516
	.p2align 4,,10
	.p2align 3
.L507:
	movq	152(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L517
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L509:
	movslq	%ebx, %rcx
	salq	$32, %rbx
	movq	%rbx, 7(%rsi)
	movq	(%rdx), %rax
	leaq	15(%rax), %rdi
	movq	%r13, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$24, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L518
.L510:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L515:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-56(%rbp), %edx
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L518:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L510
	.cfi_endproc
.LFE24029:
	.size	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory17NewWeakFixedArrayEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewWeakFixedArrayEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory17NewWeakFixedArrayEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory17NewWeakFixedArrayEiNS0_14AllocationTypeE:
.LFB24030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	1072(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	testl	%esi, %esi
	jne	.L532
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore_state
	leal	16(,%rsi,8), %r13d
	movl	%esi, %ebx
	leaq	37592(%rdi), %rdi
	xorl	%r8d, %r8d
	movl	%r13d, %esi
	movl	$1, %ecx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r13d
	jle	.L525
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	je	.L525
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L533:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L525
.L526:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L533
	.p2align 4,,10
	.p2align 3
.L525:
	movq	696(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L534
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L527:
	movslq	%ebx, %rcx
	salq	$32, %rbx
	movq	%rbx, 7(%rsi)
	movq	(%rdx), %rbx
	movq	88(%r12), %rax
	leaq	15(%rbx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$24, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L535
.L528:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L535:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L528
	.cfi_endproc
.LFE24030:
	.size	_ZN2v88internal7Factory17NewWeakFixedArrayEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory17NewWeakFixedArrayEiNS0_14AllocationTypeE
	.section	.rodata._ZN2v88internal7Factory16TryNewFixedArrayEiNS0_14AllocationTypeE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"unreachable code"
	.section	.text._ZN2v88internal7Factory16TryNewFixedArrayEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory16TryNewFixedArrayEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory16TryNewFixedArrayEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory16TryNewFixedArrayEiNS0_14AllocationTypeE:
.LFB24031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	288(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	testl	%esi, %esi
	jne	.L619
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L619:
	.cfi_restore_state
	leaq	37592(%rdi), %rax
	movl	%esi, %r12d
	leal	16(,%rsi,8), %r13d
	movq	%rax, -72(%rbp)
	testb	%dl, %dl
	je	.L620
	cmpb	$1, %dl
	je	.L621
	cmpb	$2, %dl
	je	.L622
	cmpb	$3, %dl
	je	.L623
	cmpb	$4, %dl
	jne	.L563
	movq	37896(%rdi), %r14
	movq	120(%r14), %rcx
	movq	104(%r14), %rax
	testq	%rcx, %rcx
	je	.L564
	cmpq	%rax, %rcx
	jbe	.L565
	movq	(%r14), %rdx
	leaq	_ZN2v88internal10PagedSpace24SupportsInlineAllocationEv(%rip), %rdi
	movq	128(%rdx), %rsi
	cmpq	%rdi, %rsi
	jne	.L566
	cmpl	$2, 72(%r14)
	je	.L624
.L565:
	movq	%rax, %rsi
	subq	%rcx, %rsi
	movq	%rsi, %rcx
.L564:
	movslq	%r13d, %r15
	leaq	(%r15,%rax), %rdx
	cmpq	112(%r14), %rdx
	jbe	.L569
	movq	(%r14), %rax
	movq	%rcx, -56(%rbp)
	movl	$1, %edx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	*184(%rax)
	movq	-56(%rbp), %rcx
	testb	%al, %al
	je	.L570
	movq	104(%r14), %rax
	leaq	(%r15,%rax), %rdx
.L569:
	addq	$1, %rax
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	movq	%rdx, 104(%r14)
	movq	%rax, -56(%rbp)
	jne	.L625
.L572:
	movq	-56(%rbp), %rsi
	movq	%rsi, %rax
	andl	$1, %esi
	notq	%rax
	je	.L555
.L571:
	movq	%rcx, -64(%rbp)
	testb	$1, %al
	jne	.L545
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*152(%rax)
	movq	-56(%rbp), %rcx
	testb	%al, %al
	leaq	-1(%rcx), %r15
	movq	-64(%rbp), %rcx
	jne	.L574
	leal	0(%r13,%rcx), %esi
	movq	%r14, %rdi
	movl	%r13d, %ecx
	movq	%r15, %rdx
	call	_ZN2v88internal5Space14AllocationStepEimi@PLT
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*48(%rax)
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L620:
	cmpl	$131072, %r13d
	jle	.L540
	cmpb	$0, _ZN2v88internal35FLAG_young_generation_large_objectsE(%rip)
	jne	.L626
.L617:
	movq	37872(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal16LargeObjectSpace11AllocateRawEi@PLT
	movq	%rax, -56(%rbp)
	notq	%rax
.L542:
	testb	$1, %al
	jne	.L545
	movq	-56(%rbp), %rax
	leaq	-1(%rax), %r15
.L574:
	movq	40776(%rbx), %rax
	movq	40768(%rbx), %r14
	movq	%rax, -64(%rbp)
	cmpq	%rax, %r14
	je	.L579
	.p2align 4,,10
	.p2align 3
.L578:
	movq	(%r14), %rdi
	movl	%r13d, %edx
	movq	%r15, %rsi
	addq	$8, %r14
	movq	(%rdi), %rcx
	call	*(%rcx)
	cmpq	%r14, -64(%rbp)
	jne	.L578
.L579:
	cmpb	$0, _ZN2v88internal23FLAG_fuzzer_gc_analysisE(%rip)
	je	.L627
	addl	$1, 37992(%rbx)
.L580:
	cmpl	$131072, %r13d
	jle	.L586
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L628
.L586:
	movq	152(%rbx), %rax
	movq	%rax, (%r15)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L629
	movq	-56(%rbp), %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L588:
	movslq	%r12d, %rcx
	salq	$32, %r12
	movq	%r12, 7(%rax)
	movq	-72(%rbp), %rax
	movq	(%rdx), %rbx
	movq	-37504(%rax), %rax
	leaq	15(%rbx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$40, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L627:
	.cfi_restore_state
	movl	_ZN2v88internal36FLAG_trace_allocation_stack_intervalE(%rip), %ecx
	testl	%ecx, %ecx
	jle	.L580
	movl	37992(%rbx), %eax
	xorl	%edx, %edx
	addl	$1, %eax
	movl	%eax, 37992(%rbx)
	divl	%ecx
	testl	%edx, %edx
	jne	.L580
	movq	-72(%rbp), %rax
	movq	stdout(%rip), %rsi
	leaq	-37592(%rax), %rdi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L629:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L630
.L589:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	-56(%rbp), %rax
	movq	%rax, (%rdx)
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L540:
	movq	37840(%rdi), %r14
	movq	104(%r14), %rax
	cmpq	120(%r14), %rax
	jnb	.L543
	movq	%rax, 120(%r14)
.L543:
	movslq	%r13d, %r15
	leaq	(%rax,%r15), %rdx
	cmpq	%rdx, 112(%r14)
	jnb	.L544
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8NewSpace16EnsureAllocationEiNS0_19AllocationAlignmentE@PLT
	testb	%al, %al
	je	.L545
	movq	104(%r14), %rax
	leaq	(%r15,%rax), %rdx
.L544:
	addq	$1, %rax
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	movq	%rdx, 104(%r14)
	movq	%rax, -56(%rbp)
	jne	.L618
.L546:
	movq	-56(%rbp), %rcx
	movq	%rcx, %rax
	andl	$1, %ecx
	notq	%rax
	jne	.L542
.L555:
	leaq	.LC6(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L621:
	cmpl	$131072, %r13d
	jg	.L617
	movq	37848(%rdi), %rdi
	movl	$1, %ecx
	xorl	%edx, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal10PagedSpace11AllocateRawEiNS0_19AllocationAlignmentENS0_16AllocationOriginE
	movq	%rax, -56(%rbp)
	notq	%rax
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L626:
	movq	37888(%rdi), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal19NewLargeObjectSpace11AllocateRawEi@PLT
	movq	%rax, -56(%rbp)
	notq	%rax
	jmp	.L542
	.p2align 4,,10
	.p2align 3
.L623:
	movq	37864(%rdi), %r14
	movslq	%r13d, %r15
	movq	104(%r14), %rdx
	leaq	(%rdx,%r15), %rax
	cmpq	112(%r14), %rax
	jbe	.L559
	movq	(%r14), %rax
	movl	$1, %edx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	*184(%rax)
	testb	%al, %al
	je	.L560
	movq	104(%r14), %rdx
	leaq	(%r15,%rdx), %rax
.L559:
	movq	%rax, 104(%r14)
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	leaq	1(%rdx), %rax
	movq	%rax, -56(%rbp)
	je	.L546
	.p2align 4,,10
	.p2align 3
.L618:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L622:
	movq	37856(%rdi), %r14
	movl	$131072, %edx
	movq	160(%r14), %rax
	cmpl	$131072, %eax
	cmovg	%edx, %eax
	cmpl	%eax, %r13d
	jle	.L631
	movq	37880(%rdi), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal20CodeLargeObjectSpace11AllocateRawEi@PLT
	movq	%rax, -56(%rbp)
	testb	$1, %al
	je	.L545
	movq	%rax, %r15
	leaq	37592(%rbx), %r14
	movq	%rax, %rsi
	movq	%r14, %rdi
	leaq	-1(%r15), %r15
	call	_ZN2v88internal4Heap31UnprotectAndRegisterMemoryChunkENS0_10HeapObjectE@PLT
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap13ZapCodeObjectEmi@PLT
	cmpl	$131072, %r13d
	jg	.L574
.L590:
	movq	-56(%rbp), %rax
	movq	%r15, %rsi
	andq	$-262144, %rax
	movq	272(%rax), %rdi
	call	_ZN2v88internal18CodeObjectRegistry32RegisterNewlyAllocatedCodeObjectEm@PLT
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L628:
	movq	-56(%rbp), %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L632:
	movq	%rdx, %rsi
	movq	%rdx, %rax
	orq	$256, %rsi
	lock cmpxchgq	%rsi, (%rcx)
	cmpq	%rax, %rdx
	je	.L586
.L587:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L632
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L570:
	movslq	72(%r14), %rax
	salq	$32, %rax
	movq	%rax, -56(%rbp)
	notq	%rax
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L630:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L625:
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	movq	-64(%rbp), %rcx
	jmp	.L572
.L631:
	movq	104(%r14), %rdx
	movslq	%r13d, %r15
	leaq	(%rdx,%r15), %rax
	cmpq	112(%r14), %rax
	jbe	.L551
	movq	(%r14), %rax
	movl	$1, %edx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	*184(%rax)
	testb	%al, %al
	je	.L552
	movq	104(%r14), %rdx
	leaq	(%r15,%rdx), %rax
.L551:
	movq	%rax, 104(%r14)
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	leaq	1(%rdx), %rax
	movq	%rax, -56(%rbp)
	jne	.L633
.L554:
	movq	-56(%rbp), %rcx
	movq	%rcx, %rax
	andl	$1, %ecx
	notq	%rax
	je	.L555
.L553:
	testb	$1, %al
	jne	.L545
	movq	-56(%rbp), %r15
	leaq	37592(%rbx), %r14
	movq	%r14, %rdi
	movq	%r15, %rsi
	leaq	-1(%r15), %r15
	call	_ZN2v88internal4Heap31UnprotectAndRegisterMemoryChunkENS0_10HeapObjectE@PLT
	movl	%r13d, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4Heap13ZapCodeObjectEmi@PLT
	jmp	.L590
.L560:
	movslq	72(%r14), %rax
	salq	$32, %rax
	movq	%rax, -56(%rbp)
	notq	%rax
	jmp	.L542
.L624:
	movq	%r14, %rdi
	call	*152(%rdx)
	testb	%al, %al
	je	.L567
.L616:
	movq	120(%r14), %rcx
	movq	104(%r14), %rax
.L568:
	testq	%rcx, %rcx
	jne	.L565
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L566:
	movq	%r14, %rdi
	call	*%rsi
	testb	%al, %al
	je	.L616
.L567:
	movq	104(%r14), %rcx
	movq	%rcx, 120(%r14)
	movq	%rcx, %rax
	jmp	.L568
.L552:
	movslq	72(%r14), %rax
	salq	$32, %rax
	movq	%rax, -56(%rbp)
	notq	%rax
	jmp	.L553
.L633:
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal19SpaceWithLinearArea23UpdateAllocationOriginsENS0_16AllocationOriginE@PLT
	jmp	.L554
.L563:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24031:
	.size	_ZN2v88internal7Factory16TryNewFixedArrayEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory16TryNewFixedArrayEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE:
.LFB24032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	288(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testl	%esi, %esi
	jne	.L648
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L648:
	.cfi_restore_state
	movq	96(%rdi), %r13
	movl	%esi, %ebx
	leaq	37592(%rdi), %r15
	cmpl	$134217726, %esi
	ja	.L649
.L637:
	leal	16(,%rbx,8), %r14d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r15, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r14d
	jle	.L641
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	je	.L641
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L650:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L641
.L642:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L650
	.p2align 4,,10
	.p2align 3
.L641:
	movq	152(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L651
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L643:
	movslq	%ebx, %rcx
	salq	$32, %rbx
	movq	%rbx, 7(%rsi)
	movq	(%rdx), %rax
	leaq	15(%rax), %rdi
	movq	%r13, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$24, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L652
.L644:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L649:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-56(%rbp), %edx
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L652:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L644
	.cfi_endproc
.LFE24032:
	.size	_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory22NewFixedArrayWithHolesEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE:
.LFB24033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	288(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testl	%esi, %esi
	jne	.L667
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L667:
	.cfi_restore_state
	movq	88(%rdi), %r13
	movl	%esi, %ebx
	leaq	37592(%rdi), %r15
	cmpl	$134217726, %esi
	ja	.L668
.L656:
	leal	16(,%rbx,8), %r14d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r15, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r14d
	jle	.L660
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	je	.L660
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L669:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L660
.L661:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L669
	.p2align 4,,10
	.p2align 3
.L660:
	movq	152(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L670
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L662:
	movslq	%ebx, %rcx
	salq	$32, %rbx
	movq	%rbx, 7(%rsi)
	movq	(%rdx), %rax
	leaq	15(%rax), %rdi
	movq	%r13, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$24, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L670:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L671
.L663:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L668:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-56(%rbp), %edx
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L671:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L663
	.cfi_endproc
.LFE24033:
	.size	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory27NewClosureFeedbackCellArrayEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory27NewClosureFeedbackCellArrayEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory27NewClosureFeedbackCellArrayEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory27NewClosureFeedbackCellArrayEiNS0_14AllocationTypeE:
.LFB24034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	1000(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testl	%esi, %esi
	jne	.L686
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L686:
	.cfi_restore_state
	movq	88(%rdi), %r13
	movl	%esi, %ebx
	leaq	37592(%rdi), %r15
	cmpl	$134217726, %esi
	ja	.L687
.L675:
	leal	16(,%rbx,8), %r14d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r15, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r14d
	jle	.L679
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	je	.L679
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L688:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L679
.L680:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L688
	.p2align 4,,10
	.p2align 3
.L679:
	movq	424(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L689
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L681:
	movslq	%ebx, %rcx
	salq	$32, %rbx
	movq	%rbx, 7(%rsi)
	movq	(%rdx), %rax
	leaq	15(%rax), %rdi
	movq	%r13, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$24, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L689:
	.cfi_restore_state
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L690
.L682:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L687:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-56(%rbp), %edx
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L690:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L682
	.cfi_endproc
.LFE24034:
	.size	_ZN2v88internal7Factory27NewClosureFeedbackCellArrayEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory27NewClosureFeedbackCellArrayEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory17NewFeedbackVectorENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_24ClosureFeedbackCellArrayEEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewFeedbackVectorENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_24ClosureFeedbackCellArrayEEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory17NewFeedbackVectorENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_24ClosureFeedbackCellArrayEEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory17NewFeedbackVectorENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_24ClosureFeedbackCellArrayEEENS0_14AllocationTypeE:
.LFB24035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	addq	$37592, %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	%rdx, -56(%rbp)
	movl	%ecx, %edx
	movq	-37320(%rdi), %r12
	movl	$1, %ecx
	movq	23(%rax), %rax
	movl	7(%rax), %ebx
	leal	48(,%rbx,8), %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %r13
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L692
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %r12
.L693:
	movq	(%r15), %rdx
	leaq	7(%r13), %rsi
	movq	%r13, %rdi
	movq	%rdx, 7(%r13)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-64(%rbp), %rdx
	testb	$1, %dl
	jne	.L706
.L695:
	movzbl	_ZN2v88internal24FLAG_log_function_eventsE(%rip), %eax
	movq	(%r12), %rdx
	xorl	$1, %eax
	movzbl	%al, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	(%r12), %rax
	movl	%ebx, 31(%rax)
	movq	(%r12), %rax
	movl	$0, 35(%rax)
	movq	(%r12), %rax
	movl	$0, 39(%rax)
	movq	(%r12), %rax
	movl	$0, 43(%rax)
	movq	-56(%rbp), %rax
	movq	(%r12), %rdi
	movq	(%rax), %r13
	leaq	23(%rdi), %r15
	movq	%r13, 23(%rdi)
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L707
.L697:
	movslq	%ebx, %rcx
	movq	(%r12), %rbx
	movq	88(%r14), %rax
	leaq	47(%rbx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L706:
	.cfi_restore_state
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L695
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L695
	movq	-72(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L707:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L697
	movq	-56(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L697
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L692:
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L708
.L694:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%r13, (%r12)
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L708:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L694
	.cfi_endproc
.LFE24035:
	.size	_ZN2v88internal7Factory17NewFeedbackVectorENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_24ClosureFeedbackCellArrayEEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory17NewFeedbackVectorENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_24ClosureFeedbackCellArrayEEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory20NewEmbedderDataArrayEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory20NewEmbedderDataArrayEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory20NewEmbedderDataArrayEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory20NewEmbedderDataArrayEiNS0_14AllocationTypeE:
.LFB24036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	leal	16(,%rsi,8), %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$24, %rsp
	movq	-36872(%rdi), %r13
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r13, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L710
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L711:
	movq	%r12, %rax
	salq	$32, %rax
	movq	%rax, 7(%rsi)
	testl	%r12d, %r12d
	jle	.L713
	movq	(%r8), %rdi
	movq	88(%rbx), %rax
	movslq	11(%rdi), %rdx
	addq	$15, %rdi
	leal	16(,%rdx,8), %ecx
	movslq	%ecx, %rcx
	subq	$16, %rcx
	shrq	$3, %rcx
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
.L713:
	addq	$24, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L710:
	.cfi_restore_state
	movq	41088(%rbx), %r8
	cmpq	41096(%rbx), %r8
	je	.L715
.L712:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r8)
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L715:
	movq	%rbx, %rdi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L712
	.cfi_endproc
.LFE24036:
	.size	_ZN2v88internal7Factory20NewEmbedderDataArrayEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory20NewEmbedderDataArrayEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory31NewObjectBoilerplateDescriptionEiiib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory31NewObjectBoilerplateDescriptionEiiib
	.type	_ZN2v88internal7Factory31NewObjectBoilerplateDescriptionEiiib, @function
_ZN2v88internal7Factory31NewObjectBoilerplateDescriptionEiiib:
.LFB24037:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	%ecx, %edx
	movzbl	%r8b, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edx, %ebx
	subl	%r8d, %ebx
	subq	$8, %rsp
	cmpl	%esi, %ebx
	je	.L717
	leal	2(%rsi,%rsi), %edx
	movl	$1, %ecx
	movl	$50, %esi
	salq	$32, %rbx
	call	_ZN2v88internal7Factory20NewFixedArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EENS0_9RootIndexEiNS0_14AllocationTypeE
	movq	(%rax), %rsi
	movslq	11(%rsi), %rdx
	leal	8(,%rdx,8), %ecx
	movslq	%ecx, %rcx
	movq	%rbx, -1(%rsi,%rcx)
.L718:
	movq	(%rax), %rdx
	movq	$0, 15(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore_state
	leal	1(%rbx,%rbx), %edx
	movl	$1, %ecx
	movl	$50, %esi
	call	_ZN2v88internal7Factory20NewFixedArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EENS0_9RootIndexEiNS0_14AllocationTypeE
	jmp	.L718
	.cfi_endproc
.LFE24037:
	.size	_ZN2v88internal7Factory31NewObjectBoilerplateDescriptionEiiib, .-_ZN2v88internal7Factory31NewObjectBoilerplateDescriptionEiiib
	.section	.text._ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE:
.LFB24038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	288(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	testl	%esi, %esi
	je	.L723
	movl	%esi, %ebx
	leaq	37592(%rdi), %r14
	cmpl	$134217726, %esi
	ja	.L729
.L724:
	movq	488(%r12), %r13
	leal	16(,%rbx,8), %esi
	movq	%r14, %rdi
	movl	$1, %r8d
	movl	$1, %ecx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r13, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L725
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L726:
	salq	$32, %rbx
	movq	%rbx, 7(%rsi)
.L723:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L725:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L730
.L727:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L729:
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	movl	%edx, -40(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-40(%rbp), %edx
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L730:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L727
	.cfi_endproc
.LFE24038:
	.size	_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory28NewFixedDoubleArrayWithHolesEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory28NewFixedDoubleArrayWithHolesEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory28NewFixedDoubleArrayWithHolesEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory28NewFixedDoubleArrayWithHolesEiNS0_14AllocationTypeE:
.LFB24039:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L732
	leaq	288(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L732:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%dl, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE.part.0
	testl	%ebx, %ebx
	jle	.L733
	movq	(%rax), %rsi
	cmpl	$1, %ebx
	je	.L737
	movl	%ebx, %ecx
	leaq	15(%rsi), %rdx
	movdqa	.LC10(%rip), %xmm0
	shrl	%ecx
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L735:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L735
	movl	%ebx, %edx
	andl	$-2, %edx
	andl	$1, %ebx
	je	.L733
.L734:
	leal	16(,%rdx,8), %edx
	movabsq	$-2251799814209537, %rbx
	movslq	%edx, %rdx
	movq	%rbx, -1(%rsi,%rdx)
.L733:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L737:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L734
	.cfi_endproc
.LFE24039:
	.size	_ZN2v88internal7Factory28NewFixedDoubleArrayWithHolesEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory28NewFixedDoubleArrayWithHolesEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory19NewFeedbackMetadataEiiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory19NewFeedbackMetadataEiiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory19NewFeedbackMetadataEiiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory19NewFeedbackMetadataEiiNS0_14AllocationTypeE:
.LFB24040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	movl	%ecx, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testl	%esi, %esi
	je	.L747
	leal	-1(%rsi), %ecx
	movslq	%ecx, %rax
	sarl	$31, %ecx
	imulq	$715827883, %rax, %rax
	shrq	$32, %rax
	subl	%ecx, %eax
	leal	27(,%rax,4), %esi
	andl	$-8, %esi
	leal	-16(%rsi), %r14d
	movslq	%r14d, %r14
.L743:
	movq	432(%rbx), %r12
	leaq	37592(%rbx), %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L744
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L745:
	movl	%r15d, 7(%rsi)
	movq	(%r12), %rax
	movq	%r14, %rdx
	xorl	%esi, %esi
	movl	%r13d, 11(%rax)
	movq	(%r12), %rax
	leaq	15(%rax), %rdi
	call	memset@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L744:
	.cfi_restore_state
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L749
.L746:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L747:
	movl	$16, %esi
	xorl	%r14d, %r14d
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L749:
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L746
	.cfi_endproc
.LFE24040:
	.size	_ZN2v88internal7Factory19NewFeedbackMetadataEiiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory19NewFeedbackMetadataEiiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory13NewFrameArrayEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory13NewFrameArrayEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory13NewFrameArrayEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory13NewFrameArrayEiNS0_14AllocationTypeE:
.LFB24041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leal	(%rsi,%rsi,2), %r12d
	pushq	%rbx
	addl	%r12d, %r12d
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	leal	1(%r12), %r14d
	subq	$24, %rsp
	movq	-37496(%rdi), %r13
	cmpl	$134217726, %r14d
	ja	.L772
.L751:
	leal	24(,%r12,8), %r12d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	%r12d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r12d
	jle	.L755
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L773
.L755:
	movq	152(%rbx), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L774
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L757:
	movslq	%r14d, %rcx
	salq	$32, %r14
	movq	%r14, 7(%rsi)
	movq	(%r12), %rax
	leaq	15(%rax), %rdi
	movq	%r13, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	movq	(%r12), %r14
	movq	%r13, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %r13b
	je	.L762
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L775
	testb	$24, %al
	je	.L762
.L777:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L776
.L762:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L775:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L777
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L774:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L778
.L758:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L772:
	leaq	.LC0(%rip), %rsi
	movl	%edx, -60(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rdi
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L773:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L779:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L755
.L756:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L779
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L776:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L778:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L758
	.cfi_endproc
.LFE24041:
	.size	_ZN2v88internal7Factory13NewFrameArrayEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory13NewFrameArrayEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory22NewSmallOrderedHashSetEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory22NewSmallOrderedHashSetEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory22NewSmallOrderedHashSetEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory22NewSmallOrderedHashSetEiNS0_14AllocationTypeE:
.LFB24043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$4, %edi
	pushq	%r12
	.cfi_offset 12, -40
	movl	$254, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$4, %esi
	cmovge	%esi, %edi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej@PLT
	movq	640(%r13), %r14
	leaq	37592(%r13), %rdi
	movl	%ebx, %edx
	cmpl	$254, %eax
	movl	$1, %ecx
	cmovle	%eax, %r12d
	xorl	%r8d, %r8d
	movl	%r12d, %eax
	shrl	$31, %eax
	addl	%r12d, %eax
	sarl	%eax
	leal	16(%rax,%r12,8), %eax
	leal	7(%r12,%rax), %esi
	andl	$-8, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r14, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L781
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L782:
	movq	%rsi, -48(%rbp)
	leaq	-48(%rbp), %rdi
	movl	%r12d, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE10InitializeEPNS0_7IsolateEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L786
	addq	$32, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L781:
	.cfi_restore_state
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L787
.L783:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L782
	.p2align 4,,10
	.p2align 3
.L787:
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L783
.L786:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24043:
	.size	_ZN2v88internal7Factory22NewSmallOrderedHashSetEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory22NewSmallOrderedHashSetEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory22NewSmallOrderedHashMapEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory22NewSmallOrderedHashMapEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory22NewSmallOrderedHashMapEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory22NewSmallOrderedHashMapEiNS0_14AllocationTypeE:
.LFB24044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$4, %edi
	pushq	%r12
	.cfi_offset 12, -40
	movl	$254, %r12d
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$4, %esi
	cmovge	%esi, %edi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej@PLT
	movq	632(%r13), %rbx
	leaq	37592(%r13), %rdi
	movl	%r14d, %edx
	cmpl	$254, %eax
	cmovle	%eax, %r12d
	xorl	%r8d, %r8d
	leal	1(%r12), %eax
	sall	$4, %eax
	movl	%eax, %ecx
	movl	%r12d, %eax
	shrl	$31, %eax
	addl	%r12d, %eax
	sarl	%eax
	addl	%ecx, %eax
	movl	$1, %ecx
	leal	7(%r12,%rax), %esi
	andl	$-8, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rbx, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L789
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L790:
	movq	%rsi, -48(%rbp)
	leaq	-48(%rbp), %rdi
	movl	%r12d, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE10InitializeEPNS0_7IsolateEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L794
	addq	$32, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L789:
	.cfi_restore_state
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L795
.L791:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L795:
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L791
.L794:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24044:
	.size	_ZN2v88internal7Factory22NewSmallOrderedHashMapEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory22NewSmallOrderedHashMapEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory29NewSmallOrderedNameDictionaryEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory29NewSmallOrderedNameDictionaryEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory29NewSmallOrderedNameDictionaryEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory29NewSmallOrderedNameDictionaryEiNS0_14AllocationTypeE:
.LFB24045:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$4, %edi
	pushq	%r12
	.cfi_offset 12, -40
	movl	$254, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$4, %esi
	cmovge	%esi, %edi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej@PLT
	movq	648(%r13), %r14
	movl	$1, %ecx
	leaq	37592(%r13), %rdi
	cmpl	$254, %eax
	cmovle	%eax, %r12d
	xorl	%r8d, %r8d
	movl	%r12d, %eax
	leal	3(%r12,%r12,2), %edx
	shrl	$31, %eax
	addl	%r12d, %eax
	sarl	%eax
	leal	(%rax,%rdx,8), %eax
	movl	%ebx, %edx
	leal	7(%r12,%rax), %esi
	andl	$-8, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r14, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L797
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L798:
	movq	%rsi, -48(%rbp)
	leaq	-48(%rbp), %rdi
	movl	%r12d, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE10InitializeEPNS0_7IsolateEi@PLT
	movq	(%r14), %rax
	movl	$0, 7(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L802
	addq	$32, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L797:
	.cfi_restore_state
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L803
.L799:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L803:
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L799
.L802:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24045:
	.size	_ZN2v88internal7Factory29NewSmallOrderedNameDictionaryEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory29NewSmallOrderedNameDictionaryEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory17NewOrderedHashSetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewOrderedHashSetEv
	.type	_ZN2v88internal7Factory17NewOrderedHashSetEv, @function
_ZN2v88internal7Factory17NewOrderedHashSetEv:
.LFB24046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal14OrderedHashSet8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L807
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L807:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24046:
	.size	_ZN2v88internal7Factory17NewOrderedHashSetEv, .-_ZN2v88internal7Factory17NewOrderedHashSetEv
	.section	.text._ZN2v88internal7Factory17NewOrderedHashMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewOrderedHashMapEv
	.type	_ZN2v88internal7Factory17NewOrderedHashMapEv, @function
_ZN2v88internal7Factory17NewOrderedHashMapEv:
.LFB24047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal14OrderedHashMap8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L811
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L811:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24047:
	.size	_ZN2v88internal7Factory17NewOrderedHashMapEv, .-_ZN2v88internal7Factory17NewOrderedHashMapEv
	.section	.text._ZN2v88internal7Factory24NewOrderedNameDictionaryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory24NewOrderedNameDictionaryEv
	.type	_ZN2v88internal7Factory24NewOrderedNameDictionaryEv, @function
_ZN2v88internal7Factory24NewOrderedNameDictionaryEv:
.LFB24048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal21OrderedNameDictionary8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	je	.L815
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L815:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24048:
	.size	_ZN2v88internal7Factory24NewOrderedNameDictionaryEv, .-_ZN2v88internal7Factory24NewOrderedNameDictionaryEv
	.section	.text._ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb,"axG",@progbits,_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb
	.type	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb, @function
_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb:
.LFB27174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	8(%rsi), %rsi
	movq	(%rbx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	1176(%rdi), %rax
	leal	-1(%rsi), %edi
	movl	%esi, %r8d
	movq	15(%rax), %rax
	cmpl	$9, %edi
	ja	.L817
	movzbl	(%rcx), %edi
	leal	-48(%rdi), %r10d
	cmpl	$9, %r10d
	jbe	.L834
.L818:
	movslq	%esi, %rsi
	addq	%rcx, %rsi
	cmpq	%rsi, %rcx
	je	.L824
	.p2align 4,,10
	.p2align 3
.L825:
	movzbl	(%rcx), %edi
	addq	$1, %rcx
	addl	%eax, %edi
	movl	%edi, %eax
	sall	$10, %eax
	addl	%edi, %eax
	movl	%eax, %edi
	shrl	$6, %edi
	xorl	%edi, %eax
	cmpq	%rcx, %rsi
	jne	.L825
.L824:
	leal	(%rax,%rax,8), %esi
	movl	%esi, %eax
	shrl	$11, %eax
	xorl	%eax, %esi
	movl	%esi, %eax
	sall	$15, %eax
	addl	%esi, %eax
	movl	%eax, %ecx
	andl	$1073741823, %ecx
	leal	-1(%rcx), %esi
	sarl	$31, %esi
	andl	$27, %esi
	orl	%eax, %esi
.L833:
	leal	2(,%rsi,4), %esi
.L821:
	movdqu	(%rbx), %xmm0
	movl	%esi, -88(%rbp)
	leaq	16+_ZTVN2v88internal19SequentialStringKeyIhEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-96(%rbp), %rsi
	movl	%r8d, -84(%rbp)
	movq	%rax, -96(%rbp)
	movb	%dl, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal11StringTable9LookupKeyINS0_19SequentialStringKeyIhEEEENS0_6HandleINS0_6StringEEEPNS0_7IsolateEPT_@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L835
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L817:
	.cfi_restore_state
	cmpl	$16383, %esi
	jle	.L818
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L834:
	cmpb	$48, %dil
	jne	.L827
	cmpl	$1, %esi
	jne	.L818
.L827:
	subl	$48, %edi
	cmpl	$1, %esi
	je	.L822
	leal	-2(%rsi), %r9d
	leaq	1(%rcx), %r10
	movl	$429496729, %r13d
	leaq	2(%rcx,%r9), %r14
.L823:
	movzbl	(%r10), %r9d
	leal	-48(%r9), %r11d
	cmpl	$9, %r11d
	ja	.L818
	subl	$45, %r9d
	movl	%r13d, %r15d
	sarl	$3, %r9d
	subl	%r9d, %r15d
	cmpl	%r15d, %edi
	ja	.L818
	leal	(%rdi,%rdi,4), %edi
	addq	$1, %r10
	leal	(%r11,%rdi,2), %edi
	cmpq	%r10, %r14
	jne	.L823
.L822:
	movl	%edx, -100(%rbp)
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movl	8(%rbx), %r8d
	movl	-100(%rbp), %edx
	movl	%eax, %esi
	jmp	.L821
.L835:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27174:
	.size	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb, .-_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb
	.section	.text._ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb,"axG",@progbits,_ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb
	.type	_ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb, @function
_ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb:
.LFB27201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	8(%rsi), %rsi
	movq	(%rbx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	1176(%rdi), %rax
	leal	-1(%rsi), %edi
	movl	%esi, %r8d
	movq	15(%rax), %rax
	cmpl	$9, %edi
	ja	.L837
	movzwl	(%rcx), %edi
	leal	-48(%rdi), %r10d
	cmpl	$9, %r10d
	jbe	.L854
.L838:
	movslq	%esi, %rsi
	leaq	(%rcx,%rsi,2), %rdi
	cmpq	%rdi, %rcx
	je	.L844
	.p2align 4,,10
	.p2align 3
.L845:
	movzwl	(%rcx), %esi
	addq	$2, %rcx
	addl	%eax, %esi
	movl	%esi, %eax
	sall	$10, %eax
	addl	%esi, %eax
	movl	%eax, %esi
	shrl	$6, %esi
	xorl	%esi, %eax
	cmpq	%rcx, %rdi
	jne	.L845
.L844:
	leal	(%rax,%rax,8), %esi
	movl	%esi, %eax
	shrl	$11, %eax
	xorl	%eax, %esi
	movl	%esi, %eax
	sall	$15, %eax
	addl	%esi, %eax
	movl	%eax, %ecx
	andl	$1073741823, %ecx
	leal	-1(%rcx), %esi
	sarl	$31, %esi
	andl	$27, %esi
	orl	%eax, %esi
.L853:
	leal	2(,%rsi,4), %esi
.L841:
	movdqu	(%rbx), %xmm0
	movl	%esi, -88(%rbp)
	leaq	16+_ZTVN2v88internal19SequentialStringKeyItEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-96(%rbp), %rsi
	movl	%r8d, -84(%rbp)
	movq	%rax, -96(%rbp)
	movb	%dl, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal11StringTable9LookupKeyINS0_19SequentialStringKeyItEEEENS0_6HandleINS0_6StringEEEPNS0_7IsolateEPT_@PLT
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L855
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L837:
	.cfi_restore_state
	cmpl	$16383, %esi
	jle	.L838
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L854:
	cmpw	$48, %di
	jne	.L847
	cmpl	$1, %esi
	jne	.L838
.L847:
	subl	$48, %edi
	cmpl	$1, %esi
	je	.L842
	leal	-2(%rsi), %r9d
	leaq	2(%rcx), %r10
	movl	$429496729, %r13d
	leaq	4(%rcx,%r9,2), %r14
.L843:
	movzwl	(%r10), %r9d
	leal	-48(%r9), %r11d
	cmpl	$9, %r11d
	ja	.L838
	subl	$45, %r9d
	movl	%r13d, %r15d
	sarl	$3, %r9d
	subl	%r9d, %r15d
	cmpl	%r15d, %edi
	ja	.L838
	leal	(%rdi,%rdi,4), %edi
	addq	$2, %r10
	leal	(%r11,%rdi,2), %edi
	cmpq	%r10, %r14
	jne	.L843
.L842:
	movl	%edx, -100(%rbp)
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movl	8(%rbx), %r8d
	movl	-100(%rbp), %edx
	movl	%eax, %esi
	jmp	.L841
.L855:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27201:
	.size	_ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb, .-_ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb
	.section	.text._ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE
	.type	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE, @function
_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE:
.LFB24050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-92(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	subq	$64, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	8(%rsi), %rax
	movq	%r13, %rsi
	movq	%rdx, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11Utf8DecoderC1ERKNS0_6VectorIKhEE@PLT
	movzbl	-92(%rbp), %eax
	testb	%al, %al
	je	.L864
	movslq	-84(%rbp), %r8
	cmpb	$1, %al
	jbe	.L865
	movabsq	$4611686018427387900, %rax
	leaq	(%r8,%r8), %rdi
	cmpq	%rax, %r8
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal11Utf8Decoder6DecodeItEEvPT_RKNS0_6VectorIKhEE@PLT
	movslq	-84(%rbp), %rax
	movq	%r12, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZdaPv@PLT
.L858:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L866
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L864:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb
	movq	%rax, %r12
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L865:
	movq	%r8, %rdi
	call	_Znam@PLT
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v88internal11Utf8Decoder6DecodeIhEEvPT_RKNS0_6VectorIKhEE@PLT
	movslq	-84(%rbp), %rax
	movq	%r12, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZdaPv@PLT
	jmp	.L858
.L866:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24050:
	.size	_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE, .-_ZN2v88internal7Factory21InternalizeUtf8StringERKNS0_6VectorIKcEE
	.section	.text._ZN2v88internal7Factory17InternalizeStringINS0_16SeqOneByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib,"axG",@progbits,_ZN2v88internal7Factory17InternalizeStringINS0_16SeqOneByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory17InternalizeStringINS0_16SeqOneByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib
	.type	_ZN2v88internal7Factory17InternalizeStringINS0_16SeqOneByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib, @function
_ZN2v88internal7Factory17InternalizeStringINS0_16SeqOneByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib:
.LFB27206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEEE(%rip), %rax
	movl	%edx, -40(%rbp)
	movslq	%edx, %rdx
	movq	%rax, -64(%rbp)
	movq	(%rsi), %rax
	movq	%rsi, -48(%rbp)
	leaq	15(%rax,%rdx), %rsi
	leal	-1(%rcx), %eax
	movl	$0, -56(%rbp)
	movl	%ecx, -52(%rbp)
	movb	%r8b, -36(%rbp)
	cmpl	$9, %eax
	ja	.L868
	movzbl	(%rsi), %edi
	leal	-48(%rdi), %edx
	cmpl	$9, %edx
	jbe	.L885
.L869:
	movq	1176(%r12), %rax
	movslq	%ecx, %rcx
	addq	%rsi, %rcx
	movl	15(%rax), %eax
	cmpq	%rcx, %rsi
	je	.L875
	.p2align 4,,10
	.p2align 3
.L876:
	movzbl	(%rsi), %edx
	addq	$1, %rsi
	addl	%eax, %edx
	movl	%edx, %eax
	sall	$10, %eax
	addl	%edx, %eax
	movl	%eax, %edx
	shrl	$6, %edx
	xorl	%edx, %eax
	cmpq	%rsi, %rcx
	jne	.L876
.L875:
	leal	(%rax,%rax,8), %ecx
	movl	%ecx, %eax
	shrl	$11, %eax
	xorl	%eax, %ecx
	movl	%ecx, %eax
	sall	$15, %eax
	addl	%ecx, %eax
	movl	%eax, %edx
	andl	$1073741823, %edx
	leal	-1(%rdx), %ecx
	sarl	$31, %ecx
	andl	$27, %ecx
	orl	%eax, %ecx
.L884:
	leal	2(,%rcx,4), %ecx
.L872:
	movq	%r12, %rdi
	leaq	-64(%rbp), %rsi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal11StringTable9LookupKeyINS0_15SeqSubStringKeyINS0_16SeqOneByteStringEEEEENS0_6HandleINS0_6StringEEEPNS0_7IsolateEPT_@PLT
	movq	-24(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L886
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L868:
	.cfi_restore_state
	cmpl	$16383, %ecx
	jle	.L869
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L885:
	cmpb	$48, %dil
	jne	.L878
	cmpl	$1, %ecx
	jne	.L869
.L878:
	subl	$48, %edi
	cmpl	$1, %ecx
	je	.L873
	leal	-2(%rcx), %eax
	leaq	1(%rsi), %rdx
	movl	$429496729, %r9d
	leaq	2(%rsi,%rax), %r10
.L874:
	movzbl	(%rdx), %eax
	leal	-48(%rax), %r8d
	cmpl	$9, %r8d
	ja	.L869
	subl	$45, %eax
	movl	%r9d, %r11d
	sarl	$3, %eax
	subl	%eax, %r11d
	cmpl	%r11d, %edi
	ja	.L869
	leal	(%rdi,%rdi,4), %eax
	addq	$1, %rdx
	leal	(%r8,%rax,2), %edi
	cmpq	%rdx, %r10
	jne	.L874
.L873:
	movl	%ecx, %esi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movl	%eax, %ecx
	jmp	.L872
.L886:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27206:
	.size	_ZN2v88internal7Factory17InternalizeStringINS0_16SeqOneByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib, .-_ZN2v88internal7Factory17InternalizeStringINS0_16SeqOneByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib
	.section	.text._ZN2v88internal7Factory17InternalizeStringINS0_16SeqTwoByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib,"axG",@progbits,_ZN2v88internal7Factory17InternalizeStringINS0_16SeqTwoByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory17InternalizeStringINS0_16SeqTwoByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib
	.type	_ZN2v88internal7Factory17InternalizeStringINS0_16SeqTwoByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib, @function
_ZN2v88internal7Factory17InternalizeStringINS0_16SeqTwoByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib:
.LFB27211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEEE(%rip), %rax
	movl	%edx, -40(%rbp)
	movslq	%edx, %rdx
	movq	%rax, -64(%rbp)
	movq	(%rsi), %rax
	movq	%rsi, -48(%rbp)
	leaq	15(%rax,%rdx,2), %rsi
	leal	-1(%rcx), %eax
	movl	$0, -56(%rbp)
	movl	%ecx, -52(%rbp)
	movb	%r8b, -36(%rbp)
	cmpl	$9, %eax
	ja	.L888
	movzwl	(%rsi), %edi
	leal	-48(%rdi), %edx
	cmpl	$9, %edx
	jbe	.L905
.L889:
	movq	1176(%r12), %rax
	movslq	%ecx, %rcx
	leaq	(%rsi,%rcx,2), %rcx
	movl	15(%rax), %eax
	cmpq	%rcx, %rsi
	je	.L895
	.p2align 4,,10
	.p2align 3
.L896:
	movzwl	(%rsi), %edx
	addq	$2, %rsi
	addl	%eax, %edx
	movl	%edx, %eax
	sall	$10, %eax
	addl	%edx, %eax
	movl	%eax, %edx
	shrl	$6, %edx
	xorl	%edx, %eax
	cmpq	%rsi, %rcx
	jne	.L896
.L895:
	leal	(%rax,%rax,8), %ecx
	movl	%ecx, %eax
	shrl	$11, %eax
	xorl	%eax, %ecx
	movl	%ecx, %eax
	sall	$15, %eax
	addl	%ecx, %eax
	movl	%eax, %edx
	andl	$1073741823, %edx
	leal	-1(%rdx), %ecx
	sarl	$31, %ecx
	andl	$27, %ecx
	orl	%eax, %ecx
.L904:
	leal	2(,%rcx,4), %ecx
.L892:
	movq	%r12, %rdi
	leaq	-64(%rbp), %rsi
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal11StringTable9LookupKeyINS0_15SeqSubStringKeyINS0_16SeqTwoByteStringEEEEENS0_6HandleINS0_6StringEEEPNS0_7IsolateEPT_@PLT
	movq	-24(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L906
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L888:
	.cfi_restore_state
	cmpl	$16383, %ecx
	jle	.L889
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L905:
	cmpw	$48, %di
	jne	.L898
	cmpl	$1, %ecx
	jne	.L889
.L898:
	subl	$48, %edi
	cmpl	$1, %ecx
	je	.L893
	leal	-2(%rcx), %eax
	leaq	2(%rsi), %rdx
	movl	$429496729, %r9d
	leaq	4(%rsi,%rax,2), %r10
.L894:
	movzwl	(%rdx), %eax
	leal	-48(%rax), %r8d
	cmpl	$9, %r8d
	ja	.L889
	subl	$45, %eax
	movl	%r9d, %r11d
	sarl	$3, %eax
	subl	%eax, %r11d
	cmpl	%r11d, %edi
	ja	.L889
	leal	(%rdi,%rdi,4), %eax
	addq	$2, %rdx
	leal	(%r8,%rax,2), %edi
	cmpq	%rdx, %r10
	jne	.L894
.L893:
	movl	%ecx, %esi
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movl	%eax, %ecx
	jmp	.L892
.L906:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27211:
	.size	_ZN2v88internal7Factory17InternalizeStringINS0_16SeqTwoByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib, .-_ZN2v88internal7Factory17InternalizeStringINS0_16SeqTwoByteStringEEENS0_6HandleINS0_6StringEEENS4_IT_EEiib
	.section	.text._ZN2v88internal7Factory36AllocateRawOneByteInternalizedStringEij,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory36AllocateRawOneByteInternalizedStringEij
	.type	_ZN2v88internal7Factory36AllocateRawOneByteInternalizedStringEij, @function
_ZN2v88internal7Factory36AllocateRawOneByteInternalizedStringEij:
.LFB24070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$1073741799, %esi
	jg	.L915
	movq	37896(%rdi), %rax
	movl	%esi, %r12d
	leal	23(%rsi), %esi
	movl	%edx, %r13d
	andl	$-8, %esi
	movq	192(%rdi), %r14
	movq	%rdi, %rbx
	movl	$1, %ecx
	cmpb	$1, 232(%rax)
	leaq	37592(%rdi), %rdi
	sbbl	%edx, %edx
	xorl	%r8d, %r8d
	andl	$3, %edx
	addl	$1, %edx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r14, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L910
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L911:
	movl	%r12d, 11(%rsi)
	movq	(%rax), %rdx
	movl	%r13d, 7(%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L910:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L916
.L912:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L915:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L916:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L912
	.cfi_endproc
.LFE24070:
	.size	_ZN2v88internal7Factory36AllocateRawOneByteInternalizedStringEij, .-_ZN2v88internal7Factory36AllocateRawOneByteInternalizedStringEij
	.section	.text._ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE8AsHandleEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE8AsHandleEPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE8AsHandleEPNS0_7IsolateE
	.type	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE8AsHandleEPNS0_7IsolateE, @function
_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE8AsHandleEPNS0_7IsolateE:
.LFB32784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	8(%rbx), %edx
	movl	12(%rbx), %esi
	call	_ZN2v88internal7Factory36AllocateRawOneByteInternalizedStringEij
	movslq	24(%rbx), %rsi
	movslq	12(%rbx), %rdx
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	(%r12), %rax
	addq	$15, %rcx
	leaq	15(%rax), %rdi
	leaq	(%rcx,%rsi), %r9
	cmpq	$7, %rdx
	ja	.L933
	addq	%rdi, %rdx
	cmpq	%rdi, %rdx
	jbe	.L919
	leaq	16(%rcx,%rsi), %rcx
	movq	%rax, %r8
	leaq	16(%rax), %r11
	negq	%r8
	cmpq	%rcx, %rdi
	leaq	31(%rax), %rcx
	setnb	%sil
	cmpq	%rcx, %r9
	setnb	%cl
	orb	%cl, %sil
	je	.L932
	movq	%rdx, %rcx
	subq	%rax, %rcx
	subq	$16, %rcx
	cmpq	$14, %rcx
	seta	%sil
	cmpq	%rdx, %r11
	setbe	%cl
	testb	%cl, %sil
	je	.L932
	movq	%rdx, %rbx
	subq	%rax, %rbx
	cmpq	%rdx, %r11
	movl	$1, %eax
	leaq	-15(%rbx), %r10
	cmova	%rax, %r10
	movq	%rdi, %rax
	xorl	%ecx, %ecx
	addq	%r9, %r8
	leaq	-16(%r10), %rsi
	shrq	$4, %rsi
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L923:
	movdqu	-15(%rax,%r8), %xmm0
	addq	$1, %rcx
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rcx, %rsi
	ja	.L923
	salq	$4, %rsi
	leaq	(%r9,%rsi), %rax
	addq	%rsi, %rdi
	cmpq	%rsi, %r10
	je	.L919
	movzbl	(%rax), %ecx
	movb	%cl, (%rdi)
	leaq	1(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	1(%rax), %ecx
	movb	%cl, 1(%rdi)
	leaq	2(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	2(%rax), %ecx
	movb	%cl, 2(%rdi)
	leaq	3(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	3(%rax), %ecx
	movb	%cl, 3(%rdi)
	leaq	4(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	4(%rax), %ecx
	movb	%cl, 4(%rdi)
	leaq	5(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	5(%rax), %ecx
	movb	%cl, 5(%rdi)
	leaq	6(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	6(%rax), %ecx
	movb	%cl, 6(%rdi)
	leaq	7(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	7(%rax), %ecx
	movb	%cl, 7(%rdi)
	leaq	8(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	8(%rax), %ecx
	movb	%cl, 8(%rdi)
	leaq	9(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	9(%rax), %ecx
	movb	%cl, 9(%rdi)
	leaq	10(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	10(%rax), %ecx
	movb	%cl, 10(%rdi)
	leaq	11(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	11(%rax), %ecx
	movb	%cl, 11(%rdi)
	leaq	12(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	12(%rax), %ecx
	movb	%cl, 12(%rdi)
	leaq	13(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	13(%rax), %ecx
	movb	%cl, 13(%rdi)
	leaq	14(%rdi), %rcx
	cmpq	%rcx, %rdx
	jbe	.L919
	movzbl	14(%rax), %eax
	movb	%al, 14(%rdi)
.L919:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L933:
	.cfi_restore_state
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L932:
	.cfi_restore_state
	addq	%r9, %r8
	.p2align 4,,10
	.p2align 3
.L929:
	movq	%rdi, %rax
	movzbl	-15(%rdi,%r8), %ecx
	addq	$1, %rdi
	movb	%cl, (%rax)
	cmpq	%rdi, %rdx
	ja	.L929
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE32784:
	.size	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE8AsHandleEPNS0_7IsolateE, .-_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE8AsHandleEPNS0_7IsolateE
	.section	.text._ZN2v88internal7Factory33AllocateTwoByteInternalizedStringERKNS0_6VectorIKtEEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory33AllocateTwoByteInternalizedStringERKNS0_6VectorIKtEEj
	.type	_ZN2v88internal7Factory33AllocateTwoByteInternalizedStringERKNS0_6VectorIKtEEj, @function
_ZN2v88internal7Factory33AllocateTwoByteInternalizedStringERKNS0_6VectorIKtEEj:
.LFB24071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r15
	cmpl	$1073741799, %r15d
	jg	.L940
	movq	%rsi, %r14
	leal	23(%r15,%r15), %esi
	movq	%rdi, %rbx
	movl	%edx, %r13d
	andl	$-8, %esi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	824(%rdi), %r12
	leaq	37592(%rdi), %rdi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L936
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L937:
	movl	%r15d, 11(%rsi)
	movq	(%r12), %rax
	movl	%r13d, 7(%rax)
	movq	(%r12), %rax
	movq	(%r14), %rsi
	leaq	15(%rax), %rdi
	movl	8(%r14), %eax
	leal	(%rax,%rax), %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L936:
	.cfi_restore_state
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L941
.L938:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L940:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L941:
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L938
	.cfi_endproc
.LFE24071:
	.size	_ZN2v88internal7Factory33AllocateTwoByteInternalizedStringERKNS0_6VectorIKtEEj, .-_ZN2v88internal7Factory33AllocateTwoByteInternalizedStringERKNS0_6VectorIKtEEj
	.section	.text._ZN2v88internal7Factory36AllocateRawTwoByteInternalizedStringEij,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory36AllocateRawTwoByteInternalizedStringEij
	.type	_ZN2v88internal7Factory36AllocateRawTwoByteInternalizedStringEij, @function
_ZN2v88internal7Factory36AllocateRawTwoByteInternalizedStringEij:
.LFB24072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$1073741799, %esi
	jg	.L948
	movl	%esi, %r12d
	leal	23(%rsi,%rsi), %esi
	movq	%rdi, %rbx
	movl	%edx, %r13d
	andl	$-8, %esi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	824(%rdi), %r14
	leaq	37592(%rdi), %rdi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r14, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L944
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L945:
	movl	%r12d, 11(%rsi)
	movq	(%rax), %rdx
	movl	%r13d, 7(%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L944:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L949
.L946:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L948:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L949:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L946
	.cfi_endproc
.LFE24072:
	.size	_ZN2v88internal7Factory36AllocateRawTwoByteInternalizedStringEij, .-_ZN2v88internal7Factory36AllocateRawTwoByteInternalizedStringEij
	.section	.text._ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE8AsHandleEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE8AsHandleEPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE8AsHandleEPNS0_7IsolateE
	.type	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE8AsHandleEPNS0_7IsolateE, @function
_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE8AsHandleEPNS0_7IsolateE:
.LFB32782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	cmpb	$0, 28(%rbx)
	movl	8(%rbx), %edx
	movl	12(%rbx), %esi
	je	.L951
	call	_ZN2v88internal7Factory36AllocateRawOneByteInternalizedStringEij
	movq	16(%rbx), %rdx
	movslq	24(%rbx), %rcx
	movq	(%rax), %rsi
	movslq	12(%rbx), %r8
	movq	(%rdx), %rdi
	leaq	(%rcx,%rcx), %r9
	leaq	15(%rsi), %rdx
	addq	$15, %rdi
	addq	%rdx, %r8
	leaq	(%rdi,%r9), %r11
	cmpq	%rdx, %r8
	jbe	.L953
	movq	%r8, %rcx
	leaq	16(%rsi), %rbx
	movl	$1, %r10d
	movl	$2, %r12d
	subq	%rsi, %rcx
	subq	$15, %rcx
	cmpq	%rbx, %r8
	cmovnb	%rcx, %r10
	addq	%rcx, %rcx
	cmpq	%rbx, %r8
	cmovb	%r12, %rcx
	addq	%r9, %rcx
	addq	%rdi, %rcx
	cmpq	%rcx, %rdx
	leaq	(%rdx,%r10), %rcx
	setnb	%dil
	cmpq	%rcx, %r11
	setnb	%cl
	orb	%cl, %dil
	je	.L954
	movq	%r8, %rcx
	subq	%rsi, %rcx
	subq	$16, %rcx
	cmpq	$14, %rcx
	seta	%dil
	cmpq	%rbx, %r8
	setnb	%cl
	testb	%cl, %dil
	je	.L954
	leaq	-16(%r10), %rdi
	negq	%rsi
	movdqa	.LC11(%rip), %xmm2
	movq	%rdx, %rcx
	shrq	$4, %rdi
	leaq	(%r11,%rsi,2), %r9
	xorl	%esi, %esi
	addq	$1, %rdi
	.p2align 4,,10
	.p2align 3
.L956:
	movdqu	-30(%r9,%rcx,2), %xmm0
	movdqu	-14(%r9,%rcx,2), %xmm1
	addq	$1, %rsi
	addq	$16, %rcx
	pand	%xmm2, %xmm0
	pand	%xmm2, %xmm1
	packuswb	%xmm1, %xmm0
	movups	%xmm0, -16(%rcx)
	cmpq	%rsi, %rdi
	ja	.L956
	movq	%rdi, %rcx
	salq	$5, %rdi
	salq	$4, %rcx
	addq	%r11, %rdi
	addq	%rcx, %rdx
	cmpq	%rcx, %r10
	je	.L953
	movzwl	(%rdi), %ecx
	movb	%cl, (%rdx)
	leaq	1(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	2(%rdi), %ecx
	movb	%cl, 1(%rdx)
	leaq	2(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	4(%rdi), %ecx
	movb	%cl, 2(%rdx)
	leaq	3(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	6(%rdi), %ecx
	movb	%cl, 3(%rdx)
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	8(%rdi), %ecx
	movb	%cl, 4(%rdx)
	leaq	5(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	10(%rdi), %ecx
	movb	%cl, 5(%rdx)
	leaq	6(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	12(%rdi), %ecx
	movb	%cl, 6(%rdx)
	leaq	7(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	14(%rdi), %ecx
	movb	%cl, 7(%rdx)
	leaq	8(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	16(%rdi), %ecx
	movb	%cl, 8(%rdx)
	leaq	9(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	18(%rdi), %ecx
	movb	%cl, 9(%rdx)
	leaq	10(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	20(%rdi), %ecx
	movb	%cl, 10(%rdx)
	leaq	11(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	22(%rdi), %ecx
	movb	%cl, 11(%rdx)
	leaq	12(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	24(%rdi), %ecx
	movb	%cl, 12(%rdx)
	leaq	13(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	26(%rdi), %ecx
	movb	%cl, 13(%rdx)
	leaq	14(%rdx), %rcx
	cmpq	%rcx, %r8
	jbe	.L953
	movzwl	28(%rdi), %ecx
	movb	%cl, 14(%rdx)
.L953:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L951:
	.cfi_restore_state
	call	_ZN2v88internal7Factory36AllocateRawTwoByteInternalizedStringEij
	movslq	24(%rbx), %rsi
	movslq	12(%rbx), %r9
	movq	%rax, %r12
	movq	16(%rbx), %rax
	movq	(%r12), %rcx
	addq	%rsi, %rsi
	leaq	(%r9,%r9), %rdx
	movq	(%rax), %r10
	leaq	15(%rcx), %rdi
	addq	$15, %r10
	movq	%rdi, %rax
	leaq	(%r10,%rsi), %r8
	cmpq	$3, %r9
	ja	.L983
	addq	%rdi, %rdx
	cmpq	%rdx, %rdi
	jnb	.L962
	movq	%rdx, %r11
	leaq	16(%r10,%rsi), %rsi
	leaq	16(%rcx), %rbx
	movq	%rcx, %r9
	subq	%rcx, %r11
	negq	%r9
	subq	$16, %r11
	cmpq	%rsi, %rdi
	setnb	%sil
	addq	$31, %rcx
	cmpq	%rcx, %r8
	setnb	%cl
	orb	%cl, %sil
	je	.L982
	cmpq	%rdx, %rbx
	setbe	%sil
	cmpq	$13, %r11
	seta	%cl
	testb	%cl, %sil
	je	.L982
	movq	%r11, %rcx
	movl	$1, %eax
	shrq	%rcx
	addq	$1, %rcx
	cmpq	%rdx, %rbx
	cmova	%rax, %rcx
	movq	%rdi, %rax
	addq	%r8, %r9
	movq	%rcx, %rsi
	shrq	$3, %rsi
	salq	$4, %rsi
	addq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L966:
	movdqu	-15(%rax,%r9), %xmm3
	addq	$16, %rax
	movups	%xmm3, -16(%rax)
	cmpq	%rsi, %rax
	jne	.L966
	movq	%rcx, %rax
	andq	$-8, %rax
	leaq	(%rax,%rax), %rsi
	addq	%rsi, %rdi
	addq	%rsi, %r8
	cmpq	%rax, %rcx
	je	.L962
	movzwl	(%r8), %eax
	movw	%ax, (%rdi)
	leaq	2(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L962
	movzwl	2(%r8), %eax
	movw	%ax, 2(%rdi)
	leaq	4(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L962
	movzwl	4(%r8), %eax
	movw	%ax, 4(%rdi)
	leaq	6(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L962
	movzwl	6(%r8), %eax
	movw	%ax, 6(%rdi)
	leaq	8(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L962
	movzwl	8(%r8), %eax
	movw	%ax, 8(%rdi)
	leaq	10(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L962
	movzwl	10(%r8), %eax
	movw	%ax, 10(%rdi)
	leaq	12(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L962
	movzwl	12(%r8), %eax
	movw	%ax, 12(%rdi)
.L962:
	movq	%r12, %rax
.L984:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L983:
	.cfi_restore_state
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	%r12, %rax
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L954:
	negq	%rsi
	leaq	(%r11,%rsi,2), %rdi
	.p2align 4,,10
	.p2align 3
.L960:
	movzwl	-30(%rdi,%rdx,2), %esi
	movq	%rdx, %rcx
	addq	$1, %rdx
	movb	%sil, (%rcx)
	cmpq	%rdx, %r8
	ja	.L960
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L982:
	addq	%r8, %r9
	.p2align 4,,10
	.p2align 3
.L979:
	movq	%rax, %rcx
	movzwl	-15(%rax,%r9), %esi
	addq	$2, %rax
	movw	%si, (%rcx)
	cmpq	%rax, %rdx
	ja	.L979
	jmp	.L962
	.cfi_endproc
.LFE32782:
	.size	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE8AsHandleEPNS0_7IsolateE, .-_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE8AsHandleEPNS0_7IsolateE
	.section	.text._ZN2v88internal7Factory28NewOneByteInternalizedStringERKNS0_6VectorIKhEEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory28NewOneByteInternalizedStringERKNS0_6VectorIKhEEj
	.type	_ZN2v88internal7Factory28NewOneByteInternalizedStringERKNS0_6VectorIKhEEj, @function
_ZN2v88internal7Factory28NewOneByteInternalizedStringERKNS0_6VectorIKhEEj:
.LFB24074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r15
	cmpl	$1073741799, %r15d
	jg	.L993
	movq	37896(%rdi), %rax
	movq	%rsi, %r14
	movl	%edx, %r13d
	movq	%rdi, %rbx
	leal	23(%r15), %esi
	movq	192(%rdi), %r12
	movl	$1, %ecx
	leaq	37592(%rdi), %rdi
	andl	$-8, %esi
	cmpb	$1, 232(%rax)
	sbbl	%edx, %edx
	xorl	%r8d, %r8d
	andl	$3, %edx
	addl	$1, %edx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L988
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L989:
	movl	%r15d, 11(%rsi)
	movq	(%r12), %rax
	movl	%r13d, 7(%rax)
	movq	(%r12), %rax
	movslq	8(%r14), %rdx
	movq	(%r14), %rsi
	leaq	15(%rax), %rdi
	call	memcpy@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L988:
	.cfi_restore_state
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L994
.L990:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L993:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L994:
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L990
	.cfi_endproc
.LFE24074:
	.size	_ZN2v88internal7Factory28NewOneByteInternalizedStringERKNS0_6VectorIKhEEj, .-_ZN2v88internal7Factory28NewOneByteInternalizedStringERKNS0_6VectorIKhEEj
	.section	.text._ZN2v88internal7Factory28NewTwoByteInternalizedStringERKNS0_6VectorIKtEEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory28NewTwoByteInternalizedStringERKNS0_6VectorIKtEEj
	.type	_ZN2v88internal7Factory28NewTwoByteInternalizedStringERKNS0_6VectorIKtEEj, @function
_ZN2v88internal7Factory28NewTwoByteInternalizedStringERKNS0_6VectorIKtEEj:
.LFB24075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r15
	cmpl	$1073741799, %r15d
	jg	.L1001
	movq	%rsi, %r14
	leal	23(%r15,%r15), %esi
	movq	%rdi, %rbx
	movl	%edx, %r13d
	andl	$-8, %esi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	824(%rdi), %r12
	leaq	37592(%rdi), %rdi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L997
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L998:
	movl	%r15d, 11(%rsi)
	movq	(%r12), %rax
	movl	%r13d, 7(%rax)
	movq	(%r12), %rax
	movq	(%r14), %rsi
	leaq	15(%rax), %rdi
	movl	8(%r14), %eax
	leal	(%rax,%rax), %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L997:
	.cfi_restore_state
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L1002
.L999:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1001:
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L999
	.cfi_endproc
.LFE24075:
	.size	_ZN2v88internal7Factory28NewTwoByteInternalizedStringERKNS0_6VectorIKtEEj, .-_ZN2v88internal7Factory28NewTwoByteInternalizedStringERKNS0_6VectorIKtEEj
	.section	.text._ZN2v88internal7Factory25NewInternalizedStringImplENS0_6HandleINS0_6StringEEEij,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory25NewInternalizedStringImplENS0_6HandleINS0_6StringEEEij
	.type	_ZN2v88internal7Factory25NewInternalizedStringImplENS0_6HandleINS0_6StringEEEij, @function
_ZN2v88internal7Factory25NewInternalizedStringImplENS0_6HandleINS0_6StringEEEij:
.LFB24076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	testb	$8, 11(%rax)
	je	.L1004
	movq	37896(%rdi), %rax
	leal	23(%rdx), %esi
	movq	192(%rdi), %rbx
	movl	$1, %ecx
	andl	$-8, %esi
	leaq	37592(%rdi), %rdi
	cmpb	$1, 232(%rax)
	sbbl	%edx, %edx
	xorl	%r8d, %r8d
	andl	$3, %edx
	addl	$1, %edx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rbx, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1006
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L1007:
	movl	%r13d, 11(%rsi)
	movq	(%rbx), %rax
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movl	%r12d, 7(%rax)
	movq	(%rbx), %rax
	movq	(%r14), %rdi
	leaq	15(%rax), %rsi
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii@PLT
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1004:
	.cfi_restore_state
	movq	37896(%rdi), %rax
	leal	23(%rdx,%rdx), %esi
	movq	824(%rdi), %rbx
	movl	$1, %ecx
	andl	$-8, %esi
	leaq	37592(%rdi), %rdi
	cmpb	$1, 232(%rax)
	sbbl	%edx, %edx
	xorl	%r8d, %r8d
	andl	$3, %edx
	addl	$1, %edx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rbx, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1011
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L1012:
	movl	%r13d, 11(%rsi)
	movq	(%rbx), %rax
	movl	%r13d, %ecx
	xorl	%edx, %edx
	movl	%r12d, 7(%rax)
	movq	(%rbx), %rax
	movq	(%r14), %rdi
	leaq	15(%rax), %rsi
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1006:
	.cfi_restore_state
	movq	41088(%r15), %rbx
	cmpq	41096(%r15), %rbx
	je	.L1017
.L1008:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	41088(%r15), %rbx
	cmpq	41096(%r15), %rbx
	je	.L1018
.L1013:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rbx)
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1008
	.cfi_endproc
.LFE24076:
	.size	_ZN2v88internal7Factory25NewInternalizedStringImplENS0_6HandleINS0_6StringEEEij, .-_ZN2v88internal7Factory25NewInternalizedStringImplENS0_6HandleINS0_6StringEEEij
	.section	.text._ZN2v88internal7Factory30InternalizedStringMapForStringENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory30InternalizedStringMapForStringENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal7Factory30InternalizedStringMapForStringENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal7Factory30InternalizedStringMapForStringENS0_6HandleINS0_6StringEEE:
.LFB24081:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L1020
.L1030:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$32, %eax
	cmpw	$26, %ax
	ja	.L1030
	leaq	.L1024(%rip), %rdx
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Factory30InternalizedStringMapForStringENS0_6HandleINS0_6StringEEE,"a",@progbits
	.align 4
	.align 4
.L1024:
	.long	.L1029-.L1024
	.long	.L1030-.L1024
	.long	.L1028-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1027-.L1024
	.long	.L1030-.L1024
	.long	.L1026-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1025-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1030-.L1024
	.long	.L1023-.L1024
	.section	.text._ZN2v88internal7Factory30InternalizedStringMapForStringENS0_6HandleINS0_6StringEEE
	.p2align 4,,10
	.p2align 3
.L1023:
	leaq	856(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1029:
	leaq	824(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1028:
	leaq	832(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1027:
	leaq	192(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1026:
	leaq	840(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	leaq	848(%rdi), %rax
	ret
	.cfi_endproc
.LFE24081:
	.size	_ZN2v88internal7Factory30InternalizedStringMapForStringENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal7Factory30InternalizedStringMapForStringENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt
	.type	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt, @function
_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt:
.LFB24085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpw	$255, %si
	ja	.L1032
	movzwl	%si, %r13d
	movq	4616(%rdi), %rdx
	leal	16(,%r13,8), %eax
	cltq
	movq	-1(%rax,%rdx), %r12
	cmpq	88(%rdi), %r12
	je	.L1033
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L1034
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1032:
	movw	%si, -28(%rbp)
	leaq	-28(%rbp), %rax
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -48(%rbp)
	movq	$1, -40(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb
.L1038:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1041
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1033:
	.cfi_restore_state
	leaq	-48(%rbp), %r12
	movb	%sil, -25(%rbp)
	leaq	-25(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rdi, -56(%rbp)
	movq	%rax, -48(%rbp)
	movq	$1, -40(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb
	movq	-56(%rbp), %rdi
	movl	%r13d, %esi
	movq	%rax, -56(%rbp)
	movq	4616(%rdi), %rdx
	movq	%r12, %rdi
	movq	%rdx, -48(%rbp)
	movq	(%rax), %rdx
	call	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	movq	-56(%rbp), %rax
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L1042
.L1036:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%r12, (%rax)
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rdi
	jmp	.L1036
.L1041:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24085:
	.size	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt, .-_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt
	.section	.text._ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_ib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_ib
	.type	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_ib, @function
_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_ib:
.LFB24089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testb	%r8b, %r8b
	je	.L1044
	leaq	752(%rdi), %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE
	movq	41112(%rbx), %rdi
	movq	%rax, %r15
	movq	%r15, %rsi
	testq	%rdi, %rdi
	je	.L1046
.L1079:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1047:
	movq	%rsi, %rdx
	addq	$7, %rsi
	andq	$-262144, %rdx
	movq	8(%rdx), %rdx
	testl	$262144, %edx
	jne	.L1049
	andl	$24, %edx
	jne	.L1076
.L1049:
	movl	$3, (%rsi)
	movq	(%rax), %rdx
	movl	%r14d, 11(%rdx)
	movq	(%rax), %r14
	movq	0(%r13), %r13
	leaq	15(%r14), %r15
	movq	%r13, 15(%r14)
	testb	$1, %r13b
	je	.L1052
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$4, 10(%rdx)
	jne	.L1077
.L1051:
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L1052
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L1052
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
.L1052:
	movq	(%rax), %r13
	movq	(%r12), %r12
	movq	%r12, 23(%r13)
	leaq	23(%r13), %r14
	testb	$1, %r12b
	je	.L1065
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$4, 10(%rdx)
	jne	.L1078
.L1054:
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L1065
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L1065
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1076:
	movl	$3, (%rsi)
	movq	(%rax), %rdx
	movl	%r14d, 11(%rdx)
	movq	(%rax), %rdx
	movq	0(%r13), %rcx
	movq	%rcx, 15(%rdx)
	movq	(%rax), %rdx
	movq	(%r12), %rcx
	movq	%rcx, 23(%rdx)
.L1065:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1044:
	.cfi_restore_state
	leaq	760(%rdi), %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE
	movq	41112(%rbx), %rdi
	movq	%rax, %r15
	movq	%r15, %rsi
	testq	%rdi, %rdi
	jne	.L1079
.L1046:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1080
.L1048:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r15, (%rax)
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	%rbx, %rdi
	movq	%r15, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1078:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	jmp	.L1051
	.cfi_endproc
.LFE24089:
	.size	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_ib, .-_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_ib
	.section	.text._ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE:
.LFB24096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$24, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$8, %rsp
	movq	-37416(%rdi), %r12
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rbx, %rdi
	movl	$1073741823, %esi
	movq	%r12, -1(%rax)
	movq	%rax, %r13
	call	_ZN2v88internal7Isolate20GenerateIdentityHashEj@PLT
	movq	41112(%rbx), %rdi
	movl	%eax, %r14d
	testq	%rdi, %rdi
	je	.L1082
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	movq	%rax, %r12
.L1083:
	leal	2(,%r14,4), %r14d
	movl	%r14d, 7(%r13)
	movq	(%r12), %r14
	movq	88(%rbx), %r13
	leaq	15(%r14), %r15
	movq	%r13, 15(%r14)
	testb	$1, %r13b
	je	.L1088
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1096
	testb	$24, %al
	je	.L1088
.L1098:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1097
.L1088:
	movq	(%r12), %rax
	movl	$0, 11(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1096:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1098
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L1099
.L1084:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%r12)
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1097:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L1084
	.cfi_endproc
.LFE24096:
	.size	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE, .-_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE:
.LFB24097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE
	movq	(%rax), %rdx
	orl	$1, 11(%rdx)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24097:
	.size	_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE, .-_ZN2v88internal7Factory16NewPrivateSymbolENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory20NewPrivateNameSymbolENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory20NewPrivateNameSymbolENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal7Factory20NewPrivateNameSymbolENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal7Factory20NewPrivateNameSymbolENS0_6HandleINS0_6StringEEE:
.LFB24098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$1, %esi
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_ZN2v88internal7Factory9NewSymbolENS0_14AllocationTypeE
	movq	%rax, %r12
	movq	(%rax), %rax
	orl	$17, 11(%rax)
	movq	(%r12), %r14
	movq	0(%r13), %r13
	movq	%r13, 15(%r14)
	testb	$1, %r13b
	je	.L1106
	movq	%r13, %rbx
	leaq	15(%r14), %r15
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1114
	testb	$24, %al
	je	.L1106
.L1116:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1115
.L1106:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1114:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1116
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1106
	.cfi_endproc
.LFE24098:
	.size	_ZN2v88internal7Factory20NewPrivateNameSymbolENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal7Factory20NewPrivateNameSymbolENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal7Factory10NewContextENS0_9RootIndexEiiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory10NewContextENS0_9RootIndexEiiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory10NewContextENS0_9RootIndexEiiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory10NewContextENS0_9RootIndexEiiNS0_14AllocationTypeE:
.LFB24099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	%si, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%ecx, %r13d
	movl	$1, %ecx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	movl	%r8d, %edx
	xorl	%r8d, %r8d
	subq	$16, %rsp
	movq	56(%rdi,%rsi,8), %r14
	addq	$37592, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r14, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1118
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L1119:
	salq	$32, %r13
	movq	%r13, 7(%rsi)
	cmpl	$48, %ebx
	jg	.L1123
	addq	$16, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1118:
	.cfi_restore_state
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L1124
.L1120:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L1119
	.p2align 4,,10
	.p2align 3
.L1123:
	movslq	%ebx, %rcx
	movq	(%r8), %rbx
	movq	88(%r12), %rax
	subq	$48, %rcx
	shrq	$3, %rcx
	leaq	47(%rbx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$16, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1124:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1120
	.cfi_endproc
.LFE24099:
	.size	_ZN2v88internal7Factory10NewContextENS0_9RootIndexEiiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory10NewContextENS0_9RootIndexEiiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory16NewNativeContextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory16NewNativeContextEv
	.type	_ZN2v88internal7Factory16NewNativeContextEv, @function
_ZN2v88internal7Factory16NewNativeContextEv:
.LFB24100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1976, %esi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$24, %rsp
	movq	-37248(%rdi), %r12
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1126
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L1127:
	movabsq	$1047972020224, %rax
	movl	$241, %ecx
	movq	%rax, 7(%rsi)
	movq	(%r12), %rdx
	movq	88(%rbx), %rax
	leaq	47(%rdx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	(%r12), %r14
	movq	280(%rbx), %r13
	movq	%r13, 15(%r14)
	leaq	15(%r14), %rsi
	testb	$1, %r13b
	je	.L1143
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1166
	testb	$24, %al
	je	.L1143
.L1175:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1167
	.p2align 4,,10
	.p2align 3
.L1143:
	movq	(%r12), %rax
	movq	$0, 23(%rax)
	movq	(%r12), %r14
	movq	96(%rbx), %r13
	movq	%r13, 31(%r14)
	leaq	31(%r14), %rsi
	testb	$1, %r13b
	je	.L1142
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1168
	testb	$24, %al
	je	.L1142
.L1173:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1169
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	(%r12), %rdi
	movq	%rdi, 39(%rdi)
	leaq	39(%rdi), %rsi
	testb	$1, %dil
	je	.L1141
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L1170
.L1141:
	movq	(%r12), %rax
	movq	$0, 359(%rax)
	movq	(%r12), %rax
	movq	$0, 831(%rax)
	movq	(%r12), %r14
	movq	288(%rbx), %r13
	movq	%r13, 1159(%r14)
	leaq	1159(%r14), %r15
	testb	$1, %r13b
	je	.L1140
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1171
	testb	$24, %al
	je	.L1140
.L1174:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1172
.L1140:
	movq	(%r12), %rax
	movq	$0, 1967(%rax)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1168:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1173
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1171:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1174
	jmp	.L1140
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	%rdi, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1166:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1175
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1126:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L1176
.L1128:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1167:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1169:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1172:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1140
	.p2align 4,,10
	.p2align 3
.L1176:
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1128
	.cfi_endproc
.LFE24100:
	.size	_ZN2v88internal7Factory16NewNativeContextEv, .-_ZN2v88internal7Factory16NewNativeContextEv
	.section	.text._ZN2v88internal7Factory16NewScriptContextENS0_6HandleINS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory16NewScriptContextENS0_6HandleINS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE
	.type	_ZN2v88internal7Factory16NewScriptContextENS0_6HandleINS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE, @function
_ZN2v88internal7Factory16NewScriptContextENS0_6HandleINS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE:
.LFB24101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo13ContextLengthEv@PLT
	movq	368(%rbx), %r12
	xorl	%r8d, %r8d
	leaq	37592(%rbx), %rdi
	leal	16(,%rax,8), %r14d
	movl	$1, %ecx
	movl	$1, %edx
	movl	%eax, %r13d
	movl	%r14d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1178
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L1179:
	salq	$32, %r13
	movq	%r13, 7(%rsi)
	cmpl	$48, %r14d
	jg	.L1224
.L1181:
	movq	(%r12), %r14
	movq	(%r15), %r13
	movq	%r13, 15(%r14)
	leaq	15(%r14), %rsi
	testb	$1, %r13b
	je	.L1197
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L1225
	testb	$24, %al
	je	.L1197
.L1237:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1226
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	-72(%rbp), %rax
	movq	(%r12), %r14
	movq	(%rax), %r13
	leaq	23(%r14), %rsi
	movq	%r13, 23(%r14)
	testb	$1, %r13b
	je	.L1196
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L1227
	testb	$24, %al
	je	.L1196
.L1234:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1228
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	(%r12), %r14
	movq	96(%rbx), %r13
	movq	%r13, 31(%r14)
	leaq	31(%r14), %rsi
	testb	$1, %r13b
	je	.L1195
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1229
	testb	$24, %al
	je	.L1195
.L1236:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1230
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	-72(%rbp), %rax
	movq	(%r12), %r14
	movq	(%rax), %r13
	leaq	39(%r14), %r15
	movq	%r13, 39(%r14)
	testb	$1, %r13b
	je	.L1194
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1231
	testb	$24, %al
	je	.L1194
.L1235:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1232
	.p2align 4,,10
	.p2align 3
.L1194:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1233
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1227:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1234
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1231:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1235
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1229:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-80(%rbp), %rsi
	testb	$24, %al
	jne	.L1236
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1225:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1237
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1178:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L1238
.L1180:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	(%r12), %rdx
	movslq	%r14d, %rcx
	movq	88(%rbx), %rax
	subq	$48, %rcx
	shrq	$3, %rcx
	leaq	47(%rdx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	jmp	.L1181
	.p2align 4,,10
	.p2align 3
.L1226:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1228:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1232:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	%rbx, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1180
.L1233:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24101:
	.size	_ZN2v88internal7Factory16NewScriptContextENS0_6HandleINS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE, .-_ZN2v88internal7Factory16NewScriptContextENS0_6HandleINS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE
	.section	.text._ZN2v88internal7Factory21NewScriptContextTableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory21NewScriptContextTableEv
	.type	_ZN2v88internal7Factory21NewScriptContextTableEv, @function
_ZN2v88internal7Factory21NewScriptContextTableEv:
.LFB24102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %esi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$16, %rsp
	movq	-37504(%rdi), %r12
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	movq	416(%rbx), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1240
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L1241:
	movabsq	$4294967296, %rax
	movl	$1, %ecx
	movq	%rax, 7(%rsi)
	movq	(%r8), %rax
	leaq	15(%rax), %rdi
	movq	%r12, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	(%r8), %rax
	movq	$0, 15(%rax)
	addq	$16, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1240:
	.cfi_restore_state
	movq	41088(%rbx), %r8
	cmpq	41096(%rbx), %r8
	je	.L1244
.L1242:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r8)
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L1242
	.cfi_endproc
.LFE24102:
	.size	_ZN2v88internal7Factory21NewScriptContextTableEv, .-_ZN2v88internal7Factory21NewScriptContextTableEv
	.section	.text._ZN2v88internal7Factory16NewModuleContextENS0_6HandleINS0_16SourceTextModuleEEENS2_INS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory16NewModuleContextENS0_6HandleINS0_16SourceTextModuleEEENS2_INS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE
	.type	_ZN2v88internal7Factory16NewModuleContextENS0_6HandleINS0_16SourceTextModuleEEENS2_INS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE, @function
_ZN2v88internal7Factory16NewModuleContextENS0_6HandleINS0_16SourceTextModuleEEENS2_INS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE:
.LFB24103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%rsi, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo13ContextLengthEv@PLT
	movq	352(%rbx), %r12
	xorl	%r8d, %r8d
	leaq	37592(%rbx), %rdi
	leal	16(,%rax,8), %r14d
	movl	$1, %ecx
	movl	$1, %edx
	movl	%eax, %r13d
	movl	%r14d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1246
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L1247:
	salq	$32, %r13
	movq	%r13, 7(%rsi)
	cmpl	$48, %r14d
	jg	.L1292
.L1249:
	movq	(%r12), %rbx
	movq	(%r15), %r13
	movq	%r13, 15(%rbx)
	leaq	15(%rbx), %rsi
	testb	$1, %r13b
	je	.L1265
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1293
	testb	$24, %al
	je	.L1265
.L1305:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1294
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	-72(%rbp), %rax
	movq	(%r12), %rbx
	movq	(%rax), %r13
	leaq	23(%rbx), %rsi
	movq	%r13, 23(%rbx)
	testb	$1, %r13b
	je	.L1264
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L1295
	testb	$24, %al
	je	.L1264
.L1302:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1296
	.p2align 4,,10
	.p2align 3
.L1264:
	movq	-80(%rbp), %rax
	movq	(%r12), %rbx
	movq	(%rax), %r13
	leaq	31(%rbx), %rsi
	movq	%r13, 31(%rbx)
	testb	$1, %r13b
	je	.L1263
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1297
	testb	$24, %al
	je	.L1263
.L1304:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1298
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	-72(%rbp), %rax
	movq	(%r12), %r15
	movq	(%rax), %r13
	leaq	39(%r15), %r14
	movq	%r13, 39(%r15)
	testb	$1, %r13b
	je	.L1262
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1299
	testb	$24, %al
	je	.L1262
.L1303:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1300
	.p2align 4,,10
	.p2align 3
.L1262:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1301
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1295:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1302
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1299:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1303
	jmp	.L1262
	.p2align 4,,10
	.p2align 3
.L1297:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-80(%rbp), %rsi
	testb	$24, %al
	jne	.L1304
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1293:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1305
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1246:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L1306
.L1248:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L1247
	.p2align 4,,10
	.p2align 3
.L1292:
	movq	(%r12), %rdx
	movslq	%r14d, %rcx
	movq	88(%rbx), %rax
	subq	$48, %rcx
	shrq	$3, %rcx
	leaq	47(%rdx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1294:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1296:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1298:
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1300:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1262
	.p2align 4,,10
	.p2align 3
.L1306:
	movq	%rbx, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1248
.L1301:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24103:
	.size	_ZN2v88internal7Factory16NewModuleContextENS0_6HandleINS0_16SourceTextModuleEEENS2_INS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE, .-_ZN2v88internal7Factory16NewModuleContextENS0_6HandleINS0_16SourceTextModuleEEENS2_INS0_13NativeContextEEENS2_INS0_9ScopeInfoEEE
	.section	.text._ZN2v88internal7Factory18NewFunctionContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18NewFunctionContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE
	.type	_ZN2v88internal7Factory18NewFunctionContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE, @function
_ZN2v88internal7Factory18NewFunctionContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE:
.LFB24104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-64(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo10scope_typeEv@PLT
	cmpb	$1, %al
	je	.L1326
	cmpb	$2, %al
	jne	.L1353
	movl	$21, %r15d
.L1308:
	movq	(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo13ContextLengthEv@PLT
	movl	%r15d, %esi
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movl	%eax, %ecx
	leal	16(,%rax,8), %edx
	call	_ZN2v88internal7Factory10NewContextENS0_9RootIndexEiiNS0_14AllocationTypeE
	movq	(%rbx), %r15
	movq	(%rax), %rdi
	movq	%rax, %r12
	movq	%r15, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r15b
	je	.L1324
	movq	%r15, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1354
	testb	$24, %al
	je	.L1324
.L1366:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1355
	.p2align 4,,10
	.p2align 3
.L1324:
	movq	(%r12), %rbx
	movq	0(%r13), %r15
	movq	%r15, 23(%rbx)
	leaq	23(%rbx), %rsi
	testb	$1, %r15b
	je	.L1323
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L1356
	testb	$24, %al
	je	.L1323
.L1363:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1357
	.p2align 4,,10
	.p2align 3
.L1323:
	movq	(%r12), %r15
	movq	96(%r14), %r14
	movq	%r14, 31(%r15)
	leaq	31(%r15), %rsi
	testb	$1, %r14b
	je	.L1322
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1358
	testb	$24, %al
	je	.L1322
.L1365:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1359
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	0(%r13), %rax
	movq	(%r12), %r14
	movq	39(%rax), %r13
	leaq	39(%r14), %r15
	movq	%r13, 39(%r14)
	testb	$1, %r13b
	je	.L1321
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1360
	testb	$24, %al
	je	.L1321
.L1364:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1361
	.p2align 4,,10
	.p2align 3
.L1321:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1362
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1356:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1363
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1360:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1364
	jmp	.L1321
	.p2align 4,,10
	.p2align 3
.L1358:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-72(%rbp), %rsi
	testb	$24, %al
	jne	.L1365
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1354:
	movq	%r15, %rdx
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	testb	$24, %al
	jne	.L1366
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1326:
	movl	$38, %r15d
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1355:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1357:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1359:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1361:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1321
.L1353:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1362:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24104:
	.size	_ZN2v88internal7Factory18NewFunctionContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE, .-_ZN2v88internal7Factory18NewFunctionContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE
	.section	.text._ZN2v88internal7Factory15NewCatchContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory15NewCatchContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal7Factory15NewCatchContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal7Factory15NewCatchContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_6ObjectEEE:
.LFB24105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	movl	$1, %ecx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$56, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$40, %rsp
	movq	-37200(%rdi), %r12
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1368
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L1369:
	movabsq	$21474836480, %rax
	movl	$1, %ecx
	movq	%rax, 7(%rsi)
	movq	(%r12), %rdx
	movq	88(%rbx), %rax
	leaq	47(%rdx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	(%r12), %rdi
	movq	(%r15), %r15
	movq	%r15, 15(%rdi)
	leaq	15(%rdi), %rsi
	testb	$1, %r15b
	je	.L1390
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L1422
	testb	$24, %al
	je	.L1390
.L1436:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1423
	.p2align 4,,10
	.p2align 3
.L1390:
	movq	(%r12), %rdi
	movq	0(%r13), %r15
	movq	%r15, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %r15b
	je	.L1389
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L1424
	testb	$24, %al
	je	.L1389
.L1435:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1425
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	(%r12), %rdi
	movq	96(%rbx), %r15
	movq	%r15, 31(%rdi)
	leaq	31(%rdi), %rsi
	testb	$1, %r15b
	je	.L1388
	movq	%r15, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1426
	testb	$24, %al
	je	.L1388
.L1434:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1427
	.p2align 4,,10
	.p2align 3
.L1388:
	movq	0(%r13), %rax
	movq	(%r12), %r15
	movq	39(%rax), %r13
	leaq	39(%r15), %rsi
	movq	%r13, 39(%r15)
	testb	$1, %r13b
	je	.L1387
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1428
	testb	$24, %al
	je	.L1387
.L1433:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1429
	.p2align 4,,10
	.p2align 3
.L1387:
	movq	(%r12), %r15
	movq	(%r14), %r13
	movq	%r13, 47(%r15)
	leaq	47(%r15), %r14
	testb	$1, %r13b
	je	.L1386
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1430
	testb	$24, %al
	je	.L1386
.L1432:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1431
.L1386:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1430:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1432
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1428:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1433
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	%r15, %rdx
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	testb	$24, %al
	jne	.L1434
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1424:
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1435
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1422:
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1436
	jmp	.L1390
	.p2align 4,,10
	.p2align 3
.L1368:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L1437
.L1370:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1423:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1390
	.p2align 4,,10
	.p2align 3
.L1425:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1427:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1429:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L1431:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1370
	.cfi_endproc
.LFE24105:
	.size	_ZN2v88internal7Factory15NewCatchContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal7Factory15NewCatchContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal7Factory23NewDebugEvaluateContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEES4_NS2_INS0_9StringSetEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory23NewDebugEvaluateContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEES4_NS2_INS0_9StringSetEEE
	.type	_ZN2v88internal7Factory23NewDebugEvaluateContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEES4_NS2_INS0_9StringSetEEE, @function
_ZN2v88internal7Factory23NewDebugEvaluateContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEES4_NS2_INS0_9StringSetEEE:
.LFB24106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	96(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$64, %esi
	subq	$40, %rsp
	testq	%rcx, %rcx
	movq	408(%rdi), %r12
	movq	%rdx, -56(%rbp)
	cmovne	%rcx, %r14
	addq	$37592, %rdi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1440
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L1441:
	movabsq	$25769803776, %rax
	movl	$2, %ecx
	movq	%rax, 7(%rsi)
	movq	(%r12), %rdx
	movq	88(%r15), %rax
	leaq	47(%rdx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	-56(%rbp), %rax
	movq	(%r12), %rdi
	movq	(%rax), %r15
	leaq	15(%rdi), %rsi
	movq	%r15, 15(%rdi)
	testb	$1, %r15b
	je	.L1466
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L1510
	testb	$24, %al
	je	.L1466
.L1524:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1511
	.p2align 4,,10
	.p2align 3
.L1466:
	movq	(%r12), %rdi
	movq	(%rbx), %r15
	movq	%r15, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %r15b
	je	.L1465
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L1512
	testb	$24, %al
	je	.L1465
.L1525:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1513
	.p2align 4,,10
	.p2align 3
.L1465:
	movq	(%rbx), %rax
	movq	(%r12), %rdi
	movq	39(%rax), %r15
	leaq	39(%rdi), %rsi
	movq	%r15, 39(%rdi)
	testb	$1, %r15b
	je	.L1464
	movq	%r15, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1514
	testb	$24, %al
	je	.L1464
.L1523:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1515
	.p2align 4,,10
	.p2align 3
.L1464:
	movq	(%r12), %r15
	movq	(%r14), %r14
	movq	%r14, 31(%r15)
	leaq	31(%r15), %rsi
	testb	$1, %r14b
	je	.L1463
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1516
	testb	$24, %al
	je	.L1463
.L1522:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1517
	.p2align 4,,10
	.p2align 3
.L1463:
	testq	%r13, %r13
	je	.L1455
	movq	(%r12), %r14
	movq	0(%r13), %r13
	movq	%r13, 47(%r14)
	leaq	47(%r14), %r15
	testb	$1, %r13b
	je	.L1455
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1518
.L1457:
	testb	$24, %al
	je	.L1455
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1519
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1459
	movq	(%r12), %r13
	movq	(%rax), %r15
	movq	%r15, 55(%r13)
	leaq	55(%r13), %r14
	testb	$1, %r15b
	je	.L1459
	movq	%r15, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1520
.L1461:
	testb	$24, %al
	je	.L1459
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1521
.L1459:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1516:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1522
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1514:
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-72(%rbp), %rsi
	movq	-56(%rbp), %rdi
	testb	$24, %al
	jne	.L1523
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1510:
	movq	%r15, %rdx
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1524
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1512:
	movq	%r15, %rdx
	movq	%rsi, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1525
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L1526
.L1442:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1513:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1515:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1517:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1519:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1521:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1526:
	movq	%r15, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1442
	.cfi_endproc
.LFE24106:
	.size	_ZN2v88internal7Factory23NewDebugEvaluateContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEES4_NS2_INS0_9StringSetEEE, .-_ZN2v88internal7Factory23NewDebugEvaluateContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEES4_NS2_INS0_9StringSetEEE
	.section	.text._ZN2v88internal7Factory14NewWithContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory14NewWithContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEE
	.type	_ZN2v88internal7Factory14NewWithContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEE, @function
_ZN2v88internal7Factory14NewWithContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEE:
.LFB24107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	movl	$1, %ecx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$48, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$24, %rsp
	movq	-37192(%rdi), %r12
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1528
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L1529:
	movabsq	$17179869184, %rax
	movq	%rax, 7(%rsi)
	movq	(%r12), %rbx
	movq	(%r15), %r15
	movq	%r15, 15(%rbx)
	leaq	15(%rbx), %rsi
	testb	$1, %r15b
	je	.L1546
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L1572
	testb	$24, %al
	je	.L1546
.L1583:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1573
	.p2align 4,,10
	.p2align 3
.L1546:
	movq	(%r12), %rbx
	movq	0(%r13), %r15
	movq	%r15, 23(%rbx)
	leaq	23(%rbx), %rsi
	testb	$1, %r15b
	je	.L1545
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L1574
	testb	$24, %al
	je	.L1545
.L1580:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1575
	.p2align 4,,10
	.p2align 3
.L1545:
	movq	(%r12), %r15
	movq	(%r14), %r14
	movq	%r14, 31(%r15)
	leaq	31(%r15), %rsi
	testb	$1, %r14b
	je	.L1544
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1576
	testb	$24, %al
	je	.L1544
.L1582:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1577
	.p2align 4,,10
	.p2align 3
.L1544:
	movq	0(%r13), %rax
	movq	(%r12), %r14
	movq	39(%rax), %r13
	leaq	39(%r14), %r15
	movq	%r13, 39(%r14)
	testb	$1, %r13b
	je	.L1543
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1578
	testb	$24, %al
	je	.L1543
.L1581:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1579
.L1543:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1574:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1580
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1578:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1581
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1576:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1582
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1572:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1583
	jmp	.L1546
	.p2align 4,,10
	.p2align 3
.L1528:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L1584
.L1530:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L1529
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1546
	.p2align 4,,10
	.p2align 3
.L1575:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1577:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1579:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1584:
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1530
	.cfi_endproc
.LFE24107:
	.size	_ZN2v88internal7Factory14NewWithContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEE, .-_ZN2v88internal7Factory14NewWithContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEENS2_INS0_10JSReceiverEEE
	.section	.text._ZN2v88internal7Factory15NewBlockContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory15NewBlockContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE
	.type	_ZN2v88internal7Factory15NewBlockContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE, @function
_ZN2v88internal7Factory15NewBlockContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE:
.LFB24108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-64(%rbp), %rdi
	subq	$56, %rsp
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo13ContextLengthEv@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	xorl	%edx, %edx
	leal	16(,%rax,8), %r14d
	movq	384(%rbx), %r12
	leaq	37592(%rbx), %rdi
	movl	%eax, %r13d
	movl	%r14d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1586
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L1587:
	salq	$32, %r13
	movq	%r13, 7(%rsi)
	cmpl	$48, %r14d
	jg	.L1632
.L1589:
	movq	(%r12), %r14
	movq	(%r15), %r13
	movq	%r13, 15(%r14)
	leaq	15(%r14), %rsi
	testb	$1, %r13b
	je	.L1605
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L1633
	testb	$24, %al
	je	.L1605
.L1645:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1634
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	-72(%rbp), %rax
	movq	(%r12), %r14
	movq	(%rax), %r13
	leaq	23(%r14), %rsi
	movq	%r13, 23(%r14)
	testb	$1, %r13b
	je	.L1604
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -80(%rbp)
	testl	$262144, %eax
	jne	.L1635
	testb	$24, %al
	je	.L1604
.L1642:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1636
	.p2align 4,,10
	.p2align 3
.L1604:
	movq	(%r12), %r14
	movq	96(%rbx), %r13
	movq	%r13, 31(%r14)
	leaq	31(%r14), %rsi
	testb	$1, %r13b
	je	.L1603
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1637
	testb	$24, %al
	je	.L1603
.L1644:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1638
	.p2align 4,,10
	.p2align 3
.L1603:
	movq	-72(%rbp), %rax
	movq	(%r12), %r14
	movq	(%rax), %rax
	leaq	39(%r14), %r15
	movq	39(%rax), %r13
	movq	%r13, 39(%r14)
	testb	$1, %r13b
	je	.L1602
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1639
	testb	$24, %al
	je	.L1602
.L1643:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1640
	.p2align 4,,10
	.p2align 3
.L1602:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1641
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1635:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1642
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1639:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1643
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-80(%rbp), %rsi
	testb	$24, %al
	jne	.L1644
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1633:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1645
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1586:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L1646
.L1588:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1632:
	movq	(%r12), %rdx
	movslq	%r14d, %rcx
	movq	88(%rbx), %rax
	subq	$48, %rcx
	shrq	$3, %rcx
	leaq	47(%rdx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	jmp	.L1589
	.p2align 4,,10
	.p2align 3
.L1634:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1636:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1638:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1640:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L1646:
	movq	%rbx, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1588
.L1641:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24108:
	.size	_ZN2v88internal7Factory15NewBlockContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE, .-_ZN2v88internal7Factory15NewBlockContextENS0_6HandleINS0_7ContextEEENS2_INS0_9ScopeInfoEEE
	.section	.text._ZN2v88internal7Factory17NewBuiltinContextENS0_6HandleINS0_13NativeContextEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewBuiltinContextENS0_6HandleINS0_13NativeContextEEEi
	.type	_ZN2v88internal7Factory17NewBuiltinContextENS0_6HandleINS0_13NativeContextEEEi, @function
_ZN2v88internal7Factory17NewBuiltinContextENS0_6HandleINS0_13NativeContextEEEi:
.LFB24109:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leal	16(,%rdx,8), %r14d
	pushq	%r13
	movl	%r14d, %esi
	.cfi_offset 13, -40
	movl	%edx, %r13d
	xorl	%edx, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$24, %rsp
	movq	-37368(%rdi), %r12
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1648
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L1649:
	salq	$32, %r13
	movq	%r13, 7(%rsi)
	cmpl	$48, %r14d
	jg	.L1693
.L1651:
	movq	(%r12), %r14
	movq	280(%rbx), %r13
	movq	%r13, 15(%r14)
	leaq	15(%r14), %rsi
	testb	$1, %r13b
	je	.L1667
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L1694
	testb	$24, %al
	je	.L1667
.L1705:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1695
	.p2align 4,,10
	.p2align 3
.L1667:
	movq	(%r12), %r14
	movq	(%r15), %r13
	movq	%r13, 23(%r14)
	leaq	23(%r14), %rsi
	testb	$1, %r13b
	je	.L1666
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L1696
	testb	$24, %al
	je	.L1666
.L1704:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1697
	.p2align 4,,10
	.p2align 3
.L1666:
	movq	(%r12), %r14
	movq	96(%rbx), %r13
	movq	%r13, 31(%r14)
	leaq	31(%r14), %rsi
	testb	$1, %r13b
	je	.L1665
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1698
	testb	$24, %al
	je	.L1665
.L1703:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1699
	.p2align 4,,10
	.p2align 3
.L1665:
	movq	(%r12), %r14
	movq	(%r15), %r13
	movq	%r13, 39(%r14)
	leaq	39(%r14), %r15
	testb	$1, %r13b
	je	.L1664
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1700
	testb	$24, %al
	je	.L1664
.L1702:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1701
.L1664:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1700:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1702
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1698:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1703
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1704
	jmp	.L1666
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1705
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1648:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L1706
.L1650:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L1649
	.p2align 4,,10
	.p2align 3
.L1693:
	movq	(%r12), %rdx
	movslq	%r14d, %rcx
	movq	88(%rbx), %rax
	subq	$48, %rcx
	shrq	$3, %rcx
	leaq	47(%rdx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1695:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1697:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1666
	.p2align 4,,10
	.p2align 3
.L1699:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1701:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1650
	.cfi_endproc
.LFE24109:
	.size	_ZN2v88internal7Factory17NewBuiltinContextENS0_6HandleINS0_13NativeContextEEEi, .-_ZN2v88internal7Factory17NewBuiltinContextENS0_6HandleINS0_13NativeContextEEEi
	.section	.text._ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE:
.LFB24110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	%si, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	call	_ZN2v88internal3Map12GetStructMapEPNS0_7IsolateENS0_12InstanceTypeE@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	%r14d, %edx
	movzbl	7(%rax), %ebx
	leaq	37592(%r12), %rdi
	movq	%rax, %r13
	sall	$3, %ebx
	movl	%ebx, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r13, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1708
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1709:
	movq	%rsi, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	movq	-37504(%rdx), %rcx
	cmpl	$8, %ebx
	jle	.L1714
	subl	$9, %ebx
	leaq	7(%rsi), %rdx
	shrl	$3, %ebx
	leaq	15(%rsi,%rbx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	%rcx, (%rdx)
	addq	$8, %rdx
	cmpq	%rdx, %rsi
	jne	.L1713
.L1714:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1708:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1716
.L1710:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1709
	.p2align 4,,10
	.p2align 3
.L1716:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L1710
	.cfi_endproc
.LFE24110:
	.size	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE, .-_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory16NewPrototypeInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory16NewPrototypeInfoEv
	.type	_ZN2v88internal7Factory16NewPrototypeInfoEv, @function
_ZN2v88internal7Factory16NewPrototypeInfoEv:
.LFB24017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	$95, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	movq	(%rax), %r14
	movq	%rax, %r12
	movq	%r13, 15(%r14)
	testb	$1, %r13b
	je	.L1725
	movq	%r13, %r15
	leaq	15(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1739
	testb	$24, %al
	je	.L1725
.L1744:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1740
	.p2align 4,,10
	.p2align 3
.L1725:
	movabsq	$-4294967296, %rdx
	movq	(%r12), %rax
	movq	%rdx, 23(%rax)
	movq	(%r12), %rax
	movq	$0, 47(%rax)
	movq	(%r12), %r14
	movq	88(%rbx), %r13
	movq	%r13, 7(%r14)
	leaq	7(%r14), %r15
	testb	$1, %r13b
	je	.L1724
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1741
	testb	$24, %al
	je	.L1724
.L1743:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1742
.L1724:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1741:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1743
	jmp	.L1724
	.p2align 4,,10
	.p2align 3
.L1739:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1744
	jmp	.L1725
	.p2align 4,,10
	.p2align 3
.L1740:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1725
	.p2align 4,,10
	.p2align 3
.L1742:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1724
	.cfi_endproc
.LFE24017:
	.size	_ZN2v88internal7Factory16NewPrototypeInfoEv, .-_ZN2v88internal7Factory16NewPrototypeInfoEv
	.section	.text._ZN2v88internal7Factory12NewEnumCacheENS0_6HandleINS0_10FixedArrayEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory12NewEnumCacheENS0_6HandleINS0_10FixedArrayEEES4_
	.type	_ZN2v88internal7Factory12NewEnumCacheENS0_6HandleINS0_10FixedArrayEEES4_, @function
_ZN2v88internal7Factory12NewEnumCacheENS0_6HandleINS0_10FixedArrayEEES4_:
.LFB24018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$87, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$1, %edx
	subq	$24, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	0(%r13), %r13
	movq	(%rax), %r14
	movq	%rax, %r12
	movq	%r13, 7(%r14)
	testb	$1, %r13b
	je	.L1753
	movq	%r13, %r15
	leaq	7(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1767
	testb	$24, %al
	je	.L1753
.L1772:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1768
	.p2align 4,,10
	.p2align 3
.L1753:
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %r13b
	je	.L1752
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1769
	testb	$24, %al
	je	.L1752
.L1771:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1770
.L1752:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1769:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1771
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1767:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1772
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1768:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1770:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1752
	.cfi_endproc
.LFE24018:
	.size	_ZN2v88internal7Factory12NewEnumCacheENS0_6HandleINS0_10FixedArrayEEES4_, .-_ZN2v88internal7Factory12NewEnumCacheENS0_6HandleINS0_10FixedArrayEEES4_
	.section	.text._ZN2v88internal7Factory9NewTuple2ENS0_6HandleINS0_6ObjectEEES4_NS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory9NewTuple2ENS0_6HandleINS0_6ObjectEEES4_NS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory9NewTuple2ENS0_6HandleINS0_6ObjectEEES4_NS0_14AllocationTypeE, @function
_ZN2v88internal7Factory9NewTuple2ENS0_6HandleINS0_6ObjectEEES4_NS0_14AllocationTypeE:
.LFB24019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$102, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	%ecx, %edx
	subq	$24, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	0(%r13), %r13
	movq	(%rax), %r14
	movq	%rax, %r12
	movq	%r13, 7(%r14)
	testb	$1, %r13b
	je	.L1781
	movq	%r13, %r15
	leaq	7(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1795
	testb	$24, %al
	je	.L1781
.L1800:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1796
	.p2align 4,,10
	.p2align 3
.L1781:
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %r13b
	je	.L1780
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1797
	testb	$24, %al
	je	.L1780
.L1799:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1798
.L1780:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1797:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1799
	jmp	.L1780
	.p2align 4,,10
	.p2align 3
.L1795:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1800
	jmp	.L1781
	.p2align 4,,10
	.p2align 3
.L1796:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1781
	.p2align 4,,10
	.p2align 3
.L1798:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1780
	.cfi_endproc
.LFE24019:
	.size	_ZN2v88internal7Factory9NewTuple2ENS0_6HandleINS0_6ObjectEEES4_NS0_14AllocationTypeE, .-_ZN2v88internal7Factory9NewTuple2ENS0_6HandleINS0_6ObjectEEES4_NS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory9NewTuple3ENS0_6HandleINS0_6ObjectEEES4_S4_NS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory9NewTuple3ENS0_6HandleINS0_6ObjectEEES4_S4_NS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory9NewTuple3ENS0_6HandleINS0_6ObjectEEES4_S4_NS0_14AllocationTypeE, @function
_ZN2v88internal7Factory9NewTuple3ENS0_6HandleINS0_6ObjectEEES4_S4_NS0_14AllocationTypeE:
.LFB24020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movl	%r8d, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$103, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	0(%r13), %r13
	movq	(%rax), %r15
	movq	%rax, %r12
	movq	%r13, 7(%r15)
	testb	$1, %r13b
	je	.L1813
	movq	%r13, %rcx
	leaq	7(%r15), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L1833
	testb	$24, %al
	je	.L1813
.L1839:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1834
	.p2align 4,,10
	.p2align 3
.L1813:
	movq	(%r12), %r15
	movq	(%r14), %r13
	movq	%r13, 15(%r15)
	leaq	15(%r15), %rsi
	testb	$1, %r13b
	je	.L1812
	movq	%r13, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L1835
	testb	$24, %al
	je	.L1812
.L1841:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1836
	.p2align 4,,10
	.p2align 3
.L1812:
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 23(%r14)
	leaq	23(%r14), %r15
	testb	$1, %r13b
	je	.L1811
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1837
	testb	$24, %al
	je	.L1811
.L1840:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1838
.L1811:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1833:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L1839
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1837:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1840
	jmp	.L1811
	.p2align 4,,10
	.p2align 3
.L1835:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1841
	jmp	.L1812
	.p2align 4,,10
	.p2align 3
.L1834:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1836:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1812
	.p2align 4,,10
	.p2align 3
.L1838:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1811
	.cfi_endproc
.LFE24020:
	.size	_ZN2v88internal7Factory9NewTuple3ENS0_6HandleINS0_6ObjectEEES4_S4_NS0_14AllocationTypeE, .-_ZN2v88internal7Factory9NewTuple3ENS0_6HandleINS0_6ObjectEEES4_S4_NS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory30NewArrayBoilerplateDescriptionENS0_12ElementsKindENS0_6HandleINS0_14FixedArrayBaseEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory30NewArrayBoilerplateDescriptionENS0_12ElementsKindENS0_6HandleINS0_14FixedArrayBaseEEE
	.type	_ZN2v88internal7Factory30NewArrayBoilerplateDescriptionENS0_12ElementsKindENS0_6HandleINS0_14FixedArrayBaseEEE, @function
_ZN2v88internal7Factory30NewArrayBoilerplateDescriptionENS0_12ElementsKindENS0_6HandleINS0_14FixedArrayBaseEEE:
.LFB24021:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$1, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movzbl	%sil, %ebx
	movl	$82, %esi
	salq	$32, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%rbx, 7(%rax)
	movq	(%r12), %r14
	movq	0(%r13), %r13
	movq	%r13, 15(%r14)
	testb	$1, %r13b
	je	.L1846
	movq	%r13, %rbx
	leaq	15(%r14), %r15
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1854
	testb	$24, %al
	je	.L1846
.L1856:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1855
.L1846:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1854:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1856
	jmp	.L1846
	.p2align 4,,10
	.p2align 3
.L1855:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1846
	.cfi_endproc
.LFE24021:
	.size	_ZN2v88internal7Factory30NewArrayBoilerplateDescriptionENS0_12ElementsKindENS0_6HandleINS0_14FixedArrayBaseEEE, .-_ZN2v88internal7Factory30NewArrayBoilerplateDescriptionENS0_12ElementsKindENS0_6HandleINS0_14FixedArrayBaseEEE
	.section	.text._ZN2v88internal7Factory28NewTemplateObjectDescriptionENS0_6HandleINS0_10FixedArrayEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory28NewTemplateObjectDescriptionENS0_6HandleINS0_10FixedArrayEEES4_
	.type	_ZN2v88internal7Factory28NewTemplateObjectDescriptionENS0_6HandleINS0_10FixedArrayEEES4_, @function
_ZN2v88internal7Factory28NewTemplateObjectDescriptionENS0_6HandleINS0_10FixedArrayEEES4_:
.LFB24022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$101, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$1, %edx
	subq	$24, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	0(%r13), %r13
	movq	(%rax), %r14
	movq	%rax, %r12
	movq	%r13, 7(%r14)
	testb	$1, %r13b
	je	.L1865
	movq	%r13, %r15
	leaq	7(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L1879
	testb	$24, %al
	je	.L1865
.L1884:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1880
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %r13b
	je	.L1864
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1881
	testb	$24, %al
	je	.L1864
.L1883:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1882
.L1864:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1881:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1883
	jmp	.L1864
	.p2align 4,,10
	.p2align 3
.L1879:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L1884
	jmp	.L1865
	.p2align 4,,10
	.p2align 3
.L1880:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1865
	.p2align 4,,10
	.p2align 3
.L1882:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1864
	.cfi_endproc
.LFE24022:
	.size	_ZN2v88internal7Factory28NewTemplateObjectDescriptionENS0_6HandleINS0_10FixedArrayEEES4_, .-_ZN2v88internal7Factory28NewTemplateObjectDescriptionENS0_6HandleINS0_10FixedArrayEEES4_
	.section	.text._ZN2v88internal7Factory15NewAccessorPairEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory15NewAccessorPairEv
	.type	_ZN2v88internal7Factory15NewAccessorPairEv, @function
_ZN2v88internal7Factory15NewAccessorPairEv:
.LFB24049:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	$79, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	104(%rbx), %rcx
	movq	(%rax), %rdx
	movq	%rcx, 7(%rdx)
	movq	104(%rbx), %rcx
	movq	(%rax), %rdx
	movq	%rcx, 15(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24049:
	.size	_ZN2v88internal7Factory15NewAccessorPairEv, .-_ZN2v88internal7Factory15NewAccessorPairEv
	.section	.text._ZN2v88internal7Factory24NewAliasedArgumentsEntryEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory24NewAliasedArgumentsEntryEi
	.type	_ZN2v88internal7Factory24NewAliasedArgumentsEntryEi, @function
_ZN2v88internal7Factory24NewAliasedArgumentsEntryEi:
.LFB24111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	movl	$80, %esi
	salq	$32, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	(%rax), %rdx
	movq	%rbx, 7(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24111:
	.size	_ZN2v88internal7Factory24NewAliasedArgumentsEntryEi, .-_ZN2v88internal7Factory24NewAliasedArgumentsEntryEi
	.section	.text._ZN2v88internal7Factory15NewAccessorInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory15NewAccessorInfoEv
	.type	_ZN2v88internal7Factory15NewAccessorInfoEv, @function
_ZN2v88internal7Factory15NewAccessorInfoEv:
.LFB24112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movl	$78, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	128(%rbx), %r13
	movq	(%rax), %r14
	movq	%rax, %r12
	movq	%r13, 7(%r14)
	testb	$1, %r13b
	je	.L1905
	movq	%r13, %rbx
	leaq	7(%r14), %r15
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1931
	testb	$24, %al
	je	.L1905
.L1940:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1932
	.p2align 4,,10
	.p2align 3
.L1905:
	movq	(%r12), %rax
	movq	$0, 15(%rax)
	movq	(%r12), %rdx
	movslq	19(%rdx), %rax
	orl	$8, %eax
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	(%r12), %rdx
	movslq	19(%rdx), %rax
	andb	$-15, %ah
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	(%r12), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	movq	%r13, 39(%r14)
	leaq	39(%r14), %r15
	testb	$1, %r13b
	je	.L1904
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1933
	testb	$24, %al
	je	.L1904
.L1939:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1934
	.p2align 4,,10
	.p2align 3
.L1904:
	movq	(%r12), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	movq	%r13, 31(%r14)
	leaq	31(%r14), %r15
	testb	$1, %r13b
	je	.L1903
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1935
	testb	$24, %al
	je	.L1903
.L1942:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1936
	.p2align 4,,10
	.p2align 3
.L1903:
	movq	(%r12), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	movq	%r13, 47(%r14)
	leaq	47(%r14), %r15
	testb	$1, %r13b
	je	.L1902
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1937
	testb	$24, %al
	je	.L1902
.L1941:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1938
.L1902:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1933:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1939
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L1931:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1940
	jmp	.L1905
	.p2align 4,,10
	.p2align 3
.L1937:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1941
	jmp	.L1902
	.p2align 4,,10
	.p2align 3
.L1935:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1942
	jmp	.L1903
	.p2align 4,,10
	.p2align 3
.L1932:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1905
	.p2align 4,,10
	.p2align 3
.L1934:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L1936:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1903
	.p2align 4,,10
	.p2align 3
.L1938:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1902
	.cfi_endproc
.LFE24112:
	.size	_ZN2v88internal7Factory15NewAccessorInfoEv, .-_ZN2v88internal7Factory15NewAccessorInfoEv
	.section	.rodata._ZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeE.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"disabled-by-default-v8.compile"
	.section	.rodata._ZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeE.str1.1,"aMS",@progbits,1
.LC13:
	.string	"Script"
	.section	.text._ZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeE:
.LFB24114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$96, %esi
	pushq	%r14
	.cfi_offset 14, -32
	leaq	37592(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	movl	%ecx, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	(%r15), %r15
	movq	(%rax), %rdi
	movq	%rax, %r12
	movq	%r15, %rdx
	movq	%r15, 7(%rdi)
	leaq	7(%rdi), %rsi
	movq	%rdi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	jne	.L1993
.L1944:
	movq	(%r12), %rdi
	movq	88(%rbx), %r15
	movq	%r15, 15(%rdi)
	leaq	15(%rdi), %rsi
	movq	%r15, %rdx
	movq	%rdi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	jne	.L1994
.L1946:
	movslq	%r13d, %rax
	movq	%r13, %rdx
	movq	%rax, -120(%rbp)
	movq	(%r12), %rax
	salq	$32, %rdx
	movq	%rdx, 63(%rax)
	movq	(%r12), %rax
	movq	$0, 23(%rax)
	movq	(%r12), %rax
	movq	$0, 31(%rax)
	movq	(%r12), %r15
	movq	88(%rbx), %rdx
	movq	%rdx, 39(%r15)
	leaq	39(%r15), %rsi
	movq	%r15, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-104(%rbp), %rdx
	testb	$1, %dl
	jne	.L1995
.L1948:
	movabsq	$8589934592, %rdx
	movq	(%r12), %rax
	movq	%rdx, 47(%rax)
	movq	(%r12), %r15
	movq	88(%rbx), %rdx
	movq	%rdx, 55(%r15)
	leaq	55(%r15), %rsi
	movq	%r15, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-104(%rbp), %rdx
	testb	$1, %dl
	je	.L1950
	movq	-112(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L1950:
	movq	(%r12), %r15
	movq	88(%rbx), %rdx
	movq	%rdx, 71(%r15)
	leaq	71(%r15), %rsi
	movq	%r15, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-104(%rbp), %rdx
	testb	$1, %dl
	jne	.L1996
.L1951:
	movq	(%r12), %rax
	movq	$0, 79(%rax)
	movq	(%r12), %rax
	movq	1072(%rbx), %rdx
	movq	%rdx, 87(%rax)
	movq	(%r12), %rax
	movq	$0, 95(%rax)
	movq	(%r12), %r15
	movq	288(%rbx), %rdx
	movq	%rdx, 119(%r15)
	leaq	119(%r15), %rsi
	movq	%r15, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-104(%rbp), %rdx
	testb	$1, %dl
	jne	.L1997
.L1953:
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	leaq	4680(%rbx), %rsi
	movq	%r12, -88(%rbp)
	leaq	-96(%rbp), %rdx
	movl	$0, -96(%rbp)
	call	_ZN2v88internal13WeakArrayList6AppendEPNS0_7IsolateENS0_6HandleIS1_EERKNS0_17MaybeObjectHandleENS0_14AllocationTypeE@PLT
	movq	(%rax), %rax
	movq	%rax, -32912(%r14)
	movq	41016(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L1998
.L1955:
	movq	_ZZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeEE29trace_event_unique_atomic1684(%rip), %rdx
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L1999
.L1957:
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L2000
.L1959:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2001
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1995:
	.cfi_restore_state
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1948
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1948
	movq	-112(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1948
	.p2align 4,,10
	.p2align 3
.L1997:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1953
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1953
	movq	-112(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1953
	.p2align 4,,10
	.p2align 3
.L1996:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1951
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1951
	movq	-112(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1994:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1946
	movq	-104(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1946
	movq	-112(%rbp), %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1946
	.p2align 4,,10
	.p2align 3
.L1993:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1944
	movq	-104(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1944
	movq	-112(%rbp), %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1944
	.p2align 4,,10
	.p2align 3
.L2000:
	pxor	%xmm0, %xmm0
	movq	_ZN2v88internal6Script11kTraceScopeE(%rip), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2002
.L1960:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1961
	movq	(%rdi), %rax
	call	*8(%rax)
.L1961:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1959
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1959
	.p2align 4,,10
	.p2align 3
.L1999:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2003
.L1958:
	movq	%r13, _ZZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeEE29trace_event_unique_atomic1684(%rip)
	jmp	.L1957
	.p2align 4,,10
	.p2align 3
.L1998:
	movl	%r13d, %edx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi@PLT
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L2003:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L2002:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	movq	-120(%rbp), %r9
	movq	%rbx, %r8
	pushq	$2
	leaq	.LC13(%rip), %rcx
	movl	$78, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1960
.L2001:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24114:
	.size	_ZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory9NewScriptENS0_6HandleINS0_6StringEEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory9NewScriptENS0_6HandleINS0_6StringEEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory9NewScriptENS0_6HandleINS0_6StringEEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory9NewScriptENS0_6HandleINS0_6StringEEENS0_14AllocationTypeE:
.LFB24113:
	.cfi_startproc
	endbr64
	movslq	4804(%rdi), %r8
	movl	%edx, %ecx
	movabsq	$4294967296, %rax
	movl	$1, %edx
	cmpq	$2147483647, %r8
	je	.L2005
	leal	1(%r8), %edx
	movq	%rdx, %rax
	salq	$32, %rax
.L2005:
	movq	%rax, 4800(%rdi)
	jmp	_ZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeE
	.cfi_endproc
.LFE24113:
	.size	_ZN2v88internal7Factory9NewScriptENS0_6HandleINS0_6StringEEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory9NewScriptENS0_6HandleINS0_6StringEEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory11CloneScriptENS0_6HandleINS0_6ScriptEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory11CloneScriptENS0_6HandleINS0_6ScriptEEE
	.type	_ZN2v88internal7Factory11CloneScriptENS0_6HandleINS0_6ScriptEEE, @function
_ZN2v88internal7Factory11CloneScriptENS0_6HandleINS0_6ScriptEEE:
.LFB24115:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	37592(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movabsq	$4294967296, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	4804(%rdi), %rax
	movl	$1, -92(%rbp)
	cmpq	$2147483647, %rax
	je	.L2009
	addl	$1, %eax
	movl	%eax, -92(%rbp)
	movq	%rax, %r13
	salq	$32, %r13
.L2009:
	movq	%r13, 4800(%rbx)
	movl	$1, %edx
	movl	$96, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	(%rax), %rdi
	movq	%rax, %r12
	movq	(%r14), %rax
	movq	7(%rax), %rdx
	leaq	7(%rdi), %rsi
	movq	%rdx, 7(%rdi)
	movq	%rdx, -88(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-88(%rbp), %rdx
	testb	$1, %dl
	jne	.L2049
.L2010:
	movq	(%r14), %rax
	movq	(%r12), %rdi
	movq	15(%rax), %rdx
	leaq	15(%rdi), %rsi
	movq	%rdx, 15(%rdi)
	movq	%rdx, -88(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-88(%rbp), %rdx
	testb	$1, %dl
	jne	.L2050
.L2012:
	movq	(%r12), %rax
	movq	%r13, 63(%rax)
	movq	(%r14), %rax
	movq	(%r12), %rdx
	movslq	27(%rax), %rax
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	movq	(%r14), %rax
	movq	(%r12), %rdx
	movslq	35(%rax), %rax
	salq	$32, %rax
	movq	%rax, 31(%rdx)
	movq	(%r14), %rax
	movq	(%r12), %rdi
	movq	39(%rax), %r13
	leaq	39(%rdi), %rsi
	movq	%r13, 39(%rdi)
	movq	%r13, %rdx
	movq	%rsi, -104(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L2051
.L2014:
	movq	(%r14), %rax
	movq	(%r12), %rdx
	movslq	51(%rax), %rax
	salq	$32, %rax
	movq	%rax, 47(%rdx)
	movq	-37504(%r15), %r13
	movq	(%r12), %rdi
	movq	%r13, 55(%rdi)
	leaq	55(%rdi), %r15
	movq	%r13, %rdx
	movq	%rdi, -88(%rbp)
	movq	%r15, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L2052
.L2016:
	movq	(%r14), %rax
	movq	(%r12), %r15
	movq	71(%rax), %r13
	leaq	71(%r15), %rsi
	movq	%r15, %rdi
	movq	%r13, 71(%r15)
	movq	%r13, %rdx
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L2053
.L2018:
	movq	(%r12), %rax
	movq	1072(%rbx), %rdx
	movq	%rdx, 87(%rax)
	movq	(%r14), %rax
	movq	(%r12), %rdx
	movslq	83(%rax), %rax
	salq	$32, %rax
	movq	%rax, 79(%rdx)
	movq	(%r14), %rax
	movq	(%r12), %rdx
	movslq	99(%rax), %rax
	salq	$32, %rax
	movq	%rax, 95(%rdx)
	movq	(%r14), %rax
	movq	(%r12), %r15
	movq	119(%rax), %r13
	leaq	119(%r15), %rsi
	movq	%r15, %rdi
	movq	%r13, 119(%r15)
	movq	%r13, %rdx
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L2054
.L2020:
	movq	%rbx, %rdi
	leaq	-80(%rbp), %rdx
	movq	%r12, -72(%rbp)
	leaq	4680(%rbx), %rsi
	movl	$0, -80(%rbp)
	call	_ZN2v88internal13WeakArrayList8AddToEndEPNS0_7IsolateENS0_6HandleIS1_EERKNS0_17MaybeObjectHandleE@PLT
	movq	41016(%rbx), %r13
	movq	(%rax), %rax
	movq	%r13, %rdi
	movq	%rax, 4680(%rbx)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L2055
.L2022:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2056
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2049:
	.cfi_restore_state
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2010
	movq	-104(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2010
	movq	-112(%rbp), %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2010
	.p2align 4,,10
	.p2align 3
.L2054:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2020
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2020
	movq	-88(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2053:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2018
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2018
	movq	-88(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L2052:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2016
	movq	-88(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2016
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2016
	.p2align 4,,10
	.p2align 3
.L2051:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2014
	movq	-88(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2014
	movq	-104(%rbp), %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2014
	.p2align 4,,10
	.p2align 3
.L2050:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2012
	movq	-104(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2012
	movq	-112(%rbp), %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L2055:
	movl	-92(%rbp), %edx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger11ScriptEventENS1_15ScriptEventTypeEi@PLT
	jmp	.L2022
.L2056:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24115:
	.size	_ZN2v88internal7Factory11CloneScriptENS0_6HandleINS0_6ScriptEEE, .-_ZN2v88internal7Factory11CloneScriptENS0_6HandleINS0_6ScriptEEE
	.section	.text._ZN2v88internal7Factory15NewCallableTaskENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory15NewCallableTaskENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7ContextEEE
	.type	_ZN2v88internal7Factory15NewCallableTaskENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7ContextEEE, @function
_ZN2v88internal7Factory15NewCallableTaskENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7ContextEEE:
.LFB24116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$110, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$24, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	0(%r13), %r13
	movq	(%rax), %r14
	movq	%rax, %r12
	movq	%r13, 7(%r14)
	testb	$1, %r13b
	je	.L2065
	movq	%r13, %r15
	leaq	7(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L2079
	testb	$24, %al
	je	.L2065
.L2084:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2080
	.p2align 4,,10
	.p2align 3
.L2065:
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %r13b
	je	.L2064
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2081
	testb	$24, %al
	je	.L2064
.L2083:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2082
.L2064:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2081:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L2083
	jmp	.L2064
	.p2align 4,,10
	.p2align 3
.L2079:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L2084
	jmp	.L2065
	.p2align 4,,10
	.p2align 3
.L2080:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2065
	.p2align 4,,10
	.p2align 3
.L2082:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2064
	.cfi_endproc
.LFE24116:
	.size	_ZN2v88internal7Factory15NewCallableTaskENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7ContextEEE, .-_ZN2v88internal7Factory15NewCallableTaskENS0_6HandleINS0_10JSReceiverEEENS2_INS0_7ContextEEE
	.section	.text._ZN2v88internal7Factory15NewCallbackTaskENS0_6HandleINS0_7ForeignEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory15NewCallbackTaskENS0_6HandleINS0_7ForeignEEES4_
	.type	_ZN2v88internal7Factory15NewCallbackTaskENS0_6HandleINS0_7ForeignEEES4_, @function
_ZN2v88internal7Factory15NewCallbackTaskENS0_6HandleINS0_7ForeignEEES4_:
.LFB24117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$111, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	xorl	%edx, %edx
	subq	$24, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	0(%r13), %r13
	movq	(%rax), %r14
	movq	%rax, %r12
	movq	%r13, 7(%r14)
	testb	$1, %r13b
	je	.L2093
	movq	%r13, %r15
	leaq	7(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L2107
	testb	$24, %al
	je	.L2093
.L2112:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2108
	.p2align 4,,10
	.p2align 3
.L2093:
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %r13b
	je	.L2092
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2109
	testb	$24, %al
	je	.L2092
.L2111:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2110
.L2092:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2109:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L2111
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2107:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L2112
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2108:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2110:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2092
	.cfi_endproc
.LFE24117:
	.size	_ZN2v88internal7Factory15NewCallbackTaskENS0_6HandleINS0_7ForeignEEES4_, .-_ZN2v88internal7Factory15NewCallbackTaskENS0_6HandleINS0_7ForeignEEES4_
	.section	.text._ZN2v88internal7Factory32NewPromiseResolveThenableJobTaskENS0_6HandleINS0_9JSPromiseEEENS2_INS0_10JSReceiverEEES6_NS2_INS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory32NewPromiseResolveThenableJobTaskENS0_6HandleINS0_9JSPromiseEEENS2_INS0_10JSReceiverEEES6_NS2_INS0_7ContextEEE
	.type	_ZN2v88internal7Factory32NewPromiseResolveThenableJobTaskENS0_6HandleINS0_9JSPromiseEEENS2_INS0_10JSReceiverEEES6_NS2_INS0_7ContextEEE, @function
_ZN2v88internal7Factory32NewPromiseResolveThenableJobTaskENS0_6HandleINS0_9JSPromiseEEENS2_INS0_10JSReceiverEEES6_NS2_INS0_7ContextEEE:
.LFB24118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	xorl	%edx, %edx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$114, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$40, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	0(%r13), %r13
	movq	(%rax), %rdi
	movq	%rax, %r12
	movq	%r13, 15(%rdi)
	testb	$1, %r13b
	je	.L2129
	movq	%r13, %rcx
	leaq	15(%rdi), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L2155
	testb	$24, %al
	je	.L2129
.L2164:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2156
	.p2align 4,,10
	.p2align 3
.L2129:
	movq	(%r12), %rdi
	movq	(%r15), %r13
	movq	%r13, 23(%rdi)
	leaq	23(%rdi), %rsi
	testb	$1, %r13b
	je	.L2128
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L2157
	testb	$24, %al
	je	.L2128
.L2163:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2158
	.p2align 4,,10
	.p2align 3
.L2128:
	movq	(%r12), %r15
	movq	(%r14), %r13
	movq	%r13, 31(%r15)
	leaq	31(%r15), %rsi
	testb	$1, %r13b
	je	.L2127
	movq	%r13, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L2159
	testb	$24, %al
	je	.L2127
.L2166:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2160
	.p2align 4,,10
	.p2align 3
.L2127:
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 7(%r14)
	leaq	7(%r14), %r15
	testb	$1, %r13b
	je	.L2126
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2161
	testb	$24, %al
	je	.L2126
.L2165:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2162
.L2126:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2157:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	testb	$24, %al
	jne	.L2163
	jmp	.L2128
	.p2align 4,,10
	.p2align 3
.L2155:
	movq	%r13, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L2164
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2161:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L2165
	jmp	.L2126
	.p2align 4,,10
	.p2align 3
.L2159:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L2166
	jmp	.L2127
	.p2align 4,,10
	.p2align 3
.L2156:
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2158:
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2128
	.p2align 4,,10
	.p2align 3
.L2160:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2127
	.p2align 4,,10
	.p2align 3
.L2162:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2126
	.cfi_endproc
.LFE24118:
	.size	_ZN2v88internal7Factory32NewPromiseResolveThenableJobTaskENS0_6HandleINS0_9JSPromiseEEENS2_INS0_10JSReceiverEEES6_NS2_INS0_7ContextEEE, .-_ZN2v88internal7Factory32NewPromiseResolveThenableJobTaskENS0_6HandleINS0_9JSPromiseEEENS2_INS0_10JSReceiverEEES6_NS2_INS0_7ContextEEE
	.section	.text._ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE:
.LFB24119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$24, %rsp
	movq	-37344(%rdi), %r13
	movzbl	7(%r13), %esi
	sall	$3, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r13, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2168
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%r12, 7(%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2168:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2172
.L2170:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movq	%r12, 7(%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2172:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L2170
	.cfi_endproc
.LFE24119:
	.size	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE, .-_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE:
.LFB24120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	37592(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	cmpl	$1073741808, %esi
	jg	.L2179
.L2174:
	leal	23(%rbx), %esi
	movq	%r15, %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	144(%r12), %r14
	andl	$-8, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r14, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2175
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L2176:
	salq	$32, %rbx
	movq	%rbx, 7(%rsi)
	xorl	%esi, %esi
	movq	0(%r13), %rcx
	movslq	11(%rcx), %rdx
	leal	16(%rdx), %eax
	addl	$23, %edx
	andl	$-8, %edx
	subl	%eax, %edx
	cltq
	movslq	%edx, %rdx
	leaq	-1(%rcx,%rax), %rdi
	call	memset@PLT
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2175:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L2180
.L2177:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L2176
	.p2align 4,,10
	.p2align 3
.L2179:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-56(%rbp), %edx
	jmp	.L2174
	.p2align 4,,10
	.p2align 3
.L2180:
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2177
	.cfi_endproc
.LFE24120:
	.size	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory16NewBytecodeArrayEiPKhiiNS0_6HandleINS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory16NewBytecodeArrayEiPKhiiNS0_6HandleINS0_10FixedArrayEEE
	.type	_ZN2v88internal7Factory16NewBytecodeArrayEiPKhiiNS0_6HandleINS0_10FixedArrayEEE, @function
_ZN2v88internal7Factory16NewBytecodeArrayEiPKhiiNS0_6HandleINS0_10FixedArrayEEE:
.LFB24121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$40, %rsp
	movq	%rdx, -72(%rbp)
	movl	%r8d, -60(%rbp)
	cmpl	$536870858, %esi
	jg	.L2210
.L2182:
	leal	61(%r12), %esi
	movq	464(%rbx), %r15
	xorl	%r8d, %r8d
	movl	$1, %ecx
	andl	$-8, %esi
	movl	$1, %edx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r15, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2183
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L2184:
	movq	%r12, %r10
	movslq	%r12d, %rax
	salq	$32, %r10
	movq	%rax, -56(%rbp)
	movq	%r10, 7(%rsi)
	movq	(%r15), %rax
	movl	-60(%rbp), %r12d
	movl	%r14d, 39(%rax)
	movq	(%r15), %rax
	sall	$3, %r12d
	movl	%r12d, 43(%rax)
	movq	(%r15), %rax
	movl	$0, 47(%rax)
	movq	(%r15), %rax
	movb	$0, 51(%rax)
	movq	(%r15), %rax
	movb	$0, 52(%rax)
	movq	0(%r13), %r12
	movq	(%r15), %r14
	movq	%r12, 15(%r14)
	leaq	15(%r14), %r13
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	jne	.L2211
.L2186:
	movq	(%r15), %r13
	movq	976(%rbx), %r12
	movq	%r12, 23(%r13)
	leaq	23(%r13), %r14
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	jne	.L2212
.L2188:
	movq	(%r15), %r13
	movq	88(%rbx), %r12
	movq	%r12, 31(%r13)
	leaq	31(%r13), %r14
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	jne	.L2213
.L2190:
	movq	-56(%rbp), %rax
	movq	(%r15), %rsi
	testq	%rax, %rax
	jne	.L2214
.L2192:
	movslq	11(%rsi), %rax
	leal	61(%rax), %edx
	leal	54(%rax), %ecx
	andl	$-8, %edx
	subl	%ecx, %edx
	movslq	%ecx, %rcx
	leaq	-1(%rsi,%rcx), %rdi
	movslq	%edx, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2211:
	.cfi_restore_state
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2186
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2186
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2186
	.p2align 4,,10
	.p2align 3
.L2214:
	leaq	53(%rsi), %rdi
	cmpq	$7, %rax
	ja	.L2193
	movq	-72(%rbp), %rdx
	leaq	53(%rsi,%rax), %r8
	subq	%rsi, %rdx
.L2194:
	movq	%rdi, %rax
	movzbl	-53(%rdi,%rdx), %ecx
	addq	$1, %rdi
	movb	%cl, (%rax)
	cmpq	%rdi, %r8
	jne	.L2194
	movq	(%r15), %rsi
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2213:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2190
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2190
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2190
	.p2align 4,,10
	.p2align 3
.L2212:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2188
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2188
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2183:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L2215
.L2185:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L2184
	.p2align 4,,10
	.p2align 3
.L2210:
	leaq	.LC0(%rip), %rsi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movq	-56(%rbp), %rdi
	jmp	.L2182
	.p2align 4,,10
	.p2align 3
.L2193:
	movq	-72(%rbp), %rsi
	movq	%rax, %rdx
	call	memcpy@PLT
	movq	(%r15), %rsi
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2215:
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L2185
	.cfi_endproc
.LFE24121:
	.size	_ZN2v88internal7Factory16NewBytecodeArrayEiPKhiiNS0_6HandleINS0_10FixedArrayEEE, .-_ZN2v88internal7Factory16NewBytecodeArrayEiPKhiiNS0_6HandleINS0_10FixedArrayEEE
	.section	.text._ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE:
.LFB24122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$16, %esi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$8, %rsp
	movq	-37360(%rdi), %r13
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r13, -1(%rax)
	movq	%rax, %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2217
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %r13
.L2218:
	movq	(%r14), %r14
	leaq	7(%r12), %r15
	movq	%r14, 7(%r12)
	testb	$1, %r14b
	je	.L2223
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2231
	testb	$24, %al
	je	.L2223
.L2233:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2232
.L2223:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2231:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L2233
	jmp	.L2223
	.p2align 4,,10
	.p2align 3
.L2217:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L2234
.L2219:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, 0(%r13)
	jmp	.L2218
	.p2align 4,,10
	.p2align 3
.L2232:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2223
	.p2align 4,,10
	.p2align 3
.L2234:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L2219
	.cfi_endproc
.LFE24122:
	.size	_ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal7Factory7NewCellENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal7Factory17NewNoClosuresCellENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewNoClosuresCellENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal7Factory17NewNoClosuresCellENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal7Factory17NewNoClosuresCellENS0_6HandleINS0_10HeapObjectEEE:
.LFB24123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$24, %esi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$8, %rsp
	movq	-37064(%rdi), %r13
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r13, -1(%rax)
	movq	%rax, %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2236
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %r13
.L2237:
	movq	(%r14), %r14
	leaq	7(%r12), %r15
	movq	%r14, 7(%r12)
	testb	$1, %r14b
	je	.L2244
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2252
	testb	$24, %al
	je	.L2244
.L2254:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2253
	.p2align 4,,10
	.p2align 3
.L2244:
	cmpb	$0, _ZN2v88internal29FLAG_lazy_feedback_allocationE(%rip)
	movq	0(%r13), %rdx
	movl	_ZN2v88internal21FLAG_interrupt_budgetE(%rip), %eax
	je	.L2243
	movl	_ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE(%rip), %eax
.L2243:
	movl	%eax, 15(%rdx)
	movq	0(%r13), %rax
	movl	$0, 19(%rax)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2252:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L2254
	jmp	.L2244
	.p2align 4,,10
	.p2align 3
.L2236:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L2255
.L2238:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, 0(%r13)
	jmp	.L2237
	.p2align 4,,10
	.p2align 3
.L2253:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2244
	.p2align 4,,10
	.p2align 3
.L2255:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L2238
	.cfi_endproc
.LFE24123:
	.size	_ZN2v88internal7Factory17NewNoClosuresCellENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal7Factory17NewNoClosuresCellENS0_6HandleINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal7Factory17NewOneClosureCellENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewOneClosureCellENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal7Factory17NewOneClosureCellENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal7Factory17NewOneClosureCellENS0_6HandleINS0_10HeapObjectEEE:
.LFB24124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$24, %esi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$8, %rsp
	movq	-37048(%rdi), %r13
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r13, -1(%rax)
	movq	%rax, %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2257
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %r13
.L2258:
	movq	(%r14), %r14
	leaq	7(%r12), %r15
	movq	%r14, 7(%r12)
	testb	$1, %r14b
	je	.L2265
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2273
	testb	$24, %al
	je	.L2265
.L2275:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2274
	.p2align 4,,10
	.p2align 3
.L2265:
	cmpb	$0, _ZN2v88internal29FLAG_lazy_feedback_allocationE(%rip)
	movq	0(%r13), %rdx
	movl	_ZN2v88internal21FLAG_interrupt_budgetE(%rip), %eax
	je	.L2264
	movl	_ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE(%rip), %eax
.L2264:
	movl	%eax, 15(%rdx)
	movq	0(%r13), %rax
	movl	$0, 19(%rax)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2273:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L2275
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2257:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L2276
.L2259:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, 0(%r13)
	jmp	.L2258
	.p2align 4,,10
	.p2align 3
.L2274:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2276:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L2259
	.cfi_endproc
.LFE24124:
	.size	_ZN2v88internal7Factory17NewOneClosureCellENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal7Factory17NewOneClosureCellENS0_6HandleINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal7Factory19NewManyClosuresCellENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory19NewManyClosuresCellENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal7Factory19NewManyClosuresCellENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal7Factory19NewManyClosuresCellENS0_6HandleINS0_10HeapObjectEEE:
.LFB24125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$24, %esi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$8, %rsp
	movq	-37088(%rdi), %r13
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r13, -1(%rax)
	movq	%rax, %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2278
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %r13
.L2279:
	movq	(%r14), %r14
	leaq	7(%r12), %r15
	movq	%r14, 7(%r12)
	testb	$1, %r14b
	je	.L2286
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2294
	testb	$24, %al
	je	.L2286
.L2296:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2295
	.p2align 4,,10
	.p2align 3
.L2286:
	cmpb	$0, _ZN2v88internal29FLAG_lazy_feedback_allocationE(%rip)
	movq	0(%r13), %rdx
	movl	_ZN2v88internal21FLAG_interrupt_budgetE(%rip), %eax
	je	.L2285
	movl	_ZN2v88internal42FLAG_budget_for_feedback_vector_allocationE(%rip), %eax
.L2285:
	movl	%eax, 15(%rdx)
	movq	0(%r13), %rax
	movl	$0, 19(%rax)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2294:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L2296
	jmp	.L2286
	.p2align 4,,10
	.p2align 3
.L2278:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L2297
.L2280:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, 0(%r13)
	jmp	.L2279
	.p2align 4,,10
	.p2align 3
.L2295:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2286
	.p2align 4,,10
	.p2align 3
.L2297:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L2280
	.cfi_endproc
.LFE24125:
	.size	_ZN2v88internal7Factory19NewManyClosuresCellENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal7Factory19NewManyClosuresCellENS0_6HandleINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE:
.LFB24126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$40, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$24, %rsp
	movq	-37352(%rdi), %r12
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2299
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L2300:
	movq	1072(%rbx), %rax
	movq	%rax, 31(%rsi)
	movq	(%r12), %rax
	movq	$0, 15(%rax)
	movq	(%r12), %r14
	movq	0(%r13), %r13
	movq	%r13, 7(%r14)
	leaq	7(%r14), %rsi
	testb	$1, %r13b
	je	.L2309
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L2323
	testb	$24, %al
	je	.L2309
.L2328:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2324
	.p2align 4,,10
	.p2align 3
.L2309:
	movq	(%r12), %r14
	movq	96(%rbx), %r13
	movq	%r13, 23(%r14)
	leaq	23(%r14), %r15
	testb	$1, %r13b
	je	.L2308
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L2325
	testb	$24, %al
	je	.L2308
.L2327:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2326
.L2308:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2325:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L2327
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2323:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L2328
	jmp	.L2309
	.p2align 4,,10
	.p2align 3
.L2299:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L2329
.L2301:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L2300
	.p2align 4,,10
	.p2align 3
.L2324:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2309
	.p2align 4,,10
	.p2align 3
.L2326:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2329:
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2301
	.cfi_endproc
.LFE24126:
	.size	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory18NewDescriptorArrayEiiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18NewDescriptorArrayEiiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory18NewDescriptorArrayEiiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory18NewDescriptorArrayEiiNS0_14AllocationTypeE:
.LFB24127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	movl	%ecx, %edx
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	movl	$1, %ecx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leal	1(%rsi,%r13), %eax
	leal	(%rax,%rax,2), %esi
	sall	$3, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	leaq	-48(%rbp), %rdi
	movl	%r13d, %r8d
	movl	%r12d, %ecx
	movq	480(%rbx), %rdx
	movq	%rdx, -1(%rax)
	movq	%rax, -48(%rbp)
	movq	960(%rbx), %rsi
	movq	88(%rbx), %rdx
	call	_ZN2v88internal15DescriptorArray10InitializeENS0_9EnumCacheENS0_10HeapObjectEii@PLT
	movq	41112(%rbx), %rdi
	movq	-48(%rbp), %rsi
	testq	%rdi, %rdi
	je	.L2331
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2332:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2336
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2331:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2337
.L2333:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L2332
	.p2align 4,,10
	.p2align 3
.L2337:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L2333
.L2336:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24127:
	.size	_ZN2v88internal7Factory18NewDescriptorArrayEiiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory18NewDescriptorArrayEiiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory18NewTransitionArrayEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18NewTransitionArrayEii
	.type	_ZN2v88internal7Factory18NewTransitionArrayEii, @function
_ZN2v88internal7Factory18NewTransitionArrayEii:
.LFB24128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leal	1(%rsi,%rdx), %r14d
	movl	$1, %edx
	pushq	%r13
	addl	%r14d, %r14d
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	leal	16(,%r14,8), %r12d
	pushq	%rbx
	movl	%r12d, %esi
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$24, %rsp
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r12d
	jle	.L2342
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L2367
.L2342:
	movq	264(%rbx), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2368
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L2344:
	movslq	%r14d, %rcx
	salq	$32, %r14
	movq	%r14, 7(%rsi)
	movq	(%r12), %rsi
	movq	88(%rbx), %rax
	leaq	15(%rsi), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	39656(%rbx), %rax
	cmpb	$0, 87(%rax)
	jne	.L2369
.L2346:
	movq	(%r12), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	movq	%rax, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %al
	je	.L2353
	cmpl	$3, %eax
	je	.L2353
	movq	%rax, %rdx
	andq	$-262144, %rax
	movq	%rax, %rbx
	movq	8(%rax), %rax
	andq	$-3, %rdx
	testl	$262144, %eax
	jne	.L2370
.L2351:
	testb	$24, %al
	je	.L2353
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2371
.L2353:
	movq	(%r12), %rax
	salq	$32, %r13
	movq	%r13, 23(%rax)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2368:
	.cfi_restore_state
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L2372
.L2345:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2369:
	movq	39608(%rbx), %r15
	movq	(%r12), %rdx
	movq	2192(%r15), %r14
	movq	8(%r14), %rbx
	cmpq	$64, %rbx
	je	.L2373
	leaq	1(%rbx), %rax
	movq	%rax, 8(%r14)
	movq	%rdx, 16(%r14,%rbx,8)
	jmp	.L2346
	.p2align 4,,10
	.p2align 3
.L2367:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2374:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L2342
.L2343:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L2374
	jmp	.L2342
	.p2align 4,,10
	.p2align 3
.L2370:
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-56(%rbp), %rdx
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2371:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2372:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2345
	.p2align 4,,10
	.p2align 3
.L2373:
	leaq	2832(%r15), %rdi
	movq	%rdx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	2872(%r15), %rax
	movq	%rax, (%r14)
	movq	%r14, 2872(%r15)
	movq	-56(%rbp), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movl	$528, %edi
	call	_Znwm@PLT
	movq	%rbx, %rcx
	movq	-64(%rbp), %rdx
	movq	$0, 8(%rax)
	movq	%rax, %rsi
	leaq	16(%rax), %rdi
	xorl	%eax, %eax
	rep stosq
	movq	%rsi, 2192(%r15)
	movq	8(%rsi), %rax
	cmpq	$64, %rax
	je	.L2346
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rsi)
	movq	%rdx, 16(%rsi,%rax,8)
	jmp	.L2346
	.cfi_endproc
.LFE24128:
	.size	_ZN2v88internal7Factory18NewTransitionArrayEii, .-_ZN2v88internal7Factory18NewTransitionArrayEii
	.section	.text._ZN2v88internal7Factory17NewAllocationSiteEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewAllocationSiteEb
	.type	_ZN2v88internal7Factory17NewAllocationSiteEb, @function
_ZN2v88internal7Factory17NewAllocationSiteEb:
.LFB24129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	4296(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	leaq	4304(%rdi), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	testb	%r13b, %r13b
	cmovne	%rax, %rsi
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%rbx), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L2378
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %r14
.L2379:
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r15
	movq	%r12, %rcx
	leaq	7(%r12), %rax
	andq	$-262144, %rcx
	movq	%r15, 7(%r12)
	testb	$1, %r15b
	je	.L2391
	movq	%r15, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rdx
	movq	%r8, -56(%rbp)
	testl	$262144, %edx
	jne	.L2408
	andl	$24, %edx
	je	.L2391
.L2414:
	testb	$24, 8(%rcx)
	je	.L2409
	.p2align 4,,10
	.p2align 3
.L2391:
	movq	(%rax), %rdx
	leaq	15(%r12), %rsi
	sarq	$32, %rdx
	andl	$-32, %edx
	salq	$32, %rdx
	movq	%rdx, (%rax)
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r15
	movq	%r15, 15(%r12)
	testb	$1, %r15b
	je	.L2390
	movq	%r15, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -56(%rbp)
	testl	$262144, %eax
	jne	.L2410
	testb	$24, %al
	je	.L2390
.L2413:
	testb	$24, 8(%rcx)
	je	.L2411
	.p2align 4,,10
	.p2align 3
.L2390:
	movl	$0, 31(%r12)
	movl	$0, 35(%r12)
	movq	24(%rcx), %rax
	movq	-36520(%rax), %rax
	movq	%rax, 23(%r12)
	testb	%r13b, %r13b
	jne	.L2412
.L2387:
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2410:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%r8), %rax
	testb	$24, %al
	jne	.L2413
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2408:
	movq	%r15, %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r8
	movq	-72(%rbp), %rcx
	movq	-64(%rbp), %rax
	movq	8(%r8), %rdx
	andl	$24, %edx
	jne	.L2414
	jmp	.L2391
	.p2align 4,,10
	.p2align 3
.L2378:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L2415
.L2380:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%r14)
	jmp	.L2379
	.p2align 4,,10
	.p2align 3
.L2412:
	movq	(%r14), %r13
	movq	39128(%rbx), %r12
	movq	%r12, 39(%r13)
	leaq	39(%r13), %r15
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	jne	.L2416
.L2388:
	movq	(%r14), %rax
	movq	%rax, 39128(%rbx)
	jmp	.L2387
	.p2align 4,,10
	.p2align 3
.L2416:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2388
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2388
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2388
	.p2align 4,,10
	.p2align 3
.L2409:
	movq	%rax, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rax
	jmp	.L2391
	.p2align 4,,10
	.p2align 3
.L2411:
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2415:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L2380
	.cfi_endproc
.LFE24129:
	.size	_ZN2v88internal7Factory17NewAllocationSiteEb, .-_ZN2v88internal7Factory17NewAllocationSiteEb
	.section	.rodata._ZN2v88internal7Factory13InitializeMapENS0_3MapENS0_12InstanceTypeEiNS0_12ElementsKindEi.str1.1,"aMS",@progbits,1
.LC14:
	.string	"IsAligned(value, kTaggedSize)"
	.section	.rodata._ZN2v88internal7Factory13InitializeMapENS0_3MapENS0_12InstanceTypeEiNS0_12ElementsKindEi.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"static_cast<unsigned>(value) < 256"
	.section	.rodata._ZN2v88internal7Factory13InitializeMapENS0_3MapENS0_12InstanceTypeEiNS0_12ElementsKindEi.str1.1
.LC16:
	.string	"0 == value"
.LC17:
	.string	"0 <= value"
	.section	.rodata._ZN2v88internal7Factory13InitializeMapENS0_3MapENS0_12InstanceTypeEiNS0_12ElementsKindEi.str1.8
	.align 8
.LC18:
	.string	"static_cast<unsigned>(id) < 256"
	.align 8
.LC19:
	.string	"static_cast<int>(elements_kind) < kElementsKindCount"
	.section	.text._ZN2v88internal7Factory13InitializeMapENS0_3MapENS0_12InstanceTypeEiNS0_12ElementsKindEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory13InitializeMapENS0_3MapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	.type	_ZN2v88internal7Factory13InitializeMapENS0_3MapENS0_12InstanceTypeEiNS0_12ElementsKindEi, @function
_ZN2v88internal7Factory13InitializeMapENS0_3MapENS0_12InstanceTypeEiNS0_12ElementsKindEi:
.LFB24131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, -40(%rbp)
	movw	%dx, 11(%rsi)
	movq	104(%rdi), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, 23(%rax)
	movq	104(%rdi), %rdx
	movq	-40(%rbp), %rax
	movq	%rdx, 31(%rax)
	testb	$7, %cl
	jne	.L2444
	movl	%ecx, %eax
	sarl	$3, %eax
	cmpl	$2047, %ecx
	ja	.L2421
	movq	-40(%rbp), %rdx
	movq	%rdi, %rbx
	movl	%r8d, %r12d
	movl	%r9d, %r13d
	movb	%al, 7(%rdx)
	movq	-40(%rbp), %rdx
	cmpw	$1024, 11(%rdx)
	ja	.L2445
	movb	$0, 8(%rdx)
	movq	-40(%rbp), %rax
	movq	$0, 63(%rax)
	movq	-40(%rbp), %rdi
.L2423:
	movq	1072(%rbx), %rax
	movq	%rax, 55(%rdi)
	movq	-40(%rbp), %rax
	movq	$0, 71(%rax)
	movq	-40(%rbp), %rdx
	cmpw	$1024, 11(%rdx)
	ja	.L2424
	testl	%r13d, %r13d
	jne	.L2446
	movb	$0, 9(%rdx)
.L2426:
	movq	296(%rbx), %rdx
	xorl	%ecx, %ecx
	leaq	-40(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi@PLT
	movq	-40(%rbp), %rax
	xorl	%edx, %edx
	movq	$0, 47(%rax)
	movq	-40(%rbp), %rdi
	leaq	47(%rdi), %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-40(%rbp), %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_@PLT
	cmpl	$255, %eax
	ja	.L2447
	movq	-40(%rbp), %rdx
	movb	%al, 10(%rdx)
	movq	-40(%rbp), %rax
	movb	$0, 13(%rax)
	movq	-40(%rbp), %rax
	movb	$1, 14(%rax)
	movq	-40(%rbp), %rax
	movl	$138413055, 15(%rax)
	movq	-40(%rbp), %rax
	movl	$0, 19(%rax)
	cmpb	$27, %r12b
	ja	.L2448
	movq	-40(%rbp), %rdx
	movzbl	%r12b, %r8d
	leal	0(,%r8,8), %r12d
	movzbl	14(%rdx), %eax
	andb	$7, %al
	orl	%eax, %r12d
	movb	%r12b, 14(%rdx)
	movq	40960(%rbx), %r12
	cmpb	$0, 6016(%r12)
	je	.L2431
	movq	6008(%r12), %rax
.L2432:
	testq	%rax, %rax
	je	.L2433
	addl	$1, (%rax)
.L2433:
	cmpb	$0, _ZN2v88internal15FLAG_trace_mapsE(%rip)
	jne	.L2449
.L2435:
	movq	-40(%rbp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2445:
	.cfi_restore_state
	testl	%ecx, %ecx
	leal	7(%rcx), %eax
	cmovs	%eax, %ecx
	sarl	$3, %ecx
	subl	%r9d, %ecx
	cmpl	$255, %ecx
	ja	.L2421
	movb	%cl, 8(%rdx)
	movq	4488(%rdi), %r14
	movq	-40(%rbp), %rax
	movq	%r14, 63(%rax)
	movq	%r14, %rdx
	movq	-40(%rbp), %rdi
	leaq	63(%rdi), %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-40(%rbp), %rdi
	testb	$1, %r14b
	je	.L2423
	leaq	63(%rdi), %rsi
	movq	%r14, %rdx
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	movq	-40(%rbp), %rdi
	jmp	.L2423
	.p2align 4,,10
	.p2align 3
.L2424:
	testl	%r13d, %r13d
	js	.L2450
	movzbl	7(%rdx), %eax
	movzbl	8(%rdx), %esi
	movzbl	8(%rdx), %ecx
	subl	%esi, %eax
	subl	%r13d, %eax
	addl	%ecx, %eax
	cmpl	$255, %eax
	ja	.L2451
	movb	%al, 9(%rdx)
	jmp	.L2426
	.p2align 4,,10
	.p2align 3
.L2431:
	movb	$1, 6016(%r12)
	leaq	5992(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6008(%r12)
	jmp	.L2432
	.p2align 4,,10
	.p2align 3
.L2449:
	movq	41016(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L2435
	movq	-40(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger9MapCreateENS0_3MapE@PLT
	jmp	.L2435
	.p2align 4,,10
	.p2align 3
.L2421:
	leaq	.LC15(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2444:
	leaq	.LC14(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2447:
	leaq	.LC18(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2448:
	leaq	.LC19(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2450:
	leaq	.LC17(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2446:
	leaq	.LC16(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2451:
	leaq	.LC5(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24131:
	.size	_ZN2v88internal7Factory13InitializeMapENS0_3MapENS0_12InstanceTypeEiNS0_12ElementsKindEi, .-_ZN2v88internal7Factory13InitializeMapENS0_3MapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	.section	.text._ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	.type	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi, @function
_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi:
.LFB24130:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	xorl	%r8d, %r8d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	$3, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	movl	$1, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	movl	$80, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$24, %rsp
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rbx, %rdi
	movzwl	%r12w, %edx
	movl	%r15d, %r9d
	movq	%rax, %rsi
	movq	136(%rbx), %rax
	movzbl	%r13b, %r8d
	movl	%r14d, %ecx
	movq	%rax, -1(%rsi)
	call	_ZN2v88internal7Factory13InitializeMapENS0_3MapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2453
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2453:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2457
.L2455:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2457:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L2455
	.cfi_endproc
.LFE24130:
	.size	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi, .-_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	.section	.text._ZN2v88internal7Factory21CopyFixedArrayWithMapENS0_6HandleINS0_10FixedArrayEEENS2_INS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory21CopyFixedArrayWithMapENS0_6HandleINS0_10FixedArrayEEENS2_INS0_3MapEEE
	.type	_ZN2v88internal7Factory21CopyFixedArrayWithMapENS0_6HandleINS0_10FixedArrayEEENS2_INS0_3MapEEE, @function
_ZN2v88internal7Factory21CopyFixedArrayWithMapENS0_6HandleINS0_10FixedArrayEEENS2_INS0_3MapEEE:
.LFB24141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	37592(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movslq	11(%rax), %r13
	cmpl	$134217726, %r13d
	ja	.L2475
.L2459:
	leal	16(,%r13,8), %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r9d, %esi
	movl	$1, %ecx
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movl	-56(%rbp), %r9d
	movq	%rax, %rsi
	cmpl	$131072, %r9d
	jle	.L2463
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L2476
.L2463:
	movq	(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2477
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L2465:
	movq	%r13, %rax
	movl	$4, %r9d
	salq	$32, %rax
	movq	%rax, 7(%rsi)
	movq	(%r12), %rsi
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L2467
	xorl	%r9d, %r9d
	testb	$24, %al
	sete	%r9b
	sall	$2, %r9d
.L2467:
	testq	%r13, %r13
	je	.L2469
	movq	(%r14), %rcx
	leaq	15(%rsi), %rdx
	movl	%r13d, %r8d
	movq	%r15, %rdi
	addq	$15, %rcx
	call	_ZN2v88internal4Heap9CopyRangeINS0_14FullObjectSlotEEEvNS0_10HeapObjectET_S5_iNS0_16WriteBarrierModeE@PLT
.L2469:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2477:
	.cfi_restore_state
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L2478
.L2466:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L2465
	.p2align 4,,10
	.p2align 3
.L2475:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	jmp	.L2459
	.p2align 4,,10
	.p2align 3
.L2476:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L2464
	.p2align 4,,10
	.p2align 3
.L2479:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L2463
.L2464:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L2479
	jmp	.L2463
	.p2align 4,,10
	.p2align 3
.L2478:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2466
	.cfi_endproc
.LFE24141:
	.size	_ZN2v88internal7Factory21CopyFixedArrayWithMapENS0_6HandleINS0_10FixedArrayEEENS2_INS0_3MapEEE, .-_ZN2v88internal7Factory21CopyFixedArrayWithMapENS0_6HandleINS0_10FixedArrayEEENS2_INS0_3MapEEE
	.section	.text._ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE:
.LFB24143:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L2481
	leaq	1080(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2481:
	movzbl	%dl, %edx
	jmp	_ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE.part.0
	.cfi_endproc
.LFE24143:
	.size	_ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory16NewWeakArrayListEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory16NewWeakArrayListEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory16NewWeakArrayListEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory16NewWeakArrayListEiNS0_14AllocationTypeE:
.LFB24144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	1080(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	testl	%esi, %esi
	jne	.L2488
.L2486:
	movq	(%r8), %rsi
	movq	88(%r12), %rax
	movslq	%ebx, %rcx
	leaq	23(%rsi), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2488:
	.cfi_restore_state
	movzbl	%dl, %edx
	call	_ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE.part.0
	movq	%rax, %r8
	jmp	.L2486
	.cfi_endproc
.LFE24144:
	.size	_ZN2v88internal7Factory16NewWeakArrayListEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory16NewWeakArrayListEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory24CopyWeakArrayListAndGrowENS0_6HandleINS0_13WeakArrayListEEEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory24CopyWeakArrayListAndGrowENS0_6HandleINS0_13WeakArrayListEEEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory24CopyWeakArrayListAndGrowENS0_6HandleINS0_13WeakArrayListEEEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory24CopyWeakArrayListAndGrowENS0_6HandleINS0_13WeakArrayListEEEiNS0_14AllocationTypeE:
.LFB24146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	1080(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	movq	(%rsi), %rax
	addl	11(%rax), %ebx
	jne	.L2498
.L2491:
	movslq	19(%rax), %r13
	movq	(%r12), %rax
	movl	$4, %r9d
	movq	%r13, %rdx
	salq	$32, %rdx
	movq	%rdx, 15(%rax)
	movq	(%r12), %rsi
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L2492
	xorl	%r9d, %r9d
	testb	$24, %al
	sete	%r9b
	sall	$2, %r9d
.L2492:
	testq	%r13, %r13
	je	.L2494
	movq	(%r15), %rcx
	leaq	23(%rsi), %rdx
	leaq	37592(%r14), %rdi
	movl	%r13d, %r8d
	addq	$23, %rcx
	call	_ZN2v88internal4Heap9CopyRangeINS0_19FullMaybeObjectSlotEEEvNS0_10HeapObjectET_S5_iNS0_16WriteBarrierModeE@PLT
	movq	(%r12), %rsi
.L2494:
	subl	%r13d, %ebx
	movq	88(%r14), %rax
	leaq	23(%rsi,%r13,8), %rdi
	movslq	%ebx, %rcx
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2498:
	.cfi_restore_state
	movzbl	%cl, %edx
	movl	%ebx, %esi
	call	_ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE.part.0
	movq	%rax, %r12
	movq	(%r15), %rax
	jmp	.L2491
	.cfi_endproc
.LFE24146:
	.size	_ZN2v88internal7Factory24CopyWeakArrayListAndGrowENS0_6HandleINS0_13WeakArrayListEEEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory24CopyWeakArrayListAndGrowENS0_6HandleINS0_13WeakArrayListEEEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory20CompactWeakArrayListENS0_6HandleINS0_13WeakArrayListEEEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory20CompactWeakArrayListENS0_6HandleINS0_13WeakArrayListEEEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory20CompactWeakArrayListENS0_6HandleINS0_13WeakArrayListEEEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory20CompactWeakArrayListENS0_6HandleINS0_13WeakArrayListEEEiNS0_14AllocationTypeE:
.LFB24147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	leaq	1080(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testl	%edx, %edx
	jne	.L2525
.L2501:
	movq	(%r8), %rdi
	movl	$4, %r14d
	movq	%rdi, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L2502
	xorl	%r14d, %r14d
	testb	$24, %al
	sete	%r14b
	sall	$2, %r14d
.L2502:
	movq	(%r15), %rdx
	movslq	19(%rdx), %rax
	testq	%rax, %rax
	jle	.L2514
	subl	$1, %eax
	movl	$24, %ebx
	leaq	32(,%rax,8), %r10
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2511:
	movq	-1(%rbx,%rdx), %r12
	cmpl	$3, %r12d
	je	.L2504
	leal	1(%rax), %ecx
	leal	24(,%rax,8), %eax
	cltq
	leaq	-1(%rax,%rdi), %rsi
	movq	%r12, (%rsi)
	testl	%r14d, %r14d
	je	.L2505
	movq	%r12, %rax
	notq	%rax
	andl	$1, %eax
	cmpl	$4, %r14d
	je	.L2526
	testb	%al, %al
	jne	.L2505
.L2520:
	movq	%r12, %rdx
	andq	$-262144, %r12
	andq	$-3, %rdx
	testb	$24, 8(%r12)
	je	.L2505
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2527
	.p2align 4,,10
	.p2align 3
.L2505:
	movq	(%r8), %rdi
	movl	%ecx, %eax
.L2504:
	addq	$8, %rbx
	cmpq	%rbx, %r10
	je	.L2528
	movq	(%r15), %rdx
	jmp	.L2511
	.p2align 4,,10
	.p2align 3
.L2528:
	movslq	%eax, %rdx
	subl	%eax, %r13d
	salq	$32, %rax
	salq	$3, %rdx
.L2503:
	movq	%rax, 15(%rdi)
	movslq	%r13d, %rcx
	movq	(%r8), %rsi
	movq	88(%r9), %rax
	leaq	23(%rdx,%rsi), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$56, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2526:
	.cfi_restore_state
	testb	%al, %al
	jne	.L2505
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2520
	movq	%r12, %rdx
	movq	%r9, -96(%rbp)
	andq	$-3, %rdx
	movq	%r10, -88(%rbp)
	movq	%r8, -80(%rbp)
	movl	%ecx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rsi
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r9
	jmp	.L2520
	.p2align 4,,10
	.p2align 3
.L2527:
	movq	%r9, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%r8, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r10
	movq	-64(%rbp), %r8
	movl	-56(%rbp), %ecx
	jmp	.L2505
	.p2align 4,,10
	.p2align 3
.L2525:
	movzbl	%cl, %edx
	movl	%r13d, %esi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal7Factory29NewUninitializedWeakArrayListEiNS0_14AllocationTypeE.part.0
	movq	-56(%rbp), %r9
	movq	%rax, %r8
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2514:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L2503
	.cfi_endproc
.LFE24147:
	.size	_ZN2v88internal7Factory20CompactWeakArrayListENS0_6HandleINS0_13WeakArrayListEEEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory20CompactWeakArrayListENS0_6HandleINS0_13WeakArrayListEEEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory18CopyFixedArrayUpToENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18CopyFixedArrayUpToENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory18CopyFixedArrayUpToENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory18CopyFixedArrayUpToENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE:
.LFB24149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	288(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testl	%edx, %edx
	jne	.L2546
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2546:
	.cfi_restore_state
	movq	%rsi, %r14
	movl	%edx, %ebx
	leaq	37592(%rdi), %r15
	movl	%ecx, %r13d
	cmpl	$134217726, %edx
	ja	.L2547
.L2532:
	leal	16(,%rbx,8), %eax
	movl	%r13d, %edx
	xorl	%r8d, %r8d
	movq	%r15, %rdi
	movl	%eax, %esi
	movl	$1, %ecx
	movl	%eax, %r13d
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r13d
	jle	.L2536
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	je	.L2536
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L2537
	.p2align 4,,10
	.p2align 3
.L2548:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L2536
.L2537:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L2548
	.p2align 4,,10
	.p2align 3
.L2536:
	movq	152(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2549
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L2538:
	movq	%rbx, %rax
	movl	$4, %r9d
	salq	$32, %rax
	movq	%rax, 7(%rsi)
	movq	0(%r13), %rsi
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L2540
	xorl	%r9d, %r9d
	testb	$24, %al
	sete	%r9b
	sall	$2, %r9d
.L2540:
	movq	(%r14), %rcx
	movl	%ebx, %r8d
	movq	%r15, %rdi
	leaq	15(%rsi), %rdx
	addq	$15, %rcx
	call	_ZN2v88internal4Heap9CopyRangeINS0_14FullObjectSlotEEEvNS0_10HeapObjectET_S5_iNS0_16WriteBarrierModeE@PLT
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2549:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L2550
.L2539:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L2538
	.p2align 4,,10
	.p2align 3
.L2547:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	jmp	.L2532
	.p2align 4,,10
	.p2align 3
.L2550:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2539
	.cfi_endproc
.LFE24149:
	.size	_ZN2v88internal7Factory18CopyFixedArrayUpToENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory18CopyFixedArrayUpToENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory26CopyAndTenureFixedCOWArrayENS0_6HandleINS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory26CopyAndTenureFixedCOWArrayENS0_6HandleINS0_10FixedArrayEEE
	.type	_ZN2v88internal7Factory26CopyAndTenureFixedCOWArrayENS0_6HandleINS0_10FixedArrayEEE, @function
_ZN2v88internal7Factory26CopyAndTenureFixedCOWArrayENS0_6HandleINS0_10FixedArrayEEE:
.LFB24151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	288(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movslq	11(%rax), %r14
	testq	%r14, %r14
	jne	.L2568
.L2553:
	movq	(%r12), %rax
	movq	160(%rbx), %rdx
	movq	%rdx, -1(%rax)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2568:
	.cfi_restore_state
	movq	%rsi, %r13
	leaq	37592(%rdi), %r15
	cmpl	$134217726, %r14d
	ja	.L2569
.L2554:
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r15, %rdi
	leal	16(,%r14,8), %r12d
	movl	%r12d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r12d
	jle	.L2558
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	je	.L2558
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L2559
	.p2align 4,,10
	.p2align 3
.L2570:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L2558
.L2559:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L2570
	.p2align 4,,10
	.p2align 3
.L2558:
	movq	152(%rbx), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2571
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L2560:
	movq	%r14, %rax
	movl	$4, %r9d
	salq	$32, %rax
	movq	%rax, 7(%rsi)
	movq	(%r12), %rsi
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L2562
	xorl	%r9d, %r9d
	testb	$24, %al
	sete	%r9b
	sall	$2, %r9d
.L2562:
	movq	0(%r13), %rcx
	leaq	15(%rsi), %rdx
	movl	%r14d, %r8d
	movq	%r15, %rdi
	addq	$15, %rcx
	call	_ZN2v88internal4Heap9CopyRangeINS0_14FullObjectSlotEEEvNS0_10HeapObjectET_S5_iNS0_16WriteBarrierModeE@PLT
	jmp	.L2553
	.p2align 4,,10
	.p2align 3
.L2571:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L2572
.L2561:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L2560
	.p2align 4,,10
	.p2align 3
.L2569:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2572:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2561
	.cfi_endproc
.LFE24151:
	.size	_ZN2v88internal7Factory26CopyAndTenureFixedCOWArrayENS0_6HandleINS0_10FixedArrayEEE, .-_ZN2v88internal7Factory26CopyAndTenureFixedCOWArrayENS0_6HandleINS0_10FixedArrayEEE
	.section	.text._ZN2v88internal7Factory20CopyFixedDoubleArrayENS0_6HandleINS0_16FixedDoubleArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory20CopyFixedDoubleArrayENS0_6HandleINS0_16FixedDoubleArrayEEE
	.type	_ZN2v88internal7Factory20CopyFixedDoubleArrayENS0_6HandleINS0_16FixedDoubleArrayEEE, @function
_ZN2v88internal7Factory20CopyFixedDoubleArrayENS0_6HandleINS0_16FixedDoubleArrayEEE:
.LFB24152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movslq	11(%rax), %r13
	movq	%rsi, %rax
	testq	%r13, %r13
	jne	.L2592
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2592:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE.part.0
	leal	16(,%r13,8), %ecx
	movq	(%rbx), %r8
	leal	-1(%rcx), %edx
	subl	$8, %ecx
	movq	%rax, %r12
	movq	(%rax), %rax
	cmovns	%ecx, %edx
	leaq	7(%r8), %rsi
	movq	%rsi, %r10
	leaq	7(%rax), %r9
	sarl	$3, %edx
	movslq	%edx, %rdx
	cmpq	$15, %rdx
	ja	.L2576
	leaq	22(%r8), %rcx
	subq	%r9, %rcx
	cmpq	$30, %rcx
	leaq	-1(%rdx), %rcx
	jbe	.L2577
	cmpq	$3, %rcx
	jbe	.L2577
	leaq	-2(%rdx), %rcx
	xorl	%edi, %edi
	subq	%r8, %rax
	shrq	%rcx
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L2579:
	movdqu	(%rsi), %xmm0
	addq	$1, %rdi
	movups	%xmm0, (%rax,%rsi)
	addq	$16, %rsi
	cmpq	%rcx, %rdi
	jb	.L2579
	leaq	(%rcx,%rcx), %rax
	salq	$4, %rcx
	addq	%rcx, %r9
	addq	%r10, %rcx
	cmpq	%rax, %rdx
	je	.L2580
	movq	(%rcx), %rax
	movq	%rax, (%r9)
.L2580:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2576:
	.cfi_restore_state
	salq	$3, %rdx
	movq	%r9, %rdi
	call	memcpy@PLT
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2577:
	subq	%r8, %rax
	jmp	.L2590
	.p2align 4,,10
	.p2align 3
.L2593:
	subq	$1, %rcx
.L2590:
	movq	%r10, %rsi
	leaq	(%r10,%rax), %rdx
	addq	$8, %r10
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	testq	%rcx, %rcx
	jne	.L2593
	jmp	.L2580
	.cfi_endproc
.LFE24152:
	.size	_ZN2v88internal7Factory20CopyFixedDoubleArrayENS0_6HandleINS0_16FixedDoubleArrayEEE, .-_ZN2v88internal7Factory20CopyFixedDoubleArrayENS0_6HandleINS0_16FixedDoubleArrayEEE
	.section	.text._ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE:
.LFB24153:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%xmm0, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	comisd	.LC20(%rip), %xmm0
	jb	.L2595
	movsd	.LC21(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L2595
	movabsq	$-9223372036854775808, %rax
	cmpq	%rax, %r12
	je	.L2595
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L2595
	jne	.L2595
	movq	41112(%rdi), %rdi
	movq	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L2599
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2595:
	.cfi_restore_state
	movq	256(%rbx), %r13
	leaq	37592(%rbx), %rdi
	movl	%esi, %edx
	movl	$2, %r8d
	movl	$16, %esi
	movl	$1, %ecx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r13, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2603
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2604:
	movq	%r12, 7(%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2603:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2610
.L2605:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L2604
	.p2align 4,,10
	.p2align 3
.L2599:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2611
.L2601:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2610:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L2605
	.p2align 4,,10
	.p2align 3
.L2611:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L2601
	.cfi_endproc
.LFE24153:
	.size	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE, .-_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE:
.LFB24154:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	salq	$32, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2613
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2613:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2617
.L2615:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2617:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L2615
	.cfi_endproc
.LFE24154:
	.size	_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory16NewNumberFromIntEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE:
.LFB24155:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	testl	%esi, %esi
	js	.L2619
	movq	41112(%rdi), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L2632
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2632:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2633
.L2629:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2619:
	.cfi_restore_state
	movl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	256(%rdi), %r12
	movl	$2, %r8d
	cvtsi2sdq	%rsi, %xmm0
	movl	$1, %ecx
	movl	$16, %esi
	leaq	37592(%rdi), %rdi
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2624
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2625:
	movq	-24(%rbp), %xmm1
	movq	%xmm1, 7(%rsi)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2624:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2634
.L2626:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L2625
	.p2align 4,,10
	.p2align 3
.L2633:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L2629
	.p2align 4,,10
	.p2align 3
.L2634:
	movq	%rbx, %rdi
	movq	%rsi, -32(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-32(%rbp), %rsi
	jmp	.L2626
	.cfi_endproc
.LFE24155:
	.size	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE, .-_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE:
.LFB24156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movl	$2, %r8d
	movl	$16, %esi
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$16, %rsp
	movq	-37336(%rdi), %r12
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2636
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2636:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2640
.L2638:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2640:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L2638
	.cfi_endproc
.LFE24156:
	.size	_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE, .-_ZN2v88internal7Factory13NewHeapNumberENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory29NewHeapNumberForCodeAssemblerEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory29NewHeapNumberForCodeAssemblerEd
	.type	_ZN2v88internal7Factory29NewHeapNumberForCodeAssemblerEd, @function
_ZN2v88internal7Factory29NewHeapNumberForCodeAssemblerEd:
.LFB24157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %esi
	movl	$2, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%xmm0, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	37896(%rdi), %rax
	movq	256(%rdi), %r13
	cmpb	$1, 232(%rax)
	sbbl	%edx, %edx
	addq	$37592, %rdi
	andl	$3, %edx
	addl	$1, %edx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r13, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2643
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%r12, 7(%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2643:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2648
.L2645:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movq	%r12, 7(%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2648:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L2645
	.cfi_endproc
.LFE24157:
	.size	_ZN2v88internal7Factory29NewHeapNumberForCodeAssemblerEd, .-_ZN2v88internal7Factory29NewHeapNumberForCodeAssemblerEd
	.section	.rodata._ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE.str1.1,"aMS",@progbits,1
.LC22:
	.string	"invalid BigInt length"
	.section	.text._ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE:
.LFB24158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	37592(%rdi), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpl	$16777216, %esi
	ja	.L2655
.L2650:
	movq	448(%rbx), %r15
	movq	%r14, %rdi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	leal	16(,%r12,8), %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r15, -1(%rax)
	movl	$0, 11(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2651
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2651:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2656
.L2653:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2655:
	.cfi_restore_state
	leaq	.LC22(%rip), %rsi
	movq	%r14, %rdi
	movl	%edx, -40(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-40(%rbp), %edx
	jmp	.L2650
	.p2align 4,,10
	.p2align 3
.L2656:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L2653
	.cfi_endproc
.LFE24158:
	.size	_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory9NewBigIntEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6StringEEE
	.type	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6StringEEE, @function
_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6StringEEE:
.LFB24160:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	xorl	%r9d, %r9d
	movq	%rsi, %rdx
	movl	$2, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$32, %rsp
	pushq	$0
	call	_ZN2v88internal10ErrorUtils9ConstructEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS4_INS0_6ObjectEEES8_NS0_13FrameSkipModeES8_NS1_20StackTraceCollectionE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L2663
.L2658:
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2663:
	.cfi_restore_state
	movq	41112(%rbx), %rdi
	movq	12480(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L2659
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	96(%rbx), %rdx
	movq	%rdx, 12480(%rbx)
	testq	%rax, %rax
	jne	.L2658
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2659:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2664
.L2661:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movq	96(%rbx), %rdx
	movq	%rdx, 12480(%rbx)
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2664:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L2661
	.cfi_endproc
.LFE24160:
	.size	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6StringEEE, .-_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6StringEEE
	.section	.rodata._ZN2v88internal7Factory27NewInvalidStringLengthErrorEv.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"Aborting on invalid string length"
	.section	.text._ZN2v88internal7Factory27NewInvalidStringLengthErrorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv
	.type	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv, @function
_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv:
.LFB24161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	jne	.L2672
	movq	4544(%rdi), %rdx
	movq	%rdi, %r12
	movabsq	$4294967296, %rax
	cmpq	%rax, 7(%rdx)
	je	.L2673
.L2667:
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1719(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2668
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2669:
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	popq	%r12
	xorl	%r8d, %r8d
	popq	%r13
	xorl	%ecx, %ecx
	movl	$201, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_
	.p2align 4,,10
	.p2align 3
.L2668:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2674
.L2670:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L2669
	.p2align 4,,10
	.p2align 3
.L2673:
	call	_ZN2v88internal7Isolate39InvalidateStringLengthOverflowProtectorEv@PLT
	jmp	.L2667
	.p2align 4,,10
	.p2align 3
.L2674:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L2670
.L2672:
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24161:
	.size	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv, .-_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv
	.section	.text._ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE:
.LFB24083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	cmpl	$1073741799, %esi
	jbe	.L2676
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2676:
	.cfi_restore_state
	movl	%esi, %ebx
	leal	23(%rsi), %esi
	movq	184(%rdi), %r13
	xorl	%r8d, %r8d
	andl	$-8, %esi
	leaq	37592(%rdi), %rdi
	movl	$1, %ecx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r13, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2678
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2679:
	movl	%ebx, 11(%rsi)
	movq	(%rax), %rdx
	movl	$3, 7(%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2678:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2682
.L2680:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L2679
	.p2align 4,,10
	.p2align 3
.L2682:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L2680
	.cfi_endproc
.LFE24083:
	.size	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE:
.LFB24056:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	128(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rsi), %r13
	testl	%r13d, %r13d
	je	.L2685
	movq	%rsi, %r12
	cmpl	$1, %r13d
	je	.L2703
	movl	%r13d, %esi
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L2685
	movq	(%rbx), %rax
	movslq	%r13d, %rdx
	movq	(%r12), %r8
	leaq	15(%rax), %rdi
	cmpq	$7, %rdx
	ja	.L2704
	addq	%rdi, %rdx
	cmpq	%rdi, %rdx
	jbe	.L2689
	movq	%rdx, %rcx
	movq	%rax, %r9
	leaq	16(%rax), %rsi
	subq	%rax, %rcx
	negq	%r9
	subq	$16, %rcx
	cmpq	$14, %rcx
	seta	%r10b
	cmpq	%rsi, %rdx
	setnb	%cl
	testb	%cl, %r10b
	je	.L2702
	leaq	15(%r8), %rcx
	subq	%rdi, %rcx
	cmpq	$30, %rcx
	jbe	.L2702
	movq	%rdx, %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rdx
	movl	$1, %eax
	leaq	-15(%rcx), %r10
	cmovb	%rax, %r10
	movq	%rdi, %rax
	xorl	%ecx, %ecx
	addq	%r8, %r9
	leaq	-16(%r10), %rsi
	shrq	$4, %rsi
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L2693:
	movdqu	-15(%rax,%r9), %xmm0
	addq	$1, %rcx
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rcx, %rsi
	ja	.L2693
	salq	$4, %rsi
	addq	%rsi, %rdi
	addq	%rsi, %r8
	cmpq	%rsi, %r10
	je	.L2689
	movzbl	(%r8), %eax
	movb	%al, (%rdi)
	leaq	1(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	1(%r8), %eax
	movb	%al, 1(%rdi)
	leaq	2(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	2(%r8), %eax
	movb	%al, 2(%rdi)
	leaq	3(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	3(%r8), %eax
	movb	%al, 3(%rdi)
	leaq	4(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	4(%r8), %eax
	movb	%al, 4(%rdi)
	leaq	5(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	5(%r8), %eax
	movb	%al, 5(%rdi)
	leaq	6(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	6(%r8), %eax
	movb	%al, 6(%rdi)
	leaq	7(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	7(%r8), %eax
	movb	%al, 7(%rdi)
	leaq	8(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	8(%r8), %eax
	movb	%al, 8(%rdi)
	leaq	9(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	9(%r8), %eax
	movb	%al, 9(%rdi)
	leaq	10(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	10(%r8), %eax
	movb	%al, 10(%rdi)
	leaq	11(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	11(%r8), %eax
	movb	%al, 11(%rdi)
	leaq	12(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	12(%r8), %eax
	movb	%al, 12(%rdi)
	leaq	13(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	13(%r8), %eax
	movb	%al, 13(%rdi)
	leaq	14(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L2689
	movzbl	14(%r8), %eax
	movb	%al, 14(%rdi)
	jmp	.L2689
	.p2align 4,,10
	.p2align 3
.L2704:
	movq	%r8, %rsi
	call	memcpy@PLT
.L2689:
	movq	%rbx, %rax
.L2685:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2703:
	.cfi_restore_state
	movq	(%rsi), %rax
	movzbl	(%rax), %esi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2702:
	.cfi_restore_state
	addq	%r8, %r9
	.p2align 4,,10
	.p2align 3
.L2699:
	movq	%rdi, %rax
	movzbl	-15(%rdi,%r9), %ecx
	addq	$1, %rdi
	movb	%cl, (%rax)
	cmpq	%rdi, %rdx
	ja	.L2699
	jmp	.L2689
	.cfi_endproc
.LFE24056:
	.size	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_
	.type	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_, @function
_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_:
.LFB24159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	41088(%rdi), %r12
	movq	41096(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40936(%rdi), %rax
	addl	$1, 41104(%rdi)
	movl	8(%rax), %edi
	testl	%edi, %edi
	jne	.L2735
	leaq	88(%rbx), %rax
	testq	%rcx, %rcx
	movq	%rbx, %rdi
	cmove	%rax, %rcx
	testq	%r8, %r8
	cmove	%rax, %r8
	testq	%r9, %r9
	cmove	%rax, %r9
	subq	$8, %rsp
	pushq	$2
	call	_ZN2v88internal10ErrorUtils16MakeGenericErrorEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS4_INS0_6ObjectEEES9_S9_NS0_13FrameSkipModeE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L2736
.L2719:
	movq	(%rax), %r14
	subl	$1, 41104(%rbx)
	movq	%r12, 41088(%rbx)
	cmpq	41096(%rbx), %r13
	je	.L2722
	movq	%r13, 41096(%rbx)
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2722:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2723
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2724:
	movq	41088(%rbx), %rcx
	movl	41104(%rbx), %edx
	movq	%rcx, 41088(%rbx)
	movl	%edx, 41104(%rbx)
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L2737
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2723:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2738
.L2725:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r14, (%rax)
	jmp	.L2724
	.p2align 4,,10
	.p2align 3
.L2735:
	movl	%edx, %edi
	call	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	strlen@PLT
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r14, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE
	testq	%rax, %rax
	jne	.L2719
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2736:
	movq	41112(%rbx), %rdi
	movq	12480(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L2727
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2720:
	movq	96(%rbx), %rdx
	movq	%rdx, 12480(%rbx)
	jmp	.L2719
	.p2align 4,,10
	.p2align 3
.L2738:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L2725
	.p2align 4,,10
	.p2align 3
.L2727:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2739
.L2721:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L2720
.L2739:
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L2721
.L2737:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24159:
	.size	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_, .-_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_
	.section	.text._ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE:
.LFB24084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	cmpl	$1073741799, %esi
	jbe	.L2741
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2741:
	.cfi_restore_state
	movl	%esi, %ebx
	leal	23(%rsi,%rsi), %esi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	744(%rdi), %r13
	andl	$-8, %esi
	leaq	37592(%rdi), %rdi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r13, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2743
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2744:
	movl	%ebx, 11(%rsi)
	movq	(%rax), %rdx
	movl	$3, 7(%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2743:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2747
.L2745:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L2744
	.p2align 4,,10
	.p2align 3
.L2747:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L2745
	.cfi_endproc
.LFE24084:
	.size	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE:
.LFB24058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r14
	leaq	-76(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	8(%rsi), %rax
	movq	%r14, %rsi
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11Utf8DecoderC1ERKNS0_6VectorIKhEE@PLT
	movl	-68(%rbp), %esi
	leaq	128(%r12), %rax
	testl	%esi, %esi
	je	.L2750
	cmpb	$1, -76(%rbp)
	movl	%ebx, %edx
	movq	%r12, %rdi
	jbe	.L2757
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2756
	movq	(%rax), %rax
	movq	%r14, %rdx
	movq	%r13, %rdi
	leaq	15(%rax), %rsi
	call	_ZN2v88internal11Utf8Decoder6DecodeItEEvPT_RKNS0_6VectorIKhEE@PLT
	movq	%r12, %rax
.L2750:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2758
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2757:
	.cfi_restore_state
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2756
	movq	(%rax), %rax
	movq	%r14, %rdx
	movq	%r13, %rdi
	leaq	15(%rax), %rsi
	call	_ZN2v88internal11Utf8Decoder6DecodeIhEEvPT_RKNS0_6VectorIKhEE@PLT
	movq	%r12, %rax
	jmp	.L2750
	.p2align 4,,10
	.p2align 3
.L2756:
	xorl	%eax, %eax
	jmp	.L2750
.L2758:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24058:
	.size	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE.part.0, @function
_ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE.part.0:
.LFB33125:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	%edx, %rbx
	movq	%r12, %rax
	movl	%ecx, %edx
	leaq	(%rbx,%rbx), %r13
	movq	%rbx, %rsi
	leaq	(%r12,%r13), %rcx
	cmpq	$7, %rbx
	jbe	.L2811
	testb	$7, %r12b
	jne	.L2761
	.p2align 4,,10
	.p2align 3
.L2765:
	leaq	16(%rax), %r8
	cmpq	%r8, %rcx
	jb	.L2811
	leaq	-16(%rcx), %r8
	subq	%rax, %r8
	shrq	$3, %r8
	leaq	8(%rax,%r8,8), %r9
	movabsq	$-71777214294589696, %r8
	jmp	.L2769
	.p2align 4,,10
	.p2align 3
.L2766:
	addq	$8, %rax
	cmpq	%r9, %rax
	je	.L2811
.L2769:
	testq	%r8, (%rax)
	je	.L2766
	cmpq	%rax, %rcx
	jbe	.L2767
	.p2align 4,,10
	.p2align 3
.L2768:
	cmpw	$255, (%rax)
	ja	.L2767
	addq	$2, %rax
.L2811:
	cmpq	%rax, %rcx
	ja	.L2768
.L2767:
	subq	%r12, %rax
	sarq	%rax
	cmpl	%eax, %esi
	jg	.L2771
.L2814:
	cmpl	$1, %esi
	je	.L2813
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE
	testq	%rax, %rax
	je	.L2773
	movq	(%rax), %rdi
	leaq	15(%rdi), %rdx
	addq	%rdx, %rbx
	cmpq	%rbx, %rdx
	jnb	.L2773
	movq	%rbx, %rcx
	leaq	16(%rdi), %rsi
	movl	$1, %r9d
	movl	$2, %r8d
	subq	%rdi, %rcx
	subq	$15, %rcx
	cmpq	%rsi, %rbx
	cmovnb	%rcx, %r9
	addq	%rcx, %rcx
	cmpq	%rsi, %rbx
	cmovb	%r8, %rcx
	addq	%r12, %rcx
	cmpq	%rcx, %rdx
	leaq	(%rdx,%r9), %rcx
	setnb	%r8b
	cmpq	%rcx, %r12
	setnb	%cl
	orb	%cl, %r8b
	je	.L2776
	movq	%rbx, %rcx
	subq	%rdi, %rcx
	subq	$16, %rcx
	cmpq	$14, %rcx
	seta	%r8b
	cmpq	%rsi, %rbx
	setnb	%cl
	testb	%cl, %r8b
	je	.L2776
	leaq	-16(%r9), %rsi
	negq	%rdi
	movdqa	.LC11(%rip), %xmm2
	movq	%rdx, %rcx
	shrq	$4, %rsi
	leaq	(%r12,%rdi,2), %r8
	xorl	%edi, %edi
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L2777:
	movdqu	-30(%r8,%rcx,2), %xmm0
	movdqu	-14(%r8,%rcx,2), %xmm1
	addq	$1, %rdi
	addq	$16, %rcx
	pand	%xmm2, %xmm0
	pand	%xmm2, %xmm1
	packuswb	%xmm1, %xmm0
	movups	%xmm0, -16(%rcx)
	cmpq	%rdi, %rsi
	ja	.L2777
	movq	%rsi, %rcx
	salq	$5, %rsi
	salq	$4, %rcx
	addq	%rsi, %r12
	addq	%rcx, %rdx
	cmpq	%rcx, %r9
	je	.L2773
	movzwl	(%r12), %ecx
	movb	%cl, (%rdx)
	leaq	1(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	2(%r12), %ecx
	movb	%cl, 1(%rdx)
	leaq	2(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	4(%r12), %ecx
	movb	%cl, 2(%rdx)
	leaq	3(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	6(%r12), %ecx
	movb	%cl, 3(%rdx)
	leaq	4(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	8(%r12), %ecx
	movb	%cl, 4(%rdx)
	leaq	5(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	10(%r12), %ecx
	movb	%cl, 5(%rdx)
	leaq	6(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	12(%r12), %ecx
	movb	%cl, 6(%rdx)
	leaq	7(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	14(%r12), %ecx
	movb	%cl, 7(%rdx)
	leaq	8(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	16(%r12), %ecx
	movb	%cl, 8(%rdx)
	leaq	9(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	18(%r12), %ecx
	movb	%cl, 9(%rdx)
	leaq	10(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	20(%r12), %ecx
	movb	%cl, 10(%rdx)
	leaq	11(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	22(%r12), %ecx
	movb	%cl, 11(%rdx)
	leaq	12(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	24(%r12), %ecx
	movb	%cl, 12(%rdx)
	leaq	13(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	26(%r12), %ecx
	movb	%cl, 13(%rdx)
	leaq	14(%rdx), %rcx
	cmpq	%rcx, %rbx
	jbe	.L2773
	movzwl	28(%r12), %ecx
	movb	%cl, 14(%rdx)
	jmp	.L2773
	.p2align 4,,10
	.p2align 3
.L2763:
	addq	$2, %rax
	testb	$7, %al
	je	.L2765
.L2761:
	cmpw	$255, (%rax)
	jbe	.L2763
	subq	%r12, %rax
	sarq	%rax
	cmpl	%eax, %esi
	jle	.L2814
.L2771:
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE
	movq	%rax, %r14
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L2773
	movq	(%r14), %rdx
	leaq	15(%rdx), %rdi
	movq	%rdi, %rax
	cmpq	$3, %rbx
	ja	.L2815
	addq	%rdi, %r13
	cmpq	%r13, %rdi
	jnb	.L2783
	movq	%r13, %rbx
	leaq	16(%rdx), %r8
	movq	%rdx, %rsi
	subq	%rdx, %rbx
	negq	%rsi
	movq	%rbx, %rdx
	subq	$16, %rdx
	cmpq	%r13, %r8
	setbe	%r9b
	cmpq	$13, %rdx
	seta	%cl
	testb	%cl, %r9b
	je	.L2812
	leaq	15(%r12), %rcx
	subq	%rdi, %rcx
	cmpq	$30, %rcx
	jbe	.L2812
	shrq	%rdx
	cmpq	%r13, %r8
	leaq	1(%rdx), %rax
	movl	$1, %edx
	cmova	%rdx, %rax
	movq	%rdi, %rdx
	addq	%r12, %rsi
	movq	%rax, %rcx
	shrq	$3, %rcx
	salq	$4, %rcx
	addq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L2786:
	movdqu	-15(%rdx,%rsi), %xmm3
	addq	$16, %rdx
	movups	%xmm3, -16(%rdx)
	cmpq	%rcx, %rdx
	jne	.L2786
	movq	%rax, %rdx
	andq	$-8, %rdx
	leaq	(%rdx,%rdx), %rcx
	addq	%rcx, %rdi
	addq	%rcx, %r12
	cmpq	%rdx, %rax
	je	.L2783
	movzwl	(%r12), %eax
	movw	%ax, (%rdi)
	leaq	2(%rdi), %rax
	cmpq	%rax, %r13
	jbe	.L2783
	movzwl	2(%r12), %eax
	movw	%ax, 2(%rdi)
	leaq	4(%rdi), %rax
	cmpq	%rax, %r13
	jbe	.L2783
	movzwl	4(%r12), %eax
	movw	%ax, 4(%rdi)
	leaq	6(%rdi), %rax
	cmpq	%rax, %r13
	jbe	.L2783
	movzwl	6(%r12), %eax
	movw	%ax, 6(%rdi)
	leaq	8(%rdi), %rax
	cmpq	%rax, %r13
	jbe	.L2783
	movzwl	8(%r12), %eax
	movw	%ax, 8(%rdi)
	leaq	10(%rdi), %rax
	cmpq	%rax, %r13
	jbe	.L2783
	movzwl	10(%r12), %eax
	movw	%ax, 10(%rdi)
	leaq	12(%rdi), %rax
	cmpq	%rax, %r13
	jbe	.L2783
	movzwl	12(%r12), %eax
	movw	%ax, 12(%rdi)
.L2783:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2813:
	.cfi_restore_state
	movzwl	(%r12), %esi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt
.L2773:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2815:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	jmp	.L2783
	.p2align 4,,10
	.p2align 3
.L2776:
	negq	%rdi
	leaq	(%r12,%rdi,2), %rdi
	.p2align 4,,10
	.p2align 3
.L2780:
	movzwl	-30(%rdi,%rdx,2), %esi
	movq	%rdx, %rcx
	addq	$1, %rdx
	movb	%sil, (%rcx)
	cmpq	%rdx, %rbx
	ja	.L2780
	jmp	.L2773
.L2812:
	addq	%r12, %rsi
	.p2align 4,,10
	.p2align 3
.L2807:
	movq	%rax, %rdx
	movzwl	-15(%rax,%rsi), %ecx
	addq	$2, %rax
	movw	%cx, (%rdx)
	cmpq	%rax, %r13
	ja	.L2807
	jmp	.L2783
	.cfi_endproc
.LFE33125:
	.size	_ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE.part.0, .-_ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE.part.0
	.section	.text._ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE:
.LFB24063:
	.cfi_startproc
	endbr64
	testl	%edx, %edx
	jne	.L2817
	leaq	128(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2817:
	movzbl	%cl, %ecx
	jmp	_ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE.part.0
	.cfi_endproc
.LFE24063:
	.size	_ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory20NewStringFromTwoByteERKNS0_6VectorIKtEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory20NewStringFromTwoByteERKNS0_6VectorIKtEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory20NewStringFromTwoByteERKNS0_6VectorIKtEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory20NewStringFromTwoByteERKNS0_6VectorIKtEENS0_14AllocationTypeE:
.LFB24064:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %rax
	testl	%eax, %eax
	jne	.L2821
	leaq	128(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2821:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rsi
	movzbl	%dl, %ecx
	movl	%eax, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE.part.0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24064:
	.size	_ZN2v88internal7Factory20NewStringFromTwoByteERKNS0_6VectorIKtEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory20NewStringFromTwoByteERKNS0_6VectorIKtEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory20NewStringFromTwoByteEPKNS0_10ZoneVectorItEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory20NewStringFromTwoByteEPKNS0_10ZoneVectorItEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory20NewStringFromTwoByteEPKNS0_10ZoneVectorItEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory20NewStringFromTwoByteEPKNS0_10ZoneVectorItEENS0_14AllocationTypeE:
.LFB24066:
	.cfi_startproc
	endbr64
	movq	8(%rsi), %r8
	movl	%edx, %ecx
	movq	16(%rsi), %rdx
	subq	%r8, %rdx
	sarq	%rdx
	testl	%edx, %edx
	jne	.L2827
	leaq	128(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2827:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%cl, %ecx
	movq	%r8, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Factory20NewStringFromTwoByteEPKtiNS0_14AllocationTypeE.part.0
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24066:
	.size	_ZN2v88internal7Factory20NewStringFromTwoByteEPKNS0_10ZoneVectorItEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory20NewStringFromTwoByteEPKNS0_10ZoneVectorItEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory22NewSurrogatePairStringEtt,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory22NewSurrogatePairStringEtt
	.type	_ZN2v88internal7Factory22NewSurrogatePairStringEtt, @function
_ZN2v88internal7Factory22NewSurrogatePairStringEtt:
.LFB24090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	movl	$24, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$16, %rsp
	movq	-36848(%rdi), %r14
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r14, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2833
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2834:
	movl	$2, 11(%rsi)
	movq	(%rax), %rdx
	movl	$3, 7(%rdx)
	movq	(%rax), %rdx
	movw	%r13w, 15(%rdx)
	movw	%r12w, 17(%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2833:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2837
.L2835:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L2834
	.p2align 4,,10
	.p2align 3
.L2837:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L2835
	.cfi_endproc
.LFE24090:
	.size	_ZN2v88internal7Factory22NewSurrogatePairStringEtt, .-_ZN2v88internal7Factory22NewSurrogatePairStringEtt
	.section	.text._ZN2v88internal7Factory8NewErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory8NewErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.type	_ZN2v88internal7Factory8NewErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, @function
_ZN2v88internal7Factory8NewErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_:
.LFB24162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1607(%rax), %r8
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2839
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2840:
	addq	$24, %rsp
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_
	.p2align 4,,10
	.p2align 3
.L2839:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2843
.L2841:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2840
	.p2align 4,,10
	.p2align 3
.L2843:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2841
	.cfi_endproc
.LFE24162:
	.size	_ZN2v88internal7Factory8NewErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, .-_ZN2v88internal7Factory8NewErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.section	.text._ZN2v88internal7Factory12NewEvalErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory12NewEvalErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.type	_ZN2v88internal7Factory12NewEvalErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, @function
_ZN2v88internal7Factory12NewEvalErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_:
.LFB24163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1623(%rax), %r8
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2845
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2846:
	addq	$24, %rsp
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_
	.p2align 4,,10
	.p2align 3
.L2845:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2849
.L2847:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2846
	.p2align 4,,10
	.p2align 3
.L2849:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2847
	.cfi_endproc
.LFE24163:
	.size	_ZN2v88internal7Factory12NewEvalErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, .-_ZN2v88internal7Factory12NewEvalErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.section	.text._ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.type	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, @function
_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_:
.LFB24164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1719(%rax), %r8
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2851
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2852:
	addq	$24, %rsp
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_
	.p2align 4,,10
	.p2align 3
.L2851:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2855
.L2853:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2852
	.p2align 4,,10
	.p2align 3
.L2855:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2853
	.cfi_endproc
.LFE24164:
	.size	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, .-_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.section	.text._ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.type	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, @function
_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_:
.LFB24165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1727(%rax), %r8
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2857
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2858:
	addq	$24, %rsp
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_
	.p2align 4,,10
	.p2align 3
.L2857:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2861
.L2859:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2858
	.p2align 4,,10
	.p2align 3
.L2861:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2859
	.cfi_endproc
.LFE24165:
	.size	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, .-_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.section	.text._ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.type	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, @function
_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_:
.LFB24166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1759(%rax), %r8
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2863
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2864:
	addq	$24, %rsp
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_
	.p2align 4,,10
	.p2align 3
.L2863:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2867
.L2865:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2864
	.p2align 4,,10
	.p2align 3
.L2867:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2865
	.cfi_endproc
.LFE24166:
	.size	_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, .-_ZN2v88internal7Factory14NewSyntaxErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.section	.text._ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.type	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, @function
_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_:
.LFB24167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1767(%rax), %r8
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2869
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2870:
	addq	$24, %rsp
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_
	.p2align 4,,10
	.p2align 3
.L2869:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2873
.L2871:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2870
	.p2align 4,,10
	.p2align 3
.L2873:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2871
	.cfi_endproc
.LFE24167:
	.size	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, .-_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.section	.text._ZN2v88internal7Factory19NewWasmCompileErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory19NewWasmCompileErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.type	_ZN2v88internal7Factory19NewWasmCompileErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, @function
_ZN2v88internal7Factory19NewWasmCompileErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_:
.LFB24168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1783(%rax), %r8
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2875
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2876:
	addq	$24, %rsp
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_
	.p2align 4,,10
	.p2align 3
.L2875:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2879
.L2877:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2876
	.p2align 4,,10
	.p2align 3
.L2879:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2877
	.cfi_endproc
.LFE24168:
	.size	_ZN2v88internal7Factory19NewWasmCompileErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, .-_ZN2v88internal7Factory19NewWasmCompileErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.section	.text._ZN2v88internal7Factory16NewWasmLinkErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory16NewWasmLinkErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.type	_ZN2v88internal7Factory16NewWasmLinkErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, @function
_ZN2v88internal7Factory16NewWasmLinkErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_:
.LFB24169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1791(%rax), %r8
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2881
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2882:
	addq	$24, %rsp
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_
	.p2align 4,,10
	.p2align 3
.L2881:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2885
.L2883:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2882
	.p2align 4,,10
	.p2align 3
.L2885:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2883
	.cfi_endproc
.LFE24169:
	.size	_ZN2v88internal7Factory16NewWasmLinkErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, .-_ZN2v88internal7Factory16NewWasmLinkErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.section	.text._ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.type	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, @function
_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_:
.LFB24170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1799(%rax), %r8
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2887
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2888:
	addq	$24, %rsp
	movq	%r15, %r9
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_
	.p2align 4,,10
	.p2align 3
.L2887:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2891
.L2889:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2888
	.p2align 4,,10
	.p2align 3
.L2891:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2889
	.cfi_endproc
.LFE24170:
	.size	_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_, .-_ZN2v88internal7Factory19NewWasmRuntimeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_
	.section	.text._ZN2v88internal7Factory11NewWeakCellEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory11NewWeakCellEv
	.type	_ZN2v88internal7Factory11NewWeakCellEv, @function
_ZN2v88internal7Factory11NewWeakCellEv:
.LFB24175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$72, %esi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$16, %rsp
	movq	-36864(%rdi), %r12
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2893
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2893:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2897
.L2895:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2897:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L2895
	.cfi_endproc
.LFE24175:
	.size	_ZN2v88internal7Factory11NewWeakCellEv, .-_ZN2v88internal7Factory11NewWeakCellEv
	.section	.text._ZN2v88internal7Factory12NewScopeInfoEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory12NewScopeInfoEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory12NewScopeInfoEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory12NewScopeInfoEiNS0_14AllocationTypeE:
.LFB24180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$37592, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	-37504(%rdi), %r13
	cmpl	$134217726, %esi
	ja	.L2910
.L2899:
	leal	16(,%rbx,8), %r14d
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	%r14d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	cmpl	$131072, %r14d
	jle	.L2903
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L2911
.L2903:
	movq	200(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2912
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L2905:
	movslq	%ebx, %rcx
	salq	$32, %rbx
	movq	%rbx, 7(%rsi)
	movq	(%r8), %rax
	leaq	15(%rax), %rdi
	movq	%r13, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$16, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2912:
	.cfi_restore_state
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L2913
.L2906:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L2905
	.p2align 4,,10
	.p2align 3
.L2910:
	leaq	.LC0(%rip), %rsi
	movl	%edx, -44(%rbp)
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-44(%rbp), %edx
	movq	-40(%rbp), %rdi
	jmp	.L2899
	.p2align 4,,10
	.p2align 3
.L2911:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L2904
	.p2align 4,,10
	.p2align 3
.L2914:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L2903
.L2904:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L2914
	jmp	.L2903
	.p2align 4,,10
	.p2align 3
.L2913:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L2906
	.cfi_endproc
.LFE24180:
	.size	_ZN2v88internal7Factory12NewScopeInfoEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory12NewScopeInfoEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory23NewSourceTextModuleInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory23NewSourceTextModuleInfoEv
	.type	_ZN2v88internal7Factory23NewSourceTextModuleInfoEv, @function
_ZN2v88internal7Factory23NewSourceTextModuleInfoEv:
.LFB24181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$64, %esi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$37592, %rdi
	subq	$16, %rsp
	movq	-37504(%rdi), %r12
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	movq	512(%rbx), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2916
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L2917:
	movabsq	$25769803776, %rax
	movl	$6, %ecx
	movq	%rax, 7(%rsi)
	movq	(%r8), %rax
	leaq	15(%rax), %rdi
	movq	%r12, %rax
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$16, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2916:
	.cfi_restore_state
	movq	41088(%rbx), %r8
	cmpq	41096(%rbx), %r8
	je	.L2920
.L2918:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r8)
	jmp	.L2917
	.p2align 4,,10
	.p2align 3
.L2920:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L2918
	.cfi_endproc
.LFE24181:
	.size	_ZN2v88internal7Factory23NewSourceTextModuleInfoEv, .-_ZN2v88internal7Factory23NewSourceTextModuleInfoEv
	.section	.text._ZN2v88internal7Factory15NewPreparseDataEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory15NewPreparseDataEii
	.type	_ZN2v88internal7Factory15NewPreparseDataEii, @function
_ZN2v88internal7Factory15NewPreparseDataEii:
.LFB24182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	23(%rsi), %eax
	xorl	%r8d, %r8d
	movl	$1, %ecx
	andl	$-8, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	leal	(%rax,%rdx,8), %esi
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	addq	$37592, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	%edx, %ebx
	movl	$1, %edx
	subq	$16, %rsp
	movq	-37016(%rdi), %r12
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2922
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L2923:
	movl	%r14d, 7(%rsi)
	movq	(%r12), %rax
	movslq	%ebx, %rcx
	movl	%ebx, 11(%rax)
	movq	(%r12), %rsi
	movq	104(%r13), %rax
	movl	7(%rsi), %ebx
	leal	23(%rbx), %edx
	andl	$-8, %edx
	movslq	%edx, %rdx
	leaq	-1(%rsi,%rdx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	(%r12), %rcx
	movl	7(%rcx), %edx
	leal	16(%rdx), %eax
	addl	$23, %edx
	andl	$-8, %edx
	subl	%eax, %edx
	je	.L2925
	cltq
	movslq	%edx, %rdx
	xorl	%esi, %esi
	leaq	-1(%rcx,%rax), %rdi
	call	memset@PLT
.L2925:
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2922:
	.cfi_restore_state
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L2930
.L2924:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L2923
	.p2align 4,,10
	.p2align 3
.L2930:
	movq	%r13, %rdi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2924
	.cfi_endproc
.LFE24182:
	.size	_ZN2v88internal7Factory15NewPreparseDataEii, .-_ZN2v88internal7Factory15NewPreparseDataEii
	.section	.text._ZN2v88internal7Factory36NewUncompiledDataWithoutPreparseDataENS0_6HandleINS0_6StringEEEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory36NewUncompiledDataWithoutPreparseDataENS0_6HandleINS0_6StringEEEii
	.type	_ZN2v88internal7Factory36NewUncompiledDataWithoutPreparseDataENS0_6HandleINS0_6StringEEEii, @function
_ZN2v88internal7Factory36NewUncompiledDataWithoutPreparseDataENS0_6HandleINS0_6StringEEEii:
.LFB24183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leaq	680(%rdi), %rsi
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2932
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L2933:
	leaq	_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_14UncompiledData10InitializeES5_NS1_6StringEiiSt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SE_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal14UncompiledData10InitializeES3_NS2_6StringEiiSt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES6_EEEd_UlS6_S7_S6_E_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation(%rip), %rcx
	movq	(%r15), %r15
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	(%r12), %rbx
	movq	%r15, 7(%rbx)
	leaq	7(%rbx), %rax
	testb	$1, %r15b
	je	.L2940
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -136(%rbp)
	testl	$262144, %edx
	jne	.L2952
	andl	$24, %edx
	je	.L2940
.L2956:
	movq	%rbx, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L2953
	.p2align 4,,10
	.p2align 3
.L2940:
	cmpq	$0, -80(%rbp)
	movq	%rbx, -104(%rbp)
	movq	%rax, -112(%rbp)
	movq	%r15, -120(%rbp)
	je	.L2954
	leaq	-96(%rbp), %r15
	leaq	-120(%rbp), %rcx
	leaq	-112(%rbp), %rdx
	leaq	-104(%rbp), %rsi
	movq	%r15, %rdi
	call	*-72(%rbp)
	movl	%r14d, 15(%rbx)
	movl	%r13d, 19(%rbx)
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L2939
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
.L2939:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2955
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2952:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rcx
	movq	-144(%rbp), %rax
	movq	8(%rcx), %rdx
	andl	$24, %edx
	jne	.L2956
	jmp	.L2940
	.p2align 4,,10
	.p2align 3
.L2932:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L2957
.L2934:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L2933
	.p2align 4,,10
	.p2align 3
.L2953:
	movq	%rax, %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-136(%rbp), %rax
	jmp	.L2940
	.p2align 4,,10
	.p2align 3
.L2957:
	movq	%rbx, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2934
.L2954:
	call	_ZSt25__throw_bad_function_callv@PLT
.L2955:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24183:
	.size	_ZN2v88internal7Factory36NewUncompiledDataWithoutPreparseDataENS0_6HandleINS0_6StringEEEii, .-_ZN2v88internal7Factory36NewUncompiledDataWithoutPreparseDataENS0_6HandleINS0_6StringEEEii
	.section	.text._ZN2v88internal7Factory33NewUncompiledDataWithPreparseDataENS0_6HandleINS0_6StringEEEiiNS2_INS0_12PreparseDataEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory33NewUncompiledDataWithPreparseDataENS0_6HandleINS0_6StringEEEiiNS2_INS0_12PreparseDataEEE
	.type	_ZN2v88internal7Factory33NewUncompiledDataWithPreparseDataENS0_6HandleINS0_6StringEEEiiNS2_INS0_12PreparseDataEEE, @function
_ZN2v88internal7Factory33NewUncompiledDataWithPreparseDataENS0_6HandleINS0_6StringEEEiiNS2_INS0_12PreparseDataEEE:
.LFB24184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	leaq	688(%rdi), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$168, %rsp
	movl	%edx, -188(%rbp)
	movl	%ecx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2959
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L2960:
	movq	0(%r13), %rcx
	leaq	-96(%rbp), %r14
	leaq	-128(%rbp), %r13
	movq	(%rbx), %rbx
	leaq	_ZNSt17_Function_handlerIFvN2v88internal10HeapObjectENS1_14FullObjectSlotES2_EZNS1_30UncompiledDataWithPreparseData10InitializeES5_NS1_6StringEiiNS1_12PreparseDataESt8functionIS4_EEd_UlS2_S3_S2_E_E9_M_invokeERKSt9_Any_dataOS2_OS3_SF_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v88internal30UncompiledDataWithPreparseData10InitializeES3_NS2_6StringEiiNS2_12PreparseDataESt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES7_EEEd_UlS7_S8_S7_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation(%rip), %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%rdi, %xmm0
	movq	%rax, %xmm1
	movq	%r14, %rdi
	movq	%rcx, -184(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -112(%rbp)
	movq	(%r12), %r15
	movq	$0, -80(%rbp)
	call	_ZNSt14_Function_base13_Base_managerIZN2v88internal30UncompiledDataWithPreparseData10InitializeES3_NS2_6StringEiiNS2_12PreparseDataESt8functionIFvNS2_10HeapObjectENS2_14FullObjectSlotES7_EEEd_UlS7_S8_S7_E_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation
	movdqa	-112(%rbp), %xmm2
	movq	-184(%rbp), %rcx
	leaq	7(%r15), %rax
	movaps	%xmm2, -80(%rbp)
	movq	%rcx, 7(%r15)
	testb	$1, %cl
	je	.L2973
	movq	%rcx, %r10
	andq	$-262144, %r10
	movq	8(%r10), %rdx
	movq	%r10, -184(%rbp)
	testl	$262144, %edx
	jne	.L2994
	andl	$24, %edx
	je	.L2973
.L2999:
	movq	%r15, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L2995
	.p2align 4,,10
	.p2align 3
.L2973:
	cmpq	$0, -80(%rbp)
	movq	%r15, -136(%rbp)
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	je	.L2970
	leaq	-152(%rbp), %rcx
	leaq	-144(%rbp), %rdx
	movq	%r14, %rdi
	leaq	-136(%rbp), %rsi
	call	*-72(%rbp)
	movl	-188(%rbp), %eax
	movl	%eax, 15(%r15)
	movl	-192(%rbp), %eax
	movl	%eax, 19(%r15)
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L2966
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L2966:
	movq	%rbx, 23(%r15)
	leaq	23(%r15), %r14
	testb	$1, %bl
	je	.L2972
	movq	%rbx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -184(%rbp)
	testl	$262144, %eax
	jne	.L2996
	testb	$24, %al
	je	.L2972
.L3000:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2997
	.p2align 4,,10
	.p2align 3
.L2972:
	cmpq	$0, -112(%rbp)
	movq	%r15, -160(%rbp)
	movq	%r14, -168(%rbp)
	movq	%rbx, -176(%rbp)
	je	.L2970
	leaq	-176(%rbp), %rcx
	leaq	-168(%rbp), %rdx
	movq	%r13, %rdi
	leaq	-160(%rbp), %rsi
	call	*-104(%rbp)
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L2971
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L2971:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2998
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2994:
	.cfi_restore_state
	movq	%rcx, %rdx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rcx, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %r10
	movq	-208(%rbp), %rcx
	movq	-200(%rbp), %rax
	movq	8(%r10), %rdx
	andl	$24, %edx
	jne	.L2999
	jmp	.L2973
	.p2align 4,,10
	.p2align 3
.L2996:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %rcx
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3000
	jmp	.L2972
	.p2align 4,,10
	.p2align 3
.L2959:
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L3001
.L2961:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L2960
	.p2align 4,,10
	.p2align 3
.L2995:
	movq	%rcx, %rdx
	movq	%rax, %rsi
	movq	%r15, %rdi
	movq	%rcx, -200(%rbp)
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-200(%rbp), %rcx
	movq	-184(%rbp), %rax
	jmp	.L2973
	.p2align 4,,10
	.p2align 3
.L2997:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2972
	.p2align 4,,10
	.p2align 3
.L3001:
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L2961
.L2970:
	call	_ZSt25__throw_bad_function_callv@PLT
.L2998:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24184:
	.size	_ZN2v88internal7Factory33NewUncompiledDataWithPreparseDataENS0_6HandleINS0_6StringEEEiiNS2_INS0_12PreparseDataEEE, .-_ZN2v88internal7Factory33NewUncompiledDataWithPreparseDataENS0_6HandleINS0_6StringEEEiiNS2_INS0_12PreparseDataEEE
	.section	.text._ZN2v88internal7Factory20NewCodeDataContainerEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory20NewCodeDataContainerEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory20NewCodeDataContainerEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory20NewCodeDataContainerEiNS0_14AllocationTypeE:
.LFB24186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	leaq	472(%rdi), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3003
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3004:
	movq	88(%rbx), %rdx
	movq	%rdx, 7(%rsi)
	movq	(%rax), %rdx
	movl	%r12d, 15(%rdx)
	movq	(%rax), %rdx
	movl	$0, 19(%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3003:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L3007
.L3005:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L3004
	.p2align 4,,10
	.p2align 3
.L3007:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L3005
	.cfi_endproc
.LFE24186:
	.size	_ZN2v88internal7Factory20NewCodeDataContainerEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory20NewCodeDataContainerEiNS0_14AllocationTypeE
	.section	.rodata._ZN2v88internal7Factory11CodeBuilder13BuildInternalEb.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"0 <= stack_slots && stack_slots < StackSlotsField::kMax"
	.section	.text._ZN2v88internal7Factory11CodeBuilder13BuildInternalEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory11CodeBuilder13BuildInternalEb
	.type	_ZN2v88internal7Factory11CodeBuilder13BuildInternalEb, @function
_ZN2v88internal7Factory11CodeBuilder13BuildInternalEb:
.LFB24002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%r14, %rdi
	movl	52(%rax), %esi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE
	cmpb	$0, 56(%r15)
	movq	%rax, -72(%rbp)
	je	.L3033
	movl	36(%r15), %edx
	testl	$-17, %edx
	jne	.L3034
	movq	(%r15), %rbx
	testl	%edx, %edx
	leaq	1144(%rbx), %rax
	leaq	1152(%rbx), %r12
	cmove	%rax, %r12
	jmp	.L3011
	.p2align 4,,10
	.p2align 3
.L3033:
	movl	$1, %edx
.L3009:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory20NewCodeDataContainerEiNS0_14AllocationTypeE
	movl	36(%r15), %edx
	movq	%rax, %r12
	movq	(%rax), %rax
	movl	%edx, 15(%rax)
	movq	(%r15), %rbx
.L3011:
	movq	8(%r15), %rdx
	movl	12(%rdx), %eax
	addl	$7, %eax
	andl	$-8, %eax
	cmpq	$0, 56(%rdx)
	je	.L3012
	addl	64(%rdx), %eax
	addl	$15, %eax
	andl	$-8, %eax
.L3012:
	leal	95(%rax), %r10d
	leaq	37592(%rbx), %rax
	andl	$-32, %r10d
	cmpb	$0, 37968(%rbx)
	movq	%rax, -80(%rbp)
	je	.L3013
	cmpq	$0, 37976(%rbx)
	je	.L3042
.L3013:
	movl	%r10d, -80(%rbp)
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	%r10d, %esi
	movl	$2, %edx
	leaq	37592(%rbx), %rdi
	testb	%r13b, %r13b
	je	.L3014
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movl	-80(%rbp), %r10d
	movq	%rax, %rsi
.L3015:
	cmpb	$0, 57(%r15)
	je	.L3043
.L3018:
	movq	216(%r14), %rax
	movq	%rax, -1(%rsi)
	movq	(%r15), %r13
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3019
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L3020:
	movq	8(%r15), %rax
	cmpq	$0, 56(%rax)
	movl	12(%rax), %eax
	setne	-80(%rbp)
	movl	%eax, 39(%rsi)
	movq	-72(%rbp), %rax
	movq	(%r14), %r13
	movq	(%rax), %rdx
	leaq	7(%r13), %rsi
	movq	%r13, %rdi
	movq	%rdx, 7(%r13)
	movq	%rdx, -72(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-72(%rbp), %rdx
	testb	$1, %dl
	je	.L3022
	movq	-88(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3022:
	movl	60(%r15), %edx
	movq	(%r14), %rcx
	movzbl	58(%r15), %eax
	movl	16(%r15), %esi
	cmpl	$16777214, %edx
	ja	.L3044
	sall	$7, %edx
	sall	$6, %eax
	movzbl	-80(%rbp), %r13d
	addl	%esi, %esi
	orl	%edx, %eax
	orl	%esi, %eax
	orl	%eax, %r13d
	movl	%r13d, 43(%rcx)
	movl	32(%r15), %edx
	movq	(%r14), %rax
	movl	%edx, 59(%rax)
	movq	(%r14), %r13
	movq	(%r12), %r12
	leaq	31(%r13), %rsi
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r12, 31(%r13)
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	je	.L3024
	movq	-72(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3024:
	movq	48(%r15), %rax
	movq	(%r14), %r12
	movq	(%rax), %r13
	leaq	15(%r12), %rsi
	movq	%r12, %rdi
	movq	%r13, 15(%r12)
	movq	%r13, %rdx
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	je	.L3025
	movq	-72(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3025:
	movq	40(%r15), %rax
	movq	(%r14), %r12
	movq	(%rax), %r13
	leaq	23(%r12), %rsi
	movq	%r12, %rdi
	movq	%r13, 23(%r12)
	movq	%r13, %rdx
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	je	.L3026
	movq	-72(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3026:
	movq	8(%r15), %rax
	movl	16(%rax), %edx
	movq	(%r14), %rax
	movl	%edx, 47(%rax)
	movq	8(%r15), %rax
	movl	24(%rax), %edx
	movq	(%r14), %rax
	movl	%edx, 51(%rax)
	movq	8(%r15), %rax
	movl	40(%rax), %edx
	movq	(%r14), %rax
	movl	%edx, 55(%rax)
	movq	24(%r15), %r12
	testq	%r12, %r12
	je	.L3027
	movq	(%r15), %rax
	movq	45520(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3028
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal29BuiltinsConstantsTableBuilder18PatchSelfReferenceENS0_6HandleINS0_6ObjectEEENS2_INS0_4CodeEEE@PLT
.L3028:
	movq	(%r14), %rax
	movq	%rax, (%r12)
.L3027:
	movq	(%r14), %rax
	movq	8(%r15), %rdx
	leaq	-64(%rbp), %r12
	leaq	37592(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal4Code15CopyFromNoFlushEPNS0_4HeapERKNS0_8CodeDescE@PLT
	movq	(%r14), %rcx
	testb	$1, 43(%rcx)
	je	.L3029
	movl	39(%rcx), %eax
	addl	$71, %eax
	andl	$-8, %eax
	cltq
	addq	%rcx, %rax
	movslq	-1(%rax), %rdx
	leaq	7(%rax,%rdx), %rdi
	leaq	63(%rcx), %rax
	movl	%edi, %edx
	subl	%eax, %edx
.L3030:
	addl	$7, %edx
	xorl	%esi, %esi
	andl	$-8, %edx
	addl	$95, %edx
	andl	$-32, %edx
	movslq	%edx, %rax
	leaq	-1(%rcx), %rdx
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	leaq	(%rax,%rcx), %rdx
	call	memset@PLT
	movq	(%r14), %rax
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal4Code11FlushICacheEv@PLT
	cmpb	$0, 37968(%rbx)
	je	.L3031
	cmpq	$0, 37976(%rbx)
	je	.L3045
.L3031:
	movq	%r14, %rax
.L3017:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3046
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3014:
	.cfi_restore_state
	call	_ZN2v88internal4Heap25AllocateRawWithLightRetryEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movl	-80(%rbp), %r10d
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.L3015
	xorl	%eax, %eax
	cmpb	$0, 37968(%rbx)
	je	.L3017
	cmpq	$0, 37976(%rbx)
	jne	.L3017
	leaq	37592(%rbx), %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal4Heap30ProtectUnprotectedMemoryChunksEv@PLT
	movb	$0, 40568(%rbx)
	movq	-72(%rbp), %rax
	jmp	.L3017
	.p2align 4,,10
	.p2align 3
.L3019:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L3047
.L3021:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L3020
	.p2align 4,,10
	.p2align 3
.L3042:
	movb	$1, 40568(%rbx)
	jmp	.L3013
	.p2align 4,,10
	.p2align 3
.L3034:
	movl	$4, %edx
	jmp	.L3009
	.p2align 4,,10
	.p2align 3
.L3029:
	movslq	39(%rcx), %rax
	movq	%rax, %rdx
	leaq	63(%rcx,%rax), %rdi
	jmp	.L3030
	.p2align 4,,10
	.p2align 3
.L3043:
	movl	%r10d, %edx
	leaq	37592(%rbx), %rdi
	call	_ZN2v88internal4Heap19EnsureImmovableCodeENS0_10HeapObjectEi@PLT
	movq	%rax, %rsi
	jmp	.L3018
	.p2align 4,,10
	.p2align 3
.L3045:
	leaq	37592(%rbx), %rdi
	call	_ZN2v88internal4Heap30ProtectUnprotectedMemoryChunksEv@PLT
	movb	$0, 40568(%rbx)
	jmp	.L3031
	.p2align 4,,10
	.p2align 3
.L3044:
	leaq	.LC24(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3047:
	movq	%r13, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3021
.L3046:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24002:
	.size	_ZN2v88internal7Factory11CodeBuilder13BuildInternalEb, .-_ZN2v88internal7Factory11CodeBuilder13BuildInternalEb
	.section	.text._ZN2v88internal7Factory11CodeBuilder8TryBuildEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory11CodeBuilder8TryBuildEv
	.type	_ZN2v88internal7Factory11CodeBuilder8TryBuildEv, @function
_ZN2v88internal7Factory11CodeBuilder8TryBuildEv:
.LFB24007:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal7Factory11CodeBuilder13BuildInternalEb
	.cfi_endproc
.LFE24007:
	.size	_ZN2v88internal7Factory11CodeBuilder8TryBuildEv, .-_ZN2v88internal7Factory11CodeBuilder8TryBuildEv
	.section	.text._ZN2v88internal7Factory11CodeBuilder5BuildEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory11CodeBuilder5BuildEv
	.type	_ZN2v88internal7Factory11CodeBuilder5BuildEv, @function
_ZN2v88internal7Factory11CodeBuilder5BuildEv:
.LFB24008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Factory11CodeBuilder13BuildInternalEb
	testq	%rax, %rax
	je	.L3052
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3052:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24008:
	.size	_ZN2v88internal7Factory11CodeBuilder5BuildEv, .-_ZN2v88internal7Factory11CodeBuilder5BuildEv
	.section	.rodata._ZN2v88internal7Factory23NewOffHeapTrampolineForENS0_6HandleINS0_4CodeEEEm.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"(isolate()->embedded_blob()) != nullptr"
	.align 8
.LC26:
	.string	"0 != isolate()->embedded_blob_size()"
	.align 8
.LC27:
	.string	"Builtins::IsIsolateIndependentBuiltin(*code)"
	.section	.text._ZN2v88internal7Factory23NewOffHeapTrampolineForENS0_6HandleINS0_4CodeEEEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory23NewOffHeapTrampolineForENS0_6HandleINS0_4CodeEEEm
	.type	_ZN2v88internal7Factory23NewOffHeapTrampolineForENS0_6HandleINS0_4CodeEEEm, @function
_ZN2v88internal7Factory23NewOffHeapTrampolineForENS0_6HandleINS0_4CodeEEEm:
.LFB24187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	_ZNK2v88internal7Isolate13embedded_blobEv@PLT
	testq	%rax, %rax
	je	.L3079
	movq	%r12, %rdi
	call	_ZNK2v88internal7Isolate18embedded_blob_sizeEv@PLT
	testl	%eax, %eax
	je	.L3080
	movq	(%rbx), %rdi
	call	_ZN2v88internal8Builtins27IsIsolateIndependentBuiltinENS0_4CodeE@PLT
	testb	%al, %al
	je	.L3081
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	31(%rax), %rax
	movl	15(%rax), %edx
	call	_ZN2v88internal8Builtins28GenerateOffHeapTrampolineForEPNS0_7IsolateEmi@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	movq	%rsi, %r15
	andq	$-262144, %r15
	movq	24(%r15), %rax
	movzbl	376(%rax), %r14d
	testb	%r14b, %r14b
	je	.L3057
	movzbl	8(%r15), %r14d
	andl	$1, %r14d
	jne	.L3082
.L3057:
	movq	(%rbx), %rax
	movl	43(%rax), %eax
	movl	%eax, %edi
	andl	$64, %edi
	je	.L3083
.L3058:
	movl	%eax, %ecx
	shrl	$7, %ecx
	andl	$16777215, %ecx
	testl	%edi, %edi
	movl	%ecx, %edx
	setne	%dil
	cmpl	$16777215, %ecx
	je	.L3084
	movzbl	%dil, %edi
	sall	$7, %edx
	sall	$6, %edi
.L3059:
	orl	%edi, %edx
	andl	$63, %eax
	orl	%eax, %edx
	orl	$-2147483648, %edx
	movl	%edx, 43(%rsi)
	movq	(%rbx), %rax
	movl	59(%rax), %edx
	movq	0(%r13), %rax
	movl	%edx, 59(%rax)
	movq	(%rbx), %rax
	movl	47(%rax), %edx
	movq	0(%r13), %rax
	movl	%edx, 47(%rax)
	movq	(%rbx), %rax
	movl	51(%rax), %edx
	movq	0(%r13), %rax
	movl	%edx, 51(%rax)
	movq	(%rbx), %rax
	movl	55(%rax), %edx
	movq	0(%r13), %rax
	movl	%edx, 55(%rax)
	movq	1136(%r12), %r12
	movq	0(%r13), %rbx
	movq	%r12, %rdx
	movq	%r12, 7(%rbx)
	leaq	7(%rbx), %rsi
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	je	.L3061
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3061
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3085
	.p2align 4,,10
	.p2align 3
.L3061:
	testb	%r14b, %r14b
	jne	.L3086
.L3063:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3083:
	.cfi_restore_state
	movl	%eax, %ecx
	xorl	%edx, %edx
	shrl	%ecx
	andl	$31, %ecx
	cmpl	$5, %ecx
	jne	.L3059
	jmp	.L3058
	.p2align 4,,10
	.p2align 3
.L3086:
	cmpb	$0, _ZN2v88internal12FLAG_jitlessE(%rip)
	movq	%r15, %rdi
	je	.L3064
	call	_ZN2v88internal11MemoryChunk11SetReadableEv@PLT
	jmp	.L3063
	.p2align 4,,10
	.p2align 3
.L3082:
	movq	%r15, %rdi
	call	_ZN2v88internal11MemoryChunk18SetReadAndWritableEv@PLT
	movq	(%rbx), %rax
	movq	0(%r13), %rsi
	movl	43(%rax), %eax
	movl	%eax, %edi
	andl	$64, %edi
	jne	.L3058
	jmp	.L3083
	.p2align 4,,10
	.p2align 3
.L3064:
	call	_ZN2v88internal11MemoryChunk20SetReadAndExecutableEv@PLT
	jmp	.L3063
	.p2align 4,,10
	.p2align 3
.L3085:
	movq	-56(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3061
	.p2align 4,,10
	.p2align 3
.L3079:
	leaq	.LC25(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3080:
	leaq	.LC26(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3081:
	leaq	.LC27(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3084:
	leaq	.LC24(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24187:
	.size	_ZN2v88internal7Factory23NewOffHeapTrampolineForENS0_6HandleINS0_4CodeEEEm, .-_ZN2v88internal7Factory23NewOffHeapTrampolineForENS0_6HandleINS0_4CodeEEEm
	.section	.text._ZN2v88internal7Factory8CopyCodeENS0_6HandleINS0_4CodeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory8CopyCodeENS0_6HandleINS0_4CodeEEE
	.type	_ZN2v88internal7Factory8CopyCodeENS0_6HandleINS0_4CodeEEE, @function
_ZN2v88internal7Factory8CopyCodeENS0_6HandleINS0_4CodeEEE:
.LFB24188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %esi
	call	_ZN2v88internal7Factory20NewCodeDataContainerEiNS0_14AllocationTypeE
	movq	%r14, %rdi
	movq	%rax, %r13
	leaq	37592(%rbx), %rax
	movq	%rax, -88(%rbp)
	movq	(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cmpb	$0, 37968(%rbx)
	movl	%eax, %r15d
	je	.L3088
	cmpq	$0, 37976(%rbx)
	je	.L3115
.L3088:
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$2, %edx
	movl	%r15d, %esi
	leaq	37592(%rbx), %rdi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	testl	%r15d, %r15d
	leal	7(%r15), %edx
	movq	(%r12), %r12
	cmovns	%r15d, %edx
	movq	%rax, %rcx
	leaq	-1(%rax), %rdi
	leaq	-1(%r12), %rsi
	sarl	$3, %edx
	movq	%rsi, %r8
	movslq	%edx, %rdx
	cmpq	$15, %rdx
	ja	.L3089
	leaq	14(%r12), %rax
	subq	%rdi, %rax
	cmpq	$30, %rax
	leaq	-1(%rdx), %rax
	jbe	.L3090
	cmpq	$3, %rax
	jbe	.L3090
	leaq	-2(%rdx), %rax
	movq	%rcx, %r10
	xorl	%r9d, %r9d
	shrq	%rax
	subq	%r12, %r10
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L3092:
	movdqu	(%rsi), %xmm0
	addq	$1, %r9
	movups	%xmm0, (%r10,%rsi)
	addq	$16, %rsi
	cmpq	%rax, %r9
	jb	.L3092
	leaq	(%rax,%rax), %rsi
	salq	$4, %rax
	addq	%rax, %rdi
	addq	%r8, %rax
	cmpq	%rsi, %rdx
	je	.L3093
	movq	(%rax), %rax
	movq	%rax, (%rdi)
.L3093:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3096
	movq	%rcx, %rsi
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-72(%rbp), %rcx
	movq	(%rax), %rdi
	movq	%rax, %r15
.L3097:
	movq	%rcx, -72(%rbp)
	movq	0(%r13), %r13
	leaq	31(%rdi), %rsi
	movq	%r13, %rdx
	movq	%r13, 31(%rdi)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	movq	-72(%rbp), %rcx
	jne	.L3116
.L3099:
	subq	%r12, %rcx
	movq	(%r15), %rdx
	movq	%r14, %rdi
	movq	%rcx, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal4Code8RelocateEl@PLT
	movq	39656(%rbx), %rdi
	movq	(%r15), %rsi
	call	_ZN2v88internal18IncrementalMarking27ProcessBlackAllocatedObjectENS0_10HeapObjectE@PLT
	movq	(%r15), %rdi
	call	_ZN2v88internal28Heap_WriteBarrierForCodeSlowENS0_4CodeE@PLT
	cmpb	$0, 37968(%rbx)
	je	.L3101
	cmpq	$0, 37976(%rbx)
	je	.L3117
.L3101:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3118
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3115:
	.cfi_restore_state
	movb	$1, 40568(%rbx)
	jmp	.L3088
	.p2align 4,,10
	.p2align 3
.L3116:
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L3099
	movq	-80(%rbp), %rdi
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L3099
	movq	-96(%rbp), %rsi
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	jmp	.L3099
	.p2align 4,,10
	.p2align 3
.L3096:
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L3119
.L3098:
	leaq	8(%r15), %rdx
	movq	%rcx, %rdi
	movq	%rdx, 41088(%rbx)
	movq	%rcx, (%r15)
	jmp	.L3097
	.p2align 4,,10
	.p2align 3
.L3089:
	salq	$3, %rdx
	movq	%rax, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %rcx
	jmp	.L3093
	.p2align 4,,10
	.p2align 3
.L3117:
	movq	-88(%rbp), %rdi
	call	_ZN2v88internal4Heap30ProtectUnprotectedMemoryChunksEv@PLT
	movb	$0, 40568(%rbx)
	jmp	.L3101
	.p2align 4,,10
	.p2align 3
.L3090:
	movq	%rcx, %rdi
	subq	%r12, %rdi
	jmp	.L3113
	.p2align 4,,10
	.p2align 3
.L3120:
	subq	$1, %rax
.L3113:
	movq	%r8, %rsi
	leaq	(%r8,%rdi), %rdx
	addq	$8, %r8
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	testq	%rax, %rax
	jne	.L3120
	jmp	.L3093
	.p2align 4,,10
	.p2align 3
.L3119:
	movq	%rbx, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %r15
	jmp	.L3098
.L3118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24188:
	.size	_ZN2v88internal7Factory8CopyCodeENS0_6HandleINS0_4CodeEEE, .-_ZN2v88internal7Factory8CopyCodeENS0_6HandleINS0_4CodeEEE
	.section	.text._ZN2v88internal7Factory17CopyBytecodeArrayENS0_6HandleINS0_13BytecodeArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17CopyBytecodeArrayENS0_6HandleINS0_13BytecodeArrayEEE
	.type	_ZN2v88internal7Factory17CopyBytecodeArrayENS0_6HandleINS0_13BytecodeArrayEEE, @function
_ZN2v88internal7Factory17CopyBytecodeArrayENS0_6HandleINS0_13BytecodeArrayEEE:
.LFB24189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	addq	$37592, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	-37128(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movslq	11(%rax), %rsi
	addl	$61, %esi
	andl	$-8, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%r12, -1(%rax)
	movq	%rax, %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3122
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L3123:
	movq	(%rbx), %rax
	movslq	11(%rax), %rax
	salq	$32, %rax
	movq	%rax, 7(%rsi)
	movq	(%rbx), %rax
	movl	39(%rax), %edx
	movq	(%r12), %rax
	movl	%edx, 39(%rax)
	movq	(%rbx), %rax
	movq	(%r12), %rdx
	movl	43(%rax), %eax
	andl	$-8, %eax
	movl	%eax, 43(%rdx)
	movq	(%rbx), %rax
	movq	(%r12), %rdx
	movl	47(%rax), %eax
	testl	%eax, %eax
	jne	.L3144
	movl	$0, 47(%rdx)
.L3126:
	movq	(%rbx), %rax
	movq	(%r12), %r14
	movq	15(%rax), %r13
	leaq	15(%r14), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r13, 15(%r14)
	movq	%r13, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L3145
.L3127:
	movq	(%rbx), %rax
	movq	(%r12), %r14
	movq	23(%rax), %r13
	leaq	23(%r14), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r13, 23(%r14)
	movq	%r13, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L3146
.L3129:
	movq	(%rbx), %rax
	movq	(%r12), %r14
	movq	31(%rax), %r13
	leaq	31(%r14), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r13, 31(%r14)
	movq	%r13, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L3147
.L3131:
	movq	(%rbx), %rax
	leaq	-64(%rbp), %rdi
	movzbl	51(%rax), %edx
	movq	(%r12), %rax
	movb	%dl, 51(%rax)
	movq	(%rbx), %rdx
	movq	(%r12), %rax
	movzbl	52(%rdx), %edx
	movb	%dl, 52(%rax)
	movq	(%rbx), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rsi
	call	_ZN2v88internal13BytecodeArray15CopyBytecodesToES1_@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3148
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3144:
	.cfi_restore_state
	movl	%eax, 47(%rdx)
	jmp	.L3126
	.p2align 4,,10
	.p2align 3
.L3145:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3127
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3127
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L3147:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3131
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3131
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L3146:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3129
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3129
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3129
	.p2align 4,,10
	.p2align 3
.L3122:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L3149
.L3124:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L3123
	.p2align 4,,10
	.p2align 3
.L3149:
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L3124
.L3148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24189:
	.size	_ZN2v88internal7Factory17CopyBytecodeArrayENS0_6HandleINS0_13BytecodeArrayEEE, .-_ZN2v88internal7Factory17CopyBytecodeArrayENS0_6HandleINS0_13BytecodeArrayEEE
	.section	.text._ZN2v88internal7Factory22InitializeJSObjectBodyENS0_6HandleINS0_8JSObjectEEENS2_INS0_3MapEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory22InitializeJSObjectBodyENS0_6HandleINS0_8JSObjectEEENS2_INS0_3MapEEEi
	.type	_ZN2v88internal7Factory22InitializeJSObjectBodyENS0_6HandleINS0_8JSObjectEEENS2_INS0_3MapEEEi, @function
_ZN2v88internal7Factory22InitializeJSObjectBodyENS0_6HandleINS0_8JSObjectEEENS2_INS0_3MapEEEi:
.LFB24194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	(%rdx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	7(%rdi), %eax
	sall	$3, %eax
	cmpl	%eax, %ecx
	je	.L3150
	movl	15(%rdi), %r8d
	shrl	$29, %r8d
	je	.L3152
	movq	64(%r12), %r9
	movq	(%rsi), %r10
	movq	88(%r12), %r11
	movzbl	7(%rdi), %eax
	sall	$3, %eax
	cmpq	%r11, %r9
	je	.L3154
	movzbl	9(%rdi), %esi
	cmpl	$2, %esi
	jg	.L3171
.L3157:
	sall	$3, %esi
	movl	%eax, %r13d
	subl	%esi, %r13d
	cmpl	%r13d, %ecx
	jge	.L3154
	movl	%ecx, %ebx
	movslq	%ecx, %r14
	notl	%ebx
	leaq	-1(%r10,%r14), %rsi
	leaq	7(%r10,%r14), %r14
	leaq	(%rbx,%r13), %rdi
	andl	$4294967288, %edi
	addq	%r14, %rdi
	.p2align 4,,10
	.p2align 3
.L3160:
	movq	%r11, (%rsi)
	addq	$8, %rsi
	cmpq	%rdi, %rsi
	jne	.L3160
	addl	%r13d, %ebx
	andl	$-8, %ebx
	leal	8(%rcx,%rbx), %ecx
.L3154:
	cmpl	%ecx, %eax
	jg	.L3156
	.p2align 4,,10
	.p2align 3
.L3159:
	movq	(%rdx), %rax
	leaq	-56(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -56(%rbp)
	call	_ZNK2v88internal3Map11FindRootMapEPNS0_7IsolateE@PLT
	movq	%rax, -48(%rbp)
	movl	15(%rax), %edx
	shrl	$29, %edx
	jne	.L3172
	.p2align 4,,10
	.p2align 3
.L3150:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3173
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3152:
	.cfi_restore_state
	movq	88(%r12), %r9
	movq	(%rsi), %r10
	movzbl	7(%rdi), %eax
	sall	$3, %eax
	cmpl	%eax, %ecx
	jge	.L3150
.L3156:
	subl	$1, %eax
	movslq	%ecx, %rdi
	subq	%rcx, %rax
	leaq	7(%r10,%rdi), %rcx
	leaq	-1(%r10,%rdi), %rsi
	andl	$4294967288, %eax
	addq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L3162:
	movq	%r9, (%rsi)
	addq	$8, %rsi
	cmpq	%rax, %rsi
	jne	.L3162
	testl	%r8d, %r8d
	je	.L3150
	jmp	.L3159
	.p2align 4,,10
	.p2align 3
.L3172:
	movl	15(%rax), %ecx
	movl	15(%rax), %esi
	shrl	$29, %ecx
	andl	$536870911, %esi
	leal	-1(%rcx), %edx
	sall	$29, %edx
	orl	%esi, %edx
	movl	%edx, 15(%rax)
	cmpl	$1, %ecx
	jne	.L3150
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal3Map29CompleteInobjectSlackTrackingEPNS0_7IsolateE@PLT
	jmp	.L3150
	.p2align 4,,10
	.p2align 3
.L3171:
	movzbl	7(%rdi), %edi
	subl	%esi, %edi
	movl	%edi, %esi
	jmp	.L3157
.L3173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24194:
	.size	_ZN2v88internal7Factory22InitializeJSObjectBodyENS0_6HandleINS0_8JSObjectEEENS2_INS0_3MapEEEi, .-_ZN2v88internal7Factory22InitializeJSObjectBodyENS0_6HandleINS0_8JSObjectEEENS2_INS0_3MapEEEi
	.section	.text._ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	.type	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE, @function
_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE:
.LFB24193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rbx
	movq	(%rdx), %r15
	movq	%r15, 7(%rbx)
	testb	$1, %r15b
	je	.L3184
	movq	%r15, %rcx
	leaq	7(%rbx), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L3193
	testb	$24, %al
	je	.L3184
.L3197:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3194
	.p2align 4,,10
	.p2align 3
.L3184:
	movq	(%r12), %rcx
	movq	-1(%rcx), %rdx
	movzbl	14(%rdx), %eax
	shrl	$3, %eax
	cmpl	$15, %eax
	je	.L3185
	cmpl	$5, %eax
	jbe	.L3185
	cmpb	$13, %al
	je	.L3195
	leal	-17(%rax), %esi
	cmpb	$10, %sil
	jbe	.L3196
	cmpb	$12, %al
	jne	.L3183
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	movq	-36576(%rax), %rax
	jmp	.L3180
	.p2align 4,,10
	.p2align 3
.L3185:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	movq	-37304(%rax), %rax
.L3180:
	movq	%rax, 15(%rcx)
	addq	$24, %rsp
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	$24, %ecx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory22InitializeJSObjectBodyENS0_6HandleINS0_8JSObjectEEENS2_INS0_3MapEEEi
	.p2align 4,,10
	.p2align 3
.L3193:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3197
	jmp	.L3184
	.p2align 4,,10
	.p2align 3
.L3194:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3184
	.p2align 4,,10
	.p2align 3
.L3196:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	movq	-36616(%rax), %rax
	jmp	.L3180
	.p2align 4,,10
	.p2align 3
.L3195:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	movq	-36584(%rax), %rax
	jmp	.L3180
.L3183:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24193:
	.size	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE, .-_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	.section	.text._ZN2v88internal7Factory17NewJSGlobalObjectENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewJSGlobalObjectENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal7Factory17NewJSGlobalObjectENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal7Factory17NewJSGlobalObjectENS0_6HandleINS0_10JSFunctionEEE:
.LFB24192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	41112(%rdi), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3199
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L3200:
	movl	15(%rsi), %esi
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	shrl	$9, %esi
	andl	$2046, %esi
	addl	$64, %esi
	call	_ZN2v88internal18BaseNameDictionaryINS0_16GlobalDictionaryENS0_21GlobalDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	41112(%r13), %rdi
	movq	%rax, -64(%rbp)
	movq	(%r15), %rax
	movq	39(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3202
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3203:
	movl	$0, -56(%rbp)
	movl	$24, %r12d
	movq	%r15, -72(%rbp)
	jmp	.L3212
	.p2align 4,,10
	.p2align 3
.L3238:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L3207:
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory15NewPropertyCellENS0_6HandleINS0_4NameEEENS0_14AllocationTypeE
	movq	(%rax), %rdi
	movq	%rax, %rcx
	movq	(%r14), %rax
	movq	15(%r12,%rax), %rdx
	leaq	23(%rdi), %rsi
	movq	%rdx, 23(%rdi)
	testb	$1, %dl
	je	.L3221
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -80(%rbp)
	testl	$262144, %eax
	je	.L3210
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %r8
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	8(%r8), %rax
	movq	-88(%rbp), %rdi
.L3210:
	testb	$24, %al
	je	.L3221
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3221
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L3221:
	movq	-64(%rbp), %rsi
	xorl	%r9d, %r9d
	movl	%ebx, %r8d
	movq	%r15, %rdx
	movq	%r13, %rdi
	addq	$24, %r12
	call	_ZN2v88internal18BaseNameDictionaryINS0_16GlobalDictionaryENS0_21GlobalDictionaryShapeEE3AddEPNS0_7IsolateENS0_6HandleIS2_EENS7_INS0_4NameEEENS7_INS0_6ObjectEEENS0_15PropertyDetailsEPi@PLT
.L3212:
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	movl	-56(%rbp), %ecx
	shrl	$10, %eax
	andl	$1023, %eax
	cmpl	%eax, %ecx
	jge	.L3205
	movq	(%r14), %rax
	addl	$1, %ecx
	movl	%ecx, -56(%rbp)
	movq	7(%r12,%rax), %rbx
	movq	-1(%r12,%rax), %rsi
	movq	41112(%r13), %rdi
	sarq	$32, %rbx
	andl	$56, %ebx
	orb	$-63, %bl
	testq	%rdi, %rdi
	jne	.L3238
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L3239
.L3208:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L3207
	.p2align 4,,10
	.p2align 3
.L3205:
	movq	-72(%rbp), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3213
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L3214:
	movq	-64(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r15, %rcx
	movq	%r12, %rsi
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal3Map19CopyDropDescriptorsEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %rdx
	movq	%rax, %rbx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	movq	(%rbx), %rdx
	movl	15(%rdx), %eax
	orl	$35651584, %eax
	movl	%eax, 15(%rdx)
	movq	41016(%r13), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L3240
.L3216:
	movq	-64(%rbp), %rax
	movq	(%r12), %r13
	movq	(%rax), %r14
	leaq	7(%r13), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r14, 7(%r13)
	movq	%r14, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L3217
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3217:
	movq	(%r12), %rdi
	movq	(%rbx), %rdx
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	jne	.L3241
.L3218:
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3239:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L3208
	.p2align 4,,10
	.p2align 3
.L3213:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L3242
.L3215:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L3214
	.p2align 4,,10
	.p2align 3
.L3202:
	movq	41088(%r13), %r14
	cmpq	%r14, 41096(%r13)
	je	.L3243
.L3204:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L3203
	.p2align 4,,10
	.p2align 3
.L3199:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L3244
.L3201:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L3200
	.p2align 4,,10
	.p2align 3
.L3240:
	movq	(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10MapDetailsENS0_3MapE@PLT
	jmp	.L3216
	.p2align 4,,10
	.p2align 3
.L3241:
	testb	$1, %dl
	je	.L3218
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L3218
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3218
.L3244:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L3201
.L3243:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3204
.L3242:
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L3215
	.cfi_endproc
.LFE24192:
	.size	_ZN2v88internal7Factory17NewJSGlobalObjectENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal7Factory17NewJSGlobalObjectENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0, @function
_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0:
.LFB33159:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rcx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	call	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3246
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3247:
	leaq	288(%r12), %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3246:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3250
.L3248:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3247
	.p2align 4,,10
	.p2align 3
.L3250:
	movq	%r12, %rdi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3248
	.cfi_endproc
.LFE33159:
	.size	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0, .-_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0
	.section	.text._ZN2v88internal7Factory19NewJSStringIteratorENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory19NewJSStringIteratorENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal7Factory19NewJSStringIteratorENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal7Factory19NewJSStringIteratorENS0_6HandleINS0_6StringEEE:
.LFB24095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3252
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3253:
	movq	551(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3255
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3256:
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%rax, %rbx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0
	movq	(%rbx), %r13
	movq	(%rax), %r14
	movq	%rax, %r12
	movq	%r13, 23(%r14)
	leaq	23(%r14), %r15
	testb	$1, %r13b
	je	.L3261
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L3269
	testb	$24, %al
	je	.L3261
.L3271:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3270
.L3261:
	movq	(%r12), %rax
	movq	$0, 31(%rax)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3269:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L3271
	jmp	.L3261
	.p2align 4,,10
	.p2align 3
.L3255:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3272
.L3257:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3256
	.p2align 4,,10
	.p2align 3
.L3252:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3273
.L3254:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3253
	.p2align 4,,10
	.p2align 3
.L3270:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3261
	.p2align 4,,10
	.p2align 3
.L3273:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L3254
	.p2align 4,,10
	.p2align 3
.L3272:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3257
	.cfi_endproc
.LFE24095:
	.size	_ZN2v88internal7Factory19NewJSStringIteratorENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal7Factory19NewJSStringIteratorENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal7Factory20NewFunctionPrototypeENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory20NewFunctionPrototypeENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal7Factory20NewFunctionPrototypeENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal7Factory20NewFunctionPrototypeENS0_6HandleINS0_10JSFunctionEEE:
.LFB24174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	(%rsi), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3275
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3276:
	movq	0(%r13), %rcx
	movq	23(%rcx), %rdx
	movl	47(%rdx), %edx
	andl	$31, %edx
	subl	$12, %edx
	cmpb	$1, %dl
	jbe	.L3301
	movq	23(%rcx), %rdx
	movl	47(%rdx), %edx
	movq	(%rax), %rax
	andl	$31, %edx
	leal	-9(%rdx), %ecx
	cmpb	$6, %cl
	jbe	.L3283
	cmpb	$1, %dl
	jne	.L3302
.L3283:
	movq	431(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3291
.L3297:
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3282:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0
	movq	0(%r13), %rdx
	movq	23(%rdx), %rdx
	movl	47(%rdx), %edx
	andl	$31, %edx
	leal	-9(%rdx), %ecx
	cmpb	$6, %cl
	jbe	.L3294
	cmpb	$1, %dl
	jne	.L3303
.L3294:
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3302:
	.cfi_restore_state
	movq	879(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3304
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3289:
	movq	41112(%r12), %rdi
	movq	55(%rsi), %r14
	testq	%rdi, %rdi
	jne	.L3297
	.p2align 4,,10
	.p2align 3
.L3291:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3305
.L3293:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L3282
	.p2align 4,,10
	.p2align 3
.L3275:
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	je	.L3306
.L3277:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3276
	.p2align 4,,10
	.p2align 3
.L3303:
	movq	%r13, %rcx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movl	$2, %r8d
	leaq	2304(%r12), %rdx
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-40(%rbp), %rax
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3301:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	439(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L3297
	jmp	.L3291
	.p2align 4,,10
	.p2align 3
.L3306:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L3277
	.p2align 4,,10
	.p2align 3
.L3305:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3293
	.p2align 4,,10
	.p2align 3
.L3304:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3307
.L3290:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3289
	.p2align 4,,10
	.p2align 3
.L3307:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L3290
	.cfi_endproc
.LFE24174:
	.size	_ZN2v88internal7Factory20NewFunctionPrototypeENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal7Factory20NewFunctionPrototypeENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE:
.LFB24190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	call	_ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3309
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L3310:
	movzbl	7(%rsi), %esi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	%ebx, %edx
	leaq	37592(%r12), %rdi
	sall	$3, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %r15
	testb	%bl, %bl
	je	.L3312
	movq	0(%r13), %rdx
	movq	%rdx, -1(%rax)
	testb	$1, %dl
	je	.L3318
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L3328
.L3318:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3315
.L3329:
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3316:
	leaq	288(%r12), %rdx
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3312:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%rax, -1(%r15)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L3329
.L3315:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L3330
.L3317:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%r14)
	jmp	.L3316
	.p2align 4,,10
	.p2align 3
.L3309:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3331
.L3311:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3310
	.p2align 4,,10
	.p2align 3
.L3328:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3318
	.p2align 4,,10
	.p2align 3
.L3330:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r14
	jmp	.L3317
	.p2align 4,,10
	.p2align 3
.L3331:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3311
	.cfi_endproc
.LFE24190:
	.size	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE
	.section	.rodata._ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE.str1.1,"aMS",@progbits,1
.LC28:
	.string	"ObjectWithNullProto"
	.section	.text._ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE:
.LFB24191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	subq	$16, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3333
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3334:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3336
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3337:
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	call	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3339
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3340:
	movq	%r14, %rcx
	movq	%r12, %rdi
	leaq	288(%r12), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	movq	0(%r13), %rax
	movq	-1(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3342
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3343:
	movq	%r12, %rdi
	leaq	.LC28(%rip), %rdx
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	leaq	104(%r12), %rdx
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	%rax, %r14
	movq	%rax, %rsi
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi@PLT
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3333:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3346
.L3335:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3334
	.p2align 4,,10
	.p2align 3
.L3342:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3347
.L3344:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L3343
	.p2align 4,,10
	.p2align 3
.L3339:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3348
.L3341:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3340
	.p2align 4,,10
	.p2align 3
.L3336:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L3349
.L3338:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L3337
	.p2align 4,,10
	.p2align 3
.L3346:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3335
	.p2align 4,,10
	.p2align 3
.L3349:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3338
	.p2align 4,,10
	.p2align 3
.L3348:
	movq	%r12, %rdi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3341
	.p2align 4,,10
	.p2align 3
.L3347:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3344
	.cfi_endproc
.LFE24191:
	.size	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE, .-_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory11NewExternalEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory11NewExternalEPv
	.type	_ZN2v88internal7Factory11NewExternalEPv, @function
_ZN2v88internal7Factory11NewExternalEPv:
.LFB24185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	leaq	4456(%r12), %r15
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %r14
	call	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3351
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3352:
	movq	%r12, %rdi
	leaq	288(%r12), %rdx
	movq	%r15, %rcx
	movq	%r13, %rsi
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	movq	0(%r13), %r15
	movq	(%r14), %r14
	movl	$24, %esi
	movq	-1(%r15), %rax
	leaq	-1(%r15), %r12
	movzwl	11(%rax), %edi
	cmpw	$1057, %di
	je	.L3354
	movsbl	13(%rax), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movslq	%eax, %rsi
.L3354:
	addq	%rsi, %r12
	movq	%r14, (%r12)
	testb	$1, %r14b
	je	.L3358
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L3368
	testb	$24, %al
	je	.L3358
.L3370:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3369
.L3358:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3368:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L3370
	jmp	.L3358
	.p2align 4,,10
	.p2align 3
.L3351:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3371
.L3353:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3352
	.p2align 4,,10
	.p2align 3
.L3369:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3358
	.p2align 4,,10
	.p2align 3
.L3371:
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3353
	.cfi_endproc
.LFE24185:
	.size	_ZN2v88internal7Factory11NewExternalEPv, .-_ZN2v88internal7Factory11NewExternalEPv
	.section	.text._ZN2v88internal7Factory11NewFunctionENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory11NewFunctionENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory11NewFunctionENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory11NewFunctionENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE:
.LFB24171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	%r8d, %edx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE
	movq	41112(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3373
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L3374:
	movq	-1(%rsi), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L3376
	movq	1056(%r13), %rax
	movq	%rax, 7(%rsi)
.L3377:
	movq	(%r12), %rcx
	movq	-1(%rcx), %rdx
	movzbl	14(%rdx), %eax
	shrl	$3, %eax
	cmpl	$15, %eax
	je	.L3398
	cmpl	$5, %eax
	jbe	.L3398
	cmpb	$13, %al
	je	.L3415
	leal	-17(%rax), %esi
	cmpb	$10, %sil
	jbe	.L3416
	cmpb	$12, %al
	jne	.L3383
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	movq	-36576(%rax), %rax
	jmp	.L3380
	.p2align 4,,10
	.p2align 3
.L3398:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	movq	-37304(%rax), %rax
.L3380:
	movq	%rax, 15(%rcx)
	movq	(%r12), %rdi
	movq	(%rbx), %rdx
	leaq	23(%rdi), %rsi
	movq	%rdx, 23(%rdi)
	movq	%rdx, -72(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-72(%rbp), %rdx
	testb	$1, %dl
	jne	.L3417
.L3384:
	movq	(%r12), %r8
	movq	(%rbx), %rax
	leaq	-64(%rbp), %rdi
	movq	%r8, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rdx
	movq	%rax, 47(%r8)
	leaq	47(%r8), %rsi
	testb	$1, %al
	je	.L3386
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	jne	.L3418
.L3386:
	movq	(%r12), %rbx
	movq	(%r15), %r15
	movq	%r15, 31(%rbx)
	leaq	31(%rbx), %rsi
	testb	$1, %r15b
	je	.L3395
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L3419
	testb	$24, %al
	je	.L3395
.L3424:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3420
	.p2align 4,,10
	.p2align 3
.L3395:
	movq	(%r12), %rbx
	movq	4480(%r13), %r15
	movq	%r15, 39(%rbx)
	leaq	39(%rbx), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	jne	.L3421
.L3391:
	movq	(%r14), %rax
	movl	$56, %ecx
	movzbl	13(%rax), %eax
	testb	%al, %al
	js	.L3422
.L3393:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory22InitializeJSObjectBodyENS0_6HandleINS0_8JSObjectEEENS2_INS0_3MapEEEi
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3423
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3376:
	.cfi_restore_state
	movq	288(%r13), %rax
	movq	%rax, 7(%rsi)
	jmp	.L3377
	.p2align 4,,10
	.p2align 3
.L3419:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3424
	jmp	.L3395
	.p2align 4,,10
	.p2align 3
.L3418:
	movq	%r8, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3386
	.p2align 4,,10
	.p2align 3
.L3417:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3384
	movq	-80(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3384
	movq	-88(%rbp), %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3384
	.p2align 4,,10
	.p2align 3
.L3421:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3391
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3391
	movq	-72(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r14), %rax
	movl	$56, %ecx
	movzbl	13(%rax), %eax
	testb	%al, %al
	jns	.L3393
	.p2align 4,,10
	.p2align 3
.L3422:
	movq	(%r12), %r15
	movq	96(%r13), %rbx
	movq	%rbx, 55(%r15)
	leaq	55(%r15), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %bl
	jne	.L3425
	movl	$64, %ecx
	jmp	.L3393
	.p2align 4,,10
	.p2align 3
.L3373:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L3426
.L3375:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L3374
	.p2align 4,,10
	.p2align 3
.L3420:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3395
	.p2align 4,,10
	.p2align 3
.L3425:
	movq	-72(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	movl	$64, %ecx
	jmp	.L3393
	.p2align 4,,10
	.p2align 3
.L3416:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	movq	-36616(%rax), %rax
	jmp	.L3380
	.p2align 4,,10
	.p2align 3
.L3415:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	movq	-36584(%rax), %rax
	jmp	.L3380
	.p2align 4,,10
	.p2align 3
.L3426:
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L3375
.L3423:
	call	__stack_chk_fail@PLT
.L3383:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24171:
	.size	_ZN2v88internal7Factory11NewFunctionENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory11NewFunctionENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE:
.LFB24178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_ZN2v88internal7Factory11NewFunctionENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8Compiler17PostInstantiationENS0_6HandleINS0_10JSFunctionEEE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24178:
	.size	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE:
.LFB24176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$8, %rsp
	movq	(%rdx), %rax
	movq	39(%rax), %rdx
	movq	(%rsi), %rax
	movl	47(%rax), %eax
	shrl	$15, %eax
	andl	$31, %eax
	leaq	1248(,%rax,8), %rax
	movq	-1(%rdx,%rax), %r15
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3430
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3431:
	movl	%ebx, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewFunctionENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8Compiler17PostInstantiationENS0_6HandleINS0_10JSFunctionEEE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3430:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3434
.L3432:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L3431
	.p2align 4,,10
	.p2align 3
.L3434:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3432
	.cfi_endproc
.LFE24176:
	.size	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE
	.section	.rodata._ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"new function from shared function info"
	.section	.text._ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE:
.LFB24179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	movl	%r9d, %r8d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory11NewFunctionENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE
	movq	(%r14), %r12
	movq	%rax, %r13
	movq	-1(%r12), %rax
	cmpq	%rax, 528(%rbx)
	je	.L3455
	movq	-1(%r12), %rax
	cmpq	%rax, 544(%rbx)
	je	.L3456
.L3438:
	movq	7(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L3457
.L3440:
	movq	0(%r13), %r14
	movq	%r12, %rdx
	movq	%r12, 39(%r14)
	leaq	39(%r14), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	jne	.L3458
.L3441:
	movq	%r13, %rdi
	call	_ZN2v88internal8Compiler17PostInstantiationENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3459
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3458:
	.cfi_restore_state
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3441
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3441
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3441
	.p2align 4,,10
	.p2align 3
.L3455:
	movq	544(%rbx), %rdx
	movq	%rdx, -1(%r12)
	testq	%rdx, %rdx
	jne	.L3452
.L3454:
	movq	(%r14), %r12
	jmp	.L3438
	.p2align 4,,10
	.p2align 3
.L3457:
	movq	(%r15), %rsi
	leaq	-64(%rbp), %rdi
	leaq	.LC29(%rip), %rdx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal14FeedbackVector41EvictOptimizedCodeMarkedForDeoptimizationENS0_18SharedFunctionInfoEPKc@PLT
	movq	(%r14), %r12
	jmp	.L3440
	.p2align 4,,10
	.p2align 3
.L3456:
	movq	504(%rbx), %rdx
	movq	%rdx, -1(%r12)
	testq	%rdx, %rdx
	je	.L3454
.L3452:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	jmp	.L3454
.L3459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24179:
	.size	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE:
.LFB24177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	(%rdx), %rax
	movq	39(%rax), %rdx
	movq	(%rsi), %rax
	movl	47(%rax), %eax
	shrl	$15, %eax
	andl	$31, %eax
	leaq	1248(,%rax,8), %rax
	movq	-1(%rdx,%rax), %r8
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3461
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3462:
	addq	$24, %rsp
	movl	%r15d, %r9d
	movq	%rbx, %r8
	movq	%r14, %rcx
	popq	%rbx
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE
	.p2align 4,,10
	.p2align 3
.L3461:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3465
.L3463:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L3462
	.p2align 4,,10
	.p2align 3
.L3465:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L3463
	.cfi_endproc
.LFE24177:
	.size	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS2_INS0_12FeedbackCellEEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	.type	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE, @function
_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE:
.LFB24195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	call	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3467
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3468:
	leaq	288(%r12), %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3467:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3471
.L3469:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3468
	.p2align 4,,10
	.p2align 3
.L3471:
	movq	%r12, %rdi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3469
	.cfi_endproc
.LFE24195:
	.size	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE, .-_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	.section	.text._ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	.type	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE, @function
_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE:
.LFB24196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	%edx, %esi
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	xorl	%ecx, %ecx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	call	_ZN2v88internal18BaseNameDictionaryINS0_14NameDictionaryENS0_19NameDictionaryShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r15, %rcx
	movl	%r13d, %edx
	movq	%rax, %rbx
	call	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3473
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3474:
	leaq	288(%r12), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	movq	0(%r13), %r14
	movq	(%rbx), %r12
	movq	%r12, 7(%r14)
	leaq	7(%r14), %r15
	testb	$1, %r12b
	je	.L3479
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L3487
	testb	$24, %al
	je	.L3479
.L3489:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3488
.L3479:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3487:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L3489
	jmp	.L3479
	.p2align 4,,10
	.p2align 3
.L3473:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3490
.L3475:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3474
	.p2align 4,,10
	.p2align 3
.L3488:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3479
	.p2align 4,,10
	.p2align 3
.L3490:
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3475
	.cfi_endproc
.LFE24196:
	.size	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE, .-_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	.section	.text._ZN2v88internal7Factory40NewSlowJSObjectWithPropertiesAndElementsENS0_6HandleINS0_10HeapObjectEEENS2_INS0_14NameDictionaryEEENS2_INS0_14FixedArrayBaseEEENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory40NewSlowJSObjectWithPropertiesAndElementsENS0_6HandleINS0_10HeapObjectEEENS2_INS0_14NameDictionaryEEENS2_INS0_14FixedArrayBaseEEENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory40NewSlowJSObjectWithPropertiesAndElementsENS0_6HandleINS0_10HeapObjectEEENS2_INS0_14NameDictionaryEEENS2_INS0_14FixedArrayBaseEEENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory40NewSlowJSObjectWithPropertiesAndElementsENS0_6HandleINS0_10HeapObjectEEENS2_INS0_14NameDictionaryEEENS2_INS0_14FixedArrayBaseEEENS0_14AllocationTypeE:
.LFB24197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1223(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3492
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %r9
	movq	(%rax), %rsi
	movq	%rax, %r13
.L3493:
	movq	23(%rsi), %rax
	cmpq	%rax, (%r9)
	je	.L3495
	movq	%r13, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal3Map21TransitionToPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEE@PLT
	movq	%rax, %r13
.L3495:
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	%r14d, %edx
	call	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3496
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3497:
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	288(%r12), %rdx
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	movq	(%r14), %rdi
	movq	(%r15), %r13
	movq	%r13, 7(%rdi)
	leaq	7(%rdi), %rsi
	testb	$1, %r13b
	je	.L3505
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L3516
	testb	$24, %al
	je	.L3505
.L3519:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3517
	.p2align 4,,10
	.p2align 3
.L3505:
	movq	288(%r12), %rax
	cmpq	%rax, (%rbx)
	jne	.L3518
.L3502:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3516:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	testb	$24, %al
	jne	.L3519
	jmp	.L3505
	.p2align 4,,10
	.p2align 3
.L3518:
	movl	$12, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject24GetElementsTransitionMapENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal8JSObject12MigrateToMapEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_3MapEEEi@PLT
	movq	(%r14), %r13
	movq	(%rbx), %r12
	movq	%r12, 15(%r13)
	leaq	15(%r13), %r15
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	je	.L3502
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3502
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3502
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3502
	.p2align 4,,10
	.p2align 3
.L3496:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L3520
.L3498:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L3497
	.p2align 4,,10
	.p2align 3
.L3492:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3521
.L3494:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3493
	.p2align 4,,10
	.p2align 3
.L3517:
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3505
	.p2align 4,,10
	.p2align 3
.L3521:
	movq	%r12, %rdi
	movq	%r9, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3494
	.p2align 4,,10
	.p2align 3
.L3520:
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3498
	.cfi_endproc
.LFE24197:
	.size	_ZN2v88internal7Factory40NewSlowJSObjectWithPropertiesAndElementsENS0_6HandleINS0_10HeapObjectEEENS2_INS0_14NameDictionaryEEENS2_INS0_14FixedArrayBaseEEENS0_14AllocationTypeE, .-_ZN2v88internal7Factory40NewSlowJSObjectWithPropertiesAndElementsENS0_6HandleINS0_10HeapObjectEEENS2_INS0_14NameDictionaryEEENS2_INS0_14FixedArrayBaseEEENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory32NewJSArrayWithUnverifiedElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory32NewJSArrayWithUnverifiedElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory32NewJSArrayWithUnverifiedElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory32NewJSArrayWithUnverifiedElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE:
.LFB24200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$24, %rsp
	movq	12464(%rdi), %rdx
	movq	39(%rdx), %rdx
	cmpb	$5, %al
	jbe	.L3523
.L3525:
	movq	103(%rdx), %rax
	movq	55(%rax), %rsi
.L3524:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3526
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3527:
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	call	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3529
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3530:
	leaq	288(%r12), %rdx
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rsi
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	movq	(%r15), %r12
	movq	0(%r13), %r14
	movq	%r12, 15(%r14)
	leaq	15(%r14), %r15
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	jne	.L3541
.L3532:
	movq	0(%r13), %rax
	salq	$32, %rbx
	movq	%rbx, 23(%rax)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3541:
	.cfi_restore_state
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3532
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3532
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3532
	.p2align 4,,10
	.p2align 3
.L3529:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3542
.L3531:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3530
	.p2align 4,,10
	.p2align 3
.L3526:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L3543
.L3528:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L3527
	.p2align 4,,10
	.p2align 3
.L3523:
	movzbl	%al, %eax
	leaq	664(,%rax,8), %rax
	movq	-1(%rdx,%rax), %rsi
	testq	%rsi, %rsi
	jne	.L3524
	jmp	.L3525
	.p2align 4,,10
	.p2align 3
.L3543:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3528
	.p2align 4,,10
	.p2align 3
.L3542:
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3531
	.cfi_endproc
.LFE24200:
	.size	_ZN2v88internal7Factory32NewJSArrayWithUnverifiedElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory32NewJSArrayWithUnverifiedElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE:
.LFB24199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%dl, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	call	_ZN2v88internal7Factory32NewJSArrayWithUnverifiedElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE
	movq	(%rax), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8JSObject16ValidateElementsES1_@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24199:
	.size	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory17NewJSArrayStorageENS0_12ElementsKindEiNS0_26ArrayStorageAllocationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewJSArrayStorageENS0_12ElementsKindEiNS0_26ArrayStorageAllocationModeE
	.type	_ZN2v88internal7Factory17NewJSArrayStorageENS0_12ElementsKindEiNS0_26ArrayStorageAllocationModeE, @function
_ZN2v88internal7Factory17NewJSArrayStorageENS0_12ElementsKindEiNS0_26ArrayStorageAllocationModeE:
.LFB24202:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movl	%edx, %esi
	subl	$4, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	cmpb	$1, %r8b
	jbe	.L3568
	testl	%ecx, %ecx
	jne	.L3555
	testl	%edx, %edx
	jne	.L3569
.L3551:
	leaq	288(%rdi), %rax
.L3550:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3555:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L3551
	movq	96(%rdi), %rdx
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE.constprop.0
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3568:
	.cfi_restore_state
	testl	%ecx, %ecx
	jne	.L3548
	testl	%edx, %edx
	je	.L3551
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE.part.0
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3569:
	.cfi_restore_state
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE.part.0
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3548:
	.cfi_restore_state
	testl	%edx, %edx
	je	.L3551
	xorl	%edx, %edx
	movl	%esi, -4(%rbp)
	call	_ZN2v88internal7Factory19NewFixedDoubleArrayEiNS0_14AllocationTypeE.part.0
	movl	-4(%rbp), %esi
	testl	%esi, %esi
	jle	.L3550
	movq	(%rax), %rdi
	cmpl	$1, %esi
	je	.L3556
	movl	%esi, %ecx
	leaq	15(%rdi), %rdx
	movdqa	.LC10(%rip), %xmm0
	shrl	%ecx
	salq	$4, %rcx
	addq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L3553:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L3553
	movl	%esi, %edx
	andl	$-2, %edx
	andl	$1, %esi
	je	.L3550
.L3552:
	leal	16(,%rdx,8), %edx
	movabsq	$-2251799814209537, %rsi
	movslq	%edx, %rdx
	movq	%rsi, -1(%rdi,%rdx)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3556:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L3552
	.cfi_endproc
.LFE24202:
	.size	_ZN2v88internal7Factory17NewJSArrayStorageENS0_12ElementsKindEiNS0_26ArrayStorageAllocationModeE, .-_ZN2v88internal7Factory17NewJSArrayStorageENS0_12ElementsKindEiNS0_26ArrayStorageAllocationModeE
	.section	.text._ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE:
.LFB24198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movzbl	%sil, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$24, %rsp
	testl	%ecx, %ecx
	jne	.L3571
	leaq	288(%rdi), %rsi
	movl	%edx, %ecx
	movl	%r9d, %r8d
	movl	%r13d, %edx
	call	_ZN2v88internal7Factory32NewJSArrayWithUnverifiedElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE
	movq	(%rax), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal8JSObject16ValidateElementsES1_@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3571:
	.cfi_restore_state
	addl	$1, 41104(%rdi)
	movq	41088(%rdi), %r9
	movl	%ecx, %edx
	movl	%r13d, %esi
	movl	%r8d, %ecx
	movq	41096(%rdi), %r15
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal7Factory17NewJSArrayStorageENS0_12ElementsKindEiNS0_26ArrayStorageAllocationModeE
	movl	%r13d, %edx
	movl	%ebx, %r8d
	movl	%r14d, %ecx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory32NewJSArrayWithUnverifiedElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE
	movq	-56(%rbp), %r9
	movq	(%rax), %r13
	subl	$1, 41104(%r12)
	movq	%r9, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L3573
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3573:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3574
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3574:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3578
.L3576:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r13, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3578:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L3576
	.cfi_endproc
.LFE24198:
	.size	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE, .-_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory17NewJSArrayStorageENS0_6HandleINS0_7JSArrayEEEiiNS0_26ArrayStorageAllocationModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewJSArrayStorageENS0_6HandleINS0_7JSArrayEEEiiNS0_26ArrayStorageAllocationModeE
	.type	_ZN2v88internal7Factory17NewJSArrayStorageENS0_6HandleINS0_7JSArrayEEEiiNS0_26ArrayStorageAllocationModeE, @function
_ZN2v88internal7Factory17NewJSArrayStorageENS0_6HandleINS0_7JSArrayEEEiiNS0_26ArrayStorageAllocationModeE:
.LFB24201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	testl	%ecx, %ecx
	je	.L3591
	movq	41088(%rdi), %rax
	addl	$1, 41104(%rdi)
	movl	%edx, %r13d
	movl	%ecx, %edx
	movq	41096(%rdi), %r14
	movl	%r8d, %ecx
	movq	%rax, -56(%rbp)
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	movzbl	14(%rax), %esi
	shrl	$3, %esi
	call	_ZN2v88internal7Factory17NewJSArrayStorageENS0_12ElementsKindEiNS0_26ArrayStorageAllocationModeE
	movq	(%rbx), %rdi
	movq	(%rax), %r15
	leaq	15(%rdi), %rsi
	movq	%r15, 15(%rdi)
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	jne	.L3592
.L3584:
	movq	(%rbx), %rax
	salq	$32, %r13
	movq	%r13, 23(%rax)
	subl	$1, 41104(%r12)
	movq	-56(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L3579
	movq	%r14, 41096(%r12)
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L3591:
	.cfi_restore_state
	movq	(%rsi), %r13
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r14
	movq	%r14, 23(%r13)
	leaq	23(%r13), %r15
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L3581
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3581:
	movq	(%rbx), %r13
	movq	288(%r12), %r12
	movq	%r12, 15(%r13)
	leaq	15(%r13), %r14
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	jne	.L3593
.L3579:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3592:
	.cfi_restore_state
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3584
	movq	-64(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3584
	movq	-72(%rbp), %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3584
	.p2align 4,,10
	.p2align 3
.L3593:
	addq	$40, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	.cfi_endproc
.LFE24201:
	.size	_ZN2v88internal7Factory17NewJSArrayStorageENS0_6HandleINS0_7JSArrayEEEiiNS0_26ArrayStorageAllocationModeE, .-_ZN2v88internal7Factory17NewJSArrayStorageENS0_6HandleINS0_7JSArrayEEEiiNS0_26ArrayStorageAllocationModeE
	.section	.text._ZN2v88internal7Factory12NewJSWeakMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory12NewJSWeakMapEv
	.type	_ZN2v88internal7Factory12NewJSWeakMapEv, @function
_ZN2v88internal7Factory12NewJSWeakMapEv:
.LFB24203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	767(%rax), %rax
	movq	41112(%rdi), %rdi
	movq	55(%rax), %r13
	testq	%rdi, %rdi
	je	.L3595
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3596:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0
	movq	41112(%r12), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3598
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3599:
	addl	$1, 41104(%r12)
	movq	41088(%r12), %r14
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	41096(%r12), %rbx
	call	_ZN2v88internal16JSWeakCollection10InitializeENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3601
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3601:
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3595:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3603
.L3597:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L3596
	.p2align 4,,10
	.p2align 3
.L3598:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3604
.L3600:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3599
	.p2align 4,,10
	.p2align 3
.L3603:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3597
	.p2align 4,,10
	.p2align 3
.L3604:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3600
	.cfi_endproc
.LFE24203:
	.size	_ZN2v88internal7Factory12NewJSWeakMapEv, .-_ZN2v88internal7Factory12NewJSWeakMapEv
	.section	.rodata._ZN2v88internal7Factory20NewJSModuleNamespaceEv.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"(!IsSmi() && (*layout_word_index < length())) || (IsSmi() && (*layout_word_index < 1))"
	.section	.text._ZN2v88internal7Factory20NewJSModuleNamespaceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory20NewJSModuleNamespaceEv
	.type	_ZN2v88internal7Factory20NewJSModuleNamespaceEv, @function
_ZN2v88internal7Factory20NewJSModuleNamespaceEv:
.LFB24204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	727(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3606
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3607:
	movq	%r13, %rsi
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3609
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L3610:
	movq	%r13, %rcx
	movq	%r12, %rsi
	leaq	288(%rbx), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	movq	0(%r13), %r9
	movq	39(%r9), %rax
	movq	31(%rax), %rsi
	movq	%rsi, %rax
	shrq	$38, %rsi
	shrq	$51, %rax
	andl	$7, %esi
	movq	%rax, %rdi
	movzbl	7(%r9), %eax
	movzbl	8(%r9), %ecx
	andl	$1023, %edi
	subl	%ecx, %eax
	cmpl	%eax, %edi
	setl	%cl
	jl	.L3660
	subl	%eax, %edi
	movl	$16, %r8d
	leal	16(,%rdi,8), %edi
.L3613:
	cmpl	$2, %esi
	je	.L3640
	cmpb	$2, %sil
	jg	.L3615
	je	.L3616
.L3641:
	xorl	%esi, %esi
.L3614:
	movzbl	%cl, %edx
	cltq
	movslq	%edi, %rdi
	movslq	%r8d, %r8
	salq	$17, %rax
	salq	$14, %rdx
	movq	(%r12), %r13
	movq	2856(%rbx), %r14
	orq	%rax, %rdx
	salq	$27, %r8
	orq	%rdi, %rdx
	movq	-1(%r13), %rax
	leaq	-1(%r13), %rdi
	orq	%r8, %rdx
	orq	%rdx, %rsi
	shrq	$30, %rdx
	movl	%esi, %ecx
	andl	$15, %edx
	sarl	$3, %ecx
	andl	$2047, %ecx
	subl	%edx, %ecx
	testl	$16384, %esi
	jne	.L3617
	movq	7(%r13), %r15
	andq	$-262144, %r13
	movq	24(%r13), %rax
	subq	$37592, %rax
	testb	$1, %r15b
	jne	.L3618
.L3633:
	movq	968(%rax), %r15
.L3619:
	leal	16(,%rcx,8), %eax
	cltq
	leaq	-1(%r15,%rax), %r13
	movq	%r14, 0(%r13)
	testb	$1, %r14b
	je	.L3647
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L3635
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L3635:
	testb	$24, %al
	je	.L3647
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3661
.L3647:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3615:
	.cfi_restore_state
	subl	$3, %esi
	cmpb	$1, %sil
	jbe	.L3641
.L3616:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3640:
	movl	$32768, %esi
	jmp	.L3614
	.p2align 4,,10
	.p2align 3
.L3617:
	movq	47(%rax), %r8
	testq	%r8, %r8
	je	.L3629
	movq	%r8, %rdx
	movl	$32, %eax
	notq	%rdx
	movl	%edx, %r9d
	andl	$1, %r9d
	je	.L3662
.L3621:
	cmpl	%eax, %ecx
	jnb	.L3629
	testl	%ecx, %ecx
	leal	31(%rcx), %eax
	cmovns	%ecx, %eax
	sarl	$5, %eax
	andl	$1, %edx
	jne	.L3622
	cmpl	%eax, 11(%r8)
	jle	.L3622
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$27, %edx
	addl	%edx, %ecx
	andl	$31, %ecx
	subl	%edx, %ecx
	movl	$1, %edx
	sall	%cl, %edx
	testb	%r9b, %r9b
	je	.L3663
.L3626:
	sarq	$32, %r8
	testl	%r8d, %edx
	sete	%dl
.L3628:
	movq	%rsi, %rax
	andl	$16376, %eax
	addq	%rdi, %rax
	testb	%dl, %dl
	jne	.L3629
	movq	7(%r14), %rdx
	movq	%rdx, (%rax)
	jmp	.L3647
	.p2align 4,,10
	.p2align 3
.L3606:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L3664
.L3608:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L3607
	.p2align 4,,10
	.p2align 3
.L3609:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L3665
.L3611:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L3610
	.p2align 4,,10
	.p2align 3
.L3660:
	movzbl	8(%r9), %r8d
	movzbl	8(%r9), %edx
	addl	%edx, %edi
	sall	$3, %r8d
	sall	$3, %edi
	jmp	.L3613
	.p2align 4,,10
	.p2align 3
.L3629:
	andl	$16376, %esi
	movq	%r14, %rdx
	leaq	(%rsi,%rdi), %r15
	movq	%r13, %rdi
	movq	%r14, (%r15)
	movq	%r15, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L3647
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3647
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3647
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3647
	.p2align 4,,10
	.p2align 3
.L3618:
	cmpq	288(%rax), %r15
	jne	.L3619
	jmp	.L3633
	.p2align 4,,10
	.p2align 3
.L3662:
	movslq	11(%r8), %rax
	sall	$3, %eax
	jmp	.L3621
	.p2align 4,,10
	.p2align 3
.L3665:
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L3611
	.p2align 4,,10
	.p2align 3
.L3664:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3608
	.p2align 4,,10
	.p2align 3
.L3663:
	sall	$2, %eax
	cltq
	movl	15(%r8,%rax), %eax
	testl	%eax, %edx
	sete	%dl
	jmp	.L3628
	.p2align 4,,10
	.p2align 3
.L3661:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3647
	.p2align 4,,10
	.p2align 3
.L3622:
	cmpl	$31, %ecx
	jg	.L3624
	movl	$1, %edx
	sall	%cl, %edx
	testb	%r9b, %r9b
	jne	.L3626
.L3624:
	leaq	.LC30(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24204:
	.size	_ZN2v88internal7Factory20NewJSModuleNamespaceEv, .-_ZN2v88internal7Factory20NewJSModuleNamespaceEv
	.section	.text._ZN2v88internal7Factory20NewJSGeneratorObjectENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory20NewJSGeneratorObjectENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal7Factory20NewJSGeneratorObjectENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal7Factory20NewJSGeneratorObjectENS0_6HandleINS0_10JSFunctionEEE:
.LFB24205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE@PLT
	movq	(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	55(%rax), %r13
	testq	%rdi, %rdi
	je	.L3667
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3668:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3667:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3671
.L3669:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L3668
	.p2align 4,,10
	.p2align 3
.L3671:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3669
	.cfi_endproc
.LFE24205:
	.size	_ZN2v88internal7Factory20NewJSGeneratorObjectENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal7Factory20NewJSGeneratorObjectENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal7Factory19NewSourceTextModuleENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory19NewSourceTextModuleENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal7Factory19NewSourceTextModuleENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal7Factory19NewSourceTextModuleENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB24206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	15(%rdx), %rax
	testb	$1, %al
	jne	.L3673
.L3675:
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
.L3674:
	leaq	-64(%rbp), %r15
	movq	%rax, -64(%rbp)
	movq	%r15, %rdi
	call	_ZNK2v88internal9ScopeInfo20ModuleDescriptorInfoEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3676
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L3677:
	movq	(%r12), %rax
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal20SourceTextModuleInfo18RegularExportCountEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal9HashTableINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	movq	(%r12), %rax
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal20SourceTextModuleInfo18RegularExportCountEv@PLT
	testl	%eax, %eax
	jne	.L3679
	leaq	288(%rbx), %rax
	movq	%rax, -72(%rbp)
.L3680:
	movq	(%r12), %rax
	movq	47(%rax), %rdx
	movslq	11(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.L3681
	leaq	288(%rbx), %rcx
	movq	%rcx, -80(%rbp)
.L3682:
	movq	15(%rax), %rax
	leaq	288(%rbx), %r15
	movslq	11(%rax), %rsi
	testq	%rsi, %rsi
	jle	.L3684
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE.part.0
	movq	%rax, %r15
.L3684:
	movq	%rbx, %rdi
	leaq	656(%rbx), %rsi
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%rbx), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L3685
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
	movq	%rax, %r12
.L3686:
	movq	0(%r13), %rdx
	leaq	47(%r14), %rsi
	movq	%r14, %rdi
	movq	%rdx, 47(%r14)
	movq	%rdx, -96(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-96(%rbp), %rdx
	testb	$1, %dl
	je	.L3688
	movq	-104(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3688:
	movq	-88(%rbp), %rax
	movq	(%r12), %rdi
	movq	(%rax), %r14
	leaq	7(%rdi), %rsi
	movq	%r14, 7(%rdi)
	movq	%r14, %rdx
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L3689
	movq	-96(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3689:
	movq	-72(%rbp), %rax
	movq	(%r12), %r14
	movq	(%rax), %rdx
	leaq	55(%r14), %rsi
	movq	%r14, %rdi
	movq	%rdx, 55(%r14)
	movq	%rdx, -72(%rbp)
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-72(%rbp), %rdx
	testb	$1, %dl
	je	.L3690
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3690:
	movq	-80(%rbp), %rax
	movq	(%r12), %r14
	movq	(%rax), %rdx
	leaq	63(%r14), %rsi
	movq	%r14, %rdi
	movq	%rdx, 63(%r14)
	movq	%rdx, -72(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-72(%rbp), %rdx
	testb	$1, %dl
	je	.L3691
	movq	-80(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3691:
	movl	$2147483647, %esi
	movq	%rbx, %rdi
	movq	(%r12), %r14
	call	_ZN2v88internal7Isolate20GenerateIdentityHashEj@PLT
	salq	$32, %rax
	movq	%rax, 15(%r14)
	movq	(%r12), %r14
	movq	88(%rbx), %rdx
	movq	%rdx, 31(%r14)
	leaq	31(%r14), %rsi
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-72(%rbp), %rdx
	testb	$1, %dl
	je	.L3692
	movq	-80(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3692:
	movq	(%r12), %r14
	movq	(%r15), %r15
	movq	%r15, 71(%r14)
	leaq	71(%r14), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	je	.L3693
	movq	-72(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3693:
	movq	0(%r13), %rax
	movq	(%r12), %r14
	movq	31(%rax), %r13
	leaq	79(%r14), %r15
	testb	$1, %r13b
	jne	.L3703
	movq	%r13, 79(%r14)
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
.L3696:
	movq	(%r12), %rax
	movq	$0, 23(%rax)
	movq	(%r12), %r13
	movq	96(%rbx), %r14
	movq	%r14, 39(%r13)
	leaq	39(%r13), %r15
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L3697
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3697:
	movq	(%r12), %r13
	movq	96(%rbx), %r14
	movq	%r14, 87(%r13)
	leaq	87(%r13), %r15
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L3698
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3698:
	movabsq	$-4294967296, %rax
	movq	(%r12), %rdx
	movq	%rax, 95(%rdx)
	movq	(%r12), %rdx
	movq	%rax, 103(%rdx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3704
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3685:
	.cfi_restore_state
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L3705
.L3687:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%r12)
	jmp	.L3686
	.p2align 4,,10
	.p2align 3
.L3676:
	movq	41088(%rbx), %r12
	cmpq	%r12, 41096(%rbx)
	je	.L3706
.L3678:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L3677
	.p2align 4,,10
	.p2align 3
.L3673:
	movq	-1(%rax), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L3675
	jmp	.L3674
	.p2align 4,,10
	.p2align 3
.L3703:
	movq	-1(%r13), %rax
	cmpw	$86, 11(%rax)
	je	.L3707
	movq	%r13, 79(%r14)
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
.L3699:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	jmp	.L3696
	.p2align 4,,10
	.p2align 3
.L3681:
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE.part.0
	movq	%rax, -80(%rbp)
	movq	(%r12), %rax
	jmp	.L3682
	.p2align 4,,10
	.p2align 3
.L3679:
	xorl	%edx, %edx
	movl	%eax, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE.part.0
	movq	%rax, -72(%rbp)
	jmp	.L3680
	.p2align 4,,10
	.p2align 3
.L3707:
	movq	23(%r13), %r13
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r13, 79(%r14)
	movq	%r13, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L3699
	jmp	.L3696
	.p2align 4,,10
	.p2align 3
.L3705:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L3687
	.p2align 4,,10
	.p2align 3
.L3706:
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L3678
.L3704:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24206:
	.size	_ZN2v88internal7Factory19NewSourceTextModuleENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal7Factory19NewSourceTextModuleENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal7Factory18NewSyntheticModuleENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENSA_INS_6ModuleEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18NewSyntheticModuleENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENSA_INS_6ModuleEEEE
	.type	_ZN2v88internal7Factory18NewSyntheticModuleENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENSA_INS_6ModuleEEEE, @function
_ZN2v88internal7Factory18NewSyntheticModuleENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENSA_INS_6ModuleEEEE:
.LFB24207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	xorl	%ecx, %ecx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	(%rdx), %rax
	xorl	%edx, %edx
	movslq	11(%rax), %rsi
	call	_ZN2v88internal9HashTableINS0_15ObjectHashTableENS0_20ObjectHashTableShapeEE3NewEPNS0_7IsolateEiNS0_14AllocationTypeENS0_15MinimumCapacityE@PLT
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE
	movq	%r13, %rdi
	leaq	672(%r13), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%r13), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L3709
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
	movq	%rax, %r12
.L3710:
	movl	$2147483647, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate20GenerateIdentityHashEj@PLT
	salq	$32, %rax
	movq	%rax, 15(%r14)
	movq	(%r12), %rdi
	movq	88(%r13), %rdx
	movq	%rdx, 31(%rdi)
	leaq	31(%rdi), %rsi
	movq	%rdx, -72(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-72(%rbp), %rdx
	testb	$1, %dl
	jne	.L3743
.L3712:
	movq	(%r12), %rax
	movq	$0, 23(%rax)
	movq	(%r12), %rdi
	movq	96(%r13), %rdx
	movq	%rdx, 39(%rdi)
	leaq	39(%rdi), %rsi
	movq	%rdx, -72(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-72(%rbp), %rdx
	testb	$1, %dl
	jne	.L3744
.L3714:
	movq	(%r12), %rdi
	movq	(%r15), %r15
	movq	%r15, 47(%rdi)
	leaq	47(%rdi), %rsi
	movq	%r15, %rdx
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	jne	.L3745
.L3716:
	movq	(%rbx), %r15
	movq	(%r12), %rdi
	movq	%r15, 55(%rdi)
	leaq	55(%rdi), %rbx
	movq	%r15, %rdx
	movq	%rdi, -72(%rbp)
	movq	%rbx, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	jne	.L3746
.L3718:
	movq	-56(%rbp), %rax
	movq	(%r12), %r15
	movq	(%rax), %r14
	leaq	7(%r15), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, 7(%r15)
	movq	%r14, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	jne	.L3747
.L3720:
	movq	-64(%rbp), %rax
	movq	(%r12), %r14
	movq	(%rax), %r13
	leaq	63(%r14), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r13, 63(%r14)
	movq	%r13, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L3748
.L3722:
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3743:
	.cfi_restore_state
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3712
	movq	-80(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3712
	movq	-88(%rbp), %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3712
	.p2align 4,,10
	.p2align 3
.L3748:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3722
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3722
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3722
	.p2align 4,,10
	.p2align 3
.L3747:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3720
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3720
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3720
	.p2align 4,,10
	.p2align 3
.L3746:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3718
	movq	-72(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3718
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3718
	.p2align 4,,10
	.p2align 3
.L3745:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3716
	movq	-72(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3716
	movq	-80(%rbp), %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3716
	.p2align 4,,10
	.p2align 3
.L3744:
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3714
	movq	-80(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3714
	movq	-88(%rbp), %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3714
	.p2align 4,,10
	.p2align 3
.L3709:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L3749
.L3711:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%r14, (%r12)
	jmp	.L3710
	.p2align 4,,10
	.p2align 3
.L3749:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L3711
	.cfi_endproc
.LFE24207:
	.size	_ZN2v88internal7Factory18NewSyntheticModuleENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENSA_INS_6ModuleEEEE, .-_ZN2v88internal7Factory18NewSyntheticModuleENS0_6HandleINS0_6StringEEENS2_INS0_10FixedArrayEEEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENSA_INS_6ModuleEEEE
	.section	.text._ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE:
.LFB24208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	cmpl	$1, %esi
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	je	.L3769
	testq	%rdi, %rdi
	je	.L3756
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3757:
	movq	79(%rsi), %rsi
.L3755:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3759
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3760:
	movq	41112(%r12), %rdi
	movq	55(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L3762
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3763:
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	%r13d, %edx
	call	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3765
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L3766:
	leaq	288(%r12), %rdx
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3756:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3770
.L3758:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3757
	.p2align 4,,10
	.p2align 3
.L3765:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L3771
.L3767:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L3766
	.p2align 4,,10
	.p2align 3
.L3762:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L3772
.L3764:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L3763
	.p2align 4,,10
	.p2align 3
.L3759:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3773
.L3761:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3760
	.p2align 4,,10
	.p2align 3
.L3769:
	testq	%rdi, %rdi
	je	.L3752
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3753:
	movq	1183(%rsi), %rsi
	jmp	.L3755
	.p2align 4,,10
	.p2align 3
.L3752:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3774
.L3754:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3753
	.p2align 4,,10
	.p2align 3
.L3773:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L3761
	.p2align 4,,10
	.p2align 3
.L3772:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3764
	.p2align 4,,10
	.p2align 3
.L3771:
	movq	%r12, %rdi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3767
	.p2align 4,,10
	.p2align 3
.L3770:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L3758
	.p2align 4,,10
	.p2align 3
.L3774:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L3754
	.cfi_endproc
.LFE24208:
	.size	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE, .-_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory19NewJSIteratorResultENS0_6HandleINS0_6ObjectEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory19NewJSIteratorResultENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal7Factory19NewJSIteratorResultENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal7Factory19NewJSIteratorResultENS0_6HandleINS0_6ObjectEEEb:
.LFB24209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3776
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3777:
	movq	655(%rsi), %r12
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3779
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3780:
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0
	movq	(%r14), %r14
	movq	(%rax), %r15
	movq	%rax, %r12
	movq	%r14, 23(%r15)
	leaq	23(%r15), %rsi
	testb	$1, %r14b
	je	.L3791
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L3805
	testb	$24, %al
	je	.L3791
.L3810:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3806
	.p2align 4,,10
	.p2align 3
.L3791:
	movq	(%r12), %r14
	testb	%r13b, %r13b
	je	.L3785
	movq	112(%rbx), %r13
.L3786:
	movq	%r13, 31(%r14)
	leaq	31(%r14), %r15
	testb	$1, %r13b
	je	.L3790
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L3807
	testb	$24, %al
	je	.L3790
.L3809:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3808
.L3790:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3785:
	.cfi_restore_state
	movq	120(%rbx), %r13
	jmp	.L3786
	.p2align 4,,10
	.p2align 3
.L3807:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L3809
	jmp	.L3790
	.p2align 4,,10
	.p2align 3
.L3805:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L3810
	jmp	.L3791
	.p2align 4,,10
	.p2align 3
.L3779:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L3811
.L3781:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L3780
	.p2align 4,,10
	.p2align 3
.L3776:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L3812
.L3778:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L3777
	.p2align 4,,10
	.p2align 3
.L3806:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3791
	.p2align 4,,10
	.p2align 3
.L3808:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3790
	.p2align 4,,10
	.p2align 3
.L3812:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L3778
	.p2align 4,,10
	.p2align 3
.L3811:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3781
	.cfi_endproc
.LFE24209:
	.size	_ZN2v88internal7Factory19NewJSIteratorResultENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal7Factory19NewJSIteratorResultENS0_6HandleINS0_6ObjectEEEb
	.section	.text._ZN2v88internal7Factory26NewJSAsyncFromSyncIteratorENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory26NewJSAsyncFromSyncIteratorENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal7Factory26NewJSAsyncFromSyncIteratorENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal7Factory26NewJSAsyncFromSyncIteratorENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEE:
.LFB24210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3814
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3815:
	movq	119(%rsi), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3817
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3818:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0
	movq	0(%r13), %r13
	movq	(%rax), %r14
	movq	%rax, %r12
	movq	%r13, 23(%r14)
	leaq	23(%r14), %rsi
	testb	$1, %r13b
	je	.L3827
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L3841
	testb	$24, %al
	je	.L3827
.L3846:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3842
	.p2align 4,,10
	.p2align 3
.L3827:
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 31(%r14)
	leaq	31(%r14), %r15
	testb	$1, %r13b
	je	.L3826
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L3843
	testb	$24, %al
	je	.L3826
.L3845:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3844
.L3826:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3843:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L3845
	jmp	.L3826
	.p2align 4,,10
	.p2align 3
.L3841:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L3846
	jmp	.L3827
	.p2align 4,,10
	.p2align 3
.L3817:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3847
.L3819:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L3818
	.p2align 4,,10
	.p2align 3
.L3814:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3848
.L3816:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3815
	.p2align 4,,10
	.p2align 3
.L3842:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3827
	.p2align 4,,10
	.p2align 3
.L3844:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3826
	.p2align 4,,10
	.p2align 3
.L3848:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L3816
	.p2align 4,,10
	.p2align 3
.L3847:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3819
	.cfi_endproc
.LFE24210:
	.size	_ZN2v88internal7Factory26NewJSAsyncFromSyncIteratorENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal7Factory26NewJSAsyncFromSyncIteratorENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal7Factory8NewJSMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory8NewJSMapEv
	.type	_ZN2v88internal7Factory8NewJSMapEv, @function
_ZN2v88internal7Factory8NewJSMapEv:
.LFB24211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3850
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3851:
	movq	719(%rsi), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3853
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3854:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal5JSMap10InitializeENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3850:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3857
.L3852:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3851
	.p2align 4,,10
	.p2align 3
.L3853:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3858
.L3855:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L3854
	.p2align 4,,10
	.p2align 3
.L3857:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L3852
	.p2align 4,,10
	.p2align 3
.L3858:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3855
	.cfi_endproc
.LFE24211:
	.size	_ZN2v88internal7Factory8NewJSMapEv, .-_ZN2v88internal7Factory8NewJSMapEv
	.section	.text._ZN2v88internal7Factory8NewJSSetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory8NewJSSetEv
	.type	_ZN2v88internal7Factory8NewJSSetEv, @function
_ZN2v88internal7Factory8NewJSSetEv:
.LFB24212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L3860
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3861:
	movq	743(%rsi), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3863
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L3864:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal5JSSet10InitializeENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3860:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L3867
.L3862:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3861
	.p2align 4,,10
	.p2align 3
.L3863:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L3868
.L3865:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L3864
	.p2align 4,,10
	.p2align 3
.L3867:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L3862
	.p2align 4,,10
	.p2align 3
.L3868:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L3865
	.cfi_endproc
.LFE24212:
	.size	_ZN2v88internal7Factory8NewJSSetEv, .-_ZN2v88internal7Factory8NewJSSetEv
	.section	.text._ZN2v88internal7Factory26TypeAndSizeForElementsKindENS0_12ElementsKindEPNS0_17ExternalArrayTypeEPm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory26TypeAndSizeForElementsKindENS0_12ElementsKindEPNS0_17ExternalArrayTypeEPm
	.type	_ZN2v88internal7Factory26TypeAndSizeForElementsKindENS0_12ElementsKindEPNS0_17ExternalArrayTypeEPm, @function
_ZN2v88internal7Factory26TypeAndSizeForElementsKindENS0_12ElementsKindEPNS0_17ExternalArrayTypeEPm:
.LFB24213:
	.cfi_startproc
	endbr64
	subl	$17, %edi
	cmpb	$10, %dil
	ja	.L3870
	leaq	.L3872(%rip), %rcx
	movzbl	%dil, %edi
	movslq	(%rcx,%rdi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Factory26TypeAndSizeForElementsKindENS0_12ElementsKindEPNS0_17ExternalArrayTypeEPm,"a",@progbits
	.align 4
	.align 4
.L3872:
	.long	.L3882-.L3872
	.long	.L3881-.L3872
	.long	.L3880-.L3872
	.long	.L3879-.L3872
	.long	.L3878-.L3872
	.long	.L3877-.L3872
	.long	.L3876-.L3872
	.long	.L3875-.L3872
	.long	.L3874-.L3872
	.long	.L3873-.L3872
	.long	.L3871-.L3872
	.section	.text._ZN2v88internal7Factory26TypeAndSizeForElementsKindENS0_12ElementsKindEPNS0_17ExternalArrayTypeEPm
	.p2align 4,,10
	.p2align 3
.L3873:
	movl	$11, (%rsi)
	movq	$8, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L3871:
	movl	$10, (%rsi)
	movq	$8, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L3882:
	movl	$2, (%rsi)
	movq	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L3881:
	movl	$1, (%rsi)
	movq	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L3880:
	movl	$4, (%rsi)
	movq	$2, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L3879:
	movl	$3, (%rsi)
	movq	$2, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L3878:
	movl	$6, (%rsi)
	movq	$4, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L3877:
	movl	$5, (%rsi)
	movq	$4, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L3876:
	movl	$7, (%rsi)
	movq	$4, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L3875:
	movl	$8, (%rsi)
	movq	$8, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L3874:
	movl	$9, (%rsi)
	movq	$1, (%rdx)
	ret
.L3870:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24213:
	.size	_ZN2v88internal7Factory26TypeAndSizeForElementsKindENS0_12ElementsKindEPNS0_17ExternalArrayTypeEPm, .-_ZN2v88internal7Factory26TypeAndSizeForElementsKindENS0_12ElementsKindEPNS0_17ExternalArrayTypeEPm
	.section	.rodata._ZN2v88internal7Factory20NewJSArrayBufferViewENS0_6HandleINS0_3MapEEENS2_INS0_14FixedArrayBaseEEENS2_INS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE.str1.8,"aMS",@progbits,1
	.align 8
.LC31:
	.string	"byte_length <= buffer->byte_length()"
	.align 8
.LC32:
	.string	"byte_offset <= buffer->byte_length()"
	.align 8
.LC33:
	.string	"byte_offset + byte_length <= buffer->byte_length()"
	.section	.text._ZN2v88internal7Factory20NewJSArrayBufferViewENS0_6HandleINS0_3MapEEENS2_INS0_14FixedArrayBaseEEENS2_INS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory20NewJSArrayBufferViewENS0_6HandleINS0_3MapEEENS2_INS0_14FixedArrayBaseEEENS2_INS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory20NewJSArrayBufferViewENS0_6HandleINS0_3MapEEENS2_INS0_14FixedArrayBaseEEENS2_INS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory20NewJSArrayBufferViewENS0_6HandleINS0_3MapEEENS2_INS0_14FixedArrayBaseEEENS2_INS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE:
.LFB24215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rcx), %rax
	movq	%rdx, -56(%rbp)
	movl	16(%rbp), %edx
	movq	23(%rax), %rax
	cmpq	%rax, %r9
	ja	.L3919
	movq	%r8, %r12
	cmpq	%rax, %r8
	ja	.L3920
	movq	%rcx, %r14
	leaq	(%r9,%r8), %rcx
	movq	%r9, %rbx
	cmpq	%rax, %rcx
	ja	.L3921
	movq	%rdi, %r15
	xorl	%ecx, %ecx
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	movq	41112(%r15), %rdi
	movq	-64(%rbp), %r11
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3890
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %r11
	movq	%rax, %r13
.L3891:
	leaq	288(%r15), %rdx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%r11, %rcx
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	movq	-56(%rbp), %rax
	movq	0(%r13), %rdi
	movq	(%rax), %r15
	leaq	15(%rdi), %rsi
	movq	%r15, 15(%rdi)
	movq	%r15, %rdx
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	je	.L3893
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3893
	movq	-56(%rbp), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3922
	.p2align 4,,10
	.p2align 3
.L3893:
	movq	0(%r13), %r15
	movq	(%r14), %r14
	movq	%r14, 23(%r15)
	leaq	23(%r15), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L3895
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3895
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L3923
	.p2align 4,,10
	.p2align 3
.L3895:
	movq	0(%r13), %rax
	xorl	%r14d, %r14d
	movq	%r12, 31(%rax)
	movq	0(%r13), %rax
	movq	%rbx, 39(%rax)
.L3901:
	movq	0(%r13), %rbx
	leaq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	movq	(%rax), %r15
	movl	$24, %eax
	movq	-1(%rbx), %rcx
	leaq	-1(%rbx), %rdx
	movzwl	11(%rcx), %edi
	cmpw	$1057, %di
	je	.L3897
	movq	%rdx, -56(%rbp)
	movsbl	13(%rcx), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movq	-56(%rbp), %rdx
.L3897:
	leal	(%r14,%rax), %r12d
	movslq	%r12d, %r12
	addq	%rdx, %r12
	movq	%r15, (%r12)
	testb	$1, %r15b
	je	.L3902
	movq	%r15, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	je	.L3899
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	8(%rcx), %rax
.L3899:
	testb	$24, %al
	je	.L3902
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L3902
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L3902:
	addl	$8, %r14d
	cmpl	$16, %r14d
	jne	.L3901
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3890:
	.cfi_restore_state
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L3924
.L3892:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L3891
	.p2align 4,,10
	.p2align 3
.L3923:
	movq	-56(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3895
	.p2align 4,,10
	.p2align 3
.L3922:
	movq	-64(%rbp), %rsi
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L3893
	.p2align 4,,10
	.p2align 3
.L3919:
	leaq	.LC31(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3920:
	leaq	.LC32(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3921:
	leaq	.LC33(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3924:
	movq	%r15, %rdi
	movq	%r11, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %r11
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L3892
	.cfi_endproc
.LFE24215:
	.size	_ZN2v88internal7Factory20NewJSArrayBufferViewENS0_6HandleINS0_3MapEEENS2_INS0_14FixedArrayBaseEEENS2_INS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE, .-_ZN2v88internal7Factory20NewJSArrayBufferViewENS0_6HandleINS0_3MapEEENS2_INS0_14FixedArrayBaseEEENS2_INS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE
	.section	.rodata._ZN2v88internal7Factory15NewJSTypedArrayENS0_17ExternalArrayTypeENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"length <= JSTypedArray::kMaxLength"
	.align 8
.LC35:
	.string	"length == byte_length / element_size"
	.align 8
.LC36:
	.string	"0 == byte_offset % ElementsKindToByteSize(elements_kind)"
	.section	.text._ZN2v88internal7Factory15NewJSTypedArrayENS0_17ExternalArrayTypeENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory15NewJSTypedArrayENS0_17ExternalArrayTypeENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory15NewJSTypedArrayENS0_17ExternalArrayTypeENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory15NewJSTypedArrayENS0_17ExternalArrayTypeENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE:
.LFB24216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r9d, -52(%rbp)
	cmpl	$11, %esi
	ja	.L3926
	movq	%rdx, %r13
	movl	%esi, %esi
	leaq	.L3928(%rip), %rdx
	movq	%rdi, %r14
	movslq	(%rdx,%rsi,4), %rax
	movq	%rcx, %r12
	movq	%r8, %rbx
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Factory15NewJSTypedArrayENS0_17ExternalArrayTypeENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE,"a",@progbits
	.align 4
	.align 4
.L3928:
	.long	.L3926-.L3928
	.long	.L3990-.L3928
	.long	.L3991-.L3928
	.long	.L3936-.L3928
	.long	.L3935-.L3928
	.long	.L3934-.L3928
	.long	.L3933-.L3928
	.long	.L3932-.L3928
	.long	.L3931-.L3928
	.long	.L3930-.L3928
	.long	.L3929-.L3928
	.long	.L3927-.L3928
	.section	.text._ZN2v88internal7Factory15NewJSTypedArrayENS0_17ExternalArrayTypeENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE
	.p2align 4,,10
	.p2align 3
.L3927:
	movb	$26, -64(%rbp)
	movl	$26, %edi
	movl	$8, %ecx
	leaq	0(,%r8,8), %r15
	.p2align 4,,10
	.p2align 3
.L3937:
	cmpq	$2147483647, %rbx
	ja	.L3989
	movq	%r15, %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rbx, %rax
	jne	.L4015
.L3940:
	call	_ZN2v88internal22ElementsKindToByteSizeENS0_12ElementsKindE@PLT
	xorl	%edx, %edx
	movslq	%eax, %rcx
	movq	%r12, %rax
	divq	%rcx
	testq	%rdx, %rdx
	je	.L4016
	leaq	.LC36(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3990:
	movb	$18, -64(%rbp)
	movl	$18, %edi
.L3938:
	cmpq	$2147483647, %rbx
	ja	.L3989
	movq	%rbx, %r15
	jmp	.L3940
	.p2align 4,,10
	.p2align 3
.L3991:
	movb	$17, -64(%rbp)
	movq	%r8, %r15
	movl	$17, %edi
	movl	$1, %ecx
	jmp	.L3937
	.p2align 4,,10
	.p2align 3
.L3936:
	movb	$20, -64(%rbp)
	leaq	(%r8,%r8), %r15
	movl	$20, %edi
	movl	$2, %ecx
	jmp	.L3937
	.p2align 4,,10
	.p2align 3
.L3935:
	movb	$19, -64(%rbp)
	leaq	(%r8,%r8), %r15
	movl	$19, %edi
	movl	$2, %ecx
	jmp	.L3937
	.p2align 4,,10
	.p2align 3
.L3934:
	movb	$22, -64(%rbp)
	movl	$22, %edi
	movl	$4, %ecx
	leaq	0(,%r8,4), %r15
	jmp	.L3937
	.p2align 4,,10
	.p2align 3
.L3933:
	movb	$21, -64(%rbp)
	movl	$21, %edi
	movl	$4, %ecx
	leaq	0(,%r8,4), %r15
	jmp	.L3937
	.p2align 4,,10
	.p2align 3
.L3932:
	movb	$23, -64(%rbp)
	movl	$23, %edi
	movl	$4, %ecx
	leaq	0(,%r8,4), %r15
	jmp	.L3937
	.p2align 4,,10
	.p2align 3
.L3931:
	movb	$24, -64(%rbp)
	movl	$24, %edi
	movl	$8, %ecx
	leaq	0(,%r8,8), %r15
	jmp	.L3937
	.p2align 4,,10
	.p2align 3
.L3930:
	movb	$25, -64(%rbp)
	movl	$25, %edi
	jmp	.L3938
	.p2align 4,,10
	.p2align 3
.L3929:
	movb	$27, -64(%rbp)
	movl	$27, %edi
	movl	$8, %ecx
	leaq	0(,%r8,8), %r15
	jmp	.L3937
	.p2align 4,,10
	.p2align 3
.L4016:
	movzbl	-64(%rbp), %eax
	subl	$18, %eax
	cmpb	$9, %al
	ja	.L3942
	leaq	.L3944(%rip), %rdx
	movzbl	%al, %eax
	movq	%r14, %rdi
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Factory15NewJSTypedArrayENS0_17ExternalArrayTypeENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE
	.align 4
	.align 4
.L3944:
	.long	.L3953-.L3944
	.long	.L3952-.L3944
	.long	.L3951-.L3944
	.long	.L3950-.L3944
	.long	.L3949-.L3944
	.long	.L3948-.L3944
	.long	.L3947-.L3944
	.long	.L3946-.L3944
	.long	.L3945-.L3944
	.long	.L3943-.L3944
	.section	.text._ZN2v88internal7Factory15NewJSTypedArrayENS0_17ExternalArrayTypeENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE
	.p2align 4,,10
	.p2align 3
.L3945:
	call	_ZN2v88internal7Isolate14native_contextEv
	movl	$248, %esi
	movq	(%rax), %rdi
	.p2align 4,,10
	.p2align 3
.L4006:
	call	_ZN2v88internal11TaggedFieldINS0_6ObjectELi0EE12Relaxed_LoadEPNS0_7IsolateENS0_10HeapObjectEi.isra.0
	movq	41112(%r14), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3985
.L3994:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3986:
	movq	%rax, %rsi
	movzbl	-52(%rbp), %eax
	subq	$8, %rsp
	movq	%r15, %r9
	leaq	976(%r14), %rdx
	movq	%r12, %r8
	movq	%r13, %rcx
	movq	%r14, %rdi
	pushq	%rax
	call	_ZN2v88internal7Factory20NewJSArrayBufferViewENS0_6HandleINS0_3MapEEENS2_INS0_14FixedArrayBaseEEENS2_INS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	%rbx, 47(%rax)
	movq	0(%r13), %rdx
	movq	(%r14), %rax
	addq	31(%rdx), %r12
	movq	%r12, 55(%rax)
	movq	(%r14), %r12
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	leaq	63(%r12), %r15
	movq	%r12, %rdi
	movq	%r13, 63(%r12)
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	popq	%rax
	popq	%rdx
	testb	$1, %r13b
	je	.L3992
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L3992:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3946:
	.cfi_restore_state
	call	_ZN2v88internal7Isolate14native_contextEv
	movl	$1568, %esi
	movq	(%rax), %rdi
	jmp	.L4006
	.p2align 4,,10
	.p2align 3
.L3947:
	call	_ZN2v88internal7Isolate14native_contextEv
	movl	$408, %esi
	movq	(%rax), %rdi
	jmp	.L4006
	.p2align 4,,10
	.p2align 3
.L3948:
	call	_ZN2v88internal7Isolate14native_contextEv
	movl	$400, %esi
	movq	(%rax), %rdi
	jmp	.L4006
	.p2align 4,,10
	.p2align 3
.L3949:
	call	_ZN2v88internal7Isolate14native_contextEv
	movl	$600, %esi
	movq	(%rax), %rdi
	jmp	.L4006
	.p2align 4,,10
	.p2align 3
.L3950:
	call	_ZN2v88internal7Isolate14native_contextEv
	movl	$1552, %esi
	movq	(%rax), %rdi
	jmp	.L4006
	.p2align 4,,10
	.p2align 3
.L3951:
	call	_ZN2v88internal7Isolate14native_contextEv
	movl	$592, %esi
	movq	(%rax), %rdi
	jmp	.L4006
	.p2align 4,,10
	.p2align 3
.L3952:
	call	_ZN2v88internal7Isolate14native_contextEv
	movl	$1544, %esi
	movq	(%rax), %rdi
	jmp	.L4006
	.p2align 4,,10
	.p2align 3
.L3953:
	call	_ZN2v88internal7Isolate14native_contextEv
	movl	$608, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal11TaggedFieldINS0_6ObjectELi0EE12Relaxed_LoadEPNS0_7IsolateENS0_10HeapObjectEi.isra.0
	movq	41112(%r14), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	jne	.L3994
	movq	41088(%r14), %rax
	cmpq	%rax, 41096(%r14)
	jne	.L3987
	.p2align 4,,10
	.p2align 3
.L3995:
	movq	%r14, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	jmp	.L3987
	.p2align 4,,10
	.p2align 3
.L3943:
	call	_ZN2v88internal7Isolate14native_contextEv
	movl	$240, %esi
	movq	(%rax), %rdi
	jmp	.L4006
	.p2align 4,,10
	.p2align 3
.L3985:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L3995
.L3987:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L3986
	.p2align 4,,10
	.p2align 3
.L3989:
	leaq	.LC34(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4015:
	leaq	.LC35(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3942:
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate14native_contextEv
	movl	$1560, %esi
	movq	(%rax), %rdi
	jmp	.L4006
.L3926:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24216:
	.size	_ZN2v88internal7Factory15NewJSTypedArrayENS0_17ExternalArrayTypeENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE, .-_ZN2v88internal7Factory15NewJSTypedArrayENS0_17ExternalArrayTypeENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory13NewJSDataViewENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory13NewJSDataViewENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory13NewJSDataViewENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory13NewJSDataViewENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE:
.LFB24217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4018
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L4019:
	movq	319(%rsi), %rax
	movq	41112(%r12), %rdi
	movq	55(%rax), %r8
	testq	%rdi, %rdi
	je	.L4021
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4022:
	subq	$8, %rsp
	movq	%r13, %rcx
	movq	%r15, %r9
	movq	%rbx, %r8
	pushq	%r14
	leaq	288(%r12), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewJSArrayBufferViewENS0_6HandleINS0_3MapEEENS2_INS0_14FixedArrayBaseEEENS2_INS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE
	movq	0(%r13), %rcx
	movq	(%rax), %rdx
	addq	31(%rcx), %rbx
	movq	%rbx, 47(%rdx)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4018:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L4025
.L4020:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L4019
	.p2align 4,,10
	.p2align 3
.L4021:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L4026
.L4023:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L4022
	.p2align 4,,10
	.p2align 3
.L4025:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L4020
	.p2align 4,,10
	.p2align 3
.L4026:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L4023
	.cfi_endproc
.LFE24217:
	.size	_ZN2v88internal7Factory13NewJSDataViewENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE, .-_ZN2v88internal7Factory13NewJSDataViewENS0_6HandleINS0_13JSArrayBufferEEEmmNS0_14AllocationTypeE
	.section	.rodata._ZN2v88internal7Factory18NewJSBoundFunctionENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_6VectorIS6_EE.str1.1,"aMS",@progbits,1
.LC37:
	.string	"!handle_.is_null()"
	.section	.text._ZN2v88internal7Factory18NewJSBoundFunctionENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_6VectorIS6_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18NewJSBoundFunctionENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_6VectorIS6_EE
	.type	_ZN2v88internal7Factory18NewJSBoundFunctionENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_6VectorIS6_EE, @function
_ZN2v88internal7Factory18NewJSBoundFunctionENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_6VectorIS6_EE:
.LFB24218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -120(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%r8, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$65533, %r8d
	jle	.L4028
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1719(%rax), %r12
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4029
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4030:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$305, %edx
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS0_15MessageTemplateENS2_INS0_6ObjectEEES7_S7_
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L4032:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4097
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4029:
	.cfi_restore_state
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L4098
.L4031:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r12, (%rsi)
	jmp	.L4030
	.p2align 4,,10
	.p2align 3
.L4028:
	movq	%rsi, %rbx
	movq	%rsi, %r13
	leaq	-80(%rbp), %r12
	testq	%rsi, %rsi
	je	.L4099
	.p2align 4,,10
	.p2align 3
.L4033:
	movq	0(%r13), %r15
	movq	-1(%r15), %rax
	cmpw	$1026, 11(%rax)
	je	.L4100
	movq	-1(%r15), %rax
	movzbl	13(%rax), %edx
	shrb	$5, %dl
	andl	$1, %edx
	testb	%dl, %dl
	je	.L4038
.L4104:
	movq	41112(%r14), %rdi
	movq	12464(%r14), %r15
	testq	%rdi, %rdi
	je	.L4039
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4040:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal7Isolate9MayAccessENS0_6HandleINS0_7ContextEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	jne	.L4038
	leaq	104(%r14), %r13
.L4049:
	movq	(%rbx), %rax
	leaq	-88(%rbp), %rdi
	leaq	288(%r14), %r15
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal10JSReceiver18GetCreationContextEv@PLT
	movq	%r14, %rsi
	movq	(%rax), %rdx
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal20SaveAndSwitchContextC1EPNS0_7IsolateENS0_7ContextE@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jne	.L4101
.L4051:
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
	testb	$64, 13(%rax)
	movq	12464(%r14), %rax
	movq	39(%rax), %rax
	jne	.L4102
	movq	271(%rax), %r12
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4061
.L4094:
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %rsi
.L4060:
	movq	23(%r12), %rax
	cmpq	%rax, 0(%r13)
	je	.L4064
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal3Map21TransitionToPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEE@PLT
	movq	%rax, %rsi
.L4064:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0
	movq	(%rbx), %rbx
	movq	(%rax), %r13
	movq	%rax, %r12
	movq	%rbx, %rdx
	movq	%rbx, 23(%r13)
	leaq	23(%r13), %r14
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %bl
	je	.L4065
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4065:
	movq	-120(%rbp), %rax
	movq	(%r12), %r13
	movq	(%rax), %rbx
	leaq	31(%r13), %r14
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rbx, 31(%r13)
	movq	%rbx, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %bl
	je	.L4066
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4066:
	movq	(%r12), %r13
	movq	(%r15), %r14
	movq	%r14, 39(%r13)
	leaq	39(%r13), %rbx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L4067
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4067:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal11SaveContextD2Ev@PLT
	jmp	.L4032
	.p2align 4,,10
	.p2align 3
.L4100:
	movq	%r15, %rdx
	movq	%r12, %rdi
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	movq	%rdx, -104(%rbp)
	movq	-25128(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-104(%rbp), %rdx
	movq	24(%rdx), %rdx
	testb	$1, %r15b
	jne	.L4103
.L4035:
	movq	-1(%r15), %rdx
	movq	23(%rdx), %rdx
.L4036:
	cmpq	%rax, %rdx
	setne	%dl
	testb	%dl, %dl
	jne	.L4104
.L4038:
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jne	.L4072
	movq	%r13, %rdi
	call	_ZN2v88internal7JSProxy12GetPrototypeENS0_6HandleIS1_EE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L4049
.L4048:
	xorl	%r12d, %r12d
	jmp	.L4032
	.p2align 4,,10
	.p2align 3
.L4072:
	movq	-1(%rax), %rax
	movl	$1, %r15d
	movq	23(%rax), %rsi
	cmpq	104(%r14), %rsi
	je	.L4043
	cmpw	$1026, 11(%rax)
	setne	%r15b
.L4043:
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4105
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L4044:
	testb	%r15b, %r15b
	je	.L4033
	testq	%r13, %r13
	jne	.L4049
	jmp	.L4048
	.p2align 4,,10
	.p2align 3
.L4039:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L4106
.L4041:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r15, (%rsi)
	jmp	.L4040
	.p2align 4,,10
	.p2align 3
.L4105:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L4107
.L4045:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	jmp	.L4044
	.p2align 4,,10
	.p2align 3
.L4102:
	movq	263(%rax), %r12
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L4094
.L4061:
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L4108
.L4063:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r12, (%rsi)
	jmp	.L4060
	.p2align 4,,10
	.p2align 3
.L4106:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4041
	.p2align 4,,10
	.p2align 3
.L4103:
	movq	-1(%r15), %rcx
	cmpw	$1024, 11(%rcx)
	jne	.L4035
	movq	-37488(%rdx), %rdx
	jmp	.L4036
	.p2align 4,,10
	.p2align 3
.L4101:
	movq	-112(%rbp), %r12
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE.part.0
	movq	%rax, %r15
	testl	%r12d, %r12d
	jle	.L4051
	leal	-1(%r12), %eax
	movq	%r14, -152(%rbp)
	movq	-128(%rbp), %r14
	movl	$16, %r12d
	leaq	24(,%rax,8), %rax
	movq	%r13, -144(%rbp)
	movq	%r15, %r13
	movq	%rbx, -160(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L4055:
	movq	-16(%r14,%r12), %rax
	movq	0(%r13), %r15
	movq	(%rax), %rdx
	leaq	-1(%r15,%r12), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L4068
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %r9
	movq	%rax, -104(%rbp)
	testl	$262144, %r9d
	je	.L4053
	movq	%r15, %rdi
	movq	%rdx, -128(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	8(%rax), %r9
.L4053:
	andl	$24, %r9d
	je	.L4068
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4068
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L4068:
	addq	$8, %r12
	cmpq	%r12, %rbx
	jne	.L4055
	movq	%r13, %r15
	movq	-152(%rbp), %r14
	movq	-144(%rbp), %r13
	movq	-160(%rbp), %rbx
	jmp	.L4051
	.p2align 4,,10
	.p2align 3
.L4107:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L4045
	.p2align 4,,10
	.p2align 3
.L4098:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4031
	.p2align 4,,10
	.p2align 3
.L4099:
	leaq	.LC37(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4108:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4063
.L4097:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24218:
	.size	_ZN2v88internal7Factory18NewJSBoundFunctionENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_6VectorIS6_EE, .-_ZN2v88internal7Factory18NewJSBoundFunctionENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_6VectorIS6_EE
	.section	.text._ZN2v88internal7Factory10NewJSProxyENS0_6HandleINS0_10JSReceiverEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory10NewJSProxyENS0_6HandleINS0_10JSReceiverEEES4_
	.type	_ZN2v88internal7Factory10NewJSProxyENS0_6HandleINS0_10JSReceiverEEES4_, @function
_ZN2v88internal7Factory10NewJSProxyENS0_6HandleINS0_10JSReceiverEEES4_:
.LFB24222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	testb	$2, 13(%rdx)
	je	.L4110
	movq	-1(%rax), %rax
	testb	$64, 13(%rax)
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	je	.L4111
	movq	911(%rax), %r12
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4119
.L4138:
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4115:
	movq	%rbx, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L4122
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L4123:
	movq	-1(%rsi), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L4125
	movq	1056(%rbx), %rax
	movq	%rax, 7(%rsi)
.L4126:
	movq	(%r12), %r15
	movq	0(%r13), %r13
	movq	%r13, 15(%r15)
	leaq	15(%r15), %rbx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L4142
.L4127:
	movq	(%r14), %r13
	movq	(%r12), %r15
	movq	%r13, 23(%r15)
	leaq	23(%r15), %r14
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L4143
.L4129:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4110:
	.cfi_restore_state
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	927(%rax), %r12
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L4138
.L4119:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L4144
.L4121:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, (%rsi)
	jmp	.L4115
	.p2align 4,,10
	.p2align 3
.L4125:
	movq	288(%rbx), %rax
	movq	%rax, 7(%rsi)
	jmp	.L4126
	.p2align 4,,10
	.p2align 3
.L4111:
	movq	903(%rax), %r12
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L4138
	jmp	.L4119
	.p2align 4,,10
	.p2align 3
.L4143:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4129
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4129
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4129
	.p2align 4,,10
	.p2align 3
.L4142:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4127
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4127
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4127
	.p2align 4,,10
	.p2align 3
.L4122:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L4145
.L4124:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L4123
	.p2align 4,,10
	.p2align 3
.L4144:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4121
	.p2align 4,,10
	.p2align 3
.L4145:
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L4124
	.cfi_endproc
.LFE24222:
	.size	_ZN2v88internal7Factory10NewJSProxyENS0_6HandleINS0_10JSReceiverEEES4_, .-_ZN2v88internal7Factory10NewJSProxyENS0_6HandleINS0_10JSReceiverEEES4_
	.section	.text._ZN2v88internal7Factory29NewUninitializedJSGlobalProxyEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory29NewUninitializedJSGlobalProxyEi
	.type	_ZN2v88internal7Factory29NewUninitializedJSGlobalProxyEi, @function
_ZN2v88internal7Factory29NewUninitializedJSGlobalProxyEi:
.LFB24223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$1026, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r12
	movq	(%rax), %rax
	orb	$32, 13(%rax)
	movq	(%r12), %rdx
	movl	15(%rdx), %eax
	orl	$268435456, %eax
	movl	%eax, 15(%rdx)
	movq	41016(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L4152
.L4147:
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4152:
	.cfi_restore_state
	movq	(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10MapDetailsENS0_3MapE@PLT
	jmp	.L4147
	.cfi_endproc
.LFE24223:
	.size	_ZN2v88internal7Factory29NewUninitializedJSGlobalProxyEi, .-_ZN2v88internal7Factory29NewUninitializedJSGlobalProxyEi
	.section	.rodata._ZN2v88internal7Factory25ReinitializeJSGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEENS2_INS0_10JSFunctionEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC38:
	.string	"CopyAsPrototypeForJSGlobalProxy"
	.section	.text._ZN2v88internal7Factory25ReinitializeJSGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEENS2_INS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory25ReinitializeJSGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEENS2_INS0_10JSFunctionEEE
	.type	_ZN2v88internal7Factory25ReinitializeJSGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEENS2_INS0_10JSFunctionEEE, @function
_ZN2v88internal7Factory25ReinitializeJSGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEENS2_INS0_10JSFunctionEEE:
.LFB24224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	41112(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L4154
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L4155:
	movq	(%r14), %rax
	movq	-1(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4157
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L4158:
	movq	(%r14), %rax
	movq	41112(%r12), %rdi
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L4160
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
	movq	(%rbx), %rax
	movl	15(%rax), %eax
	testl	$1048576, %eax
	jne	.L4179
.L4163:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject15NotifyMapChangeENS0_6HandleINS0_3MapEEES4_PNS0_7IsolateE@PLT
	movq	(%rbx), %rax
	movl	15(%rax), %edx
	andl	$33554432, %edx
	je	.L4180
.L4164:
	movq	(%r14), %rdi
	movq	0(%r13), %rdx
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	jne	.L4181
.L4165:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4182
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4154:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L4183
.L4156:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L4155
	.p2align 4,,10
	.p2align 3
.L4160:
	movq	41088(%r12), %r15
	cmpq	%r15, 41096(%r12)
	je	.L4184
.L4162:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	movq	(%rbx), %rax
	movl	15(%rax), %eax
	testl	$1048576, %eax
	je	.L4163
.L4179:
	leaq	.LC38(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	(%rax), %rdx
	movq	%rax, %r13
	movl	15(%rdx), %eax
	orl	$1048576, %eax
	movl	%eax, 15(%rdx)
	jmp	.L4163
	.p2align 4,,10
	.p2align 3
.L4157:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L4185
.L4159:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L4158
	.p2align 4,,10
	.p2align 3
.L4181:
	testb	$1, %dl
	je	.L4165
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L4165
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4165
	.p2align 4,,10
	.p2align 3
.L4180:
	movl	15(%rax), %edx
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	orl	$33554432, %edx
	movl	%edx, 15(%rax)
	movl	$1, %edx
	movq	55(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	jmp	.L4164
	.p2align 4,,10
	.p2align 3
.L4185:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L4159
	.p2align 4,,10
	.p2align 3
.L4184:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L4162
	.p2align 4,,10
	.p2align 3
.L4183:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L4156
.L4182:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24224:
	.size	_ZN2v88internal7Factory25ReinitializeJSGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEENS2_INS0_10JSFunctionEEE, .-_ZN2v88internal7Factory25ReinitializeJSGlobalProxyENS0_6HandleINS0_13JSGlobalProxyEEENS2_INS0_10JSFunctionEEE
	.section	.text._ZN2v88internal7Factory18NewJSMessageObjectENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEEiiNS3_INS0_18SharedFunctionInfoEEEiNS3_INS0_6ScriptEEES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18NewJSMessageObjectENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEEiiNS3_INS0_18SharedFunctionInfoEEEiNS3_INS0_6ScriptEEES5_
	.type	_ZN2v88internal7Factory18NewJSMessageObjectENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEEiiNS3_INS0_18SharedFunctionInfoEEEiNS3_INS0_6ScriptEEES5_, @function
_ZN2v88internal7Factory18NewJSMessageObjectENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEEiiNS3_INS0_18SharedFunctionInfoEEEiNS3_INS0_6ScriptEEES5_:
.LFB24226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	leaq	4464(%rdi), %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$40, %rsp
	movl	16(%rbp), %eax
	movl	%ecx, -56(%rbp)
	movq	%r9, -64(%rbp)
	movl	%eax, -72(%rbp)
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE
	movq	41112(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L4187
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L4188:
	movq	288(%r13), %rdx
	movq	%rdx, 7(%rsi)
	movq	(%r15), %rsi
	movq	-1(%rsi), %rcx
	movzbl	14(%rcx), %edx
	shrl	$3, %edx
	cmpl	$15, %edx
	je	.L4208
	cmpl	$5, %edx
	jbe	.L4208
	cmpb	$13, %dl
	je	.L4221
	leal	-17(%rdx), %edi
	cmpb	$10, %dil
	jbe	.L4222
	cmpb	$12, %dl
	jne	.L4195
	andq	$-262144, %rcx
	movq	24(%rcx), %rdx
	movq	-36576(%rdx), %rdx
	jmp	.L4192
	.p2align 4,,10
	.p2align 3
.L4208:
	andq	$-262144, %rcx
	movq	24(%rcx), %rdx
	movq	-37304(%rdx), %rdx
.L4192:
	movq	%rdx, 15(%rsi)
	salq	$32, %r12
	movq	(%r15), %rdx
	movq	288(%r13), %rcx
	movq	%rcx, 15(%rdx)
	movq	(%r15), %rdx
	movq	%r12, 23(%rdx)
	movq	(%r14), %r12
	movq	(%r15), %rdi
	movq	%r12, 31(%rdi)
	leaq	31(%rdi), %r14
	movq	%r12, %rdx
	movq	%rdi, -80(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	jne	.L4223
.L4196:
	movq	-56(%rbp), %rcx
	movq	(%r15), %rdx
	salq	$32, %rbx
	salq	$32, %rcx
	movq	%rcx, 71(%rdx)
	movq	(%r15), %rdx
	movq	%rbx, 79(%rdx)
	movq	24(%rbp), %rdx
	movq	(%r15), %r14
	movq	(%rdx), %r12
	leaq	39(%r14), %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%r12, 39(%r14)
	movq	%r12, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	jne	.L4224
.L4198:
	movl	-56(%rbp), %eax
	movq	(%r15), %r12
	testl	%eax, %eax
	js	.L4200
.L4227:
	movq	88(%r13), %r13
	leaq	55(%r12), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r13, 55(%r12)
	movq	%r13, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L4225
.L4201:
	movq	(%r15), %rdi
	xorl	%edx, %edx
	movq	$0, 63(%rdi)
	leaq	63(%rdi), %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
.L4202:
	movq	32(%rbp), %rdx
	movq	(%r15), %r13
	movq	(%rdx), %r12
	leaq	47(%r13), %r14
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r12, 47(%r13)
	movq	%r12, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	jne	.L4226
.L4206:
	movabsq	$34359738368, %rcx
	movq	(%r15), %rdx
	movq	%r15, %rax
	movq	%rcx, 87(%rdx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4225:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	jmp	.L4201
	.p2align 4,,10
	.p2align 3
.L4226:
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L4206
	movq	%r13, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L4206
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4206
	.p2align 4,,10
	.p2align 3
.L4224:
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L4198
	movq	%r14, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L4198
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-56(%rbp), %eax
	movq	(%r15), %r12
	testl	%eax, %eax
	jns	.L4227
	.p2align 4,,10
	.p2align 3
.L4200:
	movq	-72(%rbp), %rdx
	leaq	63(%r12), %rsi
	movq	%r12, %rdi
	salq	$32, %rdx
	movq	%rdx, 63(%r12)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	cmpq	$0, -64(%rbp)
	movq	(%r15), %r12
	je	.L4228
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
.L4220:
	movq	%r13, 55(%r12)
	leaq	55(%r12), %r14
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	je	.L4202
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	jmp	.L4202
	.p2align 4,,10
	.p2align 3
.L4223:
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L4196
	movq	-80(%rbp), %rdi
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L4196
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4196
	.p2align 4,,10
	.p2align 3
.L4187:
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L4229
.L4189:
	leaq	8(%r15), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L4188
	.p2align 4,,10
	.p2align 3
.L4228:
	movq	88(%r13), %r13
	jmp	.L4220
	.p2align 4,,10
	.p2align 3
.L4222:
	andq	$-262144, %rcx
	movq	24(%rcx), %rdx
	movq	-36616(%rdx), %rdx
	jmp	.L4192
	.p2align 4,,10
	.p2align 3
.L4221:
	andq	$-262144, %rcx
	movq	24(%rcx), %rdx
	movq	-36584(%rdx), %rdx
	jmp	.L4192
	.p2align 4,,10
	.p2align 3
.L4229:
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L4189
.L4195:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24226:
	.size	_ZN2v88internal7Factory18NewJSMessageObjectENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEEiiNS3_INS0_18SharedFunctionInfoEEEiNS3_INS0_6ScriptEEES5_, .-_ZN2v88internal7Factory18NewJSMessageObjectENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEEiiNS3_INS0_18SharedFunctionInfoEEEiNS3_INS0_6ScriptEEES5_
	.section	.text._ZN2v88internal7Factory21NewSharedFunctionInfoENS0_11MaybeHandleINS0_6StringEEENS2_INS0_10HeapObjectEEEiNS0_12FunctionKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory21NewSharedFunctionInfoENS0_11MaybeHandleINS0_6StringEEENS2_INS0_10HeapObjectEEEiNS0_12FunctionKindE
	.type	_ZN2v88internal7Factory21NewSharedFunctionInfoENS0_11MaybeHandleINS0_6StringEEENS2_INS0_10HeapObjectEEEiNS0_12FunctionKindE, @function
_ZN2v88internal7Factory21NewSharedFunctionInfoENS0_11MaybeHandleINS0_6StringEEENS2_INS0_10HeapObjectEEEiNS0_12FunctionKindE:
.LFB24230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$56, %rsp
	movl	%ecx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L4256
	movl	$1, %edx
	call	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	movb	$1, -80(%rbp)
	movq	%rax, %r15
.L4231:
	movq	%r14, %rdi
	leaq	208(%r14), %rsi
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%r14), %rdi
	movq	%rax, %r8
	testq	%rdi, %rdi
	je	.L4232
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r8
	movq	%rax, %r12
.L4233:
	cmpb	$0, -80(%rbp)
	leaq	15(%r8), %rsi
	je	.L4235
	movq	(%r15), %r15
	movq	%r8, %rdi
	movq	%r15, 15(%r8)
	movq	%r15, %rdx
	movq	%rsi, -88(%rbp)
	movq	%r8, -80(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %rsi
	je	.L4236
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4236
	movq	%r8, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4273
.L4236:
	movq	(%r12), %r15
	leaq	7(%r15), %rsi
	testq	%r13, %r13
	je	.L4274
.L4239:
	movq	0(%r13), %r13
	movq	%r15, %rdi
	movq	%r13, 7(%r15)
	movq	%r13, %rdx
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L4275
.L4244:
	movq	(%r12), %rax
	cmpl	$67, -72(%rbp)
	leaq	23(%rax), %rdx
	je	.L4276
.L4245:
	movq	1040(%r14), %rax
	movq	%rax, (%rdx)
.L4246:
	movq	(%r12), %rax
	movq	88(%r14), %rdx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rdx, 31(%rax)
	xorl	%edx, %edx
	movq	(%r12), %rax
	movl	$-1, 51(%rax)
	movq	(%r12), %rax
	movw	%dx, 39(%rax)
	movq	(%r12), %rax
	movw	%cx, 41(%rax)
	movq	(%r12), %rax
	movw	%si, 43(%rax)
	movq	(%r12), %rax
	movw	%di, 45(%rax)
	movq	(%r12), %rax
	movl	$0, 47(%rax)
	movq	(%r12), %rdx
	movq	7(%rdx), %rax
	testb	$1, %al
	jne	.L4247
	movq	7(%rdx), %rsi
	sarq	$32, %rsi
	cmpl	$67, %esi
	setne	%cl
	xorl	%eax, %eax
	cmpl	$165, %esi
	setne	%al
	andl	%ecx, %eax
	sall	$25, %eax
	movl	%eax, %ecx
.L4248:
	movl	47(%rdx), %eax
	andl	$-33554433, %eax
	orl	%ecx, %eax
	movl	%eax, 47(%rdx)
	movzbl	%bl, %edx
	subl	$2, %ebx
	movq	(%r12), %r13
	movl	47(%r13), %eax
	andl	$-1056, %eax
	orl	%edx, %eax
	xorl	%edx, %edx
	cmpb	$3, %bl
	setbe	%dl
	sall	$10, %edx
	orl	%edx, %eax
	movl	%eax, 47(%r13)
	movl	47(%r13), %eax
	shrl	$13, %eax
	andl	$1, %eax
	movl	%eax, %ebx
	movq	15(%r13), %rax
	testb	$1, %al
	jne	.L4277
.L4249:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L4250:
	movl	47(%r13), %edx
	movl	$753664, %ecx
	movl	47(%r13), %edi
	andl	$31, %edx
	leal	-2(%rdx), %esi
	cmpb	$3, %sil
	jbe	.L4251
	leal	-12(%rdx), %ecx
	leal	-9(%rdx), %esi
	cmpb	$3, %cl
	ja	.L4252
	cmpb	$5, %sil
	sbbl	%ecx, %ecx
	andl	$4, %ecx
	addl	$169, %ecx
.L4253:
	xorl	$1, %eax
	addl	%ebx, %ebx
	movzbl	%al, %eax
	orl	%ebx, %eax
	leal	-154(%rcx,%rax), %eax
	sall	$15, %eax
	movl	%eax, %ecx
.L4251:
	movl	47(%r13), %eax
	andl	$-1015809, %eax
	orl	%ecx, %eax
	movl	%eax, 47(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4278
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4247:
	.cfi_restore_state
	movq	7(%rdx), %rax
	xorl	%ecx, %ecx
	testb	$1, %al
	je	.L4248
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	sete	%al
	movzbl	%al, %eax
	sall	$25, %eax
	movl	%eax, %ecx
	jmp	.L4248
	.p2align 4,,10
	.p2align 3
.L4235:
	movq	$0, 15(%r8)
	xorl	%edx, %edx
	movq	%r8, %rdi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	(%r12), %r15
	leaq	7(%r15), %rsi
	testq	%r13, %r13
	jne	.L4239
.L4274:
	cmpl	$1552, -72(%rbp)
	ja	.L4279
	movq	-72(%rbp), %rax
	salq	$32, %rax
	movq	%rax, 7(%r15)
	jmp	.L4244
	.p2align 4,,10
	.p2align 3
.L4275:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4244
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4244
	movq	-80(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%r12), %rax
	cmpl	$67, -72(%rbp)
	leaq	23(%rax), %rdx
	jne	.L4245
	.p2align 4,,10
	.p2align 3
.L4276:
	movq	96(%r14), %rdx
	movq	%rdx, 23(%rax)
	jmp	.L4246
	.p2align 4,,10
	.p2align 3
.L4252:
	movl	$165, %ecx
	cmpb	$4, %sil
	jbe	.L4253
	cmpb	$17, %dl
	ja	.L4254
	movl	$236480, %esi
	movl	$161, %ecx
	btq	%rdx, %rsi
	jc	.L4253
	.p2align 4,,10
	.p2align 3
.L4254:
	andl	$64, %edi
	cmpl	$1, %edi
	sbbl	%ecx, %ecx
	andl	$-4, %ecx
	addl	$158, %ecx
	jmp	.L4253
	.p2align 4,,10
	.p2align 3
.L4232:
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L4280
.L4234:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%r8, (%r12)
	jmp	.L4233
	.p2align 4,,10
	.p2align 3
.L4277:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L4249
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L4250
	.p2align 4,,10
	.p2align 3
.L4273:
	movq	%r15, %rdx
	movq	%r8, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4236
	.p2align 4,,10
	.p2align 3
.L4256:
	movb	$0, -80(%rbp)
	jmp	.L4231
	.p2align 4,,10
	.p2align 3
.L4279:
	movabsq	$712964571136, %rax
	movq	%rax, 7(%r15)
	movq	(%r12), %rax
	leaq	23(%rax), %rdx
	jmp	.L4245
	.p2align 4,,10
	.p2align 3
.L4280:
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %r12
	jmp	.L4234
.L4278:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24230:
	.size	_ZN2v88internal7Factory21NewSharedFunctionInfoENS0_11MaybeHandleINS0_6StringEEENS2_INS0_10HeapObjectEEEiNS0_12FunctionKindE, .-_ZN2v88internal7Factory21NewSharedFunctionInfoENS0_11MaybeHandleINS0_6StringEEENS2_INS0_10HeapObjectEEEiNS0_12FunctionKindE
	.section	.text._ZN2v88internal7Factory35NewSharedFunctionInfoForApiFunctionENS0_11MaybeHandleINS0_6StringEEENS0_6HandleINS0_20FunctionTemplateInfoEEENS0_12FunctionKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory35NewSharedFunctionInfoForApiFunctionENS0_11MaybeHandleINS0_6StringEEENS0_6HandleINS0_20FunctionTemplateInfoEEENS0_12FunctionKindE
	.type	_ZN2v88internal7Factory35NewSharedFunctionInfoForApiFunctionENS0_11MaybeHandleINS0_6StringEEENS0_6HandleINS0_20FunctionTemplateInfoEEENS0_12FunctionKindE, @function
_ZN2v88internal7Factory35NewSharedFunctionInfoForApiFunctionENS0_11MaybeHandleINS0_6StringEEENS0_6HandleINS0_20FunctionTemplateInfoEEENS0_12FunctionKindE:
.LFB24227:
	.cfi_startproc
	endbr64
	movzbl	%cl, %r8d
	movl	$-1, %ecx
	jmp	_ZN2v88internal7Factory21NewSharedFunctionInfoENS0_11MaybeHandleINS0_6StringEEENS2_INS0_10HeapObjectEEEiNS0_12FunctionKindE
	.cfi_endproc
.LFE24227:
	.size	_ZN2v88internal7Factory35NewSharedFunctionInfoForApiFunctionENS0_11MaybeHandleINS0_6StringEEENS0_6HandleINS0_20FunctionTemplateInfoEEENS0_12FunctionKindE, .-_ZN2v88internal7Factory35NewSharedFunctionInfoForApiFunctionENS0_11MaybeHandleINS0_6StringEEENS0_6HandleINS0_20FunctionTemplateInfoEEENS0_12FunctionKindE
	.section	.text._ZN2v88internal7Factory40NewSharedFunctionInfoForWasmCapiFunctionENS0_6HandleINS0_20WasmCapiFunctionDataEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory40NewSharedFunctionInfoForWasmCapiFunctionENS0_6HandleINS0_20WasmCapiFunctionDataEEE
	.type	_ZN2v88internal7Factory40NewSharedFunctionInfoForWasmCapiFunctionENS0_6HandleINS0_20WasmCapiFunctionDataEEE, @function
_ZN2v88internal7Factory40NewSharedFunctionInfoForWasmCapiFunctionENS0_6HandleINS0_20WasmCapiFunctionDataEEE:
.LFB24228:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdx
	movl	$16, %r8d
	movl	$-1, %ecx
	xorl	%esi, %esi
	jmp	_ZN2v88internal7Factory21NewSharedFunctionInfoENS0_11MaybeHandleINS0_6StringEEENS2_INS0_10HeapObjectEEEiNS0_12FunctionKindE
	.cfi_endproc
.LFE24228:
	.size	_ZN2v88internal7Factory40NewSharedFunctionInfoForWasmCapiFunctionENS0_6HandleINS0_20WasmCapiFunctionDataEEE, .-_ZN2v88internal7Factory40NewSharedFunctionInfoForWasmCapiFunctionENS0_6HandleINS0_20WasmCapiFunctionDataEEE
	.section	.text._ZN2v88internal7Factory31NewSharedFunctionInfoForBuiltinENS0_11MaybeHandleINS0_6StringEEEiNS0_12FunctionKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory31NewSharedFunctionInfoForBuiltinENS0_11MaybeHandleINS0_6StringEEEiNS0_12FunctionKindE
	.type	_ZN2v88internal7Factory31NewSharedFunctionInfoForBuiltinENS0_11MaybeHandleINS0_6StringEEEiNS0_12FunctionKindE, @function
_ZN2v88internal7Factory31NewSharedFunctionInfoForBuiltinENS0_11MaybeHandleINS0_6StringEEEiNS0_12FunctionKindE:
.LFB24229:
	.cfi_startproc
	endbr64
	movzbl	%cl, %r8d
	movl	%edx, %ecx
	xorl	%edx, %edx
	jmp	_ZN2v88internal7Factory21NewSharedFunctionInfoENS0_11MaybeHandleINS0_6StringEEENS2_INS0_10HeapObjectEEEiNS0_12FunctionKindE
	.cfi_endproc
.LFE24229:
	.size	_ZN2v88internal7Factory31NewSharedFunctionInfoForBuiltinENS0_11MaybeHandleINS0_6StringEEEiNS0_12FunctionKindE, .-_ZN2v88internal7Factory31NewSharedFunctionInfoForBuiltinENS0_11MaybeHandleINS0_6StringEEEiNS0_12FunctionKindE
	.section	.rodata._ZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEb.str1.1,"aMS",@progbits,1
.LC39:
	.string	"SharedFunctionInfo"
.LC40:
	.string	"snapshot"
	.section	.text._ZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEb
	.type	_ZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEb, @function
_ZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEb:
.LFB24225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal15FunctionLiteral4kindEv@PLT
	movq	32(%r12), %rsi
	movzbl	%al, %r8d
	testq	%rsi, %rsi
	je	.L4285
	movq	(%rsi), %rsi
.L4285:
	movl	$67, %ecx
	movq	%r13, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory21NewSharedFunctionInfoENS0_11MaybeHandleINS0_6StringEEENS2_INS0_10HeapObjectEEEiNS0_12FunctionKindE
	movzbl	%bl, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal18SharedFunctionInfo23InitFromFunctionLiteralENS0_6HandleIS1_EEPNS0_15FunctionLiteralEb@PLT
	movl	28(%r12), %edx
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal18SharedFunctionInfo9SetScriptENS0_6HandleIS1_EENS2_INS0_6ObjectEEEib@PLT
	movq	_ZZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEbE29trace_event_unique_atomic3414(%rip), %rdx
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L4329
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L4330
.L4289:
	movq	_ZZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEbE29trace_event_unique_atomic3418(%rip), %rax
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L4331
.L4294:
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L4332
.L4296:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4333
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4329:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4334
.L4288:
	movq	%r14, _ZZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEbE29trace_event_unique_atomic3414(%rip)
	movzbl	(%r14), %eax
	testb	$5, %al
	je	.L4289
.L4330:
	movq	0(%r13), %rax
	leaq	-88(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo7TraceIDEPNS0_15FunctionLiteralE@PLT
	pxor	%xmm0, %xmm0
	movq	_ZN2v88internal18SharedFunctionInfo11kTraceScopeE(%rip), %r15
	movq	%rax, %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4335
.L4290:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4291
	movq	(%rdi), %rax
	call	*8(%rax)
.L4291:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4289
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	_ZZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEbE29trace_event_unique_atomic3418(%rip), %rax
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L4294
.L4331:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4336
.L4295:
	movq	%r14, _ZZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEbE29trace_event_unique_atomic3418(%rip)
	movzbl	(%r14), %eax
	testb	$5, %al
	je	.L4296
.L4332:
	movq	0(%r13), %rax
	leaq	-88(%rbp), %rbx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo7TraceIDEPNS0_15FunctionLiteralE@PLT
	leaq	-104(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	movq	%r12, %rdx
	movq	%rax, -136(%rbp)
	movq	0(%r13), %rax
	movq	_ZN2v88internal18SharedFunctionInfo11kTraceScopeE(%rip), %r15
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo13ToTracedValueEPNS0_15FunctionLiteralE@PLT
	leaq	.LC40(%rip), %rax
	movb	$8, -113(%rbp)
	movq	%rax, -96(%rbp)
	movq	-104(%rbp), %rax
	movq	$0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r10
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r10
	jne	.L4337
.L4297:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4298
	movq	(%rdi), %rax
	call	*8(%rax)
.L4298:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4299
	movq	(%rdi), %rax
	call	*8(%rax)
.L4299:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4296
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L4296
	.p2align 4,,10
	.p2align 3
.L4334:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L4288
	.p2align 4,,10
	.p2align 3
.L4335:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	movq	%rbx, %r9
	movq	%r15, %r8
	pushq	$2
	leaq	.LC39(%rip), %rcx
	movl	$78, %esi
	pushq	%rdx
	movq	%r14, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L4290
	.p2align 4,,10
	.p2align 3
.L4336:
	leaq	.LC12(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L4295
	.p2align 4,,10
	.p2align 3
.L4337:
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	movq	%r15, %r8
	movq	%r14, %rdx
	pushq	$2
	movq	-136(%rbp), %r9
	movl	$79, %esi
	leaq	.LC39(%rip), %rcx
	pushq	%rax
	leaq	-113(%rbp), %rax
	pushq	%rbx
	pushq	%rax
	leaq	-96(%rbp), %rax
	pushq	%rax
	pushq	$1
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L4297
.L4333:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24225:
	.size	_ZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEb, .-_ZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEb
	.section	.text._ZN2v88internal7Factory22NumberToStringCacheSetENS0_6HandleINS0_6ObjectEEEiPKcb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory22NumberToStringCacheSetENS0_6HandleINS0_6ObjectEEEiPKcb
	.type	_ZN2v88internal7Factory22NumberToStringCacheSetENS0_6HandleINS0_6ObjectEEEiPKcb, @function
_ZN2v88internal7Factory22NumberToStringCacheSetENS0_6HandleINS0_6ObjectEEEiPKcb:
.LFB24233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movq	%rcx, %rdi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	leaq	128(%r15), %r8
	testl	%eax, %eax
	je	.L4343
	movq	%rax, %rbx
	cmpl	$1, %eax
	je	.L4369
	movl	%r13d, %edx
	movl	%eax, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L4342
	movq	(%rax), %rax
	movslq	%ebx, %rdx
	leaq	15(%rax), %rdi
	cmpq	$7, %rdx
	ja	.L4370
	addq	%rdi, %rdx
	cmpq	%rdx, %rdi
	jnb	.L4343
	movq	%rdx, %rcx
	movq	%rax, %r11
	leaq	16(%rax), %rsi
	subq	%rax, %rcx
	negq	%r11
	subq	$16, %rcx
	cmpq	$14, %rcx
	seta	%r10b
	cmpq	%rsi, %rdx
	setnb	%cl
	testb	%cl, %r10b
	je	.L4368
	leaq	15(%r14), %rcx
	subq	%rdi, %rcx
	cmpq	$30, %rcx
	jbe	.L4368
	movq	%rdx, %rbx
	subq	%rax, %rbx
	cmpq	%rsi, %rdx
	movl	$1, %eax
	leaq	-15(%rbx), %r10
	cmovb	%rax, %r10
	movq	%rdi, %rax
	xorl	%ecx, %ecx
	addq	%r14, %r11
	leaq	-16(%r10), %rsi
	shrq	$4, %rsi
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L4350:
	movdqu	-15(%rax,%r11), %xmm0
	addq	$1, %rcx
	addq	$16, %rax
	movups	%xmm0, -16(%rax)
	cmpq	%rcx, %rsi
	ja	.L4350
	salq	$4, %rsi
	addq	%rsi, %rdi
	leaq	(%r14,%rsi), %r9
	cmpq	%rsi, %r10
	je	.L4343
	movzbl	(%r9), %eax
	movb	%al, (%rdi)
	leaq	1(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	1(%r9), %eax
	movb	%al, 1(%rdi)
	leaq	2(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	2(%r9), %eax
	movb	%al, 2(%rdi)
	leaq	3(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	3(%r9), %eax
	movb	%al, 3(%rdi)
	leaq	4(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	4(%r9), %eax
	movb	%al, 4(%rdi)
	leaq	5(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	5(%r9), %eax
	movb	%al, 5(%rdi)
	leaq	6(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	6(%r9), %eax
	movb	%al, 6(%rdi)
	leaq	7(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	7(%r9), %eax
	movb	%al, 7(%rdi)
	leaq	8(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	8(%r9), %eax
	movb	%al, 8(%rdi)
	leaq	9(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	9(%r9), %eax
	movb	%al, 9(%rdi)
	leaq	10(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	10(%r9), %eax
	movb	%al, 10(%rdi)
	leaq	11(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	11(%r9), %eax
	movb	%al, 11(%rdi)
	leaq	12(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	12(%r9), %eax
	movb	%al, 12(%rdi)
	leaq	13(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	13(%r9), %eax
	movb	%al, 13(%rdi)
	leaq	14(%rdi), %rax
	cmpq	%rax, %rdx
	jbe	.L4343
	movzbl	14(%r9), %eax
	movb	%al, 14(%rdi)
	.p2align 4,,10
	.p2align 3
.L4343:
	testb	%r13b, %r13b
	jne	.L4371
.L4355:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4372
	addq	$40, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4371:
	.cfi_restore_state
	leal	1(%r12), %eax
	movq	4648(%r15), %rdx
	sall	$4, %eax
	cltq
	movq	-1(%rax,%rdx), %rax
	cmpq	%rax, 88(%r15)
	je	.L4357
	movq	37672(%r15), %rax
	movq	%rax, %rsi
	shrq	$9, %rsi
	cmpq	$8389119, %rax
	movl	$16384, %eax
	cmova	%rax, %rsi
	movl	$512, %eax
	cmpq	$512, %rsi
	cmovb	%rax, %rsi
	addl	%esi, %esi
	cmpl	%esi, 11(%rdx)
	jne	.L4373
.L4357:
	movq	-72(%rbp), %rax
	movq	%rdx, -64(%rbp)
	addl	%r12d, %r12d
	leaq	-64(%rbp), %r13
	movl	%r12d, %esi
	movq	%r13, %rdi
	movq	%r8, -80(%rbp)
	movq	(%rax), %rdx
	call	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	movq	-80(%rbp), %r8
	leal	1(%r12), %esi
	movq	%r13, %rdi
	movq	4648(%r15), %rax
	movq	%r8, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	(%r8), %rdx
	call	_ZN2v88internal10FixedArray3setEiNS0_6ObjectE
	movq	-72(%rbp), %r8
	jmp	.L4355
	.p2align 4,,10
	.p2align 3
.L4369:
	movzbl	(%r14), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L4343
.L4342:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4373:
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE.part.0
	movq	-72(%rbp), %r8
	movq	(%rax), %rax
	movq	%rax, 4648(%r15)
	jmp	.L4355
	.p2align 4,,10
	.p2align 3
.L4370:
	movq	%r14, %rsi
	movq	%r8, -80(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %r8
	jmp	.L4343
	.p2align 4,,10
	.p2align 3
.L4368:
	addq	%r14, %r11
	.p2align 4,,10
	.p2align 3
.L4365:
	movq	%rdi, %rax
	movzbl	-15(%rdi,%r11), %ecx
	addq	$1, %rdi
	movb	%cl, (%rax)
	cmpq	%rdx, %rdi
	jb	.L4365
	jmp	.L4343
.L4372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24233:
	.size	_ZN2v88internal7Factory22NumberToStringCacheSetENS0_6HandleINS0_6ObjectEEEiPKcb, .-_ZN2v88internal7Factory22NumberToStringCacheSetENS0_6HandleINS0_6ObjectEEEiPKcb
	.section	.text._ZN2v88internal7Factory22NumberToStringCacheGetENS0_6ObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory22NumberToStringCacheGetENS0_6ObjectEi
	.type	_ZN2v88internal7Factory22NumberToStringCacheGetENS0_6ObjectEi, @function
_ZN2v88internal7Factory22NumberToStringCacheGetENS0_6ObjectEi:
.LFB24234:
	.cfi_startproc
	endbr64
	leal	1(%rdx), %ecx
	movq	4648(%rdi), %r8
	sall	$4, %ecx
	movslq	%ecx, %rcx
	movq	-1(%rcx,%r8), %rax
	cmpq	%rsi, %rax
	je	.L4379
	testb	$1, %al
	jne	.L4388
.L4378:
	leaq	88(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4388:
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L4378
	testb	$1, %sil
	je	.L4378
	movq	-1(%rsi), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L4378
	movsd	7(%rsi), %xmm0
	ucomisd	7(%rax), %xmm0
	jp	.L4378
	jne	.L4378
	.p2align 4,,10
	.p2align 3
.L4379:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	3(%rdx,%rdx), %eax
	sall	$3, %eax
	cltq
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	-1(%rax,%r8), %rsi
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L4389
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4389:
	.cfi_restore_state
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L4390
.L4383:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%rsi, (%rax)
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4390:
	.cfi_restore_state
	movq	%rsi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-16(%rbp), %rsi
	movq	-8(%rbp), %rdi
	jmp	.L4383
	.cfi_endproc
.LFE24234:
	.size	_ZN2v88internal7Factory22NumberToStringCacheGetENS0_6ObjectEi, .-_ZN2v88internal7Factory22NumberToStringCacheGetENS0_6ObjectEi
	.section	.text._ZN2v88internal7Factory14NumberToStringENS0_3SmiEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb
	.type	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb, @function
_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb:
.LFB24239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	sarq	$32, %r15
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%dl, %dl
	je	.L4399
	movq	4648(%rdi), %rax
	movslq	11(%rax), %r13
	sarl	%r13d
	subl	$1, %r13d
	andl	%r15d, %r13d
	movl	%r13d, %edx
	call	_ZN2v88internal7Factory22NumberToStringCacheGetENS0_6ObjectEi
	movq	(%rax), %rcx
	cmpq	%rcx, 88(%r12)
	je	.L4392
.L4394:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4401
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4399:
	.cfi_restore_state
	xorl	%r13d, %r13d
.L4392:
	movl	%r15d, %edi
	leaq	-160(%rbp), %rsi
	movl	$100, %edx
	movzbl	%bl, %ebx
	call	_ZN2v88internal12IntToCStringEiNS0_6VectorIcEE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L4395
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4396:
	movl	%ebx, %r8d
	movq	%r15, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory22NumberToStringCacheSetENS0_6HandleINS0_6ObjectEEEiPKcb
	jmp	.L4394
	.p2align 4,,10
	.p2align 3
.L4395:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L4402
.L4397:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L4396
	.p2align 4,,10
	.p2align 3
.L4402:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4397
.L4401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24239:
	.size	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb, .-_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb
	.section	.text._ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb:
.LFB24235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	addq	$-128, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	$1, %sil
	je	.L4422
	movq	7(%rsi), %rax
	movq	%rax, %xmm0
	comisd	.LC20(%rip), %xmm0
	jb	.L4406
	movsd	.LC21(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L4406
	movabsq	$-9223372036854775808, %rdx
	cmpq	%rdx, %rax
	jne	.L4423
	.p2align 4,,10
	.p2align 3
.L4406:
	testb	%bl, %bl
	je	.L4412
	movq	4648(%r12), %rdx
	movq	%r12, %rdi
	movsd	%xmm0, -152(%rbp)
	movslq	11(%rdx), %r14
	movq	%rax, %rdx
	sarq	$32, %rdx
	sarl	%r14d
	xorl	%edx, %eax
	subl	$1, %r14d
	andl	%eax, %r14d
	movl	%r14d, %edx
	call	_ZN2v88internal7Factory22NumberToStringCacheGetENS0_6ObjectEi
	movsd	-152(%rbp), %xmm0
	movq	(%rax), %rcx
	cmpq	%rcx, 88(%r12)
	jne	.L4405
.L4409:
	leaq	-144(%rbp), %rdi
	movl	$100, %esi
	call	_ZN2v88internal15DoubleToCStringEdNS0_6VectorIcEE@PLT
	movzbl	%bl, %r8d
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory22NumberToStringCacheSetENS0_6HandleINS0_6ObjectEEEiPKcb
	jmp	.L4405
	.p2align 4,,10
	.p2align 3
.L4423:
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L4406
	jne	.L4406
	movq	%rdx, %rsi
	salq	$32, %rsi
.L4422:
	movzbl	%bl, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb
.L4405:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4424
	subq	$-128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4412:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L4409
.L4424:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24235:
	.size	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb
	.section	.text._ZN2v88internal7Factory17NewClassPositionsEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewClassPositionsEii
	.type	_ZN2v88internal7Factory17NewClassPositionsEii, @function
_ZN2v88internal7Factory17NewClassPositionsEii:
.LFB24240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	movl	$85, %esi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	movl	$1, %edx
	salq	$32, %r12
	salq	$32, %rbx
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	(%rax), %rdx
	movq	%r12, 7(%rdx)
	movq	(%rax), %rdx
	movq	%rbx, 15(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24240:
	.size	_ZN2v88internal7Factory17NewClassPositionsEii, .-_ZN2v88internal7Factory17NewClassPositionsEii
	.section	.text._ZN2v88internal7Factory12NewDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory12NewDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	.type	_ZN2v88internal7Factory12NewDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE, @function
_ZN2v88internal7Factory12NewDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE:
.LFB24241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	37592(%rdi), %rax
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$86, %esi
	subq	$24, %rsp
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	$0, 55(%rax)
	movq	(%r12), %r15
	movq	(%rbx), %r14
	movq	%r14, 7(%r15)
	leaq	7(%r15), %r13
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	jne	.L4459
.L4428:
	movq	(%r12), %rax
	movq	$0, 15(%rax)
	movq	(%rbx), %rax
	movq	(%r12), %r15
	movq	31(%rax), %r14
	leaq	23(%r15), %r13
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%r14, 23(%r15)
	movq	%r14, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	jne	.L4460
.L4430:
	movq	-56(%rbp), %r13
	movq	(%r12), %r15
	movq	-37504(%r13), %r14
	leaq	31(%r15), %rsi
	movq	%r15, %rdi
	subq	$37592, %r13
	movq	%r14, 31(%r15)
	movq	%r14, %rdx
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	jne	.L4461
.L4432:
	movq	(%r12), %r15
	movq	88(%r13), %r14
	movq	%r14, 39(%r15)
	leaq	39(%r15), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	jne	.L4462
.L4434:
	movq	(%r12), %r14
	movq	288(%r13), %r13
	movq	%r13, 47(%r14)
	leaq	47(%r14), %r15
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L4463
.L4436:
	movq	(%rbx), %r14
	movq	(%r12), %r13
	movq	%r13, 31(%r14)
	leaq	31(%r14), %r15
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	jne	.L4464
.L4438:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4459:
	.cfi_restore_state
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4428
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4428
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4428
	.p2align 4,,10
	.p2align 3
.L4464:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4438
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4438
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4438
	.p2align 4,,10
	.p2align 3
.L4463:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4436
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4436
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4436
	.p2align 4,,10
	.p2align 3
.L4462:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4434
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4434
	movq	-56(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4434
	.p2align 4,,10
	.p2align 3
.L4461:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4432
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4432
	movq	-56(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4432
	.p2align 4,,10
	.p2align 3
.L4460:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4430
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4430
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4430
	.cfi_endproc
.LFE24241:
	.size	_ZN2v88internal7Factory12NewDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE, .-_ZN2v88internal7Factory12NewDebugInfoENS0_6HandleINS0_18SharedFunctionInfoEEE
	.section	.text._ZN2v88internal7Factory15NewCoverageInfoERKNS0_10ZoneVectorINS0_11SourceRangeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory15NewCoverageInfoERKNS0_10ZoneVectorINS0_11SourceRangeEEE
	.type	_ZN2v88internal7Factory15NewCoverageInfoERKNS0_10ZoneVectorINS0_11SourceRangeEEE, @function
_ZN2v88internal7Factory15NewCoverageInfoERKNS0_10ZoneVectorINS0_11SourceRangeEEE:
.LFB24242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	288(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rsi), %rbx
	subq	8(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	sarq	$3, %rbx
	movl	%ebx, %esi
	sall	$2, %esi
	jne	.L4473
.L4467:
	testl	%ebx, %ebx
	jle	.L4468
	leal	-1(%rbx), %r13d
	leaq	-64(%rbp), %r12
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L4469:
	movq	8(%r14), %rax
	movl	%ebx, %esi
	movq	%r12, %rdi
	leaq	(%rax,%rbx,8), %rdx
	movl	(%rdx), %r8d
	movl	4(%rdx), %ecx
	movq	(%r15), %rdx
	movq	%rdx, -64(%rbp)
	movl	%r8d, %edx
	call	_ZN2v88internal12CoverageInfo14InitializeSlotEiii@PLT
	movq	%rbx, %rdx
	addq	$1, %rbx
	cmpq	%rdx, %r13
	jne	.L4469
.L4468:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4474
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4473:
	.cfi_restore_state
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory26NewUninitializedFixedArrayEiNS0_14AllocationTypeE.part.0
	movq	%rax, %r15
	jmp	.L4467
.L4474:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24242:
	.size	_ZN2v88internal7Factory15NewCoverageInfoERKNS0_10ZoneVectorINS0_11SourceRangeEEE, .-_ZN2v88internal7Factory15NewCoverageInfoERKNS0_10ZoneVectorINS0_11SourceRangeEEE
	.section	.text._ZN2v88internal7Factory17NewBreakPointInfoEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewBreakPointInfoEi
	.type	_ZN2v88internal7Factory17NewBreakPointInfoEi, @function
_ZN2v88internal7Factory17NewBreakPointInfoEi:
.LFB24243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movl	$102, %esi
	salq	$32, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%rbx, 7(%rax)
	movq	(%r12), %r14
	movq	88(%r13), %r13
	movq	%r13, 15(%r14)
	testb	$1, %r13b
	je	.L4479
	movq	%r13, %rbx
	leaq	15(%r14), %r15
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L4487
	testb	$24, %al
	je	.L4479
.L4489:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4488
.L4479:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4487:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L4489
	jmp	.L4479
	.p2align 4,,10
	.p2align 3
.L4488:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4479
	.cfi_endproc
.LFE24243:
	.size	_ZN2v88internal7Factory17NewBreakPointInfoEi, .-_ZN2v88internal7Factory17NewBreakPointInfoEi
	.section	.text._ZN2v88internal7Factory13NewBreakPointEiNS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory13NewBreakPointEiNS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal7Factory13NewBreakPointEiNS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal7Factory13NewBreakPointEiNS0_6HandleINS0_6StringEEE:
.LFB24244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$1, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movl	$102, %esi
	salq	$32, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%rbx, 7(%rax)
	movq	(%r12), %r14
	movq	0(%r13), %r13
	movq	%r13, 15(%r14)
	testb	$1, %r13b
	je	.L4494
	movq	%r13, %rbx
	leaq	15(%r14), %r15
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L4502
	testb	$24, %al
	je	.L4494
.L4504:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4503
.L4494:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4502:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L4504
	jmp	.L4494
	.p2align 4,,10
	.p2align 3
.L4503:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4494
	.cfi_endproc
.LFE24244:
	.size	_ZN2v88internal7Factory13NewBreakPointEiNS0_6HandleINS0_6StringEEE, .-_ZN2v88internal7Factory13NewBreakPointEiNS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal7Factory18NewStackTraceFrameENS0_6HandleINS0_10FrameArrayEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18NewStackTraceFrameENS0_6HandleINS0_10FrameArrayEEEi
	.type	_ZN2v88internal7Factory18NewStackTraceFrameENS0_6HandleINS0_10FrameArrayEEEi, @function
_ZN2v88internal7Factory18NewStackTraceFrameENS0_6HandleINS0_10FrameArrayEEEi:
.LFB24245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$100, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	xorl	%edx, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	(%r14), %r14
	movq	(%rax), %r15
	movq	%rax, %r12
	movq	%r14, 7(%r15)
	testb	$1, %r14b
	je	.L4513
	movq	%r14, %rcx
	leaq	7(%r15), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L4527
	testb	$24, %al
	je	.L4513
.L4532:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4528
	.p2align 4,,10
	.p2align 3
.L4513:
	movq	(%r12), %rax
	salq	$32, %r13
	movq	%r13, 15(%rax)
	movq	(%r12), %r14
	movq	88(%rbx), %r13
	movq	%r13, 23(%r14)
	leaq	23(%r14), %rsi
	testb	$1, %r13b
	je	.L4512
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L4529
	testb	$24, %al
	je	.L4512
.L4531:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4530
.L4512:
	movl	41840(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 41840(%rbx)
	movq	(%r12), %rdx
	salq	$32, %rax
	movq	%rax, 31(%rdx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4529:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L4531
	jmp	.L4512
	.p2align 4,,10
	.p2align 3
.L4527:
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L4532
	jmp	.L4513
	.p2align 4,,10
	.p2align 3
.L4528:
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4513
	.p2align 4,,10
	.p2align 3
.L4530:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4512
	.cfi_endproc
.LFE24245:
	.size	_ZN2v88internal7Factory18NewStackTraceFrameENS0_6HandleINS0_10FrameArrayEEEi, .-_ZN2v88internal7Factory18NewStackTraceFrameENS0_6HandleINS0_10FrameArrayEEEi
	.section	.text._ZN2v88internal7Factory17NewStackFrameInfoENS0_6HandleINS0_10FrameArrayEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17NewStackFrameInfoENS0_6HandleINS0_10FrameArrayEEEi
	.type	_ZN2v88internal7Factory17NewStackFrameInfoENS0_6HandleINS0_10FrameArrayEEEi, @function
_ZN2v88internal7Factory17NewStackFrameInfoENS0_6HandleINS0_10FrameArrayEEEi:
.LFB24246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-240(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	subq	$328, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18FrameArrayIteratorC1EPNS0_7IsolateENS0_6HandleINS0_10FrameArrayEEEi@PLT
	leal	(%rbx,%rbx,2), %eax
	leal	7(%rax,%rax), %ebx
	sall	$3, %ebx
	movslq	%ebx, %rax
	movq	%rax, -312(%rbp)
	movq	%rax, %rcx
	movq	(%r12), %rax
	leaq	-1(%rcx,%rax), %rax
	movq	(%rax), %rdx
	btq	$32, %rdx
	jnc	.L4534
.L4536:
	movl	$1, %r15d
.L4535:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal18FrameArrayIterator5FrameEv@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%rbx, %rdi
	call	*104(%rax)
	movq	%rbx, %rdi
	movl	%eax, -272(%rbp)
	movq	(%rbx), %rax
	call	*112(%rax)
	movq	%rbx, %rdi
	movl	%eax, -280(%rbp)
	call	_ZNK2v88internal14StackFrameBase11GetScriptIdEv@PLT
	movq	%rbx, %rdi
	movl	%eax, -288(%rbp)
	movq	(%rbx), %rax
	call	*32(%rax)
	movq	%rbx, %rdi
	movq	%rax, -320(%rbp)
	movq	(%rbx), %rax
	call	*48(%rax)
	movq	%rbx, %rdi
	movq	%rax, -328(%rbp)
	movq	(%rbx), %rax
	call	*40(%rax)
	movq	%rax, -336(%rbp)
	testb	%r15b, %r15b
	jne	.L4537
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L4538
.L4540:
	xorl	%r14d, %r14d
.L4537:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*72(%rax)
	movq	%rbx, %rdi
	movq	%rax, -344(%rbp)
	movq	(%rbx), %rax
	call	*80(%rax)
	movq	%rbx, %rdi
	movq	%rax, -352(%rbp)
	movq	(%rbx), %rax
	call	*88(%rax)
	movq	%rbx, %rdi
	movq	%rax, -360(%rbp)
	movq	(%rbx), %rax
	call	*136(%rax)
	movq	%rbx, %rdi
	movb	%al, -257(%rbp)
	movq	(%rbx), %rax
	call	*168(%rax)
	cmpb	$0, -257(%rbp)
	movb	%al, -258(%rbp)
	jne	.L4573
	testb	%al, %al
	jne	.L4573
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	%rbx, %rdi
	movq	%rax, -296(%rbp)
	movq	(%rbx), %rax
	call	*64(%rax)
	movq	%rax, -304(%rbp)
.L4546:
	xorl	%edx, %edx
	movq	%r13, %rdi
	movl	$99, %esi
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	%rax, %r13
	movq	(%rax), %rax
	movq	$0, 103(%rax)
	movq	0(%r13), %rdx
	movslq	107(%rdx), %rax
	movl	%eax, %ecx
	andl	$-5, %eax
	orl	$4, %ecx
	testb	%r15b, %r15b
	cmovne	%ecx, %eax
	salq	$32, %rax
	movq	%rax, 103(%rdx)
	movq	0(%r13), %rcx
	movq	(%r12), %rax
	movq	-312(%rbp), %rsi
	movq	-1(%rsi,%rax), %rdx
	movslq	107(%rcx), %rax
	sarq	$32, %rdx
	movl	%eax, %esi
	andl	$-9, %eax
	orl	$8, %esi
	andl	$4, %edx
	cmovne	%esi, %eax
	salq	$32, %rax
	movq	%rax, 103(%rcx)
	movq	0(%r13), %rdx
	movslq	107(%rdx), %rax
	movl	%eax, %ecx
	andl	$-17, %eax
	orl	$16, %ecx
	testb	%r14b, %r14b
	cmovne	%ecx, %eax
	salq	$32, %rax
	movq	%rax, 103(%rdx)
	movq	-272(%rbp), %rax
	movq	0(%r13), %rdx
	salq	$32, %rax
	movq	%rax, 7(%rdx)
	movq	-280(%rbp), %rax
	movq	0(%r13), %rdx
	salq	$32, %rax
	movq	%rax, 15(%rdx)
	movq	-288(%rbp), %rax
	movq	0(%r13), %rdx
	salq	$32, %rax
	movq	%rax, 31(%rdx)
	movq	-320(%rbp), %rax
	movq	0(%r13), %r12
	movq	(%rax), %r15
	leaq	39(%r12), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r15, 39(%r12)
	movq	%r15, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	je	.L4553
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4553:
	movq	-328(%rbp), %rax
	movq	0(%r13), %r12
	movq	(%rax), %r15
	leaq	47(%r12), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r15, 47(%r12)
	movq	%r15, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	je	.L4554
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4554:
	movq	-336(%rbp), %rax
	movq	0(%r13), %r12
	movq	(%rax), %r15
	leaq	55(%r12), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r15, 55(%r12)
	movq	%r15, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	je	.L4555
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4555:
	movq	-296(%rbp), %rax
	movq	0(%r13), %r12
	movq	(%rax), %r14
	leaq	63(%r12), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, 63(%r12)
	movq	%r14, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L4556
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4556:
	movq	-304(%rbp), %rax
	movq	0(%r13), %r12
	movq	(%rax), %r14
	leaq	71(%r12), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, 71(%r12)
	movq	%r14, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L4557
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4557:
	movq	-344(%rbp), %rax
	movq	0(%r13), %r12
	movq	(%rax), %r14
	leaq	79(%r12), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, 79(%r12)
	movq	%r14, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L4558
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4558:
	movq	-352(%rbp), %rax
	movq	0(%r13), %r12
	movq	(%rax), %r14
	leaq	87(%r12), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, 87(%r12)
	movq	%r14, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L4559
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4559:
	movq	-360(%rbp), %rax
	movq	0(%r13), %r12
	movq	(%rax), %r14
	leaq	95(%r12), %r15
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%r14, 95(%r12)
	movq	%r14, %rdx
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L4560
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4560:
	movq	(%rbx), %rax
	movq	0(%r13), %r12
	movq	%rbx, %rdi
	call	*144(%rax)
	movq	%rbx, %rdi
	movl	%eax, %r9d
	movslq	107(%r12), %rax
	movl	%eax, %edx
	andl	$-2, %eax
	orl	$1, %edx
	testb	%r9b, %r9b
	cmovne	%edx, %eax
	salq	$32, %rax
	movq	%rax, 103(%r12)
	movq	0(%r13), %rdx
	movslq	107(%rdx), %rax
	movl	%eax, %ecx
	andl	$-3, %eax
	orl	$2, %ecx
	cmpb	$0, -258(%rbp)
	cmovne	%ecx, %eax
	salq	$32, %rax
	movq	%rax, 103(%rdx)
	movq	0(%r13), %rdx
	movslq	107(%rdx), %rax
	movl	%eax, %ecx
	andl	$-33, %eax
	orl	$32, %ecx
	cmpb	$0, -257(%rbp)
	cmovne	%ecx, %eax
	salq	$32, %rax
	movq	%rax, 103(%rdx)
	movq	(%rbx), %rax
	movq	0(%r13), %r12
	call	*152(%rax)
	movq	%rbx, %rdi
	movl	%eax, %r9d
	movslq	107(%r12), %rax
	movl	%eax, %edx
	andl	$-65, %eax
	orl	$64, %edx
	testb	%r9b, %r9b
	cmovne	%edx, %eax
	salq	$32, %rax
	movq	%rax, 103(%r12)
	movq	(%rbx), %rax
	movq	0(%r13), %r12
	call	*160(%rax)
	movq	%rbx, %rdi
	movl	%eax, %r9d
	movslq	107(%r12), %rax
	movl	%eax, %edx
	andb	$127, %al
	orb	$-128, %dl
	testb	%r9b, %r9b
	cmovne	%edx, %eax
	salq	$32, %rax
	movq	%rax, 103(%r12)
	movq	(%rbx), %rax
	movq	0(%r13), %r12
	call	*120(%rax)
	salq	$32, %rax
	movq	%rax, 23(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4575
	addq	$328, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4573:
	.cfi_restore_state
	leaq	88(%r13), %rax
	movq	%rax, -296(%rbp)
	movq	%rax, -304(%rbp)
	jmp	.L4546
	.p2align 4,,10
	.p2align 3
.L4534:
	movq	(%rax), %rdx
	btq	$33, %rdx
	jc	.L4536
	movq	(%rax), %r15
	shrq	$34, %r15
	andl	$1, %r15d
	jmp	.L4535
	.p2align 4,,10
	.p2align 3
.L4538:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L4540
	movq	23(%rax), %rax
	movq	31(%rax), %rdx
	movq	%rdx, %rax
	notq	%rax
	movl	%eax, %r14d
	andl	$1, %r14d
	je	.L4576
.L4541:
	leaq	-248(%rbp), %rdi
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movl	%eax, %r14d
	jmp	.L4537
	.p2align 4,,10
	.p2align 3
.L4576:
	movq	-1(%rdx), %rax
	cmpw	$86, 11(%rax)
	je	.L4577
.L4542:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rdx
	je	.L4537
	jmp	.L4541
.L4577:
	movq	23(%rdx), %rdx
	testb	$1, %dl
	jne	.L4542
	jmp	.L4541
.L4575:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24246:
	.size	_ZN2v88internal7Factory17NewStackFrameInfoENS0_6HandleINS0_10FrameArrayEEEi, .-_ZN2v88internal7Factory17NewStackFrameInfoENS0_6HandleINS0_10FrameArrayEEEi
	.section	.text._ZN2v88internal7Factory36NewSourcePositionTableWithFrameCacheENS0_6HandleINS0_9ByteArrayEEENS2_INS0_22SimpleNumberDictionaryEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory36NewSourcePositionTableWithFrameCacheENS0_6HandleINS0_9ByteArrayEEENS2_INS0_22SimpleNumberDictionaryEEE
	.type	_ZN2v88internal7Factory36NewSourcePositionTableWithFrameCacheENS0_6HandleINS0_9ByteArrayEEENS2_INS0_22SimpleNumberDictionaryEEE, @function
_ZN2v88internal7Factory36NewSourcePositionTableWithFrameCacheENS0_6HandleINS0_9ByteArrayEEENS2_INS0_22SimpleNumberDictionaryEEE:
.LFB24250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$97, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movl	$1, %edx
	subq	$24, %rsp
	call	_ZN2v88internal7Factory9NewStructENS0_12InstanceTypeENS0_14AllocationTypeE
	movq	0(%r13), %r13
	movq	(%rax), %r14
	movq	%rax, %r12
	movq	%r13, 7(%r14)
	testb	$1, %r13b
	je	.L4586
	movq	%r13, %r15
	leaq	7(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L4600
	testb	$24, %al
	je	.L4586
.L4605:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4601
	.p2align 4,,10
	.p2align 3
.L4586:
	movq	(%r12), %r14
	movq	(%rbx), %r13
	movq	%r13, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %r13b
	je	.L4585
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L4602
	testb	$24, %al
	je	.L4585
.L4604:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4603
.L4585:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4602:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L4604
	jmp	.L4585
	.p2align 4,,10
	.p2align 3
.L4600:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L4605
	jmp	.L4586
	.p2align 4,,10
	.p2align 3
.L4601:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4586
	.p2align 4,,10
	.p2align 3
.L4603:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4585
	.cfi_endproc
.LFE24250:
	.size	_ZN2v88internal7Factory36NewSourcePositionTableWithFrameCacheENS0_6HandleINS0_9ByteArrayEEENS2_INS0_22SimpleNumberDictionaryEEE, .-_ZN2v88internal7Factory36NewSourcePositionTableWithFrameCacheENS0_6HandleINS0_9ByteArrayEEENS2_INS0_22SimpleNumberDictionaryEEE
	.section	.text._ZN2v88internal7Factory18NewArgumentsObjectENS0_6HandleINS0_10JSFunctionEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18NewArgumentsObjectENS0_6HandleINS0_10JSFunctionEEEi
	.type	_ZN2v88internal7Factory18NewArgumentsObjectENS0_6HandleINS0_10JSFunctionEEEi, @function
_ZN2v88internal7Factory18NewArgumentsObjectENS0_6HandleINS0_10JSFunctionEEEi:
.LFB24251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$64, %edx
	jne	.L4612
	movq	23(%rax), %rdx
	movq	15(%rdx), %rax
	testb	$1, %al
	jne	.L4626
.L4610:
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
.L4611:
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L4612
	movq	15(%rax), %rax
	btq	$47, %rax
	jnc	.L4612
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1191(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4617
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4618:
	xorl	%r14d, %r14d
	jmp	.L4616
	.p2align 4,,10
	.p2align 3
.L4612:
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1207(%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4627
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4614:
	movl	$1, %r14d
.L4616:
	movq	%r12, %rdi
	xorl	%edx, %edx
	salq	$32, %rbx
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE.constprop.0
	movq	41112(%r12), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L4620
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L4621:
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	2768(%r12), %rdx
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%r14b, %r14b
	je	.L4628
.L4623:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4627:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L4629
.L4615:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L4614
	.p2align 4,,10
	.p2align 3
.L4620:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L4630
.L4622:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rbx, (%rcx)
	jmp	.L4621
	.p2align 4,,10
	.p2align 3
.L4628:
	movl	$1, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r15, %rsi
	leaq	2224(%r12), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	jmp	.L4623
	.p2align 4,,10
	.p2align 3
.L4626:
	movq	-1(%rax), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L4610
	jmp	.L4611
	.p2align 4,,10
	.p2align 3
.L4630:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L4622
	.p2align 4,,10
	.p2align 3
.L4617:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L4631
.L4619:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L4618
	.p2align 4,,10
	.p2align 3
.L4629:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4615
	.p2align 4,,10
	.p2align 3
.L4631:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L4619
	.cfi_endproc
.LFE24251:
	.size	_ZN2v88internal7Factory18NewArgumentsObjectENS0_6HandleINS0_10JSFunctionEEEi, .-_ZN2v88internal7Factory18NewArgumentsObjectENS0_6HandleINS0_10JSFunctionEEEi
	.section	.text._ZN2v88internal7Factory25ObjectLiteralMapFromCacheENS0_6HandleINS0_13NativeContextEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory25ObjectLiteralMapFromCacheENS0_6HandleINS0_13NativeContextEEEi
	.type	_ZN2v88internal7Factory25ObjectLiteralMapFromCacheENS0_6HandleINS0_13NativeContextEEEi, @function
_ZN2v88internal7Factory25ObjectLiteralMapFromCacheENS0_6HandleINS0_13NativeContextEEEi:
.LFB24252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	testl	%edx, %edx
	je	.L4664
	movl	%edx, %r13d
	cmpl	$128, %edx
	jle	.L4638
	movq	1223(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4651
.L4661:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4638:
	.cfi_restore_state
	movq	%rsi, %r15
	movq	799(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4642
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L4643:
	leal	8(,%r13,8), %eax
	leaq	37592(%r12), %rdi
	cltq
	leaq	-1(%rax), %rbx
	cmpq	%rsi, 88(%r12)
	je	.L4665
	movq	-1(%rsi,%rax), %rsi
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$3, %rax
	je	.L4666
.L4650:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map6CreateEPNS0_7IsolateEi@PLT
	movq	(%r14), %r14
	movq	(%rax), %rdx
	movq	%rax, %r12
	leaq	(%r14,%rbx), %r13
	movq	%rdx, %rax
	orq	$2, %rax
	movq	%rax, 0(%r13)
	testb	$1, %al
	je	.L4656
	cmpl	$3, %eax
	je	.L4656
	movq	%rdx, %r15
	andq	$-262144, %rdx
	andq	$-3, %r15
	testb	$4, 10(%rdx)
	jne	.L4667
.L4655:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L4656:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4664:
	.cfi_restore_state
	movq	879(%rax), %rax
	movq	41112(%rdi), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	jne	.L4661
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	jne	.L4653
.L4662:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L4653
	.p2align 4,,10
	.p2align 3
.L4666:
	cmpl	$3, %esi
	je	.L4650
	movq	41112(%r12), %rdi
	andq	$-3, %rsi
	testq	%rdi, %rdi
	jne	.L4661
	.p2align 4,,10
	.p2align 3
.L4651:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L4662
.L4653:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4642:
	.cfi_restore_state
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L4668
.L4644:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L4643
	.p2align 4,,10
	.p2align 3
.L4665:
	movl	$1040, %esi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	movq	696(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L4646
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L4647:
	movabsq	$549755813888, %rax
	movq	%rax, 7(%rsi)
	movq	(%r14), %rcx
	movq	88(%r12), %rax
	leaq	15(%rcx), %rdi
	movl	$128, %ecx
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	movq	(%r15), %r15
	movq	(%r14), %rdx
	movq	%rdx, 799(%r15)
	leaq	799(%r15), %rsi
	movq	%r15, %rdi
	movq	%rdx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	movq	-56(%rbp), %rdx
	testb	$1, %dl
	je	.L4650
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
	jmp	.L4650
	.p2align 4,,10
	.p2align 3
.L4646:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L4669
.L4648:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L4647
	.p2align 4,,10
	.p2align 3
.L4667:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4655
.L4669:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L4648
	.p2align 4,,10
	.p2align 3
.L4668:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L4644
	.cfi_endproc
.LFE24252:
	.size	_ZN2v88internal7Factory25ObjectLiteralMapFromCacheENS0_6HandleINS0_13NativeContextEEEi, .-_ZN2v88internal7Factory25ObjectLiteralMapFromCacheENS0_6HandleINS0_13NativeContextEEEi
	.section	.text._ZN2v88internal7Factory14NewLoadHandlerEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory14NewLoadHandlerEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory14NewLoadHandlerEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory14NewLoadHandlerEiNS0_14AllocationTypeE:
.LFB24253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$2, %esi
	je	.L4671
	cmpl	$3, %esi
	je	.L4672
	cmpl	$1, %esi
	je	.L4679
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4671:
	leaq	4320(%rdi), %rsi
.L4674:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L4675
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4675:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L4680
.L4677:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4679:
	.cfi_restore_state
	leaq	4312(%rdi), %rsi
	jmp	.L4674
	.p2align 4,,10
	.p2align 3
.L4672:
	leaq	4328(%rdi), %rsi
	jmp	.L4674
	.p2align 4,,10
	.p2align 3
.L4680:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L4677
	.cfi_endproc
.LFE24253:
	.size	_ZN2v88internal7Factory14NewLoadHandlerEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory14NewLoadHandlerEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory15NewStoreHandlerEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory15NewStoreHandlerEi
	.type	_ZN2v88internal7Factory15NewStoreHandlerEi, @function
_ZN2v88internal7Factory15NewStoreHandlerEi:
.LFB24254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$2, %esi
	je	.L4682
	jg	.L4683
	testl	%esi, %esi
	je	.L4684
	cmpl	$1, %esi
	jne	.L4686
	leaq	4344(%rdi), %rsi
.L4688:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L4689
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4683:
	.cfi_restore_state
	cmpl	$3, %esi
	jne	.L4686
	leaq	4360(%rdi), %rsi
	jmp	.L4688
	.p2align 4,,10
	.p2align 3
.L4689:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L4693
.L4691:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4684:
	.cfi_restore_state
	leaq	4336(%rdi), %rsi
	jmp	.L4688
	.p2align 4,,10
	.p2align 3
.L4682:
	leaq	4352(%rdi), %rsi
	jmp	.L4688
	.p2align 4,,10
	.p2align 3
.L4693:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L4691
.L4686:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24254:
	.size	_ZN2v88internal7Factory15NewStoreHandlerEi, .-_ZN2v88internal7Factory15NewStoreHandlerEi
	.section	.text._ZN2v88internal7Factory17SetRegExpAtomDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17SetRegExpAtomDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal7Factory17SetRegExpAtomDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal7Factory17SetRegExpAtomDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEENS2_INS0_6ObjectEEE:
.LFB24255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$4, %esi
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	salq	$32, %r13
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %rdx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE.constprop.0
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%r13, 15(%rax)
	movq	-56(%rbp), %r10
	movq	(%rbx), %rdi
	movq	(%r10), %r13
	movq	%r13, 23(%rdi)
	testb	$1, %r13b
	je	.L4706
	movq	%r13, %rcx
	leaq	23(%rdi), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L4726
	testb	$24, %al
	je	.L4706
.L4732:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4727
	.p2align 4,,10
	.p2align 3
.L4706:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 31(%rax)
	movq	(%rbx), %r13
	movq	(%r15), %r12
	movq	%r12, 39(%r13)
	leaq	39(%r13), %rsi
	testb	$1, %r12b
	je	.L4705
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L4728
	testb	$24, %al
	je	.L4705
.L4734:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4729
	.p2align 4,,10
	.p2align 3
.L4705:
	movq	(%r14), %r13
	movq	(%rbx), %r12
	movq	%r12, 23(%r13)
	leaq	23(%r13), %r14
	testb	$1, %r12b
	je	.L4694
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L4730
	testb	$24, %al
	je	.L4694
.L4733:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4731
.L4694:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4726:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L4732
	jmp	.L4706
	.p2align 4,,10
	.p2align 3
.L4730:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L4733
	jmp	.L4694
	.p2align 4,,10
	.p2align 3
.L4728:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L4734
	jmp	.L4705
	.p2align 4,,10
	.p2align 3
.L4727:
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4706
	.p2align 4,,10
	.p2align 3
.L4729:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4705
	.p2align 4,,10
	.p2align 3
.L4731:
	addq	$40, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE24255:
	.size	_ZN2v88internal7Factory17SetRegExpAtomDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEENS2_INS0_6ObjectEEE, .-_ZN2v88internal7Factory17SetRegExpAtomDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal7Factory21SetRegExpIrregexpDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory21SetRegExpIrregexpDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEEi
	.type	_ZN2v88internal7Factory21SetRegExpIrregexpDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEEi, @function
_ZN2v88internal7Factory21SetRegExpIrregexpDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEEi:
.LFB24257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$11, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	salq	$32, %r14
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	88(%rdi), %rdx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE.constprop.0
	movq	%rax, %rbx
	movq	(%rax), %rax
	movq	%r14, 15(%rax)
	movq	-56(%rbp), %r10
	movq	(%rbx), %rdi
	movq	(%r10), %r14
	movq	%r14, 23(%rdi)
	testb	$1, %r14b
	je	.L4749
	movq	%r14, %rcx
	leaq	23(%rdi), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L4772
	testb	$24, %al
	je	.L4749
.L4781:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4773
	.p2align 4,,10
	.p2align 3
.L4749:
	movq	(%rbx), %rax
	salq	$32, %r13
	movq	%r13, 31(%rax)
	movabsq	$-4294967296, %rax
	movq	(%rbx), %rdx
	movq	%rax, 39(%rdx)
	movq	(%rbx), %rdx
	movq	%rax, 47(%rdx)
	movq	(%rbx), %rdx
	movq	%rax, 55(%rdx)
	movq	(%rbx), %rdx
	movq	%rax, 63(%rdx)
	movq	(%rbx), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r13
	movq	%r13, 71(%r14)
	leaq	71(%r14), %rsi
	testb	$1, %r13b
	je	.L4748
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L4774
	testb	$24, %al
	je	.L4748
.L4780:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4775
	.p2align 4,,10
	.p2align 3
.L4748:
	movq	(%rbx), %rax
	salq	$32, %r12
	movabsq	$-4294967296, %rdx
	movq	%r12, 79(%rax)
	movq	(%rbx), %rax
	movq	%rdx, 87(%rax)
	movq	(%rbx), %r13
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r12
	movq	%r12, 95(%r13)
	leaq	95(%r13), %rsi
	testb	$1, %r12b
	je	.L4747
	movq	%r12, %r14
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	jne	.L4776
	testb	$24, %al
	je	.L4747
.L4779:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4777
	.p2align 4,,10
	.p2align 3
.L4747:
	movq	(%r15), %r13
	movq	(%rbx), %r12
	movq	%r12, 23(%r13)
	leaq	23(%r13), %r14
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r12b
	jne	.L4778
.L4735:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4776:
	.cfi_restore_state
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r14), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L4779
	jmp	.L4747
	.p2align 4,,10
	.p2align 3
.L4774:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L4780
	jmp	.L4748
	.p2align 4,,10
	.p2align 3
.L4772:
	movq	%r14, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L4781
	jmp	.L4749
	.p2align 4,,10
	.p2align 3
.L4778:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4735
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L4735
	addq	$40, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L4773:
	.cfi_restore_state
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4749
	.p2align 4,,10
	.p2align 3
.L4775:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4748
	.p2align 4,,10
	.p2align 3
.L4777:
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4747
	.cfi_endproc
.LFE24257:
	.size	_ZN2v88internal7Factory21SetRegExpIrregexpDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEEi, .-_ZN2v88internal7Factory21SetRegExpIrregexpDataENS0_6HandleINS0_8JSRegExpEEENS3_4TypeENS2_INS0_6StringEEENS_4base5FlagsINS3_4FlagEiEEi
	.section	.text._ZN2v88internal7Factory18NewRegExpMatchInfoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18NewRegExpMatchInfoEv
	.type	_ZN2v88internal7Factory18NewRegExpMatchInfoEv, @function
_ZN2v88internal7Factory18NewRegExpMatchInfoEv:
.LFB24258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$5, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	88(%rdi), %rdx
	call	_ZN2v88internal7Factory23NewFixedArrayWithFillerENS0_9RootIndexEiNS0_6ObjectENS0_14AllocationTypeE.constprop.0
	movabsq	$8589934592, %rdx
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	%rdx, 15(%rax)
	movq	(%r12), %r14
	movq	128(%rbx), %r13
	movq	%r13, 23(%r14)
	testb	$1, %r13b
	je	.L4790
	movq	%r13, %r15
	leaq	23(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L4804
	testb	$24, %al
	je	.L4790
.L4809:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4805
	.p2align 4,,10
	.p2align 3
.L4790:
	movq	(%r12), %r14
	movq	88(%rbx), %r13
	movq	%r13, 31(%r14)
	leaq	31(%r14), %r15
	testb	$1, %r13b
	je	.L4789
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L4806
	testb	$24, %al
	je	.L4789
.L4808:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4807
.L4789:
	movq	(%r12), %rax
	movq	$0, 39(%rax)
	movq	(%r12), %rax
	movq	$0, 47(%rax)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4806:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L4808
	jmp	.L4789
	.p2align 4,,10
	.p2align 3
.L4804:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L4809
	jmp	.L4790
	.p2align 4,,10
	.p2align 3
.L4805:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4790
	.p2align 4,,10
	.p2align 3
.L4807:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4789
	.cfi_endproc
.LFE24258:
	.size	_ZN2v88internal7Factory18NewRegExpMatchInfoEv, .-_ZN2v88internal7Factory18NewRegExpMatchInfoEv
	.section	.text._ZN2v88internal7Factory17GlobalConstantForENS0_6HandleINS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory17GlobalConstantForENS0_6HandleINS0_4NameEEE
	.type	_ZN2v88internal7Factory17GlobalConstantForENS0_6HandleINS0_4NameEEE, @function
_ZN2v88internal7Factory17GlobalConstantForENS0_6HandleINS0_4NameEEE:
.LFB24259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	3512(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpq	%rsi, %rdx
	je	.L4812
	movq	(%rsi), %rax
	movq	%rsi, %r12
	cmpq	3512(%rdi), %rax
	je	.L4812
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$65504, %ecx
	jne	.L4817
	movq	3512(%rdi), %rcx
	movq	-1(%rcx), %rcx
	movzwl	11(%rcx), %ecx
	andl	$65504, %ecx
	je	.L4818
.L4817:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L4818
	movq	3512(%rbx), %rax
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L4818
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	jne	.L4812
.L4818:
	leaq	2880(%rbx), %rdx
	cmpq	%r12, %rdx
	je	.L4825
	movq	(%r12), %rax
	cmpq	%rax, 2880(%rbx)
	je	.L4825
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$65504, %ecx
	je	.L4821
.L4824:
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L4826
	movq	2880(%rbx), %rax
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	je	.L4826
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6String10SlowEqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	testb	%al, %al
	jne	.L4825
.L4826:
	leaq	2696(%rbx), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	addq	$1104, %rbx
	call	_ZN2v88internal4Name6EqualsEPNS0_7IsolateENS0_6HandleIS1_EES5_
	testb	%al, %al
	movl	$0, %eax
	cmovne	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4812:
	.cfi_restore_state
	leaq	88(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4825:
	.cfi_restore_state
	leaq	1088(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4821:
	.cfi_restore_state
	movq	2880(%rbx), %rcx
	movq	-1(%rcx), %rcx
	movzwl	11(%rcx), %ecx
	andl	$65504, %ecx
	jne	.L4824
	jmp	.L4826
	.cfi_endproc
.LFE24259:
	.size	_ZN2v88internal7Factory17GlobalConstantForENS0_6HandleINS0_4NameEEE, .-_ZN2v88internal7Factory17GlobalConstantForENS0_6HandleINS0_4NameEEE
	.section	.text._ZN2v88internal7Factory9ToBooleanEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory9ToBooleanEb
	.type	_ZN2v88internal7Factory9ToBooleanEb, @function
_ZN2v88internal7Factory9ToBooleanEb:
.LFB24260:
	.cfi_startproc
	endbr64
	leaq	112(%rdi), %rax
	addq	$120, %rdi
	testb	%sil, %sil
	cmove	%rdi, %rax
	ret
	.cfi_endproc
.LFE24260:
	.size	_ZN2v88internal7Factory9ToBooleanEb, .-_ZN2v88internal7Factory9ToBooleanEb
	.section	.text._ZN2v88internal7Factory21ToPrimitiveHintStringENS0_15ToPrimitiveHintE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory21ToPrimitiveHintStringENS0_15ToPrimitiveHintE
	.type	_ZN2v88internal7Factory21ToPrimitiveHintStringENS0_15ToPrimitiveHintE, @function
_ZN2v88internal7Factory21ToPrimitiveHintStringENS0_15ToPrimitiveHintE:
.LFB24261:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	je	.L4841
	cmpl	$2, %esi
	je	.L4842
	testl	%esi, %esi
	je	.L4847
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4841:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	leaq	2984(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4847:
	leaq	2328(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4842:
	leaq	3352(%rdi), %rax
	ret
	.cfi_endproc
.LFE24261:
	.size	_ZN2v88internal7Factory21ToPrimitiveHintStringENS0_15ToPrimitiveHintE, .-_ZN2v88internal7Factory21ToPrimitiveHintStringENS0_15ToPrimitiveHintE
	.section	.text._ZN2v88internal7Factory23CreateSloppyFunctionMapENS0_12FunctionModeENS0_11MaybeHandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory23CreateSloppyFunctionMapENS0_12FunctionModeENS0_11MaybeHandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal7Factory23CreateSloppyFunctionMapENS0_12FunctionModeENS0_11MaybeHandleINS0_10JSFunctionEEE, @function
_ZN2v88internal7Factory23CreateSloppyFunctionMapENS0_12FunctionModeENS0_11MaybeHandleINS0_10JSFunctionEEE:
.LFB24262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	%esi, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	%esi, %edi
	movl	$1105, %esi
	subq	$88, %rsp
	movq	%rdx, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$12, %ecx
	setne	%r12b
	cmpl	$1, %ecx
	movl	%ecx, -116(%rbp)
	sbbl	%eax, %eax
	andl	$-8, %eax
	cmpl	$1, %ecx
	movl	$3, %ecx
	sbbl	%r14d, %r14d
	andl	$1, %edi
	andl	$1, %r8d
	movb	%dil, -117(%rbp)
	leal	64(%rax,%r8,8), %edx
	movq	%rbx, %rdi
	addl	$5, %r14d
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movzbl	%r12b, %edx
	movq	(%rax), %rcx
	movq	%rax, %r15
	movl	%edx, %esi
	sall	$6, %edx
	sall	$7, %esi
	movzbl	13(%rcx), %eax
	andl	$127, %eax
	orl	%esi, %eax
	movb	%al, 13(%rcx)
	movq	(%r15), %rax
	movq	-128(%rbp), %r9
	movzbl	13(%rax), %r12d
	andl	$-65, %r12d
	orl	%edx, %r12d
	movb	%r12b, 13(%rax)
	movq	(%r15), %rax
	orb	$2, 13(%rax)
	testq	%r9, %r9
	je	.L4850
	movl	$1, %ecx
	movq	%r9, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
.L4850:
	movl	%r14d, %edx
	movq	%r15, %rsi
	leaq	-96(%rbp), %r12
	movq	%rbx, %rdi
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	leaq	4432(%rbx), %rdx
	movl	$3, %ecx
	movq	%r12, %rdi
	leaq	2768(%rbx), %rsi
	leaq	-104(%rbp), %r14
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r15), %rax
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	cmpb	$0, -117(%rbp)
	leaq	2872(%rbx), %rsi
	je	.L4851
	movq	%rsi, %rdx
	movl	$4, %r9d
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movl	$3, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
.L4869:
	movq	(%r15), %rax
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movl	$7, %ecx
	leaq	4408(%rbx), %rdx
	movq	%r12, %rdi
	leaq	2040(%rbx), %rsi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r15), %rax
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	leaq	4416(%rbx), %rdx
	movl	$7, %ecx
	movq	%r12, %rdi
	leaq	2232(%rbx), %rsi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r15), %rax
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	je	.L4853
	xorl	%ecx, %ecx
	andl	$4, %r13d
	leaq	4440(%rbx), %rdx
	movq	%r12, %rdi
	sete	%cl
	leaq	3104(%rbx), %rsi
	addl	$6, %ecx
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r15), %rax
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
.L4853:
	movq	41016(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L4870
.L4855:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4871
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4851:
	.cfi_restore_state
	leaq	4424(%rbx), %rdx
	movl	$3, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	jmp	.L4869
	.p2align 4,,10
	.p2align 3
.L4870:
	movq	(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10MapDetailsENS0_3MapE@PLT
	jmp	.L4855
.L4871:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24262:
	.size	_ZN2v88internal7Factory23CreateSloppyFunctionMapENS0_12FunctionModeENS0_11MaybeHandleINS0_10JSFunctionEEE, .-_ZN2v88internal7Factory23CreateSloppyFunctionMapENS0_12FunctionModeENS0_11MaybeHandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE:
.LFB24263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movl	%esi, %r8d
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, %eax
	andl	$12, %eax
	setne	%dil
	andl	$2, %ecx
	andl	$1, %r8d
	movl	%eax, -120(%rbp)
	movl	%ecx, -116(%rbp)
	movl	%edi, %r15d
	movl	%r8d, %r9d
	testl	%eax, %eax
	je	.L4900
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	jne	.L4883
.L4899:
	movl	$64, %eax
	movl	$3, %r13d
.L4882:
	leal	(%rax,%r8,8), %edx
	movl	$3, %ecx
	movq	%rbx, %rdi
	addl	%r8d, %r13d
	movl	$1105, %esi
	movb	%r9b, -121(%rbp)
	movq	%r11, -136(%rbp)
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movzbl	%r15b, %ecx
	movq	%rbx, %rdi
	leaq	-96(%rbp), %r15
	movq	%rax, %r14
	movq	(%rax), %rax
	movl	%ecx, %esi
	sall	$6, %ecx
	sall	$7, %esi
	movzbl	13(%rax), %edx
	andl	$127, %edx
	orl	%esi, %edx
	movq	%r14, %rsi
	movb	%dl, 13(%rax)
	movq	(%r14), %rdx
	movq	-136(%rbp), %r11
	movzbl	13(%rdx), %eax
	andl	$-65, %eax
	orl	%ecx, %eax
	movl	$1, %ecx
	movb	%al, 13(%rdx)
	movq	(%r14), %rax
	movq	%r11, %rdx
	orb	$2, 13(%rax)
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	leaq	4432(%rbx), %rdx
	movl	$3, %ecx
	movq	%r15, %rdi
	leaq	2768(%rbx), %rsi
	leaq	-104(%rbp), %r13
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r14), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movzbl	-121(%rbp), %r9d
	leaq	2872(%rbx), %rsi
	testb	%r9b, %r9b
	je	.L4875
	xorl	%ecx, %ecx
	movq	%rsi, %rdx
	movl	$4, %r9d
	movq	%rbx, %rsi
	movl	$3, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r14), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movl	-116(%rbp), %esi
	movl	$1, %ecx
	testl	%esi, %esi
	jne	.L4901
.L4877:
	movl	-120(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L4878
	xorl	%ecx, %ecx
	andl	$4, %r12d
	leaq	4440(%rbx), %rdx
	movq	%r15, %rdi
	sete	%cl
	leaq	3104(%rbx), %rsi
	addl	$6, %ecx
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r14), %rax
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
.L4878:
	movq	41016(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L4902
.L4880:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4903
	addq	$104, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4875:
	.cfi_restore_state
	movl	$3, %ecx
	leaq	4424(%rbx), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r14), %rax
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movl	-116(%rbp), %esi
	xorl	%ecx, %ecx
	testl	%esi, %esi
	je	.L4877
.L4901:
	leaq	3720(%rbx), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	$4, %r9d
	movl	$2, %r8d
	call	_ZN2v88internal10Descriptor9DataFieldEPNS0_7IsolateENS0_6HandleINS0_4NameEEEiNS0_18PropertyAttributesENS0_14RepresentationE@PLT
	movq	(%r14), %rax
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	jmp	.L4877
	.p2align 4,,10
	.p2align 3
.L4900:
	movl	-116(%rbp), %edx
	testl	%edx, %edx
	je	.L4898
	addl	$1, %r8d
.L4898:
	movl	$56, %eax
	movl	$2, %r13d
	jmp	.L4882
	.p2align 4,,10
	.p2align 3
.L4883:
	addl	$1, %r8d
	jmp	.L4899
	.p2align 4,,10
	.p2align 3
.L4902:
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10MapDetailsENS0_3MapE@PLT
	jmp	.L4880
.L4903:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24263:
	.size	_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal7Factory23CreateStrictFunctionMapENS0_12FunctionModeENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal7Factory22CreateClassFunctionMapENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory22CreateClassFunctionMapENS0_6HandleINS0_10JSFunctionEEE
	.type	_ZN2v88internal7Factory22CreateClassFunctionMapENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal7Factory22CreateClassFunctionMapENS0_6HandleINS0_10JSFunctionEEE:
.LFB24264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	$64, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	movl	$1105, %esi
	leaq	-88(%rbp), %r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	(%rax), %rdx
	movq	%rax, %r12
	movq	%r12, %rsi
	movzbl	13(%rdx), %eax
	orl	$-128, %eax
	movb	%al, 13(%rdx)
	movq	(%r12), %rax
	orb	$64, 13(%rax)
	movq	(%r12), %rdx
	movl	15(%rdx), %eax
	orl	$1048576, %eax
	movl	%eax, 15(%rdx)
	movq	%r13, %rdx
	leaq	-80(%rbp), %r13
	movq	(%r12), %rax
	orb	$2, 13(%rax)
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal3Map21EnsureDescriptorSlackEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	movl	$3, %ecx
	movq	%r13, %rdi
	leaq	4432(%rbx), %rdx
	leaq	2768(%rbx), %rsi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	%r13, %rdi
	movl	$7, %ecx
	leaq	4440(%rbx), %rdx
	leaq	3104(%rbx), %rsi
	call	_ZN2v88internal10Descriptor16AccessorConstantENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%r12), %rax
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal3Map16AppendDescriptorEPNS0_7IsolateEPNS0_10DescriptorE
	movq	41016(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L4911
.L4905:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4912
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4911:
	.cfi_restore_state
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6Logger10MapDetailsENS0_3MapE@PLT
	jmp	.L4905
.L4912:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24264:
	.size	_ZN2v88internal7Factory22CreateClassFunctionMapENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal7Factory22CreateClassFunctionMapENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZN2v88internal7Factory23NewJSPromiseWithoutHookENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory23NewJSPromiseWithoutHookENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory23NewJSPromiseWithoutHookENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory23NewJSPromiseWithoutHookENS0_14AllocationTypeE:
.LFB24265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1711(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4914
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L4915:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSFunction19EnsureHasInitialMapENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	55(%rax), %rsi
	testq	%rdi, %rdi
	je	.L4917
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L4918:
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	call	_ZN2v88internal7Factory29AllocateRawWithAllocationSiteENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L4920
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L4921:
	leaq	288(%r12), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal7Factory25InitializeJSObjectFromMapENS0_6HandleINS0_8JSObjectEEENS2_INS0_6ObjectEEENS2_INS0_3MapEEE
	movq	0(%r13), %r14
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r12
	movq	%r12, 23(%r14)
	leaq	23(%r14), %r15
	testb	$1, %r12b
	je	.L4930
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L4947
	testb	$24, %al
	je	.L4930
.L4952:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4948
	.p2align 4,,10
	.p2align 3
.L4930:
	movq	0(%r13), %rax
	movl	$24, %esi
	movq	$0, 31(%rax)
	movq	0(%r13), %r15
	movq	_ZN2v88internal3Smi5kZeroE(%rip), %r14
	movq	-1(%r15), %rax
	leaq	-1(%r15), %r12
	movzwl	11(%rax), %edi
	cmpw	$1057, %di
	je	.L4926
	movsbl	13(%rax), %esi
	shrl	$31, %esi
	call	_ZN2v88internal8JSObject13GetHeaderSizeENS0_12InstanceTypeEb@PLT
	movslq	%eax, %rsi
.L4926:
	addq	%rsi, %r12
	movq	%r14, (%r12)
	testb	$1, %r14b
	je	.L4931
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L4949
	testb	$24, %al
	je	.L4931
.L4951:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4950
.L4931:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4949:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L4951
	jmp	.L4931
	.p2align 4,,10
	.p2align 3
.L4947:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L4952
	jmp	.L4930
	.p2align 4,,10
	.p2align 3
.L4920:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L4953
.L4922:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L4921
	.p2align 4,,10
	.p2align 3
.L4917:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L4954
.L4919:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L4918
	.p2align 4,,10
	.p2align 3
.L4914:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L4955
.L4916:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L4915
	.p2align 4,,10
	.p2align 3
.L4948:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4930
	.p2align 4,,10
	.p2align 3
.L4950:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4931
	.p2align 4,,10
	.p2align 3
.L4955:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L4916
	.p2align 4,,10
	.p2align 3
.L4954:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L4919
	.p2align 4,,10
	.p2align 3
.L4953:
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L4922
	.cfi_endproc
.LFE24265:
	.size	_ZN2v88internal7Factory23NewJSPromiseWithoutHookENS0_14AllocationTypeE, .-_ZN2v88internal7Factory23NewJSPromiseWithoutHookENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory12NewJSPromiseENS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory12NewJSPromiseENS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory12NewJSPromiseENS0_14AllocationTypeE, @function
_ZN2v88internal7Factory12NewJSPromiseENS0_14AllocationTypeE:
.LFB24266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal7Factory23NewJSPromiseWithoutHookENS0_14AllocationTypeE
	leaq	88(%r12), %rcx
	movq	%r12, %rdi
	xorl	%esi, %esi
	movq	%rax, %r13
	movq	%rax, %rdx
	call	_ZN2v88internal7Isolate14RunPromiseHookENS_15PromiseHookTypeENS0_6HandleINS0_9JSPromiseEEENS3_INS0_6ObjectEEE@PLT
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24266:
	.size	_ZN2v88internal7Factory12NewJSPromiseENS0_14AllocationTypeE, .-_ZN2v88internal7Factory12NewJSPromiseENS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory18NewCallHandlerInfoEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18NewCallHandlerInfoEb
	.type	_ZN2v88internal7Factory18NewCallHandlerInfoEb, @function
_ZN2v88internal7Factory18NewCallHandlerInfoEb:
.LFB24267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	leaq	600(%rdi), %rax
	leaq	592(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testb	%r8b, %r8b
	cmovne	%rax, %rsi
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%rbx), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L4961
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	movq	%rax, %r13
.L4962:
	movq	88(%rbx), %r14
	leaq	7(%r12), %r15
	movq	%r14, 7(%r12)
	testb	$1, %r14b
	je	.L4964
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rdx
	testl	$262144, %edx
	jne	.L4992
	andl	$24, %edx
	je	.L4966
.L4998:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4993
.L4966:
	movq	0(%r13), %r12
	movq	%r14, 15(%r12)
	leaq	15(%r12), %r15
	movq	8(%rbx), %rdx
	testl	$262144, %edx
	jne	.L4994
	andl	$24, %edx
	je	.L4968
.L5000:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4995
.L4968:
	movq	0(%r13), %r12
	movq	%r14, 23(%r12)
	leaq	23(%r12), %r15
	movq	8(%rbx), %rdx
	testl	$262144, %edx
	jne	.L4996
	andl	$24, %edx
	je	.L4978
.L4999:
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4997
.L4978:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4992:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rdx
	andl	$24, %edx
	jne	.L4998
	jmp	.L4966
	.p2align 4,,10
	.p2align 3
.L4996:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rdx
	andl	$24, %edx
	jne	.L4999
	jmp	.L4978
	.p2align 4,,10
	.p2align 3
.L4994:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rdx
	andl	$24, %edx
	jne	.L5000
	jmp	.L4968
	.p2align 4,,10
	.p2align 3
.L4961:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L5001
.L4963:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%r12, 0(%r13)
	jmp	.L4962
	.p2align 4,,10
	.p2align 3
.L4964:
	movq	0(%r13), %rax
	movq	%r14, 15(%rax)
	movq	0(%r13), %rax
	movq	%r14, 23(%rax)
	jmp	.L4978
	.p2align 4,,10
	.p2align 3
.L4993:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4966
	.p2align 4,,10
	.p2align 3
.L4995:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4968
	.p2align 4,,10
	.p2align 3
.L4997:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4978
	.p2align 4,,10
	.p2align 3
.L5001:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L4963
	.cfi_endproc
.LFE24267:
	.size	_ZN2v88internal7Factory18NewCallHandlerInfoEb, .-_ZN2v88internal7Factory18NewCallHandlerInfoEb
	.section	.text._ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_24WasmExportedFunctionDataEEENS2_INS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_24WasmExportedFunctionDataEEENS2_INS0_3MapEEE
	.type	_ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_24WasmExportedFunctionDataEEENS2_INS0_3MapEEE, @function
_ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_24WasmExportedFunctionDataEEENS2_INS0_3MapEEE:
.LFB24268:
	.cfi_startproc
	endbr64
	movq	%rdx, %xmm1
	movq	%rcx, %xmm0
	xorl	%edx, %edx
	movb	$0, 24(%rdi)
	movw	%dx, 48(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movl	$4294967295, %edx
	movq	%rdi, %rax
	movq	$-1, 28(%rdi)
	movb	$0, 36(%rdi)
	movq	%rsi, (%rdi)
	movq	$0, 40(%rdi)
	movq	%rdx, 52(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE24268:
	.size	_ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_24WasmExportedFunctionDataEEENS2_INS0_3MapEEE, .-_ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_24WasmExportedFunctionDataEEENS2_INS0_3MapEEE
	.section	.text._ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_18WasmJSFunctionDataEEENS2_INS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_18WasmJSFunctionDataEEENS2_INS0_3MapEEE
	.type	_ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_18WasmJSFunctionDataEEENS2_INS0_3MapEEE, @function
_ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_18WasmJSFunctionDataEEENS2_INS0_3MapEEE:
.LFB24275:
	.cfi_startproc
	endbr64
	movq	%rdx, %xmm1
	movq	%rcx, %xmm0
	xorl	%edx, %edx
	movb	$0, 24(%rdi)
	movw	%dx, 48(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movl	$4294967295, %edx
	movq	%rdi, %rax
	movq	$-1, 28(%rdi)
	movb	$0, 36(%rdi)
	movq	%rsi, (%rdi)
	movq	$0, 40(%rdi)
	movq	%rdx, 52(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE24275:
	.size	_ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_18WasmJSFunctionDataEEENS2_INS0_3MapEEE, .-_ZN2v88internal15NewFunctionArgs7ForWasmENS0_6HandleINS0_6StringEEENS2_INS0_18WasmJSFunctionDataEEENS2_INS0_3MapEEE
	.section	.text._ZN2v88internal15NewFunctionArgs10ForBuiltinENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15NewFunctionArgs10ForBuiltinENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEEi
	.type	_ZN2v88internal15NewFunctionArgs10ForBuiltinENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEEi, @function
_ZN2v88internal15NewFunctionArgs10ForBuiltinENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEEi:
.LFB24276:
	.cfi_startproc
	endbr64
	movq	%rdx, 8(%rdi)
	movl	$257, %edx
	movq	%rdi, %rax
	movq	$0, 16(%rdi)
	movb	$0, 24(%rdi)
	movq	$-1, 28(%rdi)
	movb	$0, 36(%rdi)
	movq	%rsi, (%rdi)
	movl	%ecx, 52(%rdi)
	movl	$0, 56(%rdi)
	movq	$0, 40(%rdi)
	movw	%dx, 48(%rdi)
	ret
	.cfi_endproc
.LFE24276:
	.size	_ZN2v88internal15NewFunctionArgs10ForBuiltinENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEEi, .-_ZN2v88internal15NewFunctionArgs10ForBuiltinENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEEi
	.section	.text._ZN2v88internal15NewFunctionArgs22ForFunctionWithoutCodeENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEENS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15NewFunctionArgs22ForFunctionWithoutCodeENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEENS0_12LanguageModeE
	.type	_ZN2v88internal15NewFunctionArgs22ForFunctionWithoutCodeENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEENS0_12LanguageModeE, @function
_ZN2v88internal15NewFunctionArgs22ForFunctionWithoutCodeENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEENS0_12LanguageModeE:
.LFB24277:
	.cfi_startproc
	endbr64
	movq	$0, 16(%rdi)
	movq	%rdi, %rax
	movb	$0, 24(%rdi)
	movq	$-1, 28(%rdi)
	movb	$0, 36(%rdi)
	movq	$0, 40(%rdi)
	movq	%rsi, (%rdi)
	movq	%rdx, 8(%rdi)
	movb	%cl, 49(%rdi)
	movq	$166, 52(%rdi)
	movb	$1, 48(%rdi)
	ret
	.cfi_endproc
.LFE24277:
	.size	_ZN2v88internal15NewFunctionArgs22ForFunctionWithoutCodeENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEENS0_12LanguageModeE, .-_ZN2v88internal15NewFunctionArgs22ForFunctionWithoutCodeENS0_6HandleINS0_6StringEEENS2_INS0_3MapEEENS0_12LanguageModeE
	.section	.text._ZN2v88internal15NewFunctionArgs23ForBuiltinWithPrototypeENS0_6HandleINS0_6StringEEENS2_INS0_10HeapObjectEEENS0_12InstanceTypeEiiiNS0_11MutableModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15NewFunctionArgs23ForBuiltinWithPrototypeENS0_6HandleINS0_6StringEEENS2_INS0_10HeapObjectEEENS0_12InstanceTypeEiiiNS0_11MutableModeE
	.type	_ZN2v88internal15NewFunctionArgs23ForBuiltinWithPrototypeENS0_6HandleINS0_6StringEEENS2_INS0_10HeapObjectEEENS0_12InstanceTypeEiiiNS0_11MutableModeE, @function
_ZN2v88internal15NewFunctionArgs23ForBuiltinWithPrototypeENS0_6HandleINS0_6StringEEENS2_INS0_10HeapObjectEEENS0_12InstanceTypeEiiiNS0_11MutableModeE:
.LFB24278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rdi, %rax
	movq	%rdx, 40(%rdi)
	movq	%rsi, (%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movw	%cx, 26(%rdi)
	movl	16(%rbp), %edx
	movl	%r8d, 28(%rdi)
	movl	%r9d, 32(%rdi)
	movl	%edx, 52(%rdi)
	movl	24(%rbp), %edx
	movb	$1, 24(%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%edx, 56(%rdi)
	movl	$257, %edx
	movb	$1, 36(%rdi)
	movw	%dx, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE24278:
	.size	_ZN2v88internal15NewFunctionArgs23ForBuiltinWithPrototypeENS0_6HandleINS0_6StringEEENS2_INS0_10HeapObjectEEENS0_12InstanceTypeEiiiNS0_11MutableModeE, .-_ZN2v88internal15NewFunctionArgs23ForBuiltinWithPrototypeENS0_6HandleINS0_6StringEEENS2_INS0_10HeapObjectEEENS0_12InstanceTypeEiiiNS0_11MutableModeE
	.section	.text._ZN2v88internal15NewFunctionArgs26ForBuiltinWithoutPrototypeENS0_6HandleINS0_6StringEEEiNS0_12LanguageModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15NewFunctionArgs26ForBuiltinWithoutPrototypeENS0_6HandleINS0_6StringEEEiNS0_12LanguageModeE
	.type	_ZN2v88internal15NewFunctionArgs26ForBuiltinWithoutPrototypeENS0_6HandleINS0_6StringEEEiNS0_12LanguageModeE, @function
_ZN2v88internal15NewFunctionArgs26ForBuiltinWithoutPrototypeENS0_6HandleINS0_6StringEEEiNS0_12LanguageModeE:
.LFB24279:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movb	$0, 24(%rdi)
	movq	%rdi, %rax
	movq	$-1, 28(%rdi)
	movb	$0, 36(%rdi)
	movq	$0, 40(%rdi)
	movq	%rsi, (%rdi)
	movl	%edx, 52(%rdi)
	movb	%cl, 49(%rdi)
	movl	$0, 56(%rdi)
	movb	$1, 48(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.cfi_endproc
.LFE24279:
	.size	_ZN2v88internal15NewFunctionArgs26ForBuiltinWithoutPrototypeENS0_6HandleINS0_6StringEEEiNS0_12LanguageModeE, .-_ZN2v88internal15NewFunctionArgs26ForBuiltinWithoutPrototypeENS0_6HandleINS0_6StringEEEiNS0_12LanguageModeE
	.section	.text._ZN2v88internal15NewFunctionArgs31SetShouldCreateAndSetInitialMapEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15NewFunctionArgs31SetShouldCreateAndSetInitialMapEv
	.type	_ZN2v88internal15NewFunctionArgs31SetShouldCreateAndSetInitialMapEv, @function
_ZN2v88internal15NewFunctionArgs31SetShouldCreateAndSetInitialMapEv:
.LFB24280:
	.cfi_startproc
	endbr64
	movb	$1, 24(%rdi)
	ret
	.cfi_endproc
.LFE24280:
	.size	_ZN2v88internal15NewFunctionArgs31SetShouldCreateAndSetInitialMapEv, .-_ZN2v88internal15NewFunctionArgs31SetShouldCreateAndSetInitialMapEv
	.section	.text._ZN2v88internal15NewFunctionArgs21SetShouldSetPrototypeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15NewFunctionArgs21SetShouldSetPrototypeEv
	.type	_ZN2v88internal15NewFunctionArgs21SetShouldSetPrototypeEv, @function
_ZN2v88internal15NewFunctionArgs21SetShouldSetPrototypeEv:
.LFB24281:
	.cfi_startproc
	endbr64
	movb	$1, 36(%rdi)
	ret
	.cfi_endproc
.LFE24281:
	.size	_ZN2v88internal15NewFunctionArgs21SetShouldSetPrototypeEv, .-_ZN2v88internal15NewFunctionArgs21SetShouldSetPrototypeEv
	.section	.text._ZN2v88internal15NewFunctionArgs24SetShouldSetLanguageModeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15NewFunctionArgs24SetShouldSetLanguageModeEv
	.type	_ZN2v88internal15NewFunctionArgs24SetShouldSetLanguageModeEv, @function
_ZN2v88internal15NewFunctionArgs24SetShouldSetLanguageModeEv:
.LFB24282:
	.cfi_startproc
	endbr64
	movb	$1, 48(%rdi)
	ret
	.cfi_endproc
.LFE24282:
	.size	_ZN2v88internal15NewFunctionArgs24SetShouldSetLanguageModeEv, .-_ZN2v88internal15NewFunctionArgs24SetShouldSetLanguageModeEv
	.section	.text._ZNK2v88internal15NewFunctionArgs6GetMapEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15NewFunctionArgs6GetMapEPNS0_7IsolateE
	.type	_ZNK2v88internal15NewFunctionArgs6GetMapEPNS0_7IsolateE, @function
_ZNK2v88internal15NewFunctionArgs6GetMapEPNS0_7IsolateE:
.LFB24283:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L5053
	ret
	.p2align 4,,10
	.p2align 3
.L5053:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	cmpq	$0, 40(%rdi)
	je	.L5054
	movl	56(%rdi), %eax
	testl	%eax, %eax
	je	.L5023
	cmpl	$1, %eax
	jne	.L5055
	movq	12464(%rsi), %rax
	cmpb	$0, 49(%rdi)
	movq	39(%rax), %rax
	je	.L5034
	movq	1295(%rax), %r12
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L5038
.L5046:
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5023:
	.cfi_restore_state
	movq	12464(%rsi), %rax
	cmpb	$0, 49(%rdi)
	movq	39(%rax), %rax
	je	.L5026
	movq	1279(%rax), %r12
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.L5046
	.p2align 4,,10
	.p2align 3
.L5038:
	movq	41088(%rsi), %rax
	cmpq	41096(%rsi), %rax
	je	.L5056
.L5040:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rsi)
	movq	%r12, (%rax)
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5034:
	.cfi_restore_state
	movq	1271(%rax), %r12
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.L5046
	jmp	.L5038
	.p2align 4,,10
	.p2align 3
.L5054:
	movq	12464(%rsi), %rax
	cmpb	$0, 49(%rdi)
	movq	39(%rax), %rax
	je	.L5016
	movq	1303(%rax), %r12
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.L5046
	jmp	.L5038
	.p2align 4,,10
	.p2align 3
.L5026:
	movq	1247(%rax), %r12
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.L5046
	jmp	.L5038
	.p2align 4,,10
	.p2align 3
.L5016:
	movq	1263(%rax), %r12
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.L5046
	jmp	.L5038
	.p2align 4,,10
	.p2align 3
.L5056:
	movq	%rsi, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L5040
.L5055:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24283:
	.size	_ZNK2v88internal15NewFunctionArgs6GetMapEPNS0_7IsolateE, .-_ZNK2v88internal15NewFunctionArgs6GetMapEPNS0_7IsolateE
	.section	.text._ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE
	.type	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE, @function
_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE:
.LFB24173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5058
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L5059:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal15NewFunctionArgs6GetMapEPNS0_7IsolateE
	movl	52(%rbx), %ecx
	movq	16(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal7Factory21NewSharedFunctionInfoENS0_11MaybeHandleINS0_6StringEEENS2_INS0_10HeapObjectEEEiNS0_12FunctionKindE
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$1, %r8d
	call	_ZN2v88internal7Factory11NewFunctionENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE
	cmpb	$0, 36(%rbx)
	movq	%rax, %r13
	je	.L5061
	movq	(%rax), %r14
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L5073
	movq	(%rax), %r15
	leaq	55(%r14), %rsi
	movq	%r14, %rdi
	movq	%r15, 55(%r14)
	movq	%r15, %rdx
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	je	.L5061
	movq	-72(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L5061:
	cmpb	$0, 48(%rbx)
	jne	.L5089
.L5064:
	cmpb	$0, 24(%rbx)
	je	.L5071
	movzwl	26(%rbx), %esi
	movl	$2, %ecx
	cmpw	$1058, %si
	je	.L5072
	xorl	%ecx, %ecx
	cmpw	$1061, %si
	setne	%cl
	leal	(%rcx,%rcx,2), %ecx
.L5072:
	movl	28(%rbx), %edx
	movl	32(%rbx), %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory6NewMapENS0_12InstanceTypeEiNS0_12ElementsKindEi
	movq	%rax, %r14
	movq	0(%r13), %rax
	movq	23(%rax), %rax
	movl	32(%rbx), %edx
	movw	%dx, 43(%rax)
	movq	40(%rbx), %r8
	testq	%r8, %r8
	je	.L5073
	movq	0(%r13), %rax
	movq	23(%rax), %rax
	movl	47(%rax), %edx
	andl	$31, %edx
	leal	-9(%rdx), %eax
	cmpb	$6, %al
	jbe	.L5074
	cmpb	$1, %dl
	jne	.L5090
.L5074:
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal10JSFunction13SetInitialMapENS0_6HandleIS1_EENS2_INS0_3MapEEENS2_INS0_10HeapObjectEEE@PLT
.L5071:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5091
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5058:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L5092
.L5060:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L5059
	.p2align 4,,10
	.p2align 3
.L5089:
	movq	0(%r13), %rax
	movq	23(%rax), %r14
	movl	47(%r14), %eax
	movzbl	49(%rbx), %edx
	andl	$-65, %eax
	sall	$6, %edx
	orl	%edx, %eax
	movl	%eax, 47(%r14)
	movl	47(%r14), %eax
	shrl	$13, %eax
	andl	$1, %eax
	movl	%eax, %r15d
	movq	15(%r14), %rax
	testb	$1, %al
	jne	.L5093
.L5065:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L5066:
	movl	47(%r14), %edx
	movl	$753664, %ecx
	movl	47(%r14), %edi
	andl	$31, %edx
	leal	-2(%rdx), %esi
	cmpb	$3, %sil
	jbe	.L5067
	leal	-12(%rdx), %ecx
	leal	-9(%rdx), %esi
	cmpb	$3, %cl
	ja	.L5068
	cmpb	$5, %sil
	sbbl	%ecx, %ecx
	andl	$4, %ecx
	addl	$169, %ecx
.L5069:
	xorl	$1, %eax
	addl	%r15d, %r15d
	movzbl	%al, %eax
	orl	%r15d, %eax
	leal	-154(%rcx,%rax), %eax
	sall	$15, %eax
	movl	%eax, %ecx
.L5067:
	movl	47(%r14), %eax
	andl	$-1015809, %eax
	orl	%ecx, %eax
	movl	%eax, 47(%r14)
	jmp	.L5064
	.p2align 4,,10
	.p2align 3
.L5090:
	movq	(%r8), %rax
	cmpq	%rax, 96(%r12)
	jne	.L5074
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20NewFunctionPrototypeENS0_6HandleINS0_10JSFunctionEEE
	movq	%rax, %r8
	jmp	.L5074
	.p2align 4,,10
	.p2align 3
.L5068:
	movl	$165, %ecx
	cmpb	$4, %sil
	jbe	.L5069
	cmpb	$17, %dl
	ja	.L5070
	movl	$236480, %esi
	movl	$161, %ecx
	btq	%rdx, %rsi
	jc	.L5069
	.p2align 4,,10
	.p2align 3
.L5070:
	andl	$64, %edi
	cmpl	$1, %edi
	sbbl	%ecx, %ecx
	andl	$-4, %ecx
	addl	$158, %ecx
	jmp	.L5069
	.p2align 4,,10
	.p2align 3
.L5073:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5093:
	movq	-1(%rax), %rdx
	cmpw	$136, 11(%rdx)
	jne	.L5065
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	jmp	.L5066
	.p2align 4,,10
	.p2align 3
.L5092:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L5060
.L5091:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24173:
	.size	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE, .-_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE
	.section	.text._ZN2v88internal7Factory18NewFunctionForTestENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18NewFunctionForTestENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal7Factory18NewFunctionForTestENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal7Factory18NewFunctionForTestENS0_6HandleINS0_6StringEEE:
.LFB24172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1247(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5095
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L5096:
	movq	%rax, -88(%rbp)
	leaq	-96(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1, %eax
	movq	$0, -80(%rbp)
	movb	$0, -72(%rbp)
	movq	$-1, -68(%rbp)
	movb	$0, -60(%rbp)
	movq	%rbx, -96(%rbp)
	movq	$166, -44(%rbp)
	movq	$0, -56(%rbp)
	movw	%ax, -48(%rbp)
	call	_ZN2v88internal7Factory11NewFunctionERKNS0_15NewFunctionArgsE
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5100
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5095:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L5101
.L5097:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L5096
	.p2align 4,,10
	.p2align 3
.L5101:
	movq	%r12, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	jmp	.L5097
.L5100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24172:
	.size	_ZN2v88internal7Factory18NewFunctionForTestENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal7Factory18NewFunctionForTestENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal7Factory16CopyArrayWithMapINS0_13PropertyArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE,"axG",@progbits,_ZN2v88internal7Factory16CopyArrayWithMapINS0_13PropertyArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory16CopyArrayWithMapINS0_13PropertyArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE
	.type	_ZN2v88internal7Factory16CopyArrayWithMapINS0_13PropertyArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE, @function
_ZN2v88internal7Factory16CopyArrayWithMapINS0_13PropertyArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE:
.LFB27433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	37592(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movslq	11(%rax), %rbx
	andl	$1023, %ebx
	leal	16(,%rbx,8), %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	%rax, %rsi
	movq	0(%r13), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5103
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L5104:
	movq	%rbx, %rax
	movl	$4, %r9d
	salq	$32, %rax
	movq	%rax, 7(%rsi)
	movq	0(%r13), %rsi
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L5106
	xorl	%r9d, %r9d
	testb	$24, %al
	sete	%r9b
	sall	$2, %r9d
.L5106:
	testl	%ebx, %ebx
	je	.L5108
	movq	(%r14), %rcx
	leaq	15(%rsi), %rdx
	movl	%ebx, %r8d
	movq	%r15, %rdi
	addq	$15, %rcx
	call	_ZN2v88internal4Heap9CopyRangeINS0_14FullObjectSlotEEEvNS0_10HeapObjectET_S5_iNS0_16WriteBarrierModeE@PLT
.L5108:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5103:
	.cfi_restore_state
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L5112
.L5105:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L5104
	.p2align 4,,10
	.p2align 3
.L5112:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L5105
	.cfi_endproc
.LFE27433:
	.size	_ZN2v88internal7Factory16CopyArrayWithMapINS0_13PropertyArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE, .-_ZN2v88internal7Factory16CopyArrayWithMapINS0_13PropertyArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE
	.section	.text._ZN2v88internal7Factory16CopyArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE,"axG",@progbits,_ZN2v88internal7Factory16CopyArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory16CopyArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE
	.type	_ZN2v88internal7Factory16CopyArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE, @function
_ZN2v88internal7Factory16CopyArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE:
.LFB27435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	37592(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movslq	11(%rax), %r13
	cmpl	$134217726, %r13d
	ja	.L5130
.L5114:
	leal	16(,%r13,8), %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%r15, %rdi
	movl	%r9d, %esi
	movl	$1, %ecx
	movl	%r9d, -56(%rbp)
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movl	-56(%rbp), %r9d
	movq	%rax, %rsi
	cmpl	$131072, %r9d
	jle	.L5118
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L5131
.L5118:
	movq	(%r12), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5132
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L5120:
	movq	%r13, %rax
	movl	$4, %r9d
	salq	$32, %rax
	movq	%rax, 7(%rsi)
	movq	(%r12), %rsi
	movq	%rsi, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rax
	testl	$262144, %eax
	jne	.L5122
	xorl	%r9d, %r9d
	testb	$24, %al
	sete	%r9b
	sall	$2, %r9d
.L5122:
	testq	%r13, %r13
	je	.L5124
	movq	(%r14), %rcx
	leaq	15(%rsi), %rdx
	movl	%r13d, %r8d
	movq	%r15, %rdi
	addq	$15, %rcx
	call	_ZN2v88internal4Heap9CopyRangeINS0_14FullObjectSlotEEEvNS0_10HeapObjectET_S5_iNS0_16WriteBarrierModeE@PLT
.L5124:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5132:
	.cfi_restore_state
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L5133
.L5121:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L5120
	.p2align 4,,10
	.p2align 3
.L5130:
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	jmp	.L5114
	.p2align 4,,10
	.p2align 3
.L5131:
	movq	%rax, %rcx
	andq	$-262144, %rcx
	addq	$8, %rcx
	jmp	.L5119
	.p2align 4,,10
	.p2align 3
.L5134:
	movq	%rdx, %rdi
	movq	%rdx, %rax
	orq	$256, %rdi
	lock cmpxchgq	%rdi, (%rcx)
	cmpq	%rax, %rdx
	je	.L5118
.L5119:
	movq	(%rcx), %rdx
	testb	$1, %dh
	je	.L5134
	jmp	.L5118
	.p2align 4,,10
	.p2align 3
.L5133:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L5121
	.cfi_endproc
.LFE27435:
	.size	_ZN2v88internal7Factory16CopyArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE, .-_ZN2v88internal7Factory16CopyArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE
	.section	.text._ZN2v88internal7Factory14CopyFixedArrayENS0_6HandleINS0_10FixedArrayEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory14CopyFixedArrayENS0_6HandleINS0_10FixedArrayEEE
	.type	_ZN2v88internal7Factory14CopyFixedArrayENS0_6HandleINS0_10FixedArrayEEE, @function
_ZN2v88internal7Factory14CopyFixedArrayENS0_6HandleINS0_10FixedArrayEEE:
.LFB24150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	(%rsi), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	jne	.L5143
	addq	$16, %rsp
	movq	%rsi, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5143:
	.cfi_restore_state
	movq	-1(%rax), %rsi
	movq	%rdi, %r13
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5138
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L5139:
	addq	$16, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Factory16CopyArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE
	.p2align 4,,10
	.p2align 3
.L5138:
	.cfi_restore_state
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L5144
.L5140:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L5139
	.p2align 4,,10
	.p2align 3
.L5144:
	movq	%r13, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L5140
	.cfi_endproc
.LFE24150:
	.size	_ZN2v88internal7Factory14CopyFixedArrayENS0_6HandleINS0_10FixedArrayEEE, .-_ZN2v88internal7Factory14CopyFixedArrayENS0_6HandleINS0_10FixedArrayEEE
	.section	.rodata._ZN2v88internal7Factory30CopyJSObjectWithAllocationSiteENS0_6HandleINS0_8JSObjectEEENS2_INS0_14AllocationSiteEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC41:
	.ascii	"map->instance_type() == JS_REGEXP_TYPE || map->instance_type"
	.ascii	"() == JS_OBJECT_TYPE || map->instance_type() == JS_ERROR_TYP"
	.ascii	"E || map->instance_type() == JS_ARRAY_TYPE || map->instance_"
	.ascii	"type() == JS_API_OBJECT_TYPE || map->instance_t"
	.string	"ype() == WASM_GLOBAL_TYPE || map->instance_type() == WASM_INSTANCE_TYPE || map->instance_type() == WASM_MEMORY_TYPE || map->instance_type() == WASM_MODULE_TYPE || map->instance_type() == WASM_TABLE_TYPE || map->instance_type() == JS_SPECIAL_API_OBJECT_TYPE"
	.section	.text._ZN2v88internal7Factory30CopyJSObjectWithAllocationSiteENS0_6HandleINS0_8JSObjectEEENS2_INS0_14AllocationSiteEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory30CopyJSObjectWithAllocationSiteENS0_6HandleINS0_8JSObjectEEENS2_INS0_14AllocationSiteEEE
	.type	_ZN2v88internal7Factory30CopyJSObjectWithAllocationSiteENS0_6HandleINS0_8JSObjectEEENS2_INS0_14AllocationSiteEEE, @function
_ZN2v88internal7Factory30CopyJSObjectWithAllocationSiteENS0_6HandleINS0_8JSObjectEEENS2_INS0_14AllocationSiteEEE:
.LFB24136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5146
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L5147:
	movzwl	11(%rsi), %eax
	subw	$1040, %ax
	cmpw	$63, %ax
	jbe	.L5221
.L5149:
	leaq	.LC41(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5221:
	movabsq	$-576460717807173631, %rdx
	btq	%rax, %rdx
	jnc	.L5149
	movzbl	7(%rsi), %ebx
	leaq	37592(%r12), %rdi
	movl	$1, %ecx
	sall	$3, %ebx
	testq	%r14, %r14
	leal	16(%rbx), %esi
	cmove	%ebx, %esi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	0(%r13), %r9
	movq	%rax, %rdx
	movl	%ebx, %eax
	leaq	-1(%r9), %rsi
	sarl	$3, %eax
	leaq	-1(%rdx), %r8
	movslq	%eax, %rcx
	movq	%rsi, %rdi
	cmpl	$15, %eax
	jg	.L5222
	leaq	14(%r9), %rax
	subq	%r8, %rax
	cmpq	$30, %rax
	leaq	-1(%rcx), %rax
	jbe	.L5153
	cmpq	$3, %rax
	jbe	.L5153
	leaq	-2(%rcx), %rax
	movq	%rdx, %r11
	xorl	%r10d, %r10d
	shrq	%rax
	subq	%r9, %r11
	addq	$1, %rax
	movq	%r11, %r9
	.p2align 4,,10
	.p2align 3
.L5155:
	movdqu	(%rsi), %xmm0
	addq	$1, %r10
	movups	%xmm0, (%r9,%rsi)
	addq	$16, %rsi
	cmpq	%rax, %r10
	jb	.L5155
	leaq	(%rax,%rax), %rsi
	salq	$4, %rax
	addq	%rax, %r8
	addq	%rdi, %rax
	cmpq	%rsi, %rcx
	je	.L5156
	movq	(%rax), %rax
	movq	%rax, (%r8)
.L5156:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5159
	movq	%rdx, %rsi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r15
.L5160:
	testq	%r14, %r14
	je	.L5162
	movq	(%r14), %rax
	movq	3992(%r12), %rcx
	movslq	%ebx, %rbx
	addq	%rbx, %rdx
	movq	%rcx, -1(%rdx)
	movq	%rax, 7(%rdx)
	cmpb	$0, _ZN2v88internal32FLAG_allocation_site_pretenuringE(%rip)
	jne	.L5223
.L5162:
	movq	0(%r13), %rax
	movq	15(%rax), %r14
	movl	11(%r14), %edx
	testl	%edx, %edx
	jg	.L5224
.L5164:
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	movq	7(%rax), %r13
	andl	$2097152, %edx
	jne	.L5174
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %r13b
	jne	.L5175
.L5177:
	movq	968(%rax), %r13
.L5176:
	testw	$1023, 11(%r13)
	je	.L5178
	movq	-1(%r13), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5179
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L5180:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5182
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5183:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory16CopyArrayWithMapINS0_13PropertyArrayEEENS0_6HandleIT_EES6_NS4_INS0_3MapEEE
	movq	(%r15), %r13
	movq	(%rax), %r12
	leaq	7(%r13), %r14
	movq	%r12, 7(%r13)
	testb	$1, %r12b
	je	.L5178
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L5186
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L5186:
	testb	$24, %al
	je	.L5178
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L5178
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L5178
	.p2align 4,,10
	.p2align 3
.L5174:
	testb	$1, %r13b
	jne	.L5189
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-36536(%rax), %r13
.L5189:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5190
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5191:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14CopyFixedArrayENS0_6HandleINS0_10FixedArrayEEE
	movq	(%r15), %r12
	movq	(%rax), %r13
	leaq	7(%r12), %r14
	movq	%r12, %rdi
	movq	%r13, 7(%r12)
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r13b
	je	.L5178
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L5178:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5223:
	.cfi_restore_state
	addl	$1, 35(%rax)
	movq	0(%r13), %rax
	movq	15(%rax), %r14
	movl	11(%r14), %edx
	testl	%edx, %edx
	jle	.L5164
.L5224:
	movq	-1(%r14), %rdx
	cmpq	%rdx, 160(%r12)
	je	.L5165
	movq	-1(%rax), %rax
	movq	41112(%r12), %rdi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$4, %eax
	cmpb	$1, %al
	ja	.L5166
	testq	%rdi, %rdi
	je	.L5167
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5168:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory20CopyFixedDoubleArrayENS0_6HandleINS0_16FixedDoubleArrayEEE
	movq	(%rax), %r14
.L5165:
	movq	(%r15), %rbx
	movq	%r14, %rdx
	movq	%r14, 15(%rbx)
	leaq	15(%rbx), %rsi
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r14b
	je	.L5173
	movq	-56(%rbp), %rsi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L5173:
	movq	0(%r13), %rax
	jmp	.L5164
	.p2align 4,,10
	.p2align 3
.L5175:
	cmpq	288(%rax), %r13
	jne	.L5176
	jmp	.L5177
	.p2align 4,,10
	.p2align 3
.L5190:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L5225
.L5192:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L5191
	.p2align 4,,10
	.p2align 3
.L5146:
	movq	41088(%r12), %rax
	cmpq	%rax, 41096(%r12)
	je	.L5226
.L5148:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L5147
	.p2align 4,,10
	.p2align 3
.L5159:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L5227
.L5161:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rdx, (%r15)
	jmp	.L5160
	.p2align 4,,10
	.p2align 3
.L5222:
	movq	-1(%r9), %rax
	salq	$3, %rcx
	leaq	7(%rdx), %rdi
	andq	$-8, %rdi
	movq	%rax, -1(%rdx)
	movq	-8(%rsi,%rcx), %rax
	movq	%rax, -8(%r8,%rcx)
	subq	%rdi, %r8
	addl	%r8d, %ecx
	subq	%r8, %rsi
	shrl	$3, %ecx
	rep movsq
	jmp	.L5156
	.p2align 4,,10
	.p2align 3
.L5166:
	testq	%rdi, %rdi
	je	.L5170
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5171:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory14CopyFixedArrayENS0_6HandleINS0_10FixedArrayEEE
	movq	(%rax), %r14
	jmp	.L5165
	.p2align 4,,10
	.p2align 3
.L5153:
	movq	%rdx, %r8
	subq	%r9, %r8
	jmp	.L5219
	.p2align 4,,10
	.p2align 3
.L5228:
	subq	$1, %rax
.L5219:
	movq	%rdi, %rsi
	leaq	(%rdi,%r8), %rcx
	addq	$8, %rdi
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	jne	.L5228
	jmp	.L5156
	.p2align 4,,10
	.p2align 3
.L5226:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L5148
	.p2align 4,,10
	.p2align 3
.L5227:
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L5161
	.p2align 4,,10
	.p2align 3
.L5182:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L5229
.L5184:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L5183
	.p2align 4,,10
	.p2align 3
.L5179:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L5230
.L5181:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L5180
	.p2align 4,,10
	.p2align 3
.L5225:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5192
	.p2align 4,,10
	.p2align 3
.L5167:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L5231
.L5169:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L5168
	.p2align 4,,10
	.p2align 3
.L5170:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L5232
.L5172:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L5171
	.p2align 4,,10
	.p2align 3
.L5230:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L5181
	.p2align 4,,10
	.p2align 3
.L5229:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5184
.L5231:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5169
.L5232:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5172
	.cfi_endproc
.LFE24136:
	.size	_ZN2v88internal7Factory30CopyJSObjectWithAllocationSiteENS0_6HandleINS0_8JSObjectEEENS2_INS0_14AllocationSiteEEE, .-_ZN2v88internal7Factory30CopyJSObjectWithAllocationSiteENS0_6HandleINS0_8JSObjectEEENS2_INS0_14AllocationSiteEEE
	.section	.text._ZN2v88internal7Factory12CopyJSObjectENS0_6HandleINS0_8JSObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory12CopyJSObjectENS0_6HandleINS0_8JSObjectEEE
	.type	_ZN2v88internal7Factory12CopyJSObjectENS0_6HandleINS0_8JSObjectEEE, @function
_ZN2v88internal7Factory12CopyJSObjectENS0_6HandleINS0_8JSObjectEEE:
.LFB24135:
	.cfi_startproc
	endbr64
	xorl	%edx, %edx
	jmp	_ZN2v88internal7Factory30CopyJSObjectWithAllocationSiteENS0_6HandleINS0_8JSObjectEEENS2_INS0_14AllocationSiteEEE
	.cfi_endproc
.LFE24135:
	.size	_ZN2v88internal7Factory12CopyJSObjectENS0_6HandleINS0_8JSObjectEEE, .-_ZN2v88internal7Factory12CopyJSObjectENS0_6HandleINS0_8JSObjectEEE
	.section	.text._ZN2v88internal7Factory16CopyArrayAndGrowINS0_10FixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE,"axG",@progbits,_ZN2v88internal7Factory16CopyArrayAndGrowINS0_10FixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory16CopyArrayAndGrowINS0_10FixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory16CopyArrayAndGrowINS0_10FixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory16CopyArrayAndGrowINS0_10FixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE:
.LFB27436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	%edx, %edi
	pushq	%r14
	leaq	37592(%r15), %r10
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rax
	movl	%edx, -52(%rbp)
	movl	%ecx, %edx
	movslq	11(%rax), %r13
	leal	(%rdi,%r13), %r14d
	cmpl	$134217726, %r14d
	ja	.L5249
.L5235:
	xorl	%r8d, %r8d
	movq	%r10, %rdi
	movl	$1, %ecx
	movq	%r10, -64(%rbp)
	leal	16(,%r14,8), %r12d
	movl	%r12d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	-64(%rbp), %r10
	movq	%rax, %rdx
	movq	%rax, %rsi
	andq	$-262144, %rdx
	cmpl	$131072, %r12d
	jle	.L5237
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L5250
.L5237:
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L5239
	movq	%r10, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r10
	movq	(%rax), %rsi
	movq	%rax, %r12
.L5240:
	salq	$32, %r14
	movl	$4, %r9d
	movq	%r14, 7(%rsi)
	movq	8(%rdx), %rax
	testl	$262144, %eax
	jne	.L5242
	xorl	%r9d, %r9d
	testb	$24, %al
	sete	%r9b
	sall	$2, %r9d
.L5242:
	movq	(%r12), %rsi
	testq	%r13, %r13
	je	.L5244
	movq	(%rbx), %rcx
	leaq	15(%rsi), %rdx
	movl	%r13d, %r8d
	movq	%r10, %rdi
	addq	$15, %rcx
	call	_ZN2v88internal4Heap9CopyRangeINS0_14FullObjectSlotEEEvNS0_10HeapObjectET_S5_iNS0_16WriteBarrierModeE@PLT
	movq	(%r12), %rsi
.L5244:
	movq	88(%r15), %rax
	movslq	-52(%rbp), %rcx
	leaq	15(%rsi,%r13,8), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5239:
	.cfi_restore_state
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L5251
.L5241:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	jmp	.L5240
	.p2align 4,,10
	.p2align 3
.L5249:
	movq	%r10, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%r10, -64(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-72(%rbp), %edx
	movq	-64(%rbp), %r10
	jmp	.L5235
	.p2align 4,,10
	.p2align 3
.L5250:
	leaq	8(%rdx), %rdi
	jmp	.L5238
	.p2align 4,,10
	.p2align 3
.L5252:
	movq	%rcx, %r8
	movq	%rcx, %rax
	orq	$256, %r8
	lock cmpxchgq	%r8, (%rdi)
	cmpq	%rax, %rcx
	je	.L5237
.L5238:
	movq	(%rdi), %rcx
	testb	$1, %ch
	je	.L5252
	jmp	.L5237
	.p2align 4,,10
	.p2align 3
.L5251:
	movq	%r15, %rdi
	movq	%r10, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	%rax, %r12
	jmp	.L5241
	.cfi_endproc
.LFE27436:
	.size	_ZN2v88internal7Factory16CopyArrayAndGrowINS0_10FixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE, .-_ZN2v88internal7Factory16CopyArrayAndGrowINS0_10FixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE:
.LFB24142:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal7Factory16CopyArrayAndGrowINS0_10FixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE
	.cfi_endproc
.LFE24142:
	.size	_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory16CopyArrayAndGrowINS0_14WeakFixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE,"axG",@progbits,_ZN2v88internal7Factory16CopyArrayAndGrowINS0_14WeakFixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory16CopyArrayAndGrowINS0_14WeakFixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory16CopyArrayAndGrowINS0_14WeakFixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory16CopyArrayAndGrowINS0_14WeakFixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE:
.LFB27442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	%edx, %edi
	pushq	%r14
	leaq	37592(%r15), %r10
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rax
	movl	%edx, -52(%rbp)
	movl	%ecx, %edx
	movslq	11(%rax), %r13
	leal	(%rdi,%r13), %r14d
	cmpl	$134217726, %r14d
	ja	.L5269
.L5255:
	xorl	%r8d, %r8d
	movq	%r10, %rdi
	movl	$1, %ecx
	movq	%r10, -64(%rbp)
	leal	16(,%r14,8), %r12d
	movl	%r12d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	-64(%rbp), %r10
	movq	%rax, %rdx
	movq	%rax, %rsi
	andq	$-262144, %rdx
	cmpl	$131072, %r12d
	jle	.L5257
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L5270
.L5257:
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L5259
	movq	%r10, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r10
	movq	(%rax), %rsi
	movq	%rax, %r12
.L5260:
	salq	$32, %r14
	movl	$4, %r9d
	movq	%r14, 7(%rsi)
	movq	8(%rdx), %rax
	testl	$262144, %eax
	jne	.L5262
	xorl	%r9d, %r9d
	testb	$24, %al
	sete	%r9b
	sall	$2, %r9d
.L5262:
	movq	(%r12), %rsi
	testq	%r13, %r13
	je	.L5264
	movq	(%rbx), %rcx
	leaq	15(%rsi), %rdx
	movl	%r13d, %r8d
	movq	%r10, %rdi
	addq	$15, %rcx
	call	_ZN2v88internal4Heap9CopyRangeINS0_19FullMaybeObjectSlotEEEvNS0_10HeapObjectET_S5_iNS0_16WriteBarrierModeE@PLT
	movq	(%r12), %rsi
.L5264:
	movq	88(%r15), %rax
	movslq	-52(%rbp), %rcx
	leaq	15(%rsi,%r13,8), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5259:
	.cfi_restore_state
	movq	41088(%r15), %r12
	cmpq	41096(%r15), %r12
	je	.L5271
.L5261:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r12)
	jmp	.L5260
	.p2align 4,,10
	.p2align 3
.L5269:
	movq	%r10, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%r10, -64(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-72(%rbp), %edx
	movq	-64(%rbp), %r10
	jmp	.L5255
	.p2align 4,,10
	.p2align 3
.L5270:
	leaq	8(%rdx), %rdi
	jmp	.L5258
	.p2align 4,,10
	.p2align 3
.L5272:
	movq	%rcx, %r8
	movq	%rcx, %rax
	orq	$256, %r8
	lock cmpxchgq	%r8, (%rdi)
	cmpq	%rax, %rcx
	je	.L5257
.L5258:
	movq	(%rdi), %rcx
	testb	$1, %ch
	je	.L5272
	jmp	.L5257
	.p2align 4,,10
	.p2align 3
.L5271:
	movq	%r15, %rdi
	movq	%r10, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L5261
	.cfi_endproc
.LFE27442:
	.size	_ZN2v88internal7Factory16CopyArrayAndGrowINS0_14WeakFixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE, .-_ZN2v88internal7Factory16CopyArrayAndGrowINS0_14WeakFixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory25CopyWeakFixedArrayAndGrowENS0_6HandleINS0_14WeakFixedArrayEEEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory25CopyWeakFixedArrayAndGrowENS0_6HandleINS0_14WeakFixedArrayEEEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory25CopyWeakFixedArrayAndGrowENS0_6HandleINS0_14WeakFixedArrayEEEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory25CopyWeakFixedArrayAndGrowENS0_6HandleINS0_14WeakFixedArrayEEEiNS0_14AllocationTypeE:
.LFB24145:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal7Factory16CopyArrayAndGrowINS0_14WeakFixedArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE
	.cfi_endproc
.LFE24145:
	.size	_ZN2v88internal7Factory25CopyWeakFixedArrayAndGrowENS0_6HandleINS0_14WeakFixedArrayEEEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory25CopyWeakFixedArrayAndGrowENS0_6HandleINS0_14WeakFixedArrayEEEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory16CopyArrayAndGrowINS0_13PropertyArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE,"axG",@progbits,_ZN2v88internal7Factory16CopyArrayAndGrowINS0_13PropertyArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory16CopyArrayAndGrowINS0_13PropertyArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory16CopyArrayAndGrowINS0_13PropertyArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory16CopyArrayAndGrowINS0_13PropertyArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE:
.LFB27443:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	movl	%edx, %edi
	pushq	%r14
	leaq	37592(%r15), %r10
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rax
	movl	%edx, -52(%rbp)
	movl	%ecx, %edx
	movslq	11(%rax), %r12
	andl	$1023, %r12d
	leal	(%rdi,%r12), %r14d
	cmpl	$134217726, %r14d
	ja	.L5289
.L5275:
	xorl	%r8d, %r8d
	movq	%r10, %rdi
	movl	$1, %ecx
	movq	%r10, -64(%rbp)
	leal	16(,%r14,8), %r13d
	movl	%r13d, %esi
	call	_ZN2v88internal4Heap26AllocateRawWithRetryOrFailEiNS0_14AllocationTypeENS0_16AllocationOriginENS0_19AllocationAlignmentE@PLT
	movq	-64(%rbp), %r10
	movq	%rax, %rdx
	movq	%rax, %rsi
	andq	$-262144, %rdx
	cmpl	$131072, %r13d
	jle	.L5277
	cmpb	$0, _ZN2v88internal29FLAG_use_marking_progress_barE(%rip)
	jne	.L5290
.L5277:
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
	movq	%rax, -1(%rsi)
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L5279
	movq	%r10, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r10
	movq	(%rax), %rsi
	movq	%rax, %r13
.L5280:
	salq	$32, %r14
	movl	$4, %r9d
	movq	%r14, 7(%rsi)
	movq	8(%rdx), %rax
	testl	$262144, %eax
	jne	.L5282
	xorl	%r9d, %r9d
	testb	$24, %al
	sete	%r9b
	sall	$2, %r9d
.L5282:
	movq	0(%r13), %rsi
	testl	%r12d, %r12d
	je	.L5284
	movq	(%rbx), %rcx
	leaq	15(%rsi), %rdx
	movl	%r12d, %r8d
	movq	%r10, %rdi
	addq	$15, %rcx
	call	_ZN2v88internal4Heap9CopyRangeINS0_14FullObjectSlotEEEvNS0_10HeapObjectET_S5_iNS0_16WriteBarrierModeE@PLT
	movq	0(%r13), %rsi
.L5284:
	movslq	%r12d, %r12
	movq	88(%r15), %rax
	movslq	-52(%rbp), %rcx
	leaq	15(%rsi,%r12,8), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5279:
	.cfi_restore_state
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L5291
.L5281:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L5280
	.p2align 4,,10
	.p2align 3
.L5289:
	movq	%r10, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%r10, -64(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal4Heap23FatalProcessOutOfMemoryEPKc@PLT
	movl	-72(%rbp), %edx
	movq	-64(%rbp), %r10
	jmp	.L5275
	.p2align 4,,10
	.p2align 3
.L5290:
	leaq	8(%rdx), %rdi
	jmp	.L5278
	.p2align 4,,10
	.p2align 3
.L5292:
	movq	%rcx, %r8
	movq	%rcx, %rax
	orq	$256, %r8
	lock cmpxchgq	%r8, (%rdi)
	cmpq	%rax, %rcx
	je	.L5277
.L5278:
	movq	(%rdi), %rcx
	testb	$1, %ch
	je	.L5292
	jmp	.L5277
	.p2align 4,,10
	.p2align 3
.L5291:
	movq	%r15, %rdi
	movq	%r10, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L5281
	.cfi_endproc
.LFE27443:
	.size	_ZN2v88internal7Factory16CopyArrayAndGrowINS0_13PropertyArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE, .-_ZN2v88internal7Factory16CopyArrayAndGrowINS0_13PropertyArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE
	.section	.text._ZN2v88internal7Factory24CopyPropertyArrayAndGrowENS0_6HandleINS0_13PropertyArrayEEEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory24CopyPropertyArrayAndGrowENS0_6HandleINS0_13PropertyArrayEEEiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory24CopyPropertyArrayAndGrowENS0_6HandleINS0_13PropertyArrayEEEiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory24CopyPropertyArrayAndGrowENS0_6HandleINS0_13PropertyArrayEEEiNS0_14AllocationTypeE:
.LFB24148:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal7Factory16CopyArrayAndGrowINS0_13PropertyArrayEEENS0_6HandleIT_EES6_iNS0_14AllocationTypeE
	.cfi_endproc
.LFE24148:
	.size	_ZN2v88internal7Factory24CopyPropertyArrayAndGrowENS0_6HandleINS0_13PropertyArrayEEEiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory24CopyPropertyArrayAndGrowENS0_6HandleINS0_13PropertyArrayEEEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_,"axG",@progbits,_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	.type	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_, @function
_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_:
.LFB28845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	(%rdi), %eax
	andl	$15, %eax
	cmpl	$13, %eax
	ja	.L5295
	leaq	.L5297(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_,"aG",@progbits,_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_,comdat
	.align 4
	.align 4
.L5297:
	.long	.L5303-.L5297
	.long	.L5300-.L5297
	.long	.L5302-.L5297
	.long	.L5298-.L5297
	.long	.L5295-.L5297
	.long	.L5296-.L5297
	.long	.L5295-.L5297
	.long	.L5295-.L5297
	.long	.L5301-.L5297
	.long	.L5300-.L5297
	.long	.L5299-.L5297
	.long	.L5298-.L5297
	.long	.L5295-.L5297
	.long	.L5296-.L5297
	.section	.text._ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_,"axG",@progbits,_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_,comdat
	.p2align 4,,10
	.p2align 3
.L5296:
	movl	(%rdx), %r8d
	movq	(%rsi), %rax
	leaq	-32(%rbp), %rdi
	movl	%r8d, %esi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
	.p2align 4,,10
	.p2align 3
.L5294:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L5309
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5300:
	.cfi_restore_state
	movl	(%rdx), %r8d
	movq	(%rsi), %rax
	leaq	-32(%rbp), %rdi
	movl	%r8d, %esi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	jmp	.L5294
	.p2align 4,,10
	.p2align 3
.L5298:
	movl	(%rdx), %r8d
	movq	(%rsi), %rax
	leaq	-32(%rbp), %rdi
	movl	%r8d, %esi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	jmp	.L5294
	.p2align 4,,10
	.p2align 3
.L5301:
	movl	(%rdx), %eax
	movq	(%rsi), %rdx
	addl	$16, %eax
	cltq
	movzbl	-1(%rax,%rdx), %eax
	jmp	.L5294
	.p2align 4,,10
	.p2align 3
.L5302:
	movq	(%rsi), %rax
	movslq	(%rdx), %rbx
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%rbx,2), %eax
	jmp	.L5294
	.p2align 4,,10
	.p2align 3
.L5299:
	movq	(%rsi), %rax
	movslq	(%rdx), %rbx
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5305
	movq	8(%rdi), %rax
.L5306:
	movzbl	(%rax,%rbx), %eax
	jmp	.L5294
	.p2align 4,,10
	.p2align 3
.L5303:
	movl	(%rdx), %eax
	movq	(%rsi), %rdx
	leal	16(%rax,%rax), %eax
	cltq
	movzwl	-1(%rax,%rdx), %eax
	jmp	.L5294
.L5295:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5305:
	call	*%rax
	jmp	.L5306
.L5309:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE28845:
	.size	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_, .-_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	.section	.text._ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_
	.type	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_, @function
_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_:
.LFB24088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L5384
.L5312:
	movq	(%rbx), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L5385
.L5317:
	movq	(%r12), %rax
	movl	11(%rax), %ebx
	testl	%ebx, %ebx
	jne	.L5321
	movq	%r13, %rax
.L5322:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5386
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5321:
	.cfi_restore_state
	movq	0(%r13), %rdx
	movl	11(%rdx), %r15d
	testl	%r15d, %r15d
	jne	.L5323
	movq	%r12, %rax
	jmp	.L5322
	.p2align 4,,10
	.p2align 3
.L5385:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L5317
	movq	41112(%r14), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L5318
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	jmp	.L5317
	.p2align 4,,10
	.p2align 3
.L5384:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L5312
	movq	41112(%rdi), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L5313
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
	jmp	.L5312
	.p2align 4,,10
	.p2align 3
.L5323:
	leal	(%rbx,%r15), %esi
	cmpl	$2, %esi
	je	.L5387
	cmpl	$1073741799, %esi
	jbe	.L5327
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	jmp	.L5322
	.p2align 4,,10
	.p2align 3
.L5313:
	movq	41088(%r14), %r12
	cmpq	%r12, 41096(%r14)
	je	.L5388
.L5315:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r12)
	jmp	.L5312
	.p2align 4,,10
	.p2align 3
.L5318:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L5389
.L5320:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	jmp	.L5317
	.p2align 4,,10
	.p2align 3
.L5387:
	movl	$0, -88(%rbp)
	movq	-1(%rax), %rdx
	leaq	-80(%rbp), %r12
	leaq	-84(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	movzwl	11(%rdx), %edx
	movq	%rax, -80(%rbp)
	movl	%edx, -84(%rbp)
	leaq	-88(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	movq	%r12, %rsi
	movq	%r15, %rdi
	movl	%eax, %ebx
	movq	0(%r13), %rax
	movl	$0, -88(%rbp)
	movq	-1(%rax), %rcx
	movq	-104(%rbp), %rdx
	movzwl	11(%rcx), %ecx
	movq	%rax, -80(%rbp)
	movl	%ecx, -84(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	movl	%ebx, %edx
	orl	%eax, %edx
	cmpw	$255, %dx
	ja	.L5325
	movb	%al, -57(%rbp)
	xorl	%edx, %edx
	leaq	-58(%rbp), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movb	%bl, -58(%rbp)
	movq	%rax, -80(%rbp)
	movq	$2, -72(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb
	jmp	.L5322
	.p2align 4,,10
	.p2align 3
.L5327:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %r8d
	movq	-1(%rdx), %rdx
	movl	%r8d, %eax
	movzwl	11(%rdx), %r8d
	shrw	$3, %ax
	shrw	$3, %r8w
	andl	$1, %r8d
	andl	%eax, %r8d
	cmpl	$12, %esi
	jg	.L5328
	xorl	%edx, %edx
	movq	%r14, %rdi
	testb	%r8b, %r8b
	je	.L5329
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L5349
	movq	(%r12), %rcx
	movq	(%rax), %r10
	movq	-1(%rcx), %rax
	leaq	15(%r10), %r9
	movq	%r9, %r14
	cmpw	$63, 11(%rax)
	ja	.L5331
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L5331
	movq	15(%rcx), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5332
	movq	8(%rdi), %rcx
	.p2align 4,,10
	.p2align 3
.L5333:
	testl	%ebx, %ebx
	jle	.L5334
	leaq	15(%rcx), %rax
	leal	-1(%rbx), %edi
	subq	%r9, %rax
	cmpq	$30, %rax
	jbe	.L5335
	cmpl	$14, %edi
	jbe	.L5335
	movl	%ebx, %esi
	movq	%rcx, %rax
	movq	%r9, %rdx
	shrl	$4, %esi
	subq	%r10, %rax
	salq	$4, %rsi
	addq	%r9, %rsi
	.p2align 4,,10
	.p2align 3
.L5336:
	movdqu	-15(%rdx,%rax), %xmm0
	addq	$16, %rdx
	movups	%xmm0, -16(%rdx)
	cmpq	%rsi, %rdx
	jne	.L5336
	movl	%ebx, %eax
	movl	%edi, %edx
	andl	$-16, %eax
	movl	%eax, %r10d
	leaq	(%r9,%r10), %rsi
	cmpl	%eax, %ebx
	je	.L5338
	movzbl	(%rcx,%r10), %r10d
	movb	%r10b, (%rsi)
	leal	1(%rax), %r10d
	cmpl	%r10d, %ebx
	jle	.L5338
	movslq	%r10d, %r10
	movzbl	(%rcx,%r10), %r10d
	movb	%r10b, 1(%rsi)
	leal	2(%rax), %r10d
	cmpl	%r10d, %ebx
	jle	.L5338
	movslq	%r10d, %r10
	movzbl	(%rcx,%r10), %r10d
	movb	%r10b, 2(%rsi)
	leal	3(%rax), %r10d
	cmpl	%r10d, %ebx
	jle	.L5338
	movslq	%r10d, %r10
	movzbl	(%rcx,%r10), %r10d
	movb	%r10b, 3(%rsi)
	leal	4(%rax), %r10d
	cmpl	%r10d, %ebx
	jle	.L5338
	movslq	%r10d, %r10
	movzbl	(%rcx,%r10), %r10d
	movb	%r10b, 4(%rsi)
	leal	5(%rax), %r10d
	cmpl	%r10d, %ebx
	jle	.L5338
	movslq	%r10d, %r10
	movzbl	(%rcx,%r10), %r10d
	movb	%r10b, 5(%rsi)
	leal	6(%rax), %r10d
	cmpl	%r10d, %ebx
	jle	.L5338
	movslq	%r10d, %r10
	movzbl	(%rcx,%r10), %r10d
	movb	%r10b, 6(%rsi)
	leal	7(%rax), %r10d
	cmpl	%r10d, %ebx
	jle	.L5338
	movslq	%r10d, %r10
	movzbl	(%rcx,%r10), %r10d
	movb	%r10b, 7(%rsi)
	leal	8(%rax), %r10d
	cmpl	%r10d, %ebx
	jle	.L5338
	movslq	%r10d, %r10
	movzbl	(%rcx,%r10), %edx
	leal	9(%rax), %r10d
	movb	%dl, 8(%rsi)
	movl	%edi, %edx
	cmpl	%r10d, %ebx
	jle	.L5338
	movslq	%r10d, %r10
	movzbl	(%rcx,%r10), %edi
	movb	%dil, 9(%rsi)
	leal	10(%rax), %edi
	cmpl	%edi, %ebx
	jle	.L5338
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	movb	%dil, 10(%rsi)
	leal	11(%rax), %edi
	cmpl	%edi, %ebx
	jle	.L5338
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	movb	%dil, 11(%rsi)
	leal	12(%rax), %edi
	cmpl	%edi, %ebx
	jle	.L5338
	movslq	%edi, %rdi
	movzbl	(%rcx,%rdi), %edi
	movb	%dil, 12(%rsi)
	leal	13(%rax), %edi
	cmpl	%edi, %ebx
	jle	.L5338
	movslq	%edi, %rdi
	addl	$14, %eax
	movzbl	(%rcx,%rdi), %edi
	movb	%dil, 13(%rsi)
	cmpl	%eax, %ebx
	jle	.L5338
	cltq
	movzbl	(%rcx,%rax), %eax
	movb	%al, 14(%rsi)
	.p2align 4,,10
	.p2align 3
.L5338:
	leaq	1(%r9,%rdx), %r14
.L5334:
	movq	0(%r13), %rcx
	movq	-1(%rcx), %rax
	cmpw	$63, 11(%rax)
	ja	.L5340
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$2, %ax
	jne	.L5340
	movq	15(%rcx), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5341
	movq	8(%rdi), %rcx
	.p2align 4,,10
	.p2align 3
.L5342:
	testl	%r15d, %r15d
	jle	.L5343
	leaq	15(%rcx), %rdx
	leal	-1(%r15), %eax
	subq	%r14, %rdx
	cmpq	$30, %rdx
	jbe	.L5344
	cmpl	$14, %eax
	jbe	.L5344
	movl	%r15d, %eax
	xorl	%edx, %edx
	shrl	$4, %eax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L5345:
	movdqu	(%rcx,%rdx), %xmm1
	movups	%xmm1, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L5345
	movl	%r15d, %eax
	andl	$-16, %eax
	movl	%eax, %edx
	addq	%rdx, %r14
	cmpl	%eax, %r15d
	je	.L5343
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, (%r14)
	leal	1(%rax), %edx
	cmpl	%r15d, %edx
	jge	.L5343
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, 1(%r14)
	leal	2(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L5343
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, 2(%r14)
	leal	3(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L5343
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, 3(%r14)
	leal	4(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L5343
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, 4(%r14)
	leal	5(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L5343
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, 5(%r14)
	leal	6(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L5343
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, 6(%r14)
	leal	7(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L5343
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, 7(%r14)
	leal	8(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L5343
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, 8(%r14)
	leal	9(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L5343
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, 9(%r14)
	leal	10(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L5343
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, 10(%r14)
	leal	11(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L5343
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, 11(%r14)
	leal	12(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L5343
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, 12(%r14)
	leal	13(%rax), %edx
	cmpl	%edx, %r15d
	jle	.L5343
	movslq	%edx, %rdx
	addl	$14, %eax
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, 13(%r14)
	cmpl	%eax, %r15d
	jle	.L5343
	cltq
	movzbl	(%rcx,%rax), %eax
	movb	%al, 14(%r14)
	.p2align 4,,10
	.p2align 3
.L5343:
	movq	%r8, %rax
	jmp	.L5322
	.p2align 4,,10
	.p2align 3
.L5329:
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L5349
	movq	(%rax), %rax
	movq	(%r12), %rdi
	xorl	%edx, %edx
	leaq	15(%rax), %r14
	movl	11(%rdi), %ecx
	movq	%r14, %rsi
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	movq	(%r12), %rax
	movq	0(%r13), %rdi
	xorl	%edx, %edx
	movslq	11(%rax), %rax
	movl	11(%rdi), %ecx
	leaq	(%r14,%rax,2), %rsi
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	movq	%rbx, %rax
	jmp	.L5322
	.p2align 4,,10
	.p2align 3
.L5325:
	movw	%ax, -60(%rbp)
	xorl	%edx, %edx
	leaq	-62(%rbp), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movw	%bx, -62(%rbp)
	movq	%rax, -80(%rbp)
	movq	$2, -72(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb
	jmp	.L5322
	.p2align 4,,10
	.p2align 3
.L5328:
	movl	%esi, %ecx
	andl	$1, %r8d
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_ib
	jmp	.L5322
	.p2align 4,,10
	.p2align 3
.L5340:
	addq	$15, %rcx
	jmp	.L5342
	.p2align 4,,10
	.p2align 3
.L5331:
	addq	$15, %rcx
	jmp	.L5333
	.p2align 4,,10
	.p2align 3
.L5389:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L5320
	.p2align 4,,10
	.p2align 3
.L5388:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L5315
.L5349:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L5344:
	movl	%eax, %edx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L5348:
	movzbl	(%rcx,%rax), %esi
	movb	%sil, (%r14,%rax)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rdx, %rsi
	jne	.L5348
	jmp	.L5343
.L5335:
	movl	%edi, %edx
	movq	%r9, %rax
	subq	%r10, %rcx
	leaq	16(%r10,%rdx), %r11
	.p2align 4,,10
	.p2align 3
.L5339:
	movq	%rax, %rsi
	movzbl	-15(%rax,%rcx), %edi
	addq	$1, %rax
	movb	%dil, (%rsi)
	cmpq	%r11, %rax
	jne	.L5339
	jmp	.L5338
.L5341:
	movq	%r8, -104(%rbp)
	call	*%rax
	movq	-104(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L5342
.L5332:
	movq	%r9, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%r10, -104(%rbp)
	call	*%rax
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r8
	movq	-120(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L5333
.L5386:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24088:
	.size	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_, .-_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_
	.section	.text._ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii
	.type	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii, @function
_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii:
.LFB24091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r15d, %ebx
	subl	%r14d, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal6String7FlattenEPNS0_7IsolateENS0_6HandleIS1_EENS0_14AllocationTypeE
	testl	%ebx, %ebx
	jle	.L5425
	movq	(%rax), %rsi
	movq	%rax, %r12
	movq	%rax, %rcx
	cmpl	$1, %ebx
	je	.L5426
	cmpl	$2, %ebx
	je	.L5427
	movq	-1(%rsi), %rax
	cmpl	$12, %ebx
	jg	.L5397
	xorl	%edx, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	testb	$8, 11(%rax)
	je	.L5398
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L5400
	movq	(%rax), %rax
	movq	(%r12), %rdi
	movl	%r15d, %ecx
	movl	%r14d, %edx
	leaq	15(%rax), %rsi
	call	_ZN2v88internal6String11WriteToFlatIhEEvS1_PT_ii@PLT
	movq	%r13, %rax
	jmp	.L5392
	.p2align 4,,10
	.p2align 3
.L5425:
	leaq	128(%r13), %rax
.L5392:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5428
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5397:
	.cfi_restore_state
	cmpw	$63, 11(%rax)
	ja	.L5402
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$3, %ax
	jne	.L5402
	movq	41112(%r13), %rdi
	movq	15(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L5404
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rcx
.L5405:
	movq	(%r12), %rax
	addl	27(%rax), %r14d
.L5402:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L5424
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L5409
.L5424:
	movq	(%rcx), %rsi
.L5408:
	movq	%rcx, -104(%rbp)
	movq	-1(%rsi), %rdx
	leaq	792(%r13), %rax
	movq	%r13, %rdi
	leaq	784(%r13), %rsi
	testb	$8, 11(%rdx)
	cmovne	%rax, %rsi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE
	movq	41112(%r13), %rdi
	movq	-104(%rbp), %rcx
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L5415
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %rcx
	movq	(%rax), %rsi
	movq	%rax, %r12
.L5416:
	movl	$3, 7(%rsi)
	movq	(%r12), %rax
	movl	%ebx, 11(%rax)
	movq	(%r12), %r13
	movq	(%rcx), %r15
	leaq	15(%r13), %rbx
	movq	%r13, %rdi
	movq	%r15, 15(%r13)
	movq	%r15, %rdx
	movq	%rbx, %rsi
	call	_ZN2v88internal14MarkingBarrierENS0_10HeapObjectENS0_14FullObjectSlotENS0_6ObjectE
	testb	$1, %r15b
	je	.L5418
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal14heap_internals27GenerationalBarrierInternalENS0_10HeapObjectEmS2_
.L5418:
	movq	(%r12), %rax
	salq	$32, %r14
	movq	%r14, 23(%rax)
	movq	%r12, %rax
	jmp	.L5392
	.p2align 4,,10
	.p2align 3
.L5426:
	movl	%r14d, -88(%rbp)
	movq	-1(%rsi), %rax
	leaq	-84(%rbp), %rdi
	leaq	-88(%rbp), %rdx
	movzwl	11(%rax), %eax
	movq	%rsi, -80(%rbp)
	leaq	-80(%rbp), %rsi
	movl	%eax, -84(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	movq	%r13, %rdi
	movzwl	%ax, %esi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt
	jmp	.L5392
	.p2align 4,,10
	.p2align 3
.L5427:
	movl	%r14d, -88(%rbp)
	movq	-1(%rsi), %rax
	leaq	-80(%rbp), %r15
	leaq	-88(%rbp), %rdx
	leaq	-84(%rbp), %rdi
	movq	%rdx, -112(%rbp)
	addl	$1, %r14d
	movzwl	11(%rax), %eax
	movq	%rsi, -80(%rbp)
	movq	%r15, %rsi
	movq	%rdi, -104(%rbp)
	movl	%eax, -84(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	movq	%r15, %rsi
	movl	%eax, %ebx
	movq	(%r12), %rax
	movl	%r14d, -88(%rbp)
	movq	-1(%rax), %rcx
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rdi
	movzwl	11(%rcx), %ecx
	movq	%rax, -80(%rbp)
	movl	%ecx, -84(%rbp)
	call	_ZN2v88internal11StringShape33DispatchToSpecificTypeWithoutCastIZNS1_22DispatchToSpecificTypeIZNS0_6String3GetEiE19StringGetDispatchertJRiEEET0_S4_DpOT1_E17CastingDispatchertJRS4_S6_EEES7_SA_
	movl	%ebx, %edx
	orl	%eax, %edx
	cmpw	$255, %dx
	ja	.L5395
	movb	%al, -57(%rbp)
	xorl	%edx, %edx
	leaq	-58(%rbp), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movb	%bl, -58(%rbp)
	movq	%rax, -80(%rbp)
	movq	$2, -72(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringIhEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb
	jmp	.L5392
	.p2align 4,,10
	.p2align 3
.L5398:
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L5400
	movq	(%rax), %rax
	movq	(%r12), %rdi
	movl	%r15d, %ecx
	movl	%r14d, %edx
	leaq	15(%rax), %rsi
	call	_ZN2v88internal6String11WriteToFlatItEEvS1_PT_ii@PLT
	movq	%r13, %rax
	jmp	.L5392
	.p2align 4,,10
	.p2align 3
.L5415:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L5429
.L5417:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
	jmp	.L5416
	.p2align 4,,10
	.p2align 3
.L5395:
	movw	%ax, -60(%rbp)
	xorl	%edx, %edx
	leaq	-62(%rbp), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movw	%bx, -62(%rbp)
	movq	%rax, -80(%rbp)
	movq	$2, -72(%rbp)
	call	_ZN2v88internal7Factory17InternalizeStringItEENS0_6HandleINS0_6StringEEERKNS0_6VectorIKT_EEb
	jmp	.L5392
	.p2align 4,,10
	.p2align 3
.L5409:
	movq	(%rcx), %rax
	movq	41112(%r13), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L5410
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
	jmp	.L5424
	.p2align 4,,10
	.p2align 3
.L5410:
	movq	41088(%r13), %rcx
	cmpq	41096(%r13), %rcx
	je	.L5430
.L5412:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rcx)
	jmp	.L5408
	.p2align 4,,10
	.p2align 3
.L5404:
	movq	41088(%r13), %rcx
	cmpq	41096(%r13), %rcx
	je	.L5431
.L5406:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rcx)
	jmp	.L5405
	.p2align 4,,10
	.p2align 3
.L5400:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5429:
	movq	%r13, %rdi
	movq	%rcx, -104(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rcx
	movq	%rax, %r12
	jmp	.L5417
.L5430:
	movq	%r13, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L5412
.L5431:
	movq	%r13, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L5406
.L5428:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24091:
	.size	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii, .-_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii
	.section	.text._ZN2v88internal7Factory26NewStringFromUtf8SubStringENS0_6HandleINS0_16SeqOneByteStringEEEiiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory26NewStringFromUtf8SubStringENS0_6HandleINS0_16SeqOneByteStringEEEiiNS0_14AllocationTypeE
	.type	_ZN2v88internal7Factory26NewStringFromUtf8SubStringENS0_6HandleINS0_16SeqOneByteStringEEEiiNS0_14AllocationTypeE, @function
_ZN2v88internal7Factory26NewStringFromUtf8SubStringENS0_6HandleINS0_16SeqOneByteStringEEEiiNS0_14AllocationTypeE:
.LFB24059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-92(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movslq	%edx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movslq	%ecx, %rdi
	subq	$88, %rsp
	movl	%r8d, -108(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rsi, -120(%rbp)
	movq	%r12, %rsi
	movq	%rdi, -128(%rbp)
	movq	%rdi, -72(%rbp)
	leaq	15(%r13,%rax), %rax
	movq	%r15, %rdi
	movl	%ecx, -104(%rbp)
	movl	%edx, -100(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal11Utf8DecoderC1ERKNS0_6VectorIKhEE@PLT
	movl	-104(%rbp), %ecx
	movl	-100(%rbp), %edx
	cmpl	$1, %ecx
	je	.L5444
	movzbl	-92(%rbp), %eax
	testb	%al, %al
	je	.L5445
	movl	-84(%rbp), %esi
	movzbl	-108(%rbp), %edx
	movq	%rbx, %rdi
	cmpb	$1, %al
	jbe	.L5446
	call	_ZN2v88internal7Factory19NewRawTwoByteStringEiNS0_14AllocationTypeE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L5443
	movq	(%r14), %rax
	movq	%r12, %rdx
	movq	%r15, %rdi
	leaq	15(%r13,%rax), %rax
	movq	%rax, -80(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	(%rbx), %rax
	leaq	15(%rax), %rsi
	call	_ZN2v88internal11Utf8Decoder6DecodeItEEvPT_RKNS0_6VectorIKhEE@PLT
	movq	%rbx, %rax
.L5434:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L5447
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5446:
	.cfi_restore_state
	call	_ZN2v88internal7Factory19NewRawOneByteStringEiNS0_14AllocationTypeE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L5443
	movq	(%r14), %rax
	movq	%r12, %rdx
	movq	%r15, %rdi
	leaq	15(%r13,%rax), %rax
	movq	%rax, -80(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	(%rbx), %rax
	leaq	15(%rax), %rsi
	call	_ZN2v88internal11Utf8Decoder6DecodeIhEEvPT_RKNS0_6VectorIKhEE@PLT
	movq	%rbx, %rax
	jmp	.L5434
	.p2align 4,,10
	.p2align 3
.L5445:
	leal	(%rcx,%rdx), %r8d
	testl	%edx, %edx
	jne	.L5436
	movq	(%r14), %rax
	cmpl	11(%rax), %ecx
	je	.L5437
.L5436:
	movl	%r8d, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii
	movq	%rax, -120(%rbp)
.L5437:
	movq	-120(%rbp), %rax
	jmp	.L5434
	.p2align 4,,10
	.p2align 3
.L5444:
	leaq	-94(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r12, %rdx
	call	_ZN2v88internal11Utf8Decoder6DecodeItEEvPT_RKNS0_6VectorIKhEE@PLT
	movzwl	-94(%rbp), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory35LookupSingleCharacterStringFromCodeEt
	jmp	.L5434
	.p2align 4,,10
	.p2align 3
.L5443:
	xorl	%eax, %eax
	jmp	.L5434
.L5447:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24059:
	.size	_ZN2v88internal7Factory26NewStringFromUtf8SubStringENS0_6HandleINS0_16SeqOneByteStringEEEiiNS0_14AllocationTypeE, .-_ZN2v88internal7Factory26NewStringFromUtf8SubStringENS0_6HandleINS0_16SeqOneByteStringEEEiiNS0_14AllocationTypeE
	.section	.rodata._ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_.str1.1,"aMS",@progbits,1
.LC42:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB29066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movabsq	$1152921504606846975, %rsi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r12
	movq	%rcx, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L5475
	movq	%r13, %r9
	movq	%rdi, %r15
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L5460
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L5476
.L5450:
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L5459:
	movq	(%rdx), %rax
	movq	%rax, (%rbx,%r9)
	cmpq	%r12, %r13
	je	.L5452
	leaq	-8(%r13), %rsi
	leaq	15(%rbx), %rax
	subq	%r12, %rsi
	subq	%r12, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L5462
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L5462
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L5454:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L5454
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%r8, %rdi
	je	.L5456
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L5456:
	leaq	16(%rbx,%rsi), %r14
.L5452:
	cmpq	%rcx, %r13
	je	.L5457
	subq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r14
.L5457:
	testq	%r12, %r12
	je	.L5458
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L5458:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5476:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L5451
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L5459
	.p2align 4,,10
	.p2align 3
.L5460:
	movl	$8, %r14d
	jmp	.L5450
	.p2align 4,,10
	.p2align 3
.L5462:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L5453:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %r13
	jne	.L5453
	jmp	.L5456
.L5451:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r14
	salq	$3, %r14
	jmp	.L5450
.L5475:
	leaq	.LC42(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29066:
	.size	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZN2v88internal7Factory28NewExternalStringFromTwoByteEPKNS_6String22ExternalStringResourceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory28NewExternalStringFromTwoByteEPKNS_6String22ExternalStringResourceE
	.type	_ZN2v88internal7Factory28NewExternalStringFromTwoByteEPKNS_6String22ExternalStringResourceE, @function
_ZN2v88internal7Factory28NewExternalStringFromTwoByteEPKNS_6String22ExternalStringResourceE:
.LFB24093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*56(%rax)
	cmpq	$1073741799, %rax
	ja	.L5497
	movq	%rax, %rbx
	leaq	128(%r12), %rax
	testq	%rbx, %rbx
	je	.L5479
	movq	0(%r13), %rax
	leaq	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5481
.L5483:
	leaq	800(%r12), %rsi
.L5482:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L5484
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L5485:
	movl	%ebx, 11(%rsi)
	movq	(%r14), %rax
	movl	$3, 7(%rax)
	movq	(%r14), %r15
	movq	%r13, 15(%r15)
	movq	-1(%r15), %rax
	testb	$16, 11(%rax)
	jne	.L5487
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*48(%rax)
	movq	%rax, 23(%r15)
.L5487:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*56(%rax)
	addq	%rax, %rax
	jne	.L5498
.L5493:
	movq	(%r14), %rax
	movq	%rax, %rdx
	movq	%rax, -64(%rbp)
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L5488
	movq	40376(%r12), %rsi
	cmpq	40384(%r12), %rsi
	je	.L5489
	movq	%rax, (%rsi)
	addq	$8, 40376(%r12)
.L5490:
	movq	%r14, %rax
.L5479:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5499
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5497:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	jmp	.L5479
	.p2align 4,,10
	.p2align 3
.L5484:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L5500
.L5486:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L5485
	.p2align 4,,10
	.p2align 3
.L5488:
	movq	40400(%r12), %rsi
	cmpq	40408(%r12), %rsi
	je	.L5491
	movq	%rax, (%rsi)
	addq	$8, 40400(%r12)
	jmp	.L5490
	.p2align 4,,10
	.p2align 3
.L5498:
	leaq	37592(%r12), %rdi
	movq	%rax, %rcx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal4Heap20UpdateExternalStringENS0_6StringEmm@PLT
	jmp	.L5493
	.p2align 4,,10
	.p2align 3
.L5481:
	movq	%r13, %rdi
	call	*%rax
	testb	%al, %al
	jne	.L5483
	leaq	816(%r12), %rsi
	jmp	.L5482
	.p2align 4,,10
	.p2align 3
.L5489:
	leaq	-64(%rbp), %rdx
	leaq	40368(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L5490
	.p2align 4,,10
	.p2align 3
.L5491:
	leaq	-64(%rbp), %rdx
	leaq	40392(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L5490
	.p2align 4,,10
	.p2align 3
.L5500:
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L5486
.L5499:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24093:
	.size	_ZN2v88internal7Factory28NewExternalStringFromTwoByteEPKNS_6String22ExternalStringResourceE, .-_ZN2v88internal7Factory28NewExternalStringFromTwoByteEPKNS_6String22ExternalStringResourceE
	.section	.text._ZN2v88internal7Factory21NewNativeSourceStringEPKNS_6String29ExternalOneByteStringResourceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory21NewNativeSourceStringEPKNS_6String29ExternalOneByteStringResourceE
	.type	_ZN2v88internal7Factory21NewNativeSourceStringEPKNS_6String29ExternalOneByteStringResourceE, @function
_ZN2v88internal7Factory21NewNativeSourceStringEPKNS_6String29ExternalOneByteStringResourceE:
.LFB24094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	_ZNK2v88internal29NativesExternalStringResource6lengthEv(%rip), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	56(%rax), %rax
	cmpq	%r14, %rax
	jne	.L5502
	movq	16(%rsi), %r15
.L5503:
	leaq	736(%rbx), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L5504
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r12
.L5505:
	movl	%r15d, 11(%rsi)
	movq	(%r12), %rax
	movl	$3, 7(%rax)
	movq	(%r12), %r15
	movq	%r13, 15(%r15)
	movq	-1(%r15), %rax
	testb	$16, 11(%rax)
	jne	.L5507
	movq	0(%r13), %rax
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5508
	movq	8(%r13), %rax
.L5509:
	movq	%rax, 23(%r15)
.L5507:
	movq	0(%r13), %rax
	movq	56(%rax), %rax
	cmpq	%r14, %rax
	jne	.L5525
	movq	16(%r13), %rcx
.L5510:
	testq	%rcx, %rcx
	jne	.L5526
.L5511:
	movq	(%r12), %rax
	movq	%rax, %rdx
	movq	%rax, -64(%rbp)
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L5512
	movq	40376(%rbx), %rsi
	cmpq	40384(%rbx), %rsi
	je	.L5513
	movq	%rax, (%rsi)
	addq	$8, 40376(%rbx)
.L5514:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5527
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5526:
	.cfi_restore_state
	leaq	37592(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal4Heap20UpdateExternalStringENS0_6StringEmm@PLT
	jmp	.L5511
	.p2align 4,,10
	.p2align 3
.L5512:
	movq	40400(%rbx), %rsi
	cmpq	40408(%rbx), %rsi
	je	.L5515
	movq	%rax, (%rsi)
	addq	$8, 40400(%rbx)
	jmp	.L5514
	.p2align 4,,10
	.p2align 3
.L5504:
	movq	41088(%rbx), %r12
	cmpq	41096(%rbx), %r12
	je	.L5528
.L5506:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r12)
	jmp	.L5505
	.p2align 4,,10
	.p2align 3
.L5502:
	movq	%rsi, %rdi
	call	*%rax
	movq	%rax, %r15
	jmp	.L5503
	.p2align 4,,10
	.p2align 3
.L5513:
	leaq	-64(%rbp), %rdx
	leaq	40368(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L5514
	.p2align 4,,10
	.p2align 3
.L5508:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L5509
	.p2align 4,,10
	.p2align 3
.L5525:
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %rcx
	jmp	.L5510
	.p2align 4,,10
	.p2align 3
.L5515:
	leaq	-64(%rbp), %rdx
	leaq	40392(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L5514
	.p2align 4,,10
	.p2align 3
.L5528:
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L5506
.L5527:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24094:
	.size	_ZN2v88internal7Factory21NewNativeSourceStringEPKNS_6String29ExternalOneByteStringResourceE, .-_ZN2v88internal7Factory21NewNativeSourceStringEPKNS_6String29ExternalOneByteStringResourceE
	.section	.text._ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalTwoByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE,"axG",@progbits,_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalTwoByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalTwoByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE
	.type	_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalTwoByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE, @function
_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalTwoByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE:
.LFB27263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$32, %eax
	cmpw	$26, %ax
	ja	.L5530
	leaq	.L5532(%rip), %rdx
	movzwl	%ax, %eax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalTwoByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE,"aG",@progbits,_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalTwoByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE,comdat
	.align 4
	.align 4
.L5532:
	.long	.L5537-.L5532
	.long	.L5530-.L5532
	.long	.L5536-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5535-.L5532
	.long	.L5530-.L5532
	.long	.L5534-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5533-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5530-.L5532
	.long	.L5531-.L5532
	.section	.text._ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalTwoByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE,"axG",@progbits,_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalTwoByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE,comdat
	.p2align 4,,10
	.p2align 3
.L5530:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5537:
	leaq	824(%rdi), %rsi
	.p2align 4,,10
	.p2align 3
.L5538:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L5551
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L5539:
	movq	(%r12), %rdx
	movl	11(%rdx), %edx
	movl	%edx, 11(%rsi)
	movq	(%r12), %rdx
	movl	7(%rdx), %ecx
	movq	(%rax), %rdx
	movl	%ecx, 7(%rdx)
	movq	(%rax), %rdx
	movq	$0, 15(%rdx)
	movq	(%rax), %rdx
	movq	%rdx, %rcx
	movq	%rdx, -32(%rbp)
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	je	.L5541
	movq	40376(%rbx), %rsi
	cmpq	40384(%rbx), %rsi
	je	.L5542
	movq	%rdx, (%rsi)
	addq	$8, 40376(%rbx)
.L5543:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5552
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5536:
	.cfi_restore_state
	leaq	832(%rdi), %rsi
	jmp	.L5538
	.p2align 4,,10
	.p2align 3
.L5535:
	leaq	192(%rdi), %rsi
	jmp	.L5538
	.p2align 4,,10
	.p2align 3
.L5534:
	leaq	840(%rdi), %rsi
	jmp	.L5538
	.p2align 4,,10
	.p2align 3
.L5533:
	leaq	848(%rdi), %rsi
	jmp	.L5538
	.p2align 4,,10
	.p2align 3
.L5531:
	leaq	856(%rdi), %rsi
	jmp	.L5538
	.p2align 4,,10
	.p2align 3
.L5541:
	movq	40400(%rbx), %rsi
	cmpq	40408(%rbx), %rsi
	je	.L5544
	movq	%rdx, (%rsi)
	addq	$8, 40400(%rbx)
	jmp	.L5543
	.p2align 4,,10
	.p2align 3
.L5551:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L5553
.L5540:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L5539
	.p2align 4,,10
	.p2align 3
.L5542:
	leaq	-32(%rbp), %rdx
	leaq	40368(%rbx), %rdi
	movq	%rax, -40(%rbp)
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	-40(%rbp), %rax
	jmp	.L5543
	.p2align 4,,10
	.p2align 3
.L5544:
	leaq	-32(%rbp), %rdx
	leaq	40392(%rbx), %rdi
	movq	%rax, -40(%rbp)
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	-40(%rbp), %rax
	jmp	.L5543
	.p2align 4,,10
	.p2align 3
.L5553:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L5540
.L5552:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27263:
	.size	_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalTwoByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE, .-_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalTwoByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE
	.section	.text._ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalOneByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE,"axG",@progbits,_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalOneByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalOneByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE
	.type	_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalOneByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE, @function
_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalOneByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE:
.LFB27262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subl	$32, %eax
	cmpw	$26, %ax
	ja	.L5555
	leaq	.L5557(%rip), %rdx
	movzwl	%ax, %eax
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalOneByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE,"aG",@progbits,_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalOneByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE,comdat
	.align 4
	.align 4
.L5557:
	.long	.L5562-.L5557
	.long	.L5555-.L5557
	.long	.L5561-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5560-.L5557
	.long	.L5555-.L5557
	.long	.L5559-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5558-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5555-.L5557
	.long	.L5556-.L5557
	.section	.text._ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalOneByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE,"axG",@progbits,_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalOneByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE,comdat
	.p2align 4,,10
	.p2align 3
.L5555:
	leaq	.LC3(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5562:
	leaq	824(%rdi), %rsi
	.p2align 4,,10
	.p2align 3
.L5563:
	movq	%rbx, %rdi
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L5576
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L5564:
	movq	(%r12), %rdx
	movl	11(%rdx), %edx
	movl	%edx, 11(%rsi)
	movq	(%r12), %rdx
	movl	7(%rdx), %ecx
	movq	(%rax), %rdx
	movl	%ecx, 7(%rdx)
	movq	(%rax), %rdx
	movq	$0, 15(%rdx)
	movq	(%rax), %rdx
	movq	%rdx, %rcx
	movq	%rdx, -32(%rbp)
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	je	.L5566
	movq	40376(%rbx), %rsi
	cmpq	40384(%rbx), %rsi
	je	.L5567
	movq	%rdx, (%rsi)
	addq	$8, 40376(%rbx)
.L5568:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5577
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5561:
	.cfi_restore_state
	leaq	832(%rdi), %rsi
	jmp	.L5563
	.p2align 4,,10
	.p2align 3
.L5560:
	leaq	192(%rdi), %rsi
	jmp	.L5563
	.p2align 4,,10
	.p2align 3
.L5559:
	leaq	840(%rdi), %rsi
	jmp	.L5563
	.p2align 4,,10
	.p2align 3
.L5558:
	leaq	848(%rdi), %rsi
	jmp	.L5563
	.p2align 4,,10
	.p2align 3
.L5556:
	leaq	856(%rdi), %rsi
	jmp	.L5563
	.p2align 4,,10
	.p2align 3
.L5566:
	movq	40400(%rbx), %rsi
	cmpq	40408(%rbx), %rsi
	je	.L5569
	movq	%rdx, (%rsi)
	addq	$8, 40400(%rbx)
	jmp	.L5568
	.p2align 4,,10
	.p2align 3
.L5576:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L5578
.L5565:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L5564
	.p2align 4,,10
	.p2align 3
.L5567:
	leaq	-32(%rbp), %rdx
	leaq	40368(%rbx), %rdi
	movq	%rax, -40(%rbp)
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	-40(%rbp), %rax
	jmp	.L5568
	.p2align 4,,10
	.p2align 3
.L5569:
	leaq	-32(%rbp), %rdx
	leaq	40392(%rbx), %rdi
	movq	%rax, -40(%rbp)
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	-40(%rbp), %rax
	jmp	.L5568
	.p2align 4,,10
	.p2align 3
.L5578:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L5565
.L5577:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE27262:
	.size	_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalOneByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE, .-_ZN2v88internal7Factory25InternalizeExternalStringINS0_21ExternalOneByteStringEEENS0_6HandleIT_EENS4_INS0_6StringEEE
	.section	.text._ZN2v88internal7Factory28NewExternalStringFromOneByteEPKNS_6String29ExternalOneByteStringResourceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Factory28NewExternalStringFromOneByteEPKNS_6String29ExternalOneByteStringResourceE
	.type	_ZN2v88internal7Factory28NewExternalStringFromOneByteEPKNS_6String29ExternalOneByteStringResourceE, @function
_ZN2v88internal7Factory28NewExternalStringFromOneByteEPKNS_6String29ExternalOneByteStringResourceE:
.LFB24092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZNK2v88internal29NativesExternalStringResource6lengthEv(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	56(%rax), %rax
	cmpq	%r15, %rax
	jne	.L5580
	movq	16(%rsi), %r14
.L5581:
	cmpq	$1073741799, %r14
	ja	.L5609
	leaq	128(%r12), %rax
	testq	%r14, %r14
	je	.L5583
	movq	0(%r13), %rax
	leaq	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5585
.L5587:
	leaq	808(%r12), %rsi
.L5586:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory3NewENS0_6HandleINS0_3MapEEENS0_14AllocationTypeE.constprop.0
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L5588
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L5589:
	movl	%r14d, 11(%rsi)
	movq	(%rbx), %rax
	movl	$3, 7(%rax)
	movq	(%rbx), %r14
	movq	%r13, 15(%r14)
	movq	-1(%r14), %rax
	testb	$16, 11(%rax)
	jne	.L5591
	movq	0(%r13), %rax
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5592
	movq	8(%r13), %rax
.L5593:
	movq	%rax, 23(%r14)
.L5591:
	movq	0(%r13), %rax
	movq	56(%rax), %rax
	cmpq	%r15, %rax
	jne	.L5610
	movq	16(%r13), %rcx
.L5594:
	testq	%rcx, %rcx
	jne	.L5611
.L5595:
	movq	(%rbx), %rax
	movq	%rax, %rdx
	movq	%rax, -64(%rbp)
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L5596
	movq	40376(%r12), %rsi
	cmpq	40384(%r12), %rsi
	je	.L5597
	movq	%rax, (%rsi)
	addq	$8, 40376(%r12)
.L5598:
	movq	%rbx, %rax
.L5583:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5612
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5609:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory27NewInvalidStringLengthErrorEv
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	jmp	.L5583
	.p2align 4,,10
	.p2align 3
.L5580:
	movq	%rsi, %rdi
	call	*%rax
	movq	%rax, %r14
	jmp	.L5581
	.p2align 4,,10
	.p2align 3
.L5588:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L5613
.L5590:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L5589
	.p2align 4,,10
	.p2align 3
.L5596:
	movq	40400(%r12), %rsi
	cmpq	40408(%r12), %rsi
	je	.L5599
	movq	%rax, (%rsi)
	addq	$8, 40400(%r12)
	jmp	.L5598
	.p2align 4,,10
	.p2align 3
.L5611:
	leaq	37592(%r12), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v88internal4Heap20UpdateExternalStringENS0_6StringEmm@PLT
	jmp	.L5595
	.p2align 4,,10
	.p2align 3
.L5585:
	movq	%r13, %rdi
	call	*%rax
	testb	%al, %al
	jne	.L5587
	leaq	864(%r12), %rsi
	jmp	.L5586
	.p2align 4,,10
	.p2align 3
.L5610:
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %rcx
	jmp	.L5594
	.p2align 4,,10
	.p2align 3
.L5599:
	leaq	-64(%rbp), %rdx
	leaq	40392(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L5598
	.p2align 4,,10
	.p2align 3
.L5597:
	leaq	-64(%rbp), %rdx
	leaq	40368(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal6ObjectESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L5598
	.p2align 4,,10
	.p2align 3
.L5613:
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L5590
	.p2align 4,,10
	.p2align 3
.L5592:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L5593
.L5612:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24092:
	.size	_ZN2v88internal7Factory28NewExternalStringFromOneByteEPKNS_6String29ExternalOneByteStringResourceE, .-_ZN2v88internal7Factory28NewExternalStringFromOneByteEPKNS_6String29ExternalOneByteStringResourceE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal7Factory11CodeBuilderC2EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal7Factory11CodeBuilderC2EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE, @function
_GLOBAL__sub_I__ZN2v88internal7Factory11CodeBuilderC2EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE:
.LFB32805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE32805:
	.size	_GLOBAL__sub_I__ZN2v88internal7Factory11CodeBuilderC2EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE, .-_GLOBAL__sub_I__ZN2v88internal7Factory11CodeBuilderC2EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal7Factory11CodeBuilderC2EPNS0_7IsolateERKNS0_8CodeDescENS0_4Code4KindE
	.weak	_ZTVN2v88internal19SequentialStringKeyIhEE
	.section	.data.rel.ro.local._ZTVN2v88internal19SequentialStringKeyIhEE,"awG",@progbits,_ZTVN2v88internal19SequentialStringKeyIhEE,comdat
	.align 8
	.type	_ZTVN2v88internal19SequentialStringKeyIhEE, @object
	.size	_ZTVN2v88internal19SequentialStringKeyIhEE, 48
_ZTVN2v88internal19SequentialStringKeyIhEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19SequentialStringKeyIhED1Ev
	.quad	_ZN2v88internal19SequentialStringKeyIhED0Ev
	.quad	_ZN2v88internal19SequentialStringKeyIhE8AsHandleEPNS0_7IsolateE
	.quad	_ZN2v88internal19SequentialStringKeyIhE7IsMatchENS0_6StringE
	.weak	_ZTVN2v88internal19SequentialStringKeyItEE
	.section	.data.rel.ro.local._ZTVN2v88internal19SequentialStringKeyItEE,"awG",@progbits,_ZTVN2v88internal19SequentialStringKeyItEE,comdat
	.align 8
	.type	_ZTVN2v88internal19SequentialStringKeyItEE, @object
	.size	_ZTVN2v88internal19SequentialStringKeyItEE, 48
_ZTVN2v88internal19SequentialStringKeyItEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal19SequentialStringKeyItED1Ev
	.quad	_ZN2v88internal19SequentialStringKeyItED0Ev
	.quad	_ZN2v88internal19SequentialStringKeyItE8AsHandleEPNS0_7IsolateE
	.quad	_ZN2v88internal19SequentialStringKeyItE7IsMatchENS0_6StringE
	.weak	_ZTVN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEEE
	.section	.data.rel.ro.local._ZTVN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEEE,"awG",@progbits,_ZTVN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEEE,comdat
	.align 8
	.type	_ZTVN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEEE, @object
	.size	_ZTVN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEEE, 48
_ZTVN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED1Ev
	.quad	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEED0Ev
	.quad	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE8AsHandleEPNS0_7IsolateE
	.quad	_ZN2v88internal15SeqSubStringKeyINS0_16SeqOneByteStringEE7IsMatchENS0_6StringE
	.weak	_ZTVN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEEE
	.section	.data.rel.ro.local._ZTVN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEEE,"awG",@progbits,_ZTVN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEEE,comdat
	.align 8
	.type	_ZTVN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEEE, @object
	.size	_ZTVN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEEE, 48
_ZTVN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED1Ev
	.quad	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEED0Ev
	.quad	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE8AsHandleEPNS0_7IsolateE
	.quad	_ZN2v88internal15SeqSubStringKeyINS0_16SeqTwoByteStringEE7IsMatchENS0_6StringE
	.section	.bss._ZZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEbE29trace_event_unique_atomic3418,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEbE29trace_event_unique_atomic3418, @object
	.size	_ZZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEbE29trace_event_unique_atomic3418, 8
_ZZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEbE29trace_event_unique_atomic3418:
	.zero	8
	.section	.bss._ZZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEbE29trace_event_unique_atomic3414,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEbE29trace_event_unique_atomic3414, @object
	.size	_ZZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEbE29trace_event_unique_atomic3414, 8
_ZZN2v88internal7Factory31NewSharedFunctionInfoForLiteralEPNS0_15FunctionLiteralENS0_6HandleINS0_6ScriptEEEbE29trace_event_unique_atomic3414:
	.zero	8
	.section	.bss._ZZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeEE29trace_event_unique_atomic1684,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeEE29trace_event_unique_atomic1684, @object
	.size	_ZZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeEE29trace_event_unique_atomic1684, 8
_ZZN2v88internal7Factory15NewScriptWithIdENS0_6HandleINS0_6StringEEEiNS0_14AllocationTypeEE29trace_event_unique_atomic1684:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC10:
	.quad	-2251799814209537
	.quad	-2251799814209537
	.align 16
.LC11:
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.value	255
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC20:
	.long	0
	.long	-1042284544
	.align 8
.LC21:
	.long	4290772992
	.long	1105199103
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
