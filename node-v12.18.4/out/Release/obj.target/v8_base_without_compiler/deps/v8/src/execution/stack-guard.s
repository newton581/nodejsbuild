	.file	"stack-guard.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB3897:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE3897:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB3898:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE3898:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB3900:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3900:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8800:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8800:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.text._ZN2v88internal10StackGuard13SetStackLimitEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard13SetStackLimitEm
	.type	_ZN2v88internal10StackGuard13SetStackLimitEm, @function
_ZN2v88internal10StackGuard13SetStackLimitEm:
.LFB11187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	leaq	40976(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movq	24(%rbx), %rax
	cmpq	%rax, 8(%rbx)
	je	.L19
	movq	32(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	je	.L20
.L17:
	movq	%r12, %xmm0
	movq	%r13, %rdi
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	%r12, 24(%rbx)
	movq	32(%rbx), %rax
	cmpq	%rax, 16(%rbx)
	jne	.L17
.L20:
	movq	%r12, 32(%rbx)
	jmp	.L17
	.cfi_endproc
.LFE11187:
	.size	_ZN2v88internal10StackGuard13SetStackLimitEm, .-_ZN2v88internal10StackGuard13SetStackLimitEm
	.section	.text._ZN2v88internal10StackGuard28AdjustStackLimitForSimulatorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard28AdjustStackLimitForSimulatorEv
	.type	_ZN2v88internal10StackGuard28AdjustStackLimitForSimulatorEv, @function
_ZN2v88internal10StackGuard28AdjustStackLimitForSimulatorEv:
.LFB11188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	leaq	40976(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	cmpq	%rax, 8(%rbx)
	je	.L24
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	%rdx, 24(%rbx)
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	.cfi_endproc
.LFE11188:
	.size	_ZN2v88internal10StackGuard28AdjustStackLimitForSimulatorEv, .-_ZN2v88internal10StackGuard28AdjustStackLimitForSimulatorEv
	.section	.text._ZN2v88internal10StackGuard16EnableInterruptsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard16EnableInterruptsEv
	.type	_ZN2v88internal10StackGuard16EnableInterruptsEv, @function
_ZN2v88internal10StackGuard16EnableInterruptsEv:
.LFB11189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	leaq	40976(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	cmpq	$0, 48(%rbx)
	jne	.L28
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	$-2, 24(%rbx)
	movq	%r12, %rdi
	movq	$-2, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	.cfi_endproc
.LFE11189:
	.size	_ZN2v88internal10StackGuard16EnableInterruptsEv, .-_ZN2v88internal10StackGuard16EnableInterruptsEv
	.section	.text._ZN2v88internal10StackGuard17DisableInterruptsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard17DisableInterruptsEv
	.type	_ZN2v88internal10StackGuard17DisableInterruptsEv, @function
_ZN2v88internal10StackGuard17DisableInterruptsEv:
.LFB11190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	leaq	40976(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movq	8(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, 24(%rbx)
	movq	16(%rbx), %rax
	movq	%rax, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	.cfi_endproc
.LFE11190:
	.size	_ZN2v88internal10StackGuard17DisableInterruptsEv, .-_ZN2v88internal10StackGuard17DisableInterruptsEv
	.section	.text._ZN2v88internal10StackGuard19PushInterruptsScopeEPNS0_15InterruptsScopeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard19PushInterruptsScopeEPNS0_15InterruptsScopeE
	.type	_ZN2v88internal10StackGuard19PushInterruptsScopeEPNS0_15InterruptsScopeE, @function
_ZN2v88internal10StackGuard19PushInterruptsScopeEPNS0_15InterruptsScopeE:
.LFB11191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	leaq	40976(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movl	32(%r12), %eax
	testl	%eax, %eax
	jne	.L32
	movq	48(%rbx), %rcx
	andq	16(%r12), %rcx
	movq	%rcx, 24(%r12)
	notq	%rcx
	andq	48(%rbx), %rcx
	movq	%rcx, 48(%rbx)
	testq	%rcx, %rcx
	je	.L40
.L36:
	movq	40(%rbx), %rax
	movq	%r13, %rdi
	movq	%rax, 40(%r12)
	movq	%r12, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	40(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L37
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L35:
	movq	24(%rdx), %rdi
	movq	16(%r12), %rax
	movl	%edi, %esi
	andl	%eax, %esi
	notq	%rax
	andq	%rdi, %rax
	orl	%esi, %ecx
	movq	%rax, 24(%rdx)
	movq	40(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L35
	movslq	%ecx, %rcx
.L34:
	orq	48(%rbx), %rcx
	movq	%rcx, 48(%rbx)
	testq	%rcx, %rcx
	jne	.L36
.L40:
	movq	8(%rbx), %rax
	movq	%rax, 24(%rbx)
	movq	16(%rbx), %rax
	movq	%rax, 32(%rbx)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%ecx, %ecx
	jmp	.L34
	.cfi_endproc
.LFE11191:
	.size	_ZN2v88internal10StackGuard19PushInterruptsScopeEPNS0_15InterruptsScopeE, .-_ZN2v88internal10StackGuard19PushInterruptsScopeEPNS0_15InterruptsScopeE
	.section	.text._ZN2v88internal10StackGuard18PopInterruptsScopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard18PopInterruptsScopeEv
	.type	_ZN2v88internal10StackGuard18PopInterruptsScopeEv, @function
_ZN2v88internal10StackGuard18PopInterruptsScopeEv:
.LFB11192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	leaq	40976(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movq	40(%rbx), %r12
	movl	32(%r12), %eax
	testl	%eax, %eax
	jne	.L42
	movq	48(%rbx), %rax
	orq	24(%r12), %rax
	movq	%rax, 48(%rbx)
.L43:
	testq	%rax, %rax
	jne	.L92
.L60:
	movq	40(%r12), %rax
	movq	%r13, %rdi
	movq	%rax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	40(%r12), %rdi
	movq	48(%rbx), %rax
	testq	%rdi, %rdi
	je	.L43
	testb	$1, %al
	jne	.L93
.L44:
	testb	$2, %al
	jne	.L94
.L46:
	testb	$4, %al
	jne	.L95
.L48:
	testb	$8, %al
	jne	.L96
.L50:
	testb	$16, %al
	jne	.L97
.L52:
	testb	$32, %al
	jne	.L98
.L54:
	testb	$64, %al
	jne	.L99
.L56:
	testb	$-128, %al
	je	.L43
	movq	40(%r12), %rdi
	movl	$128, %esi
	call	_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE@PLT
	testb	%al, %al
	movq	48(%rbx), %rax
	je	.L43
	andb	$127, %al
	movq	%rax, 48(%rbx)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L92:
	movq	$-2, 24(%rbx)
	movq	$-2, 32(%rbx)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L99:
	movq	40(%r12), %rdi
	movl	$64, %esi
	call	_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE@PLT
	testb	%al, %al
	movq	48(%rbx), %rax
	je	.L56
	andq	$-65, %rax
	movq	%rax, 48(%rbx)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L98:
	movq	40(%r12), %rdi
	movl	$32, %esi
	call	_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE@PLT
	testb	%al, %al
	movq	48(%rbx), %rax
	je	.L54
	andq	$-33, %rax
	movq	%rax, 48(%rbx)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L97:
	movq	40(%r12), %rdi
	movl	$16, %esi
	call	_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE@PLT
	testb	%al, %al
	movq	48(%rbx), %rax
	je	.L52
	andq	$-17, %rax
	movq	%rax, 48(%rbx)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L96:
	movq	40(%r12), %rdi
	movl	$8, %esi
	call	_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE@PLT
	testb	%al, %al
	movq	48(%rbx), %rax
	je	.L50
	andq	$-9, %rax
	movq	%rax, 48(%rbx)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L95:
	movq	40(%r12), %rdi
	movl	$4, %esi
	call	_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE@PLT
	testb	%al, %al
	movq	48(%rbx), %rax
	je	.L48
	andq	$-5, %rax
	movq	%rax, 48(%rbx)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L94:
	movq	40(%r12), %rdi
	movl	$2, %esi
	call	_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE@PLT
	testb	%al, %al
	movq	48(%rbx), %rax
	je	.L46
	andq	$-3, %rax
	movq	%rax, 48(%rbx)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$1, %esi
	call	_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE@PLT
	testb	%al, %al
	movq	48(%rbx), %rax
	je	.L44
	andq	$-2, %rax
	movq	%rax, 48(%rbx)
	jmp	.L44
	.cfi_endproc
.LFE11192:
	.size	_ZN2v88internal10StackGuard18PopInterruptsScopeEv, .-_ZN2v88internal10StackGuard18PopInterruptsScopeEv
	.section	.text._ZN2v88internal10StackGuard14CheckInterruptENS1_13InterruptFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard14CheckInterruptENS1_13InterruptFlagE
	.type	_ZN2v88internal10StackGuard14CheckInterruptENS1_13InterruptFlagE, @function
_ZN2v88internal10StackGuard14CheckInterruptENS1_13InterruptFlagE:
.LFB11193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movslq	%esi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	leaq	40976(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	testq	%rbx, 48(%r13)
	movq	%r12, %rdi
	setne	%r13b
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11193:
	.size	_ZN2v88internal10StackGuard14CheckInterruptENS1_13InterruptFlagE, .-_ZN2v88internal10StackGuard14CheckInterruptENS1_13InterruptFlagE
	.section	.text._ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE
	.type	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE, @function
_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE:
.LFB11194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movslq	%esi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	leaq	40976(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L103
	movl	%r12d, %esi
	call	_ZN2v88internal15InterruptsScope9InterceptENS0_10StackGuard13InterruptFlagE@PLT
	testb	%al, %al
	jne	.L111
.L103:
	orq	%r12, 48(%rbx)
	movq	$-2, 24(%rbx)
	movq	$-2, 32(%rbx)
	movq	(%rbx), %rdi
	addq	$45552, %rdi
	call	_ZN2v88internal17FutexWaitListNode10NotifyWakeEv@PLT
.L111:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	.cfi_endproc
.LFE11194:
	.size	_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE, .-_ZN2v88internal10StackGuard16RequestInterruptENS1_13InterruptFlagE
	.section	.text._ZN2v88internal10StackGuard14ClearInterruptENS1_13InterruptFlagE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard14ClearInterruptENS1_13InterruptFlagE
	.type	_ZN2v88internal10StackGuard14ClearInterruptENS1_13InterruptFlagE, @function
_ZN2v88internal10StackGuard14ClearInterruptENS1_13InterruptFlagE:
.LFB11195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	notl	%ebx
	subq	$8, %rsp
	movq	(%rdi), %rax
	leaq	40976(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movq	40(%r12), %rax
	movslq	%ebx, %rdx
	testq	%rax, %rax
	je	.L116
	.p2align 4,,10
	.p2align 3
.L113:
	andq	%rdx, 24(%rax)
	movq	40(%rax), %rax
	testq	%rax, %rax
	jne	.L113
.L116:
	andq	%rdx, 48(%r12)
	je	.L122
.L115:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	8(%r12), %rax
	movq	%rax, 24(%r12)
	movq	16(%r12), %rax
	movq	%rax, 32(%r12)
	jmp	.L115
	.cfi_endproc
.LFE11195:
	.size	_ZN2v88internal10StackGuard14ClearInterruptENS1_13InterruptFlagE, .-_ZN2v88internal10StackGuard14ClearInterruptENS1_13InterruptFlagE
	.section	.text._ZN2v88internal10StackGuard23FetchAndClearInterruptsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard23FetchAndClearInterruptsEv
	.type	_ZN2v88internal10StackGuard23FetchAndClearInterruptsEv, @function
_ZN2v88internal10StackGuard23FetchAndClearInterruptsEv:
.LFB11196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	leaq	40976(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movq	48(%rbx), %rax
	testb	$1, %al
	je	.L124
	andq	$-2, %rax
	movl	$1, %r13d
	movq	%rax, 48(%rbx)
	je	.L128
.L125:
	movq	%r12, %rdi
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	movq	$0, 48(%rbx)
	movl	%eax, %r13d
.L128:
	movq	8(%rbx), %rax
	movq	%rax, 24(%rbx)
	movq	16(%rbx), %rax
	movq	%rax, 32(%rbx)
	jmp	.L125
	.cfi_endproc
.LFE11196:
	.size	_ZN2v88internal10StackGuard23FetchAndClearInterruptsEv, .-_ZN2v88internal10StackGuard23FetchAndClearInterruptsEv
	.section	.text._ZN2v88internal10StackGuard17ArchiveStackGuardEPc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard17ArchiveStackGuardEPc
	.type	_ZN2v88internal10StackGuard17ArchiveStackGuardEPc, @function
_ZN2v88internal10StackGuard17ArchiveStackGuardEPc:
.LFB11197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	leaq	40976(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movdqu	8(%rbx), %xmm1
	movdqa	.LC0(%rip), %xmm0
	movq	%r13, %rdi
	movups	%xmm1, (%r12)
	movdqu	24(%rbx), %xmm2
	movups	%xmm2, 16(%r12)
	movdqu	40(%rbx), %xmm3
	movups	%xmm3, 32(%r12)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movups	%xmm0, 8(%rbx)
	movups	%xmm0, 24(%rbx)
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	addq	$8, %rsp
	leaq	48(%r12), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11197:
	.size	_ZN2v88internal10StackGuard17ArchiveStackGuardEPc, .-_ZN2v88internal10StackGuard17ArchiveStackGuardEPc
	.section	.text._ZN2v88internal10StackGuard17RestoreStackGuardEPc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard17RestoreStackGuardEPc
	.type	_ZN2v88internal10StackGuard17RestoreStackGuardEPc, @function
_ZN2v88internal10StackGuard17RestoreStackGuardEPc:
.LFB11198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	leaq	40976(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movdqu	(%rbx), %xmm0
	movq	%r13, %rdi
	movups	%xmm0, 8(%r12)
	movdqu	16(%rbx), %xmm1
	movups	%xmm1, 24(%r12)
	movdqu	32(%rbx), %xmm2
	movups	%xmm2, 40(%r12)
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	addq	$8, %rsp
	leaq	48(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11198:
	.size	_ZN2v88internal10StackGuard17RestoreStackGuardEPc, .-_ZN2v88internal10StackGuard17RestoreStackGuardEPc
	.section	.text._ZN2v88internal10StackGuard19FreeThreadResourcesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard19FreeThreadResourcesEv
	.type	_ZN2v88internal10StackGuard19FreeThreadResourcesEv, @function
_ZN2v88internal10StackGuard19FreeThreadResourcesEv:
.LFB11199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	_ZN2v88internal7Isolate40FindOrAllocatePerThreadDataForThisThreadEv@PLT
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11199:
	.size	_ZN2v88internal10StackGuard19FreeThreadResourcesEv, .-_ZN2v88internal10StackGuard19FreeThreadResourcesEv
	.section	.text._ZN2v88internal10StackGuard11ThreadLocal10InitializeEPNS0_7IsolateERKNS0_15ExecutionAccessE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard11ThreadLocal10InitializeEPNS0_7IsolateERKNS0_15ExecutionAccessE
	.type	_ZN2v88internal10StackGuard11ThreadLocal10InitializeEPNS0_7IsolateERKNS0_15ExecutionAccessE, @function
_ZN2v88internal10StackGuard11ThreadLocal10InitializeEPNS0_7IsolateERKNS0_15ExecutionAccessE:
.LFB11200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	_ZN2v88internal15FLAG_stack_sizeE(%rip), %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	sall	$10, %r12d
	movslq	%r12d, %r12
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	subq	%r12, %rax
	movq	%rax, (%rbx)
	movq	%rax, 16(%rbx)
	movq	%rax, 8(%rbx)
	movq	%rax, 24(%rbx)
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11200:
	.size	_ZN2v88internal10StackGuard11ThreadLocal10InitializeEPNS0_7IsolateERKNS0_15ExecutionAccessE, .-_ZN2v88internal10StackGuard11ThreadLocal10InitializeEPNS0_7IsolateERKNS0_15ExecutionAccessE
	.section	.text._ZN2v88internal10StackGuard10InitThreadERKNS0_15ExecutionAccessE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard10InitThreadERKNS0_15ExecutionAccessE
	.type	_ZN2v88internal10StackGuard10InitThreadERKNS0_15ExecutionAccessE, @function
_ZN2v88internal10StackGuard10InitThreadERKNS0_15ExecutionAccessE:
.LFB11201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	_ZN2v88internal15FLAG_stack_sizeE(%rip), %r12d
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	sall	$10, %r12d
	movslq	%r12d, %r12
	subq	%r12, %rax
	movq	%rax, 8(%rbx)
	movq	%rax, 24(%rbx)
	movq	%rax, 16(%rbx)
	movq	%rax, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate40FindOrAllocatePerThreadDataForThisThreadEv@PLT
	movq	16(%rax), %r12
	testq	%r12, %r12
	jne	.L142
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movq	(%rbx), %rax
	leaq	40976(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movq	24(%rbx), %rax
	cmpq	8(%rbx), %rax
	je	.L143
.L139:
	movq	32(%rbx), %rax
	cmpq	16(%rbx), %rax
	je	.L144
.L140:
	movq	%r12, %xmm0
	movq	%r13, %rdi
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	%r12, 24(%rbx)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L144:
	movq	%r12, 32(%rbx)
	jmp	.L140
	.cfi_endproc
.LFE11201:
	.size	_ZN2v88internal10StackGuard10InitThreadERKNS0_15ExecutionAccessE, .-_ZN2v88internal10StackGuard10InitThreadERKNS0_15ExecutionAccessE
	.section	.rodata._ZN2v88internal10StackGuard16HandleInterruptsEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"v8.execute"
.LC2:
	.string	"V8.HandleInterrupts"
.LC3:
	.string	"V8.TerminateExecution"
.LC4:
	.string	"disabled-by-default-v8.gc"
.LC5:
	.string	"V8.GCHandleGCRequest"
.LC6:
	.string	"disabled-by-default-v8.wasm"
.LC7:
	.string	"V8.WasmGrowSharedMemory"
	.section	.rodata._ZN2v88internal10StackGuard16HandleInterruptsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"V8.GCDeoptMarkedAllocationSites"
	.align 8
.LC9:
	.string	"disabled-by-default-v8.compile"
	.section	.rodata._ZN2v88internal10StackGuard16HandleInterruptsEv.str1.1
.LC10:
	.string	"V8.InstallOptimizedFunctions"
	.section	.rodata._ZN2v88internal10StackGuard16HandleInterruptsEv.str1.8
	.align 8
.LC11:
	.string	"V8.InvokeApiInterruptCallbacks"
	.section	.rodata._ZN2v88internal10StackGuard16HandleInterruptsEv.str1.1
.LC12:
	.string	"LogCode"
.LC13:
	.string	"WasmCodeGC"
	.section	.text._ZN2v88internal10StackGuard16HandleInterruptsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackGuard16HandleInterruptsEv
	.type	_ZN2v88internal10StackGuard16HandleInterruptsEv, @function
_ZN2v88internal10StackGuard16HandleInterruptsEv:
.LFB11206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic247(%rip), %r12
	testq	%r12, %r12
	je	.L358
.L147:
	movq	$0, -128(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L359
.L149:
	movq	(%rbx), %rax
	leaq	40976(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movq	48(%rbx), %r12
	testb	$1, %r12b
	je	.L153
	andq	$-2, %r12
	movq	%r12, 48(%rbx)
	je	.L360
.L154:
	movq	%r13, %rdi
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	movq	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic262(%rip), %r12
	testq	%r12, %r12
	je	.L361
.L157:
	movq	$0, -96(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L362
.L159:
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate18TerminateExecutionEv@PLT
	leaq	-96(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
.L163:
	leaq	-128(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L363
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	$0, 48(%rbx)
	movq	%r13, %rdi
	movq	%rax, 24(%rbx)
	movq	16(%rbx), %rax
	movq	%rax, 32(%rbx)
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	testb	$2, %r12b
	jne	.L364
	testb	$32, %r12b
	jne	.L365
.L172:
	testb	$16, %r12b
	jne	.L366
.L180:
	testb	$4, %r12b
	jne	.L367
.L188:
	testb	$8, %r12b
	jne	.L368
.L196:
	testb	$64, %r12b
	jne	.L369
.L204:
	andl	$128, %r12d
	jne	.L370
.L212:
	movq	(%rbx), %rax
	movq	40960(%rax), %r12
	cmpb	$0, 7168(%r12)
	je	.L220
	movq	7160(%r12), %rax
.L221:
	testq	%rax, %rax
	je	.L222
	addl	$1, (%rax)
.L222:
	movq	(%rbx), %rax
	movq	40960(%rax), %r12
	cmpb	$0, 7200(%r12)
	je	.L223
	movq	7192(%r12), %rax
.L224:
	testq	%rax, %rax
	je	.L225
	addl	$1, (%rax)
.L225:
	movq	(%rbx), %rax
	movq	40944(%rax), %rdi
	call	_ZN2v88internal15RuntimeProfiler29MarkCandidatesForOptimizationEv@PLT
	movq	(%rbx), %rax
	movq	88(%rax), %r12
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L220:
	movb	$1, 7168(%r12)
	leaq	7144(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7160(%r12)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L360:
	movq	8(%rbx), %rax
	movq	%rax, 24(%rbx)
	movq	16(%rbx), %rax
	movq	%rax, 32(%rbx)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L370:
	movq	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic303(%rip), %r12
	testq	%r12, %r12
	je	.L371
.L214:
	movq	$0, -96(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L372
.L216:
	movq	(%rbx), %rsi
	movq	45752(%rsi), %rdi
	call	_ZN2v88internal4wasm10WasmEngine28ReportLiveCodeFromStackForGCEPNS0_7IsolateE@PLT
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L369:
	movq	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic298(%rip), %r13
	testq	%r13, %r13
	je	.L373
.L206:
	movq	$0, -96(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L374
.L208:
	movq	(%rbx), %rsi
	movq	45752(%rsi), %rdi
	call	_ZN2v88internal4wasm10WasmEngine29LogOutstandingCodesForIsolateEPNS0_7IsolateE@PLT
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	andl	$128, %r12d
	je	.L212
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L367:
	movq	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic285(%rip), %r13
	testq	%r13, %r13
	je	.L375
.L190:
	movq	$0, -96(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L376
.L192:
	movq	(%rbx), %rax
	movq	45416(%rax), %rdi
	call	_ZN2v88internal27OptimizingCompileDispatcher25InstallOptimizedFunctionsEv@PLT
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	testb	$8, %r12b
	je	.L196
.L368:
	movq	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic292(%rip), %r13
	testq	%r13, %r13
	je	.L377
.L198:
	movq	$0, -96(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L378
.L200:
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Isolate27InvokeApiInterruptCallbacksEv@PLT
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	testb	$64, %r12b
	je	.L204
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L365:
	movq	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic272(%rip), %r13
	testq	%r13, %r13
	je	.L379
.L174:
	movq	$0, -96(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L380
.L176:
	movq	(%rbx), %rsi
	movq	45752(%rsi), %rdi
	call	_ZN2v88internal4wasm17WasmMemoryTracker27UpdateSharedMemoryInstancesEPNS0_7IsolateE@PLT
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	testb	$16, %r12b
	je	.L180
.L366:
	movq	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic279(%rip), %r13
	testq	%r13, %r13
	je	.L381
.L182:
	movq	$0, -96(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L382
.L184:
	movq	(%rbx), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap26DeoptMarkedAllocationSitesEv@PLT
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	testb	$4, %r12b
	je	.L188
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L364:
	movq	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic267(%rip), %r13
	testq	%r13, %r13
	je	.L383
.L166:
	movq	$0, -96(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L384
.L168:
	movq	(%rbx), %rax
	leaq	37592(%rax), %rdi
	call	_ZN2v88internal4Heap15HandleGCRequestEv@PLT
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	testb	$32, %r12b
	je	.L172
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L358:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L385
.L148:
	movq	%r12, _ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic247(%rip)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L359:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L386
.L150:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L151
	movq	(%rdi), %rax
	call	*8(%rax)
.L151:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L152
	movq	(%rdi), %rax
	call	*8(%rax)
.L152:
	leaq	.LC2(%rip), %rax
	movq	%r12, -120(%rbp)
	movq	%rax, -112(%rbp)
	leaq	-120(%rbp), %rax
	movq	%r13, -104(%rbp)
	movq	%rax, -128(%rbp)
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L223:
	movb	$1, 7200(%r12)
	leaq	7176(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 7192(%r12)
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L382:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L387
.L185:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L186
	movq	(%rdi), %rax
	call	*8(%rax)
.L186:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L187
	movq	(%rdi), %rax
	call	*8(%rax)
.L187:
	leaq	.LC8(%rip), %rax
	movq	%r13, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r14, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L378:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L388
.L201:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L202
	movq	(%rdi), %rax
	call	*8(%rax)
.L202:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L203
	movq	(%rdi), %rax
	call	*8(%rax)
.L203:
	leaq	.LC11(%rip), %rax
	movq	%r13, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r14, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L380:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L389
.L177:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L178
	movq	(%rdi), %rax
	call	*8(%rax)
.L178:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L179
	movq	(%rdi), %rax
	call	*8(%rax)
.L179:
	leaq	.LC7(%rip), %rax
	movq	%r13, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r14, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L374:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L390
.L209:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L210
	movq	(%rdi), %rax
	call	*8(%rax)
.L210:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L211
	movq	(%rdi), %rax
	call	*8(%rax)
.L211:
	leaq	.LC12(%rip), %rax
	movq	%r13, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r14, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L376:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L391
.L193:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L194
	movq	(%rdi), %rax
	call	*8(%rax)
.L194:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L195
	movq	(%rdi), %rax
	call	*8(%rax)
.L195:
	leaq	.LC10(%rip), %rax
	movq	%r13, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r14, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L372:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L392
.L217:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L218
	movq	(%rdi), %rax
	call	*8(%rax)
.L218:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L219
	movq	(%rdi), %rax
	call	*8(%rax)
.L219:
	leaq	.LC13(%rip), %rax
	movq	%r12, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r13, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L384:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L393
.L169:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L170
	movq	(%rdi), %rax
	call	*8(%rax)
.L170:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L171
	movq	(%rdi), %rax
	call	*8(%rax)
.L171:
	leaq	.LC5(%rip), %rax
	movq	%r13, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r14, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L383:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L394
.L167:
	movq	%r13, _ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic267(%rip)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L381:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L395
.L183:
	movq	%r13, _ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic279(%rip)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L379:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L396
.L175:
	movq	%r13, _ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic272(%rip)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L377:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L397
.L199:
	movq	%r13, _ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic292(%rip)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L375:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L398
.L191:
	movq	%r13, _ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic285(%rip)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L373:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L399
.L207:
	movq	%r13, _ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic298(%rip)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L371:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L400
.L215:
	movq	%r12, _ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic303(%rip)
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L362:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L401
.L160:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L161
	movq	(%rdi), %rax
	call	*8(%rax)
.L161:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L162
	movq	(%rdi), %rax
	call	*8(%rax)
.L162:
	leaq	.LC3(%rip), %rax
	movq	%r12, -88(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%r13, -72(%rbp)
	movq	%rax, -96(%rbp)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L361:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L402
.L158:
	movq	%r12, _ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic262(%rip)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L386:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L385:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L401:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC3(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L402:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L388:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L387:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L392:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L391:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L390:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L389:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L400:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L399:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L398:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L397:
	leaq	.LC1(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L396:
	leaq	.LC6(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L395:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L394:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L393:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L169
.L363:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11206:
	.size	_ZN2v88internal10StackGuard16HandleInterruptsEv, .-_ZN2v88internal10StackGuard16HandleInterruptsEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10StackGuard13SetStackLimitEm,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10StackGuard13SetStackLimitEm, @function
_GLOBAL__sub_I__ZN2v88internal10StackGuard13SetStackLimitEm:
.LFB13120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13120:
	.size	_GLOBAL__sub_I__ZN2v88internal10StackGuard13SetStackLimitEm, .-_GLOBAL__sub_I__ZN2v88internal10StackGuard13SetStackLimitEm
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10StackGuard13SetStackLimitEm
	.section	.bss._ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic303,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic303, @object
	.size	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic303, 8
_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic303:
	.zero	8
	.section	.bss._ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic298,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic298, @object
	.size	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic298, 8
_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic298:
	.zero	8
	.section	.bss._ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic292,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic292, @object
	.size	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic292, 8
_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic292:
	.zero	8
	.section	.bss._ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic285,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic285, @object
	.size	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic285, 8
_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic285:
	.zero	8
	.section	.bss._ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic279,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic279, @object
	.size	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic279, 8
_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic279:
	.zero	8
	.section	.bss._ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic272,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic272, @object
	.size	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic272, 8
_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic272:
	.zero	8
	.section	.bss._ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic267,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic267, @object
	.size	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic267, 8
_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic267:
	.zero	8
	.section	.bss._ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic262,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic262, @object
	.size	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic262, 8
_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic262:
	.zero	8
	.section	.bss._ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic247,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic247, @object
	.size	_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic247, 8
_ZZN2v88internal10StackGuard16HandleInterruptsEvE28trace_event_unique_atomic247:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	-8
	.quad	-8
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
