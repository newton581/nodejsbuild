	.file	"wasm-translation.cc"
	.text
	.section	.text._ZN12v8_inspector15WasmTranslation14TranslatorImpl8LessThanERKN2v85debug31WasmDisassemblyOffsetTableEntryERKNS1_13TransLocationE,"axG",@progbits,_ZN12v8_inspector15WasmTranslation14TranslatorImpl8LessThanERKN2v85debug31WasmDisassemblyOffsetTableEntryERKNS1_13TransLocationE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector15WasmTranslation14TranslatorImpl8LessThanERKN2v85debug31WasmDisassemblyOffsetTableEntryERKNS1_13TransLocationE
	.type	_ZN12v8_inspector15WasmTranslation14TranslatorImpl8LessThanERKN2v85debug31WasmDisassemblyOffsetTableEntryERKNS1_13TransLocationE, @function
_ZN12v8_inspector15WasmTranslation14TranslatorImpl8LessThanERKN2v85debug31WasmDisassemblyOffsetTableEntryERKNS1_13TransLocationE:
.LFB7698:
	.cfi_startproc
	endbr64
	movl	48(%rsi), %edx
	movl	$1, %eax
	cmpl	%edx, 4(%rdi)
	jl	.L1
	movl	$0, %eax
	je	.L6
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	52(%rsi), %eax
	cmpl	%eax, 8(%rdi)
	setl	%al
	ret
	.cfi_endproc
.LFE7698:
	.size	_ZN12v8_inspector15WasmTranslation14TranslatorImpl8LessThanERKN2v85debug31WasmDisassemblyOffsetTableEntryERKNS1_13TransLocationE, .-_ZN12v8_inspector15WasmTranslation14TranslatorImpl8LessThanERKN2v85debug31WasmDisassemblyOffsetTableEntryERKNS1_13TransLocationE
	.section	.text._ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS7_EEELb0EEEEE18_M_deallocate_nodeEPSC_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS7_EEELb0EEEEE18_M_deallocate_nodeEPSC_.isra.0, @function
_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS7_EEELb0EEEEE18_M_deallocate_nodeEPSC_.isra.0:
.LFB13549:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %r13
	testq	%r13, %r13
	je	.L8
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L14
	.p2align 4,,10
	.p2align 3
.L9:
	movq	16(%r13), %rax
	movq	8(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	8(%r13), %rdi
	leaq	56(%r13), %rax
	movq	$0, 32(%r13)
	movq	$0, 24(%r13)
	cmpq	%rax, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L16:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L8:
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L9
.L13:
	movq	%rbx, %r12
.L14:
	movq	88(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L11
	call	_ZdlPv@PLT
.L11:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L39
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L13
	jmp	.L9
	.cfi_endproc
.LFE13549:
	.size	_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS7_EEELb0EEEEE18_M_deallocate_nodeEPSC_.isra.0, .-_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS7_EEELb0EEEEE18_M_deallocate_nodeEPSC_.isra.0
	.section	.text._ZN12v8_inspector15WasmTranslationC2EPN2v87IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15WasmTranslationC2EPN2v87IsolateE
	.type	_ZN12v8_inspector15WasmTranslationC2EPN2v87IsolateE, @function
_ZN12v8_inspector15WasmTranslationC2EPN2v87IsolateE:
.LFB7784:
	.cfi_startproc
	endbr64
	movss	.LC0(%rip), %xmm0
	leaq	56(%rdi), %rax
	movq	%rsi, (%rdi)
	movq	%rax, 8(%rdi)
	leaq	112(%rdi), %rax
	movq	$1, 16(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movq	%rax, 64(%rdi)
	movq	$1, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 104(%rdi)
	movq	$0, 112(%rdi)
	movss	%xmm0, 40(%rdi)
	movss	%xmm0, 96(%rdi)
	ret
	.cfi_endproc
.LFE7784:
	.size	_ZN12v8_inspector15WasmTranslationC2EPN2v87IsolateE, .-_ZN12v8_inspector15WasmTranslationC2EPN2v87IsolateE
	.globl	_ZN12v8_inspector15WasmTranslationC1EPN2v87IsolateE
	.set	_ZN12v8_inspector15WasmTranslationC1EPN2v87IsolateE,_ZN12v8_inspector15WasmTranslationC2EPN2v87IsolateE
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5clearEv,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5clearEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5clearEv
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5clearEv, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5clearEv:
.LFB10334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	%rdi, -56(%rbp)
	testq	%r13, %r13
	je	.L42
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%r13, %r14
	movq	0(%r13), %r13
	movq	16(%r14), %r12
	testq	%r12, %r12
	je	.L43
	movq	24(%r12), %r15
	testq	%r15, %r15
	jne	.L49
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L79:
	call	_ZdlPv@PLT
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L44
.L48:
	movq	%rbx, %r15
.L49:
	movq	88(%r15), %rdi
	movq	(%r15), %rbx
	testq	%rdi, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	movq	64(%r15), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movq	16(%r15), %rdi
	leaq	32(%r15), %rax
	cmpq	%rax, %rdi
	jne	.L79
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L48
.L44:
	movq	16(%r12), %rax
	movq	8(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	8(%r12), %rdi
	leaq	56(%r12), %rax
	movq	$0, 32(%r12)
	movq	$0, 24(%r12)
	cmpq	%rax, %rdi
	je	.L50
	call	_ZdlPv@PLT
.L50:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L51
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L51:
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L43:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L52
.L42:
	movq	-56(%rbp), %rbx
	xorl	%esi, %esi
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdi
	leaq	0(,%rcx,8), %rdx
	movq	%rcx, -56(%rbp)
	call	memset@PLT
	movq	$0, 24(%rbx)
	movq	$0, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10334:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5clearEv, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5clearEv
	.section	.text._ZN12v8_inspector15WasmTranslation5ClearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15WasmTranslation5ClearEv
	.type	_ZN12v8_inspector15WasmTranslation5ClearEv, @function
_ZN12v8_inspector15WasmTranslation5ClearEv:
.LFB7827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$8, %rsp
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5clearEv
	movq	80(%rbx), %r12
	testq	%r12, %r12
	jne	.L84
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L95:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L81
.L83:
	movq	%r13, %r12
.L84:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	(%r12), %r13
	cmpq	%rax, %rdi
	jne	.L95
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L83
.L81:
	movq	72(%rbx), %rax
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, 88(%rbx)
	movq	$0, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7827:
	.size	_ZN12v8_inspector15WasmTranslation5ClearEv, .-_ZN12v8_inspector15WasmTranslation5ClearEv
	.section	.text._ZN12v8_inspector15WasmTranslationD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15WasmTranslationD2Ev
	.type	_ZN12v8_inspector15WasmTranslationD2Ev, @function
_ZN12v8_inspector15WasmTranslationD2Ev:
.LFB7787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	8(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5clearEv
	movq	80(%rbx), %r12
	testq	%r12, %r12
	jne	.L100
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L113:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L97
.L99:
	movq	%r14, %r12
.L100:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	(%r12), %r14
	cmpq	%rax, %rdi
	jne	.L113
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L99
.L97:
	movq	72(%rbx), %rax
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%rbx), %rax
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	64(%rbx), %rdi
	leaq	112(%rbx), %rax
	movq	$0, 88(%rbx)
	movq	$0, 80(%rbx)
	cmpq	%rax, %rdi
	je	.L101
	call	_ZdlPv@PLT
.L101:
	movq	%r13, %rdi
	addq	$56, %rbx
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE5clearEv
	movq	-48(%rbx), %rdi
	cmpq	%rbx, %rdi
	je	.L96
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7787:
	.size	_ZN12v8_inspector15WasmTranslationD2Ev, .-_ZN12v8_inspector15WasmTranslationD2Ev
	.globl	_ZN12v8_inspector15WasmTranslationD1Ev
	.set	_ZN12v8_inspector15WasmTranslationD1Ev,_ZN12v8_inspector15WasmTranslationD2Ev
	.section	.text._ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKiSt6vectorIiSaIiEEEENS0_5__ops16_Iter_equals_valIS2_EEET_SB_SB_T0_St26random_access_iterator_tag,"axG",@progbits,_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKiSt6vectorIiSaIiEEEENS0_5__ops16_Iter_equals_valIS2_EEET_SB_SB_T0_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKiSt6vectorIiSaIiEEEENS0_5__ops16_Iter_equals_valIS2_EEET_SB_SB_T0_St26random_access_iterator_tag
	.type	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKiSt6vectorIiSaIiEEEENS0_5__ops16_Iter_equals_valIS2_EEET_SB_SB_T0_St26random_access_iterator_tag, @function
_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKiSt6vectorIiSaIiEEEENS0_5__ops16_Iter_equals_valIS2_EEET_SB_SB_T0_St26random_access_iterator_tag:
.LFB11881:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	subq	%rdi, %r8
	movq	%r8, %rax
	sarq	$4, %r8
	sarq	$2, %rax
	testq	%r8, %r8
	jle	.L115
	salq	$4, %r8
	movl	(%rdx), %ecx
	addq	%rdi, %r8
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L116:
	cmpl	4(%rdi), %ecx
	je	.L133
	cmpl	8(%rdi), %ecx
	je	.L134
	cmpl	12(%rdi), %ecx
	je	.L135
	addq	$16, %rdi
	cmpq	%r8, %rdi
	je	.L136
.L121:
	cmpl	(%rdi), %ecx
	jne	.L116
.L132:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$2, %rax
.L115:
	cmpq	$2, %rax
	je	.L122
	cmpq	$3, %rax
	je	.L123
	cmpq	$1, %rax
	je	.L137
.L125:
	movq	%rsi, %rax
	ret
.L123:
	movl	(%rdx), %eax
	cmpl	%eax, (%rdi)
	je	.L132
	addq	$4, %rdi
	jmp	.L126
.L137:
	movl	(%rdx), %eax
.L127:
	cmpl	(%rdi), %eax
	jne	.L125
	jmp	.L132
.L122:
	movl	(%rdx), %eax
.L126:
	cmpl	%eax, (%rdi)
	je	.L132
	addq	$4, %rdi
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L133:
	leaq	4(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	leaq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	leaq	12(%rdi), %rax
	ret
	.cfi_endproc
.LFE11881:
	.size	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKiSt6vectorIiSaIiEEEENS0_5__ops16_Iter_equals_valIS2_EEET_SB_SB_T0_St26random_access_iterator_tag, .-_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKiSt6vectorIiSaIiEEEENS0_5__ops16_Iter_equals_valIS2_EEET_SB_SB_T0_St26random_access_iterator_tag
	.section	.text._ZN12v8_inspector15WasmTranslation5ClearEPN2v87IsolateERKSt6vectorIiSaIiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15WasmTranslation5ClearEPN2v87IsolateERKSt6vectorIiSaIiEE
	.type	_ZN12v8_inspector15WasmTranslation5ClearEPN2v87IsolateERKSt6vectorIiSaIiEE, @function
_ZN12v8_inspector15WasmTranslation5ClearEPN2v87IsolateERKSt6vectorIiSaIiEE:
.LFB7828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	80(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L152
	.p2align 4,,10
	.p2align 3
.L139:
	movq	48(%r12), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L142
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L142:
	call	_ZNK2v85debug6Script9ContextIdEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rdx
	sarq	$32, %rdx
	testb	%al, %al
	movl	$0, %eax
	cmove	%eax, %edx
	movl	%edx, -88(%rbp)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	-88(%rbp), %edx
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	movl	%edx, -80(%rbp)
	movq	%r13, %rdx
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKiSt6vectorIiSaIiEEEENS0_5__ops16_Iter_equals_valIS2_EEET_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 8(%rbx)
	je	.L144
	movq	72(%r14), %rdi
	movq	56(%r12), %rax
	xorl	%edx, %edx
	movq	64(%r14), %r9
	divq	%rdi
	leaq	0(,%rdx,8), %r10
	movq	%rdx, %r8
	leaq	(%r9,%r10), %r11
	movq	(%r11), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%rdx, %rsi
	movq	(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L145
	movq	(%r12), %rcx
	cmpq	%rsi, %rax
	je	.L189
	testq	%rcx, %rcx
	je	.L148
	movq	56(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r8
	je	.L148
	movq	%rsi, (%r9,%rdx,8)
	movq	(%r12), %rcx
.L148:
	movq	%rcx, (%rsi)
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L150
	movq	%rcx, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %rcx
.L150:
	movq	%r12, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZdlPv@PLT
	subq	$1, 88(%r14)
	movq	-88(%rbp), %rcx
	movq	%rcx, %r12
.L151:
	testq	%r12, %r12
	jne	.L139
.L152:
	movq	24(%r14), %r12
	leaq	-80(%rbp), %r13
	testq	%r12, %r12
	je	.L138
	.p2align 4,,10
	.p2align 3
.L140:
	movq	16(%r12), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-88(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L153
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L153:
	call	_ZNK2v85debug6Script9ContextIdEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rdx
	sarq	$32, %rdx
	testb	%al, %al
	movl	$0, %eax
	cmove	%eax, %edx
	movl	%edx, -88(%rbp)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	-88(%rbp), %edx
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	movl	%edx, -80(%rbp)
	movq	%r13, %rdx
	call	_ZSt9__find_ifIN9__gnu_cxx17__normal_iteratorIPKiSt6vectorIiSaIiEEEENS0_5__ops16_Iter_equals_valIS2_EEET_SB_SB_T0_St26random_access_iterator_tag
	cmpq	%rax, 8(%rbx)
	je	.L155
	movq	16(%r14), %r8
	movslq	8(%r12), %rax
	xorl	%edx, %edx
	movq	8(%r14), %r10
	divq	%r8
	leaq	0(,%rdx,8), %r11
	movq	%rdx, %r9
	leaq	(%r10,%r11), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%rdi, %rsi
	movq	(%rdi), %rdi
	cmpq	%r12, %rdi
	jne	.L156
	movq	(%r12), %rcx
	cmpq	%rsi, %rax
	je	.L190
	testq	%rcx, %rcx
	je	.L191
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r9
	je	.L159
	movq	%rsi, (%r10,%rdx,8)
	movq	(%r12), %rcx
.L159:
	movq	%rcx, (%rsi)
	movq	%r12, %rdi
	movq	%rcx, -88(%rbp)
	call	_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS7_EEELb0EEEEE18_M_deallocate_nodeEPSC_.isra.0
	subq	$1, 32(%r14)
	movq	-88(%rbp), %rcx
	movq	%rcx, %r12
.L162:
	testq	%r12, %r12
	jne	.L140
	.p2align 4,,10
	.p2align 3
.L138:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movq	(%r12), %r12
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L144:
	movq	(%r12), %r12
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L189:
	testq	%rcx, %rcx
	je	.L165
	movq	56(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r8
	je	.L148
	movq	%rsi, (%r9,%rdx,8)
	movq	64(%r14), %r11
	leaq	80(%r14), %rdx
	addq	%r10, %r11
	movq	(%r11), %rax
	cmpq	%rdx, %rax
	je	.L193
.L149:
	movq	$0, (%r11)
	movq	(%r12), %rcx
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L190:
	testq	%rcx, %rcx
	je	.L167
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r9
	je	.L159
	movq	%rsi, (%r10,%rdx,8)
	movq	8(%r14), %rdx
	leaq	24(%r14), %rdi
	addq	%r11, %rdx
	movq	(%rdx), %rax
	cmpq	%rdi, %rax
	je	.L194
.L160:
	movq	$0, (%rdx)
	movq	(%r12), %rcx
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L191:
	movq	$0, (%rsi)
	call	_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS7_EEELb0EEEEE18_M_deallocate_nodeEPSC_.isra.0
	subq	$1, 32(%r14)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%rsi, %rax
	leaq	24(%r14), %rdi
	cmpq	%rdi, %rax
	jne	.L160
.L194:
	movq	%rcx, 24(%r14)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%rsi, %rax
	leaq	80(%r14), %rdx
	cmpq	%rdx, %rax
	jne	.L149
.L193:
	movq	%rcx, 80(%r14)
	jmp	.L149
.L192:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7828:
	.size	_ZN12v8_inspector15WasmTranslation5ClearEPN2v87IsolateERKSt6vectorIiSaIiEE, .-_ZN12v8_inspector15WasmTranslation5ClearEPN2v87IsolateERKSt6vectorIiSaIiEE
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag.str1.1,"aMS",@progbits,1
.LC2:
	.string	"basic_string::_M_create"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag:
.LFB11991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	jne	.L196
	testq	%rdx, %rdx
	jne	.L212
.L196:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L197
	movq	(%r12), %rdi
.L198:
	cmpq	$2, %rbx
	je	.L213
	testq	%rbx, %rbx
	je	.L201
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L201:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L197:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L214
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L198
.L212:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L214:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11991:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.section	.text._ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_,"axG",@progbits,_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_
	.type	_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_, @function
_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_:
.LFB9237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-272(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	leaq	-176(%rbp), %rbx
	subq	$248, %rsp
	movl	%edx, -280(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movq	(%r14), %rsi
	movq	8(%r14), %rax
	leaq	-192(%rbp), %rdi
	movq	%rbx, -192(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%r14), %rax
	movq	(%r15), %rsi
	leaq	-240(%rbp), %r8
	movq	%r8, %rdi
	leaq	-224(%rbp), %r14
	movq	%r8, -288(%rbp)
	movq	%rax, -160(%rbp)
	movq	8(%r15), %rax
	movq	%r14, -240(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-288(%rbp), %r8
	movq	32(%r15), %rax
	movq	%r12, %rdi
	leaq	-128(%rbp), %r15
	movq	%r8, %rsi
	movq	%rax, -208(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	leaq	-144(%rbp), %rdi
	movq	%r15, -144(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-160(%rbp), %rax
	movsbl	-280(%rbp), %esi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rdx
	leaq	-96(%rbp), %r8
	leaq	-80(%rbp), %rax
	movq	%r8, %rdi
	movq	%r8, -280(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -96(%rbp)
	movq	%rax, -288(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-280(%rbp), %r8
	movq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r8, %rsi
	movq	%rdx, -64(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	-288(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L216
	call	_ZdlPv@PLT
.L216:
	movq	-144(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L217
	call	_ZdlPv@PLT
.L217:
	movq	-240(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L218
	call	_ZdlPv@PLT
.L218:
	movq	-192(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L219
	call	_ZdlPv@PLT
.L219:
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L226
	addq	$248, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L226:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9237:
	.size	_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_, .-_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_
	.section	.text._ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_,"axG",@progbits,_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_,comdat
	.p2align 4
	.weak	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_
	.type	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_, @function
_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_:
.LFB12665:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L243
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	12(%rdi), %rbx
	subq	$24, %rsp
	cmpq	%rsi, %rbx
	jne	.L239
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L229:
	cmpl	%eax, %r15d
	jne	.L246
	cmpl	%edx, %ecx
	setb	%al
	testb	%al, %al
	je	.L240
.L248:
	cmpq	%rbx, %r12
	je	.L233
	movl	$12, %eax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movl	%ecx, -52(%rbp)
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
	movl	-52(%rbp), %ecx
.L233:
	addq	$12, %rbx
	movl	%ecx, (%r12)
	movl	%r13d, 4(%r12)
	movl	%r15d, 8(%r12)
	cmpq	%rbx, %r14
	je	.L227
.L239:
	movl	(%rbx), %ecx
	movl	4(%rbx), %r13d
	movl	8(%rbx), %r15d
	movl	(%r12), %edx
	movl	8(%r12), %eax
	cmpl	4(%r12), %r13d
	je	.L229
.L246:
	setl	%al
	testb	%al, %al
	jne	.L248
.L240:
	movq	%rbx, %rax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L235:
	cmpl	%edx, %r15d
	jne	.L247
	cmpl	%edi, %ecx
	setb	%dl
	subq	$12, %rax
	testb	%dl, %dl
	je	.L238
.L249:
	movq	(%rax), %rdx
	movq	%rdx, 12(%rax)
	movl	8(%rax), %edx
	movl	%edx, 20(%rax)
.L232:
	movl	-12(%rax), %edi
	movl	-4(%rax), %edx
	movq	%rax, %rsi
	cmpl	-8(%rax), %r13d
	je	.L235
.L247:
	setl	%dl
	subq	$12, %rax
	testb	%dl, %dl
	jne	.L249
.L238:
	addq	$12, %rbx
	movl	%ecx, (%rsi)
	movl	%r13d, 4(%rsi)
	movl	%r15d, 8(%rsi)
	cmpq	%rbx, %r14
	jne	.L239
.L227:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE12665:
	.size	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_, .-_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_
	.section	.text._ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm:
.LFB12711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L251
	movq	(%rbx), %r8
.L252:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L261
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L262:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L275
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L276
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L254:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L256
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L258:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L259:
	testq	%rsi, %rsi
	je	.L256
.L257:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L258
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L264
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L257
	.p2align 4,,10
	.p2align 3
.L256:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L260
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L260:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L261:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L263
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L263:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L264:
	movq	%rdx, %rdi
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L254
.L276:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE12711:
	.size	_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.section	.text._ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS3_EEEES0_INS6_14_Node_iteratorIS4_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS3_EEEES0_INS6_14_Node_iteratorIS4_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS3_EEEES0_INS6_14_Node_iteratorIS4_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS3_EEEES0_INS6_14_Node_iteratorIS4_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS3_EEEES0_INS6_14_Node_iteratorIS4_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB11825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$112, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	(%rbx), %esi
	movq	8(%rbx), %rdx
	leaq	32(%rax), %r14
	movq	$0, (%rax)
	movq	%rax, %r12
	movl	%esi, 8(%rax)
	movq	%r14, 16(%rax)
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdx
	je	.L299
	movq	%rdx, 16(%r12)
	movq	24(%rbx), %rdx
	movq	%rdx, 32(%r12)
.L279:
	movq	40(%rbx), %r10
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	movq	72(%rbx), %rcx
	pxor	%xmm0, %xmm0
	movq	64(%rbx), %r9
	movw	%ax, 24(%rbx)
	movq	96(%rbx), %rax
	movq	%r10, 48(%r12)
	movq	48(%rbx), %r10
	movq	56(%rbx), %rdi
	movq	%rdx, 24(%r12)
	movq	88(%rbx), %rdx
	movups	%xmm0, 56(%rbx)
	movq	80(%rbx), %r8
	movq	$0, 16(%rbx)
	movups	%xmm0, 72(%rbx)
	movups	%xmm0, 88(%rbx)
	movq	%r10, 56(%r12)
	movslq	%esi, %r10
	movq	%rcx, 80(%r12)
	movq	8(%r13), %rcx
	movq	%rdx, 96(%r12)
	xorl	%edx, %edx
	movq	%rax, 104(%r12)
	movq	%r10, %rax
	divq	%rcx
	movq	0(%r13), %rax
	movq	%r9, 72(%r12)
	movq	%rdi, 64(%r12)
	movq	%r8, 88(%r12)
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L280
	movq	(%rax), %rbx
	movl	8(%rbx), %r11d
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L300:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L280
	movslq	8(%rbx), %rax
	xorl	%edx, %edx
	movq	%rax, %r11
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L280
.L282:
	cmpl	%r11d, %esi
	jne	.L300
	testq	%r8, %r8
	je	.L287
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	movq	64(%r12), %rdi
.L287:
	testq	%rdi, %rdi
	je	.L283
	call	_ZdlPv@PLT
.L283:
	movq	16(%r12), %rdi
	cmpq	%rdi, %r14
	je	.L284
	call	_ZdlPv@PLT
.L284:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	movq	%r12, %rcx
	movq	%r10, %rdx
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	%r9, %rsi
	call	_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	movdqu	24(%rbx), %xmm1
	movups	%xmm1, 32(%r12)
	jmp	.L279
	.cfi_endproc
.LFE11825:
	.size	_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS3_EEEES0_INS6_14_Node_iteratorIS4_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS3_EEEES0_INS6_14_Node_iteratorIS4_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm:
.LFB12770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L302
	movq	(%rbx), %r8
.L303:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L312
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L313:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L326
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L327
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L305:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L307
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L309:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L310:
	testq	%rsi, %rsi
	je	.L307
.L308:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L309
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L315
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L308
	.p2align 4,,10
	.p2align 3
.L307:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L311
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L311:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L312:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L314
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L314:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%rdx, %rdi
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L326:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L305
.L327:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE12770:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.section	.text._ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m,"axG",@progbits,_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m
	.type	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m, @function
_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m:
.LFB12790:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	(%rax,%rsi,8), %r10
	testq	%r10, %r10
	je	.L345
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%r10), %rdx
	movl	$2147483648, %ebx
	movq	56(%rdx), %r8
	cmpq	%r8, %rcx
	je	.L348
	.p2align 4,,10
	.p2align 3
.L330:
	movq	(%rdx), %r9
	testq	%r9, %r9
	je	.L335
	movq	56(%r9), %r8
	movq	%rdx, %r10
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	8(%rdi)
	cmpq	%rdx, %rsi
	jne	.L335
	movq	%r9, %rdx
	cmpq	%r8, %rcx
	jne	.L330
.L348:
	movq	8(%r11), %r8
	movq	16(%rdx), %r15
	movq	8(%rdx), %r13
	movq	(%r11), %r14
	cmpq	%r15, %r8
	movq	%r15, %r9
	cmovbe	%r8, %r9
	testq	%r9, %r9
	je	.L331
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L332:
	movzwl	0(%r13,%rax,2), %r12d
	cmpw	%r12w, (%r14,%rax,2)
	jne	.L330
	addq	$1, %rax
	cmpq	%rax, %r9
	jne	.L332
.L331:
	subq	%r15, %r8
	cmpq	%rbx, %r8
	jge	.L330
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r8
	jle	.L330
	testl	%r8d, %r8d
	jne	.L330
	popq	%rbx
	movq	%r10, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	xorl	%r10d, %r10d
	popq	%rbx
	popq	%r12
	movq	%r10, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	%r10, %rax
	ret
	.cfi_endproc
.LFE12790:
	.size	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m, .-_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m
	.section	.text._ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S6_EEEES2_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S6_EEEES2_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S6_EEEES2_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S6_EEEES2_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S6_EEEES2_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB11905:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$64, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	_Znwm@PLT
	movq	(%rbx), %r15
	leaq	24(%rax), %r8
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%r8, 8(%rax)
	leaq	16(%rbx), %rax
	cmpq	%rax, %r15
	je	.L389
	movq	16(%rbx), %rdx
	movq	%r15, 8(%r13)
	movq	%rdx, 24(%r13)
.L351:
	movq	8(%rbx), %rdx
	movq	32(%rbx), %r12
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	movw	%ax, 16(%rbx)
	movq	40(%rbx), %rax
	leaq	8(%r13), %r10
	movq	%rdx, 16(%r13)
	movq	$0, 8(%rbx)
	movq	%r12, 40(%r13)
	movq	%rax, 48(%r13)
	testq	%r12, %r12
	jne	.L352
	leaq	(%r15,%rdx,2), %rcx
	cmpq	%r15, %rcx
	je	.L353
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L354:
	movq	%r12, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r12, %rdx
	movq	%rdx, %r12
	movsbq	-2(%rax), %rdx
	addq	%rdx, %r12
	movq	%r12, 40(%r13)
	cmpq	%rax, %rcx
	jne	.L354
	testq	%r12, %r12
	je	.L353
.L352:
	movq	8(%r14), %rbx
	xorl	%edx, %edx
	movq	%r12, %rax
	movq	%r12, %rcx
	movq	%r14, %rdi
	movq	%r8, -64(%rbp)
	divq	%rbx
	movq	%rdx, %r9
	movq	%r10, %rdx
	movq	%r9, %rsi
	movq	%r9, -56(%rbp)
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	je	.L355
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L355
	movq	-64(%rbp), %r8
	cmpq	%r15, %r8
	je	.L356
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L356:
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	addq	$24, %rsp
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	movq	24(%r14), %rdx
	movq	%rbx, %rsi
	leaq	32(%r14), %rdi
	movl	$1, %ecx
	movq	%r9, -56(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rbx
	testb	%al, %al
	jne	.L358
	movq	(%r14), %r15
	movq	-56(%rbp), %r9
.L359:
	salq	$3, %r9
	movq	%r12, 56(%r13)
	leaq	(%r15,%r9), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L368
	movq	(%rdx), %rdx
	movq	%rdx, 0(%r13)
	movq	(%rax), %rax
	movq	%r13, (%rax)
.L369:
	addq	$1, 24(%r14)
	addq	$24, %rsp
	movq	%r13, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	movq	$1, 40(%r13)
	movl	$1, %r12d
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L389:
	movdqu	16(%rbx), %xmm0
	movq	%r8, %r15
	movups	%xmm0, 24(%r13)
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L358:
	cmpq	$1, %rdx
	je	.L390
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L391
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%r14), %r9
.L361:
	movq	16(%r14), %rsi
	movq	$0, 16(%r14)
	testq	%rsi, %rsi
	je	.L363
	xorl	%edi, %edi
	leaq	16(%r14), %r8
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L365:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L366:
	testq	%rsi, %rsi
	je	.L363
.L364:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	56(%rcx), %rax
	divq	%rbx
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L365
	movq	16(%r14), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%r14)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L371
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L364
	.p2align 4,,10
	.p2align 3
.L363:
	movq	(%r14), %rdi
	cmpq	%r9, %rdi
	je	.L367
	call	_ZdlPv@PLT
.L367:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rbx, 8(%r14)
	divq	%rbx
	movq	%r15, (%r14)
	movq	%rdx, %r9
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L368:
	movq	16(%r14), %rdx
	movq	%r13, 16(%r14)
	movq	%rdx, 0(%r13)
	testq	%rdx, %rdx
	je	.L370
	movq	56(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r14)
	movq	%r13, (%r15,%rdx,8)
	movq	(%r14), %rax
	addq	%r9, %rax
.L370:
	leaq	16(%r14), %rdx
	movq	%rdx, (%rax)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L371:
	movq	%rdx, %rdi
	jmp	.L366
.L390:
	leaq	48(%r14), %r15
	movq	$0, 48(%r14)
	movq	%r15, %r9
	jmp	.L361
.L391:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE11905:
	.size	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S6_EEEES2_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S6_EEEES2_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZN12v8_inspector15WasmTranslation13AddFakeScriptERKNS_8String16EPNS0_14TranslatorImplE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15WasmTranslation13AddFakeScriptERKNS_8String16EPNS0_14TranslatorImplE
	.type	_ZN12v8_inspector15WasmTranslation13AddFakeScriptERKNS_8String16EPNS0_14TranslatorImplE, @function
_ZN12v8_inspector15WasmTranslation13AddFakeScriptERKNS_8String16EPNS0_14TranslatorImplE:
.LFB7838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	(%rsi), %r15
	movq	8(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -112(%rbp)
	leaq	(%rcx,%rcx), %r12
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L393
	testq	%r15, %r15
	je	.L411
.L393:
	movq	%r12, %r8
	movq	%r14, %rdi
	sarq	%r8
	cmpq	$14, %r12
	ja	.L412
.L394:
	cmpq	$2, %r12
	je	.L413
	testq	%r12, %r12
	je	.L397
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	memmove@PLT
	movq	-112(%rbp), %rdi
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r9
.L397:
	xorl	%eax, %eax
	movq	%r8, -104(%rbp)
	leaq	-112(%rbp), %rsi
	movw	%ax, (%rdi,%rcx,2)
	movq	32(%rbx), %rax
	leaq	64(%r13), %rdi
	movq	%r9, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S6_EEEES2_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L392
	call	_ZdlPv@PLT
.L392:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L414
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	movzwl	(%r15), %eax
	movw	%ax, (%rdi)
	movq	-112(%rbp), %rdi
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L412:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L415
	leaq	2(%r12), %rdi
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_Znwm@PLT
	movq	-136(%rbp), %r8
	movq	-128(%rbp), %rcx
	movq	%rax, -112(%rbp)
	movq	-120(%rbp), %r9
	movq	%rax, %rdi
	movq	%r8, -96(%rbp)
	jmp	.L394
.L411:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L414:
	call	__stack_chk_fail@PLT
.L415:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7838:
	.size	_ZN12v8_inspector15WasmTranslation13AddFakeScriptERKNS_8String16EPNS0_14TranslatorImplE, .-_ZN12v8_inspector15WasmTranslation13AddFakeScriptERKNS_8String16EPNS0_14TranslatorImplE
	.section	.rodata._ZN12v8_inspector15WasmTranslation9AddScriptEN2v85LocalINS1_5debug10WasmScriptEEEPNS_19V8DebuggerAgentImplE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"wasm://wasm/"
	.section	.text._ZN12v8_inspector15WasmTranslation9AddScriptEN2v85LocalINS1_5debug10WasmScriptEEEPNS_19V8DebuggerAgentImplE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15WasmTranslation9AddScriptEN2v85LocalINS1_5debug10WasmScriptEEEPNS_19V8DebuggerAgentImplE
	.type	_ZN12v8_inspector15WasmTranslation9AddScriptEN2v85LocalINS1_5debug10WasmScriptEEEPNS_19V8DebuggerAgentImplE, @function
_ZN12v8_inspector15WasmTranslation9AddScriptEN2v85LocalINS1_5debug10WasmScriptEEEPNS_19V8DebuggerAgentImplE:
.LFB7789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -432(%rbp)
	movl	$64, %edi
	movq	%rdx, -472(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	(%r14), %rdi
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.L456
	movq	%r12, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, %rdi
.L417:
	leaq	56(%rbx), %rax
	movq	%rdi, (%rbx)
	leaq	_ZN12v8_inspector15WasmTranslation14TranslatorImpl24kGlobalScriptHandleLabelE(%rip), %rsi
	movq	%rax, 8(%rbx)
	movq	$1, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movl	$0x3f800000, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	call	_ZN2v82V822AnnotateStrongRetainerEPmPKc@PLT
	movq	%r12, %rdi
	call	_ZNK2v85debug6Script2IdEv@PLT
	movl	$24, %edi
	movl	%eax, %r12d
	call	_Znwm@PLT
	movq	-432(%rbp), %rcx
	movslq	%r12d, %r9
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movq	16(%rcx), %r8
	movl	%r12d, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%r9, %rax
	divq	%r8
	movq	8(%rcx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rsi
	testq	%rax, %rax
	je	.L418
	movq	(%rax), %rbx
	movl	8(%rbx), %ecx
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L485:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L418
	movslq	8(%rbx), %rax
	xorl	%edx, %edx
	movq	%rax, %rcx
	divq	%r8
	cmpq	%rdx, %rsi
	jne	.L418
.L420:
	cmpl	%ecx, %r12d
	jne	.L485
	call	_ZNSt8__detail16_Hashtable_allocISaINS_10_Hash_nodeISt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS7_EEELb0EEEEE18_M_deallocate_nodeEPSC_.isra.0
.L454:
	movq	16(%rbx), %rax
	movq	-432(%rbp), %rcx
	movq	(%rax), %r12
	movq	(%rcx), %rdi
	movq	%rax, -448(%rbp)
	movq	%rdi, -440(%rbp)
	testq	%r12, %r12
	je	.L421
	movq	(%r12), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
.L421:
	movq	%r12, %rdi
	call	_ZNK2v85debug10WasmScript12NumFunctionsEv@PLT
	movq	%r12, %rdi
	movl	%eax, %r15d
	movl	%eax, -460(%rbp)
	call	_ZNK2v85debug10WasmScript20NumImportedFunctionsEv@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZNK2v85debug6Script2IdEv@PLT
	leaq	-352(%rbp), %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	cmpl	%ebx, %r15d
	jle	.L451
	movq	-432(%rbp), %rax
	leaq	-112(%rbp), %r12
	leaq	-384(%rbp), %r13
	addq	$64, %rax
	movq	%rax, -480(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, -424(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, -416(%rbp)
	leaq	-304(%rbp), %rax
	movq	%rax, -520(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -408(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -504(%rbp)
	leaq	-256(%rbp), %rax
	movq	%rax, -488(%rbp)
	leaq	-192(%rbp), %rax
	movq	%rax, -512(%rbp)
	leaq	-240(%rbp), %rax
	movq	%rax, -456(%rbp)
	leaq	-392(%rbp), %rax
	movq	%rax, -496(%rbp)
	leaq	-288(%rbp), %rax
	movq	%rax, -528(%rbp)
	.p2align 4,,10
	.p2align 3
.L453:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	-352(%rbp), %rsi
	movq	-344(%rbp), %rax
	movq	-424(%rbp), %r15
	movq	-416(%rbp), %r14
	leaq	(%rsi,%rax,2), %rdx
	movq	%r15, %rdi
	movq	%r14, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	%r12, %rcx
	movl	$45, %edx
	movq	%r15, %rsi
	movq	-320(%rbp), %rax
	movq	-520(%rbp), %rdi
	movq	%rax, -128(%rbp)
	call	_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_
	movq	-160(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L425
	call	_ZdlPv@PLT
.L425:
	movq	-112(%rbp), %rdi
	cmpq	-408(%rbp), %rdi
	je	.L426
	call	_ZdlPv@PLT
.L426:
	movq	-448(%rbp), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L427
	movq	(%r15), %rsi
	movq	-440(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r15
.L427:
	movq	%r15, %rdi
	call	_ZNK2v85debug6Script4NameEv@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L486
.L428:
	movq	-440(%rbp), %rsi
	movq	-504(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	%r15, %rdi
	call	_ZNK2v85debug10WasmScript12NumFunctionsEv@PLT
	movq	%r15, %rdi
	movl	%eax, %r14d
	call	_ZNK2v85debug10WasmScript20NumImportedFunctionsEv@PLT
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movq	-416(%rbp), %rax
	movq	-208(%rbp), %rsi
	movq	-424(%rbp), %rdi
	movq	%rax, -160(%rbp)
	movq	-200(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-176(%rbp), %rax
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	cmpq	-408(%rbp), %rdi
	je	.L429
	call	_ZdlPv@PLT
.L429:
	movq	-408(%rbp), %rax
	movq	-160(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	movq	-152(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-128(%rbp), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	%r13, %rdi
	movl	$47, %esi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	-112(%rbp), %rdi
	cmpq	-408(%rbp), %rdi
	je	.L430
	call	_ZdlPv@PLT
.L430:
	movq	-160(%rbp), %rdi
	cmpq	-416(%rbp), %rdi
	je	.L431
	call	_ZdlPv@PLT
.L431:
	movl	%r14d, %eax
	subl	%r15d, %eax
	cmpl	$300, %eax
	jg	.L487
.L432:
	movq	-208(%rbp), %rsi
	movq	-200(%rbp), %rax
	movq	%r12, %rdi
	movq	-408(%rbp), %r15
	leaq	(%rsi,%rax,2), %rdx
	movq	%r15, -112(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-176(%rbp), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	%r13, %rdi
	movl	$45, %esi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L438
	call	_ZdlPv@PLT
.L438:
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15String16Builder12appendNumberEi@PLT
	movq	-488(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L439
	call	_ZdlPv@PLT
.L439:
	movq	-208(%rbp), %rdi
	cmpq	-512(%rbp), %rdi
	je	.L440
	call	_ZdlPv@PLT
.L440:
	movq	-408(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-256(%rbp), %rax
	cmpq	-456(%rbp), %rax
	je	.L488
	movq	%rax, -112(%rbp)
	movq	-240(%rbp), %rax
	movq	%rax, -96(%rbp)
.L442:
	movq	-248(%rbp), %rax
	movq	-304(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	$0, -248(%rbp)
	movq	-424(%rbp), %rdi
	movw	%cx, -240(%rbp)
	movq	%rax, -104(%rbp)
	movq	-456(%rbp), %rax
	movq	%rax, -256(%rbp)
	movq	-224(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-416(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-296(%rbp), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-272(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-448(%rbp), %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.L443
	movq	(%rcx), %rsi
	movq	-440(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rcx
.L443:
	subq	$8, %rsp
	movq	-432(%rbp), %rdx
	movq	-496(%rbp), %rdi
	movq	%r12, %r9
	movq	-424(%rbp), %r8
	movq	-440(%rbp), %rsi
	pushq	%rbx
	call	_ZN12v8_inspector16V8DebuggerScript10CreateWasmEPN2v87IsolateEPNS_15WasmTranslationENS1_5LocalINS1_5debug10WasmScriptEEENS_8String16ESA_i@PLT
	popq	%rax
	movq	-160(%rbp), %rdi
	popq	%rdx
	cmpq	-416(%rbp), %rdi
	je	.L444
	call	_ZdlPv@PLT
.L444:
	movq	-112(%rbp), %rdi
	cmpq	-408(%rbp), %rdi
	je	.L445
	call	_ZdlPv@PLT
.L445:
	movq	-392(%rbp), %r14
	movq	-408(%rbp), %r15
	movq	%r12, %rdi
	movq	%r15, -112(%rbp)
	movq	8(%r14), %rsi
	movq	16(%r14), %rax
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	40(%r14), %rax
	movq	-480(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -80(%rbp)
	movq	-448(%rbp), %rax
	movq	%rax, -72(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S6_EEEES2_INS9_14_Node_iteratorIS7_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	movq	-112(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L446
	call	_ZdlPv@PLT
.L446:
	movq	-392(%rbp), %rax
	movq	-472(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movq	$0, -392(%rbp)
	movq	%rax, -384(%rbp)
	call	_ZN12v8_inspector19V8DebuggerAgentImpl14didParseSourceESt10unique_ptrINS_16V8DebuggerScriptESt14default_deleteIS2_EEb@PLT
	movq	-384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L447
	movq	(%rdi), %rax
	call	*8(%rax)
.L447:
	movq	-392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L448
	movq	(%rdi), %rax
	call	*8(%rax)
.L448:
	movq	-256(%rbp), %rdi
	cmpq	-456(%rbp), %rdi
	je	.L449
	call	_ZdlPv@PLT
.L449:
	movq	-304(%rbp), %rdi
	cmpq	-528(%rbp), %rdi
	je	.L450
	call	_ZdlPv@PLT
	addl	$1, %ebx
	cmpl	%ebx, -460(%rbp)
	jne	.L453
.L451:
	movq	-352(%rbp), %rdi
	leaq	-336(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L416
	call	_ZdlPv@PLT
.L416:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L489
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	addl	$1, %ebx
	cmpl	%ebx, -460(%rbp)
	jne	.L453
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L488:
	movdqa	-240(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L487:
	leal	-1(%r14), %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	-104(%rbp), %r14
	movq	-112(%rbp), %rdi
	cmpq	-408(%rbp), %rdi
	je	.L433
	call	_ZdlPv@PLT
.L433:
	movslq	%ebx, %rsi
	movl	%ebx, %eax
	movq	-424(%rbp), %rdi
	imulq	$1374389535, %rsi, %rsi
	sarl	$31, %eax
	sarq	$37, %rsi
	subl	%eax, %esi
	imull	$100, %esi, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	-152(%rbp), %r15
	cmpq	%r15, %r14
	jbe	.L434
	.p2align 4,,10
	.p2align 3
.L435:
	movl	$48, %esi
	movq	%r13, %rdi
	addq	$1, %r15
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	cmpq	%r15, %r14
	jne	.L435
	movq	-152(%rbp), %r15
.L434:
	movq	-160(%rbp), %rsi
	movq	-408(%rbp), %r14
	movq	%r12, %rdi
	leaq	(%rsi,%r15,2), %rdx
	movq	%r14, -112(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-128(%rbp), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	%r13, %rdi
	movl	$47, %esi
	call	_ZN12v8_inspector15String16Builder6appendEc@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L436
	call	_ZdlPv@PLT
.L436:
	movq	-160(%rbp), %rdi
	cmpq	-416(%rbp), %rdi
	je	.L432
	call	_ZdlPv@PLT
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L486:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L418:
	movq	-432(%rbp), %rax
	movq	%rdi, %rcx
	movl	$1, %r8d
	movq	%r9, %rdx
	leaq	8(%rax), %r10
	movq	%r10, %rdi
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN12v8_inspector15WasmTranslation14TranslatorImplESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	movq	%rax, %rbx
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L456:
	xorl	%edi, %edi
	jmp	.L417
.L489:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7789:
	.size	_ZN12v8_inspector15WasmTranslation9AddScriptEN2v85LocalINS1_5debug10WasmScriptEEEPNS_19V8DebuggerAgentImplE, .-_ZN12v8_inspector15WasmTranslation9AddScriptEN2v85LocalINS1_5debug10WasmScriptEEEPNS_19V8DebuggerAgentImplE
	.section	.text.unlikely._ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei,"ax",@progbits
	.align 2
.LCOLDB4:
	.section	.text._ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei,"ax",@progbits
.LHOTB4:
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei
	.type	_ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei, @function
_ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei:
.LFB7832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	64(%rsi), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	32(%rdx), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.L491
	movq	(%rdx), %rax
	movq	8(%rdx), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L494
	.p2align 4,,10
	.p2align 3
.L493:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r8)
	cmpq	%rax, %rsi
	jne	.L493
	testq	%rcx, %rcx
	je	.L494
.L491:
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	72(%rbx)
	movq	%rdx, %rsi
	movq	%r8, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m
	testq	%rax, %rax
	je	.L495
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L495
	movq	(%rbx), %r15
	movq	48(%rax), %rax
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rax, -120(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-120(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L496
	movq	(%rdi), %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L496:
	movl	%r13d, %esi
	leaq	-80(%rbp), %r13
	call	_ZN2v85debug10WasmScript15GetFunctionHashEi@PLT
	movq	%r13, %rdi
	movl	%eax, %r15d
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15String16Builder19appendUnsignedAsHexEj@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L497
	call	_ZdlPv@PLT
.L497:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L518
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	movq	$1, 32(%r8)
	movl	$1, %ecx
	jmp	.L491
.L518:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei
	.cfi_startproc
	.type	_ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei.cold, @function
_ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei.cold:
.LFSB7832:
.L495:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	48, %rax
	ud2
	.cfi_endproc
.LFE7832:
	.section	.text._ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei
	.size	_ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei, .-_ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei
	.section	.text.unlikely._ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei
	.size	_ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei.cold, .-_ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei.cold
.LCOLDE4:
	.section	.text._ZN12v8_inspector15WasmTranslation7GetHashERKNS_8String16Ei
.LHOTE4:
	.section	.text._ZSt22__move_median_to_firstIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_SH_SH_T0_,"axG",@progbits,_ZSt22__move_median_to_firstIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_SH_SH_T0_,comdat
	.p2align 4
	.weak	_ZSt22__move_median_to_firstIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_SH_SH_T0_
	.type	_ZSt22__move_median_to_firstIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_SH_SH_T0_, @function
_ZSt22__move_median_to_firstIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_SH_SH_T0_:
.LFB13039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	4(%rsi), %eax
	movl	4(%rdx), %r9d
	movl	(%rsi), %r14d
	movl	8(%rsi), %ebx
	movl	(%rdx), %r13d
	movl	8(%rdx), %r11d
	cmpl	%r9d, %eax
	jne	.L542
	cmpl	%r11d, %ebx
	je	.L522
.L542:
	setl	%r8b
.L521:
	movl	(%rcx), %r15d
	movl	4(%rcx), %r10d
	movl	8(%rcx), %r12d
	testb	%r8b, %r8b
	je	.L523
	cmpl	%r9d, %r10d
	je	.L524
	setg	%r13b
.L525:
	movl	(%rdi), %r11d
	movl	4(%rdi), %r9d
	movl	8(%rdi), %r8d
	testb	%r13b, %r13b
	je	.L527
.L545:
	movq	(%rdx), %rax
	movq	%rax, (%rdi)
	movl	8(%rdx), %eax
	movl	%eax, 8(%rdi)
	movl	%r11d, (%rdx)
	movl	%r9d, 4(%rdx)
	movl	%r8d, 8(%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	cmpl	%eax, %r10d
	je	.L533
	setg	%r14b
.L534:
	movl	(%rdi), %ebx
	movl	4(%rdi), %r8d
	movl	8(%rdi), %eax
	testb	%r14b, %r14b
	je	.L536
.L543:
	movq	(%rsi), %rdx
	movq	%rdx, (%rdi)
	movl	8(%rsi), %edx
	movl	%edx, 8(%rdi)
	movl	%ebx, (%rsi)
	movl	%r8d, 4(%rsi)
	movl	%eax, 8(%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L533:
	.cfi_restore_state
	cmpl	%r12d, %ebx
	je	.L535
	setl	%r14b
	movl	(%rdi), %ebx
	movl	4(%rdi), %r8d
	movl	8(%rdi), %eax
	testb	%r14b, %r14b
	jne	.L543
.L536:
	cmpl	%r9d, %r10d
	jne	.L544
	cmpl	%r12d, %r11d
	je	.L539
	setl	%sil
.L538:
	testb	%sil, %sil
	je	.L540
.L548:
	movq	(%rcx), %rdx
	movq	%rdx, (%rdi)
	movl	8(%rcx), %edx
	movl	%edx, 8(%rdi)
	movl	%ebx, (%rcx)
	movl	%r8d, 4(%rcx)
	movl	%eax, 8(%rcx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	cmpl	%r12d, %r11d
	je	.L526
	setl	%r13b
	movl	(%rdi), %r11d
	movl	4(%rdi), %r9d
	movl	8(%rdi), %r8d
	testb	%r13b, %r13b
	jne	.L545
.L527:
	cmpl	%eax, %r10d
	jne	.L546
	cmpl	%r12d, %ebx
	je	.L531
	setl	%al
.L530:
	testb	%al, %al
	je	.L532
.L547:
	movq	(%rcx), %rax
	movq	%rax, (%rdi)
	movl	8(%rcx), %eax
	movl	%eax, 8(%rdi)
	movl	%r11d, (%rcx)
	movl	%r9d, 4(%rcx)
	movl	%r8d, 8(%rcx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	cmpl	%r13d, %r14d
	setb	%r8b
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L546:
	setg	%al
	testb	%al, %al
	jne	.L547
.L532:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 8(%rdi)
	movl	%r11d, (%rsi)
	movl	%r9d, 4(%rsi)
	movl	%r8d, 8(%rsi)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L544:
	.cfi_restore_state
	setg	%sil
	testb	%sil, %sil
	jne	.L548
.L540:
	movq	(%rdx), %rcx
	movq	%rcx, (%rdi)
	movl	8(%rdx), %ecx
	movl	%ecx, 8(%rdi)
	movl	%ebx, (%rdx)
	movl	%r8d, 4(%rdx)
	movl	%eax, 8(%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	cmpl	%r13d, %r15d
	seta	%r13b
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L535:
	cmpl	%r14d, %r15d
	seta	%r14b
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L539:
	cmpl	%r13d, %r15d
	seta	%sil
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L531:
	cmpl	%r14d, %r15d
	seta	%al
	jmp	.L530
	.cfi_endproc
.LFE13039:
	.size	_ZSt22__move_median_to_firstIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_SH_SH_T0_, .-_ZSt22__move_median_to_firstIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_SH_SH_T0_
	.section	.text._ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_T0_SI_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_T0_SI_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_T0_SI_T1_T2_
	.type	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_T0_SI_T1_T2_, @function
_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_T0_SI_T1_T2_:
.LFB13297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rax, %r11
	shrq	$63, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	addq	%rax, %r11
	movq	%rcx, %rax
	pushq	%r14
	shrq	$32, %rax
	sarq	%r11
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rax, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	andl	$1, %ebx
	movq	%rdx, -88(%rbp)
	movq	%rcx, -56(%rbp)
	movl	%r8d, -48(%rbp)
	movq	%rbx, -72(%rbp)
	movl	%ecx, -60(%rbp)
	cmpq	%r11, %rsi
	jge	.L550
	movq	%rsi, -80(%rbp)
	movq	%rsi, %r10
	.p2align 4,,10
	.p2align 3
.L557:
	leaq	1(%r10), %rax
	leaq	(%rax,%rax), %rdx
	leaq	-1(%rdx), %r9
	leaq	(%rdx,%rax,4), %rax
	leaq	(%r9,%r9,2), %rcx
	leaq	(%rdi,%rax,4), %rax
	leaq	(%rdi,%rcx,4), %rcx
	movl	(%rax), %r14d
	movl	8(%rax), %ebx
	movl	(%rcx), %r15d
	movl	8(%rcx), %r12d
	movl	4(%rax), %esi
	cmpl	%esi, 4(%rcx)
	jne	.L576
	cmpl	%ebx, %r12d
	jne	.L576
	cmpl	%r14d, %r15d
	seta	%bl
	testb	%bl, %bl
	jne	.L554
.L578:
	movq	(%rax), %r9
	leaq	(%r10,%r10,2), %rcx
	leaq	(%rdi,%rcx,4), %rcx
	movq	%r9, (%rcx)
	movl	8(%rax), %r9d
	movl	%r9d, 8(%rcx)
	cmpq	%rdx, %r11
	jle	.L572
	movq	%rdx, %r10
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L576:
	setg	%bl
	testb	%bl, %bl
	je	.L578
.L554:
	movq	(%rcx), %rdx
	leaq	(%r10,%r10,2), %rax
	leaq	(%rdi,%rax,4), %rax
	movq	%rdx, (%rax)
	movl	8(%rcx), %edx
	movl	%edx, 8(%rax)
	cmpq	%r9, %r11
	jle	.L565
	movq	%r9, %r10
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L572:
	movq	-80(%rbp), %rsi
.L555:
	cmpq	$0, -72(%rbp)
	jne	.L558
.L564:
	movq	-88(%rbp), %r9
	subq	$2, %r9
	movq	%r9, %rcx
	shrq	$63, %rcx
	addq	%r9, %rcx
	sarq	%rcx
	cmpq	%rdx, %rcx
	jne	.L558
	leaq	1(%rdx,%rdx), %rdx
	leaq	(%rdx,%rdx,2), %rcx
	leaq	(%rdi,%rcx,4), %rcx
	movq	(%rcx), %r9
	movq	%r9, (%rax)
	movl	8(%rcx), %r9d
	movl	%r9d, 8(%rax)
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L558:
	leaq	-1(%rdx), %rcx
	movq	%rcx, %r9
	shrq	$63, %r9
	addq	%rcx, %r9
	sarq	%r9
	cmpq	%rsi, %rdx
	jg	.L563
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L560:
	cmpl	%r8d, %eax
	jne	.L577
	cmpl	-60(%rbp), %r10d
	leaq	(%rdx,%rdx,2), %rax
	setb	%r10b
	leaq	(%rdi,%rax,4), %rax
	testb	%r10b, %r10b
	je	.L559
.L580:
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movl	8(%rcx), %edx
	movl	%edx, 8(%rax)
	leaq	-1(%r9), %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	movq	%r9, %rdx
	sarq	%rax
	cmpq	%r9, %rsi
	jge	.L579
	movq	%rax, %r9
.L563:
	leaq	(%r9,%r9,2), %rax
	leaq	(%rdi,%rax,4), %rcx
	movl	(%rcx), %r10d
	movl	8(%rcx), %eax
	cmpl	%r13d, 4(%rcx)
	je	.L560
.L577:
	leaq	(%rdx,%rdx,2), %rax
	setl	%r10b
	leaq	(%rdi,%rax,4), %rax
	testb	%r10b, %r10b
	jne	.L580
.L559:
	movl	-60(%rbp), %esi
	movl	%r13d, 4(%rax)
	movl	%r8d, 8(%rax)
	movl	%esi, (%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_restore_state
	movq	%rcx, %rax
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L565:
	movq	-80(%rbp), %rsi
	movq	%rcx, %rax
	movq	%r9, %rdx
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L550:
	leaq	(%rsi,%rsi,2), %rax
	cmpq	$0, -72(%rbp)
	leaq	(%rdi,%rax,4), %rax
	jne	.L559
	movq	%rsi, %rdx
	jmp	.L564
	.cfi_endproc
.LFE13297:
	.size	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_T0_SI_T1_T2_, .-_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_T0_SI_T1_T2_
	.section	.text._ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_T1_,comdat
	.p2align 4
	.weak	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_T1_
	.type	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_T1_, @function
_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_T1_:
.LFB11800:
	.cfi_startproc
	endbr64
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$192, %rax
	jle	.L610
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	12(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	testq	%rdx, %rdx
	je	.L615
.L585:
	movq	%r13, %rax
	leaq	-12(%r13), %rcx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	subq	%rbx, %rax
	subq	$1, %r14
	movq	%r15, %r12
	movabsq	$-6148914691236517205, %rdx
	sarq	$2, %rax
	imulq	%rax, %rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	movq	%rax, %rdx
	andq	$-2, %rax
	sarq	%rdx
	addq	%rdx, %rax
	leaq	(%rbx,%rax,4), %rdx
	call	_ZSt22__move_median_to_firstIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_SH_SH_T0_
	movl	(%rbx), %edi
	movl	4(%rbx), %esi
	movq	%r13, %rax
	movl	8(%rbx), %ecx
	.p2align 4,,10
	.p2align 3
.L590:
	movl	4(%r12), %r8d
	movl	(%r12), %r9d
	movq	%r12, -80(%rbp)
	movl	8(%r12), %r11d
	cmpl	%esi, %r8d
	jne	.L613
	cmpl	%ecx, %r11d
	jne	.L613
	cmpl	%edi, %r9d
	setb	%dl
.L592:
	testb	%dl, %dl
	jne	.L594
	leaq	-12(%rax), %rdx
	movl	%ecx, %r10d
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L595:
	cmpl	%r10d, %eax
	jne	.L614
	cmpl	%edi, %ecx
	seta	%cl
	subq	$12, %rdx
	testb	%cl, %cl
	je	.L616
.L598:
	movl	(%rdx), %ecx
	movl	8(%rdx), %eax
	movq	%rdx, -72(%rbp)
	cmpl	%esi, 4(%rdx)
	je	.L595
.L614:
	setg	%cl
	subq	$12, %rdx
	testb	%cl, %cl
	jne	.L598
.L616:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r12
	jnb	.L617
	movq	(%rax), %rdx
	movq	%rdx, (%r12)
	movl	8(%rax), %edx
	movl	%edx, 8(%r12)
	movl	%r9d, (%rax)
	movl	%r8d, 4(%rax)
	movl	%r11d, 8(%rax)
	movl	(%rbx), %edi
	movl	4(%rbx), %esi
	movl	8(%rbx), %ecx
.L594:
	addq	$12, %r12
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L613:
	setl	%dl
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L617:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_T1_
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$192, %rax
	jle	.L581
	testq	%r14, %r14
	je	.L583
	movq	%r12, %r13
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L615:
	movq	%rsi, -80(%rbp)
.L583:
	movabsq	$-6148914691236517205, %r13
	sarq	$2, %rax
	imulq	%rax, %r13
	leaq	-2(%r13), %r12
	sarq	%r12
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L586:
	subq	$1, %r12
.L588:
	leaq	(%r12,%r12,2), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	(%rbx,%rax,4), %rcx
	movl	8(%rbx,%rax,4), %r8d
	movq	%rcx, -60(%rbp)
	movl	%r8d, -52(%rbp)
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_T0_SI_T1_T2_
	testq	%r12, %r12
	jne	.L586
	movabsq	$-6148914691236517205, %r13
	movq	-80(%rbp), %r12
	subq	$12, %r12
	.p2align 4,,10
	.p2align 3
.L587:
	movq	(%rbx), %rax
	movq	%r12, %r14
	movq	(%r12), %rcx
	xorl	%esi, %esi
	subq	%rbx, %r14
	movl	8(%r12), %r8d
	movq	%rbx, %rdi
	subq	$12, %r12
	movq	%rax, 12(%r12)
	movq	%r14, %rdx
	movl	8(%rbx), %eax
	sarq	$2, %rdx
	movq	%rcx, -60(%rbp)
	movl	%eax, 20(%r12)
	imulq	%r13, %rdx
	movl	%r8d, -52(%rbp)
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElS4_NS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_T0_SI_T1_T2_
	cmpq	$12, %r14
	jg	.L587
.L581:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE11800:
	.size	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_T1_, .-_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_T1_
	.section	.text._ZN12v8_inspector15WasmTranslation14TranslatorImpl20GetSourceInformationEPN2v87IsolateEi,"axG",@progbits,_ZN12v8_inspector15WasmTranslation14TranslatorImpl20GetSourceInformationEPN2v87IsolateEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector15WasmTranslation14TranslatorImpl20GetSourceInformationEPN2v87IsolateEi
	.type	_ZN12v8_inspector15WasmTranslation14TranslatorImpl20GetSourceInformationEPN2v87IsolateEi, @function
_ZN12v8_inspector15WasmTranslation14TranslatorImpl20GetSourceInformationEPN2v87IsolateEi:
.LFB7704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	%edx, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	8(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L619
	movq	(%rax), %rsi
	movq	%rdx, %r9
	movl	8(%rsi), %r8d
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L711:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L619
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L619
.L621:
	cmpl	%r13d, %r8d
	jne	.L711
	leaq	16(%rsi), %r15
.L618:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L712
	addq	$392, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L619:
	.cfi_restore_state
	leaq	-400(%rbp), %rax
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -408(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	je	.L622
	movq	(%rsi), %rsi
	movq	%r12, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
.L622:
	leaq	-320(%rbp), %rdi
	movl	%r13d, %edx
	leaq	-352(%rbp), %r12
	call	_ZNK2v85debug10WasmScript19DisassembleFunctionEi@PLT
	movq	-280(%rbp), %r8
	pxor	%xmm0, %xmm0
	movq	-272(%rbp), %rax
	movq	-312(%rbp), %rdx
	movq	-320(%rbp), %rsi
	leaq	-368(%rbp), %rdi
	movq	$0, -272(%rbp)
	movq	%r8, -424(%rbp)
	movq	-288(%rbp), %r14
	leaq	-240(%rbp), %rbx
	movq	%rax, -416(%rbp)
	movaps	%xmm0, -288(%rbp)
	call	_ZN12v8_inspector8String16C1EPKcm@PLT
	movq	-368(%rbp), %rdi
	movq	%rbx, -256(%rbp)
	movq	-416(%rbp), %rax
	movq	-424(%rbp), %r8
	cmpq	%r12, %rdi
	je	.L713
	movq	-352(%rbp), %rdx
	movq	%rdi, -256(%rbp)
	movq	%rdx, -240(%rbp)
.L624:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	movq	-360(%rbp), %rdx
	movq	-336(%rbp), %rsi
	punpcklqdq	%xmm1, %xmm0
	xorl	%ecx, %ecx
	movq	%r12, -368(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rdx, -248(%rbp)
	movq	$0, -360(%rbp)
	movw	%cx, -352(%rbp)
	movq	%rsi, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movaps	%xmm0, -176(%rbp)
	testq	%rdx, %rdx
	je	.L673
	xorl	%eax, %eax
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L714:
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L673
.L627:
	cmpw	$10, (%rdi,%rax,2)
	leaq	(%rax,%rax), %rsi
	jne	.L714
	movq	%rsi, %rax
	sarq	%rax
	cmpq	$-2, %rsi
	je	.L673
	movl	%eax, %r10d
	addl	$1, %eax
	xorl	%r9d, %r9d
	movl	$1, %esi
	cltq
	cmpq	%rax, %rdx
	jbe	.L625
	.p2align 4,,10
	.p2align 3
.L628:
	movq	%rdx, %rsi
	leaq	(%rax,%rax), %r11
	subq	%rax, %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L632:
	cmpw	$10, (%rdi,%r11)
	je	.L630
	addq	$1, %rax
	addq	$2, %r11
	cmpq	%rax, %rsi
	jne	.L632
	leal	1(%r9), %esi
.L625:
	subl	%r10d, %edx
	movq	%r8, %r10
	movl	%esi, -216(%rbp)
	movabsq	$-6148914691236517205, %rax
	subq	%r14, %r10
	subl	$1, %edx
	movq	%r10, %r9
	movl	%edx, -212(%rbp)
	sarq	$2, %r9
	imulq	%rax, %r9
	testq	%r9, %r9
	jne	.L715
	cmpq	%r8, %r14
	je	.L716
	movq	%r10, %rdx
	movq	%r14, %rsi
	xorl	%edi, %edi
	movq	%r9, -424(%rbp)
	movq	%r10, -416(%rbp)
	call	memmove@PLT
	movq	-184(%rbp), %r11
	movq	-416(%rbp), %r10
	movq	-424(%rbp), %r9
	leaq	(%r11,%r10), %r8
.L640:
	movq	%r10, -416(%rbp)
	movq	%r8, %rdx
	movq	%r8, -176(%rbp)
	cmpq	%r8, %r11
	je	.L642
	bsrq	%r9, %r9
	movl	$63, %edx
	movq	%r8, %rsi
	movq	%r11, %rdi
	xorq	$63, %r9
	movq	%r8, -432(%rbp)
	subl	%r9d, %edx
	movq	%r11, -424(%rbp)
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEElNS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_T1_
	movq	-416(%rbp), %r10
	movq	-424(%rbp), %r11
	movq	-432(%rbp), %r8
	cmpq	$192, %r10
	jle	.L643
	leaq	192(%r11), %r14
	movq	%r11, %rdi
	movq	%r8, -416(%rbp)
	movq	%r14, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_
	movq	-416(%rbp), %r8
	movq	%r14, %rsi
	cmpq	%r14, %r8
	je	.L645
	.p2align 4,,10
	.p2align 3
.L651:
	movl	(%rsi), %r11d
	movl	4(%rsi), %r10d
	movq	%rsi, %rax
	movl	8(%rsi), %r9d
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L646:
	cmpl	%edx, %r9d
	jne	.L710
	cmpl	%r14d, %r11d
	setb	%dl
	subq	$12, %rax
	testb	%dl, %dl
	je	.L649
.L717:
	movq	(%rax), %rdx
	movq	%rdx, 12(%rax)
	movl	8(%rax), %edx
	movl	%edx, 20(%rax)
.L650:
	movl	-12(%rax), %r14d
	movl	-4(%rax), %edx
	movq	%rax, %rdi
	cmpl	-8(%rax), %r10d
	je	.L646
.L710:
	setl	%dl
	subq	$12, %rax
	testb	%dl, %dl
	jne	.L717
.L649:
	addq	$12, %rsi
	movl	%r11d, (%rdi)
	movl	%r10d, 4(%rdi)
	movl	%r9d, 8(%rdi)
	cmpq	%rsi, %r8
	jne	.L651
.L645:
	movq	-184(%rbp), %r8
	movq	-176(%rbp), %rdx
.L642:
	movq	-256(%rbp), %rax
	leaq	-136(%rbp), %r14
	movl	%r13d, -160(%rbp)
	movq	%r14, -152(%rbp)
	cmpq	%rbx, %rax
	je	.L718
	movq	%rax, -152(%rbp)
	movq	-240(%rbp), %rax
	movq	%rax, -136(%rbp)
.L654:
	movq	-248(%rbp), %rax
	pxor	%xmm0, %xmm0
	leaq	8(%r15), %rdi
	movq	%r8, -80(%rbp)
	movdqa	-208(%rbp), %xmm2
	movq	%rdx, -72(%rbp)
	leaq	-160(%rbp), %rsi
	movq	%rax, -144(%rbp)
	xorl	%eax, %eax
	movw	%ax, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	%rbx, -256(%rbp)
	movq	%rax, -120(%rbp)
	movq	-216(%rbp), %rax
	movq	$0, -248(%rbp)
	movq	%rax, -112(%rbp)
	movq	-192(%rbp), %rax
	movups	%xmm2, -104(%rbp)
	movq	%rax, -88(%rbp)
	movq	-168(%rbp), %rax
	movaps	%xmm0, -208(%rbp)
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -192(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZNSt10_HashtableIiSt4pairIKiN12v8_inspector21WasmSourceInformationEESaIS4_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS3_EEEES0_INS6_14_Node_iteratorIS4_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	movq	-80(%rbp), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L655
	call	_ZdlPv@PLT
.L655:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L656
	call	_ZdlPv@PLT
.L656:
	movq	-152(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L657
	call	_ZdlPv@PLT
.L657:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L658
	call	_ZdlPv@PLT
.L658:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L659
	call	_ZdlPv@PLT
.L659:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L660
	call	_ZdlPv@PLT
.L660:
	movq	-368(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L661
	call	_ZdlPv@PLT
.L661:
	movq	-288(%rbp), %rdi
	addq	$16, %r15
	testq	%rdi, %rdi
	je	.L662
	call	_ZdlPv@PLT
.L662:
	movq	-320(%rbp), %rdi
	leaq	-304(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L663
	call	_ZdlPv@PLT
.L663:
	movq	-408(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L630:
	movq	%r11, %rax
	leal	1(%r9), %esi
	sarq	%rax
	cmpq	$-2, %r11
	je	.L625
	movl	%eax, %r10d
	addl	$1, %eax
	cltq
	cmpq	%rax, %rdx
	jbe	.L719
	movl	%esi, %r9d
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L719:
	leal	2(%r9), %esi
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L673:
	movl	$-1, %r10d
	xorl	%esi, %esi
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L713:
	movdqa	-352(%rbp), %xmm3
	movq	%rbx, %rdi
	movaps	%xmm3, -240(%rbp)
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L716:
	movq	%r10, %r8
	xorl	%r11d, %r11d
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L718:
	movdqa	-240(%rbp), %xmm4
	movups	%xmm4, -136(%rbp)
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L643:
	movq	%r8, %rsi
	movq	%r11, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v85debug31WasmDisassemblyOffsetTableEntryESt6vectorIS4_SaIS4_EEEENS0_5__ops15_Iter_comp_iterIZN12v8_inspector21WasmSourceInformationC4ENSC_8String16ES8_EUlS4_S4_E_EEEvT_SH_T0_
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L715:
	movabsq	$768614336404564650, %rax
	cmpq	%rax, %r9
	ja	.L720
	movq	%r10, %rdi
	movq	%r9, -432(%rbp)
	movq	%r8, -424(%rbp)
	movq	%r10, -416(%rbp)
	call	_Znwm@PLT
	movq	-424(%rbp), %r8
	movq	-416(%rbp), %r10
	movq	-432(%rbp), %r9
	movq	%rax, %r11
	cmpq	%r8, %r14
	je	.L639
	.p2align 4,,10
	.p2align 3
.L638:
	movq	(%r14), %rdx
	addq	$12, %r14
	addq	$12, %rax
	movq	%rdx, -12(%rax)
	movl	-4(%r14), %edx
	movl	%edx, -4(%rax)
	cmpq	%r14, %r8
	jne	.L638
.L639:
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L637
	movq	%r9, -432(%rbp)
	movq	%r11, -424(%rbp)
	movq	%r10, -416(%rbp)
	call	_ZdlPv@PLT
	movq	-432(%rbp), %r9
	movq	-424(%rbp), %r11
	movq	-416(%rbp), %r10
.L637:
	leaq	(%r11,%r10), %r8
	movq	%r11, -184(%rbp)
	movq	%r8, -168(%rbp)
	jmp	.L640
.L712:
	call	__stack_chk_fail@PLT
.L720:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7704:
	.size	_ZN12v8_inspector15WasmTranslation14TranslatorImpl20GetSourceInformationEPN2v87IsolateEi, .-_ZN12v8_inspector15WasmTranslation14TranslatorImpl20GetSourceInformationEPN2v87IsolateEi
	.section	.text.unlikely._ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei,"ax",@progbits
	.align 2
.LCOLDB5:
	.section	.text._ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei,"ax",@progbits
.LHOTB5:
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei
	.type	_ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei, @function
_ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei:
.LFB7829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rsi), %rcx
	movq	%rdi, %rbx
	addq	$64, %rdi
	testq	%rcx, %rcx
	jne	.L722
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L725
	.p2align 4,,10
	.p2align 3
.L724:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r8)
	cmpq	%rax, %rsi
	jne	.L724
	testq	%rcx, %rcx
	je	.L725
.L722:
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	72(%rbx)
	movq	%rdx, %rsi
	movq	%r8, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m
	testq	%rax, %rax
	je	.L726
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L726
	movq	(%rbx), %rsi
	movq	48(%rax), %rdi
	movl	%r12d, %edx
	call	_ZN12v8_inspector15WasmTranslation14TranslatorImpl20GetSourceInformationEPN2v87IsolateEi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L725:
	.cfi_restore_state
	movq	$1, 32(%r8)
	movl	$1, %ecx
	jmp	.L722
	.cfi_endproc
	.section	.text.unlikely._ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei
	.cfi_startproc
	.type	_ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei.cold, @function
_ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei.cold:
.LFSB7829:
.L726:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	48, %rax
	ud2
	.cfi_endproc
.LFE7829:
	.section	.text._ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei
	.size	_ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei, .-_ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei
	.section	.text.unlikely._ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei
	.size	_ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei.cold, .-_ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei.cold
.LCOLDE5:
	.section	.text._ZN12v8_inspector15WasmTranslation9GetSourceERKNS_8String16Ei
.LHOTE5:
	.section	.text.unlikely._ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei,"ax",@progbits
	.align 2
.LCOLDB6:
	.section	.text._ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei,"ax",@progbits
.LHOTB6:
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei
	.type	_ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei, @function
_ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei:
.LFB7830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rsi), %rcx
	movq	%rdi, %rbx
	addq	$64, %rdi
	testq	%rcx, %rcx
	jne	.L741
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L744
	.p2align 4,,10
	.p2align 3
.L743:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r8)
	cmpq	%rax, %rsi
	jne	.L743
	testq	%rcx, %rcx
	je	.L744
.L741:
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	72(%rbx)
	movq	%rdx, %rsi
	movq	%r8, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m
	testq	%rax, %rax
	je	.L745
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L745
	movq	(%rbx), %rsi
	movq	48(%rax), %rdi
	movl	%r12d, %edx
	call	_ZN12v8_inspector15WasmTranslation14TranslatorImpl20GetSourceInformationEPN2v87IsolateEi
	popq	%rbx
	popq	%r12
	movl	40(%rax), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L744:
	.cfi_restore_state
	movq	$1, 32(%r8)
	movl	$1, %ecx
	jmp	.L741
	.cfi_endproc
	.section	.text.unlikely._ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei
	.cfi_startproc
	.type	_ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei.cold, @function
_ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei.cold:
.LFSB7830:
.L745:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	48, %rax
	ud2
	.cfi_endproc
.LFE7830:
	.section	.text._ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei
	.size	_ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei, .-_ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei
	.section	.text.unlikely._ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei
	.size	_ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei.cold, .-_ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei.cold
.LCOLDE6:
	.section	.text._ZN12v8_inspector15WasmTranslation10GetEndLineERKNS_8String16Ei
.LHOTE6:
	.section	.text.unlikely._ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei,"ax",@progbits
	.align 2
.LCOLDB7:
	.section	.text._ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei,"ax",@progbits
.LHOTB7:
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei
	.type	_ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei, @function
_ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei:
.LFB7831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rsi), %rcx
	movq	%rdi, %rbx
	addq	$64, %rdi
	testq	%rcx, %rcx
	jne	.L760
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rsi
	cmpq	%rsi, %rax
	je	.L763
	.p2align 4,,10
	.p2align 3
.L762:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movsbq	-2(%rax), %rcx
	addq	%rdx, %rcx
	movq	%rcx, 32(%r8)
	cmpq	%rax, %rsi
	jne	.L762
	testq	%rcx, %rcx
	je	.L763
.L760:
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	72(%rbx)
	movq	%rdx, %rsi
	movq	%r8, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m
	testq	%rax, %rax
	je	.L764
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L764
	movq	(%rbx), %rsi
	movq	48(%rax), %rdi
	movl	%r12d, %edx
	call	_ZN12v8_inspector15WasmTranslation14TranslatorImpl20GetSourceInformationEPN2v87IsolateEi
	popq	%rbx
	popq	%r12
	movl	44(%rax), %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L763:
	.cfi_restore_state
	movq	$1, 32(%r8)
	movl	$1, %ecx
	jmp	.L760
	.cfi_endproc
	.section	.text.unlikely._ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei
	.cfi_startproc
	.type	_ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei.cold, @function
_ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei.cold:
.LFSB7831:
.L764:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	48, %rax
	ud2
	.cfi_endproc
.LFE7831:
	.section	.text._ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei
	.size	_ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei, .-_ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei
	.section	.text.unlikely._ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei
	.size	_ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei.cold, .-_ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei.cold
.LCOLDE7:
	.section	.text._ZN12v8_inspector15WasmTranslation12GetEndColumnERKNS_8String16Ei
.LHOTE7:
	.section	.text._ZN12v8_inspector15WasmTranslation45TranslateWasmScriptLocationToProtocolLocationEPNS_8String16EPiS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15WasmTranslation45TranslateWasmScriptLocationToProtocolLocationEPNS_8String16EPiS3_
	.type	_ZN12v8_inspector15WasmTranslation45TranslateWasmScriptLocationToProtocolLocationEPNS_8String16EPiS3_, @function
_ZN12v8_inspector15WasmTranslation45TranslateWasmScriptLocationToProtocolLocationEPNS_8String16EPiS3_:
.LFB7833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	-257(%rbp), %rsi
	movq	%rbx, %rdi
	subq	$296, %rsp
	movq	%rdx, -280(%rbp)
	movq	%rcx, -288(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, -257(%rbp)
	call	_ZNK12v8_inspector8String169toIntegerEPb@PLT
	movzbl	-257(%rbp), %r13d
	testb	%r13b, %r13b
	jne	.L833
.L778:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L834
	addq	$296, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L833:
	.cfi_restore_state
	movq	16(%r12), %r8
	movl	%eax, %esi
	xorl	%edx, %edx
	cltq
	divq	%r8
	movq	8(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L810
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L835:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L810
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L810
.L781:
	cmpl	%edi, %esi
	jne	.L835
	movq	-288(%rbp), %rax
	leaq	16(%rbx), %rsi
	movq	16(%rcx), %rdi
	movq	%rsi, -304(%rbp)
	movl	(%rax), %r10d
	movq	-280(%rbp), %rax
	movl	(%rax), %edx
	movq	(%rbx), %rax
	cmpq	%rsi, %rax
	je	.L836
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	movq	16(%rbx), %r8
	movq	8(%rbx), %rsi
	movq	%rcx, (%rbx)
	movq	32(%rbx), %rcx
	movw	%r9w, 16(%rbx)
	leaq	-88(%rbp), %r9
	movq	%r9, -296(%rbp)
	movq	%r9, -104(%rbp)
	leaq	-144(%rbp), %r9
	movq	%rax, -160(%rbp)
	movq	%r8, -144(%rbp)
	movq	%rsi, -152(%rbp)
	movq	$0, 8(%rbx)
	movq	%rcx, -128(%rbp)
	movq	%r12, -112(%rbp)
	movq	%r9, -312(%rbp)
	cmpq	%r9, %rax
	je	.L782
	movq	%rax, -104(%rbp)
	movq	%r8, -88(%rbp)
.L784:
	movq	%rsi, -96(%rbp)
	movq	(%r12), %rsi
	xorl	%r12d, %r12d
	movq	%rcx, -72(%rbp)
	movl	%edx, -64(%rbp)
	movl	%r10d, -60(%rbp)
	call	_ZN12v8_inspector15WasmTranslation14TranslatorImpl20GetSourceInformationEPN2v87IsolateEi
	movl	-60(%rbp), %r15d
	movq	48(%rax), %rsi
	movq	%rax, %r14
	movq	56(%rax), %rax
	subq	%rsi, %rax
	sarq	$2, %rax
	imull	$-1431655765, %eax, %eax
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L837:
	leal	(%r12,%rax), %edx
	shrl	%edx
	movl	%edx, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	cmpl	(%rsi,%rcx,4), %r15d
	cmovb	%edx, %eax
	cmovnb	%rdx, %r12
.L786:
	movl	%eax, %edx
	subl	%r12d, %edx
	cmpl	$1, %edx
	ja	.L837
	leaq	-160(%rbp), %rcx
	movl	-64(%rbp), %esi
	movq	%rcx, %rdi
	movq	%rcx, -336(%rbp)
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdx
	leaq	-208(%rbp), %r11
	leaq	-192(%rbp), %rax
	movq	%r11, %rdi
	movq	%r11, -320(%rbp)
	leaq	(%rsi,%rdx,2), %rdx
	movq	%rax, -208(%rbp)
	movq	%rax, -328(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-72(%rbp), %rdx
	movq	-320(%rbp), %r11
	leaq	-256(%rbp), %rdi
	movq	-336(%rbp), %rcx
	movq	%rdx, -176(%rbp)
	movq	%r11, %rsi
	movl	$45, %edx
	call	_ZN12v8_inspector8String166concatIJS0_cS0_EEES0_DpT_
	movq	-208(%rbp), %rdi
	movq	-328(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L788
	call	_ZdlPv@PLT
.L788:
	movq	-160(%rbp), %rdi
	cmpq	-312(%rbp), %rdi
	je	.L789
	call	_ZdlPv@PLT
.L789:
	movq	-256(%rbp), %rdx
	leaq	-240(%rbp), %rcx
	movq	-104(%rbp), %rdi
	movq	-248(%rbp), %rax
	cmpq	%rcx, %rdx
	je	.L838
	movq	-240(%rbp), %r11
	cmpq	-296(%rbp), %rdi
	je	.L839
	movq	%rax, %xmm0
	movq	%r11, %xmm1
	movq	-88(%rbp), %rsi
	movq	%rdx, -104(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -96(%rbp)
	testq	%rdi, %rdi
	je	.L795
	movq	%rdi, -256(%rbp)
	movq	%rsi, -240(%rbp)
.L793:
	movq	$0, -248(%rbp)
	xorl	%esi, %esi
	movw	%si, (%rdi)
	movq	-224(%rbp), %rax
	movq	-256(%rbp), %rdi
	movq	%rax, -72(%rbp)
	cmpq	%rcx, %rdi
	je	.L796
	call	_ZdlPv@PLT
.L796:
	movq	48(%r14), %rdx
	leaq	(%r12,%r12,2), %rax
	salq	$2, %rax
	addq	%rax, %rdx
	cmpl	(%rdx), %r15d
	je	.L840
	movq	$0, -64(%rbp)
.L798:
	movq	(%rbx), %rdi
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rax
	cmpq	-296(%rbp), %rdx
	je	.L841
	movq	-88(%rbp), %rcx
	cmpq	%rdi, -304(%rbp)
	je	.L842
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	16(%rbx), %rsi
	movq	%rdx, (%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	testq	%rdi, %rdi
	je	.L804
	movq	%rdi, -104(%rbp)
	movq	%rsi, -88(%rbp)
.L802:
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movw	%ax, (%rdi)
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, 32(%rbx)
	movl	-64(%rbp), %eax
	movq	-280(%rbp), %rbx
	movl	%eax, (%rbx)
	movl	-60(%rbp), %eax
	movq	-288(%rbp), %rbx
	movl	%eax, (%rbx)
	cmpq	-296(%rbp), %rdi
	je	.L778
	call	_ZdlPv@PLT
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L810:
	xorl	%r13d, %r13d
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L839:
	movq	%rax, %xmm0
	movq	%r11, %xmm5
	movq	%rdx, -104(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -96(%rbp)
.L795:
	movq	%rcx, -256(%rbp)
	leaq	-240(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L840:
	movl	4(%rdx), %edx
	movl	%edx, -64(%rbp)
	movq	48(%r14), %rdx
	movl	8(%rdx,%rax), %eax
	movl	%eax, -60(%rbp)
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L841:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L800
	cmpq	$1, %rax
	je	.L843
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L800
	movq	-296(%rbp), %rsi
	call	memmove@PLT
	movq	-96(%rbp), %rax
	movq	(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L800:
	xorl	%ecx, %ecx
	movq	%rax, 8(%rbx)
	movw	%cx, (%rdi,%rdx)
	movq	-104(%rbp), %rdi
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L838:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L791
	cmpq	$1, %rax
	je	.L844
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L791
	movq	%rcx, %rsi
	movq	%rcx, -312(%rbp)
	call	memmove@PLT
	movq	-248(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	-312(%rbp), %rcx
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L791:
	xorl	%r8d, %r8d
	movq	%rax, -96(%rbp)
	movw	%r8w, (%rdi,%rdx)
	movq	-256(%rbp), %rdi
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L836:
	leaq	-88(%rbp), %rax
	xorl	%r11d, %r11d
	movdqu	16(%rbx), %xmm3
	movq	8(%rbx), %rsi
	movq	%rax, -296(%rbp)
	movq	32(%rbx), %rcx
	movq	%rax, -104(%rbp)
	leaq	-144(%rbp), %rax
	movq	$0, 8(%rbx)
	movw	%r11w, 16(%rbx)
	movq	%r12, -112(%rbp)
	movq	%rax, -312(%rbp)
	movaps	%xmm3, -144(%rbp)
.L782:
	movdqa	-144(%rbp), %xmm4
	movups	%xmm4, -88(%rbp)
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L842:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%rdx, (%rbx)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%rbx)
.L804:
	movq	-296(%rbp), %rax
	movq	%rax, -104(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	%rax, %rdi
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L844:
	movzwl	-240(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-248(%rbp), %rax
	movq	-104(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L843:
	movzwl	-88(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rax
	movq	(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L800
.L834:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7833:
	.size	_ZN12v8_inspector15WasmTranslation45TranslateWasmScriptLocationToProtocolLocationEPNS_8String16EPiS3_, .-_ZN12v8_inspector15WasmTranslation45TranslateWasmScriptLocationToProtocolLocationEPNS_8String16EPiS3_
	.section	.rodata._ZN12v8_inspector15WasmTranslation45TranslateProtocolLocationToWasmScriptLocationEPNS_8String16EPiS3_.str1.1,"aMS",@progbits,1
.LC8:
	.string	"basic_string::substr"
	.section	.rodata._ZN12v8_inspector15WasmTranslation45TranslateProtocolLocationToWasmScriptLocationEPNS_8String16EPiS3_.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.section	.text._ZN12v8_inspector15WasmTranslation45TranslateProtocolLocationToWasmScriptLocationEPNS_8String16EPiS3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector15WasmTranslation45TranslateProtocolLocationToWasmScriptLocationEPNS_8String16EPiS3_
	.type	_ZN12v8_inspector15WasmTranslation45TranslateProtocolLocationToWasmScriptLocationEPNS_8String16EPiS3_, @function
_ZN12v8_inspector15WasmTranslation45TranslateProtocolLocationToWasmScriptLocationEPNS_8String16EPiS3_:
.LFB7837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	addq	$64, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -216(%rbp)
	movq	32(%rsi), %r12
	movq	%rcx, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	jne	.L846
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %rcx
	cmpq	%rcx, %rax
	je	.L849
	.p2align 4,,10
	.p2align 3
.L848:
	movq	%r12, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%r12, %rdx
	movsbq	-2(%rax), %r12
	addq	%rdx, %r12
	movq	%r12, 32(%r14)
	cmpq	%rax, %rcx
	jne	.L848
	testq	%r12, %r12
	je	.L849
.L846:
	xorl	%edx, %edx
	movq	%r12, %rax
	movq	%r12, %rcx
	divq	72(%r13)
	movq	%rdx, %rsi
	movq	%r14, %rdx
	call	_ZNKSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_PNS0_15WasmTranslation14TranslatorImplEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS3_m
	testq	%rax, %rax
	je	.L897
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L897
	movq	48(%rax), %rax
	movq	(%r14), %rsi
	movq	%rax, -232(%rbp)
	movq	-224(%rbp), %rax
	movl	(%rax), %edx
	movq	-216(%rbp), %rax
	movl	(%rax), %ecx
	leaq	16(%r14), %rax
	movq	%rax, -248(%rbp)
	cmpq	%rax, %rsi
	je	.L942
	movq	%rax, %rbx
	movq	8(%r14), %r15
	movq	16(%r14), %rax
	xorl	%r10d, %r10d
	movq	%rbx, (%r14)
	leaq	-144(%rbp), %rdi
	leaq	-88(%rbp), %rbx
	movq	%rsi, -160(%rbp)
	movq	%rax, -144(%rbp)
	movq	%r15, -152(%rbp)
	movq	$0, 8(%r14)
	movw	%r10w, 16(%r14)
	movq	%r12, -128(%rbp)
	movq	%r13, -112(%rbp)
	movq	%rbx, -104(%rbp)
	cmpq	%rdi, %rsi
	je	.L852
	movq	%rsi, -104(%rbp)
	movq	%rax, -88(%rbp)
.L854:
	movq	0(%r13), %rax
	movq	%r15, -96(%rbp)
	movq	%r12, -72(%rbp)
	movl	%ecx, -64(%rbp)
	movl	%edx, -60(%rbp)
	movq	%rax, -240(%rbp)
	testq	%r15, %r15
	je	.L855
	leaq	-1(%r15), %rdx
	movl	$4294967295, %eax
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L856:
	cmpw	$45, (%rsi,%rdx,2)
	je	.L943
	subq	$1, %rdx
	cmpq	$-1, %rdx
	jne	.L856
	movl	$4294967295, %r13d
	leaq	-176(%rbp), %r12
	movb	$1, -193(%rbp)
	cmpq	%r13, %r15
	movq	%r12, -192(%rbp)
	cmovbe	%r15, %r13
	addq	%r13, %r13
.L892:
	movq	%r13, %r15
	sarq	%r15
	cmpq	$14, %r13
	jbe	.L944
	leaq	2(%r13), %rdi
	movq	%rsi, -256(%rbp)
	call	_Znwm@PLT
	movq	%r15, -176(%rbp)
	movq	-256(%rbp), %rsi
	movq	%rax, -192(%rbp)
	movq	%rax, %rdi
.L860:
	movq	%r13, %rdx
	call	memmove@PLT
	movq	-192(%rbp), %rax
.L862:
	xorl	%r9d, %r9d
	movq	%r15, -184(%rbp)
	leaq	-192(%rbp), %rsi
	movw	%r9w, (%rax,%r13)
	leaq	-160(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-192(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L863
	call	_ZdlPv@PLT
.L863:
	movq	%r13, %rdi
	leaq	-193(%rbp), %rsi
	leaq	-144(%rbp), %r15
	call	_ZNK12v8_inspector8String169toIntegerEPb@PLT
	movq	-160(%rbp), %rdi
	movl	%eax, %r12d
	cmpq	%r15, %rdi
	je	.L864
	call	_ZdlPv@PLT
.L864:
	movq	-240(%rbp), %rsi
	movq	-232(%rbp), %rdi
	movl	%r12d, %edx
	call	_ZN12v8_inspector15WasmTranslation14TranslatorImpl20GetSourceInformationEPN2v87IsolateEi
	movq	80(%rax), %r9
	movq	72(%rax), %rsi
	cmpq	%rsi, %r9
	je	.L865
	movq	%r9, %rax
	movl	-64(%rbp), %r10d
	movl	-60(%rbp), %edi
	movq	%rsi, %rcx
	movabsq	$-6148914691236517205, %rdx
	subq	%rsi, %rax
	sarq	$2, %rax
	imulq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L870:
	testq	%rax, %rax
	jle	.L866
.L867:
	movq	%rax, %rdx
	sarq	%rax
	leaq	(%rax,%rax,2), %r8
	leaq	(%rcx,%r8,4), %r8
	cmpl	%r10d, 4(%r8)
	jl	.L868
	je	.L945
.L869:
	testq	%rax, %rax
	jne	.L867
.L866:
	movq	-232(%rbp), %rax
	movq	(%rax), %rdi
	cmpq	%rcx, %r9
	je	.L946
	cmpl	%r10d, 4(%rcx)
	je	.L902
	cmpq	%rcx, %rsi
	je	.L902
	movl	-12(%rcx), %eax
	movl	%eax, -256(%rbp)
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L849:
	movq	$1, 32(%r14)
	movl	$1, %r12d
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L902:
	movl	(%rcx), %eax
	movl	%eax, -256(%rbp)
.L873:
	testq	%rdi, %rdi
	je	.L876
	movq	(%rdi), %rsi
	movq	-240(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L876:
	call	_ZNK2v85debug6Script2IdEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movq	-160(%rbp), %rdx
	movq	-104(%rbp), %rdi
	movq	-152(%rbp), %rax
	cmpq	%r15, %rdx
	je	.L947
	movq	-144(%rbp), %rcx
	cmpq	%rbx, %rdi
	je	.L948
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	-88(%rbp), %rsi
	movq	%rdx, -104(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -96(%rbp)
	testq	%rdi, %rdi
	je	.L882
	movq	%rdi, -160(%rbp)
	movq	%rsi, -144(%rbp)
.L880:
	movq	$0, -152(%rbp)
	xorl	%esi, %esi
	movw	%si, (%rdi)
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %rdi
	movq	%rax, -72(%rbp)
	cmpq	%r15, %rdi
	je	.L883
	call	_ZdlPv@PLT
.L883:
	movl	-256(%rbp), %eax
	movl	%r12d, -64(%rbp)
	movl	%eax, -60(%rbp)
.L865:
	movq	-104(%rbp), %rdx
	movq	(%r14), %rdi
	movq	-96(%rbp), %rax
	cmpq	%rbx, %rdx
	je	.L949
	movq	-88(%rbp), %rcx
	cmpq	%rdi, -248(%rbp)
	je	.L950
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	16(%r14), %rsi
	movq	%rdx, (%r14)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r14)
	testq	%rdi, %rdi
	je	.L889
	movq	%rdi, -104(%rbp)
	movq	%rsi, -88(%rbp)
.L887:
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movq	-216(%rbp), %rcx
	movl	$1, %r12d
	movw	%ax, (%rdi)
	movq	-72(%rbp), %rax
	movq	-104(%rbp), %rdi
	movq	%rax, 32(%r14)
	movl	-64(%rbp), %eax
	movl	%eax, (%rcx)
	movl	-60(%rbp), %eax
	movq	-224(%rbp), %rcx
	movl	%eax, (%rcx)
	cmpq	%rbx, %rdi
	je	.L845
	call	_ZdlPv@PLT
.L845:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L951
	addq	$216, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L945:
	.cfi_restore_state
	cmpl	%edi, 8(%r8)
	jge	.L869
	.p2align 4,,10
	.p2align 3
.L868:
	subq	%rax, %rdx
	leaq	12(%r8), %rcx
	movq	%rdx, %rax
	subq	$1, %rax
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L897:
	xorl	%r12d, %r12d
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L855:
	leaq	-176(%rbp), %r12
	movb	$1, -193(%rbp)
	xorl	%r13d, %r13d
	movq	%r12, -192(%rbp)
.L898:
	movq	%r12, %rax
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L944:
	cmpq	$2, %r13
	jne	.L861
	movzwl	(%rsi), %eax
	movw	%ax, -176(%rbp)
	movq	-192(%rbp), %rax
	jmp	.L862
.L861:
	testq	%r13, %r13
	je	.L898
	movq	%r12, %rdi
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L943:
	addq	$1, %rdx
	movb	$1, -193(%rbp)
	cmpq	%r15, %rdx
	ja	.L952
	subq	%rdx, %r15
	movl	$4294967295, %eax
	leaq	-176(%rbp), %r12
	cmpq	%rax, %r15
	movq	%r12, -192(%rbp)
	leaq	(%rsi,%rdx,2), %rsi
	cmova	%rax, %r15
	leaq	(%r15,%r15), %r13
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L942:
	xorl	%r11d, %r11d
	leaq	-88(%rbp), %rbx
	movq	8(%r14), %r15
	movq	%r13, -112(%rbp)
	movdqu	16(%r14), %xmm3
	movq	$0, 8(%r14)
	movw	%r11w, 16(%r14)
	movq	%rbx, -104(%rbp)
	movaps	%xmm3, -144(%rbp)
.L852:
	movdqa	-144(%rbp), %xmm4
	movq	%rbx, %rsi
	movups	%xmm4, -88(%rbp)
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L949:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L885
	cmpq	$1, %rax
	je	.L953
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L885
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-96(%rbp), %rax
	movq	(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L885:
	xorl	%ecx, %ecx
	movq	%rax, 8(%r14)
	movw	%cx, (%rdi,%rdx)
	movq	-104(%rbp), %rdi
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L950:
	movq	%rax, %xmm0
	movq	%rcx, %xmm5
	movq	%rdx, (%r14)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 8(%r14)
.L889:
	movq	%rbx, -104(%rbp)
	leaq	-88(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L946:
	testq	%rdi, %rdi
	je	.L872
	movq	(%rdi), %rsi
	movq	-240(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L872:
	movl	%r12d, %esi
	call	_ZNK2v85debug10WasmScript16GetFunctionRangeEi@PLT
	movq	%rax, %rdx
	shrq	$32, %rdx
	movl	%edx, %ecx
	subl	%eax, %ecx
	movq	-232(%rbp), %rax
	movl	%ecx, -256(%rbp)
	movq	(%rax), %rdi
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L947:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L878
	cmpq	$1, %rax
	je	.L954
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L878
	movq	%r15, %rsi
	call	memmove@PLT
	movq	-152(%rbp), %rax
	movq	-104(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L878:
	xorl	%r8d, %r8d
	movq	%rax, -96(%rbp)
	movw	%r8w, (%rdi,%rdx)
	movq	-160(%rbp), %rdi
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L948:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%rdx, -104(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -96(%rbp)
.L882:
	movq	%r15, -160(%rbp)
	leaq	-144(%rbp), %r15
	movq	%r15, %rdi
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L953:
	movzwl	-88(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rax
	movq	(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L954:
	movzwl	-144(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-152(%rbp), %rax
	movq	-104(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L878
.L951:
	call	__stack_chk_fail@PLT
.L952:
	movq	%r15, %rcx
	leaq	.LC8(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE7837:
	.size	_ZN12v8_inspector15WasmTranslation45TranslateProtocolLocationToWasmScriptLocationEPNS_8String16EPiS3_, .-_ZN12v8_inspector15WasmTranslation45TranslateProtocolLocationToWasmScriptLocationEPNS_8String16EPiS3_
	.globl	_ZN12v8_inspector15WasmTranslation14TranslatorImpl24kGlobalScriptHandleLabelE
	.section	.rodata._ZN12v8_inspector15WasmTranslation14TranslatorImpl24kGlobalScriptHandleLabelE,"a"
	.align 32
	.type	_ZN12v8_inspector15WasmTranslation14TranslatorImpl24kGlobalScriptHandleLabelE, @object
	.size	_ZN12v8_inspector15WasmTranslation14TranslatorImpl24kGlobalScriptHandleLabelE, 41
_ZN12v8_inspector15WasmTranslation14TranslatorImpl24kGlobalScriptHandleLabelE:
	.string	"WasmTranslation::TranslatorImpl::script_"
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
