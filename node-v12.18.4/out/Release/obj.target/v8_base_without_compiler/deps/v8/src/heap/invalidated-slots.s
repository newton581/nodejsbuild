	.file	"invalidated-slots.cc"
	.text
	.section	.text._ZN2v88internal22InvalidatedSlotsFilter8OldToOldEPNS0_11MemoryChunkE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22InvalidatedSlotsFilter8OldToOldEPNS0_11MemoryChunkE
	.type	_ZN2v88internal22InvalidatedSlotsFilter8OldToOldEPNS0_11MemoryChunkE, @function
_ZN2v88internal22InvalidatedSlotsFilter8OldToOldEPNS0_11MemoryChunkE:
.LFB8309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	168(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L11
.L2:
	movq	144(%rbx), %rdx
	leaq	64(%r12), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, 40(%r12)
	movl	$0, 64(%r12)
	movq	$0, 72(%r12)
	movq	%rcx, 80(%r12)
	movq	%rcx, 88(%r12)
	movq	$0, 96(%r12)
	movb	%al, 52(%r12)
	movups	%xmm0, (%r12)
	testq	%rdx, %rdx
	je	.L3
	movq	24(%rdx), %rcx
	addq	$8, %rdx
	movq	48(%rbx), %rax
	movq	%rdx, %xmm1
	movq	%rcx, %xmm0
	movq	%rax, 16(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	cmpq	%rdx, %rcx
	je	.L4
	movq	32(%rcx), %rax
	subq	$1, %rax
	movq	%rax, 24(%r12)
	movslq	40(%rcx), %rdx
	addq	%rdx, %rax
	movq	%rax, 32(%r12)
.L5:
	movl	$0, 48(%r12)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	_ZNK2v88internal11MemoryChunk10InOldSpaceEv@PLT
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	movq	48(%rbx), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 16(%r12)
	movups	%xmm0, (%r12)
.L4:
	movq	%rax, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 24(%r12)
	jmp	.L5
	.cfi_endproc
.LFE8309:
	.size	_ZN2v88internal22InvalidatedSlotsFilter8OldToOldEPNS0_11MemoryChunkE, .-_ZN2v88internal22InvalidatedSlotsFilter8OldToOldEPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal22InvalidatedSlotsFilter8OldToNewEPNS0_11MemoryChunkE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22InvalidatedSlotsFilter8OldToNewEPNS0_11MemoryChunkE
	.type	_ZN2v88internal22InvalidatedSlotsFilter8OldToNewEPNS0_11MemoryChunkE, @function
_ZN2v88internal22InvalidatedSlotsFilter8OldToNewEPNS0_11MemoryChunkE:
.LFB8331:
	.cfi_startproc
	endbr64
	movq	136(%rsi), %rdx
	leaq	64(%rdi), %rcx
	pxor	%xmm0, %xmm0
	movb	$1, 52(%rdi)
	movq	$0, 40(%rdi)
	movq	%rdi, %rax
	movl	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	%rcx, 80(%rdi)
	movq	%rcx, 88(%rdi)
	movq	$0, 96(%rdi)
	movups	%xmm0, (%rdi)
	testq	%rdx, %rdx
	je	.L13
	movq	24(%rdx), %rcx
	addq	$8, %rdx
	movq	48(%rsi), %rsi
	movq	%rdx, %xmm1
	movq	%rcx, %xmm0
	movq	%rsi, 16(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	cmpq	%rdx, %rcx
	je	.L14
	movq	32(%rcx), %rdi
	leaq	-1(%rdi), %rdx
	movq	%rdx, 24(%rax)
	movslq	40(%rcx), %rcx
	movl	$0, 48(%rax)
	addq	%rcx, %rdx
	movq	%rdx, 32(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	48(%rsi), %rsi
	movq	%rcx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movq	%rsi, 16(%rdi)
	movups	%xmm0, (%rdi)
.L14:
	movq	%rsi, %xmm0
	movl	$0, 48(%rax)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 24(%rax)
	ret
	.cfi_endproc
.LFE8331:
	.size	_ZN2v88internal22InvalidatedSlotsFilter8OldToNewEPNS0_11MemoryChunkE, .-_ZN2v88internal22InvalidatedSlotsFilter8OldToNewEPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal22InvalidatedSlotsFilterC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22InvalidatedSlotsFilterC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEb
	.type	_ZN2v88internal22InvalidatedSlotsFilterC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEb, @function
_ZN2v88internal22InvalidatedSlotsFilterC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEb:
.LFB8339:
	.cfi_startproc
	endbr64
	leaq	64(%rdi), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 40(%rdi)
	movl	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	%rax, 80(%rdi)
	movq	%rax, 88(%rdi)
	movq	$0, 96(%rdi)
	movb	%cl, 52(%rdi)
	movups	%xmm0, (%rdi)
	testq	%rdx, %rdx
	je	.L18
	leaq	8(%rdx), %rcx
	movq	24(%rdx), %rdx
	movq	48(%rsi), %rax
	movq	%rcx, %xmm1
	movq	%rdx, %xmm0
	movq	%rax, 16(%rdi)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rdi)
	cmpq	%rdx, %rcx
	je	.L19
	movq	32(%rdx), %rax
	subq	$1, %rax
	movq	%rax, 24(%rdi)
	movslq	40(%rdx), %rdx
	movl	$0, 48(%rdi)
	addq	%rdx, %rax
	movq	%rax, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rax, %xmm0
	movq	48(%rsi), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 16(%rdi)
	movups	%xmm0, (%rdi)
.L19:
	movq	%rax, %xmm0
	movl	$0, 48(%rdi)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 24(%rdi)
	ret
	.cfi_endproc
.LFE8339:
	.size	_ZN2v88internal22InvalidatedSlotsFilterC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEb, .-_ZN2v88internal22InvalidatedSlotsFilterC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEb
	.globl	_ZN2v88internal22InvalidatedSlotsFilterC1EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEb
	.set	_ZN2v88internal22InvalidatedSlotsFilterC1EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEb,_ZN2v88internal22InvalidatedSlotsFilterC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEEb
	.section	.text._ZN2v88internal23InvalidatedSlotsCleanup8OldToNewEPNS0_11MemoryChunkE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23InvalidatedSlotsCleanup8OldToNewEPNS0_11MemoryChunkE
	.type	_ZN2v88internal23InvalidatedSlotsCleanup8OldToNewEPNS0_11MemoryChunkE, @function
_ZN2v88internal23InvalidatedSlotsCleanup8OldToNewEPNS0_11MemoryChunkE:
.LFB8341:
	.cfi_startproc
	endbr64
	movq	136(%rsi), %rcx
	leaq	32(%rdi), %rdx
	pxor	%xmm0, %xmm0
	movq	%rdi, %rax
	movl	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	%rdx, 48(%rdi)
	movq	%rdx, 56(%rdi)
	movq	$0, 64(%rdi)
	movups	%xmm0, (%rdi)
	testq	%rcx, %rcx
	je	.L27
	leaq	8(%rcx), %rdx
.L24:
	movq	%rcx, 16(%rax)
	movq	48(%rsi), %rsi
	movq	24(%rcx), %rcx
	movq	%rdx, 8(%rax)
	movq	%rsi, 72(%rax)
	movq	%rcx, (%rax)
	cmpq	%rdx, %rcx
	je	.L25
	movq	32(%rcx), %rdi
	leaq	-1(%rdi), %rdx
	movq	%rdx, 80(%rax)
	movslq	40(%rcx), %rcx
	addq	%rcx, %rdx
	movq	%rdx, 88(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	24(%rdi), %rcx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rsi, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 80(%rax)
	ret
	.cfi_endproc
.LFE8341:
	.size	_ZN2v88internal23InvalidatedSlotsCleanup8OldToNewEPNS0_11MemoryChunkE, .-_ZN2v88internal23InvalidatedSlotsCleanup8OldToNewEPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal23InvalidatedSlotsCleanup9NoCleanupEPNS0_11MemoryChunkE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23InvalidatedSlotsCleanup9NoCleanupEPNS0_11MemoryChunkE
	.type	_ZN2v88internal23InvalidatedSlotsCleanup9NoCleanupEPNS0_11MemoryChunkE, @function
_ZN2v88internal23InvalidatedSlotsCleanup9NoCleanupEPNS0_11MemoryChunkE:
.LFB8348:
	.cfi_startproc
	endbr64
	leaq	32(%rdi), %rdx
	leaq	24(%rdi), %rcx
	movq	%rdi, %rax
	movl	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	%rdx, 48(%rdi)
	movq	%rdx, 56(%rdi)
	movq	$0, 64(%rdi)
	movq	%rdx, (%rdi)
	movq	%rdx, 8(%rdi)
	movq	48(%rsi), %rdx
	movq	%rcx, 16(%rdi)
	movq	%rdx, 72(%rdi)
	movq	%rdx, 80(%rdi)
	movq	%rdx, 88(%rdi)
	ret
	.cfi_endproc
.LFE8348:
	.size	_ZN2v88internal23InvalidatedSlotsCleanup9NoCleanupEPNS0_11MemoryChunkE, .-_ZN2v88internal23InvalidatedSlotsCleanup9NoCleanupEPNS0_11MemoryChunkE
	.section	.text._ZN2v88internal23InvalidatedSlotsCleanupC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23InvalidatedSlotsCleanupC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEE
	.type	_ZN2v88internal23InvalidatedSlotsCleanupC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEE, @function
_ZN2v88internal23InvalidatedSlotsCleanupC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEE:
.LFB8350:
	.cfi_startproc
	endbr64
	leaq	32(%rdi), %rax
	pxor	%xmm0, %xmm0
	movl	$0, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	%rax, 48(%rdi)
	movq	%rax, 56(%rdi)
	movq	$0, 64(%rdi)
	movups	%xmm0, (%rdi)
	testq	%rdx, %rdx
	je	.L34
	leaq	8(%rdx), %rax
.L31:
	movq	%rdx, 16(%rdi)
	movq	48(%rsi), %rcx
	movq	24(%rdx), %rdx
	movq	%rax, 8(%rdi)
	movq	%rcx, 72(%rdi)
	movq	%rdx, (%rdi)
	cmpq	%rax, %rdx
	je	.L32
	movq	32(%rdx), %rax
	subq	$1, %rax
	movq	%rax, 80(%rdi)
	movslq	40(%rdx), %rdx
	addq	%rdx, %rax
	movq	%rax, 88(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	24(%rdi), %rdx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rcx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 80(%rdi)
	ret
	.cfi_endproc
.LFE8350:
	.size	_ZN2v88internal23InvalidatedSlotsCleanupC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEE, .-_ZN2v88internal23InvalidatedSlotsCleanupC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEE
	.globl	_ZN2v88internal23InvalidatedSlotsCleanupC1EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEE
	.set	_ZN2v88internal23InvalidatedSlotsCleanupC1EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEE,_ZN2v88internal23InvalidatedSlotsCleanupC2EPNS0_11MemoryChunkEPSt3mapINS0_10HeapObjectEiNS0_6Object8ComparerESaISt4pairIKS5_iEEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal22InvalidatedSlotsFilter8OldToOldEPNS0_11MemoryChunkE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal22InvalidatedSlotsFilter8OldToOldEPNS0_11MemoryChunkE, @function
_GLOBAL__sub_I__ZN2v88internal22InvalidatedSlotsFilter8OldToOldEPNS0_11MemoryChunkE:
.LFB9896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE9896:
	.size	_GLOBAL__sub_I__ZN2v88internal22InvalidatedSlotsFilter8OldToOldEPNS0_11MemoryChunkE, .-_GLOBAL__sub_I__ZN2v88internal22InvalidatedSlotsFilter8OldToOldEPNS0_11MemoryChunkE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal22InvalidatedSlotsFilter8OldToOldEPNS0_11MemoryChunkE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
