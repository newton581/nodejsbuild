	.file	"gc-tracer.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5205:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5205:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5206:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5206:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv,"axG",@progbits,_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv
	.type	_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv, @function
_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv:
.LFB10088:
	.cfi_startproc
	endbr64
	movq	120(%rdi), %rax
	ret
	.cfi_endproc
.LFE10088:
	.size	_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv, .-_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv
	.section	.text._ZN2v88internal8NewSpace4SizeEv,"axG",@progbits,_ZN2v88internal8NewSpace4SizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8NewSpace4SizeEv
	.type	_ZN2v88internal8NewSpace4SizeEv, @function
_ZN2v88internal8NewSpace4SizeEv:
.LFB10026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movslq	360(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	352(%rbx), %rcx
	movq	104(%rbx), %rdx
	imulq	%rax, %r12
	popq	%rbx
	subq	40(%rcx), %rdx
	leaq	(%rdx,%r12), %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10026:
	.size	_ZN2v88internal8NewSpace4SizeEv, .-_ZN2v88internal8NewSpace4SizeEv
	.section	.text._ZN2v88internal10PagedSpace5WasteEv,"axG",@progbits,_ZN2v88internal10PagedSpace5WasteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10PagedSpace5WasteEv
	.type	_ZN2v88internal10PagedSpace5WasteEv, @function
_ZN2v88internal10PagedSpace5WasteEv:
.LFB9968:
	.cfi_startproc
	endbr64
	movq	96(%rdi), %rax
	movq	24(%rax), %rax
	ret
	.cfi_endproc
.LFE9968:
	.size	_ZN2v88internal10PagedSpace5WasteEv, .-_ZN2v88internal10PagedSpace5WasteEv
	.section	.text._ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv
	.type	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv, @function
_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv:
.LFB20057:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	3368(%rax), %rax
	addq	$50888, %rax
	ret
	.cfi_endproc
.LFE20057:
	.size	_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv, .-_ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv
	.section	.text._ZN2v88internal8GCTracer19RCSCounterFromScopeENS1_5Scope7ScopeIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer19RCSCounterFromScopeENS1_5Scope7ScopeIdE
	.type	_ZN2v88internal8GCTracer19RCSCounterFromScopeENS1_5Scope7ScopeIdE, @function
_ZN2v88internal8GCTracer19RCSCounterFromScopeENS1_5Scope7ScopeIdE:
.LFB20058:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	ret
	.cfi_endproc
.LFE20058:
	.size	_ZN2v88internal8GCTracer19RCSCounterFromScopeENS1_5Scope7ScopeIdE, .-_ZN2v88internal8GCTracer19RCSCounterFromScopeENS1_5Scope7ScopeIdE
	.section	.text._ZN2v88internal8GCTracer29RCSCounterFromBackgroundScopeENS1_15BackgroundScope7ScopeIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer29RCSCounterFromBackgroundScopeENS1_15BackgroundScope7ScopeIdE
	.type	_ZN2v88internal8GCTracer29RCSCounterFromBackgroundScopeENS1_15BackgroundScope7ScopeIdE, @function
_ZN2v88internal8GCTracer29RCSCounterFromBackgroundScopeENS1_15BackgroundScope7ScopeIdE:
.LFB20059:
	.cfi_startproc
	endbr64
	leal	98(%rdi), %eax
	ret
	.cfi_endproc
.LFE20059:
	.size	_ZN2v88internal8GCTracer29RCSCounterFromBackgroundScopeENS1_15BackgroundScope7ScopeIdE, .-_ZN2v88internal8GCTracer29RCSCounterFromBackgroundScopeENS1_15BackgroundScope7ScopeIdE
	.section	.text._ZN2v88internal8GCTracer5ScopeC2EPS1_NS2_7ScopeIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer5ScopeC2EPS1_NS2_7ScopeIdE
	.type	_ZN2v88internal8GCTracer5ScopeC2EPS1_NS2_7ScopeIdE, @function
_ZN2v88internal8GCTracer5ScopeC2EPS1_NS2_7ScopeIdE:
.LFB20061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 56(%rdi)
	movups	%xmm0, 32(%rdi)
	movq	(%rsi), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movsd	%xmm0, 16(%rbx)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L14
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	(%rbx), %rax
	leaq	24(%rbx), %rsi
	movl	%r12d, %edx
	movq	(%rax), %rax
	movq	3368(%rax), %rdi
	addq	$23240, %rdi
	movq	%rdi, 56(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	.cfi_endproc
.LFE20061:
	.size	_ZN2v88internal8GCTracer5ScopeC2EPS1_NS2_7ScopeIdE, .-_ZN2v88internal8GCTracer5ScopeC2EPS1_NS2_7ScopeIdE
	.globl	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE
	.set	_ZN2v88internal8GCTracer5ScopeC1EPS1_NS2_7ScopeIdE,_ZN2v88internal8GCTracer5ScopeC2EPS1_NS2_7ScopeIdE
	.section	.text._ZN2v88internal8GCTracer5ScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer5ScopeD2Ev
	.type	_ZN2v88internal8GCTracer5ScopeD2Ev, @function
_ZN2v88internal8GCTracer5ScopeD2Ev:
.LFB20064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	(%r12), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movslq	8(%rbx), %rax
	subsd	16(%rbx), %xmm0
	cmpl	$9, %eax
	jbe	.L22
	leaq	(%r12,%rax,8), %rax
	addsd	128(%rax), %xmm0
	movsd	%xmm0, 128(%rax)
.L17:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L23
.L15:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	leaq	(%rax,%rax,2), %rax
	leaq	(%r12,%rax,8), %rax
	movsd	2512(%rax), %xmm1
	addl	$1, 2528(%rax)
	comisd	2520(%rax), %xmm0
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 2512(%rax)
	jbe	.L17
	movsd	%xmm0, 2520(%rax)
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L15
.L23:
	leaq	24(%rbx), %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	.cfi_endproc
.LFE20064:
	.size	_ZN2v88internal8GCTracer5ScopeD2Ev, .-_ZN2v88internal8GCTracer5ScopeD2Ev
	.globl	_ZN2v88internal8GCTracer5ScopeD1Ev
	.set	_ZN2v88internal8GCTracer5ScopeD1Ev,_ZN2v88internal8GCTracer5ScopeD2Ev
	.section	.text._ZN2v88internal8GCTracer15BackgroundScopeC2EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer15BackgroundScopeC2EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE
	.type	_ZN2v88internal8GCTracer15BackgroundScopeC2EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE, @function
_ZN2v88internal8GCTracer15BackgroundScopeC2EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE:
.LFB20067:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 48(%rdi)
	movq	%rcx, 56(%rdi)
	movups	%xmm0, 32(%rdi)
	movq	(%rsi), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movsd	%xmm0, 16(%rbx)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L27
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	56(%rbx), %rdi
	leal	98(%r12), %edx
	leaq	24(%rbx), %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	.cfi_endproc
.LFE20067:
	.size	_ZN2v88internal8GCTracer15BackgroundScopeC2EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE, .-_ZN2v88internal8GCTracer15BackgroundScopeC2EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE
	.globl	_ZN2v88internal8GCTracer15BackgroundScopeC1EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE
	.set	_ZN2v88internal8GCTracer15BackgroundScopeC1EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE,_ZN2v88internal8GCTracer15BackgroundScopeC2EPS1_NS2_7ScopeIdEPNS0_16RuntimeCallStatsE
	.section	.text._ZN2v88internal8GCTracer15BackgroundScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer15BackgroundScopeD2Ev
	.type	_ZN2v88internal8GCTracer15BackgroundScopeD2Ev, @function
_ZN2v88internal8GCTracer15BackgroundScopeD2Ev:
.LFB20070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rax
	movq	(%rax), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	(%rbx), %r14
	subsd	16(%rbx), %xmm0
	movslq	8(%rbx), %r12
	leaq	4384(%r14), %r13
	movq	%r13, %rdi
	movsd	%xmm0, -40(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	(%r14,%r12,8), %rax
	movsd	-40(%rbp), %xmm0
	movq	%r13, %rdi
	addsd	4424(%rax), %xmm0
	movsd	%xmm0, 4424(%rax)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L31
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	addq	$16, %rsp
	leaq	24(%rbx), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	.cfi_endproc
.LFE20070:
	.size	_ZN2v88internal8GCTracer15BackgroundScopeD2Ev, .-_ZN2v88internal8GCTracer15BackgroundScopeD2Ev
	.globl	_ZN2v88internal8GCTracer15BackgroundScopeD1Ev
	.set	_ZN2v88internal8GCTracer15BackgroundScopeD1Ev,_ZN2v88internal8GCTracer15BackgroundScopeD2Ev
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"V8.GC_SCAVENGER_BACKGROUND_SCAVENGE_PARALLEL"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"V8.GC_MC_INCREMENTAL"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC2:
	.string	"V8.GC_MC_INCREMENTAL_EMBEDDER_TRACING"
	.align 8
.LC3:
	.string	"V8.GC_MC_INCREMENTAL_EXTERNAL_EPILOGUE"
	.align 8
.LC4:
	.string	"V8.GC_MC_INCREMENTAL_EXTERNAL_PROLOGUE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC5:
	.string	"V8.GC_MC_INCREMENTAL_FINALIZE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC6:
	.string	"V8.GC_MC_INCREMENTAL_FINALIZE_BODY"
	.align 8
.LC7:
	.string	"V8.GC_MC_INCREMENTAL_LAYOUT_CHANGE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC8:
	.string	"V8.GC_MC_INCREMENTAL_START"
.LC9:
	.string	"V8.GC_MC_INCREMENTAL_SWEEPING"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC10:
	.string	"V8.GC_HEAP_EMBEDDER_TRACING_EPILOGUE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC11:
	.string	"V8.GC_HEAP_EPILOGUE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC12:
	.string	"V8.GC_HEAP_EPILOGUE_REDUCE_NEW_SPACE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC13:
	.string	"V8.GC_HEAP_EXTERNAL_EPILOGUE"
.LC14:
	.string	"V8.GC_HEAP_EXTERNAL_PROLOGUE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC15:
	.string	"V8.GC_HEAP_EXTERNAL_WEAK_GLOBAL_HANDLES"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC16:
	.string	"V8.GC_HEAP_PROLOGUE"
.LC17:
	.string	"V8.GC_MC_CLEAR"
.LC18:
	.string	"V8.GC_MC_EPILOGUE"
.LC19:
	.string	"V8.GC_MC_EVACUATE"
.LC20:
	.string	"V8.GC_MC_FINISH"
.LC21:
	.string	"V8.GC_MC_MARK"
.LC22:
	.string	"V8.GC_MC_PROLOGUE"
.LC23:
	.string	"V8.GC_MC_SWEEP"
.LC24:
	.string	"V8.GC_MC_CLEAR_DEPENDENT_CODE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC25:
	.string	"V8.GC_MC_CLEAR_FLUSHABLE_BYTECODE"
	.align 8
.LC26:
	.string	"V8.GC_MC_CLEAR_FLUSHED_JS_FUNCTIONS"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC27:
	.string	"V8.GC_MC_CLEAR_MAPS"
.LC28:
	.string	"V8.GC_MC_CLEAR_SLOTS_BUFFER"
.LC29:
	.string	"V8.GC_MC_CLEAR_STORE_BUFFER"
.LC30:
	.string	"V8.GC_MC_CLEAR_STRING_TABLE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC31:
	.string	"V8.GC_MC_CLEAR_WEAK_COLLECTIONS"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC32:
	.string	"V8.GC_MC_CLEAR_WEAK_LISTS"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC33:
	.string	"V8.GC_MC_CLEAR_WEAK_REFERENCES"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC34:
	.string	"V8.GC_MC_EVACUATE_CANDIDATES"
.LC35:
	.string	"V8.GC_MC_EVACUATE_CLEAN_UP"
.LC36:
	.string	"V8.GC_MC_EVACUATE_COPY"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC37:
	.string	"V8.GC_MC_EVACUATE_COPY_PARALLEL"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC38:
	.string	"V8.GC_MC_EVACUATE_EPILOGUE"
.LC39:
	.string	"V8.GC_MC_EVACUATE_PROLOGUE"
.LC40:
	.string	"V8.GC_MC_EVACUATE_REBALANCE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC41:
	.string	"V8.GC_MC_EVACUATE_UPDATE_POINTERS"
	.align 8
.LC42:
	.string	"V8.GC_MC_EVACUATE_UPDATE_POINTERS_PARALLEL"
	.align 8
.LC43:
	.string	"V8.GC_MC_EVACUATE_UPDATE_POINTERS_SLOTS_MAIN"
	.align 8
.LC44:
	.string	"V8.GC_MC_EVACUATE_UPDATE_POINTERS_SLOTS_MAP_SPACE"
	.align 8
.LC45:
	.string	"V8.GC_MC_EVACUATE_UPDATE_POINTERS_TO_NEW_ROOTS"
	.align 8
.LC46:
	.string	"V8.GC_MC_EVACUATE_UPDATE_POINTERS_WEAK"
	.align 8
.LC47:
	.string	"V8.GC_MC_MARK_EMBEDDER_PROLOGUE"
	.align 8
.LC48:
	.string	"V8.GC_MC_MARK_EMBEDDER_TRACING"
	.align 8
.LC49:
	.string	"V8.GC_MC_MARK_EMBEDDER_TRACING_CLOSURE"
	.align 8
.LC50:
	.string	"V8.GC_MC_MARK_FINISH_INCREMENTAL"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC51:
	.string	"V8.GC_MC_MARK_MAIN"
.LC52:
	.string	"V8.GC_MC_MARK_ROOTS"
.LC53:
	.string	"V8.GC_MC_MARK_WEAK_CLOSURE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC54:
	.string	"V8.GC_MC_MARK_WEAK_CLOSURE_EPHEMERON"
	.align 8
.LC55:
	.string	"V8.GC_MC_MARK_WEAK_CLOSURE_EPHEMERON_MARKING"
	.align 8
.LC56:
	.string	"V8.GC_MC_MARK_WEAK_CLOSURE_EPHEMERON_LINEAR"
	.align 8
.LC57:
	.string	"V8.GC_MC_MARK_WEAK_CLOSURE_WEAK_HANDLES"
	.align 8
.LC58:
	.string	"V8.GC_MC_MARK_WEAK_CLOSURE_WEAK_ROOTS"
	.align 8
.LC59:
	.string	"V8.GC_MC_MARK_WEAK_CLOSURE_HARMONY"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC60:
	.string	"V8.GC_MC_SWEEP_CODE"
.LC61:
	.string	"V8.GC_MC_SWEEP_MAP"
.LC62:
	.string	"V8.GC_MC_SWEEP_OLD"
.LC63:
	.string	"V8.GC_MINOR_MC"
.LC64:
	.string	"V8.GC_MINOR_MC_CLEAR"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC65:
	.string	"V8.GC_MINOR_MC_CLEAR_STRING_TABLE"
	.align 8
.LC66:
	.string	"V8.GC_MINOR_MC_CLEAR_WEAK_LISTS"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC67:
	.string	"V8.GC_MINOR_MC_EVACUATE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC68:
	.string	"V8.GC_MINOR_MC_EVACUATE_CLEAN_UP"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC69:
	.string	"V8.GC_MINOR_MC_EVACUATE_COPY"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC70:
	.string	"V8.GC_MINOR_MC_EVACUATE_COPY_PARALLEL"
	.align 8
.LC71:
	.string	"V8.GC_MINOR_MC_EVACUATE_EPILOGUE"
	.align 8
.LC72:
	.string	"V8.GC_MINOR_MC_EVACUATE_PROLOGUE"
	.align 8
.LC73:
	.string	"V8.GC_MINOR_MC_EVACUATE_REBALANCE"
	.align 8
.LC74:
	.string	"V8.GC_MINOR_MC_EVACUATE_UPDATE_POINTERS"
	.align 8
.LC75:
	.string	"V8.GC_MINOR_MC_EVACUATE_UPDATE_POINTERS_PARALLEL"
	.align 8
.LC76:
	.string	"V8.GC_MINOR_MC_EVACUATE_UPDATE_POINTERS_SLOTS"
	.align 8
.LC77:
	.string	"V8.GC_MINOR_MC_EVACUATE_UPDATE_POINTERS_TO_NEW_ROOTS"
	.align 8
.LC78:
	.string	"V8.GC_MINOR_MC_EVACUATE_UPDATE_POINTERS_WEAK"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC79:
	.string	"V8.GC_MINOR_MC_MARK"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC80:
	.string	"V8.GC_MINOR_MC_MARK_GLOBAL_HANDLES"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC81:
	.string	"V8.GC_MINOR_MC_MARK_PARALLEL"
.LC82:
	.string	"V8.GC_MINOR_MC_MARK_SEED"
.LC83:
	.string	"V8.GC_MINOR_MC_MARK_ROOTS"
.LC84:
	.string	"V8.GC_MINOR_MC_MARK_WEAK"
.LC85:
	.string	"V8.GC_MINOR_MC_MARKING_DEQUE"
.LC86:
	.string	"V8.GC_MINOR_MC_RESET_LIVENESS"
.LC87:
	.string	"V8.GC_MINOR_MC_SWEEPING"
.LC88:
	.string	"V8.GC_SCAVENGER_FAST_PROMOTE"
.LC89:
	.string	"V8.GC_SCAVENGER_SCAVENGE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC90:
	.string	"V8.GC_SCAVENGER_PROCESS_ARRAY_BUFFERS"
	.align 8
.LC91:
	.string	"V8.GC_SCAVENGER_SCAVENGE_WEAK_GLOBAL_HANDLES_IDENTIFY"
	.align 8
.LC92:
	.string	"V8.GC_SCAVENGER_SCAVENGE_WEAK_GLOBAL_HANDLES_PROCESS"
	.align 8
.LC93:
	.string	"V8.GC_SCAVENGER_SCAVENGE_PARALLEL"
	.align 8
.LC94:
	.string	"V8.GC_SCAVENGER_SCAVENGE_ROOTS"
	.align 8
.LC95:
	.string	"V8.GC_SCAVENGER_SCAVENGE_UPDATE_REFS"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC96:
	.string	"V8.GC_SCAVENGER_SCAVENGE_WEAK"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC97:
	.string	"V8.GC_SCAVENGER_SCAVENGE_FINALIZE"
	.align 8
.LC98:
	.string	"V8.GC_BACKGROUND_ARRAY_BUFFER_FREE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC99:
	.string	"V8.GC_BACKGROUND_STORE_BUFFER"
.LC100:
	.string	"V8.GC_BACKGROUND_UNMAPPER"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC101:
	.string	"V8.GC_MC_BACKGROUND_EVACUATE_COPY"
	.align 8
.LC102:
	.string	"V8.GC_MC_BACKGROUND_EVACUATE_UPDATE_POINTERS"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC103:
	.string	"V8.GC_MC_BACKGROUND_MARKING"
.LC104:
	.string	"V8.GC_MC_BACKGROUND_SWEEPING"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.8
	.align 8
.LC105:
	.string	"V8.GC_MINOR_MC_BACKGROUND_EVACUATE_COPY"
	.align 8
.LC106:
	.string	"V8.GC_MINOR_MC_BACKGROUND_EVACUATE_UPDATE_POINTERS"
	.align 8
.LC107:
	.string	"V8.GC_MINOR_MC_BACKGROUND_MARKING"
	.align 8
.LC108:
	.string	"V8.GC_MC_INCREMENTAL_EMBEDDER_PROLOGUE"
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE.str1.1
.LC109:
	.string	"unreachable code"
	.section	.text._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE
	.type	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE, @function
_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE:
.LFB20072:
	.cfi_startproc
	endbr64
	cmpl	$108, %edi
	ja	.L33
	leaq	.L35(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE,"a",@progbits
	.align 4
	.align 4
.L35:
	.long	.L143-.L35
	.long	.L142-.L35
	.long	.L144-.L35
	.long	.L140-.L35
	.long	.L139-.L35
	.long	.L138-.L35
	.long	.L137-.L35
	.long	.L136-.L35
	.long	.L135-.L35
	.long	.L134-.L35
	.long	.L133-.L35
	.long	.L132-.L35
	.long	.L131-.L35
	.long	.L130-.L35
	.long	.L129-.L35
	.long	.L128-.L35
	.long	.L127-.L35
	.long	.L126-.L35
	.long	.L125-.L35
	.long	.L124-.L35
	.long	.L123-.L35
	.long	.L122-.L35
	.long	.L121-.L35
	.long	.L120-.L35
	.long	.L119-.L35
	.long	.L118-.L35
	.long	.L117-.L35
	.long	.L116-.L35
	.long	.L115-.L35
	.long	.L114-.L35
	.long	.L113-.L35
	.long	.L112-.L35
	.long	.L111-.L35
	.long	.L110-.L35
	.long	.L109-.L35
	.long	.L108-.L35
	.long	.L107-.L35
	.long	.L106-.L35
	.long	.L105-.L35
	.long	.L104-.L35
	.long	.L103-.L35
	.long	.L102-.L35
	.long	.L101-.L35
	.long	.L100-.L35
	.long	.L99-.L35
	.long	.L98-.L35
	.long	.L97-.L35
	.long	.L96-.L35
	.long	.L95-.L35
	.long	.L94-.L35
	.long	.L93-.L35
	.long	.L92-.L35
	.long	.L91-.L35
	.long	.L90-.L35
	.long	.L89-.L35
	.long	.L88-.L35
	.long	.L87-.L35
	.long	.L86-.L35
	.long	.L85-.L35
	.long	.L84-.L35
	.long	.L83-.L35
	.long	.L82-.L35
	.long	.L81-.L35
	.long	.L80-.L35
	.long	.L79-.L35
	.long	.L78-.L35
	.long	.L77-.L35
	.long	.L76-.L35
	.long	.L75-.L35
	.long	.L74-.L35
	.long	.L73-.L35
	.long	.L72-.L35
	.long	.L71-.L35
	.long	.L70-.L35
	.long	.L69-.L35
	.long	.L68-.L35
	.long	.L67-.L35
	.long	.L66-.L35
	.long	.L65-.L35
	.long	.L64-.L35
	.long	.L63-.L35
	.long	.L62-.L35
	.long	.L61-.L35
	.long	.L60-.L35
	.long	.L59-.L35
	.long	.L58-.L35
	.long	.L57-.L35
	.long	.L56-.L35
	.long	.L55-.L35
	.long	.L54-.L35
	.long	.L53-.L35
	.long	.L52-.L35
	.long	.L51-.L35
	.long	.L50-.L35
	.long	.L49-.L35
	.long	.L48-.L35
	.long	.L47-.L35
	.long	.L46-.L35
	.long	.L45-.L35
	.long	.L44-.L35
	.long	.L43-.L35
	.long	.L42-.L35
	.long	.L41-.L35
	.long	.L40-.L35
	.long	.L39-.L35
	.long	.L38-.L35
	.long	.L37-.L35
	.long	.L36-.L35
	.long	.L34-.L35
	.section	.text._ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE
.L36:
	leaq	.LC107(%rip), %rax
	ret
.L37:
	leaq	.LC106(%rip), %rax
	ret
.L38:
	leaq	.LC105(%rip), %rax
	ret
.L39:
	leaq	.LC104(%rip), %rax
	ret
.L40:
	leaq	.LC103(%rip), %rax
	ret
.L41:
	leaq	.LC102(%rip), %rax
	ret
.L42:
	leaq	.LC101(%rip), %rax
	ret
.L43:
	leaq	.LC100(%rip), %rax
	ret
.L44:
	leaq	.LC99(%rip), %rax
	ret
.L45:
	leaq	.LC98(%rip), %rax
	ret
.L46:
	leaq	.LC97(%rip), %rax
	ret
.L47:
	leaq	.LC96(%rip), %rax
	ret
.L48:
	leaq	.LC95(%rip), %rax
	ret
.L49:
	leaq	.LC94(%rip), %rax
	ret
.L50:
	leaq	.LC93(%rip), %rax
	ret
.L51:
	leaq	.LC92(%rip), %rax
	ret
.L52:
	leaq	.LC91(%rip), %rax
	ret
.L53:
	leaq	.LC90(%rip), %rax
	ret
.L54:
	leaq	.LC89(%rip), %rax
	ret
.L55:
	leaq	.LC88(%rip), %rax
	ret
.L56:
	leaq	.LC87(%rip), %rax
	ret
.L57:
	leaq	.LC86(%rip), %rax
	ret
.L58:
	leaq	.LC85(%rip), %rax
	ret
.L59:
	leaq	.LC84(%rip), %rax
	ret
.L60:
	leaq	.LC83(%rip), %rax
	ret
.L61:
	leaq	.LC82(%rip), %rax
	ret
.L62:
	leaq	.LC81(%rip), %rax
	ret
.L63:
	leaq	.LC80(%rip), %rax
	ret
.L64:
	leaq	.LC79(%rip), %rax
	ret
.L65:
	leaq	.LC78(%rip), %rax
	ret
.L66:
	leaq	.LC77(%rip), %rax
	ret
.L67:
	leaq	.LC76(%rip), %rax
	ret
.L68:
	leaq	.LC75(%rip), %rax
	ret
.L69:
	leaq	.LC74(%rip), %rax
	ret
.L70:
	leaq	.LC73(%rip), %rax
	ret
.L71:
	leaq	.LC72(%rip), %rax
	ret
.L72:
	leaq	.LC71(%rip), %rax
	ret
.L73:
	leaq	.LC70(%rip), %rax
	ret
.L74:
	leaq	.LC69(%rip), %rax
	ret
.L75:
	leaq	.LC68(%rip), %rax
	ret
.L76:
	leaq	.LC67(%rip), %rax
	ret
.L77:
	leaq	.LC66(%rip), %rax
	ret
.L78:
	leaq	.LC65(%rip), %rax
	ret
.L79:
	leaq	.LC64(%rip), %rax
	ret
.L80:
	leaq	.LC63(%rip), %rax
	ret
.L81:
	leaq	.LC62(%rip), %rax
	ret
.L82:
	leaq	.LC61(%rip), %rax
	ret
.L83:
	leaq	.LC60(%rip), %rax
	ret
.L84:
	leaq	.LC59(%rip), %rax
	ret
.L85:
	leaq	.LC58(%rip), %rax
	ret
.L86:
	leaq	.LC57(%rip), %rax
	ret
.L87:
	leaq	.LC56(%rip), %rax
	ret
.L88:
	leaq	.LC55(%rip), %rax
	ret
.L89:
	leaq	.LC54(%rip), %rax
	ret
.L90:
	leaq	.LC53(%rip), %rax
	ret
.L91:
	leaq	.LC52(%rip), %rax
	ret
.L92:
	leaq	.LC51(%rip), %rax
	ret
.L93:
	leaq	.LC50(%rip), %rax
	ret
.L94:
	leaq	.LC49(%rip), %rax
	ret
.L95:
	leaq	.LC48(%rip), %rax
	ret
.L96:
	leaq	.LC47(%rip), %rax
	ret
.L97:
	leaq	.LC46(%rip), %rax
	ret
.L98:
	leaq	.LC45(%rip), %rax
	ret
.L99:
	leaq	.LC44(%rip), %rax
	ret
.L100:
	leaq	.LC43(%rip), %rax
	ret
.L101:
	leaq	.LC42(%rip), %rax
	ret
.L102:
	leaq	.LC41(%rip), %rax
	ret
.L103:
	leaq	.LC40(%rip), %rax
	ret
.L104:
	leaq	.LC39(%rip), %rax
	ret
.L105:
	leaq	.LC38(%rip), %rax
	ret
.L106:
	leaq	.LC37(%rip), %rax
	ret
.L107:
	leaq	.LC36(%rip), %rax
	ret
.L108:
	leaq	.LC35(%rip), %rax
	ret
.L109:
	leaq	.LC34(%rip), %rax
	ret
.L110:
	leaq	.LC33(%rip), %rax
	ret
.L111:
	leaq	.LC32(%rip), %rax
	ret
.L112:
	leaq	.LC31(%rip), %rax
	ret
.L113:
	leaq	.LC30(%rip), %rax
	ret
.L114:
	leaq	.LC29(%rip), %rax
	ret
.L115:
	leaq	.LC28(%rip), %rax
	ret
.L116:
	leaq	.LC27(%rip), %rax
	ret
.L117:
	leaq	.LC26(%rip), %rax
	ret
.L118:
	leaq	.LC25(%rip), %rax
	ret
.L119:
	leaq	.LC24(%rip), %rax
	ret
.L120:
	leaq	.LC23(%rip), %rax
	ret
.L121:
	leaq	.LC22(%rip), %rax
	ret
.L122:
	leaq	.LC21(%rip), %rax
	ret
.L123:
	leaq	.LC20(%rip), %rax
	ret
.L124:
	leaq	.LC19(%rip), %rax
	ret
.L125:
	leaq	.LC18(%rip), %rax
	ret
.L126:
	leaq	.LC17(%rip), %rax
	ret
.L127:
	leaq	.LC16(%rip), %rax
	ret
.L128:
	leaq	.LC15(%rip), %rax
	ret
.L129:
	leaq	.LC14(%rip), %rax
	ret
.L130:
	leaq	.LC13(%rip), %rax
	ret
.L131:
	leaq	.LC12(%rip), %rax
	ret
.L132:
	leaq	.LC11(%rip), %rax
	ret
.L133:
	leaq	.LC10(%rip), %rax
	ret
.L134:
	leaq	.LC9(%rip), %rax
	ret
.L135:
	leaq	.LC8(%rip), %rax
	ret
.L136:
	leaq	.LC7(%rip), %rax
	ret
.L137:
	leaq	.LC6(%rip), %rax
	ret
.L138:
	leaq	.LC5(%rip), %rax
	ret
.L139:
	leaq	.LC4(%rip), %rax
	ret
.L140:
	leaq	.LC3(%rip), %rax
	ret
.L34:
	leaq	.LC0(%rip), %rax
	ret
.L144:
	leaq	.LC2(%rip), %rax
	ret
.L142:
	leaq	.LC108(%rip), %rax
	ret
.L143:
	leaq	.LC1(%rip), %rax
	ret
.L33:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC109(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20072:
	.size	_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE, .-_ZN2v88internal8GCTracer5Scope4NameENS2_7ScopeIdE
	.section	.text._ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE
	.type	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE, @function
_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE:
.LFB20073:
	.cfi_startproc
	endbr64
	cmpl	$10, %edi
	ja	.L148
	leaq	.L150(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE,"a",@progbits
	.align 4
	.align 4
.L150:
	.long	.L160-.L150
	.long	.L159-.L150
	.long	.L161-.L150
	.long	.L157-.L150
	.long	.L156-.L150
	.long	.L155-.L150
	.long	.L154-.L150
	.long	.L153-.L150
	.long	.L152-.L150
	.long	.L151-.L150
	.long	.L149-.L150
	.section	.text._ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE
	.p2align 4,,10
	.p2align 3
.L151:
	leaq	.LC107(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	leaq	.LC0(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	.LC98(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	leaq	.LC99(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	.LC100(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	leaq	.LC101(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	leaq	.LC102(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	leaq	.LC103(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	leaq	.LC104(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	leaq	.LC105(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	leaq	.LC106(%rip), %rax
	ret
.L148:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC109(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20073:
	.size	_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE, .-_ZN2v88internal8GCTracer15BackgroundScope4NameENS2_7ScopeIdE
	.section	.text._ZN2v88internal8GCTracer5EventC2ENS2_4TypeENS0_23GarbageCollectionReasonEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer5EventC2ENS2_4TypeENS0_23GarbageCollectionReasonEPKc
	.type	_ZN2v88internal8GCTracer5EventC2ENS2_4TypeENS0_23GarbageCollectionReasonEPKc, @function
_ZN2v88internal8GCTracer5EventC2ENS2_4TypeENS0_23GarbageCollectionReasonEPKc:
.LFB20075:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	%rcx, 8(%rdi)
	movq	%rdi, %rax
	movl	%esi, (%rdi)
	leaq	128(%rdi), %rdi
	movl	%edx, -124(%rdi)
	movups	%xmm0, -112(%rdi)
	movb	$0, -96(%rdi)
	movups	%xmm1, -88(%rdi)
	movups	%xmm1, -72(%rdi)
	movups	%xmm1, -56(%rdi)
	movups	%xmm1, -40(%rdi)
	movq	$0, -24(%rdi)
	movq	$0x000000000, -16(%rdi)
	movups	%xmm0, 864(%rdi)
	movl	$0, 880(%rdi)
	movups	%xmm0, 888(%rdi)
	movl	$0, 904(%rdi)
	movups	%xmm0, 912(%rdi)
	movl	$0, 928(%rdi)
	movups	%xmm0, 936(%rdi)
	movl	$0, 952(%rdi)
	movups	%xmm0, 960(%rdi)
	movl	$0, 976(%rdi)
	movups	%xmm0, 984(%rdi)
	movl	$0, 1000(%rdi)
	movups	%xmm0, 1008(%rdi)
	movl	$0, 1024(%rdi)
	movups	%xmm0, 1032(%rdi)
	movl	$0, 1048(%rdi)
	movups	%xmm0, 1056(%rdi)
	movl	$0, 1072(%rdi)
	movups	%xmm0, 1080(%rdi)
	movl	$0, 1096(%rdi)
	movq	$0, -8(%rdi)
	movq	$0, 856(%rdi)
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	992(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	ret
	.cfi_endproc
.LFE20075:
	.size	_ZN2v88internal8GCTracer5EventC2ENS2_4TypeENS0_23GarbageCollectionReasonEPKc, .-_ZN2v88internal8GCTracer5EventC2ENS2_4TypeENS0_23GarbageCollectionReasonEPKc
	.globl	_ZN2v88internal8GCTracer5EventC1ENS2_4TypeENS0_23GarbageCollectionReasonEPKc
	.set	_ZN2v88internal8GCTracer5EventC1ENS2_4TypeENS0_23GarbageCollectionReasonEPKc,_ZN2v88internal8GCTracer5EventC2ENS2_4TypeENS0_23GarbageCollectionReasonEPKc
	.section	.rodata._ZNK2v88internal8GCTracer5Event8TypeNameEb.str1.1,"aMS",@progbits,1
.LC111:
	.string	"Unknown Event Type"
.LC112:
	.string	"st"
.LC113:
	.string	"Scavenge"
.LC114:
	.string	"Minor Mark-Compact"
.LC115:
	.string	"Mark-sweep"
.LC116:
	.string	"mmc"
.LC117:
	.string	"ms"
.LC118:
	.string	"s"
.LC119:
	.string	"Start"
	.section	.text._ZNK2v88internal8GCTracer5Event8TypeNameEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer5Event8TypeNameEb
	.type	_ZNK2v88internal8GCTracer5Event8TypeNameEb, @function
_ZNK2v88internal8GCTracer5Event8TypeNameEb:
.LFB20077:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	cmpl	$3, %eax
	je	.L166
	ja	.L167
	testl	%eax, %eax
	je	.L168
	testb	%sil, %sil
	leaq	.LC115(%rip), %rdx
	leaq	.LC117(%rip), %rax
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	cmpl	$4, %eax
	jne	.L175
	testb	%sil, %sil
	leaq	.LC119(%rip), %rdx
	leaq	.LC112(%rip), %rax
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	.LC111(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	testb	%sil, %sil
	leaq	.LC113(%rip), %rdx
	leaq	.LC118(%rip), %rax
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	testb	%sil, %sil
	leaq	.LC114(%rip), %rdx
	leaq	.LC116(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE20077:
	.size	_ZNK2v88internal8GCTracer5Event8TypeNameEb, .-_ZNK2v88internal8GCTracer5Event8TypeNameEb
	.section	.text._ZN2v88internal8GCTracerC2EPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracerC2EPNS0_4HeapE
	.type	_ZN2v88internal8GCTracerC2EPNS0_4HeapE, @function
_ZN2v88internal8GCTracerC2EPNS0_4HeapE:
.LFB20079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	136(%rdi), %rdi
	movl	%ebx, %edx
	subq	$8, %rsp
	movups	%xmm1, -88(%rdi)
	movups	%xmm1, -72(%rdi)
	movups	%xmm1, -56(%rdi)
	movups	%xmm1, -40(%rdi)
	movq	%rsi, -136(%rdi)
	leaq	8(%rbx), %rsi
	movups	%xmm0, -112(%rdi)
	movups	%xmm0, 864(%rdi)
	movups	%xmm0, 888(%rdi)
	movups	%xmm0, 912(%rdi)
	movups	%xmm0, 936(%rdi)
	movups	%xmm0, 960(%rdi)
	movups	%xmm0, 984(%rdi)
	movups	%xmm0, 1008(%rdi)
	movups	%xmm0, 1032(%rdi)
	movups	%xmm0, 1056(%rdi)
	movq	$4, -128(%rdi)
	movq	$0, -120(%rdi)
	movb	$0, -96(%rdi)
	movq	$0, -24(%rdi)
	movq	$0x000000000, -16(%rdi)
	movl	$0, 880(%rdi)
	movl	$0, 904(%rdi)
	movl	$0, 928(%rdi)
	movl	$0, 952(%rdi)
	movl	$0, 976(%rdi)
	movl	$0, 1000(%rdi)
	movl	$0, 1024(%rdi)
	movl	$0, 1048(%rdi)
	movl	$0, 1072(%rdi)
	movups	%xmm0, 1080(%rdi)
	movl	$0, 1096(%rdi)
	movq	$0, -8(%rdi)
	movq	$0, 856(%rdi)
	andq	$-8, %rdi
	subl	%edi, %edx
	leal	1000(%rdx), %ecx
	shrl	$3, %ecx
	rep stosq
	leaq	1240(%rbx), %rdi
	movl	$154, %ecx
	rep movsq
	movups	%xmm0, 2536(%rbx)
	movl	$20, %ecx
	leaq	2864(%rbx), %rdi
	movups	%xmm0, 2560(%rbx)
	movups	%xmm0, 2584(%rbx)
	movups	%xmm0, 2608(%rbx)
	movups	%xmm0, 2632(%rbx)
	movups	%xmm0, 2656(%rbx)
	movups	%xmm0, 2680(%rbx)
	movups	%xmm0, 2704(%rbx)
	movups	%xmm0, 2728(%rbx)
	movq	$0, 2472(%rbx)
	movq	$0x000000000, 2480(%rbx)
	movq	$0x000000000, 2488(%rbx)
	movq	$0x000000000, 2496(%rbx)
	movq	$0x000000000, 2504(%rbx)
	movq	$0x000000000, 2512(%rbx)
	movq	$0x000000000, 2520(%rbx)
	movl	$0, 2528(%rbx)
	movl	$0, 2552(%rbx)
	movl	$0, 2576(%rbx)
	movl	$0, 2600(%rbx)
	movl	$0, 2624(%rbx)
	movl	$0, 2648(%rbx)
	movl	$0, 2672(%rbx)
	movl	$0, 2696(%rbx)
	movl	$0, 2720(%rbx)
	movl	$0, 2744(%rbx)
	movq	$0x000000000, 2752(%rbx)
	movups	%xmm0, 2832(%rbx)
	movq	.LC120(%rip), %xmm0
	movups	%xmm1, 2760(%rbx)
	movups	%xmm1, 2792(%rbx)
	movups	%xmm0, 2848(%rbx)
	movq	$0, 2776(%rbx)
	movq	$0x000000000, 2784(%rbx)
	movq	$0, 2808(%rbx)
	movq	$0x000000000, 2816(%rbx)
	movl	$0, 2824(%rbx)
	rep stosq
	leaq	3032(%rbx), %rdi
	movl	$20, %ecx
	movq	$0, 3024(%rbx)
	rep stosq
	leaq	3200(%rbx), %rdi
	movl	$20, %ecx
	movq	$0, 3192(%rbx)
	rep stosq
	leaq	3368(%rbx), %rdi
	movl	$20, %ecx
	movq	$0, 3360(%rbx)
	rep stosq
	leaq	3536(%rbx), %rdi
	movl	$20, %ecx
	movq	$0, 3528(%rbx)
	rep stosq
	leaq	3704(%rbx), %rdi
	movl	$20, %ecx
	movq	$0, 3696(%rbx)
	rep stosq
	leaq	3872(%rbx), %rdi
	movl	$20, %ecx
	movq	$0, 3864(%rbx)
	rep stosq
	leaq	4040(%rbx), %rdi
	movl	$20, %ecx
	movq	$0, 4032(%rbx)
	rep stosq
	leaq	4384(%rbx), %rdi
	movq	$0, 4200(%rbx)
	movq	$0, 4288(%rbx)
	movq	$0, 4376(%rbx)
	call	_ZN2v84base5MutexC1Ev@PLT
	movq	(%rbx), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	leaq	4432(%rbx), %rdi
	xorl	%eax, %eax
	movq	$0, 4424(%rbx)
	andq	$-8, %rdi
	movsd	%xmm0, 32(%rbx)
	movq	$0, 4504(%rbx)
	subl	%edi, %ebx
	leal	4512(%rbx), %ecx
	shrl	$3, %ecx
	rep stosq
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20079:
	.size	_ZN2v88internal8GCTracerC2EPNS0_4HeapE, .-_ZN2v88internal8GCTracerC2EPNS0_4HeapE
	.globl	_ZN2v88internal8GCTracerC1EPNS0_4HeapE
	.set	_ZN2v88internal8GCTracerC1EPNS0_4HeapE,_ZN2v88internal8GCTracerC2EPNS0_4HeapE
	.section	.text._ZN2v88internal8GCTracer15ResetForTestingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer15ResetForTestingEv
	.type	_ZN2v88internal8GCTracer15ResetForTestingEv, @function
_ZN2v88internal8GCTracer15ResetForTestingEv:
.LFB20081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	movl	$109, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	leaq	-1160(%rbp), %rdx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	leaq	-1280(%rbp), %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	leaq	4384(%rbx), %r13
	subq	$1256, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -1264(%rbp)
	movabsq	$90194313220, %rax
	movq	%rax, -1280(%rbp)
	movq	%r12, %rax
	movups	%xmm2, -1240(%rbp)
	movups	%xmm2, -1224(%rbp)
	movups	%xmm2, -1208(%rbp)
	movups	%xmm2, -1192(%rbp)
	movaps	%xmm1, -288(%rbp)
	movups	%xmm1, -264(%rbp)
	movaps	%xmm1, -240(%rbp)
	movups	%xmm1, -216(%rbp)
	movaps	%xmm1, -192(%rbp)
	movups	%xmm1, -168(%rbp)
	movaps	%xmm1, -144(%rbp)
	movups	%xmm1, -120(%rbp)
	movq	$0, -1272(%rbp)
	movb	$0, -1248(%rbp)
	movq	$0, -1176(%rbp)
	movq	$0x000000000, -1168(%rbp)
	movl	$0, -272(%rbp)
	movl	$0, -248(%rbp)
	movl	$0, -224(%rbp)
	movl	$0, -200(%rbp)
	movl	$0, -176(%rbp)
	movl	$0, -152(%rbp)
	movl	$0, -128(%rbp)
	movl	$0, -104(%rbp)
	rep stosq
	movaps	%xmm1, -96(%rbp)
	leaq	8(%rbx), %rdi
	movl	$154, %ecx
	movups	%xmm1, -72(%rbp)
	movl	$0, -80(%rbp)
	movl	$0, -56(%rbp)
	rep movsq
	movq	(%rbx), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	pxor	%xmm1, %xmm1
	pxor	%xmm2, %xmm2
	leaq	1240(%rbx), %rdi
	movsd	%xmm0, 32(%rbx)
	leaq	8(%rbx), %rsi
	movl	$154, %ecx
	rep movsq
	movups	%xmm1, 2512(%rbx)
	movq	%r13, %rdi
	movups	%xmm1, 2536(%rbx)
	movups	%xmm1, 2560(%rbx)
	movups	%xmm1, 2584(%rbx)
	movups	%xmm1, 2608(%rbx)
	movups	%xmm1, 2632(%rbx)
	movups	%xmm1, 2656(%rbx)
	movups	%xmm1, 2680(%rbx)
	movups	%xmm1, 2704(%rbx)
	movups	%xmm1, 2728(%rbx)
	movups	%xmm2, 2760(%rbx)
	movups	%xmm2, 2792(%rbx)
	movq	$0, 2472(%rbx)
	movq	$0x000000000, 2480(%rbx)
	movl	$0, 2528(%rbx)
	movl	$0, 2552(%rbx)
	movl	$0, 2576(%rbx)
	movl	$0, 2600(%rbx)
	movl	$0, 2624(%rbx)
	movl	$0, 2648(%rbx)
	movl	$0, 2672(%rbx)
	movl	$0, 2696(%rbx)
	movl	$0, 2720(%rbx)
	movl	$0, 2744(%rbx)
	movq	$0x000000000, 2752(%rbx)
	movq	$0x000000000, 2784(%rbx)
	movq	$0x000000000, 2816(%rbx)
	movq	$0, 3024(%rbx)
	movq	$0, 3192(%rbx)
	movq	$0, 3360(%rbx)
	movq	.LC120(%rip), %xmm0
	movq	$0, 3696(%rbx)
	movq	$0, 3528(%rbx)
	movq	$0, 3864(%rbx)
	movq	$0, 4032(%rbx)
	movq	$0, 4200(%rbx)
	movq	$0, 4288(%rbx)
	movq	$0, 4376(%rbx)
	movl	$0, 2824(%rbx)
	movups	%xmm1, 2832(%rbx)
	movups	%xmm0, 2848(%rbx)
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	4432(%rbx), %rdi
	movq	%r12, %rax
	movq	$0, 4424(%rbx)
	movq	$0, 4504(%rbx)
	andq	$-8, %rdi
	subl	%edi, %ebx
	leal	4512(%rbx), %ecx
	shrl	$3, %ecx
	rep stosq
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L182
	addq	$1256, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
.L182:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20081:
	.size	_ZN2v88internal8GCTracer15ResetForTestingEv, .-_ZN2v88internal8GCTracer15ResetForTestingEv
	.section	.text._ZN2v88internal8GCTracer29NotifyYoungGenerationHandlingENS0_23YoungGenerationHandlingE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer29NotifyYoungGenerationHandlingENS0_23YoungGenerationHandlingE
	.type	_ZN2v88internal8GCTracer29NotifyYoungGenerationHandlingENS0_23YoungGenerationHandlingE, @function
_ZN2v88internal8GCTracer29NotifyYoungGenerationHandlingENS0_23YoungGenerationHandlingE:
.LFB20082:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	3368(%rax), %rdi
	addq	$928, %rdi
	jmp	_ZN2v88internal9Histogram9AddSampleEi@PLT
	.cfi_endproc
.LFE20082:
	.size	_ZN2v88internal8GCTracer29NotifyYoungGenerationHandlingENS0_23YoungGenerationHandlingE, .-_ZN2v88internal8GCTracer29NotifyYoungGenerationHandlingENS0_23YoungGenerationHandlingE
	.section	.rodata._ZN2v88internal8GCTracer5StartENS0_16GarbageCollectorENS0_23GarbageCollectionReasonEPKc.str1.8,"aMS",@progbits,1
	.align 8
.LC121:
	.string	"FreeLists statistics before collection:\n"
	.section	.text._ZN2v88internal8GCTracer5StartENS0_16GarbageCollectorENS0_23GarbageCollectionReasonEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer5StartENS0_16GarbageCollectorENS0_23GarbageCollectionReasonEPKc
	.type	_ZN2v88internal8GCTracer5StartENS0_16GarbageCollectorENS0_23GarbageCollectionReasonEPKc, @function
_ZN2v88internal8GCTracer5StartENS0_16GarbageCollectorENS0_23GarbageCollectionReasonEPKc:
.LFB20083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1336, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -1316(%rbp)
	movl	%edx, -1320(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	2824(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 2824(%rdi)
	cmpl	$1, %eax
	je	.L224
.L184:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L225
	addq	$1336, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L224:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	%rcx, %r15
	leaq	1240(%rdi), %rdi
	movl	$154, %ecx
	leaq	8(%r12), %rsi
	movq	%rsi, -1368(%rbp)
	rep movsq
	movq	(%r12), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	(%r12), %rdi
	movsd	%xmm0, -1328(%rbp)
	call	_ZNK2v88internal4Heap25EmbedderAllocationCounterEv@PLT
	movq	(%r12), %r13
	movq	%rax, -1344(%rbp)
	movq	%r13, %rdi
	movq	2152(%r13), %rbx
	call	_ZN2v88internal4Heap26OldGenerationSizeOfObjectsEv@PLT
	movq	(%r12), %r9
	subq	2160(%r13), %rbx
	addq	%rbx, %rax
	movq	248(%r9), %rsi
	movq	%rax, -1352(%rbp)
	movq	2144(%r9), %rax
	movq	336(%rsi), %rdx
	movq	104(%rsi), %rcx
	movq	%rsi, -1360(%rbp)
	movq	%rax, -1336(%rbp)
	leaq	-8(%rcx), %r13
	leaq	-8(%rdx), %rax
	movq	%rcx, %rbx
	andq	$-262144, %rax
	andq	$-262144, %r13
	subq	%rdx, %rbx
	cmpq	%r13, %rax
	je	.L190
	movq	48(%rax), %rbx
	movq	224(%rax), %r14
	subq	%rdx, %rbx
	cmpq	%r14, %r13
	je	.L188
	.p2align 4,,10
	.p2align 3
.L189:
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	224(%r14), %r14
	addq	%rax, %rbx
	cmpq	%r14, %r13
	jne	.L189
	movq	-1360(%rbp), %rax
	movq	(%r12), %r9
	movq	104(%rax), %rcx
.L188:
	addq	%rcx, %rbx
	subq	40(%r14), %rbx
.L190:
	movsd	2752(%r12), %xmm1
	addq	-1336(%rbp), %rbx
	ucomisd	.LC110(%rip), %xmm1
	jnp	.L226
.L191:
	movsd	-1328(%rbp), %xmm3
	movq	%rbx, %xmm0
	movq	-1344(%rbp), %rax
	movq	2776(%r12), %rdx
	movhps	-1352(%rbp), %xmm0
	movdqu	2760(%r12), %xmm2
	movsd	%xmm3, 2752(%r12)
	subsd	%xmm1, %xmm3
	movsd	2784(%r12), %xmm1
	movq	%rax, 2776(%r12)
	addq	2808(%r12), %rax
	movups	%xmm0, 2760(%r12)
	subq	%rdx, %rax
	addsd	%xmm3, %xmm1
	movq	%rax, 2808(%r12)
	movsd	%xmm1, 2784(%r12)
	movdqu	2792(%r12), %xmm1
	psubq	%xmm2, %xmm1
	paddq	%xmm1, %xmm0
	movups	%xmm0, 2792(%r12)
.L193:
	movl	-1316(%rbp), %eax
	cmpl	$1, %eax
	je	.L194
	cmpl	$2, %eax
	je	.L195
	testl	%eax, %eax
	je	.L227
.L196:
	movl	2756(%r9), %eax
	movq	%r9, %rdi
	leaq	-1312(%rbp), %r14
	xorl	%ebx, %ebx
	movsd	-1328(%rbp), %xmm4
	movb	%al, 40(%r12)
	andb	$1, 40(%r12)
	movsd	%xmm4, 24(%r12)
	call	_ZN2v88internal4Heap13SizeOfObjectsEv@PLT
	movq	%r14, %rdi
	movq	%rax, 48(%r12)
	movq	(%r12), %rax
	movq	2048(%rax), %rax
	movq	80(%rax), %rax
	movq	%rax, 64(%r12)
	movl	$2, -1304(%rbp)
	movq	(%r12), %rax
	movq	%rax, -1312(%rbp)
	call	_ZN2v88internal18PagedSpaceIterator4NextEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L198
	leaq	_ZN2v88internal10PagedSpace5WasteEv(%rip), %r15
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L228:
	movq	96(%rdi), %rax
	movq	24(%rax), %r13
.L223:
	movq	(%rdi), %rax
	call	*96(%rax)
	movq	%r14, %rdi
	addq	%rax, %r13
	call	_ZN2v88internal18PagedSpaceIterator4NextEv@PLT
	addq	%r13, %rbx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L198
.L202:
	movq	(%rdi), %rax
	movq	144(%rax), %rax
	cmpq	%r15, %rax
	je	.L228
	movq	%rdi, -1328(%rbp)
	call	*%rax
	movq	-1328(%rbp), %rdi
	movq	%rax, %r13
	jmp	.L223
.L198:
	movq	(%r12), %rax
	movq	%rbx, 80(%r12)
	leaq	_ZN2v88internal8NewSpace4SizeEv(%rip), %rdx
	movq	248(%rax), %r14
	movq	(%r14), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L203
	movslq	360(%r14), %r13
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	352(%r14), %rdx
	movq	104(%r14), %rbx
	imulq	%rax, %r13
	subq	40(%rdx), %rbx
	leaq	(%rbx,%r13), %rbx
.L204:
	movq	(%r12), %rdx
	leaq	_ZN2v88internal16LargeObjectSpace13SizeOfObjectsEv(%rip), %rcx
	movq	296(%rdx), %rdi
	movq	(%rdi), %rax
	movq	80(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L205
	movq	120(%rdi), %rax
.L206:
	leaq	136(%r12), %rdi
	addq	%rax, %rbx
	movl	%r12d, %eax
	movq	$0, 112(%r12)
	andq	$-8, %rdi
	movq	%rbx, 96(%r12)
	movq	$0x000000000, 120(%r12)
	subl	%edi, %eax
	leal	1000(%rax), %ecx
	xorl	%eax, %eax
	movq	$0, 128(%r12)
	movq	$0, 992(%r12)
	shrl	$3, %ecx
	rep stosq
	movq	3368(%rdx), %rax
	testl	$-3, -1316(%rbp)
	je	.L229
	movl	-1320(%rbp), %esi
	leaq	368(%rax), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cmpb	$0, _ZN2v88internal23FLAG_trace_gc_freelistsE(%rip)
	je	.L184
	movq	(%r12), %rax
	leaq	.LC121(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	movq	(%r12), %rdi
	call	_ZN2v88internal4Heap19PrintFreeListsStatsEv@PLT
	jmp	.L184
.L229:
	movl	-1320(%rbp), %esi
	leaq	888(%rax), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	jmp	.L184
.L226:
	jne	.L191
	movsd	-1328(%rbp), %xmm5
	movq	-1344(%rbp), %rax
	movq	%rbx, %xmm0
	movhps	-1352(%rbp), %xmm0
	movq	%rax, 2776(%r12)
	movsd	%xmm5, 2752(%r12)
	movups	%xmm0, 2760(%r12)
	jmp	.L193
.L227:
	movl	$0, -1296(%rbp)
.L220:
	movl	-1320(%rbp), %eax
	leaq	-1176(%rbp), %rdx
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movl	$109, %ecx
	movq	%rdx, %rdi
	movq	%r15, -1288(%rbp)
	leaq	-1296(%rbp), %rsi
	movl	%eax, -1292(%rbp)
	xorl	%eax, %eax
	movb	$0, -1264(%rbp)
	movq	$0, -1192(%rbp)
	movq	$0x000000000, -1184(%rbp)
	movl	$0, -288(%rbp)
	movl	$0, -264(%rbp)
	movl	$0, -240(%rbp)
	movl	$0, -216(%rbp)
	movl	$0, -192(%rbp)
	movl	$0, -168(%rbp)
	movl	$0, -144(%rbp)
	movl	$0, -120(%rbp)
	movl	$0, -96(%rbp)
	movl	$0, -72(%rbp)
	movaps	%xmm0, -1280(%rbp)
	movups	%xmm1, -1256(%rbp)
	movups	%xmm1, -1240(%rbp)
	movups	%xmm1, -1224(%rbp)
	movups	%xmm1, -1208(%rbp)
	movaps	%xmm0, -304(%rbp)
	movups	%xmm0, -280(%rbp)
	movaps	%xmm0, -256(%rbp)
	movups	%xmm0, -232(%rbp)
	movaps	%xmm0, -208(%rbp)
	movups	%xmm0, -184(%rbp)
	movaps	%xmm0, -160(%rbp)
	movups	%xmm0, -136(%rbp)
	movaps	%xmm0, -112(%rbp)
	movups	%xmm0, -88(%rbp)
	rep stosq
	movl	$154, %ecx
	movq	-1368(%rbp), %rdi
	rep movsq
	jmp	.L196
.L195:
	movl	$3, -1296(%rbp)
	jmp	.L220
.L194:
	movq	2064(%r9), %rdi
	call	_ZN2v88internal18IncrementalMarking12WasActivatedEv@PLT
	testb	%al, %al
	jne	.L230
	movl	$1, -1296(%rbp)
.L221:
	movl	-1320(%rbp), %eax
	leaq	-1176(%rbp), %rdx
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movl	$109, %ecx
	movq	%rdx, %rdi
	movq	%r15, -1288(%rbp)
	leaq	-1296(%rbp), %rsi
	movl	%eax, -1292(%rbp)
	xorl	%eax, %eax
	movb	$0, -1264(%rbp)
	movq	$0, -1192(%rbp)
	movq	$0x000000000, -1184(%rbp)
	movl	$0, -288(%rbp)
	movl	$0, -264(%rbp)
	movl	$0, -240(%rbp)
	movl	$0, -216(%rbp)
	movl	$0, -192(%rbp)
	movl	$0, -168(%rbp)
	movl	$0, -144(%rbp)
	movl	$0, -120(%rbp)
	movl	$0, -96(%rbp)
	movl	$0, -72(%rbp)
	movaps	%xmm0, -1280(%rbp)
	movups	%xmm1, -1256(%rbp)
	movups	%xmm1, -1240(%rbp)
	movups	%xmm1, -1224(%rbp)
	movups	%xmm1, -1208(%rbp)
	movaps	%xmm0, -304(%rbp)
	movups	%xmm0, -280(%rbp)
	movaps	%xmm0, -256(%rbp)
	movups	%xmm0, -232(%rbp)
	movaps	%xmm0, -208(%rbp)
	movups	%xmm0, -184(%rbp)
	movaps	%xmm0, -160(%rbp)
	movups	%xmm0, -136(%rbp)
	movaps	%xmm0, -112(%rbp)
	movups	%xmm0, -88(%rbp)
	rep stosq
	movl	$154, %ecx
	movq	-1368(%rbp), %rdi
	rep movsq
	movq	(%r12), %r9
	jmp	.L196
.L203:
	movq	%r14, %rdi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L204
.L205:
	call	*%rax
	movq	(%r12), %rdx
	jmp	.L206
.L230:
	movl	$2, -1296(%rbp)
	jmp	.L221
.L225:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20083:
	.size	_ZN2v88internal8GCTracer5StartENS0_16GarbageCollectorENS0_23GarbageCollectionReasonEPKc, .-_ZN2v88internal8GCTracer5StartENS0_16GarbageCollectorENS0_23GarbageCollectionReasonEPKc
	.section	.text._ZN2v88internal8GCTracer31ResetIncrementalMarkingCountersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer31ResetIncrementalMarkingCountersEv
	.type	_ZN2v88internal8GCTracer31ResetIncrementalMarkingCountersEv, @function
_ZN2v88internal8GCTracer31ResetIncrementalMarkingCountersEv:
.LFB20084:
	.cfi_startproc
	endbr64
	movq	$0, 2472(%rdi)
	pxor	%xmm0, %xmm0
	movq	$0x000000000, 2480(%rdi)
	movl	$0, 2528(%rdi)
	movl	$0, 2552(%rdi)
	movl	$0, 2576(%rdi)
	movl	$0, 2600(%rdi)
	movl	$0, 2624(%rdi)
	movl	$0, 2648(%rdi)
	movl	$0, 2672(%rdi)
	movl	$0, 2696(%rdi)
	movl	$0, 2720(%rdi)
	movl	$0, 2744(%rdi)
	movups	%xmm0, 2512(%rdi)
	movups	%xmm0, 2536(%rdi)
	movups	%xmm0, 2560(%rdi)
	movups	%xmm0, 2584(%rdi)
	movups	%xmm0, 2608(%rdi)
	movups	%xmm0, 2632(%rdi)
	movups	%xmm0, 2656(%rdi)
	movups	%xmm0, 2680(%rdi)
	movups	%xmm0, 2704(%rdi)
	movups	%xmm0, 2728(%rdi)
	ret
	.cfi_endproc
.LFE20084:
	.size	_ZN2v88internal8GCTracer31ResetIncrementalMarkingCountersEv, .-_ZN2v88internal8GCTracer31ResetIncrementalMarkingCountersEv
	.section	.rodata._ZN2v88internal8GCTracer23NotifySweepingCompletedEv.str1.8,"aMS",@progbits,1
	.align 8
.LC122:
	.string	"FreeLists statistics after sweeping completed:\n"
	.section	.text._ZN2v88internal8GCTracer23NotifySweepingCompletedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer23NotifySweepingCompletedEv
	.type	_ZN2v88internal8GCTracer23NotifySweepingCompletedEv, @function
_ZN2v88internal8GCTracer23NotifySweepingCompletedEv:
.LFB20086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, _ZN2v88internal23FLAG_trace_gc_freelistsE(%rip)
	jne	.L236
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	jne	.L237
.L232:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	.LC122(%rip), %rsi
	leaq	-37592(%rax), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal12PrintIsolateEPvPKcz@PLT
	movq	(%rbx), %rdi
	call	_ZN2v88internal4Heap19PrintFreeListsStatsEv@PLT
	cmpb	$0, _ZN2v88internal30FLAG_trace_allocations_originsE(%rip)
	je	.L232
.L237:
	movq	(%rbx), %rax
	movq	248(%rax), %rdi
	call	_ZN2v88internal19SpaceWithLinearArea23PrintAllocationsOriginsEv@PLT
	movq	(%rbx), %rax
	movq	256(%rax), %rdi
	call	_ZN2v88internal19SpaceWithLinearArea23PrintAllocationsOriginsEv@PLT
	movq	(%rbx), %rax
	movq	264(%rax), %rdi
	call	_ZN2v88internal19SpaceWithLinearArea23PrintAllocationsOriginsEv@PLT
	movq	(%rbx), %rax
	movq	272(%rax), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal19SpaceWithLinearArea23PrintAllocationsOriginsEv@PLT
	.cfi_endproc
.LFE20086:
	.size	_ZN2v88internal8GCTracer23NotifySweepingCompletedEv, .-_ZN2v88internal8GCTracer23NotifySweepingCompletedEv
	.section	.text._ZN2v88internal8GCTracer16SampleAllocationEdmmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer16SampleAllocationEdmmm
	.type	_ZN2v88internal8GCTracer16SampleAllocationEdmmm, @function
_ZN2v88internal8GCTracer16SampleAllocationEdmmm:
.LFB20087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdx, -8(%rbp)
	movsd	2752(%rdi), %xmm2
	ucomisd	.LC110(%rip), %xmm2
	jnp	.L244
.L239:
	movq	2776(%rdi), %rax
	movdqu	2792(%rdi), %xmm4
	movhps	-8(%rbp), %xmm1
	movq	%rcx, 2776(%rdi)
	movdqu	2760(%rdi), %xmm3
	movsd	%xmm0, 2752(%rdi)
	subsd	%xmm2, %xmm0
	addsd	2784(%rdi), %xmm0
	subq	%rax, %rcx
	addq	%rcx, 2808(%rdi)
	movups	%xmm1, 2760(%rdi)
	paddq	%xmm4, %xmm1
	psubq	%xmm3, %xmm1
	movsd	%xmm0, 2784(%rdi)
	movups	%xmm1, 2792(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	jne	.L239
	movq	%rcx, 2776(%rdi)
	movhps	-8(%rbp), %xmm1
	movsd	%xmm0, 2752(%rdi)
	movups	%xmm1, 2760(%rdi)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20087:
	.size	_ZN2v88internal8GCTracer16SampleAllocationEdmmm, .-_ZN2v88internal8GCTracer16SampleAllocationEdmmm
	.section	.text._ZN2v88internal8GCTracer13AddAllocationEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer13AddAllocationEd
	.type	_ZN2v88internal8GCTracer13AddAllocationEd, @function
_ZN2v88internal8GCTracer13AddAllocationEd:
.LFB20088:
	.cfi_startproc
	endbr64
	movsd	%xmm0, 2752(%rdi)
	movsd	2784(%rdi), %xmm0
	comisd	.LC110(%rip), %xmm0
	jbe	.L246
	movslq	3868(%rdi), %rax
	movq	2792(%rdi), %rdx
	cmpl	$10, %eax
	je	.L255
	leal	1(%rax), %ecx
	salq	$4, %rax
	addq	%rdi, %rax
	movl	%ecx, 3868(%rdi)
	movq	%rdx, 3704(%rax)
	movsd	%xmm0, 3712(%rax)
.L249:
	movslq	4036(%rdi), %rax
	movq	2800(%rdi), %rdx
	cmpl	$10, %eax
	je	.L256
	leal	1(%rax), %ecx
	salq	$4, %rax
	addq	%rdi, %rax
	movl	%ecx, 4036(%rdi)
	movq	%rdx, 3872(%rax)
	movsd	%xmm0, 3880(%rax)
.L251:
	movslq	4204(%rdi), %rax
	movq	2808(%rdi), %rdx
	cmpl	$10, %eax
	je	.L257
	leal	1(%rax), %ecx
	salq	$4, %rax
	addq	%rdi, %rax
	movl	%ecx, 4204(%rdi)
	movq	%rdx, 4040(%rax)
	movsd	%xmm0, 4048(%rax)
.L246:
	movq	$0x000000000, 2784(%rdi)
	pxor	%xmm0, %xmm0
	movq	$0, 2808(%rdi)
	movups	%xmm0, 2792(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	movslq	3864(%rdi), %rax
	leal	1(%rax), %ecx
	salq	$4, %rax
	addq	%rdi, %rax
	movl	%ecx, 3864(%rdi)
	movq	%rdx, 3704(%rax)
	movsd	%xmm0, 3712(%rax)
	cmpl	$10, %ecx
	jne	.L249
	movl	$0, 3864(%rdi)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L257:
	movslq	4200(%rdi), %rax
	leal	1(%rax), %ecx
	salq	$4, %rax
	addq	%rdi, %rax
	movl	%ecx, 4200(%rdi)
	movq	%rdx, 4040(%rax)
	movsd	%xmm0, 4048(%rax)
	cmpl	$10, %ecx
	jne	.L246
	movl	$0, 4200(%rdi)
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L256:
	movslq	4032(%rdi), %rax
	leal	1(%rax), %ecx
	salq	$4, %rax
	addq	%rdi, %rax
	movl	%ecx, 4032(%rdi)
	movq	%rdx, 3872(%rax)
	movsd	%xmm0, 3880(%rax)
	cmpl	$10, %ecx
	jne	.L251
	movl	$0, 4032(%rdi)
	jmp	.L251
	.cfi_endproc
.LFE20088:
	.size	_ZN2v88internal8GCTracer13AddAllocationEd, .-_ZN2v88internal8GCTracer13AddAllocationEd
	.section	.text._ZN2v88internal8GCTracer22AddContextDisposalTimeEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer22AddContextDisposalTimeEd
	.type	_ZN2v88internal8GCTracer22AddContextDisposalTimeEd, @function
_ZN2v88internal8GCTracer22AddContextDisposalTimeEd:
.LFB20089:
	.cfi_startproc
	endbr64
	movslq	4292(%rdi), %rax
	cmpl	$10, %eax
	je	.L261
	leal	1(%rax), %edx
	movl	%edx, 4292(%rdi)
	movsd	%xmm0, 4208(%rdi,%rax,8)
.L258:
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	movslq	4288(%rdi), %rax
	leal	1(%rax), %edx
	movl	%edx, 4288(%rdi)
	movsd	%xmm0, 4208(%rdi,%rax,8)
	cmpl	$10, %edx
	jne	.L258
	movl	$0, 4288(%rdi)
	ret
	.cfi_endproc
.LFE20089:
	.size	_ZN2v88internal8GCTracer22AddContextDisposalTimeEd, .-_ZN2v88internal8GCTracer22AddContextDisposalTimeEd
	.section	.text._ZN2v88internal8GCTracer18AddCompactionEventEdm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer18AddCompactionEventEdm
	.type	_ZN2v88internal8GCTracer18AddCompactionEventEdm, @function
_ZN2v88internal8GCTracer18AddCompactionEventEdm:
.LFB20090:
	.cfi_startproc
	endbr64
	movslq	3364(%rdi), %rax
	cmpl	$10, %eax
	je	.L265
	leal	1(%rax), %edx
	salq	$4, %rax
	movl	%edx, 3364(%rdi)
	addq	%rax, %rdi
	movq	%rsi, 3200(%rdi)
	movsd	%xmm0, 3208(%rdi)
.L262:
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	movslq	3360(%rdi), %rax
	leal	1(%rax), %edx
	salq	$4, %rax
	addq	%rdi, %rax
	movl	%edx, 3360(%rdi)
	movq	%rsi, 3200(%rax)
	movsd	%xmm0, 3208(%rax)
	cmpl	$10, %edx
	jne	.L262
	movl	$0, 3360(%rdi)
	ret
	.cfi_endproc
.LFE20090:
	.size	_ZN2v88internal8GCTracer18AddCompactionEventEdm, .-_ZN2v88internal8GCTracer18AddCompactionEventEdm
	.section	.text._ZN2v88internal8GCTracer16AddSurvivalRatioEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer16AddSurvivalRatioEd
	.type	_ZN2v88internal8GCTracer16AddSurvivalRatioEd, @function
_ZN2v88internal8GCTracer16AddSurvivalRatioEd:
.LFB20091:
	.cfi_startproc
	endbr64
	movslq	4380(%rdi), %rax
	cmpl	$10, %eax
	je	.L269
	leal	1(%rax), %edx
	movl	%edx, 4380(%rdi)
	movsd	%xmm0, 4296(%rdi,%rax,8)
.L266:
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	movslq	4376(%rdi), %rax
	leal	1(%rax), %edx
	movl	%edx, 4376(%rdi)
	movsd	%xmm0, 4296(%rdi,%rax,8)
	cmpl	$10, %edx
	jne	.L266
	movl	$0, 4376(%rdi)
	ret
	.cfi_endproc
.LFE20091:
	.size	_ZN2v88internal8GCTracer16AddSurvivalRatioEd, .-_ZN2v88internal8GCTracer16AddSurvivalRatioEd
	.section	.text._ZN2v88internal8GCTracer25AddIncrementalMarkingStepEdm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer25AddIncrementalMarkingStepEdm
	.type	_ZN2v88internal8GCTracer25AddIncrementalMarkingStepEdm, @function
_ZN2v88internal8GCTracer25AddIncrementalMarkingStepEdm:
.LFB20092:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L270
	addsd	2480(%rdi), %xmm0
	addq	%rsi, 2472(%rdi)
	movsd	%xmm0, 2480(%rdi)
.L270:
	ret
	.cfi_endproc
.LFE20092:
	.size	_ZN2v88internal8GCTracer25AddIncrementalMarkingStepEdm, .-_ZN2v88internal8GCTracer25AddIncrementalMarkingStepEdm
	.section	.text._ZNK2v88internal8GCTracer6OutputEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer6OutputEPKcz
	.type	_ZNK2v88internal8GCTracer6OutputEPKcz, @function
_ZNK2v88internal8GCTracer6OutputEPKcz:
.LFB20093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$480, %rsp
	movq	%rdx, -192(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L276
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L276:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal13FLAG_trace_gcE(%rip)
	leaq	-504(%rbp), %r14
	jne	.L281
.L277:
	leaq	16(%rbp), %rax
	movq	%r14, %rcx
	movq	%r12, %rdx
	movl	$256, %esi
	leaq	-480(%rbp), %r13
	movq	%rax, -496(%rbp)
	leaq	-208(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, -488(%rbp)
	movl	$16, -504(%rbp)
	movl	$48, -500(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4Heap15AddToRingBufferEPKc@PLT
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L282
	addq	$480, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	leaq	16(%rbp), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movl	$16, -504(%rbp)
	movq	%rax, -496(%rbp)
	leaq	-208(%rbp), %rax
	movl	$48, -500(%rbp)
	movq	%rax, -488(%rbp)
	call	_ZN2v84base2OS6VPrintEPKcP13__va_list_tag@PLT
	jmp	.L277
.L282:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20093:
	.size	_ZNK2v88internal8GCTracer6OutputEPKcz, .-_ZNK2v88internal8GCTracer6OutputEPKcz
	.section	.rodata._ZNK2v88internal8GCTracer5PrintEv.str1.1,"aMS",@progbits,1
.LC123:
	.string	""
	.section	.rodata._ZNK2v88internal8GCTracer5PrintEv.str1.8,"aMS",@progbits,1
	.align 8
.LC125:
	.string	" (+ %.1f ms in %d steps since start of marking, biggest step %.1f ms, walltime since start of marking %.f ms)"
	.align 8
.LC127:
	.string	"[%d:%p] %8.0f ms: %s %.1f (%.1f) -> %.1f (%.1f) MB, %.1f / %.1f ms %s (average mu = %.3f, current mu = %.3f) %s %s\n"
	.section	.text._ZNK2v88internal8GCTracer5PrintEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer5PrintEv
	.type	_ZNK2v88internal8GCTracer5PrintEv, @function
_ZNK2v88internal8GCTracer5PrintEv:
.LFB20098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-192(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movsd	32(%rdi), %xmm2
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 8(%rdi)
	movaps	%xmm0, -192(%rbp)
	movapd	%xmm2, %xmm5
	subsd	24(%rdi), %xmm5
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movsd	%xmm5, -200(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	je	.L308
.L284:
	movq	16(%r12), %r14
	movl	12(%r12), %edi
	leaq	.LC123(%rip), %rax
	testq	%r14, %r14
	cmove	%rax, %r14
	call	_ZN2v88internal4Heap31GarbageCollectionReasonToStringENS0_23GarbageCollectionReasonE@PLT
	movsd	2832(%r12), %xmm7
	movsd	2840(%r12), %xmm0
	movsd	2848(%r12), %xmm5
	movq	%rax, %r15
	addsd	%xmm7, %xmm0
	ucomisd	.LC110(%rip), %xmm0
	jnp	.L309
.L304:
	divsd	%xmm0, %xmm7
.L286:
	movsd	248(%r12), %xmm6
	movq	72(%r12), %rax
	addsd	232(%r12), %xmm6
	addsd	240(%r12), %xmm6
	addsd	152(%r12), %xmm6
	addsd	160(%r12), %xmm6
	testq	%rax, %rax
	js	.L288
	pxor	%xmm4, %xmm4
	cvtsi2sdq	%rax, %xmm4
.L289:
	movsd	.LC126(%rip), %xmm0
	movq	56(%r12), %rax
	mulsd	%xmm0, %xmm4
	testq	%rax, %rax
	js	.L290
	pxor	%xmm3, %xmm3
	cvtsi2sdq	%rax, %xmm3
.L291:
	movq	64(%r12), %rax
	mulsd	%xmm0, %xmm3
	testq	%rax, %rax
	js	.L292
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
.L293:
	movq	48(%r12), %rax
	mulsd	%xmm0, %xmm2
	testq	%rax, %rax
	js	.L294
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
.L295:
	movl	8(%r12), %eax
	mulsd	%xmm0, %xmm1
	leaq	.LC114(%rip), %rbx
	cmpl	$3, %eax
	je	.L296
	ja	.L297
	testl	%eax, %eax
	leaq	.LC115(%rip), %rbx
	leaq	.LC113(%rip), %r8
	cmove	%r8, %rbx
.L296:
	movq	(%r12), %rdi
	movsd	%xmm5, -272(%rbp)
	movsd	%xmm7, -264(%rbp)
	leaq	-37592(%rdi), %rax
	movsd	%xmm6, -256(%rbp)
	movsd	%xmm1, -248(%rbp)
	movsd	%xmm2, -240(%rbp)
	movsd	%xmm3, -232(%rbp)
	movsd	%xmm4, -224(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	-208(%rbp), %rax
	subsd	41464(%rax), %xmm0
	movq	(%r12), %rax
	leaq	-37592(%rax), %rcx
	movq	%rcx, -208(%rbp)
	movsd	%xmm0, -216(%rbp)
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	subq	$8, %rsp
	movq	%r13, %r9
	movq	%rbx, %r8
	pushq	%r14
	movsd	-272(%rbp), %xmm5
	movl	%eax, %edx
	movq	%r12, %rdi
	pushq	%r15
	movsd	-264(%rbp), %xmm7
	movl	$8, %eax
	leaq	.LC127(%rip), %rsi
	movsd	-256(%rbp), %xmm6
	movsd	-224(%rbp), %xmm4
	movsd	-232(%rbp), %xmm3
	movsd	-240(%rbp), %xmm2
	subq	$8, %rsp
	movsd	-248(%rbp), %xmm1
	movsd	-216(%rbp), %xmm0
	movsd	%xmm5, (%rsp)
	movq	-208(%rbp), %rcx
	movsd	-200(%rbp), %xmm5
	call	_ZNK2v88internal8GCTracer6OutputEPKcz
	addq	$32, %rsp
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L310
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	cmpl	$4, %eax
	leaq	.LC111(%rip), %rbx
	leaq	.LC119(%rip), %r8
	cmove	%r8, %rbx
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L288:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm4, %xmm4
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm4
	addsd	%xmm4, %xmm4
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm1, %xmm1
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm2, %xmm2
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L290:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm3, %xmm3
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm3
	addsd	%xmm3, %xmm3
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L309:
	jne	.L304
	movsd	.LC124(%rip), %xmm7
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L308:
	subsd	2488(%rdi), %xmm2
	movl	1016(%rdi), %ecx
	leaq	.LC125(%rip), %rdx
	movl	$128, %esi
	movsd	128(%rdi), %xmm0
	movsd	1008(%rdi), %xmm1
	movl	$3, %eax
	movq	%r13, %rdi
	call	_ZN2v84base2OS8SNPrintFEPciPKcz@PLT
	jmp	.L284
.L310:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20098:
	.size	_ZNK2v88internal8GCTracer5PrintEv, .-_ZNK2v88internal8GCTracer5PrintEv
	.section	.rodata._ZNK2v88internal8GCTracer8PrintNVPEv.str1.8,"aMS",@progbits,1
	.align 8
.LC130:
	.ascii	"pause=%.1f mutator=%.1f gc=%s reduce_memory=%d heap.prologue"
	.ascii	"=%.2f heap.epilogue=%.2f heap.epilogue.reduce_new_space=%.2f"
	.ascii	" heap.external.prologue=%.2f heap.external.epilogue=%.2f hea"
	.ascii	"p.external_weak_global_handles=%.2f fast_promote=%.2f scaven"
	.ascii	"ge=%.2f scavenge.process_array_buffers=%.2f scavenge.roots=%"
	.ascii	".2f scavenge.weak=%.2f scavenge.weak_global_handles.identify"
	.ascii	"=%.2f scavenge.weak_global_handles.process=%.2f scavenge.par"
	.ascii	"allel=%.2f scavenge.update_refs=%.2f background.scavenge.par"
	.ascii	"allel=%.2f background.array_buffer_free=%.2f background.stor"
	.ascii	"e_buffer=%.2f background.unmapper=%.2f incremental.steps_cou"
	.ascii	"nt=%d incremental.steps_took=%.1f scavenge_throughput=%.f to"
	.ascii	"tal_size_before=%zu total_size_after=%zu holes_size_before=%"
	.ascii	"zu holes_size_after=%zu allocated=%zu promoted=%zu semi_spac"
	.ascii	"e_cop"
	.string	"ied=%zu nodes_died_in_new=%d nodes_copied_in_new=%d nodes_promoted=%d promotion_ratio=%.1f%% average_survival_ratio=%.1f%% promotion_rate=%.1f%% semi_space_copy_rate=%.1f%% new_space_allocation_throughput=%.1f unmapper_chunks=%d context_disposal_rate=%.1f\n"
	.align 8
.LC131:
	.ascii	"pause=%.1f mutator=%.1f gc=%s reduce_memory=%d minor_mc=%.2f"
	.ascii	" finish_sweeping=%.2f mark=%.2f mark.seed=%.2f mark.roots=%."
	.ascii	"2f mark.weak=%.2f mark.global_handles=%.2f clear=%.2f clear."
	.ascii	"string_table=%.2f clear.weak_lists=%.2f evacuate=%.2f evacua"
	.ascii	"te.copy=%.2f evacuate.update_pointers=%.2f evacuate.update_p"
	.ascii	"ointers.to_new_roots=%.2f evacua"
	.string	"te.update_pointers.slots=%.2f background.mark=%.2f background.evacuate.copy=%.2f background.evacuate.update_pointers=%.2f background.array_buffer_free=%.2f background.store_buffer=%.2f background.unmapper=%.2f update_marking_deque=%.2f reset_liveness=%.2f\n"
	.align 8
.LC132:
	.ascii	"pause=%.1f mutator=%.1f gc=%s reduce_memory=%d heap.prologue"
	.ascii	"=%.2f heap.embedder_tracing_epilogue=%.2f heap.epilogue=%.2f"
	.ascii	" heap.epilogue.reduce_new_space=%.2f heap.external.prologue="
	.ascii	"%.1f heap.external.epilogue=%.1f heap.external.weak_global_h"
	.ascii	"andles=%.1f clear=%1.f clear.dependent_code=%.1f clear.maps="
	.ascii	"%.1f clear.slots_buffer=%.1f clear.store_buffer=%.1f clear.s"
	.ascii	"tring_table=%.1f clear.weak_collections=%.1f clear.weak_list"
	.ascii	"s=%.1f clear.weak_references=%.1f epilogue=%.1f evacuate=%.1"
	.ascii	"f evacuate.candidates=%.1f evacuate.clean_up=%.1f evacuate.c"
	.ascii	"opy=%.1f evacuate.prologue=%.1f evacuate.epilogue=%.1f evacu"
	.ascii	"ate.rebalance=%.1f evacuate.update_pointers=%.1f evacuate.up"
	.ascii	"date_pointers.to_new_roots=%.1f evacuate.update_pointers.slo"
	.ascii	"ts.main=%.1f evacuate.update_pointers.slots.map_space=%.1f e"
	.ascii	"vacuate.update_pointers.weak=%.1f finish=%.1f mark=%.1f mark"
	.ascii	".finish_incremental=%.1f mark.roots=%.1f mark.main=%.1f mark"
	.ascii	".weak_closure=%.1f mark.weak_closure.ephemeron=%.1f mark.wea"
	.ascii	"k_closure.ephemeron.marking=%.1f mark.weak_closure.ephemeron"
	.ascii	".linear=%.1f mark.weak_closure.weak_handles=%.1f mark.weak_c"
	.ascii	"losure.weak_roots=%.1f mark.weak_closure.harmony=%.1f mark.e"
	.ascii	"mbedder_prologue=%.1f mark.embedder_tracing=%.1f prologue=%."
	.ascii	"1f sweep=%.1f sweep.code=%.1f sweep.map=%.1f sweep.old=%.1f "
	.ascii	"incremental=%.1f incremental.finalize=%.1f incremental.final"
	.ascii	"ize.body=%.1f incremental.finalize.external.prologue=%.1f in"
	.ascii	"cremental.finalize.external.epilogue=%.1f incremental.layout"
	.ascii	"_change=%.1f incremental.start=%.1f incremental.sweeping=%.1"
	.ascii	"f incremental.embedder_prologue=%.1f incremental.embedder_tr"
	.ascii	"acing=%.1f incremental_wrapper_tracing_longest_step=%.1f inc"
	.ascii	"remental_finalize_longest_step=%.1f incremental_finalize_ste"
	.ascii	"ps_count=%d incremental_longest_step=%.1f incremental_steps_"
	.ascii	"count=%d incremental_marking_throughput=%.f incremental_wall"
	.ascii	"time_duration=%.f background.mark=%.1f background.sweep=%.1f"
	.ascii	" background.evacuate.copy=%.1f background.evacuate.update_po"
	.ascii	"inters=%.1f background.array_buffer_free=%.2f background.sto"
	.ascii	"re_buffer=%.2f backg"
	.ascii	"round.unmapper=%.1f total_size_before=%zu total_size_after=%"
	.ascii	"zu holes_size_before=%zu holes_size_after=%zu allocated=%zu "
	.ascii	"promoted=%zu semi_space_copied=%zu nodes_died_in"
	.string	"_new=%d nodes_copied_in_new=%d nodes_promoted=%d promotion_ratio=%.1f%% average_survival_ratio=%.1f%% promotion_rate=%.1f%% semi_space_copy_rate=%.1f%% new_space_allocation_throughput=%.1f unmapper_chunks=%d context_disposal_rate=%.1f compaction_speed=%.f\n"
	.section	.text._ZNK2v88internal8GCTracer8PrintNVPEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer8PrintNVPEv
	.type	_ZNK2v88internal8GCTracer8PrintNVPEv, @function
_ZNK2v88internal8GCTracer8PrintNVPEv:
.LFB20099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$536, %rsp
	movsd	32(%rdi), %xmm0
	movsd	24(%rdi), %xmm1
	movl	8(%rdi), %eax
	movq	48(%rdi), %r13
	movapd	%xmm0, %xmm4
	subq	1288(%rdi), %r13
	subsd	%xmm1, %xmm4
	subsd	1264(%rdi), %xmm1
	movsd	%xmm4, -56(%rbp)
	movsd	%xmm1, -64(%rbp)
	cmpl	$2, %eax
	je	.L439
	cmpl	$3, %eax
	je	.L314
	ja	.L315
	testl	%eax, %eax
	jne	.L440
	movq	(%rdi), %rdi
	cmpl	$9, 4292(%rbx)
	leaq	-37592(%rdi), %r12
	jg	.L441
	pxor	%xmm2, %xmm2
	movsd	%xmm2, -192(%rbp)
.L319:
	movq	2048(%rdi), %rdi
	movsd	%xmm2, -80(%rbp)
	addq	$136, %rdi
	call	_ZN2v88internal15MemoryAllocator8Unmapper14NumberOfChunksEv@PLT
	movl	3868(%rbx), %edi
	movsd	-80(%rbp), %xmm2
	movl	%eax, -72(%rbp)
	movl	3864(%rbx), %eax
	movq	2792(%rbx), %rsi
	movsd	2784(%rbx), %xmm0
	addl	%edi, %eax
	leal	-1(%rax), %edx
	subl	$11, %eax
	cmpl	$9, %edx
	cmovg	%eax, %edx
	testl	%edi, %edi
	jle	.L327
	leaq	3704(%rbx), %r8
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L330:
	movslq	%edx, %rax
	subl	$1, %edx
	addl	$1, %ecx
	salq	$4, %rax
	addq	%r8, %rax
	addsd	8(%rax), %xmm0
	addq	(%rax), %rsi
	cmpl	$-1, %edx
	je	.L442
	cmpl	%ecx, %edi
	jne	.L330
.L327:
	ucomisd	%xmm2, %xmm0
	jnp	.L443
.L416:
	testq	%rsi, %rsi
	js	.L333
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rsi, %xmm1
.L334:
	divsd	%xmm0, %xmm1
	comisd	.LC128(%rip), %xmm1
	jnb	.L397
	movsd	.LC124(%rip), %xmm3
	movapd	%xmm1, %xmm0
	cmpnlesd	%xmm3, %xmm0
	andpd	%xmm0, %xmm1
	andnpd	%xmm3, %xmm0
	orpd	%xmm0, %xmm1
.L331:
	movq	(%rbx), %rsi
	movl	4380(%rbx), %ecx
	movapd	%xmm2, %xmm8
	movsd	1960(%rsi), %xmm9
	movsd	1936(%rsi), %xmm10
	testl	%ecx, %ecx
	je	.L336
	movl	4376(%rbx), %edx
	addl	%ecx, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%ecx, %ecx
	jle	.L338
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L341:
	movslq	%eax, %rdi
	subl	$1, %eax
	addl	$1, %edx
	addsd	4296(%rbx,%rdi,8), %xmm8
	cmpl	$-1, %eax
	je	.L444
	cmpl	%edx, %ecx
	jne	.L341
.L338:
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	divsd	%xmm0, %xmm8
.L336:
	movl	1976(%rsi), %eax
	movq	1944(%rsi), %r15
	movsd	1928(%rsi), %xmm11
	movq	1920(%rsi), %r14
	movq	80(%rbx), %r10
	movq	56(%rbx), %r11
	movl	%eax, -80(%rbp)
	movl	1972(%rsi), %eax
	movq	48(%rbx), %r9
	movl	%eax, -88(%rbp)
	movl	1968(%rsi), %eax
	movl	3028(%rbx), %esi
	movl	%eax, -96(%rbp)
	movq	88(%rbx), %rax
	movq	%rax, -104(%rbp)
	movl	3024(%rbx), %eax
	addl	%esi, %eax
	leal	-1(%rax), %edx
	subl	$11, %eax
	cmpl	$9, %edx
	cmovg	%eax, %edx
	testl	%esi, %esi
	jle	.L343
	leaq	2864(%rbx), %r8
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movapd	%xmm2, %xmm3
	.p2align 4,,10
	.p2align 3
.L347:
	movslq	%edx, %rax
	subl	$1, %edx
	addl	$1, %ecx
	salq	$4, %rax
	addq	%r8, %rax
	addq	(%rax), %rdi
	addsd	8(%rax), %xmm3
	movq	%rdi, %rax
	cmpl	$-1, %edx
	je	.L445
	cmpl	%ecx, %esi
	jne	.L347
.L345:
	ucomisd	%xmm2, %xmm3
	jnp	.L446
.L418:
	testq	%rax, %rax
	js	.L350
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L351:
	divsd	%xmm3, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L401
	movsd	.LC124(%rip), %xmm3
	movapd	%xmm0, %xmm2
	cmpnlesd	%xmm3, %xmm2
	andpd	%xmm2, %xmm0
	andnpd	%xmm3, %xmm2
	orpd	%xmm2, %xmm0
.L348:
	movsd	920(%rbx), %xmm6
	movl	1016(%rbx), %r8d
	leaq	.LC116(%rip), %rdx
	movsd	912(%rbx), %xmm7
	movzbl	40(%rbx), %ecx
	movsd	%xmm6, -112(%rbp)
	movsd	992(%rbx), %xmm6
	movsd	880(%rbx), %xmm5
	movsd	%xmm7, -120(%rbp)
	movsd	888(%rbx), %xmm7
	movsd	128(%rbx), %xmm15
	movsd	%xmm6, -128(%rbp)
	movsd	872(%rbx), %xmm6
	movsd	928(%rbx), %xmm14
	movsd	%xmm7, -136(%rbp)
	movsd	864(%rbx), %xmm7
	movsd	840(%rbx), %xmm13
	movsd	%xmm6, -144(%rbp)
	movsd	856(%rbx), %xmm6
	movsd	832(%rbx), %xmm12
	movsd	%xmm7, -152(%rbp)
	movsd	896(%rbx), %xmm7
	movsd	224(%rbx), %xmm4
	movsd	%xmm6, -160(%rbp)
	movsd	848(%rbx), %xmm6
	movsd	216(%rbx), %xmm3
	movsd	%xmm7, -168(%rbp)
	movsd	256(%rbx), %xmm2
	movsd	248(%rbx), %xmm7
	movsd	%xmm5, -176(%rbp)
	movsd	240(%rbx), %xmm5
	movsd	%xmm6, -184(%rbp)
	movsd	232(%rbx), %xmm6
	movl	8(%rbx), %eax
	cmpl	$3, %eax
	je	.L353
	ja	.L354
	testl	%eax, %eax
	leaq	.LC118(%rip), %rdx
	leaq	.LC117(%rip), %rax
	cmovne	%rax, %rdx
.L353:
	movl	-72(%rbp), %eax
	subq	$8, %rsp
	pushq	-192(%rbp)
	movq	%r12, %rdi
	leaq	.LC130(%rip), %rsi
	pushq	%rax
	movl	-80(%rbp), %eax
	subq	$40, %rsp
	movsd	%xmm1, 32(%rsp)
	movsd	%xmm9, 24(%rsp)
	movsd	%xmm10, 16(%rsp)
	movsd	%xmm8, 8(%rsp)
	movsd	%xmm11, (%rsp)
	pushq	%rax
	movl	-88(%rbp), %eax
	pushq	%rax
	movl	-96(%rbp), %eax
	pushq	%rax
	movl	$8, %eax
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	-104(%rbp)
	pushq	%r10
	pushq	%r11
	subq	$24, %rsp
	movsd	%xmm0, 16(%rsp)
	movsd	%xmm15, 8(%rsp)
	movsd	%xmm14, (%rsp)
	pushq	-112(%rbp)
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	movsd	-64(%rbp), %xmm1
	pushq	-144(%rbp)
	movsd	-56(%rbp), %xmm0
	pushq	-152(%rbp)
	pushq	-160(%rbp)
	pushq	-168(%rbp)
	pushq	-176(%rbp)
	pushq	-184(%rbp)
	subq	$16, %rsp
	movsd	%xmm13, 8(%rsp)
	movsd	%xmm12, (%rsp)
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	addq	$256, %rsp
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	cmpl	$4, %eax
	jne	.L447
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	subsd	2488(%rdi), %xmm0
	pxor	%xmm2, %xmm2
	movsd	%xmm0, -80(%rbp)
.L313:
	movl	3364(%rbx), %esi
	movl	3360(%rbx), %eax
	movq	(%rbx), %r8
	addl	%esi, %eax
	leal	-1(%rax), %edx
	subl	$11, %eax
	leaq	-37592(%r8), %r12
	cmpl	$9, %edx
	cmovg	%eax, %edx
	testl	%esi, %esi
	jle	.L356
	leaq	3200(%rbx), %r9
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movapd	%xmm2, %xmm1
	.p2align 4,,10
	.p2align 3
.L360:
	movslq	%edx, %rax
	subl	$1, %edx
	addl	$1, %ecx
	salq	$4, %rax
	addq	%r9, %rax
	addq	(%rax), %rdi
	addsd	8(%rax), %xmm1
	movq	%rdi, %rax
	cmpl	$-1, %edx
	je	.L448
	cmpl	%ecx, %esi
	jne	.L360
.L358:
	ucomisd	%xmm2, %xmm1
	jnp	.L449
.L420:
	testq	%rax, %rax
	js	.L363
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L364:
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	movapd	%xmm0, %xmm5
	jnb	.L406
	movsd	.LC124(%rip), %xmm1
	cmpnlesd	%xmm1, %xmm0
	andpd	%xmm0, %xmm5
	andnpd	%xmm1, %xmm0
	orpd	%xmm5, %xmm0
	movsd	%xmm0, -88(%rbp)
.L361:
	cmpl	$9, 4292(%rbx)
	movsd	%xmm2, -72(%rbp)
	jle	.L366
	movq	%r8, %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movl	4292(%rbx), %esi
	movl	4288(%rbx), %edx
	movsd	-72(%rbp), %xmm2
	addl	%esi, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%esi, %esi
	jle	.L368
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L372:
	movslq	%eax, %rcx
	subl	$1, %eax
	addl	$1, %edx
	movsd	4208(%rbx,%rcx,8), %xmm1
	cmpl	$-1, %eax
	je	.L450
	cmpl	%edx, %esi
	jne	.L372
.L370:
	subsd	%xmm1, %xmm0
.L368:
	pxor	%xmm1, %xmm1
	movq	(%rbx), %r8
	cvtsi2sdl	%esi, %xmm1
	divsd	%xmm1, %xmm0
	movsd	%xmm0, -72(%rbp)
.L366:
	movq	2048(%r8), %rdi
	movsd	%xmm2, -96(%rbp)
	addq	$136, %rdi
	call	_ZN2v88internal15MemoryAllocator8Unmapper14NumberOfChunksEv@PLT
	movl	3864(%rbx), %edx
	movl	3868(%rbx), %r8d
	movq	2792(%rbx), %rdi
	movsd	2784(%rbx), %xmm1
	addl	%r8d, %edx
	movsd	-96(%rbp), %xmm2
	leal	-1(%rdx), %ecx
	subl	$11, %edx
	cmpl	$9, %ecx
	cmovg	%edx, %ecx
	testl	%r8d, %r8d
	jle	.L374
	leaq	3704(%rbx), %r9
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L377:
	movslq	%ecx, %rdx
	subl	$1, %ecx
	addl	$1, %esi
	salq	$4, %rdx
	addq	%r9, %rdx
	addsd	8(%rdx), %xmm1
	addq	(%rdx), %rdi
	cmpl	$-1, %ecx
	je	.L451
	cmpl	%esi, %r8d
	jne	.L377
.L374:
	ucomisd	%xmm2, %xmm1
	jnp	.L452
.L422:
	testq	%rdi, %rdi
	js	.L380
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdi, %xmm0
.L381:
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L409
	movsd	.LC124(%rip), %xmm3
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm3, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm3, %xmm1
	orpd	%xmm1, %xmm0
.L378:
	movq	(%rbx), %rdi
	movl	4380(%rbx), %esi
	movapd	%xmm2, %xmm8
	movsd	1960(%rdi), %xmm7
	movsd	1936(%rdi), %xmm5
	movsd	%xmm7, -96(%rbp)
	movsd	%xmm5, -104(%rbp)
	testl	%esi, %esi
	je	.L383
	movl	4376(%rbx), %ecx
	movapd	%xmm2, %xmm3
	addl	%esi, %ecx
	leal	-1(%rcx), %edx
	subl	$11, %ecx
	cmpl	$9, %edx
	cmovg	%ecx, %edx
	testl	%esi, %esi
	jle	.L385
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L388:
	movslq	%edx, %r8
	subl	$1, %edx
	addl	$1, %ecx
	addsd	4296(%rbx,%r8,8), %xmm3
	cmpl	$-1, %edx
	je	.L453
	cmpl	%ecx, %esi
	jne	.L388
.L385:
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	divsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm8
.L383:
	movsd	1928(%rdi), %xmm6
	movq	80(%rbx), %rcx
	movsd	2496(%rbx), %xmm1
	movsd	928(%rbx), %xmm7
	movsd	%xmm6, -112(%rbp)
	movsd	920(%rbx), %xmm5
	movsd	912(%rbx), %xmm6
	movq	%rcx, -120(%rbp)
	movq	56(%rbx), %rcx
	ucomisd	%xmm2, %xmm1
	movsd	936(%rbx), %xmm4
	movsd	%xmm7, -144(%rbp)
	movsd	944(%rbx), %xmm7
	movq	%rcx, -128(%rbp)
	movq	48(%rbx), %rcx
	movsd	%xmm5, -152(%rbp)
	movsd	960(%rbx), %xmm5
	movl	1976(%rdi), %r15d
	movsd	%xmm6, -160(%rbp)
	movsd	952(%rbx), %xmm6
	movl	1972(%rdi), %r14d
	movl	1968(%rdi), %r11d
	movq	88(%rbx), %rsi
	movq	%rcx, -136(%rbp)
	movq	1944(%rdi), %r10
	movsd	%xmm7, -168(%rbp)
	movq	1920(%rdi), %rdi
	movsd	%xmm4, -176(%rbp)
	movsd	%xmm5, -184(%rbp)
	movsd	%xmm6, -192(%rbp)
	jp	.L389
	jne	.L389
	movsd	2480(%rbx), %xmm3
	ucomisd	%xmm2, %xmm3
	jp	.L424
	movsd	.LC129(%rip), %xmm1
	jne	.L424
.L389:
	movsd	1008(%rbx), %xmm7
	movsd	192(%rbx), %xmm4
	leaq	.LC116(%rip), %rdx
	movsd	184(%rbx), %xmm5
	movsd	152(%rbx), %xmm6
	movsd	%xmm7, -200(%rbp)
	movsd	160(%rbx), %xmm7
	movl	1016(%rbx), %r9d
	movsd	%xmm4, -208(%rbp)
	movsd	176(%rbx), %xmm4
	movl	1160(%rbx), %r8d
	movsd	%xmm5, -216(%rbp)
	movsd	168(%rbx), %xmm5
	movsd	1152(%rbx), %xmm15
	movsd	%xmm6, -224(%rbp)
	movsd	128(%rbx), %xmm6
	movsd	1056(%rbx), %xmm14
	movsd	%xmm7, -232(%rbp)
	movsd	624(%rbx), %xmm7
	movsd	144(%rbx), %xmm13
	movsd	%xmm4, -240(%rbp)
	movsd	616(%rbx), %xmm4
	movsd	136(%rbx), %xmm12
	movsd	%xmm5, -248(%rbp)
	movsd	608(%rbx), %xmm5
	movsd	200(%rbx), %xmm11
	movsd	%xmm6, -256(%rbp)
	movsd	312(%rbx), %xmm6
	movsd	%xmm7, -264(%rbp)
	movsd	304(%rbx), %xmm7
	movsd	%xmm4, -272(%rbp)
	movsd	%xmm5, -280(%rbp)
	movsd	%xmm6, -288(%rbp)
	movsd	%xmm7, -296(%rbp)
	movsd	512(%rbx), %xmm4
	movsd	504(%rbx), %xmm5
	movsd	600(%rbx), %xmm6
	movsd	592(%rbx), %xmm7
	movsd	%xmm4, -304(%rbp)
	movsd	584(%rbx), %xmm4
	movsd	552(%rbx), %xmm3
	movsd	%xmm5, -312(%rbp)
	movsd	576(%rbx), %xmm5
	movsd	%xmm6, -320(%rbp)
	movsd	568(%rbx), %xmm6
	movsd	%xmm7, -328(%rbp)
	movsd	560(%rbx), %xmm7
	movsd	%xmm4, -336(%rbp)
	movsd	536(%rbx), %xmm4
	movsd	%xmm5, -344(%rbp)
	movsd	544(%rbx), %xmm5
	movsd	%xmm6, -352(%rbp)
	movsd	528(%rbx), %xmm6
	movsd	%xmm7, -360(%rbp)
	movsd	296(%rbx), %xmm7
	movsd	%xmm3, -368(%rbp)
	movsd	288(%rbx), %xmm3
	movsd	%xmm4, -376(%rbp)
	movsd	496(%rbx), %xmm4
	movsd	%xmm5, -384(%rbp)
	movsd	480(%rbx), %xmm5
	movsd	%xmm7, -400(%rbp)
	movsd	%xmm3, -408(%rbp)
	movsd	%xmm4, -416(%rbp)
	movsd	%xmm5, -424(%rbp)
	movsd	%xmm6, -392(%rbp)
	movsd	472(%rbx), %xmm6
	movsd	488(%rbx), %xmm7
	movsd	456(%rbx), %xmm3
	movsd	448(%rbx), %xmm4
	movsd	432(%rbx), %xmm5
	movsd	%xmm6, -432(%rbp)
	movsd	440(%rbx), %xmm6
	movsd	%xmm7, -440(%rbp)
	movsd	416(%rbx), %xmm7
	movsd	%xmm3, -448(%rbp)
	movsd	408(%rbx), %xmm3
	movsd	%xmm4, -456(%rbp)
	movsd	400(%rbx), %xmm4
	movsd	%xmm5, -464(%rbp)
	movsd	280(%rbx), %xmm5
	movsd	%xmm6, -472(%rbp)
	movsd	272(%rbx), %xmm6
	movsd	%xmm7, -480(%rbp)
	movsd	392(%rbx), %xmm7
	movsd	%xmm3, -488(%rbp)
	movsd	384(%rbx), %xmm3
	movsd	%xmm4, -496(%rbp)
	movsd	376(%rbx), %xmm4
	movsd	%xmm5, -504(%rbp)
	movsd	368(%rbx), %xmm5
	movsd	%xmm6, -512(%rbp)
	movsd	360(%rbx), %xmm6
	movsd	%xmm3, -528(%rbp)
	movsd	%xmm4, -536(%rbp)
	movsd	%xmm5, -544(%rbp)
	movsd	%xmm6, -552(%rbp)
	movsd	%xmm7, -520(%rbp)
	movsd	352(%rbx), %xmm7
	movsd	344(%rbx), %xmm3
	movsd	248(%rbx), %xmm4
	movsd	240(%rbx), %xmm6
	movsd	320(%rbx), %xmm10
	movsd	224(%rbx), %xmm5
	movsd	%xmm7, -560(%rbp)
	movsd	264(%rbx), %xmm9
	movsd	232(%rbx), %xmm7
	movsd	%xmm3, -568(%rbp)
	movsd	256(%rbx), %xmm2
	movsd	208(%rbx), %xmm3
	movsd	%xmm4, -576(%rbp)
	movzbl	40(%rbx), %ecx
	movsd	216(%rbx), %xmm4
	movl	8(%rbx), %ebx
	cmpl	$3, %ebx
	je	.L393
	ja	.L394
	testl	%ebx, %ebx
	leaq	.LC118(%rip), %rdx
	leaq	.LC117(%rip), %rbx
	cmovne	%rbx, %rdx
.L393:
	pushq	-88(%rbp)
	pushq	-72(%rbp)
	pushq	%rax
	movl	$8, %eax
	subq	$8, %rsp
	movsd	%xmm0, (%rsp)
	pushq	-96(%rbp)
	pushq	-104(%rbp)
	subq	$8, %rsp
	movsd	%xmm8, (%rsp)
	pushq	-112(%rbp)
	pushq	%r15
	pushq	%r14
	pushq	%r11
	pushq	%r10
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%r13
	pushq	%rsi
	leaq	.LC132(%rip), %rsi
	pushq	-120(%rbp)
	pushq	-128(%rbp)
	pushq	-136(%rbp)
	pushq	-144(%rbp)
	pushq	-152(%rbp)
	pushq	-160(%rbp)
	pushq	-168(%rbp)
	pushq	-176(%rbp)
	pushq	-184(%rbp)
	pushq	-192(%rbp)
	pushq	-80(%rbp)
	subq	$8, %rsp
	movsd	%xmm1, (%rsp)
	pushq	-200(%rbp)
	subq	$40, %rsp
	movsd	%xmm15, 32(%rsp)
	movsd	%xmm14, 24(%rsp)
	movsd	%xmm13, 16(%rsp)
	movsd	%xmm12, 8(%rsp)
	movsd	%xmm11, (%rsp)
	pushq	-208(%rbp)
	pushq	-216(%rbp)
	pushq	-224(%rbp)
	pushq	-232(%rbp)
	pushq	-240(%rbp)
	pushq	-248(%rbp)
	pushq	-256(%rbp)
	pushq	-264(%rbp)
	pushq	-272(%rbp)
	pushq	-280(%rbp)
	pushq	-288(%rbp)
	pushq	-296(%rbp)
	pushq	-304(%rbp)
	pushq	-312(%rbp)
	pushq	-320(%rbp)
	pushq	-328(%rbp)
	pushq	-336(%rbp)
	pushq	-344(%rbp)
	pushq	-352(%rbp)
	pushq	-360(%rbp)
	pushq	-368(%rbp)
	pushq	-376(%rbp)
	pushq	-384(%rbp)
	pushq	-392(%rbp)
	pushq	-400(%rbp)
	pushq	-408(%rbp)
	pushq	-416(%rbp)
	pushq	-424(%rbp)
	pushq	-432(%rbp)
	pushq	-440(%rbp)
	pushq	-448(%rbp)
	pushq	-456(%rbp)
	pushq	-464(%rbp)
	pushq	-472(%rbp)
	pushq	-480(%rbp)
	pushq	-488(%rbp)
	pushq	-496(%rbp)
	pushq	-504(%rbp)
	pushq	-512(%rbp)
	pushq	-520(%rbp)
	movsd	-64(%rbp), %xmm1
	pushq	-528(%rbp)
	movsd	-56(%rbp), %xmm0
	pushq	-536(%rbp)
	pushq	-544(%rbp)
	pushq	-552(%rbp)
	pushq	-560(%rbp)
	pushq	-568(%rbp)
	subq	$16, %rsp
	movsd	%xmm10, 8(%rsp)
	movsd	%xmm9, (%rsp)
	pushq	-576(%rbp)
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	addq	$656, %rsp
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L448:
	.cfi_restore_state
	cmpl	%ecx, %esi
	je	.L358
	movl	$9, %edx
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L314:
	subq	$8, %rsp
	movzbl	40(%rdi), %ecx
	movq	(%rdi), %rax
	movapd	%xmm4, %xmm0
	pushq	816(%rbx)
	leaq	.LC116(%rip), %rdx
	leaq	.LC131(%rip), %rsi
	pushq	808(%rbx)
	leaq	-37592(%rax), %rdi
	movl	$8, %eax
	pushq	928(%rbx)
	pushq	920(%rbx)
	pushq	912(%rbx)
	pushq	976(%rbx)
	pushq	968(%rbx)
	pushq	984(%rbx)
	pushq	736(%rbx)
	pushq	744(%rbx)
	pushq	720(%rbx)
	pushq	680(%rbx)
	pushq	664(%rbx)
	pushq	656(%rbx)
	pushq	648(%rbx)
	pushq	640(%rbx)
	pushq	768(%rbx)
	movsd	800(%rbx), %xmm7
	movsd	792(%rbx), %xmm6
	movsd	784(%rbx), %xmm5
	movsd	760(%rbx), %xmm4
	movsd	824(%rbx), %xmm3
	movsd	632(%rbx), %xmm2
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	addq	$144, %rsp
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movl	4292(%rbx), %esi
	movl	4288(%rbx), %edx
	addl	%esi, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%esi, %esi
	jle	.L321
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L325:
	movslq	%eax, %rcx
	subl	$1, %eax
	addl	$1, %edx
	movsd	4208(%rbx,%rcx,8), %xmm1
	cmpl	$-1, %eax
	je	.L454
	cmpl	%edx, %esi
	jne	.L325
.L323:
	subsd	%xmm1, %xmm0
.L321:
	pxor	%xmm1, %xmm1
	movq	(%rbx), %rdi
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%esi, %xmm1
	divsd	%xmm1, %xmm0
	movsd	%xmm0, -192(%rbp)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L449:
	movsd	%xmm2, -88(%rbp)
	je	.L361
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L451:
	cmpl	%esi, %r8d
	je	.L374
	movl	$9, %ecx
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L442:
	cmpl	%ecx, %edi
	je	.L327
	movl	$9, %edx
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L445:
	cmpl	%ecx, %esi
	je	.L345
	movl	$9, %edx
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L453:
	cmpl	%ecx, %esi
	je	.L385
	movl	$9, %edx
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L444:
	cmpl	%edx, %ecx
	je	.L338
	movl	$9, %eax
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L454:
	cmpl	%edx, %esi
	je	.L323
	movl	$9, %eax
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L450:
	cmpl	%edx, %esi
	je	.L370
	movl	$9, %eax
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L394:
	cmpl	$4, %ebx
	leaq	.LC112(%rip), %rdx
	leaq	.LC111(%rip), %rbx
	cmovne	%rbx, %rdx
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L354:
	cmpl	$4, %eax
	leaq	.LC112(%rip), %rdx
	leaq	.LC111(%rip), %rax
	cmovne	%rax, %rdx
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L452:
	jne	.L422
	movapd	%xmm2, %xmm0
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L443:
	jne	.L416
	movapd	%xmm2, %xmm1
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm1, %xmm1
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L380:
	movq	%rdi, %rdx
	andl	$1, %edi
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rdi, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L350:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L363:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L446:
	movapd	%xmm2, %xmm0
	je	.L348
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L406:
	movq	.LC128(%rip), %rax
	movq	%rax, -88(%rbp)
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L397:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm1
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L401:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L409:
	movq	.LC128(%rip), %rsi
	movq	%rsi, %xmm0
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L424:
	movq	2472(%rbx), %rdx
	testq	%rdx, %rdx
	js	.L391
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
.L392:
	divsd	%xmm3, %xmm1
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L391:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm1, %xmm1
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L343:
	movapd	%xmm2, %xmm0
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L356:
	movsd	%xmm2, -88(%rbp)
	jmp	.L361
.L440:
	pxor	%xmm2, %xmm2
	movsd	%xmm2, -80(%rbp)
	jmp	.L313
.L447:
	leaq	.LC109(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20099:
	.size	_ZNK2v88internal8GCTracer8PrintNVPEv, .-_ZNK2v88internal8GCTracer8PrintNVPEv
	.section	.text._ZN2v88internal8GCTracer12AverageSpeedERKNS_4base10RingBufferISt4pairImdEEERKS5_d,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer12AverageSpeedERKNS_4base10RingBufferISt4pairImdEEERKS5_d
	.type	_ZN2v88internal8GCTracer12AverageSpeedERKNS_4base10RingBufferISt4pairImdEEERKS5_d, @function
_ZN2v88internal8GCTracer12AverageSpeedERKNS_4base10RingBufferISt4pairImdEEERKS5_d:
.LFB20100:
	.cfi_startproc
	endbr64
	movl	164(%rdi), %ecx
	movl	160(%rdi), %edx
	movapd	%xmm0, %xmm2
	pxor	%xmm0, %xmm0
	movsd	8(%rsi), %xmm1
	addl	%ecx, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	movq	(%rsi), %rdx
	testl	%ecx, %ecx
	jle	.L457
	ucomisd	%xmm0, %xmm2
	movl	$0, %r8d
	jp	.L458
	jne	.L458
	.p2align 4,,10
	.p2align 3
.L462:
	movslq	%eax, %rsi
	subl	$1, %eax
	addl	$1, %r8d
	salq	$4, %rsi
	addq	%rdi, %rsi
	addsd	8(%rsi), %xmm1
	addq	(%rsi), %rdx
	cmpl	$-1, %eax
	je	.L478
	cmpl	%r8d, %ecx
	jne	.L462
.L457:
	ucomisd	%xmm0, %xmm1
	jnp	.L479
.L474:
	testq	%rdx, %rdx
	js	.L468
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L473
.L481:
	movsd	.LC124(%rip), %xmm2
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm2, %xmm1
	orpd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L480:
	cmpl	%r8d, %ecx
	je	.L457
	movl	$9, %eax
.L458:
	movslq	%eax, %rsi
	salq	$4, %rsi
	addq	%rdi, %rsi
	comisd	%xmm2, %xmm1
	movq	(%rsi), %r9
	movsd	8(%rsi), %xmm3
	jnb	.L463
	addsd	%xmm3, %xmm1
	addq	%r9, %rdx
.L463:
	subl	$1, %eax
	addl	$1, %r8d
	cmpl	$-1, %eax
	je	.L480
	cmpl	%r8d, %ecx
	jne	.L458
	ucomisd	%xmm0, %xmm1
	jp	.L474
.L479:
	jne	.L474
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	cmpl	%r8d, %ecx
	je	.L457
	movl	$9, %eax
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L468:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jb	.L481
.L473:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	ret
	.cfi_endproc
.LFE20100:
	.size	_ZN2v88internal8GCTracer12AverageSpeedERKNS_4base10RingBufferISt4pairImdEEERKS5_d, .-_ZN2v88internal8GCTracer12AverageSpeedERKNS_4base10RingBufferISt4pairImdEEERKS5_d
	.section	.text._ZN2v88internal8GCTracer12AverageSpeedERKNS_4base10RingBufferISt4pairImdEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer12AverageSpeedERKNS_4base10RingBufferISt4pairImdEEE
	.type	_ZN2v88internal8GCTracer12AverageSpeedERKNS_4base10RingBufferISt4pairImdEEE, @function
_ZN2v88internal8GCTracer12AverageSpeedERKNS_4base10RingBufferISt4pairImdEEE:
.LFB20111:
	.cfi_startproc
	endbr64
	movl	164(%rdi), %esi
	movl	160(%rdi), %edx
	addl	%esi, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%esi, %esi
	jle	.L484
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movapd	%xmm0, %xmm1
	.p2align 4,,10
	.p2align 3
.L488:
	movslq	%eax, %rdx
	subl	$1, %eax
	addl	$1, %ecx
	salq	$4, %rdx
	addq	%rdi, %rdx
	addq	(%rdx), %r8
	addsd	8(%rdx), %xmm1
	movq	%r8, %rdx
	cmpl	$-1, %eax
	je	.L485
	cmpl	%esi, %ecx
	jne	.L488
.L486:
	ucomisd	%xmm0, %xmm1
	jnp	.L498
.L496:
	testq	%rdx, %rdx
	js	.L491
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L495
.L499:
	movsd	.LC124(%rip), %xmm2
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm2, %xmm1
	orpd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	cmpl	%ecx, %esi
	je	.L486
	movl	$9, %eax
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L491:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jb	.L499
.L495:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	jne	.L496
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE20111:
	.size	_ZN2v88internal8GCTracer12AverageSpeedERKNS_4base10RingBufferISt4pairImdEEE, .-_ZN2v88internal8GCTracer12AverageSpeedERKNS_4base10RingBufferISt4pairImdEEE
	.section	.text._ZN2v88internal8GCTracer29RecordIncrementalMarkingSpeedEmd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer29RecordIncrementalMarkingSpeedEmd
	.type	_ZN2v88internal8GCTracer29RecordIncrementalMarkingSpeedEmd, @function
_ZN2v88internal8GCTracer29RecordIncrementalMarkingSpeedEmd:
.LFB20112:
	.cfi_startproc
	endbr64
	pxor	%xmm2, %xmm2
	movl	$0, %edx
	movapd	%xmm0, %xmm1
	ucomisd	%xmm2, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L500
	testq	%rsi, %rsi
	je	.L500
	js	.L502
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L503:
	divsd	%xmm1, %xmm0
	movsd	2496(%rdi), %xmm1
	ucomisd	%xmm2, %xmm1
	jp	.L504
	jne	.L504
.L507:
	movsd	%xmm0, 2496(%rdi)
.L500:
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L504:
	addsd	%xmm1, %xmm0
	mulsd	.LC133(%rip), %xmm0
	jmp	.L507
	.cfi_endproc
.LFE20112:
	.size	_ZN2v88internal8GCTracer29RecordIncrementalMarkingSpeedEmd, .-_ZN2v88internal8GCTracer29RecordIncrementalMarkingSpeedEmd
	.section	.text._ZN2v88internal8GCTracer19RecordEmbedderSpeedEmd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer19RecordEmbedderSpeedEmd
	.type	_ZN2v88internal8GCTracer19RecordEmbedderSpeedEmd, @function
_ZN2v88internal8GCTracer19RecordEmbedderSpeedEmd:
.LFB20113:
	.cfi_startproc
	endbr64
	pxor	%xmm2, %xmm2
	movl	$0, %edx
	movapd	%xmm0, %xmm1
	ucomisd	%xmm2, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L508
	testq	%rsi, %rsi
	je	.L508
	js	.L510
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L511:
	divsd	%xmm1, %xmm0
	movsd	2504(%rdi), %xmm1
	ucomisd	%xmm2, %xmm1
	jp	.L512
	jne	.L512
.L515:
	movsd	%xmm0, 2504(%rdi)
.L508:
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L512:
	addsd	%xmm1, %xmm0
	mulsd	.LC133(%rip), %xmm0
	jmp	.L515
	.cfi_endproc
.LFE20113:
	.size	_ZN2v88internal8GCTracer19RecordEmbedderSpeedEmd, .-_ZN2v88internal8GCTracer19RecordEmbedderSpeedEmd
	.section	.text._ZN2v88internal8GCTracer24RecordMutatorUtilizationEdd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer24RecordMutatorUtilizationEdd
	.type	_ZN2v88internal8GCTracer24RecordMutatorUtilizationEdd, @function
_ZN2v88internal8GCTracer24RecordMutatorUtilizationEdd:
.LFB20114:
	.cfi_startproc
	endbr64
	movsd	2856(%rdi), %xmm3
	pxor	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm3
	jp	.L517
	jne	.L517
	movsd	%xmm0, 2856(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	movsd	2840(%rdi), %xmm5
	movapd	%xmm0, %xmm4
	subsd	%xmm3, %xmm4
	movsd	2832(%rdi), %xmm3
	ucomisd	%xmm2, %xmm5
	movapd	%xmm4, %xmm6
	subsd	%xmm1, %xmm6
	jp	.L520
	jne	.L520
	ucomisd	%xmm2, %xmm3
	jp	.L520
	jne	.L520
	movapd	%xmm6, %xmm3
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L520:
	addsd	%xmm5, %xmm1
	addsd	%xmm6, %xmm3
	movsd	.LC133(%rip), %xmm5
	mulsd	%xmm5, %xmm1
	mulsd	%xmm5, %xmm3
.L522:
	ucomisd	%xmm2, %xmm4
	unpcklpd	%xmm1, %xmm3
	movups	%xmm3, 2832(%rdi)
	jp	.L530
	je	.L524
.L530:
	divsd	%xmm4, %xmm6
	movapd	%xmm6, %xmm2
.L524:
	unpcklpd	%xmm0, %xmm2
	movups	%xmm2, 2848(%rdi)
	ret
	.cfi_endproc
.LFE20114:
	.size	_ZN2v88internal8GCTracer24RecordMutatorUtilizationEdd, .-_ZN2v88internal8GCTracer24RecordMutatorUtilizationEdd
	.section	.text._ZNK2v88internal8GCTracer36AverageMarkCompactMutatorUtilizationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer36AverageMarkCompactMutatorUtilizationEv
	.type	_ZNK2v88internal8GCTracer36AverageMarkCompactMutatorUtilizationEv, @function
_ZNK2v88internal8GCTracer36AverageMarkCompactMutatorUtilizationEv:
.LFB20115:
	.cfi_startproc
	endbr64
	movsd	2832(%rdi), %xmm0
	movsd	2840(%rdi), %xmm1
	addsd	%xmm0, %xmm1
	ucomisd	.LC110(%rip), %xmm1
	jnp	.L537
.L536:
	divsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	jne	.L536
	movsd	.LC124(%rip), %xmm0
	ret
	.cfi_endproc
.LFE20115:
	.size	_ZNK2v88internal8GCTracer36AverageMarkCompactMutatorUtilizationEv, .-_ZNK2v88internal8GCTracer36AverageMarkCompactMutatorUtilizationEv
	.section	.text._ZNK2v88internal8GCTracer36CurrentMarkCompactMutatorUtilizationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer36CurrentMarkCompactMutatorUtilizationEv
	.type	_ZNK2v88internal8GCTracer36CurrentMarkCompactMutatorUtilizationEv, @function
_ZNK2v88internal8GCTracer36CurrentMarkCompactMutatorUtilizationEv:
.LFB20116:
	.cfi_startproc
	endbr64
	movsd	2848(%rdi), %xmm0
	ret
	.cfi_endproc
.LFE20116:
	.size	_ZNK2v88internal8GCTracer36CurrentMarkCompactMutatorUtilizationEv, .-_ZNK2v88internal8GCTracer36CurrentMarkCompactMutatorUtilizationEv
	.section	.text._ZNK2v88internal8GCTracer44IncrementalMarkingSpeedInBytesPerMillisecondEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer44IncrementalMarkingSpeedInBytesPerMillisecondEv
	.type	_ZNK2v88internal8GCTracer44IncrementalMarkingSpeedInBytesPerMillisecondEv, @function
_ZNK2v88internal8GCTracer44IncrementalMarkingSpeedInBytesPerMillisecondEv:
.LFB20117:
	.cfi_startproc
	endbr64
	movsd	2496(%rdi), %xmm0
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L539
	jne	.L539
	movsd	2480(%rdi), %xmm2
	ucomisd	%xmm1, %xmm2
	jp	.L545
	movsd	.LC129(%rip), %xmm0
	jne	.L545
.L539:
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	movq	2472(%rdi), %rax
	testq	%rax, %rax
	js	.L542
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L543:
	divsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L542:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L543
	.cfi_endproc
.LFE20117:
	.size	_ZNK2v88internal8GCTracer44IncrementalMarkingSpeedInBytesPerMillisecondEv, .-_ZNK2v88internal8GCTracer44IncrementalMarkingSpeedInBytesPerMillisecondEv
	.section	.text._ZNK2v88internal8GCTracer34EmbedderSpeedInBytesPerMillisecondEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer34EmbedderSpeedInBytesPerMillisecondEv
	.type	_ZNK2v88internal8GCTracer34EmbedderSpeedInBytesPerMillisecondEv, @function
_ZNK2v88internal8GCTracer34EmbedderSpeedInBytesPerMillisecondEv:
.LFB20118:
	.cfi_startproc
	endbr64
	movsd	2504(%rdi), %xmm0
	ret
	.cfi_endproc
.LFE20118:
	.size	_ZNK2v88internal8GCTracer34EmbedderSpeedInBytesPerMillisecondEv, .-_ZNK2v88internal8GCTracer34EmbedderSpeedInBytesPerMillisecondEv
	.section	.text._ZNK2v88internal8GCTracer34ScavengeSpeedInBytesPerMillisecondENS0_17ScavengeSpeedModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer34ScavengeSpeedInBytesPerMillisecondENS0_17ScavengeSpeedModeE
	.type	_ZNK2v88internal8GCTracer34ScavengeSpeedInBytesPerMillisecondENS0_17ScavengeSpeedModeE, @function
_ZNK2v88internal8GCTracer34ScavengeSpeedInBytesPerMillisecondENS0_17ScavengeSpeedModeE:
.LFB20119:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jne	.L549
	movl	3028(%rdi), %esi
	movl	3024(%rdi), %edx
	addl	%esi, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%esi, %esi
	jle	.L563
	pxor	%xmm0, %xmm0
	addq	$2864, %rdi
	xorl	%r8d, %r8d
	movapd	%xmm0, %xmm1
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L555:
	movslq	%eax, %rdx
	subl	$1, %eax
	addl	$1, %ecx
	salq	$4, %rdx
	addq	%rdi, %rdx
	addq	(%rdx), %r8
	addsd	8(%rdx), %xmm1
	movq	%r8, %rdx
	cmpl	$-1, %eax
	je	.L552
	cmpl	%ecx, %esi
	jne	.L555
.L565:
	ucomisd	%xmm0, %xmm1
	jnp	.L587
.L579:
	testq	%rdx, %rdx
	js	.L570
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L576
.L588:
	movsd	.LC124(%rip), %xmm2
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm2, %xmm1
	orpd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L549:
	movl	3196(%rdi), %esi
	movl	3192(%rdi), %edx
	addl	%esi, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%esi, %esi
	jle	.L563
	pxor	%xmm0, %xmm0
	addq	$3032, %rdi
	xorl	%r8d, %r8d
	movapd	%xmm0, %xmm1
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L567:
	movslq	%eax, %rdx
	subl	$1, %eax
	addl	$1, %ecx
	salq	$4, %rdx
	addq	%rdi, %rdx
	addq	(%rdx), %r8
	addsd	8(%rdx), %xmm1
	movq	%r8, %rdx
	cmpl	$-1, %eax
	je	.L564
	cmpl	%esi, %ecx
	jne	.L567
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L564:
	cmpl	%ecx, %esi
	je	.L565
	movl	$9, %eax
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L552:
	cmpl	%ecx, %esi
	je	.L565
	movl	$9, %eax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L570:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jb	.L588
.L576:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L587:
	jne	.L579
	ret
	.p2align 4,,10
	.p2align 3
.L563:
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE20119:
	.size	_ZNK2v88internal8GCTracer34ScavengeSpeedInBytesPerMillisecondENS0_17ScavengeSpeedModeE, .-_ZNK2v88internal8GCTracer34ScavengeSpeedInBytesPerMillisecondENS0_17ScavengeSpeedModeE
	.section	.text._ZNK2v88internal8GCTracer36CompactionSpeedInBytesPerMillisecondEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer36CompactionSpeedInBytesPerMillisecondEv
	.type	_ZNK2v88internal8GCTracer36CompactionSpeedInBytesPerMillisecondEv, @function
_ZNK2v88internal8GCTracer36CompactionSpeedInBytesPerMillisecondEv:
.LFB20120:
	.cfi_startproc
	endbr64
	movl	3364(%rdi), %esi
	movl	3360(%rdi), %edx
	addl	%esi, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%esi, %esi
	jle	.L591
	pxor	%xmm0, %xmm0
	addq	$3200, %rdi
	xorl	%r8d, %r8d
	movapd	%xmm0, %xmm1
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L595:
	movslq	%eax, %rdx
	subl	$1, %eax
	addl	$1, %ecx
	salq	$4, %rdx
	addq	%rdi, %rdx
	addq	(%rdx), %r8
	addsd	8(%rdx), %xmm1
	movq	%r8, %rdx
	cmpl	$-1, %eax
	je	.L592
	cmpl	%esi, %ecx
	jne	.L595
.L593:
	ucomisd	%xmm0, %xmm1
	jnp	.L605
.L603:
	testq	%rdx, %rdx
	js	.L598
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L602
.L606:
	movsd	.LC124(%rip), %xmm2
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm2, %xmm1
	orpd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L592:
	cmpl	%ecx, %esi
	je	.L593
	movl	$9, %eax
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L598:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jb	.L606
.L602:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L605:
	jne	.L603
	ret
	.p2align 4,,10
	.p2align 3
.L591:
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE20120:
	.size	_ZNK2v88internal8GCTracer36CompactionSpeedInBytesPerMillisecondEv, .-_ZNK2v88internal8GCTracer36CompactionSpeedInBytesPerMillisecondEv
	.section	.text._ZNK2v88internal8GCTracer37MarkCompactSpeedInBytesPerMillisecondEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer37MarkCompactSpeedInBytesPerMillisecondEv
	.type	_ZNK2v88internal8GCTracer37MarkCompactSpeedInBytesPerMillisecondEv, @function
_ZNK2v88internal8GCTracer37MarkCompactSpeedInBytesPerMillisecondEv:
.LFB20121:
	.cfi_startproc
	endbr64
	movl	3700(%rdi), %esi
	movl	3696(%rdi), %edx
	addl	%esi, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%esi, %esi
	jle	.L609
	pxor	%xmm0, %xmm0
	addq	$3536, %rdi
	xorl	%r8d, %r8d
	movapd	%xmm0, %xmm1
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L613:
	movslq	%eax, %rdx
	subl	$1, %eax
	addl	$1, %ecx
	salq	$4, %rdx
	addq	%rdi, %rdx
	addq	(%rdx), %r8
	addsd	8(%rdx), %xmm1
	movq	%r8, %rdx
	cmpl	$-1, %eax
	je	.L610
	cmpl	%esi, %ecx
	jne	.L613
.L611:
	ucomisd	%xmm0, %xmm1
	jnp	.L623
.L621:
	testq	%rdx, %rdx
	js	.L616
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L620
.L624:
	movsd	.LC124(%rip), %xmm2
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm2, %xmm1
	orpd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	cmpl	%ecx, %esi
	je	.L611
	movl	$9, %eax
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L616:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jb	.L624
.L620:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L623:
	jne	.L621
	ret
	.p2align 4,,10
	.p2align 3
.L609:
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE20121:
	.size	_ZNK2v88internal8GCTracer37MarkCompactSpeedInBytesPerMillisecondEv, .-_ZNK2v88internal8GCTracer37MarkCompactSpeedInBytesPerMillisecondEv
	.section	.text._ZNK2v88internal8GCTracer53FinalIncrementalMarkCompactSpeedInBytesPerMillisecondEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer53FinalIncrementalMarkCompactSpeedInBytesPerMillisecondEv
	.type	_ZNK2v88internal8GCTracer53FinalIncrementalMarkCompactSpeedInBytesPerMillisecondEv, @function
_ZNK2v88internal8GCTracer53FinalIncrementalMarkCompactSpeedInBytesPerMillisecondEv:
.LFB20122:
	.cfi_startproc
	endbr64
	movl	3532(%rdi), %esi
	movl	3528(%rdi), %edx
	addl	%esi, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%esi, %esi
	jle	.L627
	pxor	%xmm0, %xmm0
	addq	$3368, %rdi
	xorl	%r8d, %r8d
	movapd	%xmm0, %xmm1
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L631:
	movslq	%eax, %rdx
	subl	$1, %eax
	addl	$1, %ecx
	salq	$4, %rdx
	addq	%rdi, %rdx
	addq	(%rdx), %r8
	addsd	8(%rdx), %xmm1
	movq	%r8, %rdx
	cmpl	$-1, %eax
	je	.L628
	cmpl	%esi, %ecx
	jne	.L631
.L629:
	ucomisd	%xmm0, %xmm1
	jnp	.L641
.L639:
	testq	%rdx, %rdx
	js	.L634
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L638
.L642:
	movsd	.LC124(%rip), %xmm2
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm2, %xmm1
	orpd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L628:
	cmpl	%ecx, %esi
	je	.L629
	movl	$9, %eax
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L634:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jb	.L642
.L638:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	jne	.L639
	ret
	.p2align 4,,10
	.p2align 3
.L627:
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE20122:
	.size	_ZNK2v88internal8GCTracer53FinalIncrementalMarkCompactSpeedInBytesPerMillisecondEv, .-_ZNK2v88internal8GCTracer53FinalIncrementalMarkCompactSpeedInBytesPerMillisecondEv
	.section	.text._ZN2v88internal8GCTracer45CombinedMarkCompactSpeedInBytesPerMillisecondEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer45CombinedMarkCompactSpeedInBytesPerMillisecondEv
	.type	_ZN2v88internal8GCTracer45CombinedMarkCompactSpeedInBytesPerMillisecondEv, @function
_ZN2v88internal8GCTracer45CombinedMarkCompactSpeedInBytesPerMillisecondEv:
.LFB20123:
	.cfi_startproc
	endbr64
	movsd	2816(%rdi), %xmm0
	pxor	%xmm1, %xmm1
	comisd	%xmm1, %xmm0
	jbe	.L702
.L643:
	ret
	.p2align 4,,10
	.p2align 3
.L702:
	movl	3700(%rdi), %ecx
	movl	3696(%rdi), %eax
	addl	%ecx, %eax
	leal	-1(%rax), %edx
	subl	$11, %eax
	cmpl	$9, %edx
	cmovg	%eax, %edx
	testl	%ecx, %ecx
	jle	.L651
	leaq	3536(%rdi), %r10
	movl	%edx, %r8d
	xorl	%r9d, %r9d
	movapd	%xmm1, %xmm2
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L650:
	movslq	%r8d, %rax
	subl	$1, %r8d
	addl	$1, %esi
	salq	$4, %rax
	addq	%r10, %rax
	addq	(%rax), %r9
	addsd	8(%rax), %xmm2
	movq	%r9, %rax
	cmpl	$-1, %r8d
	je	.L647
	cmpl	%ecx, %esi
	jne	.L650
.L648:
	ucomisd	%xmm1, %xmm2
	jnp	.L703
.L695:
	testq	%rax, %rax
	js	.L653
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L654:
	divsd	%xmm2, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L688
	movsd	.LC124(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jnb	.L689
	comisd	%xmm1, %xmm0
	movsd	%xmm0, 2816(%rdi)
	ja	.L643
.L687:
	movsd	2496(%rdi), %xmm3
	ucomisd	%xmm1, %xmm3
	jp	.L656
	jne	.L656
	movsd	2480(%rdi), %xmm0
	ucomisd	%xmm1, %xmm0
	jp	.L696
	movsd	.LC129(%rip), %xmm3
	jne	.L696
.L656:
	movl	3532(%rdi), %r9d
	movl	3528(%rdi), %eax
	addl	%r9d, %eax
	leal	-1(%rax), %esi
	subl	$11, %eax
	cmpl	$9, %esi
	cmovg	%eax, %esi
	testl	%r9d, %r9d
	jle	.L673
	leaq	3368(%rdi), %r11
	xorl	%r10d, %r10d
	xorl	%r8d, %r8d
	movapd	%xmm1, %xmm0
	.p2align 4,,10
	.p2align 3
.L665:
	movslq	%esi, %rax
	subl	$1, %esi
	addl	$1, %r8d
	salq	$4, %rax
	addq	%r11, %rax
	addq	(%rax), %r10
	addsd	8(%rax), %xmm0
	movq	%r10, %rax
	cmpl	$-1, %esi
	je	.L662
	cmpl	%r8d, %r9d
	jne	.L665
.L663:
	ucomisd	%xmm1, %xmm0
	jnp	.L704
.L697:
	testq	%rax, %rax
	js	.L668
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
.L669:
	divsd	%xmm0, %xmm2
	comisd	.LC128(%rip), %xmm2
	jnb	.L691
	movsd	.LC124(%rip), %xmm0
	comisd	%xmm2, %xmm0
	jnb	.L692
	movsd	.LC133(%rip), %xmm0
	comisd	%xmm3, %xmm0
	ja	.L673
	comisd	%xmm2, %xmm0
	jbe	.L675
	.p2align 4,,10
	.p2align 3
.L673:
	testl	%ecx, %ecx
	jle	.L676
	leaq	3536(%rdi), %r10
	xorl	%r9d, %r9d
	movapd	%xmm1, %xmm2
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L680:
	movslq	%edx, %rax
	subl	$1, %edx
	addl	$1, %esi
	salq	$4, %rax
	addq	%r10, %rax
	addq	(%rax), %r9
	addsd	8(%rax), %xmm2
	movq	%r9, %r8
	cmpl	$-1, %edx
	je	.L677
	cmpl	%esi, %ecx
	jne	.L680
.L678:
	ucomisd	%xmm1, %xmm2
	jnp	.L705
.L698:
	testq	%r8, %r8
	js	.L683
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r8, %xmm0
.L684:
	divsd	%xmm2, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L688
	movsd	.LC124(%rip), %xmm2
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm2, %xmm1
	orpd	%xmm1, %xmm0
.L701:
	movsd	%xmm0, 2816(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	cmpl	%esi, %ecx
	je	.L648
	movl	$9, %r8d
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L653:
	movq	%rax, %rsi
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rsi
	orq	%rax, %rsi
	cvtsi2sdq	%rsi, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L662:
	cmpl	%r8d, %r9d
	je	.L663
	movl	$9, %esi
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L677:
	cmpl	%esi, %ecx
	je	.L678
	movl	$9, %edx
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L703:
	jne	.L695
.L651:
	movq	$0x000000000, 2816(%rdi)
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L688:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	movsd	%xmm0, 2816(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L668:
	movq	%rax, %rsi
	andl	$1, %eax
	pxor	%xmm2, %xmm2
	shrq	%rsi
	orq	%rax, %rsi
	cvtsi2sdq	%rsi, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L704:
	je	.L673
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L696:
	movq	2472(%rdi), %rax
	testq	%rax, %rax
	js	.L658
	pxor	%xmm3, %xmm3
	cvtsi2sdq	%rax, %xmm3
.L659:
	divsd	%xmm0, %xmm3
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L691:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm2
.L670:
	movsd	.LC133(%rip), %xmm0
	comisd	%xmm3, %xmm0
	ja	.L673
.L675:
	movapd	%xmm3, %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm2
	divsd	%xmm2, %xmm0
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L658:
	movq	%rax, %rsi
	andl	$1, %eax
	pxor	%xmm3, %xmm3
	shrq	%rsi
	orq	%rax, %rsi
	cvtsi2sdq	%rsi, %xmm3
	addsd	%xmm3, %xmm3
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L689:
	movapd	%xmm2, %xmm0
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L683:
	movq	%r8, %rax
	andl	$1, %r8d
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%r8, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L705:
	movapd	%xmm1, %xmm0
	je	.L701
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L692:
	movapd	%xmm0, %xmm2
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L676:
	movapd	%xmm1, %xmm0
	jmp	.L701
	.cfi_endproc
.LFE20123:
	.size	_ZN2v88internal8GCTracer45CombinedMarkCompactSpeedInBytesPerMillisecondEv, .-_ZN2v88internal8GCTracer45CombinedMarkCompactSpeedInBytesPerMillisecondEv
	.section	.text._ZN2v88internal8GCTracer34CombineSpeedsInBytesPerMillisecondEdd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer34CombineSpeedsInBytesPerMillisecondEdd
	.type	_ZN2v88internal8GCTracer34CombineSpeedsInBytesPerMillisecondEdd, @function
_ZN2v88internal8GCTracer34CombineSpeedsInBytesPerMillisecondEdd:
.LFB20124:
	.cfi_startproc
	endbr64
	movsd	.LC133(%rip), %xmm2
	comisd	%xmm1, %xmm2
	ja	.L706
	movapd	%xmm1, %xmm2
	addsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm0
.L706:
	ret
	.cfi_endproc
.LFE20124:
	.size	_ZN2v88internal8GCTracer34CombineSpeedsInBytesPerMillisecondEdd, .-_ZN2v88internal8GCTracer34CombineSpeedsInBytesPerMillisecondEdd
	.section	.text._ZNK2v88internal8GCTracer49NewSpaceAllocationThroughputInBytesPerMillisecondEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer49NewSpaceAllocationThroughputInBytesPerMillisecondEd
	.type	_ZNK2v88internal8GCTracer49NewSpaceAllocationThroughputInBytesPerMillisecondEd, @function
_ZNK2v88internal8GCTracer49NewSpaceAllocationThroughputInBytesPerMillisecondEd:
.LFB20125:
	.cfi_startproc
	endbr64
	movl	3868(%rdi), %ecx
	movl	3864(%rdi), %esi
	movapd	%xmm0, %xmm2
	pxor	%xmm0, %xmm0
	movq	2792(%rdi), %rdx
	movsd	2784(%rdi), %xmm1
	addl	%ecx, %esi
	leal	-1(%rsi), %eax
	subl	$11, %esi
	cmpl	$9, %eax
	cmovg	%esi, %eax
	testl	%ecx, %ecx
	jle	.L711
	pxor	%xmm0, %xmm0
	addq	$3704, %rdi
	movl	$0, %r8d
	ucomisd	%xmm0, %xmm2
	jp	.L712
	jne	.L712
	.p2align 4,,10
	.p2align 3
.L716:
	movslq	%eax, %rsi
	subl	$1, %eax
	addl	$1, %r8d
	salq	$4, %rsi
	addq	%rdi, %rsi
	addsd	8(%rsi), %xmm1
	addq	(%rsi), %rdx
	cmpl	$-1, %eax
	je	.L713
	cmpl	%r8d, %ecx
	jne	.L716
.L711:
	ucomisd	%xmm0, %xmm1
	jnp	.L732
.L728:
	testq	%rdx, %rdx
	js	.L722
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L727
.L734:
	movsd	.LC124(%rip), %xmm2
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm2, %xmm1
	orpd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L733:
	cmpl	%r8d, %ecx
	je	.L711
	movl	$9, %eax
.L712:
	movslq	%eax, %rsi
	salq	$4, %rsi
	addq	%rdi, %rsi
	comisd	%xmm2, %xmm1
	movq	(%rsi), %r9
	movsd	8(%rsi), %xmm3
	jnb	.L717
	addsd	%xmm3, %xmm1
	addq	%r9, %rdx
.L717:
	subl	$1, %eax
	addl	$1, %r8d
	cmpl	$-1, %eax
	je	.L733
	cmpl	%r8d, %ecx
	jne	.L712
	ucomisd	%xmm0, %xmm1
	jp	.L728
.L732:
	jne	.L728
	ret
	.p2align 4,,10
	.p2align 3
.L713:
	cmpl	%r8d, %ecx
	je	.L711
	movl	$9, %eax
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L722:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jb	.L734
.L727:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	ret
	.cfi_endproc
.LFE20125:
	.size	_ZNK2v88internal8GCTracer49NewSpaceAllocationThroughputInBytesPerMillisecondEd, .-_ZNK2v88internal8GCTracer49NewSpaceAllocationThroughputInBytesPerMillisecondEd
	.section	.text._ZNK2v88internal8GCTracer54OldGenerationAllocationThroughputInBytesPerMillisecondEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer54OldGenerationAllocationThroughputInBytesPerMillisecondEd
	.type	_ZNK2v88internal8GCTracer54OldGenerationAllocationThroughputInBytesPerMillisecondEd, @function
_ZNK2v88internal8GCTracer54OldGenerationAllocationThroughputInBytesPerMillisecondEd:
.LFB20126:
	.cfi_startproc
	endbr64
	movl	4036(%rdi), %ecx
	movl	4032(%rdi), %esi
	movapd	%xmm0, %xmm2
	pxor	%xmm0, %xmm0
	movq	2800(%rdi), %rdx
	movsd	2784(%rdi), %xmm1
	addl	%ecx, %esi
	leal	-1(%rsi), %eax
	subl	$11, %esi
	cmpl	$9, %eax
	cmovg	%esi, %eax
	testl	%ecx, %ecx
	jle	.L737
	pxor	%xmm0, %xmm0
	addq	$3872, %rdi
	movl	$0, %r8d
	ucomisd	%xmm0, %xmm2
	jp	.L738
	jne	.L738
	.p2align 4,,10
	.p2align 3
.L742:
	movslq	%eax, %rsi
	subl	$1, %eax
	addl	$1, %r8d
	salq	$4, %rsi
	addq	%rdi, %rsi
	addsd	8(%rsi), %xmm1
	addq	(%rsi), %rdx
	cmpl	$-1, %eax
	je	.L739
	cmpl	%r8d, %ecx
	jne	.L742
.L737:
	ucomisd	%xmm0, %xmm1
	jnp	.L758
.L754:
	testq	%rdx, %rdx
	js	.L748
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L753
.L760:
	movsd	.LC124(%rip), %xmm2
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm2, %xmm1
	orpd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L759:
	cmpl	%r8d, %ecx
	je	.L737
	movl	$9, %eax
.L738:
	movslq	%eax, %rsi
	salq	$4, %rsi
	addq	%rdi, %rsi
	comisd	%xmm2, %xmm1
	movq	(%rsi), %r9
	movsd	8(%rsi), %xmm3
	jnb	.L743
	addsd	%xmm3, %xmm1
	addq	%r9, %rdx
.L743:
	subl	$1, %eax
	addl	$1, %r8d
	cmpl	$-1, %eax
	je	.L759
	cmpl	%r8d, %ecx
	jne	.L738
	ucomisd	%xmm0, %xmm1
	jp	.L754
.L758:
	jne	.L754
	ret
	.p2align 4,,10
	.p2align 3
.L739:
	cmpl	%r8d, %ecx
	je	.L737
	movl	$9, %eax
	jmp	.L742
	.p2align 4,,10
	.p2align 3
.L748:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jb	.L760
.L753:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	ret
	.cfi_endproc
.LFE20126:
	.size	_ZNK2v88internal8GCTracer54OldGenerationAllocationThroughputInBytesPerMillisecondEd, .-_ZNK2v88internal8GCTracer54OldGenerationAllocationThroughputInBytesPerMillisecondEd
	.section	.text._ZNK2v88internal8GCTracer49EmbedderAllocationThroughputInBytesPerMillisecondEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer49EmbedderAllocationThroughputInBytesPerMillisecondEd
	.type	_ZNK2v88internal8GCTracer49EmbedderAllocationThroughputInBytesPerMillisecondEd, @function
_ZNK2v88internal8GCTracer49EmbedderAllocationThroughputInBytesPerMillisecondEd:
.LFB20127:
	.cfi_startproc
	endbr64
	movl	4204(%rdi), %ecx
	movl	4200(%rdi), %esi
	movapd	%xmm0, %xmm2
	pxor	%xmm0, %xmm0
	movq	2808(%rdi), %rdx
	movsd	2784(%rdi), %xmm1
	addl	%ecx, %esi
	leal	-1(%rsi), %eax
	subl	$11, %esi
	cmpl	$9, %eax
	cmovg	%esi, %eax
	testl	%ecx, %ecx
	jle	.L763
	pxor	%xmm0, %xmm0
	addq	$4040, %rdi
	movl	$0, %r8d
	ucomisd	%xmm0, %xmm2
	jp	.L764
	jne	.L764
	.p2align 4,,10
	.p2align 3
.L768:
	movslq	%eax, %rsi
	subl	$1, %eax
	addl	$1, %r8d
	salq	$4, %rsi
	addq	%rdi, %rsi
	addsd	8(%rsi), %xmm1
	addq	(%rsi), %rdx
	cmpl	$-1, %eax
	je	.L765
	cmpl	%r8d, %ecx
	jne	.L768
.L763:
	ucomisd	%xmm0, %xmm1
	jnp	.L784
.L780:
	testq	%rdx, %rdx
	js	.L774
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L779
.L786:
	movsd	.LC124(%rip), %xmm2
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm2, %xmm1
	orpd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L785:
	cmpl	%r8d, %ecx
	je	.L763
	movl	$9, %eax
.L764:
	movslq	%eax, %rsi
	salq	$4, %rsi
	addq	%rdi, %rsi
	comisd	%xmm2, %xmm1
	movq	(%rsi), %r9
	movsd	8(%rsi), %xmm3
	jnb	.L769
	addsd	%xmm3, %xmm1
	addq	%r9, %rdx
.L769:
	subl	$1, %eax
	addl	$1, %r8d
	cmpl	$-1, %eax
	je	.L785
	cmpl	%r8d, %ecx
	jne	.L764
	ucomisd	%xmm0, %xmm1
	jp	.L780
.L784:
	jne	.L780
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	cmpl	%r8d, %ecx
	je	.L763
	movl	$9, %eax
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L774:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jb	.L786
.L779:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	ret
	.cfi_endproc
.LFE20127:
	.size	_ZNK2v88internal8GCTracer49EmbedderAllocationThroughputInBytesPerMillisecondEd, .-_ZNK2v88internal8GCTracer49EmbedderAllocationThroughputInBytesPerMillisecondEd
	.section	.text._ZNK2v88internal8GCTracer41AllocationThroughputInBytesPerMillisecondEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer41AllocationThroughputInBytesPerMillisecondEd
	.type	_ZNK2v88internal8GCTracer41AllocationThroughputInBytesPerMillisecondEd, @function
_ZNK2v88internal8GCTracer41AllocationThroughputInBytesPerMillisecondEd:
.LFB20128:
	.cfi_startproc
	endbr64
	movl	3868(%rdi), %ecx
	movl	3864(%rdi), %esi
	movapd	%xmm0, %xmm4
	movq	2792(%rdi), %rax
	movsd	2784(%rdi), %xmm2
	addl	%ecx, %esi
	leal	-1(%rsi), %edx
	subl	$11, %esi
	cmpl	$9, %edx
	cmovg	%esi, %edx
	testl	%ecx, %ecx
	jle	.L818
	pxor	%xmm1, %xmm1
	movapd	%xmm2, %xmm3
	movl	$0, %r8d
	ucomisd	%xmm1, %xmm0
	leaq	3704(%rdi), %r9
	jp	.L790
	jne	.L790
	.p2align 4,,10
	.p2align 3
.L794:
	movslq	%edx, %rsi
	subl	$1, %edx
	addl	$1, %r8d
	salq	$4, %rsi
	addq	%r9, %rsi
	addsd	8(%rsi), %xmm3
	addq	(%rsi), %rax
	cmpl	$-1, %edx
	je	.L791
	cmpl	%r8d, %ecx
	jne	.L794
.L789:
	ucomisd	%xmm1, %xmm3
	jnp	.L830
.L825:
	testq	%rax, %rax
	js	.L800
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	%xmm3, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L821
.L834:
	movsd	.LC124(%rip), %xmm5
	movapd	%xmm0, %xmm3
	cmpnlesd	%xmm5, %xmm3
	andpd	%xmm3, %xmm0
	andnpd	%xmm5, %xmm3
	orpd	%xmm3, %xmm0
.L798:
	movl	4036(%rdi), %ecx
	movl	4032(%rdi), %esi
	movq	2800(%rdi), %rdx
	addl	%ecx, %esi
	leal	-1(%rsi), %eax
	subl	$11, %esi
	cmpl	$9, %eax
	cmovg	%esi, %eax
	testl	%ecx, %ecx
	jle	.L804
	addq	$3872, %rdi
	movl	$0, %r8d
	ucomisd	%xmm1, %xmm4
	jp	.L805
	jne	.L805
	.p2align 4,,10
	.p2align 3
.L809:
	movslq	%eax, %rsi
	subl	$1, %eax
	addl	$1, %r8d
	salq	$4, %rsi
	addq	%rdi, %rsi
	addsd	8(%rsi), %xmm2
	addq	(%rsi), %rdx
	cmpl	$-1, %eax
	je	.L806
	cmpl	%r8d, %ecx
	jne	.L809
.L804:
	ucomisd	%xmm1, %xmm2
	jnp	.L831
.L827:
	testq	%rdx, %rdx
	js	.L815
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
	divsd	%xmm2, %xmm1
	comisd	.LC128(%rip), %xmm1
	jnb	.L824
.L835:
	movsd	.LC124(%rip), %xmm3
	movapd	%xmm1, %xmm2
	cmpnlesd	%xmm3, %xmm2
	andpd	%xmm2, %xmm1
	andnpd	%xmm3, %xmm2
	orpd	%xmm2, %xmm1
	addsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L832:
	cmpl	%r8d, %ecx
	je	.L789
	movl	$9, %edx
.L790:
	movslq	%edx, %rsi
	salq	$4, %rsi
	addq	%r9, %rsi
	comisd	%xmm4, %xmm3
	movq	(%rsi), %r10
	movsd	8(%rsi), %xmm0
	jnb	.L795
	addsd	%xmm0, %xmm3
	addq	%r10, %rax
.L795:
	subl	$1, %edx
	addl	$1, %r8d
	cmpl	$-1, %edx
	je	.L832
	cmpl	%r8d, %ecx
	jne	.L790
	ucomisd	%xmm1, %xmm3
	jp	.L825
.L830:
	jne	.L825
	movapd	%xmm1, %xmm0
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L791:
	cmpl	%r8d, %ecx
	je	.L789
	movl	$9, %edx
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L833:
	cmpl	%r8d, %ecx
	je	.L804
	movl	$9, %eax
.L805:
	movslq	%eax, %rsi
	salq	$4, %rsi
	addq	%rdi, %rsi
	comisd	%xmm4, %xmm2
	movq	(%rsi), %r9
	movsd	8(%rsi), %xmm3
	jnb	.L810
	addsd	%xmm3, %xmm2
	addq	%r9, %rdx
.L810:
	subl	$1, %eax
	addl	$1, %r8d
	cmpl	$-1, %eax
	je	.L833
	cmpl	%r8d, %ecx
	jne	.L805
	ucomisd	%xmm1, %xmm2
	jp	.L827
.L831:
	jne	.L827
	addsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L800:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm3, %xmm0
	comisd	.LC128(%rip), %xmm0
	jb	.L834
.L821:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L806:
	cmpl	%r8d, %ecx
	je	.L804
	movl	$9, %eax
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L815:
	movq	%rdx, %rax
	andl	$1, %edx
	pxor	%xmm1, %xmm1
	shrq	%rax
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	divsd	%xmm2, %xmm1
	comisd	.LC128(%rip), %xmm1
	jb	.L835
.L824:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm1
	addsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L818:
	movapd	%xmm2, %xmm3
	pxor	%xmm1, %xmm1
	jmp	.L789
	.cfi_endproc
.LFE20128:
	.size	_ZNK2v88internal8GCTracer41AllocationThroughputInBytesPerMillisecondEd, .-_ZNK2v88internal8GCTracer41AllocationThroughputInBytesPerMillisecondEd
	.section	.text._ZNK2v88internal8GCTracer48CurrentAllocationThroughputInBytesPerMillisecondEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer48CurrentAllocationThroughputInBytesPerMillisecondEv
	.type	_ZNK2v88internal8GCTracer48CurrentAllocationThroughputInBytesPerMillisecondEv, @function
_ZNK2v88internal8GCTracer48CurrentAllocationThroughputInBytesPerMillisecondEv:
.LFB20129:
	.cfi_startproc
	endbr64
	movl	3868(%rdi), %r8d
	movl	3864(%rdi), %ecx
	movq	2792(%rdi), %rax
	movsd	2784(%rdi), %xmm3
	addl	%r8d, %ecx
	leal	-1(%rcx), %edx
	subl	$11, %ecx
	cmpl	$9, %edx
	cmovg	%ecx, %edx
	testl	%r8d, %r8d
	jle	.L859
	movsd	.LC134(%rip), %xmm0
	leaq	3704(%rdi), %r10
	movapd	%xmm3, %xmm2
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L842:
	movslq	%edx, %rcx
	salq	$4, %rcx
	addq	%r10, %rcx
	comisd	%xmm0, %xmm2
	movq	(%rcx), %r9
	movsd	8(%rcx), %xmm1
	jnb	.L839
	addsd	%xmm1, %xmm2
	addq	%r9, %rax
.L839:
	subl	$1, %edx
	addl	$1, %esi
	cmpl	$-1, %edx
	je	.L868
	cmpl	%esi, %r8d
	jne	.L842
.L838:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm2
	jnp	.L869
.L864:
	testq	%rax, %rax
	js	.L845
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L846:
	divsd	%xmm2, %xmm0
	comisd	.LC128(%rip), %xmm0
	movapd	%xmm0, %xmm2
	jnb	.L861
	movsd	.LC124(%rip), %xmm5
	movapd	%xmm0, %xmm4
	cmpnlesd	%xmm5, %xmm4
	andpd	%xmm4, %xmm2
	andnpd	%xmm5, %xmm4
	movapd	%xmm2, %xmm0
	movapd	%xmm4, %xmm2
	orpd	%xmm0, %xmm2
.L843:
	movl	4036(%rdi), %r8d
	movl	4032(%rdi), %edx
	movq	2800(%rdi), %rsi
	addl	%r8d, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%r8d, %r8d
	jle	.L849
	movsd	.LC134(%rip), %xmm0
	addq	$3872, %rdi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L853:
	movslq	%eax, %rdx
	salq	$4, %rdx
	addq	%rdi, %rdx
	comisd	%xmm0, %xmm3
	movq	(%rdx), %r9
	movsd	8(%rdx), %xmm4
	jnb	.L850
	addsd	%xmm4, %xmm3
	addq	%r9, %rsi
.L850:
	subl	$1, %eax
	addl	$1, %ecx
	cmpl	$-1, %eax
	je	.L870
	cmpl	%ecx, %r8d
	jne	.L853
.L849:
	ucomisd	%xmm1, %xmm3
	jnp	.L871
.L866:
	testq	%rsi, %rsi
	js	.L856
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	divsd	%xmm3, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L863
.L872:
	movsd	.LC124(%rip), %xmm3
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm3, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm3, %xmm1
	orpd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L868:
	cmpl	%esi, %r8d
	je	.L838
	movl	$9, %edx
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L845:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L870:
	cmpl	%ecx, %r8d
	je	.L849
	movl	$9, %eax
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L869:
	jne	.L864
	movapd	%xmm1, %xmm2
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L871:
	jne	.L866
	movapd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L856:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm3, %xmm0
	comisd	.LC128(%rip), %xmm0
	jb	.L872
.L863:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	addsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L861:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm2
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L859:
	movapd	%xmm3, %xmm2
	jmp	.L838
	.cfi_endproc
.LFE20129:
	.size	_ZNK2v88internal8GCTracer48CurrentAllocationThroughputInBytesPerMillisecondEv, .-_ZNK2v88internal8GCTracer48CurrentAllocationThroughputInBytesPerMillisecondEv
	.section	.text._ZNK2v88internal8GCTracer61CurrentOldGenerationAllocationThroughputInBytesPerMillisecondEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer61CurrentOldGenerationAllocationThroughputInBytesPerMillisecondEv
	.type	_ZNK2v88internal8GCTracer61CurrentOldGenerationAllocationThroughputInBytesPerMillisecondEv, @function
_ZNK2v88internal8GCTracer61CurrentOldGenerationAllocationThroughputInBytesPerMillisecondEv:
.LFB20130:
	.cfi_startproc
	endbr64
	movl	4036(%rdi), %r8d
	movl	4032(%rdi), %edx
	movq	2800(%rdi), %rsi
	movsd	2784(%rdi), %xmm1
	addl	%r8d, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%r8d, %r8d
	jle	.L875
	addq	$3872, %rdi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L879:
	movslq	%eax, %rdx
	salq	$4, %rdx
	addq	%rdi, %rdx
	comisd	.LC134(%rip), %xmm1
	movq	(%rdx), %r9
	movsd	8(%rdx), %xmm0
	jnb	.L876
	addsd	%xmm0, %xmm1
	addq	%r9, %rsi
.L876:
	subl	$1, %eax
	addl	$1, %ecx
	cmpl	$-1, %eax
	je	.L889
	cmpl	%ecx, %r8d
	jne	.L879
.L875:
	pxor	%xmm3, %xmm3
	ucomisd	%xmm3, %xmm1
	jnp	.L890
.L887:
	testq	%rsi, %rsi
	js	.L882
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L886
.L891:
	movsd	.LC124(%rip), %xmm2
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm2, %xmm1
	orpd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L889:
	cmpl	%ecx, %r8d
	je	.L875
	movl	$9, %eax
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L882:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jb	.L891
.L886:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L890:
	jne	.L887
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE20130:
	.size	_ZNK2v88internal8GCTracer61CurrentOldGenerationAllocationThroughputInBytesPerMillisecondEv, .-_ZNK2v88internal8GCTracer61CurrentOldGenerationAllocationThroughputInBytesPerMillisecondEv
	.section	.text._ZNK2v88internal8GCTracer56CurrentEmbedderAllocationThroughputInBytesPerMillisecondEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer56CurrentEmbedderAllocationThroughputInBytesPerMillisecondEv
	.type	_ZNK2v88internal8GCTracer56CurrentEmbedderAllocationThroughputInBytesPerMillisecondEv, @function
_ZNK2v88internal8GCTracer56CurrentEmbedderAllocationThroughputInBytesPerMillisecondEv:
.LFB20131:
	.cfi_startproc
	endbr64
	movl	4204(%rdi), %r8d
	movl	4200(%rdi), %edx
	movq	2808(%rdi), %rsi
	movsd	2784(%rdi), %xmm1
	addl	%r8d, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%r8d, %r8d
	jle	.L894
	addq	$4040, %rdi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L898:
	movslq	%eax, %rdx
	salq	$4, %rdx
	addq	%rdi, %rdx
	comisd	.LC134(%rip), %xmm1
	movq	(%rdx), %r9
	movsd	8(%rdx), %xmm0
	jnb	.L895
	addsd	%xmm0, %xmm1
	addq	%r9, %rsi
.L895:
	subl	$1, %eax
	addl	$1, %ecx
	cmpl	$-1, %eax
	je	.L908
	cmpl	%ecx, %r8d
	jne	.L898
.L894:
	pxor	%xmm3, %xmm3
	ucomisd	%xmm3, %xmm1
	jnp	.L909
.L906:
	testq	%rsi, %rsi
	js	.L901
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jnb	.L905
.L910:
	movsd	.LC124(%rip), %xmm2
	movapd	%xmm0, %xmm1
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm1, %xmm0
	andnpd	%xmm2, %xmm1
	orpd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L908:
	cmpl	%ecx, %r8d
	je	.L894
	movl	$9, %eax
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L901:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm1, %xmm0
	comisd	.LC128(%rip), %xmm0
	jb	.L910
.L905:
	movq	.LC128(%rip), %rax
	movq	%rax, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	jne	.L906
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE20131:
	.size	_ZNK2v88internal8GCTracer56CurrentEmbedderAllocationThroughputInBytesPerMillisecondEv, .-_ZNK2v88internal8GCTracer56CurrentEmbedderAllocationThroughputInBytesPerMillisecondEv
	.section	.text._ZNK2v88internal8GCTracer33ContextDisposalRateInMillisecondsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer33ContextDisposalRateInMillisecondsEv
	.type	_ZNK2v88internal8GCTracer33ContextDisposalRateInMillisecondsEv, @function
_ZNK2v88internal8GCTracer33ContextDisposalRateInMillisecondsEv:
.LFB20132:
	.cfi_startproc
	endbr64
	cmpl	$9, 4292(%rdi)
	jg	.L925
	pxor	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L925:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movl	4292(%rbx), %esi
	movl	4288(%rbx), %edx
	addl	%esi, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%esi, %esi
	jle	.L914
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L918:
	movslq	%eax, %rcx
	subl	$1, %eax
	addl	$1, %edx
	movsd	4208(%rbx,%rcx,8), %xmm1
	cmpl	$-1, %eax
	je	.L926
	cmpl	%edx, %esi
	jne	.L918
.L916:
	subsd	%xmm1, %xmm0
.L914:
	pxor	%xmm1, %xmm1
	addq	$8, %rsp
	cvtsi2sdl	%esi, %xmm1
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	divsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L926:
	.cfi_restore_state
	cmpl	%edx, %esi
	je	.L916
	movl	$9, %eax
	jmp	.L918
	.cfi_endproc
.LFE20132:
	.size	_ZNK2v88internal8GCTracer33ContextDisposalRateInMillisecondsEv, .-_ZNK2v88internal8GCTracer33ContextDisposalRateInMillisecondsEv
	.section	.text._ZNK2v88internal8GCTracer20AverageSurvivalRatioEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer20AverageSurvivalRatioEv
	.type	_ZNK2v88internal8GCTracer20AverageSurvivalRatioEv, @function
_ZNK2v88internal8GCTracer20AverageSurvivalRatioEv:
.LFB20136:
	.cfi_startproc
	endbr64
	movl	4380(%rdi), %esi
	pxor	%xmm0, %xmm0
	testl	%esi, %esi
	je	.L927
	movl	4376(%rdi), %edx
	addl	%esi, %edx
	leal	-1(%rdx), %eax
	subl	$11, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	testl	%esi, %esi
	jle	.L930
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L933:
	movslq	%eax, %rcx
	subl	$1, %eax
	addl	$1, %edx
	addsd	4296(%rdi,%rcx,8), %xmm0
	cmpl	$-1, %eax
	je	.L931
	cmpl	%edx, %esi
	jne	.L933
.L930:
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	divsd	%xmm1, %xmm0
.L927:
	ret
	.p2align 4,,10
	.p2align 3
.L931:
	cmpl	%edx, %esi
	je	.L930
	movl	$9, %eax
	jmp	.L933
	.cfi_endproc
.LFE20136:
	.size	_ZNK2v88internal8GCTracer20AverageSurvivalRatioEv, .-_ZNK2v88internal8GCTracer20AverageSurvivalRatioEv
	.section	.text._ZNK2v88internal8GCTracer22SurvivalEventsRecordedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal8GCTracer22SurvivalEventsRecordedEv
	.type	_ZNK2v88internal8GCTracer22SurvivalEventsRecordedEv, @function
_ZNK2v88internal8GCTracer22SurvivalEventsRecordedEv:
.LFB20140:
	.cfi_startproc
	endbr64
	movl	4380(%rdi), %eax
	testl	%eax, %eax
	setg	%al
	ret
	.cfi_endproc
.LFE20140:
	.size	_ZNK2v88internal8GCTracer22SurvivalEventsRecordedEv, .-_ZNK2v88internal8GCTracer22SurvivalEventsRecordedEv
	.section	.text._ZN2v88internal8GCTracer19ResetSurvivalEventsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer19ResetSurvivalEventsEv
	.type	_ZN2v88internal8GCTracer19ResetSurvivalEventsEv, @function
_ZN2v88internal8GCTracer19ResetSurvivalEventsEv:
.LFB20141:
	.cfi_startproc
	endbr64
	movq	$0, 4376(%rdi)
	ret
	.cfi_endproc
.LFE20141:
	.size	_ZN2v88internal8GCTracer19ResetSurvivalEventsEv, .-_ZN2v88internal8GCTracer19ResetSurvivalEventsEv
	.section	.text._ZN2v88internal8GCTracer29NotifyIncrementalMarkingStartEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer29NotifyIncrementalMarkingStartEv
	.type	_ZN2v88internal8GCTracer29NotifyIncrementalMarkingStartEv, @function
_ZN2v88internal8GCTracer29NotifyIncrementalMarkingStartEv:
.LFB20142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movsd	%xmm0, 2488(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20142:
	.size	_ZN2v88internal8GCTracer29NotifyIncrementalMarkingStartEv, .-_ZN2v88internal8GCTracer29NotifyIncrementalMarkingStartEv
	.section	.text._ZN2v88internal8GCTracer34FetchBackgroundMarkCompactCountersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer34FetchBackgroundMarkCompactCountersEv
	.type	_ZN2v88internal8GCTracer34FetchBackgroundMarkCompactCountersEv, @function
_ZN2v88internal8GCTracer34FetchBackgroundMarkCompactCountersEv:
.LFB20143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	4384(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movupd	4448(%rbx), %xmm2
	pxor	%xmm1, %xmm1
	movq	%r12, %rdi
	movupd	936(%rbx), %xmm0
	movupd	4464(%rbx), %xmm3
	movups	%xmm1, 4448(%rbx)
	movups	%xmm1, 4464(%rbx)
	addpd	%xmm2, %xmm0
	movups	%xmm0, 936(%rbx)
	movupd	952(%rbx), %xmm0
	addpd	%xmm3, %xmm0
	movups	%xmm0, 952(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%rbx), %rax
	cvttsd2sil	952(%rbx), %esi
	movq	3368(%rax), %rdi
	addq	$48, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	(%rbx), %rax
	cvttsd2sil	960(%rbx), %esi
	popq	%rbx
	popq	%r12
	movq	3368(%rax), %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	subq	$-128, %rdi
	jmp	_ZN2v88internal9Histogram9AddSampleEi@PLT
	.cfi_endproc
.LFE20143:
	.size	_ZN2v88internal8GCTracer34FetchBackgroundMarkCompactCountersEv, .-_ZN2v88internal8GCTracer34FetchBackgroundMarkCompactCountersEv
	.section	.text._ZN2v88internal8GCTracer30FetchBackgroundMinorGCCountersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer30FetchBackgroundMinorGCCountersEv
	.type	_ZN2v88internal8GCTracer30FetchBackgroundMinorGCCountersEv, @function
_ZN2v88internal8GCTracer30FetchBackgroundMinorGCCountersEv:
.LFB20144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	4384(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movupd	968(%rbx), %xmm0
	pxor	%xmm1, %xmm1
	movq	%r12, %rdi
	movupd	4480(%rbx), %xmm2
	movupd	4496(%rbx), %xmm3
	movups	%xmm1, 4480(%rbx)
	movups	%xmm1, 4496(%rbx)
	addpd	%xmm2, %xmm0
	movups	%xmm0, 968(%rbx)
	movupd	984(%rbx), %xmm0
	addpd	%xmm3, %xmm0
	movups	%xmm0, 984(%rbx)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%rbx), %rax
	cvttsd2sil	992(%rbx), %esi
	popq	%rbx
	popq	%r12
	movq	3368(%rax), %rdi
	popq	%rbp
	.cfi_def_cfa 7, 8
	addq	$88, %rdi
	jmp	_ZN2v88internal9Histogram9AddSampleEi@PLT
	.cfi_endproc
.LFE20144:
	.size	_ZN2v88internal8GCTracer30FetchBackgroundMinorGCCountersEv, .-_ZN2v88internal8GCTracer30FetchBackgroundMinorGCCountersEv
	.section	.text._ZN2v88internal8GCTracer30FetchBackgroundGeneralCountersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer30FetchBackgroundGeneralCountersEv
	.type	_ZN2v88internal8GCTracer30FetchBackgroundGeneralCountersEv, @function
_ZN2v88internal8GCTracer30FetchBackgroundGeneralCountersEv:
.LFB20145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	4384(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movupd	912(%rbx), %xmm0
	movupd	4424(%rbx), %xmm1
	movq	%r12, %rdi
	addpd	%xmm1, %xmm0
	movups	%xmm0, 912(%rbx)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 4424(%rbx)
	movsd	928(%rbx), %xmm0
	addsd	4440(%rbx), %xmm0
	movq	$0x000000000, 4440(%rbx)
	movsd	%xmm0, 928(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE20145:
	.size	_ZN2v88internal8GCTracer30FetchBackgroundGeneralCountersEv, .-_ZN2v88internal8GCTracer30FetchBackgroundGeneralCountersEv
	.section	.text._ZN2v88internal8GCTracer23FetchBackgroundCountersEiiii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer23FetchBackgroundCountersEiiii
	.type	_ZN2v88internal8GCTracer23FetchBackgroundCountersEiiii, @function
_ZN2v88internal8GCTracer23FetchBackgroundCountersEiiii:
.LFB20146:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	4384(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r15, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	movl	%r13d, %ecx
	subl	%ebx, %ecx
	js	.L948
	leal	1(%rcx), %esi
	testl	%ecx, %ecx
	je	.L949
	movslq	%r12d, %rax
	movslq	%ebx, %rdx
	movl	%esi, %ecx
	leaq	128(%r14,%rax,8), %rax
	leaq	4424(%r14,%rdx,8), %rdx
	shrl	%ecx
	movupd	(%rax), %xmm0
	movupd	(%rdx), %xmm2
	addpd	%xmm2, %xmm0
	movups	%xmm0, (%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdx)
	cmpl	$1, %ecx
	je	.L950
	movupd	16(%rax), %xmm1
	movupd	16(%rdx), %xmm3
	addpd	%xmm3, %xmm1
	movups	%xmm1, 16(%rax)
	movups	%xmm0, 16(%rdx)
	cmpl	$2, %ecx
	je	.L950
	movupd	32(%rax), %xmm1
	movupd	32(%rdx), %xmm4
	addpd	%xmm4, %xmm1
	movups	%xmm1, 32(%rax)
	movups	%xmm0, 32(%rdx)
	cmpl	$3, %ecx
	je	.L950
	movupd	48(%rax), %xmm1
	movupd	48(%rdx), %xmm5
	addpd	%xmm5, %xmm1
	movups	%xmm1, 48(%rax)
	movups	%xmm0, 48(%rdx)
	cmpl	$4, %ecx
	je	.L950
	movupd	64(%rax), %xmm1
	movupd	64(%rdx), %xmm6
	addpd	%xmm6, %xmm1
	movups	%xmm1, 64(%rax)
	movups	%xmm0, 64(%rdx)
	cmpl	$5, %ecx
	je	.L950
	movupd	80(%rax), %xmm1
	movupd	80(%rdx), %xmm7
	addpd	%xmm7, %xmm1
	movups	%xmm1, 80(%rax)
	movups	%xmm0, 80(%rdx)
	cmpl	$6, %ecx
	je	.L950
	movl	$96, %edi
	movl	$6, %r8d
	movapd	%xmm0, %xmm1
.L951:
	movupd	(%rax,%rdi), %xmm0
	movupd	(%rdx,%rdi), %xmm7
	addl	$1, %r8d
	addpd	%xmm7, %xmm0
	movups	%xmm0, (%rax,%rdi)
	movups	%xmm1, (%rdx,%rdi)
	addq	$16, %rdi
	cmpl	%ecx, %r8d
	jne	.L951
	.p2align 4,,10
	.p2align 3
.L950:
	movl	%esi, %ecx
	andl	$-2, %ecx
	andl	$1, %esi
	je	.L948
.L949:
	leal	(%r12,%rcx), %esi
	addl	%ebx, %ecx
	movslq	%esi, %rsi
	movslq	%ecx, %rcx
	leaq	(%r14,%rsi,8), %rdx
	leaq	(%r14,%rcx,8), %rax
	movsd	128(%rdx), %xmm0
	addsd	4424(%rax), %xmm0
	movsd	%xmm0, 128(%rdx)
	movq	$0x000000000, 4424(%rax)
.L948:
	addq	$8, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE20146:
	.size	_ZN2v88internal8GCTracer23FetchBackgroundCountersEiiii, .-_ZN2v88internal8GCTracer23FetchBackgroundCountersEiiii
	.section	.text._ZN2v88internal8GCTracer24AddBackgroundScopeSampleENS1_15BackgroundScope7ScopeIdEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer24AddBackgroundScopeSampleENS1_15BackgroundScope7ScopeIdEd
	.type	_ZN2v88internal8GCTracer24AddBackgroundScopeSampleENS1_15BackgroundScope7ScopeIdEd, @function
_ZN2v88internal8GCTracer24AddBackgroundScopeSampleENS1_15BackgroundScope7ScopeIdEd:
.LFB20147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	4384(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movslq	%esi, %rbx
	subq	$24, %rsp
	movsd	%xmm0, -40(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	leaq	(%r12,%rbx,8), %rax
	movsd	-40(%rbp), %xmm0
	movq	%r13, %rdi
	addsd	4424(%rax), %xmm0
	movsd	%xmm0, 4424(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE20147:
	.size	_ZN2v88internal8GCTracer24AddBackgroundScopeSampleENS1_15BackgroundScope7ScopeIdEd, .-_ZN2v88internal8GCTracer24AddBackgroundScopeSampleENS1_15BackgroundScope7ScopeIdEd
	.section	.text._ZN2v88internal8GCTracer24RecordGCPhasesHistogramsEPNS0_14TimedHistogramE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer24RecordGCPhasesHistogramsEPNS0_14TimedHistogramE
	.type	_ZN2v88internal8GCTracer24RecordGCPhasesHistogramsEPNS0_14TimedHistogramE, @function
_ZN2v88internal8GCTracer24RecordGCPhasesHistogramsEPNS0_14TimedHistogramE:
.LFB20148:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rax
	movq	3368(%rax), %r12
	leaq	3192(%r12), %rax
	cmpq	%rax, %rsi
	je	.L990
	leaq	3480(%r12), %rax
	cmpq	%rax, %rsi
	je	.L991
.L978:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L990:
	.cfi_restore_state
	cvttsd2sil	264(%rdi), %esi
	leaq	408(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cvttsd2sil	272(%rbx), %esi
	leaq	448(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cvttsd2sil	280(%rbx), %esi
	leaq	488(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cvttsd2sil	288(%rbx), %esi
	leaq	528(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cvttsd2sil	296(%rbx), %esi
	leaq	568(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cvttsd2sil	304(%rbx), %esi
	leaq	608(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cvttsd2sil	312(%rbx), %esi
	leaq	648(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movsd	2480(%rbx), %xmm0
	comisd	.LC110(%rip), %xmm0
	jbe	.L980
	movq	(%rbx), %rax
	cvttsd2sil	%xmm0, %esi
	movq	3368(%rax), %rdi
	addq	$328, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movsd	2480(%rbx), %xmm0
.L980:
	addsd	296(%rbx), %xmm0
	movq	(%rbx), %rax
	movq	3368(%rax), %rdi
	cvttsd2sil	%xmm0, %esi
	movsd	%xmm0, -24(%rbp)
	addq	$808, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	call	_ZN2v84base9TimeTicks16IsHighResolutionEv@PLT
	testb	%al, %al
	je	.L978
	movq	(%rbx), %rdi
	call	_ZN2v88internal4Heap13SizeOfObjectsEv@PLT
	cmpq	$1048576, %rax
	jbe	.L978
	movsd	-24(%rbp), %xmm2
	subsd	512(%rbx), %xmm2
	movq	(%rbx), %rdi
	movsd	%xmm2, -24(%rbp)
	call	_ZN2v88internal4Heap13SizeOfObjectsEv@PLT
	testq	%rax, %rax
	js	.L985
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L986:
	divsd	-24(%rbp), %xmm0
	mulsd	.LC135(%rip), %xmm0
	movsd	.LC136(%rip), %xmm1
	movq	(%rbx), %rax
	movq	3368(%rax), %rdi
	mulsd	%xmm1, %xmm0
	addq	$848, %rdi
	mulsd	%xmm1, %xmm0
	cvttsd2sil	%xmm0, %esi
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L991:
	cvttsd2sil	872(%rdi), %esi
	leaq	688(%r12), %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	cvttsd2sil	880(%rbx), %esi
	leaq	728(%r12), %rdi
.L989:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Histogram9AddSampleEi@PLT
.L985:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L986
	.cfi_endproc
.LFE20148:
	.size	_ZN2v88internal8GCTracer24RecordGCPhasesHistogramsEPNS0_14TimedHistogramE, .-_ZN2v88internal8GCTracer24RecordGCPhasesHistogramsEPNS0_14TimedHistogramE
	.section	.rodata._ZN2v88internal8GCTracer19RecordGCSumCountersEd.str1.1,"aMS",@progbits,1
.LC137:
	.string	"disabled-by-default-v8.gc"
.LC138:
	.string	"duration"
.LC139:
	.string	"background_duration"
.LC140:
	.string	"V8.GCMarkCompactorSummary"
	.section	.rodata._ZN2v88internal8GCTracer19RecordGCSumCountersEd.str1.8,"aMS",@progbits,1
	.align 8
.LC141:
	.string	"V8.GCMarkCompactorMarkingSummary"
	.section	.text._ZN2v88internal8GCTracer19RecordGCSumCountersEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer19RecordGCSumCountersEd
	.type	_ZN2v88internal8GCTracer19RecordGCSumCountersEd, @function
_ZN2v88internal8GCTracer19RecordGCSumCountersEd:
.LFB20149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	4384(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$152, %rsp
	movsd	%xmm0, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base5Mutex4LockEv@PLT
	movsd	1168(%rbx), %xmm0
	movq	(%rbx), %rax
	movsd	1192(%rbx), %xmm1
	movsd	2480(%rbx), %xmm3
	movsd	1120(%rbx), %xmm4
	movsd	-104(%rbp), %xmm2
	addsd	%xmm0, %xmm1
	movsd	4448(%rbx), %xmm6
	movsd	4456(%rbx), %xmm7
	movsd	%xmm3, -120(%rbp)
	movq	3368(%rax), %rdi
	movsd	4464(%rbx), %xmm5
	movsd	%xmm4, -128(%rbp)
	movsd	%xmm6, -144(%rbp)
	movsd	4472(%rbx), %xmm6
	movsd	%xmm1, -112(%rbp)
	addsd	1216(%rbx), %xmm1
	addq	$768, %rdi
	movsd	%xmm7, -152(%rbp)
	movsd	296(%rbx), %xmm7
	movsd	%xmm5, -104(%rbp)
	movapd	%xmm1, %xmm0
	movsd	%xmm6, -160(%rbp)
	addsd	%xmm3, %xmm0
	movsd	%xmm7, -168(%rbp)
	addsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm2
	cvttsd2sil	%xmm2, %esi
	movsd	%xmm2, -136(%rbp)
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	_ZZN2v88internal8GCTracer19RecordGCSumCountersEdE29trace_event_unique_atomic1259(%rip), %rdx
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L1029
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1030
.L996:
	movq	_ZZN2v88internal8GCTracer19RecordGCSumCountersEdE29trace_event_unique_atomic1263(%rip), %rdx
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L1031
.L1001:
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L1032
.L1003:
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1033
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1029:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1034
.L995:
	movq	%r13, _ZZN2v88internal8GCTracer19RecordGCSumCountersEdE29trace_event_unique_atomic1259(%rip)
	movzbl	0(%r13), %eax
	testb	$5, %al
	je	.L996
.L1030:
	leaq	.LC139(%rip), %rax
	leaq	.LC138(%rip), %rcx
	movl	$1028, %edx
	movq	-136(%rbp), %xmm5
	movq	%rcx, %xmm0
	movq	%rax, %xmm6
	movw	%dx, -42(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -96(%rbp)
	movsd	-144(%rbp), %xmm0
	addsd	-152(%rbp), %xmm0
	addsd	-104(%rbp), %xmm0
	addsd	-160(%rbp), %xmm0
	punpcklqdq	%xmm0, %xmm5
	pxor	%xmm0, %xmm0
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1035
.L997:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L998
	movq	(%rdi), %rax
	call	*8(%rax)
.L998:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L996
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	_ZZN2v88internal8GCTracer19RecordGCSumCountersEdE29trace_event_unique_atomic1263(%rip), %rdx
	movq	%rdx, %r13
	testq	%rdx, %rdx
	jne	.L1001
.L1031:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1036
.L1002:
	movq	%r13, _ZZN2v88internal8GCTracer19RecordGCSumCountersEdE29trace_event_unique_atomic1263(%rip)
	movzbl	0(%r13), %eax
	testb	$5, %al
	je	.L1003
.L1032:
	leaq	.LC139(%rip), %rax
	leaq	.LC138(%rip), %rcx
	movq	%rax, %xmm6
	movq	%rcx, %xmm0
	movl	$1028, %eax
	punpcklqdq	%xmm6, %xmm0
	movw	%ax, -42(%rbp)
	movaps	%xmm0, -96(%rbp)
	movsd	-112(%rbp), %xmm0
	addsd	-120(%rbp), %xmm0
	addsd	-128(%rbp), %xmm0
	addsd	-168(%rbp), %xmm0
	movhps	-104(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r10
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r10
	jne	.L1037
.L1004:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1005
	movq	(%rdi), %rax
	call	*8(%rax)
.L1005:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1003
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1034:
	leaq	.LC137(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L1035:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$16
	leaq	.LC140(%rip), %rcx
	movl	$73, %esi
	pushq	%rdx
	leaq	-80(%rbp), %rdx
	pushq	%rdx
	leaq	-42(%rbp), %rdx
	pushq	%rdx
	leaq	-96(%rbp), %rdx
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$2
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1036:
	leaq	.LC137(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1037:
	subq	$8, %rsp
	leaq	-64(%rbp), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$16
	leaq	.LC141(%rip), %rcx
	movq	%r13, %rdx
	movl	$73, %esi
	pushq	%rax
	leaq	-80(%rbp), %rax
	pushq	%rax
	leaq	-42(%rbp), %rax
	pushq	%rax
	leaq	-96(%rbp), %rax
	pushq	%rax
	pushq	$2
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L1004
.L1033:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20149:
	.size	_ZN2v88internal8GCTracer19RecordGCSumCountersEd, .-_ZN2v88internal8GCTracer19RecordGCSumCountersEd
	.section	.rodata._ZN2v88internal8GCTracer4StopENS0_16GarbageCollectorE.str1.1,"aMS",@progbits,1
.LC142:
	.string	"Unknown collector"
.LC143:
	.string	"Mark-Compact"
.LC144:
	.string	"Scavenger"
	.section	.rodata._ZN2v88internal8GCTracer4StopENS0_16GarbageCollectorE.str1.8,"aMS",@progbits,1
	.align 8
.LC145:
	.string	"[Finished reentrant %s during %s.]\n"
	.section	.rodata._ZN2v88internal8GCTracer4StopENS0_16GarbageCollectorE.str1.1
.LC146:
	.string	"stats"
.LC147:
	.string	"V8.GC_Heap_Stats"
	.section	.text._ZN2v88internal8GCTracer4StopENS0_16GarbageCollectorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal8GCTracer4StopENS0_16GarbageCollectorE
	.type	_ZN2v88internal8GCTracer4StopENS0_16GarbageCollectorE, @function
_ZN2v88internal8GCTracer4StopENS0_16GarbageCollectorE:
.LFB20085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$520, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	subl	$1, 2824(%rdi)
	je	.L1039
	cmpb	$0, _ZN2v88internal21FLAG_trace_gc_verboseE(%rip)
	je	.L1038
	movq	(%rdi), %rax
	leaq	.LC114(%rip), %rcx
	leaq	-37592(%rax), %rdi
	movl	8(%r14), %eax
	cmpl	$3, %eax
	je	.L1042
	ja	.L1043
	testl	%eax, %eax
	leaq	.LC115(%rip), %rcx
	leaq	.LC113(%rip), %rax
	cmove	%rax, %rcx
.L1042:
	leaq	.LC143(%rip), %rdx
	cmpl	$1, %esi
	je	.L1044
	leaq	.LC114(%rip), %rdx
	cmpl	$2, %esi
	je	.L1044
	testl	%esi, %esi
	leaq	.LC142(%rip), %rdx
	leaq	.LC144(%rip), %rax
	cmove	%rax, %rdx
.L1044:
	leaq	.LC145(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
.L1038:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1158
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1043:
	.cfi_restore_state
	cmpl	$4, %eax
	leaq	.LC111(%rip), %rcx
	leaq	.LC119(%rip), %rax
	cmove	%rax, %rcx
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1039:
	movq	(%rdi), %rdi
	leaq	-512(%rbp), %rbx
	xorl	%r12d, %r12d
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	(%r14), %rdi
	movsd	%xmm0, 32(%r14)
	call	_ZN2v88internal4Heap13SizeOfObjectsEv@PLT
	movq	%rbx, %rdi
	movq	%rax, 56(%r14)
	movq	(%r14), %rax
	movq	2048(%rax), %rax
	movq	80(%rax), %rax
	movq	%rax, 72(%r14)
	movl	$2, -504(%rbp)
	movq	(%r14), %rax
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal18PagedSpaceIterator4NextEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L1049
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	96(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %r13
	movq	(%r15), %rax
	call	*96(%rax)
	movq	%rbx, %rdi
	addq	%r13, %rax
	addq	%rax, %r12
	call	_ZN2v88internal18PagedSpaceIterator4NextEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1045
.L1049:
	movq	(%r15), %rax
	leaq	_ZN2v88internal10PagedSpace5WasteEv(%rip), %rdx
	movq	144(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1159
	movq	%r15, %rdi
	call	*%rax
	movq	%r15, %rdi
	movq	%rax, %r13
	movq	(%r15), %rax
	call	*96(%rax)
	movq	%rbx, %rdi
	addq	%rax, %r13
	call	_ZN2v88internal18PagedSpaceIterator4NextEv@PLT
	addq	%r13, %r12
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L1049
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	(%r14), %rcx
	movsd	32(%r14), %xmm0
	movq	%r12, 88(%r14)
	movq	%r14, %rdi
	movq	1944(%rcx), %rax
	addq	1920(%rcx), %rax
	movq	%rax, 104(%r14)
	call	_ZN2v88internal8GCTracer13AddAllocationEd
	movsd	32(%r14), %xmm2
	cmpl	$4, 8(%r14)
	movapd	%xmm2, %xmm1
	subsd	24(%r14), %xmm1
	ja	.L1050
	movl	8(%r14), %eax
	leaq	.L1052(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal8GCTracer4StopENS0_16GarbageCollectorE,"a",@progbits
	.align 4
	.align 4
.L1052:
	.long	.L1053-.L1052
	.long	.L1055-.L1052
	.long	.L1054-.L1052
	.long	.L1053-.L1052
	.long	.L1051-.L1052
	.section	.text._ZN2v88internal8GCTracer4StopENS0_16GarbageCollectorE
	.p2align 4,,10
	.p2align 3
.L1053:
	movslq	3028(%r14), %rax
	movq	96(%r14), %rcx
	cmpl	$10, %eax
	je	.L1160
	leal	1(%rax), %esi
	salq	$4, %rax
	addq	%r14, %rax
	movl	%esi, 3028(%r14)
	movq	%rcx, 2864(%rax)
	movsd	%xmm1, 2872(%rax)
.L1057:
	movslq	3196(%r14), %rax
	movq	104(%r14), %rcx
	cmpl	$10, %eax
	je	.L1161
	leal	1(%rax), %esi
	salq	$4, %rax
	addq	%r14, %rax
	movl	%esi, 3196(%r14)
	movq	%rcx, 3032(%rax)
	movsd	%xmm1, 3040(%rax)
.L1059:
	leaq	4384(%r14), %r12
	movsd	%xmm1, -544(%rbp)
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	pxor	%xmm2, %xmm2
	movq	%r12, %rdi
	movupd	968(%r14), %xmm5
	movupd	4480(%r14), %xmm0
	movupd	4496(%r14), %xmm6
	movups	%xmm2, 4480(%r14)
	movups	%xmm2, 4496(%r14)
	addpd	%xmm5, %xmm0
	movups	%xmm0, 968(%r14)
	movupd	984(%r14), %xmm0
	addpd	%xmm6, %xmm0
	movups	%xmm0, 984(%r14)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%r14), %rax
	cvttsd2sil	992(%r14), %esi
	movq	3368(%rax), %rdi
	addq	$88, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movsd	-544(%rbp), %xmm1
.L1060:
	movq	%r12, %rdi
	movsd	%xmm1, -544(%rbp)
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	%r12, %rdi
	movupd	912(%r14), %xmm7
	movupd	4424(%r14), %xmm0
	addpd	%xmm7, %xmm0
	movups	%xmm0, 912(%r14)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 4424(%r14)
	movsd	928(%r14), %xmm0
	addsd	4440(%r14), %xmm0
	movq	$0x000000000, 4440(%r14)
	movsd	%xmm0, 928(%r14)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movsd	-544(%rbp), %xmm1
	movq	(%r14), %rdi
	movapd	%xmm1, %xmm0
	call	_ZN2v88internal4Heap17UpdateTotalGCTimeEd@PLT
	movl	8(%r14), %eax
	testl	%eax, %eax
	je	.L1126
	cmpl	$3, %eax
	je	.L1126
	cmpb	$0, _ZN2v88internal17FLAG_trace_gc_nvpE(%rip)
	movq	%r14, %rdi
	je	.L1091
.L1167:
	call	_ZNK2v88internal8GCTracer8PrintNVPEv
	cmpb	$0, _ZN2v88internal13FLAG_trace_gcE(%rip)
	jne	.L1162
.L1093:
	movl	_ZN2v88internal12TracingFlags2gcE(%rip), %eax
	testb	$2, %al
	je	.L1038
	movq	.LC148(%rip), %xmm1
	leaq	-320(%rbp), %r13
	leaq	-448(%rbp), %r15
	movq	%r13, %rdi
	movhps	.LC149(%rip), %xmm1
	movaps	%xmm1, -544(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rax
	leaq	-432(%rbp,%rax), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r12
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-544(%rbp), %xmm1
	movq	%r12, -448(%rbp)
	movq	-24(%r12), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -552(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -544(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	(%r14), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal4Heap22DumpJSONHeapStatisticsERNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE@PLT
	movq	_ZZN2v88internal8GCTracer4StopENS0_16GarbageCollectorEE28trace_event_unique_atomic388(%rip), %rax
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1163
.L1095:
	movzbl	(%r15), %eax
	testb	$5, %al
	jne	.L1164
.L1097:
	movq	.LC148(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC150(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-544(%rbp), %rdi
	je	.L1105
	call	_ZdlPv@PLT
.L1105:
	movq	-552(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r12, -448(%rbp)
	movq	-24(%r12), %rax
	movq	%r13, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rdx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1054:
	movsd	2512(%r14), %xmm0
	movq	2472(%r14), %rax
	movdqu	2512(%r14), %xmm5
	movdqu	2536(%r14), %xmm6
	movdqu	2584(%r14), %xmm4
	movq	%rax, 112(%r14)
	movl	2528(%r14), %eax
	movups	%xmm5, 1000(%r14)
	movsd	2480(%r14), %xmm3
	movsd	%xmm0, 128(%r14)
	movsd	2536(%r14), %xmm0
	movdqu	2560(%r14), %xmm5
	movl	%eax, 1016(%r14)
	movl	2552(%r14), %eax
	movsd	%xmm0, 136(%r14)
	movsd	2560(%r14), %xmm0
	movl	%eax, 1040(%r14)
	movl	2576(%r14), %eax
	movups	%xmm6, 1024(%r14)
	movdqu	2608(%r14), %xmm6
	movl	%eax, 1064(%r14)
	movl	2600(%r14), %eax
	movsd	%xmm0, 144(%r14)
	movsd	2584(%r14), %xmm0
	movl	%eax, 1088(%r14)
	movl	2624(%r14), %eax
	movups	%xmm5, 1048(%r14)
	movl	%eax, 1112(%r14)
	movups	%xmm4, 1072(%r14)
	movups	%xmm6, 1096(%r14)
	movsd	%xmm3, 120(%r14)
	movsd	%xmm0, 152(%r14)
	movsd	2608(%r14), %xmm0
	movl	2648(%r14), %eax
	movdqu	2632(%r14), %xmm5
	movdqu	2656(%r14), %xmm4
	movdqu	2680(%r14), %xmm6
	movl	%eax, 1136(%r14)
	movsd	%xmm0, 160(%r14)
	movl	2672(%r14), %eax
	movsd	2632(%r14), %xmm0
	movups	%xmm5, 1120(%r14)
	movdqu	2704(%r14), %xmm5
	movsd	%xmm0, 168(%r14)
	movsd	2656(%r14), %xmm0
	movl	%eax, 1160(%r14)
	movl	2696(%r14), %eax
	movsd	%xmm0, 176(%r14)
	movsd	2680(%r14), %xmm0
	movl	%eax, 1184(%r14)
	movl	2720(%r14), %eax
	movsd	%xmm0, 184(%r14)
	movsd	2704(%r14), %xmm0
	movl	%eax, 1208(%r14)
	movl	2744(%r14), %eax
	movups	%xmm4, 1144(%r14)
	movdqu	2728(%r14), %xmm4
	movsd	%xmm0, 192(%r14)
	movsd	2728(%r14), %xmm0
	movl	%eax, 1232(%r14)
	movsd	%xmm0, 200(%r14)
	pxor	%xmm0, %xmm0
	movups	%xmm6, 1168(%r14)
	movups	%xmm5, 1192(%r14)
	movups	%xmm4, 1216(%r14)
	movsd	2856(%r14), %xmm4
	ucomisd	%xmm0, %xmm4
	jp	.L1061
	jne	.L1061
	movsd	%xmm2, 2856(%r14)
.L1063:
	ucomisd	%xmm0, %xmm3
	movl	$0, %esi
	movq	112(%r14), %rax
	setnp	%cl
	cmovne	%esi, %ecx
	testb	%cl, %cl
	jne	.L1070
	testq	%rax, %rax
	je	.L1070
	js	.L1071
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rax, %xmm2
.L1072:
	divsd	%xmm3, %xmm2
	movsd	2496(%r14), %xmm3
	ucomisd	%xmm0, %xmm3
	jp	.L1073
	jne	.L1073
.L1156:
	movsd	%xmm2, 2496(%r14)
.L1070:
	movslq	3532(%r14), %rax
	movq	56(%r14), %rcx
	cmpl	$10, %eax
	je	.L1165
	leal	1(%rax), %esi
	salq	$4, %rax
	addq	%r14, %rax
	movl	%esi, 3532(%r14)
	movq	%rcx, 3368(%rax)
	movsd	%xmm1, 3376(%rax)
.L1076:
	movapd	%xmm1, %xmm0
	movq	%r14, %rdi
	movsd	%xmm1, -544(%rbp)
	leaq	4384(%r14), %r12
	call	_ZN2v88internal8GCTracer19RecordGCSumCountersEd
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, 2472(%r14)
	movq	$0x000000000, 2480(%r14)
	movl	$0, 2528(%r14)
	movl	$0, 2552(%r14)
	movl	$0, 2576(%r14)
	movl	$0, 2600(%r14)
	movl	$0, 2624(%r14)
	movl	$0, 2648(%r14)
	movl	$0, 2672(%r14)
	movl	$0, 2696(%r14)
	movl	$0, 2720(%r14)
	movl	$0, 2744(%r14)
	movq	$0x000000000, 2816(%r14)
	movups	%xmm0, 2512(%r14)
	movups	%xmm0, 2536(%r14)
	movups	%xmm0, 2560(%r14)
	movups	%xmm0, 2584(%r14)
	movups	%xmm0, 2608(%r14)
	movups	%xmm0, 2632(%r14)
	movups	%xmm0, 2656(%r14)
	movups	%xmm0, 2680(%r14)
	movups	%xmm0, 2704(%r14)
	movups	%xmm0, 2728(%r14)
	call	_ZN2v84base5Mutex4LockEv@PLT
	pxor	%xmm0, %xmm0
	movupd	936(%r14), %xmm2
	movupd	4448(%r14), %xmm3
	movupd	4464(%r14), %xmm6
	movups	%xmm0, 4448(%r14)
	addpd	%xmm3, %xmm2
	movups	%xmm2, 936(%r14)
	movupd	952(%r14), %xmm2
	addpd	%xmm6, %xmm2
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1055:
	movsd	2856(%r14), %xmm3
	pxor	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm3
	jp	.L1077
	jne	.L1077
	movsd	%xmm2, 2856(%r14)
.L1079:
	movslq	3700(%r14), %rax
	movq	56(%r14), %rcx
	cmpl	$10, %eax
	je	.L1166
	leal	1(%rax), %esi
	salq	$4, %rax
	addq	%r14, %rax
	movl	%esi, 3700(%r14)
	movq	%rcx, 3536(%rax)
	movsd	%xmm1, 3544(%rax)
.L1087:
	movapd	%xmm1, %xmm0
	movq	%r14, %rdi
	movsd	%xmm1, -544(%rbp)
	leaq	4384(%r14), %r12
	call	_ZN2v88internal8GCTracer19RecordGCSumCountersEd
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, 2472(%r14)
	movq	$0x000000000, 2480(%r14)
	movl	$0, 2528(%r14)
	movl	$0, 2552(%r14)
	movl	$0, 2576(%r14)
	movl	$0, 2600(%r14)
	movl	$0, 2624(%r14)
	movl	$0, 2648(%r14)
	movl	$0, 2672(%r14)
	movl	$0, 2696(%r14)
	movl	$0, 2720(%r14)
	movl	$0, 2744(%r14)
	movq	$0x000000000, 2816(%r14)
	movups	%xmm0, 2512(%r14)
	movups	%xmm0, 2536(%r14)
	movups	%xmm0, 2560(%r14)
	movups	%xmm0, 2584(%r14)
	movups	%xmm0, 2608(%r14)
	movups	%xmm0, 2632(%r14)
	movups	%xmm0, 2656(%r14)
	movups	%xmm0, 2680(%r14)
	movups	%xmm0, 2704(%r14)
	movups	%xmm0, 2728(%r14)
	call	_ZN2v84base5Mutex4LockEv@PLT
	pxor	%xmm0, %xmm0
	movupd	936(%r14), %xmm2
	movupd	4448(%r14), %xmm5
	movupd	952(%r14), %xmm3
	movups	%xmm0, 4448(%r14)
	addpd	%xmm5, %xmm2
	movups	%xmm2, 936(%r14)
	movupd	4464(%r14), %xmm2
	addpd	%xmm3, %xmm2
.L1157:
	movups	%xmm2, 952(%r14)
	movq	%r12, %rdi
	movups	%xmm0, 4464(%r14)
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	(%r14), %rax
	cvttsd2sil	952(%r14), %esi
	movq	3368(%rax), %rdi
	addq	$48, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	(%r14), %rax
	cvttsd2sil	960(%r14), %esi
	movq	3368(%rax), %rdi
	subq	$-128, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movsd	-544(%rbp), %xmm1
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1126:
	cmpb	$0, _ZN2v88internal30FLAG_trace_gc_ignore_scavengerE(%rip)
	jne	.L1038
	cmpb	$0, _ZN2v88internal17FLAG_trace_gc_nvpE(%rip)
	movq	%r14, %rdi
	jne	.L1167
.L1091:
	call	_ZNK2v88internal8GCTracer5PrintEv
	cmpb	$0, _ZN2v88internal13FLAG_trace_gcE(%rip)
	je	.L1093
.L1162:
	movq	(%r14), %rdi
	call	_ZN2v88internal4Heap24PrintShortHeapStatisticsEv@PLT
	jmp	.L1093
.L1050:
	leaq	4384(%r14), %r12
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1077:
	movsd	120(%r14), %xmm4
	movapd	%xmm2, %xmm5
	movsd	2840(%r14), %xmm6
	subsd	%xmm3, %xmm5
	movsd	2832(%r14), %xmm3
	addsd	%xmm1, %xmm4
	ucomisd	%xmm0, %xmm6
	movapd	%xmm5, %xmm7
	subsd	%xmm4, %xmm7
	jp	.L1080
	jne	.L1080
	ucomisd	%xmm0, %xmm3
	jp	.L1080
	jne	.L1080
	movapd	%xmm7, %xmm3
.L1082:
	ucomisd	%xmm0, %xmm5
	unpcklpd	%xmm4, %xmm3
	movups	%xmm3, 2832(%r14)
	jp	.L1125
	je	.L1084
.L1125:
	divsd	%xmm5, %xmm7
	movapd	%xmm7, %xmm0
.L1084:
	unpcklpd	%xmm2, %xmm0
	movups	%xmm0, 2848(%r14)
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1061:
	movsd	2840(%r14), %xmm8
	movapd	%xmm2, %xmm7
	movapd	%xmm3, %xmm6
	subsd	%xmm4, %xmm7
	addsd	%xmm1, %xmm6
	movsd	2832(%r14), %xmm4
	ucomisd	%xmm0, %xmm8
	movapd	%xmm7, %xmm5
	subsd	%xmm6, %xmm5
	jp	.L1064
	jne	.L1064
	ucomisd	%xmm0, %xmm4
	jp	.L1064
	jne	.L1064
	movapd	%xmm5, %xmm4
.L1066:
	ucomisd	%xmm0, %xmm7
	unpcklpd	%xmm6, %xmm4
	movups	%xmm4, 2832(%r14)
	jp	.L1121
	je	.L1151
.L1121:
	divsd	%xmm7, %xmm5
.L1068:
	unpcklpd	%xmm2, %xmm5
	movups	%xmm5, 2848(%r14)
	jmp	.L1063
.L1051:
	leaq	.LC109(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1071:
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm2, %xmm2
	shrq	%rcx
	orq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm2
	addsd	%xmm2, %xmm2
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1080:
	addsd	%xmm6, %xmm4
	addsd	%xmm7, %xmm3
	movsd	.LC133(%rip), %xmm6
	mulsd	%xmm6, %xmm4
	mulsd	%xmm6, %xmm3
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1064:
	addsd	%xmm8, %xmm6
	addsd	%xmm5, %xmm4
	movsd	.LC133(%rip), %xmm8
	mulsd	%xmm8, %xmm6
	mulsd	%xmm8, %xmm4
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1165:
	movslq	3528(%r14), %rax
	leal	1(%rax), %esi
	salq	$4, %rax
	addq	%r14, %rax
	movl	%esi, 3528(%r14)
	movq	%rcx, 3368(%rax)
	movsd	%xmm1, 3376(%rax)
	cmpl	$10, %esi
	jne	.L1076
	movl	$0, 3528(%r14)
	jmp	.L1076
	.p2align 4,,10
	.p2align 3
.L1161:
	movslq	3192(%r14), %rax
	leal	1(%rax), %esi
	salq	$4, %rax
	addq	%r14, %rax
	movl	%esi, 3192(%r14)
	movq	%rcx, 3032(%rax)
	movsd	%xmm1, 3040(%rax)
	cmpl	$10, %esi
	jne	.L1059
	movl	$0, 3192(%r14)
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1160:
	movslq	3024(%r14), %rax
	leal	1(%rax), %esi
	salq	$4, %rax
	addq	%r14, %rax
	movl	%esi, 3024(%r14)
	movq	%rcx, 2864(%rax)
	movsd	%xmm1, 2872(%rax)
	cmpl	$10, %esi
	jne	.L1057
	movl	$0, 3024(%r14)
	jmp	.L1057
	.p2align 4,,10
	.p2align 3
.L1166:
	movslq	3696(%r14), %rax
	leal	1(%rax), %esi
	salq	$4, %rax
	addq	%r14, %rax
	movl	%esi, 3696(%r14)
	movq	%rcx, 3536(%rax)
	movsd	%xmm1, 3544(%rax)
	cmpl	$10, %esi
	jne	.L1087
	movl	$0, 3696(%r14)
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1073:
	addsd	%xmm3, %xmm2
	mulsd	.LC133(%rip), %xmm2
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1151:
	movapd	%xmm0, %xmm5
	jmp	.L1068
.L1163:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1168
.L1096:
	movq	%r15, _ZZN2v88internal8GCTracer4StopENS0_16GarbageCollectorEE28trace_event_unique_atomic388(%rip)
	jmp	.L1095
.L1164:
	movq	-384(%rbp), %rax
	leaq	-464(%rbp), %r14
	movq	$0, -472(%rbp)
	leaq	-480(%rbp), %rdi
	movq	%r14, -480(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L1098
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1099
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1100:
	leaq	.LC146(%rip), %rax
	pxor	%xmm0, %xmm0
	movb	$7, -521(%rbp)
	movq	%rax, -520(%rbp)
	movq	-480(%rbp), %rax
	movaps	%xmm0, -496(%rbp)
	movq	%rax, -512(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1169
.L1101:
	movq	-488(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1102
	movq	(%rdi), %rax
	call	*8(%rax)
.L1102:
	movq	-496(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1103
	movq	(%rdi), %rax
	call	*8(%rax)
.L1103:
	movq	-480(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1097
	call	_ZdlPv@PLT
	jmp	.L1097
.L1099:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1100
.L1169:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$73, %esi
	leaq	-496(%rbp), %rdx
	pushq	$16
	leaq	.LC147(%rip), %rcx
	pushq	%rdx
	leaq	-521(%rbp), %rdx
	pushq	%rbx
	pushq	%rdx
	leaq	-520(%rbp), %rdx
	pushq	%rdx
	movq	%r15, %rdx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1101
.L1168:
	leaq	.LC137(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L1096
.L1098:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1100
.L1158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20085:
	.size	_ZN2v88internal8GCTracer4StopENS0_16GarbageCollectorE, .-_ZN2v88internal8GCTracer4StopENS0_16GarbageCollectorE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv, @function
_GLOBAL__sub_I__ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv:
.LFB25162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25162:
	.size	_GLOBAL__sub_I__ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv, .-_GLOBAL__sub_I__ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8GCTracer32worker_thread_runtime_call_statsEv
	.section	.bss._ZZN2v88internal8GCTracer19RecordGCSumCountersEdE29trace_event_unique_atomic1263,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8GCTracer19RecordGCSumCountersEdE29trace_event_unique_atomic1263, @object
	.size	_ZZN2v88internal8GCTracer19RecordGCSumCountersEdE29trace_event_unique_atomic1263, 8
_ZZN2v88internal8GCTracer19RecordGCSumCountersEdE29trace_event_unique_atomic1263:
	.zero	8
	.section	.bss._ZZN2v88internal8GCTracer19RecordGCSumCountersEdE29trace_event_unique_atomic1259,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8GCTracer19RecordGCSumCountersEdE29trace_event_unique_atomic1259, @object
	.size	_ZZN2v88internal8GCTracer19RecordGCSumCountersEdE29trace_event_unique_atomic1259, 8
_ZZN2v88internal8GCTracer19RecordGCSumCountersEdE29trace_event_unique_atomic1259:
	.zero	8
	.section	.bss._ZZN2v88internal8GCTracer4StopENS0_16GarbageCollectorEE28trace_event_unique_atomic388,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal8GCTracer4StopENS0_16GarbageCollectorEE28trace_event_unique_atomic388, @object
	.size	_ZZN2v88internal8GCTracer4StopENS0_16GarbageCollectorEE28trace_event_unique_atomic388, 8
_ZZN2v88internal8GCTracer4StopENS0_16GarbageCollectorEE28trace_event_unique_atomic388:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC110:
	.long	0
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC120:
	.long	0
	.long	1072693248
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC124:
	.long	0
	.long	1072693248
	.align 8
.LC126:
	.long	0
	.long	1051721728
	.align 8
.LC128:
	.long	0
	.long	1104150528
	.align 8
.LC129:
	.long	0
	.long	1090519040
	.align 8
.LC133:
	.long	0
	.long	1071644672
	.align 8
.LC134:
	.long	0
	.long	1085507584
	.align 8
.LC135:
	.long	0
	.long	1083129856
	.align 8
.LC136:
	.long	0
	.long	1062207488
	.section	.data.rel.ro,"aw"
	.align 8
.LC148:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC149:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC150:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
