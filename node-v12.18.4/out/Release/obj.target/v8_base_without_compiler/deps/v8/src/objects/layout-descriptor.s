	.file	"layout-descriptor.cc"
	.text
	.section	.rodata._ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"(!IsSmi() && (*layout_word_index < length())) || (IsSmi() && (*layout_word_index < 1))"
	.section	.rodata._ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi.str1.8
	.align 8
.LC2:
	.string	"GetIndexes(field_index, &layout_word_index, &layout_bit_index)"
	.section	.text._ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi
	.type	_ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi, @function
_ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi:
.LFB17782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%r12), %rax
	movq	(%rdx), %rsi
	movzbl	7(%rax), %ecx
	movzbl	8(%rax), %eax
	subl	%eax, %ecx
	je	.L3
	movq	%rdx, %r13
	cmpl	$32, %r15d
	jg	.L44
.L37:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L16
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L17:
	movq	(%r12), %rax
	movq	0(%r13), %rdx
	movzbl	7(%rax), %r8d
	movzbl	8(%rax), %eax
	subl	%eax, %r8d
	testl	%r15d, %r15d
	jle	.L20
	leal	-1(%r15), %r14d
.L19:
	leaq	(%r14,%r14,2), %rcx
	leaq	31(%rdx), %rax
	movl	$1, %r9d
	leaq	55(%rdx,%rcx,8), %rdi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L46:
	movl	%ecx, %r10d
	sarl	$5, %r10d
	andl	$1, %edx
	jne	.L27
	cmpl	%r10d, 11(%rsi)
	jle	.L27
	movl	%r9d, %edx
	sall	%cl, %edx
	testb	%r11b, %r11b
	je	.L45
.L31:
	sarq	$32, %rsi
	orl	%edx, %esi
	salq	$32, %rsi
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$24, %rax
	cmpq	%rdi, %rax
	je	.L20
.L21:
	movq	(%rax), %rcx
	sarq	$32, %rcx
	testb	$2, %cl
	jne	.L24
	movl	%ecx, %edx
	shrl	$6, %edx
	andl	$7, %edx
	cmpl	$2, %edx
	jne	.L24
	shrl	$19, %ecx
	andl	$1023, %ecx
	cmpl	%r8d, %ecx
	jge	.L24
	movq	%rsi, %rdx
	movl	$32, %r10d
	notq	%rdx
	movl	%edx, %r11d
	andl	$1, %r11d
	jne	.L25
	movslq	11(%rsi), %r10
	sall	$3, %r10d
.L25:
	cmpl	%ecx, %r10d
	ja	.L46
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L44:
	leal	-1(%r15), %eax
	leaq	31(%rsi), %rdx
	movq	%rax, %r14
	leaq	(%rax,%rax,2), %rax
	leaq	55(%rsi,%rax,8), %rdi
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rdx), %rax
	sarq	$32, %rax
	testb	$2, %al
	jne	.L9
	movl	%eax, %r8d
	shrl	$6, %r8d
	andl	$7, %r8d
	cmpl	$2, %r8d
	jne	.L9
	shrl	$19, %eax
	andl	$1023, %eax
	cmpl	%ecx, %eax
	jge	.L9
	addl	$1, %eax
	cmpl	%eax, %esi
	cmovl	%eax, %esi
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$24, %rdx
	cmpq	%rdx, %rdi
	jne	.L5
	cmpl	%ecx, %esi
	cmovle	%esi, %ecx
	testl	%ecx, %ecx
	jne	.L10
.L3:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L11
	xorl	%esi, %esi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L39:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L47
.L13:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	$0, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	cmpl	$32, %ecx
	jle	.L37
	leal	63(%rcx), %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	sarl	$3, %esi
	andl	$-8, %esi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	xorl	%esi, %esi
	movq	(%rax), %rdi
	movq	%rax, %r15
	movslq	11(%rdi), %rdx
	addq	$15, %rdi
	addl	$7, %edx
	andl	$-8, %edx
	movslq	%edx, %rdx
	call	memset@PLT
	movq	(%r12), %rax
	movq	(%r15), %rsi
	movq	0(%r13), %rdx
	movzbl	7(%rax), %r8d
	movzbl	8(%rax), %eax
	subl	%eax, %r8d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L45:
	sall	$2, %r10d
	addq	$24, %rax
	movslq	%r10d, %r10
	leaq	15(%rsi,%r10), %rcx
	movl	(%rcx), %r10d
	orl	%r10d, %edx
	movl	%edx, (%rcx)
	cmpq	%rdi, %rax
	jne	.L21
	.p2align 4,,10
	.p2align 3
.L20:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	testb	%r11b, %r11b
	je	.L29
	cmpl	$31, %ecx
	jg	.L29
	movl	%r9d, %edx
	sall	%cl, %edx
	jmp	.L31
.L34:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L48
.L36:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L39
.L16:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L49
.L18:
	leaq	8(%rax), %rdx
	xorl	%esi, %esi
	movq	%rdx, 41088(%rbx)
	movq	$0, (%rax)
	jmp	.L17
.L47:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L13
.L29:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L48:
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L36
.L49:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L18
	.cfi_endproc
.LFE17782:
	.size	_ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi, .-_ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi
	.section	.text._ZN2v88internal16LayoutDescriptor21AppendIfFastOrUseFullEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsENS4_IS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LayoutDescriptor21AppendIfFastOrUseFullEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsENS4_IS1_EE
	.type	_ZN2v88internal16LayoutDescriptor21AppendIfFastOrUseFullEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsENS4_IS1_EE, @function
_ZN2v88internal16LayoutDescriptor21AppendIfFastOrUseFullEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsENS4_IS1_EE:
.LFB17784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	(%rsi), %r8
	movq	47(%r8), %r12
	testb	$1, %r12b
	je	.L65
.L52:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
	movq	(%rsi), %rcx
	movzbl	7(%rcx), %esi
	movzbl	8(%rcx), %r8d
	testb	$2, %dl
	je	.L66
.L55:
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L57
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	%edx, %ecx
	shrl	$6, %ecx
	andl	$7, %ecx
	cmpl	$2, %ecx
	jne	.L55
	shrl	$19, %edx
	subl	%r8d, %esi
	andl	$1023, %edx
	cmpl	%esi, %edx
	jge	.L55
	cmpl	$31, %edx
	jg	.L52
	movq	%r12, %rsi
	movq	41112(%rdi), %r8
	sarq	$32, %rsi
	btsl	%edx, %esi
	salq	$32, %rsi
	testq	%r8, %r8
	je	.L61
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L57:
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L67
.L59:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%r12, (%rax)
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rdi
	jmp	.L59
.L61:
	movq	41088(%rdi), %rax
	cmpq	41096(%rdi), %rax
	je	.L68
.L63:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rdi)
	movq	%rsi, (%rax)
	jmp	.L52
.L68:
	movq	%rsi, -32(%rbp)
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-32(%rbp), %rsi
	movq	-24(%rbp), %rdi
	jmp	.L63
	.cfi_endproc
.LFE17784:
	.size	_ZN2v88internal16LayoutDescriptor21AppendIfFastOrUseFullEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsENS4_IS1_EE, .-_ZN2v88internal16LayoutDescriptor21AppendIfFastOrUseFullEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsENS4_IS1_EE
	.section	.text._ZN2v88internal16LayoutDescriptor14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LayoutDescriptor14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal16LayoutDescriptor14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal16LayoutDescriptor14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB17785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$32, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	(%rsi), %rcx
	testb	$1, %cl
	je	.L70
	movslq	11(%rcx), %rax
	sall	$3, %eax
.L70:
	cmpl	%eax, %edx
	jg	.L71
	addq	$16, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	cmpl	$32, %edx
	jle	.L81
	addl	$63, %edx
	andl	$-64, %edx
	leal	7(%rdx), %esi
	cmovns	%edx, %esi
	movl	$1, %edx
	sarl	$3, %esi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	xorl	%esi, %esi
	movq	(%rax), %rdi
	movq	%rax, %r12
	movslq	11(%rdi), %rdx
	addq	$15, %rdi
	addl	$7, %edx
	andl	$-8, %edx
	movslq	%edx, %rdx
	call	memset@PLT
	movq	(%r12), %rax
	leaq	15(%rax), %rdi
.L75:
	movq	(%rbx), %rsi
	testb	$1, %sil
	je	.L77
.L82:
	movslq	11(%rsi), %rdx
	addq	$15, %rsi
	addl	$7, %edx
	andl	$-8, %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L74
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rbx), %rsi
	movq	%rax, %r12
	movq	(%rax), %rax
	leaq	15(%rax), %rdi
	testb	$1, %sil
	jne	.L82
.L77:
	shrq	$32, %rsi
	movq	%r12, %rax
	movl	%esi, (%rdi)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	41088(%rdi), %r12
	cmpq	41096(%rdi), %r12
	je	.L83
.L76:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rdi)
	movl	$15, %edi
	movq	$0, (%r12)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rdi
	movq	%rax, %r12
	jmp	.L76
	.cfi_endproc
.LFE17785:
	.size	_ZN2v88internal16LayoutDescriptor14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal16LayoutDescriptor14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal16LayoutDescriptor11ShareAppendEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LayoutDescriptor11ShareAppendEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsE
	.type	_ZN2v88internal16LayoutDescriptor11ShareAppendEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsE, @function
_ZN2v88internal16LayoutDescriptor11ShareAppendEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsE:
.LFB17783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movq	47(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L86:
	movq	(%r12), %rdx
	movzbl	7(%rdx), %esi
	movzbl	8(%rdx), %ecx
	testb	$2, %bl
	je	.L110
.L106:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movl	%ebx, %edx
	shrl	$6, %edx
	andl	$7, %edx
	cmpl	$2, %edx
	jne	.L106
	shrl	$19, %ebx
	subl	%ecx, %esi
	andl	$1023, %ebx
	cmpl	%esi, %ebx
	jge	.L106
	leal	1(%rbx), %edx
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal16LayoutDescriptor14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi
	movl	$32, %edx
	movq	(%rax), %rsi
	movq	%rsi, %rax
	notq	%rax
	movl	%eax, %edi
	andl	$1, %edi
	je	.L111
.L93:
	cmpl	%ebx, %edx
	jbe	.L94
	movl	%ebx, %edx
	sarl	$5, %edx
	testb	$1, %al
	jne	.L95
	cmpl	%edx, 11(%rsi)
	jle	.L95
	movl	$1, %eax
	movl	%ebx, %ecx
	sall	%cl, %eax
	testb	%dil, %dil
	je	.L112
.L99:
	sarq	$32, %rsi
	orl	%eax, %esi
	salq	$32, %rsi
.L101:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L102
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L85:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L113
.L87:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L111:
	movslq	11(%rsi), %rdx
	sall	$3, %edx
	jmp	.L93
.L95:
	testb	%dil, %dil
	je	.L97
	movl	$1, %eax
	movl	%ebx, %ecx
	sall	%cl, %eax
	cmpl	$31, %ebx
	jle	.L99
.L97:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L102:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L114
.L104:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L112:
	sall	$2, %edx
	movslq	%edx, %rdx
	leaq	15(%rsi,%rdx), %rdx
	movl	(%rdx), %ecx
	orl	%ecx, %eax
	movl	%eax, (%rdx)
	jmp	.L101
.L114:
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L104
	.cfi_endproc
.LFE17783:
	.size	_ZN2v88internal16LayoutDescriptor11ShareAppendEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsE, .-_ZN2v88internal16LayoutDescriptor11ShareAppendEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS0_15PropertyDetailsE
	.section	.text._ZN2v88internal16LayoutDescriptor8IsTaggedEiiPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LayoutDescriptor8IsTaggedEiiPi
	.type	_ZN2v88internal16LayoutDescriptor8IsTaggedEiiPi, @function
_ZN2v88internal16LayoutDescriptor8IsTaggedEiiPi:
.LFB17786:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	%rcx, %r8
	testq	%rdi, %rdi
	je	.L119
	movq	%rdi, %rax
	movl	$32, %ecx
	notq	%rax
	movl	%eax, %r11d
	andl	$1, %r11d
	je	.L173
	cmpl	%ecx, %esi
	jnb	.L119
.L175:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leal	31(%rsi), %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	testl	%esi, %esi
	cmovns	%esi, %r12d
	sarl	$5, %r12d
	testb	$1, %al
	jne	.L120
	cmpl	%r12d, 11(%rdi)
	jle	.L120
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$27, %eax
	leal	(%rsi,%rax), %ecx
	andl	$31, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	testb	%r11b, %r11b
	je	.L174
.L124:
	movq	%rdi, %rbx
	shrq	$32, %rbx
	movq	%rbx, %r9
.L126:
	movl	%r9d, %ebx
	movl	%r9d, %r13d
	andl	%eax, %ebx
	notl	%r13d
	cmovne	%r13d, %r9d
	sete	%r10b
	negl	%eax
	andl	%eax, %r9d
	xorl	%eax, %eax
	rep bsfl	%r9d, %eax
	testb	%r11b, %r11b
	jne	.L128
	testl	%r9d, %r9d
	movl	$32, %r9d
	cmove	%r9d, %eax
	movl	%eax, %r9d
	movl	%eax, %r11d
	subl	%ecx, %r9d
	cmpl	$32, %eax
	je	.L130
.L172:
	cmpl	%r9d, %edx
	cmovg	%r9d, %edx
.L131:
	movl	%edx, (%r8)
	addq	$8, %rsp
	movl	%r10d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %r10d
	movl	%edx, (%r8)
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	movslq	11(%rdi), %rcx
	sall	$3, %ecx
	cmpl	%ecx, %esi
	jb	.L175
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	testl	%r9d, %r9d
	movl	$32, %edi
	cmove	%edi, %eax
	subl	%ecx, %eax
	testl	%ebx, %ebx
	jne	.L171
	addl	%eax, %esi
	cmpl	$32, %esi
	je	.L131
.L171:
	cmpl	%eax, %edx
	cmovg	%eax, %edx
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L174:
	leal	0(,%r12,4), %r9d
	movq	%rdi, %r11
	movslq	%r9d, %r9
	notq	%r11
	movl	15(%rdi,%r9), %r9d
	andl	$1, %r11d
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L120:
	cmpl	$31, %esi
	jg	.L122
	testb	%r11b, %r11b
	je	.L122
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$27, %eax
	leal	(%rsi,%rax), %ecx
	andl	$31, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L130:
	movslq	11(%rdi), %r13
	leal	1(%r12), %ecx
	testl	%r13d, %r13d
	leal	3(%r13), %eax
	cmovns	%r13d, %eax
	sarl	$2, %eax
	cmpl	%eax, %ecx
	jge	.L132
	subl	%r12d, %eax
	sall	$2, %ecx
	movslq	%r12d, %r12
	movslq	%ecx, %rcx
	subl	$2, %eax
	testl	%ebx, %ebx
	jne	.L133
	leaq	2(%rax,%r12), %r12
	salq	$2, %r12
	.p2align 4,,10
	.p2align 3
.L136:
	movl	15(%rcx,%rdi), %eax
	testb	$1, %al
	jne	.L140
	testl	%eax, %eax
	je	.L176
.L135:
	rep bsfl	%eax, %eax
	addl	%eax, %r9d
.L137:
	testl	%ebx, %ebx
	jne	.L172
.L140:
	addl	%r9d, %esi
	testb	$1, %dil
	jne	.L147
.L141:
	cmpl	%r11d, %esi
	jne	.L172
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L176:
	addl	$32, %r9d
	cmpl	%r9d, %edx
	jle	.L140
	addq	$4, %rcx
	cmpq	%rcx, %r12
	jne	.L136
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L133:
	leaq	2(%rax,%r12), %r13
	salq	$2, %r13
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L138:
	addl	$32, %r9d
	cmpl	%r9d, %edx
	jle	.L137
	addq	$4, %rcx
	cmpq	%r13, %rcx
	je	.L137
.L139:
	movl	15(%rdi,%rcx), %eax
	notl	%eax
	movl	%eax, %r12d
	andl	$1, %r12d
	cmpb	%r12b, %r10b
	jne	.L137
	testl	%eax, %eax
	jne	.L135
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L122:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L132:
	addl	%r9d, %esi
	testl	%ebx, %ebx
	jne	.L172
	.p2align 4,,10
	.p2align 3
.L147:
	movslq	11(%rdi), %rax
	leal	0(,%rax,8), %r11d
	jmp	.L141
	.cfi_endproc
.LFE17786:
	.size	_ZN2v88internal16LayoutDescriptor8IsTaggedEiiPi, .-_ZN2v88internal16LayoutDescriptor8IsTaggedEiiPi
	.section	.text._ZN2v88internal16LayoutDescriptor13NewForTestingEPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LayoutDescriptor13NewForTestingEPNS0_7IsolateEi
	.type	_ZN2v88internal16LayoutDescriptor13NewForTestingEPNS0_7IsolateEi, @function
_ZN2v88internal16LayoutDescriptor13NewForTestingEPNS0_7IsolateEi:
.LFB17787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	cmpl	$32, %esi
	jg	.L178
	movq	41112(%rdi), %r8
	testq	%r8, %r8
	je	.L179
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	movq	%rax, %r12
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	addl	$63, %esi
	movl	$1, %edx
	andl	$-64, %esi
	leal	7(%rsi), %eax
	cmovs	%eax, %esi
	sarl	$3, %esi
	call	_ZN2v88internal7Factory12NewByteArrayEiNS0_14AllocationTypeE@PLT
	xorl	%esi, %esi
	movq	(%rax), %rdi
	movq	%rax, %r12
	movslq	11(%rdi), %rdx
	addq	$15, %rdi
	addl	$7, %edx
	andl	$-8, %edx
	movslq	%edx, %rdx
	call	memset@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	41088(%rdi), %r12
	cmpq	41096(%rdi), %r12
	je	.L183
.L181:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rdi)
	movq	%r12, %rax
	movq	$0, (%r12)
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rdi
	movq	%rax, %r12
	jmp	.L181
	.cfi_endproc
.LFE17787:
	.size	_ZN2v88internal16LayoutDescriptor13NewForTestingEPNS0_7IsolateEi, .-_ZN2v88internal16LayoutDescriptor13NewForTestingEPNS0_7IsolateEi
	.section	.text._ZN2v88internal16LayoutDescriptor19SetTaggedForTestingEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LayoutDescriptor19SetTaggedForTestingEib
	.type	_ZN2v88internal16LayoutDescriptor19SetTaggedForTestingEib, @function
_ZN2v88internal16LayoutDescriptor19SetTaggedForTestingEib:
.LFB17788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %r8
	movl	$32, %ecx
	movq	%r8, %rax
	notq	%rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%eax, %r10d
	andl	$1, %r10d
	jne	.L185
	movslq	11(%r8), %rcx
	sall	$3, %ecx
.L185:
	cmpl	%ecx, %esi
	jnb	.L186
	testl	%esi, %esi
	leal	31(%rsi), %r9d
	cmovns	%esi, %r9d
	sarl	$5, %r9d
	testb	$1, %al
	jne	.L187
	cmpl	%r9d, 11(%r8)
	jle	.L187
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$27, %eax
	leal	(%rsi,%rax), %ecx
	andl	$31, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	testb	%r10b, %r10b
	je	.L204
.L191:
	movl	%eax, %ecx
	sarq	$32, %r8
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	notl	%ecx
	orl	%r8d, %eax
	andl	%r8d, %ecx
	testb	%dl, %dl
	cmovne	%ecx, %eax
	salq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	sall	$2, %r9d
	movl	%eax, %esi
	movslq	%r9d, %r9
	notl	%esi
	movl	15(%r8,%r9), %ecx
	andl	%ecx, %esi
	orl	%eax, %ecx
	testb	%dl, %dl
	movl	%esi, %edx
	cmove	%ecx, %edx
	movl	%edx, 15(%r9,%r8)
	movq	(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	testb	%r10b, %r10b
	je	.L189
	cmpl	$31, %esi
	jg	.L189
	movl	%esi, %eax
	movl	$1, %edi
	sarl	$31, %eax
	shrl	$27, %eax
	leal	(%rsi,%rax), %ecx
	andl	$31, %ecx
	subl	%eax, %ecx
	movl	%edi, %eax
	sall	%cl, %eax
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17788:
	.size	_ZN2v88internal16LayoutDescriptor19SetTaggedForTestingEib, .-_ZN2v88internal16LayoutDescriptor19SetTaggedForTestingEib
	.section	.text._ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi
	.type	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi, @function
_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi:
.LFB17789:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %r8d
	movq	%rcx, %rax
	testb	%r8b, %r8b
	je	.L206
	movl	%edx, (%rcx)
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	subl	%esi, %edx
	movq	8(%rdi), %r9
	movl	4(%rdi), %r10d
	leal	7(%rdx), %ecx
	cmovs	%ecx, %edx
	sarl	$3, %edx
	movl	%edx, %ebx
	testq	%r9, %r9
	je	.L212
	movq	%r9, %r11
	movl	$32, %r13d
	notq	%r11
	movl	%r11d, %r12d
	andl	$1, %r12d
	je	.L268
.L211:
	movl	%esi, %edi
	movl	$0, %r14d
	subl	%r10d, %edi
	leal	7(%rdi), %ecx
	cmovns	%edi, %ecx
	sarl	$3, %ecx
	cmovs	%r14d, %ecx
	cmpl	%ecx, %r13d
	jbe	.L212
	movl	%ecx, %r13d
	sarl	$5, %r13d
	andl	$1, %r11d
	jne	.L213
	cmpl	%r13d, 11(%r9)
	jle	.L213
	movl	%ecx, %edi
	andl	$31, %edi
	movl	%edi, -56(%rbp)
	movl	$1, %edi
	sall	%cl, %edi
	testb	%r12b, %r12b
	je	.L269
.L217:
	movq	%r9, %r15
	shrq	$32, %r15
	movq	%r15, %r11
.L219:
	movl	%r11d, %r14d
	movl	%r11d, %r15d
	andl	%edi, %r14d
	notl	%r15d
	cmovne	%r15d, %r11d
	sete	-57(%rbp)
	negl	%edi
	andl	%r11d, %edi
	testb	%r12b, %r12b
	jne	.L221
	xorl	%r15d, %r15d
	rep bsfl	%edi, %r15d
	testl	%edi, %edi
	movl	$32, %edi
	cmove	%edi, %r15d
	movl	%r15d, %edi
	subl	-56(%rbp), %edi
	cmpl	$32, %r15d
	je	.L270
	cmpl	%edi, %edx
	movl	%edi, %ebx
	cmovle	%edx, %ebx
	cmpl	%esi, %r10d
	jle	.L245
	testl	%r14d, %r14d
	je	.L209
.L237:
	movl	%r10d, (%rax)
	movl	$1, %r8d
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L212:
	movl	$1, %r8d
	cmpl	%esi, %r10d
	jg	.L209
.L210:
	leal	(%rsi,%rbx,8), %edx
	movl	%edx, (%rax)
.L205:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movslq	11(%r9), %r13
	sall	$3, %r13d
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L221:
	xorl	%r9d, %r9d
	rep bsfl	%edi, %r9d
	testl	%edi, %edi
	movl	$32, %edi
	cmovne	%r9d, %edi
	subl	-56(%rbp), %edi
	testl	%r14d, %r14d
	jne	.L232
	addl	%edi, %ecx
	cmpl	$32, %ecx
	je	.L234
.L257:
	cmpl	%edi, %edx
	movl	%edi, %ebx
	cmovle	%edx, %ebx
.L234:
	cmpl	%r10d, %esi
	jl	.L209
.L245:
	movzbl	-57(%rbp), %r8d
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L209:
	leal	(%r10,%rbx,8), %edx
	movl	$1, %r8d
	movl	%edx, (%rax)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L269:
	leal	0(,%r13,4), %r11d
	movq	%r9, %r12
	movslq	%r11d, %r11
	notq	%r12
	movl	15(%r9,%r11), %r11d
	andl	$1, %r12d
	jmp	.L219
.L224:
	addl	%edi, %ecx
	testl	%r14d, %r14d
	je	.L239
	.p2align 4,,10
	.p2align 3
.L232:
	cmpl	%r10d, %esi
	jl	.L237
	cmpl	%edi, %edx
	movl	%edi, %ebx
	cmovle	%edx, %ebx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L213:
	cmpl	$255, %edi
	jg	.L215
	testb	%r12b, %r12b
	je	.L215
	movl	%ecx, %edi
	andl	$31, %edi
	movl	%edi, -56(%rbp)
	movl	$1, %edi
	sall	%cl, %edi
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L270:
	leal	1(%r13), %r12d
	movl	%r12d, -56(%rbp)
	movslq	11(%r9), %r12
	testl	%r12d, %r12d
	leal	3(%r12), %r11d
	cmovns	%r12d, %r11d
	leal	1(%r13), %r12d
	sarl	$2, %r11d
	cmpl	%r11d, %r12d
	jge	.L224
	testl	%r14d, %r14d
	jne	.L225
	subl	%r13d, %r11d
	movslq	%r13d, %r13
	sall	$2, %r12d
	subl	$2, %r11d
	movslq	%r12d, %r12
	leaq	2(%r11,%r13), %r13
	salq	$2, %r13
	.p2align 4,,10
	.p2align 3
.L228:
	movl	15(%r9,%r12), %r11d
	testb	$1, %r11b
	jne	.L241
	testl	%r11d, %r11d
	je	.L271
.L227:
	rep bsfl	%r11d, %r11d
	addl	%r11d, %edi
.L229:
	testl	%r14d, %r14d
	jne	.L232
.L241:
	addl	%edi, %ecx
	testb	$1, %r9b
	jne	.L239
.L233:
	cmpl	%ecx, %r15d
	jne	.L257
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L271:
	addl	$32, %edi
	cmpl	%edi, %edx
	jle	.L241
	addq	$4, %r12
	cmpq	%r12, %r13
	jne	.L228
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L225:
	subl	%r13d, %r11d
	leal	1(%r13), %r12d
	movslq	%r13d, %r13
	subl	$2, %r11d
	sall	$2, %r12d
	leaq	2(%r11,%r13), %r11
	movslq	%r12d, %r12
	salq	$2, %r11
	movq	%r11, -56(%rbp)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L230:
	addl	$32, %edi
	cmpl	%edi, %edx
	jle	.L229
	addq	$4, %r12
	cmpq	%r12, -56(%rbp)
	je	.L229
.L231:
	movl	15(%r9,%r12), %r11d
	notl	%r11d
	movl	%r11d, %r13d
	andl	$1, %r13d
	cmpb	%r13b, -57(%rbp)
	jne	.L229
	testl	%r11d, %r11d
	jne	.L227
	jmp	.L230
.L215:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L239:
	movslq	11(%r9), %r15
	sall	$3, %r15d
	jmp	.L233
	.cfi_endproc
.LFE17789:
	.size	_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi, .-_ZN2v88internal22LayoutDescriptorHelper8IsTaggedEiiPi
	.section	.text._ZN2v88internal16LayoutDescriptor4TrimEPNS0_4HeapENS0_3MapENS0_15DescriptorArrayEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LayoutDescriptor4TrimEPNS0_4HeapENS0_3MapENS0_15DescriptorArrayEi
	.type	_ZN2v88internal16LayoutDescriptor4TrimEPNS0_4HeapENS0_3MapENS0_15DescriptorArrayEi, @function
_ZN2v88internal16LayoutDescriptor4TrimEPNS0_4HeapENS0_3MapENS0_15DescriptorArrayEi:
.LFB17790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	testb	$1, %al
	jne	.L305
.L301:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movq	%rcx, %r13
	movzbl	7(%rdx), %ecx
	movzbl	8(%rdx), %eax
	movq	%rsi, %rdi
	movq	%rdx, %r12
	movl	%r8d, %r14d
	subl	%eax, %ecx
	je	.L275
	movl	$32, %esi
	cmpl	$32, %r8d
	jle	.L276
	leal	-1(%r8), %eax
	leaq	31(%r13), %rdx
	xorl	%esi, %esi
	leaq	(%rax,%rax,2), %rax
	leaq	55(%r13,%rax,8), %r8
	.p2align 4,,10
	.p2align 3
.L277:
	movq	(%rdx), %rax
	sarq	$32, %rax
	testb	$2, %al
	jne	.L280
	movl	%eax, %r9d
	shrl	$6, %r9d
	andl	$7, %r9d
	cmpl	$2, %r9d
	jne	.L280
	shrl	$19, %eax
	andl	$1023, %eax
	cmpl	%ecx, %eax
	jge	.L280
	addl	$1, %eax
	cmpl	%eax, %esi
	cmovl	%eax, %esi
	.p2align 4,,10
	.p2align 3
.L280:
	addq	$24, %rdx
	cmpq	%rdx, %r8
	jne	.L277
.L276:
	cmpl	%ecx, %esi
	cmovle	%esi, %ecx
	addl	$63, %ecx
	andl	$-64, %ecx
	leal	7(%rcx), %eax
	cmovs	%eax, %ecx
	sarl	$3, %ecx
.L275:
	movq	(%rbx), %rsi
	movslq	11(%rsi), %rdx
	cmpl	%ecx, %edx
	jne	.L306
.L281:
	addl	$7, %edx
	leaq	15(%rsi), %rdi
	xorl	%esi, %esi
	andl	$-8, %edx
	movslq	%edx, %rdx
	call	memset@PLT
	movq	(%rbx), %rax
	movzbl	7(%r12), %r8d
	movzbl	8(%r12), %edx
	subl	%edx, %r8d
	testl	%r14d, %r14d
	jle	.L301
	leal	-1(%r14), %ecx
	leaq	31(%r13), %rdx
	movl	$1, %r9d
	leaq	(%rcx,%rcx,2), %rcx
	leaq	55(%r13,%rcx,8), %rdi
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L308:
	movl	%ecx, %r10d
	sarl	$5, %r10d
	andl	$1, %esi
	jne	.L289
	cmpl	%r10d, 11(%rax)
	jle	.L289
	movl	%r9d, %ebx
	sall	%cl, %ebx
	movl	%ebx, %ecx
	testb	%r11b, %r11b
	je	.L307
.L293:
	sarq	$32, %rax
	orl	%ecx, %eax
	salq	$32, %rax
	.p2align 4,,10
	.p2align 3
.L286:
	addq	$24, %rdx
	cmpq	%rdi, %rdx
	je	.L301
.L283:
	movq	(%rdx), %rcx
	sarq	$32, %rcx
	testb	$2, %cl
	jne	.L286
	movl	%ecx, %esi
	shrl	$6, %esi
	andl	$7, %esi
	cmpl	$2, %esi
	jne	.L286
	shrl	$19, %ecx
	andl	$1023, %ecx
	cmpl	%r8d, %ecx
	jge	.L286
	movq	%rax, %rsi
	movl	$32, %r10d
	notq	%rsi
	movl	%esi, %r11d
	andl	$1, %r11d
	jne	.L287
	movslq	11(%rax), %r10
	sall	$3, %r10d
.L287:
	cmpl	%r10d, %ecx
	jb	.L308
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L307:
	sall	$2, %r10d
	movslq	%r10d, %r10
	leaq	15(%rax,%r10), %rsi
	movl	(%rsi), %r10d
	orl	%r10d, %ecx
	movl	%ecx, (%rsi)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L306:
	subl	%ecx, %edx
	call	_ZN2v88internal4Heap19RightTrimFixedArrayENS0_14FixedArrayBaseEi@PLT
	movq	(%rbx), %rsi
	movslq	11(%rsi), %rdx
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L289:
	cmpl	$31, %ecx
	jg	.L291
	testb	%r11b, %r11b
	je	.L291
	movl	%r9d, %ebx
	sall	%cl, %ebx
	movl	%ebx, %ecx
	jmp	.L293
.L291:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17790:
	.size	_ZN2v88internal16LayoutDescriptor4TrimEPNS0_4HeapENS0_3MapENS0_15DescriptorArrayEi, .-_ZN2v88internal16LayoutDescriptor4TrimEPNS0_4HeapENS0_3MapENS0_15DescriptorArrayEi
	.section	.rodata._ZN2v88internal16LayoutDescriptor19IsConsistentWithMapENS0_3MapEb.str1.1,"aMS",@progbits,1
.LC3:
	.string	"unreachable code"
	.section	.text._ZN2v88internal16LayoutDescriptor19IsConsistentWithMapENS0_3MapEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16LayoutDescriptor19IsConsistentWithMapENS0_3MapEb
	.type	_ZN2v88internal16LayoutDescriptor19IsConsistentWithMapENS0_3MapEb, @function
_ZN2v88internal16LayoutDescriptor19IsConsistentWithMapENS0_3MapEb:
.LFB17791:
	.cfi_startproc
	endbr64
	movq	39(%rsi), %rdx
	movl	15(%rsi), %eax
	shrl	$10, %eax
	andl	$1023, %eax
	je	.L351
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$1, %eax
	leaq	39(%rsi), %r10
	leaq	(%rax,%rax,2), %rax
	leaq	55(,%rax,8), %r8
	movl	$31, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L312:
	addq	$24, %rax
	cmpq	%r8, %rax
	je	.L310
.L311:
	movq	(%rdx,%rax), %rcx
	sarq	$32, %rcx
	testb	$2, %cl
	jne	.L312
	movq	(%r10), %r9
	movq	(%r9,%rax), %r9
	movzbl	7(%rsi), %r11d
	movzbl	8(%rsi), %r12d
	movq	%r9, %rbx
	shrq	$38, %r9
	shrq	$51, %rbx
	subl	%r12d, %r11d
	andl	$7, %r9d
	andl	$1023, %ebx
	cmpl	%r11d, %ebx
	setl	%r12b
	jl	.L354
	subl	%r11d, %ebx
	leal	16(,%rbx,8), %ebx
.L314:
	cmpl	$2, %r9d
	je	.L315
	cmpb	$2, %r9b
	jg	.L316
	je	.L317
.L315:
	movzbl	%r12b, %r11d
	andl	$16384, %ebx
	movq	(%rdi), %r9
	salq	$14, %r11
	orq	%r11, %rbx
	je	.L318
	movl	%ecx, %r11d
	shrl	$6, %r11d
	andl	$7, %r11d
	cmpl	$2, %r11d
	je	.L355
.L318:
	movl	$1, %ebx
	testq	%r9, %r9
	je	.L312
.L329:
	movq	%r9, %r11
	movl	$32, %r12d
	notq	%r11
	movl	%r11d, %r14d
	andl	$1, %r14d
	je	.L356
.L320:
	shrl	$19, %ecx
	movl	%ecx, %r15d
	andl	$1023, %r15d
	cmpl	%r15d, %r12d
	jbe	.L334
	movl	%r15d, %r12d
	sarl	$5, %r12d
	andl	$1, %r11d
	jne	.L322
	cmpl	%r12d, 11(%r9)
	jle	.L322
	movl	%r13d, %r15d
	sall	%cl, %r15d
	movl	%r15d, %ecx
	testb	%r14b, %r14b
	je	.L357
.L326:
	sarq	$32, %r9
	testl	%r9d, %ecx
	sete	%cl
.L321:
	cmpb	%bl, %cl
	je	.L312
.L330:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	subl	$3, %r9d
	cmpb	$1, %r9b
	jbe	.L315
.L317:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L354:
	movzbl	8(%rsi), %r11d
	movzbl	8(%rsi), %r11d
	addl	%r11d, %ebx
	sall	$3, %ebx
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L356:
	movslq	11(%r9), %r12
	sall	$3, %r12d
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L355:
	testq	%r9, %r9
	je	.L330
	xorl	%ebx, %ebx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L334:
	movl	$1, %ecx
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L310:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	andl	$992, %ecx
	jne	.L324
	testb	%r14b, %r14b
	je	.L324
	movl	%r15d, %ecx
	movl	%r13d, %r14d
	sall	%cl, %r14d
	movl	%r14d, %ecx
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L357:
	sall	$2, %r12d
	movslq	%r12d, %r12
	movl	15(%r9,%r12), %r9d
	testl	%r9d, %r15d
	sete	%cl
	jmp	.L321
.L324:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L351:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE17791:
	.size	_ZN2v88internal16LayoutDescriptor19IsConsistentWithMapENS0_3MapEb, .-_ZN2v88internal16LayoutDescriptor19IsConsistentWithMapENS0_3MapEb
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi, @function
_GLOBAL__sub_I__ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi:
.LFB21450:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21450:
	.size	_GLOBAL__sub_I__ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi, .-_GLOBAL__sub_I__ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
