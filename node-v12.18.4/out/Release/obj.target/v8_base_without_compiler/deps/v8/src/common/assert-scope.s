	.file	"assert-scope.cc"
	.text
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EEC2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EEC2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EEC2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EEC2Ev:
.LFB9261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	$0, (%rdi)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L17
.L3:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L18
.L5:
	movzbl	(%rbx), %eax
	addl	$1, 8(%rbx)
	movb	$0, (%rbx)
	orq	%rbx, %rax
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L3
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$12, %edi
	call	_Znwm@PLT
	movl	$0, 8(%rax)
	movq	%rax, %rbx
	movl	$16843009, (%rax)
	movb	$1, 4(%rax)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L19
.L7:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	movq	%rbx, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L7
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L7
	.cfi_endproc
.LFE9261:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EEC2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EEC2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EEC1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EEC1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EEC2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9IsAllowedEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9IsAllowedEv,comdat
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9IsAllowedEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9IsAllowedEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9IsAllowedEv:
.LFB9266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%al, %al
	je	.L31
.L22:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L20
	movzbl	(%rax), %r8d
.L20:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L22
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L22
	.cfi_endproc
.LFE9266:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9IsAllowedEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9IsAllowedEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE7ReleaseEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE7ReleaseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE7ReleaseEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE7ReleaseEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE7ReleaseEv:
.LFB9267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, %r12
	andq	$-2, %r12
	movb	%al, (%r12)
	andb	$1, (%r12)
	subl	$1, 8(%r12)
	je	.L46
.L34:
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L47
.L36:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	xorl	%esi, %esi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	testq	%r12, %r12
	je	.L34
	movq	%r12, %rdi
	movl	$12, %esi
	call	_ZdlPvm@PLT
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L36
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L36
	.cfi_endproc
.LFE9267:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE7ReleaseEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE7ReleaseEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EED2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EED2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EED2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EED2Ev:
.LFB9264:
	.cfi_startproc
	endbr64
	testq	$-2, (%rdi)
	jne	.L50
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	jmp	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE7ReleaseEv
	.cfi_endproc
.LFE9264:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EED2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EED2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EED1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EED1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EED2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE8set_dataEPNS0_19PerThreadAssertDataE,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE8set_dataEPNS0_19PerThreadAssertDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE8set_dataEPNS0_19PerThreadAssertDataE
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE8set_dataEPNS0_19PerThreadAssertDataE, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE8set_dataEPNS0_19PerThreadAssertDataE:
.LFB9268:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9268:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE8set_dataEPNS0_19PerThreadAssertDataE, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE8set_dataEPNS0_19PerThreadAssertDataE
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE4dataEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE4dataEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE4dataEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE4dataEv:
.LFB9269:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andq	$-2, %rax
	ret
	.cfi_endproc
.LFE9269:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE4dataEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE4dataEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE13set_old_stateEb,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE13set_old_stateEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE13set_old_stateEb
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE13set_old_stateEb, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE13set_old_stateEb:
.LFB9270:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movq	(%rdi), %rsi
	movzbl	%r8b, %eax
	andq	$-2, %rsi
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9270:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE13set_old_stateEb, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE13set_old_stateEb
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9old_stateEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9old_stateEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9old_stateEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9old_stateEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9old_stateEv:
.LFB9271:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9271:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9old_stateEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb0EE9old_stateEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EEC2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EEC2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EEC2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EEC2Ev:
.LFB9273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	$0, (%rdi)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L70
.L57:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L71
.L59:
	movzbl	(%rbx), %eax
	addl	$1, 8(%rbx)
	movb	$1, (%rbx)
	orq	%rbx, %rax
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L57
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$12, %edi
	call	_Znwm@PLT
	movl	$0, 8(%rax)
	movq	%rax, %rbx
	movl	$16843009, (%rax)
	movb	$1, 4(%rax)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L72
.L61:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	movq	%rbx, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L72:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L61
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L61
	.cfi_endproc
.LFE9273:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EEC2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EEC2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EEC1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EEC1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EEC2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9IsAllowedEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9IsAllowedEv,comdat
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9IsAllowedEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9IsAllowedEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9IsAllowedEv:
.LFB9278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%al, %al
	je	.L84
.L75:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L73
	movzbl	(%rax), %r8d
.L73:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L75
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L75
	.cfi_endproc
.LFE9278:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9IsAllowedEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9IsAllowedEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE7ReleaseEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE7ReleaseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE7ReleaseEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE7ReleaseEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE7ReleaseEv:
.LFB9279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, %r12
	andq	$-2, %r12
	movb	%al, (%r12)
	andb	$1, (%r12)
	subl	$1, 8(%r12)
	je	.L99
.L87:
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L100
.L89:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	xorl	%esi, %esi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	testq	%r12, %r12
	je	.L87
	movq	%r12, %rdi
	movl	$12, %esi
	call	_ZdlPvm@PLT
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L89
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L89
	.cfi_endproc
.LFE9279:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE7ReleaseEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE7ReleaseEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EED2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EED2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EED2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EED2Ev:
.LFB9276:
	.cfi_startproc
	endbr64
	testq	$-2, (%rdi)
	jne	.L103
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	jmp	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE7ReleaseEv
	.cfi_endproc
.LFE9276:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EED2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EED2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EED1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EED1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EED2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE8set_dataEPNS0_19PerThreadAssertDataE,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE8set_dataEPNS0_19PerThreadAssertDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE8set_dataEPNS0_19PerThreadAssertDataE
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE8set_dataEPNS0_19PerThreadAssertDataE, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE8set_dataEPNS0_19PerThreadAssertDataE:
.LFB9280:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9280:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE8set_dataEPNS0_19PerThreadAssertDataE, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE8set_dataEPNS0_19PerThreadAssertDataE
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE4dataEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE4dataEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE4dataEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE4dataEv:
.LFB9281:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andq	$-2, %rax
	ret
	.cfi_endproc
.LFE9281:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE4dataEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE4dataEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE13set_old_stateEb,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE13set_old_stateEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE13set_old_stateEb
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE13set_old_stateEb, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE13set_old_stateEb:
.LFB9282:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movq	(%rdi), %rsi
	movzbl	%r8b, %eax
	andq	$-2, %rsi
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9282:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE13set_old_stateEb, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE13set_old_stateEb
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9old_stateEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9old_stateEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9old_stateEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9old_stateEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9old_stateEv:
.LFB9283:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9283:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9old_stateEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE0ELb1EE9old_stateEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EEC2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EEC2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EEC2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EEC2Ev:
.LFB9285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	$0, (%rdi)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L123
.L110:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L124
.L112:
	movzbl	1(%rbx), %eax
	addl	$1, 8(%rbx)
	movb	$0, 1(%rbx)
	orq	%rbx, %rax
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L110
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L124:
	movl	$12, %edi
	call	_Znwm@PLT
	movl	$0, 8(%rax)
	movq	%rax, %rbx
	movl	$16843009, (%rax)
	movb	$1, 4(%rax)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L125
.L114:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	movq	%rbx, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L125:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L114
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L114
	.cfi_endproc
.LFE9285:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EEC2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EEC2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EEC1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EEC1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EEC2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9IsAllowedEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9IsAllowedEv,comdat
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9IsAllowedEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9IsAllowedEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9IsAllowedEv:
.LFB9290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%al, %al
	je	.L137
.L128:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L126
	movzbl	1(%rax), %r8d
.L126:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L128
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L128
	.cfi_endproc
.LFE9290:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9IsAllowedEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9IsAllowedEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE7ReleaseEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE7ReleaseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE7ReleaseEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE7ReleaseEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE7ReleaseEv:
.LFB9291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, %r12
	andq	$-2, %r12
	movb	%al, 1(%r12)
	andb	$1, 1(%r12)
	subl	$1, 8(%r12)
	je	.L152
.L140:
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L153
.L142:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	xorl	%esi, %esi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	testq	%r12, %r12
	je	.L140
	movq	%r12, %rdi
	movl	$12, %esi
	call	_ZdlPvm@PLT
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L142
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L142
	.cfi_endproc
.LFE9291:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE7ReleaseEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE7ReleaseEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EED2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EED2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EED2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EED2Ev:
.LFB9288:
	.cfi_startproc
	endbr64
	testq	$-2, (%rdi)
	jne	.L156
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	jmp	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE7ReleaseEv
	.cfi_endproc
.LFE9288:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EED2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EED2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EED1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EED1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EED2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE8set_dataEPNS0_19PerThreadAssertDataE,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE8set_dataEPNS0_19PerThreadAssertDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE8set_dataEPNS0_19PerThreadAssertDataE
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE8set_dataEPNS0_19PerThreadAssertDataE, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE8set_dataEPNS0_19PerThreadAssertDataE:
.LFB9292:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9292:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE8set_dataEPNS0_19PerThreadAssertDataE, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE8set_dataEPNS0_19PerThreadAssertDataE
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE4dataEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE4dataEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE4dataEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE4dataEv:
.LFB9293:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andq	$-2, %rax
	ret
	.cfi_endproc
.LFE9293:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE4dataEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE4dataEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE13set_old_stateEb,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE13set_old_stateEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE13set_old_stateEb
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE13set_old_stateEb, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE13set_old_stateEb:
.LFB9294:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movq	(%rdi), %rsi
	movzbl	%r8b, %eax
	andq	$-2, %rsi
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9294:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE13set_old_stateEb, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE13set_old_stateEb
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9old_stateEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9old_stateEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9old_stateEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9old_stateEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9old_stateEv:
.LFB9295:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9295:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9old_stateEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb0EE9old_stateEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EEC2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EEC2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EEC2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EEC2Ev:
.LFB9297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	$0, (%rdi)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L176
.L163:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L177
.L165:
	movzbl	1(%rbx), %eax
	addl	$1, 8(%rbx)
	movb	$1, 1(%rbx)
	orq	%rbx, %rax
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L163
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$12, %edi
	call	_Znwm@PLT
	movl	$0, 8(%rax)
	movq	%rax, %rbx
	movl	$16843009, (%rax)
	movb	$1, 4(%rax)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L178
.L167:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	movq	%rbx, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L178:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L167
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L167
	.cfi_endproc
.LFE9297:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EEC2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EEC2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EEC1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EEC1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EEC2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9IsAllowedEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9IsAllowedEv,comdat
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9IsAllowedEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9IsAllowedEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9IsAllowedEv:
.LFB9302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%al, %al
	je	.L190
.L181:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L179
	movzbl	1(%rax), %r8d
.L179:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L181
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L181
	.cfi_endproc
.LFE9302:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9IsAllowedEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9IsAllowedEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE7ReleaseEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE7ReleaseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE7ReleaseEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE7ReleaseEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE7ReleaseEv:
.LFB9303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, %r12
	andq	$-2, %r12
	movb	%al, 1(%r12)
	andb	$1, 1(%r12)
	subl	$1, 8(%r12)
	je	.L205
.L193:
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L206
.L195:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	xorl	%esi, %esi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	testq	%r12, %r12
	je	.L193
	movq	%r12, %rdi
	movl	$12, %esi
	call	_ZdlPvm@PLT
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L195
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L195
	.cfi_endproc
.LFE9303:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE7ReleaseEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE7ReleaseEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EED2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EED2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EED2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EED2Ev:
.LFB9300:
	.cfi_startproc
	endbr64
	testq	$-2, (%rdi)
	jne	.L209
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	jmp	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE7ReleaseEv
	.cfi_endproc
.LFE9300:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EED2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EED2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EED1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EED1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EED2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE8set_dataEPNS0_19PerThreadAssertDataE,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE8set_dataEPNS0_19PerThreadAssertDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE8set_dataEPNS0_19PerThreadAssertDataE
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE8set_dataEPNS0_19PerThreadAssertDataE, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE8set_dataEPNS0_19PerThreadAssertDataE:
.LFB9304:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9304:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE8set_dataEPNS0_19PerThreadAssertDataE, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE8set_dataEPNS0_19PerThreadAssertDataE
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE4dataEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE4dataEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE4dataEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE4dataEv:
.LFB9305:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andq	$-2, %rax
	ret
	.cfi_endproc
.LFE9305:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE4dataEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE4dataEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE13set_old_stateEb,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE13set_old_stateEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE13set_old_stateEb
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE13set_old_stateEb, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE13set_old_stateEb:
.LFB9306:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movq	(%rdi), %rsi
	movzbl	%r8b, %eax
	andq	$-2, %rsi
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9306:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE13set_old_stateEb, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE13set_old_stateEb
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9old_stateEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9old_stateEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9old_stateEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9old_stateEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9old_stateEv:
.LFB9307:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9307:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9old_stateEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE1ELb1EE9old_stateEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EEC2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EEC2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EEC2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EEC2Ev:
.LFB9309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	$0, (%rdi)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L229
.L216:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L230
.L218:
	movzbl	2(%rbx), %eax
	addl	$1, 8(%rbx)
	movb	$0, 2(%rbx)
	orq	%rbx, %rax
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L216
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L230:
	movl	$12, %edi
	call	_Znwm@PLT
	movl	$0, 8(%rax)
	movq	%rax, %rbx
	movl	$16843009, (%rax)
	movb	$1, 4(%rax)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L231
.L220:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	movq	%rbx, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L231:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L220
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L220
	.cfi_endproc
.LFE9309:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EEC2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EEC2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EEC1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EEC1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EEC2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9IsAllowedEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9IsAllowedEv,comdat
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9IsAllowedEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9IsAllowedEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9IsAllowedEv:
.LFB9314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%al, %al
	je	.L243
.L234:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L232
	movzbl	2(%rax), %r8d
.L232:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L234
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L234
	.cfi_endproc
.LFE9314:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9IsAllowedEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9IsAllowedEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE7ReleaseEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE7ReleaseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE7ReleaseEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE7ReleaseEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE7ReleaseEv:
.LFB9315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, %r12
	andq	$-2, %r12
	movb	%al, 2(%r12)
	andb	$1, 2(%r12)
	subl	$1, 8(%r12)
	je	.L258
.L246:
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L259
.L248:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	xorl	%esi, %esi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	testq	%r12, %r12
	je	.L246
	movq	%r12, %rdi
	movl	$12, %esi
	call	_ZdlPvm@PLT
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L248
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L248
	.cfi_endproc
.LFE9315:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE7ReleaseEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE7ReleaseEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EED2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EED2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EED2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EED2Ev:
.LFB9312:
	.cfi_startproc
	endbr64
	testq	$-2, (%rdi)
	jne	.L262
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	jmp	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE7ReleaseEv
	.cfi_endproc
.LFE9312:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EED2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EED2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EED1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EED1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EED2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE8set_dataEPNS0_19PerThreadAssertDataE,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE8set_dataEPNS0_19PerThreadAssertDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE8set_dataEPNS0_19PerThreadAssertDataE
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE8set_dataEPNS0_19PerThreadAssertDataE, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE8set_dataEPNS0_19PerThreadAssertDataE:
.LFB9316:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9316:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE8set_dataEPNS0_19PerThreadAssertDataE, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE8set_dataEPNS0_19PerThreadAssertDataE
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE4dataEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE4dataEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE4dataEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE4dataEv:
.LFB9317:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andq	$-2, %rax
	ret
	.cfi_endproc
.LFE9317:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE4dataEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE4dataEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE13set_old_stateEb,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE13set_old_stateEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE13set_old_stateEb
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE13set_old_stateEb, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE13set_old_stateEb:
.LFB9318:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movq	(%rdi), %rsi
	movzbl	%r8b, %eax
	andq	$-2, %rsi
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9318:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE13set_old_stateEb, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE13set_old_stateEb
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9old_stateEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9old_stateEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9old_stateEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9old_stateEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9old_stateEv:
.LFB9319:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9319:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9old_stateEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb0EE9old_stateEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EEC2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EEC2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EEC2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EEC2Ev:
.LFB9321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	$0, (%rdi)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L282
.L269:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L283
.L271:
	movzbl	2(%rbx), %eax
	addl	$1, 8(%rbx)
	movb	$1, 2(%rbx)
	orq	%rbx, %rax
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L269
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L283:
	movl	$12, %edi
	call	_Znwm@PLT
	movl	$0, 8(%rax)
	movq	%rax, %rbx
	movl	$16843009, (%rax)
	movb	$1, 4(%rax)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L284
.L273:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	movq	%rbx, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L284:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L273
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L273
	.cfi_endproc
.LFE9321:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EEC2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EEC2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EEC1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EEC1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EEC2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9IsAllowedEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9IsAllowedEv,comdat
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9IsAllowedEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9IsAllowedEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9IsAllowedEv:
.LFB9326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%al, %al
	je	.L296
.L287:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L285
	movzbl	2(%rax), %r8d
.L285:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L287
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L287
	.cfi_endproc
.LFE9326:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9IsAllowedEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9IsAllowedEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE7ReleaseEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE7ReleaseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE7ReleaseEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE7ReleaseEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE7ReleaseEv:
.LFB9327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, %r12
	andq	$-2, %r12
	movb	%al, 2(%r12)
	andb	$1, 2(%r12)
	subl	$1, 8(%r12)
	je	.L311
.L299:
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L312
.L301:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	xorl	%esi, %esi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	testq	%r12, %r12
	je	.L299
	movq	%r12, %rdi
	movl	$12, %esi
	call	_ZdlPvm@PLT
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L301
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L301
	.cfi_endproc
.LFE9327:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE7ReleaseEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE7ReleaseEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EED2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EED2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EED2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EED2Ev:
.LFB9324:
	.cfi_startproc
	endbr64
	testq	$-2, (%rdi)
	jne	.L315
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	jmp	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE7ReleaseEv
	.cfi_endproc
.LFE9324:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EED2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EED2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EED1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EED1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EED2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE8set_dataEPNS0_19PerThreadAssertDataE,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE8set_dataEPNS0_19PerThreadAssertDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE8set_dataEPNS0_19PerThreadAssertDataE
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE8set_dataEPNS0_19PerThreadAssertDataE, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE8set_dataEPNS0_19PerThreadAssertDataE:
.LFB9328:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9328:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE8set_dataEPNS0_19PerThreadAssertDataE, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE8set_dataEPNS0_19PerThreadAssertDataE
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE4dataEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE4dataEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE4dataEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE4dataEv:
.LFB9329:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andq	$-2, %rax
	ret
	.cfi_endproc
.LFE9329:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE4dataEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE4dataEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE13set_old_stateEb,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE13set_old_stateEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE13set_old_stateEb
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE13set_old_stateEb, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE13set_old_stateEb:
.LFB9330:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movq	(%rdi), %rsi
	movzbl	%r8b, %eax
	andq	$-2, %rsi
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9330:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE13set_old_stateEb, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE13set_old_stateEb
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9old_stateEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9old_stateEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9old_stateEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9old_stateEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9old_stateEv:
.LFB9331:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9331:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9old_stateEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE2ELb1EE9old_stateEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EEC2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EEC2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EEC2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EEC2Ev:
.LFB9333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	$0, (%rdi)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L335
.L322:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L336
.L324:
	movzbl	3(%rbx), %eax
	addl	$1, 8(%rbx)
	movb	$0, 3(%rbx)
	orq	%rbx, %rax
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L322
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L336:
	movl	$12, %edi
	call	_Znwm@PLT
	movl	$0, 8(%rax)
	movq	%rax, %rbx
	movl	$16843009, (%rax)
	movb	$1, 4(%rax)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L337
.L326:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	movq	%rbx, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L337:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L326
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L326
	.cfi_endproc
.LFE9333:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EEC2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EEC2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EEC1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EEC1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EEC2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9IsAllowedEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9IsAllowedEv,comdat
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9IsAllowedEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9IsAllowedEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9IsAllowedEv:
.LFB9338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%al, %al
	je	.L349
.L340:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L338
	movzbl	3(%rax), %r8d
.L338:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L340
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L340
	.cfi_endproc
.LFE9338:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9IsAllowedEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9IsAllowedEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE7ReleaseEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE7ReleaseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE7ReleaseEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE7ReleaseEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE7ReleaseEv:
.LFB9339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, %r12
	andq	$-2, %r12
	movb	%al, 3(%r12)
	andb	$1, 3(%r12)
	subl	$1, 8(%r12)
	je	.L364
.L352:
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L365
.L354:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	xorl	%esi, %esi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	testq	%r12, %r12
	je	.L352
	movq	%r12, %rdi
	movl	$12, %esi
	call	_ZdlPvm@PLT
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L354
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L354
	.cfi_endproc
.LFE9339:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE7ReleaseEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE7ReleaseEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EED2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EED2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EED2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EED2Ev:
.LFB9336:
	.cfi_startproc
	endbr64
	testq	$-2, (%rdi)
	jne	.L368
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	jmp	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE7ReleaseEv
	.cfi_endproc
.LFE9336:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EED2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EED2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EED1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EED1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EED2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE8set_dataEPNS0_19PerThreadAssertDataE,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE8set_dataEPNS0_19PerThreadAssertDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE8set_dataEPNS0_19PerThreadAssertDataE
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE8set_dataEPNS0_19PerThreadAssertDataE, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE8set_dataEPNS0_19PerThreadAssertDataE:
.LFB9340:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9340:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE8set_dataEPNS0_19PerThreadAssertDataE, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE8set_dataEPNS0_19PerThreadAssertDataE
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE4dataEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE4dataEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE4dataEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE4dataEv:
.LFB9341:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andq	$-2, %rax
	ret
	.cfi_endproc
.LFE9341:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE4dataEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE4dataEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE13set_old_stateEb,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE13set_old_stateEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE13set_old_stateEb
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE13set_old_stateEb, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE13set_old_stateEb:
.LFB9342:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movq	(%rdi), %rsi
	movzbl	%r8b, %eax
	andq	$-2, %rsi
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9342:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE13set_old_stateEb, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE13set_old_stateEb
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9old_stateEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9old_stateEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9old_stateEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9old_stateEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9old_stateEv:
.LFB9343:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9343:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9old_stateEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb0EE9old_stateEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EEC2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EEC2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EEC2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EEC2Ev:
.LFB9345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	$0, (%rdi)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L388
.L375:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L389
.L377:
	movzbl	3(%rbx), %eax
	addl	$1, 8(%rbx)
	movb	$1, 3(%rbx)
	orq	%rbx, %rax
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L375
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L389:
	movl	$12, %edi
	call	_Znwm@PLT
	movl	$0, 8(%rax)
	movq	%rax, %rbx
	movl	$16843009, (%rax)
	movb	$1, 4(%rax)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L390
.L379:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	movq	%rbx, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L379
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L379
	.cfi_endproc
.LFE9345:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EEC2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EEC2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EEC1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EEC1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EEC2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9IsAllowedEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9IsAllowedEv,comdat
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9IsAllowedEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9IsAllowedEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9IsAllowedEv:
.LFB9350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%al, %al
	je	.L402
.L393:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L391
	movzbl	3(%rax), %r8d
.L391:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L393
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L393
	.cfi_endproc
.LFE9350:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9IsAllowedEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9IsAllowedEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE7ReleaseEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE7ReleaseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE7ReleaseEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE7ReleaseEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE7ReleaseEv:
.LFB9351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, %r12
	andq	$-2, %r12
	movb	%al, 3(%r12)
	andb	$1, 3(%r12)
	subl	$1, 8(%r12)
	je	.L417
.L405:
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore_state
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L418
.L407:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	xorl	%esi, %esi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	testq	%r12, %r12
	je	.L405
	movq	%r12, %rdi
	movl	$12, %esi
	call	_ZdlPvm@PLT
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L407
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L407
	.cfi_endproc
.LFE9351:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE7ReleaseEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE7ReleaseEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EED2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EED2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EED2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EED2Ev:
.LFB9348:
	.cfi_startproc
	endbr64
	testq	$-2, (%rdi)
	jne	.L421
	ret
	.p2align 4,,10
	.p2align 3
.L421:
	jmp	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE7ReleaseEv
	.cfi_endproc
.LFE9348:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EED2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EED2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EED1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EED1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EED2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE8set_dataEPNS0_19PerThreadAssertDataE,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE8set_dataEPNS0_19PerThreadAssertDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE8set_dataEPNS0_19PerThreadAssertDataE
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE8set_dataEPNS0_19PerThreadAssertDataE, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE8set_dataEPNS0_19PerThreadAssertDataE:
.LFB9352:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9352:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE8set_dataEPNS0_19PerThreadAssertDataE, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE8set_dataEPNS0_19PerThreadAssertDataE
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE4dataEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE4dataEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE4dataEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE4dataEv:
.LFB9353:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andq	$-2, %rax
	ret
	.cfi_endproc
.LFE9353:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE4dataEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE4dataEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE13set_old_stateEb,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE13set_old_stateEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE13set_old_stateEb
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE13set_old_stateEb, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE13set_old_stateEb:
.LFB9354:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movq	(%rdi), %rsi
	movzbl	%r8b, %eax
	andq	$-2, %rsi
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9354:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE13set_old_stateEb, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE13set_old_stateEb
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9old_stateEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9old_stateEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9old_stateEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9old_stateEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9old_stateEv:
.LFB9355:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9355:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9old_stateEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE3ELb1EE9old_stateEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EEC2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EEC2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EEC2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EEC2Ev:
.LFB9357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	$0, (%rdi)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L441
.L428:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L442
.L430:
	movzbl	4(%rbx), %eax
	addl	$1, 8(%rbx)
	movb	$0, 4(%rbx)
	orq	%rbx, %rax
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L428
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L442:
	movl	$12, %edi
	call	_Znwm@PLT
	movl	$0, 8(%rax)
	movq	%rax, %rbx
	movl	$16843009, (%rax)
	movb	$1, 4(%rax)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L443
.L432:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	movq	%rbx, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L443:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L432
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L432
	.cfi_endproc
.LFE9357:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EEC2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EEC2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EEC1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EEC1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EEC2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9IsAllowedEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9IsAllowedEv,comdat
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9IsAllowedEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9IsAllowedEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9IsAllowedEv:
.LFB9362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%al, %al
	je	.L455
.L446:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L444
	movzbl	4(%rax), %r8d
.L444:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L446
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L446
	.cfi_endproc
.LFE9362:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9IsAllowedEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9IsAllowedEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE7ReleaseEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE7ReleaseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE7ReleaseEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE7ReleaseEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE7ReleaseEv:
.LFB9363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, %r12
	andq	$-2, %r12
	movb	%al, 4(%r12)
	andb	$1, 4(%r12)
	subl	$1, 8(%r12)
	je	.L470
.L458:
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L471
.L460:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	xorl	%esi, %esi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	testq	%r12, %r12
	je	.L458
	movq	%r12, %rdi
	movl	$12, %esi
	call	_ZdlPvm@PLT
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L471:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L460
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L460
	.cfi_endproc
.LFE9363:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE7ReleaseEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE7ReleaseEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EED2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EED2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EED2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EED2Ev:
.LFB9360:
	.cfi_startproc
	endbr64
	testq	$-2, (%rdi)
	jne	.L474
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	jmp	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE7ReleaseEv
	.cfi_endproc
.LFE9360:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EED2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EED2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EED1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EED1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EED2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE8set_dataEPNS0_19PerThreadAssertDataE,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE8set_dataEPNS0_19PerThreadAssertDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE8set_dataEPNS0_19PerThreadAssertDataE
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE8set_dataEPNS0_19PerThreadAssertDataE, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE8set_dataEPNS0_19PerThreadAssertDataE:
.LFB9364:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9364:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE8set_dataEPNS0_19PerThreadAssertDataE, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE8set_dataEPNS0_19PerThreadAssertDataE
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE4dataEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE4dataEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE4dataEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE4dataEv:
.LFB9365:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andq	$-2, %rax
	ret
	.cfi_endproc
.LFE9365:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE4dataEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE4dataEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE13set_old_stateEb,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE13set_old_stateEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE13set_old_stateEb
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE13set_old_stateEb, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE13set_old_stateEb:
.LFB9366:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movq	(%rdi), %rsi
	movzbl	%r8b, %eax
	andq	$-2, %rsi
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9366:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE13set_old_stateEb, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE13set_old_stateEb
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9old_stateEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9old_stateEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9old_stateEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9old_stateEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9old_stateEv:
.LFB9367:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9367:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9old_stateEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb0EE9old_stateEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EEC2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EEC5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EEC2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EEC2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EEC2Ev:
.LFB9369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	$0, (%rdi)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L494
.L481:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L495
.L483:
	movzbl	4(%rbx), %eax
	addl	$1, 8(%rbx)
	movb	$1, 4(%rbx)
	orq	%rbx, %rax
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L481
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L495:
	movl	$12, %edi
	call	_Znwm@PLT
	movl	$0, 8(%rax)
	movq	%rax, %rbx
	movl	$16843009, (%rax)
	movb	$1, 4(%rax)
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L496
.L485:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	movq	%rbx, %rsi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L496:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L485
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L485
	.cfi_endproc
.LFE9369:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EEC2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EEC2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EEC1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EEC1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EEC2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9IsAllowedEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9IsAllowedEv,comdat
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9IsAllowedEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9IsAllowedEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9IsAllowedEv:
.LFB9374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	%al, %al
	je	.L508
.L499:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	call	_ZN2v84base6Thread14GetThreadLocalEi@PLT
	movl	$1, %r8d
	testq	%rax, %rax
	je	.L497
	movzbl	4(%rax), %r8d
.L497:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L499
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L499
	.cfi_endproc
.LFE9374:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9IsAllowedEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9IsAllowedEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE7ReleaseEv,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE7ReleaseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE7ReleaseEv
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE7ReleaseEv, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE7ReleaseEv:
.LFB9375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, %r12
	andq	$-2, %r12
	movb	%al, 4(%r12)
	andb	$1, 4(%r12)
	subl	$1, 8(%r12)
	je	.L523
.L511:
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %eax
	testb	%al, %al
	je	.L524
.L513:
	movl	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %edi
	xorl	%esi, %esi
	call	_ZN2v84base6Thread14SetThreadLocalEiPv@PLT
	testq	%r12, %r12
	je	.L511
	movq	%r12, %rdi
	movl	$12, %esi
	call	_ZdlPvm@PLT
	andq	$1, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L524:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L513
	call	_ZN2v84base6Thread20CreateThreadLocalKeyEv@PLT
	leaq	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip), %rdi
	movl	%eax, _ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L513
	.cfi_endproc
.LFE9375:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE7ReleaseEv, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE7ReleaseEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EED2Ev,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EED2Ev
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EED2Ev, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EED2Ev:
.LFB9372:
	.cfi_startproc
	endbr64
	testq	$-2, (%rdi)
	jne	.L527
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	jmp	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE7ReleaseEv
	.cfi_endproc
.LFE9372:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EED2Ev, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EED2Ev
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EED1Ev
	.set	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EED1Ev,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EED2Ev
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE8set_dataEPNS0_19PerThreadAssertDataE,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE8set_dataEPNS0_19PerThreadAssertDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE8set_dataEPNS0_19PerThreadAssertDataE
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE8set_dataEPNS0_19PerThreadAssertDataE, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE8set_dataEPNS0_19PerThreadAssertDataE:
.LFB9376:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9376:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE8set_dataEPNS0_19PerThreadAssertDataE, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE8set_dataEPNS0_19PerThreadAssertDataE
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE4dataEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE4dataEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE4dataEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE4dataEv:
.LFB9377:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andq	$-2, %rax
	ret
	.cfi_endproc
.LFE9377:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE4dataEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE4dataEv
	.section	.text._ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE13set_old_stateEb,"axG",@progbits,_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE13set_old_stateEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE13set_old_stateEb
	.type	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE13set_old_stateEb, @function
_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE13set_old_stateEb:
.LFB9378:
	.cfi_startproc
	endbr64
	movl	%esi, %r8d
	movq	(%rdi), %rsi
	movzbl	%r8b, %eax
	andq	$-2, %rsi
	orq	%rax, %rsi
	movq	%rsi, (%rdi)
	ret
	.cfi_endproc
.LFE9378:
	.size	_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE13set_old_stateEb, .-_ZN2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE13set_old_stateEb
	.section	.text._ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9old_stateEv,"axG",@progbits,_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9old_stateEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9old_stateEv
	.type	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9old_stateEv, @function
_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9old_stateEv:
.LFB9379:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9379:
	.size	_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9old_stateEv, .-_ZNK2v88internal20PerThreadAssertScopeILNS0_19PerThreadAssertTypeE4ELb1EE9old_stateEv
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC2EPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC5EPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC2EPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC2EPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC2EPNS0_7IsolateE:
.LFB9381:
	.cfi_startproc
	endbr64
	movl	41776(%rsi), %eax
	movq	%rsi, (%rdi)
	movl	%eax, 8(%rdi)
	andl	$-2, %eax
	movl	%eax, 41776(%rsi)
	ret
	.cfi_endproc
.LFE9381:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC2EPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC2EPNS0_7IsolateE
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC1EPNS0_7IsolateE,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EEC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED2Ev,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED2Ev
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED2Ev, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED2Ev:
.LFB9384:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	movl	%edx, 41776(%rax)
	ret
	.cfi_endproc
.LFE9384:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED2Ev, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED2Ev
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED1Ev,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EED2Ev
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EE9IsAllowedEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EE9IsAllowedEPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EE9IsAllowedEPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EE9IsAllowedEPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EE9IsAllowedEPNS0_7IsolateE:
.LFB9386:
	.cfi_startproc
	endbr64
	movl	41776(%rdi), %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9386:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EE9IsAllowedEPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb0EE9IsAllowedEPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC2EPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC5EPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC2EPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC2EPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC2EPNS0_7IsolateE:
.LFB9388:
	.cfi_startproc
	endbr64
	movl	41776(%rsi), %eax
	movq	%rsi, (%rdi)
	movl	%eax, 8(%rdi)
	orl	$1, %eax
	movl	%eax, 41776(%rsi)
	ret
	.cfi_endproc
.LFE9388:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC2EPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC2EPNS0_7IsolateE
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC1EPNS0_7IsolateE
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC1EPNS0_7IsolateE,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EEC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED2Ev,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED2Ev
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED2Ev, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED2Ev:
.LFB9391:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	movl	%edx, 41776(%rax)
	ret
	.cfi_endproc
.LFE9391:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED2Ev, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED2Ev
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED1Ev
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED1Ev,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EED2Ev
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EE9IsAllowedEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EE9IsAllowedEPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EE9IsAllowedEPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EE9IsAllowedEPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EE9IsAllowedEPNS0_7IsolateE:
.LFB9393:
	.cfi_startproc
	endbr64
	movl	41776(%rdi), %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9393:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EE9IsAllowedEPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE0ELb1EE9IsAllowedEPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EEC2EPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EEC5EPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EEC2EPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EEC2EPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EEC2EPNS0_7IsolateE:
.LFB9395:
	.cfi_startproc
	endbr64
	movl	41776(%rsi), %eax
	movq	%rsi, (%rdi)
	movl	%eax, 8(%rdi)
	andl	$-3, %eax
	movl	%eax, 41776(%rsi)
	ret
	.cfi_endproc
.LFE9395:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EEC2EPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EEC2EPNS0_7IsolateE
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EEC1EPNS0_7IsolateE
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EEC1EPNS0_7IsolateE,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EEC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EED2Ev,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EED2Ev
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EED2Ev, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EED2Ev:
.LFB9398:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	movl	%edx, 41776(%rax)
	ret
	.cfi_endproc
.LFE9398:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EED2Ev, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EED2Ev
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EED1Ev
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EED1Ev,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EED2Ev
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EE9IsAllowedEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EE9IsAllowedEPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EE9IsAllowedEPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EE9IsAllowedEPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EE9IsAllowedEPNS0_7IsolateE:
.LFB9400:
	.cfi_startproc
	endbr64
	movl	41776(%rdi), %eax
	shrl	%eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9400:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EE9IsAllowedEPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb0EE9IsAllowedEPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EEC2EPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EEC5EPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EEC2EPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EEC2EPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EEC2EPNS0_7IsolateE:
.LFB9402:
	.cfi_startproc
	endbr64
	movl	41776(%rsi), %eax
	movq	%rsi, (%rdi)
	movl	%eax, 8(%rdi)
	orl	$2, %eax
	movl	%eax, 41776(%rsi)
	ret
	.cfi_endproc
.LFE9402:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EEC2EPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EEC2EPNS0_7IsolateE
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EEC1EPNS0_7IsolateE
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EEC1EPNS0_7IsolateE,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EEC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EED2Ev,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EED2Ev
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EED2Ev, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EED2Ev:
.LFB9405:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	movl	%edx, 41776(%rax)
	ret
	.cfi_endproc
.LFE9405:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EED2Ev, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EED2Ev
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EED1Ev
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EED1Ev,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EED2Ev
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EE9IsAllowedEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EE9IsAllowedEPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EE9IsAllowedEPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EE9IsAllowedEPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EE9IsAllowedEPNS0_7IsolateE:
.LFB9407:
	.cfi_startproc
	endbr64
	movl	41776(%rdi), %eax
	shrl	%eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9407:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EE9IsAllowedEPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE1ELb1EE9IsAllowedEPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EEC2EPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EEC5EPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EEC2EPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EEC2EPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EEC2EPNS0_7IsolateE:
.LFB9409:
	.cfi_startproc
	endbr64
	movl	41776(%rsi), %eax
	movq	%rsi, (%rdi)
	movl	%eax, 8(%rdi)
	andl	$-5, %eax
	movl	%eax, 41776(%rsi)
	ret
	.cfi_endproc
.LFE9409:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EEC2EPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EEC2EPNS0_7IsolateE
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EEC1EPNS0_7IsolateE
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EEC1EPNS0_7IsolateE,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EEC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EED2Ev,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EED2Ev
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EED2Ev, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EED2Ev:
.LFB9412:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	movl	%edx, 41776(%rax)
	ret
	.cfi_endproc
.LFE9412:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EED2Ev, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EED2Ev
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EED1Ev
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EED1Ev,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EED2Ev
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EE9IsAllowedEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EE9IsAllowedEPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EE9IsAllowedEPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EE9IsAllowedEPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EE9IsAllowedEPNS0_7IsolateE:
.LFB9414:
	.cfi_startproc
	endbr64
	movl	41776(%rdi), %eax
	shrl	$2, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9414:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EE9IsAllowedEPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb0EE9IsAllowedEPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EEC2EPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EEC5EPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EEC2EPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EEC2EPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EEC2EPNS0_7IsolateE:
.LFB9416:
	.cfi_startproc
	endbr64
	movl	41776(%rsi), %eax
	movq	%rsi, (%rdi)
	movl	%eax, 8(%rdi)
	orl	$4, %eax
	movl	%eax, 41776(%rsi)
	ret
	.cfi_endproc
.LFE9416:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EEC2EPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EEC2EPNS0_7IsolateE
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EEC1EPNS0_7IsolateE
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EEC1EPNS0_7IsolateE,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EEC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EED2Ev,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EED2Ev
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EED2Ev, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EED2Ev:
.LFB9419:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	movl	%edx, 41776(%rax)
	ret
	.cfi_endproc
.LFE9419:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EED2Ev, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EED2Ev
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EED1Ev
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EED1Ev,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EED2Ev
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EE9IsAllowedEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EE9IsAllowedEPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EE9IsAllowedEPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EE9IsAllowedEPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EE9IsAllowedEPNS0_7IsolateE:
.LFB9421:
	.cfi_startproc
	endbr64
	movl	41776(%rdi), %eax
	shrl	$2, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9421:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EE9IsAllowedEPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE2ELb1EE9IsAllowedEPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EEC2EPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EEC5EPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EEC2EPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EEC2EPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EEC2EPNS0_7IsolateE:
.LFB9423:
	.cfi_startproc
	endbr64
	movl	41776(%rsi), %eax
	movq	%rsi, (%rdi)
	movl	%eax, 8(%rdi)
	andl	$-9, %eax
	movl	%eax, 41776(%rsi)
	ret
	.cfi_endproc
.LFE9423:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EEC2EPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EEC2EPNS0_7IsolateE
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EEC1EPNS0_7IsolateE
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EEC1EPNS0_7IsolateE,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EEC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EED2Ev,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EED2Ev
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EED2Ev, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EED2Ev:
.LFB9426:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	movl	%edx, 41776(%rax)
	ret
	.cfi_endproc
.LFE9426:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EED2Ev, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EED2Ev
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EED1Ev
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EED1Ev,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EED2Ev
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EE9IsAllowedEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EE9IsAllowedEPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EE9IsAllowedEPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EE9IsAllowedEPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EE9IsAllowedEPNS0_7IsolateE:
.LFB9428:
	.cfi_startproc
	endbr64
	movl	41776(%rdi), %eax
	shrl	$3, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9428:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EE9IsAllowedEPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb0EE9IsAllowedEPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EEC2EPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EEC5EPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EEC2EPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EEC2EPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EEC2EPNS0_7IsolateE:
.LFB9430:
	.cfi_startproc
	endbr64
	movl	41776(%rsi), %eax
	movq	%rsi, (%rdi)
	movl	%eax, 8(%rdi)
	orl	$8, %eax
	movl	%eax, 41776(%rsi)
	ret
	.cfi_endproc
.LFE9430:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EEC2EPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EEC2EPNS0_7IsolateE
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EEC1EPNS0_7IsolateE
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EEC1EPNS0_7IsolateE,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EEC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EED2Ev,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EED2Ev
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EED2Ev, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EED2Ev:
.LFB9433:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	movl	%edx, 41776(%rax)
	ret
	.cfi_endproc
.LFE9433:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EED2Ev, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EED2Ev
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EED1Ev
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EED1Ev,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EED2Ev
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EE9IsAllowedEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EE9IsAllowedEPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EE9IsAllowedEPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EE9IsAllowedEPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EE9IsAllowedEPNS0_7IsolateE:
.LFB9435:
	.cfi_startproc
	endbr64
	movl	41776(%rdi), %eax
	shrl	$3, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9435:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EE9IsAllowedEPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE3ELb1EE9IsAllowedEPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EEC2EPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EEC5EPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EEC2EPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EEC2EPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EEC2EPNS0_7IsolateE:
.LFB9437:
	.cfi_startproc
	endbr64
	movl	41776(%rsi), %eax
	movq	%rsi, (%rdi)
	movl	%eax, 8(%rdi)
	andl	$-17, %eax
	movl	%eax, 41776(%rsi)
	ret
	.cfi_endproc
.LFE9437:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EEC2EPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EEC2EPNS0_7IsolateE
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EEC1EPNS0_7IsolateE
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EEC1EPNS0_7IsolateE,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EEC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EED2Ev,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EED2Ev
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EED2Ev, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EED2Ev:
.LFB9440:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	movl	%edx, 41776(%rax)
	ret
	.cfi_endproc
.LFE9440:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EED2Ev, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EED2Ev
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EED1Ev
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EED1Ev,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EED2Ev
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EE9IsAllowedEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EE9IsAllowedEPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EE9IsAllowedEPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EE9IsAllowedEPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EE9IsAllowedEPNS0_7IsolateE:
.LFB9442:
	.cfi_startproc
	endbr64
	movl	41776(%rdi), %eax
	shrl	$4, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9442:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EE9IsAllowedEPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb0EE9IsAllowedEPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EEC2EPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EEC5EPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EEC2EPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EEC2EPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EEC2EPNS0_7IsolateE:
.LFB9444:
	.cfi_startproc
	endbr64
	movl	41776(%rsi), %eax
	movq	%rsi, (%rdi)
	movl	%eax, 8(%rdi)
	orl	$16, %eax
	movl	%eax, 41776(%rsi)
	ret
	.cfi_endproc
.LFE9444:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EEC2EPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EEC2EPNS0_7IsolateE
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EEC1EPNS0_7IsolateE
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EEC1EPNS0_7IsolateE,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EEC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EED2Ev,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EED2Ev
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EED2Ev, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EED2Ev:
.LFB9447:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	movl	%edx, 41776(%rax)
	ret
	.cfi_endproc
.LFE9447:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EED2Ev, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EED2Ev
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EED1Ev
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EED1Ev,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EED2Ev
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EE9IsAllowedEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EE9IsAllowedEPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EE9IsAllowedEPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EE9IsAllowedEPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EE9IsAllowedEPNS0_7IsolateE:
.LFB9449:
	.cfi_startproc
	endbr64
	movl	41776(%rdi), %eax
	shrl	$4, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9449:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EE9IsAllowedEPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE4ELb1EE9IsAllowedEPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EEC2EPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EEC5EPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EEC2EPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EEC2EPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EEC2EPNS0_7IsolateE:
.LFB9451:
	.cfi_startproc
	endbr64
	movl	41776(%rsi), %eax
	movq	%rsi, (%rdi)
	movl	%eax, 8(%rdi)
	andl	$-33, %eax
	movl	%eax, 41776(%rsi)
	ret
	.cfi_endproc
.LFE9451:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EEC2EPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EEC2EPNS0_7IsolateE
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EEC1EPNS0_7IsolateE
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EEC1EPNS0_7IsolateE,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EEC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EED2Ev,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EED2Ev
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EED2Ev, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EED2Ev:
.LFB9454:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	movl	%edx, 41776(%rax)
	ret
	.cfi_endproc
.LFE9454:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EED2Ev, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EED2Ev
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EED1Ev
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EED1Ev,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EED2Ev
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EE9IsAllowedEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EE9IsAllowedEPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EE9IsAllowedEPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EE9IsAllowedEPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EE9IsAllowedEPNS0_7IsolateE:
.LFB9456:
	.cfi_startproc
	endbr64
	movl	41776(%rdi), %eax
	shrl	$5, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9456:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EE9IsAllowedEPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb0EE9IsAllowedEPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EEC2EPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EEC5EPNS0_7IsolateE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EEC2EPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EEC2EPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EEC2EPNS0_7IsolateE:
.LFB9458:
	.cfi_startproc
	endbr64
	movl	41776(%rsi), %eax
	movq	%rsi, (%rdi)
	movl	%eax, 8(%rdi)
	orl	$32, %eax
	movl	%eax, 41776(%rsi)
	ret
	.cfi_endproc
.LFE9458:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EEC2EPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EEC2EPNS0_7IsolateE
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EEC1EPNS0_7IsolateE
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EEC1EPNS0_7IsolateE,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EEC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EED2Ev,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EED2Ev
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EED2Ev, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EED2Ev:
.LFB9461:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	movl	%edx, 41776(%rax)
	ret
	.cfi_endproc
.LFE9461:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EED2Ev, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EED2Ev
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EED1Ev
	.set	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EED1Ev,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EED2Ev
	.section	.text._ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EE9IsAllowedEPNS0_7IsolateE,"axG",@progbits,_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EE9IsAllowedEPNS0_7IsolateE,comdat
	.p2align 4
	.weak	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EE9IsAllowedEPNS0_7IsolateE
	.type	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EE9IsAllowedEPNS0_7IsolateE, @function
_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EE9IsAllowedEPNS0_7IsolateE:
.LFB9463:
	.cfi_startproc
	endbr64
	movl	41776(%rdi), %eax
	shrl	$5, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE9463:
	.size	_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EE9IsAllowedEPNS0_7IsolateE, .-_ZN2v88internal21PerIsolateAssertScopeILNS0_20PerIsolateAssertTypeE5ELb1EE9IsAllowedEPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I_assert_scope.cc,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I_assert_scope.cc, @function
_GLOBAL__sub_I_assert_scope.cc:
.LFB10112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10112:
	.size	_GLOBAL__sub_I_assert_scope.cc, .-_GLOBAL__sub_I_assert_scope.cc
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_assert_scope.cc
	.section	.bss._ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object, @object
	.size	_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object, 8
_ZGVZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object,"aw",@nobits
	.align 4
	.type	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object, @object
	.size	_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object, 4
_ZZN2v88internal12_GLOBAL__N_121GetPerThreadAssertKeyEvE6object:
	.zero	4
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
