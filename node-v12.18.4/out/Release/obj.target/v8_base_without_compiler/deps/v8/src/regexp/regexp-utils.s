	.file	"regexp-utils.cc"
	.text
	.section	.rodata._ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NewArray"
	.section	.text._ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0, @function
_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0:
.LFB21572:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$8, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L2
.L4:
	movq	$0, (%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$8, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L4
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE21572:
	.size	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0, .-_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0
	.section	.text._ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb
	.type	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb, @function
_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb:
.LFB17802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leal	(%rdx,%rdx), %edi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %r8
	movq	15(%r8), %rax
	sarq	$32, %rax
	cmpl	%eax, %edi
	jl	.L10
.L28:
	testq	%rcx, %rcx
	je	.L12
	movb	$0, (%rcx)
.L12:
	popq	%rbx
	leaq	128(%r12), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	leal	40(,%rdi,8), %eax
	cltq
	movq	-1(%r8,%rax), %rbx
	leal	48(,%rdi,8), %eax
	cltq
	movq	-1(%r8,%rax), %r13
	sarq	$32, %rbx
	sarq	$32, %r13
	cmpl	$-1, %ebx
	je	.L28
	cmpl	$-1, %r13d
	je	.L28
	testq	%rcx, %rcx
	je	.L17
	movb	$1, (%rcx)
	movq	(%rsi), %r8
.L17:
	movq	23(%r8), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L18
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L19:
	testl	%ebx, %ebx
	jne	.L21
	movq	(%rsi), %rax
	cmpl	11(%rax), %r13d
	je	.L22
.L21:
	movl	%r13d, %ecx
	movl	%ebx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %rsi
.L22:
	popq	%rbx
	movq	%rsi, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L29
.L20:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L20
	.cfi_endproc
.LFE17802:
	.size	_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb, .-_ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb
	.section	.text._ZN2v88internal11RegExpUtils12SetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpUtils12SetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEm
	.type	_ZN2v88internal11RegExpUtils12SetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEm, @function
_ZN2v88internal11RegExpUtils12SetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEm:
.LFB17804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2147483648, %eax
	movl	$4294967295, %ecx
	addq	%rdx, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	cmpq	%rcx, %rax
	ja	.L31
	movq	41112(%rdi), %rdi
	salq	$32, %rdx
	movq	%rdx, %rsi
	testq	%rdi, %rdi
	je	.L32
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L35:
	movq	12464(%r12), %rax
	movq	0(%r13), %rbx
	movq	39(%rax), %rax
	movq	1047(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L36
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	-1(%rbx), %rax
	cmpq	55(%rsi), %rax
	je	.L42
.L46:
	addq	$16, %rsp
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	leaq	2760(%r12), %rdx
	movl	$1, %r9d
	popq	%r12
	xorl	%r8d, %r8d
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L44
.L34:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L36:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L45
.L38:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	-1(%rbx), %rax
	cmpq	55(%rsi), %rax
	jne	.L46
.L42:
	movq	0(%r13), %rax
	movq	(%r14), %rdx
	movq	%rdx, 47(%rax)
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	cvtsi2sdq	%rdx, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r12, %rdi
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L34
	.cfi_endproc
.LFE17804:
	.size	_ZN2v88internal11RegExpUtils12SetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEm, .-_ZN2v88internal11RegExpUtils12SetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEm
	.section	.text._ZN2v88internal11RegExpUtils12GetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpUtils12GetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE
	.type	_ZN2v88internal11RegExpUtils12GetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE, @function
_ZN2v88internal11RegExpUtils12GetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE:
.LFB17805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$112, %rsp
	movq	(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	1047(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L49:
	movq	-1(%r13), %rax
	cmpq	55(%rsi), %rax
	jne	.L51
	movq	(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	47(%rax), %rsi
	testq	%rdi, %rdi
	je	.L52
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L55:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L67
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L68
.L54:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L51:
	movq	(%rbx), %rax
	leaq	2760(%r12), %r14
	testb	$1, %al
	jne	.L56
.L58:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r13
.L57:
	movq	2760(%r12), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L59
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L59:
	movl	%eax, -128(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -116(%rbp)
	movq	2760(%r12), %rax
	movq	%r12, -104(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L69
.L60:
	leaq	-128(%rbp), %r12
	movq	%r14, -96(%rbp)
	movq	%r12, %rdi
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%r13, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -124(%rbp)
	jne	.L61
	movq	-104(%rbp), %rax
	addq	$88, %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L48:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L70
.L50:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L56:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L58
	movq	%rbx, %r13
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r14
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	jmp	.L54
.L67:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17805:
	.size	_ZN2v88internal11RegExpUtils12GetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE, .-_ZN2v88internal11RegExpUtils12GetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEE
	.section	.rodata._ZN2v88internal11RegExpUtils10RegExpExecEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"RegExp.prototype.exec"
.LC2:
	.string	"(location_) != nullptr"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal11RegExpUtils10RegExpExecEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpUtils10RegExpExecEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal11RegExpUtils10RegExpExecEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal11RegExpUtils10RegExpExecEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEE:
.LFB17806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	cmpq	88(%rdi), %rax
	je	.L101
.L72:
	testb	$1, %al
	jne	.L102
.L81:
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	je	.L103
	leaq	.LC1(%rip), %rax
	xorl	%edx, %edx
	leaq	-144(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	movq	$21, -136(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L104
	movl	$62, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L100:
	xorl	%r12d, %r12d
.L80:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	je	.L81
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	movq	%rax, %r8
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	testq	%rax, %rax
	je	.L99
	movq	(%rax), %rdx
	testb	$1, %dl
	jne	.L106
.L87:
	cmpq	%rdx, 104(%r12)
	je	.L89
	xorl	%edx, %edx
	movl	$66, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L99:
	xorl	%r12d, %r12d
.L86:
	movq	%r15, %rdi
	call	_ZdaPv@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L103:
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1039(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L92:
	call	_ZN2v88internal8NewArrayINS0_6HandleINS0_6ObjectEEEEEPT_m.constprop.0
	movq	%r12, %rdi
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%rbx, (%rax)
	movq	%rax, %r13
	movq	%rax, %r8
	movq	%r15, %rsi
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZdaPv@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L101:
	movq	(%rsi), %rax
	leaq	2512(%rdi), %r15
	testb	$1, %al
	jne	.L73
.L75:
	movl	$-1, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r13
.L74:
	movq	2512(%r12), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L76
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L76:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	2512(%r12), %rax
	movq	%r12, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L107
.L77:
	movq	%r13, -80(%rbp)
	leaq	-144(%rbp), %r13
	movq	%r13, %rdi
	movq	%r15, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L78
	movq	-120(%rbp), %rax
	leaq	88(%rax), %r13
	movq	88(%rax), %rax
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L91:
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L108
.L93:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L73:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L75
	movq	%rsi, %r13
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L104:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L100
	movq	(%rax), %rax
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L106:
	movq	-1(%rdx), %rcx
	cmpw	$1023, 11(%rcx)
	jbe	.L87
.L89:
	movq	%rax, %r12
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r15
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L93
.L105:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17806:
	.size	_ZN2v88internal11RegExpUtils10RegExpExecEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal11RegExpUtils10RegExpExecEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal11RegExpUtils8IsRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpUtils8IsRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal11RegExpUtils8IsRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal11RegExpUtils8IsRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB17808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L110
.L112:
	movl	$1, %eax
.L111:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L134
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L112
	movq	3880(%rdi), %rax
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	$3, %edx
	leaq	3880(%rdi), %rsi
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L113
	movl	11(%rax), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%edx, %edx
	andl	$3, %edx
.L113:
	movabsq	$824633720832, %rax
	movl	%edx, -128(%rbp)
	movq	%rax, -116(%rbp)
	movq	3880(%r12), %rax
	movq	%r12, -104(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L135
.L114:
	leaq	-128(%rbp), %r13
	movq	%rsi, -96(%rbp)
	movq	%r13, %rdi
	movq	$0, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	$-1, -56(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -124(%rbp)
	jne	.L115
	movq	-104(%rbp), %rax
	addq	$88, %rax
.L116:
	movq	(%rax), %rax
	cmpq	%rax, 88(%r12)
	je	.L117
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movl	%eax, %r13d
	movq	(%rbx), %rax
	testb	%r13b, %r13b
	je	.L118
	testb	$1, %al
	jne	.L119
.L122:
	movl	$72, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
.L120:
	movl	$1, %eax
	movl	%r13d, %ecx
	movb	%cl, %ah
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L115:
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L116
	movb	$0, %ah
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L117:
	movq	(%rbx), %rax
	xorl	%edx, %edx
	testb	$1, %al
	jne	.L136
.L124:
	movl	$1, %eax
	movb	%dl, %ah
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L118:
	testb	$1, %al
	je	.L120
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	jne	.L120
	movl	$73, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L136:
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	sete	%dl
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rsi
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L119:
	movq	-1(%rax), %rax
	cmpw	$1075, 11(%rax)
	jne	.L122
	jmp	.L120
.L134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17808:
	.size	_ZN2v88internal11RegExpUtils8IsRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal11RegExpUtils8IsRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal11RegExpUtils18IsUnmodifiedRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpUtils18IsUnmodifiedRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal11RegExpUtils18IsUnmodifiedRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal11RegExpUtils18IsUnmodifiedRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB17809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rsi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %rax
	notq	%rax
	movl	%eax, %r12d
	andl	$1, %r12d
	je	.L138
.L140:
	xorl	%r12d, %r12d
.L137:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L160
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movq	-1(%rbx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L140
	movq	12464(%rdi), %rax
	movq	%rbx, -48(%rbp)
	movq	%rdi, %r13
	movq	39(%rax), %rax
	movq	1047(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L141
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L142:
	movq	-1(%rbx), %rax
	cmpq	55(%rsi), %rax
	jne	.L137
	movq	-48(%rbp), %rax
	movq	-1(%rax), %rax
	movq	23(%rax), %rbx
	testb	$1, %bl
	je	.L137
	movq	-1(%rbx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L137
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	1087(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L146
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L147:
	movq	-1(%rbx), %rdx
	cmpq	%rdx, (%rax)
	jne	.L137
	movq	39(%rdx), %rax
	movq	55(%rax), %rax
	btq	$34, %rax
	jnc	.L137
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal10JSReceiver18GetCreationContextEv@PLT
	movq	(%rax), %rax
	movq	1239(%rax), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L137
	sarq	$32, %rax
	subq	$1, %rax
	jne	.L137
	movq	-48(%rbp), %rax
	movq	47(%rax), %rax
	testb	$1, %al
	jne	.L137
	sarq	$32, %rax
	notq	%rax
	shrq	$63, %rax
	movq	%rax, %r12
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L141:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L161
.L143:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L143
.L146:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L162
.L148:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L147
.L162:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L148
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17809:
	.size	_ZN2v88internal11RegExpUtils18IsUnmodifiedRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal11RegExpUtils18IsUnmodifiedRegExpEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.rodata._ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb.str1.1,"aMS",@progbits,1
.LC4:
	.string	"unreachable code"
	.section	.text._ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb
	.type	_ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb, @function
_ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb:
.LFB17810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	1(%rsi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movslq	11(%rax), %r14
	cmpq	%rsi, %r14
	jbe	.L164
	testb	%dl, %dl
	jne	.L194
.L164:
	movq	%r13, %r15
.L163:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L195
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L165
	leaq	.L167(%rip), %rcx
	movzwl	%dx, %edx
	movq	%rdi, %r12
	movslq	(%rcx,%rdx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb,"a",@progbits
	.align 4
	.align 4
.L167:
	.long	.L173-.L167
	.long	.L170-.L167
	.long	.L172-.L167
	.long	.L168-.L167
	.long	.L165-.L167
	.long	.L166-.L167
	.long	.L165-.L167
	.long	.L165-.L167
	.long	.L171-.L167
	.long	.L170-.L167
	.long	.L169-.L167
	.long	.L168-.L167
	.long	.L165-.L167
	.long	.L166-.L167
	.section	.text._ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb
	.p2align 4,,10
	.p2align 3
.L169:
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
.L171:
	leaq	1(%rbx), %r15
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L166:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
.L174:
	addw	$10240, %ax
	leaq	1(%rbx), %r13
	cmpw	$1023, %ax
	ja	.L164
	movq	%r13, %r15
	cmpq	%r13, %r14
	jbe	.L164
	movq	(%r12), %rdx
	leal	1(%rbx), %r12d
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L165
	leaq	.L176(%rip), %rcx
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb
	.align 4
	.align 4
.L176:
	.long	.L182-.L176
	.long	.L179-.L176
	.long	.L181-.L176
	.long	.L177-.L176
	.long	.L165-.L176
	.long	.L175-.L176
	.long	.L165-.L176
	.long	.L165-.L176
	.long	.L163-.L176
	.long	.L179-.L176
	.long	.L178-.L176
	.long	.L177-.L176
	.long	.L165-.L176
	.long	.L175-.L176
	.section	.text._ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb
	.p2align 4,,10
	.p2align 3
.L170:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	jmp	.L174
.L165:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L173:
	leal	16(%rsi,%rsi), %edx
	movslq	%edx, %rdx
	movzwl	-1(%rax,%rdx), %eax
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L172:
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%ebx, %rdx
	movzwl	(%rax,%rdx,2), %eax
	jmp	.L174
.L175:
	leaq	-64(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10ThinString3GetEi@PLT
.L183:
	addw	$9216, %ax
	leaq	2(%rbx), %r15
	cmpw	$1023, %ax
	ja	.L164
	jmp	.L163
.L177:
	leaq	-64(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal12SlicedString3GetEi@PLT
	jmp	.L183
.L179:
	leaq	-64(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10ConsString3GetEi@PLT
	jmp	.L183
.L181:
	movq	15(%rdx), %rdi
	movslq	%r12d, %r12
	movq	(%rdi), %rax
	call	*48(%rax)
	movzwl	(%rax,%r12,2), %eax
	jmp	.L183
.L182:
	leal	18(%rbx,%rbx), %eax
	cltq
	movzwl	-1(%rdx,%rax), %eax
	jmp	.L183
.L178:
	movq	15(%rdx), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	jmp	.L163
.L195:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17810:
	.size	_ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb, .-_ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb
	.section	.text._ZN2v88internal11RegExpUtils22SetAdvancedStringIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11RegExpUtils22SetAdvancedStringIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEEb
	.type	_ZN2v88internal11RegExpUtils22SetAdvancedStringIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEEb, @function
_ZN2v88internal11RegExpUtils22SetAdvancedStringIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEEb:
.LFB17811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	2760(%rdi), %rbx
	subq	$120, %rsp
	movl	%ecx, -148(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L197
.L199:
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %r15
.L198:
	movq	2760(%r12), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rcx
	cmpw	$64, 11(%rcx)
	jne	.L200
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L200:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	2760(%r12), %rax
	movq	%r12, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L231
.L201:
	movq	%r15, -80(%rbp)
	leaq	-144(%rbp), %r15
	movq	%r15, %rdi
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	jne	.L202
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L203:
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L205
	sarq	$32, %rsi
	movl	$0, %eax
	movq	41112(%r12), %rdi
	cmovs	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L206
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L207:
	testq	%rax, %rax
	je	.L210
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L211
.L234:
	sarq	$32, %rsi
	movl	$0, %eax
	testq	%rsi, %rsi
	cmovle	%rax, %rsi
.L212:
	movzbl	-148(%rbp), %edx
	movq	%r14, %rdi
	call	_ZN2v88internal11RegExpUtils18AdvanceStringIndexENS0_6HandleINS0_6StringEEEmb
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal11RegExpUtils12SetLastIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEm
.L204:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L232
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L233
.L208:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	movq	(%rax), %rsi
	testb	$1, %sil
	je	.L234
.L211:
	movsd	7(%rsi), %xmm0
	xorl	%esi, %esi
	comisd	.LC5(%rip), %xmm0
	jb	.L212
	movsd	.LC6(%rip), %xmm1
	movq	$-1, %rsi
	comisd	%xmm0, %xmm1
	jbe	.L212
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L215
	cvttsd2siq	%xmm0, %rsi
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L202:
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	jne	.L203
.L210:
	xorl	%eax, %eax
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L197:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L199
	movq	%rsi, %r15
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToLengthEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L231:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %rbx
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L215:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rsi
	btcq	$63, %rsi
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%r12, %rdi
	movq	%rsi, -160(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-160(%rbp), %rsi
	jmp	.L208
.L232:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17811:
	.size	_ZN2v88internal11RegExpUtils22SetAdvancedStringIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEEb, .-_ZN2v88internal11RegExpUtils22SetAdvancedStringIndexEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEENS4_INS0_6StringEEEb
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb, @function
_GLOBAL__sub_I__ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb:
.LFB21514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21514:
	.size	_GLOBAL__sub_I__ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb, .-_GLOBAL__sub_I__ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11RegExpUtils20GenericCaptureGetterEPNS0_7IsolateENS0_6HandleINS0_15RegExpMatchInfoEEEiPb
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC5:
	.long	0
	.long	1072693248
	.align 8
.LC6:
	.long	0
	.long	1139802112
	.align 8
.LC7:
	.long	0
	.long	1138753536
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
