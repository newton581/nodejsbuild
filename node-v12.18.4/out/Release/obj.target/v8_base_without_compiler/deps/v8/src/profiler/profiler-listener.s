	.file	"profiler-listener.cc"
	.text
	.section	.text._ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.type	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv, @function
_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv:
.LFB6703:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6703:
	.size	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv, .-_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.section	.text._ZN2v88internal16ProfilerListener17CodeMovingGCEventEv,"axG",@progbits,_ZN2v88internal16ProfilerListener17CodeMovingGCEventEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16ProfilerListener17CodeMovingGCEventEv
	.type	_ZN2v88internal16ProfilerListener17CodeMovingGCEventEv, @function
_ZN2v88internal16ProfilerListener17CodeMovingGCEventEv:
.LFB7580:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7580:
	.size	_ZN2v88internal16ProfilerListener17CodeMovingGCEventEv, .-_ZN2v88internal16ProfilerListener17CodeMovingGCEventEv
	.section	.text._ZN2v88internal16ProfilerListener27SharedFunctionInfoMoveEventEmm,"axG",@progbits,_ZN2v88internal16ProfilerListener27SharedFunctionInfoMoveEventEmm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16ProfilerListener27SharedFunctionInfoMoveEventEmm
	.type	_ZN2v88internal16ProfilerListener27SharedFunctionInfoMoveEventEmm, @function
_ZN2v88internal16ProfilerListener27SharedFunctionInfoMoveEventEmm:
.LFB7581:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7581:
	.size	_ZN2v88internal16ProfilerListener27SharedFunctionInfoMoveEventEmm, .-_ZN2v88internal16ProfilerListener27SharedFunctionInfoMoveEventEmm
	.section	.text._ZN2v88internal16ProfilerListener22NativeContextMoveEventEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener22NativeContextMoveEventEmm
	.type	_ZN2v88internal16ProfilerListener22NativeContextMoveEventEmm, @function
_ZN2v88internal16ProfilerListener22NativeContextMoveEventEmm:
.LFB18517:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$6, -80(%rbp)
	leaq	-80(%rbp), %rsi
	movups	%xmm0, -72(%rbp)
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L8
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18517:
	.size	_ZN2v88internal16ProfilerListener22NativeContextMoveEventEmm, .-_ZN2v88internal16ProfilerListener22NativeContextMoveEventEmm
	.section	.text._ZN2v88internal16ProfilerListenerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListenerD2Ev
	.type	_ZN2v88internal16ProfilerListenerD2Ev, @function
_ZN2v88internal16ProfilerListenerD2Ev:
.LFB18394:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal16ProfilerListenerE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	jmp	_ZN2v88internal14StringsStorageD1Ev@PLT
	.cfi_endproc
.LFE18394:
	.size	_ZN2v88internal16ProfilerListenerD2Ev, .-_ZN2v88internal16ProfilerListenerD2Ev
	.globl	_ZN2v88internal16ProfilerListenerD1Ev
	.set	_ZN2v88internal16ProfilerListenerD1Ev,_ZN2v88internal16ProfilerListenerD2Ev
	.section	.text._ZN2v88internal16ProfilerListenerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListenerD0Ev
	.type	_ZN2v88internal16ProfilerListenerD0Ev, @function
_ZN2v88internal16ProfilerListenerD0Ev:
.LFB18396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal16ProfilerListenerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -24(%rdi)
	call	_ZN2v88internal14StringsStorageD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18396:
	.size	_ZN2v88internal16ProfilerListenerD0Ev, .-_ZN2v88internal16ProfilerListenerD0Ev
	.section	.text._ZN2v88internal16ProfilerListener13CallbackEventENS0_4NameEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener13CallbackEventENS0_4NameEm
	.type	_ZN2v88internal16ProfilerListener13CallbackEventENS0_4NameEm, @function
_ZN2v88internal16ProfilerListener13CallbackEventENS0_4NameEm:
.LFB18397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$24, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -88(%rbp)
	movl	$1, -96(%rbp)
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movl	$64, %edi
	movq	%rax, %r12
	call	_Znwm@PLT
	movq	_ZN2v88internal9CodeEntry18kEmptyResourceNameE(%rip), %rdx
	movq	16(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movl	$397577, (%rax)
	leaq	-96(%rbp), %rsi
	movq	%r12, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	%rax, -80(%rbp)
	movl	$1, -72(%rbp)
	movups	%xmm0, 24(%rax)
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18397:
	.size	_ZN2v88internal16ProfilerListener13CallbackEventENS0_4NameEm, .-_ZN2v88internal16ProfilerListener13CallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.type	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE, @function
_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE:
.LFB18510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	addq	$24, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	movq	%rcx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	orl	$-2147086080, %r12d
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movl	$1, -128(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal14StringsStorage7GetCopyEPKc@PLT
	movl	$64, %edi
	movq	(%rbx), %r15
	movq	%rax, %r14
	call	_Znwm@PLT
	movq	_ZN2v88internal9CodeEntry23kWasmResourceNamePrefixE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	16(%r13), %rdi
	movl	%r12d, (%rax)
	leaq	-128(%rbp), %rsi
	movq	%r14, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 40(%rax)
	movq	%r15, 48(%rax)
	movq	$0, 56(%rax)
	movq	%rax, -112(%rbp)
	movups	%xmm0, 24(%rax)
	movq	8(%rbx), %rax
	movl	%eax, -104(%rbp)
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L19
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18510:
	.size	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE, .-_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.section	.rodata._ZN2v88internal16ProfilerListener19GetterCallbackEventENS0_4NameEm.str1.1,"aMS",@progbits,1
.LC0:
	.string	"get "
	.section	.text._ZN2v88internal16ProfilerListener19GetterCallbackEventENS0_4NameEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener19GetterCallbackEventENS0_4NameEm
	.type	_ZN2v88internal16ProfilerListener19GetterCallbackEventENS0_4NameEm, @function
_ZN2v88internal16ProfilerListener19GetterCallbackEventENS0_4NameEm:
.LFB18514:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$24, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -88(%rbp)
	movq	%rsi, %rdx
	leaq	.LC0(%rip), %rsi
	movl	$1, -96(%rbp)
	call	_ZN2v88internal14StringsStorage11GetConsNameEPKcNS0_4NameE@PLT
	movl	$64, %edi
	movq	%rax, %r12
	call	_Znwm@PLT
	movq	_ZN2v88internal9CodeEntry18kEmptyResourceNameE(%rip), %rdx
	movq	16(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movl	$397577, (%rax)
	leaq	-96(%rbp), %rsi
	movq	%r12, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	%rax, -80(%rbp)
	movl	$1, -72(%rbp)
	movups	%xmm0, 24(%rax)
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L23:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18514:
	.size	_ZN2v88internal16ProfilerListener19GetterCallbackEventENS0_4NameEm, .-_ZN2v88internal16ProfilerListener19GetterCallbackEventENS0_4NameEm
	.section	.rodata._ZN2v88internal16ProfilerListener19SetterCallbackEventENS0_4NameEm.str1.1,"aMS",@progbits,1
.LC1:
	.string	"set "
	.section	.text._ZN2v88internal16ProfilerListener19SetterCallbackEventENS0_4NameEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener19SetterCallbackEventENS0_4NameEm
	.type	_ZN2v88internal16ProfilerListener19SetterCallbackEventENS0_4NameEm, @function
_ZN2v88internal16ProfilerListener19SetterCallbackEventENS0_4NameEm:
.LFB18516:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$24, %rdi
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -88(%rbp)
	movq	%rsi, %rdx
	leaq	.LC1(%rip), %rsi
	movl	$1, -96(%rbp)
	call	_ZN2v88internal14StringsStorage11GetConsNameEPKcNS0_4NameE@PLT
	movl	$64, %edi
	movq	%rax, %r12
	call	_Znwm@PLT
	movq	_ZN2v88internal9CodeEntry18kEmptyResourceNameE(%rip), %rdx
	movq	16(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movl	$397577, (%rax)
	leaq	-96(%rbp), %rsi
	movq	%r12, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	%rax, -80(%rbp)
	movl	$1, -72(%rbp)
	movups	%xmm0, 24(%rax)
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L27:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18516:
	.size	_ZN2v88internal16ProfilerListener19SetterCallbackEventENS0_4NameEm, .-_ZN2v88internal16ProfilerListener19SetterCallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal16ProfilerListener13CodeMoveEventENS0_12AbstractCodeES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener13CodeMoveEventENS0_12AbstractCodeES2_
	.type	_ZN2v88internal16ProfilerListener13CodeMoveEventENS0_12AbstractCodeES2_, @function
_ZN2v88internal16ProfilerListener13CodeMoveEventENS0_12AbstractCodeES2_:
.LFB18511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$2, -96(%rbp)
	leaq	53(%rsi), %rax
	movq	-1(%rsi), %rdx
	cmpw	$69, 11(%rdx)
	je	.L39
.L32:
	movq	%rax, -88(%rbp)
	movq	-1(%rbx), %rdx
	leaq	53(%rbx), %rax
	cmpw	$69, 11(%rdx)
	je	.L40
.L36:
	movq	16(%r12), %rdi
	movq	%rax, -80(%rbp)
	leaq	-96(%rbp), %rsi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movl	43(%rsi), %ecx
	movq	%rsi, -104(%rbp)
	leaq	63(%rsi), %rax
	testl	%ecx, %ecx
	jns	.L32
	leaq	-104(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L40:
	movl	43(%rbx), %edx
	movq	%rbx, -104(%rbp)
	leaq	63(%rbx), %rax
	testl	%edx, %edx
	jns	.L36
	leaq	-104(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L36
.L41:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18511:
	.size	_ZN2v88internal16ProfilerListener13CodeMoveEventENS0_12AbstractCodeES2_, .-_ZN2v88internal16ProfilerListener13CodeMoveEventENS0_12AbstractCodeES2_
	.section	.text._ZN2v88internal16ProfilerListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal16ProfilerListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, @function
_ZN2v88internal16ProfilerListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE:
.LFB18512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$3, -96(%rbp)
	leaq	53(%rsi), %rax
	movq	-1(%rsi), %rdx
	cmpw	$69, 11(%rdx)
	je	.L49
.L46:
	movq	%rax, -88(%rbp)
	movl	47(%r12), %edi
	shrl	$20, %edi
	andl	$15, %edi
	call	_ZN2v88internal16GetBailoutReasonENS0_13BailoutReasonE@PLT
	movq	16(%rbx), %rdi
	leaq	-96(%rbp), %rsi
	movq	%rax, -80(%rbp)
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movl	43(%rsi), %edx
	movq	%rsi, -104(%rbp)
	leaq	63(%rsi), %rax
	testl	%edx, %edx
	jns	.L46
	leaq	-104(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L46
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18512:
	.size	_ZN2v88internal16ProfilerListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, .-_ZN2v88internal16ProfilerListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.section	.text._ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.type	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc, @function
_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc:
.LFB18398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movq	%rcx, %rsi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, -128(%rbp)
	leaq	53(%r14), %rax
	movq	-1(%rdx), %rdx
	cmpw	$69, 11(%rdx)
	je	.L66
.L55:
	leaq	24(%r12), %rdi
	movq	%rax, -120(%rbp)
	leaq	53(%r14), %r15
	call	_ZN2v88internal14StringsStorage7GetCopyEPKc@PLT
	movq	%rax, %r13
	movq	-1(%r14), %rax
	cmpw	$69, 11(%rax)
	je	.L67
.L59:
	movl	$64, %edi
	orl	$397568, %ebx
	call	_Znwm@PLT
	movq	_ZN2v88internal9CodeEntry18kEmptyResourceNameE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	%r13, 8(%rax)
	movq	%rax, -112(%rbp)
	movl	%ebx, (%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 40(%rax)
	movq	%r15, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, 24(%rax)
	movq	-1(%r14), %rax
	cmpw	$69, 11(%rax)
	je	.L68
	movl	11(%r14), %eax
.L63:
	movq	16(%r12), %rdi
	movl	%eax, -104(%rbp)
	leaq	-128(%rbp), %rsi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	43(%r14), %ecx
	movq	%r14, -136(%rbp)
	leaq	63(%r14), %rax
	testl	%ecx, %ecx
	jns	.L55
	leaq	-136(%rbp), %rdi
	movq	%rsi, -152(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-152(%rbp), %rsi
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L68:
	movl	43(%r14), %eax
	movq	%r14, -136(%rbp)
	testl	%eax, %eax
	js	.L70
	movl	39(%r14), %eax
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L67:
	movl	43(%r14), %edx
	movq	%r14, -136(%rbp)
	leaq	63(%r14), %r15
	testl	%edx, %edx
	jns	.L59
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %r15
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	jmp	.L63
.L69:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18398:
	.size	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc, .-_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.section	.text._ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.type	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE, @function
_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE:
.LFB18399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	movq	%rcx, %rsi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, -128(%rbp)
	leaq	53(%r14), %rax
	movq	-1(%rdx), %rdx
	cmpw	$69, 11(%rdx)
	je	.L86
.L75:
	leaq	24(%r12), %rdi
	movq	%rax, -120(%rbp)
	leaq	53(%r14), %r15
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movq	%rax, %r13
	movq	-1(%r14), %rax
	cmpw	$69, 11(%rax)
	je	.L87
.L79:
	movl	$64, %edi
	orl	$397568, %ebx
	call	_Znwm@PLT
	movq	_ZN2v88internal9CodeEntry18kEmptyResourceNameE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	%r13, 8(%rax)
	movq	%rax, -112(%rbp)
	movl	%ebx, (%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 40(%rax)
	movq	%r15, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, 24(%rax)
	movq	-1(%r14), %rax
	cmpw	$69, 11(%rax)
	je	.L88
	movl	11(%r14), %eax
.L83:
	movq	16(%r12), %rdi
	movl	%eax, -104(%rbp)
	leaq	-128(%rbp), %rsi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movl	43(%r14), %ecx
	movq	%r14, -136(%rbp)
	leaq	63(%r14), %rax
	testl	%ecx, %ecx
	jns	.L75
	leaq	-136(%rbp), %rdi
	movq	%rsi, -152(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-152(%rbp), %rsi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L88:
	movl	43(%r14), %eax
	movq	%r14, -136(%rbp)
	testl	%eax, %eax
	js	.L90
	movl	39(%r14), %eax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L87:
	movl	43(%r14), %edx
	movq	%r14, -136(%rbp)
	leaq	63(%r14), %r15
	testl	%edx, %edx
	jns	.L79
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %r15
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	jmp	.L83
.L89:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18399:
	.size	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE, .-_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.section	.rodata._ZN2v88internal16ProfilerListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"RegExp: "
	.section	.text._ZN2v88internal16ProfilerListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.type	_ZN2v88internal16ProfilerListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE, @function
_ZN2v88internal16ProfilerListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE:
.LFB18515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$1, -112(%rbp)
	leaq	53(%rsi), %rax
	movq	-1(%rsi), %rcx
	cmpw	$69, 11(%rcx)
	je	.L106
.L95:
	leaq	24(%r12), %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal14StringsStorage11GetConsNameEPKcNS0_4NameE@PLT
	leaq	53(%rbx), %r14
	movq	%rax, %r13
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	je	.L107
.L99:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	_ZN2v88internal9CodeEntry18kEmptyResourceNameE(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	%r13, 8(%rax)
	movq	%rax, -96(%rbp)
	movl	$397584, (%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 40(%rax)
	movq	%r14, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, 24(%rax)
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	je	.L108
	movl	11(%rbx), %eax
.L103:
	movq	16(%r12), %rdi
	movl	%eax, -88(%rbp)
	leaq	-112(%rbp), %rsi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	.cfi_restore_state
	movl	43(%rsi), %ecx
	movq	%rsi, -120(%rbp)
	leaq	63(%rsi), %rax
	testl	%ecx, %ecx
	jns	.L95
	leaq	-120(%rbp), %rdi
	movq	%rdx, -136(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-136(%rbp), %rdx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L108:
	movl	43(%rbx), %eax
	movq	%rbx, -120(%rbp)
	testl	%eax, %eax
	js	.L110
	movl	39(%rbx), %eax
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L107:
	movl	43(%rbx), %edx
	movq	%rbx, -120(%rbp)
	leaq	63(%rbx), %r14
	testl	%edx, %edx
	jns	.L99
	leaq	-120(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %r14
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L110:
	leaq	-120(%rbp), %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	jmp	.L103
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18515:
	.size	_ZN2v88internal16ProfilerListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE, .-_ZN2v88internal16ProfilerListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.section	.text._ZN2v88internal16ProfilerListenerC2EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListenerC2EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE
	.type	_ZN2v88internal16ProfilerListenerC2EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE, @function
_ZN2v88internal16ProfilerListenerC2EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE:
.LFB18391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal16ProfilerListenerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdi
	movq	%rax, -24(%rdi)
	movq	%rsi, -16(%rdi)
	movq	%rdx, -8(%rdi)
	call	_ZN2v88internal14StringsStorageC1Ev@PLT
	movl	%r12d, 48(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18391:
	.size	_ZN2v88internal16ProfilerListenerC2EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE, .-_ZN2v88internal16ProfilerListenerC2EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE
	.globl	_ZN2v88internal16ProfilerListenerC1EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE
	.set	_ZN2v88internal16ProfilerListenerC1EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE,_ZN2v88internal16ProfilerListenerC2EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE
	.section	.text._ZN2v88internal16ProfilerListener15InferScriptNameENS0_4NameENS0_18SharedFunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener15InferScriptNameENS0_4NameENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal16ProfilerListener15InferScriptNameENS0_4NameENS0_18SharedFunctionInfoE, @function
_ZN2v88internal16ProfilerListener15InferScriptNameENS0_4NameENS0_18SharedFunctionInfoE:
.LFB18518:
	.cfi_startproc
	endbr64
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L114
	movl	11(%rsi), %eax
	testl	%eax, %eax
	je	.L114
.L120:
	movq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	movq	31(%rdx), %rax
	testb	$1, %al
	je	.L120
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$86, 11(%rcx)
	je	.L121
.L117:
	movq	(%rdx), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L120
	testb	$1, %al
	jne	.L122
.L118:
	movq	103(%rax), %rax
	testb	$1, %al
	je	.L120
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	ja	.L120
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	movq	23(%rax), %rdx
	testb	$1, %dl
	je	.L120
	subq	$1, %rdx
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L122:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L118
	movq	23(%rax), %rax
	jmp	.L118
	.cfi_endproc
.LFE18518:
	.size	_ZN2v88internal16ProfilerListener15InferScriptNameENS0_4NameENS0_18SharedFunctionInfoE, .-_ZN2v88internal16ProfilerListener15InferScriptNameENS0_4NameENS0_18SharedFunctionInfoE
	.section	.text._ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.type	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE, @function
_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE:
.LFB18400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$136, %rsp
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$1, -128(%rbp)
	leaq	53(%rbx), %rax
	movq	-1(%rdx), %rdx
	cmpw	$69, 11(%rdx)
	je	.L138
.L127:
	leaq	-152(%rbp), %rdi
	leaq	24(%r12), %r15
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movq	-152(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	leaq	53(%rbx), %r14
	call	_ZN2v88internal16ProfilerListener15InferScriptNameENS0_4NameENS0_18SharedFunctionInfoE
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movq	%rax, -176(%rbp)
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	je	.L139
.L131:
	movq	-160(%rbp), %xmm0
	movl	$64, %edi
	orl	$397568, %r13d
	movhps	-176(%rbp), %xmm0
	movaps	%xmm0, -176(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm0
	movq	-152(%rbp), %rsi
	movl	%r13d, (%rax)
	movq	%rax, %rdi
	movq	$0, 40(%rax)
	movq	%r14, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, 8(%rax)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%rax)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal9CodeEntry16FillFunctionInfoENS0_18SharedFunctionInfoE@PLT
	movq	-1(%rbx), %rax
	cmpw	$69, 11(%rax)
	je	.L140
	movl	11(%rbx), %eax
.L135:
	movq	16(%r12), %rdi
	movl	%eax, -104(%rbp)
	leaq	-128(%rbp), %rsi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movl	43(%rbx), %ecx
	movq	%rbx, -136(%rbp)
	leaq	63(%rbx), %rax
	testl	%ecx, %ecx
	jns	.L127
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L140:
	movl	43(%rbx), %eax
	movq	%rbx, -136(%rbp)
	testl	%eax, %eax
	js	.L142
	movl	39(%rbx), %eax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L139:
	movl	43(%rbx), %edx
	movq	%rbx, -136(%rbp)
	leaq	63(%rbx), %r14
	testl	%edx, %edx
	jns	.L131
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %r14
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	-136(%rbp), %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	jmp	.L135
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18400:
	.size	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE, .-_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.section	.rodata._ZN2v88internal16ProfilerListener15GetFunctionNameENS0_18SharedFunctionInfoE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"unreachable code"
	.section	.text._ZN2v88internal16ProfilerListener15GetFunctionNameENS0_18SharedFunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener15GetFunctionNameENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal16ProfilerListener15GetFunctionNameENS0_18SharedFunctionInfoE, @function
_ZN2v88internal16ProfilerListener15GetFunctionNameENS0_18SharedFunctionInfoE:
.LFB18519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%rsi, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	48(%rdi), %eax
	testl	%eax, %eax
	je	.L144
	cmpl	$1, %eax
	jne	.L145
	leaq	-40(%rbp), %rdi
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	leaq	24(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
.L143:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L156
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movq	-40(%rbp), %rdx
	movq	15(%rdx), %rax
	testb	$1, %al
	jne	.L157
.L147:
	cmpq	%rax, _ZN2v88internal18SharedFunctionInfo21kNoSharedNameSentinelE(%rip)
	setne	%al
.L149:
	testb	%al, %al
	je	.L158
	movq	15(%rdx), %rsi
	testb	$1, %sil
	jne	.L159
.L151:
	leaq	24(%rbx), %rdi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L157:
	movq	-1(%rax), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L147
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal9ScopeInfo21HasSharedFunctionNameEv@PLT
	movq	-40(%rbp), %rdx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L158:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	movq	-37464(%rax), %rsi
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L159:
	movq	-1(%rsi), %rax
	cmpw	$136, 11(%rax)
	jne	.L151
	leaq	-32(%rbp), %r12
	movq	%rsi, -32(%rbp)
	movq	%r12, %rdi
	movq	%rsi, -48(%rbp)
	call	_ZNK2v88internal9ScopeInfo15HasFunctionNameEv@PLT
	testb	%al, %al
	je	.L153
	movq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rsi, -32(%rbp)
	call	_ZNK2v88internal9ScopeInfo12FunctionNameEv@PLT
	movq	%rax, %rsi
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L153:
	movq	-40(%rbp), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37464(%rax), %rsi
	jmp	.L151
.L145:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L156:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18519:
	.size	_ZN2v88internal16ProfilerListener15GetFunctionNameENS0_18SharedFunctionInfoE, .-_ZN2v88internal16ProfilerListener15GetFunctionNameENS0_18SharedFunctionInfoE
	.section	.text._ZN2v88internal16ProfilerListener24AttachDeoptInlinedFramesENS0_4CodeEPNS0_20CodeDeoptEventRecordE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener24AttachDeoptInlinedFramesENS0_4CodeEPNS0_20CodeDeoptEventRecordE
	.type	_ZN2v88internal16ProfilerListener24AttachDeoptInlinedFramesENS0_4CodeEPNS0_20CodeDeoptEventRecordE, @function
_ZN2v88internal16ProfilerListener24AttachDeoptInlinedFramesENS0_4CodeEPNS0_20CodeDeoptEventRecordE:
.LFB18520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$152, %rsp
	movl	24(%rdx), %r15d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 48(%rdx)
	movl	$0, 56(%rdx)
	movl	$90112, %edx
	movq	$0, -168(%rbp)
	call	_ZN2v88internal13RelocIteratorC1ENS0_4CodeEi@PLT
	cmpb	$0, -72(%rbp)
	jne	.L160
.L174:
	movzbl	-104(%rbp), %eax
	cmpb	$13, %al
	je	.L192
	cmpb	$16, %al
	je	.L193
.L163:
	movq	%r12, %rdi
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	cmpb	$0, -72(%rbp)
	je	.L174
.L160:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L194
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	cmpl	-96(%rbp), %r15d
	jne	.L163
	movq	8(%r14), %r15
	addl	$1, 41104(%r15)
	movq	8(%r14), %r14
	movq	41088(%r15), %rax
	movq	41096(%r15), %r12
	movq	41112(%r14), %rdi
	movq	%rax, -184(%rbp)
	testq	%rdi, %rdi
	je	.L165
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L166:
	leaq	-160(%rbp), %rdi
	leaq	-168(%rbp), %rsi
	call	_ZNK2v88internal14SourcePosition13InliningStackENS0_6HandleINS0_4CodeEEE@PLT
	movq	-152(%rbp), %rax
	subq	-160(%rbp), %rax
	movabsq	$576460752303423487, %rdx
	sarq	$5, %rax
	movq	%rax, %rdi
	salq	$4, %rdi
	cmpq	%rdx, %rax
	movq	$-1, %rax
	cmova	%rax, %rdi
	call	_Znam@PLT
	movq	-160(%rbp), %r10
	movq	-152(%rbp), %r9
	cmpq	%r10, %r9
	je	.L179
	movq	%r10, %rcx
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L171:
	movq	(%rcx), %rdx
	shrq	%rdx
	andl	$1073741823, %edx
	je	.L170
	movq	16(%rcx), %r11
	testq	%r11, %r11
	je	.L170
	movq	(%r11), %r11
	movslq	%edi, %rsi
	subl	$1, %edx
	addl	$1, %edi
	salq	$4, %rsi
	movslq	%edx, %rdx
	movslq	67(%r11), %r11
	addq	%rax, %rsi
	movq	%rdx, 8(%rsi)
	movl	%r11d, (%rsi)
.L170:
	addq	$32, %rcx
	cmpq	%rcx, %r9
	jne	.L171
.L169:
	movq	%rax, 48(%rbx)
	movl	%edi, 56(%rbx)
	testq	%r10, %r10
	je	.L172
	movq	%r10, %rdi
	call	_ZdlPv@PLT
.L172:
	movq	-184(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %r12
	je	.L160
	movq	%r12, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L192:
	movq	-96(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal13RelocIterator4nextEv@PLT
	movl	-184(%rbp), %eax
	movabsq	$-140735340871681, %rcx
	addl	$1, %eax
	cltq
	addq	%rax, %rax
	andq	%rcx, %rax
	movl	-96(%rbp), %ecx
	leal	1(%rcx), %edx
	movslq	%edx, %rdx
	salq	$31, %rdx
	orq	%rdx, %rax
	movq	%rax, -168(%rbp)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L165:
	movq	41088(%r14), %rdx
	cmpq	%rdx, 41096(%r14)
	je	.L195
.L167:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%r13, (%rdx)
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L179:
	xorl	%edi, %edi
	jmp	.L169
.L195:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L167
.L194:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18520:
	.size	_ZN2v88internal16ProfilerListener24AttachDeoptInlinedFramesENS0_4CodeEPNS0_20CodeDeoptEventRecordE, .-_ZN2v88internal16ProfilerListener24AttachDeoptInlinedFramesENS0_4CodeEPNS0_20CodeDeoptEventRecordE
	.section	.text._ZN2v88internal16ProfilerListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.type	_ZN2v88internal16ProfilerListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, @function
_ZN2v88internal16ProfilerListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi:
.LFB18513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 3, -48
	movq	%rsi, -120(%rbp)
	movq	%rcx, %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$4, -112(%rbp)
	call	_ZN2v88internal11Deoptimizer12GetDeoptInfoENS0_4CodeEm@PLT
	movq	%rdx, %rbx
	movq	-120(%rbp), %rdx
	movl	43(%rdx), %eax
	testl	%eax, %eax
	js	.L202
	leaq	63(%rdx), %rax
.L198:
	movl	%ebx, %edi
	movq	%rax, -104(%rbp)
	sarq	$32, %rbx
	call	_ZN2v88internal24DeoptimizeReasonToStringENS0_16DeoptimizeReasonE@PLT
	movq	%r13, -80(%rbp)
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-112(%rbp), %r13
	movq	%rax, -96(%rbp)
	movq	%r13, %rdx
	movl	%ebx, -88(%rbp)
	movl	%r14d, -72(%rbp)
	call	_ZN2v88internal16ProfilerListener24AttachDeoptInlinedFramesENS0_4CodeEPNS0_20CodeDeoptEventRecordE
	movq	16(%r12), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L203
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	leaq	-120(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L198
.L203:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18513:
	.size	_ZN2v88internal16ProfilerListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, .-_ZN2v88internal16ProfilerListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.section	.text._ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED2Ev,"axG",@progbits,_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED2Ev
	.type	_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED2Ev, @function
_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED2Ev:
.LFB20587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L205
	.p2align 4,,10
	.p2align 3
.L216:
	movq	%r12, %rax
	movq	%r12, -56(%rbp)
	movq	(%r12), %r12
	movq	8(%rax), %r13
	testq	%r13, %r13
	je	.L206
	movq	56(%r13), %r15
	testq	%r15, %r15
	je	.L207
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L208
	call	_ZdlPv@PLT
.L208:
	leaq	80(%r15), %rdi
	call	_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED1Ev
	movq	40(%r15), %r9
	testq	%r9, %r9
	jne	.L212
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r9, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r9
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L209
.L211:
	movq	%r14, %r9
.L212:
	movq	16(%r9), %rdi
	movq	(%r9), %r14
	testq	%rdi, %rdi
	jne	.L247
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L211
.L209:
	movq	32(%r15), %rax
	movq	24(%r15), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	24(%r15), %rdi
	leaq	72(%r15), %rax
	movq	$0, 48(%r15)
	movq	$0, 40(%r15)
	cmpq	%rax, %rdi
	je	.L213
	call	_ZdlPv@PLT
.L213:
	movl	$160, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L207:
	movq	40(%r13), %r14
	testq	%r14, %r14
	je	.L214
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	movq	%r14, %rdi
	call	_ZN2v88internal8MalloceddlEPv@PLT
.L214:
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L206:
	movq	-56(%rbp), %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L216
.L205:
	movq	8(%rbx), %rax
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	addq	$48, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L204
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20587:
	.size	_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED2Ev, .-_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED2Ev
	.weak	_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED1Ev
	.set	_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED1Ev,_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED2Ev
	.section	.text._ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS6_NS8_10_AllocNodeISaINS8_10_Hash_nodeIS6_Lb1EEEEEEEESt4pairINS8_14_Node_iteratorIS6_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,"axG",@progbits,_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS6_NS8_10_AllocNodeISaINS8_10_Hash_nodeIS6_Lb1EEEEEEEESt4pairINS8_14_Node_iteratorIS6_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS6_NS8_10_AllocNodeISaINS8_10_Hash_nodeIS6_Lb1EEEEEEEESt4pairINS8_14_Node_iteratorIS6_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.type	_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS6_NS8_10_AllocNodeISaINS8_10_Hash_nodeIS6_Lb1EEEEEEEESt4pairINS8_14_Node_iteratorIS6_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, @function
_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS6_NS8_10_AllocNodeISaINS8_10_Hash_nodeIS6_Lb1EEEEEEEESt4pairINS8_14_Node_iteratorIS6_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm:
.LFB22609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rsi), %rdi
	call	_ZNK2v88internal9CodeEntry7GetHashEv@PLT
	xorl	%edx, %edx
	movl	%eax, %eax
	movq	%rax, -56(%rbp)
	divq	8(%rbx)
	leaq	0(,%rdx,8), %rax
	movq	%rax, -64(%rbp)
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %r15
	testq	%r15, %r15
	je	.L249
	movq	%rdx, %r10
	movq	(%r15), %rdx
	movq	16(%rdx), %rcx
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L289:
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L249
.L287:
	movq	16(%rsi), %rcx
	movq	%rdx, %r15
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	8(%rbx)
	cmpq	%rdx, %r10
	jne	.L249
	movq	%rsi, %rdx
.L254:
	cmpq	%rcx, -56(%rbp)
	jne	.L289
	movq	8(%rdx), %rsi
	movq	(%r12), %rdi
	movq	%r10, -80(%rbp)
	movq	%rdx, -72(%rbp)
	call	_ZNK2v88internal9CodeEntry16IsSameFunctionAsEPKS1_@PLT
	testb	%al, %al
	jne	.L252
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %r10
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.L287
	.p2align 4,,10
	.p2align 3
.L249:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	32(%rbx), %rdi
	movq	%r13, %rcx
	movq	%rax, %r9
	movq	$0, (%rax)
	movq	(%r12), %rax
	movq	$0, (%r12)
	movq	24(%rbx), %rdx
	movq	%rax, 8(%r9)
	movq	8(%rbx), %rsi
	movq	%r9, -72(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-72(%rbp), %r9
	testb	%al, %al
	movq	%rdx, %r12
	jne	.L256
	movq	(%rbx), %r13
.L257:
	movq	-56(%rbp), %rax
	movq	%rax, 16(%r9)
	movq	-64(%rbp), %rax
	addq	%r13, %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L266
	movq	(%rdx), %rdx
	movq	%rdx, (%r9)
	movq	(%rax), %rax
	movq	%r9, (%rax)
.L267:
	addq	$1, 24(%rbx)
	movq	%r9, %rax
	movb	$1, %r14b
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L252:
	movq	(%r15), %rax
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.L249
.L281:
	addq	$40, %rsp
	movq	%r14, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L290
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L291
	leaq	0(,%rdx,8), %r15
	movq	%r9, -64(%rbp)
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	movq	-64(%rbp), %r9
	leaq	48(%rbx), %r11
.L259:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L261
	xorl	%edi, %edi
	leaq	16(%rbx), %r10
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L263:
	movq	(%r15), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L264:
	testq	%rsi, %rsi
	je	.L261
.L262:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	16(%rcx), %rax
	divq	%r12
	leaq	0(%r13,%rdx,8), %rax
	movq	(%rax), %r15
	testq	%r15, %r15
	jne	.L263
	movq	16(%rbx), %r15
	movq	%r15, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L270
	movq	%rcx, 0(%r13,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L262
	.p2align 4,,10
	.p2align 3
.L261:
	movq	(%rbx), %rdi
	cmpq	%r11, %rdi
	je	.L265
	movq	%r9, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r9
.L265:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r13, (%rbx)
	divq	%r12
	leaq	0(,%rdx,8), %rax
	movq	%rax, -64(%rbp)
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L266:
	movq	16(%rbx), %rdx
	movq	%r9, 16(%rbx)
	movq	%rdx, (%r9)
	testq	%rdx, %rdx
	je	.L268
	movq	16(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	-64(%rbp), %rax
	movq	%r9, 0(%r13,%rdx,8)
	addq	(%rbx), %rax
.L268:
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax)
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L270:
	movq	%rdx, %rdi
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L290:
	leaq	48(%rbx), %r13
	movq	$0, 48(%rbx)
	movq	%r13, %r11
	jmp	.L259
.L291:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE22609:
	.size	_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS6_NS8_10_AllocNodeISaINS8_10_Hash_nodeIS6_Lb1EEEEEEEESt4pairINS8_14_Node_iteratorIS6_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm, .-_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS6_NS8_10_AllocNodeISaINS8_10_Hash_nodeIS6_Lb1EEEEEEEESt4pairINS8_14_Node_iteratorIS6_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	.section	.rodata._ZNSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB22664:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%r9, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L306
	movq	%rdx, %r13
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L302
	movabsq	$9223372036854775792, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L307
.L294:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L301:
	movl	8(%r13), %ecx
	movq	0(%r13), %rsi
	leaq	(%rbx,%rdx), %rax
	subq	%r8, %r9
	leaq	16(%rbx,%rdx), %r10
	movq	%r9, %r13
	movq	%rsi, (%rax)
	movl	%ecx, 8(%rax)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L308
	testq	%r9, %r9
	jg	.L297
	testq	%r15, %r15
	jne	.L300
.L298:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L297
.L300:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L307:
	testq	%rsi, %rsi
	jne	.L295
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L298
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$16, %r14d
	jmp	.L294
.L306:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L295:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$4, %r14
	jmp	.L294
	.cfi_endproc
.LFE22664:
	.size	_ZNSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm:
.LFB23017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L310
	movq	(%rbx), %r8
.L311:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L320
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L321:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L334
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L335
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L313:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L315
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L317:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L318:
	testq	%rsi, %rsi
	je	.L315
.L316:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L317
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L323
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L316
	.p2align 4,,10
	.p2align 3
.L315:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L319
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L319:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L320:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L322
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L322:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%rdx, %rdi
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L334:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L313
.L335:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE23017:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiS7_EEES0_INSA_14_Node_iteratorIS8_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiS7_EEES0_INSA_14_Node_iteratorIS8_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiS7_EEES0_INSA_14_Node_iteratorIS8_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiS7_EEES0_INSA_14_Node_iteratorIS8_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiS7_EEES0_INSA_14_Node_iteratorIS8_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB22667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$40, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	call	_Znwm@PLT
	movslq	(%r14), %r10
	movdqu	(%rbx), %xmm1
	pxor	%xmm0, %xmm0
	movq	$0, (%rax)
	movq	%rax, %r12
	movq	(%rbx), %r8
	xorl	%edx, %edx
	movl	%r10d, 8(%rax)
	movups	%xmm1, 16(%rax)
	movq	16(%rbx), %rax
	movups	%xmm0, (%rbx)
	movq	$0, 16(%rbx)
	movq	8(%r13), %rcx
	movq	%rax, 32(%r12)
	movq	%r10, %rax
	divq	%rcx
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L337
	movq	(%rax), %rbx
	movq	%r10, %rsi
	movl	8(%rbx), %edi
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L351:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L337
	movslq	8(%rbx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L337
.L339:
	cmpl	%edi, %esi
	jne	.L351
	testq	%r8, %r8
	je	.L342
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L342:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movq	%r12, %rcx
	movq	%r10, %rdx
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	%r9, %rsi
	call	_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22667:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiS7_EEES0_INSA_14_Node_iteratorIS8_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiS7_EEES0_INSA_14_Node_iteratorIS8_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.type	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii, @function
_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii:
.LFB18403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$664, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -508(%rbp)
	movq	%rcx, -504(%rbp)
	movq	%r8, -616(%rbp)
	movl	%r9d, -620(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	53(%r14), %rax
	movl	$1, -192(%rbp)
	movq	-1(%rdx), %rdx
	cmpw	$69, 11(%rdx)
	je	.L517
.L356:
	movq	%rax, -184(%rbp)
	leaq	24(%r15), %rcx
	leaq	-336(%rbp), %rax
	movss	.LC5(%rip), %xmm0
	movq	%rax, -592(%rbp)
	movq	%rax, -384(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, -632(%rbp)
	movq	%rax, -320(%rbp)
	movq	-504(%rbp), %rax
	movq	$1, -376(%rbp)
	movq	31(%rax), %rax
	movq	$0, -368(%rbp)
	movq	$0, -360(%rbp)
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	movq	$1, -312(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	movq	$0, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rcx, -600(%rbp)
	movss	%xmm0, -352(%rbp)
	movss	%xmm0, -288(%rbp)
	testb	$1, %al
	jne	.L357
.L360:
	leaq	-320(%rbp), %rax
	movq	$0, -536(%rbp)
	movq	%rax, -584(%rbp)
.L358:
	movq	-504(%rbp), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal16ProfilerListener15GetFunctionNameENS0_18SharedFunctionInfoE
	movq	-616(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movq	%rax, -528(%rbp)
	leaq	53(%r14), %rbx
	call	_ZN2v88internal16ProfilerListener15InferScriptNameENS0_4NameENS0_18SharedFunctionInfoE
	movq	-600(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movq	%rax, -544(%rbp)
	movq	-1(%r14), %rax
	cmpw	$69, 11(%rax)
	je	.L518
.L416:
	movq	-528(%rbp), %xmm0
	movl	$64, %edi
	movhps	-544(%rbp), %xmm0
	movaps	%xmm0, -528(%rbp)
	call	_Znwm@PLT
	movl	-508(%rbp), %esi
	movdqa	-528(%rbp), %xmm0
	movq	%rax, %rdi
	orl	$397568, %esi
	movups	%xmm0, 8(%rax)
	movl	%esi, (%rax)
	movl	-620(%rbp), %eax
	movq	$0, 32(%rdi)
	movl	%eax, 24(%rdi)
	movl	16(%rbp), %eax
	movq	%rbx, 48(%rdi)
	movl	%eax, 28(%rdi)
	movq	-536(%rbp), %rax
	movq	$0, 56(%rdi)
	movq	%rax, 40(%rdi)
	movq	-360(%rbp), %rax
	movq	%rdi, -176(%rbp)
	testq	%rax, %rax
	jne	.L519
.L417:
	movq	-504(%rbp), %rsi
	call	_ZN2v88internal9CodeEntry16FillFunctionInfoENS0_18SharedFunctionInfoE@PLT
	movq	-1(%r14), %rax
	cmpw	$69, 11(%rax)
	je	.L520
	movl	11(%r14), %eax
.L431:
	movq	16(%r15), %rdi
	movl	%eax, -168(%rbp)
	leaq	-192(%rbp), %rsi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	-584(%rbp), %rdi
	call	_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED1Ev
	movq	-368(%rbp), %r12
	testq	%r12, %r12
	jne	.L432
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L521:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L436
.L437:
	movq	%rbx, %r12
.L432:
	movq	16(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	jne	.L521
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L437
.L436:
	movq	-376(%rbp), %rax
	movq	-384(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-384(%rbp), %rdi
	movq	$0, -360(%rbp)
	movq	$0, -368(%rbp)
	cmpq	-592(%rbp), %rdi
	je	.L352
	call	_ZdlPv@PLT
.L352:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L522
	addq	$664, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$86, 11(%rcx)
	je	.L523
.L359:
	movq	(%rdx), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L360
	testb	$1, %al
	jne	.L524
.L361:
	movl	$24, %edi
	movq	%rax, -488(%rbp)
	call	_ZN2v88internal8MallocednwEm@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	%rax, -536(%rbp)
	movq	$0, 16(%rax)
	movq	8(%r15), %rax
	movq	41088(%rax), %rcx
	addl	$1, 41104(%rax)
	movq	%rax, -680(%rbp)
	movq	%rcx, -696(%rbp)
	movq	41096(%rax), %rcx
	movq	-488(%rbp), %rax
	movq	%rcx, -688(%rbp)
	movslq	99(%rax), %rax
	sarl	$2, %eax
	movl	%eax, -624(%rbp)
	movq	-1(%r14), %rax
	cmpw	$69, 11(%rax)
	je	.L525
	movq	31(%r14), %rsi
	testb	$1, %sil
	jne	.L367
.L371:
	movq	%r14, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %rdx
	cmpq	-37280(%rax), %rsi
	je	.L526
.L503:
	movq	7(%rsi), %rsi
.L366:
	leaq	-128(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE@PLT
	leaq	-320(%rbp), %rax
	cmpl	$-1, -104(%rbp)
	movq	%rax, -584(%rbp)
	leaq	-448(%rbp), %rax
	movq	%rax, -672(%rbp)
	je	.L412
	movq	%r15, -608(%rbp)
	movq	%r14, -664(%rbp)
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L528:
	shrq	%rsi
	leaq	-488(%rbp), %rdi
	andl	$1073741823, %esi
	subl	$1, %esi
	call	_ZNK2v88internal6Script13GetLineNumberEi@PLT
	movl	-492(%rbp), %ecx
	movl	-96(%rbp), %esi
	movq	-536(%rbp), %rdi
	leal	1(%rax), %edx
	call	_ZN2v88internal19SourcePositionTable11SetPositionEiii@PLT
.L376:
	movq	-648(%rbp), %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -104(%rbp)
	je	.L527
.L372:
	movq	-88(%rbp), %rsi
	movq	%rsi, %rax
	shrq	$31, %rax
	movzwl	%ax, %eax
	subl	$1, %eax
	movl	%eax, -492(%rbp)
	cmpl	$-1, %eax
	je	.L528
	movq	-608(%rbp), %rcx
	movq	%rsi, -256(%rbp)
	movq	-664(%rbp), %rax
	movq	8(%rcx), %rbx
	movq	%rax, -480(%rbp)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L377
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L378:
	movq	-672(%rbp), %rdi
	leaq	-256(%rbp), %r14
	leaq	-464(%rbp), %r15
	movq	%r14, %rsi
	call	_ZNK2v88internal14SourcePosition13InliningStackENS0_6HandleINS0_4CodeEEE@PLT
	movq	-448(%rbp), %rax
	movl	-96(%rbp), %esi
	movl	-492(%rbp), %ecx
	movq	-536(%rbp), %rdi
	movl	24(%rax), %edx
	addl	$1, %edx
	call	_ZN2v88internal19SourcePositionTable11SetPositionEiii@PLT
	leaq	-416(%rbp), %rcx
	movq	-440(%rbp), %rax
	movq	-448(%rbp), %r13
	movq	%rcx, -656(%rbp)
	pxor	%xmm0, %xmm0
	leaq	-472(%rbp), %rcx
	movq	$0, -400(%rbp)
	movq	%rax, -528(%rbp)
	movq	%rcx, -640(%rbp)
	movaps	%xmm0, -416(%rbp)
	cmpq	%r13, %rax
	je	.L410
	.p2align 4,,10
	.p2align 3
.L409:
	testl	$2147483646, 0(%r13)
	je	.L408
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.L408
	movq	(%rax), %rax
	movq	%r14, %rdi
	movq	%rax, -256(%rbp)
	movq	0(%r13), %rsi
	shrq	%rsi
	andl	$1073741823, %esi
	subl	$1, %esi
	call	_ZNK2v88internal6Script13GetLineNumberEi@PLT
	addl	$1, %eax
	movl	%eax, -512(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rsi
	testb	$1, %sil
	jne	.L529
.L384:
	movq	_ZN2v88internal9CodeEntry18kEmptyResourceNameE(%rip), %rcx
	movq	%rcx, -576(%rbp)
.L386:
	movslq	99(%rax), %rbx
	movq	8(%r13), %rax
	movq	%r15, %rdi
	movq	(%rax), %rax
	sarl	$2, %ebx
	movq	%rax, -464(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movq	8(%r13), %rdx
	movq	%r14, %rdi
	leal	1(%rax), %esi
	movabsq	$-140735340871681, %rax
	movslq	%esi, %rsi
	addq	%rsi, %rsi
	andq	%rax, %rsi
	call	_ZN2v88internal18SourcePositionInfoC1ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	-480(%rbp), %rax
	movl	43(%rax), %ecx
	leaq	63(%rax), %r9
	testl	%ecx, %ecx
	js	.L530
.L388:
	movl	-228(%rbp), %eax
	movq	-608(%rbp), %rdi
	movq	%r9, -568(%rbp)
	sall	$31, %ebx
	leal	1(%rax), %edx
	movl	-232(%rbp), %eax
	movl	%edx, -560(%rbp)
	leal	1(%rax), %ecx
	movq	8(%r13), %rax
	movl	%ecx, -552(%rbp)
	movq	(%rax), %rsi
	call	_ZN2v88internal16ProfilerListener15GetFunctionNameENS0_18SharedFunctionInfoE
	movl	$64, %edi
	movq	%rax, -544(%rbp)
	call	_Znwm@PLT
	movl	-560(%rbp), %edx
	movq	-544(%rbp), %xmm0
	movl	-552(%rbp), %ecx
	movq	-568(%rbp), %r9
	movq	%rax, %r12
	movq	$0, 32(%rax)
	orl	-508(%rbp), %ebx
	movl	%edx, 28(%rax)
	movhps	-576(%rbp), %xmm0
	movq	%r12, %rdi
	orl	$397568, %ebx
	movl	%ecx, 24(%rax)
	movl	%ebx, (%rax)
	movq	%r9, 48(%rax)
	movups	%xmm0, 8(%rax)
	movq	$0, 40(%rax)
	movq	$0, 56(%rax)
	movq	8(%r13), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal9CodeEntry16FillFunctionInfoENS0_18SharedFunctionInfoE@PLT
	movq	%r12, %rdi
	movq	%r12, -472(%rbp)
	call	_ZNK2v88internal9CodeEntry7GetHashEv@PLT
	xorl	%edx, %edx
	movl	%eax, %ebx
	movq	%rbx, %rax
	divq	-312(%rbp)
	movq	-320(%rbp), %rax
	movq	(%rax,%rdx,8), %r10
	movq	%rdx, %r11
	testq	%r10, %r10
	je	.L389
	movq	(%r10), %rdx
	movq	-472(%rbp), %r12
	movq	%r13, -544(%rbp)
	movq	%r10, %r13
	movq	%r14, -552(%rbp)
	movq	%r11, %r14
	movq	%r12, %rdi
	movq	16(%rdx), %rcx
	movq	%rbx, %r12
	movq	%rdx, %rbx
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L390:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L510
.L531:
	movq	16(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rbx, %r13
	movq	%rcx, %rax
	divq	-312(%rbp)
	cmpq	%rdx, %r14
	jne	.L510
	movq	%rsi, %rbx
.L393:
	cmpq	%r12, %rcx
	jne	.L390
	movq	8(%rbx), %rsi
	call	_ZNK2v88internal9CodeEntry16IsSameFunctionAsEPKS1_@PLT
	testb	%al, %al
	jne	.L391
	movq	(%rbx), %rsi
	movq	-472(%rbp), %rdi
	testq	%rsi, %rsi
	jne	.L531
	.p2align 4,,10
	.p2align 3
.L510:
	movq	-544(%rbp), %r13
	movq	-552(%rbp), %r14
	movq	%rdi, %r12
.L392:
	movq	-584(%rbp), %rax
	movq	-640(%rbp), %rsi
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, -464(%rbp)
	call	_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEE9_M_insertIS6_NS8_10_AllocNodeISaINS8_10_Hash_nodeIS6_Lb1EEEEEEEESt4pairINS8_14_Node_iteratorIS6_Lb1ELb1EEEbEOT_RKT0_St17integral_constantIbLb1EEm
	movq	-472(%rbp), %r10
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L519:
	movq	-384(%rbp), %rcx
	movq	%rax, -104(%rbp)
	leaq	-80(%rbp), %rbx
	movq	-376(%rbp), %rsi
	movq	-368(%rbp), %rdx
	movq	$0, -80(%rbp)
	movdqa	-352(%rbp), %xmm1
	movq	%rcx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movaps	%xmm1, -96(%rbp)
	cmpq	-592(%rbp), %rcx
	je	.L532
.L418:
	testq	%rdx, %rdx
	je	.L419
	movslq	8(%rdx), %rax
	xorl	%edx, %edx
	divq	%rsi
	leaq	-112(%rbp), %rax
	movq	%rax, (%rcx,%rdx,8)
.L419:
	movq	-592(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	$0, -344(%rbp)
	movq	-312(%rbp), %rsi
	movq	-296(%rbp), %rdx
	movq	$1, -376(%rbp)
	movq	%rax, -384(%rbp)
	movdqa	-288(%rbp), %xmm2
	movq	-304(%rbp), %rax
	movq	$0, -336(%rbp)
	movq	$0, -368(%rbp)
	movq	$0, -360(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rsi, -248(%rbp)
	movq	%rax, -240(%rbp)
	movq	%rdx, -232(%rbp)
	movq	$0, -208(%rbp)
	movaps	%xmm2, -224(%rbp)
	cmpq	-632(%rbp), %rcx
	je	.L533
.L420:
	testq	%rax, %rax
	je	.L421
	movq	16(%rax), %rax
	xorl	%edx, %edx
	divq	%rsi
	leaq	-240(%rbp), %rax
	movq	%rax, (%rcx,%rdx,8)
.L421:
	movq	-632(%rbp), %rax
	leaq	-128(%rbp), %rdx
	leaq	-256(%rbp), %r12
	movq	$0, -280(%rbp)
	movq	$1, -312(%rbp)
	movq	%r12, %rsi
	movq	%rax, -320(%rbp)
	movq	$0, -272(%rbp)
	movq	$0, -304(%rbp)
	movq	$0, -296(%rbp)
	call	_ZN2v88internal9CodeEntry15SetInlineStacksESt13unordered_setISt10unique_ptrIS1_St14default_deleteIS1_EENS1_6HasherENS1_6EqualsESaIS6_EESt13unordered_mapIiSt6vectorINS0_22CodeEntryAndLineNumberESaISD_EESt4hashIiESt8equal_toIiESaISt4pairIKiSF_EEE@PLT
	movq	%r12, %rdi
	call	_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED1Ev
	movq	-112(%rbp), %r12
	testq	%r12, %r12
	jne	.L422
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L534:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L426
.L427:
	movq	%r13, %r12
.L422:
	movq	16(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	jne	.L534
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L427
.L426:
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-128(%rbp), %rdi
	movq	$0, -104(%rbp)
	movq	$0, -112(%rbp)
	cmpq	%rbx, %rdi
	je	.L423
	call	_ZdlPv@PLT
.L423:
	movq	-176(%rbp), %rdi
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L517:
	movl	43(%r14), %esi
	movq	%r14, -128(%rbp)
	leaq	63(%r14), %rax
	testl	%esi, %esi
	jns	.L356
	leaq	-128(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L518:
	movl	43(%r14), %edx
	movq	%r14, -128(%rbp)
	leaq	63(%r14), %rbx
	testl	%edx, %edx
	jns	.L416
	leaq	-128(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rbx
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L520:
	movl	43(%r14), %eax
	movq	%r14, -128(%rbp)
	testl	%eax, %eax
	js	.L535
	movl	39(%r14), %eax
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L535:
	leaq	-128(%rbp), %rdi
	call	_ZNK2v88internal4Code22OffHeapInstructionSizeEv@PLT
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L391:
	movq	%r13, %r10
	movq	-552(%rbp), %r14
	movq	-544(%rbp), %r13
	movq	(%r10), %rax
	movq	-472(%rbp), %r10
	movq	%r10, %r12
	testq	%rax, %rax
	je	.L392
	movq	8(%rax), %r12
.L394:
	testq	%r10, %r10
	je	.L395
	movq	56(%r10), %rbx
	testq	%rbx, %rbx
	je	.L396
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L397
	movq	%r10, -544(%rbp)
	call	_ZdlPv@PLT
	movq	-544(%rbp), %r10
.L397:
	leaq	80(%rbx), %rdi
	movq	%r10, -544(%rbp)
	call	_ZNSt10_HashtableISt10unique_ptrIN2v88internal9CodeEntryESt14default_deleteIS3_EES6_SaIS6_ENSt8__detail9_IdentityENS3_6EqualsENS3_6HasherENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb1ELb1ELb1EEEED1Ev
	movq	40(%rbx), %r11
	movq	-544(%rbp), %r10
	testq	%r11, %r11
	je	.L402
	movq	%r12, -544(%rbp)
	movq	%r11, %r12
	movq	%rbx, -552(%rbp)
	movq	%r10, -560(%rbp)
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L536:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L512
.L403:
	movq	%rbx, %r12
.L398:
	movq	16(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	jne	.L536
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L403
.L512:
	movq	-544(%rbp), %r12
	movq	-552(%rbp), %rbx
	movq	-560(%rbp), %r10
.L402:
	movq	32(%rbx), %rax
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r10, -544(%rbp)
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	24(%rbx), %rdi
	leaq	72(%rbx), %rax
	movq	$0, 48(%rbx)
	movq	$0, 40(%rbx)
	movq	-544(%rbp), %r10
	cmpq	%rax, %rdi
	je	.L399
	movq	%r10, -544(%rbp)
	call	_ZdlPv@PLT
	movq	-544(%rbp), %r10
.L399:
	movl	$160, %esi
	movq	%rbx, %rdi
	movq	%r10, -544(%rbp)
	call	_ZdlPvm@PLT
	movq	-544(%rbp), %r10
.L396:
	movq	40(%r10), %rbx
	testq	%rbx, %rbx
	je	.L404
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L405
	movq	%r10, -544(%rbp)
	call	_ZdlPv@PLT
	movq	-544(%rbp), %r10
.L405:
	movq	%rbx, %rdi
	movq	%r10, -544(%rbp)
	call	_ZN2v88internal8MalloceddlEPv@PLT
	movq	-544(%rbp), %r10
.L404:
	movl	$64, %esi
	movq	%r10, %rdi
	call	_ZdlPvm@PLT
.L395:
	movl	-512(%rbp), %eax
	movq	%r12, -464(%rbp)
	movq	-408(%rbp), %rsi
	movl	%eax, -456(%rbp)
	cmpq	-400(%rbp), %rsi
	je	.L406
	movq	%r12, (%rsi)
	movl	%eax, 8(%rsi)
	addq	$16, -408(%rbp)
.L408:
	addq	$32, %r13
	cmpq	%r13, -528(%rbp)
	jne	.L409
.L410:
	movq	-656(%rbp), %rdx
	leaq	-384(%rbp), %rdi
	leaq	-492(%rbp), %rsi
	call	_ZNSt10_HashtableIiSt4pairIKiSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS5_EEESaIS8_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiS7_EEES0_INSA_14_Node_iteratorIS8_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	movq	-416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L382
	call	_ZdlPv@PLT
.L382:
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L376
	call	_ZdlPv@PLT
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L529:
	movq	-1(%rsi), %rdx
	cmpw	$64, 11(%rdx)
	ja	.L384
	movq	-600(%rbp), %rdi
	call	_ZN2v88internal14StringsStorage7GetNameENS0_4NameE@PLT
	movq	%rax, -576(%rbp)
	movq	16(%r13), %rax
	movq	(%rax), %rax
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L530:
	leaq	-480(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %r9
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L406:
	movq	-656(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorIN2v88internal22CodeEntryAndLineNumberESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L377:
	movq	41088(%rbx), %rdx
	cmpq	%rdx, 41096(%rbx)
	je	.L537
.L379:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	-664(%rbp), %rax
	movq	%rax, (%rdx)
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L389:
	movq	-472(%rbp), %r12
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L527:
	movq	-608(%rbp), %r15
	movq	-664(%rbp), %r14
.L412:
	movq	-680(%rbp), %rax
	movq	-696(%rbp), %rcx
	subl	$1, 41104(%rax)
	movq	%rcx, 41088(%rax)
	movq	-688(%rbp), %rcx
	cmpq	41096(%rax), %rcx
	je	.L374
	movq	%rcx, 41096(%rax)
	movq	%rax, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L374:
	movl	-624(%rbp), %eax
	sall	$31, %eax
	orl	%eax, -508(%rbp)
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L523:
	movq	23(%rax), %rdx
	testb	$1, %dl
	je	.L360
	subq	$1, %rdx
	jmp	.L359
.L367:
	movq	-1(%rsi), %rax
	cmpw	$70, 11(%rax)
	jne	.L371
	jmp	.L366
.L526:
	movq	976(%rdx), %rsi
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L525:
	movq	23(%r14), %rsi
	testb	$1, %sil
	je	.L503
	movq	-1(%rsi), %rax
	cmpw	$70, 11(%rax)
	jne	.L503
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L524:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L361
	movq	23(%rax), %rax
	jmp	.L361
.L533:
	movq	-272(%rbp), %rdx
	leaq	-208(%rbp), %rcx
	movq	%rcx, -256(%rbp)
	movq	%rdx, -208(%rbp)
	jmp	.L420
.L532:
	movq	-336(%rbp), %rax
	movq	%rbx, -128(%rbp)
	movq	%rbx, %rcx
	movq	%rax, -80(%rbp)
	jmp	.L418
.L537:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L379
.L522:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18403:
	.size	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii, .-_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal16ProfilerListenerC2EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal16ProfilerListenerC2EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE, @function
_GLOBAL__sub_I__ZN2v88internal16ProfilerListenerC2EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE:
.LFB23552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23552:
	.size	_GLOBAL__sub_I__ZN2v88internal16ProfilerListenerC2EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE, .-_GLOBAL__sub_I__ZN2v88internal16ProfilerListenerC2EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal16ProfilerListenerC2EPNS0_7IsolateEPNS0_17CodeEventObserverENS_22CpuProfilingNamingModeE
	.weak	_ZTVN2v88internal16ProfilerListenerE
	.section	.data.rel.ro.local._ZTVN2v88internal16ProfilerListenerE,"awG",@progbits,_ZTVN2v88internal16ProfilerListenerE,comdat
	.align 8
	.type	_ZTVN2v88internal16ProfilerListenerE, @object
	.size	_ZTVN2v88internal16ProfilerListenerE, 160
_ZTVN2v88internal16ProfilerListenerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal16ProfilerListenerD1Ev
	.quad	_ZN2v88internal16ProfilerListenerD0Ev
	.quad	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.quad	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.quad	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.quad	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.quad	_ZN2v88internal16ProfilerListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.quad	_ZN2v88internal16ProfilerListener13CallbackEventENS0_4NameEm
	.quad	_ZN2v88internal16ProfilerListener19GetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal16ProfilerListener19SetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal16ProfilerListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.quad	_ZN2v88internal16ProfilerListener13CodeMoveEventENS0_12AbstractCodeES2_
	.quad	_ZN2v88internal16ProfilerListener27SharedFunctionInfoMoveEventEmm
	.quad	_ZN2v88internal16ProfilerListener22NativeContextMoveEventEmm
	.quad	_ZN2v88internal16ProfilerListener17CodeMovingGCEventEv
	.quad	_ZN2v88internal16ProfilerListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.quad	_ZN2v88internal16ProfilerListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.quad	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC5:
	.long	1065353216
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
