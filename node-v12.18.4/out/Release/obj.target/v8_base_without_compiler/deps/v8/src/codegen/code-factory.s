	.file	"code-factory.cc"
	.text
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD2Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD2Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD2Ev:
.LFB8827:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8827:
	.size	_ZN2v88internal23CallInterfaceDescriptorD2Ev, .-_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.weak	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.set	_ZN2v88internal23CallInterfaceDescriptorD1Ev,_ZN2v88internal23CallInterfaceDescriptorD2Ev
	.section	.rodata._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE:
.LFB8820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE8820:
	.size	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.type	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, @function
_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE:
.LFB8821:
	.cfi_startproc
	endbr64
	movl	(%rsi), %ecx
	movq	%rsi, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_ZN2v88internal27CallInterfaceDescriptorData29InitializePlatformIndependentENS_4base5FlagsINS1_4FlagEiEEiiPKNS0_11MachineTypeEi@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE, .-_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.text._ZN2v88internal23CallInterfaceDescriptorD0Ev,"axG",@progbits,_ZN2v88internal23CallInterfaceDescriptorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.type	_ZN2v88internal23CallInterfaceDescriptorD0Ev, @function
_ZN2v88internal23CallInterfaceDescriptorD0Ev:
.LFB8829:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8829:
	.size	_ZN2v88internal23CallInterfaceDescriptorD0Ev, .-_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.section	.text._ZN2v88internal11CodeFactory13RuntimeCEntryEPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory13RuntimeCEntryEPNS0_7IsolateEi
	.type	_ZN2v88internal11CodeFactory13RuntimeCEntryEPNS0_7IsolateEi, @function
_ZN2v88internal11CodeFactory13RuntimeCEntryEPNS0_7IsolateEi:
.LFB20595:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	jne	.L8
	addq	$41184, %rdi
	movl	$695, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$2, %esi
	je	.L12
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	addq	$41184, %rdi
	movl	$700, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.cfi_endproc
.LFE20595:
	.size	_ZN2v88internal11CodeFactory13RuntimeCEntryEPNS0_7IsolateEi, .-_ZN2v88internal11CodeFactory13RuntimeCEntryEPNS0_7IsolateEi
	.section	.text._ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb
	.type	_ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb, @function
_ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb:
.LFB20596:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	sete	%al
	testl	%edx, %edx
	sete	%r9b
	testb	%al, %al
	je	.L14
	testb	%r9b, %r9b
	je	.L14
	testl	%ecx, %ecx
	movl	%r8d, %eax
	sete	%dl
	xorl	$1, %eax
	testb	%dl, %dl
	je	.L16
	testb	%al, %al
	jne	.L79
	testb	%r8b, %r8b
	jne	.L80
.L16:
	cmpl	$1, %ecx
	jne	.L17
	testb	%al, %al
	je	.L17
	addq	$41184, %rdi
	movl	$697, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	$1, %edx
	sete	%dl
	testb	%al, %al
	je	.L18
	testb	%dl, %dl
	je	.L18
	testl	%ecx, %ecx
	sete	%al
	cmpb	$1, %r8b
	je	.L19
	testb	%al, %al
	jne	.L81
.L17:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	cmpl	$2, %esi
	sete	%al
	testb	%r9b, %r9b
	je	.L22
	testb	%al, %al
	je	.L22
	testl	%ecx, %ecx
	movl	%r8d, %eax
	sete	%dl
	xorl	$1, %eax
	testb	%dl, %dl
	je	.L24
	testb	%al, %al
	je	.L23
	addq	$41184, %rdi
	movl	$700, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.p2align 4,,10
	.p2align 3
.L79:
	addq	$41184, %rdi
	movl	$695, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	addq	$41184, %rdi
	movl	$696, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	testb	%al, %al
	je	.L17
	testb	%r8b, %r8b
	je	.L17
	addq	$41184, %rdi
	movl	$699, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	cmpl	$1, %ecx
	jne	.L17
	testb	%al, %al
	je	.L17
	addq	$41184, %rdi
	movl	$702, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	testb	%r8b, %r8b
	je	.L24
	addq	$41184, %rdi
	movl	$701, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$41184, %rdi
	movl	$698, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
.L22:
	testb	%dl, %dl
	je	.L17
	testb	%al, %al
	je	.L17
	testl	%ecx, %ecx
	sete	%al
	cmpb	$1, %r8b
	je	.L25
	testb	%al, %al
	je	.L17
	addq	$41184, %rdi
	movl	$703, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
.L25:
	testb	%al, %al
	je	.L17
	testb	%r8b, %r8b
	je	.L17
	addq	$41184, %rdi
	movl	$704, %esi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.cfi_endproc
.LFE20596:
	.size	_ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb, .-_ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb
	.section	.text._ZN2v88internal11CodeFactory9ApiGetterEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory9ApiGetterEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory9ApiGetterEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory9ApiGetterEPNS0_7IsolateE:
.LFB20597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$76, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L85:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20597:
	.size	_ZN2v88internal11CodeFactory9ApiGetterEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory9ApiGetterEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory15CallApiCallbackEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory15CallApiCallbackEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory15CallApiCallbackEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory15CallApiCallbackEPNS0_7IsolateE:
.LFB20604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$75, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L89:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20604:
	.size	_ZN2v88internal11CodeFactory15CallApiCallbackEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory15CallApiCallbackEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory12LoadGlobalICEPNS0_7IsolateENS0_10TypeofModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory12LoadGlobalICEPNS0_7IsolateENS0_10TypeofModeE
	.type	_ZN2v88internal11CodeFactory12LoadGlobalICEPNS0_7IsolateENS0_10TypeofModeE, @function
_ZN2v88internal11CodeFactory12LoadGlobalICEPNS0_7IsolateENS0_10TypeofModeE:
.LFB20605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %edx
	je	.L95
	movl	$387, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
.L90:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movl	$386, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L90
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20605:
	.size	_ZN2v88internal11CodeFactory12LoadGlobalICEPNS0_7IsolateENS0_10TypeofModeE, .-_ZN2v88internal11CodeFactory12LoadGlobalICEPNS0_7IsolateENS0_10TypeofModeE
	.section	.text._ZN2v88internal11CodeFactory27LoadGlobalICInOptimizedCodeEPNS0_7IsolateENS0_10TypeofModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory27LoadGlobalICInOptimizedCodeEPNS0_7IsolateENS0_10TypeofModeE
	.type	_ZN2v88internal11CodeFactory27LoadGlobalICInOptimizedCodeEPNS0_7IsolateENS0_10TypeofModeE, @function
_ZN2v88internal11CodeFactory27LoadGlobalICInOptimizedCodeEPNS0_7IsolateENS0_10TypeofModeE:
.LFB20606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %edx
	je	.L102
	movl	$385, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
.L97:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movl	$384, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L97
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20606:
	.size	_ZN2v88internal11CodeFactory27LoadGlobalICInOptimizedCodeEPNS0_7IsolateENS0_10TypeofModeE, .-_ZN2v88internal11CodeFactory27LoadGlobalICInOptimizedCodeEPNS0_7IsolateENS0_10TypeofModeE
	.section	.text._ZN2v88internal11CodeFactory10StoreOwnICEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory10StoreOwnICEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory10StoreOwnICEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory10StoreOwnICEPNS0_7IsolateE:
.LFB20607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$380, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L107:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20607:
	.size	_ZN2v88internal11CodeFactory10StoreOwnICEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory10StoreOwnICEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory25StoreOwnICInOptimizedCodeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory25StoreOwnICInOptimizedCodeEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory25StoreOwnICInOptimizedCodeEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory25StoreOwnICInOptimizedCodeEPNS0_7IsolateE:
.LFB20608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$379, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L111:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20608:
	.size	_ZN2v88internal11CodeFactory25StoreOwnICInOptimizedCodeEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory25StoreOwnICInOptimizedCodeEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory28KeyedStoreIC_SloppyArgumentsEPNS0_7IsolateENS0_20KeyedAccessStoreModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory28KeyedStoreIC_SloppyArgumentsEPNS0_7IsolateENS0_20KeyedAccessStoreModeE
	.type	_ZN2v88internal11CodeFactory28KeyedStoreIC_SloppyArgumentsEPNS0_7IsolateENS0_20KeyedAccessStoreModeE, @function
_ZN2v88internal11CodeFactory28KeyedStoreIC_SloppyArgumentsEPNS0_7IsolateENS0_20KeyedAccessStoreModeE:
.LFB20609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$3, %edx
	ja	.L113
	movq	%rdi, %r12
	addl	$132, %edx
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-48(%rbp), %rax
	movq	%rax, (%r12)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, 8(%r12)
	movq	-32(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L113:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L117:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20609:
	.size	_ZN2v88internal11CodeFactory28KeyedStoreIC_SloppyArgumentsEPNS0_7IsolateENS0_20KeyedAccessStoreModeE, .-_ZN2v88internal11CodeFactory28KeyedStoreIC_SloppyArgumentsEPNS0_7IsolateENS0_20KeyedAccessStoreModeE
	.section	.text._ZN2v88internal11CodeFactory17KeyedStoreIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory17KeyedStoreIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE
	.type	_ZN2v88internal11CodeFactory17KeyedStoreIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE, @function
_ZN2v88internal11CodeFactory17KeyedStoreIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE:
.LFB20610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$3, %edx
	ja	.L119
	movq	%rdi, %r12
	addl	$144, %edx
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-48(%rbp), %rax
	movq	%rax, (%r12)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, 8(%r12)
	movq	-32(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L123
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L119:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L123:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20610:
	.size	_ZN2v88internal11CodeFactory17KeyedStoreIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE, .-_ZN2v88internal11CodeFactory17KeyedStoreIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE
	.section	.text._ZN2v88internal11CodeFactory26StoreInArrayLiteralIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory26StoreInArrayLiteralIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE
	.type	_ZN2v88internal11CodeFactory26StoreInArrayLiteralIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE, @function
_ZN2v88internal11CodeFactory26StoreInArrayLiteralIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE:
.LFB20611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$3, %edx
	ja	.L125
	leaq	CSWTCH.47(%rip), %rax
	movl	%edx, %edx
	movq	%rdi, %r12
	movl	(%rax,%rdx,4), %edx
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-48(%rbp), %rax
	movq	%rax, (%r12)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, 8(%r12)
	movq	-32(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L129
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L125:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L129:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20611:
	.size	_ZN2v88internal11CodeFactory26StoreInArrayLiteralIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE, .-_ZN2v88internal11CodeFactory26StoreInArrayLiteralIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE
	.section	.text._ZN2v88internal11CodeFactory26ElementsTransitionAndStoreEPNS0_7IsolateENS0_20KeyedAccessStoreModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory26ElementsTransitionAndStoreEPNS0_7IsolateENS0_20KeyedAccessStoreModeE
	.type	_ZN2v88internal11CodeFactory26ElementsTransitionAndStoreEPNS0_7IsolateENS0_20KeyedAccessStoreModeE, @function
_ZN2v88internal11CodeFactory26ElementsTransitionAndStoreEPNS0_7IsolateENS0_20KeyedAccessStoreModeE:
.LFB20612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$3, %edx
	ja	.L131
	movq	%rdi, %r12
	addl	$148, %edx
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-48(%rbp), %rax
	movq	%rax, (%r12)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, 8(%r12)
	movq	-32(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L131:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20612:
	.size	_ZN2v88internal11CodeFactory26ElementsTransitionAndStoreEPNS0_7IsolateENS0_20KeyedAccessStoreModeE, .-_ZN2v88internal11CodeFactory26ElementsTransitionAndStoreEPNS0_7IsolateENS0_20KeyedAccessStoreModeE
	.section	.text._ZN2v88internal11CodeFactory18StoreFastElementICEPNS0_7IsolateENS0_20KeyedAccessStoreModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory18StoreFastElementICEPNS0_7IsolateENS0_20KeyedAccessStoreModeE
	.type	_ZN2v88internal11CodeFactory18StoreFastElementICEPNS0_7IsolateENS0_20KeyedAccessStoreModeE, @function
_ZN2v88internal11CodeFactory18StoreFastElementICEPNS0_7IsolateENS0_20KeyedAccessStoreModeE:
.LFB20613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$3, %edx
	ja	.L137
	movq	%rdi, %r12
	addl	$137, %edx
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-48(%rbp), %rax
	movq	%rax, (%r12)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, 8(%r12)
	movq	-32(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L137:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20613:
	.size	_ZN2v88internal11CodeFactory18StoreFastElementICEPNS0_7IsolateENS0_20KeyedAccessStoreModeE, .-_ZN2v88internal11CodeFactory18StoreFastElementICEPNS0_7IsolateENS0_20KeyedAccessStoreModeE
	.section	.text._ZN2v88internal11CodeFactory15BinaryOperationEPNS0_7IsolateENS0_9OperationE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory15BinaryOperationEPNS0_7IsolateENS0_9OperationE
	.type	_ZN2v88internal11CodeFactory15BinaryOperationEPNS0_7IsolateENS0_9OperationE, @function
_ZN2v88internal11CodeFactory15BinaryOperationEPNS0_7IsolateENS0_9OperationE:
.LFB20614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$11, %edx
	ja	.L143
	leaq	.L145(%rip), %rcx
	movl	%edx, %edx
	movq	%rdi, %r12
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11CodeFactory15BinaryOperationEPNS0_7IsolateENS0_9OperationE,"a",@progbits
	.align 4
	.align 4
.L145:
	.long	.L155-.L145
	.long	.L154-.L145
	.long	.L153-.L145
	.long	.L152-.L145
	.long	.L151-.L145
	.long	.L143-.L145
	.long	.L150-.L145
	.long	.L149-.L145
	.long	.L148-.L145
	.long	.L147-.L145
	.long	.L146-.L145
	.long	.L144-.L145
	.section	.text._ZN2v88internal11CodeFactory15BinaryOperationEPNS0_7IsolateENS0_9OperationE
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$445, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	.p2align 4,,10
	.p2align 3
.L142:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L159
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movl	$434, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L154:
	movl	$435, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L153:
	movl	$436, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L152:
	movl	$437, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L151:
	movl	$438, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L150:
	movl	$440, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L149:
	movl	$441, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L148:
	movl	$442, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L147:
	movl	$443, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L146:
	movl	$444, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L142
.L143:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L159:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20614:
	.size	_ZN2v88internal11CodeFactory15BinaryOperationEPNS0_7IsolateENS0_9OperationE, .-_ZN2v88internal11CodeFactory15BinaryOperationEPNS0_7IsolateENS0_9OperationE
	.section	.text._ZN2v88internal11CodeFactory23NonPrimitiveToPrimitiveEPNS0_7IsolateENS0_15ToPrimitiveHintE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory23NonPrimitiveToPrimitiveEPNS0_7IsolateENS0_15ToPrimitiveHintE
	.type	_ZN2v88internal11CodeFactory23NonPrimitiveToPrimitiveEPNS0_7IsolateENS0_15ToPrimitiveHintE, @function
_ZN2v88internal11CodeFactory23NonPrimitiveToPrimitiveEPNS0_7IsolateENS0_15ToPrimitiveHintE:
.LFB20615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	41184(%rsi), %rdi
	movl	%edx, %esi
	subq	$8, %rsp
	call	_ZN2v88internal8Builtins23NonPrimitiveToPrimitiveENS0_15ToPrimitiveHintE@PLT
	movq	%rax, (%r12)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, 8(%r12)
	leaq	2480+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20615:
	.size	_ZN2v88internal11CodeFactory23NonPrimitiveToPrimitiveEPNS0_7IsolateENS0_15ToPrimitiveHintE, .-_ZN2v88internal11CodeFactory23NonPrimitiveToPrimitiveEPNS0_7IsolateENS0_15ToPrimitiveHintE
	.section	.text._ZN2v88internal11CodeFactory19OrdinaryToPrimitiveEPNS0_7IsolateENS0_23OrdinaryToPrimitiveHintE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory19OrdinaryToPrimitiveEPNS0_7IsolateENS0_23OrdinaryToPrimitiveHintE
	.type	_ZN2v88internal11CodeFactory19OrdinaryToPrimitiveEPNS0_7IsolateENS0_23OrdinaryToPrimitiveHintE, @function
_ZN2v88internal11CodeFactory19OrdinaryToPrimitiveEPNS0_7IsolateENS0_23OrdinaryToPrimitiveHintE:
.LFB20616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	41184(%rsi), %rdi
	movl	%edx, %esi
	subq	$8, %rsp
	call	_ZN2v88internal8Builtins19OrdinaryToPrimitiveENS0_23OrdinaryToPrimitiveHintE@PLT
	movq	%rax, (%r12)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, 8(%r12)
	leaq	2480+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20616:
	.size	_ZN2v88internal11CodeFactory19OrdinaryToPrimitiveEPNS0_7IsolateENS0_23OrdinaryToPrimitiveHintE, .-_ZN2v88internal11CodeFactory19OrdinaryToPrimitiveEPNS0_7IsolateENS0_23OrdinaryToPrimitiveHintE
	.section	.text._ZN2v88internal11CodeFactory9StringAddEPNS0_7IsolateENS0_14StringAddFlagsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory9StringAddEPNS0_7IsolateENS0_14StringAddFlagsE
	.type	_ZN2v88internal11CodeFactory9StringAddEPNS0_7IsolateENS0_14StringAddFlagsE, @function
_ZN2v88internal11CodeFactory9StringAddEPNS0_7IsolateENS0_14StringAddFlagsE:
.LFB20617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %edx
	je	.L165
	cmpl	$2, %edx
	je	.L166
	testl	%edx, %edx
	je	.L171
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L165:
	movl	$889, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
.L164:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movl	$706, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L166:
	movl	$890, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L164
.L172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20617:
	.size	_ZN2v88internal11CodeFactory9StringAddEPNS0_7IsolateENS0_14StringAddFlagsE, .-_ZN2v88internal11CodeFactory9StringAddEPNS0_7IsolateENS0_14StringAddFlagsE
	.section	.text._ZN2v88internal11CodeFactory15ResumeGeneratorEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory15ResumeGeneratorEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory15ResumeGeneratorEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory15ResumeGeneratorEPNS0_7IsolateE:
.LFB20618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$45, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L176
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L176:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20618:
	.size	_ZN2v88internal11CodeFactory15ResumeGeneratorEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory15ResumeGeneratorEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory22FrameDropperTrampolineEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory22FrameDropperTrampolineEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory22FrameDropperTrampolineEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory22FrameDropperTrampolineEPNS0_7IsolateE:
.LFB20619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$89, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L180:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20619:
	.size	_ZN2v88internal11CodeFactory22FrameDropperTrampolineEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory22FrameDropperTrampolineEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory23HandleDebuggerStatementEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory23HandleDebuggerStatementEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory23HandleDebuggerStatementEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory23HandleDebuggerStatementEPNS0_7IsolateE:
.LFB20620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$90, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L184
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L184:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20620:
	.size	_ZN2v88internal11CodeFactory23HandleDebuggerStatementEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory23HandleDebuggerStatementEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory22FastNewFunctionContextEPNS0_7IsolateENS0_9ScopeTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory22FastNewFunctionContextEPNS0_7IsolateENS0_9ScopeTypeE
	.type	_ZN2v88internal11CodeFactory22FastNewFunctionContextEPNS0_7IsolateENS0_9ScopeTypeE, @function
_ZN2v88internal11CodeFactory22FastNewFunctionContextEPNS0_7IsolateENS0_9ScopeTypeE:
.LFB20621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$1, %dl
	je	.L186
	cmpb	$2, %dl
	jne	.L193
	movl	$34, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
.L185:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L194
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movl	$33, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L185
.L193:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L194:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20621:
	.size	_ZN2v88internal11CodeFactory22FastNewFunctionContextEPNS0_7IsolateENS0_9ScopeTypeE, .-_ZN2v88internal11CodeFactory22FastNewFunctionContextEPNS0_7IsolateENS0_9ScopeTypeE
	.section	.text._ZN2v88internal11CodeFactory15ArgumentAdaptorEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory15ArgumentAdaptorEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory15ArgumentAdaptorEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory15ArgumentAdaptorEPNS0_7IsolateE:
.LFB20622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L198
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L198:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20622:
	.size	_ZN2v88internal11CodeFactory15ArgumentAdaptorEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory15ArgumentAdaptorEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE
	.type	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE, @function
_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE:
.LFB20623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	41184(%rsi), %rdi
	movl	%edx, %esi
	subq	$8, %rsp
	call	_ZN2v88internal8Builtins4CallENS0_19ConvertReceiverModeE@PLT
	movq	%rax, (%r12)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, 8(%r12)
	leaq	720+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20623:
	.size	_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE, .-_ZN2v88internal11CodeFactory4CallEPNS0_7IsolateENS0_19ConvertReceiverModeE
	.section	.text._ZN2v88internal11CodeFactory17CallWithArrayLikeEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory17CallWithArrayLikeEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory17CallWithArrayLikeEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory17CallWithArrayLikeEPNS0_7IsolateE:
.LFB20624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$14, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L204:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20624:
	.size	_ZN2v88internal11CodeFactory17CallWithArrayLikeEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory17CallWithArrayLikeEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory14CallWithSpreadEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory14CallWithSpreadEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory14CallWithSpreadEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory14CallWithSpreadEPNS0_7IsolateE:
.LFB20625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$13, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L208
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L208:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20625:
	.size	_ZN2v88internal11CodeFactory14CallWithSpreadEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory14CallWithSpreadEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory12CallFunctionEPNS0_7IsolateENS0_19ConvertReceiverModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory12CallFunctionEPNS0_7IsolateENS0_19ConvertReceiverModeE
	.type	_ZN2v88internal11CodeFactory12CallFunctionEPNS0_7IsolateENS0_19ConvertReceiverModeE, @function
_ZN2v88internal11CodeFactory12CallFunctionEPNS0_7IsolateENS0_19ConvertReceiverModeE:
.LFB20626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	41184(%rsi), %rdi
	movl	%edx, %esi
	subq	$8, %rsp
	call	_ZN2v88internal8Builtins12CallFunctionENS0_19ConvertReceiverModeE@PLT
	movq	%rax, (%r12)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, 8(%r12)
	leaq	720+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20626:
	.size	_ZN2v88internal11CodeFactory12CallFunctionEPNS0_7IsolateENS0_19ConvertReceiverModeE, .-_ZN2v88internal11CodeFactory12CallFunctionEPNS0_7IsolateENS0_19ConvertReceiverModeE
	.section	.text._ZN2v88internal11CodeFactory11CallVarargsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory11CallVarargsEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory11CallVarargsEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory11CallVarargsEPNS0_7IsolateE:
.LFB20627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$12, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L214
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L214:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20627:
	.size	_ZN2v88internal11CodeFactory11CallVarargsEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory11CallVarargsEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory18CallForwardVarargsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory18CallForwardVarargsEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory18CallForwardVarargsEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory18CallForwardVarargsEPNS0_7IsolateE:
.LFB20628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$15, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L218:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20628:
	.size	_ZN2v88internal11CodeFactory18CallForwardVarargsEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory18CallForwardVarargsEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory26CallFunctionForwardVarargsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory26CallFunctionForwardVarargsEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory26CallFunctionForwardVarargsEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory26CallFunctionForwardVarargsEPNS0_7IsolateE:
.LFB20629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L222
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L222:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20629:
	.size	_ZN2v88internal11CodeFactory26CallFunctionForwardVarargsEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory26CallFunctionForwardVarargsEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory9ConstructEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory9ConstructEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory9ConstructEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory9ConstructEPNS0_7IsolateE:
.LFB20630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$23, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L226
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L226:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20630:
	.size	_ZN2v88internal11CodeFactory9ConstructEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory9ConstructEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory19ConstructWithSpreadEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory19ConstructWithSpreadEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory19ConstructWithSpreadEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory19ConstructWithSpreadEPNS0_7IsolateE:
.LFB20631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$25, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L230:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20631:
	.size	_ZN2v88internal11CodeFactory19ConstructWithSpreadEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory19ConstructWithSpreadEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory17ConstructFunctionEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory17ConstructFunctionEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory17ConstructFunctionEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory17ConstructFunctionEPNS0_7IsolateE:
.LFB20632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$20, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L234
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L234:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20632:
	.size	_ZN2v88internal11CodeFactory17ConstructFunctionEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory17ConstructFunctionEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory16ConstructVarargsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory16ConstructVarargsEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory16ConstructVarargsEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory16ConstructVarargsEPNS0_7IsolateE:
.LFB20633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$24, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L238
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L238:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20633:
	.size	_ZN2v88internal11CodeFactory16ConstructVarargsEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory16ConstructVarargsEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory23ConstructForwardVarargsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory23ConstructForwardVarargsEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory23ConstructForwardVarargsEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory23ConstructForwardVarargsEPNS0_7IsolateE:
.LFB20634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$27, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L242
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L242:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20634:
	.size	_ZN2v88internal11CodeFactory23ConstructForwardVarargsEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory23ConstructForwardVarargsEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory31ConstructFunctionForwardVarargsEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory31ConstructFunctionForwardVarargsEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory31ConstructFunctionForwardVarargsEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory31ConstructFunctionForwardVarargsEPNS0_7IsolateE:
.LFB20635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$28, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L246
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L246:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20635:
	.size	_ZN2v88internal11CodeFactory31ConstructFunctionForwardVarargsEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory31ConstructFunctionForwardVarargsEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory27InterpreterPushArgsThenCallEPNS0_7IsolateENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory27InterpreterPushArgsThenCallEPNS0_7IsolateENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE
	.type	_ZN2v88internal11CodeFactory27InterpreterPushArgsThenCallEPNS0_7IsolateENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE, @function
_ZN2v88internal11CodeFactory27InterpreterPushArgsThenCallEPNS0_7IsolateENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE:
.LFB20636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %ecx
	je	.L248
	cmpl	$2, %ecx
	jne	.L252
	testl	%edx, %edx
	je	.L251
	subl	$1, %edx
	cmpl	$1, %edx
	ja	.L252
	movl	$58, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
.L247:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L257
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movl	$59, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L248:
	movl	$60, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L247
.L252:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L257:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20636:
	.size	_ZN2v88internal11CodeFactory27InterpreterPushArgsThenCallEPNS0_7IsolateENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE, .-_ZN2v88internal11CodeFactory27InterpreterPushArgsThenCallEPNS0_7IsolateENS0_19ConvertReceiverModeENS0_23InterpreterPushArgsModeE
	.section	.text._ZN2v88internal11CodeFactory32InterpreterPushArgsThenConstructEPNS0_7IsolateENS0_23InterpreterPushArgsModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory32InterpreterPushArgsThenConstructEPNS0_7IsolateENS0_23InterpreterPushArgsModeE
	.type	_ZN2v88internal11CodeFactory32InterpreterPushArgsThenConstructEPNS0_7IsolateENS0_23InterpreterPushArgsModeE, @function
_ZN2v88internal11CodeFactory32InterpreterPushArgsThenConstructEPNS0_7IsolateENS0_23InterpreterPushArgsModeE:
.LFB20637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %edx
	je	.L259
	cmpl	$2, %edx
	je	.L260
	testl	%edx, %edx
	je	.L265
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L259:
	movl	$63, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
.L258:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L266
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_restore_state
	movl	$62, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$61, %edx
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L258
.L266:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20637:
	.size	_ZN2v88internal11CodeFactory32InterpreterPushArgsThenConstructEPNS0_7IsolateENS0_23InterpreterPushArgsModeE, .-_ZN2v88internal11CodeFactory32InterpreterPushArgsThenConstructEPNS0_7IsolateENS0_23InterpreterPushArgsModeE
	.section	.text._ZN2v88internal11CodeFactory17InterpreterCEntryEPNS0_7IsolateEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory17InterpreterCEntryEPNS0_7IsolateEi
	.type	_ZN2v88internal11CodeFactory17InterpreterCEntryEPNS0_7IsolateEi, @function
_ZN2v88internal11CodeFactory17InterpreterCEntryEPNS0_7IsolateEi:
.LFB20638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	cmpl	$1, %edx
	jne	.L268
	leaq	41184(%rsi), %rdi
	movl	$697, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%rax, (%r12)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, 8(%r12)
	leaq	1520+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	cmpl	$2, %edx
	je	.L272
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L272:
	leaq	41184(%rsi), %rdi
	movl	$702, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%rax, (%r12)
	leaq	16+_ZTVN2v88internal23CallInterfaceDescriptorE(%rip), %rax
	movq	%rax, 8(%r12)
	leaq	1560+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20638:
	.size	_ZN2v88internal11CodeFactory17InterpreterCEntryEPNS0_7IsolateEi, .-_ZN2v88internal11CodeFactory17InterpreterCEntryEPNS0_7IsolateEi
	.section	.text._ZN2v88internal11CodeFactory29InterpreterOnStackReplacementEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory29InterpreterOnStackReplacementEPNS0_7IsolateE
	.type	_ZN2v88internal11CodeFactory29InterpreterOnStackReplacementEPNS0_7IsolateE, @function
_ZN2v88internal11CodeFactory29InterpreterOnStackReplacementEPNS0_7IsolateE:
.LFB20639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$66, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L276:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20639:
	.size	_ZN2v88internal11CodeFactory29InterpreterOnStackReplacementEPNS0_7IsolateE, .-_ZN2v88internal11CodeFactory29InterpreterOnStackReplacementEPNS0_7IsolateE
	.section	.text._ZN2v88internal11CodeFactory26ArrayNoArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory26ArrayNoArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE
	.type	_ZN2v88internal11CodeFactory26ArrayNoArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE, @function
_ZN2v88internal11CodeFactory26ArrayNoArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE:
.LFB20640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%dl, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rdi
	movq	%rdi, -24(%rbp)
	xorl	%edi, %edi
	testl	%ecx, %ecx
	jne	.L278
	cmpb	$1, %dl
	jbe	.L299
.L278:
	cmpb	$5, %dl
	ja	.L281
	leaq	.L283(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11CodeFactory26ArrayNoArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE,"a",@progbits
	.align 4
	.align 4
.L283:
	.long	.L288-.L283
	.long	.L287-.L283
	.long	.L286-.L283
	.long	.L285-.L283
	.long	.L284-.L283
	.long	.L282-.L283
	.section	.text._ZN2v88internal11CodeFactory26ArrayNoArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE
	.p2align 4,,10
	.p2align 3
.L299:
	je	.L300
	movl	$172, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L277
.L284:
	movl	$178, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	.p2align 4,,10
	.p2align 3
.L277:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L301
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L282:
	.cfi_restore_state
	movl	$179, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L277
.L288:
	movl	$174, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L277
.L287:
	movl	$175, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L277
.L286:
	movl	$176, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L277
.L285:
	movl	$177, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L300:
	movl	$173, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L277
.L301:
	call	__stack_chk_fail@PLT
.L281:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20640:
	.size	_ZN2v88internal11CodeFactory26ArrayNoArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE, .-_ZN2v88internal11CodeFactory26ArrayNoArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE
	.section	.text._ZN2v88internal11CodeFactory30ArraySingleArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11CodeFactory30ArraySingleArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE
	.type	_ZN2v88internal11CodeFactory30ArraySingleArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE, @function
_ZN2v88internal11CodeFactory30ArraySingleArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE:
.LFB20641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%dl, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rdi
	movq	%rdi, -24(%rbp)
	xorl	%edi, %edi
	testl	%ecx, %ecx
	jne	.L303
	cmpb	$1, %dl
	jbe	.L324
.L303:
	cmpb	$5, %dl
	ja	.L306
	leaq	.L308(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11CodeFactory30ArraySingleArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE,"a",@progbits
	.align 4
	.align 4
.L308:
	.long	.L313-.L308
	.long	.L312-.L308
	.long	.L311-.L308
	.long	.L310-.L308
	.long	.L309-.L308
	.long	.L307-.L308
	.section	.text._ZN2v88internal11CodeFactory30ArraySingleArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE
	.p2align 4,,10
	.p2align 3
.L324:
	je	.L325
	movl	$180, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L302
.L309:
	movl	$186, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	.p2align 4,,10
	.p2align 3
.L302:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L326
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L307:
	.cfi_restore_state
	movl	$187, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L302
.L313:
	movl	$182, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L302
.L312:
	movl	$183, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L302
.L311:
	movl	$184, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L302
.L310:
	movl	$185, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L325:
	movl	$181, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal8Builtins11CallableForEPNS0_7IsolateENS1_4NameE@PLT
	jmp	.L302
.L326:
	call	__stack_chk_fail@PLT
.L306:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE20641:
	.size	_ZN2v88internal11CodeFactory30ArraySingleArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE, .-_ZN2v88internal11CodeFactory30ArraySingleArgumentConstructorEPNS0_7IsolateENS0_12ElementsKindENS0_26AllocationSiteOverrideModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11CodeFactory13RuntimeCEntryEPNS0_7IsolateEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11CodeFactory13RuntimeCEntryEPNS0_7IsolateEi, @function
_GLOBAL__sub_I__ZN2v88internal11CodeFactory13RuntimeCEntryEPNS0_7IsolateEi:
.LFB25409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25409:
	.size	_GLOBAL__sub_I__ZN2v88internal11CodeFactory13RuntimeCEntryEPNS0_7IsolateEi, .-_GLOBAL__sub_I__ZN2v88internal11CodeFactory13RuntimeCEntryEPNS0_7IsolateEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11CodeFactory13RuntimeCEntryEPNS0_7IsolateEi
	.section	.rodata.CSWTCH.47,"a"
	.align 16
	.type	CSWTCH.47, @object
	.size	CSWTCH.47, 16
CSWTCH.47:
	.long	136
	.long	141
	.long	142
	.long	143
	.weak	_ZTVN2v88internal23CallInterfaceDescriptorE
	.section	.data.rel.ro.local._ZTVN2v88internal23CallInterfaceDescriptorE,"awG",@progbits,_ZTVN2v88internal23CallInterfaceDescriptorE,comdat
	.align 8
	.type	_ZTVN2v88internal23CallInterfaceDescriptorE, @object
	.size	_ZTVN2v88internal23CallInterfaceDescriptorE, 48
_ZTVN2v88internal23CallInterfaceDescriptorE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23CallInterfaceDescriptorD1Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptorD0Ev
	.quad	_ZN2v88internal23CallInterfaceDescriptor26InitializePlatformSpecificEPNS0_27CallInterfaceDescriptorDataE
	.quad	_ZN2v88internal23CallInterfaceDescriptor29InitializePlatformIndependentEPNS0_27CallInterfaceDescriptorDataE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
