	.file	"ic-stats.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5073:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5073:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5074:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5074:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v88internal7ICStatsC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7ICStatsC2Ev
	.type	_ZN2v88internal7ICStatsC2Ev, @function
_ZN2v88internal7ICStatsC2Ev:
.LFB18350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	$4096, (%rdi)
	movq	$0, 24(%rdi)
	movups	%xmm0, 8(%rdi)
	movl	$589824, %edi
	call	_Znwm@PLT
	leaq	589824(%rax), %rdx
	movq	%rax, 8(%rbx)
	movq	%rdx, 24(%rbx)
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	16(%rax), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	leaq	80(%rax), %rcx
	movq	%rcx, 64(%rax)
	leaq	128(%rax), %rcx
	addq	$144, %rax
	movb	$0, -128(%rax)
	movq	$0, -112(%rax)
	movl	$0, -104(%rax)
	movq	$0, -96(%rax)
	movl	$-1, -88(%rax)
	movb	$0, -84(%rax)
	movb	$0, -83(%rax)
	movq	$0, -72(%rax)
	movb	$0, -64(%rax)
	movq	$0, -48(%rax)
	movb	$0, -40(%rax)
	movl	$0, -36(%rax)
	movq	%rcx, -32(%rax)
	movq	$0, -24(%rax)
	movb	$0, -16(%rax)
	cmpq	%rax, %rdx
	jne	.L5
	movss	.LC0(%rip), %xmm0
	leaq	80(%rbx), %rax
	movq	%rdx, 16(%rbx)
	movq	%rax, 32(%rbx)
	leaq	136(%rbx), %rax
	movq	$1, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	%rax, 88(%rbx)
	movq	$1, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 128(%rbx)
	movq	$0, 136(%rbx)
	movl	$0, 144(%rbx)
	movss	%xmm0, 64(%rbx)
	movss	%xmm0, 120(%rbx)
	movl	$0, 4(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18350:
	.size	_ZN2v88internal7ICStatsC2Ev, .-_ZN2v88internal7ICStatsC2Ev
	.globl	_ZN2v88internal7ICStatsC1Ev
	.set	_ZN2v88internal7ICStatsC1Ev,_ZN2v88internal7ICStatsC2Ev
	.section	.text._ZN2v88internal7ICStats5BeginEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7ICStats5BeginEv
	.type	_ZN2v88internal7ICStats5BeginEv, @function
_ZN2v88internal7ICStats5BeginEv:
.LFB18352:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags8ic_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L13
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, 4(%rdi)
	ret
	.cfi_endproc
.LFE18352:
	.size	_ZN2v88internal7ICStats5BeginEv, .-_ZN2v88internal7ICStats5BeginEv
	.section	.rodata._ZN2v88internal7ICStats5ResetEv.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v88internal7ICStats5ResetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7ICStats5ResetEv
	.type	_ZN2v88internal7ICStats5ResetEv, @function
_ZN2v88internal7ICStats5ResetEv:
.LFB18354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-216(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-192(%rbp), %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -256(%rbp)
	movq	8(%rdi), %r15
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	16(%rdi), %rcx
	movq	%rcx, -232(%rbp)
	cmpq	%rcx, %r15
	je	.L36
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r12, -208(%rbp)
	movq	(%r15), %rbx
	movq	8(%r15), %r13
	movq	%rbx, %rax
	addq	%r13, %rax
	je	.L16
	testq	%rbx, %rbx
	je	.L21
.L16:
	movq	%r13, -216(%rbp)
	cmpq	$15, %r13
	ja	.L59
	cmpq	$1, %r13
	jne	.L19
	movzbl	(%rbx), %eax
	movb	%al, -192(%rbp)
	movq	%r12, %rax
.L20:
	movq	%r13, -200(%rbp)
	leaq	-128(%rbp), %rbx
	movb	$0, (%rax,%r13)
	movq	32(%r15), %rax
	movq	%rax, -176(%rbp)
	movl	40(%r15), %eax
	movl	%eax, -168(%rbp)
	movq	48(%r15), %rax
	movq	%rax, -160(%rbp)
	movl	56(%r15), %eax
	movl	%eax, -152(%rbp)
	movzbl	60(%r15), %eax
	movb	%al, -148(%rbp)
	movzbl	61(%r15), %eax
	movq	%rbx, -144(%rbp)
	movb	%al, -147(%rbp)
	movq	64(%r15), %r9
	movq	72(%r15), %r13
	movq	%r9, %rax
	addq	%r13, %rax
	je	.L42
	testq	%r9, %r9
	je	.L21
.L42:
	movq	%r13, -216(%rbp)
	cmpq	$15, %r13
	ja	.L60
	cmpq	$1, %r13
	jne	.L25
	movzbl	(%r9), %eax
	movb	%al, -128(%rbp)
	movq	%rbx, %rax
.L26:
	movq	%r13, -136(%rbp)
	movb	$0, (%rax,%r13)
	movq	96(%r15), %rax
	leaq	-80(%rbp), %r13
	movq	%rax, -112(%rbp)
	movzbl	104(%r15), %eax
	movb	%al, -104(%rbp)
	movl	108(%r15), %eax
	movq	%r13, -96(%rbp)
	movl	%eax, -100(%rbp)
	movq	112(%r15), %r10
	movq	120(%r15), %r8
	movq	%r10, %rax
	addq	%r8, %rax
	je	.L43
	testq	%r10, %r10
	je	.L21
.L43:
	movq	%r8, -216(%rbp)
	cmpq	$15, %r8
	ja	.L61
	cmpq	$1, %r8
	jne	.L30
	movzbl	(%r10), %eax
	movb	%al, -80(%rbp)
	movq	%r13, %rax
.L31:
	movb	$0, (%rax,%r8)
	movq	-208(%rbp), %rax
	movq	$0, -200(%rbp)
	movb	$0, (%rax)
	xorl	%eax, %eax
	movw	%ax, -148(%rbp)
	movq	-144(%rbp), %rax
	movq	$0, -176(%rbp)
	movl	$0, -168(%rbp)
	movq	$0, -160(%rbp)
	movl	$-1, -152(%rbp)
	movq	$0, -136(%rbp)
	movb	$0, (%rax)
	movq	-96(%rbp), %rax
	movq	$0, -112(%rbp)
	movb	$0, -104(%rbp)
	movl	$0, -100(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, (%rax)
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L32
	call	_ZdlPv@PLT
.L32:
	movq	-144(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L33
	call	_ZdlPv@PLT
.L33:
	movq	-208(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L34
	call	_ZdlPv@PLT
	addq	$144, %r15
	cmpq	%r15, -232(%rbp)
	jne	.L37
.L36:
	movq	-256(%rbp), %rax
	movl	$0, 144(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L62
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	addq	$144, %r15
	cmpq	%r15, -232(%rbp)
	jne	.L37
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L19:
	testq	%r13, %r13
	jne	.L63
	movq	%r12, %rax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L30:
	testq	%r8, %r8
	jne	.L64
	movq	%r13, %rax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L25:
	testq	%r13, %r13
	jne	.L65
	movq	%rbx, %rax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	-208(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -208(%rbp)
	movq	%rax, %rdi
	movq	-216(%rbp), %rax
	movq	%rax, -192(%rbp)
.L18:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-216(%rbp), %r13
	movq	-208(%rbp), %rax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	-144(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r9, -240(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-240(%rbp), %r9
	movq	%rax, -144(%rbp)
	movq	%rax, %rdi
	movq	-216(%rbp), %rax
	movq	%rax, -128(%rbp)
.L24:
	movq	%r13, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-216(%rbp), %r13
	movq	-144(%rbp), %rax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r8, -248(%rbp)
	movq	%r10, -240(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-240(%rbp), %r10
	movq	-248(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-216(%rbp), %rax
	movq	%rax, -80(%rbp)
.L29:
	movq	%r8, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-216(%rbp), %r8
	movq	-96(%rbp), %rax
	jmp	.L31
.L21:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L65:
	movq	%rbx, %rdi
	jmp	.L24
.L64:
	movq	%r13, %rdi
	jmp	.L29
.L63:
	movq	%r12, %rdi
	jmp	.L18
.L62:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18354:
	.size	_ZN2v88internal7ICStats5ResetEv, .-_ZN2v88internal7ICStats5ResetEv
	.section	.text._ZN2v88internal6ICInfoC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6ICInfoC2Ev
	.type	_ZN2v88internal6ICInfoC2Ev, @function
_ZN2v88internal6ICInfoC2Ev:
.LFB18373:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movw	%ax, 60(%rdi)
	leaq	80(%rdi), %rax
	movq	%rax, 64(%rdi)
	leaq	128(%rdi), %rax
	movb	$0, 16(%rdi)
	movq	$0, 32(%rdi)
	movl	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movl	$-1, 56(%rdi)
	movq	$0, 72(%rdi)
	movb	$0, 80(%rdi)
	movq	$0, 96(%rdi)
	movb	$0, 104(%rdi)
	movl	$0, 108(%rdi)
	movq	%rax, 112(%rdi)
	movq	$0, 120(%rdi)
	movb	$0, 128(%rdi)
	ret
	.cfi_endproc
.LFE18373:
	.size	_ZN2v88internal6ICInfoC2Ev, .-_ZN2v88internal6ICInfoC2Ev
	.globl	_ZN2v88internal6ICInfoC1Ev
	.set	_ZN2v88internal6ICInfoC1Ev,_ZN2v88internal6ICInfoC2Ev
	.section	.text._ZN2v88internal6ICInfo5ResetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6ICInfo5ResetEv
	.type	_ZN2v88internal6ICInfo5ResetEv, @function
_ZN2v88internal6ICInfo5ResetEv:
.LFB18375:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	$0, 8(%rdi)
	movb	$0, (%rax)
	xorl	%eax, %eax
	movw	%ax, 60(%rdi)
	movq	64(%rdi), %rax
	movq	$0, 32(%rdi)
	movl	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movl	$-1, 56(%rdi)
	movq	$0, 72(%rdi)
	movb	$0, (%rax)
	movq	112(%rdi), %rax
	movq	$0, 96(%rdi)
	movb	$0, 104(%rdi)
	movl	$0, 108(%rdi)
	movq	$0, 120(%rdi)
	movb	$0, (%rax)
	ret
	.cfi_endproc
.LFE18375:
	.size	_ZN2v88internal6ICInfo5ResetEv, .-_ZN2v88internal6ICInfo5ResetEv
	.section	.rodata._ZNK2v88internal6ICInfo19AppendToTracedValueEPNS_7tracing11TracedValueE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"type"
.LC3:
	.string	"functionName"
.LC4:
	.string	"optimized"
.LC5:
	.string	"offset"
.LC6:
	.string	"scriptName"
.LC7:
	.string	"lineNum"
.LC8:
	.string	"constructor"
.LC9:
	.string	"state"
.LC10:
	.string	"map"
.LC11:
	.string	"dict"
.LC12:
	.string	"own"
.LC13:
	.string	"instanceType"
	.section	.text._ZNK2v88internal6ICInfo19AppendToTracedValueEPNS_7tracing11TracedValueE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6ICInfo19AppendToTracedValueEPNS_7tracing11TracedValueE
	.type	_ZNK2v88internal6ICInfo19AppendToTracedValueEPNS_7tracing11TracedValueE, @function
_ZNK2v88internal6ICInfo19AppendToTracedValueEPNS_7tracing11TracedValueE:
.LFB18376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$456, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87tracing11TracedValue15BeginDictionaryEv@PLT
	movq	(%rbx), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	32(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L70
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87tracing11TracedValue9SetStringEPKcS3_@PLT
	cmpb	$0, 61(%rbx)
	jne	.L97
.L70:
	movl	40(%rbx), %edx
	testl	%edx, %edx
	jne	.L98
.L72:
	movq	48(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L73
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87tracing11TracedValue9SetStringEPKcS3_@PLT
.L73:
	movl	56(%rbx), %edx
	cmpl	$-1, %edx
	je	.L74
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87tracing11TracedValue10SetIntegerEPKci@PLT
.L74:
	cmpb	$0, 60(%rbx)
	jne	.L99
.L75:
	cmpq	$0, 72(%rbx)
	jne	.L100
.L76:
	cmpq	$0, 96(%rbx)
	je	.L85
	movq	.LC14(%rip), %xmm1
	leaq	-320(%rbp), %r13
	leaq	-448(%rbp), %r14
	movq	%r13, %rdi
	leaq	-432(%rbp), %r15
	movhps	.LC15(%rip), %xmm1
	movaps	%xmm1, -496(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -448(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	-448(%rbp), %rax
	movq	$0, -440(%rbp)
	movq	-24(%rax), %rdi
	addq	%r14, %rdi
	leaq	-368(%rbp), %r14
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	xorl	%esi, %esi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-496(%rbp), %xmm1
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -496(%rbp)
	movq	%rax, -352(%rbp)
	movl	$24, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	96(%rbx), %rsi
	movq	%r15, %rdi
	leaq	-464(%rbp), %r15
	call	_ZNSo9_M_insertIPKvEERSoT_@PLT
	movq	-384(%rbp), %rax
	movq	%r15, -480(%rbp)
	leaq	-480(%rbp), %rdi
	movq	$0, -472(%rbp)
	movb	$0, -464(%rbp)
	testq	%rax, %rax
	je	.L78
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L79
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L80:
	movq	-480(%rbp), %rdx
	movq	%r12, %rdi
	leaq	.LC10(%rip), %rsi
	call	_ZN2v87tracing11TracedValue9SetStringEPKcS3_@PLT
	movq	-480(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	movq	.LC14(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -448(%rbp)
	addq	$80, %rax
	movhps	.LC16(%rip), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-496(%rbp), %rdi
	je	.L82
	call	_ZdlPv@PLT
.L82:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, %rdi
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -448(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -440(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpq	$0, 96(%rbx)
	je	.L85
	movzbl	104(%rbx), %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87tracing11TracedValue10SetIntegerEPKci@PLT
	cmpq	$0, 96(%rbx)
	je	.L85
	movl	108(%rbx), %edx
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87tracing11TracedValue10SetIntegerEPKci@PLT
.L85:
	cmpq	$0, 120(%rbx)
	jne	.L101
.L84:
	movq	%r12, %rdi
	call	_ZN2v87tracing11TracedValue13EndDictionaryEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87tracing11TracedValue10SetIntegerEPKci@PLT
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L101:
	movq	112(%rbx), %rdx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87tracing11TracedValue9SetStringEPKcS3_@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L100:
	movq	64(%rbx), %rdx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87tracing11TracedValue9SetStringEPKcS3_@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87tracing11TracedValue10SetIntegerEPKci@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L79:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87tracing11TracedValue10SetIntegerEPKci@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L80
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18376:
	.size	_ZNK2v88internal6ICInfo19AppendToTracedValueEPNS_7tracing11TracedValueE, .-_ZNK2v88internal6ICInfo19AppendToTracedValueEPNS_7tracing11TracedValueE
	.section	.rodata._ZN2v88internal7ICStats4DumpEv.str1.1,"aMS",@progbits,1
.LC17:
	.string	"data"
	.section	.rodata._ZN2v88internal7ICStats4DumpEv.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"disabled-by-default-v8.ic_stats"
	.section	.rodata._ZN2v88internal7ICStats4DumpEv.str1.1
.LC19:
	.string	"ic-stats"
.LC20:
	.string	"V8.ICStats"
	.section	.text._ZN2v88internal7ICStats4DumpEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7ICStats4DumpEv
	.type	_ZN2v88internal7ICStats4DumpEv, @function
_ZN2v88internal7ICStats4DumpEv:
.LFB18361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-88(%rbp), %rdi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87tracing11TracedValue6CreateEv@PLT
	movq	-88(%rbp), %rdi
	leaq	.LC17(%rip), %rsi
	call	_ZN2v87tracing11TracedValue10BeginArrayEPKc@PLT
	movl	144(%r12), %eax
	testl	%eax, %eax
	jle	.L104
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	(%rbx,%rbx,8), %rdi
	movq	-88(%rbp), %rsi
	addq	$1, %rbx
	salq	$4, %rdi
	addq	8(%r12), %rdi
	call	_ZNK2v88internal6ICInfo19AppendToTracedValueEPNS_7tracing11TracedValueE
	cmpl	%ebx, 144(%r12)
	jg	.L105
.L104:
	movq	-88(%rbp), %rdi
	call	_ZN2v87tracing11TracedValue8EndArrayEv@PLT
	movq	_ZZN2v88internal7ICStats4DumpEvE27trace_event_unique_atomic52(%rip), %rdx
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L130
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L131
.L109:
	movq	%r12, %rdi
	call	_ZN2v88internal7ICStats5ResetEv
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L103
	movq	(%rdi), %rax
	call	*8(%rax)
.L103:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L133
.L108:
	movq	%r13, _ZZN2v88internal7ICStats4DumpEvE27trace_event_unique_atomic52(%rip)
	movzbl	0(%r13), %eax
	testb	$5, %al
	je	.L109
.L131:
	leaq	.LC19(%rip), %rax
	movb	$8, -89(%rbp)
	movq	%rax, -80(%rbp)
	movq	-88(%rbp), %rax
	movq	$0, -56(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L134
.L110:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L111
	movq	(%rdi), %rax
	call	*8(%rax)
.L111:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L109
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L109
.L133:
	leaq	.LC18(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L108
.L134:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$16
	leaq	.LC20(%rip), %rcx
	movl	$73, %esi
	pushq	%rdx
	leaq	-72(%rbp), %rdx
	pushq	%rdx
	leaq	-89(%rbp), %rdx
	pushq	%rdx
	leaq	-80(%rbp), %rdx
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L110
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18361:
	.size	_ZN2v88internal7ICStats4DumpEv, .-_ZN2v88internal7ICStats4DumpEv
	.section	.text._ZN2v88internal7ICStats3EndEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7ICStats3EndEv
	.type	_ZN2v88internal7ICStats3EndEv, @function
_ZN2v88internal7ICStats3EndEv:
.LFB18353:
	.cfi_startproc
	endbr64
	movl	4(%rdi), %eax
	cmpl	$1, %eax
	jne	.L139
	movl	144(%rdi), %eax
	addl	$1, %eax
	movl	%eax, 144(%rdi)
	cmpl	(%rdi), %eax
	je	.L143
	movl	$0, 4(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	_ZN2v88internal7ICStats4DumpEv
	movq	-8(%rbp), %rdi
	movl	$0, 4(%rdi)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18353:
	.size	_ZN2v88internal7ICStats3EndEv, .-_ZN2v88internal7ICStats3EndEv
	.section	.text._ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	.type	_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm, @function
_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm:
.LFB22051:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L145
	movq	(%rbx), %r8
.L146:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L155
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L156:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L169
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L170
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L148:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L150
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L152:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L153:
	testq	%rsi, %rsi
	je	.L150
.L151:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L152
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L158
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L151
	.p2align 4,,10
	.p2align 3
.L150:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r10
	je	.L154
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L154:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L155:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L157
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L157:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%rdx, %rdi
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L169:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L148
.L170:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE22051:
	.size	_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm, .-_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	.section	.text._ZNSt8__detail9_Map_baseImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS4_EEESaIS8_ENS_10_Select1stESt8equal_toImESt4hashImENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,"axG",@progbits,_ZNSt8__detail9_Map_baseImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS4_EEESaIS8_ENS_10_Select1stESt8equal_toImESt4hashImENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS4_EEESaIS8_ENS_10_Select1stESt8equal_toImESt4hashImENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.type	_ZNSt8__detail9_Map_baseImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS4_EEESaIS8_ENS_10_Select1stESt8equal_toImESt4hashImENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, @function
_ZNSt8__detail9_Map_baseImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS4_EEESaIS8_ENS_10_Select1stESt8equal_toImESt4hashImENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_:
.LFB21415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %r13
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r13, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L172
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L184:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L172
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L172
.L174:
	cmpq	%r8, %r13
	jne	.L184
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movl	$1, %r8d
	movq	$0, 16(%rcx)
	movq	%rax, 8(%rcx)
	call	_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21415:
	.size	_ZNSt8__detail9_Map_baseImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS4_EEESaIS8_ENS_10_Select1stESt8equal_toImESt4hashImENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, .-_ZNSt8__detail9_Map_baseImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS4_EEESaIS8_ENS_10_Select1stESt8equal_toImESt4hashImENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.section	.text._ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_ImS6_EEEES0_INS9_14_Node_iteratorIS7_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_ImS6_EEEES0_INS9_14_Node_iteratorIS7_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_ImS6_EEEES0_INS9_14_Node_iteratorIS7_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_ImS6_EEEES0_INS9_14_Node_iteratorIS7_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_ImS6_EEEES0_INS9_14_Node_iteratorIS7_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB22065:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	(%rbx), %r9
	movq	8(%rbx), %rdi
	xorl	%edx, %edx
	movq	$0, 8(%rbx)
	movq	8(%r13), %rcx
	movq	%rax, %r12
	movq	$0, (%rax)
	movq	%r9, 8(%rax)
	movq	%rdi, 16(%rax)
	movq	%r9, %rax
	divq	%rcx
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L186
	movq	(%rax), %rbx
	movq	8(%rbx), %rsi
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L200:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L186
	movq	8(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L186
.L188:
	cmpq	%rsi, %r9
	jne	.L200
	testq	%rdi, %rdi
	je	.L191
	call	_ZdaPv@PLT
.L191:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$8, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	movq	%r12, %rcx
	movq	%r9, %rdx
	movq	%r13, %rdi
	movl	$1, %r8d
	movq	%r10, %rsi
	call	_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	addq	$8, %rsp
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22065:
	.size	_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_ImS6_EEEES0_INS9_14_Node_iteratorIS7_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_ImS6_EEEES0_INS9_14_Node_iteratorIS7_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZN2v88internal7ICStats20GetOrCacheScriptNameENS0_6ScriptE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7ICStats20GetOrCacheScriptNameENS0_6ScriptE
	.type	_ZN2v88internal7ICStats20GetOrCacheScriptNameENS0_6ScriptE, @function
_ZN2v88internal7ICStats20GetOrCacheScriptNameENS0_6ScriptE:
.LFB18363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	32(%rdi), %r13
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -40
	movq	40(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	%rsi, -80(%rbp)
	divq	%r8
	movq	32(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L202
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movq	8(%rcx), %rdi
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L225:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L202
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L202
.L204:
	cmpq	%rdi, %rsi
	jne	.L225
	leaq	-80(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt8__detail9_Map_baseImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS4_EEESaIS8_ENS_10_Select1stESt8equal_toImESt4hashImENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	(%rax), %r12
.L201:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L226
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	15(%rsi), %rax
	testb	$1, %al
	jne	.L227
.L205:
	movq	%r13, %rdi
	leaq	-64(%rbp), %rsi
	movq	%r10, -64(%rbp)
	movq	$0, -56(%rbp)
	call	_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_ImS6_EEEES0_INS9_14_Node_iteratorIS7_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L209
	call	_ZdaPv@PLT
.L209:
	xorl	%r12d, %r12d
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L227:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L206
	movq	-80(%rbp), %r10
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L206:
	leaq	-64(%rbp), %r14
	leaq	-72(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-64(%rbp), %r12
	movq	-80(%rbp), %rax
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	movq	%r12, -56(%rbp)
	call	_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_ImS6_EEEES0_INS9_14_Node_iteratorIS7_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L201
	call	_ZdaPv@PLT
	jmp	.L201
.L226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18363:
	.size	_ZN2v88internal7ICStats20GetOrCacheScriptNameENS0_6ScriptE, .-_ZN2v88internal7ICStats20GetOrCacheScriptNameENS0_6ScriptE
	.section	.text._ZN2v88internal7ICStats22GetOrCacheFunctionNameENS0_10JSFunctionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7ICStats22GetOrCacheFunctionNameENS0_10JSFunctionE
	.type	_ZN2v88internal7ICStats22GetOrCacheFunctionNameENS0_10JSFunctionE, @function
_ZN2v88internal7ICStats22GetOrCacheFunctionNameENS0_10JSFunctionE:
.LFB18371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	88(%rdi), %r13
	pushq	%r12
	subq	$72, %rsp
	.cfi_offset 12, -40
	movq	96(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	%rsi, -88(%rbp)
	divq	%r8
	movq	88(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L229
	movq	(%rax), %rcx
	movq	%rdx, %r10
	movq	8(%rcx), %r9
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L253:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L229
	movq	8(%rcx), %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L229
.L231:
	cmpq	%r9, %rsi
	jne	.L253
	leaq	-88(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt8__detail9_Map_baseImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS4_EEESaIS8_ENS_10_Select1stESt8equal_toImESt4hashImENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	(%rax), %r12
.L228:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L254
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	.cfi_restore_state
	movq	23(%rsi), %rax
	movq	%rax, -80(%rbp)
	movslq	144(%rdi), %rax
	leaq	(%rax,%rax,8), %rax
	salq	$4, %rax
	addq	8(%rdi), %rax
	movq	47(%rsi), %rdx
	cmpl	$67, 59(%rdx)
	jne	.L232
.L234:
	xorl	%edx, %edx
.L233:
	movb	%dl, 61(%rax)
	leaq	-80(%rbp), %rdi
	leaq	-64(%rbp), %r14
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	leaq	-72(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	%r14, %rdi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-64(%rbp), %r12
	movq	-88(%rbp), %rax
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rax, -64(%rbp)
	movq	%r12, -56(%rbp)
	call	_ZNSt10_HashtableImSt4pairIKmSt10unique_ptrIA_cSt14default_deleteIS3_EEESaIS7_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_ImS6_EEEES0_INS9_14_Node_iteratorIS7_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L228
	call	_ZdaPv@PLT
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L232:
	movabsq	$287762808832, %rcx
	movq	23(%rsi), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L234
	testb	$1, %dl
	jne	.L235
.L238:
	movq	47(%rsi), %rdx
	testb	$62, 43(%rdx)
	jne	.L234
	movq	47(%rsi), %rdx
	movq	31(%rdx), %rdx
	movl	15(%rdx), %edx
	andl	$1, %edx
	xorl	$1, %edx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L235:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L234
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L238
	jmp	.L234
.L254:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18371:
	.size	_ZN2v88internal7ICStats22GetOrCacheFunctionNameENS0_10JSFunctionE, .-_ZN2v88internal7ICStats22GetOrCacheFunctionNameENS0_10JSFunctionE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal7ICStats9instance_E,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal7ICStats9instance_E, @function
_GLOBAL__sub_I__ZN2v88internal7ICStats9instance_E:
.LFB22599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22599:
	.size	_GLOBAL__sub_I__ZN2v88internal7ICStats9instance_E, .-_GLOBAL__sub_I__ZN2v88internal7ICStats9instance_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal7ICStats9instance_E
	.section	.bss._ZZN2v88internal7ICStats4DumpEvE27trace_event_unique_atomic52,"aw",@nobits
	.align 8
	.type	_ZZN2v88internal7ICStats4DumpEvE27trace_event_unique_atomic52, @object
	.size	_ZZN2v88internal7ICStats4DumpEvE27trace_event_unique_atomic52, 8
_ZZN2v88internal7ICStats4DumpEvE27trace_event_unique_atomic52:
	.zero	8
	.globl	_ZN2v88internal7ICStats9instance_E
	.section	.bss._ZN2v88internal7ICStats9instance_E,"aw",@nobits
	.align 32
	.type	_ZN2v88internal7ICStats9instance_E, @object
	.size	_ZN2v88internal7ICStats9instance_E, 160
_ZN2v88internal7ICStats9instance_E:
	.zero	160
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.section	.data.rel.ro,"aw"
	.align 8
.LC14:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC15:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC16:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
