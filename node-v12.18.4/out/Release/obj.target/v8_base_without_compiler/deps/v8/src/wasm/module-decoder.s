	.file	"module-decoder.cc"
	.text
	.section	.text._ZN2v88internal4wasm7Decoder12onFirstErrorEv,"axG",@progbits,_ZN2v88internal4wasm7Decoder12onFirstErrorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.type	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, @function
_ZN2v88internal4wasm7Decoder12onFirstErrorEv:
.LFB6912:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6912:
	.size	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, .-_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl12onFirstErrorEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl12onFirstErrorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl12onFirstErrorEv
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl12onFirstErrorEv, @function
_ZN2v88internal4wasm17ModuleDecoderImpl12onFirstErrorEv:
.LFB19011:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movq	%rax, 16(%rdi)
	ret
	.cfi_endproc
.LFE19011:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl12onFirstErrorEv, .-_ZN2v88internal4wasm17ModuleDecoderImpl12onFirstErrorEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB25295:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25295:
	.size	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB25332:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE25332:
	.size	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB25337:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25337:
	.size	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB25340:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE25340:
	.size	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZN2v88internal4wasm7DecoderD2Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD2Ev
	.type	_ZN2v88internal4wasm7DecoderD2Ev, @function
_ZN2v88internal4wasm7DecoderD2Ev:
.LFB18997:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %r8
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	addq	$64, %rdi
	movq	%rax, -64(%rdi)
	cmpq	%rdi, %r8
	je	.L8
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	ret
	.cfi_endproc
.LFE18997:
	.size	_ZN2v88internal4wasm7DecoderD2Ev, .-_ZN2v88internal4wasm7DecoderD2Ev
	.weak	_ZN2v88internal4wasm7DecoderD1Ev
	.set	_ZN2v88internal4wasm7DecoderD1Ev,_ZN2v88internal4wasm7DecoderD2Ev
	.section	.text._ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB25336:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE25336:
	.size	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB25334:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25334:
	.size	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB25297:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25297:
	.size	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB25339:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25339:
	.size	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN2v88internal4wasm7DecoderD0Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD0Ev
	.type	_ZN2v88internal4wasm7DecoderD0Ev, @function
_ZN2v88internal4wasm7DecoderD0Ev:
.LFB18999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18999:
	.size	_ZN2v88internal4wasm7DecoderD0Ev, .-_ZN2v88internal4wasm7DecoderD0Ev
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImplD0Ev,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImplD0Ev
	.type	_ZN2v88internal4wasm17ModuleDecoderImplD0Ev, @function
_ZN2v88internal4wasm17ModuleDecoderImplD0Ev:
.LFB22798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm17ModuleDecoderImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	136(%rdi), %rdi
	leaq	152(%r12), %rax
	cmpq	%rax, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movq	104(%r12), %r13
	testq	%r13, %r13
	je	.L20
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L21
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L28
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	48(%r12), %rdi
	movq	%rax, (%r12)
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L26
	call	_ZdlPv@PLT
.L26:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$176, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L20
.L28:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L24
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L25:
	cmpl	$1, %eax
	jne	.L20
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L24:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L25
	.cfi_endproc
.LFE22798:
	.size	_ZN2v88internal4wasm17ModuleDecoderImplD0Ev, .-_ZN2v88internal4wasm17ModuleDecoderImplD0Ev
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImplD2Ev,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImplD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImplD2Ev
	.type	_ZN2v88internal4wasm17ModuleDecoderImplD2Ev, @function
_ZN2v88internal4wasm17ModuleDecoderImplD2Ev:
.LFB22796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm17ModuleDecoderImplE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	136(%rdi), %rdi
	leaq	152(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L30
	call	_ZdlPv@PLT
.L30:
	movq	104(%rbx), %r12
	testq	%r12, %r12
	je	.L32
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L33
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L40
	.p2align 4,,10
	.p2align 3
.L32:
	movq	48(%rbx), %rdi
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	addq	$64, %rbx
	movq	%rax, -64(%rbx)
	cmpq	%rbx, %rdi
	je	.L29
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L32
.L40:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L36
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L37:
	cmpl	$1, %eax
	jne	.L32
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L29:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L37
	.cfi_endproc
.LFE22796:
	.size	_ZN2v88internal4wasm17ModuleDecoderImplD2Ev, .-_ZN2v88internal4wasm17ModuleDecoderImplD2Ev
	.weak	_ZN2v88internal4wasm17ModuleDecoderImplD1Ev
	.set	_ZN2v88internal4wasm17ModuleDecoderImplD1Ev,_ZN2v88internal4wasm17ModuleDecoderImplD2Ev
	.section	.text._ZN2v88internal4wasm9WasmErrorC2EjPKcz,"axG",@progbits,_ZN2v88internal4wasm9WasmErrorC5EjPKcz,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm9WasmErrorC2EjPKcz
	.type	_ZN2v88internal4wasm9WasmErrorC2EjPKcz, @function
_ZN2v88internal4wasm9WasmErrorC2EjPKcz:
.LFB5276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	%esi, %edi
	movq	%rdx, %rsi
	subq	$264, %rsp
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L42
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L42:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	24(%rbx), %r13
	movl	%edi, (%rbx)
	leaq	16(%rbp), %rax
	movq	%r13, 8(%rbx)
	leaq	-256(%rbp), %rdi
	leaq	-280(%rbp), %rdx
	movq	$0, 16(%rbx)
	leaq	-240(%rbp), %r12
	movb	$0, 24(%rbx)
	movq	%rax, -272(%rbp)
	leaq	-208(%rbp), %rax
	movl	$24, -280(%rbp)
	movl	$48, -276(%rbp)
	movq	%rax, -264(%rbp)
	call	_ZN2v88internal4wasm9WasmError11FormatErrorB5cxx11EPKcP13__va_list_tag@PLT
	movq	-256(%rbp), %rdx
	movq	8(%rbx), %rdi
	cmpq	%r12, %rdx
	je	.L58
	movq	-248(%rbp), %rax
	movq	-240(%rbp), %rcx
	cmpq	%rdi, %r13
	je	.L59
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	24(%rbx), %rsi
	movq	%rdx, 8(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%rbx)
	testq	%rdi, %rdi
	je	.L48
	movq	%rdi, -256(%rbp)
	movq	%rsi, -240(%rbp)
.L46:
	movq	$0, -248(%rbp)
	movb	$0, (%rdi)
	movq	-256(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L41
	call	_ZdlPv@PLT
.L41:
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, 8(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%rbx)
.L48:
	movq	%r12, -256(%rbp)
	leaq	-240(%rbp), %r12
	movq	%r12, %rdi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L58:
	movq	-248(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L44
	cmpq	$1, %rdx
	je	.L61
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %rdx
	movq	8(%rbx), %rdi
.L44:
	movq	%rdx, 16(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	-256(%rbp), %rdi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L61:
	movzbl	-240(%rbp), %eax
	movb	%al, (%rdi)
	movq	-248(%rbp), %rdx
	movq	8(%rbx), %rdi
	jmp	.L44
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5276:
	.size	_ZN2v88internal4wasm9WasmErrorC2EjPKcz, .-_ZN2v88internal4wasm9WasmErrorC2EjPKcz
	.weak	_ZN2v88internal4wasm9WasmErrorC1EjPKcz
	.set	_ZN2v88internal4wasm9WasmErrorC1EjPKcz,_ZN2v88internal4wasm9WasmErrorC2EjPKcz
	.section	.rodata._ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<unknown>"
.LC1:
	.string	"i32"
.LC2:
	.string	"f32"
.LC3:
	.string	"f64"
.LC4:
	.string	"anyref"
.LC5:
	.string	"funcref"
.LC6:
	.string	"nullref"
.LC7:
	.string	"exn"
.LC8:
	.string	"s128"
.LC9:
	.string	"<stmt>"
.LC10:
	.string	"<bot>"
.LC11:
	.string	"i64"
	.section	.text._ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,comdat
	.p2align 4
	.weak	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	.type	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE, @function
_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE:
.LFB6970:
	.cfi_startproc
	endbr64
	movzbl	%dil, %edx
	cmpb	$10, %dil
	ja	.L63
	leaq	.L65(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,"aG",@progbits,_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,comdat
	.align 4
	.align 4
.L65:
	.long	.L75-.L65
	.long	.L76-.L65
	.long	.L73-.L65
	.long	.L72-.L65
	.long	.L71-.L65
	.long	.L70-.L65
	.long	.L69-.L65
	.long	.L68-.L65
	.long	.L67-.L65
	.long	.L66-.L65
	.long	.L64-.L65
	.section	.text._ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE,comdat
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	.LC1(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	.LC9(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	.LC7(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	.LC8(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	.LC4(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	.LC5(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	.LC6(%rip), %rax
	ret
.L63:
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE6970:
	.size	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE, .-_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	.section	.rodata._ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE.str1.1,"aMS",@progbits,1
.LC12:
	.string	"Unknown"
.LC13:
	.string	"Import"
.LC14:
	.string	"Function"
.LC15:
	.string	"Table"
.LC16:
	.string	"Memory"
.LC17:
	.string	"Global"
.LC18:
	.string	"Export"
.LC19:
	.string	"Start"
.LC20:
	.string	"Code"
.LC21:
	.string	"Element"
.LC22:
	.string	"Data"
.LC23:
	.string	"Exception"
.LC24:
	.string	"DataCount"
.LC25:
	.string	"Type"
	.section	.text._ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE
	.type	_ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE, @function
_ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE:
.LFB18978:
	.cfi_startproc
	endbr64
	movzbl	%dil, %edx
	cmpb	$16, %dil
	ja	.L78
	leaq	.L80(%rip), %rcx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE,"a",@progbits
	.align 4
	.align 4
.L80:
	.long	.L97-.L80
	.long	.L95-.L80
	.long	.L94-.L80
	.long	.L93-.L80
	.long	.L92-.L80
	.long	.L91-.L80
	.long	.L90-.L80
	.long	.L89-.L80
	.long	.L88-.L80
	.long	.L87-.L80
	.long	.L86-.L80
	.long	.L85-.L80
	.long	.L84-.L80
	.long	.L83-.L80
	.long	.L82-.L80
	.long	.L81-.L80
	.long	.L79-.L80
	.section	.text._ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	.LC25(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	.LC12(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L23kCompilationHintsStringE(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L11kNameStringE(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L23kSourceMappingURLStringE(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	.LC20(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	leaq	.LC22(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	.LC24(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	leaq	.LC23(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	.LC13(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	.LC14(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	leaq	.LC15(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	.LC16(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	.LC17(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	.LC18(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	leaq	.LC19(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	.LC21(%rip), %rax
	ret
.L78:
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE18978:
	.size	_ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE, .-_ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE
	.section	.text._ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED2Ev,"axG",@progbits,_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED2Ev
	.type	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED2Ev, @function
_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED2Ev:
.LFB19043:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	leaq	40(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L99
	call	_ZdlPv@PLT
.L99:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L98
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L102
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L108
.L98:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L98
.L108:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L105
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L106:
	cmpl	$1, %eax
	jne	.L98
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L106
	.cfi_endproc
.LFE19043:
	.size	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED2Ev, .-_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED2Ev
	.weak	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED1Ev
	.set	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED1Ev,_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED2Ev
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl22CalculateGlobalOffsetsEPNS1_10WasmModuleE.str1.1,"aMS",@progbits,1
.LC26:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl22CalculateGlobalOffsetsEPNS1_10WasmModuleE,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl22CalculateGlobalOffsetsEPNS1_10WasmModuleE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl22CalculateGlobalOffsetsEPNS1_10WasmModuleE
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl22CalculateGlobalOffsetsEPNS1_10WasmModuleE, @function
_ZN2v88internal4wasm17ModuleDecoderImpl22CalculateGlobalOffsetsEPNS1_10WasmModuleE:
.LFB19072:
	.cfi_startproc
	endbr64
	movq	24(%rsi), %rax
	movq	32(%rsi), %r10
	cmpq	%r10, %rax
	je	.L121
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
	leaq	.L117(%rip), %rdi
	xorl	%r8d, %r8d
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L128:
	cmpb	$0, 28(%rax)
	je	.L111
	movl	%r11d, 24(%rax)
	addl	$1, %r11d
.L112:
	addq	$32, %rax
	cmpq	%rax, %r10
	je	.L110
.L120:
	cmpb	$0, 1(%rax)
	jne	.L128
.L111:
	movzbl	(%rax), %edx
	leal	-6(%rdx), %ecx
	cmpb	$1, %cl
	jbe	.L123
	cmpb	$9, %dl
	jne	.L113
.L123:
	movl	%r9d, 24(%rax)
	addq	$32, %rax
	addl	$1, %r9d
	cmpq	%rax, %r10
	jne	.L120
.L110:
	movl	%r8d, 48(%rsi)
	movl	%r9d, 52(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	cmpb	$7, %dl
	ja	.L115
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl22CalculateGlobalOffsetsEPNS1_10WasmModuleE,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl22CalculateGlobalOffsetsEPNS1_10WasmModuleE,comdat
	.align 4
	.align 4
.L117:
	.long	.L115-.L117
	.long	.L122-.L117
	.long	.L116-.L117
	.long	.L122-.L117
	.long	.L116-.L117
	.long	.L118-.L117
	.long	.L116-.L117
	.long	.L116-.L117
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl22CalculateGlobalOffsetsEPNS1_10WasmModuleE,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl22CalculateGlobalOffsetsEPNS1_10WasmModuleE,comdat
.L122:
	movl	$-4, %ecx
	movl	$4, %edx
.L119:
	leal	-1(%r8,%rdx), %r8d
	andl	%r8d, %ecx
	movl	%ecx, 24(%rax)
	leal	(%rcx,%rdx), %r8d
	jmp	.L112
.L116:
	movl	$-8, %ecx
	movl	$8, %edx
	jmp	.L119
.L115:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
.L118:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	$-16, %ecx
	movl	$16, %edx
	jmp	.L119
.L121:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	jmp	.L110
	.cfi_endproc
.LFE19072:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl22CalculateGlobalOffsetsEPNS1_10WasmModuleE, .-_ZN2v88internal4wasm17ModuleDecoderImpl22CalculateGlobalOffsetsEPNS1_10WasmModuleE
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl18VerifyFunctionBodyEPNS0_19AccountingAllocatorEjRKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPNS1_12WasmFunctionE.str1.1,"aMS",@progbits,1
.LC27:
	.string	"in function "
.LC28:
	.string	": "
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl18VerifyFunctionBodyEPNS0_19AccountingAllocatorEjRKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPNS1_12WasmFunctionE,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl18VerifyFunctionBodyEPNS0_19AccountingAllocatorEjRKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPNS1_12WasmFunctionE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl18VerifyFunctionBodyEPNS0_19AccountingAllocatorEjRKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPNS1_12WasmFunctionE
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl18VerifyFunctionBodyEPNS0_19AccountingAllocatorEjRKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPNS1_12WasmFunctionE, @function
_ZN2v88internal4wasm17ModuleDecoderImpl18VerifyFunctionBodyEPNS0_19AccountingAllocatorEjRKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPNS1_12WasmFunctionE:
.LFB19073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rcx, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$664, %rsp
	movq	%rsi, -688(%rbp)
	movq	%r9, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal4wasm15ModuleWireBytes13GetNameOrNullEPKNS1_12WasmFunctionEPKNS1_10WasmModuleE@PLT
	movl	16(%rbx), %ecx
	movl	32(%r13), %esi
	movq	%rbx, -656(%rbp)
	movq	%rdx, -640(%rbp)
	movl	20(%rbx), %edx
	movq	%rax, -648(%rbp)
	movq	8(%r13), %rax
	addl	%ecx, %edx
	movq	(%rbx), %rdi
	movl	%ecx, -584(%rbp)
	subl	%esi, %ecx
	subl	%esi, %edx
	leaq	-448(%rbp), %rbx
	movq	$0, -480(%rbp)
	addq	%rax, %rdx
	addq	%rcx, %rax
	cmpb	$0, 168(%r13)
	movq	%rdi, -592(%rbp)
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	112(%r13), %rax
	movl	$0, -472(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, -464(%rbp)
	movq	$0, -456(%rbp)
	movb	$0, -448(%rbp)
	movaps	%xmm0, -576(%rbp)
	jne	.L130
	leaq	4344(%rax), %rdi
.L131:
	leaq	-624(%rbp), %r15
	xorl	%edx, %edx
	movq	%rdi, -616(%rbp)
	leaq	-432(%rbp), %r12
	movq	$0, -624(%rbp)
	movq	%r15, %rsi
	movq	$0, -608(%rbp)
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	leaq	80(%r13), %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	-688(%rbp), %rsi
	leaq	-592(%rbp), %r9
	leaq	-669(%rbp), %r8
	movq	$0, -669(%rbp)
	movl	$0, -661(%rbp)
	movb	$0, -657(%rbp)
	call	_ZN2v88internal4wasm14VerifyWasmCodeEPNS0_19AccountingAllocatorERKNS1_12WasmFeaturesEPKNS1_10WasmModuleEPS4_RKNS1_12FunctionBodyE@PLT
	movq	-432(%rbp), %rax
	leaq	-464(%rbp), %rdi
	leaq	-416(%rbp), %rsi
	movq	%rax, -480(%rbp)
	movl	-424(%rbp), %eax
	movl	%eax, -472(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_@PLT
	movq	-416(%rbp), %rdi
	leaq	-400(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L132
	call	_ZdlPv@PLT
.L132:
	movq	-608(%rbp), %rdx
	movq	-616(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	cmpq	$0, -456(%rbp)
	je	.L133
	cmpq	$0, 144(%r13)
	je	.L145
.L133:
	movq	-464(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L129
	call	_ZdlPv@PLT
.L129:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L146
	addq	$664, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	leaq	4296(%rax), %rdi
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L145:
	movq	.LC29(%rip), %xmm1
	leaq	-320(%rbp), %r14
	leaq	-368(%rbp), %r15
	movq	%r14, %rdi
	movhps	.LC30(%rip), %xmm1
	movaps	%xmm1, -688(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	%rax, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-688(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r14, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -688(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$12, %edx
	movq	%r12, %rdi
	leaq	.LC27(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	leaq	-656(%rbp), %rsi
	call	_ZN2v88internal4wasmlsERSoRKNS1_16WasmFunctionNameE@PLT
	movl	$2, %edx
	leaq	.LC28(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-456(%rbp), %rdx
	movq	-464(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-544(%rbp), %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-384(%rbp), %rax
	movq	%r12, -560(%rbp)
	leaq	-560(%rbp), %rdi
	movb	$0, -544(%rbp)
	movl	-472(%rbp), %r9d
	movq	$0, -552(%rbp)
	testq	%rax, %rax
	je	.L134
	movq	-400(%rbp), %r8
	movl	%r9d, -696(%rbp)
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L135
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movl	-696(%rbp), %r9d
.L136:
	movq	-560(%rbp), %rax
	leaq	-504(%rbp), %rdx
	movl	%r9d, -528(%rbp)
	movq	%rdx, -520(%rbp)
	cmpq	%r12, %rax
	je	.L147
	movq	%rax, -520(%rbp)
	movq	-544(%rbp), %rax
	movq	%rax, -504(%rbp)
.L138:
	movl	%r9d, 128(%r13)
	movq	-552(%rbp), %rax
	leaq	136(%r13), %rdi
	leaq	-520(%rbp), %rsi
	movq	%rdx, -696(%rbp)
	movq	%rax, -512(%rbp)
	movq	%r12, -560(%rbp)
	movq	$0, -552(%rbp)
	movb	$0, -544(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_@PLT
	movq	-520(%rbp), %rdi
	movq	-696(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L139
	call	_ZdlPv@PLT
.L139:
	movq	-560(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	movq	.LC29(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC31(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-688(%rbp), %rdi
	je	.L141
	call	_ZdlPv@PLT
.L141:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L135:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movl	-696(%rbp), %r9d
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L147:
	movdqa	-544(%rbp), %xmm3
	movups	%xmm3, -504(%rbp)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L134:
	leaq	-352(%rbp), %rsi
	movl	%r9d, -696(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	-696(%rbp), %r9d
	jmp	.L136
.L146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19073:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl18VerifyFunctionBodyEPNS0_19AccountingAllocatorEjRKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPNS1_12WasmFunctionE, .-_ZN2v88internal4wasm17ModuleDecoderImpl18VerifyFunctionBodyEPNS0_19AccountingAllocatorEjRKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPNS1_12WasmFunctionE
	.section	.text._ZN2v88internal4wasm13ModuleDecoderC2ERKNS1_12WasmFeaturesE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm13ModuleDecoderC2ERKNS1_12WasmFeaturesE
	.type	_ZN2v88internal4wasm13ModuleDecoderC2ERKNS1_12WasmFeaturesE, @function
_ZN2v88internal4wasm13ModuleDecoderC2ERKNS1_12WasmFeaturesE:
.LFB19115:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movl	8(%rsi), %eax
	movl	%eax, 8(%rdi)
	movzbl	12(%rsi), %eax
	movq	$0, 16(%rdi)
	movb	%al, 12(%rdi)
	ret
	.cfi_endproc
.LFE19115:
	.size	_ZN2v88internal4wasm13ModuleDecoderC2ERKNS1_12WasmFeaturesE, .-_ZN2v88internal4wasm13ModuleDecoderC2ERKNS1_12WasmFeaturesE
	.globl	_ZN2v88internal4wasm13ModuleDecoderC1ERKNS1_12WasmFeaturesE
	.set	_ZN2v88internal4wasm13ModuleDecoderC1ERKNS1_12WasmFeaturesE,_ZN2v88internal4wasm13ModuleDecoderC2ERKNS1_12WasmFeaturesE
	.section	.text._ZN2v88internal4wasm13ModuleDecoderD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm13ModuleDecoderD2Ev
	.type	_ZN2v88internal4wasm13ModuleDecoderD2Ev, @function
_ZN2v88internal4wasm13ModuleDecoderD2Ev:
.LFB19118:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L149
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L149:
	ret
	.cfi_endproc
.LFE19118:
	.size	_ZN2v88internal4wasm13ModuleDecoderD2Ev, .-_ZN2v88internal4wasm13ModuleDecoderD2Ev
	.globl	_ZN2v88internal4wasm13ModuleDecoderD1Ev
	.set	_ZN2v88internal4wasm13ModuleDecoderD1Ev,_ZN2v88internal4wasm13ModuleDecoderD2Ev
	.section	.text._ZNK2v88internal4wasm13ModuleDecoder13shared_moduleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal4wasm13ModuleDecoder13shared_moduleEv
	.type	_ZNK2v88internal4wasm13ModuleDecoder13shared_moduleEv, @function
_ZNK2v88internal4wasm13ModuleDecoder13shared_moduleEv:
.LFB19120:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	addq	$96, %rax
	ret
	.cfi_endproc
.LFE19120:
	.size	_ZNK2v88internal4wasm13ModuleDecoder13shared_moduleEv, .-_ZNK2v88internal4wasm13ModuleDecoder13shared_moduleEv
	.section	.rodata._ZN2v88internal4wasm13ModuleDecoder13StartDecodingEPNS0_8CountersEPNS0_19AccountingAllocatorENS1_12ModuleOriginE.str1.1,"aMS",@progbits,1
.LC32:
	.string	"(module_) == nullptr"
.LC33:
	.string	"Check failed: %s."
.LC34:
	.string	"signatures"
	.section	.text._ZN2v88internal4wasm13ModuleDecoder13StartDecodingEPNS0_8CountersEPNS0_19AccountingAllocatorENS1_12ModuleOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm13ModuleDecoder13StartDecodingEPNS0_8CountersEPNS0_19AccountingAllocatorENS1_12ModuleOriginE
	.type	_ZN2v88internal4wasm13ModuleDecoder13StartDecodingEPNS0_8CountersEPNS0_19AccountingAllocatorENS1_12ModuleOriginE, @function
_ZN2v88internal4wasm13ModuleDecoder13StartDecodingEPNS0_8CountersEPNS0_19AccountingAllocatorENS1_12ModuleOriginE:
.LFB19121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$176, %edi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movl	$1, %ecx
	movq	16(%r12), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, 8(%rax)
	movq	%rax, %rbx
	addq	$64, %rax
	movq	$0, -48(%rax)
	movq	$0, -40(%rax)
	movl	$0, -32(%rax)
	movl	$0, -24(%rax)
	cmpb	$0, _ZN2v88internal24FLAG_assume_asmjs_originE(%rip)
	movq	%rax, 48(%rbx)
	leaq	16+_ZTVN2v88internal4wasm17ModuleDecoderImplE(%rip), %rax
	cmovne	%ecx, %r13d
	movq	%rax, (%rbx)
	movq	(%r12), %rax
	movq	$0, 56(%rbx)
	movq	%rax, 80(%rbx)
	movl	8(%r12), %eax
	movb	$0, 64(%rbx)
	movl	%eax, 88(%rbx)
	movzbl	12(%r12), %eax
	movq	$0, 112(%rbx)
	movb	%al, 92(%rbx)
	leaq	152(%rbx), %rax
	movb	$1, 120(%rbx)
	movq	$0, 124(%rbx)
	movq	%rax, 136(%rbx)
	movq	$0, 144(%rbx)
	movb	$0, 152(%rbx)
	movb	%r13b, 168(%rbx)
	movq	%rbx, 16(%r12)
	movups	%xmm0, 96(%rbx)
	testq	%rdi, %rdi
	je	.L154
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	16(%r12), %rbx
	cmpq	$0, 96(%rbx)
	jne	.L172
.L154:
	movq	%r15, 112(%rbx)
	movl	$64, %edi
	call	_Znwm@PLT
	leaq	.LC34(%rip), %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movl	$440, %edi
	movq	%r12, -64(%rbp)
	call	_Znwm@PLT
	leaq	-64(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal4wasm10WasmModuleC1ESt10unique_ptrINS0_4ZoneESt14default_deleteIS4_EE@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rdx
	movq	%r12, %xmm0
	movabsq	$4294967297, %rcx
	movq	%rcx, 8(%rax)
	movq	%rax, %xmm1
	movq	%rdx, (%rax)
	punpcklqdq	%xmm1, %xmm0
	movq	%r12, 16(%rax)
	movq	104(%rbx), %r13
	movups	%xmm0, 96(%rbx)
	testq	%r13, %r13
	je	.L156
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	testq	%r12, %r12
	je	.L157
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L173
	.p2align 4,,10
	.p2align 3
.L156:
	movq	-64(%rbp), %r12
	testq	%r12, %r12
	je	.L162
	movq	%r12, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L162:
	movq	96(%rbx), %rax
	movl	$0, 8(%rax)
	movq	96(%rbx), %rax
	movl	$0, 12(%rax)
	movq	96(%rbx), %rax
	movb	$0, 19(%rax)
	movq	96(%rbx), %rax
	movzbl	168(%rbx), %edx
	movb	%dl, 392(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L156
.L173:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L160
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L161:
	cmpl	$1, %eax
	jne	.L156
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	.LC32(%rip), %rsi
	leaq	.LC33(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L160:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L161
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19121:
	.size	_ZN2v88internal4wasm13ModuleDecoder13StartDecodingEPNS0_8CountersEPNS0_19AccountingAllocatorENS1_12ModuleOriginE, .-_ZN2v88internal4wasm13ModuleDecoder13StartDecodingEPNS0_8CountersEPNS0_19AccountingAllocatorENS1_12ModuleOriginE
	.section	.text._ZN2v88internal4wasm13ModuleDecoder18DecodeFunctionBodyEjjjb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm13ModuleDecoder18DecodeFunctionBodyEjjjb
	.type	_ZN2v88internal4wasm13ModuleDecoder18DecodeFunctionBodyEjjjb, @function
_ZN2v88internal4wasm13ModuleDecoder18DecodeFunctionBodyEjjjb:
.LFB19124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	96(%rdi), %rax
	addl	60(%rax), %r9d
	salq	$5, %r9
	addq	136(%rax), %r9
	movl	%ecx, 16(%r9)
	movl	%edx, 20(%r9)
	testb	%r8b, %r8b
	jne	.L179
.L175:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	8(%rdi), %rdx
	movq	24(%rdi), %rax
	leaq	-32(%rbp), %rcx
	movq	96(%rdi), %r8
	subq	%rdx, %rax
	movq	%rdx, -32(%rbp)
	cltq
	movl	60(%r8), %edx
	movq	%rax, -24(%rbp)
	movq	(%r8), %rax
	addl	%esi, %edx
	movq	32(%rax), %rsi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl18VerifyFunctionBodyEPNS0_19AccountingAllocatorEjRKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPNS1_12WasmFunctionE
	jmp	.L175
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19124:
	.size	_ZN2v88internal4wasm13ModuleDecoder18DecodeFunctionBodyEjjjb, .-_ZN2v88internal4wasm13ModuleDecoder18DecodeFunctionBodyEjjjb
	.section	.text._ZN2v88internal4wasm13ModuleDecoder2okEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm13ModuleDecoder2okEv
	.type	_ZN2v88internal4wasm13ModuleDecoder2okEv, @function
_ZN2v88internal4wasm13ModuleDecoder2okEv:
.LFB19130:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	cmpq	$0, 56(%rax)
	sete	%al
	ret
	.cfi_endproc
.LFE19130:
	.size	_ZN2v88internal4wasm13ModuleDecoder2okEv, .-_ZN2v88internal4wasm13ModuleDecoder2okEv
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv:
.LFB21606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L183
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
	cmpl	$1, %eax
	je	.L191
.L182:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	cmpl	$1, %eax
	jne	.L182
.L191:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L187
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L188:
	cmpl	$1, %eax
	jne	.L182
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L188
	.cfi_endproc
.LFE21606:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
	.section	.rodata._ZNSt6vectorIPN2v88internal9SignatureINS1_4wasm9ValueTypeEEESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_.str1.1,"aMS",@progbits,1
.LC35:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN2v88internal9SignatureINS1_4wasm9ValueTypeEEESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal9SignatureINS1_4wasm9ValueTypeEEESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal9SignatureINS1_4wasm9ValueTypeEEESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal9SignatureINS1_4wasm9ValueTypeEEESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal9SignatureINS1_4wasm9ValueTypeEEESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_:
.LFB22560:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L206
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L202
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L207
.L194:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L201:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L208
	testq	%r13, %r13
	jg	.L197
	testq	%r9, %r9
	jne	.L200
.L198:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L197
.L200:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L207:
	testq	%rsi, %rsi
	jne	.L195
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L198
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L202:
	movl	$8, %r14d
	jmp	.L194
.L206:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L195:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L194
	.cfi_endproc
.LFE22560:
	.size	_ZNSt6vectorIPN2v88internal9SignatureINS1_4wasm9ValueTypeEEESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, .-_ZNSt6vectorIPN2v88internal9SignatureINS1_4wasm9ValueTypeEEESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.section	.text._ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	.type	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_, @function
_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_:
.LFB22564:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L223
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L219
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L224
.L211:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L218:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L225
	testq	%r13, %r13
	jg	.L214
	testq	%r9, %r9
	jne	.L217
.L215:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L214
.L217:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L224:
	testq	%rsi, %rsi
	jne	.L212
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L215
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$4, %r14d
	jmp	.L211
.L223:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L212:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L211
	.cfi_endproc
.LFE22564:
	.size	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_, .-_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm9WasmTableESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm9WasmTableESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm9WasmTableESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm9WasmTableESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm9WasmTableESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB22589:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L245
	movq	%rsi, %rdx
	movq	%rdi, %r15
	movq	%rsi, %rbx
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L236
	movabsq	$9223372036854775792, %r10
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L246
.L228:
	movq	%r10, %rdi
	movq	%rdx, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %rdx
	movq	%rax, %r13
	addq	%rax, %r10
	leaq	16(%rax), %rax
.L235:
	addq	%r13, %rdx
	xorl	%ecx, %ecx
	movb	$0, (%rdx)
	movq	$0, 4(%rdx)
	movw	%cx, 12(%rdx)
	movb	$0, 14(%rdx)
	cmpq	%r14, %rbx
	je	.L230
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L231:
	movzbl	(%rax), %r11d
	movl	4(%rax), %r9d
	addq	$16, %rax
	addq	$16, %rdx
	movl	-8(%rax), %r8d
	movzbl	-4(%rax), %edi
	movzbl	-3(%rax), %esi
	movzbl	-2(%rax), %ecx
	movb	%r11b, -16(%rdx)
	movl	%r9d, -12(%rdx)
	movl	%r8d, -8(%rdx)
	movb	%dil, -4(%rdx)
	movb	%sil, -3(%rdx)
	movb	%cl, -2(%rdx)
	cmpq	%rax, %rbx
	jne	.L231
	movq	%rbx, %rax
	subq	%r14, %rax
	leaq	16(%r13,%rax), %rax
.L230:
	cmpq	%r12, %rbx
	je	.L232
	movq	%rax, %rcx
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L233:
	movzbl	12(%rdx), %edi
	movl	4(%rdx), %r9d
	addq	$16, %rdx
	addq	$16, %rcx
	movl	-8(%rdx), %r8d
	movzbl	-3(%rdx), %esi
	movzbl	-16(%rdx), %r11d
	movb	%dil, -4(%rcx)
	movzbl	-2(%rdx), %edi
	movl	%r9d, -12(%rcx)
	movb	%r11b, -16(%rcx)
	movl	%r8d, -8(%rcx)
	movb	%sil, -3(%rcx)
	movb	%dil, -2(%rcx)
	cmpq	%r12, %rdx
	jne	.L233
	subq	%rbx, %rdx
	addq	%rdx, %rax
.L232:
	testq	%r14, %r14
	je	.L234
	movq	%r14, %rdi
	movq	%r10, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %rax
.L234:
	movq	%r13, %xmm0
	movq	%rax, %xmm1
	movq	%r10, 16(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L229
	movl	$16, %eax
	xorl	%r10d, %r10d
	xorl	%r13d, %r13d
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L236:
	movl	$16, %r10d
	jmp	.L228
.L229:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r10
	salq	$4, %r10
	jmp	.L228
.L245:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22589:
	.size	_ZNSt6vectorIN2v88internal4wasm9WasmTableESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm9WasmTableESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm13WasmExceptionESaIS3_EE17_M_realloc_insertIJRPNS1_9SignatureINS2_9ValueTypeEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm13WasmExceptionESaIS3_EE17_M_realloc_insertIJRPNS1_9SignatureINS2_9ValueTypeEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm13WasmExceptionESaIS3_EE17_M_realloc_insertIJRPNS1_9SignatureINS2_9ValueTypeEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm13WasmExceptionESaIS3_EE17_M_realloc_insertIJRPNS1_9SignatureINS2_9ValueTypeEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm13WasmExceptionESaIS3_EE17_M_realloc_insertIJRPNS1_9SignatureINS2_9ValueTypeEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB22599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movabsq	$1152921504606846975, %rsi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r12
	movq	%rcx, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L274
	movq	%r13, %r9
	movq	%rdi, %r15
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L259
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L275
.L249:
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L258:
	movq	(%rdx), %rax
	movq	%rax, (%rbx,%r9)
	cmpq	%r12, %r13
	je	.L251
	leaq	-8(%r13), %rsi
	leaq	15(%rbx), %rax
	subq	%r12, %rsi
	subq	%r12, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L261
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L261
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L253:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L253
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%r8, %rdi
	je	.L255
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L255:
	leaq	16(%rbx,%rsi), %r14
.L251:
	cmpq	%rcx, %r13
	je	.L256
	subq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r14
.L256:
	testq	%r12, %r12
	je	.L257
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L257:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L250
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L259:
	movl	$8, %r14d
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L252:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %r13
	jne	.L252
	jmp	.L255
.L250:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r14
	salq	$3, %r14
	jmp	.L249
.L274:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22599:
	.size	_ZNSt6vectorIN2v88internal4wasm13WasmExceptionESaIS3_EE17_M_realloc_insertIJRPNS1_9SignatureINS2_9ValueTypeEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm13WasmExceptionESaIS3_EE17_M_realloc_insertIJRPNS1_9SignatureINS2_9ValueTypeEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm,"axG",@progbits,_ZN2v88internal4wasm19TruncatedUserStringILi50EEC5EPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm
	.type	_ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm, @function
_ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm:
.LFB22658:
	.cfi_startproc
	endbr64
	cmpl	$50, %edx
	movl	$50, %eax
	movq	%rsi, (%rdi)
	cmovl	%edx, %eax
	movl	%eax, 8(%rdi)
	cmpq	$50, %rdx
	ja	.L279
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	movdqu	(%rsi), %xmm0
	leaq	12(%rdi), %rax
	movups	%xmm0, 12(%rdi)
	movdqu	16(%rsi), %xmm1
	movups	%xmm1, 28(%rdi)
	movq	32(%rsi), %rdx
	movq	%rdx, 44(%rdi)
	movl	40(%rsi), %edx
	movl	%edx, 52(%rdi)
	movzwl	44(%rsi), %edx
	movw	%dx, 56(%rdi)
	movzbl	46(%rsi), %edx
	movb	$46, 61(%rdi)
	movb	%dl, 58(%rdi)
	movl	$11822, %edx
	movw	%dx, 59(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE22658:
	.size	_ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm, .-_ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm
	.weak	_ZN2v88internal4wasm19TruncatedUserStringILi50EEC1EPKcm
	.set	_ZN2v88internal4wasm19TruncatedUserStringILi50EEC1EPKcm,_ZN2v88internal4wasm19TruncatedUserStringILi50EEC2EPKcm
	.section	.text._ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJRjRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJRjRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJRjRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJRjRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJRjRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB22662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r13
	movq	%rdi, -72(%rbp)
	movabsq	$7905747460161236407, %rdi
	movq	%rbx, %rax
	subq	%r13, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movabsq	$164703072086692425, %rdi
	cmpq	%rdi, %rax
	je	.L301
	movq	%rsi, %r9
	movq	%rsi, %r12
	subq	%r13, %r9
	testq	%rax, %rax
	je	.L293
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L302
.L282:
	movq	%r15, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %rdx
	addq	%rax, %r15
	movq	%rax, -56(%rbp)
	movq	-96(%rbp), %rcx
	leaq	56(%rax), %r8
	movq	%r15, -64(%rbp)
.L292:
	movq	-56(%rbp), %r14
	movl	(%rdx), %edx
	pxor	%xmm0, %xmm0
	movdqu	(%rcx), %xmm5
	leaq	(%r14,%r9), %rax
	movl	%edx, (%rax)
	movq	$0, 40(%rax)
	movb	$1, 48(%rax)
	movups	%xmm5, 8(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	%r13, %r12
	je	.L284
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L288:
	movl	(%r15), %ecx
	movl	%ecx, (%r14)
	movdqu	8(%r15), %xmm1
	movups	%xmm1, 8(%r14)
	movdqu	24(%r15), %xmm2
	movups	%xmm2, 24(%r14)
	movq	40(%r15), %rcx
	movq	%rcx, 40(%r14)
	movzbl	48(%r15), %ecx
	movups	%xmm0, 24(%r15)
	movq	$0, 40(%r15)
	movb	%cl, 48(%r14)
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L285
	call	_ZdlPv@PLT
	addq	$56, %r15
	addq	$56, %r14
	pxor	%xmm0, %xmm0
	cmpq	%r15, %r12
	jne	.L288
.L286:
	leaq	-56(%r12), %rax
	movq	-56(%rbp), %rsi
	movabsq	$988218432520154551, %rdx
	subq	%r13, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$2, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	leaq	(%rsi,%rdx,8), %r8
.L284:
	cmpq	%rbx, %r12
	je	.L289
	movq	%r12, %rax
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L290:
	movl	(%rax), %ecx
	movdqu	8(%rax), %xmm3
	addq	$56, %rax
	addq	$56, %rdx
	movdqu	-32(%rax), %xmm4
	movl	%ecx, -56(%rdx)
	movq	-16(%rax), %rcx
	movups	%xmm3, -48(%rdx)
	movq	%rcx, -16(%rdx)
	movzbl	-8(%rax), %ecx
	movups	%xmm4, -32(%rdx)
	movb	%cl, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L290
	movabsq	$988218432520154551, %rdx
	subq	%r12, %rax
	subq	$56, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	leaq	(%r8,%rdx,8), %r8
.L289:
	testq	%r13, %r13
	je	.L291
	movq	%r13, %rdi
	movq	%r8, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r8
.L291:
	movq	-56(%rbp), %xmm0
	movq	-72(%rbp), %rax
	movq	%r8, %xmm6
	movq	-64(%rbp), %rsi
	punpcklqdq	%xmm6, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	addq	$56, %r15
	addq	$56, %r14
	cmpq	%r15, %r12
	jne	.L288
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L302:
	testq	%r8, %r8
	jne	.L283
	movq	$0, -64(%rbp)
	movl	$56, %r8d
	movq	$0, -56(%rbp)
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L293:
	movl	$56, %r15d
	jmp	.L282
.L283:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	imulq	$56, %rdi, %r15
	jmp	.L282
.L301:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22662:
	.size	_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJRjRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJRjRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB22666:
	.cfi_startproc
	endbr64
	movabsq	$7905747460161236407, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movabsq	$164703072086692425, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	(%rdi), %r13
	movq	%rdi, -72(%rbp)
	movq	%rbx, %rax
	subq	%r13, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L324
	movq	%rsi, %rdx
	movq	%rsi, %r12
	subq	%r13, %rdx
	testq	%rax, %rax
	je	.L316
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L325
.L305:
	movq	%r15, %rdi
	movq	%rdx, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rdx
	addq	%rax, %r15
	movq	%rax, -56(%rbp)
	leaq	56(%rax), %r8
	movq	%r15, -64(%rbp)
.L315:
	movq	-56(%rbp), %r14
	pxor	%xmm0, %xmm0
	leaq	(%r14,%rdx), %rax
	movl	$0, (%rax)
	movl	$0, 8(%rax)
	movq	$0, 40(%rax)
	movb	$0, 48(%rax)
	movups	%xmm0, 24(%rax)
	cmpq	%r13, %r12
	je	.L307
	movq	%r13, %r15
	.p2align 4,,10
	.p2align 3
.L311:
	movl	(%r15), %ecx
	movl	%ecx, (%r14)
	movdqu	8(%r15), %xmm1
	movups	%xmm1, 8(%r14)
	movdqu	24(%r15), %xmm2
	movups	%xmm2, 24(%r14)
	movq	40(%r15), %rcx
	movq	%rcx, 40(%r14)
	movzbl	48(%r15), %ecx
	movups	%xmm0, 24(%r15)
	movq	$0, 40(%r15)
	movb	%cl, 48(%r14)
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.L308
	call	_ZdlPv@PLT
	addq	$56, %r15
	addq	$56, %r14
	pxor	%xmm0, %xmm0
	cmpq	%r15, %r12
	jne	.L311
.L309:
	leaq	-56(%r12), %rax
	movq	-56(%rbp), %rsi
	movabsq	$988218432520154551, %rdx
	subq	%r13, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$2, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	leaq	(%rsi,%rdx,8), %r8
.L307:
	cmpq	%rbx, %r12
	je	.L312
	movq	%r12, %rax
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L313:
	movl	(%rax), %ecx
	movdqu	8(%rax), %xmm3
	addq	$56, %rax
	addq	$56, %rdx
	movdqu	-32(%rax), %xmm4
	movl	%ecx, -56(%rdx)
	movq	-16(%rax), %rcx
	movups	%xmm3, -48(%rdx)
	movq	%rcx, -16(%rdx)
	movzbl	-8(%rax), %ecx
	movups	%xmm4, -32(%rdx)
	movb	%cl, -8(%rdx)
	cmpq	%rbx, %rax
	jne	.L313
	movabsq	$988218432520154551, %rdx
	subq	%r12, %rax
	subq	$56, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	leaq	(%r8,%rdx,8), %r8
.L312:
	testq	%r13, %r13
	je	.L314
	movq	%r13, %rdi
	movq	%r8, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r8
.L314:
	movq	-56(%rbp), %xmm0
	movq	-72(%rbp), %rax
	movq	%r8, %xmm5
	movq	-64(%rbp), %rsi
	punpcklqdq	%xmm5, %xmm0
	movq	%rsi, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	addq	$56, %r15
	addq	$56, %r14
	cmpq	%r15, %r12
	jne	.L311
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L325:
	testq	%rdi, %rdi
	jne	.L306
	movq	$0, -64(%rbp)
	movl	$56, %r8d
	movq	$0, -56(%rbp)
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L316:
	movl	$56, %r15d
	jmp	.L305
.L306:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	imulq	$56, %rcx, %r15
	jmp	.L305
.L324:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22666:
	.size	_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB22680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r14
	movabsq	$288230376151711743, %rdi
	movq	%rcx, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L343
	movq	%rsi, %r12
	subq	%r14, %rsi
	testq	%rax, %rax
	je	.L335
	movabsq	$9223372036854775776, %r13
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L344
.L328:
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	leaq	(%rax,%r13), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	32(%rbx), %r13
.L334:
	movdqu	(%rdx), %xmm3
	leaq	(%rbx,%rsi), %rax
	movq	$0, 16(%rax)
	movb	$1, 24(%rax)
	movups	%xmm3, (%rax)
	cmpq	%r14, %r12
	je	.L330
	movq	%rbx, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L331:
	movdqu	(%rax), %xmm1
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rax), %xmm2
	movups	%xmm2, -16(%rdx)
	cmpq	%rax, %r12
	jne	.L331
	movq	%r12, %rax
	subq	%r14, %rax
	leaq	32(%rbx,%rax), %r13
.L330:
	cmpq	%rcx, %r12
	je	.L332
	subq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r13
.L332:
	testq	%r14, %r14
	je	.L333
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L333:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r13, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L329
	movq	$0, -56(%rbp)
	movl	$32, %r13d
	xorl	%ebx, %ebx
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L335:
	movl	$32, %r13d
	jmp	.L328
.L329:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	movq	%rdi, %r13
	salq	$5, %r13
	jmp	.L328
.L343:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22680:
	.size	_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB22682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movabsq	$288230376151711743, %rsi
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r14
	movq	%rcx, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L362
	movq	%r12, %rdx
	movq	%rdi, %r15
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L354
	movabsq	$9223372036854775776, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L363
.L347:
	movq	%r13, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rax, %rbx
	leaq	(%rax,%r13), %rax
	movq	%rax, -56(%rbp)
	leaq	32(%rbx), %r13
.L353:
	leaq	(%rbx,%rdx), %rax
	movl	$0, (%rax)
	movq	$0, 16(%rax)
	movb	$0, 24(%rax)
	cmpq	%r14, %r12
	je	.L349
	movq	%rbx, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L350:
	movdqu	(%rax), %xmm1
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rax), %xmm2
	movups	%xmm2, -16(%rdx)
	cmpq	%rax, %r12
	jne	.L350
	movq	%r12, %rax
	subq	%r14, %rax
	leaq	32(%rbx,%rax), %r13
.L349:
	cmpq	%rcx, %r12
	je	.L351
	subq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r13
.L351:
	testq	%r14, %r14
	je	.L352
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L352:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r13, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L348
	movq	$0, -56(%rbp)
	movl	$32, %r13d
	xorl	%ebx, %ebx
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L354:
	movl	$32, %r13d
	jmp	.L347
.L348:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r13
	salq	$5, %r13
	jmp	.L347
.L362:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22682:
	.size	_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZN2v88internal4wasm10WasmModuleD2Ev,"axG",@progbits,_ZN2v88internal4wasm10WasmModuleD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm10WasmModuleD2Ev
	.type	_ZN2v88internal4wasm10WasmModuleD2Ev, @function
_ZN2v88internal4wasm10WasmModuleD2Ev:
.LFB22714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	424(%rbx), %rax
	subq	$8, %rsp
	movq	408(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	movq	400(%rbx), %r13
	testq	%r13, %r13
	je	.L366
	movq	16(%r13), %r12
	testq	%r12, %r12
	je	.L370
	.p2align 4,,10
	.p2align 3
.L367:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L367
.L370:
	movq	8(%r13), %rax
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	0(%r13), %rdi
	leaq	48(%r13), %rax
	movq	$0, 24(%r13)
	movq	$0, 16(%r13)
	cmpq	%rax, %rdi
	je	.L368
	call	_ZdlPv@PLT
.L368:
	movl	$56, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L366:
	movq	352(%rbx), %r12
	testq	%r12, %r12
	je	.L374
	.p2align 4,,10
	.p2align 3
.L371:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L371
.L374:
	movq	344(%rbx), %rax
	movq	336(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	336(%rbx), %rdi
	leaq	384(%rbx), %rax
	movq	$0, 360(%rbx)
	movq	$0, 352(%rbx)
	cmpq	%rax, %rdi
	je	.L372
	call	_ZdlPv@PLT
.L372:
	movq	304(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L375
	call	_ZdlPv@PLT
.L375:
	movq	288(%rbx), %r13
	movq	280(%rbx), %r12
	cmpq	%r12, %r13
	je	.L376
	.p2align 4,,10
	.p2align 3
.L380:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L377
	call	_ZdlPv@PLT
	addq	$56, %r12
	cmpq	%r12, %r13
	jne	.L380
.L378:
	movq	280(%rbx), %r12
.L376:
	testq	%r12, %r12
	je	.L381
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L381:
	movq	256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L382
	call	_ZdlPv@PLT
.L382:
	movq	232(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L383
	call	_ZdlPv@PLT
.L383:
	movq	208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L384
	call	_ZdlPv@PLT
.L384:
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L385
	call	_ZdlPv@PLT
.L385:
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L386
	call	_ZdlPv@PLT
.L386:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L387
	call	_ZdlPv@PLT
.L387:
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L388
	call	_ZdlPv@PLT
.L388:
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L389
	call	_ZdlPv@PLT
.L389:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L390
	call	_ZdlPv@PLT
.L390:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L364
	movq	%r12, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	addq	$56, %r12
	cmpq	%r12, %r13
	jne	.L380
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L364:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22714:
	.size	_ZN2v88internal4wasm10WasmModuleD2Ev, .-_ZN2v88internal4wasm10WasmModuleD2Ev
	.weak	_ZN2v88internal4wasm10WasmModuleD1Ev
	.set	_ZN2v88internal4wasm10WasmModuleD1Ev,_ZN2v88internal4wasm10WasmModuleD2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB25338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L439
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm10WasmModuleD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$440, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25338:
	.size	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB25335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L442
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm10WasmModuleD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$440, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25335:
	.size	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt6vectorIN2v88internal4wasm9ValueTypeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm9ValueTypeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm9ValueTypeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm9ValueTypeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm9ValueTypeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB22789:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L459
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L454
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L460
.L456:
	movq	%rcx, %r14
.L447:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L453:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L461
	testq	%rsi, %rsi
	jg	.L449
	testq	%r15, %r15
	jne	.L452
.L450:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L449
.L452:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L460:
	testq	%r14, %r14
	js	.L456
	jne	.L447
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L449:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L450
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L454:
	movl	$1, %r14d
	jmp	.L447
.L459:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22789:
	.size	_ZNSt6vectorIN2v88internal4wasm9ValueTypeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm9ValueTypeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIS_IN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EESaIS5_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIS_IN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EESaIS5_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIS_IN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EESaIS5_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorIS_IN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EESaIS5_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorIS_IN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EESaIS5_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB22832:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movabsq	$384307168202282325, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L494
	movq	%rbx, %rdx
	movq	%rdi, %r13
	subq	%r14, %rdx
	testq	%rax, %rax
	je	.L477
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L495
.L464:
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	%rax, %r12
	addq	%rax, %r15
	leaq	24(%rax), %r8
.L476:
	leaq	(%r12,%rdx), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	cmpq	%r14, %rbx
	je	.L466
	leaq	-24(%rbx), %rax
	movq	%r14, %rdx
	movabsq	$768614336404564651, %rcx
	subq	%r14, %rax
	shrq	$3, %rax
	imulq	%rcx, %rax
	leaq	47(%r12), %rcx
	subq	%r14, %rcx
	cmpq	$94, %rcx
	jbe	.L479
	movabsq	$2305843009213693950, %rcx
	testq	%rcx, %rax
	je	.L479
	movabsq	$2305843009213693951, %rdi
	movq	%r12, %rcx
	andq	%rax, %rdi
	addq	$1, %rdi
	movq	%rdi, %r8
	shrq	%r8
	leaq	(%r8,%r8,2), %r8
	salq	$4, %r8
	addq	%r14, %r8
	.p2align 4,,10
	.p2align 3
.L468:
	movdqu	16(%rdx), %xmm1
	movdqu	(%rdx), %xmm7
	addq	$48, %rdx
	addq	$48, %rcx
	movdqu	-16(%rdx), %xmm0
	movups	%xmm7, -48(%rcx)
	movups	%xmm1, -32(%rcx)
	movups	%xmm0, -16(%rcx)
	cmpq	%r8, %rdx
	jne	.L468
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	(%r8,%r8,2), %rdx
	salq	$3, %rdx
	leaq	(%r14,%rdx), %rcx
	addq	%r12, %rdx
	cmpq	%r8, %rdi
	je	.L470
	movdqu	(%rcx), %xmm6
	movq	16(%rcx), %rcx
	movq	%rcx, 16(%rdx)
	movups	%xmm6, (%rdx)
.L470:
	leaq	(%rax,%rax,2), %rax
	leaq	48(%r12,%rax,8), %r8
.L466:
	cmpq	%rsi, %rbx
	je	.L471
	movabsq	$768614336404564651, %rcx
	subq	%rbx, %rsi
	movq	%rbx, %rdx
	leaq	-24(%rsi), %rax
	shrq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$2305843009213693951, %rcx
	andq	%rcx, %rax
	leaq	1(%rax), %rsi
	je	.L480
	movq	%rsi, %rax
	movq	%r8, %rcx
	shrq	%rax
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L473:
	movdqu	(%rdx), %xmm3
	movdqu	16(%rdx), %xmm4
	addq	$48, %rdx
	addq	$48, %rcx
	movdqu	-16(%rdx), %xmm5
	movups	%xmm3, -48(%rcx)
	movups	%xmm4, -32(%rcx)
	movups	%xmm5, -16(%rcx)
	cmpq	%rax, %rdx
	jne	.L473
	movq	%rsi, %rcx
	andq	$-2, %rcx
	leaq	(%rcx,%rcx,2), %rax
	salq	$3, %rax
	leaq	(%r8,%rax), %rdx
	addq	%rax, %rbx
	cmpq	%rcx, %rsi
	je	.L474
.L472:
	movq	16(%rbx), %rax
	movdqu	(%rbx), %xmm2
	movq	%rax, 16(%rdx)
	movups	%xmm2, (%rdx)
.L474:
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%r8,%rax,8), %r8
.L471:
	testq	%r14, %r14
	je	.L475
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L475:
	movq	%r12, %xmm0
	movq	%r8, %xmm7
	movq	%r15, 16(%r13)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L465
	movl	$24, %r8d
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L477:
	movl	$24, %r15d
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L479:
	movq	%r12, %rcx
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L467:
	movdqu	(%rdx), %xmm1
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rdi
	movq	%rdi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L467
	jmp	.L470
.L480:
	movq	%r8, %rdx
	jmp	.L472
.L465:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	imulq	$24, %rcx, %r15
	jmp	.L464
.L494:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22832:
	.size	_ZNSt6vectorIS_IN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EESaIS5_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorIS_IN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EESaIS5_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm21LocalNamesPerFunctionESaIS3_EE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm21LocalNamesPerFunctionESaIS3_EE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm21LocalNamesPerFunctionESaIS3_EE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm21LocalNamesPerFunctionESaIS3_EE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm21LocalNamesPerFunctionESaIS3_EE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB22889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$288230376151711743, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L515
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L506
	movabsq	$9223372036854775776, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L516
.L498:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rax, %r14
	leaq	32(%rax), %r8
	addq	%rax, %rsi
.L505:
	movl	(%rdx), %edx
	addq	%r14, %rcx
	pxor	%xmm0, %xmm0
	movl	$-1, 4(%rcx)
	movl	%edx, (%rcx)
	movq	$0, 24(%rcx)
	movups	%xmm0, 8(%rcx)
	cmpq	%r15, %rbx
	je	.L500
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L501:
	movl	(%rdx), %eax
	addq	$32, %rdx
	addq	$32, %rcx
	movl	%eax, -32(%rcx)
	movl	-28(%rdx), %eax
	movl	%eax, -28(%rcx)
	movdqu	-24(%rdx), %xmm1
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rax
	movq	%rax, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L501
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%r14,%rdx), %r8
.L500:
	cmpq	%r12, %rbx
	je	.L502
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L503:
	movl	(%rdx), %eax
	movdqu	8(%rdx), %xmm2
	addq	$32, %rdx
	addq	$32, %rcx
	movl	%eax, -32(%rcx)
	movl	-28(%rdx), %eax
	movups	%xmm2, -24(%rcx)
	movl	%eax, -28(%rcx)
	movq	-8(%rdx), %rax
	movq	%rax, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L503
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L502:
	testq	%r15, %r15
	je	.L504
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %r8
.L504:
	movq	%r14, %xmm0
	movq	%r8, %xmm3
	movq	%rsi, 16(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L499
	movl	$32, %r8d
	xorl	%esi, %esi
	xorl	%r14d, %r14d
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L506:
	movl	$32, %esi
	jmp	.L498
.L499:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$5, %rax
	movq	%rax, %rsi
	jmp	.L498
.L515:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22889:
	.size	_ZNSt6vectorIN2v88internal4wasm21LocalNamesPerFunctionESaIS3_EE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm21LocalNamesPerFunctionESaIS3_EE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm10WasmImportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm10WasmImportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm10WasmImportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm10WasmImportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm10WasmImportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23515:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$384307168202282325, %rcx
	cmpq	%rcx, %rax
	je	.L534
	movq	%r12, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L526
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L535
.L519:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	24(%r13), %rbx
.L525:
	movdqu	(%rdx), %xmm2
	movq	16(%rdx), %rax
	movups	%xmm2, 0(%r13,%r8)
	movq	%rax, 16(%r13,%r8)
	cmpq	%r14, %r12
	je	.L521
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L522:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L522
	leaq	-24(%r12), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	48(%r13,%rax,8), %rbx
.L521:
	cmpq	%rsi, %r12
	je	.L523
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-24(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	24(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L523:
	testq	%r14, %r14
	je	.L524
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L524:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L520
	movq	$0, -56(%rbp)
	movl	$24, %ebx
	xorl	%r13d, %r13d
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L526:
	movl	$24, %ebx
	jmp	.L519
.L520:
	cmpq	%rcx, %rdi
	movq	%rcx, %rbx
	cmovbe	%rdi, %rbx
	imulq	$24, %rbx, %rbx
	jmp	.L519
.L534:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23515:
	.size	_ZNSt6vectorIN2v88internal4wasm10WasmImportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm10WasmImportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm12WasmFunctionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm12WasmFunctionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm12WasmFunctionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm12WasmFunctionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm12WasmFunctionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r14
	movabsq	$288230376151711743, %rdi
	movq	%rcx, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L553
	movq	%rsi, %r12
	subq	%r14, %rsi
	testq	%rax, %rax
	je	.L545
	movabsq	$9223372036854775776, %r13
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L554
.L538:
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	leaq	(%rax,%r13), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	32(%rbx), %r13
.L544:
	movdqu	(%rdx), %xmm3
	movdqu	16(%rdx), %xmm4
	movups	%xmm3, (%rbx,%rsi)
	movups	%xmm4, 16(%rbx,%rsi)
	cmpq	%r14, %r12
	je	.L540
	movq	%rbx, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L541:
	movdqu	(%rax), %xmm1
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rax), %xmm2
	movups	%xmm2, -16(%rdx)
	cmpq	%rax, %r12
	jne	.L541
	movq	%r12, %rax
	subq	%r14, %rax
	leaq	32(%rbx,%rax), %r13
.L540:
	cmpq	%rcx, %r12
	je	.L542
	subq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r13
.L542:
	testq	%r14, %r14
	je	.L543
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L543:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r13, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L554:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L539
	movq	$0, -56(%rbp)
	movl	$32, %r13d
	xorl	%ebx, %ebx
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L545:
	movl	$32, %r13d
	jmp	.L538
.L539:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	movq	%rdi, %r13
	salq	$5, %r13
	jmp	.L538
.L553:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23520:
	.size	_ZNSt6vectorIN2v88internal4wasm12WasmFunctionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm12WasmFunctionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm10WasmGlobalESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm10WasmGlobalESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm10WasmGlobalESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm10WasmGlobalESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm10WasmGlobalESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r14
	movabsq	$288230376151711743, %rdi
	movq	%rcx, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L572
	movq	%rsi, %r12
	subq	%r14, %rsi
	testq	%rax, %rax
	je	.L564
	movabsq	$9223372036854775776, %r13
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L573
.L557:
	movq	%r13, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	leaq	(%rax,%r13), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	32(%rbx), %r13
.L563:
	movdqu	(%rdx), %xmm3
	movdqu	16(%rdx), %xmm4
	movups	%xmm3, (%rbx,%rsi)
	movups	%xmm4, 16(%rbx,%rsi)
	cmpq	%r14, %r12
	je	.L559
	movq	%rbx, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L560:
	movdqu	(%rax), %xmm1
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm1, -32(%rdx)
	movdqu	-16(%rax), %xmm2
	movups	%xmm2, -16(%rdx)
	cmpq	%rax, %r12
	jne	.L560
	movq	%r12, %rax
	subq	%r14, %rax
	leaq	32(%rbx,%rax), %r13
.L559:
	cmpq	%rcx, %r12
	je	.L561
	subq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r13
.L561:
	testq	%r14, %r14
	je	.L562
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L562:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r13, %xmm5
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L558
	movq	$0, -56(%rbp)
	movl	$32, %r13d
	xorl	%ebx, %ebx
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L564:
	movl	$32, %r13d
	jmp	.L557
.L558:
	cmpq	%rdi, %r8
	cmovbe	%r8, %rdi
	movq	%rdi, %r13
	salq	$5, %r13
	jmp	.L557
.L572:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23543:
	.size	_ZNSt6vectorIN2v88internal4wasm10WasmGlobalESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm10WasmGlobalESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm10WasmExportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm10WasmExportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm10WasmExportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm10WasmExportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm10WasmExportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$576460752303423487, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L593
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L584
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L594
.L576:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	16(%r14), %r9
.L583:
	movzbl	8(%rdx), %edi
	movl	12(%rdx), %esi
	addq	%r14, %rcx
	movq	(%rdx), %rdx
	movb	%dil, 8(%rcx)
	movq	%rdx, (%rcx)
	movl	%esi, 12(%rcx)
	cmpq	%r15, %rbx
	je	.L578
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L579:
	movl	(%rdx), %r9d
	movl	4(%rdx), %r8d
	addq	$16, %rdx
	addq	$16, %rcx
	movzbl	-8(%rdx), %edi
	movl	-4(%rdx), %esi
	movl	%r9d, -16(%rcx)
	movl	%r8d, -12(%rcx)
	movb	%dil, -8(%rcx)
	movl	%esi, -4(%rcx)
	cmpq	%rdx, %rbx
	jne	.L579
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	16(%r14,%rdx), %r9
.L578:
	cmpq	%r12, %rbx
	je	.L580
	movq	%rbx, %rdx
	movq	%r9, %rcx
	.p2align 4,,10
	.p2align 3
.L581:
	movl	4(%rdx), %r8d
	movzbl	8(%rdx), %edi
	addq	$16, %rdx
	addq	$16, %rcx
	movl	-4(%rdx), %esi
	movl	-16(%rdx), %r10d
	movl	%r8d, -12(%rcx)
	movl	%r10d, -16(%rcx)
	movb	%dil, -8(%rcx)
	movl	%esi, -4(%rcx)
	cmpq	%r12, %rdx
	jne	.L581
	subq	%rbx, %rdx
	addq	%rdx, %r9
.L580:
	testq	%r15, %r15
	je	.L582
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r9
.L582:
	movq	%r14, %xmm0
	movq	%r9, %xmm1
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L594:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L577
	movl	$16, %r9d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L584:
	movl	$16, %esi
	jmp	.L576
.L577:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$4, %rax
	movq	%rax, %rsi
	jmp	.L576
.L593:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23589:
	.size	_ZNSt6vectorIN2v88internal4wasm10WasmExportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm10WasmExportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt17_Temporary_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES5_EC2ESA_l,"axG",@progbits,_ZNSt17_Temporary_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES5_EC5ESA_l,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt17_Temporary_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES5_EC2ESA_l
	.type	_ZNSt17_Temporary_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES5_EC2ESA_l, @function
_ZNSt17_Temporary_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES5_EC2ESA_l:
.LFB23616:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZSt7nothrow(%rip), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	%rax, %rdx
	movq	%rdx, (%rdi)
	cmovle	%rdx, %rax
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	%rax, %r12
	testq	%rdx, %rdx
	jle	.L601
.L596:
	movq	%r12, %r14
	movq	%r15, %rsi
	salq	$4, %r14
	movq	%r14, %rdi
	call	_ZnwmRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L598
	movl	12(%r13), %edx
	movq	0(%r13), %rdi
	leaq	(%rax,%r14), %rcx
	movq	%rax, 16(%rbx)
	movzbl	8(%r13), %esi
	movq	%r12, 8(%rbx)
	movl	%edx, 12(%rax)
	leaq	16(%rax), %rdx
	movq	%rdi, (%rax)
	movb	%sil, 8(%rax)
	cmpq	%rdx, %rcx
	je	.L599
.L600:
	movl	-12(%rdx), %r8d
	movzbl	-8(%rdx), %edi
	addq	$16, %rdx
	movl	-20(%rdx), %esi
	movl	-32(%rdx), %r9d
	movl	%r8d, -12(%rdx)
	movl	%r9d, -16(%rdx)
	movb	%dil, -8(%rdx)
	movl	%esi, -4(%rdx)
	cmpq	%rdx, %rcx
	jne	.L600
	leaq	-16(%rax,%r14), %rax
.L599:
	movdqu	(%rax), %xmm0
	movups	%xmm0, 0(%r13)
.L595:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L598:
	.cfi_restore_state
	sarq	%r12
	jne	.L596
	.p2align 4,,10
	.p2align 3
.L601:
	movq	$0, 16(%rbx)
	movq	$0, 8(%rbx)
	jmp	.L595
	.cfi_endproc
.LFE23616:
	.size	_ZNSt17_Temporary_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES5_EC2ESA_l, .-_ZNSt17_Temporary_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES5_EC2ESA_l
	.weak	_ZNSt17_Temporary_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES5_EC1ESA_l
	.set	_ZNSt17_Temporary_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES5_EC1ESA_l,_ZNSt17_Temporary_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES5_EC2ESA_l
	.section	.text._ZNSt6vectorIN2v88internal4wasm19WasmCompilationHintESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm19WasmCompilationHintESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm19WasmCompilationHintESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm19WasmCompilationHintESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm19WasmCompilationHintESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23674:
	.cfi_startproc
	endbr64
	movabsq	$3074457345618258602, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movabsq	$-6148914691236517205, %rdx
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r15
	movq	%r9, %rax
	subq	%r15, %rax
	imulq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L623
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L619
	movabsq	$9223372036854775806, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L624
.L611:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L618:
	movzwl	0(%r13), %eax
	subq	%r8, %r9
	leaq	3(%rbx,%rdx), %r10
	movw	%ax, (%rbx,%rdx)
	movzbl	2(%r13), %eax
	movq	%r9, %r13
	movb	%al, 2(%rbx,%rdx)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L625
	testq	%r9, %r9
	jg	.L614
	testq	%r15, %r15
	jne	.L617
.L615:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L614
.L617:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L624:
	testq	%rsi, %rsi
	jne	.L612
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L614:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L615
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L619:
	movl	$3, %r14d
	jmp	.L611
.L623:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L612:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	leaq	(%rcx,%rcx,2), %r14
	jmp	.L611
	.cfi_endproc
.LFE23674:
	.size	_ZNSt6vectorIN2v88internal4wasm19WasmCompilationHintESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm19WasmCompilationHintESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23837:
	.cfi_startproc
	endbr64
	movabsq	$768614336404564650, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movabsq	$-6148914691236517205, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r9
	movq	(%rdi), %r8
	movq	%r9, %rax
	subq	%r8, %rax
	sarq	$2, %rax
	imulq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L640
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r13
	subq	%r8, %rdx
	testq	%rax, %rax
	je	.L636
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L641
.L628:
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rdx
	movq	%rax, %rbx
	addq	%rax, %r14
.L635:
	movq	(%r15), %rax
	subq	%r13, %r9
	leaq	12(%rbx,%rdx), %r10
	movq	%rax, (%rbx,%rdx)
	movl	8(%r15), %eax
	movq	%r9, %r15
	movl	%eax, 8(%rbx,%rdx)
	leaq	(%r10,%r9), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L642
	testq	%r9, %r9
	jg	.L631
	testq	%r8, %r8
	jne	.L634
.L632:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movq	%r10, -72(%rbp)
	movq	%r8, -64(%rbp)
	call	memmove@PLT
	testq	%r15, %r15
	movq	-64(%rbp), %r8
	movq	-72(%rbp), %r10
	jg	.L631
.L634:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L641:
	testq	%rsi, %rsi
	jne	.L629
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L631:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r8, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	testq	%r8, %r8
	je	.L632
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L636:
	movl	$12, %r14d
	jmp	.L628
.L640:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L629:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	imulq	$12, %rcx, %r14
	jmp	.L628
	.cfi_endproc
.LFE23837:
	.size	_ZNSt6vectorIN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm19CustomSectionOffsetESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm19CustomSectionOffsetESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm19CustomSectionOffsetESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm19CustomSectionOffsetESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm19CustomSectionOffsetESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB23853:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$384307168202282325, %rcx
	cmpq	%rcx, %rax
	je	.L660
	movq	%r12, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L652
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L661
.L645:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	24(%r13), %rbx
.L651:
	movdqu	(%rdx), %xmm2
	movq	16(%rdx), %rax
	movups	%xmm2, 0(%r13,%r8)
	movq	%rax, 16(%r13,%r8)
	cmpq	%r14, %r12
	je	.L647
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L648:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L648
	leaq	-24(%r12), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	48(%r13,%rax,8), %rbx
.L647:
	cmpq	%rsi, %r12
	je	.L649
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-24(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	24(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L649:
	testq	%r14, %r14
	je	.L650
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L650:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L661:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L646
	movq	$0, -56(%rbp)
	movl	$24, %ebx
	xorl	%r13d, %r13d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L652:
	movl	$24, %ebx
	jmp	.L645
.L646:
	cmpq	%rcx, %rdi
	movq	%rcx, %rbx
	cmovbe	%rdi, %rbx
	imulq	$24, %rbx, %rbx
	jmp	.L645
.L660:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE23853:
	.size	_ZNSt6vectorIN2v88internal4wasm19CustomSectionOffsetESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm19CustomSectionOffsetESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.rodata._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag:
.LFB23898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L663
	testq	%rsi, %rsi
	je	.L679
.L663:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L680
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L666
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L667:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L681
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L666:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L667
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L665:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L667
.L679:
	leaq	.LC36(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L681:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23898:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.section	.rodata._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.str1.1,"aMS",@progbits,1
.LC37:
	.string	"0 < len"
	.section	.text._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag,"axG",@progbits,_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	.type	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag, @function
_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag:
.LFB6932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$368, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 56(%rdi)
	je	.L692
.L682:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L693
	addq	$368, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore_state
	movq	%rdi, %r12
	movl	%esi, %r13d
	leaq	-304(%rbp), %rdi
	movl	$256, %esi
	movq	%rdi, -320(%rbp)
	movq	$256, -312(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	testl	%eax, %eax
	jle	.L694
	movq	-320(%rbp), %rsi
	cltq
	leaq	-400(%rbp), %rdi
	leaq	-384(%rbp), %rbx
	movq	%rbx, -400(%rbp)
	leaq	-344(%rbp), %r14
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-400(%rbp), %rax
	movl	%r13d, -368(%rbp)
	movq	%r14, -360(%rbp)
	cmpq	%rbx, %rax
	je	.L695
	movq	%rax, -360(%rbp)
	movq	-384(%rbp), %rax
	movq	%rax, -344(%rbp)
.L687:
	movl	%r13d, 40(%r12)
	movq	-392(%rbp), %rax
	leaq	48(%r12), %rdi
	leaq	-360(%rbp), %rsi
	movq	%rbx, -400(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -392(%rbp)
	movb	$0, -384(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_@PLT
	movq	-360(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L688
	call	_ZdlPv@PLT
.L688:
	movq	-400(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L689
	call	_ZdlPv@PLT
.L689:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L695:
	movdqa	-384(%rbp), %xmm0
	movups	%xmm0, -344(%rbp)
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L694:
	leaq	.LC37(%rip), %rsi
	leaq	.LC33(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L693:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6932:
	.size	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag, .-_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEjPKcz,"axG",@progbits,_ZN2v88internal4wasm7Decoder6errorfEjPKcz,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.type	_ZN2v88internal4wasm7Decoder6errorfEjPKcz, @function
_ZN2v88internal4wasm7Decoder6errorfEjPKcz:
.LFB6910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L697
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L697:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %rcx
	movl	$24, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L700
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L700:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6910:
	.size	_ZN2v88internal4wasm7Decoder6errorfEjPKcz, .-_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.section	.rodata._ZN2v88internal4wasm7Decoder5errorEPKhPKc.str1.1,"aMS",@progbits,1
.LC38:
	.string	"%s"
	.section	.text._ZN2v88internal4wasm7Decoder5errorEPKhPKc,"axG",@progbits,_ZN2v88internal4wasm7Decoder5errorEPKhPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.type	_ZN2v88internal4wasm7Decoder5errorEPKhPKc, @function
_ZN2v88internal4wasm7Decoder5errorEPKhPKc:
.LFB6908:
	.cfi_startproc
	endbr64
	movq	%rdx, %rcx
	subq	8(%rdi), %rsi
	leaq	.LC38(%rip), %rdx
	xorl	%eax, %eax
	addl	32(%rdi), %esi
	jmp	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.cfi_endproc
.LFE6908:
	.size	_ZN2v88internal4wasm7Decoder5errorEPKhPKc, .-_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.section	.text._ZN2v88internal4wasm7Decoder5errorEPKc,"axG",@progbits,_ZN2v88internal4wasm7Decoder5errorEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder5errorEPKc
	.type	_ZN2v88internal4wasm7Decoder5errorEPKc, @function
_ZN2v88internal4wasm7Decoder5errorEPKc:
.LFB6907:
	.cfi_startproc
	endbr64
	movq	%rsi, %rcx
	leaq	.LC38(%rip), %rdx
	movq	16(%rdi), %rsi
	xorl	%eax, %eax
	subq	8(%rdi), %rsi
	addl	32(%rdi), %esi
	jmp	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.cfi_endproc
.LFE6907:
	.size	_ZN2v88internal4wasm7Decoder5errorEPKc, .-_ZN2v88internal4wasm7Decoder5errorEPKc
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEPKhPKcz,"axG",@progbits,_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	.type	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz, @function
_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz:
.LFB6911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L704
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L704:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	subq	8(%rdi), %rsi
	leaq	-208(%rbp), %rcx
	movq	%rax, -200(%rbp)
	addl	32(%rdi), %esi
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$24, -208(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L707
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L707:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6911:
	.size	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz, .-_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	.section	.rodata._ZN2v88internal4wasm7Decoder13consume_bytesEjPKc.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"expected %u bytes, fell off end"
	.section	.text._ZN2v88internal4wasm7Decoder13consume_bytesEjPKc.isra.0,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb,comdat
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder13consume_bytesEjPKc.isra.0, @function
_ZN2v88internal4wasm7Decoder13consume_bytesEjPKc.isra.0:
.LFB25633:
	.cfi_startproc
	movq	24(%rdi), %rax
	movl	%esi, %ecx
	movq	16(%rdi), %rsi
	subq	%rsi, %rax
	cmpl	%eax, %ecx
	ja	.L709
	addq	%rsi, %rcx
	movq	%rcx, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L709:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	leaq	.LC39(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-8(%rbp), %rdi
	movq	24(%rdi), %rax
	movq	%rax, 16(%rdi)
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE25633:
	.size	_ZN2v88internal4wasm7Decoder13consume_bytesEjPKc.isra.0, .-_ZN2v88internal4wasm7Decoder13consume_bytesEjPKc.isra.0
	.section	.rodata._ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_.str1.1,"aMS",@progbits,1
.LC40:
	.string	"expected %s"
.LC41:
	.string	"extra bits in varint"
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_,"axG",@progbits,_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_:
.LFB22987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r9
	cmpq	%rsi, %r9
	ja	.L730
	movq	%rsi, 16(%rdi)
	xorl	%eax, %eax
	movl	$1, (%rdx)
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L714:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L730:
	.cfi_restore_state
	movzbl	(%rsi), %r10d
	movl	%r10d, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%r8d, %eax
	leaq	1(%rsi), %r8
	testb	%r10b, %r10b
	js	.L731
	movq	%r8, 16(%rdi)
	movl	$2, (%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L731:
	.cfi_restore_state
	cmpq	%r8, %r9
	jbe	.L717
	movzbl	1(%rsi), %r10d
	movl	%r10d, %r8d
	sall	$14, %r8d
	andl	$2080768, %r8d
	orl	%r8d, %eax
	leaq	2(%rsi), %r8
	testb	%r10b, %r10b
	js	.L732
	movq	%r8, 16(%rdi)
	movl	$3, (%rdx)
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L717:
	movq	%r8, 16(%rdi)
	movl	$2, (%rdx)
.L729:
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L732:
	.cfi_restore_state
	cmpq	%r8, %r9
	jbe	.L719
	movzbl	2(%rsi), %r10d
	leaq	3(%rsi), %r12
	movl	%r10d, %r8d
	sall	$21, %r8d
	andl	$266338304, %r8d
	orl	%r8d, %eax
	testb	%r10b, %r10b
	js	.L733
	movq	%r12, 16(%rdi)
	movl	$4, (%rdx)
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L719:
	movq	%r8, 16(%rdi)
	movl	$3, (%rdx)
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L733:
	cmpq	%r12, %r9
	jbe	.L721
	movzbl	3(%rsi), %r8d
	addq	$4, %rsi
	movq	%rsi, 16(%rdi)
	movl	%r8d, %ebx
	movl	$5, (%rdx)
	andl	$-16, %ebx
	testb	%r8b, %r8b
	js	.L722
	sall	$28, %r8d
	orl	%r8d, %eax
.L723:
	testb	%bl, %bl
	je	.L714
	leaq	.LC41(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L714
.L721:
	movq	%r12, 16(%rdi)
	xorl	%eax, %eax
	movq	%r12, %rsi
	movl	$4, (%rdx)
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
	jmp	.L714
.L722:
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-24(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L723
	.cfi_endproc
.LFE22987:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2:
.LFB25958:
	.cfi_startproc
	cmpq	%rsi, 24(%rdi)
	ja	.L741
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsi, 16(%rdi)
	movl	$0, (%rdx)
	leaq	.LC40(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L741:
	.cfi_restore 6
	movzbl	(%rsi), %r8d
	addq	$1, %rsi
	movl	%r8d, %eax
	andl	$127, %eax
	testb	%r8b, %r8b
	js	.L742
	movq	%rsi, 16(%rdi)
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L742:
	movl	%eax, %r8d
	jmp	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_
	.cfi_endproc
.LFE25958:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h.str1.1,"aMS",@progbits,1
.LC42:
	.string	"initial size"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h.str1.8,"aMS",@progbits,1
	.align 8
.LC43:
	.string	"initial %s size (%u %s) is larger than implementation limit (%u)"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h.str1.1
.LC44:
	.string	"maximum size"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h.str1.8
	.align 8
.LC45:
	.string	"maximum %s size (%u %s) is larger than implementation limit (%u)"
	.align 8
.LC46:
	.string	"maximum %s size (%u %s) is less than initial (%u %s)"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h, @function
_ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h:
.LFB19093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-60(%rbp), %r15
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%ecx, %r12d
	leaq	.LC42(%rip), %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$72, %rsp
	movq	24(%rbp), %rax
	movq	%rsi, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	16(%rdi), %rsi
	movq	%r15, %rdx
	movq	%rax, -72(%rbp)
	movl	32(%rbp), %eax
	movl	%eax, -76(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, -88(%rbp)
	movl	$0, -60(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	-88(%rbp), %rsi
	movl	%eax, (%rbx)
	movb	$0, (%r14)
	movl	(%rbx), %r8d
	cmpl	%r12d, %r8d
	ja	.L750
	testb	$1, -76(%rbp)
	jne	.L751
.L745:
	movq	-72(%rbp), %rax
	movb	$0, (%r14)
	movl	%r12d, (%rax)
.L743:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L752
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L750:
	.cfi_restore_state
	subq	$8, %rsp
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %rcx
	movq	%r13, %rdi
	pushq	%r12
	leaq	.LC43(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rdi
	popq	%r8
	testb	$1, -76(%rbp)
	je	.L745
.L751:
	movb	$1, (%r14)
	movq	16(%r13), %r12
	movq	%r15, %rdx
	movq	%r13, %rdi
	leaq	.LC44(%rip), %rcx
	movl	$0, -60(%rbp)
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movl	%eax, %r8d
	movq	-72(%rbp), %rax
	movl	%r8d, (%rax)
	cmpl	%r8d, 16(%rbp)
	jb	.L753
.L746:
	movl	(%rbx), %eax
	cmpl	%r8d, %eax
	jbe	.L743
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	.LC46(%rip), %rdx
	pushq	%r9
	pushq	%rax
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rax
	popq	%rdx
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L753:
	movl	16(%rbp), %eax
	subq	$8, %rsp
	movq	-96(%rbp), %rcx
	movq	%r12, %rsi
	movq	-104(%rbp), %r9
	leaq	.LC45(%rip), %rdx
	movq	%r13, %rdi
	pushq	%rax
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-72(%rbp), %rax
	popq	%rcx
	popq	%rsi
	movl	(%rax), %r8d
	jmp	.L746
.L752:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19093:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h, .-_ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h
	.section	.rodata._ZN2v88internal4wasm13ModuleDecoder19CheckFunctionsCountEjj.str1.8,"aMS",@progbits,1
	.align 8
.LC47:
	.string	"function body count %u mismatch (%u expected)"
	.section	.text._ZN2v88internal4wasm13ModuleDecoder19CheckFunctionsCountEjj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm13ModuleDecoder19CheckFunctionsCountEjj
	.type	_ZN2v88internal4wasm13ModuleDecoder19CheckFunctionsCountEjj, @function
_ZN2v88internal4wasm13ModuleDecoder19CheckFunctionsCountEjj:
.LFB19125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$1, %eax
	movq	96(%r13), %rcx
	cmpl	68(%rcx), %esi
	jne	.L761
.L754:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L762
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	.cfi_restore_state
	movl	%edx, 32(%r13)
	pxor	%xmm0, %xmm0
	leaq	48(%r13), %rdi
	movl	%esi, %r12d
	movq	$0, 24(%r13)
	leaq	-56(%rbp), %rbx
	leaq	-72(%rbp), %rsi
	movl	$0, 40(%r13)
	movups	%xmm0, 8(%r13)
	movl	$0, -76(%rbp)
	movl	$0, -80(%rbp)
	movq	%rbx, -72(%rbp)
	movq	$0, -64(%rbp)
	movb	$0, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_@PLT
	movq	-72(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L756
	call	_ZdlPv@PLT
.L756:
	movq	96(%r13), %rax
	movl	%r12d, %ecx
	leaq	.LC47(%rip), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	68(%rax), %r8d
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
	jmp	.L754
.L762:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19125:
	.size	_ZN2v88internal4wasm13ModuleDecoder19CheckFunctionsCountEjj, .-_ZN2v88internal4wasm13ModuleDecoder19CheckFunctionsCountEjj
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl18DecodeModuleHeaderENS0_6VectorIKhEEh.str1.8,"aMS",@progbits,1
	.align 8
.LC48:
	.string	"expected magic word %02x %02x %02x %02x, found %02x %02x %02x %02x"
	.align 8
.LC49:
	.string	"expected version %02x %02x %02x %02x, found %02x %02x %02x %02x"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl18DecodeModuleHeaderENS0_6VectorIKhEEh,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl18DecodeModuleHeaderENS0_6VectorIKhEEh,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl18DecodeModuleHeaderENS0_6VectorIKhEEh
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl18DecodeModuleHeaderENS0_6VectorIKhEEh, @function
_ZN2v88internal4wasm17ModuleDecoderImpl18DecodeModuleHeaderENS0_6VectorIKhEEh:
.LFB19014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 56(%rdi)
	je	.L781
.L763:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L782
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L781:
	.cfi_restore_state
	addq	%rsi, %rdx
	movzbl	%cl, %ecx
	movq	%rsi, 8(%rdi)
	movq	%rdi, %r12
	movq	%rsi, 16(%rdi)
	leaq	-40(%rbp), %r13
	leaq	48(%rdi), %rdi
	movq	%rdx, -24(%rdi)
	leaq	-56(%rbp), %rsi
	movl	%ecx, -16(%rdi)
	movl	$0, -8(%rdi)
	movl	$0, -60(%rbp)
	movl	$0, -64(%rbp)
	movq	%r13, -56(%rbp)
	movq	$0, -48(%rbp)
	movb	$0, -40(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_@PLT
	movq	-56(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L766
	call	_ZdlPv@PLT
.L766:
	movq	24(%r12), %rax
	movq	16(%r12), %r13
	movq	%rax, %rdx
	subq	%r13, %rdx
	cmpl	$3, %edx
	jbe	.L767
	movl	0(%r13), %edx
	leaq	4(%r13), %rsi
	movq	%rsi, 16(%r12)
	cmpl	$1836278016, %edx
	jne	.L783
.L768:
	subq	%rsi, %rax
	cmpl	$3, %eax
	jbe	.L770
	movl	(%rsi), %eax
	leaq	4(%rsi), %rdx
	movq	%rdx, 16(%r12)
	cmpl	$1, %eax
	je	.L763
	movl	%eax, %edx
	movl	%eax, %edi
	movzbl	%ah, %ecx
	movzbl	%al, %eax
	shrl	$16, %edx
	shrl	$24, %edi
	movzbl	%dl, %edx
.L772:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rdx
	leaq	.LC49(%rip), %rdx
	pushq	%rcx
	movl	$1, %ecx
	pushq	%rax
	xorl	%eax, %eax
	pushq	$0
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$48, %rsp
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L783:
	movl	%edx, %eax
	movl	%edx, %esi
	movzbl	%dh, %ecx
	movzbl	%dl, %edx
	shrl	$16, %eax
	shrl	$24, %esi
	movzbl	%al, %eax
.L769:
	subq	$8, %rsp
	movl	$115, %r9d
	movl	$97, %r8d
	movq	%r12, %rdi
	pushq	%rsi
	movq	%r13, %rsi
	pushq	%rax
	xorl	%eax, %eax
	pushq	%rcx
	xorl	%ecx, %ecx
	pushq	%rdx
	leaq	.LC48(%rip), %rdx
	pushq	$109
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %rsi
	movq	24(%r12), %rax
	addq	$48, %rsp
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L770:
	movl	$4, %ecx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movq	%rsi, -72(%rbp)
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	movq	-72(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%edi, %edi
	movq	%rax, 16(%r12)
	xorl	%eax, %eax
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L767:
	movl	$4, %ecx
	movq	%r13, %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, 16(%r12)
	xorl	%eax, %eax
	jmp	.L769
.L782:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19014:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl18DecodeModuleHeaderENS0_6VectorIKhEEh, .-_ZN2v88internal4wasm17ModuleDecoderImpl18DecodeModuleHeaderENS0_6VectorIKhEEh
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl29DecodeCompilationHintsSectionEv.str1.1,"aMS",@progbits,1
.LC50:
	.string	"compilation hint count"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl29DecodeCompilationHintsSectionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC51:
	.string	"Expected %u compilation hints (%u found)"
	.align 8
.LC52:
	.string	"Invalid compilation hint %#x (forbidden downgrade)"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl29DecodeCompilationHintsSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl29DecodeCompilationHintsSectionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl29DecodeCompilationHintsSectionEv
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl29DecodeCompilationHintsSectionEv, @function
_ZN2v88internal4wasm17ModuleDecoderImpl29DecodeCompilationHintsSectionEv:
.LFB19036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	120(%rdi), %eax
	subl	$4, %eax
	cmpb	$6, %al
	ja	.L784
	movl	124(%rdi), %eax
	movq	%rdi, %rbx
	movl	%eax, %r15d
	andl	$65536, %r15d
	je	.L839
.L784:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L840
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L839:
	.cfi_restore_state
	orl	$65536, %eax
	movq	16(%rdi), %rsi
	leaq	-60(%rbp), %r13
	leaq	.LC50(%rip), %rcx
	movl	%eax, 124(%rdi)
	movq	%r13, %rdx
	movl	$0, -60(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	96(%rbx), %r14
	movl	%eax, %r12d
	movl	68(%r14), %ecx
	cmpl	%eax, %ecx
	jne	.L841
	cmpq	$0, 56(%rbx)
	je	.L842
	.p2align 4,,10
	.p2align 3
.L789:
	movq	304(%r14), %rax
	cmpq	312(%r14), %rax
	je	.L784
	movq	%rax, 312(%r14)
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L841:
	movq	16(%rbx), %rsi
	movl	%eax, %r8d
	leaq	.LC51(%rip), %rdx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	cmpq	$0, 56(%rbx)
	movq	96(%rbx), %r14
	jne	.L789
.L842:
	movq	304(%r14), %r11
	testl	%r12d, %r12d
	movl	%r12d, %edx
	movq	320(%r14), %rax
	movabsq	$-6148914691236517205, %rcx
	setne	%r8b
	subq	%r11, %rax
	imulq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L843
	testl	%r12d, %r12d
	je	.L784
.L807:
	leaq	.LC39(%rip), %r14
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L800:
	movq	96(%rbx), %rdi
	movq	312(%rdi), %rsi
	cmpq	320(%rdi), %rsi
	je	.L844
	movzwl	-60(%rbp), %eax
	movw	%ax, (%rsi)
	movzbl	-58(%rbp), %eax
	movb	%al, 2(%rsi)
	addq	$3, 312(%rdi)
	movq	56(%rbx), %rax
.L802:
	addl	$1, %r15d
	cmpl	%r15d, %r12d
	jbe	.L795
	testq	%rax, %rax
	jne	.L834
.L803:
	movq	16(%rbx), %rsi
	cmpl	%esi, 24(%rbx)
	je	.L796
	movzbl	(%rsi), %ecx
	addq	$1, %rsi
	cmpq	$0, 56(%rbx)
	movq	%rsi, 16(%rbx)
	jne	.L834
	movl	%ecx, %eax
	movl	%ecx, %edx
	andl	$3, %eax
	sarl	$2, %edx
	movb	%al, -60(%rbp)
	movl	%ecx, %eax
	andl	$3, %edx
	sarl	$4, %eax
	movb	%dl, -59(%rbp)
	andl	$3, %eax
	movb	%al, -58(%rbp)
	testb	%al, %al
	je	.L800
	cmpb	%al, %dl
	jbe	.L800
	xorl	%eax, %eax
	leaq	.LC52(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	cmpq	$0, 56(%rbx)
	je	.L800
.L834:
	movq	96(%rbx), %r14
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L796:
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%rbx), %rax
	cmpq	$0, 56(%rbx)
	movq	%rax, 16(%rbx)
	jne	.L834
	xorl	%eax, %eax
	movb	$0, -58(%rbp)
	movw	%ax, -60(%rbp)
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L843:
	movq	312(%r14), %r9
	leaq	(%rdx,%rdx,2), %r10
	subq	%r11, %r9
	testq	%rdx, %rdx
	je	.L809
	movq	%r10, %rdi
	movb	%r8b, -81(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_Znwm@PLT
	movq	304(%r14), %r11
	movq	312(%r14), %rdx
	movq	-72(%rbp), %r10
	movq	-80(%rbp), %r9
	movq	%rax, %rcx
	movzbl	-81(%rbp), %r8d
	subq	%r11, %rdx
.L791:
	testq	%rdx, %rdx
	jg	.L845
	testq	%r11, %r11
	jne	.L793
.L794:
	leaq	(%rcx,%r9), %rax
	movq	%rcx, 304(%r14)
	addq	%r10, %rcx
	movq	%rax, 312(%r14)
	movq	%rcx, 320(%r14)
	movq	56(%rbx), %rax
	testq	%rax, %rax
	jne	.L834
	testb	%r8b, %r8b
	jne	.L807
.L795:
	testq	%rax, %rax
	je	.L784
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L844:
	addq	$304, %rdi
	movq	%r13, %rdx
	call	_ZNSt6vectorIN2v88internal4wasm19WasmCompilationHintESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	56(%rbx), %rax
	jmp	.L802
.L845:
	movq	%r11, %rsi
	movq	%rcx, %rdi
	movq	%r10, -96(%rbp)
	movb	%r8b, -81(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r11, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r9
	movzbl	-81(%rbp), %r8d
	movq	-96(%rbp), %r10
	movq	%rax, %rcx
.L793:
	movq	%r11, %rdi
	movq	%r10, -96(%rbp)
	movb	%r8b, -81(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %r10
	movzbl	-81(%rbp), %r8d
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r9
	jmp	.L794
.L809:
	movq	%r9, %rdx
	xorl	%ecx, %ecx
	jmp	.L791
.L840:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19036:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl29DecodeCompilationHintsSectionEv, .-_ZN2v88internal4wasm17ModuleDecoderImpl29DecodeCompilationHintsSectionEv
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl18consume_value_typeEv.str1.1,"aMS",@progbits,1
.LC53:
	.string	"invalid local type"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl18consume_value_typeEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl18consume_value_typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl18consume_value_typeEv
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl18consume_value_typeEv, @function
_ZN2v88internal4wasm17ModuleDecoderImpl18consume_value_typeEv:
.LFB19097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	16(%rdi), %rsi
	cmpl	%esi, 24(%rdi)
	je	.L847
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %rax
	movq	%rax, 16(%rdi)
	leal	-124(%rdx), %eax
	cmpb	$3, %al
	jbe	.L848
	cmpb	$0, 168(%rdi)
	jne	.L850
	cmpb	$112, %dl
	je	.L852
	ja	.L853
	cmpb	$104, %dl
	je	.L854
	cmpb	$111, %dl
	jne	.L850
	cmpb	$0, 87(%rdi)
	movl	$6, %eax
	jne	.L846
	.p2align 4,,10
	.p2align 3
.L850:
	leaq	.LC53(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
.L846:
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L848:
	.cfi_restore_state
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$-128, %eax
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L853:
	.cfi_restore_state
	cmpb	$123, %dl
	jne	.L850
	cmpb	$0, 83(%rdi)
	movl	$5, %eax
	je	.L850
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L854:
	cmpb	$0, 81(%rdi)
	movl	$9, %eax
	je	.L850
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L847:
	xorl	%eax, %eax
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%rdi, -8(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-8(%rbp), %rdi
	leaq	.LC53(%rip), %rdx
	movq	24(%rdi), %rsi
	movq	%rsi, 16(%rdi)
	subq	$1, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L852:
	cmpb	$0, 87(%rdi)
	movl	$7, %eax
	je	.L850
	jmp	.L846
	.cfi_endproc
.LFE19097:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl18consume_value_typeEv, .-_ZN2v88internal4wasm17ModuleDecoderImpl18consume_value_typeEv
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.1.str1.1,"aMS",@progbits,1
.LC54:
	.string	"string length"
.LC55:
	.string	"%s: no valid UTF-8 string"
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.1,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.1, @function
_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.1:
.LFB25961:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC54(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-60(%rbp), %rdx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	16(%rbx), %r15
	movl	%eax, %r12d
	movq	%r15, %r13
	subq	8(%rbx), %r13
	addl	32(%rbx), %r13d
	testl	%eax, %eax
	jne	.L873
.L864:
	cmpq	$0, 56(%rbx)
	jne	.L870
.L868:
	salq	$32, %r12
	movl	%r13d, %eax
	orq	%r12, %rax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L874
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L873:
	.cfi_restore_state
	movq	24(%rbx), %rax
	subq	%r15, %rax
	cmpl	%eax, %r12d
	ja	.L865
	movl	%r12d, %eax
	addq	%r15, %rax
	movq	%rax, 16(%rbx)
.L866:
	cmpq	$0, 56(%rbx)
	je	.L867
.L870:
	xorl	%r12d, %r12d
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L867:
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN7unibrow4Utf816ValidateEncodingEPKhm@PLT
	testb	%al, %al
	jne	.L864
	movq	%r14, %rcx
	leaq	.LC55(%rip), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L865:
	xorl	%eax, %eax
	movl	%r12d, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%rbx), %rax
	movq	%rax, 16(%rbx)
	jmp	.L866
.L874:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25961:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.1, .-_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.1
	.section	.rodata._ZN2v88internal4wasm13ModuleDecoder22IdentifyUnknownSectionEPNS1_7DecoderEPKh.str1.1,"aMS",@progbits,1
.LC56:
	.string	"section name"
	.section	.text._ZN2v88internal4wasm13ModuleDecoder22IdentifyUnknownSectionEPNS1_7DecoderEPKh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm13ModuleDecoder22IdentifyUnknownSectionEPNS1_7DecoderEPKh
	.type	_ZN2v88internal4wasm13ModuleDecoder22IdentifyUnknownSectionEPNS1_7DecoderEPKh, @function
_ZN2v88internal4wasm13ModuleDecoder22IdentifyUnknownSectionEPNS1_7DecoderEPKh:
.LFB19127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	leaq	.LC56(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.1
	xorl	%r8d, %r8d
	cmpq	$0, 56(%rbx)
	jne	.L875
	cmpq	16(%rbx), %r12
	jb	.L875
	movq	%rax, %rdx
	subl	32(%rbx), %eax
	addq	8(%rbx), %rax
	shrq	$32, %rdx
	movq	%rax, %r12
	cmpl	$4, %edx
	je	.L886
	cmpl	$16, %edx
	je	.L887
.L875:
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L887:
	.cfi_restore_state
	movl	$16, %edx
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L23kSourceMappingURLStringE(%rip), %rsi
	movq	%rax, %rdi
	call	strncmp@PLT
	movl	$15, %r8d
	testl	%eax, %eax
	je	.L875
	movq	%r12, %rdi
	movl	$16, %edx
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L23kCompilationHintsStringE(%rip), %rsi
	call	strncmp@PLT
	popq	%rbx
	popq	%r12
	testl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	sete	%r8b
	sall	$4, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L886:
	.cfi_restore_state
	movl	$4, %edx
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L11kNameStringE(%rip), %rsi
	movq	%rax, %rdi
	call	strncmp@PLT
	popq	%rbx
	popq	%r12
	cmpl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	sbbl	%r8d, %r8d
	andl	$14, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE19127:
	.size	_ZN2v88internal4wasm13ModuleDecoder22IdentifyUnknownSectionEPNS1_7DecoderEPKh, .-_ZN2v88internal4wasm13ModuleDecoder22IdentifyUnknownSectionEPNS1_7DecoderEPKh
	.section	.text._ZN2v88internal4wasm13ModuleDecoder18DecodeModuleHeaderENS0_6VectorIKhEEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm13ModuleDecoder18DecodeModuleHeaderENS0_6VectorIKhEEj
	.type	_ZN2v88internal4wasm13ModuleDecoder18DecodeModuleHeaderENS0_6VectorIKhEEj, @function
_ZN2v88internal4wasm13ModuleDecoder18DecodeModuleHeaderENS0_6VectorIKhEEj:
.LFB19122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$64, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 56(%r12)
	je	.L906
.L888:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L907
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L906:
	.cfi_restore_state
	addq	%rsi, %rdx
	andl	$255, %ecx
	leaq	-40(%rbp), %rax
	movq	%rsi, 8(%r12)
	movq	%rdx, 24(%r12)
	movq	48(%r12), %rdx
	movq	%rsi, 16(%r12)
	movq	%rax, -56(%rbp)
	movl	%ecx, 32(%r12)
	movl	$0, -60(%rbp)
	movl	$0, -64(%rbp)
	movb	$0, -40(%rbp)
	movl	$0, 40(%r12)
	movb	$0, (%rdx)
	movq	-56(%rbp), %rdx
	movq	$0, -48(%rbp)
	movb	$0, (%rdx)
	movq	-56(%rbp), %rdi
	cmpq	%rax, %rdi
	je	.L891
	call	_ZdlPv@PLT
.L891:
	movq	24(%r12), %rax
	movq	16(%r12), %r13
	movq	%rax, %rdx
	subq	%r13, %rdx
	cmpl	$3, %edx
	jbe	.L892
	movl	0(%r13), %edx
	leaq	4(%r13), %rsi
	movq	%rsi, 16(%r12)
	cmpl	$1836278016, %edx
	jne	.L908
.L893:
	subq	%rsi, %rax
	cmpl	$3, %eax
	jbe	.L895
	movl	(%rsi), %eax
	leaq	4(%rsi), %rdx
	movq	%rdx, 16(%r12)
	cmpl	$1, %eax
	je	.L888
	movl	%eax, %edx
	movl	%eax, %edi
	movzbl	%ah, %ecx
	movzbl	%al, %eax
	shrl	$16, %edx
	shrl	$24, %edi
	movzbl	%dl, %edx
.L897:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	%rdi
	movq	%r12, %rdi
	pushq	%rdx
	leaq	.LC49(%rip), %rdx
	pushq	%rcx
	movl	$1, %ecx
	pushq	%rax
	xorl	%eax, %eax
	pushq	$0
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$48, %rsp
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L908:
	movl	%edx, %eax
	movl	%edx, %esi
	movzbl	%dh, %ecx
	movzbl	%dl, %edx
	shrl	$16, %eax
	shrl	$24, %esi
	movzbl	%al, %eax
.L894:
	subq	$8, %rsp
	movl	$115, %r9d
	movl	$97, %r8d
	movq	%r12, %rdi
	pushq	%rsi
	movq	%r13, %rsi
	pushq	%rax
	xorl	%eax, %eax
	pushq	%rcx
	xorl	%ecx, %ecx
	pushq	%rdx
	leaq	.LC48(%rip), %rdx
	pushq	$109
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r12), %rsi
	movq	24(%r12), %rax
	addq	$48, %rsp
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L895:
	movl	$4, %ecx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movq	%rsi, -72(%rbp)
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	movq	-72(%rbp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%edi, %edi
	movq	%rax, 16(%r12)
	xorl	%eax, %eax
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L892:
	movl	$4, %ecx
	movq	%r13, %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, 16(%r12)
	xorl	%eax, %eax
	jmp	.L894
.L907:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19122:
	.size	_ZN2v88internal4wasm13ModuleDecoder18DecodeModuleHeaderENS0_6VectorIKhEEj, .-_ZN2v88internal4wasm13ModuleDecoder18DecodeModuleHeaderENS0_6VectorIKhEEj
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_119WasmSectionIterator4nextEv.str1.1,"aMS",@progbits,1
.LC57:
	.string	"section length"
.LC58:
	.string	"unknown section code #0x%02x"
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_119WasmSectionIterator4nextEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_119WasmSectionIterator4nextEv, @function
_ZN2v88internal4wasm12_GLOBAL__N_119WasmSectionIterator4nextEv:
.LFB18994:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%r12), %rsi
	cmpq	24(%r12), %rsi
	jb	.L910
	movb	$0, 8(%rdi)
.L909:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L931
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L910:
	.cfi_restore_state
	movq	%rsi, 16(%rdi)
	cmpl	%esi, 24(%r12)
	je	.L912
	movzbl	(%rsi), %r13d
	addq	$1, %rsi
	movq	%rsi, 16(%r12)
.L913:
	movq	(%rbx), %rdi
	leaq	.LC57(%rip), %rcx
	leaq	-44(%rbp), %rdx
	movl	$0, -44(%rbp)
	movq	16(%rdi), %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	(%rbx), %rdi
	movl	%eax, %ecx
	movq	16(%rdi), %rsi
	movq	%rsi, 24(%rbx)
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	subq	%r8, %rax
	cmpl	%eax, %ecx
	ja	.L914
	addq	%rcx, %rsi
	movq	%rsi, 32(%rbx)
.L915:
	testb	%r13b, %r13b
	je	.L932
	leal	-1(%r13), %eax
	cmpb	$15, %al
	ja	.L933
	cmpq	$0, 56(%rdi)
	je	.L934
.L921:
	movb	$0, 8(%rbx)
.L925:
	movq	16(%rdi), %r8
	cmpq	%rsi, %r8
	jnb	.L909
	movq	24(%rdi), %rax
	subq	%r8, %rsi
	subq	%r8, %rax
	cmpl	%eax, %esi
	ja	.L924
	movl	%esi, %esi
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L934:
	movb	%r13b, 8(%rbx)
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L932:
	call	_ZN2v88internal4wasm13ModuleDecoder22IdentifyUnknownSectionEPNS1_7DecoderEPKh
	movq	(%rbx), %rdi
	movq	16(%rdi), %rdx
	movq	%rdx, 24(%rbx)
	cmpq	$0, 56(%rdi)
	jne	.L917
	movb	%al, 8(%rbx)
	testb	%al, %al
	jne	.L909
	movq	32(%rbx), %rsi
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L933:
	movq	16(%rdi), %rsi
	xorl	%eax, %eax
	movzbl	%r13b, %ecx
	leaq	.LC58(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	(%rbx), %rdi
	cmpq	$0, 56(%rdi)
	je	.L920
.L917:
	movq	32(%rbx), %rsi
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L914:
	movq	%r8, %rsi
	leaq	.LC39(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%rbx), %rsi
	movq	(%rbx), %rdi
	movq	%rsi, 32(%rbx)
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L912:
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	movq	%rax, 16(%r12)
	jmp	.L913
.L924:
	movl	%esi, %ecx
	xorl	%eax, %eax
	leaq	.LC39(%rip), %rdx
	movq	%r8, %rsi
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-56(%rbp), %rdi
	movq	24(%rdi), %rax
	movq	%rax, 16(%rdi)
	jmp	.L909
.L931:
	call	__stack_chk_fail@PLT
.L920:
	movb	$0, 8(%rbx)
	movq	32(%rbx), %rsi
	jmp	.L925
	.cfi_endproc
.LFE18994:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_119WasmSectionIterator4nextEv, .-_ZN2v88internal4wasm12_GLOBAL__N_119WasmSectionIterator4nextEv
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm.str1.8,"aMS",@progbits,1
	.align 8
.LC59:
	.string	"%s of %u exceeds internal limit of %zu"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm, @function
_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm:
.LFB19085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -32
	.cfi_offset 3, -40
	movq	16(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$0, -44(%rbp)
	cmpq	24(%rdi), %r13
	jb	.L942
	movq	%rsi, %rcx
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L935:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L943
	addq	$40, %rsp
	popq	%rbx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L942:
	.cfi_restore_state
	movq	%rdx, %rbx
	movzbl	0(%r13), %edx
	leaq	1(%r13), %rsi
	movl	%edx, %eax
	andl	$127, %eax
	testb	%dl, %dl
	js	.L944
	movq	%rsi, 16(%rdi)
.L939:
	movl	%eax, %edx
	cmpq	%rbx, %rdx
	jbe	.L935
	movl	%eax, %r8d
	movq	%rbx, %r9
	xorl	%eax, %eax
	movq	%r14, %rcx
	leaq	.LC59(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	%ebx, %eax
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L944:
	leaq	-44(%rbp), %rdx
	movl	%eax, %r8d
	movq	%r14, %rcx
	movq	%rdi, -56(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_
	movq	-56(%rbp), %rdi
	jmp	.L939
.L943:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19085:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm, .-_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl11consume_sigEPNS0_4ZoneE.str1.1,"aMS",@progbits,1
.LC60:
	.string	"type form"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl11consume_sigEPNS0_4ZoneE.str1.8,"aMS",@progbits,1
	.align 8
.LC61:
	.string	"expected %s 0x%02x, got 0x%02x"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl11consume_sigEPNS0_4ZoneE.str1.1
.LC62:
	.string	"param count"
.LC63:
	.string	"return count"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl11consume_sigEPNS0_4ZoneE,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl11consume_sigEPNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl11consume_sigEPNS0_4ZoneE
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl11consume_sigEPNS0_4ZoneE, @function
_ZN2v88internal4wasm17ModuleDecoderImpl11consume_sigEPNS0_4ZoneE:
.LFB19099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -136(%rbp)
	movq	16(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	%r14d, 24(%rdi)
	je	.L946
	movzbl	(%r14), %r9d
	leaq	1(%r14), %rax
	movq	%rax, 16(%rdi)
	cmpb	$96, %r9b
	jne	.L948
	movl	$1000, %edx
	leaq	.LC62(%rip), %rsi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm
	cmpq	$0, 56(%r12)
	movl	%eax, %ebx
	jne	.L997
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	xorl	%r14d, %r14d
	movaps	%xmm0, -112(%rbp)
	testl	%eax, %eax
	jne	.L950
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L954:
	movl	$-128, %eax
	subl	%edx, %eax
.L957:
	movb	%al, -80(%rbp)
	movq	-104(%rbp), %rsi
	cmpq	-96(%rbp), %rsi
	je	.L962
.L1036:
	movb	%al, (%rsi)
	addl	$1, %r14d
	addq	$1, -104(%rbp)
	cmpq	$0, 56(%r12)
	jne	.L965
.L1035:
	cmpl	%r14d, %ebx
	jbe	.L965
.L950:
	movq	16(%r12), %rsi
	cmpl	%esi, 24(%r12)
	je	.L953
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %rax
	movq	%rax, 16(%r12)
	leal	-124(%rdx), %eax
	cmpb	$3, %al
	jbe	.L954
	cmpb	$0, 168(%r12)
	jne	.L956
	cmpb	$112, %dl
	je	.L958
	ja	.L959
	cmpb	$104, %dl
	je	.L960
	cmpb	$111, %dl
	jne	.L956
	cmpb	$0, 87(%r12)
	movl	$6, %eax
	jne	.L957
	.p2align 4,,10
	.p2align 3
.L956:
	leaq	.LC53(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	movq	-104(%rbp), %rsi
	movb	%al, -80(%rbp)
	cmpq	-96(%rbp), %rsi
	jne	.L1036
.L962:
	leaq	-80(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	addl	$1, %r14d
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	cmpq	$0, 56(%r12)
	je	.L1035
	.p2align 4,,10
	.p2align 3
.L965:
	cmpb	$1, 80(%r12)
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	sbbq	%rdx, %rdx
	leaq	.LC63(%rip), %rsi
	movaps	%xmm0, -80(%rbp)
	andq	$-999, %rdx
	addq	$1000, %rdx
	call	_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm
	movq	56(%r12), %r13
	movl	%eax, %r14d
	testq	%r13, %r13
	jne	.L982
	xorl	%r15d, %r15d
	testl	%eax, %eax
	je	.L969
	.p2align 4,,10
	.p2align 3
.L968:
	movq	16(%r12), %rsi
	cmpl	%esi, 24(%r12)
	je	.L970
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %rax
	movq	%rax, 16(%r12)
	leal	-124(%rdx), %eax
	cmpb	$3, %al
	jbe	.L971
	cmpb	$0, 168(%r12)
	jne	.L973
	cmpb	$112, %dl
	je	.L975
	ja	.L976
	cmpb	$104, %dl
	je	.L977
	cmpb	$111, %dl
	jne	.L973
	cmpb	$0, 87(%r12)
	movl	$6, %eax
	jne	.L974
	.p2align 4,,10
	.p2align 3
.L973:
	leaq	.LC53(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
.L974:
	movb	%al, -113(%rbp)
	movq	-72(%rbp), %rsi
	cmpq	-64(%rbp), %rsi
	je	.L979
.L1040:
	movb	%al, (%rsi)
	addq	$1, -72(%rbp)
.L980:
	addl	$1, %r15d
	cmpq	$0, 56(%r12)
	jne	.L982
	cmpl	%r15d, %r14d
	jne	.L968
.L969:
	movq	-136(%rbp), %rax
	leal	(%rbx,%r14), %esi
	addq	$7, %rsi
	movq	16(%rax), %r12
	movq	24(%rax), %rax
	andq	$-8, %rsi
	movq	%rax, -144(%rbp)
	subq	%r12, %rax
	cmpq	%rax, %rsi
	ja	.L1037
	movq	-136(%rbp), %rax
	addq	%r12, %rsi
	movq	%rsi, 16(%rax)
.L983:
	leal	-1(%r14), %ecx
	xorl	%eax, %eax
	xorl	%esi, %esi
	testl	%r14d, %r14d
	je	.L988
	.p2align 4,,10
	.p2align 3
.L989:
	movq	-80(%rbp), %rdx
	movzbl	(%rdx,%rax), %edx
	movb	%dl, (%r12,%rax)
	movq	%rax, %rdx
	addq	$1, %rax
	cmpq	%rdx, %rcx
	jne	.L989
	movl	%r14d, %esi
.L988:
	leal	-1(%rbx), %ecx
	testl	%ebx, %ebx
	je	.L990
	.p2align 4,,10
	.p2align 3
.L991:
	movq	-112(%rbp), %rax
	movzbl	(%rax,%r13), %edx
	leal	(%rsi,%r13), %eax
	movb	%dl, (%r12,%rax)
	movq	%r13, %rax
	addq	$1, %r13
	cmpq	%rax, %rcx
	jne	.L991
.L990:
	movq	-136(%rbp), %rcx
	movq	16(%rcx), %rax
	movq	24(%rcx), %rcx
	movq	%rcx, %rdx
	movq	%rcx, -144(%rbp)
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L1038
	movq	-136(%rbp), %rcx
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rcx)
.L992:
	movd	%r14d, %xmm0
	movd	%ebx, %xmm1
	movq	%r12, 16(%rax)
	movq	%rax, %r13
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L959:
	cmpb	$123, %dl
	jne	.L956
	cmpb	$0, 83(%r12)
	movl	$5, %eax
	je	.L956
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L982:
	xorl	%r13d, %r13d
.L967:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L993
	call	_ZdlPv@PLT
.L993:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L945
	call	_ZdlPv@PLT
.L945:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1039
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L971:
	.cfi_restore_state
	movl	$-128, %eax
	movq	-72(%rbp), %rsi
	subl	%edx, %eax
	movb	%al, -113(%rbp)
	cmpq	-64(%rbp), %rsi
	jne	.L1040
.L979:
	leaq	-113(%rbp), %rdx
	leaq	-80(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L976:
	cmpb	$123, %dl
	jne	.L973
	cmpb	$0, 83(%r12)
	movl	$5, %eax
	je	.L973
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L953:
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rsi
	movq	%rsi, 16(%r12)
	subq	$1, %rsi
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L946:
	xorl	%eax, %eax
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	xorl	%r9d, %r9d
	movq	%rax, 16(%r12)
.L948:
	movl	$96, %r8d
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC60(%rip), %rcx
	leaq	.LC61(%rip), %rdx
	xorl	%r13d, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L960:
	cmpb	$0, 81(%r12)
	movl	$9, %eax
	je	.L956
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L958:
	cmpb	$0, 87(%r12)
	movl	$7, %eax
	je	.L956
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L970:
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rsi
	movq	%rsi, 16(%r12)
	subq	$1, %rsi
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L975:
	cmpb	$0, 87(%r12)
	movl	$7, %eax
	je	.L973
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L977:
	cmpb	$0, 81(%r12)
	movl	$9, %eax
	je	.L973
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L997:
	xorl	%r13d, %r13d
	jmp	.L945
.L1037:
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L983
.L1038:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L992
.L1039:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19099:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl11consume_sigEPNS0_4ZoneE, .-_ZN2v88internal4wasm17ModuleDecoderImpl11consume_sigEPNS0_4ZoneE
	.section	.rodata._ZN2v88internal4wasm28DecodeWasmFunctionForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneERKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPKhSE_PNS0_8CountersE.str1.8,"aMS",@progbits,1
	.align 8
.LC64:
	.string	"function_start <= function_end"
	.align 8
.LC65:
	.string	"size > maximum function size (%zu): %zu"
	.section	.text._ZN2v88internal4wasm28DecodeWasmFunctionForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneERKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPKhSE_PNS0_8CountersE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm28DecodeWasmFunctionForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneERKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPKhSE_PNS0_8CountersE
	.type	_ZN2v88internal4wasm28DecodeWasmFunctionForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneERKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPKhSE_PNS0_8CountersE, @function
_ZN2v88internal4wasm28DecodeWasmFunctionForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneERKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPKhSE_PNS0_8CountersE:
.LFB19136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rdx
	movq	24(%rbp), %r15
	movq	%rcx, -416(%rbp)
	movq	%rdx, %r8
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	subq	%r9, %r8
	cmpq	%r9, %rdx
	jb	.L1065
	cmpb	$0, 392(%r13)
	movq	%rdi, %r12
	movq	%rsi, %rax
	movq	%r9, %rbx
	leaq	1168(%r15), %rdi
	jne	.L1044
	leaq	1208(%r15), %rdi
.L1044:
	movl	%r8d, %esi
	movq	%rdx, -408(%rbp)
	movq	%rax, -400(%rbp)
	movq	%r8, -392(%rbp)
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	-392(%rbp), %r8
	movq	-400(%rbp), %rax
	movq	-408(%rbp), %rdx
	cmpq	$7654321, %r8
	ja	.L1066
	movq	%rdx, -216(%rbp)
	leaq	-176(%rbp), %rdx
	leaq	-88(%rbp), %r10
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal4wasm17ModuleDecoderImplE(%rip), %rsi
	movq	%rdx, -192(%rbp)
	movq	(%rax), %rdx
	movl	$32, %edi
	movq	%r10, -104(%rbp)
	movq	%rdx, -160(%rbp)
	movl	8(%rax), %edx
	movzbl	12(%rax), %eax
	movq	%r10, -400(%rbp)
	movl	%edx, -152(%rbp)
	movb	%al, -148(%rbp)
	movzbl	_ZN2v88internal24FLAG_assume_asmjs_originE(%rip), %eax
	movq	%rbx, -232(%rbp)
	movq	%rbx, -224(%rbp)
	leaq	-240(%rbp), %rbx
	movq	%rsi, -240(%rbp)
	movaps	%xmm0, -144(%rbp)
	movl	$0, -208(%rbp)
	movl	$0, -200(%rbp)
	movq	$0, -184(%rbp)
	movb	$0, -176(%rbp)
	movb	$1, -120(%rbp)
	movq	$0, -116(%rbp)
	movq	$0, -96(%rbp)
	movb	$0, -88(%rbp)
	movb	%al, -72(%rbp)
	movq	%r15, -128(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, -392(%rbp)
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movq	-232(%rbp), %rax
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal4wasm17ModuleDecoderImpl11consume_sigEPNS0_4ZoneE
	movq	-392(%rbp), %r9
	movq	-216(%rbp), %rdx
	movq	-400(%rbp), %r10
	movq	%rax, (%r9)
	movq	-224(%rbp), %rax
	subq	%rax, %rdx
	subq	-232(%rbp), %rax
	addl	-208(%rbp), %eax
	cmpq	$0, -184(%rbp)
	movl	%eax, 16(%r9)
	movl	%edx, 20(%r9)
	je	.L1067
	movq	-96(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1052
.L1071:
	movl	-112(%rbp), %ecx
	movq	-104(%rbp), %rax
	movl	%ecx, -384(%rbp)
	cmpq	%r10, %rax
	je	.L1068
	movq	-88(%rbp), %rsi
	leaq	-360(%rbp), %rdi
	movq	%r10, -104(%rbp)
	movq	%rax, -376(%rbp)
	movq	%rsi, -360(%rbp)
	movq	%rdx, -368(%rbp)
	movq	$0, -96(%rbp)
	movb	$0, -88(%rbp)
	movq	$0, -288(%rbp)
	movl	%ecx, -280(%rbp)
	cmpq	%rdi, %rax
	je	.L1054
	movq	%rdi, -376(%rbp)
	leaq	-256(%rbp), %rdi
	movq	%rax, -272(%rbp)
	movq	%rsi, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	$0, -368(%rbp)
	movb	$0, -360(%rbp)
	movq	$0, -336(%rbp)
	movl	%ecx, -328(%rbp)
	cmpq	%rdi, %rax
	je	.L1056
	movl	%ecx, 8(%r12)
	leaq	32(%r12), %rcx
	movq	%rcx, 16(%r12)
	leaq	-304(%rbp), %rcx
	movq	%rax, -320(%rbp)
	movq	%rsi, -304(%rbp)
	movq	%rdx, -312(%rbp)
	movq	$0, (%r12)
	cmpq	%rcx, %rax
	je	.L1058
	movq	%rax, 16(%r12)
	movq	%rsi, 32(%r12)
.L1061:
	movq	%rdx, 24(%r12)
	movl	$32, %esi
	movq	%r9, %rdi
	call	_ZdlPvm@PLT
.L1060:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImplD1Ev
.L1041:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1069
	addq	$376, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1066:
	.cfi_restore_state
	leaq	.LC65(%rip), %rdx
	xorl	%esi, %esi
	xorl	%eax, %eax
	movl	$7654321, %ecx
	leaq	-288(%rbp), %rdi
	call	_ZN2v88internal4wasm9WasmErrorC1EjPKcz
	movl	-288(%rbp), %edx
	movq	-280(%rbp), %rax
	leaq	-264(%rbp), %rsi
	movq	$0, -240(%rbp)
	movl	%edx, -232(%rbp)
	cmpq	%rsi, %rax
	je	.L1070
	movq	-264(%rbp), %rdi
	movq	-272(%rbp), %rcx
	movl	%edx, 8(%r12)
	leaq	32(%r12), %rdx
	movq	%rdx, 16(%r12)
	leaq	-208(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rdi, -208(%rbp)
	movq	%rcx, -216(%rbp)
	movq	%rsi, -280(%rbp)
	movq	$0, -272(%rbp)
	movb	$0, -264(%rbp)
	movq	$0, (%r12)
	cmpq	%rdx, %rax
	je	.L1047
	movq	%rax, 16(%r12)
	movq	%rdi, 32(%r12)
.L1049:
	movq	%rcx, 24(%r12)
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1067:
	movq	32(%r14), %rsi
	movq	-416(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r13, %r8
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl18VerifyFunctionBodyEPNS0_19AccountingAllocatorEjRKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPNS1_12WasmFunctionE
	movq	-96(%rbp), %rdx
	movq	-400(%rbp), %r10
	movq	-392(%rbp), %r9
	testq	%rdx, %rdx
	jne	.L1071
.L1052:
	movb	$0, -256(%rbp)
	leaq	32(%r12), %rax
	movdqa	-256(%rbp), %xmm0
	movq	%r9, (%r12)
	movl	$0, 8(%r12)
	movq	%rax, 16(%r12)
	movq	$0, 24(%r12)
	movaps	%xmm0, -304(%rbp)
	movups	%xmm0, 32(%r12)
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1070:
	leaq	32(%r12), %rax
	movdqu	-264(%rbp), %xmm1
	movq	$0, (%r12)
	movl	%edx, 8(%r12)
	movq	-272(%rbp), %rcx
	movq	%rax, 16(%r12)
	movaps	%xmm1, -208(%rbp)
.L1047:
	movdqa	-208(%rbp), %xmm2
	movups	%xmm2, 32(%r12)
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1065:
	leaq	.LC64(%rip), %rsi
	leaq	.LC33(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1068:
	movdqu	-88(%rbp), %xmm3
	movq	$0, -96(%rbp)
	movb	$0, -88(%rbp)
	movups	%xmm3, -360(%rbp)
.L1054:
	movdqu	-360(%rbp), %xmm4
	movaps	%xmm4, -256(%rbp)
.L1056:
	leaq	32(%r12), %rax
	movdqa	-256(%rbp), %xmm5
	movl	%ecx, 8(%r12)
	movq	$0, (%r12)
	movq	%rax, 16(%r12)
	movaps	%xmm5, -304(%rbp)
.L1058:
	movdqa	-304(%rbp), %xmm6
	movups	%xmm6, 32(%r12)
	jmp	.L1061
.L1069:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19136:
	.size	_ZN2v88internal4wasm28DecodeWasmFunctionForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneERKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPKhSE_PNS0_8CountersE, .-_ZN2v88internal4wasm28DecodeWasmFunctionForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneERKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPKhSE_PNS0_8CountersE
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv.str1.1,"aMS",@progbits,1
.LC66:
	.string	"imports count"
.LC67:
	.string	"module name"
.LC68:
	.string	"field name"
.LC69:
	.string	"signature index"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC70:
	.string	"signature index %u out of bounds (%d signatures)"
	.align 8
.LC71:
	.string	"At most one table is supported"
	.align 8
.LC72:
	.string	"Invalid type. Set --experimental-wasm-anyref to use 'AnyRef'"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv.str1.1
.LC73:
	.string	"invalid table type"
.LC74:
	.string	"invalid reference type"
.LC75:
	.string	"element count"
.LC76:
	.string	"invalid %s limits flags"
.LC77:
	.string	"elements"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv.str1.8
	.align 8
.LC78:
	.string	"At most one memory is supported"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv.str1.1
.LC79:
	.string	"invalid memory limits flags"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv.str1.8
	.align 8
.LC80:
	.string	"memory limits flags should have maximum defined if shared is true"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv.str1.1
.LC81:
	.string	"pages"
.LC82:
	.string	"memory"
.LC83:
	.string	"invalid mutability"
.LC84:
	.string	"unknown import kind 0x%02x"
.LC85:
	.string	"exception attribute"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv.str1.8
	.align 8
.LC86:
	.string	"exception attribute %u not supported"
	.align 8
.LC87:
	.string	"exception signature %u has non-void return"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv, @function
_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv:
.LFB19020:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$100000, %edx
	leaq	.LC66(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm
	movq	96(%rbx), %r13
	movabsq	$-6148914691236517205, %rcx
	movl	%eax, -124(%rbp)
	movl	%eax, %edx
	movq	208(%r13), %r8
	movq	224(%r13), %rax
	subq	%r8, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rax, %rdx
	ja	.L1162
.L1073:
	cmpq	$0, 56(%rbx)
	jne	.L1072
	movl	-124(%rbp), %edi
	testl	%edi, %edi
	je	.L1072
	xorl	%r12d, %r12d
	leaq	.L1085(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L1127:
	movq	96(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movb	$0, -80(%rbp)
	movl	$0, -76(%rbp)
	movaps	%xmm0, -96(%rbp)
	movq	216(%rdi), %rsi
	cmpq	224(%rdi), %rsi
	je	.L1080
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 216(%rdi)
.L1081:
	movq	96(%rbx), %rax
	leaq	.LC67(%rip), %rsi
	movq	%rbx, %rdi
	movq	216(%rax), %r14
	movq	16(%rbx), %rax
	movq	%rax, -120(%rbp)
	leaq	-24(%r14), %r15
	call	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.1
	leaq	.LC68(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, -24(%r14)
	call	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.1
	movq	%rax, 8(%r15)
	movq	16(%rbx), %rsi
	cmpl	%esi, 24(%rbx)
	je	.L1082
	movzbl	(%rsi), %ecx
	addq	$1, %rsi
	movq	%rsi, 16(%rbx)
	movb	%cl, -8(%r14)
	cmpb	$4, %cl
	ja	.L1083
	movslq	0(%r13,%rcx,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv,comdat
	.align 4
	.align 4
.L1085:
	.long	.L1089-.L1085
	.long	.L1088-.L1085
	.long	.L1087-.L1085
	.long	.L1086-.L1085
	.long	.L1084-.L1085
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv,comdat
	.p2align 4,,10
	.p2align 3
.L1086:
	movq	96(%rbx), %rdx
	movq	32(%rdx), %rax
	subq	24(%rdx), %rax
	movl	$1, %edx
	sarq	$5, %rax
	movl	%eax, -4(%r14)
	movq	96(%rbx), %rdi
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movl	$0, -88(%rbp)
	movl	$0, -72(%rbp)
	movw	%dx, -68(%rbp)
	movq	32(%rdi), %rsi
	cmpq	40(%rdi), %rsi
	je	.L1115
	movdqa	-96(%rbp), %xmm5
	movups	%xmm5, (%rsi)
	movdqa	-80(%rbp), %xmm6
	movups	%xmm6, 16(%rsi)
	addq	$32, 32(%rdi)
.L1116:
	movq	96(%rbx), %rax
	movq	%rbx, %rdi
	movq	32(%rax), %r15
	call	_ZN2v88internal4wasm17ModuleDecoderImpl18consume_value_typeEv
	movb	%al, -32(%r15)
	movq	16(%rbx), %rsi
	cmpl	%esi, 24(%rbx)
	je	.L1117
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%rbx)
	cmpb	$1, %al
	ja	.L1163
	movl	%eax, %edx
	andl	$1, %edx
	movb	%dl, -31(%r15)
	testb	%al, %al
	jne	.L1120
	.p2align 4,,10
	.p2align 3
.L1094:
	addl	$1, %r12d
	cmpq	$0, 56(%rbx)
	jne	.L1072
	cmpl	%r12d, -124(%rbp)
	ja	.L1127
.L1072:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1164
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1087:
	.cfi_restore_state
	movq	96(%rbx), %rax
	cmpb	$0, 18(%rax)
	jne	.L1165
	movb	$1, 18(%rax)
	movq	96(%rbx), %r14
	movq	16(%rbx), %rsi
	cmpl	%esi, 24(%rbx)
	je	.L1109
	movzbl	(%rsi), %r15d
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%rbx)
	movb	$0, 16(%r14)
	cmpb	$0, 82(%rbx)
	movl	%r15d, %eax
	jne	.L1166
	testb	$-2, %al
	jne	.L1161
.L1112:
	movq	96(%rbx), %rax
	subq	$8, %rsp
	movl	$65536, %ecx
	movq	%rbx, %rdi
	pushq	%r15
	leaq	.LC82(%rip), %rsi
	leaq	12(%rax), %rdx
	leaq	17(%rax), %r9
	pushq	%rdx
	leaq	8(%rax), %r8
	leaq	.LC81(%rip), %rdx
	pushq	$65536
	call	_ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h
	addq	$32, %rsp
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1082:
	xorl	%eax, %eax
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%rbx), %rax
	movq	%rax, 16(%rbx)
	movb	$0, -8(%r14)
.L1089:
	movq	96(%rbx), %rdx
	movl	$1, %esi
	movq	144(%rdx), %rax
	subq	136(%rdx), %rax
	sarq	$5, %rax
	movl	%eax, -4(%r14)
	movq	96(%rbx), %rax
	addl	$1, 60(%rax)
	movq	96(%rbx), %rdi
	movq	$0, -96(%rbp)
	movl	-4(%r14), %eax
	movl	$0, -84(%rbp)
	movl	%eax, -88(%rbp)
	movq	$0, -80(%rbp)
	movw	%si, -72(%rbp)
	movq	144(%rdi), %rsi
	cmpq	152(%rdi), %rsi
	je	.L1090
	movdqa	-96(%rbp), %xmm3
	leaq	-96(%rbp), %rdx
	movups	%xmm3, (%rsi)
	movdqa	-80(%rbp), %xmm4
	movups	%xmm4, 16(%rsi)
	addq	$32, 144(%rdi)
.L1091:
	movq	16(%rbx), %r14
	movq	96(%rbx), %r8
	leaq	.LC69(%rip), %rcx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	144(%r8), %r15
	movq	%r8, -120(%rbp)
	movl	$0, -96(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	-120(%rbp), %r8
	movl	%eax, %edx
	movq	88(%r8), %rax
	movq	96(%r8), %r8
	movq	%rdx, %rcx
	subq	%rax, %r8
	sarq	$3, %r8
	cmpq	%r8, %rdx
	jnb	.L1167
	movq	(%rax,%rdx,8), %rax
	movq	%rax, -32(%r15)
.L1093:
	movl	%ecx, -20(%r15)
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1084:
	cmpb	$0, 81(%rbx)
	movl	$4, %ecx
	je	.L1083
	movq	96(%rbx), %rdx
	leaq	-100(%rbp), %r9
	leaq	.LC85(%rip), %rcx
	movq	%rbx, %rdi
	movq	%r9, -120(%rbp)
	movq	264(%rdx), %rax
	subq	256(%rdx), %rax
	movq	%r9, %rdx
	sarq	$3, %rax
	movl	%eax, -4(%r14)
	movq	16(%rbx), %r15
	movq	$0, -96(%rbp)
	movq	%r15, %rsi
	movl	$0, -100(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	-120(%rbp), %r9
	testl	%eax, %eax
	jne	.L1168
.L1123:
	movq	16(%rbx), %r15
	movq	96(%rbx), %r8
	movq	%r9, %rdx
	movq	%rbx, %rdi
	leaq	.LC69(%rip), %rcx
	movl	$0, -100(%rbp)
	movq	%r15, %rsi
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	-120(%rbp), %r8
	movl	%eax, %edx
	movq	88(%r8), %rax
	movq	96(%r8), %r8
	movq	%rdx, %rcx
	subq	%rax, %r8
	sarq	$3, %r8
	cmpq	%r8, %rdx
	jnb	.L1169
	movq	(%rax,%rdx,8), %rdx
	movq	%rdx, -96(%rbp)
	testq	%rdx, %rdx
	je	.L1125
	cmpq	$0, (%rdx)
	jne	.L1170
.L1125:
	movq	96(%rbx), %rax
	movq	264(%rax), %rsi
	cmpq	272(%rax), %rsi
	je	.L1126
	movq	%rdx, (%rsi)
	addq	$8, 264(%rax)
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1088:
	movq	96(%rbx), %rax
	cmpb	$0, 87(%rbx)
	movq	184(%rax), %rdx
	movq	192(%rax), %rax
	jne	.L1095
	cmpq	%rdx, %rax
	je	.L1095
	leaq	.LC71(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1095:
	subq	%rdx, %rax
	sarq	$4, %rax
	movl	%eax, -4(%r14)
	movq	96(%rbx), %rax
	addl	$1, 64(%rax)
	movq	96(%rbx), %rdi
	movq	192(%rdi), %rsi
	cmpq	200(%rdi), %rsi
	je	.L1096
	xorl	%ecx, %ecx
	movb	$0, (%rsi)
	movq	$0, 4(%rsi)
	movw	%cx, 12(%rsi)
	movb	$0, 14(%rsi)
	addq	$16, 192(%rdi)
.L1097:
	movq	96(%rbx), %rax
	movq	192(%rax), %r15
	movb	$1, -3(%r15)
	movq	16(%rbx), %rsi
	cmpl	%esi, 24(%rbx)
	je	.L1098
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%rbx)
	cmpb	$111, %al
	je	.L1099
	movl	$7, %edx
	cmpb	$112, %al
	jne	.L1101
.L1100:
	movb	%dl, -16(%r15)
	movq	16(%rbx), %rsi
	cmpl	%esi, 24(%rbx)
	je	.L1105
	movzbl	(%rsi), %r14d
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%rbx)
	movl	%r14d, %eax
	testb	$-2, %al
	jne	.L1171
.L1107:
	subq	$8, %rsp
	leaq	-8(%r15), %rax
	leaq	-4(%r15), %r9
	movq	%rbx, %rdi
	movl	_ZN2v88internal24FLAG_wasm_max_table_sizeE(%rip), %ecx
	pushq	%r14
	leaq	-12(%r15), %r8
	leaq	.LC77(%rip), %rdx
	pushq	%rax
	leaq	.LC75(%rip), %rsi
	pushq	%rcx
	call	_ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h
	addq	$32, %rsp
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	-120(%rbp), %rsi
	leaq	.LC84(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1080:
	leaq	-96(%rbp), %rdx
	addq	$208, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm10WasmImportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1081
	.p2align 4,,10
	.p2align 3
.L1163:
	leaq	.LC83(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movb	$1, -31(%r15)
.L1120:
	movq	96(%rbx), %rax
	addl	$1, 56(%rax)
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1166:
	testb	$-4, %al
	jne	.L1161
	cmpb	$3, %r15b
	je	.L1172
	cmpb	$2, %r15b
	jne	.L1112
	leaq	.LC80(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	movl	$2, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1112
.L1098:
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%rbx), %rsi
	movq	%rsi, 16(%rbx)
	subq	$1, %rsi
.L1101:
	leaq	.LC74(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	cmpb	$0, 87(%rbx)
	jne	.L1173
.L1104:
	movq	16(%rbx), %rax
	leaq	.LC73(%rip), %rdx
	movq	%rbx, %rdi
	leaq	-1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1171:
	leaq	.LC75(%rip), %rcx
	leaq	.LC76(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1165:
	leaq	.LC78(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	216(%r13), %rdi
	leaq	(%rdx,%rdx,2), %r15
	leaq	0(,%r15,8), %r9
	xorl	%r15d, %r15d
	movq	%rdi, %r12
	subq	%r8, %r12
	testq	%rdx, %rdx
	je	.L1074
	movq	%r9, %rdi
	movq	%r9, -120(%rbp)
	call	_Znwm@PLT
	movq	216(%r13), %rdi
	movq	208(%r13), %r8
	movq	-120(%rbp), %r9
	movq	%rax, %r15
.L1074:
	movq	%r15, %rcx
	movq	%r8, %rdx
	cmpq	%rdi, %r8
	je	.L1078
	.p2align 4,,10
	.p2align 3
.L1075:
	movdqu	(%rdx), %xmm2
	addq	$24, %rdx
	addq	$24, %rcx
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdi, %rdx
	jne	.L1075
.L1078:
	testq	%r8, %r8
	je	.L1077
	movq	%r8, %rdi
	movq	%r9, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %r9
.L1077:
	addq	%r15, %r12
	movq	%r15, 208(%r13)
	addq	%r9, %r15
	movq	%r12, 216(%r13)
	movq	%r15, 224(%r13)
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1161:
	leaq	.LC79(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1099:
	cmpb	$0, 87(%rbx)
	je	.L1102
.L1103:
	movl	$6, %edx
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1167:
	leaq	.LC70(%rip), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	$0, -32(%r15)
	xorl	%ecx, %ecx
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1115:
	leaq	-96(%rbp), %rdx
	addq	$24, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm10WasmGlobalESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1090:
	leaq	-96(%rbp), %rdx
	addq	$136, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm12WasmFunctionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	-120(%rbp), %rdx
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1172:
	movb	$1, 16(%r14)
	movl	$3, %r15d
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1170:
	leaq	.LC87(%rip), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	$0, -96(%rbp)
	xorl	%edx, %edx
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1096:
	addq	$184, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9WasmTableESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1168:
	movl	%eax, %ecx
	leaq	.LC86(%rip), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-120(%rbp), %r9
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1169:
	leaq	.LC70(%rip), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	$0, -96(%rbp)
	xorl	%edx, %edx
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1117:
	xorl	%eax, %eax
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%rbx), %rax
	movq	%rax, 16(%rbx)
	movb	$0, -31(%r15)
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1126:
	leaq	-96(%rbp), %rdx
	leaq	256(%rax), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm13WasmExceptionESaIS3_EE17_M_realloc_insertIJRPNS1_9SignatureINS2_9ValueTypeEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1173:
	xorl	%edx, %edx
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1102:
	leaq	.LC72(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	cmpb	$0, 87(%rbx)
	je	.L1104
	jmp	.L1103
.L1109:
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%rbx), %rax
	movq	%rax, 16(%rbx)
	movb	$0, 16(%r14)
	jmp	.L1112
.L1105:
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%rbx), %rax
	movq	%rax, 16(%rbx)
	jmp	.L1107
.L1164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19020:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv, .-_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv
	.section	.text._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0, @function
_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0:
.LFB25951:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$368, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 56(%rdi)
	je	.L1184
.L1174:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1185
	addq	$368, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1184:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	%esi, %r13d
	leaq	-304(%rbp), %rdi
	movl	$256, %esi
	movq	%rdi, -320(%rbp)
	movq	$256, -312(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	testl	%eax, %eax
	jle	.L1186
	movq	-320(%rbp), %rsi
	cltq
	leaq	-400(%rbp), %rdi
	leaq	-384(%rbp), %r12
	movq	%r12, -400(%rbp)
	leaq	-344(%rbp), %r14
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-400(%rbp), %rax
	movl	%r13d, -368(%rbp)
	movq	%r14, -360(%rbp)
	cmpq	%r12, %rax
	je	.L1187
	movq	%rax, -360(%rbp)
	movq	-384(%rbp), %rax
	movq	%rax, -344(%rbp)
.L1179:
	movl	%r13d, 40(%rbx)
	movq	-392(%rbp), %rax
	leaq	48(%rbx), %rdi
	leaq	-360(%rbp), %rsi
	movq	%r12, -400(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -392(%rbp)
	movb	$0, -384(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_@PLT
	movq	-360(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1180
	call	_ZdlPv@PLT
.L1180:
	movq	-400(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1174
	call	_ZdlPv@PLT
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1187:
	movdqa	-384(%rbp), %xmm0
	movups	%xmm0, -344(%rbp)
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1186:
	leaq	.LC37(%rip), %rsi
	leaq	.LC33(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25951:
	.size	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0, .-_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0, @function
_ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0:
.LFB25952:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L1189
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L1189:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %rcx
	movl	$24, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	.LC38(%rip), %rdx
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1192
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1192:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25952:
	.size	_ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0, .-_ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0
	.section	.text._ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0, @function
_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0:
.LFB25953:
	.cfi_startproc
	movq	%rsi, %rcx
	leaq	.LC38(%rip), %rdx
	movq	16(%rdi), %rsi
	xorl	%eax, %eax
	subq	8(%rdi), %rsi
	addl	32(%rdi), %esi
	jmp	_ZN2v88internal4wasm7Decoder6errorfEjPKcz.constprop.0
	.cfi_endproc
.LFE25953:
	.size	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0, .-_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1, @function
_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1:
.LFB25954:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L1195
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L1195:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	subq	8(%rdi), %rsi
	leaq	-208(%rbp), %rcx
	movq	%rax, -200(%rbp)
	addl	32(%rdi), %esi
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$24, -208(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.0
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1198
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1198:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25954:
	.size	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1, .-_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.1,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.1, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.1:
.LFB25959:
	.cfi_startproc
	cmpq	%rsi, 24(%rdi)
	ja	.L1206
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsi, 16(%rdi)
	movl	$0, (%rdx)
	leaq	.LC40(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1206:
	.cfi_restore 6
	movzbl	(%rsi), %r8d
	addq	$1, %rsi
	movl	%r8d, %eax
	andl	$127, %eax
	testb	%r8b, %r8b
	js	.L1207
	movq	%rsi, 16(%rdi)
	movl	$1, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L1207:
	movl	%eax, %r8d
	jmp	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_
	.cfi_endproc
.LFE25959:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.1, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.1
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.0, @function
_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.0:
.LFB25962:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC54(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	leaq	-60(%rbp), %rdx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -60(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.1
	movq	16(%rbx), %r8
	movl	%eax, %r12d
	movq	%r8, %r13
	subq	8(%rbx), %r13
	addl	32(%rbx), %r13d
	testl	%eax, %eax
	jne	.L1209
.L1221:
	movq	56(%rbx), %rax
.L1210:
	testq	%rax, %rax
	movl	$0, %eax
	cmovne	%eax, %r12d
	movl	%r13d, %eax
	salq	$32, %r12
	orq	%r12, %rax
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1222
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1209:
	.cfi_restore_state
	movq	24(%rbx), %rax
	subq	%r8, %rax
	cmpl	%eax, %r12d
	ja	.L1211
	movl	%r12d, %eax
	addq	%r8, %rax
	movq	%rax, 16(%rbx)
.L1212:
	movq	56(%rbx), %rax
	testq	%rax, %rax
	jne	.L1210
	testb	%r14b, %r14b
	je	.L1210
	movq	%r8, %rdi
	movl	%r12d, %esi
	movq	%r8, -72(%rbp)
	call	_ZN7unibrow4Utf816ValidateEncodingEPKhm@PLT
	movq	-72(%rbp), %r8
	testb	%al, %al
	jne	.L1221
	movq	%r15, %rcx
	leaq	.LC55(%rip), %rdx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1211:
	movq	%r8, %rsi
	xorl	%eax, %eax
	movl	%r12d, %ecx
	movq	%rbx, %rdi
	leaq	.LC39(%rip), %rdx
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	24(%rbx), %rax
	movq	-72(%rbp), %r8
	movq	%rax, 16(%rbx)
	jmp	.L1212
.L1222:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25962:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.0, .-_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.0
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_115FindNameSectionEPNS1_7DecoderE.constprop.0.str1.1,"aMS",@progbits,1
.LC88:
	.string	"longer"
.LC89:
	.string	"shorter"
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_115FindNameSectionEPNS1_7DecoderE.constprop.0.str1.8,"aMS",@progbits,1
	.align 8
.LC90:
	.string	"section was %s than expected size (%u bytes expected, %zu decoded)"
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_115FindNameSectionEPNS1_7DecoderE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_115FindNameSectionEPNS1_7DecoderE.constprop.0, @function
_ZN2v88internal4wasm12_GLOBAL__N_115FindNameSectionEPNS1_7DecoderE.constprop.0:
.LFB25977:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$112, %rsp
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpl	$7, %eax
	jbe	.L1224
	addq	$8, %rsi
	movq	%rsi, 16(%rdi)
.L1225:
	leaq	-128(%rbp), %r12
	movq	%rbx, -128(%rbp)
	movq	%r12, %rdi
	movb	$0, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_119WasmSectionIterator4nextEv
	cmpq	$0, 56(%rbx)
	jne	.L1226
	leaq	.LC88(%rip), %r14
	leaq	.LC89(%rip), %r13
.L1239:
	movq	-128(%rbp), %rdi
	cmpq	$0, 56(%rdi)
	jne	.L1229
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	cmpq	%rax, %rsi
	jnb	.L1229
	cmpb	$14, -120(%rbp)
	je	.L1231
	movq	-96(%rbp), %r8
	cmpq	%r8, %rsi
	jb	.L1247
	movq	%r14, %rcx
	je	.L1237
.L1238:
	movq	-112(%rbp), %rax
	movq	%rsi, %r9
	leaq	.LC90(%rip), %rdx
	subq	%rax, %r9
	subl	%eax, %r8d
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L1237:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_119WasmSectionIterator4nextEv
	cmpq	$0, 56(%rbx)
	je	.L1239
.L1226:
	movq	-128(%rbp), %rax
	cmpq	$0, 56(%rax)
	jne	.L1229
	movq	24(%rax), %rcx
	cmpq	%rcx, 16(%rax)
	jnb	.L1229
.L1231:
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rax
	leaq	48(%rbx), %rdi
	leaq	-56(%rbp), %r12
	movq	16(%rbx), %rcx
	subq	8(%rbx), %rcx
	movl	$0, 40(%rbx)
	leaq	-72(%rbp), %rsi
	subl	%edx, %eax
	addl	%ecx, 32(%rbx)
	addq	%rdx, %rax
	movq	%rdx, 8(%rbx)
	movq	%rdx, 16(%rbx)
	movq	%rax, 24(%rbx)
	movl	$0, -76(%rbp)
	movl	$0, -80(%rbp)
	movq	%r12, -72(%rbp)
	movq	$0, -64(%rbp)
	movb	$0, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_@PLT
	movq	-72(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1240
	call	_ZdlPv@PLT
.L1240:
	movl	$1, %eax
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1229:
	xorl	%eax, %eax
.L1223:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1248
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1247:
	.cfi_restore_state
	subq	%rsi, %r8
	subq	%rsi, %rax
	cmpl	%eax, %r8d
	ja	.L1234
	movl	%r8d, %r8d
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L1235:
	movq	-96(%rbp), %r8
	cmpq	%rsi, %r8
	je	.L1237
	movq	%r14, %rcx
	cmova	%r13, %rcx
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1224:
	movl	$8, %ecx
	leaq	.LC39(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	24(%rbx), %rsi
	movq	%rsi, 16(%rbx)
	jmp	.L1225
.L1234:
	xorl	%eax, %eax
	movl	%r8d, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%rdi, -136(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-136(%rbp), %rdi
	movq	24(%rdi), %rax
	movq	%rax, 16(%rdi)
	movq	-128(%rbp), %rdi
	movq	16(%rdi), %rsi
	jmp	.L1235
.L1248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25977:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_115FindNameSectionEPNS1_7DecoderE.constprop.0, .-_ZN2v88internal4wasm12_GLOBAL__N_115FindNameSectionEPNS1_7DecoderE.constprop.0
	.section	.rodata._ZN2v88internal4wasm20DecodeCustomSectionsEPKhS3_.str1.1,"aMS",@progbits,1
.LC91:
	.string	"name length"
.LC92:
	.string	"invalid section length"
	.section	.text._ZN2v88internal4wasm20DecodeCustomSectionsEPKhS3_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm20DecodeCustomSectionsEPKhS3_
	.type	_ZN2v88internal4wasm20DecodeCustomSectionsEPKhS3_, @function
_ZN2v88internal4wasm20DecodeCustomSectionsEPKhS3_:
.LFB19161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rsi, -136(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rdx, %rax
	subq	%rsi, %rax
	movq	%rsi, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movl	$0, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	cmpl	$3, %eax
	jbe	.L1250
	leaq	4(%rsi), %r8
	movq	%rdx, %rax
	subq	%r8, %rax
	cmpl	$3, %eax
	jbe	.L1315
	addq	$8, %rsi
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rdi)
	movq	%rsi, -128(%rbp)
	movups	%xmm0, (%rdi)
	cmpq	%rsi, %rdx
	jbe	.L1274
	leaq	-176(%rbp), %r15
	movq	%r15, -184(%rbp)
	movq	%rdi, %r15
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1257:
	movzbl	1(%rsi), %eax
	leaq	2(%rsi), %r9
	movl	%eax, %r14d
	andl	$127, %r14d
	testb	%al, %al
	js	.L1316
.L1259:
	movq	%r9, -128(%rbp)
.L1300:
	testb	%bl, %bl
	jne	.L1317
	movq	-136(%rbp), %rax
	movl	-112(%rbp), %r12d
	movq	%r9, %rbx
	subq	%rax, %rbx
	addl	%r12d, %ebx
	cmpq	%r9, %rdx
	ja	.L1318
.L1275:
	leaq	.LC91(%rip), %rcx
	movq	%r9, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
.L1285:
	movq	-128(%rbp), %rsi
	xorl	%r10d, %r10d
	movq	%rsi, %r9
	subq	-136(%rbp), %r9
	addl	-112(%rbp), %r9d
	movl	%r9d, %r12d
.L1296:
	movl	%r12d, %eax
	movq	%rsi, -128(%rbp)
	subl	%ebx, %eax
	cmpl	%r14d, %eax
	ja	.L1319
.L1290:
	movq	-120(%rbp), %rax
	leal	(%rbx,%r14), %ecx
	subl	%r12d, %ecx
	subq	%rsi, %rax
	cmpl	%eax, %ecx
	ja	.L1291
	movl	%ecx, %eax
	addq	%rax, %rsi
	cmpq	$0, -88(%rbp)
	movq	%rsi, -128(%rbp)
	jne	.L1313
.L1326:
	movd	%r9d, %xmm1
	movd	%r10d, %xmm2
	movd	%ebx, %xmm0
	movl	%r12d, -160(%rbp)
	movd	%r14d, %xmm3
	punpckldq	%xmm2, %xmm1
	movl	%ecx, -156(%rbp)
	movq	8(%r15), %rsi
	punpckldq	%xmm3, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -176(%rbp)
	cmpq	16(%r15), %rsi
	je	.L1293
	movdqa	-176(%rbp), %xmm4
	movups	%xmm4, (%rsi)
	movq	-160(%rbp), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 8(%r15)
.L1294:
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
.L1273:
	cmpq	%rdx, %rsi
	jnb	.L1313
.L1254:
	cmpl	%esi, %edx
	je	.L1256
	leaq	1(%rsi), %r9
	movzbl	(%rsi), %ebx
	movq	%r9, -128(%rbp)
	cmpq	%rdx, %r9
	jb	.L1257
.L1258:
	leaq	.LC57(%rip), %rcx
	movq	%r9, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
.L1270:
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %rdx
	testb	%bl, %bl
	jne	.L1303
.L1322:
	movq	-136(%rbp), %rax
	movl	-112(%rbp), %r12d
	movq	%r9, %rbx
	xorl	%r14d, %r14d
	subq	%rax, %rbx
	addl	%r12d, %ebx
	cmpq	%r9, %rdx
	jbe	.L1275
.L1318:
	movzbl	(%r9), %ecx
	leaq	1(%r9), %rsi
	movl	%ecx, %r10d
	andl	$127, %r10d
	testb	%cl, %cl
	js	.L1320
.L1276:
	movq	%rsi, -128(%rbp)
.L1297:
	movq	%rsi, %r9
	subq	%rsi, %rdx
	subq	%rax, %r9
	addl	%r12d, %r9d
	cmpl	%edx, %r10d
	ja	.L1288
	movl	%r10d, %edx
	addq	%rdx, %rsi
	movq	%rsi, %rdi
	subq	%rax, %rdi
	addl	%edi, %r12d
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1316:
	cmpq	%r9, %rdx
	jbe	.L1260
	movzbl	2(%rsi), %ecx
	leaq	3(%rsi), %r9
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r14d
	testb	%cl, %cl
	jns	.L1259
	cmpq	%rdx, %r9
	jnb	.L1260
	movzbl	3(%rsi), %ecx
	leaq	4(%rsi), %r9
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r14d
	testb	%cl, %cl
	jns	.L1259
	cmpq	%rdx, %r9
	jnb	.L1260
	movzbl	4(%rsi), %ecx
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r14d
	testb	%cl, %cl
	js	.L1321
	leaq	5(%rsi), %r9
	movq	%r9, -128(%rbp)
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1260:
	leaq	.LC40(%rip), %rdx
	movq	%r9, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC57(%rip), %rcx
	movq	%r9, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %rdx
	testb	%bl, %bl
	je	.L1322
.L1303:
	movq	%r9, %rsi
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	%rdx, %rax
	subq	%r9, %rax
	cmpl	%eax, %r14d
	ja	.L1272
	movl	%r14d, %esi
	addq	%r9, %rsi
.L1298:
	movq	%rsi, -128(%rbp)
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1320:
	cmpq	%rdx, %rsi
	jnb	.L1283
	movzbl	1(%r9), %edi
	leaq	2(%r9), %rsi
	movl	%edi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %r10d
	testb	%dil, %dil
	jns	.L1276
	cmpq	%rdx, %rsi
	jnb	.L1283
	movzbl	2(%r9), %edi
	leaq	3(%r9), %rsi
	movl	%edi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %r10d
	testb	%dil, %dil
	jns	.L1276
	cmpq	%rdx, %rsi
	jnb	.L1283
	movzbl	3(%r9), %edi
	leaq	4(%r9), %rsi
	movl	%edi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %r10d
	testb	%dil, %dil
	jns	.L1276
	cmpq	%rdx, %rsi
	jnb	.L1283
	movzbl	4(%r9), %ecx
	addq	$5, %r9
	movq	%r9, -128(%rbp)
	movl	%ecx, %r11d
	andl	$-16, %r11d
	testb	%cl, %cl
	js	.L1323
	testb	%r11b, %r11b
	je	.L1324
.L1287:
	leaq	.LC41(%rip), %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1283:
	leaq	.LC91(%rip), %rcx
	movq	%r13, %rdi
	xorl	%eax, %eax
	movq	%rsi, -128(%rbp)
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1293:
	movq	-184(%rbp), %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm19CustomSectionOffsetESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1256:
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-120(%rbp), %r9
	movq	%r9, -128(%rbp)
	jmp	.L1258
.L1250:
	movl	$4, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-120(%rbp), %r8
	movq	%r8, -128(%rbp)
.L1253:
	xorl	%eax, %eax
	movl	$4, %ecx
	movq	%r8, %rsi
	movq	%r13, %rdi
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-120(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r14)
	movups	%xmm0, (%r14)
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L1274:
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -144(%rbp)
	cmpq	-192(%rbp), %rdi
	je	.L1249
	call	_ZdlPv@PLT
.L1249:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1325
	addq	$168, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1288:
	.cfi_restore_state
	movl	%r10d, %ecx
	xorl	%eax, %eax
	leaq	.LC39(%rip), %rdx
	movq	%r13, %rdi
	movl	%r9d, -204(%rbp)
	movl	%r10d, -200(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-120(%rbp), %rsi
	movl	-204(%rbp), %r9d
	movl	-200(%rbp), %r10d
	movq	%rsi, %r12
	subq	-136(%rbp), %r12
	addl	-112(%rbp), %r12d
	movq	%rsi, -128(%rbp)
	movl	%r12d, %eax
	subl	%ebx, %eax
	cmpl	%r14d, %eax
	jbe	.L1290
	.p2align 4,,10
	.p2align 3
.L1319:
	leaq	.LC92(%rip), %rsi
	movq	%r13, %rdi
	movq	%r15, %r14
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1291:
	xorl	%eax, %eax
	movq	%r13, %rdi
	leaq	.LC39(%rip), %rdx
	movl	%r10d, -208(%rbp)
	movl	%r9d, -204(%rbp)
	movl	%ecx, -200(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-120(%rbp), %rax
	cmpq	$0, -88(%rbp)
	movl	-208(%rbp), %r10d
	movl	-204(%rbp), %r9d
	movq	%rax, -128(%rbp)
	movl	-200(%rbp), %ecx
	je	.L1326
	.p2align 4,,10
	.p2align 3
.L1313:
	movq	%r15, %r14
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1272:
	movl	%r14d, %r12d
	xorl	%eax, %eax
	leaq	.LC39(%rip), %rdx
	movq	%r9, %rsi
	movl	%r12d, %ecx
	movq	%r13, %rdi
	movq	%r15, %r14
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-120(%rbp), %rax
	movq	%rax, -128(%rbp)
	jmp	.L1274
.L1321:
	leaq	5(%rsi), %r12
	cmpq	%rdx, %r12
	jnb	.L1266
	movzbl	5(%rsi), %eax
	addq	$6, %rsi
	movq	%rsi, -128(%rbp)
	testb	%al, %al
	js	.L1267
	movl	%eax, %edx
	andl	$-16, %eax
	sall	$28, %edx
	movl	%eax, %r9d
	orl	%edx, %r14d
.L1268:
	testb	%r9b, %r9b
	je	.L1269
	leaq	.LC41(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1270
.L1266:
	leaq	.LC57(%rip), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r12, -128(%rbp)
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L1269:
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %rdx
	jmp	.L1300
.L1267:
	andl	$-16, %eax
	leaq	.LC57(%rip), %rcx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movb	%al, -200(%rbp)
	leaq	.LC40(%rip), %rdx
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-200(%rbp), %r9d
	jmp	.L1268
.L1323:
	xorl	%eax, %eax
	leaq	.LC91(%rip), %rcx
	movq	%r13, %rdi
	movb	%r11b, -204(%rbp)
	leaq	.LC40(%rip), %rdx
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-204(%rbp), %r11d
	movq	-200(%rbp), %rsi
	testb	%r11b, %r11b
	jne	.L1287
	jmp	.L1285
.L1324:
	sall	$28, %ecx
	movq	%r9, %rsi
	orl	%ecx, %r10d
	jmp	.L1297
.L1315:
	movq	%r8, -128(%rbp)
	jmp	.L1253
.L1325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19161:
	.size	_ZN2v88internal4wasm20DecodeCustomSectionsEPKhS3_, .-_ZN2v88internal4wasm20DecodeCustomSectionsEPKhS3_
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17DecodeNameSectionEv.str1.1,"aMS",@progbits,1
.LC93:
	.string	"name type if not varuint7"
.LC94:
	.string	"name payload length"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl17DecodeNameSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeNameSectionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeNameSectionEv
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeNameSectionEv, @function
_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeNameSectionEv:
.LFB19034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	124(%rdi), %edx
	movq	16(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rcx
	movq	24(%rdi), %rax
	testb	$64, %dh
	jne	.L1328
	orb	$64, %dh
	leaq	-80(%rbp), %r12
	movq	%rsi, -128(%rbp)
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r13
	movl	%edx, 124(%rdi)
	movl	32(%rdi), %edx
	leaq	-144(%rbp), %rbx
	movq	%r13, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rax, -120(%rbp)
	movl	%edx, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L1329:
	cmpq	%rsi, %rax
	jbe	.L1348
	cmpl	%esi, %eax
	je	.L1331
	movzbl	(%rsi), %r15d
	addq	$1, %rsi
	movq	%rsi, -128(%rbp)
	testb	%r15b, %r15b
	js	.L1367
.L1333:
	cmpq	%rsi, %rax
	jbe	.L1334
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %r8
	movl	%edx, %ecx
	andl	$127, %ecx
	testb	%dl, %dl
	js	.L1368
.L1335:
	movq	%r8, -128(%rbp)
.L1355:
	subq	%r8, %rax
	cmpl	%eax, %ecx
	ja	.L1369
.L1347:
	testb	%r15b, %r15b
	jne	.L1370
	xorl	%esi, %esi
	leaq	.LC67(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.0
	cmpq	$0, -88(%rbp)
	movq	%rax, %r15
	je	.L1371
.L1348:
	movq	-96(%rbp), %rdi
	movq	%r13, -144(%rbp)
	cmpq	%r12, %rdi
	je	.L1352
	call	_ZdlPv@PLT
.L1352:
	movq	24(%r14), %rax
	movq	8(%r14), %rcx
	movq	16(%r14), %rsi
.L1328:
	movq	%rax, %rbx
	subq	%rsi, %rax
	subq	%rcx, %rbx
	movq	%rbx, %rcx
	cmpl	%eax, %ebx
	ja	.L1353
	movl	%ebx, %ecx
	addq	%rsi, %rcx
	movq	%rcx, 16(%r14)
.L1327:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1372
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1370:
	.cfi_restore_state
	addq	%r8, %rcx
	movq	-88(%rbp), %rax
	movq	%rcx, -128(%rbp)
.L1351:
	testq	%rax, %rax
	jne	.L1348
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1331:
	movl	$1, %ecx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-120(%rbp), %rsi
	movq	%rsi, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L1334:
	leaq	.LC94(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
.L1344:
	movq	-128(%rbp), %r8
	xorl	%ecx, %ecx
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1368:
	cmpq	%rax, %r8
	jnb	.L1342
	movzbl	1(%rsi), %edi
	leaq	2(%rsi), %r8
	movl	%edi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L1335
	cmpq	%rax, %r8
	jnb	.L1342
	movzbl	2(%rsi), %edi
	leaq	3(%rsi), %r8
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L1335
	cmpq	%rax, %r8
	jnb	.L1342
	movzbl	3(%rsi), %edi
	leaq	4(%rsi), %r8
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L1335
	cmpq	%rax, %r8
	jnb	.L1342
	movzbl	4(%rsi), %edx
	addq	$5, %rsi
	movq	%rsi, -128(%rbp)
	movl	%edx, %r9d
	andl	$-16, %r9d
	testb	%dl, %dl
	js	.L1373
	sall	$28, %edx
	orl	%edx, %ecx
	testb	%r9b, %r9b
	je	.L1374
.L1346:
	leaq	.LC41(%rip), %rdx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1367:
	leaq	.LC93(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1342:
	leaq	.LC94(%rip), %rcx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1371:
	movq	%rax, %rsi
	movl	%eax, %edi
	subl	-112(%rbp), %edi
	addq	-136(%rbp), %rdi
	shrq	$32, %rsi
	call	_ZN7unibrow4Utf816ValidateEncodingEPKhm@PLT
	testb	%al, %al
	je	.L1366
	movq	96(%r14), %rax
	movq	%r15, 80(%rax)
.L1366:
	movq	-88(%rbp), %rax
	jmp	.L1351
	.p2align 4,,10
	.p2align 3
.L1369:
	leaq	-144(%rbp), %rdi
	leaq	.LC39(%rip), %rdx
	movq	%r8, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	jmp	.L1348
	.p2align 4,,10
	.p2align 3
.L1353:
	xorl	%eax, %eax
	leaq	.LC39(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r14), %rax
	movq	%rax, 16(%r14)
	jmp	.L1327
.L1373:
	movq	%r8, %rsi
	xorl	%eax, %eax
	leaq	.LC94(%rip), %rcx
	movq	%rbx, %rdi
	leaq	.LC40(%rip), %rdx
	movb	%r9b, -153(%rbp)
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-153(%rbp), %r9d
	movq	-152(%rbp), %r8
	testb	%r9b, %r9b
	jne	.L1346
	jmp	.L1344
.L1374:
	movq	%rsi, %r8
	jmp	.L1355
.L1372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19034:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeNameSectionEv, .-_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeNameSectionEv
	.section	.rodata._ZN2v88internal4wasm16DecodeLocalNamesEPKhS3_PNS1_10LocalNamesE.str1.1,"aMS",@progbits,1
.LC95:
	.string	"local names count"
.LC96:
	.string	"function index"
.LC97:
	.string	"namings count"
.LC98:
	.string	"local index"
.LC99:
	.string	"local name"
	.section	.text._ZN2v88internal4wasm16DecodeLocalNamesEPKhS3_PNS1_10LocalNamesE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm16DecodeLocalNamesEPKhS3_PNS1_10LocalNamesE
	.type	_ZN2v88internal4wasm16DecodeLocalNamesEPKhS3_PNS1_10LocalNamesE, @function
_ZN2v88internal4wasm16DecodeLocalNamesEPKhS3_PNS1_10LocalNamesE:
.LFB19199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rdi, -136(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rdi, -128(%rbp)
	movq	%r15, %rdi
	movq	%rsi, -120(%rbp)
	movl	$0, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	%rax, -240(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_115FindNameSectionEPNS1_7DecoderE.constprop.0
	testb	%al, %al
	je	.L1386
.L1388:
	cmpq	$0, -88(%rbp)
	jne	.L1386
.L1467:
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	cmpq	%rdx, %rsi
	jnb	.L1386
	cmpl	%esi, %edx
	je	.L1381
	movzbl	(%rsi), %ebx
	leaq	1(%rsi), %r8
	movq	%r8, -128(%rbp)
	testb	%bl, %bl
	js	.L1386
	movl	$0, -148(%rbp)
	cmpq	%r8, %rdx
	jbe	.L1383
	movzbl	1(%rsi), %eax
	addq	$2, %rsi
	movl	%eax, %r8d
	andl	$127, %r8d
	testb	%al, %al
	js	.L1465
	subq	%rsi, %rdx
	movq	%rsi, -128(%rbp)
	cmpl	%edx, %r8d
	ja	.L1466
.L1385:
	cmpb	$2, %bl
	je	.L1387
.L1469:
	addq	%r8, %rsi
	cmpq	$0, -88(%rbp)
	movq	%rsi, -128(%rbp)
	je	.L1467
	.p2align 4,,10
	.p2align 3
.L1386:
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -144(%rbp)
	cmpq	-240(%rbp), %rdi
	je	.L1375
	call	_ZdlPv@PLT
.L1375:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1468
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1381:
	.cfi_restore_state
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-120(%rbp), %r8
	movl	$0, -148(%rbp)
	movq	%r8, -128(%rbp)
.L1383:
	movq	%r8, %rsi
	leaq	.LC94(%rip), %rcx
	movq	%r15, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-128(%rbp), %rsi
	xorl	%r8d, %r8d
	cmpb	$2, %bl
	jne	.L1469
.L1387:
	leaq	-148(%rbp), %rax
	movq	%r15, %rdi
	leaq	.LC95(%rip), %rcx
	movl	$0, -148(%rbp)
	movq	%rax, %rdx
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.1
	movl	%eax, -176(%rbp)
	testl	%eax, %eax
	je	.L1388
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movl	$0, -172(%rbp)
	cmpq	%rdx, %rsi
	jb	.L1470
	.p2align 4,,10
	.p2align 3
.L1390:
	leaq	.LC96(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
.L1400:
	xorl	%eax, %eax
.L1432:
	movq	-184(%rbp), %rbx
	movl	%eax, -148(%rbp)
	movq	16(%rbx), %rsi
	cmpq	24(%rbx), %rsi
	je	.L1403
	pxor	%xmm0, %xmm0
	movl	%eax, (%rsi)
	movq	%rbx, %rax
	movl	$-1, 4(%rsi)
	movq	$0, 24(%rsi)
	movups	%xmm0, 8(%rsi)
	movq	16(%rbx), %rbx
	leaq	32(%rbx), %r14
	movq	%rbx, -168(%rbp)
	movq	%r14, 16(%rax)
.L1404:
	movq	-184(%rbp), %rbx
	movl	-32(%r14), %eax
	movq	%r15, %rdi
	leaq	.LC97(%rip), %rcx
	movq	-192(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movl	$0, -148(%rbp)
	cmpl	%eax, (%rbx)
	cmovge	(%rbx), %eax
	movl	%eax, (%rbx)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.1
	movl	%eax, -168(%rbp)
	testl	%eax, %eax
	je	.L1402
	xorl	%r13d, %r13d
	jmp	.L1427
	.p2align 4,,10
	.p2align 3
.L1405:
	leaq	.LC98(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
.L1417:
	movl	$1, %esi
	leaq	.LC99(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.0
	movl	%eax, %esi
	shrq	$32, %rax
	cmpq	$0, -88(%rbp)
	movq	%rax, %rbx
	jne	.L1402
	xorl	%ecx, %ecx
.L1430:
	cmpl	%ecx, -28(%r14)
	movl	%ecx, %eax
	cmovge	-28(%r14), %eax
	movq	-16(%r14), %rdx
	movl	%eax, -28(%r14)
	cmpq	-8(%r14), %rdx
	je	.L1420
	movl	%ecx, (%rdx)
	movl	%esi, 4(%rdx)
	movl	%ebx, 8(%rdx)
	addq	$12, -16(%r14)
.L1419:
	addl	$1, %r13d
	cmpl	-168(%rbp), %r13d
	je	.L1402
.L1427:
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	cmpq	%rax, %rsi
	jnb	.L1405
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %r8
	movl	%edx, %r12d
	andl	$127, %r12d
	testb	%dl, %dl
	js	.L1471
.L1406:
	leaq	.LC99(%rip), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.0
	cmpq	$0, -88(%rbp)
	jne	.L1402
	movl	%eax, %esi
	shrq	$32, %rax
	movl	%r12d, %ecx
	movq	%rax, %rbx
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1465:
	leaq	-148(%rbp), %rdx
	leaq	.LC94(%rip), %rcx
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movl	%eax, %r8d
	subq	%rsi, %rdx
	cmpl	%edx, %r8d
	jbe	.L1385
.L1466:
	movl	%r8d, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1478:
	testl	%eax, %eax
	jns	.L1432
.L1402:
	addl	$1, -172(%rbp)
	movl	-172(%rbp), %eax
	cmpl	-176(%rbp), %eax
	je	.L1388
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	cmpq	%rdx, %rsi
	jnb	.L1390
.L1470:
	movzbl	(%rsi), %ecx
	leaq	1(%rsi), %r8
	movl	%ecx, %eax
	andl	$127, %eax
	testb	%cl, %cl
	js	.L1472
.L1391:
	movq	%r8, -128(%rbp)
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1471:
	cmpq	%r8, %rax
	jbe	.L1407
	movzbl	1(%rsi), %edi
	leaq	2(%rsi), %r8
	movl	%edi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r12d
	testb	%dil, %dil
	jns	.L1406
	cmpq	%r8, %rax
	jbe	.L1407
	movzbl	2(%rsi), %edi
	leaq	3(%rsi), %r8
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r12d
	testb	%dil, %dil
	jns	.L1406
	cmpq	%r8, %rax
	jbe	.L1407
	movzbl	3(%rsi), %edi
	leaq	4(%rsi), %r8
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r12d
	testb	%dil, %dil
	jns	.L1406
	cmpq	%r8, %rax
	jbe	.L1413
	movzbl	4(%rsi), %eax
	addq	$5, %rsi
	movq	%rsi, -128(%rbp)
	movl	%eax, %ebx
	andl	$-16, %ebx
	testb	%al, %al
	js	.L1414
	sall	$28, %eax
	orl	%eax, %r12d
.L1415:
	testb	%bl, %bl
	je	.L1416
	leaq	.LC41(%rip), %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1407:
	leaq	.LC98(%rip), %rcx
	movq	%r8, %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1420:
	movq	-24(%r14), %r8
	movq	%rdx, %r10
	movabsq	$-6148914691236517205, %rdi
	subq	%r8, %r10
	movq	%r10, %rax
	sarq	$2, %rax
	imulq	%rdi, %rax
	movabsq	$768614336404564650, %rdi
	cmpq	%rdi, %rax
	je	.L1473
	testq	%rax, %rax
	je	.L1438
	movabsq	$9223372036854775800, %r12
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1474
.L1422:
	movq	%r12, %rdi
	movq	%r10, -232(%rbp)
	movq	%r8, -224(%rbp)
	movq	%rdx, -216(%rbp)
	movl	%ecx, -208(%rbp)
	movl	%esi, -200(%rbp)
	call	_Znwm@PLT
	movl	-200(%rbp), %esi
	movl	-208(%rbp), %ecx
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %r8
	leaq	(%rax,%r12), %r9
	leaq	12(%rax), %r12
	movq	-232(%rbp), %r10
.L1423:
	addq	%rax, %r10
	movl	%ecx, (%r10)
	movl	%esi, 4(%r10)
	movl	%ebx, 8(%r10)
	cmpq	%r8, %rdx
	je	.L1424
	movq	%rax, %rsi
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L1425:
	movq	(%rcx), %rdi
	addq	$12, %rcx
	addq	$12, %rsi
	movq	%rdi, -12(%rsi)
	movl	-4(%rcx), %edi
	movl	%edi, -4(%rsi)
	cmpq	%rcx, %rdx
	jne	.L1425
	movabsq	$3074457345618258603, %rbx
	subq	$12, %rdx
	subq	%r8, %rdx
	shrq	$2, %rdx
	imulq	%rbx, %rdx
	movabsq	$4611686018427387903, %rbx
	andq	%rbx, %rdx
	leaq	6(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,4), %r12
.L1424:
	testq	%r8, %r8
	je	.L1426
	movq	%r8, %rdi
	movq	%r9, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %r9
	movq	-200(%rbp), %rax
.L1426:
	movq	%rax, %xmm0
	movq	%r12, %xmm1
	movq	%r9, -8(%r14)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -24(%r14)
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1472:
	cmpq	%r8, %rdx
	jbe	.L1392
	movzbl	1(%rsi), %edi
	leaq	2(%rsi), %r8
	movl	%edi, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%ecx, %eax
	testb	%dil, %dil
	jns	.L1391
	cmpq	%r8, %rdx
	jbe	.L1392
	movzbl	2(%rsi), %edi
	leaq	3(%rsi), %r8
	movl	%edi, %ecx
	sall	$14, %ecx
	andl	$2080768, %ecx
	orl	%ecx, %eax
	testb	%dil, %dil
	jns	.L1391
	cmpq	%r8, %rdx
	jbe	.L1392
	movzbl	3(%rsi), %edi
	leaq	4(%rsi), %r12
	movl	%edi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ecx, %eax
	testb	%dil, %dil
	js	.L1475
	movq	%r12, -128(%rbp)
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1474:
	testq	%rdi, %rdi
	jne	.L1476
	movl	$12, %r12d
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L1438:
	movl	$12, %r12d
	jmp	.L1422
	.p2align 4,,10
	.p2align 3
.L1392:
	leaq	.LC96(%rip), %rcx
	movq	%r8, %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1403:
	movq	-192(%rbp), %rdx
	leaq	8(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm21LocalNamesPerFunctionESaIS3_EE17_M_realloc_insertIJiEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movq	16(%rbx), %r14
	jmp	.L1404
.L1413:
	movq	%r8, %rsi
	xorl	%eax, %eax
	leaq	.LC98(%rip), %rcx
	movq	%r15, %rdi
	leaq	.LC40(%rip), %rdx
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	leaq	.LC99(%rip), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.0
	cmpq	$0, -88(%rbp)
	jne	.L1402
	xorl	%ecx, %ecx
.L1436:
	movl	%eax, %esi
	shrq	$32, %rax
	movq	%rax, %rbx
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1475:
	cmpq	%r12, %rdx
	jbe	.L1398
	movzbl	4(%rsi), %edx
	addq	$5, %rsi
	movq	%rsi, -128(%rbp)
	movl	%edx, %ebx
	andl	$-16, %ebx
	testb	%dl, %dl
	js	.L1477
	sall	$28, %edx
	orl	%edx, %eax
	testb	%bl, %bl
	je	.L1478
.L1401:
	leaq	.LC41(%rip), %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1416:
	leaq	.LC99(%rip), %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.0
	cmpq	$0, -88(%rbp)
	jne	.L1402
	movl	%r12d, %ecx
	testl	%r12d, %r12d
	js	.L1419
	jmp	.L1436
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	%r8, %rsi
	leaq	.LC98(%rip), %rcx
	movq	%r15, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, -200(%rbp)
	xorl	%r12d, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-200(%rbp), %r8
	jmp	.L1415
.L1398:
	leaq	.LC96(%rip), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r12, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1400
.L1477:
	xorl	%eax, %eax
	leaq	.LC96(%rip), %rcx
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	testb	%bl, %bl
	jne	.L1401
	jmp	.L1400
.L1468:
	call	__stack_chk_fail@PLT
.L1473:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1476:
	movabsq	$768614336404564650, %rax
	cmpq	%rax, %rdi
	movq	%rax, %r9
	cmovbe	%rdi, %r9
	imulq	$12, %r9, %r12
	jmp	.L1422
	.cfi_endproc
.LFE19199:
	.size	_ZN2v88internal4wasm16DecodeLocalNamesEPKhS3_PNS1_10LocalNamesE, .-_ZN2v88internal4wasm16DecodeLocalNamesEPKhS3_PNS1_10LocalNamesE
	.section	.text._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.1,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.1, @function
_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.1:
.LFB25950:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$368, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 56(%rdi)
	je	.L1489
.L1479:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1490
	addq	$368, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1489:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	%esi, %r13d
	leaq	-304(%rbp), %rdi
	movl	$256, %esi
	movq	%rdi, -320(%rbp)
	movq	$256, -312(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	testl	%eax, %eax
	jle	.L1491
	movq	-320(%rbp), %rsi
	cltq
	leaq	-400(%rbp), %rdi
	leaq	-384(%rbp), %r12
	movq	%r12, -400(%rbp)
	leaq	-344(%rbp), %r14
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-400(%rbp), %rax
	movl	%r13d, -368(%rbp)
	movq	%r14, -360(%rbp)
	cmpq	%r12, %rax
	je	.L1492
	movq	%rax, -360(%rbp)
	movq	-384(%rbp), %rax
	movq	%rax, -344(%rbp)
.L1484:
	movl	%r13d, 40(%rbx)
	movq	-392(%rbp), %rax
	leaq	48(%rbx), %rdi
	leaq	-360(%rbp), %rsi
	movq	%r12, -400(%rbp)
	movq	%rax, -352(%rbp)
	movq	$0, -392(%rbp)
	movb	$0, -384(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_@PLT
	movq	-360(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1485
	call	_ZdlPv@PLT
.L1485:
	movq	-400(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1486
	call	_ZdlPv@PLT
.L1486:
	movq	24(%rbx), %rax
	movq	%rax, 16(%rbx)
	jmp	.L1479
	.p2align 4,,10
	.p2align 3
.L1492:
	movdqa	-384(%rbp), %xmm0
	movups	%xmm0, -344(%rbp)
	jmp	.L1484
	.p2align 4,,10
	.p2align 3
.L1491:
	leaq	.LC37(%rip), %rsi
	leaq	.LC33(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1490:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25950:
	.size	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.1, .-_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.1
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0, @function
_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0:
.LFB25955:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L1494
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L1494:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	subq	8(%rdi), %rsi
	leaq	-208(%rbp), %rcx
	movq	%rax, -200(%rbp)
	addl	32(%rdi), %esi
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$24, -208(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.constprop.1
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1497
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1497:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25955:
	.size	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0, .-_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	.section	.rodata._ZN2v88internal4wasm29DecodeWasmSignatureForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneEPKhS8_.str1.1,"aMS",@progbits,1
.LC100:
	.string	"end is less than start"
	.section	.text._ZN2v88internal4wasm29DecodeWasmSignatureForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneEPKhS8_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm29DecodeWasmSignatureForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneEPKhS8_
	.type	_ZN2v88internal4wasm29DecodeWasmSignatureForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneEPKhS8_, @function
_ZN2v88internal4wasm29DecodeWasmSignatureForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneEPKhS8_:
.LFB19131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-88(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	16+_ZTVN2v88internal4wasm17ModuleDecoderImplE(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-240(%rbp), %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-176(%rbp), %rax
	movq	%rdx, -232(%rbp)
	movq	%rax, -336(%rbp)
	movq	%rax, -192(%rbp)
	movq	(%rdi), %rax
	movq	%rdx, -224(%rbp)
	movq	%rax, -160(%rbp)
	movl	8(%rdi), %eax
	movq	%rcx, -216(%rbp)
	movl	%eax, -152(%rbp)
	movzbl	12(%rdi), %eax
	movl	$0, -208(%rbp)
	movb	%al, -148(%rbp)
	movl	$0, -200(%rbp)
	movq	$0, -184(%rbp)
	movb	$0, -176(%rbp)
	movq	%r13, -240(%rbp)
	movq	$0, -128(%rbp)
	movb	$1, -120(%rbp)
	movq	$0, -116(%rbp)
	movq	%r15, -104(%rbp)
	movq	$0, -96(%rbp)
	movaps	%xmm0, -144(%rbp)
	movb	$0, -88(%rbp)
	movzbl	_ZN2v88internal24FLAG_assume_asmjs_originE(%rip), %eax
	movb	%al, -72(%rbp)
	cmpq	%rcx, %rdx
	ja	.L1607
	movq	%r14, -224(%rbp)
	cmpl	%r14d, %ecx
	je	.L1500
.L1617:
	movzbl	(%r14), %r9d
	leaq	1(%r14), %r10
	movq	%r10, -224(%rbp)
	cmpb	$96, %r9b
	jne	.L1502
	movl	$0, -272(%rbp)
	cmpq	%rcx, %r10
	jnb	.L1504
	movzbl	1(%r14), %eax
	movq	%r10, -328(%rbp)
	leaq	2(%r14), %rsi
	movl	%eax, %ebx
	andl	$127, %ebx
	testb	%al, %al
	js	.L1608
	movq	%rsi, -224(%rbp)
.L1506:
	cmpq	$0, -184(%rbp)
	jne	.L1507
	movq	$0, -288(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -304(%rbp)
	testl	%ebx, %ebx
	je	.L1509
.L1508:
	xorl	%r14d, %r14d
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1511:
	movl	$-128, %eax
	subl	%edx, %eax
.L1514:
	movb	%al, -272(%rbp)
	movq	-296(%rbp), %rsi
	cmpq	-288(%rbp), %rsi
	je	.L1519
	movb	%al, (%rsi)
	addl	$1, %r14d
	addq	$1, -296(%rbp)
	cmpq	$0, -184(%rbp)
	jne	.L1509
.L1605:
	cmpl	%ebx, %r14d
	jnb	.L1509
.L1522:
	movq	-224(%rbp), %rsi
	cmpl	%esi, -216(%rbp)
	je	.L1510
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %rax
	movq	%rax, -224(%rbp)
	leal	-124(%rdx), %eax
	cmpb	$3, %al
	jbe	.L1511
	cmpb	$0, -72(%rbp)
	jne	.L1513
	cmpb	$112, %dl
	je	.L1515
	ja	.L1516
	cmpb	$104, %dl
	je	.L1517
	cmpb	$111, %dl
	jne	.L1513
	cmpb	$0, -153(%rbp)
	movl	$6, %eax
	jne	.L1514
	.p2align 4,,10
	.p2align 3
.L1513:
	leaq	.LC53(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1514
	.p2align 4,,10
	.p2align 3
.L1504:
	xorl	%eax, %eax
	leaq	.LC62(%rip), %rcx
	movq	%r10, %rsi
	movq	%r12, %rdi
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	cmpq	$0, -184(%rbp)
	je	.L1609
.L1507:
	xorl	%r12d, %r12d
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1516:
	cmpb	$123, %dl
	jne	.L1513
	cmpb	$0, -157(%rbp)
	movl	$5, %eax
	je	.L1513
	jmp	.L1514
	.p2align 4,,10
	.p2align 3
.L1519:
	leaq	-272(%rbp), %rdx
	leaq	-304(%rbp), %rdi
	addl	$1, %r14d
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	cmpq	$0, -184(%rbp)
	je	.L1605
	.p2align 4,,10
	.p2align 3
.L1509:
	cmpb	$1, -160(%rbp)
	pxor	%xmm0, %xmm0
	movq	$0, -256(%rbp)
	sbbq	%r9, %r9
	movq	-224(%rbp), %r10
	movl	$0, -308(%rbp)
	andq	$-999, %r9
	movaps	%xmm0, -272(%rbp)
	addq	$1000, %r9
	cmpq	-216(%rbp), %r10
	jb	.L1610
	leaq	.LC63(%rip), %rcx
	movq	%r10, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-184(%rbp), %rcx
	movl	%ebx, %esi
	testq	%rcx, %rcx
	jne	.L1527
.L1531:
	movq	-344(%rbp), %rax
	addq	$7, %rsi
	andq	$-8, %rsi
	movq	16(%rax), %rdx
	movq	24(%rax), %rax
	movq	%rax, -328(%rbp)
	subq	%rdx, %rax
	cmpq	%rax, %rsi
	ja	.L1611
	movq	-344(%rbp), %rax
	addq	%rdx, %rsi
	movq	%rsi, 16(%rax)
.L1545:
	leal	-1(%r14), %edi
	xorl	%eax, %eax
	testl	%r14d, %r14d
	je	.L1550
	.p2align 4,,10
	.p2align 3
.L1551:
	movq	-272(%rbp), %rsi
	movzbl	(%rsi,%rax), %esi
	movb	%sil, (%rdx,%rax)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%rdi, %rsi
	jne	.L1551
.L1550:
	leal	-1(%rbx), %r9d
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.L1552
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	-304(%rbp), %rsi
	movzbl	(%rsi,%rax), %edi
	leal	(%r14,%rax), %esi
	movb	%dil, (%rdx,%rsi)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpq	%r9, %rsi
	jne	.L1553
.L1552:
	movq	-344(%rbp), %rax
	movq	16(%rax), %r12
	movq	24(%rax), %rax
	movq	%rax, -328(%rbp)
	subq	%r12, %rax
	cmpq	$23, %rax
	jbe	.L1612
	movq	-344(%rbp), %rdi
	leaq	24(%r12), %rax
	movq	%rax, 16(%rdi)
.L1554:
	movl	%ebx, %eax
	movq	%rcx, (%r12)
	movq	%rax, 8(%r12)
	movq	%rdx, 16(%r12)
	jmp	.L1530
	.p2align 4,,10
	.p2align 3
.L1610:
	movzbl	(%r10), %eax
	leaq	1(%r10), %rsi
	movl	%eax, %r14d
	andl	$127, %r14d
	testb	%al, %al
	js	.L1613
	movl	%r14d, %eax
	movq	%rsi, -224(%rbp)
	cmpq	%rax, %r9
	jb	.L1614
.L1526:
	movq	-184(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L1529
.L1527:
	xorl	%r12d, %r12d
.L1530:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1555
	call	_ZdlPv@PLT
.L1555:
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1606
	call	_ZdlPv@PLT
.L1606:
	movq	-184(%rbp), %rax
.L1503:
	testq	%rax, %rax
	movl	$0, %eax
	cmovne	%rax, %r12
.L1557:
	movq	-104(%rbp), %rdi
	movq	%r13, -240(%rbp)
	cmpq	%r15, %rdi
	je	.L1558
	call	_ZdlPv@PLT
.L1558:
	movq	-136(%rbp), %r13
	testq	%r13, %r13
	je	.L1560
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1561
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1615
	.p2align 4,,10
	.p2align 3
.L1560:
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-192(%rbp), %rdi
	movq	%rax, -240(%rbp)
	cmpq	-336(%rbp), %rdi
	je	.L1498
	call	_ZdlPv@PLT
.L1498:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1616
	addq	$312, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1510:
	.cfi_restore_state
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-216(%rbp), %rsi
	movq	%rsi, -224(%rbp)
	subq	$1, %rsi
	jmp	.L1513
	.p2align 4,,10
	.p2align 3
.L1607:
	leaq	.LC100(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-232(%rbp), %rcx
	movq	%r14, -224(%rbp)
	movq	%rcx, -216(%rbp)
	cmpl	%r14d, %ecx
	jne	.L1617
.L1500:
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-216(%rbp), %rax
	xorl	%r9d, %r9d
	movq	%rax, -224(%rbp)
.L1502:
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	$96, %r8d
	movq	%r14, %rsi
	leaq	.LC60(%rip), %rcx
	leaq	.LC61(%rip), %rdx
	xorl	%r12d, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-184(%rbp), %rax
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1517:
	cmpb	$0, -159(%rbp)
	movl	$9, %eax
	je	.L1513
	jmp	.L1514
	.p2align 4,,10
	.p2align 3
.L1515:
	cmpb	$0, -153(%rbp)
	movl	$7, %eax
	je	.L1513
	jmp	.L1514
	.p2align 4,,10
	.p2align 3
.L1561:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1560
.L1615:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1564
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1565:
	cmpl	$1, %eax
	jne	.L1560
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1560
	.p2align 4,,10
	.p2align 3
.L1529:
	movl	%ebx, %esi
	testl	%r14d, %r14d
	je	.L1531
.L1528:
	movl	$0, -328(%rbp)
	jmp	.L1532
	.p2align 4,,10
	.p2align 3
.L1534:
	movl	$-128, %eax
	subl	%edx, %eax
.L1537:
	movb	%al, -308(%rbp)
	movq	-264(%rbp), %rsi
	cmpq	-256(%rbp), %rsi
	je	.L1542
	movb	%al, (%rsi)
	addq	$1, -264(%rbp)
.L1543:
	addl	$1, -328(%rbp)
	cmpq	$0, -184(%rbp)
	jne	.L1527
	cmpl	%r14d, -328(%rbp)
	je	.L1618
.L1532:
	movq	-224(%rbp), %rsi
	cmpl	%esi, -216(%rbp)
	je	.L1533
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %rax
	movq	%rax, -224(%rbp)
	leal	-124(%rdx), %eax
	cmpb	$3, %al
	jbe	.L1534
	cmpb	$0, -72(%rbp)
	jne	.L1536
	cmpb	$112, %dl
	je	.L1538
	ja	.L1539
	cmpb	$104, %dl
	je	.L1540
	cmpb	$111, %dl
	jne	.L1536
	cmpb	$0, -153(%rbp)
	movl	$6, %eax
	jne	.L1537
	.p2align 4,,10
	.p2align 3
.L1536:
	leaq	.LC53(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1613:
	movl	%r14d, %r8d
	leaq	-308(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r10, -352(%rbp)
	leaq	.LC63(%rip), %rcx
	movq	%r9, -328(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_
	movq	-328(%rbp), %r9
	movq	-352(%rbp), %r10
	movl	%eax, %r14d
	movl	%r14d, %eax
	cmpq	%rax, %r9
	jnb	.L1526
.L1614:
	movl	%r14d, %r8d
	xorl	%eax, %eax
	leaq	.LC63(%rip), %rcx
	movq	%r10, %rsi
	leaq	.LC59(%rip), %rdx
	movq	%r12, %rdi
	movq	%r9, -328(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-328(%rbp), %r9
	cmpq	$0, -184(%rbp)
	movl	%r9d, %r14d
	je	.L1528
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1609:
	pxor	%xmm0, %xmm0
	xorl	%ebx, %ebx
	movq	$0, -288(%rbp)
	movaps	%xmm0, -304(%rbp)
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L1539:
	cmpb	$123, %dl
	jne	.L1536
	cmpb	$0, -157(%rbp)
	movl	$5, %eax
	je	.L1536
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1542:
	leaq	-308(%rbp), %rdx
	leaq	-272(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1543
	.p2align 4,,10
	.p2align 3
.L1533:
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-216(%rbp), %rsi
	movq	%rsi, -224(%rbp)
	subq	$1, %rsi
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L1540:
	cmpb	$0, -159(%rbp)
	movl	$9, %eax
	je	.L1536
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1538:
	cmpb	$0, -153(%rbp)
	movl	$7, %eax
	je	.L1536
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1608:
	movl	%ebx, %r8d
	leaq	-272(%rbp), %rdx
	leaq	.LC62(%rip), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi1EEET_PKhPjPKcS7_
	movl	%eax, %ebx
	cmpl	$1000, %eax
	jbe	.L1506
	movq	-328(%rbp), %r10
	movl	%eax, %r8d
	xorl	%eax, %eax
	movl	$1000, %r9d
	leaq	.LC62(%rip), %rcx
	leaq	.LC59(%rip), %rdx
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	cmpq	$0, -184(%rbp)
	jne	.L1507
	pxor	%xmm0, %xmm0
	movl	$1000, %ebx
	movq	$0, -288(%rbp)
	movaps	%xmm0, -304(%rbp)
	jmp	.L1508
	.p2align 4,,10
	.p2align 3
.L1618:
	leal	(%r14,%rbx), %esi
	movl	%r14d, %ecx
	jmp	.L1531
	.p2align 4,,10
	.p2align 3
.L1564:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1611:
	movq	-344(%rbp), %rdi
	movq	%rcx, -328(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-328(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1612:
	movq	-344(%rbp), %rdi
	movl	$24, %esi
	movq	%rcx, -352(%rbp)
	movq	%rdx, -328(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-328(%rbp), %rdx
	movq	-352(%rbp), %rcx
	movq	%rax, %r12
	jmp	.L1554
.L1616:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19131:
	.size	_ZN2v88internal4wasm29DecodeWasmSignatureForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneEPKhS8_, .-_ZN2v88internal4wasm29DecodeWasmSignatureForTestingERKNS1_12WasmFeaturesEPNS0_4ZoneEPKhS8_
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag:
.LFB23927:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1620
	testq	%rsi, %rsi
	je	.L1636
.L1620:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L1637
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L1623
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L1624:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1638
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1623:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1624
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1622:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L1624
.L1636:
	leaq	.LC36(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1638:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23927:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl14FinishDecodingEb.str1.8,"aMS",@progbits,1
	.align 8
.LC101:
	.string	"function count is %u, but code section is absent"
	.align 8
.LC102:
	.string	"data segments count %u mismatch (%u expected)"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl14FinishDecodingEb,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl14FinishDecodingEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl14FinishDecodingEb
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl14FinishDecodingEb, @function
_ZN2v88internal4wasm17ModuleDecoderImpl14FinishDecodingEb:
.LFB19041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$216, %rsp
	movl	%edx, -252(%rbp)
	movq	56(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1687
.L1640:
	movq	48(%rbx), %rsi
	movl	40(%rbx), %eax
	leaq	-216(%rbp), %r15
	leaq	-232(%rbp), %rdi
	movq	%r15, -232(%rbp)
	addq	%rsi, %rdx
	movl	%eax, -240(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movl	-240(%rbp), %edx
	movq	-232(%rbp), %rax
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -112(%rbp)
	movl	%edx, -96(%rbp)
	cmpq	%r15, %rax
	je	.L1688
	movq	-216(%rbp), %rcx
	leaq	-136(%rbp), %r14
	leaq	-72(%rbp), %rdi
	movq	-224(%rbp), %rsi
	movq	%r15, -232(%rbp)
	movq	%rcx, -72(%rbp)
	movq	$0, -224(%rbp)
	movb	$0, -216(%rbp)
	movl	%edx, -160(%rbp)
	movq	%r14, -152(%rbp)
	movq	%rdi, -248(%rbp)
	movaps	%xmm0, -176(%rbp)
	cmpq	%rdi, %rax
	je	.L1646
	movq	%rax, -152(%rbp)
	movq	%rcx, -136(%rbp)
.L1648:
	movq	-248(%rbp), %rax
	leaq	-112(%rbp), %r13
	movb	$0, -72(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -144(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -80(%rbp)
	call	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED1Ev
	movq	-232(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1650
	call	_ZdlPv@PLT
.L1650:
	movdqa	-176(%rbp), %xmm1
	movl	-160(%rbp), %esi
	pxor	%xmm0, %xmm0
	leaq	-200(%rbp), %r15
	movq	-152(%rbp), %rax
	movq	-176(%rbp), %rcx
	movq	%r15, -216(%rbp)
	movq	-168(%rbp), %rdi
	movl	%esi, -224(%rbp)
	movaps	%xmm1, -240(%rbp)
	movaps	%xmm0, -176(%rbp)
	cmpq	%r14, %rax
	je	.L1689
	movq	-136(%rbp), %rdx
	movq	%rax, -216(%rbp)
	movq	%rdx, -200(%rbp)
.L1652:
	movq	-144(%rbp), %rdx
	movq	%rdx, -208(%rbp)
	testq	%rdx, %rdx
	jne	.L1653
	cmpb	$0, -252(%rbp)
	jne	.L1690
.L1653:
	movq	%rcx, %xmm0
	movq	%rdi, %xmm2
	leaq	40(%r12), %rcx
	movl	%esi, 16(%r12)
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, 24(%r12)
	movups	%xmm0, (%r12)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -240(%rbp)
	cmpq	%r15, %rax
	je	.L1691
	movq	%rax, 24(%r12)
	movq	-200(%rbp), %rax
	movq	%rax, 40(%r12)
.L1666:
	movq	%rdx, 32(%r12)
.L1639:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1692
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1687:
	.cfi_restore_state
	movq	96(%rsi), %rsi
	movl	68(%rsi), %ecx
	testl	%ecx, %ecx
	je	.L1641
	movl	60(%rsi), %eax
	salq	$5, %rax
	addq	136(%rsi), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	je	.L1693
.L1641:
	testb	$16, 125(%rbx)
	je	.L1643
	movq	168(%rsi), %rcx
	movl	76(%rsi), %r8d
	subq	160(%rsi), %rcx
	sarq	$5, %rcx
	cmpl	%r8d, %ecx
	jne	.L1694
.L1643:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl22CalculateGlobalOffsetsEPNS1_10WasmModuleE
.L1642:
	movq	56(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L1640
	pxor	%xmm0, %xmm0
	movb	$0, -72(%rbp)
	leaq	-112(%rbp), %r13
	leaq	-72(%rbp), %rax
	movdqu	96(%rbx), %xmm3
	movdqu	-72(%rbp), %xmm4
	movups	%xmm0, 96(%rbx)
	movq	%r13, %rdi
	leaq	-136(%rbp), %r14
	movl	$0, -96(%rbp)
	movq	%rax, -248(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -80(%rbp)
	movl	$0, -160(%rbp)
	movq	%r14, -152(%rbp)
	movq	$0, -144(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm0, -112(%rbp)
	movups	%xmm4, -136(%rbp)
	call	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED1Ev
	jmp	.L1650
	.p2align 4,,10
	.p2align 3
.L1689:
	movdqu	-136(%rbp), %xmm5
	movq	%r15, %rax
	movups	%xmm5, -200(%rbp)
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1691:
	movdqu	-200(%rbp), %xmm6
	movups	%xmm6, 40(%r12)
	jmp	.L1666
	.p2align 4,,10
	.p2align 3
.L1690:
	movq	144(%rbx), %r9
	testq	%r9, %r9
	je	.L1653
	movl	128(%rbx), %edx
	movq	136(%rbx), %rax
	leaq	152(%rbx), %rsi
	movl	%edx, -176(%rbp)
	cmpq	%rsi, %rax
	je	.L1695
	movq	152(%rbx), %rcx
	movq	%rsi, 136(%rbx)
	pxor	%xmm0, %xmm0
	movq	$0, 144(%rbx)
	movb	$0, 152(%rbx)
	leaq	-152(%rbp), %rbx
	movq	%rcx, -152(%rbp)
	movl	%edx, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	cmpq	%rbx, %rax
	je	.L1658
	movl	%edx, 16(%r12)
	leaq	40(%r12), %rdx
	movq	%rcx, -72(%rbp)
	movq	%rbx, -168(%rbp)
	movq	$0, -160(%rbp)
	movb	$0, -152(%rbp)
	movq	%rdx, 24(%r12)
	movups	%xmm0, (%r12)
	cmpq	-248(%rbp), %rax
	je	.L1660
	movq	%rax, 24(%r12)
	movq	%rcx, 40(%r12)
.L1662:
	movq	%r9, 32(%r12)
	movq	-248(%rbp), %rax
	movq	%r13, %rdi
	movq	$0, -80(%rbp)
	movq	%rax, -88(%rbp)
	movb	$0, -72(%rbp)
	call	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED1Ev
	movq	-168(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1663
	call	_ZdlPv@PLT
.L1663:
	movq	-216(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1686
	call	_ZdlPv@PLT
.L1686:
	movq	-232(%rbp), %r13
	testq	%r13, %r13
	je	.L1639
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1670
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1671:
	cmpl	$1, %eax
	jne	.L1639
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1672
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1673:
	cmpl	$1, %eax
	jne	.L1639
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1688:
	leaq	-136(%rbp), %r14
	leaq	-72(%rbp), %rax
	movdqu	-216(%rbp), %xmm7
	movq	-224(%rbp), %rsi
	movb	$0, -216(%rbp)
	movq	$0, -224(%rbp)
	movl	%edx, -160(%rbp)
	movq	%r14, -152(%rbp)
	movq	%rax, -248(%rbp)
	movups	%xmm7, -72(%rbp)
	movaps	%xmm0, -176(%rbp)
.L1646:
	movdqu	-72(%rbp), %xmm7
	movups	%xmm7, -136(%rbp)
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1670:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L1671
	.p2align 4,,10
	.p2align 3
.L1672:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1673
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	16(%rbx), %rsi
	leaq	.LC102(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1693:
	movq	16(%rbx), %rsi
	leaq	.LC101(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1695:
	movdqu	152(%rbx), %xmm6
	movl	%edx, -96(%rbp)
	pxor	%xmm0, %xmm0
	movq	$0, 144(%rbx)
	movb	$0, 152(%rbx)
	leaq	-152(%rbp), %rbx
	movups	%xmm6, -152(%rbp)
	movaps	%xmm0, -112(%rbp)
.L1658:
	leaq	40(%r12), %rax
	movl	%edx, 16(%r12)
	movdqu	-152(%rbp), %xmm6
	pxor	%xmm0, %xmm0
	movq	%rbx, -168(%rbp)
	movq	$0, -160(%rbp)
	movb	$0, -152(%rbp)
	movq	%rax, 24(%r12)
	movups	%xmm6, -72(%rbp)
	movups	%xmm0, (%r12)
.L1660:
	movdqu	-72(%rbp), %xmm5
	movups	%xmm5, 40(%r12)
	jmp	.L1662
.L1692:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19041:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl14FinishDecodingEb, .-_ZN2v88internal4wasm17ModuleDecoderImpl14FinishDecodingEb
	.section	.text._ZN2v88internal4wasm13ModuleDecoder14FinishDecodingEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm13ModuleDecoder14FinishDecodingEb
	.type	_ZN2v88internal4wasm13ModuleDecoder14FinishDecodingEb, @function
_ZN2v88internal4wasm13ModuleDecoder14FinishDecodingEb:
.LFB19126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%dl, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	16(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm17ModuleDecoderImpl14FinishDecodingEb
	movl	-64(%rbp), %eax
	pxor	%xmm0, %xmm0
	leaq	-40(%rbp), %rdx
	movdqa	-80(%rbp), %xmm1
	movaps	%xmm0, -80(%rbp)
	movl	%eax, 16(%r12)
	leaq	40(%r12), %rax
	movq	%rax, 24(%r12)
	movq	-56(%rbp), %rax
	movups	%xmm1, (%r12)
	cmpq	%rdx, %rax
	je	.L1701
	movq	%rax, 24(%r12)
	movq	-40(%rbp), %rax
	movq	%rax, 40(%r12)
.L1698:
	movq	-48(%rbp), %rax
	movq	%rax, 32(%r12)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1702
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1701:
	.cfi_restore_state
	movdqu	-40(%rbp), %xmm2
	movups	%xmm2, 40(%r12)
	jmp	.L1698
.L1702:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19126:
	.size	_ZN2v88internal4wasm13ModuleDecoder14FinishDecodingEb, .-_ZN2v88internal4wasm13ModuleDecoder14FinishDecodingEb
	.section	.rodata._ZN2v88internal4wasm18DecodeAsmJsOffsetsEPKhS3_.str1.1,"aMS",@progbits,1
.LC103:
	.string	"functions count"
.LC104:
	.string	"table size"
	.section	.rodata._ZN2v88internal4wasm18DecodeAsmJsOffsetsEPKhS3_.str1.8,"aMS",@progbits,1
	.align 8
.LC105:
	.string	"illegal asm function offset table size"
	.section	.rodata._ZN2v88internal4wasm18DecodeAsmJsOffsetsEPKhS3_.str1.1
.LC106:
	.string	"locals size"
.LC107:
	.string	"function start pos"
.LC108:
	.string	"byte offset delta"
.LC109:
	.string	"call position delta"
.LC110:
	.string	"to_number position delta"
.LC111:
	.string	"broken asm offset table"
.LC112:
	.string	"unexpected additional bytes"
	.section	.text._ZN2v88internal4wasm18DecodeAsmJsOffsetsEPKhS3_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm18DecodeAsmJsOffsetsEPKhS3_
	.type	_ZN2v88internal4wasm18DecodeAsmJsOffsetsEPKhS3_, @function
_ZN2v88internal4wasm18DecodeAsmJsOffsetsEPKhS3_:
.LFB19137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	leaq	.LC103(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-144(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	%r13, %rbx
	subq	$424, %rsp
	movq	%rdi, -432(%rbp)
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -448(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-352(%rbp), %rax
	movq	%rax, %rdx
	movq	%rsi, -136(%rbp)
	movq	$0, -368(%rbp)
	movq	%rsi, -128(%rbp)
	movl	$0, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	movl	$0, -352(%rbp)
	movq	%rax, -424(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.1
	movl	%eax, -416(%rbp)
	cmpl	%ebx, %eax
	jb	.L1906
.L1704:
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	je	.L1712
	movl	$0, -412(%rbp)
	jmp	.L1732
	.p2align 4,,10
	.p2align 3
.L1714:
	leaq	.LC104(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
.L1725:
	movq	-376(%rbp), %rsi
	cmpq	-368(%rbp), %rsi
	je	.L1730
.L1928:
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rsi)
	movups	%xmm0, (%rsi)
	addq	$24, -376(%rbp)
.L1731:
	addl	$1, -412(%rbp)
	movl	-412(%rbp), %eax
	cmpl	%eax, -416(%rbp)
	je	.L1712
.L1732:
	movq	-88(%rbp), %rax
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	testq	%rax, %rax
	jne	.L1713
	cmpq	%rdx, %rsi
	jnb	.L1714
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %r15
	movl	%eax, %r9d
	andl	$127, %r9d
	testb	%al, %al
	js	.L1907
.L1715:
	movq	%r15, -128(%rbp)
.L1844:
	testl	%r9d, %r9d
	je	.L1725
	movq	%rdx, %rax
	subq	%r15, %rax
	cmpl	%eax, %r9d
	ja	.L1908
.L1733:
	movl	%r9d, %r13d
	leaq	(%r15,%r13), %rax
	movq	%rax, -408(%rbp)
	cmpq	%rdx, %r15
	jb	.L1909
	leaq	.LC40(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC106(%rip), %rcx
	movl	%r9d, -440(%rbp)
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movl	-440(%rbp), %r9d
.L1746:
	cmpq	%rdx, %rsi
	jb	.L1910
	leaq	.LC107(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	leaq	.LC40(%rip), %rdx
	movl	%r9d, -440(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movl	-440(%rbp), %r9d
.L1759:
	pxor	%xmm0, %xmm0
	shrl	$2, %r9d
	movq	$0, -336(%rbp)
	movaps	%xmm0, -352(%rbp)
	jne	.L1911
	movq	-424(%rbp), %rdi
	xorl	%esi, %esi
	leaq	-396(%rbp), %rdx
	movl	$0, -396(%rbp)
	movl	%r14d, -392(%rbp)
	movl	%r14d, -388(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.p2align 4,,10
	.p2align 3
.L1851:
	cmpq	$0, -88(%rbp)
	movq	-128(%rbp), %rsi
	jne	.L1764
	.p2align 4,,10
	.p2align 3
.L1809:
	cmpq	%rsi, -408(%rbp)
	jbe	.L1764
	movq	-120(%rbp), %rax
	cmpq	%rax, %rsi
	jb	.L1912
	xorl	%eax, %eax
	leaq	.LC108(%rip), %rcx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rax
.L1780:
	addl	%ebx, %r15d
	movl	%r15d, %ebx
	cmpq	%rax, %r8
	jb	.L1913
	movq	%r8, %rsi
	xorl	%eax, %eax
	leaq	.LC109(%rip), %rcx
	movq	%r12, %rdi
	leaq	.LC40(%rip), %rdx
	movl	%r14d, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
.L1793:
	cmpq	%rsi, %rax
	ja	.L1914
	leaq	.LC110(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	%r13d, %r14d
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
.L1806:
	movl	%r15d, -396(%rbp)
	movq	-344(%rbp), %rsi
	movl	%r13d, -392(%rbp)
	movl	%r14d, -388(%rbp)
	cmpq	-336(%rbp), %rsi
	je	.L1807
	movq	-396(%rbp), %rax
	movq	%rax, (%rsi)
	movl	-388(%rbp), %eax
	movl	%eax, 8(%rsi)
	movq	-128(%rbp), %rsi
	addq	$12, -344(%rbp)
	cmpq	$0, -88(%rbp)
	je	.L1809
.L1764:
	cmpq	%rsi, -408(%rbp)
	je	.L1767
	leaq	.LC111(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
.L1767:
	movq	-376(%rbp), %rbx
	cmpq	-368(%rbp), %rbx
	je	.L1810
	movq	-352(%rbp), %rax
	movq	%rax, (%rbx)
	movq	-344(%rbp), %rax
	movq	%rax, 8(%rbx)
	movq	-336(%rbp), %rax
	addq	$24, -376(%rbp)
	movq	%rax, 16(%rbx)
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1914:
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %r8
	movl	%edx, %r14d
	andl	$127, %r14d
	testb	%dl, %dl
	js	.L1915
	sall	$25, %r14d
	movq	%r8, -128(%rbp)
	sarl	$25, %r14d
	addl	%r13d, %r14d
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1913:
	movzbl	(%r8), %edx
	leaq	1(%r8), %rsi
	movl	%edx, %r13d
	andl	$127, %r13d
	testb	%dl, %dl
	js	.L1916
	sall	$25, %r13d
	movq	%rsi, -128(%rbp)
	sarl	$25, %r13d
	addl	%r14d, %r13d
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1912:
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %r8
	movl	%edx, %r15d
	andl	$127, %r15d
	testb	%dl, %dl
	jns	.L1769
	cmpq	%r8, %rax
	jbe	.L1770
	movzbl	1(%rsi), %ecx
	leaq	2(%rsi), %r8
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r15d
	testb	%cl, %cl
	js	.L1917
.L1769:
	movq	%r8, -128(%rbp)
	jmp	.L1780
	.p2align 4,,10
	.p2align 3
.L1915:
	cmpq	%rax, %r8
	jnb	.L1796
	movzbl	1(%rsi), %ecx
	movzbl	%r14b, %r14d
	leaq	2(%rsi), %r8
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r14d
	testb	%cl, %cl
	js	.L1918
	movq	%r8, -128(%rbp)
	sall	$18, %r14d
	sarl	$18, %r14d
.L1905:
	addl	%r13d, %r14d
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1916:
	cmpq	%rsi, %rax
	jbe	.L1783
	movzbl	1(%r8), %ecx
	movzbl	%r13b, %r13d
	leaq	2(%r8), %rsi
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r13d
	testb	%cl, %cl
	js	.L1919
	sall	$18, %r13d
	movq	%rsi, -128(%rbp)
	sarl	$18, %r13d
	addl	%r14d, %r13d
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1807:
	movq	-424(%rbp), %rdi
	leaq	-396(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	cmpq	$0, -88(%rbp)
	movq	-128(%rbp), %rsi
	je	.L1809
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1917:
	cmpq	%r8, %rax
	jbe	.L1770
	movzbl	2(%rsi), %ecx
	leaq	3(%rsi), %r8
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r15d
	testb	%cl, %cl
	jns	.L1769
	cmpq	%r8, %rax
	ja	.L1920
	.p2align 4,,10
	.p2align 3
.L1770:
	movq	%r8, %rsi
	xorl	%eax, %eax
	leaq	.LC108(%rip), %rcx
	movq	%r12, %rdi
	leaq	.LC40(%rip), %rdx
	movq	%r8, -128(%rbp)
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rax
	jmp	.L1780
	.p2align 4,,10
	.p2align 3
.L1924:
	cmpq	%rax, %r8
	jb	.L1921
	.p2align 4,,10
	.p2align 3
.L1796:
	leaq	.LC110(%rip), %rcx
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, -128(%rbp)
	movl	%r13d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1923:
	cmpq	%rax, %rsi
	jb	.L1922
	.p2align 4,,10
	.p2align 3
.L1783:
	xorl	%eax, %eax
	leaq	.LC109(%rip), %rcx
	movq	%r12, %rdi
	movq	%rsi, -128(%rbp)
	leaq	.LC40(%rip), %rdx
	movl	%r14d, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1919:
	cmpq	%rax, %rsi
	jnb	.L1783
	movzbl	2(%r8), %ecx
	leaq	3(%r8), %rsi
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r13d
	testb	%cl, %cl
	js	.L1923
	sall	$11, %r13d
	movq	%rsi, -128(%rbp)
	sarl	$11, %r13d
	addl	%r14d, %r13d
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1918:
	cmpq	%rax, %r8
	jnb	.L1796
	movzbl	2(%rsi), %ecx
	leaq	3(%rsi), %r8
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r14d
	testb	%cl, %cl
	js	.L1924
	sall	$11, %r14d
	movq	%r8, -128(%rbp)
	sarl	$11, %r14d
	addl	%r13d, %r14d
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L1910:
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %r10
	movl	%eax, %r8d
	andl	$127, %r8d
	testb	%al, %al
	js	.L1925
.L1748:
	movq	%r10, -128(%rbp)
.L1904:
	movl	%r8d, %r14d
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1909:
	movzbl	(%r15), %eax
	leaq	1(%r15), %rsi
	movl	%eax, %ebx
	andl	$127, %ebx
	testb	%al, %al
	js	.L1926
.L1735:
	movq	%rsi, -128(%rbp)
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L1907:
	cmpq	%r15, %rdx
	jbe	.L1722
	movzbl	1(%rsi), %ecx
	leaq	2(%rsi), %r15
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r9d
	testb	%cl, %cl
	jns	.L1715
	cmpq	%rdx, %r15
	jnb	.L1722
	movzbl	2(%rsi), %ecx
	leaq	3(%rsi), %r15
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r9d
	testb	%cl, %cl
	jns	.L1715
	cmpq	%rdx, %r15
	jnb	.L1722
	movzbl	3(%rsi), %ecx
	leaq	4(%rsi), %r15
	movl	%ecx, %eax
	sall	$21, %eax
	andl	$266338304, %eax
	orl	%eax, %r9d
	testb	%cl, %cl
	jns	.L1715
	cmpq	%rdx, %r15
	jnb	.L1722
	movzbl	4(%rsi), %eax
	addq	$5, %rsi
	movq	%rsi, -128(%rbp)
	movl	%eax, %ebx
	andl	$-16, %ebx
	testb	%al, %al
	js	.L1927
	sall	$28, %eax
	orl	%eax, %r9d
	testb	%bl, %bl
	jne	.L1726
	movq	%rsi, %r15
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1722:
	movq	%r15, %rsi
	leaq	.LC104(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r15, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-376(%rbp), %rsi
	cmpq	-368(%rbp), %rsi
	jne	.L1928
.L1730:
	leaq	-384(%rbp), %rdi
	call	_ZNSt6vectorIS_IN2v88internal4wasm16AsmJsOffsetEntryESaIS3_EESaIS5_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1911:
	movl	%r9d, %r9d
	leaq	(%r9,%r9,2), %rax
	leaq	0(,%rax,4), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -440(%rbp)
	call	_Znwm@PLT
	movq	-352(%rbp), %r9
	movq	-344(%rbp), %rdx
	movq	-440(%rbp), %rcx
	movq	%rax, %r15
	subq	%r9, %rdx
	testq	%rdx, %rdx
	jg	.L1929
	testq	%r9, %r9
	jne	.L1762
.L1763:
	leaq	(%r15,%rcx), %rax
	movq	%r15, %xmm0
	movl	%r14d, -392(%rbp)
	movq	%rax, -336(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movl	$0, -396(%rbp)
	movq	-396(%rbp), %rax
	movaps	%xmm0, -352(%rbp)
	movl	%r14d, -388(%rbp)
	addq	$12, -344(%rbp)
	movq	%rax, (%r15)
	movl	%r14d, 8(%r15)
	jmp	.L1851
	.p2align 4,,10
	.p2align 3
.L1925:
	cmpq	%rdx, %r10
	jnb	.L1749
	movzbl	1(%rsi), %ecx
	leaq	2(%rsi), %r10
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r8d
	testb	%cl, %cl
	jns	.L1748
	cmpq	%rdx, %r10
	jnb	.L1749
	movzbl	2(%rsi), %ecx
	leaq	3(%rsi), %r10
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %r8d
	testb	%cl, %cl
	jns	.L1748
	cmpq	%rdx, %r10
	jnb	.L1749
	movzbl	3(%rsi), %eax
	leaq	4(%rsi), %r14
	movl	%eax, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%r8d, %ecx
	testb	%al, %al
	js	.L1930
	movq	%r14, -128(%rbp)
	movl	%ecx, %r14d
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1926:
	cmpq	%rdx, %rsi
	jnb	.L1736
	movzbl	1(%r15), %ecx
	leaq	2(%r15), %rsi
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L1735
	cmpq	%rdx, %rsi
	jnb	.L1736
	movzbl	2(%r15), %ecx
	leaq	3(%r15), %rsi
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%eax, %ebx
	testb	%cl, %cl
	jns	.L1735
	cmpq	%rdx, %rsi
	jnb	.L1736
	movzbl	3(%r15), %eax
	leaq	4(%r15), %rsi
	movl	%eax, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%ebx, %ecx
	testb	%al, %al
	js	.L1931
	movq	%rsi, -128(%rbp)
	movl	%ecx, %ebx
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L1929:
	movq	%r9, %rsi
	movq	%rax, %rdi
	movq	%rcx, -456(%rbp)
	movq	%r9, -440(%rbp)
	call	memmove@PLT
	movq	-440(%rbp), %r9
	movq	-456(%rbp), %rcx
.L1762:
	movq	%r9, %rdi
	movq	%rcx, -440(%rbp)
	call	_ZdlPv@PLT
	movq	-440(%rbp), %rcx
	jmp	.L1763
	.p2align 4,,10
	.p2align 3
.L1810:
	movq	-384(%rbp), %r15
	movq	%rbx, %r14
	movabsq	$-6148914691236517205, %rdi
	subq	%r15, %r14
	movq	%r14, %rax
	sarq	$3, %rax
	imulq	%rdi, %rax
	movabsq	$384307168202282325, %rdi
	cmpq	%rdi, %rax
	je	.L1932
	testq	%rax, %rax
	je	.L1933
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	ja	.L1934
	testq	%rcx, %rcx
	jne	.L1935
	movq	$24, -408(%rbp)
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	jmp	.L1814
	.p2align 4,,10
	.p2align 3
.L1736:
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movq	%rsi, -128(%rbp)
	leaq	.LC106(%rip), %rcx
	movl	%r9d, -440(%rbp)
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movl	-440(%rbp), %r9d
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L1749:
	leaq	.LC107(%rip), %rcx
	movq	%r10, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movl	%r9d, -440(%rbp)
	xorl	%r14d, %r14d
	movq	%r10, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-440(%rbp), %r9d
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1712:
	movq	-120(%rbp), %rax
	cmpq	%rax, -128(%rbp)
	jb	.L1728
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	jne	.L1845
.L1821:
	movq	-432(%rbp), %rdi
	movq	-368(%rbp), %rdx
	movb	$0, -160(%rbp)
	pxor	%xmm1, %xmm1
	movq	-376(%rbp), %rcx
	movq	-384(%rbp), %xmm0
	movq	$0, -368(%rbp)
	movq	%rdx, 16(%rdi)
	leaq	48(%rdi), %rdx
	movdqa	-160(%rbp), %xmm7
	movq	%rcx, %xmm6
	movl	$0, 24(%rdi)
	movq	%rdx, 32(%rdi)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm1, -384(%rbp)
	movaps	%xmm7, -224(%rbp)
	movups	%xmm0, (%rdi)
.L1825:
	movdqa	-224(%rbp), %xmm5
	movq	%rdi, %rbx
	movups	%xmm5, 48(%rdi)
.L1827:
	movq	%rax, 40(%rbx)
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -144(%rbp)
	cmpq	-448(%rbp), %rdi
	je	.L1828
	call	_ZdlPv@PLT
.L1828:
	movq	-376(%rbp), %rbx
	movq	-384(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1829
	.p2align 4,,10
	.p2align 3
.L1833:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1830
	call	_ZdlPv@PLT
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1833
.L1831:
	movq	-384(%rbp), %r12
.L1829:
	testq	%r12, %r12
	je	.L1703
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1703:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1936
	movq	-432(%rbp), %rax
	addq	$424, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1908:
	.cfi_restore_state
	movl	%r9d, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	%r9d, -408(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-128(%rbp), %r15
	movq	-120(%rbp), %rdx
	movl	-408(%rbp), %r9d
	jmp	.L1733
.L1922:
	movzbl	3(%r8), %ecx
	leaq	4(%r8), %rsi
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%r13d, %edx
	testb	%cl, %cl
	js	.L1937
	sall	$4, %edx
	movq	%rsi, -128(%rbp)
	movl	%edx, %r13d
	sarl	$4, %r13d
	addl	%r14d, %r13d
	jmp	.L1793
.L1921:
	movzbl	3(%rsi), %ecx
	leaq	4(%rsi), %r8
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r14d
	testb	%cl, %cl
	js	.L1938
	sall	$4, %r14d
	movq	%r8, -128(%rbp)
	sarl	$4, %r14d
	addl	%r13d, %r14d
	jmp	.L1806
.L1920:
	movzbl	3(%rsi), %ecx
	leaq	4(%rsi), %r8
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r15d
	testb	%cl, %cl
	jns	.L1769
	cmpq	%r8, %rax
	jbe	.L1776
	movzbl	4(%rsi), %eax
	addq	$5, %rsi
	movq	%rsi, -128(%rbp)
	movl	%eax, %r13d
	andl	$-16, %r13d
	testb	%al, %al
	js	.L1777
	sall	$28, %eax
	orl	%eax, %r15d
.L1778:
	testb	%r13b, %r13b
	je	.L1848
	movq	%r8, %rsi
	leaq	.LC41(%rip), %rdx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rax
	jmp	.L1780
.L1934:
	movabsq	$9223372036854775800, %rsi
.L1812:
	movq	%rsi, %rdi
	movq	%rsi, -408(%rbp)
	call	_Znwm@PLT
	movq	-408(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %rsi
	leaq	24(%rax), %rax
	movq	%rax, -408(%rbp)
.L1814:
	movq	-336(%rbp), %rax
	movdqa	-352(%rbp), %xmm5
	leaq	0(%r13,%r14), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, -336(%rbp)
	movq	%rax, 16(%rdx)
	movups	%xmm5, (%rdx)
	movaps	%xmm0, -352(%rbp)
	cmpq	%r15, %rbx
	je	.L1815
	leaq	-24(%rbx), %rdi
	movq	%r15, %rcx
	movabsq	$768614336404564651, %rax
	subq	%r15, %rdi
	shrq	$3, %rdi
	imulq	%rax, %rdi
	leaq	47(%r13), %rax
	subq	%r15, %rax
	cmpq	$94, %rax
	jbe	.L1857
	movabsq	$2305843009213693950, %rax
	testq	%rax, %rdi
	je	.L1857
	movabsq	$2305843009213693951, %r8
	movq	%r13, %rdx
	andq	%rdi, %r8
	addq	$1, %r8
	movq	%r8, %rax
	shrq	%rax
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L1817:
	movdqu	16(%rcx), %xmm1
	movdqu	(%rcx), %xmm3
	addq	$48, %rdx
	addq	$48, %rcx
	movdqu	-16(%rcx), %xmm0
	movups	%xmm3, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rax, %rdx
	jne	.L1817
	movq	%r8, %rcx
	andq	$-2, %rcx
	leaq	(%rcx,%rcx,2), %rax
	salq	$3, %rax
	leaq	(%r15,%rax), %rdx
	addq	%r13, %rax
	cmpq	%r8, %rcx
	je	.L1819
	movdqu	(%rdx), %xmm2
	movq	16(%rdx), %rdx
	movq	%rdx, 16(%rax)
	movups	%xmm2, (%rax)
.L1819:
	leaq	(%rdi,%rdi,2), %rax
	leaq	48(%r13,%rax,8), %rax
	movq	%rax, -408(%rbp)
.L1815:
	testq	%r15, %r15
	je	.L1820
	movq	%r15, %rdi
	movq	%rsi, -440(%rbp)
	call	_ZdlPv@PLT
	movq	-440(%rbp), %rsi
.L1820:
	movq	%r13, %xmm0
	movq	-352(%rbp), %rdi
	movq	%rsi, -368(%rbp)
	movhps	-408(%rbp), %xmm0
	movaps	%xmm0, -384(%rbp)
	testq	%rdi, %rdi
	je	.L1731
	call	_ZdlPv@PLT
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1713:
	cmpq	%rdx, %rsi
	jb	.L1728
.L1845:
	movl	-104(%rbp), %edx
	movq	-96(%rbp), %rsi
	leaq	-296(%rbp), %rbx
	leaq	-312(%rbp), %rdi
	movq	%rbx, -312(%rbp)
	movl	%edx, -320(%rbp)
	leaq	(%rsi,%rax), %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movl	-320(%rbp), %ecx
	movq	-312(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, -192(%rbp)
	movl	%ecx, -184(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	%rbx, %rdx
	je	.L1939
	movq	-296(%rbp), %rsi
	movq	-304(%rbp), %rax
	leaq	-160(%rbp), %rdi
	movq	%rdx, -176(%rbp)
	movq	%rbx, -312(%rbp)
	movq	%rsi, -160(%rbp)
	movq	%rax, -168(%rbp)
	movq	$0, -304(%rbp)
	movb	$0, -296(%rbp)
	movq	$0, -256(%rbp)
	movl	%ecx, -248(%rbp)
	movaps	%xmm0, -272(%rbp)
	cmpq	%rdi, %rdx
	je	.L1823
	movq	-432(%rbp), %rbx
	movq	%rdx, -240(%rbp)
	movq	%rsi, -224(%rbp)
	movl	%ecx, 24(%rbx)
	leaq	48(%rbx), %rcx
	movq	%rbx, %rdi
	movq	%rcx, 32(%rbx)
	leaq	-224(%rbp), %rcx
	movq	%rax, -232(%rbp)
	movq	$0, 16(%rbx)
	movups	%xmm0, (%rbx)
	cmpq	%rcx, %rdx
	je	.L1825
	movq	%rdx, 32(%rbx)
	movq	%rsi, 48(%rbx)
	jmp	.L1827
.L1728:
	leaq	.LC112(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc.constprop.0
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L1821
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1830:
	addq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L1833
	jmp	.L1831
.L1906:
	movq	-368(%rbp), %rdx
	movl	%eax, %ecx
	movq	-384(%rbp), %rax
	movabsq	$-6148914691236517205, %rsi
	subq	%rax, %rdx
	sarq	$3, %rdx
	imulq	%rsi, %rdx
	cmpq	%rdx, %rcx
	jbe	.L1704
	movq	-376(%rbp), %rdi
	leaq	(%rcx,%rcx,2), %r13
	xorl	%r14d, %r14d
	salq	$3, %r13
	movq	%rdi, %rbx
	subq	%rax, %rbx
	testq	%rcx, %rcx
	je	.L1705
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	-376(%rbp), %rdi
	movq	%rax, %r14
	movq	-384(%rbp), %rax
.L1705:
	cmpq	%rdi, %rax
	je	.L1706
	leaq	-24(%rdi), %rdx
	movq	%rax, %rcx
	movabsq	$768614336404564651, %rsi
	subq	%rax, %rdx
	shrq	$3, %rdx
	imulq	%rsi, %rdx
	leaq	47(%r14), %rsi
	subq	%rax, %rsi
	cmpq	$94, %rsi
	jbe	.L1854
	movabsq	$2305843009213693950, %rsi
	testq	%rsi, %rdx
	je	.L1854
	addq	$1, %rsi
	andq	%rsi, %rdx
	leaq	1(%rdx), %rsi
	movq	%r14, %rdx
	movq	%rsi, %rdi
	shrq	%rdi
	leaq	(%rdi,%rdi,2), %rdi
	salq	$4, %rdi
	addq	%r14, %rdi
.L1708:
	movdqu	16(%rcx), %xmm1
	movdqu	(%rcx), %xmm7
	addq	$48, %rdx
	addq	$48, %rcx
	movdqu	-16(%rcx), %xmm0
	movups	%xmm7, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	%xmm0, -16(%rdx)
	cmpq	%rdx, %rdi
	jne	.L1708
	movq	%rsi, %rcx
	andq	$-2, %rcx
	leaq	(%rcx,%rcx,2), %rdx
	salq	$3, %rdx
	addq	%rdx, %rax
	addq	%r14, %rdx
	cmpq	%rsi, %rcx
	je	.L1710
	movdqu	(%rax), %xmm6
	movq	16(%rax), %rax
	movq	%rax, 16(%rdx)
	movups	%xmm6, (%rdx)
.L1710:
	movq	-384(%rbp), %rdi
.L1706:
	testq	%rdi, %rdi
	je	.L1711
	call	_ZdlPv@PLT
.L1711:
	addq	%r14, %rbx
	addq	%r14, %r13
	movq	%r14, -384(%rbp)
	movq	%rbx, -376(%rbp)
	movq	%r13, -368(%rbp)
	jmp	.L1704
.L1933:
	movl	$24, %esi
	jmp	.L1812
.L1935:
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	leaq	(%rcx,%rcx,2), %rsi
	salq	$3, %rsi
	jmp	.L1812
.L1776:
	leaq	.LC108(%rip), %rcx
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, -128(%rbp)
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L1848:
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rax
	jmp	.L1780
.L1938:
	cmpq	%rax, %r8
	jnb	.L1802
	movzbl	4(%rsi), %eax
	addq	$5, %rsi
	movq	%rsi, -128(%rbp)
	movl	%eax, %edx
	andl	$248, %edx
	sete	%r9b
	cmpb	$120, %dl
	sete	%dl
	orl	%edx, %r9d
	testb	%al, %al
	js	.L1803
	sall	$28, %eax
	orl	%eax, %r14d
.L1804:
	testb	%r9b, %r9b
	jne	.L1905
	leaq	.LC41(%rip), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	movl	%r13d, %r14d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1806
.L1937:
	cmpq	%rax, %rsi
	jnb	.L1789
	movzbl	4(%r8), %eax
	addq	$5, %r8
	movq	%r8, -128(%rbp)
	movl	%eax, %ecx
	andl	$-8, %ecx
	cmpb	$120, %cl
	sete	%r8b
	testb	%cl, %cl
	sete	%cl
	orl	%ecx, %r8d
	testb	%al, %al
	js	.L1790
	movl	%eax, %r13d
	sall	$28, %r13d
	orl	%edx, %r13d
.L1791:
	testb	%r8b, %r8b
	jne	.L1847
	leaq	.LC41(%rip), %rdx
	movq	%r12, %rdi
	movl	%r14d, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	jmp	.L1793
.L1789:
	leaq	.LC109(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movq	%rsi, -128(%rbp)
	leaq	.LC40(%rip), %rdx
	xorl	%r13d, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L1847:
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	addl	%r14d, %r13d
	jmp	.L1793
.L1857:
	movq	%r13, %rdx
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L1816:
	movdqu	(%rax), %xmm4
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm4, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rbx
	jne	.L1816
	jmp	.L1819
.L1939:
	movdqu	-296(%rbp), %xmm6
	movq	-304(%rbp), %rax
	movaps	%xmm6, -160(%rbp)
.L1823:
	movq	-432(%rbp), %rbx
	movdqa	-160(%rbp), %xmm7
	pxor	%xmm0, %xmm0
	leaq	48(%rbx), %rdx
	movl	%ecx, 24(%rbx)
	movq	%rbx, %rdi
	movq	$0, 16(%rbx)
	movq	%rdx, 32(%rbx)
	movaps	%xmm7, -224(%rbp)
	movups	%xmm0, (%rbx)
	jmp	.L1825
.L1802:
	leaq	.LC110(%rip), %rcx
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, -128(%rbp)
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L1905
.L1931:
	cmpq	%rdx, %rsi
	jnb	.L1742
	movzbl	4(%r15), %eax
	addq	$5, %r15
	movq	%r15, -128(%rbp)
	movl	%eax, %r15d
	andl	$-16, %r15d
	testb	%al, %al
	js	.L1743
	movl	%eax, %ebx
	sall	$28, %ebx
	orl	%ecx, %ebx
.L1744:
	testb	%r15b, %r15b
	je	.L1850
	leaq	.LC41(%rip), %rdx
	movq	%r12, %rdi
	movl	%r9d, -440(%rbp)
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	movl	-440(%rbp), %r9d
	jmp	.L1746
.L1930:
	cmpq	%rdx, %r14
	jnb	.L1755
	movzbl	4(%rsi), %eax
	addq	$5, %rsi
	movq	%rsi, -128(%rbp)
	movl	%eax, %r15d
	andl	$-16, %r15d
	testb	%al, %al
	js	.L1756
	movl	%eax, %r8d
	sall	$28, %r8d
	orl	%ecx, %r8d
.L1757:
	testb	%r15b, %r15b
	je	.L1904
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	movl	%r9d, -440(%rbp)
	leaq	.LC41(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	-440(%rbp), %r9d
	jmp	.L1759
.L1854:
	movq	%r14, %rdx
.L1707:
	movdqu	(%rax), %xmm7
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm7, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rdi
	jne	.L1707
	jmp	.L1710
.L1755:
	leaq	.LC107(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movl	%r9d, -440(%rbp)
	movq	%r14, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-440(%rbp), %r9d
	xorl	%r8d, %r8d
	jmp	.L1904
.L1927:
	xorl	%eax, %eax
	leaq	.LC104(%rip), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	testb	%bl, %bl
	je	.L1725
.L1726:
	leaq	.LC41(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L1725
.L1742:
	leaq	.LC106(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movq	%rsi, -128(%rbp)
	leaq	.LC40(%rip), %rdx
	movl	%r9d, -440(%rbp)
	xorl	%ebx, %ebx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-440(%rbp), %r9d
.L1850:
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rdx
	jmp	.L1746
.L1756:
	leaq	.LC107(%rip), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movl	%r9d, -440(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-440(%rbp), %r9d
	xorl	%r8d, %r8d
	jmp	.L1757
.L1936:
	call	__stack_chk_fail@PLT
.L1790:
	leaq	.LC109(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC40(%rip), %rdx
	movb	%r8b, -456(%rbp)
	movq	%rsi, -440(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-456(%rbp), %r8d
	movq	-440(%rbp), %rsi
	jmp	.L1791
.L1777:
	movq	%r8, %rsi
	leaq	.LC108(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, -440(%rbp)
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-440(%rbp), %r8
	jmp	.L1778
.L1743:
	leaq	.LC106(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	leaq	.LC40(%rip), %rdx
	movl	%r9d, -456(%rbp)
	movq	%rsi, -440(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-456(%rbp), %r9d
	movq	-440(%rbp), %rsi
	jmp	.L1744
.L1932:
	leaq	.LC35(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1803:
	movq	%r8, %rsi
	leaq	.LC110(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movb	%r9b, -456(%rbp)
	xorl	%r14d, %r14d
	movq	%r8, -440(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-456(%rbp), %r9d
	movq	-440(%rbp), %r8
	jmp	.L1804
	.cfi_endproc
.LFE19137:
	.size	_ZN2v88internal4wasm18DecodeAsmJsOffsetsEPKhS3_, .-_ZN2v88internal4wasm18DecodeAsmJsOffsetsEPKhS3_
	.section	.text._ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_,"axG",@progbits,_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_,comdat
	.p2align 4
	.weak	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_
	.type	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_, @function
_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_:
.LFB24157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	cmpq	%rsi, %rdi
	je	.L1940
	leaq	16(%rdi), %rcx
	movq	%rdi, %r14
	cmpq	%rsi, %rcx
	je	.L1940
	movq	%rdx, %r15
	movq	%rcx, %r13
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L1956:
	setb	%al
.L1944:
	movzbl	8(%r13), %ebx
	movb	%bl, -57(%rbp)
	movl	12(%r13), %ebx
	movl	%ebx, -56(%rbp)
	testb	%al, %al
	je	.L1952
	cmpq	%r13, %r14
	je	.L1946
	movq	%r13, %rdx
	movl	$16, %eax
	movq	%r14, %rsi
	subq	%r14, %rdx
	leaq	(%r14,%rax), %rdi
	call	memmove@PLT
.L1946:
	movl	-52(%rbp), %eax
	movl	%r12d, 4(%r14)
	addq	$16, %r13
	movl	%eax, (%r14)
	movzbl	-57(%rbp), %eax
	movb	%al, 8(%r14)
	movl	-56(%rbp), %eax
	movl	%eax, 12(%r14)
	cmpq	%r13, -72(%rbp)
	je	.L1940
.L1951:
	movl	0(%r13), %eax
	movl	4(%r13), %r12d
	movl	%eax, -52(%rbp)
	cmpl	4(%r14), %r12d
	jne	.L1956
	movl	%eax, %ecx
	movl	(%r14), %esi
	movl	32(%r15), %eax
	movl	%r12d, %edx
	movq	8(%r15), %rdi
	subl	%eax, %esi
	subl	%eax, %ecx
	addq	%rdi, %rsi
	addq	%rcx, %rdi
	call	memcmp@PLT
	shrl	$31, %eax
	jmp	.L1944
	.p2align 4,,10
	.p2align 3
.L1952:
	movl	%r12d, %eax
	movq	%r13, -80(%rbp)
	movq	%r14, -88(%rbp)
	movq	%r13, %r14
	movq	%rax, %r13
	jmp	.L1945
	.p2align 4,,10
	.p2align 3
.L1957:
	subq	$16, %r14
	cmpl	%eax, %r12d
	jnb	.L1954
.L1949:
	movdqu	(%r14), %xmm0
	movups	%xmm0, 16(%r14)
.L1945:
	movl	-12(%r14), %eax
	movq	%r14, %rbx
	cmpl	%eax, %r12d
	jne	.L1957
	movl	32(%r15), %eax
	movl	-16(%r14), %esi
	movq	%r13, %rdx
	subq	$16, %r14
	movl	-52(%rbp), %ecx
	movq	8(%r15), %rdi
	subl	%eax, %esi
	subl	%eax, %ecx
	addq	%rdi, %rsi
	addq	%rcx, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	js	.L1949
.L1954:
	movl	-52(%rbp), %eax
	movq	-80(%rbp), %r13
	movl	%r12d, 4(%rbx)
	movq	-88(%rbp), %r14
	movl	%eax, (%rbx)
	movzbl	-57(%rbp), %eax
	addq	$16, %r13
	movb	%al, 8(%rbx)
	movl	-56(%rbp), %eax
	movl	%eax, 12(%rbx)
	cmpq	%r13, -72(%rbp)
	jne	.L1951
.L1940:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24157:
	.size	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_, .-_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_
	.section	.text._ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_,"axG",@progbits,_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_,comdat
	.p2align 4
	.weak	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_
	.type	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_, @function
_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_:
.LFB24161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	movq	%rax, %rcx
	pushq	%r13
	sarq	$4, %rcx
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -128(%rbp)
	addq	%rax, %rdx
	movq	%rdi, -120(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rax, -136(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -56(%rbp)
	cmpq	$96, %rax
	jle	.L1959
	movq	%rdi, %r12
	movq	%rsi, %rbx
	.p2align 4,,10
	.p2align 3
.L1960:
	movq	%r12, %rdi
	addq	$112, %r12
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_
	movq	%rbx, %rax
	subq	%r12, %rax
	cmpq	$96, %rax
	jg	.L1960
	movq	-72(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_
	cmpq	$112, -136(%rbp)
	jle	.L1958
	movl	$7, %r13d
	.p2align 4,,10
	.p2align 3
.L2003:
	leaq	(%r13,%r13), %rax
	movq	%rax, -64(%rbp)
	cmpq	%rax, -96(%rbp)
	jl	.L2009
	movq	%r13, %rax
	movq	-120(%rbp), %r9
	movq	%r13, %rcx
	movq	%r14, -88(%rbp)
	salq	$5, %rax
	salq	$4, %rcx
	movq	-128(%rbp), %r12
	movq	%r13, -112(%rbp)
	movq	%rax, -80(%rbp)
	leaq	(%r9,%rcx), %rbx
	movq	%r9, %r13
	.p2align 4,,10
	.p2align 3
.L1973:
	movq	-80(%rbp), %rax
	movq	%rbx, %r15
	leaq	0(%r13,%rax), %r14
	cmpq	%rbx, %r14
	jne	.L1963
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L2034:
	movdqu	(%r15), %xmm0
	addq	$16, %r12
	addq	$16, %r15
	movups	%xmm0, -16(%r12)
	cmpq	%r13, %rbx
	je	.L2033
.L1969:
	cmpq	%r15, %r14
	je	.L1964
.L1963:
	movl	4(%r15), %edx
	cmpl	4(%r13), %edx
	je	.L1965
	setb	%al
.L1966:
	testb	%al, %al
	jne	.L2034
	movdqu	0(%r13), %xmm1
	addq	$16, %r13
	addq	$16, %r12
	movups	%xmm1, -16(%r12)
	cmpq	%r13, %rbx
	jne	.L1969
.L2033:
	xorl	%edx, %edx
	jmp	.L2004
	.p2align 4,,10
	.p2align 3
.L1965:
	movq	-88(%rbp), %rax
	movl	0(%r13), %esi
	movl	32(%rax), %r8d
	movq	8(%rax), %rdi
	movl	(%r15), %eax
	subl	%r8d, %esi
	subl	%r8d, %eax
	addq	%rdi, %rsi
	addq	%rax, %rdi
	call	memcmp@PLT
	shrl	$31, %eax
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L1964:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	subq	%r13, %rdx
	movq	%rdx, -104(%rbp)
	call	memmove@PLT
	movq	-104(%rbp), %rdx
.L2004:
	movq	%r14, %r13
	addq	%rdx, %r12
	subq	%r15, %r13
	cmpq	%r15, %r14
	je	.L1971
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	memmove@PLT
.L1971:
	movq	-72(%rbp), %rdx
	addq	%r13, %r12
	addq	-80(%rbp), %rbx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$4, %rax
	cmpq	%rax, -64(%rbp)
	jg	.L2023
	movq	%r14, %r13
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L2023:
	movq	%r14, %r15
	movq	-112(%rbp), %r13
	movq	-88(%rbp), %r14
.L1962:
	cmpq	%r13, %rax
	cmovg	%r13, %rax
	salq	$4, %rax
	leaq	(%r15,%rax), %rbx
	cmpq	%r15, %rbx
	je	.L2012
	movq	%rbx, %r9
	.p2align 4,,10
	.p2align 3
.L1981:
	cmpq	%r9, -72(%rbp)
	je	.L1975
.L2039:
	movl	4(%r9), %edx
	cmpl	4(%r15), %edx
	je	.L1976
	setb	%al
.L1977:
	testb	%al, %al
	je	.L1978
	movdqu	(%r9), %xmm2
	addq	$16, %r12
	addq	$16, %r9
	movups	%xmm2, -16(%r12)
	cmpq	%r15, %rbx
	jne	.L1981
.L1980:
	movq	-72(%rbp), %rdx
	subq	%r9, %rdx
.L1974:
	subq	%r15, %rbx
	leaq	(%r12,%rbx), %rdi
	cmpq	%r9, -72(%rbp)
	je	.L2005
	movq	%r9, %rsi
	call	memmove@PLT
.L2005:
	salq	$2, %r13
	cmpq	%r13, -96(%rbp)
	jl	.L2013
.L2040:
	movq	-64(%rbp), %rcx
	movq	-128(%rbp), %r9
	movq	%r13, %rax
	movq	%r14, -104(%rbp)
	salq	$4, %rax
	movq	%r13, -88(%rbp)
	movq	-120(%rbp), %rbx
	salq	$4, %rcx
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	leaq	(%r9,%rcx), %r12
	movq	%r12, %r15
	movq	%r9, %r12
	leaq	(%r12,%rax), %r14
	cmpq	%r15, %r14
	je	.L1983
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	%r15, %r13
	.p2align 4,,10
	.p2align 3
.L1989:
	movl	4(%r13), %edx
	cmpl	4(%r12), %edx
	je	.L1984
.L2038:
	setb	%al
.L1985:
	testb	%al, %al
	je	.L1986
	movdqu	0(%r13), %xmm4
	addq	$16, %r13
	addq	$16, %rbx
	movups	%xmm4, -16(%rbx)
	cmpq	%r13, %r14
	je	.L1987
	cmpq	%r15, %r12
	jne	.L1989
.L1987:
	movq	%r15, %rdx
	subq	%r12, %rdx
	cmpq	%r12, %r15
	je	.L1990
.L2007:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -112(%rbp)
	call	memmove@PLT
	movq	-112(%rbp), %rdx
.L1990:
	movq	%r14, %r12
	addq	%rdx, %rbx
	subq	%r13, %r12
	cmpq	%r13, %r14
	je	.L1991
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	addq	%r12, %rbx
	call	memmove@PLT
	movq	-56(%rbp), %rax
	addq	-80(%rbp), %r15
	subq	%r14, %rax
	sarq	$4, %rax
	cmpq	%rax, -88(%rbp)
	jg	.L2035
.L1992:
	movq	-80(%rbp), %rax
	movq	%r14, %r12
	leaq	(%r12,%rax), %r14
	cmpq	%r15, %r14
	jne	.L2036
.L1983:
	movq	%r15, %rdx
	movq	%r15, %r13
	subq	%r12, %rdx
	cmpq	%r12, %r15
	jne	.L2007
	addq	%rdx, %rbx
	xorl	%r12d, %r12d
	movq	%r15, %r13
	.p2align 4,,10
	.p2align 3
.L1991:
	movq	-56(%rbp), %rax
	addq	%r12, %rbx
	addq	-80(%rbp), %r15
	subq	%r13, %rax
	sarq	$4, %rax
	cmpq	%rax, -88(%rbp)
	jle	.L1992
	movq	%r13, %r15
	movq	-104(%rbp), %r14
	movq	-88(%rbp), %r13
	movq	%r15, %r8
.L1982:
	movq	-64(%rbp), %rcx
	cmpq	%rax, %rcx
	movq	%rcx, %r15
	cmovg	%rax, %r15
	salq	$4, %r15
	addq	%r8, %r15
	movq	%r15, %r12
	cmpq	%r15, -56(%rbp)
	jne	.L2030
	jmp	.L1994
	.p2align 4,,10
	.p2align 3
.L2037:
	movdqu	(%r12), %xmm6
	addq	$16, %rbx
	addq	$16, %r12
	movups	%xmm6, -16(%rbx)
	cmpq	%r12, -56(%rbp)
	je	.L1994
.L2030:
	cmpq	%r8, %r15
	je	.L1994
.L1999:
	movl	4(%r12), %edx
	cmpl	4(%r8), %edx
	je	.L1995
	setb	%al
.L1996:
	testb	%al, %al
	jne	.L2037
	movdqu	(%r8), %xmm7
	addq	$16, %r8
	addq	$16, %rbx
	movups	%xmm7, -16(%rbx)
	cmpq	%r8, %r15
	je	.L1994
	cmpq	%r12, -56(%rbp)
	jne	.L1999
.L1994:
	movq	%r15, %rdx
	subq	%r8, %rdx
	cmpq	%r8, %r15
	je	.L2000
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -64(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %rdx
.L2000:
	movq	-56(%rbp), %rax
	cmpq	%r12, %rax
	je	.L2001
	movq	%rax, %r8
	leaq	(%rbx,%rdx), %rdi
	movq	%r12, %rsi
	subq	%r12, %r8
	movq	%r8, %rdx
	call	memmove@PLT
.L2001:
	cmpq	%r13, -96(%rbp)
	jg	.L2003
.L1958:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1986:
	.cfi_restore_state
	movdqu	(%r12), %xmm5
	addq	$16, %r12
	addq	$16, %rbx
	movups	%xmm5, -16(%rbx)
	cmpq	%r15, %r12
	je	.L1987
	cmpq	%r13, %r14
	je	.L1987
	movl	4(%r13), %edx
	cmpl	4(%r12), %edx
	jne	.L2038
.L1984:
	movq	-104(%rbp), %rax
	movl	(%r12), %esi
	movl	32(%rax), %ecx
	movq	8(%rax), %rdi
	movl	0(%r13), %eax
	subl	%ecx, %esi
	subl	%ecx, %eax
	addq	%rdi, %rsi
	addq	%rax, %rdi
	call	memcmp@PLT
	shrl	$31, %eax
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L1995:
	movl	32(%r14), %ecx
	movl	(%r8), %esi
	movq	%r8, -64(%rbp)
	movl	(%r12), %eax
	movq	8(%r14), %rdi
	subl	%ecx, %esi
	subl	%ecx, %eax
	addq	%rdi, %rsi
	addq	%rax, %rdi
	call	memcmp@PLT
	movq	-64(%rbp), %r8
	shrl	$31, %eax
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L1978:
	movdqu	(%r15), %xmm3
	addq	$16, %r15
	addq	$16, %r12
	movups	%xmm3, -16(%r12)
	cmpq	%r15, %rbx
	je	.L1980
	cmpq	%r9, -72(%rbp)
	jne	.L2039
.L1975:
	subq	%r15, %rbx
	movq	%r15, %rsi
	movq	%r12, %rdi
	salq	$2, %r13
	movq	%rbx, %rdx
	call	memmove@PLT
	cmpq	%r13, -96(%rbp)
	jge	.L2040
.L2013:
	movq	-120(%rbp), %rbx
	movq	-96(%rbp), %rax
	movq	-128(%rbp), %r8
	jmp	.L1982
	.p2align 4,,10
	.p2align 3
.L1976:
	movl	32(%r14), %ecx
	movl	(%r9), %eax
	movq	%r9, -80(%rbp)
	movl	(%r15), %esi
	movq	8(%r14), %rdi
	subl	%ecx, %eax
	subl	%ecx, %esi
	addq	%rdi, %rsi
	addq	%rax, %rdi
	call	memcmp@PLT
	movq	-80(%rbp), %r9
	shrl	$31, %eax
	jmp	.L1977
	.p2align 4,,10
	.p2align 3
.L2035:
	movq	%r14, %r8
	movq	-88(%rbp), %r13
	movq	-104(%rbp), %r14
	jmp	.L1982
	.p2align 4,,10
	.p2align 3
.L2009:
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %r12
	movq	-96(%rbp), %rax
	movq	-120(%rbp), %r15
	jmp	.L1962
	.p2align 4,,10
	.p2align 3
.L2012:
	movq	%r15, %r9
	jmp	.L1974
	.p2align 4,,10
	.p2align 3
.L1959:
	addq	$104, %rsp
	movq	%r14, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_
	.cfi_endproc
.LFE24161:
	.size	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_, .-_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN2v88internal4wasm10WasmModuleESt14default_deleteIS7_EEEOSt10unique_ptrIT_T0_E,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5IN2v88internal4wasm10WasmModuleESt14default_deleteIS7_EEEOSt10unique_ptrIT_T0_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN2v88internal4wasm10WasmModuleESt14default_deleteIS7_EEEOSt10unique_ptrIT_T0_E
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN2v88internal4wasm10WasmModuleESt14default_deleteIS7_EEEOSt10unique_ptrIT_T0_E, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN2v88internal4wasm10WasmModuleESt14default_deleteIS7_EEEOSt10unique_ptrIT_T0_E:
.LFB24277:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rsi)
	movq	$0, (%rdi)
	je	.L2044
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	$0, (%rbx)
	movabsq	$4294967297, %rcx
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2044:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE24277:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN2v88internal4wasm10WasmModuleESt14default_deleteIS7_EEEOSt10unique_ptrIT_T0_E, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN2v88internal4wasm10WasmModuleESt14default_deleteIS7_EEEOSt10unique_ptrIT_T0_E
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN2v88internal4wasm10WasmModuleESt14default_deleteIS7_EEEOSt10unique_ptrIT_T0_E
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN2v88internal4wasm10WasmModuleESt14default_deleteIS7_EEEOSt10unique_ptrIT_T0_E,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN2v88internal4wasm10WasmModuleESt14default_deleteIS7_EEEOSt10unique_ptrIT_T0_E
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,"axG",@progbits,_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_:
.LFB24290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r10
	cmpq	%rsi, %r10
	ja	.L2065
	movl	$0, (%rdx)
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L2047:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2065:
	.cfi_restore_state
	movzbl	(%rsi), %r9d
	movl	%r9d, %eax
	andl	$127, %eax
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L2066
	movl	$1, (%rdx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2066:
	.cfi_restore_state
	leaq	1(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L2050
	movzbl	1(%rsi), %r9d
	movl	%r9d, %r8d
	sall	$7, %r8d
	andl	$16256, %r8d
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L2067
	movl	$2, (%rdx)
	jmp	.L2047
	.p2align 4,,10
	.p2align 3
.L2050:
	movl	$1, (%rdx)
.L2064:
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2067:
	.cfi_restore_state
	leaq	2(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L2052
	movzbl	2(%rsi), %r9d
	movl	%r9d, %r8d
	sall	$14, %r8d
	andl	$2080768, %r8d
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L2068
	movl	$3, (%rdx)
	jmp	.L2047
	.p2align 4,,10
	.p2align 3
.L2052:
	movl	$2, (%rdx)
	jmp	.L2064
	.p2align 4,,10
	.p2align 3
.L2068:
	leaq	3(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2069
	movl	$3, (%rdx)
	jmp	.L2064
.L2069:
	movzbl	3(%rsi), %r9d
	movl	%r9d, %r8d
	sall	$21, %r8d
	andl	$266338304, %r8d
	orl	%r8d, %eax
	testb	%r9b, %r9b
	js	.L2070
	movl	$4, (%rdx)
	jmp	.L2047
.L2070:
	leaq	4(%rsi), %r12
	cmpq	%r12, %r10
	jbe	.L2056
	movzbl	4(%rsi), %ebx
	movl	$5, (%rdx)
	testb	%bl, %bl
	js	.L2057
	movl	%ebx, %edx
	sall	$28, %edx
	orl	%edx, %eax
.L2058:
	andl	$240, %ebx
	je	.L2047
	leaq	.LC41(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L2047
.L2056:
	movl	$4, (%rdx)
	xorl	%ebx, %ebx
.L2057:
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-24(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L2058
	.cfi_endproc
.LFE24290:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.section	.text._ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,"axG",@progbits,_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.type	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, @function
_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_:
.LFB24292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r10
	cmpq	%rsi, %r10
	ja	.L2105
	movl	$0, (%rdx)
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L2071:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2105:
	.cfi_restore_state
	movzbl	(%rsi), %r9d
	movq	%r9, %rax
	andl	$127, %eax
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2106
	movl	$1, (%rdx)
	salq	$57, %rax
	addq	$16, %rsp
	popq	%rbx
	sarq	$57, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2106:
	.cfi_restore_state
	leaq	1(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L2074
	movzbl	1(%rsi), %r9d
	movq	%r9, %r8
	salq	$7, %r8
	andl	$16256, %r8d
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2107
	salq	$50, %rax
	movl	$2, (%rdx)
	sarq	$50, %rax
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2074:
	movl	$1, (%rdx)
.L2104:
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2107:
	.cfi_restore_state
	leaq	2(%rsi), %r8
	cmpq	%r8, %r10
	jbe	.L2076
	movzbl	2(%rsi), %r8d
	movq	%r8, %r9
	salq	$14, %r9
	andl	$2080768, %r9d
	orq	%r9, %rax
	testb	%r8b, %r8b
	js	.L2108
	salq	$43, %rax
	movl	$3, (%rdx)
	sarq	$43, %rax
	jmp	.L2071
	.p2align 4,,10
	.p2align 3
.L2076:
	movl	$2, (%rdx)
	jmp	.L2104
	.p2align 4,,10
	.p2align 3
.L2108:
	leaq	3(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2109
	movl	$3, (%rdx)
	jmp	.L2104
.L2109:
	movzbl	3(%rsi), %r9d
	movq	%r9, %r8
	salq	$21, %r8
	andl	$266338304, %r8d
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2110
	salq	$36, %rax
	movl	$4, (%rdx)
	sarq	$36, %rax
	jmp	.L2071
.L2110:
	leaq	4(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2111
	movl	$4, (%rdx)
	jmp	.L2104
.L2111:
	movabsq	$34091302912, %r11
	movzbl	4(%rsi), %r9d
	movq	%r9, %r8
	salq	$28, %r8
	andq	%r11, %r8
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2112
	salq	$29, %rax
	movl	$5, (%rdx)
	sarq	$29, %rax
	jmp	.L2071
.L2112:
	leaq	5(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2113
	movl	$5, (%rdx)
	xorl	%eax, %eax
	movq	%r8, %rsi
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L2097:
	salq	$22, %rax
	sarq	$22, %rax
	jmp	.L2071
.L2113:
	movabsq	$4363686772736, %r11
	movzbl	5(%rsi), %r9d
	movq	%r9, %r8
	salq	$35, %r8
	andq	%r11, %r8
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2114
	movl	$6, (%rdx)
	jmp	.L2097
.L2114:
	leaq	6(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2115
	movl	$6, (%rdx)
	xorl	%eax, %eax
	movq	%r8, %rsi
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L2096:
	salq	$15, %rax
	sarq	$15, %rax
	jmp	.L2071
.L2115:
	movabsq	$558551906910208, %r11
	movzbl	6(%rsi), %r9d
	movq	%r9, %r8
	salq	$42, %r8
	andq	%r11, %r8
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2116
	movl	$7, (%rdx)
	jmp	.L2096
.L2116:
	leaq	7(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2117
	movl	$7, (%rdx)
	xorl	%eax, %eax
	movq	%r8, %rsi
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L2095:
	salq	$8, %rax
	sarq	$8, %rax
	jmp	.L2071
.L2117:
	movabsq	$71494644084506624, %r11
	movzbl	7(%rsi), %r9d
	movq	%r9, %r8
	salq	$49, %r8
	andq	%r11, %r8
	orq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2118
	movl	$8, (%rdx)
	jmp	.L2095
.L2118:
	leaq	8(%rsi), %r8
	cmpq	%r8, %r10
	ja	.L2119
	movl	$8, (%rdx)
	xorl	%eax, %eax
	movq	%r8, %rsi
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%eax, %eax
.L2094:
	addq	%rax, %rax
	sarq	%rax
	jmp	.L2071
.L2119:
	movabsq	$9151314442816847872, %r11
	movzbl	8(%rsi), %r9d
	movq	%r9, %r8
	salq	$56, %r8
	andq	%r11, %r8
	orq	%rax, %r8
	movq	%r8, %rax
	testb	%r9b, %r9b
	js	.L2120
	movl	$9, (%rdx)
	jmp	.L2094
.L2120:
	leaq	9(%rsi), %r12
	cmpq	%r12, %r10
	jbe	.L2090
	movzbl	9(%rsi), %ebx
	movl	$10, (%rdx)
	testb	%bl, %bl
	js	.L2091
	movq	%rbx, %rax
	salq	$63, %rax
	orq	%r8, %rax
.L2092:
	testb	%bl, %bl
	je	.L2071
	cmpb	$127, %bl
	je	.L2071
	leaq	.LC41(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
	jmp	.L2071
.L2090:
	movl	$9, (%rdx)
	xorl	%ebx, %ebx
.L2091:
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-24(%rbp), %rdi
	xorl	%eax, %eax
	jmp	.L2092
	.cfi_endproc
.LFE24292:
	.size	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_, .-_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE.str1.1,"aMS",@progbits,1
.LC115:
	.string	"global index"
.LC116:
	.string	"global index is out of bounds"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE.str1.8,"aMS",@progbits,1
	.align 8
.LC117:
	.string	"only immutable imported globals can be used in initializer expressions"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE.str1.1
.LC118:
	.string	"immi32"
.LC119:
	.string	"immf32"
.LC120:
	.string	"immi64"
.LC121:
	.string	"immf64"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE.str1.8
	.align 8
.LC122:
	.string	"invalid opcode in initialization expression"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE.str1.1
.LC123:
	.string	"end opcode"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE.str1.8
	.align 8
.LC124:
	.string	"type error in init expression, expected %s, got %s"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE, @function
_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE:
.LFB19095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	16(%rdi), %r14
	movq	24(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	%r14d, %edx
	je	.L2122
	movzbl	(%r14), %eax
	leaq	1(%r14), %rsi
	movq	%rsi, 16(%rdi)
	cmpb	$68, %al
	ja	.L2123
	cmpb	$34, %al
	jbe	.L2124
	subl	$35, %eax
	cmpb	$33, %al
	ja	.L2124
	leaq	.L2126(%rip), %rdi
	movzbl	%al, %eax
	movq	%rdx, %rcx
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE,comdat
	.align 4
	.align 4
.L2126:
	.long	.L2130-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2124-.L2126
	.long	.L2129-.L2126
	.long	.L2128-.L2126
	.long	.L2127-.L2126
	.long	.L2125-.L2126
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE,comdat
	.p2align 4,,10
	.p2align 3
.L2123:
	cmpb	$-48, %al
	je	.L2131
	cmpb	$-46, %al
	jne	.L2124
	cmpb	$0, 87(%rdi)
	jne	.L2191
.L2124:
	leaq	.LC122(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r12), %rsi
	movq	24(%r12), %rdx
	xorl	%r9d, %r9d
.L2170:
	movq	%rsi, 16(%r12)
	cmpl	%esi, %edx
	je	.L2157
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %rcx
	movq	%rcx, 16(%r12)
	cmpb	$11, %dl
	jne	.L2192
	testb	%bl, %bl
	jne	.L2193
.L2162:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	movq	%r9, %rax
	jne	.L2194
	addq	$56, %rsp
	movq	%r15, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2193:
	.cfi_restore_state
	leaq	.L2164(%rip), %rdx
	movslq	(%rdx,%r9,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE,comdat
	.align 4
	.align 4
.L2164:
	.long	.L2161-.L2164
	.long	.L2169-.L2164
	.long	.L2162-.L2164
	.long	.L2168-.L2164
	.long	.L2167-.L2164
	.long	.L2166-.L2164
	.long	.L2180-.L2164
	.long	.L2163-.L2164
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE,comdat
.L2180:
	movl	$8, %edi
.L2165:
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movzbl	%bl, %edi
	movq	%rax, %r13
	call	_ZN2v88internal4wasm10ValueTypes8TypeNameENS1_9ValueTypeE
	movq	%r13, %r8
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	.LC124(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-88(%rbp), %r9
	jmp	.L2162
.L2163:
	movl	$7, %edi
	jmp	.L2165
	.p2align 4,,10
	.p2align 3
.L2192:
	movzbl	%dl, %r9d
.L2160:
	movl	$11, %r8d
	leaq	.LC123(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC61(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%r9d, %r9d
	testb	%bl, %bl
	je	.L2162
.L2161:
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	jmp	.L2165
.L2125:
	cmpq	%rsi, %rdx
	jb	.L2153
	subq	%rsi, %rcx
	movl	%ecx, %eax
	cmpl	$7, %ecx
	jbe	.L2153
	movsd	1(%r14), %xmm0
.L2154:
	movq	%xmm0, %r15
	movl	$5, %r9d
	movl	$8, %r8d
.L2137:
	cmpl	%eax, %r8d
	ja	.L2156
.L2195:
	addq	%r8, %rsi
	jmp	.L2170
.L2130:
	leaq	-64(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movb	$0, -76(%rbp)
	leaq	.LC115(%rip), %rcx
	movq	$0, -72(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	24(%r13), %rcx
	movq	32(%r13), %rdx
	leaq	.LC116(%rip), %rsi
	movl	%eax, -80(%rbp)
	movl	%eax, %eax
	subq	%rcx, %rdx
	sarq	$5, %rdx
	cmpq	%rdx, %rax
	jnb	.L2190
	movq	%rax, %rdx
	salq	$5, %rdx
	addq	%rdx, %rcx
	cmpb	$0, 1(%rcx)
	jne	.L2135
	cmpb	$0, 28(%rcx)
	jne	.L2136
.L2135:
	leaq	.LC117(%rip), %rsi
.L2190:
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
.L2134:
	movq	24(%r12), %rdx
	movq	16(%r12), %rsi
	movl	%edx, %eax
	subl	%esi, %eax
	cmpl	%eax, %r8d
	jbe	.L2195
	.p2align 4,,10
	.p2align 3
.L2156:
	movl	%r8d, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rsi
	movq	%rsi, 16(%r12)
.L2157:
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%rsi, -88(%rbp)
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	movq	-88(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	%rax, 16(%r12)
	jmp	.L2160
.L2128:
	leaq	-72(%rbp), %rdx
	xorl	%r8d, %r8d
	leaq	.LC120(%rip), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	24(%r12), %rdx
	movq	16(%r12), %rsi
	movl	$3, %r9d
	movq	%rax, %r15
	movl	-72(%rbp), %r8d
	movl	%edx, %eax
	subl	%esi, %eax
	jmp	.L2137
.L2127:
	cmpq	%rsi, %rdx
	jb	.L2151
	subq	%rsi, %rcx
	movl	%ecx, %eax
	cmpl	$3, %ecx
	jbe	.L2151
	movss	1(%r14), %xmm0
.L2152:
	movd	%xmm0, %r15d
	movl	$4, %r9d
	movl	$4, %r8d
	jmp	.L2137
.L2129:
	cmpq	%rsi, %rdx
	ja	.L2196
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	leaq	.LC118(%rip), %rcx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rdx
	movq	16(%r12), %rsi
	xorl	%r8d, %r8d
.L2150:
	movl	%edx, %eax
	movl	$2, %r9d
	subl	%esi, %eax
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L2131:
	cmpb	$0, 87(%rdi)
	movl	$6, %r9d
	jne	.L2170
	cmpb	$0, 81(%rdi)
	jne	.L2170
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2191:
	leaq	-76(%rbp), %rdx
	xorl	%r8d, %r8d
	movabsq	$4294967296, %rax
	leaq	.LC96(%rip), %rcx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	24(%r12), %rdx
	movq	16(%r12), %rsi
	movl	$7, %r9d
	movl	%eax, %r15d
	movl	-76(%rbp), %r8d
	movl	%edx, %eax
	subl	%esi, %eax
	jmp	.L2137
	.p2align 4,,10
	.p2align 3
.L2122:
	xorl	%eax, %eax
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rax
	movq	%rax, 16(%r12)
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2196:
	movzbl	1(%r14), %eax
	movl	%eax, %r15d
	andl	$127, %r15d
	testb	%al, %al
	js	.L2197
	sall	$25, %r15d
	movl	$1, %r8d
	sarl	$25, %r15d
	jmp	.L2150
.L2166:
	movl	$4, %edi
	jmp	.L2165
.L2169:
	movq	24(%r13), %rcx
	movq	32(%r13), %rax
	movl	%r15d, %edx
	subq	%rcx, %rax
	sarq	$5, %rax
	cmpq	%rax, %rdx
	jnb	.L2181
	salq	$5, %rdx
	movzbl	(%rcx,%rdx), %edi
	cmpb	$1, %dil
	je	.L2162
	jmp	.L2165
.L2168:
	movl	$2, %edi
	jmp	.L2165
.L2167:
	movl	$3, %edi
	jmp	.L2165
	.p2align 4,,10
	.p2align 3
.L2136:
	movl	-64(%rbp), %r8d
	movq	%rax, %r15
	movl	$1, %r9d
	jmp	.L2134
	.p2align 4,,10
	.p2align 3
.L2197:
	leaq	2(%r14), %r8
	cmpq	%r8, %rdx
	ja	.L2198
	movq	%r8, %rsi
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC118(%rip), %rcx
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rdx
	movq	16(%r12), %rsi
	movl	$1, %r8d
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2151:
	leaq	.LC119(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rdx
	movq	16(%r12), %rsi
	pxor	%xmm0, %xmm0
	movl	%edx, %eax
	subl	%esi, %eax
	jmp	.L2152
	.p2align 4,,10
	.p2align 3
.L2153:
	leaq	.LC121(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rdx
	movq	16(%r12), %rsi
	pxor	%xmm0, %xmm0
	movl	%edx, %eax
	subl	%esi, %eax
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2181:
	xorl	%edi, %edi
	jmp	.L2165
	.p2align 4,,10
	.p2align 3
.L2198:
	movzbl	2(%r14), %edi
	movzbl	%r15b, %r15d
	movl	%edi, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r15d
	testb	%dil, %dil
	js	.L2199
	sall	$18, %r15d
	movl	$2, %r8d
	sarl	$18, %r15d
	jmp	.L2150
.L2199:
	leaq	3(%r14), %r8
	cmpq	%r8, %rdx
	ja	.L2200
	movq	%r8, %rsi
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC118(%rip), %rcx
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rdx
	movq	16(%r12), %rsi
	movl	$2, %r8d
	jmp	.L2150
.L2200:
	movzbl	3(%r14), %edi
	movl	%edi, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%r15d, %eax
	testb	%dil, %dil
	js	.L2201
	sall	$11, %eax
	movl	$3, %r8d
	movl	%eax, %r15d
	sarl	$11, %r15d
	jmp	.L2150
.L2201:
	leaq	4(%r14), %rsi
	cmpq	%rsi, %rdx
	ja	.L2202
	leaq	.LC118(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r12), %rcx
	movl	$3, %r8d
	xorl	%eax, %eax
.L2145:
	sall	$4, %eax
	movq	16(%r12), %rsi
	movq	%rcx, %rdx
	movl	%eax, %r15d
	sarl	$4, %r15d
	jmp	.L2150
.L2202:
	movzbl	4(%r14), %esi
	movl	$4, %r8d
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%eax, %edx
	movl	%edx, %eax
	testb	%sil, %sil
	jns	.L2145
	leaq	5(%r14), %rsi
	cmpq	%rsi, %rcx
	jbe	.L2176
	movzbl	5(%r14), %r9d
	testb	%r9b, %r9b
	js	.L2177
	movl	%r9d, %r15d
	movl	$5, %r8d
	sall	$28, %r15d
	orl	%edx, %r15d
.L2147:
	andb	$-8, %r9b
	je	.L2182
	cmpb	$120, %r9b
	je	.L2182
	leaq	.LC41(%rip), %rdx
	movq	%r12, %rdi
	movl	%r8d, -88(%rbp)
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	24(%r12), %rdx
	movq	16(%r12), %rsi
	movl	-88(%rbp), %r8d
	jmp	.L2150
.L2194:
	call	__stack_chk_fail@PLT
.L2176:
	xorl	%r9d, %r9d
.L2146:
	leaq	.LC118(%rip), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movb	%r9b, -93(%rbp)
	leaq	.LC40(%rip), %rdx
	movl	%r8d, -92(%rbp)
	xorl	%r15d, %r15d
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-93(%rbp), %r9d
	movl	-92(%rbp), %r8d
	movq	-88(%rbp), %rsi
	jmp	.L2147
.L2182:
	movq	24(%r12), %rdx
	movq	16(%r12), %rsi
	jmp	.L2150
.L2177:
	movl	$5, %r8d
	jmp	.L2146
	.cfi_endproc
.LFE19095:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE, .-_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv.str1.1,"aMS",@progbits,1
.LC125:
	.string	"globals count"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC126:
	.string	"invalid global index in init expression, index %u, other_index %u"
	.align 8
.LC127:
	.string	"type mismatch in global initialization (from global #%u), expected %s, got %s"
	.align 8
.LC128:
	.string	"type error in global initialization, expected %s, got %s"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv, @function
_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv:
.LFB19024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC125(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movl	$0, -96(%rbp)
	movq	%rax, %rdx
	movq	%r13, %rsi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movl	%eax, %r12d
	cmpl	$1000000, %eax
	ja	.L2349
.L2204:
	movq	96(%r14), %r13
	movq	32(%r13), %rsi
	movq	24(%r13), %rdi
	movq	%rsi, %r8
	subq	%rdi, %r8
	movq	%r8, %rax
	sarq	$5, %rax
	leal	(%r12,%rax), %edx
	movl	%eax, %ebx
	movq	40(%r13), %rax
	movl	%edx, -100(%rbp)
	subq	%rdi, %rax
	sarq	$5, %rax
	cmpq	%rax, %rdx
	ja	.L2350
.L2205:
	cmpq	$0, 56(%r14)
	jne	.L2203
	testl	%r12d, %r12d
	je	.L2214
	.p2align 4,,10
	.p2align 3
.L2213:
	movq	96(%r14), %rdi
	xorl	%eax, %eax
	xorl	%edx, %edx
	movl	$0, -88(%rbp)
	movw	%ax, -96(%rbp)
	movl	$0, -72(%rbp)
	movw	%dx, -68(%rbp)
	movq	32(%rdi), %rsi
	cmpq	40(%rdi), %rsi
	je	.L2215
	movdqa	-96(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movdqa	-80(%rbp), %xmm1
	movups	%xmm1, 16(%rsi)
	addq	$32, 32(%rdi)
.L2216:
	movq	96(%r14), %r12
	movq	16(%r14), %rsi
	movq	32(%r12), %r13
	leaq	-32(%r13), %rax
	movq	%rax, -112(%rbp)
	cmpl	%esi, 24(%r14)
	je	.L2217
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%r14)
	leal	-124(%rax), %edx
	cmpb	$3, %dl
	jbe	.L2218
	cmpb	$0, 168(%r14)
	jne	.L2220
	cmpb	$112, %al
	je	.L2222
	ja	.L2223
	cmpb	$104, %al
	je	.L2224
	cmpb	$111, %al
	jne	.L2220
	cmpb	$0, 87(%r14)
	movl	$6, %eax
	jne	.L2221
	.p2align 4,,10
	.p2align 3
.L2220:
	leaq	.LC53(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	xorl	%eax, %eax
.L2221:
	movb	%al, -32(%r13)
	movq	16(%r14), %rsi
	cmpl	%esi, 24(%r14)
	je	.L2226
.L2355:
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%r14)
	testb	%al, %al
	setne	%r15b
	cmpb	$1, %al
	jbe	.L2228
	leaq	.LC83(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
.L2228:
	movb	%r15b, -31(%r13)
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	16(%r14), %r15
	call	_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE
	movl	%eax, -24(%r13)
	movq	%rdx, -16(%r13)
	cmpl	$1, %eax
	jne	.L2229
	movl	-16(%r13), %ecx
	cmpl	%ebx, %ecx
	jnb	.L2351
	movl	%ecx, %eax
	movzbl	-32(%r13), %esi
	salq	$5, %rax
	addq	24(%r12), %rax
	movzbl	(%rax), %eax
	cmpb	%sil, %al
	jne	.L2352
	.p2align 4,,10
	.p2align 3
.L2231:
	cmpq	$0, 56(%r14)
	jne	.L2203
	addl	$1, %ebx
	cmpl	%ebx, -100(%rbp)
	jne	.L2213
.L2214:
	movq	96(%r14), %r10
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	xorl	%r11d, %r11d
	leaq	.L2298(%rip), %rsi
	movq	24(%r10), %rdx
	movq	32(%r10), %r9
	cmpq	%r9, %rdx
	jne	.L2301
	jmp	.L2306
	.p2align 4,,10
	.p2align 3
.L2353:
	cmpb	$0, 28(%rdx)
	je	.L2293
	movl	%r11d, 24(%rdx)
	addl	$1, %r11d
.L2294:
	addq	$32, %rdx
	cmpq	%rdx, %r9
	je	.L2306
.L2301:
	cmpb	$0, 1(%rdx)
	jne	.L2353
.L2293:
	movzbl	(%rdx), %eax
	leal	-6(%rax), %ecx
	cmpb	$1, %cl
	jbe	.L2323
	cmpb	$9, %al
	jne	.L2295
.L2323:
	movl	%r8d, 24(%rdx)
	addq	$32, %rdx
	addl	$1, %r8d
	cmpq	%rdx, %r9
	jne	.L2301
.L2306:
	movl	%edi, 48(%r10)
	movl	%r8d, 52(%r10)
.L2203:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2354
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2352:
	.cfi_restore_state
	cmpb	$10, %al
	ja	.L2232
	leaq	.L2234(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,comdat
	.align 4
	.align 4
.L2234:
	.long	.L2244-.L2234
	.long	.L2316-.L2234
	.long	.L2242-.L2234
	.long	.L2241-.L2234
	.long	.L2240-.L2234
	.long	.L2239-.L2234
	.long	.L2238-.L2234
	.long	.L2237-.L2234
	.long	.L2236-.L2234
	.long	.L2235-.L2234
	.long	.L2233-.L2234
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,comdat
.L2242:
	leaq	.LC11(%rip), %r9
.L2243:
	cmpb	$10, %sil
	ja	.L2245
	leaq	.L2247(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,comdat
	.align 4
	.align 4
.L2247:
	.long	.L2257-.L2247
	.long	.L2317-.L2247
	.long	.L2255-.L2247
	.long	.L2254-.L2247
	.long	.L2253-.L2247
	.long	.L2252-.L2247
	.long	.L2251-.L2247
	.long	.L2250-.L2247
	.long	.L2249-.L2247
	.long	.L2248-.L2247
	.long	.L2246-.L2247
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,comdat
.L2316:
	leaq	.LC1(%rip), %r9
	jmp	.L2243
	.p2align 4,,10
	.p2align 3
.L2218:
	movl	$-128, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	movb	%al, -32(%r13)
	movq	16(%r14), %rsi
	cmpl	%esi, 24(%r14)
	jne	.L2355
.L2226:
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%r14, %rdi
	xorl	%r15d, %r15d
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r14), %rax
	movq	%rax, 16(%r14)
	jmp	.L2228
	.p2align 4,,10
	.p2align 3
.L2229:
	movzbl	-32(%r13), %ecx
	cmpl	$7, %eax
	ja	.L2258
	leaq	.L2260(%rip), %rdi
	movl	%eax, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,comdat
	.align 4
	.align 4
.L2260:
	.long	.L2266-.L2260
	.long	.L2258-.L2260
	.long	.L2265-.L2260
	.long	.L2264-.L2260
	.long	.L2263-.L2260
	.long	.L2318-.L2260
	.long	.L2261-.L2260
	.long	.L2319-.L2260
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,comdat
	.p2align 4,,10
	.p2align 3
.L2261:
	cmpb	$8, %cl
	je	.L2231
	cmpb	$6, %cl
	je	.L2231
	movl	$1, %esi
	movl	$8, %eax
	jmp	.L2267
	.p2align 4,,10
	.p2align 3
.L2318:
	movl	$4, %eax
.L2262:
	cmpb	%al, %cl
	je	.L2231
.L2308:
	cmpb	$8, %al
	ja	.L2268
	leaq	.L2270(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,comdat
	.align 4
	.align 4
.L2270:
	.long	.L2268-.L2270
	.long	.L2320-.L2270
	.long	.L2276-.L2270
	.long	.L2275-.L2270
	.long	.L2274-.L2270
	.long	.L2273-.L2270
	.long	.L2272-.L2270
	.long	.L2271-.L2270
	.long	.L2269-.L2270
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,comdat
	.p2align 4,,10
	.p2align 3
.L2319:
	movl	$1, %edx
	movl	$7, %eax
.L2259:
	cmpb	%al, %cl
	je	.L2231
	cmpb	$6, %cl
	jne	.L2308
	testb	%dl, %dl
	jne	.L2231
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2263:
	movl	$3, %eax
	jmp	.L2262
	.p2align 4,,10
	.p2align 3
.L2264:
	xorl	%edx, %edx
	movl	$2, %eax
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L2265:
	movl	$1, %eax
	jmp	.L2262
	.p2align 4,,10
	.p2align 3
.L2266:
	testb	%cl, %cl
	je	.L2231
	xorl	%esi, %esi
	xorl	%eax, %eax
.L2267:
	leal	-7(%rcx), %edx
	andl	$253, %edx
	jne	.L2308
	testb	%sil, %sil
	jne	.L2231
	jmp	.L2308
.L2276:
	leaq	.LC11(%rip), %r8
.L2277:
	cmpb	$10, %cl
	ja	.L2278
	leaq	.L2280(%rip), %rdi
	movslq	(%rdi,%rcx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,comdat
	.align 4
	.align 4
.L2280:
	.long	.L2290-.L2280
	.long	.L2321-.L2280
	.long	.L2288-.L2280
	.long	.L2287-.L2280
	.long	.L2286-.L2280
	.long	.L2285-.L2280
	.long	.L2284-.L2280
	.long	.L2283-.L2280
	.long	.L2282-.L2280
	.long	.L2281-.L2280
	.long	.L2279-.L2280
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,comdat
.L2320:
	leaq	.LC1(%rip), %r8
	jmp	.L2277
	.p2align 4,,10
	.p2align 3
.L2223:
	cmpb	$123, %al
	jne	.L2220
	cmpb	$0, 83(%r14)
	movl	$5, %eax
	je	.L2220
	jmp	.L2221
	.p2align 4,,10
	.p2align 3
.L2215:
	movq	-120(%rbp), %rdx
	addq	$24, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm10WasmGlobalESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2216
	.p2align 4,,10
	.p2align 3
.L2351:
	movl	%ecx, %r8d
	leaq	.LC126(%rip), %rdx
	movl	%ebx, %ecx
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2231
	.p2align 4,,10
	.p2align 3
.L2217:
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r14), %rsi
	movq	%rsi, 16(%r14)
	subq	$1, %rsi
	jmp	.L2220
	.p2align 4,,10
	.p2align 3
.L2222:
	cmpb	$0, 87(%r14)
	movl	$7, %eax
	je	.L2220
	jmp	.L2221
	.p2align 4,,10
	.p2align 3
.L2224:
	cmpb	$0, 81(%r14)
	movl	$9, %eax
	je	.L2220
	jmp	.L2221
.L2258:
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2295:
	cmpb	$7, %al
	ja	.L2258
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,comdat
	.align 4
	.align 4
.L2298:
	.long	.L2258-.L2298
	.long	.L2322-.L2298
	.long	.L2297-.L2298
	.long	.L2322-.L2298
	.long	.L2297-.L2298
	.long	.L2299-.L2298
	.long	.L2297-.L2298
	.long	.L2297-.L2298
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv,comdat
.L2322:
	movl	$-4, %ecx
	movl	$4, %eax
.L2300:
	leal	-1(%rdi,%rax), %edi
	andl	%edi, %ecx
	movl	%ecx, 24(%rdx)
	leal	(%rcx,%rax), %edi
	jmp	.L2294
.L2297:
	movl	$-8, %ecx
	movl	$8, %eax
	jmp	.L2300
	.p2align 4,,10
	.p2align 3
.L2350:
	movq	%rdx, %r15
	xorl	%eax, %eax
	salq	$5, %r15
	testq	%rdx, %rdx
	je	.L2206
	movq	%r15, %rdi
	movq	%r8, -112(%rbp)
	call	_Znwm@PLT
	movq	32(%r13), %rsi
	movq	24(%r13), %rdi
	movq	-112(%rbp), %r8
.L2206:
	movq	%rax, %rcx
	movq	%rdi, %rdx
	cmpq	%rdi, %rsi
	je	.L2210
	.p2align 4,,10
	.p2align 3
.L2207:
	movdqu	(%rdx), %xmm2
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm2, -32(%rcx)
	movdqu	-16(%rdx), %xmm3
	movups	%xmm3, -16(%rcx)
	cmpq	%rsi, %rdx
	jne	.L2207
.L2210:
	testq	%rdi, %rdi
	je	.L2209
	movq	%rax, -128(%rbp)
	movq	%r8, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %rax
	movq	-112(%rbp), %r8
.L2209:
	leaq	(%rax,%r8), %rdx
	movq	%rax, 24(%r13)
	addq	%r15, %rax
	movq	%rdx, 32(%r13)
	movq	%rax, 40(%r13)
	jmp	.L2205
	.p2align 4,,10
	.p2align 3
.L2349:
	movl	%eax, %r8d
	movl	$1000000, %r9d
	movq	%r13, %rsi
	movq	%r14, %rdi
	leaq	.LC125(%rip), %rcx
	leaq	.LC59(%rip), %rdx
	xorl	%eax, %eax
	movl	$1000000, %r12d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2204
.L2299:
	movl	$-16, %ecx
	movl	$16, %eax
	jmp	.L2300
.L2233:
	leaq	.LC10(%rip), %r9
	jmp	.L2243
.L2244:
	leaq	.LC9(%rip), %r9
	jmp	.L2243
.L2241:
	leaq	.LC2(%rip), %r9
	jmp	.L2243
.L2240:
	leaq	.LC3(%rip), %r9
	jmp	.L2243
.L2239:
	leaq	.LC8(%rip), %r9
	jmp	.L2243
.L2238:
	leaq	.LC4(%rip), %r9
	jmp	.L2243
.L2237:
	leaq	.LC5(%rip), %r9
	jmp	.L2243
.L2236:
	leaq	.LC6(%rip), %r9
	jmp	.L2243
.L2235:
	leaq	.LC7(%rip), %r9
	jmp	.L2243
.L2268:
	leaq	.LC9(%rip), %r8
	jmp	.L2277
.L2269:
	leaq	.LC6(%rip), %r8
	jmp	.L2277
.L2275:
	leaq	.LC2(%rip), %r8
	jmp	.L2277
.L2274:
	leaq	.LC3(%rip), %r8
	jmp	.L2277
.L2273:
	leaq	.LC8(%rip), %r8
	jmp	.L2277
.L2272:
	leaq	.LC4(%rip), %r8
	jmp	.L2277
.L2271:
	leaq	.LC5(%rip), %r8
	jmp	.L2277
	.p2align 4,,10
	.p2align 3
.L2317:
	leaq	.LC1(%rip), %r8
	.p2align 4,,10
	.p2align 3
.L2256:
	leaq	.LC127(%rip), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2231
	.p2align 4,,10
	.p2align 3
.L2255:
	leaq	.LC11(%rip), %r8
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2254:
	leaq	.LC2(%rip), %r8
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2253:
	leaq	.LC3(%rip), %r8
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2249:
	leaq	.LC6(%rip), %r8
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2248:
	leaq	.LC7(%rip), %r8
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2246:
	leaq	.LC10(%rip), %r8
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2257:
	leaq	.LC9(%rip), %r8
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2252:
	leaq	.LC8(%rip), %r8
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2251:
	leaq	.LC4(%rip), %r8
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2250:
	leaq	.LC5(%rip), %r8
	jmp	.L2256
.L2321:
	leaq	.LC1(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L2289:
	leaq	.LC128(%rip), %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2231
.L2288:
	leaq	.LC11(%rip), %rcx
	jmp	.L2289
.L2284:
	leaq	.LC4(%rip), %rcx
	jmp	.L2289
.L2283:
	leaq	.LC5(%rip), %rcx
	jmp	.L2289
.L2287:
	leaq	.LC2(%rip), %rcx
	jmp	.L2289
.L2290:
	leaq	.LC9(%rip), %rcx
	jmp	.L2289
.L2281:
	leaq	.LC7(%rip), %rcx
	jmp	.L2289
.L2279:
	leaq	.LC10(%rip), %rcx
	jmp	.L2289
.L2286:
	leaq	.LC3(%rip), %rcx
	jmp	.L2289
.L2285:
	leaq	.LC8(%rip), %rcx
	jmp	.L2289
.L2282:
	leaq	.LC6(%rip), %rcx
	jmp	.L2289
.L2232:
	leaq	.LC0(%rip), %r9
	jmp	.L2243
.L2245:
	leaq	.LC0(%rip), %r8
	jmp	.L2256
.L2278:
	leaq	.LC0(%rip), %rcx
	jmp	.L2289
.L2354:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19024:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv, .-_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17DecodeDataSectionEv.str1.1,"aMS",@progbits,1
.LC129:
	.string	"data segments count"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17DecodeDataSectionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC130:
	.string	"cannot load data without memory"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17DecodeDataSectionEv.str1.1
.LC131:
	.string	"flag"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17DecodeDataSectionEv.str1.8
	.align 8
.LC132:
	.string	"Passive element segments require --experimental-wasm-bulk-memory"
	.align 8
.LC133:
	.string	"Element segments with table indices require --experimental-wasm-bulk-memory or --experimental-wasm-anyref"
	.align 8
.LC134:
	.string	"illegal flag value %u. Must be 0, 1, or 2"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl17DecodeDataSectionEv.str1.1
.LC135:
	.string	"memory index"
.LC136:
	.string	"illegal memory index %u != 0"
.LC137:
	.string	"source size"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl17DecodeDataSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeDataSectionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeDataSectionEv
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeDataSectionEv, @function
_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeDataSectionEv:
.LFB19033:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC129(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	movl	$0, -80(%rbp)
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movl	%eax, %r13d
	cmpl	$100000, %eax
	ja	.L2438
.L2357:
	movq	96(%r15), %r14
	testb	$16, 125(%r15)
	je	.L2358
	movl	76(%r14), %r8d
	cmpl	%r8d, %r13d
	jne	.L2439
.L2358:
	movq	160(%r14), %rdi
	movq	176(%r14), %rax
	movl	%r13d, %edx
	subq	%rdi, %rax
	sarq	$5, %rax
	cmpq	%rax, %rdx
	ja	.L2440
.L2360:
	cmpq	$0, 56(%r15)
	jne	.L2356
	leaq	-84(%rbp), %rax
	xorl	%ebx, %ebx
	movq	%rax, -112(%rbp)
	testl	%r13d, %r13d
	jne	.L2366
	jmp	.L2356
	.p2align 4,,10
	.p2align 3
.L2370:
	leaq	.LC131(%rip), %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	96(%r15), %r8
.L2381:
	movl	$1, %edx
	movq	%r8, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE
	cmpq	$0, 56(%r15)
	movl	%eax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jne	.L2356
.L2390:
	movq	24(%r15), %rax
.L2392:
	movq	16(%r15), %rsi
	cmpq	%rax, %rsi
	jb	.L2441
	leaq	.LC137(%rip), %rcx
	leaq	.LC40(%rip), %rdx
.L2437:
	movq	%r15, %rdi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%r15), %r8
.L2405:
	movq	96(%r15), %rax
	subq	8(%r15), %r8
	addl	32(%r15), %r8d
	movq	168(%rax), %rdx
	movq	176(%rax), %rsi
	leaq	160(%rax), %rdi
	testb	%r12b, %r12b
	je	.L2406
	cmpq	%rdx, %rsi
	je	.L2407
	movdqa	-80(%rbp), %xmm0
	movq	$0, 16(%rdx)
	movb	$1, 24(%rdx)
	movups	%xmm0, (%rdx)
	addq	$32, 168(%rax)
.L2408:
	movq	96(%r15), %rax
	movq	16(%r15), %rsi
	movq	168(%rax), %r12
	movq	24(%r15), %rax
	subq	%rsi, %rax
	cmpl	%eax, %r14d
	ja	.L2410
	movl	%r14d, %eax
	addq	%rax, %rsi
	movq	%rsi, 16(%r15)
.L2411:
	cmpq	$0, 56(%r15)
	jne	.L2356
	movl	%r8d, -16(%r12)
	addl	$1, %ebx
	movl	%r14d, -12(%r12)
	cmpq	$0, 56(%r15)
	jne	.L2356
	cmpl	%r13d, %ebx
	jnb	.L2356
.L2366:
	movq	96(%r15), %r8
	movq	16(%r15), %r14
	movzbl	18(%r8), %r12d
	testb	%r12b, %r12b
	je	.L2442
	movq	24(%r15), %rax
	movl	$0, -80(%rbp)
	cmpq	%rax, %r14
	jnb	.L2370
	movzbl	(%r14), %edi
	leaq	1(%r14), %rsi
	movl	%edi, %ecx
	andl	$127, %ecx
	testb	%dil, %dil
	js	.L2443
.L2371:
	movq	%rsi, 16(%r15)
.L2414:
	cmpl	$1, %ecx
	je	.L2444
	cmpl	$2, %ecx
	je	.L2445
	testl	%ecx, %ecx
	je	.L2381
	leaq	.LC134(%rip), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L2385:
	cmpq	$0, 56(%r15)
	jne	.L2356
	testb	%r12b, %r12b
	je	.L2390
.L2389:
	movl	-100(%rbp), %eax
	testl	%eax, %eax
	je	.L2390
	movl	-100(%rbp), %ecx
	leaq	.LC136(%rip), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	.p2align 4,,10
	.p2align 3
.L2356:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2446
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2406:
	.cfi_restore_state
	cmpq	%rdx, %rsi
	je	.L2409
	movl	$0, (%rdx)
	movq	$0, 16(%rdx)
	movb	$0, 24(%rdx)
	addq	$32, 168(%rax)
	jmp	.L2408
	.p2align 4,,10
	.p2align 3
.L2441:
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %r8
	movl	%edx, %r14d
	andl	$127, %r14d
	testb	%dl, %dl
	js	.L2447
.L2394:
	movq	%r8, 16(%r15)
	jmp	.L2405
	.p2align 4,,10
	.p2align 3
.L2443:
	cmpq	%rsi, %rax
	jbe	.L2378
	movzbl	1(%r14), %edi
	movl	%edi, %esi
	sall	$7, %esi
	andl	$16256, %esi
	orl	%esi, %ecx
	leaq	2(%r14), %rsi
	testb	%dil, %dil
	jns	.L2371
	cmpq	%rsi, %rax
	jbe	.L2378
	movzbl	2(%r14), %edi
	movl	%edi, %esi
	sall	$14, %esi
	andl	$2080768, %esi
	orl	%esi, %ecx
	leaq	3(%r14), %rsi
	testb	%dil, %dil
	jns	.L2371
	cmpq	%rsi, %rax
	jbe	.L2378
	movzbl	3(%r14), %edi
	movl	%edi, %esi
	sall	$21, %esi
	andl	$266338304, %esi
	orl	%esi, %ecx
	leaq	4(%r14), %rsi
	testb	%dil, %dil
	jns	.L2371
	cmpq	%rsi, %rax
	jbe	.L2378
	movzbl	4(%r14), %edi
	leaq	5(%r14), %r11
	movq	%r11, 16(%r15)
	movl	%edi, %r11d
	andl	$-16, %r11d
	testb	%dil, %dil
	js	.L2448
	sall	$28, %edi
	orl	%edi, %ecx
	testb	%r11b, %r11b
	je	.L2414
.L2382:
	leaq	.LC41(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	96(%r15), %r8
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2447:
	cmpq	%rax, %r8
	jnb	.L2395
	movzbl	1(%rsi), %edi
	leaq	2(%rsi), %r8
	movl	%edi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r14d
	testb	%dil, %dil
	jns	.L2394
	cmpq	%rax, %r8
	jnb	.L2395
	movzbl	2(%rsi), %edi
	leaq	3(%rsi), %r8
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r14d
	testb	%dil, %dil
	jns	.L2394
	cmpq	%rax, %r8
	jnb	.L2395
	movzbl	3(%rsi), %edi
	leaq	4(%rsi), %r8
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r14d
	testb	%dil, %dil
	jns	.L2394
	cmpq	%rax, %r8
	jnb	.L2401
	movzbl	4(%rsi), %eax
	addq	$5, %rsi
	movq	%rsi, 16(%r15)
	movl	%eax, %ecx
	andl	$-16, %ecx
	movb	%cl, -128(%rbp)
	testb	%al, %al
	js	.L2402
	sall	$28, %eax
	orl	%eax, %r14d
.L2403:
	cmpb	$0, -128(%rbp)
	je	.L2415
	movq	%r8, %rsi
	leaq	.LC41(%rip), %rdx
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	16(%r15), %r8
	jmp	.L2405
	.p2align 4,,10
	.p2align 3
.L2444:
	cmpb	$0, 89(%r15)
	je	.L2449
	cmpq	$0, 56(%r15)
	jne	.L2356
	xorl	%r12d, %r12d
	jmp	.L2392
	.p2align 4,,10
	.p2align 3
.L2445:
	cmpb	$0, 89(%r15)
	jne	.L2387
	cmpb	$0, 87(%r15)
	je	.L2450
.L2387:
	movq	16(%r15), %rsi
	movq	-112(%rbp), %rdx
	movq	%r15, %rdi
	leaq	.LC135(%rip), %rcx
	movl	$0, -84(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	96(%r15), %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	movl	%eax, -100(%rbp)
	call	_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE
	cmpq	$0, 56(%r15)
	movl	%eax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	je	.L2389
	jmp	.L2356
	.p2align 4,,10
	.p2align 3
.L2378:
	movq	%rsi, 16(%r15)
	leaq	.LC131(%rip), %rcx
	movq	%r15, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	96(%r15), %r8
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2395:
	movq	%r8, 16(%r15)
	leaq	.LC137(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	movq	%r8, %rsi
	jmp	.L2437
	.p2align 4,,10
	.p2align 3
.L2410:
	xorl	%eax, %eax
	movl	%r14d, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r15, %rdi
	movl	%r8d, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r15), %rax
	movl	-128(%rbp), %r8d
	movq	%rax, 16(%r15)
	jmp	.L2411
	.p2align 4,,10
	.p2align 3
.L2409:
	movl	%r8d, -128(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movl	-128(%rbp), %r8d
	jmp	.L2408
	.p2align 4,,10
	.p2align 3
.L2407:
	movq	-120(%rbp), %rdx
	movl	%r8d, -128(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm15WasmDataSegmentESaIS3_EE17_M_realloc_insertIJRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	movl	-128(%rbp), %r8d
	jmp	.L2408
	.p2align 4,,10
	.p2align 3
.L2449:
	leaq	.LC132(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L2385
	.p2align 4,,10
	.p2align 3
.L2440:
	movq	168(%r14), %rsi
	movq	%rdx, %r8
	xorl	%eax, %eax
	salq	$5, %r8
	movq	%rsi, %rbx
	subq	%rdi, %rbx
	testq	%rdx, %rdx
	je	.L2361
	movq	%r8, %rdi
	movq	%r8, -112(%rbp)
	call	_Znwm@PLT
	movq	168(%r14), %rsi
	movq	160(%r14), %rdi
	movq	-112(%rbp), %r8
.L2361:
	movq	%rax, %rcx
	movq	%rdi, %rdx
	cmpq	%rsi, %rdi
	je	.L2365
	.p2align 4,,10
	.p2align 3
.L2362:
	movdqu	(%rdx), %xmm1
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm1, -32(%rcx)
	movdqu	-16(%rdx), %xmm2
	movups	%xmm2, -16(%rcx)
	cmpq	%rsi, %rdx
	jne	.L2362
.L2365:
	testq	%rdi, %rdi
	je	.L2364
	movq	%r8, -128(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r8
	movq	-112(%rbp), %rax
.L2364:
	addq	%rax, %rbx
	movq	%rax, 160(%r14)
	addq	%r8, %rax
	movq	%rbx, 168(%r14)
	movq	%rax, 176(%r14)
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2438:
	movl	%eax, %r8d
	movl	$100000, %r9d
	movq	%r14, %rsi
	movq	%r15, %rdi
	leaq	.LC129(%rip), %rcx
	leaq	.LC59(%rip), %rdx
	xorl	%eax, %eax
	movl	$100000, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2357
	.p2align 4,,10
	.p2align 3
.L2439:
	movq	16(%r15), %rsi
	movl	%r13d, %ecx
	leaq	.LC102(%rip), %rdx
	xorl	%eax, %eax
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2356
	.p2align 4,,10
	.p2align 3
.L2450:
	leaq	.LC133(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L2385
	.p2align 4,,10
	.p2align 3
.L2442:
	leaq	.LC130(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L2356
.L2401:
	movq	%r8, 16(%r15)
	leaq	.LC137(%rip), %rcx
	movq	%r8, %rsi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L2415:
	movq	16(%r15), %r8
	jmp	.L2405
.L2448:
	xorl	%eax, %eax
	leaq	.LC131(%rip), %rcx
	movq	%r15, %rdi
	movq	%rsi, -128(%rbp)
	leaq	.LC40(%rip), %rdx
	movb	%r11b, -136(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movzbl	-136(%rbp), %r11d
	movq	-128(%rbp), %rsi
	testb	%r11b, %r11b
	jne	.L2382
	movq	96(%r15), %r8
	jmp	.L2381
.L2402:
	movq	%r8, %rsi
	leaq	.LC137(%rip), %rcx
	movq	%r15, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, -136(%rbp)
	xorl	%r14d, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-136(%rbp), %r8
	jmp	.L2403
.L2446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19033:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeDataSectionEv, .-_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeDataSectionEv
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv.str1.1,"aMS",@progbits,1
.LC138:
	.string	"y"
.LC139:
	.string	"ies"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC140:
	.string	"The element section requires a table"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv.str1.1
.LC141:
	.string	"table index"
.LC142:
	.string	"out of bounds table index %u"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv.str1.8
	.align 8
.LC143:
	.string	"Invalid element segment. Table %u is not of type FuncRef"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv.str1.1
.LC144:
	.string	"invalid element segment type"
.LC145:
	.string	"number of elements"
.LC146:
	.string	"element function index"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv.str1.8
	.align 8
.LC147:
	.string	"%s %u out of bounds (%d entr%s)"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv.str1.1
.LC148:
	.string	"invalid opcode in element"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv, @function
_ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv:
.LFB19028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC75(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movl	_ZN2v88internal24FLAG_wasm_max_table_sizeE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	movl	$0, -80(%rbp)
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movl	%eax, -124(%rbp)
	movl	%eax, %eax
	cmpq	%rax, %r13
	jb	.L2585
.L2452:
	movl	-124(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L2451
	movq	96(%r14), %rax
	movq	192(%rax), %rbx
	cmpq	%rbx, 184(%rax)
	je	.L2586
.L2455:
	cmpq	$0, 56(%r14)
	jne	.L2451
	movl	$0, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L2549:
	movq	16(%r14), %r12
	movq	24(%r14), %rax
	movl	$0, -80(%rbp)
	cmpq	%rax, %r12
	jb	.L2587
	leaq	.LC131(%rip), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L2468:
	movl	$0, -88(%rbp)
.L2581:
	movq	96(%r14), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl17consume_init_exprEPNS1_10WasmModuleENS1_9ValueTypeE
	cmpq	$0, 56(%r14)
	movl	%eax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jne	.L2451
	movq	96(%r14), %rax
	movl	-88(%rbp), %edx
	movq	184(%rax), %rcx
	movq	192(%rax), %rax
	movq	%rdx, %r8
	subq	%rcx, %rax
	sarq	$4, %rax
	cmpq	%rax, %rdx
	jnb	.L2588
	salq	$4, %rdx
	movzbl	(%rcx,%rdx), %eax
	subl	$6, %eax
	cmpb	$1, %al
	ja	.L2589
	movb	$1, -105(%rbp)
.L2483:
	call	_ZN2v88internal4wasm22max_table_init_entriesEv@PLT
	movq	16(%r14), %rsi
	movl	%eax, %ebx
	movq	24(%r14), %rax
	cmpq	%rax, %rsi
	jb	.L2590
	leaq	.LC145(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$0, -104(%rbp)
.L2497:
	movq	96(%r14), %rdx
	cmpb	$0, -105(%rbp)
	movq	288(%rdx), %rax
	movq	296(%rdx), %rsi
	leaq	280(%rdx), %rdi
	je	.L2499
	cmpq	%rax, %rsi
	je	.L2500
	movdqa	-80(%rbp), %xmm0
	movl	-88(%rbp), %ecx
	movb	$1, 48(%rax)
	movq	$0, 40(%rax)
	movups	%xmm0, 8(%rax)
	pxor	%xmm0, %xmm0
	movl	%ecx, (%rax)
	movups	%xmm0, 24(%rax)
	addq	$56, 288(%rdx)
.L2501:
	movq	96(%r14), %rax
	movl	-104(%rbp), %edi
	xorl	%r15d, %r15d
	movq	288(%rax), %rbx
	testl	%edi, %edi
	je	.L2547
	.p2align 4,,10
	.p2align 3
.L2503:
	cmpb	$0, -105(%rbp)
	movq	24(%r14), %rax
	movq	16(%r14), %r12
	je	.L2505
	movq	96(%r14), %r8
	cmpq	%r12, %rax
	ja	.L2591
	movq	%r8, -120(%rbp)
	leaq	.LC146(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rsi
.L2583:
	movq	%r14, %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-120(%rbp), %r8
	xorl	%edx, %edx
.L2518:
	movq	144(%r8), %rax
	subq	136(%r8), %rax
	movq	%rax, %r9
	sarq	$5, %r9
	cmpq	%rdx, %r9
	jbe	.L2519
.L2584:
	movq	56(%r14), %rax
.L2520:
	movl	%r13d, -84(%rbp)
	testq	%rax, %rax
	jne	.L2451
	movq	-24(%rbx), %rsi
	cmpq	-16(%rbx), %rsi
	je	.L2546
	movl	%r13d, (%rsi)
	addl	$1, %r15d
	addq	$4, -24(%rbx)
	cmpl	%r15d, -104(%rbp)
	jne	.L2503
.L2547:
	addl	$1, -112(%rbp)
	movq	56(%r14), %rax
	movl	-112(%rbp), %ecx
	cmpl	-124(%rbp), %ecx
	jnb	.L2451
	testq	%rax, %rax
	je	.L2549
	jmp	.L2451
	.p2align 4,,10
	.p2align 3
.L2505:
	cmpl	%r12d, %eax
	je	.L2522
	movq	56(%r14), %r8
	leaq	1(%r12), %r10
	movzbl	(%r12), %edx
	movq	%r10, 16(%r14)
	testq	%r8, %r8
	jne	.L2451
	cmpb	$-48, %dl
	je	.L2565
	cmpb	$-46, %dl
	jne	.L2525
	movq	96(%r14), %r9
	cmpq	%rax, %r10
	jb	.L2592
	movq	%r10, %rsi
	leaq	.LC146(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r9, -144(%rbp)
	xorl	%r13d, %r13d
	movq	%r8, -136(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-120(%rbp), %r10
	movq	-136(%rbp), %r8
	movq	-144(%rbp), %r9
.L2539:
	movq	144(%r9), %rdx
	subq	136(%r9), %rdx
	movq	%rdx, %r9
	sarq	$5, %r9
	cmpq	%r8, %r9
	jbe	.L2556
	cmpq	$0, 56(%r14)
	je	.L2593
	.p2align 4,,10
	.p2align 3
.L2451:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2594
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2591:
	.cfi_restore_state
	movzbl	(%r12), %ecx
	leaq	1(%r12), %rsi
	movl	%ecx, %edx
	movl	%ecx, %r13d
	andl	$127, %edx
	andl	$127, %r13d
	testb	%cl, %cl
	js	.L2595
	movq	%rsi, 16(%r14)
	movzbl	%dl, %edx
	jmp	.L2518
	.p2align 4,,10
	.p2align 3
.L2519:
	cmpq	$32, %rax
	movl	%r13d, %r8d
	leaq	.LC138(%rip), %rdi
	movq	%r12, %rsi
	leaq	.LC139(%rip), %rax
	leaq	.LC146(%rip), %rcx
	cmove	%rdi, %rax
	subq	$8, %rsp
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	leaq	.LC147(%rip), %rdx
	pushq	%rax
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	popq	%rcx
	movq	56(%r14), %rax
	popq	%rsi
	jmp	.L2520
	.p2align 4,,10
	.p2align 3
.L2546:
	leaq	-84(%rbp), %rdx
	leaq	-32(%rbx), %rdi
	addl	$1, %r15d
	call	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	cmpl	-104(%rbp), %r15d
	jne	.L2503
	jmp	.L2547
	.p2align 4,,10
	.p2align 3
.L2565:
	movl	$-1, %r13d
.L2526:
	cmpl	%r10d, %eax
	je	.L2542
.L2596:
	movzbl	(%r10), %r9d
	leaq	1(%r10), %rax
	movq	%rax, 16(%r14)
	cmpb	$11, %r9b
	je	.L2584
.L2544:
	movl	$11, %r8d
	movq	%r10, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC123(%rip), %rcx
	leaq	.LC61(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2584
	.p2align 4,,10
	.p2align 3
.L2522:
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r14), %rax
	cmpq	$0, 56(%r14)
	movq	%rax, 16(%r14)
	jne	.L2451
.L2525:
	leaq	.LC148(%rip), %rsi
	movq	%r14, %rdi
	movl	$-1, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%r14), %r10
	movq	24(%r14), %rax
	cmpl	%r10d, %eax
	jne	.L2596
.L2542:
	movq	%r10, %rsi
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%r14, %rdi
	leaq	.LC39(%rip), %rdx
	movq	%r10, -120(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r14), %rax
	movq	-120(%rbp), %r10
	xorl	%r9d, %r9d
	movq	%rax, 16(%r14)
	jmp	.L2544
	.p2align 4,,10
	.p2align 3
.L2595:
	cmpq	%rax, %rsi
	jnb	.L2508
	movzbl	1(%r12), %ecx
	leaq	2(%r12), %rsi
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r13d
	testb	%cl, %cl
	js	.L2597
.L2509:
	movq	%rsi, 16(%r14)
.L2582:
	movl	%r13d, %edx
	jmp	.L2518
	.p2align 4,,10
	.p2align 3
.L2592:
	movzbl	1(%r12), %edx
	leaq	2(%r12), %rsi
	movl	%edx, %r13d
	andl	$127, %r13d
	testb	%dl, %dl
	js	.L2598
.L2528:
	movq	%rsi, 16(%r14)
	movq	144(%r9), %rdx
	movl	%r13d, %ecx
	subq	136(%r9), %rdx
	movq	%rdx, %r9
	sarq	$5, %r9
	cmpq	%rcx, %r9
	jbe	.L2556
	movq	%rsi, %r10
	jmp	.L2526
	.p2align 4,,10
	.p2align 3
.L2597:
	cmpq	%rax, %rsi
	jnb	.L2508
	movzbl	2(%r12), %ecx
	leaq	3(%r12), %rsi
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r13d
	testb	%cl, %cl
	jns	.L2509
	cmpq	%rax, %rsi
	jb	.L2599
	.p2align 4,,10
	.p2align 3
.L2508:
	movq	%r8, -120(%rbp)
	leaq	.LC146(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	movq	%rsi, 16(%r14)
	jmp	.L2583
	.p2align 4,,10
	.p2align 3
.L2587:
	movzbl	(%r12), %edx
	leaq	1(%r12), %rsi
	movl	%edx, %ecx
	andl	$127, %ecx
	testb	%dl, %dl
	js	.L2600
.L2458:
	movq	%rsi, 16(%r14)
.L2554:
	cmpl	$1, %ecx
	jne	.L2470
	cmpb	$0, 89(%r14)
	je	.L2601
	cmpq	$0, 56(%r14)
	jne	.L2451
.L2553:
	cmpl	%esi, %eax
	je	.L2484
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%r14)
	cmpb	$111, %al
	je	.L2485
	cmpb	$112, %al
	jne	.L2486
.L2561:
	movb	$0, -105(%rbp)
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L2499:
	cmpq	%rax, %rsi
	je	.L2502
	pxor	%xmm0, %xmm0
	movl	$0, (%rax)
	movl	$0, 8(%rax)
	movq	$0, 40(%rax)
	movb	$0, 48(%rax)
	movups	%xmm0, 24(%rax)
	addq	$56, 288(%rdx)
	jmp	.L2501
	.p2align 4,,10
	.p2align 3
.L2590:
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %r8
	movl	%edx, %edi
	andl	$127, %edi
	movl	%edi, -104(%rbp)
	testb	%dl, %dl
	js	.L2602
.L2488:
	movq	%r8, 16(%r14)
.L2552:
	movl	%ebx, %r9d
	cmpl	%ebx, -104(%rbp)
	jbe	.L2497
	movl	-104(%rbp), %r8d
	leaq	.LC145(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC59(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	%ebx, -104(%rbp)
	jmp	.L2497
	.p2align 4,,10
	.p2align 3
.L2556:
	cmpq	$32, %rdx
	movl	%r13d, %r8d
	movq	%r10, %rsi
	movq	%r14, %rdi
	leaq	.LC138(%rip), %rcx
	leaq	.LC139(%rip), %rax
	cmove	%rcx, %rax
	subq	$8, %rsp
	leaq	.LC147(%rip), %rdx
	xorl	%r13d, %r13d
	leaq	.LC146(%rip), %rcx
	pushq	%rax
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	cmpq	$0, 56(%r14)
	popq	%rax
	popq	%rdx
	jne	.L2451
.L2593:
	movq	16(%r14), %r10
	movq	24(%r14), %rax
	jmp	.L2526
	.p2align 4,,10
	.p2align 3
.L2470:
	cmpl	$2, %ecx
	jne	.L2603
	cmpb	$0, 89(%r14)
	jne	.L2475
	cmpb	$0, 87(%r14)
	je	.L2604
.L2475:
	leaq	-84(%rbp), %rdx
	leaq	.LC141(%rip), %rcx
	movq	%r14, %rdi
	movl	$0, -84(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movl	%eax, -88(%rbp)
	jmp	.L2581
	.p2align 4,,10
	.p2align 3
.L2598:
	cmpq	%rsi, %rax
	jbe	.L2529
	movzbl	2(%r12), %ecx
	leaq	3(%r12), %rsi
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r13d
	testb	%cl, %cl
	jns	.L2528
	cmpq	%rax, %rsi
	jnb	.L2529
	movzbl	3(%r12), %ecx
	leaq	4(%r12), %rsi
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r13d
	testb	%cl, %cl
	jns	.L2528
	cmpq	%rax, %rsi
	jnb	.L2529
	movzbl	4(%r12), %ecx
	leaq	5(%r12), %rsi
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r13d
	testb	%cl, %cl
	jns	.L2528
	cmpq	%rax, %rsi
	jnb	.L2535
	movzbl	5(%r12), %eax
	addq	$6, %r12
	movq	%r12, 16(%r14)
	testb	%al, %al
	js	.L2536
	movl	%eax, %edx
	andl	$-16, %eax
	sall	$28, %edx
	movl	%eax, %r12d
	orl	%edx, %r13d
.L2537:
	testb	%r12b, %r12b
	je	.L2557
	leaq	.LC41(%rip), %rdx
	movq	%r14, %rdi
	movq	%r8, -120(%rbp)
	xorl	%r13d, %r13d
	movq	%r10, -144(%rbp)
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %r9
	movq	-120(%rbp), %r8
	jmp	.L2539
	.p2align 4,,10
	.p2align 3
.L2529:
	movq	%rsi, 16(%r14)
	leaq	.LC146(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r10, -144(%rbp)
	xorl	%r13d, %r13d
	movq	%r9, -136(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-120(%rbp), %r8
	movq	-136(%rbp), %r9
	movq	-144(%rbp), %r10
	jmp	.L2539
	.p2align 4,,10
	.p2align 3
.L2600:
	cmpq	%rsi, %rax
	jbe	.L2465
	movzbl	1(%r12), %edi
	leaq	2(%r12), %rsi
	movl	%edi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L2458
	cmpq	%rsi, %rax
	jbe	.L2465
	movzbl	2(%r12), %edi
	leaq	3(%r12), %rsi
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L2458
	cmpq	%rsi, %rax
	jbe	.L2465
	movzbl	3(%r12), %edi
	leaq	4(%r12), %rsi
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L2458
	cmpq	%rsi, %rax
	jbe	.L2465
	movzbl	4(%r12), %edx
	leaq	5(%r12), %rdi
	movq	%rdi, 16(%r14)
	movl	%edx, %ebx
	andl	$-16, %ebx
	testb	%dl, %dl
	js	.L2605
	sall	$28, %edx
	orl	%edx, %ecx
	testb	%bl, %bl
	je	.L2606
.L2469:
	leaq	.LC41(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2468
	.p2align 4,,10
	.p2align 3
.L2465:
	movq	%rsi, 16(%r14)
	leaq	.LC131(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2468
	.p2align 4,,10
	.p2align 3
.L2602:
	cmpq	%r8, %rax
	jbe	.L2489
	movzbl	1(%rsi), %ecx
	leaq	2(%rsi), %r8
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %edi
	movl	%edi, -104(%rbp)
	testb	%cl, %cl
	jns	.L2488
	cmpq	%r8, %rax
	jbe	.L2489
	movzbl	2(%rsi), %ecx
	leaq	3(%rsi), %r8
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %edi
	movl	%edi, -104(%rbp)
	testb	%cl, %cl
	jns	.L2488
	cmpq	%r8, %rax
	jbe	.L2489
	movzbl	3(%rsi), %ecx
	leaq	4(%rsi), %r13
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %edi
	movl	%edi, -104(%rbp)
	testb	%cl, %cl
	js	.L2607
	movq	%r13, 16(%r14)
	jmp	.L2552
	.p2align 4,,10
	.p2align 3
.L2489:
	movq	%r8, 16(%r14)
	leaq	.LC145(%rip), %rcx
	movq	%r8, %rsi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$0, -104(%rbp)
	jmp	.L2497
.L2484:
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r14), %rsi
	movq	%rsi, 16(%r14)
	subq	$1, %rsi
.L2486:
	movq	%r14, %rdi
	leaq	.LC74(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	16(%r14), %rax
	leaq	.LC144(%rip), %rdx
	movq	%r14, %rdi
	leaq	-1(%rax), %rsi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2451
	.p2align 4,,10
	.p2align 3
.L2485:
	movzbl	87(%r14), %eax
	movb	%al, -105(%rbp)
	testb	%al, %al
	jne	.L2561
	leaq	.LC72(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2483
.L2500:
	movq	-152(%rbp), %rcx
	leaq	-88(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJRjRNS2_12WasmInitExprEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2501
.L2502:
	call	_ZNSt6vectorIN2v88internal4wasm15WasmElemSegmentESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2501
.L2601:
	leaq	.LC132(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
.L2472:
	cmpq	$0, 56(%r14)
	jne	.L2451
	movq	24(%r14), %rax
	movq	16(%r14), %rsi
	jmp	.L2553
	.p2align 4,,10
	.p2align 3
.L2589:
	movl	%r8d, %ecx
	leaq	.LC143(%rip), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2451
.L2599:
	movzbl	3(%r12), %ecx
	leaq	4(%r12), %rsi
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r13d
	testb	%cl, %cl
	jns	.L2509
	cmpq	%rax, %rsi
	jnb	.L2514
	movzbl	4(%r12), %eax
	leaq	5(%r12), %rdx
	movq	%rdx, 16(%r14)
	movl	%eax, %ecx
	andl	$-16, %ecx
	movb	%cl, -120(%rbp)
	testb	%al, %al
	js	.L2515
	sall	$28, %eax
	orl	%eax, %r13d
.L2516:
	cmpb	$0, -120(%rbp)
	je	.L2582
	leaq	.LC41(%rip), %rdx
	movq	%r14, %rdi
	movq	%r8, -120(%rbp)
	xorl	%r13d, %r13d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-120(%rbp), %r8
	xorl	%edx, %edx
	jmp	.L2518
.L2604:
	leaq	.LC133(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	jmp	.L2472
.L2585:
	movl	-124(%rbp), %r8d
	movq	%r13, %r9
	leaq	.LC75(%rip), %rcx
	xorl	%eax, %eax
	leaq	.LC59(%rip), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	%r13d, -124(%rbp)
	jmp	.L2452
.L2514:
	movq	%rsi, 16(%r14)
	leaq	.LC146(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, -120(%rbp)
	xorl	%r13d, %r13d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-120(%rbp), %r8
	jmp	.L2582
.L2586:
	movq	16(%r14), %rsi
	leaq	.LC140(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2455
.L2588:
	movl	%edx, %ecx
	movq	%r12, %rsi
	leaq	.LC142(%rip), %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2451
.L2535:
	movq	%rsi, 16(%r14)
	leaq	.LC146(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r10, -136(%rbp)
	xorl	%r13d, %r13d
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-120(%rbp), %r9
	movq	-136(%rbp), %r10
.L2557:
	movl	%r13d, %r8d
	jmp	.L2539
.L2607:
	cmpq	%r13, %rax
	jbe	.L2495
	movzbl	4(%rsi), %eax
	leaq	5(%rsi), %rdx
	movq	%rdx, 16(%r14)
	movl	%eax, %r12d
	andl	$-16, %r12d
	testb	%al, %al
	js	.L2608
	sall	$28, %eax
	orl	%eax, -104(%rbp)
	testb	%r12b, %r12b
	je	.L2552
.L2498:
	leaq	.LC41(%rip), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movl	$0, -104(%rbp)
	jmp	.L2497
.L2515:
	leaq	.LC146(%rip), %rcx
	movq	%r14, %rdi
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC40(%rip), %rdx
	movq	%r8, -144(%rbp)
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-144(%rbp), %r8
	movq	-136(%rbp), %rsi
	jmp	.L2516
.L2495:
	movq	%r13, 16(%r14)
	leaq	.LC145(%rip), %rcx
	movq	%r13, %rsi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$0, -104(%rbp)
	jmp	.L2497
.L2536:
	andl	$-16, %eax
	leaq	.LC146(%rip), %rcx
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	movl	%eax, %r12d
	leaq	.LC40(%rip), %rdx
	xorl	%eax, %eax
	movq	%r10, -160(%rbp)
	movq	%r9, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-160(%rbp), %r10
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %r8
	movq	-120(%rbp), %rsi
	jmp	.L2537
.L2608:
	leaq	.LC145(%rip), %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$0, -104(%rbp)
	testb	%r12b, %r12b
	jne	.L2498
	jmp	.L2497
.L2606:
	movq	%rdi, %rsi
	jmp	.L2554
.L2605:
	xorl	%eax, %eax
	leaq	.LC131(%rip), %rcx
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	testb	%bl, %bl
	movq	-104(%rbp), %rsi
	jne	.L2469
	jmp	.L2468
.L2603:
	testl	%ecx, %ecx
	je	.L2468
	leaq	.LC134(%rip), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2472
.L2594:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19028:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv, .-_ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv
	.section	.text._ZN2v88internal4wasm28DecodeWasmInitExprForTestingERKNS1_12WasmFeaturesEPKhS6_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm28DecodeWasmInitExprForTestingERKNS1_12WasmFeaturesEPKhS6_
	.type	_ZN2v88internal4wasm28DecodeWasmInitExprForTestingERKNS1_12WasmFeaturesEPKhS6_, @function
_ZN2v88internal4wasm28DecodeWasmInitExprForTestingERKNS1_12WasmFeaturesEPKhS6_:
.LFB19132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r15
	leaq	-88(%rbp), %r14
	pushq	%r13
	leaq	-240(%rbp), %rdi
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	(%rax), %rcx
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	leaq	16+_ZTVN2v88internal19AccountingAllocatorE(%rip), %rbx
	movq	%rsi, -232(%rbp)
	movq	%rcx, -160(%rbp)
	movl	8(%rax), %ecx
	movzbl	12(%rax), %eax
	movq	%rbx, -304(%rbp)
	leaq	16+_ZTVN2v88internal4wasm17ModuleDecoderImplE(%rip), %rbx
	movq	%rsi, -224(%rbp)
	movb	%al, -148(%rbp)
	movq	%rdx, -216(%rbp)
	movl	$0, -208(%rbp)
	movl	$0, -200(%rbp)
	movq	%r15, -192(%rbp)
	movq	$0, -184(%rbp)
	movb	$0, -176(%rbp)
	movq	%rbx, -240(%rbp)
	movl	%ecx, -152(%rbp)
	movq	$0, -128(%rbp)
	movb	$1, -120(%rbp)
	movq	$0, -116(%rbp)
	movq	%r14, -104(%rbp)
	movq	$0, -96(%rbp)
	movups	%xmm0, -296(%rbp)
	movaps	%xmm0, -144(%rbp)
	movb	$0, -88(%rbp)
	movzbl	_ZN2v88internal24FLAG_assume_asmjs_originE(%rip), %eax
	movb	%al, -72(%rbp)
	cmpq	%rdx, %rsi
	ja	.L2673
	movq	%r12, -224(%rbp)
	cmpl	%r12d, %edx
	je	.L2611
.L2677:
	movzbl	(%r12), %eax
	leaq	1(%r12), %rsi
	movq	%rsi, -224(%rbp)
	cmpb	$68, %al
	ja	.L2612
	cmpb	$34, %al
	jbe	.L2613
	subl	$35, %eax
	cmpb	$33, %al
	ja	.L2613
	leaq	.L2615(%rip), %rcx
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm28DecodeWasmInitExprForTestingERKNS1_12WasmFeaturesEPKhS6_,"a",@progbits
	.align 4
	.align 4
.L2615:
	.long	.L2619-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2613-.L2615
	.long	.L2618-.L2615
	.long	.L2617-.L2615
	.long	.L2616-.L2615
	.long	.L2614-.L2615
	.section	.text._ZN2v88internal4wasm28DecodeWasmInitExprForTestingERKNS1_12WasmFeaturesEPKhS6_
	.p2align 4,,10
	.p2align 3
.L2612:
	cmpb	$-48, %al
	je	.L2620
	cmpb	$-46, %al
	jne	.L2613
	cmpb	$0, -153(%rbp)
	jne	.L2674
.L2613:
	leaq	.LC122(%rip), %rsi
	movq	%rdi, -312(%rbp)
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdx
	movq	-312(%rbp), %rdi
.L2655:
	movq	%rsi, -224(%rbp)
	cmpl	%esi, %edx
	je	.L2642
	movzbl	(%rsi), %r9d
	leaq	1(%rsi), %rax
	movq	%rax, -224(%rbp)
	cmpb	$11, %r9b
	jne	.L2645
.L2644:
	movq	-104(%rbp), %rdi
	movq	%rbx, -240(%rbp)
	andl	$7, %r12d
	cmpq	%r14, %rdi
	je	.L2646
	call	_ZdlPv@PLT
.L2646:
	movq	-136(%rbp), %r14
	testq	%r14, %r14
	je	.L2648
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2649
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L2675
	.p2align 4,,10
	.p2align 3
.L2648:
	movq	-192(%rbp), %rdi
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rax, -240(%rbp)
	cmpq	%r15, %rdi
	je	.L2654
	call	_ZdlPv@PLT
.L2654:
	leaq	-304(%rbp), %rdi
	call	_ZN2v88internal19AccountingAllocatorD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2676
	addq	$296, %rsp
	movl	%r12d, %eax
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2641:
	.cfi_restore_state
	movl	%r8d, %ecx
	leaq	.LC39(%rip), %rdx
	xorl	%eax, %eax
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-216(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movq	%rsi, -224(%rbp)
.L2642:
	xorl	%eax, %eax
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%rsi, -320(%rbp)
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-216(%rbp), %rax
	movq	-320(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	-312(%rbp), %rdi
	movq	%rax, -224(%rbp)
.L2645:
	movl	$11, %r8d
	leaq	.LC123(%rip), %rcx
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	leaq	.LC61(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	jmp	.L2644
	.p2align 4,,10
	.p2align 3
.L2673:
	leaq	.LC100(%rip), %rdx
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-232(%rbp), %rdx
	movq	%r12, -224(%rbp)
	movq	-312(%rbp), %rdi
	movq	%rdx, -216(%rbp)
	cmpl	%r12d, %edx
	jne	.L2677
.L2611:
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%r12, %rsi
	movq	%rdi, -312(%rbp)
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.0
	movq	-216(%rbp), %rax
	movq	-312(%rbp), %rdi
	movq	%rax, -224(%rbp)
	jmp	.L2613
	.p2align 4,,10
	.p2align 3
.L2649:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L2648
.L2675:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L2652
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L2653:
	cmpl	$1, %eax
	jne	.L2648
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L2648
.L2614:
	cmpq	%rdx, %rsi
	ja	.L2638
	movq	%rdx, %rcx
	subq	%rsi, %rcx
	movl	%ecx, %eax
	cmpl	$7, %ecx
	jbe	.L2638
	movsd	1(%r12), %xmm0
.L2639:
	movq	%xmm0, %r13
	movl	$5, %r12d
	movl	$8, %r8d
.L2635:
	cmpl	%eax, %r8d
	ja	.L2641
	addq	%r8, %rsi
	jmp	.L2655
.L2619:
	leaq	-256(%rbp), %rdx
	xorl	%r8d, %r8d
	leaq	.LC115(%rip), %rcx
	movb	$0, -268(%rbp)
	movq	$0, -264(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movl	%eax, -272(%rbp)
	movq	24, %rax
	ud2
.L2617:
	leaq	-264(%rbp), %rdx
	xorl	%r8d, %r8d
	leaq	.LC120(%rip), %rcx
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIlLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movl	$3, %r12d
	movq	%rax, %r13
	movl	-264(%rbp), %r8d
	movq	-312(%rbp), %rdi
	movl	%edx, %eax
	subl	%esi, %eax
	jmp	.L2635
.L2616:
	cmpq	%rdx, %rsi
	ja	.L2636
	movq	%rdx, %rcx
	subq	%rsi, %rcx
	movl	%ecx, %eax
	cmpl	$3, %ecx
	jbe	.L2636
	movss	1(%r12), %xmm0
.L2637:
	movd	%xmm0, %r13d
	movl	$4, %r12d
	movl	$4, %r8d
	jmp	.L2635
.L2618:
	cmpq	%rdx, %rsi
	jb	.L2678
	leaq	.LC40(%rip), %rdx
	leaq	.LC118(%rip), %rcx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	-312(%rbp), %rdi
.L2634:
	movl	%edx, %eax
	movl	$2, %r12d
	subl	%esi, %eax
	jmp	.L2635
	.p2align 4,,10
	.p2align 3
.L2620:
	cmpb	$0, -153(%rbp)
	movl	$6, %r12d
	jne	.L2655
	cmpb	$0, -159(%rbp)
	jne	.L2655
	jmp	.L2613
	.p2align 4,,10
	.p2align 3
.L2674:
	leaq	-268(%rbp), %rdx
	xorl	%r8d, %r8d
	movabsq	$4294967296, %rax
	movq	%rdi, -312(%rbp)
	leaq	.LC96(%rip), %rcx
	movq	%rax, -272(%rbp)
	movl	$7, %r12d
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE0ELNS2_9TraceFlagE0ELi0EEET_PKhPjPKcS7_
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movl	%eax, %r13d
	movl	-268(%rbp), %r8d
	movq	-312(%rbp), %rdi
	movl	%edx, %eax
	subl	%esi, %eax
	jmp	.L2635
	.p2align 4,,10
	.p2align 3
.L2652:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L2653
	.p2align 4,,10
	.p2align 3
.L2678:
	movzbl	1(%r12), %eax
	movl	%eax, %r13d
	andl	$127, %r13d
	testb	%al, %al
	js	.L2679
	sall	$25, %r13d
	movl	$1, %r8d
	sarl	$25, %r13d
	jmp	.L2634
	.p2align 4,,10
	.p2align 3
.L2679:
	leaq	2(%r12), %r8
	cmpq	%rdx, %r8
	jb	.L2680
	movq	%r8, %rsi
	leaq	.LC40(%rip), %rdx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC118(%rip), %rcx
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movl	$1, %r8d
	movq	-312(%rbp), %rdi
	jmp	.L2634
	.p2align 4,,10
	.p2align 3
.L2636:
	leaq	.LC119(%rip), %rdx
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	-312(%rbp), %rdi
	movl	%edx, %eax
	subl	%esi, %eax
	jmp	.L2637
	.p2align 4,,10
	.p2align 3
.L2638:
	leaq	.LC121(%rip), %rdx
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	-312(%rbp), %rdi
	movl	%edx, %eax
	subl	%esi, %eax
	jmp	.L2639
	.p2align 4,,10
	.p2align 3
.L2680:
	movzbl	2(%r12), %ecx
	movzbl	%r13b, %r13d
	movl	%ecx, %eax
	sall	$7, %eax
	andl	$16256, %eax
	orl	%eax, %r13d
	testb	%cl, %cl
	js	.L2681
	sall	$18, %r13d
	movl	$2, %r8d
	sarl	$18, %r13d
	jmp	.L2634
.L2681:
	leaq	3(%r12), %r8
	cmpq	%rdx, %r8
	jb	.L2682
	movq	%r8, %rsi
	leaq	.LC40(%rip), %rdx
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	leaq	.LC118(%rip), %rcx
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movl	$2, %r8d
	movq	-312(%rbp), %rdi
	jmp	.L2634
.L2682:
	movzbl	3(%r12), %ecx
	movl	%ecx, %eax
	sall	$14, %eax
	andl	$2080768, %eax
	orl	%r13d, %eax
	testb	%cl, %cl
	js	.L2683
	sall	$11, %eax
	movl	$3, %r8d
	movl	%eax, %r13d
	sarl	$11, %r13d
	jmp	.L2634
.L2683:
	leaq	4(%r12), %rsi
	cmpq	%rdx, %rsi
	jb	.L2684
	xorl	%eax, %eax
	leaq	.LC118(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-312(%rbp), %rdi
	movl	$3, %r8d
	xorl	%eax, %eax
.L2629:
	sall	$4, %eax
	movl	%eax, %r13d
	sarl	$4, %r13d
.L2672:
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	jmp	.L2634
.L2684:
	movzbl	4(%r12), %esi
	movl	$4, %r8d
	movl	%esi, %ecx
	sall	$21, %ecx
	andl	$266338304, %ecx
	orl	%eax, %ecx
	movl	%ecx, %eax
	testb	%sil, %sil
	jns	.L2629
	leaq	5(%r12), %rsi
	cmpq	%rdx, %rsi
	jnb	.L2661
	movzbl	5(%r12), %r12d
	testb	%r12b, %r12b
	js	.L2662
	movl	%r12d, %r13d
	movl	$5, %r8d
	sall	$28, %r13d
	orl	%ecx, %r13d
.L2631:
	andb	$-8, %r12b
	je	.L2672
	cmpb	$120, %r12b
	je	.L2672
	leaq	.LC41(%rip), %rdx
	movl	%r8d, -320(%rbp)
	xorl	%r13d, %r13d
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	-312(%rbp), %rdi
	movl	-320(%rbp), %r8d
	jmp	.L2634
.L2676:
	call	__stack_chk_fail@PLT
.L2661:
	xorl	%r12d, %r12d
.L2630:
	leaq	.LC118(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	xorl	%eax, %eax
	movl	%r8d, -324(%rbp)
	movq	%rsi, -320(%rbp)
	xorl	%r13d, %r13d
	movq	%rdi, -312(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	-324(%rbp), %r8d
	movq	-320(%rbp), %rsi
	movq	-312(%rbp), %rdi
	jmp	.L2631
.L2662:
	movl	$5, %r8d
	jmp	.L2630
	.cfi_endproc
.LFE19132:
	.size	_ZN2v88internal4wasm28DecodeWasmInitExprForTestingERKNS1_12WasmFeaturesEPKhS6_, .-_ZN2v88internal4wasm28DecodeWasmInitExprForTestingERKNS1_12WasmFeaturesEPKhS6_
	.section	.text._ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.type	_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, @function
_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm:
.LFB24393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L2686
	movq	(%rbx), %r8
.L2687:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L2696
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L2697:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2686:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L2710
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2711
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L2689:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L2691
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L2692
	.p2align 4,,10
	.p2align 3
.L2693:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2694:
	testq	%rsi, %rsi
	je	.L2691
.L2692:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movl	8(%rcx), %eax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L2693
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L2699
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L2692
	.p2align 4,,10
	.p2align 3
.L2691:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L2695
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L2695:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L2687
	.p2align 4,,10
	.p2align 3
.L2696:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L2698
	movl	8(%rax), %eax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L2698:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2699:
	movq	%rdx, %rdi
	jmp	.L2694
	.p2align 4,,10
	.p2align 3
.L2710:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L2689
.L2711:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE24393:
	.size	_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, .-_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.section	.text._ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IjS5_EEEES0_INS8_14_Node_iteratorIS6_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IjS5_EEEES0_INS8_14_Node_iteratorIS6_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IjS5_EEEES0_INS8_14_Node_iteratorIS6_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IjS5_EEEES0_INS8_14_Node_iteratorIS6_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IjS5_EEEES0_INS8_14_Node_iteratorIS6_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB23859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	(%rbx), %r10d
	movq	8(%r12), %rcx
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movl	%r10d, 8(%rax)
	movq	4(%rbx), %rax
	movq	%rax, 12(%rdi)
	movq	%r10, %rax
	divq	%rcx
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2713
	movq	(%rax), %rbx
	movq	%r10, %rsi
	movl	8(%rbx), %r8d
	jmp	.L2715
	.p2align 4,,10
	.p2align 3
.L2726:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2713
	movl	8(%rbx), %eax
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L2713
.L2715:
	cmpl	%r8d, %esi
	jne	.L2726
	call	_ZdlPv@PLT
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2713:
	.cfi_restore_state
	movq	%rdi, %rcx
	movq	%r10, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	%r9, %rsi
	call	_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23859:
	.size	_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IjS5_EEEES0_INS8_14_Node_iteratorIS6_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IjS5_EEEES0_INS8_14_Node_iteratorIS6_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZN2v88internal4wasm19DecodeFunctionNamesEPKhS3_PSt13unordered_mapIjNS1_12WireBytesRefESt4hashIjESt8equal_toIjESaISt4pairIKjS5_EEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm19DecodeFunctionNamesEPKhS3_PSt13unordered_mapIjNS1_12WireBytesRefESt4hashIjESt8equal_toIjESaISt4pairIKjS5_EEE
	.type	_ZN2v88internal4wasm19DecodeFunctionNamesEPKhS3_PSt13unordered_mapIjNS1_12WireBytesRefESt4hashIjESt8equal_toIjESaISt4pairIKjS5_EEE, @function
_ZN2v88internal4wasm19DecodeFunctionNamesEPKhS3_PSt13unordered_mapIjNS1_12WireBytesRefESt4hashIjESt8equal_toIjESaISt4pairIKjS5_EEE:
.LFB19178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-144(%rbp), %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rdi, -136(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rdi, -128(%rbp)
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	movl	$0, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_115FindNameSectionEPNS1_7DecoderE.constprop.0
	testb	%al, %al
	je	.L2749
	cmpq	$0, -88(%rbp)
	leaq	.LC96(%rip), %r14
	je	.L2729
	jmp	.L2749
	.p2align 4,,10
	.p2align 3
.L2827:
	movzbl	1(%rsi), %edx
	leaq	2(%rsi), %r8
	movl	%edx, %ecx
	andl	$127, %ecx
	testb	%dl, %dl
	js	.L2824
.L2736:
	movq	%r8, -128(%rbp)
.L2786:
	subq	%r8, %rax
	cmpl	%eax, %ecx
	ja	.L2825
	cmpb	$1, %bl
	je	.L2826
.L2788:
	addq	%r8, %rcx
	movq	-88(%rbp), %rax
	movq	%rcx, -128(%rbp)
.L2750:
	testq	%rax, %rax
	jne	.L2749
.L2729:
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	cmpq	%rax, %rsi
	jnb	.L2749
	cmpl	%esi, %eax
	je	.L2733
	movzbl	(%rsi), %ebx
	leaq	1(%rsi), %r8
	movq	%r8, -128(%rbp)
	testb	%bl, %bl
	js	.L2749
	cmpq	%r8, %rax
	ja	.L2827
.L2735:
	leaq	.LC94(%rip), %rcx
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
.L2745:
	movq	-128(%rbp), %r8
	xorl	%ecx, %ecx
	cmpb	$1, %bl
	jne	.L2788
.L2826:
	leaq	-156(%rbp), %rax
	leaq	.LC103(%rip), %rcx
	movq	%r8, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, -192(%rbp)
	movl	$0, -156(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.1
	movl	%eax, %ebx
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	jne	.L2749
	.p2align 4,,10
	.p2align 3
.L2821:
	testl	%ebx, %ebx
	je	.L2750
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rax
	cmpq	%rax, %r8
	jb	.L2828
	movq	%r8, %rsi
	xorl	%eax, %eax
	movq	%r14, %rcx
	movq	%r12, %rdi
	leaq	.LC40(%rip), %rdx
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
.L2763:
	cmpq	%rax, %rsi
	jb	.L2829
.L2764:
	leaq	.LC54(%rip), %rcx
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-128(%rbp), %r13
	xorl	%ecx, %ecx
	subq	-136(%rbp), %r13
	addl	-112(%rbp), %r13d
.L2776:
	cmpq	$0, -88(%rbp)
	jne	.L2749
	movl	%ecx, %esi
	movl	%r13d, %edi
	subl	-112(%rbp), %edi
	addq	-136(%rbp), %rdi
	movl	%ecx, -168(%rbp)
	call	_ZN7unibrow4Utf816ValidateEncodingEPKhm@PLT
	movl	-168(%rbp), %ecx
	testb	%al, %al
	jne	.L2830
	movq	-88(%rbp), %rax
	subl	$1, %ebx
	testq	%rax, %rax
	je	.L2821
	.p2align 4,,10
	.p2align 3
.L2749:
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -144(%rbp)
	cmpq	-176(%rbp), %rdi
	je	.L2727
	call	_ZdlPv@PLT
.L2727:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2831
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2824:
	.cfi_restore_state
	cmpq	%r8, %rax
	jbe	.L2737
	movzbl	2(%rsi), %edi
	leaq	3(%rsi), %r8
	movl	%edi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L2736
	cmpq	%r8, %rax
	jbe	.L2737
	movzbl	3(%rsi), %edi
	leaq	4(%rsi), %r8
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L2736
	cmpq	%r8, %rax
	jbe	.L2737
	movzbl	4(%rsi), %edi
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	js	.L2832
	leaq	5(%rsi), %r8
	movq	%r8, -128(%rbp)
	jmp	.L2786
	.p2align 4,,10
	.p2align 3
.L2828:
	movzbl	(%r8), %edx
	leaq	1(%r8), %rsi
	movl	%edx, %r15d
	andl	$127, %r15d
	testb	%dl, %dl
	js	.L2833
.L2752:
	movq	%rsi, -128(%rbp)
	cmpq	%rax, %rsi
	jnb	.L2764
.L2829:
	movzbl	(%rsi), %edx
	leaq	1(%rsi), %r8
	movl	%edx, %ecx
	andl	$127, %ecx
	testb	%dl, %dl
	js	.L2834
.L2765:
	movq	%r8, -128(%rbp)
.L2785:
	movq	%r8, %r13
	subq	-136(%rbp), %r13
	addl	-112(%rbp), %r13d
	testl	%ecx, %ecx
	je	.L2776
	movq	-120(%rbp), %rax
	subq	%r8, %rax
	cmpl	%eax, %ecx
	ja	.L2777
	movl	%ecx, %esi
	addq	%r8, %rsi
	movq	%rsi, -128(%rbp)
	jmp	.L2776
	.p2align 4,,10
	.p2align 3
.L2833:
	cmpq	%rsi, %rax
	jbe	.L2753
	movzbl	1(%r8), %ecx
	leaq	2(%r8), %rsi
	movl	%ecx, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %r15d
	testb	%cl, %cl
	jns	.L2752
	cmpq	%rsi, %rax
	jbe	.L2753
	movzbl	2(%r8), %ecx
	leaq	3(%r8), %rsi
	movl	%ecx, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %r15d
	testb	%cl, %cl
	jns	.L2752
	cmpq	%rsi, %rax
	jbe	.L2753
	movzbl	3(%r8), %ecx
	leaq	4(%r8), %rsi
	movl	%ecx, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %r15d
	testb	%cl, %cl
	jns	.L2752
	cmpq	%rsi, %rax
	jbe	.L2759
	movzbl	4(%r8), %eax
	addq	$5, %r8
	movq	%r8, -128(%rbp)
	movl	%eax, %r13d
	andl	$-16, %r13d
	testb	%al, %al
	js	.L2760
	sall	$28, %eax
	orl	%eax, %r15d
.L2761:
	testb	%r13b, %r13b
	je	.L2787
	leaq	.LC41(%rip), %rdx
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	jmp	.L2763
	.p2align 4,,10
	.p2align 3
.L2834:
	cmpq	%rax, %r8
	jnb	.L2772
	movzbl	1(%rsi), %edi
	leaq	2(%rsi), %r8
	movl	%edi, %edx
	sall	$7, %edx
	andl	$16256, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L2765
	cmpq	%rax, %r8
	jnb	.L2772
	movzbl	2(%rsi), %edi
	leaq	3(%rsi), %r8
	movl	%edi, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L2765
	cmpq	%rax, %r8
	jnb	.L2772
	movzbl	3(%rsi), %edi
	leaq	4(%rsi), %r8
	movl	%edi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %ecx
	testb	%dil, %dil
	jns	.L2765
	cmpq	%rax, %r8
	jnb	.L2772
	movzbl	4(%rsi), %eax
	addq	$5, %rsi
	movq	%rsi, -128(%rbp)
	movl	%eax, %r13d
	andl	$-16, %r13d
	testb	%al, %al
	js	.L2773
	sall	$28, %eax
	orl	%eax, %ecx
.L2774:
	testb	%r13b, %r13b
	je	.L2775
	leaq	.LC41(%rip), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-128(%rbp), %r13
	xorl	%ecx, %ecx
	subq	-136(%rbp), %r13
	addl	-112(%rbp), %r13d
	jmp	.L2776
	.p2align 4,,10
	.p2align 3
.L2772:
	leaq	.LC54(%rip), %rcx
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-128(%rbp), %r13
	xorl	%ecx, %ecx
	subq	-136(%rbp), %r13
	addl	-112(%rbp), %r13d
	jmp	.L2776
	.p2align 4,,10
	.p2align 3
.L2753:
	xorl	%eax, %eax
	movq	%r14, %rcx
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rdi
	movq	%rsi, -128(%rbp)
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	jmp	.L2763
	.p2align 4,,10
	.p2align 3
.L2830:
	movq	-192(%rbp), %rsi
	movl	%ecx, -148(%rbp)
	subl	$1, %ebx
	movq	-184(%rbp), %rdi
	movl	%r15d, -156(%rbp)
	movl	%r13d, -152(%rbp)
	call	_ZNSt10_HashtableIjSt4pairIKjN2v88internal4wasm12WireBytesRefEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IjS5_EEEES0_INS8_14_Node_iteratorIS6_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L2821
	jmp	.L2749
	.p2align 4,,10
	.p2align 3
.L2737:
	leaq	.LC94(%rip), %rcx
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2745
	.p2align 4,,10
	.p2align 3
.L2733:
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-120(%rbp), %r8
	movq	%r8, -128(%rbp)
	jmp	.L2735
	.p2align 4,,10
	.p2align 3
.L2825:
	leaq	.LC39(%rip), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	jmp	.L2749
	.p2align 4,,10
	.p2align 3
.L2777:
	xorl	%eax, %eax
	leaq	.LC39(%rip), %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	movl	%ecx, -168(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz.constprop.1
	movq	-120(%rbp), %rax
	movl	-168(%rbp), %ecx
	movq	%rax, -128(%rbp)
	jmp	.L2776
.L2759:
	movq	%r14, %rcx
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movq	%rsi, -128(%rbp)
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L2787:
	movq	-128(%rbp), %rsi
	movq	-120(%rbp), %rax
	jmp	.L2763
.L2832:
	leaq	5(%rsi), %r15
	cmpq	%r15, %rax
	jbe	.L2743
	movzbl	5(%rsi), %r13d
	leaq	6(%rsi), %r8
	movq	%r8, -128(%rbp)
	testb	%r13b, %r13b
	js	.L2835
	movl	%r13d, %edx
	sall	$28, %edx
	orl	%edx, %ecx
	andl	$240, %r13d
	je	.L2786
.L2747:
	leaq	.LC41(%rip), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L2745
.L2743:
	leaq	.LC94(%rip), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r15, -128(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2745
.L2760:
	movq	%r14, %rcx
	leaq	.LC40(%rip), %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	movq	%rsi, -168(%rbp)
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-168(%rbp), %rsi
	jmp	.L2761
.L2773:
	leaq	.LC54(%rip), %rcx
	movq	%r8, %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC40(%rip), %rdx
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-168(%rbp), %r8
	xorl	%ecx, %ecx
	jmp	.L2774
.L2835:
	xorl	%eax, %eax
	leaq	.LC94(%rip), %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	leaq	.LC40(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	andl	$240, %r13d
	jne	.L2747
	jmp	.L2745
.L2775:
	movq	-128(%rbp), %r8
	jmp	.L2785
.L2831:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19178:
	.size	_ZN2v88internal4wasm19DecodeFunctionNamesEPKhS3_PSt13unordered_mapIjNS1_12WireBytesRefESt4hashIjESt8equal_toIjESaISt4pairIKjS5_EEE, .-_ZN2v88internal4wasm19DecodeFunctionNamesEPKhS3_PSt13unordered_mapIjNS1_12WireBytesRefESt4hashIjESt8equal_toIjESaISt4pairIKjS5_EEE
	.section	.text._ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag,"axG",@progbits,_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag,comdat
	.p2align 4
	.weak	_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag
	.type	_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag, @function
_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag:
.LFB24879:
	.cfi_startproc
	endbr64
	movq	%rdx, %r10
	cmpq	%rsi, %rdi
	je	.L2858
	cmpq	%rsi, %rdx
	je	.L2859
	movq	%rdx, %rax
	movq	%rsi, %r8
	subq	%rsi, %r10
	movq	%rdi, %r9
	subq	%rdi, %rax
	subq	%rdi, %r8
	addq	%rdi, %r10
	sarq	$4, %rax
	sarq	$4, %r8
	movq	%rax, %rdx
	subq	%r8, %rdx
	cmpq	%r8, %rdx
	je	.L2860
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rax, %rdi
	subq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	cmpq	%r8, %rdi
	jle	.L2843
	.p2align 4,,10
	.p2align 3
.L2861:
	movq	%r8, %rcx
	salq	$4, %rcx
	addq	%r9, %rcx
	testq	%rdi, %rdi
	jle	.L2844
	movq	%r9, %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2845:
	movl	(%rsi), %r13d
	movl	4(%rsi), %r12d
	addq	$1, %rdx
	addq	$16, %rsi
	movdqu	(%rcx), %xmm0
	movzbl	-8(%rsi), %ebx
	addq	$16, %rcx
	movl	-4(%rsi), %r11d
	movups	%xmm0, -16(%rsi)
	movl	%r13d, -16(%rcx)
	movl	%r12d, -12(%rcx)
	movb	%bl, -8(%rcx)
	movl	%r11d, -4(%rcx)
	cmpq	%rdx, %rdi
	jne	.L2845
	salq	$4, %rdi
	addq	%rdi, %r9
.L2844:
	cqto
	idivq	%r8
	testq	%rdx, %rdx
	je	.L2857
	movq	%r8, %rdi
	subq	%rdx, %r8
.L2847:
	movq	%rdi, %rax
	movq	%rax, %rdi
	subq	%r8, %rdi
	cmpq	%r8, %rdi
	jg	.L2861
.L2843:
	movq	%rax, %rcx
	movq	%rdi, %rdx
	salq	$4, %rcx
	salq	$4, %rdx
	addq	%r9, %rcx
	movq	%rcx, %r9
	subq	%rdx, %r9
	testq	%r8, %r8
	jle	.L2848
	subq	$16, %rcx
	leaq	-16(%r9), %rsi
	xorl	%r11d, %r11d
	.p2align 4,,10
	.p2align 3
.L2849:
	movl	(%rsi), %r13d
	movl	4(%rsi), %r12d
	addq	$1, %r11
	subq	$16, %rcx
	movzbl	8(%rsi), %ebx
	movl	12(%rsi), %edx
	subq	$16, %rsi
	movdqu	16(%rcx), %xmm1
	movups	%xmm1, 16(%rsi)
	movl	%r13d, 16(%rcx)
	movl	%r12d, 20(%rcx)
	movb	%bl, 24(%rcx)
	movl	%edx, 28(%rcx)
	cmpq	%r8, %r11
	jne	.L2849
	salq	$4, %r11
	subq	%r11, %r9
.L2848:
	cqto
	idivq	%rdi
	movq	%rdx, %r8
	testq	%rdx, %rdx
	jne	.L2847
.L2857:
	popq	%rbx
	movq	%r10, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.L2859:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L2858:
	movq	%rdx, %rax
	ret
.L2860:
	movq	%rdi, %rax
	movq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L2841:
	movdqu	(%rdx), %xmm2
	movl	(%rax), %r9d
	addq	$16, %rax
	addq	$16, %rdx
	movl	-12(%rax), %r8d
	movzbl	-8(%rax), %edi
	movl	-4(%rax), %ecx
	movups	%xmm2, -16(%rax)
	movl	%r9d, -16(%rdx)
	movl	%r8d, -12(%rdx)
	movb	%dil, -8(%rdx)
	movl	%ecx, -4(%rdx)
	cmpq	%rax, %rsi
	jne	.L2841
	movq	%rsi, %rax
	ret
	.cfi_endproc
.LFE24879:
	.size	_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag, .-_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag
	.section	.text._ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_,"axG",@progbits,_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_,comdat
	.p2align 4
	.weak	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_
	.type	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_, @function
_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_:
.LFB24159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -64(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%r8, -80(%rbp)
	testq	%rcx, %rcx
	je	.L2862
	testq	%r8, %r8
	je	.L2862
	leaq	(%rcx,%r8), %rax
	movq	%rcx, %r12
	movq	%r9, %r14
	cmpq	$2, %rax
	je	.L2885
	cmpq	-80(%rbp), %rcx
	jle	.L2867
	movq	%rcx, %rbx
	movq	%rdx, %r15
	movq	%rsi, %r13
	movq	%r12, %r8
	shrq	$63, %rbx
	subq	%rsi, %r15
	addq	%rcx, %rbx
	sarq	$4, %r15
	sarq	%rbx
	movq	%rbx, %r10
	movq	%rbx, %rcx
	salq	$4, %r10
	addq	-64(%rbp), %r10
	.p2align 4,,10
	.p2align 3
.L2868:
	testq	%r15, %r15
	jle	.L2869
.L2886:
	movq	%r15, %r12
	sarq	%r12
	movq	%r12, %rbx
	salq	$4, %rbx
	addq	%r13, %rbx
	movl	4(%rbx), %edx
	cmpl	4(%r10), %edx
	je	.L2870
	jnb	.L2872
.L2871:
	subq	%r12, %r15
	leaq	16(%rbx), %r13
	subq	$1, %r15
	testq	%r15, %r15
	jg	.L2886
.L2869:
	movq	%r13, %r15
	subq	-72(%rbp), %r15
	movq	%rcx, %rbx
	movq	%r8, %r12
	sarq	$4, %r15
.L2874:
	movq	-72(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r10, %rdi
	movq	%r10, -96(%rbp)
	call	_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag
	movq	-96(%rbp), %r10
	movq	-64(%rbp), %rdi
	movq	%r14, %r9
	movq	%r15, %r8
	movq	%rbx, %rcx
	movq	%rax, %rdx
	movq	%rax, -72(%rbp)
	movq	%r10, %rsi
	call	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %r11
	movq	%r12, %rcx
	movq	-88(%rbp), %rdx
	addq	$72, %rsp
	subq	%rbx, %rcx
	movq	%r14, %r9
	popq	%rbx
	subq	%r15, %r8
	popq	%r12
	movq	%r13, %rsi
	movq	%r11, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_
	.p2align 4,,10
	.p2align 3
.L2867:
	.cfi_restore_state
	movq	%r8, %r15
	movq	-72(%rbp), %rax
	movq	%rdi, %r10
	shrq	$63, %r15
	addq	%r8, %r15
	movq	%r12, %r8
	sarq	%r15
	movq	%r15, %r13
	movq	%r15, %rcx
	movq	%r9, %r15
	salq	$4, %r13
	addq	%rax, %r13
	subq	%rdi, %rax
	movq	%rax, %rbx
	movq	%r13, %r14
	sarq	$4, %rbx
	.p2align 4,,10
	.p2align 3
.L2875:
	testq	%rbx, %rbx
	jle	.L2876
.L2887:
	movq	%rbx, %r13
	movl	4(%r14), %edx
	sarq	%r13
	movq	%r13, %r12
	salq	$4, %r12
	addq	%r10, %r12
	cmpl	4(%r12), %edx
	je	.L2877
	jnb	.L2879
.L2878:
	movq	%r13, %rbx
	testq	%rbx, %rbx
	jg	.L2887
.L2876:
	movq	%r10, %rbx
	subq	-64(%rbp), %rbx
	movq	%r14, %r13
	movq	%r8, %r12
	movq	%r15, %r14
	sarq	$4, %rbx
	movq	%rcx, %r15
	jmp	.L2874
	.p2align 4,,10
	.p2align 3
.L2870:
	movl	32(%r14), %r9d
	movl	(%r10), %esi
	movq	%r8, -112(%rbp)
	movl	(%rbx), %eax
	movq	8(%r14), %rdi
	movq	%rcx, -104(%rbp)
	subl	%r9d, %esi
	movq	%r10, -96(%rbp)
	subl	%r9d, %eax
	addq	%rdi, %rsi
	addq	%rax, %rdi
	call	memcmp@PLT
	movq	-96(%rbp), %r10
	movq	-104(%rbp), %rcx
	testl	%eax, %eax
	movq	-112(%rbp), %r8
	js	.L2871
.L2872:
	movq	%r12, %r15
	jmp	.L2868
	.p2align 4,,10
	.p2align 3
.L2877:
	movl	32(%r15), %r9d
	movl	(%r12), %esi
	movq	%r8, -112(%rbp)
	movl	(%r14), %eax
	movq	8(%r15), %rdi
	movq	%r10, -104(%rbp)
	subl	%r9d, %esi
	movq	%rcx, -96(%rbp)
	subl	%r9d, %eax
	addq	%rdi, %rsi
	addq	%rax, %rdi
	call	memcmp@PLT
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r10
	testl	%eax, %eax
	movq	-112(%rbp), %r8
	js	.L2878
.L2879:
	subq	%r13, %rbx
	leaq	16(%r12), %r10
	subq	$1, %rbx
	jmp	.L2875
	.p2align 4,,10
	.p2align 3
.L2885:
	movl	4(%rsi), %edx
	movq	%rdi, %rax
	cmpl	4(%rdi), %edx
	je	.L2865
	setb	%al
.L2866:
	testb	%al, %al
	je	.L2862
	movq	-64(%rbp), %rdi
	movq	-72(%rbp), %rsi
	movzbl	8(%rdi), %edx
	movdqu	(%rsi), %xmm0
	movq	(%rdi), %rcx
	movl	12(%rdi), %eax
	movups	%xmm0, (%rdi)
	movq	%rcx, (%rsi)
	movb	%dl, 8(%rsi)
	movl	%eax, 12(%rsi)
	movaps	%xmm0, -64(%rbp)
.L2862:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2865:
	.cfi_restore_state
	movl	(%rax), %esi
	movq	-72(%rbp), %rax
	movl	32(%r9), %ecx
	movq	8(%r9), %rdi
	movl	(%rax), %eax
	subl	%ecx, %esi
	subl	%ecx, %eax
	addq	%rdi, %rsi
	addq	%rax, %rdi
	call	memcmp@PLT
	shrl	$31, %eax
	jmp	.L2866
	.cfi_endproc
.LFE24159:
	.size	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_, .-_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_
	.section	.text._ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_,"axG",@progbits,_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_,comdat
	.p2align 4
	.weak	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_
	.type	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_, @function
_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_:
.LFB23622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	cmpq	$224, %rcx
	jle	.L2891
	sarq	$5, %rcx
	movq	%rcx, %rbx
	salq	$4, %rbx
	leaq	(%rdi,%rbx), %r15
	sarq	$4, %rbx
	movq	%r15, %rsi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_
	movq	%r13, %r8
	addq	$8, %rsp
	movq	%rbx, %rcx
	subq	%r15, %r8
	popq	%rbx
	movq	%r14, %r9
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	sarq	$4, %r8
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt22__merge_without_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_
	.p2align 4,,10
	.p2align 3
.L2891:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_
	.cfi_endproc
.LFE23622:
	.size	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_, .-_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_
	.section	.text._ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_SJ_T2_,"axG",@progbits,_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_SJ_T2_,comdat
	.p2align 4
	.weak	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_SJ_T2_
	.type	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_SJ_T2_, @function
_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_SJ_T2_:
.LFB24162:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %r14
	movq	24(%rbp), %rbx
	movq	%rdx, -64(%rbp)
	movq	%rcx, -80(%rbp)
	cmpq	%r14, %r8
	movq	%r8, -72(%rbp)
	cmovg	%r14, %rax
	cmpq	%rax, %rcx
	jg	.L2893
	movq	%rsi, %r14
	movq	%rsi, %r15
	subq	%rdi, %r14
	cmpq	%rdi, %rsi
	je	.L2894
	movq	%rdi, %rsi
	movq	%r14, %rdx
	movq	%r9, %rdi
	call	memmove@PLT
.L2894:
	addq	%r12, %r14
	cmpq	%r14, %r12
	je	.L2892
	.p2align 4,,10
	.p2align 3
.L2903:
	cmpq	%r15, -64(%rbp)
	je	.L2897
.L2959:
	movl	4(%r15), %edx
	cmpl	4(%r12), %edx
	je	.L2898
	setb	%al
	testb	%al, %al
	je	.L2900
.L2958:
	movdqu	(%r15), %xmm0
	addq	$16, %r13
	addq	$16, %r15
	movups	%xmm0, -16(%r13)
	cmpq	%r12, %r14
	jne	.L2903
.L2892:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2898:
	.cfi_restore_state
	movl	32(%rbx), %ecx
	movl	(%r12), %esi
	movl	(%r15), %eax
	movq	8(%rbx), %rdi
	subl	%ecx, %esi
	subl	%ecx, %eax
	addq	%rdi, %rsi
	addq	%rax, %rdi
	call	memcmp@PLT
	shrl	$31, %eax
	testb	%al, %al
	jne	.L2958
.L2900:
	movdqu	(%r12), %xmm1
	addq	$16, %r12
	addq	$16, %r13
	movups	%xmm1, -16(%r13)
	cmpq	%r12, %r14
	je	.L2892
	cmpq	%r15, -64(%rbp)
	jne	.L2959
.L2897:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	subq	%r12, %rdx
.L2956:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L2893:
	.cfi_restore_state
	movq	%rsi, %r11
	cmpq	%r14, -72(%rbp)
	jg	.L2905
	movq	%rdx, %r15
	movq	%rdx, %rcx
	subq	%rsi, %r15
	cmpq	%rsi, %rdx
	je	.L2906
	movq	%rdx, -72(%rbp)
	movq	%r9, %rdi
	movq	%r15, %rdx
	movq	%rsi, -56(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %rcx
	movq	-56(%rbp), %r11
.L2906:
	leaq	(%r12,%r15), %rdx
	cmpq	%r13, %r11
	je	.L2960
	cmpq	%rdx, %r12
	je	.L2892
	movq	%rbx, %rax
	leaq	-16(%r11), %r14
	leaq	-16(%rdx), %r15
	movq	%rcx, %rbx
	movq	%r12, %rcx
	movq	%r13, %r12
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L2909:
	movl	4(%r15), %edx
	cmpl	4(%r14), %edx
	je	.L2910
.L2962:
	setb	%al
	subq	$16, %rbx
	testb	%al, %al
	je	.L2912
.L2963:
	movdqu	(%r14), %xmm2
	movups	%xmm2, (%rbx)
	cmpq	%r14, %r12
	je	.L2961
	subq	$16, %r14
	movl	4(%r15), %edx
	cmpl	4(%r14), %edx
	jne	.L2962
.L2910:
	movl	32(%r13), %r8d
	movl	(%r14), %esi
	movq	%rcx, -56(%rbp)
	subq	$16, %rbx
	movl	(%r15), %eax
	movq	8(%r13), %rdi
	subl	%r8d, %esi
	subl	%r8d, %eax
	addq	%rdi, %rsi
	addq	%rax, %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	shrl	$31, %eax
	testb	%al, %al
	jne	.L2963
.L2912:
	movdqu	(%r15), %xmm3
	movups	%xmm3, (%rbx)
	cmpq	%r15, %rcx
	je	.L2892
	subq	$16, %r15
	jmp	.L2909
	.p2align 4,,10
	.p2align 3
.L2905:
	cmpq	%r8, %rcx
	jle	.L2915
	movq	%rcx, %rax
	movq	%rdx, %r15
	movq	%rsi, -56(%rbp)
	movq	%r14, %r8
	shrq	$63, %rax
	subq	%rsi, %r15
	movq	%rbx, %r13
	movq	%rdi, %r14
	addq	%rcx, %rax
	sarq	$4, %r15
	movq	%r9, %rcx
	sarq	%rax
	movq	%r15, %rbx
	movq	%rax, %r10
	movq	%rax, -96(%rbp)
	salq	$4, %r10
	addq	%rdi, %r10
	.p2align 4,,10
	.p2align 3
.L2916:
	testq	%rbx, %rbx
	jle	.L2917
.L2964:
	movq	%rbx, %r12
	sarq	%r12
	movq	%r12, %r15
	salq	$4, %r15
	addq	-56(%rbp), %r15
	movl	4(%r15), %edx
	cmpl	4(%r10), %edx
	je	.L2918
	jnb	.L2920
.L2919:
	subq	%r12, %rbx
	leaq	16(%r15), %rax
	subq	$1, %rbx
	movq	%rax, -56(%rbp)
	testq	%rbx, %rbx
	jg	.L2964
.L2917:
	movq	-56(%rbp), %rax
	movq	%r13, %rbx
	movq	%rcx, %r12
	movq	%r14, %r13
	movq	%r8, %r14
	subq	%r11, %rax
	sarq	$4, %rax
	movq	%rax, -88(%rbp)
.L2922:
	movq	-88(%rbp), %rcx
	movq	-80(%rbp), %rax
	subq	-96(%rbp), %rax
	movq	%rax, -80(%rbp)
	cmpq	%rcx, %rax
	jle	.L2929
	cmpq	%r14, %rcx
	jg	.L2929
	movq	%r10, %r15
	testq	%rcx, %rcx
	je	.L2930
	movq	-56(%rbp), %rax
	movq	%rax, %r15
	subq	%r11, %r15
	cmpq	%r11, %rax
	je	.L2931
	movq	%r11, %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r10, -112(%rbp)
	movq	%r11, -104(%rbp)
	call	memmove@PLT
	movq	-104(%rbp), %r11
	movq	-112(%rbp), %r10
	cmpq	%r11, %r10
	je	.L2932
.L2940:
	movq	-56(%rbp), %rdi
	movq	%r11, %rdx
	movq	%r10, %rsi
	movq	%r10, -104(%rbp)
	subq	%r10, %rdx
	subq	%rdx, %rdi
	call	memmove@PLT
	movq	-104(%rbp), %r10
.L2932:
	testq	%r15, %r15
	je	.L2934
	movq	%r10, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	memmove@PLT
	movq	%rax, %r10
.L2934:
	addq	%r10, %r15
.L2930:
	pushq	%rbx
	movq	-88(%rbp), %r8
	movq	%r12, %r9
	movq	%r15, %rdx
	pushq	%r14
	movq	-96(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r10, %rsi
	call	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_SJ_T2_
	movq	%rbx, 24(%rbp)
	popq	%rax
	movq	%r12, %r9
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rcx
	movq	%r14, 16(%rbp)
	movq	%r15, %rdi
	subq	-88(%rbp), %rax
	movq	-56(%rbp), %rsi
	popq	%rdx
	movq	-64(%rbp), %rdx
	leaq	-40(%rbp), %rsp
	movq	%rax, %r8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_SJ_T2_
	.p2align 4,,10
	.p2align 3
.L2915:
	.cfi_restore_state
	movq	%r8, %rax
	movq	%rsi, %r15
	movq	%rbx, %r13
	movq	%rdi, %r10
	shrq	$63, %rax
	subq	%rdi, %r15
	movq	%r9, %rcx
	addq	%r8, %rax
	sarq	$4, %r15
	movq	%r14, %r8
	movq	%rdi, %r14
	sarq	%rax
	movq	%r15, %rbx
	movq	%rax, -88(%rbp)
	salq	$4, %rax
	addq	%rsi, %rax
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L2923:
	testq	%rbx, %rbx
	jle	.L2924
.L2965:
	movq	%rbx, %r12
	movq	-56(%rbp), %rax
	sarq	%r12
	movq	%r12, %r15
	movl	4(%rax), %edx
	salq	$4, %r15
	addq	%r10, %r15
	cmpl	4(%r15), %edx
	je	.L2925
	jnb	.L2927
.L2926:
	movq	%r12, %rbx
	testq	%rbx, %rbx
	jg	.L2965
.L2924:
	movq	%r13, %rbx
	movq	%r10, %rax
	movq	%r14, %r13
	movq	%rcx, %r12
	subq	%r13, %rax
	movq	%r8, %r14
	sarq	$4, %rax
	movq	%rax, -96(%rbp)
	jmp	.L2922
	.p2align 4,,10
	.p2align 3
.L2918:
	movl	32(%r13), %r9d
	movl	(%r10), %esi
	movq	%r8, -120(%rbp)
	movl	(%r15), %eax
	movq	8(%r13), %rdi
	movq	%rcx, -112(%rbp)
	subl	%r9d, %esi
	movq	%r11, -104(%rbp)
	subl	%r9d, %eax
	addq	%rdi, %rsi
	movq	%r10, -88(%rbp)
	addq	%rax, %rdi
	call	memcmp@PLT
	movq	-88(%rbp), %r10
	movq	-104(%rbp), %r11
	testl	%eax, %eax
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r8
	js	.L2919
.L2920:
	movq	%r12, %rbx
	jmp	.L2916
	.p2align 4,,10
	.p2align 3
.L2925:
	movl	32(%r13), %r9d
	movl	(%r15), %esi
	movq	%rcx, -112(%rbp)
	movq	-56(%rbp), %rcx
	movq	8(%r13), %rax
	movq	%r8, -120(%rbp)
	subl	%r9d, %esi
	movq	%r11, -104(%rbp)
	movl	(%rcx), %edi
	addq	%rax, %rsi
	movq	%r10, -96(%rbp)
	subl	%r9d, %edi
	addq	%rax, %rdi
	call	memcmp@PLT
	movq	-96(%rbp), %r10
	movq	-104(%rbp), %r11
	testl	%eax, %eax
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r8
	js	.L2926
.L2927:
	subq	%r12, %rbx
	leaq	16(%r15), %r10
	subq	$1, %rbx
	jmp	.L2923
	.p2align 4,,10
	.p2align 3
.L2929:
	movq	-80(%rbp), %rax
	cmpq	%r14, %rax
	jg	.L2935
	movq	-56(%rbp), %rdi
	movq	%rdi, %r15
	testq	%rax, %rax
	je	.L2930
	movq	%r11, %r8
	subq	%r10, %r8
	subq	%r8, %r15
	cmpq	%r11, %r10
	je	.L2936
	movq	%r8, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	%r11, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%r10, -104(%rbp)
	call	memmove@PLT
	movq	-120(%rbp), %r11
	cmpq	%r11, -56(%rbp)
	movq	-104(%rbp), %r10
	movq	-112(%rbp), %r8
	je	.L2937
.L2942:
	movq	-56(%rbp), %rdx
	movq	%r10, %rdi
	movq	%r11, %rsi
	movq	%r8, -104(%rbp)
	subq	%r11, %rdx
	call	memmove@PLT
	movq	-104(%rbp), %r8
	movq	%rax, %r10
.L2937:
	testq	%r8, %r8
	je	.L2930
	movq	%r8, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r10, -104(%rbp)
	call	memmove@PLT
	movq	-104(%rbp), %r10
	jmp	.L2930
	.p2align 4,,10
	.p2align 3
.L2961:
	movq	%rcx, %r12
	leaq	16(%r15), %rdx
	movq	%rbx, %rcx
	cmpq	%rdx, %r12
	je	.L2892
	subq	%r12, %rdx
	movq	%r12, %rsi
	subq	%rdx, %rcx
	movq	%rcx, %rdi
	jmp	.L2956
	.p2align 4,,10
	.p2align 3
.L2931:
	cmpq	-56(%rbp), %r10
	jne	.L2940
	jmp	.L2934
	.p2align 4,,10
	.p2align 3
.L2960:
	cmpq	%rdx, %r12
	je	.L2892
	movq	-64(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	subq	%r15, %rdi
	jmp	.L2956
	.p2align 4,,10
	.p2align 3
.L2935:
	movq	-56(%rbp), %rdx
	movq	%r10, %rdi
	movq	%r11, %rsi
	movq	%r10, -104(%rbp)
	call	_ZNSt3_V28__rotateIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS6_SaIS6_EEEEEET_SC_SC_SC_St26random_access_iterator_tag
	movq	-104(%rbp), %r10
	movq	%rax, %r15
	jmp	.L2930
	.p2align 4,,10
	.p2align 3
.L2936:
	cmpq	%r10, -56(%rbp)
	jne	.L2942
	jmp	.L2930
	.cfi_endproc
.LFE24162:
	.size	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_SJ_T2_, .-_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_SJ_T2_
	.section	.text._ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_T2_,"axG",@progbits,_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_T2_,comdat
	.p2align 4
	.weak	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_T2_
	.type	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_T2_, @function
_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_T2_:
.LFB23624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	subq	%rdi, %rax
	sarq	$4, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	1(%rax), %rcx
	movq	%rcx, %rax
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	shrq	$63, %rax
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	addq	%rcx, %rax
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	sarq	%rax
	movq	%rax, %r9
	salq	$4, %r9
	subq	$24, %rsp
	leaq	(%rdi,%r9), %r10
	movq	%r9, -64(%rbp)
	cmpq	%r14, %rax
	jle	.L2967
	movq	%r10, %rsi
	movq	%r14, %rcx
	movq	%r10, -56(%rbp)
	call	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_T2_
	movq	-56(%rbp), %r10
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_T2_
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r9
.L2968:
	pushq	%r15
	movq	%r13, %r8
	movq	%r9, %rcx
	movq	%r13, %rdx
	pushq	%r14
	subq	%r10, %r8
	movq	%rbx, %r9
	movq	%r12, %rdi
	sarq	$4, %rcx
	sarq	$4, %r8
	movq	%r10, %rsi
	call	_ZSt16__merge_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEElS6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_SI_T0_SJ_T1_SJ_T2_
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2967:
	.cfi_restore_state
	movq	%r8, %rcx
	movq	%r10, %rsi
	movq	%r10, -56(%rbp)
	call	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_
	movq	-56(%rbp), %r10
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r10, %rdi
	call	_ZSt24__merge_sort_with_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_NS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %r10
	jmp	.L2968
	.cfi_endproc
.LFE23624:
	.size	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_T2_, .-_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_T2_
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv.str1.1,"aMS",@progbits,1
.LC149:
	.string	"unknown"
.LC150:
	.string	"function"
.LC151:
	.string	"global"
.LC152:
	.string	"exception"
.LC153:
	.string	"table"
.LC154:
	.string	"exports count"
.LC155:
	.string	"export function index"
.LC156:
	.string	"invalid memory index != 0"
.LC157:
	.string	"invalid export kind 0x%02x"
.LC158:
	.string	"exception index"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC159:
	.string	"Duplicate export name '%.*s' for %s %d and %s %d"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv, @function
_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv:
.LFB19025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$100000, %edx
	leaq	.LC154(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm
	movq	96(%r14), %r12
	movl	%eax, -168(%rbp)
	movl	%eax, %edx
	movq	232(%r12), %r11
	movq	248(%r12), %rax
	subq	%r11, %rax
	sarq	$4, %rax
	cmpq	%rax, %rdx
	ja	.L3062
.L2971:
	cmpq	$0, 56(%r14)
	jne	.L2970
	movl	-168(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L2978
	leaq	-160(%rbp), %r15
	xorl	%r12d, %r12d
	leaq	.L2985(%rip), %r13
	movq	%r15, %rax
	movq	%r14, %r15
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L2979:
	movq	96(%r15), %rdi
	movq	$0, -160(%rbp)
	movb	$0, -152(%rbp)
	movl	$0, -148(%rbp)
	movq	240(%rdi), %rsi
	cmpq	248(%rdi), %rsi
	je	.L2980
	movq	$0, (%rsi)
	movb	$0, 8(%rsi)
	movl	$0, 12(%rsi)
	addq	$16, 240(%rdi)
.L2981:
	movq	96(%r15), %rax
	leaq	.LC68(%rip), %rsi
	movq	%r15, %rdi
	movq	240(%rax), %rbx
	call	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.1
	movq	%rax, -16(%rbx)
	movq	16(%r15), %rsi
	cmpl	%esi, 24(%r15)
	je	.L2982
	movzbl	(%rsi), %ecx
	leaq	1(%rsi), %rax
	movq	%rax, 16(%r15)
	movb	%cl, -8(%rbx)
	cmpb	$4, %cl
	ja	.L2983
	movslq	0(%r13,%rcx,4), %rax
	addq	%r13, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,comdat
	.align 4
	.align 4
.L2985:
	.long	.L2989-.L2985
	.long	.L2988-.L2985
	.long	.L2987-.L2985
	.long	.L2986-.L2985
	.long	.L2984-.L2985
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,comdat
	.p2align 4,,10
	.p2align 3
.L2986:
	movq	96(%r15), %r9
	movq	16(%r15), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	leaq	.LC115(%rip), %rcx
	movl	$0, -160(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	-184(%rbp), %r9
	movq	-176(%rbp), %rsi
	movl	%eax, %edx
	movq	32(%r9), %rcx
	movq	24(%r9), %rax
	movq	%rdx, %r8
	subq	%rax, %rcx
	movq	%rcx, %r9
	sarq	$5, %r9
	cmpq	%r9, %rdx
	jnb	.L3063
	salq	$5, %rdx
	movl	%r8d, -4(%rbx)
	addq	%rdx, %rax
	je	.L2992
	movb	$1, 29(%rax)
	.p2align 4,,10
	.p2align 3
.L2992:
	addl	$1, %r12d
	cmpq	$0, 56(%r15)
	jne	.L2970
	cmpl	%r12d, -168(%rbp)
	jne	.L2979
	movq	%r15, %r14
.L2978:
	cmpb	$0, 168(%r14)
	je	.L3030
.L2970:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3064
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2987:
	.cfi_restore_state
	movq	16(%r15), %rsi
	movq	%r14, %rdx
	leaq	.LC135(%rip), %rcx
	movq	%r15, %rdi
	movl	$0, -160(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	96(%r15), %rdx
	cmpb	$1, 18(%rdx)
	jne	.L3041
	testl	%eax, %eax
	jne	.L3041
	movb	$1, 19(%rdx)
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L2988:
	movq	96(%r15), %r9
	movq	16(%r15), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	leaq	.LC141(%rip), %rcx
	movl	$0, -160(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	-184(%rbp), %r9
	movq	-176(%rbp), %rsi
	movl	%eax, %edx
	movq	192(%r9), %rcx
	movq	184(%r9), %rax
	movq	%rdx, %r8
	subq	%rax, %rcx
	movq	%rcx, %r9
	sarq	$4, %r9
	cmpq	%r9, %rdx
	jnb	.L3065
	salq	$4, %rdx
	movl	%r8d, -4(%rbx)
	addq	%rdx, %rax
	je	.L2992
	movb	$1, 14(%rax)
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L2982:
	xorl	%eax, %eax
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%r15), %rax
	movq	%rax, 16(%r15)
	movb	$0, -8(%rbx)
.L2989:
	movq	96(%r15), %r9
	movq	16(%r15), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	leaq	.LC155(%rip), %rcx
	movl	$0, -160(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	-184(%rbp), %r9
	movq	-176(%rbp), %rsi
	movl	%eax, %edx
	movq	144(%r9), %rcx
	movq	136(%r9), %rax
	movq	%rdx, %r8
	subq	%rax, %rcx
	movq	%rcx, %r9
	sarq	$5, %r9
	cmpq	%r9, %rdx
	jnb	.L3066
	movl	%r8d, -4(%rbx)
	movq	96(%r15), %rcx
	salq	$5, %rdx
	addl	$1, 72(%rcx)
	addq	%rdx, %rax
	je	.L2992
	movb	$1, 25(%rax)
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L2984:
	cmpb	$0, 81(%r15)
	jne	.L3001
	movl	$4, %ecx
	leaq	.LC157(%rip), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L3041:
	leaq	.LC156(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	96(%r15), %rdx
	movb	$1, 19(%rdx)
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L2980:
	addq	$232, %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIN2v88internal4wasm10WasmExportESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L2981
.L2983:
	leaq	.LC157(%rip), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L3001:
	movq	96(%r15), %r9
	movq	16(%r15), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	leaq	.LC158(%rip), %rcx
	movl	$0, -160(%rbp)
	movq	%r9, -184(%rbp)
	movq	%rsi, -176(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	-184(%rbp), %r9
	movq	-176(%rbp), %rsi
	movl	%eax, %edx
	movq	264(%r9), %rax
	subq	256(%r9), %rax
	movq	%rdx, %r8
	movq	%rax, %r9
	sarq	$3, %r9
	cmpq	%r9, %rdx
	jnb	.L3067
	movl	%r8d, -4(%rbx)
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L3030:
	movq	96(%r14), %rbx
	movq	240(%rbx), %rdi
	subq	232(%rbx), %rdi
	movq	%rdi, %rax
	sarq	$4, %rax
	cmpq	$31, %rdi
	jbe	.L2970
	movabsq	$576460752303423487, %rdx
	cmpq	%rdx, %rax
	ja	.L3068
	call	_Znwm@PLT
	movq	240(%rbx), %r9
	movq	232(%rbx), %r11
	movq	%rax, %r12
	cmpq	%r11, %r9
	je	.L3038
	movq	%r11, %rax
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L3007:
	movl	(%rax), %r8d
	movl	4(%rax), %edi
	addq	$16, %rax
	addq	$16, %rdx
	movzbl	-8(%rax), %esi
	movl	-4(%rax), %ecx
	movl	%r8d, -16(%rdx)
	movl	%edi, -12(%rdx)
	movb	%sil, -8(%rdx)
	movl	%ecx, -4(%rdx)
	cmpq	%rax, %r9
	jne	.L3007
	subq	%r11, %r9
	movq	%r9, %rdx
	leaq	(%r12,%r9), %r13
	sarq	$4, %rdx
.L3006:
	leaq	-160(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt17_Temporary_bufferIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES5_EC1ESA_l
	movq	-144(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L3069
	movq	-152(%rbp), %rcx
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZSt22__stable_sort_adaptiveIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEES6_lNS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_T1_T2_
.L3009:
	movq	-144(%rbp), %rdi
	leaq	16(%r12), %rbx
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	je	.L3010
	movq	%r12, -168(%rbp)
	movq	%r14, %r12
	movq	%r13, %r14
	jmp	.L3028
	.p2align 4,,10
	.p2align 3
.L3072:
	setb	%al
.L3012:
	testb	%al, %al
	je	.L3070
	addq	$16, %rbx
	cmpq	%rbx, %r14
	je	.L3071
.L3028:
	movl	4(%rbx), %r15d
	leaq	-16(%rbx), %r13
	cmpl	%r15d, -12(%rbx)
	jne	.L3072
	movl	32(%r12), %r8d
	movl	(%rbx), %esi
	movl	%r15d, %edx
	movl	-16(%rbx), %eax
	movq	8(%r12), %rdi
	subl	%r8d, %esi
	subl	%r8d, %eax
	addq	%rdi, %rsi
	addq	%rax, %rdi
	call	memcmp@PLT
	shrl	$31, %eax
	jmp	.L3012
	.p2align 4,,10
	.p2align 3
.L3067:
	cmpq	$8, %rax
	leaq	.LC138(%rip), %rdx
	leaq	.LC139(%rip), %rax
	movq	%r15, %rdi
	cmove	%rdx, %rax
	subq	$8, %rsp
	leaq	.LC147(%rip), %rdx
	leaq	.LC158(%rip), %rcx
	pushq	%rax
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%r8d, %r8d
	popq	%rax
	popq	%rdx
	movl	%r8d, -4(%rbx)
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L3063:
	cmpq	$32, %rcx
	leaq	.LC139(%rip), %rax
	leaq	.LC138(%rip), %rdx
	cmove	%rdx, %rax
	subq	$8, %rsp
	leaq	.LC115(%rip), %rcx
	pushq	%rax
.L3061:
	leaq	.LC147(%rip), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$0, -4(%rbx)
	popq	%rcx
	popq	%rsi
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L3066:
	cmpq	$32, %rcx
	leaq	.LC138(%rip), %rdx
	movq	%r15, %rdi
	leaq	.LC139(%rip), %rax
	leaq	.LC155(%rip), %rcx
	cmove	%rdx, %rax
	subq	$8, %rsp
	leaq	.LC147(%rip), %rdx
	pushq	%rax
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movl	$0, -4(%rbx)
	movq	96(%r15), %rax
	addl	$1, 72(%rax)
	popq	%rdi
	popq	%r8
	jmp	.L2992
	.p2align 4,,10
	.p2align 3
.L3065:
	cmpq	$16, %rcx
	leaq	.LC139(%rip), %rax
	leaq	.LC138(%rip), %rdx
	cmove	%rdx, %rax
	subq	$8, %rsp
	leaq	.LC141(%rip), %rcx
	pushq	%rax
	jmp	.L3061
	.p2align 4,,10
	.p2align 3
.L3062:
	movq	240(%r12), %rax
	movq	%rdx, %r13
	xorl	%r15d, %r15d
	salq	$4, %r13
	movq	%rax, %rbx
	subq	%r11, %rbx
	testq	%rdx, %rdx
	je	.L2972
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	232(%r12), %r11
	movq	%rax, %r15
	movq	240(%r12), %rax
.L2972:
	movq	%r15, %rcx
	movq	%r11, %rdx
	cmpq	%rax, %r11
	je	.L2976
	.p2align 4,,10
	.p2align 3
.L2973:
	movl	(%rdx), %r9d
	movl	4(%rdx), %r8d
	addq	$16, %rdx
	addq	$16, %rcx
	movzbl	-8(%rdx), %edi
	movl	-4(%rdx), %esi
	movl	%r9d, -16(%rcx)
	movl	%r8d, -12(%rcx)
	movb	%dil, -8(%rcx)
	movl	%esi, -4(%rcx)
	cmpq	%rax, %rdx
	jne	.L2973
.L2976:
	testq	%r11, %r11
	je	.L2975
	movq	%r11, %rdi
	call	_ZdlPv@PLT
.L2975:
	addq	%r15, %rbx
	addq	%r15, %r13
	movq	%r15, 232(%r12)
	movq	%rbx, 240(%r12)
	movq	%r13, 248(%r12)
	jmp	.L2971
.L3026:
	leaq	.LC153(%rip), %r9
.L3027:
	subq	$8, %rsp
	movq	-128(%rbp), %r8
	leaq	.LC159(%rip), %rdx
	xorl	%eax, %eax
	pushq	%rsi
	movq	%r13, %rsi
	pushq	%rcx
	movl	-120(%rbp), %ecx
	pushq	%rdi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	addq	$32, %rsp
.L3010:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	jmp	.L2970
.L3070:
	movq	%r12, %r14
	movq	%r13, -176(%rbp)
	movl	(%rbx), %r13d
	movl	%r15d, %edx
	subl	32(%r14), %r13d
	addq	8(%r14), %r13
	leaq	-128(%rbp), %rdi
	movq	%r13, %rsi
	movq	-168(%rbp), %r12
	call	_ZN2v88internal4wasm19TruncatedUserStringILi50EEC1EPKcm
	cmpb	$4, 8(%rbx)
	movl	12(%rbx), %esi
	movq	-176(%rbp), %r8
	ja	.L3014
	movzbl	8(%rbx), %eax
	leaq	.L3016(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,comdat
	.align 4
	.align 4
.L3016:
	.long	.L3039-.L3016
	.long	.L3019-.L3016
	.long	.L3018-.L3016
	.long	.L3017-.L3016
	.long	.L3015-.L3016
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,comdat
.L3019:
	leaq	.LC153(%rip), %rcx
.L3020:
	cmpb	$4, 8(%r8)
	movl	12(%r8), %edi
	ja	.L3021
	movzbl	8(%r8), %eax
	leaq	.L3023(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,comdat
	.align 4
	.align 4
.L3023:
	.long	.L3040-.L3023
	.long	.L3026-.L3023
	.long	.L3025-.L3023
	.long	.L3024-.L3023
	.long	.L3022-.L3023
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv,comdat
.L3040:
	leaq	.LC150(%rip), %r9
	jmp	.L3027
.L3039:
	leaq	.LC150(%rip), %rcx
	jmp	.L3020
.L3015:
	leaq	.LC152(%rip), %rcx
	jmp	.L3020
.L3022:
	leaq	.LC152(%rip), %r9
	jmp	.L3027
.L3024:
	leaq	.LC151(%rip), %r9
	jmp	.L3027
.L3025:
	leaq	.LC82(%rip), %r9
	jmp	.L3027
.L3017:
	leaq	.LC151(%rip), %rcx
	jmp	.L3020
.L3018:
	leaq	.LC82(%rip), %rcx
	jmp	.L3020
.L3071:
	movq	-168(%rbp), %r12
	jmp	.L3010
.L3069:
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZSt21__inplace_stable_sortIN9__gnu_cxx17__normal_iteratorIPN2v88internal4wasm10WasmExportESt6vectorIS5_SaIS5_EEEENS0_5__ops15_Iter_comp_iterIZNS4_17ModuleDecoderImpl19DecodeExportSectionEvEUlRKS5_SF_E_EEEvT_SI_T0_
	jmp	.L3009
.L3038:
	movq	%rax, %r13
	xorl	%edx, %edx
	jmp	.L3006
.L3021:
	leaq	.LC149(%rip), %r9
	jmp	.L3027
.L3014:
	leaq	.LC149(%rip), %rcx
	jmp	.L3020
.L3068:
	call	_ZSt17__throw_bad_allocv@PLT
.L3064:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19025:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv, .-_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb.str1.1,"aMS",@progbits,1
.LC160:
	.string	"unexpected section <%s>"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb.str1.8,"aMS",@progbits,1
	.align 8
.LC161:
	.string	"Multiple %s sections not allowed"
	.align 8
.LC162:
	.string	"The %s section must appear before the %s section"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb.str1.1
.LC163:
	.string	"types count"
.LC164:
	.string	"table count"
.LC165:
	.string	"table elements"
.LC166:
	.string	"memory count"
.LC167:
	.string	"start function index"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb.str1.8
	.align 8
.LC168:
	.string	"invalid start function: non-zero parameter or return count"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb.str1.1
.LC169:
	.string	"body size"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb.str1.8
	.align 8
.LC170:
	.string	"size %u > maximum function size %zu"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb.str1.1
.LC171:
	.string	"exception count"
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb.str1.8
	.align 8
.LC172:
	.string	"section was %s than expected size (%zu bytes expected, %zu decoded)"
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb, @function
_ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb:
.LFB19017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -208(%rbp)
	movq	%rcx, -216(%rbp)
	movb	%sil, -200(%rbp)
	movb	%r9b, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 56(%rdi)
	je	.L3316
.L3073:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3317
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3316:
	.cfi_restore_state
	movq	%rdx, %rax
	movq	%rdx, 8(%rdi)
	leaq	48(%rdi), %r14
	movq	%rdi, %rbx
	addq	%rcx, %rax
	movq	%rdx, 16(%rdi)
	movl	%esi, %r12d
	leaq	-120(%rbp), %r13
	movq	%rax, 24(%rdi)
	leaq	-136(%rbp), %r15
	movl	%r8d, 32(%rdi)
	movq	%r15, %rsi
	movl	$0, 40(%rdi)
	movq	%r14, %rdi
	movq	%rax, -232(%rbp)
	movl	$0, -140(%rbp)
	movl	$0, -144(%rbp)
	movq	%r13, -136(%rbp)
	movq	$0, -128(%rbp)
	movb	$0, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_@PLT
	movq	-136(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3076
	call	_ZdlPv@PLT
.L3076:
	movzbl	120(%rbx), %eax
	movsbl	%r12b, %edi
	cmpl	%eax, %edi
	jge	.L3077
	cmpb	$11, %r12b
	jle	.L3090
.L3077:
	cmpb	$13, %r12b
	je	.L3078
	jg	.L3079
	testb	%r12b, %r12b
	je	.L3080
	cmpb	$12, %r12b
	jne	.L3082
	movl	124(%rbx), %edx
	movl	$12, %edi
	testb	$16, %dh
	jne	.L3314
	orb	$16, %dh
	movl	%edx, 124(%rbx)
	cmpl	$10, %eax
	jg	.L3318
	je	.L3086
	movb	$10, 120(%rbx)
.L3086:
	cmpb	$0, 89(%rbx)
	je	.L3177
	movq	96(%rbx), %r12
	movl	$100000, %edx
	leaq	.LC129(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm
	movl	%eax, 76(%r12)
	movq	16(%rbx), %r14
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3079:
	leal	-14(%r12), %eax
	cmpb	$2, %al
	jbe	.L3083
.L3082:
	leal	1(%r12), %eax
	movb	%al, 120(%rbx)
.L3083:
	cmpb	$16, %r12b
	ja	.L3090
	movzbl	-200(%rbp), %r12d
	leaq	.L3092(%rip), %rdx
	movslq	(%rdx,%r12,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb,"aG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb,comdat
	.align 4
	.align 4
.L3092:
	.long	.L3080-.L3092
	.long	.L3105-.L3092
	.long	.L3104-.L3092
	.long	.L3103-.L3092
	.long	.L3102-.L3092
	.long	.L3101-.L3092
	.long	.L3100-.L3092
	.long	.L3099-.L3092
	.long	.L3098-.L3092
	.long	.L3097-.L3092
	.long	.L3096-.L3092
	.long	.L3095-.L3092
	.long	.L3086-.L3092
	.long	.L3090-.L3092
	.long	.L3094-.L3092
	.long	.L3093-.L3092
	.long	.L3091-.L3092
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb,comdat
	.p2align 4,,10
	.p2align 3
.L3078:
	movl	124(%rbx), %edx
	movl	%edx, %r12d
	andl	$8192, %r12d
	jne	.L3319
	orb	$32, %dh
	movl	%edx, 124(%rbx)
	cmpl	$7, %eax
	jg	.L3320
	je	.L3089
	movb	$7, 120(%rbx)
.L3089:
	cmpb	$0, 81(%rbx)
	je	.L3178
	movl	$1000000, %edx
	leaq	.LC171(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm
	cmpq	$0, 56(%rbx)
	movl	%eax, -200(%rbp)
	jne	.L3080
	leaq	-176(%rbp), %rsi
	leaq	-180(%rbp), %r13
	movq	%rsi, -224(%rbp)
	testl	%eax, %eax
	jne	.L3179
	jmp	.L3080
	.p2align 4,,10
	.p2align 3
.L3181:
	movq	16(%rbx), %r14
	movq	96(%rbx), %r15
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC69(%rip), %rcx
	movl	$0, -180(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	96(%r15), %r8
	movl	%eax, %edx
	movq	88(%r15), %rax
	movq	%rdx, %rcx
	subq	%rax, %r8
	sarq	$3, %r8
	cmpq	%r8, %rdx
	jnb	.L3321
	movq	(%rax,%rdx,8), %rdx
	movq	%rdx, -176(%rbp)
	testq	%rdx, %rdx
	je	.L3183
	cmpq	$0, (%rdx)
	jne	.L3322
.L3183:
	movq	96(%rbx), %rax
	movq	264(%rax), %rsi
	cmpq	272(%rax), %rsi
	je	.L3184
	movq	%rdx, (%rsi)
	addl	$1, %r12d
	addq	$8, 264(%rax)
	cmpq	$0, 56(%rbx)
	jne	.L3080
.L3312:
	cmpl	%r12d, -200(%rbp)
	jbe	.L3080
.L3179:
	movq	16(%rbx), %r14
	leaq	.LC85(%rip), %rcx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	$0, -176(%rbp)
	movl	$0, -180(%rbp)
	movq	%r14, %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	testl	%eax, %eax
	je	.L3181
	movl	%eax, %ecx
	leaq	.LC86(%rip), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3181
	.p2align 4,,10
	.p2align 3
.L3093:
	movq	24(%rbx), %rdx
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %r12
	movb	$0, -80(%rbp)
	movl	32(%rbx), %eax
	movq	16(%rbx), %rcx
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r13
	leaq	-144(%rbp), %rdi
	movq	%rsi, -136(%rbp)
	movl	$1, %esi
	movq	%rdx, -120(%rbp)
	leaq	.LC67(%rip), %rdx
	movq	%r13, -144(%rbp)
	movq	%rcx, -128(%rbp)
	movl	%eax, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	%r12, -96(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_114consume_stringEPNS1_7DecoderEbPKc.constprop.0
	cmpq	$0, -88(%rbp)
	jne	.L3174
	testb	$-128, 125(%rbx)
	je	.L3323
.L3174:
	movq	24(%rbx), %rsi
	movq	%rbx, %rdi
	subq	8(%rbx), %rsi
	call	_ZN2v88internal4wasm7Decoder13consume_bytesEjPKc.isra.0
	movq	-96(%rbp), %rdi
	movq	%r13, -144(%rbp)
	cmpq	%r12, %rdi
	je	.L3080
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3080:
	movq	16(%rbx), %r14
.L3116:
	cmpq	%r14, -232(%rbp)
	je	.L3073
	leaq	.LC88(%rip), %rax
	movq	%r14, %r9
	movq	%r14, %rsi
	movq	%rbx, %rdi
	leaq	.LC89(%rip), %rcx
	movq	-216(%rbp), %r8
	leaq	.LC172(%rip), %rdx
	cmovbe	%rax, %rcx
	subq	-208(%rbp), %r9
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3073
	.p2align 4,,10
	.p2align 3
.L3090:
	call	_ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE
	movq	16(%rbx), %rsi
	leaq	.LC160(%rip), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3073
	.p2align 4,,10
	.p2align 3
.L3319:
	movl	$13, %edi
.L3314:
	call	_ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE
	movq	16(%rbx), %rsi
	leaq	.LC161(%rip), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3073
	.p2align 4,,10
	.p2align 3
.L3095:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeDataSectionEv
	movq	16(%rbx), %r14
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3105:
	movl	$1000000, %edx
	leaq	.LC163(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm
	movq	96(%rbx), %r15
	movl	%eax, %edx
	movq	88(%r15), %r14
	movq	104(%r15), %rax
	movq	%rdx, %r13
	subq	%r14, %rax
	sarq	$3, %rax
	cmpq	%rax, %rdx
	ja	.L3324
.L3106:
	cmpq	$0, 56(%rbx)
	jne	.L3111
	testl	%r13d, %r13d
	je	.L3111
	xorl	%r12d, %r12d
	leaq	-176(%rbp), %r14
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L3325:
	movq	%rax, (%rsi)
	addq	$8, 96(%rdi)
.L3113:
	movq	-176(%rbp), %rsi
	movq	96(%rbx), %rdi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3115
	addq	$328, %rdi
	call	_ZN2v88internal4wasm12SignatureMap12FindOrInsertERKNS0_9SignatureINS1_9ValueTypeEEE@PLT
	movq	96(%rbx), %rdi
.L3115:
	movl	%eax, -180(%rbp)
	movq	120(%rdi), %rsi
	cmpq	128(%rdi), %rsi
	je	.L3117
	movl	%eax, (%rsi)
	addl	$1, %r12d
	addq	$4, 120(%rdi)
	cmpq	$0, 56(%rbx)
	movq	96(%rbx), %r15
	jne	.L3111
.L3309:
	cmpl	%r12d, %r13d
	jbe	.L3111
.L3120:
	movq	(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl11consume_sigEPNS0_4ZoneE
	movq	96(%rbx), %rdi
	movq	%rax, -176(%rbp)
	movq	96(%rdi), %rsi
	cmpq	104(%rdi), %rsi
	jne	.L3325
	addq	$88, %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIPN2v88internal9SignatureINS1_4wasm9ValueTypeEEESaIS6_EE17_M_realloc_insertIJRKS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L3113
	.p2align 4,,10
	.p2align 3
.L3104:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeImportSectionEv
	movq	16(%rbx), %r14
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3103:
	movl	$1000000, %edx
	leaq	.LC103(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm
	cmpb	$0, 168(%rbx)
	movl	%eax, -224(%rbp)
	movq	112(%rbx), %rax
	jne	.L3121
	leaq	1008(%rax), %rdi
.L3122:
	movl	-224(%rbp), %r15d
	movl	%r15d, %esi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	96(%rbx), %r13
	movl	%r15d, %edx
	movq	136(%r13), %rdi
	movq	152(%r13), %rax
	addl	60(%r13), %edx
	subq	%rdi, %rax
	sarq	$5, %rax
	cmpq	%rax, %rdx
	ja	.L3326
.L3123:
	movl	-224(%rbp), %eax
	xorl	%r14d, %r14d
	leaq	-176(%rbp), %r12
	movl	%eax, 68(%r13)
	testl	%eax, %eax
	jne	.L3129
	jmp	.L3080
	.p2align 4,,10
	.p2align 3
.L3132:
	movq	(%rax,%rdx,8), %rax
	movq	%rax, -32(%r15)
.L3133:
	movl	%ecx, -20(%r15)
	cmpq	$0, 56(%rbx)
	jne	.L3080
	addl	$1, %r14d
	cmpl	%r14d, -224(%rbp)
	je	.L3080
.L3129:
	movq	96(%rbx), %rdi
	xorl	%r8d, %r8d
	movq	144(%rdi), %rsi
	movq	%rsi, %rax
	subq	136(%rdi), %rax
	movq	$0, -176(%rbp)
	sarq	$5, %rax
	movw	%r8w, -152(%rbp)
	movl	%eax, -168(%rbp)
	movl	$0, -164(%rbp)
	movq	$0, -160(%rbp)
	cmpq	152(%rdi), %rsi
	je	.L3130
	movdqa	-176(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movdqa	-160(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	addq	$32, 144(%rdi)
.L3131:
	movq	16(%rbx), %r13
	movq	96(%rbx), %r8
	movq	%r12, %rdx
	movq	%rbx, %rdi
	leaq	.LC69(%rip), %rcx
	movq	%r13, %rsi
	movq	144(%r8), %r15
	movq	%r8, -200(%rbp)
	movl	$0, -176(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	-200(%rbp), %r8
	movl	%eax, %edx
	movq	88(%r8), %rax
	movq	96(%r8), %r8
	movq	%rdx, %rcx
	subq	%rax, %r8
	sarq	$3, %r8
	cmpq	%r8, %rdx
	jb	.L3132
	leaq	.LC70(%rip), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	$0, -32(%r15)
	xorl	%ecx, %ecx
	jmp	.L3133
	.p2align 4,,10
	.p2align 3
.L3102:
	cmpb	$1, 87(%rbx)
	leaq	.LC164(%rip), %rsi
	movq	%rbx, %rdi
	sbbq	%rdx, %rdx
	andq	$-99999, %rdx
	addq	$100000, %rdx
	call	_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm
	cmpq	$0, 56(%rbx)
	movl	%eax, %r12d
	jne	.L3080
	xorl	%r13d, %r13d
	testl	%eax, %eax
	je	.L3080
.L3137:
	movq	96(%rbx), %rax
	cmpb	$0, 87(%rbx)
	movq	192(%rax), %rsi
	jne	.L3139
	cmpq	%rsi, 184(%rax)
	je	.L3139
	leaq	.LC71(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%rbx), %r14
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3101:
	movl	$1, %edx
	leaq	.LC166(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl13consume_countEPKcm
	cmpq	$0, 56(%rbx)
	movl	%eax, %r12d
	jne	.L3080
	xorl	%r13d, %r13d
	testl	%eax, %eax
	je	.L3080
.L3150:
	movq	96(%rbx), %rax
	cmpb	$0, 18(%rax)
	jne	.L3327
	movb	$1, 18(%rax)
	movq	96(%rbx), %r15
	movq	16(%rbx), %rsi
	cmpl	%esi, 24(%rbx)
	je	.L3153
	movzbl	(%rsi), %r14d
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%rbx)
	movb	$0, 16(%r15)
	cmpb	$0, 82(%rbx)
	movl	%r14d, %eax
	je	.L3328
	testb	$-4, %al
	jne	.L3310
	cmpb	$3, %r14b
	je	.L3329
	cmpb	$2, %r14b
	je	.L3330
	.p2align 4,,10
	.p2align 3
.L3156:
	movq	96(%rbx), %rax
	subq	$8, %rsp
	movl	$65536, %ecx
	movq	%rbx, %rdi
	pushq	%r14
	leaq	.LC82(%rip), %rsi
	addl	$1, %r13d
	leaq	12(%rax), %rdx
	leaq	17(%rax), %r9
	pushq	%rdx
	leaq	8(%rax), %r8
	leaq	.LC81(%rip), %rdx
	pushq	$65536
	call	_ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h
	addq	$32, %rsp
	cmpq	$0, 56(%rbx)
	jne	.L3080
	cmpl	%r13d, %r12d
	ja	.L3150
	jmp	.L3080
	.p2align 4,,10
	.p2align 3
.L3100:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeGlobalSectionEv
	movq	16(%rbx), %r14
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3099:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl19DecodeExportSectionEv
	movq	16(%rbx), %r14
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3098:
	movq	16(%rbx), %r12
	movq	96(%rbx), %r13
	movq	%rbx, %rdi
	leaq	-176(%rbp), %rdx
	leaq	.LC167(%rip), %rcx
	movl	$0, -176(%rbp)
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movq	144(%r13), %rcx
	movl	%eax, %edx
	movq	136(%r13), %rax
	movq	%rdx, %r8
	subq	%rax, %rcx
	movq	%rcx, %r9
	sarq	$5, %r9
	cmpq	%r9, %rdx
	jnb	.L3331
	movq	96(%rbx), %rcx
	salq	$5, %rdx
	movl	%r8d, 20(%rcx)
	addq	%rdx, %rax
	je	.L3080
	movq	(%rax), %rax
	cmpq	$0, 8(%rax)
	jne	.L3163
	cmpq	$0, (%rax)
	je	.L3080
.L3163:
	leaq	.LC168(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	16(%rbx), %r14
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3091:
	cmpb	$0, 86(%rbx)
	je	.L3176
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl29DecodeCompilationHintsSectionEv
	movq	16(%rbx), %r14
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3094:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl17DecodeNameSectionEv
	movq	16(%rbx), %r14
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3097:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl20DecodeElementSectionEv
	movq	16(%rbx), %r14
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3096:
	movq	8(%rbx), %rax
	movq	16(%rbx), %rsi
	movq	%rbx, %rdi
	leaq	-176(%rbp), %r12
	leaq	.LC103(%rip), %rcx
	movq	%r12, %rdx
	movl	$0, -176(%rbp)
	movq	%rax, -248(%rbp)
	movl	32(%rbx), %eax
	movq	%rsi, -240(%rbp)
	movl	%eax, -252(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movl	%eax, %esi
	movl	%eax, -200(%rbp)
	movq	96(%rbx), %rax
	cmpl	68(%rax), %esi
	movq	-240(%rbp), %rsi
	jne	.L3332
.L3165:
	cmpq	$0, 56(%rbx)
	movq	16(%rbx), %r14
	jne	.L3116
	movl	-200(%rbp), %eax
	testl	%eax, %eax
	je	.L3116
	xorl	%r13d, %r13d
	jmp	.L3173
	.p2align 4,,10
	.p2align 3
.L3170:
	addl	$1, %r13d
	cmpq	$0, 56(%rbx)
	movq	16(%rbx), %r14
	jne	.L3116
.L3311:
	cmpl	%r13d, -200(%rbp)
	jbe	.L3116
.L3173:
	leaq	.LC169(%rip), %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	$0, -176(%rbp)
	call	_ZN2v88internal4wasm7Decoder13read_leb_tailIjLNS2_12ValidateFlagE1ELNS2_13AdvancePCFlagE1ELNS2_9TraceFlagE1ELi0EEET_PKhPjPKcS7_.constprop.2
	movl	%eax, %ecx
	cmpl	$7654321, %eax
	ja	.L3333
	movq	16(%rbx), %rsi
	movq	24(%rbx), %rax
	subq	%rsi, %rax
	movq	%rsi, %r15
	subq	8(%rbx), %r15
	addl	32(%rbx), %r15d
	cmpl	%eax, %ecx
	ja	.L3168
	movl	%ecx, %r14d
	addq	%rsi, %r14
	movq	%r14, 16(%rbx)
.L3169:
	cmpq	$0, 56(%rbx)
	jne	.L3116
	movq	96(%rbx), %rax
	movl	%r13d, %r9d
	addl	60(%rax), %r9d
	salq	$5, %r9
	addq	136(%rax), %r9
	cmpb	$0, -224(%rbp)
	movl	%r15d, 16(%r9)
	movl	%ecx, 20(%r9)
	je	.L3170
	movq	8(%rbx), %rdx
	movq	24(%rbx), %rax
	movq	%r12, %rcx
	movq	%rbx, %rdi
	movq	96(%rbx), %r8
	subq	%rdx, %rax
	movq	%rdx, -176(%rbp)
	cltq
	movq	%rax, -168(%rbp)
	movq	(%r8), %rax
	movl	60(%r8), %edx
	movq	32(%rax), %rsi
	addl	%r13d, %edx
	addl	$1, %r13d
	call	_ZN2v88internal4wasm17ModuleDecoderImpl18VerifyFunctionBodyEPNS0_19AccountingAllocatorEjRKNS1_15ModuleWireBytesEPKNS1_10WasmModuleEPNS1_12WasmFunctionE
	cmpq	$0, 56(%rbx)
	movq	16(%rbx), %r14
	je	.L3311
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3130:
	addq	$136, %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIN2v88internal4wasm12WasmFunctionESaIS3_EE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L3139:
	cmpq	%rsi, 200(%rax)
	je	.L3140
	xorl	%edi, %edi
	movb	$0, (%rsi)
	movq	$0, 4(%rsi)
	movw	%di, 12(%rsi)
	movb	$0, 14(%rsi)
	addq	$16, 192(%rax)
.L3141:
	movq	96(%rbx), %rax
	movq	16(%rbx), %rsi
	movq	192(%rax), %r14
	cmpl	%esi, 24(%rbx)
	je	.L3142
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%rbx)
	cmpb	$111, %al
	je	.L3143
	movl	$7, %r15d
	cmpb	$112, %al
	jne	.L3145
.L3144:
	movb	%r15b, -16(%r14)
	movq	16(%rbx), %rsi
	cmpl	%esi, 24(%rbx)
	je	.L3146
	movzbl	(%rsi), %r15d
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%rbx)
	movl	%r15d, %eax
	testb	$-2, %al
	je	.L3148
	leaq	.LC165(%rip), %rcx
	leaq	.LC76(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L3148:
	subq	$8, %rsp
	leaq	-8(%r14), %rax
	leaq	-4(%r14), %r9
	movq	%rbx, %rdi
	movl	_ZN2v88internal24FLAG_wasm_max_table_sizeE(%rip), %ecx
	pushq	%r15
	leaq	-12(%r14), %r8
	addl	$1, %r13d
	pushq	%rax
	leaq	.LC77(%rip), %rdx
	leaq	.LC165(%rip), %rsi
	pushq	%rcx
	call	_ZN2v88internal4wasm17ModuleDecoderImpl24consume_resizable_limitsEPKcS4_jPjPbjS5_h
	addq	$32, %rsp
	cmpq	$0, 56(%rbx)
	jne	.L3080
	cmpl	%r13d, %r12d
	ja	.L3137
	jmp	.L3080
	.p2align 4,,10
	.p2align 3
.L3117:
	addq	$112, %rdi
	leaq	-180(%rbp), %rdx
	addl	$1, %r12d
	call	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	cmpq	$0, 56(%rbx)
	movq	96(%rbx), %r15
	je	.L3309
	.p2align 4,,10
	.p2align 3
.L3111:
	movb	$1, 328(%r15)
	movq	16(%rbx), %r14
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3168:
	leaq	.LC39(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	movl	%ecx, -240(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%rbx), %r14
	movl	-240(%rbp), %ecx
	movq	%r14, 16(%rbx)
	jmp	.L3169
	.p2align 4,,10
	.p2align 3
.L3321:
	leaq	.LC70(%rip), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	movq	$0, -176(%rbp)
	jmp	.L3183
	.p2align 4,,10
	.p2align 3
.L3327:
	leaq	.LC78(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKc
	movq	16(%rbx), %r14
	jmp	.L3116
	.p2align 4,,10
	.p2align 3
.L3328:
	testb	$-2, %al
	je	.L3156
.L3310:
	leaq	.LC79(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3156
	.p2align 4,,10
	.p2align 3
.L3184:
	movq	-224(%rbp), %rdx
	leaq	256(%rax), %rdi
	addl	$1, %r12d
	call	_ZNSt6vectorIN2v88internal4wasm13WasmExceptionESaIS3_EE17_M_realloc_insertIJRPNS1_9SignatureINS2_9ValueTypeEEEEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	cmpq	$0, 56(%rbx)
	je	.L3312
	jmp	.L3080
	.p2align 4,,10
	.p2align 3
.L3318:
	movl	$10, %edi
	call	_ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE
	movl	$12, %edi
	movq	%rax, %r8
.L3315:
	call	_ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE
	movq	16(%rbx), %rsi
	leaq	.LC162(%rip), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3073
	.p2align 4,,10
	.p2align 3
.L3320:
	movl	$7, %edi
	call	_ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE
	movl	$13, %edi
	movq	%rax, %r8
	jmp	.L3315
.L3322:
	leaq	.LC87(%rip), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	xorl	%edx, %edx
	movq	$0, -176(%rbp)
	jmp	.L3183
.L3177:
	movl	$12, %edi
.L3313:
	call	_ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE
	movq	16(%rbx), %rsi
	leaq	.LC160(%rip), %rdx
	movq	%rbx, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%rbx), %r14
	jmp	.L3116
.L3142:
	movl	$1, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%rbx), %rsi
	movq	%rsi, 16(%rbx)
	subq	$1, %rsi
.L3145:
	leaq	.LC74(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L3144
.L3143:
	cmpb	$0, 87(%rbx)
	movl	$6, %r15d
	jne	.L3144
	leaq	.LC72(%rip), %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	jmp	.L3144
.L3178:
	movl	$13, %edi
	jmp	.L3313
.L3176:
	movq	24(%rbx), %rsi
	movq	%rbx, %rdi
	subq	8(%rbx), %rsi
	call	_ZN2v88internal4wasm7Decoder13consume_bytesEjPKc.isra.0
	movq	16(%rbx), %r14
	jmp	.L3116
.L3121:
	leaq	968(%rax), %rdi
	jmp	.L3122
.L3331:
	cmpq	$32, %rcx
	leaq	.LC139(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC138(%rip), %rax
	leaq	.LC167(%rip), %rcx
	cmovne	%rdx, %rax
	subq	$8, %rsp
	leaq	.LC147(%rip), %rdx
	pushq	%rax
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	96(%rbx), %rax
	movl	$0, 20(%rax)
	popq	%rdx
	movq	16(%rbx), %r14
	popq	%rcx
	jmp	.L3116
.L3326:
	movq	144(%r13), %rcx
	movq	%rdx, %r15
	xorl	%r14d, %r14d
	salq	$5, %r15
	movq	%rcx, %r12
	subq	%rdi, %r12
	testq	%rdx, %rdx
	je	.L3124
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	144(%r13), %rcx
	movq	136(%r13), %rdi
	movq	%rax, %r14
.L3124:
	movq	%r14, %rdx
	movq	%rdi, %rax
	cmpq	%rdi, %rcx
	je	.L3128
	.p2align 4,,10
	.p2align 3
.L3125:
	movdqu	(%rax), %xmm3
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm3, -32(%rdx)
	movdqu	-16(%rax), %xmm4
	movups	%xmm4, -16(%rdx)
	cmpq	%rcx, %rax
	jne	.L3125
.L3128:
	testq	%rdi, %rdi
	je	.L3127
	call	_ZdlPv@PLT
.L3127:
	addq	%r14, %r12
	leaq	(%r14,%r15), %rsi
	movq	%r14, 136(%r13)
	movq	%r12, 144(%r13)
	movq	%rsi, 152(%r13)
	movq	96(%rbx), %r13
	jmp	.L3123
.L3332:
	subq	-248(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movb	$0, -120(%rbp)
	addl	-252(%rbp), %esi
	movq	$0, 24(%rbx)
	movl	%esi, 32(%rbx)
	movq	%r15, %rsi
	movl	$0, 40(%rbx)
	movups	%xmm0, 8(%rbx)
	movl	$0, -140(%rbp)
	movl	$0, -144(%rbp)
	movq	%r13, -136(%rbp)
	movq	$0, -128(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEaSEOS4_@PLT
	movq	-136(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L3166
	call	_ZdlPv@PLT
.L3166:
	movq	96(%rbx), %rax
	movl	-200(%rbp), %ecx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	.LC47(%rip), %rdx
	movl	68(%rax), %r8d
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3165
.L3324:
	movq	96(%r15), %r12
	leaq	0(,%rdx,8), %rcx
	subq	%r14, %r12
	testq	%rdx, %rdx
	je	.L3191
	movq	%rcx, %rdi
	movq	%rcx, -200(%rbp)
	call	_Znwm@PLT
	movq	88(%r15), %r14
	movq	96(%r15), %rdx
	movq	-200(%rbp), %rcx
	movq	%rax, %r8
	subq	%r14, %rdx
.L3107:
	testq	%rdx, %rdx
	jg	.L3334
	testq	%r14, %r14
	jne	.L3109
.L3110:
	addq	%r8, %r12
	addq	%r8, %rcx
	movq	%r8, 88(%r15)
	movq	%r12, 96(%r15)
	movq	%rcx, 104(%r15)
	movq	96(%rbx), %r15
	jmp	.L3106
.L3140:
	leaq	184(%rax), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm9WasmTableESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L3141
.L3333:
	movq	%r14, %rsi
	movl	$7654321, %r8d
	movq	%rbx, %rdi
	xorl	%eax, %eax
	leaq	.LC170(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	16(%rbx), %r14
	jmp	.L3116
.L3329:
	movb	$1, 16(%r15)
	movl	$3, %r14d
	jmp	.L3156
.L3334:
	movq	%r8, %rdi
	movq	%r14, %rsi
	movq	%rcx, -200(%rbp)
	call	memmove@PLT
	movq	-200(%rbp), %rcx
	movq	%rax, %r8
.L3109:
	movq	%r14, %rdi
	movq	%r8, -224(%rbp)
	movq	%rcx, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-224(%rbp), %r8
	movq	-200(%rbp), %rcx
	jmp	.L3110
.L3323:
	movq	96(%rbx), %rdx
	movl	%eax, %ecx
	shrq	$32, %rax
	subl	-112(%rbp), %ecx
	movq	%rax, %r8
	addq	-136(%rbp), %rcx
	xorl	%esi, %esi
	movq	416(%rdx), %r9
	leaq	408(%rdx), %rdi
	movq	%r9, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	orl	$32768, 124(%rbx)
	jmp	.L3174
.L3153:
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%rbx), %rax
	movq	%rax, 16(%rbx)
	movb	$0, 16(%r15)
	jmp	.L3156
.L3146:
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%rbx, %rdi
	xorl	%r15d, %r15d
	leaq	.LC39(%rip), %rdx
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	24(%rbx), %rax
	movq	%rax, 16(%rbx)
	jmp	.L3148
.L3191:
	movq	%r12, %rdx
	xorl	%r8d, %r8d
	jmp	.L3107
.L3330:
	leaq	.LC80(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	movl	$2, %r14d
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	jmp	.L3156
.L3317:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19017:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb, .-_ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb
	.section	.text._ZN2v88internal4wasm17ModuleDecoderImpl12DecodeModuleEPNS0_8CountersEPNS0_19AccountingAllocatorEb,"axG",@progbits,_ZN2v88internal4wasm17ModuleDecoderImpl12DecodeModuleEPNS0_8CountersEPNS0_19AccountingAllocatorEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm17ModuleDecoderImpl12DecodeModuleEPNS0_8CountersEPNS0_19AccountingAllocatorEb
	.type	_ZN2v88internal4wasm17ModuleDecoderImpl12DecodeModuleEPNS0_8CountersEPNS0_19AccountingAllocatorEb, @function
_ZN2v88internal4wasm17ModuleDecoderImpl12DecodeModuleEPNS0_8CountersEPNS0_19AccountingAllocatorEb:
.LFB19048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movb	%r8b, -376(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 96(%rsi)
	jne	.L3411
	movq	%rdx, 112(%rsi)
	movq	%rdi, %r12
	movl	$64, %edi
	movq	%rcx, %r14
	movl	%r8d, %ebx
	movq	%rsi, %r15
	call	_Znwm@PLT
	leaq	.LC34(%rip), %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc@PLT
	movl	$440, %edi
	movq	%r13, -352(%rbp)
	leaq	-352(%rbp), %r13
	call	_Znwm@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal4wasm10WasmModuleC1ESt10unique_ptrINS0_4ZoneESt14default_deleteIS4_EE@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	movq	104(%r15), %rdi
	movq	%r14, %xmm0
	movabsq	$4294967297, %rcx
	movq	%rax, %xmm1
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, (%rax)
	movq	%r14, 16(%rax)
	movups	%xmm0, 96(%r15)
	testq	%rdi, %rdi
	je	.L3338
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L3339
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
	cmpl	$1, %eax
	je	.L3412
	.p2align 4,,10
	.p2align 3
.L3338:
	movq	-352(%rbp), %r14
	testq	%r14, %r14
	je	.L3344
	movq	%r14, %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3344:
	movq	96(%r15), %rax
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	leaq	40(%r12), %r14
	movl	$0, 8(%rax)
	movq	96(%r15), %rax
	movl	$0, 12(%rax)
	movq	96(%r15), %rax
	movb	$0, 19(%rax)
	movzbl	168(%r15), %edx
	movq	96(%r15), %rax
	movb	%dl, 392(%rax)
	movq	8(%r15), %rsi
	movq	24(%r15), %rdx
	subq	%rsi, %rdx
	call	_ZN2v88internal4wasm17ModuleDecoderImpl18DecodeModuleHeaderENS0_6VectorIKhEEh
	cmpq	$0, 56(%r15)
	je	.L3345
	leaq	-144(%rbp), %r13
	movzbl	%bl, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl14FinishDecodingEb
	movl	-128(%rbp), %eax
	movq	-120(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-144(%rbp), %xmm2
	movq	%r14, 24(%r12)
	movl	%eax, 16(%r12)
	leaq	-104(%rbp), %rax
	movups	%xmm2, (%r12)
	movaps	%xmm0, -144(%rbp)
	cmpq	%rax, %rdx
	je	.L3413
	movq	%rdx, 24(%r12)
	movq	-104(%rbp), %rdx
	movq	%rdx, 40(%r12)
.L3347:
	movq	-112(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	%rdx, 32(%r12)
	movb	$0, -104(%rbp)
	call	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED1Ev
.L3335:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3414
	addq	$360, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3345:
	.cfi_restore_state
	movq	24(%r15), %rdx
	movq	8(%r15), %rax
	movq	%r13, %rdi
	movb	$0, -80(%rbp)
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rcx
	movl	$8, -112(%rbp)
	addq	$8, %rax
	movq	%rcx, -144(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%rdx, -120(%rbp)
	leaq	-144(%rbp), %rdx
	movq	%rax, -136(%rbp)
	movq	%rax, -128(%rbp)
	movl	$0, -104(%rbp)
	movq	%rcx, -384(%rbp)
	movq	%rcx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -352(%rbp)
	movb	$0, -344(%rbp)
	movq	%rax, -336(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_119WasmSectionIterator4nextEv
	cmpq	$0, 56(%r15)
	jne	.L3349
	movzbl	-376(%rbp), %eax
	movl	$8, %r11d
	movl	%ebx, -376(%rbp)
	movq	%r13, %rbx
	movl	%r11d, %r13d
	movl	%eax, -392(%rbp)
.L3360:
	movq	-352(%rbp), %rdi
	cmpq	$0, 56(%rdi)
	jne	.L3408
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	jnb	.L3408
	movq	-328(%rbp), %rdx
	movzbl	-344(%rbp), %eax
	movq	%rdx, %r8
	subq	-336(%rbp), %r8
	addl	%r8d, %r13d
	movq	-320(%rbp), %r8
	movq	%r8, %rcx
	subq	%rdx, %rcx
	testb	%al, %al
	jne	.L3415
.L3351:
	addl	%ecx, %r13d
	cmpq	%rsi, %r8
	ja	.L3416
	leaq	.LC88(%rip), %rcx
	je	.L3358
.L3359:
	movq	-336(%rbp), %rax
	movq	%rsi, %r9
	leaq	.LC90(%rip), %rdx
	subq	%rax, %r9
	subl	%eax, %r8d
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
.L3358:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_119WasmSectionIterator4nextEv
	cmpq	$0, 56(%r15)
	je	.L3360
	.p2align 4,,10
	.p2align 3
.L3408:
	movl	-376(%rbp), %ebx
.L3349:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L3417
	movq	-96(%rbp), %rsi
	movl	-104(%rbp), %eax
	leaq	-280(%rbp), %rbx
	leaq	-296(%rbp), %rdi
	movq	%rbx, -296(%rbp)
	addq	%rsi, %rdx
	movl	%eax, -304(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPcEEvT_S7_St20forward_iterator_tag
	movq	-296(%rbp), %rax
	movl	-304(%rbp), %ecx
	cmpq	%rbx, %rax
	je	.L3418
	movq	-280(%rbp), %rsi
	movq	-288(%rbp), %rdx
	movq	%rbx, -296(%rbp)
	leaq	-224(%rbp), %rbx
	movl	%ecx, -248(%rbp)
	leaq	-176(%rbp), %rcx
	movq	%rax, -192(%rbp)
	movq	%rsi, -176(%rbp)
	movq	%rdx, -184(%rbp)
	movq	$0, -288(%rbp)
	movb	$0, -280(%rbp)
	movq	$0, -256(%rbp)
	movq	%rbx, -240(%rbp)
	cmpq	%rcx, %rax
	je	.L3362
	movq	%rax, -240(%rbp)
	movq	%rsi, -224(%rbp)
.L3364:
	pxor	%xmm0, %xmm0
	leaq	-256(%rbp), %rsi
	leaq	-360(%rbp), %rdi
	movq	%rdx, -232(%rbp)
	movaps	%xmm0, -208(%rbp)
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN2v88internal4wasm10WasmModuleESt14default_deleteIS7_EEEOSt10unique_ptrIT_T0_E
	movq	-360(%rbp), %r15
	movq	-200(%rbp), %r13
	cmpq	%r13, %r15
	je	.L3365
	testq	%r15, %r15
	je	.L3366
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L3367
	lock addl	$1, 8(%r15)
	movq	-200(%rbp), %r13
.L3366:
	testq	%r13, %r13
	je	.L3368
	movq	%r13, %rdi
	call	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_releaseEv
.L3368:
	movq	%r15, -200(%rbp)
	movq	-360(%rbp), %r13
.L3365:
	testq	%r13, %r13
	je	.L3369
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L3370
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L3371:
	cmpl	$1, %eax
	je	.L3372
.L3410:
	movq	-200(%rbp), %r15
.L3369:
	movl	-248(%rbp), %eax
	movq	-240(%rbp), %rdx
	movl	%eax, -192(%rbp)
	cmpq	%rbx, %rdx
	je	.L3419
	movq	-224(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movl	%eax, 16(%r12)
	pxor	%xmm0, %xmm0
	leaq	-168(%rbp), %rax
	movq	-232(%rbp), %rsi
	movq	%rbx, -240(%rbp)
	movq	%rcx, -168(%rbp)
	movq	$0, -232(%rbp)
	movb	$0, -224(%rbp)
	movq	%rdi, (%r12)
	movq	%r15, 8(%r12)
	movq	%r14, 24(%r12)
	movaps	%xmm0, -208(%rbp)
	cmpq	%rax, %rdx
	je	.L3377
	movq	%rdx, 24(%r12)
	movq	%rcx, 40(%r12)
.L3379:
	movq	%rsi, 32(%r12)
	leaq	-208(%rbp), %rdi
	movq	%rax, -184(%rbp)
	movq	$0, -176(%rbp)
	movb	$0, -168(%rbp)
	call	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED1Ev
	movq	-240(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3380
	call	_ZdlPv@PLT
.L3380:
	movq	-256(%rbp), %r13
	testq	%r13, %r13
	je	.L3382
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm10WasmModuleD1Ev
	movl	$440, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3382
	.p2align 4,,10
	.p2align 3
.L3417:
	leaq	-208(%rbp), %r13
	movzbl	%bl, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl14FinishDecodingEb
	movl	-192(%rbp), %eax
	movq	-184(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movdqa	-208(%rbp), %xmm3
	movq	%r14, 24(%r12)
	movl	%eax, 16(%r12)
	leaq	-168(%rbp), %rax
	movups	%xmm3, (%r12)
	movaps	%xmm0, -208(%rbp)
	cmpq	%rax, %rdx
	je	.L3420
	movq	%rdx, 24(%r12)
	movq	-168(%rbp), %rdx
	movq	%rdx, 40(%r12)
.L3384:
	movq	-176(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, -184(%rbp)
	movq	$0, -176(%rbp)
	movq	%rdx, 32(%r12)
	movb	$0, -168(%rbp)
	call	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED1Ev
.L3382:
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -144(%rbp)
	cmpq	-384(%rbp), %rdi
	je	.L3335
	call	_ZdlPv@PLT
	jmp	.L3335
	.p2align 4,,10
	.p2align 3
.L3339:
	movl	8(%rdi), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	cmpl	$1, %eax
	jne	.L3338
.L3412:
	movq	(%rdi), %rax
	movq	%rdx, -392(%rbp)
	movq	%rdi, -384(%rbp)
	call	*16(%rax)
	movq	-392(%rbp), %rdx
	movq	-384(%rbp), %rdi
	testq	%rdx, %rdx
	je	.L3342
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L3343:
	cmpl	$1, %eax
	jne	.L3338
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L3338
	.p2align 4,,10
	.p2align 3
.L3420:
	movdqu	-168(%rbp), %xmm7
	movups	%xmm7, 40(%r12)
	jmp	.L3384
	.p2align 4,,10
	.p2align 3
.L3413:
	movdqu	-104(%rbp), %xmm4
	movups	%xmm4, 40(%r12)
	jmp	.L3347
	.p2align 4,,10
	.p2align 3
.L3411:
	leaq	.LC32(%rip), %rsi
	leaq	.LC33(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3342:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L3343
	.p2align 4,,10
	.p2align 3
.L3419:
	movq	-208(%rbp), %rdx
	movq	-232(%rbp), %rsi
	movq	%r15, 8(%r12)
	pxor	%xmm0, %xmm0
	movdqa	-224(%rbp), %xmm5
	movq	%r14, 24(%r12)
	movq	$0, -232(%rbp)
	movb	$0, -224(%rbp)
	movq	%rdx, (%r12)
	movl	%eax, 16(%r12)
	leaq	-168(%rbp), %rax
	movups	%xmm5, -168(%rbp)
	movaps	%xmm0, -208(%rbp)
.L3377:
	movdqu	-168(%rbp), %xmm6
	movups	%xmm6, 40(%r12)
	jmp	.L3379
	.p2align 4,,10
	.p2align 3
.L3370:
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L3371
	.p2align 4,,10
	.p2align 3
.L3367:
	addl	$1, 8(%r15)
	movq	-200(%rbp), %r13
	jmp	.L3366
	.p2align 4,,10
	.p2align 3
.L3418:
	leaq	-224(%rbp), %rbx
	movdqu	-280(%rbp), %xmm5
	movq	$0, -256(%rbp)
	movl	%ecx, -248(%rbp)
	movq	-288(%rbp), %rdx
	movq	%rbx, -240(%rbp)
	movaps	%xmm5, -176(%rbp)
.L3362:
	movdqa	-176(%rbp), %xmm6
	movaps	%xmm6, -224(%rbp)
	jmp	.L3364
	.p2align 4,,10
	.p2align 3
.L3415:
	movl	-392(%rbp), %r9d
	movl	%ecx, %ecx
	movsbl	%al, %esi
	movl	%r13d, %r8d
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb
	movq	-320(%rbp), %r8
	movq	-352(%rbp), %rdi
	movq	%r8, %rcx
	movq	16(%rdi), %rsi
	subq	-328(%rbp), %rcx
	jmp	.L3351
	.p2align 4,,10
	.p2align 3
.L3416:
	movq	24(%rdi), %rax
	subq	%rsi, %r8
	subq	%rsi, %rax
	cmpl	%eax, %r8d
	ja	.L3355
	movl	%r8d, %r8d
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L3356:
	movq	-320(%rbp), %r8
	cmpq	%rsi, %r8
	je	.L3358
	leaq	.LC88(%rip), %rcx
	leaq	.LC89(%rip), %rax
	cmova	%rax, %rcx
	jmp	.L3359
	.p2align 4,,10
	.p2align 3
.L3372:
	movq	0(%r13), %rax
	movq	%rdx, -376(%rbp)
	movq	%r13, %rdi
	call	*16(%rax)
	movq	-376(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L3373
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L3374:
	cmpl	$1, %eax
	jne	.L3410
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L3410
	.p2align 4,,10
	.p2align 3
.L3373:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L3374
.L3355:
	xorl	%eax, %eax
	movl	%r8d, %ecx
	leaq	.LC39(%rip), %rdx
	movq	%rdi, -400(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-400(%rbp), %rdi
	movq	24(%rdi), %rax
	movq	%rax, 16(%rdi)
	movq	-352(%rbp), %rdi
	movq	16(%rdi), %rsi
	jmp	.L3356
.L3414:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19048:
	.size	_ZN2v88internal4wasm17ModuleDecoderImpl12DecodeModuleEPNS0_8CountersEPNS0_19AccountingAllocatorEb, .-_ZN2v88internal4wasm17ModuleDecoderImpl12DecodeModuleEPNS0_8CountersEPNS0_19AccountingAllocatorEb
	.section	.rodata._ZN2v88internal4wasm16DecodeWasmModuleERKNS1_12WasmFeaturesEPKhS6_bNS1_12ModuleOriginEPNS0_8CountersEPNS0_19AccountingAllocatorE.str1.1,"aMS",@progbits,1
.LC173:
	.string	"module_start <= module_end"
	.section	.rodata._ZN2v88internal4wasm16DecodeWasmModuleERKNS1_12WasmFeaturesEPKhS6_bNS1_12ModuleOriginEPNS0_8CountersEPNS0_19AccountingAllocatorE.str1.8,"aMS",@progbits,1
	.align 8
.LC174:
	.string	"size > maximum module size (%zu): %zu"
	.section	.text._ZN2v88internal4wasm16DecodeWasmModuleERKNS1_12WasmFeaturesEPKhS6_bNS1_12ModuleOriginEPNS0_8CountersEPNS0_19AccountingAllocatorE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm16DecodeWasmModuleERKNS1_12WasmFeaturesEPKhS6_bNS1_12ModuleOriginEPNS0_8CountersEPNS0_19AccountingAllocatorE
	.type	_ZN2v88internal4wasm16DecodeWasmModuleERKNS1_12WasmFeaturesEPKhS6_bNS1_12ModuleOriginEPNS0_8CountersEPNS0_19AccountingAllocatorE, @function
_ZN2v88internal4wasm16DecodeWasmModuleERKNS1_12WasmFeaturesEPKhS6_bNS1_12ModuleOriginEPNS0_8CountersEPNS0_19AccountingAllocatorE:
.LFB19113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-400(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rsi
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movq	%rcx, -416(%rbp)
	movl	%r8d, -424(%rbp)
	movq	16(%rbp), %rbx
	movl	%r9d, -404(%rbp)
	movq	%rax, -432(%rbp)
	leaq	4248(%rbx), %rdx
	leaq	4200(%rbx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%r9b, %r9b
	movq	$0, -400(%rbp)
	movq	$0, -384(%rbp)
	cmove	%rdx, %rdi
	xorl	%edx, %edx
	movq	%rdi, -392(%rbp)
	call	_ZN2v88internal14TimedHistogram5StartEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-416(%rbp), %rcx
	movq	%rcx, %rsi
	subq	%r14, %rsi
	cmpq	%r14, %rcx
	jb	.L3461
	cmpq	$1073741823, %rsi
	ja	.L3462
	leaq	1288(%rbx), %rdx
	cmpb	$0, -404(%rbp)
	leaq	1248(%rbx), %rdi
	movq	%rcx, -416(%rbp)
	cmove	%rdx, %rdi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	(%r15), %rdx
	pxor	%xmm0, %xmm0
	leaq	-176(%rbp), %rax
	movq	%rax, -440(%rbp)
	movq	-416(%rbp), %rcx
	leaq	-304(%rbp), %rdi
	leaq	-240(%rbp), %rsi
	movq	%rdx, -160(%rbp)
	movl	8(%r15), %edx
	movq	%rax, -192(%rbp)
	leaq	-88(%rbp), %rax
	cmpb	$0, _ZN2v88internal24FLAG_assume_asmjs_originE(%rip)
	movl	%edx, -152(%rbp)
	movzbl	12(%r15), %edx
	leaq	-328(%rbp), %r15
	movq	%rax, -416(%rbp)
	movzbl	-424(%rbp), %r8d
	movq	%rax, -104(%rbp)
	movzbl	-404(%rbp), %eax
	movb	%dl, -148(%rbp)
	movl	$1, %edx
	cmove	%eax, %edx
	movq	%rcx, -216(%rbp)
	movq	-432(%rbp), %rcx
	movq	%r14, -232(%rbp)
	movq	%r14, -224(%rbp)
	leaq	16+_ZTVN2v88internal4wasm17ModuleDecoderImplE(%rip), %r14
	movb	%dl, -72(%rbp)
	movq	%rbx, %rdx
	movq	%rdi, -424(%rbp)
	movaps	%xmm0, -144(%rbp)
	movl	$0, -208(%rbp)
	movl	$0, -200(%rbp)
	movq	$0, -184(%rbp)
	movb	$0, -176(%rbp)
	movq	%r14, -240(%rbp)
	movq	$0, -128(%rbp)
	movb	$1, -120(%rbp)
	movq	$0, -116(%rbp)
	movq	$0, -96(%rbp)
	movb	$0, -88(%rbp)
	call	_ZN2v88internal4wasm17ModuleDecoderImpl12DecodeModuleEPNS0_8CountersEPNS0_19AccountingAllocatorEb
	movl	-288(%rbp), %edx
	movq	-280(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-304(%rbp), %xmm1
	movq	%r15, -344(%rbp)
	movl	%edx, -352(%rbp)
	leaq	-264(%rbp), %rdx
	movq	-424(%rbp), %rdi
	cmpq	%rdx, %rcx
	movaps	%xmm1, -368(%rbp)
	movaps	%xmm0, -304(%rbp)
	je	.L3463
	movq	%rcx, -344(%rbp)
	movq	-264(%rbp), %rcx
	movq	%rcx, -328(%rbp)
.L3435:
	movq	-272(%rbp), %rcx
	movq	%rdx, -280(%rbp)
	movq	$0, -272(%rbp)
	movq	%rcx, -336(%rbp)
	movb	$0, -264(%rbp)
	call	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED1Ev
	movq	-336(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.L3436
	leaq	1448(%rbx), %rax
	leaq	1488(%rbx), %rdi
	cmpb	$0, -404(%rbp)
	cmovne	%rax, %rdi
	movq	-368(%rbp), %rax
	movq	(%rax), %rax
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L3439
	movq	16(%rax), %rbx
	leaq	-24(%rbx), %rsi
	subq	%rdx, %rsi
.L3439:
	addq	(%rax), %rsi
	call	_ZN2v88internal9Histogram9AddSampleEi@PLT
	movq	-336(%rbp), %rsi
.L3436:
	movl	-352(%rbp), %eax
	movdqa	-368(%rbp), %xmm2
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -368(%rbp)
	movl	%eax, 16(%r12)
	leaq	40(%r12), %rax
	movq	%rax, 24(%r12)
	movq	-344(%rbp), %rax
	movups	%xmm2, (%r12)
	cmpq	%r15, %rax
	je	.L3464
	movq	%rax, 24(%r12)
	movq	-328(%rbp), %rax
	movq	%rax, 40(%r12)
.L3441:
	movq	%rsi, 32(%r12)
	leaq	-368(%rbp), %rdi
	movq	%r15, -344(%rbp)
	movq	$0, -336(%rbp)
	movb	$0, -328(%rbp)
	call	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED1Ev
	movq	%r14, -240(%rbp)
	movq	-104(%rbp), %rdi
	cmpq	-416(%rbp), %rdi
	je	.L3442
	call	_ZdlPv@PLT
.L3442:
	movq	-136(%rbp), %r14
	testq	%r14, %r14
	je	.L3444
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3445
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L3446:
	cmpl	$1, %eax
	jne	.L3444
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L3448
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L3449:
	cmpl	$1, %eax
	je	.L3465
	.p2align 4,,10
	.p2align 3
.L3444:
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	-192(%rbp), %rdi
	movq	%rax, -240(%rbp)
	cmpq	-440(%rbp), %rdi
	je	.L3431
.L3460:
	call	_ZdlPv@PLT
.L3431:
	movq	-384(%rbp), %rdx
	movq	-392(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal14TimedHistogram4StopEPNS_4base12ElapsedTimerEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3466
	addq	$408, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3462:
	.cfi_restore_state
	movq	%rsi, %r8
	xorl	%eax, %eax
	movl	$1073741824, %ecx
	xorl	%esi, %esi
	leaq	.LC174(%rip), %rdx
	leaq	-304(%rbp), %rdi
	call	_ZN2v88internal4wasm9WasmErrorC1EjPKcz
	movl	-304(%rbp), %edx
	movq	-296(%rbp), %rax
	pxor	%xmm0, %xmm0
	leaq	-280(%rbp), %rbx
	movaps	%xmm0, -240(%rbp)
	movl	%edx, -224(%rbp)
	cmpq	%rbx, %rax
	je	.L3467
	movq	-280(%rbp), %rcx
	movl	%edx, 16(%r12)
	leaq	40(%r12), %rdx
	movq	%rdx, 24(%r12)
	leaq	-200(%rbp), %rdx
	movq	-288(%rbp), %rsi
	movq	%rcx, -200(%rbp)
	movq	%rbx, -296(%rbp)
	movq	$0, -288(%rbp)
	movb	$0, -280(%rbp)
	movups	%xmm0, (%r12)
	cmpq	%rdx, %rax
	je	.L3427
	movq	%rax, 24(%r12)
	movq	%rcx, 40(%r12)
.L3429:
	movq	%rsi, 32(%r12)
	leaq	-240(%rbp), %rdi
	movq	%rdx, -216(%rbp)
	movq	$0, -208(%rbp)
	movb	$0, -200(%rbp)
	call	_ZN2v88internal4wasm6ResultISt10shared_ptrINS1_10WasmModuleEEED1Ev
	movq	-296(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L3460
	jmp	.L3431
	.p2align 4,,10
	.p2align 3
.L3463:
	movdqu	-264(%rbp), %xmm3
	movups	%xmm3, -328(%rbp)
	jmp	.L3435
	.p2align 4,,10
	.p2align 3
.L3445:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L3446
	.p2align 4,,10
	.p2align 3
.L3464:
	movdqu	-328(%rbp), %xmm4
	movups	%xmm4, 40(%r12)
	jmp	.L3441
	.p2align 4,,10
	.p2align 3
.L3461:
	leaq	.LC173(%rip), %rsi
	leaq	.LC33(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3467:
	leaq	40(%r12), %rax
	movdqu	-280(%rbp), %xmm5
	movl	%edx, 16(%r12)
	leaq	-200(%rbp), %rdx
	movq	-288(%rbp), %rsi
	movb	$0, -280(%rbp)
	movq	$0, -288(%rbp)
	movq	%rax, 24(%r12)
	movups	%xmm5, -200(%rbp)
	movups	%xmm0, (%r12)
.L3427:
	movdqu	-200(%rbp), %xmm6
	movups	%xmm6, 40(%r12)
	jmp	.L3429
	.p2align 4,,10
	.p2align 3
.L3448:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L3449
	.p2align 4,,10
	.p2align 3
.L3465:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L3444
.L3466:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19113:
	.size	_ZN2v88internal4wasm16DecodeWasmModuleERKNS1_12WasmFeaturesEPKhS6_bNS1_12ModuleOriginEPNS0_8CountersEPNS0_19AccountingAllocatorE, .-_ZN2v88internal4wasm16DecodeWasmModuleERKNS1_12WasmFeaturesEPKhS6_bNS1_12ModuleOriginEPNS0_8CountersEPNS0_19AccountingAllocatorE
	.section	.text._ZN2v88internal4wasm13ModuleDecoder13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm13ModuleDecoder13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb
	.type	_ZN2v88internal4wasm13ModuleDecoder13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb, @function
_ZN2v88internal4wasm13ModuleDecoder13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb:
.LFB19123:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	movsbl	%sil, %esi
	movzbl	%r9b, %r9d
	jmp	_ZN2v88internal4wasm17ModuleDecoderImpl13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb
	.cfi_endproc
.LFE19123:
	.size	_ZN2v88internal4wasm13ModuleDecoder13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb, .-_ZN2v88internal4wasm13ModuleDecoder13DecodeSectionENS1_11SectionCodeENS0_6VectorIKhEEjb
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE, @function
_GLOBAL__sub_I__ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE:
.LFB25357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25357:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE, .-_GLOBAL__sub_I__ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm11SectionNameENS1_11SectionCodeE
	.weak	_ZTVN2v88internal4wasm7DecoderE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm7DecoderE,"awG",@progbits,_ZTVN2v88internal4wasm7DecoderE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm7DecoderE, @object
	.size	_ZTVN2v88internal4wasm7DecoderE, 40
_ZTVN2v88internal4wasm7DecoderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm7DecoderD1Ev
	.quad	_ZN2v88internal4wasm7DecoderD0Ev
	.quad	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.weak	_ZTVN2v88internal4wasm17ModuleDecoderImplE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm17ModuleDecoderImplE,"awG",@progbits,_ZTVN2v88internal4wasm17ModuleDecoderImplE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm17ModuleDecoderImplE, @object
	.size	_ZTVN2v88internal4wasm17ModuleDecoderImplE, 40
_ZTVN2v88internal4wasm17ModuleDecoderImplE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm17ModuleDecoderImplD1Ev
	.quad	_ZN2v88internal4wasm17ModuleDecoderImplD0Ev
	.quad	_ZN2v88internal4wasm17ModuleDecoderImpl12onFirstErrorEv
	.weak	_ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN2v88internal4wasm10WasmModuleELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt19_Sp_counted_deleterIPN2v88internal4wasm10WasmModuleESt14default_deleteIS3_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L23kCompilationHintsStringE,"a"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L23kCompilationHintsStringE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L23kCompilationHintsStringE, 17
_ZN2v88internal4wasm12_GLOBAL__N_1L23kCompilationHintsStringE:
	.string	"compilationHints"
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L23kSourceMappingURLStringE,"a"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L23kSourceMappingURLStringE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L23kSourceMappingURLStringE, 17
_ZN2v88internal4wasm12_GLOBAL__N_1L23kSourceMappingURLStringE:
	.string	"sourceMappingURL"
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kNameStringE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kNameStringE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kNameStringE, 5
_ZN2v88internal4wasm12_GLOBAL__N_1L11kNameStringE:
	.string	"name"
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.data.rel.ro,"aw"
	.align 8
.LC29:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC30:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC31:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
