	.file	"wasm-opcodes.cc"
	.text
	.section	.rodata._ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unknown"
.LC1:
	.string	"i32.atomic.cmpxchng32"
.LC2:
	.string	"f32.eq"
.LC3:
	.string	"f64.eq"
.LC4:
	.string	"i32.eq"
.LC5:
	.string	"i64.eq"
.LC6:
	.string	"f32.ne"
.LC7:
	.string	"f64.ne"
.LC8:
	.string	"i32.ne"
.LC9:
	.string	"i64.ne"
.LC10:
	.string	"f32.add"
.LC11:
	.string	"f64.add"
.LC12:
	.string	"i32.add"
.LC13:
	.string	"i64.add"
.LC14:
	.string	"f32.sub"
.LC15:
	.string	"f64.sub"
.LC16:
	.string	"i32.sub"
.LC17:
	.string	"i64.sub"
.LC18:
	.string	"f32.mul"
.LC19:
	.string	"f64.mul"
.LC20:
	.string	"i32.mul"
.LC21:
	.string	"i64.mul"
.LC22:
	.string	"f32.lt"
.LC23:
	.string	"f64.lt"
.LC24:
	.string	"i32.lt_s"
.LC25:
	.string	"i64.lt_s"
.LC26:
	.string	"i32.lt_u"
.LC27:
	.string	"i64.lt_u"
.LC28:
	.string	"f32.gt"
.LC29:
	.string	"f64.gt"
.LC30:
	.string	"i32.gt_s"
.LC31:
	.string	"i64.gt_s"
.LC32:
	.string	"i32.gt_u"
.LC33:
	.string	"i64.gt_u"
.LC34:
	.string	"f32.le"
.LC35:
	.string	"f64.le"
.LC36:
	.string	"i32.le_s"
.LC37:
	.string	"i64.le_s"
.LC38:
	.string	"i32.le_u"
.LC39:
	.string	"i64.le_u"
.LC40:
	.string	"f32.ge"
.LC41:
	.string	"f64.ge"
.LC42:
	.string	"i32.ge_s"
.LC43:
	.string	"i64.ge_s"
.LC44:
	.string	"i32.ge_u"
.LC45:
	.string	"i64.ge_u"
.LC46:
	.string	"i32.clz"
.LC47:
	.string	"i64.clz"
.LC48:
	.string	"i32.ctz"
.LC49:
	.string	"i64.ctz"
.LC50:
	.string	"i32.popcnt"
.LC51:
	.string	"i64.popcnt"
.LC52:
	.string	"f32.div"
.LC53:
	.string	"f64.div"
.LC54:
	.string	"i32.div_s"
.LC55:
	.string	"i64.div_s"
.LC56:
	.string	"i32.div_u"
.LC57:
	.string	"i64.div_u"
.LC58:
	.string	"i32.rem_s"
.LC59:
	.string	"i64.rem_s"
.LC60:
	.string	"i32.rem_u"
.LC61:
	.string	"i64.rem_u"
.LC62:
	.string	"i32.and"
.LC63:
	.string	"i64.and"
.LC64:
	.string	"i32.or"
.LC65:
	.string	"i64.or"
.LC66:
	.string	"i32.xor"
.LC67:
	.string	"i64.xor"
.LC68:
	.string	"i32.shl"
.LC69:
	.string	"i64.shl"
.LC70:
	.string	"i32.shr_s"
.LC71:
	.string	"i64.shr_s"
.LC72:
	.string	"i32.shr_u"
.LC73:
	.string	"i64.shr_u"
.LC74:
	.string	"i32.rol"
.LC75:
	.string	"i64.rol"
.LC76:
	.string	"i32.ror"
.LC77:
	.string	"i64.ror"
.LC78:
	.string	"f32.abs"
.LC79:
	.string	"f64.abs"
.LC80:
	.string	"f32.neg"
.LC81:
	.string	"f64.neg"
.LC82:
	.string	"f32.ceil"
.LC83:
	.string	"f64.ceil"
.LC84:
	.string	"f32.floor"
.LC85:
	.string	"f64.floor"
.LC86:
	.string	"f32.trunc"
.LC87:
	.string	"f64.trunc"
.LC88:
	.string	"f32.nearest"
.LC89:
	.string	"f64.nearest"
.LC90:
	.string	"f32.sqrt"
.LC91:
	.string	"f64.sqrt"
.LC92:
	.string	"f32.min"
.LC93:
	.string	"f64.min"
.LC94:
	.string	"f32.max"
.LC95:
	.string	"f64.max"
.LC96:
	.string	"f32.copysign"
.LC97:
	.string	"f64.copysign"
.LC98:
	.string	"ref.null"
.LC99:
	.string	"ref.is_null"
.LC100:
	.string	"ref.func"
.LC101:
	.string	"i32.wrap_i64"
.LC102:
	.string	"i32.trunc_f32_u"
.LC103:
	.string	"i64.trunc_f32_u"
.LC104:
	.string	"i32.trunc_f32_s"
.LC105:
	.string	"i64.trunc_f32_s"
.LC106:
	.string	"i32.trunc_f64_u"
.LC107:
	.string	"i64.trunc_f64_u"
.LC108:
	.string	"i32.trunc_f64_s"
.LC109:
	.string	"i64.trunc_f64_s"
.LC110:
	.string	"i64.extend_i32_u"
.LC111:
	.string	"i64.extend_i32_s"
.LC112:
	.string	"f32.convert_i32_u"
.LC113:
	.string	"f32.convert_i32_s"
.LC114:
	.string	"f32.convert_i64_u"
.LC115:
	.string	"f32.convert_i64_s"
.LC116:
	.string	"f32.demote_f64"
.LC117:
	.string	"f64.convert_i32_u"
.LC118:
	.string	"f64.convert_i32_s"
.LC119:
	.string	"f64.convert_i64_u"
.LC120:
	.string	"f64.convert_i64_s"
.LC121:
	.string	"f64.promote_f32"
.LC122:
	.string	"i32.reinterpret_f32"
.LC123:
	.string	"i64.reinterpret_f64"
.LC124:
	.string	"f32.reinterpret_i32"
.LC125:
	.string	"f64.reinterpret_i64"
.LC126:
	.string	"i32.extend8_s"
.LC127:
	.string	"i64.extend8_s"
.LC128:
	.string	"i32.extend16_s"
.LC129:
	.string	"i64.extend16_s"
.LC130:
	.string	"i64.extend32_s"
.LC131:
	.string	"unreachable"
.LC132:
	.string	"nop"
.LC133:
	.string	"block"
.LC134:
	.string	"loop"
.LC135:
	.string	"if"
.LC136:
	.string	"else"
.LC137:
	.string	"end"
.LC138:
	.string	"br"
.LC139:
	.string	"br_if"
.LC140:
	.string	"br_table"
.LC141:
	.string	"return"
.LC142:
	.string	"call"
.LC143:
	.string	"call_indirect"
.LC144:
	.string	"return_call"
.LC145:
	.string	"return_call_indirect"
.LC146:
	.string	"drop"
.LC147:
	.string	"select"
.LC148:
	.string	"i32.eqz"
.LC149:
	.string	"local.get"
.LC150:
	.string	"local.set"
.LC151:
	.string	"local.tee"
.LC152:
	.string	"global.get"
.LC153:
	.string	"global.set"
.LC154:
	.string	"table.get"
.LC155:
	.string	"table.set"
.LC156:
	.string	"f32.const"
.LC157:
	.string	"f64.const"
.LC158:
	.string	"i32.const"
.LC159:
	.string	"i64.const"
.LC160:
	.string	"memory.size"
.LC161:
	.string	"memory.grow"
.LC162:
	.string	"f32.load"
.LC163:
	.string	"f64.load"
.LC164:
	.string	"i32.load"
.LC165:
	.string	"i64.load"
.LC166:
	.string	"i32.load8_s"
.LC167:
	.string	"i64.load8_s"
.LC168:
	.string	"i32.load8_u"
.LC169:
	.string	"i64.load8_u"
.LC170:
	.string	"i32.load16_s"
.LC171:
	.string	"i64.load16_s"
.LC172:
	.string	"i32.load16_u"
.LC173:
	.string	"i64.load16_u"
.LC174:
	.string	"i64.load32_s"
.LC175:
	.string	"i64.load32_u"
.LC176:
	.string	"s128.load128"
.LC177:
	.string	"f32.store"
.LC178:
	.string	"f64.store"
.LC179:
	.string	"i32.store"
.LC180:
	.string	"i64.store"
.LC181:
	.string	"i32.store8"
.LC182:
	.string	"i64.store8"
.LC183:
	.string	"i32.store16"
.LC184:
	.string	"i64.store16"
.LC185:
	.string	"i64.store32"
.LC186:
	.string	"s128.store128"
.LC187:
	.string	"try"
.LC188:
	.string	"catch"
.LC189:
	.string	"throw"
.LC190:
	.string	"rethrow"
.LC191:
	.string	"br_on_exn"
.LC192:
	.string	"f64.acos"
.LC193:
	.string	"f64.asin"
.LC194:
	.string	"f64.atan"
.LC195:
	.string	"f64.cos"
.LC196:
	.string	"f64.sin"
.LC197:
	.string	"f64.tan"
.LC198:
	.string	"f64.exp"
.LC199:
	.string	"f64.log"
.LC200:
	.string	"f64.atan2"
.LC201:
	.string	"f64.pow"
.LC202:
	.string	"f64.mod"
.LC203:
	.string	"f32.asmjs_load"
.LC204:
	.string	"f64.asmjs_load"
.LC205:
	.string	"i32.asmjs_load8_s"
.LC206:
	.string	"i32.asmjs_load8_u"
.LC207:
	.string	"i32.asmjs_load16_s"
.LC208:
	.string	"i32.asmjs_load16_u"
.LC209:
	.string	"i32.asmjs_load32"
.LC210:
	.string	"i32.asmjs_store"
.LC211:
	.string	"f32.asmjs_store"
.LC212:
	.string	"f64.asmjs_store"
.LC213:
	.string	"i32.asmjs_store8"
.LC214:
	.string	"i32.asmjs_store16"
.LC215:
	.string	"i32.asmjs_div_s"
.LC216:
	.string	"i32.asmjs_div_u"
.LC217:
	.string	"i32.asmjs_rem_s"
.LC218:
	.string	"i32.asmjs_rem_u"
.LC219:
	.string	"i32.asmjs_convert_f32_s"
.LC220:
	.string	"i32.asmjs_convert_f32_u"
.LC221:
	.string	"i32.asmjs_convert_f64_s"
.LC222:
	.string	"i32.asmjs_convert_f64_u"
.LC223:
	.string	"i32.trunc_sat_f32_u"
.LC224:
	.string	"i32.trunc_sat_f32_s"
.LC225:
	.string	"i32.trunc_sat_f64_u"
.LC226:
	.string	"i32.trunc_sat_f64_s"
.LC227:
	.string	"i64.trunc_sat_f32_u"
.LC228:
	.string	"i64.trunc_sat_f32_s"
.LC229:
	.string	"i64.trunc_sat_f64_u"
.LC230:
	.string	"i64.trunc_sat_f64_s"
.LC231:
	.string	"memory.init"
.LC232:
	.string	"data.drop"
.LC233:
	.string	"memory.copy"
.LC234:
	.string	"memory.fill"
.LC235:
	.string	"table.init"
.LC236:
	.string	"elem.drop"
.LC237:
	.string	"table.copy"
.LC238:
	.string	"table.grow"
.LC239:
	.string	"table.size"
.LC240:
	.string	"table.fill"
.LC241:
	.string	"f32x4.splat"
.LC242:
	.string	"i32x4.splat"
.LC243:
	.string	"i16x8.splat"
.LC244:
	.string	"i8x16.splat"
.LC245:
	.string	"f32x4.neg"
.LC246:
	.string	"i32x4.neg"
.LC247:
	.string	"i16x8.neg"
.LC248:
	.string	"i8x16.neg"
.LC249:
	.string	"f64x2.neg"
.LC250:
	.string	"i64x2.neg"
.LC251:
	.string	"f32x4.eq"
.LC252:
	.string	"i32x4.eq"
.LC253:
	.string	"i16x8.eq"
.LC254:
	.string	"i8x16.eq"
.LC255:
	.string	"f64x2.eq"
.LC256:
	.string	"i64x2.eq"
.LC257:
	.string	"f32x4.ne"
.LC258:
	.string	"i32x4.ne"
.LC259:
	.string	"i16x8.ne"
.LC260:
	.string	"i8x16.ne"
.LC261:
	.string	"f64x2.ne"
.LC262:
	.string	"i64x2.ne"
.LC263:
	.string	"f32x4.add"
.LC264:
	.string	"i32x4.add"
.LC265:
	.string	"i16x8.add"
.LC266:
	.string	"i8x16.add"
.LC267:
	.string	"f64x2.add"
.LC268:
	.string	"i64x2.add"
.LC269:
	.string	"f32x4.sub"
.LC270:
	.string	"i32x4.sub"
.LC271:
	.string	"i16x8.sub"
.LC272:
	.string	"i8x16.sub"
.LC273:
	.string	"f64x2.sub"
.LC274:
	.string	"i64x2.sub"
.LC275:
	.string	"f32x4.mul"
.LC276:
	.string	"i32x4.mul"
.LC277:
	.string	"i16x8.mul"
.LC278:
	.string	"i8x16.mul"
.LC279:
	.string	"f64x2.mul"
.LC280:
	.string	"i64x2.mul"
.LC281:
	.string	"f64x2.div"
.LC282:
	.string	"f32x4.div"
.LC283:
	.string	"f64x2.splat"
.LC284:
	.string	"f64x2.lt"
.LC285:
	.string	"f64x2.le"
.LC286:
	.string	"f64x2.gt"
.LC287:
	.string	"f64x2.ge"
.LC288:
	.string	"f64x2.abs"
.LC289:
	.string	"f32x4.abs"
.LC290:
	.string	"f32x4.add_horizontal"
.LC291:
	.string	"f32x4.recip_approx"
.LC292:
	.string	"f32x4.recip_sqrt_approx"
.LC293:
	.string	"f64x2.min"
.LC294:
	.string	"f32x4.min"
.LC295:
	.string	"f64x2.max"
.LC296:
	.string	"f32x4.max"
.LC297:
	.string	"f32x4.lt"
.LC298:
	.string	"f32x4.le"
.LC299:
	.string	"f32x4.gt"
.LC300:
	.string	"f32x4.ge"
.LC301:
	.string	"f32x4.convert_i32_u"
.LC302:
	.string	"f32x4.convert_i32_s"
.LC303:
	.string	"i32x4.convert_f32_u"
.LC304:
	.string	"i32x4.convert_f32_s"
.LC305:
	.string	"i32x4.convert_i32_u"
.LC306:
	.string	"i32x4.convert_i32_s"
.LC307:
	.string	"i64.atomic.cmpxchng32_u"
.LC308:
	.string	"i64.atomic.cmpxchng16_u"
.LC309:
	.string	"i16x8.convert_i32_u"
.LC310:
	.string	"i16x8.convert_i32_s"
.LC311:
	.string	"i64.atomic.cmpxchng8_u"
.LC312:
	.string	"i32.atomic.cmpxchng16_u"
.LC313:
	.string	"i64.atomic.cmpxchng64"
.LC314:
	.string	"i32.atomic.cmpxchng8_u"
.LC315:
	.string	"i8x16.convert_i32_u"
.LC316:
	.string	"i8x16.convert_i32_s"
.LC317:
	.string	"f64x2.extract_lane"
.LC318:
	.string	"f64x2.replace_lane"
.LC319:
	.string	"f32x4.extract_lane"
.LC320:
	.string	"f32x4.replace_lane"
.LC321:
	.string	"i64x2.extract_lane"
.LC322:
	.string	"i64x2.replace_lane"
.LC323:
	.string	"i32x4.extract_lane"
.LC324:
	.string	"i16x8.extract_lane"
.LC325:
	.string	"i8x16.extract_lane"
.LC326:
	.string	"i32x4.replace_lane"
.LC327:
	.string	"i16x8.replace_lane"
.LC328:
	.string	"i8x16.replace_lane"
.LC329:
	.string	"i32x4.min_s"
.LC330:
	.string	"i16x8.min_s"
.LC331:
	.string	"i8x16.min_s"
.LC332:
	.string	"i32x4.min_u"
.LC333:
	.string	"i16x8.min_u"
.LC334:
	.string	"i8x16.min_u"
.LC335:
	.string	"i64x2.min_s"
.LC336:
	.string	"i64x2.min_u"
.LC337:
	.string	"i32x4.max_s"
.LC338:
	.string	"i16x8.max_s"
.LC339:
	.string	"i8x16.max_s"
.LC340:
	.string	"i32x4.max_u"
.LC341:
	.string	"i16x8.max_u"
.LC342:
	.string	"i8x16.max_u"
.LC343:
	.string	"i64x2.max_s"
.LC344:
	.string	"i64x2.max_u"
.LC345:
	.string	"i32x4.lt_s"
.LC346:
	.string	"i16x8.lt_s"
.LC347:
	.string	"i8x16.lt_s"
.LC348:
	.string	"i32x4.lt_u"
.LC349:
	.string	"i16x8.lt_u"
.LC350:
	.string	"i8x16.lt_u"
.LC351:
	.string	"i64x2.lt_s"
.LC352:
	.string	"i64x2.lt_u"
.LC353:
	.string	"i32x4.le_s"
.LC354:
	.string	"i16x8.le_s"
.LC355:
	.string	"i8x16.le_s"
.LC356:
	.string	"i32x4.le_u"
.LC357:
	.string	"i16x8.le_u"
.LC358:
	.string	"i8x16.le_u"
.LC359:
	.string	"i64x2.le_s"
.LC360:
	.string	"i64x2.le_u"
.LC361:
	.string	"i32x4.gt_s"
.LC362:
	.string	"i16x8.gt_s"
.LC363:
	.string	"i8x16.gt_s"
.LC364:
	.string	"i32x4.gt_u"
.LC365:
	.string	"i16x8.gt_u"
.LC366:
	.string	"i8x16.gt_u"
.LC367:
	.string	"i64x2.gt_s"
.LC368:
	.string	"i64x2.gt_u"
.LC369:
	.string	"i32x4.ge_s"
.LC370:
	.string	"i16x8.ge_s"
.LC371:
	.string	"i8x16.ge_s"
.LC372:
	.string	"i32x4.ge_u"
.LC373:
	.string	"i16x8.ge_u"
.LC374:
	.string	"i8x16.ge_u"
.LC375:
	.string	"i64x2.ge_s"
.LC376:
	.string	"i64x2.ge_u"
.LC377:
	.string	"i32x4.shr_s"
.LC378:
	.string	"i16x8.shr_s"
.LC379:
	.string	"i8x16.shr_s"
.LC380:
	.string	"i32x4.shr_u"
.LC381:
	.string	"i16x8.shr_u"
.LC382:
	.string	"i8x16.shr_u"
.LC383:
	.string	"i64x2.shr_s"
.LC384:
	.string	"i64x2.shr_u"
.LC385:
	.string	"i32x4.shl"
.LC386:
	.string	"i16x8.shl"
.LC387:
	.string	"i8x16.shl"
.LC388:
	.string	"i64x2.shl"
.LC389:
	.string	"i64x2.splat"
.LC390:
	.string	"i32x4.add_horizontal"
.LC391:
	.string	"i16x8.add_horizontal"
.LC392:
	.string	"i16x8.add_saturate_s"
.LC393:
	.string	"i16x8.add_saturate_u"
.LC394:
	.string	"i8x16.add_saturate_s"
.LC395:
	.string	"i8x16.add_saturate_u"
.LC396:
	.string	"i16x8.sub_saturate_s"
.LC397:
	.string	"i16x8.sub_saturate_u"
.LC398:
	.string	"i8x16.sub_saturate_s"
.LC399:
	.string	"i8x16.sub_saturate_u"
.LC400:
	.string	"s128.and"
.LC401:
	.string	"s128.or"
.LC402:
	.string	"s128.xor"
.LC403:
	.string	"s128.not"
.LC404:
	.string	"s128.select"
.LC405:
	.string	"s8x16.shuffle"
.LC406:
	.string	"s1x2.any_true"
.LC407:
	.string	"s1x2.all_true"
.LC408:
	.string	"s1x4.any_true"
.LC409:
	.string	"s1x4.all_true"
.LC410:
	.string	"s1x8.any_true"
.LC411:
	.string	"s1x8.all_true"
.LC412:
	.string	"s1x16.any_true"
.LC413:
	.string	"s1x16.all_true"
.LC414:
	.string	"atomic.notify"
.LC415:
	.string	"i32.atomic.wait"
.LC416:
	.string	"i64.atomic.wait"
.LC417:
	.string	"atomic.fence"
.LC418:
	.string	"i32.atomic.load32"
.LC419:
	.string	"i32.atomic.load8_u"
.LC420:
	.string	"i32.atomic.load16_u"
.LC421:
	.string	"i64.atomic.load64"
.LC422:
	.string	"i64.atomic.load8_u"
.LC423:
	.string	"i64.atomic.load16_u"
.LC424:
	.string	"i64.atomic.load32_u"
.LC425:
	.string	"i32.atomic.store32"
.LC426:
	.string	"i32.atomic.store8_u"
.LC427:
	.string	"i32.atomic.store16_u"
.LC428:
	.string	"i64.atomic.store64"
.LC429:
	.string	"i64.atomic.store8_u"
.LC430:
	.string	"i64.atomic.store16_u"
.LC431:
	.string	"i64.atomic.store32_u"
.LC432:
	.string	"i32.atomic.add32"
.LC433:
	.string	"i32.atomic.add8_u"
.LC434:
	.string	"i32.atomic.add16_u"
.LC435:
	.string	"i64.atomic.add64"
.LC436:
	.string	"i64.atomic.add8_u"
.LC437:
	.string	"i64.atomic.add16_u"
.LC438:
	.string	"i64.atomic.add32_u"
.LC439:
	.string	"i32.atomic.sub32"
.LC440:
	.string	"i32.atomic.sub8_u"
.LC441:
	.string	"i32.atomic.sub16_u"
.LC442:
	.string	"i64.atomic.sub64"
.LC443:
	.string	"i64.atomic.sub8_u"
.LC444:
	.string	"i64.atomic.sub16_u"
.LC445:
	.string	"i64.atomic.sub32_u"
.LC446:
	.string	"i32.atomic.and32"
.LC447:
	.string	"i32.atomic.and8_u"
.LC448:
	.string	"i32.atomic.and16_u"
.LC449:
	.string	"i64.atomic.and64"
.LC450:
	.string	"i64.atomic.and8_u"
.LC451:
	.string	"i64.atomic.and16_u"
.LC452:
	.string	"i64.atomic.and32_u"
.LC453:
	.string	"i32.atomic.or32"
.LC454:
	.string	"i32.atomic.or8_u"
.LC455:
	.string	"i32.atomic.or16_u"
.LC456:
	.string	"i64.atomic.or64"
.LC457:
	.string	"i64.atomic.or8_u"
.LC458:
	.string	"i64.atomic.or16_u"
.LC459:
	.string	"i64.atomic.or32_u"
.LC460:
	.string	"i32.atomic.xor32"
.LC461:
	.string	"i32.atomic.xor8_u"
.LC462:
	.string	"i32.atomic.xor16_u"
.LC463:
	.string	"i64.atomic.xor64"
.LC464:
	.string	"i64.atomic.xor8_u"
.LC465:
	.string	"i64.atomic.xor16_u"
.LC466:
	.string	"i64.atomic.xor32_u"
.LC467:
	.string	"i32.atomic.xchng32"
.LC468:
	.string	"i32.atomic.xchng8_u"
.LC469:
	.string	"i32.atomic.xchng16_u"
.LC470:
	.string	"i64.atomic.xchng64"
.LC471:
	.string	"i64.atomic.xchng8_u"
.LC472:
	.string	"i64.atomic.xchng16_u"
.LC473:
	.string	"i64.atomic.xchng32_u"
.LC474:
	.string	"i64.eqz"
	.section	.text._ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE
	.type	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE:
.LFB5584:
	.cfi_startproc
	endbr64
	cmpl	$230, %edi
	jbe	.L486
	subl	$64512, %edi
	cmpl	$590, %edi
	ja	.L487
	leaq	.L6(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE,"a",@progbits
	.align 4
	.align 4
.L6:
	.long	.L259-.L6
	.long	.L258-.L6
	.long	.L257-.L6
	.long	.L256-.L6
	.long	.L255-.L6
	.long	.L254-.L6
	.long	.L253-.L6
	.long	.L252-.L6
	.long	.L251-.L6
	.long	.L250-.L6
	.long	.L249-.L6
	.long	.L248-.L6
	.long	.L247-.L6
	.long	.L246-.L6
	.long	.L245-.L6
	.long	.L244-.L6
	.long	.L243-.L6
	.long	.L242-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L241-.L6
	.long	.L482-.L6
	.long	.L4-.L6
	.long	.L239-.L6
	.long	.L238-.L6
	.long	.L237-.L6
	.long	.L4-.L6
	.long	.L236-.L6
	.long	.L235-.L6
	.long	.L234-.L6
	.long	.L4-.L6
	.long	.L233-.L6
	.long	.L232-.L6
	.long	.L231-.L6
	.long	.L230-.L6
	.long	.L229-.L6
	.long	.L228-.L6
	.long	.L227-.L6
	.long	.L226-.L6
	.long	.L225-.L6
	.long	.L224-.L6
	.long	.L223-.L6
	.long	.L222-.L6
	.long	.L221-.L6
	.long	.L220-.L6
	.long	.L219-.L6
	.long	.L218-.L6
	.long	.L217-.L6
	.long	.L216-.L6
	.long	.L215-.L6
	.long	.L214-.L6
	.long	.L213-.L6
	.long	.L212-.L6
	.long	.L211-.L6
	.long	.L210-.L6
	.long	.L209-.L6
	.long	.L208-.L6
	.long	.L207-.L6
	.long	.L206-.L6
	.long	.L205-.L6
	.long	.L204-.L6
	.long	.L203-.L6
	.long	.L202-.L6
	.long	.L201-.L6
	.long	.L200-.L6
	.long	.L199-.L6
	.long	.L198-.L6
	.long	.L197-.L6
	.long	.L196-.L6
	.long	.L195-.L6
	.long	.L194-.L6
	.long	.L193-.L6
	.long	.L192-.L6
	.long	.L191-.L6
	.long	.L190-.L6
	.long	.L189-.L6
	.long	.L188-.L6
	.long	.L187-.L6
	.long	.L186-.L6
	.long	.L185-.L6
	.long	.L184-.L6
	.long	.L183-.L6
	.long	.L182-.L6
	.long	.L181-.L6
	.long	.L180-.L6
	.long	.L179-.L6
	.long	.L178-.L6
	.long	.L177-.L6
	.long	.L176-.L6
	.long	.L175-.L6
	.long	.L174-.L6
	.long	.L173-.L6
	.long	.L172-.L6
	.long	.L171-.L6
	.long	.L170-.L6
	.long	.L169-.L6
	.long	.L168-.L6
	.long	.L167-.L6
	.long	.L166-.L6
	.long	.L165-.L6
	.long	.L164-.L6
	.long	.L163-.L6
	.long	.L162-.L6
	.long	.L161-.L6
	.long	.L160-.L6
	.long	.L159-.L6
	.long	.L158-.L6
	.long	.L157-.L6
	.long	.L156-.L6
	.long	.L155-.L6
	.long	.L154-.L6
	.long	.L153-.L6
	.long	.L152-.L6
	.long	.L151-.L6
	.long	.L150-.L6
	.long	.L149-.L6
	.long	.L148-.L6
	.long	.L147-.L6
	.long	.L146-.L6
	.long	.L145-.L6
	.long	.L144-.L6
	.long	.L143-.L6
	.long	.L142-.L6
	.long	.L141-.L6
	.long	.L140-.L6
	.long	.L139-.L6
	.long	.L138-.L6
	.long	.L137-.L6
	.long	.L136-.L6
	.long	.L135-.L6
	.long	.L134-.L6
	.long	.L133-.L6
	.long	.L132-.L6
	.long	.L131-.L6
	.long	.L130-.L6
	.long	.L129-.L6
	.long	.L128-.L6
	.long	.L127-.L6
	.long	.L126-.L6
	.long	.L125-.L6
	.long	.L124-.L6
	.long	.L123-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L122-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L121-.L6
	.long	.L120-.L6
	.long	.L119-.L6
	.long	.L118-.L6
	.long	.L117-.L6
	.long	.L116-.L6
	.long	.L115-.L6
	.long	.L114-.L6
	.long	.L113-.L6
	.long	.L112-.L6
	.long	.L111-.L6
	.long	.L110-.L6
	.long	.L4-.L6
	.long	.L109-.L6
	.long	.L108-.L6
	.long	.L107-.L6
	.long	.L106-.L6
	.long	.L105-.L6
	.long	.L104-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L103-.L6
	.long	.L102-.L6
	.long	.L4-.L6
	.long	.L101-.L6
	.long	.L100-.L6
	.long	.L99-.L6
	.long	.L98-.L6
	.long	.L97-.L6
	.long	.L96-.L6
	.long	.L95-.L6
	.long	.L94-.L6
	.long	.L93-.L6
	.long	.L92-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L91-.L6
	.long	.L90-.L6
	.long	.L89-.L6
	.long	.L88-.L6
	.long	.L87-.L6
	.long	.L86-.L6
	.long	.L85-.L6
	.long	.L84-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L83-.L6
	.long	.L82-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L81-.L6
	.long	.L80-.L6
	.long	.L79-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L78-.L6
	.long	.L77-.L6
	.long	.L76-.L6
	.long	.L75-.L6
	.long	.L76-.L6
	.long	.L76-.L6
	.long	.L75-.L6
	.long	.L75-.L6
	.long	.L74-.L6
	.long	.L74-.L6
	.long	.L73-.L6
	.long	.L73-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L72-.L6
	.long	.L71-.L6
	.long	.L70-.L6
	.long	.L69-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L68-.L6
	.long	.L67-.L6
	.long	.L66-.L6
	.long	.L65-.L6
	.long	.L64-.L6
	.long	.L63-.L6
	.long	.L62-.L6
	.long	.L61-.L6
	.long	.L60-.L6
	.long	.L59-.L6
	.long	.L58-.L6
	.long	.L57-.L6
	.long	.L56-.L6
	.long	.L55-.L6
	.long	.L54-.L6
	.long	.L53-.L6
	.long	.L52-.L6
	.long	.L51-.L6
	.long	.L50-.L6
	.long	.L49-.L6
	.long	.L48-.L6
	.long	.L47-.L6
	.long	.L46-.L6
	.long	.L45-.L6
	.long	.L44-.L6
	.long	.L43-.L6
	.long	.L42-.L6
	.long	.L41-.L6
	.long	.L40-.L6
	.long	.L39-.L6
	.long	.L38-.L6
	.long	.L37-.L6
	.long	.L36-.L6
	.long	.L35-.L6
	.long	.L34-.L6
	.long	.L33-.L6
	.long	.L32-.L6
	.long	.L31-.L6
	.long	.L30-.L6
	.long	.L29-.L6
	.long	.L28-.L6
	.long	.L27-.L6
	.long	.L26-.L6
	.long	.L25-.L6
	.long	.L24-.L6
	.long	.L23-.L6
	.long	.L22-.L6
	.long	.L21-.L6
	.long	.L20-.L6
	.long	.L19-.L6
	.long	.L18-.L6
	.long	.L17-.L6
	.long	.L16-.L6
	.long	.L15-.L6
	.long	.L14-.L6
	.long	.L13-.L6
	.long	.L12-.L6
	.long	.L11-.L6
	.long	.L10-.L6
	.long	.L9-.L6
	.long	.L8-.L6
	.long	.L7-.L6
	.long	.L5-.L6
	.section	.text._ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE
.L482:
	leaq	.LC186(%rip), %rax
	ret
.L241:
	leaq	.LC176(%rip), %rax
	ret
.L4:
	leaq	.LC0(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	leaq	.L262(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE
	.align 4
	.align 4
.L262:
	.long	.L480-.L262
	.long	.L479-.L262
	.long	.L478-.L262
	.long	.L477-.L262
	.long	.L476-.L262
	.long	.L475-.L262
	.long	.L474-.L262
	.long	.L473-.L262
	.long	.L472-.L262
	.long	.L471-.L262
	.long	.L470-.L262
	.long	.L469-.L262
	.long	.L468-.L262
	.long	.L467-.L262
	.long	.L466-.L262
	.long	.L465-.L262
	.long	.L464-.L262
	.long	.L463-.L262
	.long	.L462-.L262
	.long	.L461-.L262
	.long	.L260-.L262
	.long	.L260-.L262
	.long	.L260-.L262
	.long	.L260-.L262
	.long	.L260-.L262
	.long	.L260-.L262
	.long	.L460-.L262
	.long	.L459-.L262
	.long	.L459-.L262
	.long	.L260-.L262
	.long	.L260-.L262
	.long	.L260-.L262
	.long	.L458-.L262
	.long	.L457-.L262
	.long	.L456-.L262
	.long	.L455-.L262
	.long	.L454-.L262
	.long	.L453-.L262
	.long	.L452-.L262
	.long	.L260-.L262
	.long	.L451-.L262
	.long	.L450-.L262
	.long	.L449-.L262
	.long	.L448-.L262
	.long	.L447-.L262
	.long	.L446-.L262
	.long	.L445-.L262
	.long	.L444-.L262
	.long	.L443-.L262
	.long	.L442-.L262
	.long	.L441-.L262
	.long	.L440-.L262
	.long	.L439-.L262
	.long	.L438-.L262
	.long	.L437-.L262
	.long	.L436-.L262
	.long	.L435-.L262
	.long	.L434-.L262
	.long	.L433-.L262
	.long	.L432-.L262
	.long	.L431-.L262
	.long	.L430-.L262
	.long	.L429-.L262
	.long	.L428-.L262
	.long	.L427-.L262
	.long	.L426-.L262
	.long	.L425-.L262
	.long	.L424-.L262
	.long	.L423-.L262
	.long	.L483-.L262
	.long	.L422-.L262
	.long	.L421-.L262
	.long	.L420-.L262
	.long	.L419-.L262
	.long	.L418-.L262
	.long	.L417-.L262
	.long	.L416-.L262
	.long	.L415-.L262
	.long	.L414-.L262
	.long	.L413-.L262
	.long	.L412-.L262
	.long	.L411-.L262
	.long	.L410-.L262
	.long	.L409-.L262
	.long	.L408-.L262
	.long	.L407-.L262
	.long	.L406-.L262
	.long	.L405-.L262
	.long	.L404-.L262
	.long	.L403-.L262
	.long	.L402-.L262
	.long	.L401-.L262
	.long	.L400-.L262
	.long	.L399-.L262
	.long	.L398-.L262
	.long	.L397-.L262
	.long	.L396-.L262
	.long	.L395-.L262
	.long	.L394-.L262
	.long	.L393-.L262
	.long	.L392-.L262
	.long	.L391-.L262
	.long	.L390-.L262
	.long	.L389-.L262
	.long	.L388-.L262
	.long	.L387-.L262
	.long	.L386-.L262
	.long	.L385-.L262
	.long	.L384-.L262
	.long	.L383-.L262
	.long	.L382-.L262
	.long	.L381-.L262
	.long	.L380-.L262
	.long	.L379-.L262
	.long	.L378-.L262
	.long	.L377-.L262
	.long	.L376-.L262
	.long	.L375-.L262
	.long	.L374-.L262
	.long	.L373-.L262
	.long	.L372-.L262
	.long	.L371-.L262
	.long	.L370-.L262
	.long	.L369-.L262
	.long	.L368-.L262
	.long	.L367-.L262
	.long	.L366-.L262
	.long	.L365-.L262
	.long	.L364-.L262
	.long	.L363-.L262
	.long	.L362-.L262
	.long	.L361-.L262
	.long	.L360-.L262
	.long	.L359-.L262
	.long	.L358-.L262
	.long	.L357-.L262
	.long	.L356-.L262
	.long	.L355-.L262
	.long	.L354-.L262
	.long	.L353-.L262
	.long	.L352-.L262
	.long	.L351-.L262
	.long	.L350-.L262
	.long	.L349-.L262
	.long	.L348-.L262
	.long	.L347-.L262
	.long	.L346-.L262
	.long	.L345-.L262
	.long	.L344-.L262
	.long	.L343-.L262
	.long	.L342-.L262
	.long	.L341-.L262
	.long	.L340-.L262
	.long	.L339-.L262
	.long	.L338-.L262
	.long	.L337-.L262
	.long	.L336-.L262
	.long	.L335-.L262
	.long	.L334-.L262
	.long	.L333-.L262
	.long	.L332-.L262
	.long	.L331-.L262
	.long	.L330-.L262
	.long	.L329-.L262
	.long	.L328-.L262
	.long	.L327-.L262
	.long	.L326-.L262
	.long	.L325-.L262
	.long	.L324-.L262
	.long	.L323-.L262
	.long	.L322-.L262
	.long	.L321-.L262
	.long	.L320-.L262
	.long	.L319-.L262
	.long	.L318-.L262
	.long	.L317-.L262
	.long	.L316-.L262
	.long	.L315-.L262
	.long	.L314-.L262
	.long	.L313-.L262
	.long	.L312-.L262
	.long	.L311-.L262
	.long	.L310-.L262
	.long	.L309-.L262
	.long	.L308-.L262
	.long	.L307-.L262
	.long	.L306-.L262
	.long	.L305-.L262
	.long	.L304-.L262
	.long	.L303-.L262
	.long	.L302-.L262
	.long	.L301-.L262
	.long	.L300-.L262
	.long	.L299-.L262
	.long	.L298-.L262
	.long	.L297-.L262
	.long	.L296-.L262
	.long	.L295-.L262
	.long	.L294-.L262
	.long	.L293-.L262
	.long	.L292-.L262
	.long	.L291-.L262
	.long	.L290-.L262
	.long	.L289-.L262
	.long	.L288-.L262
	.long	.L287-.L262
	.long	.L286-.L262
	.long	.L285-.L262
	.long	.L284-.L262
	.long	.L283-.L262
	.long	.L282-.L262
	.long	.L281-.L262
	.long	.L280-.L262
	.long	.L279-.L262
	.long	.L278-.L262
	.long	.L277-.L262
	.long	.L276-.L262
	.long	.L275-.L262
	.long	.L274-.L262
	.long	.L273-.L262
	.long	.L272-.L262
	.long	.L271-.L262
	.long	.L270-.L262
	.long	.L269-.L262
	.long	.L268-.L262
	.long	.L267-.L262
	.long	.L266-.L262
	.long	.L265-.L262
	.long	.L264-.L262
	.long	.L263-.L262
	.long	.L261-.L262
	.section	.text._ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE
.L412:
	leaq	.LC474(%rip), %rax
	ret
.L483:
	leaq	.LC148(%rip), %rax
	ret
.L260:
	leaq	.LC0(%rip), %rax
	ret
.L75:
	leaq	.LC309(%rip), %rax
	ret
.L76:
	leaq	.LC310(%rip), %rax
	ret
.L459:
	leaq	.LC147(%rip), %rax
	ret
.L73:
	leaq	.LC305(%rip), %rax
	ret
.L74:
	leaq	.LC306(%rip), %rax
	ret
.L204:
	leaq	.LC354(%rip), %rax
	ret
.L209:
	leaq	.LC259(%rip), %rax
	ret
.L210:
	leaq	.LC253(%rip), %rax
	ret
.L211:
	leaq	.LC374(%rip), %rax
	ret
.L212:
	leaq	.LC371(%rip), %rax
	ret
.L213:
	leaq	.LC358(%rip), %rax
	ret
.L214:
	leaq	.LC355(%rip), %rax
	ret
.L215:
	leaq	.LC366(%rip), %rax
	ret
.L216:
	leaq	.LC363(%rip), %rax
	ret
.L217:
	leaq	.LC350(%rip), %rax
	ret
.L218:
	leaq	.LC347(%rip), %rax
	ret
.L219:
	leaq	.LC260(%rip), %rax
	ret
.L220:
	leaq	.LC254(%rip), %rax
	ret
.L221:
	leaq	.LC318(%rip), %rax
	ret
.L222:
	leaq	.LC317(%rip), %rax
	ret
.L223:
	leaq	.LC283(%rip), %rax
	ret
.L224:
	leaq	.LC320(%rip), %rax
	ret
.L225:
	leaq	.LC319(%rip), %rax
	ret
.L226:
	leaq	.LC241(%rip), %rax
	ret
.L227:
	leaq	.LC322(%rip), %rax
	ret
.L228:
	leaq	.LC321(%rip), %rax
	ret
.L229:
	leaq	.LC389(%rip), %rax
	ret
.L230:
	leaq	.LC326(%rip), %rax
	ret
.L231:
	leaq	.LC323(%rip), %rax
	ret
.L232:
	leaq	.LC242(%rip), %rax
	ret
.L233:
	leaq	.LC327(%rip), %rax
	ret
.L234:
	leaq	.LC324(%rip), %rax
	ret
.L235:
	leaq	.LC243(%rip), %rax
	ret
.L236:
	leaq	.LC328(%rip), %rax
	ret
.L237:
	leaq	.LC325(%rip), %rax
	ret
.L238:
	leaq	.LC244(%rip), %rax
	ret
.L239:
	leaq	.LC405(%rip), %rax
	ret
.L242:
	leaq	.LC240(%rip), %rax
	ret
.L243:
	leaq	.LC239(%rip), %rax
	ret
.L244:
	leaq	.LC238(%rip), %rax
	ret
.L245:
	leaq	.LC237(%rip), %rax
	ret
.L246:
	leaq	.LC236(%rip), %rax
	ret
.L247:
	leaq	.LC235(%rip), %rax
	ret
.L248:
	leaq	.LC234(%rip), %rax
	ret
.L249:
	leaq	.LC233(%rip), %rax
	ret
.L250:
	leaq	.LC232(%rip), %rax
	ret
.L251:
	leaq	.LC231(%rip), %rax
	ret
.L252:
	leaq	.LC229(%rip), %rax
	ret
.L253:
	leaq	.LC230(%rip), %rax
	ret
.L254:
	leaq	.LC227(%rip), %rax
	ret
.L255:
	leaq	.LC228(%rip), %rax
	ret
.L256:
	leaq	.LC225(%rip), %rax
	ret
.L257:
	leaq	.LC226(%rip), %rax
	ret
.L258:
	leaq	.LC223(%rip), %rax
	ret
.L259:
	leaq	.LC224(%rip), %rax
	ret
.L261:
	leaq	.LC222(%rip), %rax
	ret
.L263:
	leaq	.LC221(%rip), %rax
	ret
.L264:
	leaq	.LC220(%rip), %rax
	ret
.L265:
	leaq	.LC219(%rip), %rax
	ret
.L266:
	leaq	.LC212(%rip), %rax
	ret
.L267:
	leaq	.LC211(%rip), %rax
	ret
.L268:
	leaq	.LC210(%rip), %rax
	ret
.L269:
	leaq	.LC214(%rip), %rax
	ret
.L270:
	leaq	.LC213(%rip), %rax
	ret
.L271:
	leaq	.LC204(%rip), %rax
	ret
.L272:
	leaq	.LC203(%rip), %rax
	ret
.L273:
	leaq	.LC209(%rip), %rax
	ret
.L274:
	leaq	.LC208(%rip), %rax
	ret
.L275:
	leaq	.LC207(%rip), %rax
	ret
.L276:
	leaq	.LC206(%rip), %rax
	ret
.L277:
	leaq	.LC205(%rip), %rax
	ret
.L278:
	leaq	.LC218(%rip), %rax
	ret
.L279:
	leaq	.LC217(%rip), %rax
	ret
.L280:
	leaq	.LC216(%rip), %rax
	ret
.L281:
	leaq	.LC215(%rip), %rax
	ret
.L282:
	leaq	.LC100(%rip), %rax
	ret
.L283:
	leaq	.LC99(%rip), %rax
	ret
.L284:
	leaq	.LC98(%rip), %rax
	ret
.L285:
	leaq	.LC202(%rip), %rax
	ret
.L286:
	leaq	.LC201(%rip), %rax
	ret
.L287:
	leaq	.LC200(%rip), %rax
	ret
.L288:
	leaq	.LC199(%rip), %rax
	ret
.L289:
	leaq	.LC198(%rip), %rax
	ret
.L290:
	leaq	.LC197(%rip), %rax
	ret
.L291:
	leaq	.LC196(%rip), %rax
	ret
.L292:
	leaq	.LC195(%rip), %rax
	ret
.L78:
	leaq	.LC316(%rip), %rax
	ret
.L79:
	leaq	.LC290(%rip), %rax
	ret
.L80:
	leaq	.LC390(%rip), %rax
	ret
.L81:
	leaq	.LC391(%rip), %rax
	ret
.L67:
	leaq	.LC421(%rip), %rax
	ret
.L68:
	leaq	.LC418(%rip), %rax
	ret
.L69:
	leaq	.LC417(%rip), %rax
	ret
.L70:
	leaq	.LC416(%rip), %rax
	ret
.L59:
	leaq	.LC426(%rip), %rax
	ret
.L60:
	leaq	.LC428(%rip), %rax
	ret
.L61:
	leaq	.LC425(%rip), %rax
	ret
.L62:
	leaq	.LC424(%rip), %rax
	ret
.L63:
	leaq	.LC423(%rip), %rax
	ret
.L64:
	leaq	.LC422(%rip), %rax
	ret
.L65:
	leaq	.LC420(%rip), %rax
	ret
.L66:
	leaq	.LC419(%rip), %rax
	ret
.L43:
	leaq	.LC443(%rip), %rax
	ret
.L44:
	leaq	.LC441(%rip), %rax
	ret
.L45:
	leaq	.LC440(%rip), %rax
	ret
.L46:
	leaq	.LC442(%rip), %rax
	ret
.L47:
	leaq	.LC439(%rip), %rax
	ret
.L48:
	leaq	.LC438(%rip), %rax
	ret
.L49:
	leaq	.LC437(%rip), %rax
	ret
.L50:
	leaq	.LC436(%rip), %rax
	ret
.L51:
	leaq	.LC434(%rip), %rax
	ret
.L52:
	leaq	.LC433(%rip), %rax
	ret
.L53:
	leaq	.LC435(%rip), %rax
	ret
.L54:
	leaq	.LC432(%rip), %rax
	ret
.L55:
	leaq	.LC431(%rip), %rax
	ret
.L56:
	leaq	.LC430(%rip), %rax
	ret
.L57:
	leaq	.LC429(%rip), %rax
	ret
.L58:
	leaq	.LC427(%rip), %rax
	ret
.L11:
	leaq	.LC313(%rip), %rax
	ret
.L12:
	leaq	.LC1(%rip), %rax
	ret
.L13:
	leaq	.LC473(%rip), %rax
	ret
.L14:
	leaq	.LC472(%rip), %rax
	ret
.L15:
	leaq	.LC471(%rip), %rax
	ret
.L16:
	leaq	.LC469(%rip), %rax
	ret
.L17:
	leaq	.LC468(%rip), %rax
	ret
.L18:
	leaq	.LC470(%rip), %rax
	ret
.L19:
	leaq	.LC467(%rip), %rax
	ret
.L20:
	leaq	.LC466(%rip), %rax
	ret
.L21:
	leaq	.LC465(%rip), %rax
	ret
.L140:
	leaq	.LC265(%rip), %rax
	ret
.L141:
	leaq	.LC381(%rip), %rax
	ret
.L142:
	leaq	.LC378(%rip), %rax
	ret
.L143:
	leaq	.LC386(%rip), %rax
	ret
.L144:
	leaq	.LC411(%rip), %rax
	ret
.L145:
	leaq	.LC410(%rip), %rax
	ret
.L146:
	leaq	.LC247(%rip), %rax
	ret
.L147:
	leaq	.LC342(%rip), %rax
	ret
.L148:
	leaq	.LC339(%rip), %rax
	ret
.L149:
	leaq	.LC334(%rip), %rax
	ret
.L150:
	leaq	.LC331(%rip), %rax
	ret
.L151:
	leaq	.LC278(%rip), %rax
	ret
.L152:
	leaq	.LC399(%rip), %rax
	ret
.L153:
	leaq	.LC398(%rip), %rax
	ret
.L154:
	leaq	.LC272(%rip), %rax
	ret
.L155:
	leaq	.LC395(%rip), %rax
	ret
.L156:
	leaq	.LC394(%rip), %rax
	ret
.L157:
	leaq	.LC266(%rip), %rax
	ret
.L158:
	leaq	.LC382(%rip), %rax
	ret
.L159:
	leaq	.LC379(%rip), %rax
	ret
.L160:
	leaq	.LC387(%rip), %rax
	ret
.L161:
	leaq	.LC413(%rip), %rax
	ret
.L162:
	leaq	.LC412(%rip), %rax
	ret
.L163:
	leaq	.LC248(%rip), %rax
	ret
.L164:
	leaq	.LC404(%rip), %rax
	ret
.L165:
	leaq	.LC402(%rip), %rax
	ret
.L166:
	leaq	.LC401(%rip), %rax
	ret
.L167:
	leaq	.LC400(%rip), %rax
	ret
.L168:
	leaq	.LC403(%rip), %rax
	ret
.L169:
	leaq	.LC287(%rip), %rax
	ret
.L170:
	leaq	.LC285(%rip), %rax
	ret
.L171:
	leaq	.LC286(%rip), %rax
	ret
.L172:
	leaq	.LC284(%rip), %rax
	ret
.L173:
	leaq	.LC261(%rip), %rax
	ret
.L174:
	leaq	.LC255(%rip), %rax
	ret
.L175:
	leaq	.LC300(%rip), %rax
	ret
.L176:
	leaq	.LC298(%rip), %rax
	ret
.L177:
	leaq	.LC299(%rip), %rax
	ret
.L178:
	leaq	.LC297(%rip), %rax
	ret
.L179:
	leaq	.LC257(%rip), %rax
	ret
.L180:
	leaq	.LC251(%rip), %rax
	ret
.L181:
	leaq	.LC376(%rip), %rax
	ret
.L182:
	leaq	.LC375(%rip), %rax
	ret
.L183:
	leaq	.LC360(%rip), %rax
	ret
.L184:
	leaq	.LC359(%rip), %rax
	ret
.L185:
	leaq	.LC368(%rip), %rax
	ret
.L186:
	leaq	.LC367(%rip), %rax
	ret
.L187:
	leaq	.LC352(%rip), %rax
	ret
.L188:
	leaq	.LC351(%rip), %rax
	ret
.L189:
	leaq	.LC262(%rip), %rax
	ret
.L190:
	leaq	.LC256(%rip), %rax
	ret
.L191:
	leaq	.LC372(%rip), %rax
	ret
.L192:
	leaq	.LC369(%rip), %rax
	ret
.L193:
	leaq	.LC356(%rip), %rax
	ret
.L194:
	leaq	.LC353(%rip), %rax
	ret
.L195:
	leaq	.LC364(%rip), %rax
	ret
.L196:
	leaq	.LC361(%rip), %rax
	ret
.L197:
	leaq	.LC348(%rip), %rax
	ret
.L198:
	leaq	.LC345(%rip), %rax
	ret
.L199:
	leaq	.LC258(%rip), %rax
	ret
.L200:
	leaq	.LC252(%rip), %rax
	ret
.L201:
	leaq	.LC373(%rip), %rax
	ret
.L202:
	leaq	.LC370(%rip), %rax
	ret
.L203:
	leaq	.LC357(%rip), %rax
	ret
.L108:
	leaq	.LC274(%rip), %rax
	ret
.L109:
	leaq	.LC280(%rip), %rax
	ret
.L110:
	leaq	.LC268(%rip), %rax
	ret
.L111:
	leaq	.LC384(%rip), %rax
	ret
.L112:
	leaq	.LC383(%rip), %rax
	ret
.L113:
	leaq	.LC388(%rip), %rax
	ret
.L114:
	leaq	.LC407(%rip), %rax
	ret
.L115:
	leaq	.LC406(%rip), %rax
	ret
.L116:
	leaq	.LC250(%rip), %rax
	ret
.L117:
	leaq	.LC340(%rip), %rax
	ret
.L118:
	leaq	.LC337(%rip), %rax
	ret
.L119:
	leaq	.LC332(%rip), %rax
	ret
.L120:
	leaq	.LC329(%rip), %rax
	ret
.L121:
	leaq	.LC276(%rip), %rax
	ret
.L122:
	leaq	.LC270(%rip), %rax
	ret
.L123:
	leaq	.LC264(%rip), %rax
	ret
.L124:
	leaq	.LC380(%rip), %rax
	ret
.L125:
	leaq	.LC377(%rip), %rax
	ret
.L126:
	leaq	.LC385(%rip), %rax
	ret
.L127:
	leaq	.LC409(%rip), %rax
	ret
.L128:
	leaq	.LC408(%rip), %rax
	ret
.L205:
	leaq	.LC365(%rip), %rax
	ret
.L206:
	leaq	.LC362(%rip), %rax
	ret
.L207:
	leaq	.LC349(%rip), %rax
	ret
.L208:
	leaq	.LC346(%rip), %rax
	ret
.L414:
	leaq	.LC42(%rip), %rax
	ret
.L415:
	leaq	.LC38(%rip), %rax
	ret
.L416:
	leaq	.LC36(%rip), %rax
	ret
.L421:
	leaq	.LC8(%rip), %rax
	ret
.L405:
	leaq	.LC37(%rip), %rax
	ret
.L406:
	leaq	.LC33(%rip), %rax
	ret
.L407:
	leaq	.LC31(%rip), %rax
	ret
.L408:
	leaq	.LC27(%rip), %rax
	ret
.L409:
	leaq	.LC25(%rip), %rax
	ret
.L410:
	leaq	.LC9(%rip), %rax
	ret
.L411:
	leaq	.LC5(%rip), %rax
	ret
.L413:
	leaq	.LC44(%rip), %rax
	ret
.L389:
	leaq	.LC46(%rip), %rax
	ret
.L390:
	leaq	.LC41(%rip), %rax
	ret
.L391:
	leaq	.LC35(%rip), %rax
	ret
.L392:
	leaq	.LC29(%rip), %rax
	ret
.L393:
	leaq	.LC23(%rip), %rax
	ret
.L394:
	leaq	.LC7(%rip), %rax
	ret
.L395:
	leaq	.LC3(%rip), %rax
	ret
.L396:
	leaq	.LC40(%rip), %rax
	ret
.L397:
	leaq	.LC34(%rip), %rax
	ret
.L398:
	leaq	.LC28(%rip), %rax
	ret
.L399:
	leaq	.LC22(%rip), %rax
	ret
.L400:
	leaq	.LC6(%rip), %rax
	ret
.L401:
	leaq	.LC2(%rip), %rax
	ret
.L402:
	leaq	.LC45(%rip), %rax
	ret
.L403:
	leaq	.LC43(%rip), %rax
	ret
.L404:
	leaq	.LC39(%rip), %rax
	ret
.L357:
	leaq	.LC71(%rip), %rax
	ret
.L358:
	leaq	.LC69(%rip), %rax
	ret
.L359:
	leaq	.LC67(%rip), %rax
	ret
.L360:
	leaq	.LC65(%rip), %rax
	ret
.L361:
	leaq	.LC63(%rip), %rax
	ret
.L362:
	leaq	.LC61(%rip), %rax
	ret
.L363:
	leaq	.LC59(%rip), %rax
	ret
.L364:
	leaq	.LC57(%rip), %rax
	ret
.L365:
	leaq	.LC55(%rip), %rax
	ret
.L366:
	leaq	.LC21(%rip), %rax
	ret
.L367:
	leaq	.LC17(%rip), %rax
	ret
.L368:
	leaq	.LC13(%rip), %rax
	ret
.L369:
	leaq	.LC51(%rip), %rax
	ret
.L370:
	leaq	.LC49(%rip), %rax
	ret
.L371:
	leaq	.LC47(%rip), %rax
	ret
.L372:
	leaq	.LC76(%rip), %rax
	ret
.L373:
	leaq	.LC74(%rip), %rax
	ret
.L374:
	leaq	.LC72(%rip), %rax
	ret
.L375:
	leaq	.LC70(%rip), %rax
	ret
.L376:
	leaq	.LC68(%rip), %rax
	ret
.L377:
	leaq	.LC66(%rip), %rax
	ret
.L378:
	leaq	.LC64(%rip), %rax
	ret
.L379:
	leaq	.LC62(%rip), %rax
	ret
.L380:
	leaq	.LC60(%rip), %rax
	ret
.L381:
	leaq	.LC58(%rip), %rax
	ret
.L382:
	leaq	.LC56(%rip), %rax
	ret
.L383:
	leaq	.LC54(%rip), %rax
	ret
.L384:
	leaq	.LC20(%rip), %rax
	ret
.L385:
	leaq	.LC16(%rip), %rax
	ret
.L386:
	leaq	.LC12(%rip), %rax
	ret
.L387:
	leaq	.LC50(%rip), %rax
	ret
.L388:
	leaq	.LC48(%rip), %rax
	ret
.L293:
	leaq	.LC194(%rip), %rax
	ret
.L294:
	leaq	.LC193(%rip), %rax
	ret
.L295:
	leaq	.LC192(%rip), %rax
	ret
.L296:
	leaq	.LC130(%rip), %rax
	ret
.L297:
	leaq	.LC129(%rip), %rax
	ret
.L298:
	leaq	.LC127(%rip), %rax
	ret
.L299:
	leaq	.LC128(%rip), %rax
	ret
.L300:
	leaq	.LC126(%rip), %rax
	ret
.L301:
	leaq	.LC125(%rip), %rax
	ret
.L302:
	leaq	.LC124(%rip), %rax
	ret
.L303:
	leaq	.LC123(%rip), %rax
	ret
.L304:
	leaq	.LC122(%rip), %rax
	ret
.L305:
	leaq	.LC121(%rip), %rax
	ret
.L306:
	leaq	.LC119(%rip), %rax
	ret
.L307:
	leaq	.LC120(%rip), %rax
	ret
.L308:
	leaq	.LC117(%rip), %rax
	ret
.L309:
	leaq	.LC118(%rip), %rax
	ret
.L310:
	leaq	.LC116(%rip), %rax
	ret
.L311:
	leaq	.LC114(%rip), %rax
	ret
.L312:
	leaq	.LC115(%rip), %rax
	ret
.L313:
	leaq	.LC112(%rip), %rax
	ret
.L314:
	leaq	.LC113(%rip), %rax
	ret
.L315:
	leaq	.LC107(%rip), %rax
	ret
.L316:
	leaq	.LC109(%rip), %rax
	ret
.L317:
	leaq	.LC103(%rip), %rax
	ret
.L318:
	leaq	.LC105(%rip), %rax
	ret
.L319:
	leaq	.LC110(%rip), %rax
	ret
.L320:
	leaq	.LC111(%rip), %rax
	ret
.L321:
	leaq	.LC106(%rip), %rax
	ret
.L322:
	leaq	.LC108(%rip), %rax
	ret
.L323:
	leaq	.LC102(%rip), %rax
	ret
.L324:
	leaq	.LC104(%rip), %rax
	ret
.L325:
	leaq	.LC101(%rip), %rax
	ret
.L326:
	leaq	.LC97(%rip), %rax
	ret
.L327:
	leaq	.LC95(%rip), %rax
	ret
.L328:
	leaq	.LC93(%rip), %rax
	ret
.L329:
	leaq	.LC53(%rip), %rax
	ret
.L330:
	leaq	.LC19(%rip), %rax
	ret
.L331:
	leaq	.LC15(%rip), %rax
	ret
.L332:
	leaq	.LC11(%rip), %rax
	ret
.L333:
	leaq	.LC91(%rip), %rax
	ret
.L334:
	leaq	.LC89(%rip), %rax
	ret
.L335:
	leaq	.LC87(%rip), %rax
	ret
.L336:
	leaq	.LC85(%rip), %rax
	ret
.L337:
	leaq	.LC83(%rip), %rax
	ret
.L338:
	leaq	.LC81(%rip), %rax
	ret
.L339:
	leaq	.LC79(%rip), %rax
	ret
.L340:
	leaq	.LC96(%rip), %rax
	ret
.L341:
	leaq	.LC94(%rip), %rax
	ret
.L342:
	leaq	.LC92(%rip), %rax
	ret
.L343:
	leaq	.LC52(%rip), %rax
	ret
.L344:
	leaq	.LC18(%rip), %rax
	ret
.L345:
	leaq	.LC14(%rip), %rax
	ret
.L346:
	leaq	.LC10(%rip), %rax
	ret
.L347:
	leaq	.LC90(%rip), %rax
	ret
.L348:
	leaq	.LC88(%rip), %rax
	ret
.L349:
	leaq	.LC86(%rip), %rax
	ret
.L350:
	leaq	.LC84(%rip), %rax
	ret
.L351:
	leaq	.LC82(%rip), %rax
	ret
.L352:
	leaq	.LC80(%rip), %rax
	ret
.L353:
	leaq	.LC78(%rip), %rax
	ret
.L354:
	leaq	.LC77(%rip), %rax
	ret
.L355:
	leaq	.LC75(%rip), %rax
	ret
.L356:
	leaq	.LC73(%rip), %rax
	ret
.L129:
	leaq	.LC246(%rip), %rax
	ret
.L130:
	leaq	.LC341(%rip), %rax
	ret
.L131:
	leaq	.LC338(%rip), %rax
	ret
.L132:
	leaq	.LC333(%rip), %rax
	ret
.L133:
	leaq	.LC330(%rip), %rax
	ret
.L134:
	leaq	.LC277(%rip), %rax
	ret
.L135:
	leaq	.LC397(%rip), %rax
	ret
.L136:
	leaq	.LC396(%rip), %rax
	ret
.L137:
	leaq	.LC271(%rip), %rax
	ret
.L138:
	leaq	.LC393(%rip), %rax
	ret
.L139:
	leaq	.LC392(%rip), %rax
	ret
.L92:
	leaq	.LC249(%rip), %rax
	ret
.L93:
	leaq	.LC288(%rip), %rax
	ret
.L94:
	leaq	.LC296(%rip), %rax
	ret
.L95:
	leaq	.LC294(%rip), %rax
	ret
.L96:
	leaq	.LC282(%rip), %rax
	ret
.L97:
	leaq	.LC275(%rip), %rax
	ret
.L98:
	leaq	.LC269(%rip), %rax
	ret
.L99:
	leaq	.LC263(%rip), %rax
	ret
.L100:
	leaq	.LC292(%rip), %rax
	ret
.L101:
	leaq	.LC291(%rip), %rax
	ret
.L102:
	leaq	.LC245(%rip), %rax
	ret
.L103:
	leaq	.LC289(%rip), %rax
	ret
.L104:
	leaq	.LC344(%rip), %rax
	ret
.L105:
	leaq	.LC343(%rip), %rax
	ret
.L106:
	leaq	.LC336(%rip), %rax
	ret
.L107:
	leaq	.LC335(%rip), %rax
	ret
.L84:
	leaq	.LC303(%rip), %rax
	ret
.L85:
	leaq	.LC304(%rip), %rax
	ret
.L86:
	leaq	.LC295(%rip), %rax
	ret
.L87:
	leaq	.LC293(%rip), %rax
	ret
.L88:
	leaq	.LC281(%rip), %rax
	ret
.L89:
	leaq	.LC279(%rip), %rax
	ret
.L90:
	leaq	.LC273(%rip), %rax
	ret
.L91:
	leaq	.LC267(%rip), %rax
	ret
.L71:
	leaq	.LC415(%rip), %rax
	ret
.L72:
	leaq	.LC414(%rip), %rax
	ret
.L82:
	leaq	.LC301(%rip), %rax
	ret
.L83:
	leaq	.LC302(%rip), %rax
	ret
.L417:
	leaq	.LC32(%rip), %rax
	ret
.L418:
	leaq	.LC30(%rip), %rax
	ret
.L419:
	leaq	.LC26(%rip), %rax
	ret
.L420:
	leaq	.LC24(%rip), %rax
	ret
.L22:
	leaq	.LC464(%rip), %rax
	ret
.L23:
	leaq	.LC462(%rip), %rax
	ret
.L24:
	leaq	.LC461(%rip), %rax
	ret
.L25:
	leaq	.LC463(%rip), %rax
	ret
.L26:
	leaq	.LC460(%rip), %rax
	ret
.L27:
	leaq	.LC459(%rip), %rax
	ret
.L28:
	leaq	.LC458(%rip), %rax
	ret
.L29:
	leaq	.LC457(%rip), %rax
	ret
.L30:
	leaq	.LC455(%rip), %rax
	ret
.L31:
	leaq	.LC454(%rip), %rax
	ret
.L32:
	leaq	.LC456(%rip), %rax
	ret
.L33:
	leaq	.LC453(%rip), %rax
	ret
.L34:
	leaq	.LC452(%rip), %rax
	ret
.L35:
	leaq	.LC451(%rip), %rax
	ret
.L36:
	leaq	.LC450(%rip), %rax
	ret
.L37:
	leaq	.LC448(%rip), %rax
	ret
.L38:
	leaq	.LC447(%rip), %rax
	ret
.L39:
	leaq	.LC449(%rip), %rax
	ret
.L40:
	leaq	.LC446(%rip), %rax
	ret
.L41:
	leaq	.LC445(%rip), %rax
	ret
.L42:
	leaq	.LC444(%rip), %rax
	ret
.L77:
	leaq	.LC315(%rip), %rax
	ret
.L458:
	leaq	.LC149(%rip), %rax
	ret
.L460:
	leaq	.LC146(%rip), %rax
	ret
.L461:
	leaq	.LC145(%rip), %rax
	ret
.L454:
	leaq	.LC153(%rip), %rax
	ret
.L455:
	leaq	.LC152(%rip), %rax
	ret
.L456:
	leaq	.LC151(%rip), %rax
	ret
.L457:
	leaq	.LC150(%rip), %rax
	ret
.L462:
	leaq	.LC144(%rip), %rax
	ret
.L463:
	leaq	.LC143(%rip), %rax
	ret
.L464:
	leaq	.LC142(%rip), %rax
	ret
.L465:
	leaq	.LC141(%rip), %rax
	ret
.L466:
	leaq	.LC140(%rip), %rax
	ret
.L467:
	leaq	.LC139(%rip), %rax
	ret
.L468:
	leaq	.LC138(%rip), %rax
	ret
.L469:
	leaq	.LC137(%rip), %rax
	ret
.L470:
	leaq	.LC191(%rip), %rax
	ret
.L471:
	leaq	.LC190(%rip), %rax
	ret
.L472:
	leaq	.LC189(%rip), %rax
	ret
.L473:
	leaq	.LC188(%rip), %rax
	ret
.L474:
	leaq	.LC187(%rip), %rax
	ret
.L475:
	leaq	.LC136(%rip), %rax
	ret
.L476:
	leaq	.LC135(%rip), %rax
	ret
.L477:
	leaq	.LC134(%rip), %rax
	ret
.L478:
	leaq	.LC133(%rip), %rax
	ret
.L479:
	leaq	.LC132(%rip), %rax
	ret
.L480:
	leaq	.LC131(%rip), %rax
	ret
.L5:
	leaq	.LC307(%rip), %rax
	ret
.L7:
	leaq	.LC308(%rip), %rax
	ret
.L8:
	leaq	.LC311(%rip), %rax
	ret
.L9:
	leaq	.LC312(%rip), %rax
	ret
.L10:
	leaq	.LC314(%rip), %rax
	ret
.L422:
	leaq	.LC4(%rip), %rax
	ret
.L423:
	leaq	.LC157(%rip), %rax
	ret
.L424:
	leaq	.LC156(%rip), %rax
	ret
.L425:
	leaq	.LC159(%rip), %rax
	ret
.L426:
	leaq	.LC158(%rip), %rax
	ret
.L427:
	leaq	.LC161(%rip), %rax
	ret
.L428:
	leaq	.LC160(%rip), %rax
	ret
.L429:
	leaq	.LC185(%rip), %rax
	ret
.L430:
	leaq	.LC184(%rip), %rax
	ret
.L431:
	leaq	.LC182(%rip), %rax
	ret
.L432:
	leaq	.LC183(%rip), %rax
	ret
.L433:
	leaq	.LC181(%rip), %rax
	ret
.L434:
	leaq	.LC178(%rip), %rax
	ret
.L435:
	leaq	.LC177(%rip), %rax
	ret
.L436:
	leaq	.LC180(%rip), %rax
	ret
.L437:
	leaq	.LC179(%rip), %rax
	ret
.L438:
	leaq	.LC175(%rip), %rax
	ret
.L439:
	leaq	.LC174(%rip), %rax
	ret
.L440:
	leaq	.LC173(%rip), %rax
	ret
.L441:
	leaq	.LC171(%rip), %rax
	ret
.L442:
	leaq	.LC169(%rip), %rax
	ret
.L443:
	leaq	.LC167(%rip), %rax
	ret
.L444:
	leaq	.LC172(%rip), %rax
	ret
.L445:
	leaq	.LC170(%rip), %rax
	ret
.L446:
	leaq	.LC168(%rip), %rax
	ret
.L447:
	leaq	.LC166(%rip), %rax
	ret
.L448:
	leaq	.LC163(%rip), %rax
	ret
.L449:
	leaq	.LC162(%rip), %rax
	ret
.L450:
	leaq	.LC165(%rip), %rax
	ret
.L451:
	leaq	.LC164(%rip), %rax
	ret
.L452:
	leaq	.LC155(%rip), %rax
	ret
.L453:
	leaq	.LC154(%rip), %rax
	ret
.L487:
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE5584:
	.size	_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE
	.section	.text._ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE
	.type	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE:
.LFB5585:
	.cfi_startproc
	endbr64
	subl	$252, %edi
	cmpl	$2, %edi
	setbe	%al
	ret
	.cfi_endproc
.LFE5585:
	.size	_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm11WasmOpcodes14IsPrefixOpcodeENS1_10WasmOpcodeE
	.section	.text._ZN2v88internal4wasm11WasmOpcodes15IsControlOpcodeENS1_10WasmOpcodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11WasmOpcodes15IsControlOpcodeENS1_10WasmOpcodeE
	.type	_ZN2v88internal4wasm11WasmOpcodes15IsControlOpcodeENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm11WasmOpcodes15IsControlOpcodeENS1_10WasmOpcodeE:
.LFB5586:
	.cfi_startproc
	endbr64
	cmpl	$15, %edi
	setbe	%al
	ret
	.cfi_endproc
.LFE5586:
	.size	_ZN2v88internal4wasm11WasmOpcodes15IsControlOpcodeENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm11WasmOpcodes15IsControlOpcodeENS1_10WasmOpcodeE
	.section	.text._ZN2v88internal4wasm11WasmOpcodes19IsUnconditionalJumpENS1_10WasmOpcodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11WasmOpcodes19IsUnconditionalJumpENS1_10WasmOpcodeE
	.type	_ZN2v88internal4wasm11WasmOpcodes19IsUnconditionalJumpENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm11WasmOpcodes19IsUnconditionalJumpENS1_10WasmOpcodeE:
.LFB5587:
	.cfi_startproc
	endbr64
	movl	%edi, %ecx
	cmpl	$15, %edi
	ja	.L492
	movl	$1, %eax
	salq	%cl, %rax
	testl	$53249, %eax
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L492:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5587:
	.size	_ZN2v88internal4wasm11WasmOpcodes19IsUnconditionalJumpENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm11WasmOpcodes19IsUnconditionalJumpENS1_10WasmOpcodeE
	.section	.text._ZN2v88internal4wasm11WasmOpcodes21IsSignExtensionOpcodeENS1_10WasmOpcodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11WasmOpcodes21IsSignExtensionOpcodeENS1_10WasmOpcodeE
	.type	_ZN2v88internal4wasm11WasmOpcodes21IsSignExtensionOpcodeENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm11WasmOpcodes21IsSignExtensionOpcodeENS1_10WasmOpcodeE:
.LFB5588:
	.cfi_startproc
	endbr64
	subl	$192, %edi
	cmpl	$4, %edi
	setbe	%al
	ret
	.cfi_endproc
.LFE5588:
	.size	_ZN2v88internal4wasm11WasmOpcodes21IsSignExtensionOpcodeENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm11WasmOpcodes21IsSignExtensionOpcodeENS1_10WasmOpcodeE
	.section	.text._ZN2v88internal4wasm11WasmOpcodes14IsAnyRefOpcodeENS1_10WasmOpcodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11WasmOpcodes14IsAnyRefOpcodeENS1_10WasmOpcodeE
	.type	_ZN2v88internal4wasm11WasmOpcodes14IsAnyRefOpcodeENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm11WasmOpcodes14IsAnyRefOpcodeENS1_10WasmOpcodeE:
.LFB5589:
	.cfi_startproc
	endbr64
	subl	$208, %edi
	cmpl	$2, %edi
	setbe	%al
	ret
	.cfi_endproc
.LFE5589:
	.size	_ZN2v88internal4wasm11WasmOpcodes14IsAnyRefOpcodeENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm11WasmOpcodes14IsAnyRefOpcodeENS1_10WasmOpcodeE
	.section	.text._ZN2v88internal4wasm11WasmOpcodes16IsThrowingOpcodeENS1_10WasmOpcodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11WasmOpcodes16IsThrowingOpcodeENS1_10WasmOpcodeE
	.type	_ZN2v88internal4wasm11WasmOpcodes16IsThrowingOpcodeENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm11WasmOpcodes16IsThrowingOpcodeENS1_10WasmOpcodeE:
.LFB5590:
	.cfi_startproc
	endbr64
	cmpl	$9, %edi
	ja	.L496
	cmpl	$7, %edi
	seta	%al
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	subl	$16, %edi
	cmpl	$1, %edi
	setbe	%al
	ret
	.cfi_endproc
.LFE5590:
	.size	_ZN2v88internal4wasm11WasmOpcodes16IsThrowingOpcodeENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm11WasmOpcodes16IsThrowingOpcodeENS1_10WasmOpcodeE
	.section	.rodata._ZN2v88internal4wasmlsERSoRKNS0_9SignatureINS1_9ValueTypeEEE.str1.1,"aMS",@progbits,1
.LC475:
	.string	"v"
.LC476:
	.string	"_"
	.section	.text._ZN2v88internal4wasmlsERSoRKNS0_9SignatureINS1_9ValueTypeEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasmlsERSoRKNS0_9SignatureINS1_9ValueTypeEEE
	.type	_ZN2v88internal4wasmlsERSoRKNS0_9SignatureINS1_9ValueTypeEEE, @function
_ZN2v88internal4wasmlsERSoRKNS0_9SignatureINS1_9ValueTypeEEE:
.LFB5591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	(%rsi), %r13
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testq	%r13, %r13
	je	.L521
.L499:
	movq	-72(%rbp), %rax
	leaq	-57(%rbp), %r12
	leaq	CSWTCH.27(%rip), %r14
	movq	16(%rax), %rbx
	addq	%rbx, %r13
	cmpq	%rbx, %r13
	jne	.L506
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L522:
	movzbl	(%r14,%rax), %eax
	movb	%al, -57(%rbp)
.L518:
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	addq	$1, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%rbx, %r13
	je	.L504
.L506:
	movzbl	(%rbx), %eax
	cmpb	$10, %al
	jbe	.L522
	movb	$63, -57(%rbp)
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L504:
	movl	$1, %edx
	leaq	.LC476(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rax
	movq	8(%rax), %r13
	testq	%r13, %r13
	je	.L523
.L502:
	movq	-72(%rbp), %rax
	leaq	-57(%rbp), %r12
	leaq	CSWTCH.27(%rip), %r14
	movq	16(%rax), %rdx
	movq	(%rax), %rax
	addq	%rax, %r13
	leaq	(%rdx,%rax), %rbx
	addq	%rdx, %r13
	cmpq	%r13, %rbx
	jne	.L511
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L524:
	movzbl	(%r14,%rax), %eax
	movb	%al, -57(%rbp)
.L520:
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	addq	$1, %rbx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r13, %rbx
	je	.L509
.L511:
	movzbl	(%rbx), %eax
	cmpb	$10, %al
	jbe	.L524
	movb	$63, -57(%rbp)
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L509:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L525
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L521:
	.cfi_restore_state
	movl	$1, %edx
	leaq	.LC475(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rax
	movq	(%rax), %r13
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L523:
	movl	$1, %edx
	leaq	.LC475(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %rax
	movq	8(%rax), %r13
	jmp	.L502
.L525:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5591:
	.size	_ZN2v88internal4wasmlsERSoRKNS0_9SignatureINS1_9ValueTypeEEE, .-_ZN2v88internal4wasmlsERSoRKNS0_9SignatureINS1_9ValueTypeEEE
	.section	.text._ZN2v88internal4wasm23IsJSCompatibleSignatureEPKNS0_9SignatureINS1_9ValueTypeEEERKNS1_12WasmFeaturesE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal4wasm23IsJSCompatibleSignatureEPKNS0_9SignatureINS1_9ValueTypeEEERKNS1_12WasmFeaturesE
	.type	_ZN2v88internal4wasm23IsJSCompatibleSignatureEPKNS0_9SignatureINS1_9ValueTypeEEERKNS1_12WasmFeaturesE, @function
_ZN2v88internal4wasm23IsJSCompatibleSignatureEPKNS0_9SignatureINS1_9ValueTypeEEERKNS1_12WasmFeaturesE:
.LFB5592:
	.cfi_startproc
	endbr64
	movzbl	(%rsi), %eax
	movq	(%rdi), %rcx
	testb	%al, %al
	jne	.L527
	cmpq	$1, %rcx
	ja	.L526
.L527:
	movq	16(%rdi), %rdx
	addq	8(%rdi), %rcx
	movq	%rcx, %rdi
	addq	%rdx, %rdi
	cmpq	%rdi, %rdx
	je	.L530
	movzbl	4(%rsi), %esi
	xorl	$1, %esi
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L536:
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	je	.L530
.L529:
	movzbl	(%rdx), %ecx
	cmpb	$2, %cl
	sete	%al
	andb	%sil, %al
	jne	.L531
	cmpb	$5, %cl
	jne	.L536
.L526:
	ret
	.p2align 4,,10
	.p2align 3
.L530:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5592:
	.size	_ZN2v88internal4wasm23IsJSCompatibleSignatureEPKNS0_9SignatureINS1_9ValueTypeEEERKNS1_12WasmFeaturesE, .-_ZN2v88internal4wasm23IsJSCompatibleSignatureEPKNS0_9SignatureINS1_9ValueTypeEEERKNS1_12WasmFeaturesE
	.section	.rodata._ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE.str1.1,"aMS",@progbits,1
.LC477:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE
	.type	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE:
.LFB5611:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	sarl	$8, %eax
	cmpl	$253, %eax
	je	.L538
	jg	.L539
	testl	%eax, %eax
	je	.L540
	cmpl	$252, %eax
	jne	.L542
	movzbl	%dil, %edi
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L20kNumericExprSigTableE(%rip), %rax
	movzbl	(%rax,%rdi), %edx
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L11kCachedSigsE(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	cmpl	$254, %eax
	jne	.L542
	movzbl	%dil, %edi
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L19kAtomicExprSigTableE(%rip), %rax
	movzbl	(%rax,%rdi), %edx
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L11kCachedSigsE(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	movzbl	%dil, %edi
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L17kSimdExprSigTableE(%rip), %rax
	movzbl	(%rax,%rdi), %edx
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L11kCachedSigsE(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L540:
	movl	%edi, %edi
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L14kShortSigTableE(%rip), %rax
	movzbl	(%rax,%rdi), %edx
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L11kCachedSigsE(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	ret
.L542:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC477(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5611:
	.size	_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm11WasmOpcodes9SignatureENS1_10WasmOpcodeE
	.section	.text._ZN2v88internal4wasm11WasmOpcodes14AsmjsSignatureENS1_10WasmOpcodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11WasmOpcodes14AsmjsSignatureENS1_10WasmOpcodeE
	.type	_ZN2v88internal4wasm11WasmOpcodes14AsmjsSignatureENS1_10WasmOpcodeE, @function
_ZN2v88internal4wasm11WasmOpcodes14AsmjsSignatureENS1_10WasmOpcodeE:
.LFB5614:
	.cfi_startproc
	endbr64
	movl	%edi, %edi
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L24kSimpleAsmjsExprSigTableE(%rip), %rax
	movzbl	(%rax,%rdi), %edx
	leaq	_ZN2v88internal4wasm12_GLOBAL__N_1L11kCachedSigsE(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	ret
	.cfi_endproc
.LFE5614:
	.size	_ZN2v88internal4wasm11WasmOpcodes14AsmjsSignatureENS1_10WasmOpcodeE, .-_ZN2v88internal4wasm11WasmOpcodes14AsmjsSignatureENS1_10WasmOpcodeE
	.section	.text._ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE
	.type	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE, @function
_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE:
.LFB5615:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpl	$11, %edi
	ja	.L548
	movl	%edi, %edi
	leaq	CSWTCH.38(%rip), %rax
	movl	(%rax,%rdi,4), %eax
.L548:
	ret
	.cfi_endproc
.LFE5615:
	.size	_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE, .-_ZN2v88internal4wasm11WasmOpcodes21TrapReasonToMessageIdENS1_10TrapReasonE
	.section	.text._ZN2v88internal4wasm11WasmOpcodes17TrapReasonMessageENS1_10TrapReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11WasmOpcodes17TrapReasonMessageENS1_10TrapReasonE
	.type	_ZN2v88internal4wasm11WasmOpcodes17TrapReasonMessageENS1_10TrapReasonE, @function
_ZN2v88internal4wasm11WasmOpcodes17TrapReasonMessageENS1_10TrapReasonE:
.LFB5616:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	cmpl	$11, %edi
	ja	.L552
	movl	%edi, %edi
	leaq	CSWTCH.38(%rip), %rax
	movl	(%rax,%rdi,4), %r8d
.L552:
	movl	%r8d, %edi
	jmp	_ZN2v88internal16MessageFormatter14TemplateStringENS0_15MessageTemplateE@PLT
	.cfi_endproc
.LFE5616:
	.size	_ZN2v88internal4wasm11WasmOpcodes17TrapReasonMessageENS1_10TrapReasonE, .-_ZN2v88internal4wasm11WasmOpcodes17TrapReasonMessageENS1_10TrapReasonE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE, @function
_GLOBAL__sub_I__ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE:
.LFB6436:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE6436:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE, .-_GLOBAL__sub_I__ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm11WasmOpcodes10OpcodeNameENS1_10WasmOpcodeE
	.section	.rodata.CSWTCH.38,"a"
	.align 32
	.type	CSWTCH.38, @object
	.size	CSWTCH.38, 48
CSWTCH.38:
	.long	340
	.long	341
	.long	342
	.long	343
	.long	344
	.long	345
	.long	346
	.long	347
	.long	348
	.long	350
	.long	351
	.long	352
	.section	.rodata.CSWTCH.27,"a"
	.align 8
	.type	CSWTCH.27, @object
	.size	CSWTCH.27, 11
CSWTCH.27:
	.byte	118
	.byte	105
	.byte	108
	.byte	102
	.byte	100
	.byte	115
	.byte	114
	.byte	97
	.byte	63
	.byte	63
	.byte	42
	.globl	_ZN2v88internal4wasm9StoreType7kMemRepE
	.section	.rodata._ZN2v88internal4wasm9StoreType7kMemRepE,"a"
	.align 8
	.type	_ZN2v88internal4wasm9StoreType7kMemRepE, @object
	.size	_ZN2v88internal4wasm9StoreType7kMemRepE, 10
_ZN2v88internal4wasm9StoreType7kMemRepE:
	.byte	4
	.byte	2
	.byte	3
	.byte	5
	.byte	2
	.byte	3
	.byte	4
	.byte	12
	.byte	13
	.byte	14
	.globl	_ZN2v88internal4wasm9StoreType10kValueTypeE
	.section	.rodata._ZN2v88internal4wasm9StoreType10kValueTypeE,"a"
	.align 8
	.type	_ZN2v88internal4wasm9StoreType10kValueTypeE, @object
	.size	_ZN2v88internal4wasm9StoreType10kValueTypeE, 10
_ZN2v88internal4wasm9StoreType10kValueTypeE:
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.globl	_ZN2v88internal4wasm9StoreType14kStoreSizeLog2E
	.section	.rodata._ZN2v88internal4wasm9StoreType14kStoreSizeLog2E,"a"
	.align 8
	.type	_ZN2v88internal4wasm9StoreType14kStoreSizeLog2E, @object
	.size	_ZN2v88internal4wasm9StoreType14kStoreSizeLog2E, 10
_ZN2v88internal4wasm9StoreType14kStoreSizeLog2E:
	.string	"\002"
	.string	"\001\003"
	.ascii	"\001\002\002\003\004"
	.globl	_ZN2v88internal4wasm8LoadType8kMemTypeE
	.section	.rodata._ZN2v88internal4wasm8LoadType8kMemTypeE,"a"
	.align 16
	.type	_ZN2v88internal4wasm8LoadType8kMemTypeE, @object
	.size	_ZN2v88internal4wasm8LoadType8kMemTypeE, 30
_ZN2v88internal4wasm8LoadType8kMemTypeE:
	.byte	4
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	2
	.byte	3
	.byte	3
	.byte	5
	.byte	4
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	2
	.byte	3
	.byte	3
	.byte	4
	.byte	2
	.byte	4
	.byte	3
	.byte	12
	.byte	6
	.byte	13
	.byte	6
	.byte	14
	.byte	0
	.globl	_ZN2v88internal4wasm8LoadType10kValueTypeE
	.section	.rodata._ZN2v88internal4wasm8LoadType10kValueTypeE,"a"
	.align 8
	.type	_ZN2v88internal4wasm8LoadType10kValueTypeE, @object
	.size	_ZN2v88internal4wasm8LoadType10kValueTypeE, 15
_ZN2v88internal4wasm8LoadType10kValueTypeE:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.globl	_ZN2v88internal4wasm8LoadType13kLoadSizeLog2E
	.section	.rodata._ZN2v88internal4wasm8LoadType13kLoadSizeLog2E,"a"
	.align 8
	.type	_ZN2v88internal4wasm8LoadType13kLoadSizeLog2E, @object
	.size	_ZN2v88internal4wasm8LoadType13kLoadSizeLog2E, 15
_ZN2v88internal4wasm8LoadType13kLoadSizeLog2E:
	.string	"\002"
	.string	""
	.string	"\001\001\003"
	.string	""
	.ascii	"\001\001\002\002\002\003\004"
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L20kNumericExprSigTableE,"a"
	.align 32
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L20kNumericExprSigTableE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L20kNumericExprSigTableE, 256
_ZN2v88internal4wasm12_GLOBAL__N_1L20kNumericExprSigTableE:
	.byte	16
	.byte	16
	.byte	18
	.byte	18
	.byte	24
	.byte	24
	.byte	25
	.byte	25
	.byte	43
	.byte	11
	.byte	43
	.byte	43
	.byte	43
	.byte	11
	.byte	43
	.byte	49
	.byte	14
	.byte	43
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L19kAtomicExprSigTableE,"a"
	.align 32
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L19kAtomicExprSigTableE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L19kAtomicExprSigTableE, 256
_ZN2v88internal4wasm12_GLOBAL__N_1L19kAtomicExprSigTableE:
	.byte	12
	.byte	46
	.byte	47
	.byte	11
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	13
	.byte	23
	.byte	13
	.byte	13
	.byte	23
	.byte	23
	.byte	23
	.byte	36
	.byte	41
	.byte	36
	.byte	36
	.byte	41
	.byte	41
	.byte	41
	.byte	12
	.byte	42
	.byte	12
	.byte	12
	.byte	42
	.byte	42
	.byte	42
	.byte	12
	.byte	42
	.byte	12
	.byte	12
	.byte	42
	.byte	42
	.byte	42
	.byte	12
	.byte	42
	.byte	12
	.byte	12
	.byte	42
	.byte	42
	.byte	42
	.byte	12
	.byte	42
	.byte	12
	.byte	12
	.byte	42
	.byte	42
	.byte	42
	.byte	12
	.byte	42
	.byte	12
	.byte	12
	.byte	42
	.byte	42
	.byte	42
	.byte	12
	.byte	42
	.byte	12
	.byte	12
	.byte	42
	.byte	42
	.byte	42
	.byte	44
	.byte	45
	.byte	44
	.byte	44
	.byte	45
	.byte	45
	.byte	45
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L17kSimdExprSigTableE,"a"
	.align 32
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L17kSimdExprSigTableE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L17kSimdExprSigTableE, 256
_ZN2v88internal4wasm12_GLOBAL__N_1L17kSimdExprSigTableE:
	.byte	5
	.byte	9
	.byte	0
	.byte	0
	.byte	5
	.byte	0
	.byte	0
	.byte	0
	.byte	5
	.byte	0
	.byte	0
	.byte	0
	.byte	5
	.byte	0
	.byte	0
	.byte	6
	.byte	0
	.byte	0
	.byte	2
	.byte	0
	.byte	0
	.byte	3
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	1
	.byte	4
	.byte	4
	.byte	4
	.byte	10
	.byte	1
	.byte	8
	.byte	8
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	1
	.byte	8
	.byte	8
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	1
	.byte	8
	.byte	8
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	0
	.byte	4
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	1
	.byte	8
	.byte	8
	.byte	7
	.byte	7
	.byte	7
	.byte	4
	.byte	0
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	0
	.byte	1
	.byte	1
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	4
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	4
	.byte	4
	.byte	4
	.byte	4
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L24kSimpleAsmjsExprSigTableE,"a"
	.align 32
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L24kSimpleAsmjsExprSigTableE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L24kSimpleAsmjsExprSigTableE, 256
_ZN2v88internal4wasm12_GLOBAL__N_1L24kSimpleAsmjsExprSigTableE:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	31
	.byte	31
	.byte	31
	.byte	0
	.byte	0
	.byte	0
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	29
	.byte	34
	.byte	12
	.byte	12
	.byte	12
	.byte	40
	.byte	38
	.byte	16
	.byte	16
	.byte	18
	.byte	18
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L14kShortSigTableE,"a"
	.align 32
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L14kShortSigTableE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L14kShortSigTableE, 256
_ZN2v88internal4wasm12_GLOBAL__N_1L14kShortSigTableE:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	13
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	19
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	17
	.byte	17
	.byte	17
	.byte	17
	.byte	17
	.byte	17
	.byte	13
	.byte	13
	.byte	13
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	22
	.byte	22
	.byte	22
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	31
	.byte	31
	.byte	31
	.byte	31
	.byte	31
	.byte	31
	.byte	31
	.byte	19
	.byte	16
	.byte	16
	.byte	18
	.byte	18
	.byte	23
	.byte	23
	.byte	24
	.byte	24
	.byte	25
	.byte	25
	.byte	29
	.byte	29
	.byte	30
	.byte	30
	.byte	28
	.byte	34
	.byte	34
	.byte	35
	.byte	35
	.byte	33
	.byte	16
	.byte	25
	.byte	29
	.byte	35
	.byte	13
	.byte	13
	.byte	22
	.byte	22
	.byte	22
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	48
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L11kCachedSigsE,"aw"
	.align 32
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kCachedSigsE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kCachedSigsE, 400
_ZN2v88internal4wasm12_GLOBAL__N_1L11kCachedSigsE:
	.quad	0
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_sE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_fE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_dE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_s_ssE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_iE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_lE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_s_siE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_sE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_isE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_s_sssE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_v_vE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_iiE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_iE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_vE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_ffE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_fE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_ddE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_dE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_lE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_l_llE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_llE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_lE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_iE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_fE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_dE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_f_ffE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_fE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_dE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_iE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_lE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_d_ddE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_dE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_fE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_iE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_lE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_iiE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_idE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_d_idE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_ifE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_f_ifE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_ilE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_l_ilE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_v_iiiE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_iiiE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_l_illE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_iilE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_illE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_rE
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_aiE
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_aiE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_aiE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_aiE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_aiE:
	.quad	1
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_aiE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_aiE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_aiE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_aiE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_aiE:
	.byte	1
	.byte	7
	.byte	1
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_rE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_rE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_rE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_rE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_rE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_rE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_rE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_rE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_rE:
	.byte	1
	.byte	6
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_illE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_illE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_illE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_illE:
	.quad	1
	.quad	3
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_illE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_illE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_illE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_illE, 4
_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_illE:
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_iilE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_iilE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_iilE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_iilE:
	.quad	1
	.quad	3
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_iilE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_iilE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_iilE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_iilE, 4
_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_iilE:
	.byte	1
	.byte	1
	.byte	1
	.byte	2
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_l_illE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_l_illE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_l_illE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_l_illE:
	.quad	1
	.quad	3
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_l_illE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_l_illE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_l_illE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_l_illE, 4
_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_l_illE:
	.byte	2
	.byte	1
	.byte	2
	.byte	2
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_iiiE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_iiiE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_iiiE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_i_iiiE:
	.quad	1
	.quad	3
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_iiiE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_iiiE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_iiiE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_iiiE, 4
_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_i_iiiE:
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_v_iiiE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_v_iiiE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_v_iiiE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_v_iiiE:
	.quad	0
	.quad	3
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_v_iiiE+1
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_v_iiiE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_v_iiiE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_v_iiiE, 4
_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_v_iiiE:
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_l_ilE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_l_ilE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_l_ilE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_l_ilE:
	.quad	1
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_l_ilE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_l_ilE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_l_ilE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_l_ilE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_l_ilE:
	.byte	2
	.byte	1
	.byte	2
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_ilE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_ilE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_ilE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_ilE:
	.quad	0
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_ilE+1
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_ilE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_ilE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_ilE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_ilE:
	.byte	0
	.byte	1
	.byte	2
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_f_ifE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_f_ifE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_f_ifE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_f_ifE:
	.quad	1
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_f_ifE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_f_ifE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_f_ifE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_f_ifE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_f_ifE:
	.byte	3
	.byte	1
	.byte	3
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_ifE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_ifE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_ifE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_ifE:
	.quad	0
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_ifE+1
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_ifE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_ifE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_ifE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_ifE:
	.byte	0
	.byte	1
	.byte	3
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_d_idE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_d_idE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_d_idE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_d_idE:
	.quad	1
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_d_idE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_d_idE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_d_idE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_d_idE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_d_idE:
	.byte	4
	.byte	1
	.byte	4
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_idE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_idE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_idE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_idE:
	.quad	0
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_idE+1
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_idE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_idE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_idE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_idE:
	.byte	0
	.byte	1
	.byte	4
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_iiE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_iiE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_iiE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_iiE:
	.quad	0
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_iiE+1
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_iiE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_iiE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_iiE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_iiE:
	.byte	0
	.byte	1
	.byte	1
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_lE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_lE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_lE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_lE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_lE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_lE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_lE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_lE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_lE:
	.byte	4
	.byte	2
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_iE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_iE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_iE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_iE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_iE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_iE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_iE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_iE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_iE:
	.byte	4
	.byte	1
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_fE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_fE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_fE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_fE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_fE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_fE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_fE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_fE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_fE:
	.byte	4
	.byte	3
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_dE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_dE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_dE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_d_dE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_dE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_dE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_dE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_dE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_d_dE:
	.byte	4
	.byte	4
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_d_ddE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_d_ddE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_d_ddE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_d_ddE:
	.quad	1
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_d_ddE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_d_ddE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_d_ddE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_d_ddE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_d_ddE:
	.byte	4
	.byte	4
	.byte	4
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_lE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_lE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_lE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_lE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_lE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_lE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_lE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_lE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_lE:
	.byte	3
	.byte	2
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_iE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_iE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_iE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_iE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_iE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_iE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_iE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_iE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_iE:
	.byte	3
	.byte	1
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_dE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_dE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_dE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_dE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_dE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_dE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_dE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_dE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_dE:
	.byte	3
	.byte	4
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_fE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_fE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_fE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_f_fE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_fE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_fE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_fE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_fE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_f_fE:
	.byte	3
	.byte	3
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_f_ffE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_f_ffE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_f_ffE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_f_ffE:
	.quad	1
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_f_ffE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_f_ffE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_f_ffE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_f_ffE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_f_ffE:
	.byte	3
	.byte	3
	.byte	3
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_dE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_dE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_dE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_dE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_dE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_dE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_dE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_dE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_dE:
	.byte	2
	.byte	4
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_fE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_fE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_fE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_fE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_fE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_fE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_fE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_fE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_fE:
	.byte	2
	.byte	3
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_iE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_iE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_iE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_iE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_iE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_iE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_iE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_iE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_iE:
	.byte	2
	.byte	1
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_lE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_lE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_lE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_l_lE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_lE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_lE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_lE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_lE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_l_lE:
	.byte	2
	.byte	2
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_llE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_llE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_llE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_llE:
	.quad	1
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_llE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_llE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_llE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_llE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_llE:
	.byte	1
	.byte	2
	.byte	2
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_l_llE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_l_llE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_l_llE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_l_llE:
	.quad	1
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_l_llE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_l_llE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_l_llE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_l_llE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_l_llE:
	.byte	2
	.byte	2
	.byte	2
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_lE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_lE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_lE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_lE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_lE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_lE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_lE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_lE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_lE:
	.byte	1
	.byte	2
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_dE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_dE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_dE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_dE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_dE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_dE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_dE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_dE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_dE:
	.byte	1
	.byte	4
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_ddE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_ddE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_ddE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_ddE:
	.quad	1
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_ddE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_ddE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_ddE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_ddE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_ddE:
	.byte	1
	.byte	4
	.byte	4
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_fE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_fE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_fE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_fE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_fE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_fE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_fE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_fE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_fE:
	.byte	1
	.byte	3
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_ffE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_ffE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_ffE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_ffE:
	.quad	1
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_ffE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_ffE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_ffE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_ffE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_ffE:
	.byte	1
	.byte	3
	.byte	3
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_vE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_vE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_vE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_vE:
	.quad	1
	.quad	0
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_vE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_vE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_vE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_vE, 1
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_vE:
	.byte	1
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_iE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_iE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_iE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_iE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_iE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_iE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_iE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_iE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_iE:
	.byte	1
	.byte	1
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_iiE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_iiE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_iiE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_i_iiE:
	.quad	1
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_iiE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_iiE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_iiE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_iiE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_i_iiE:
	.byte	1
	.byte	1
	.byte	1
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_v_vE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_v_vE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_v_vE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_v_vE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_v_vE+1
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_v_vE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_v_vE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_v_vE, 1
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_v_vE:
	.zero	1
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_s_sssE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_s_sssE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_s_sssE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L10kSig_s_sssE:
	.quad	1
	.quad	3
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_s_sssE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_s_sssE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_s_sssE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_s_sssE, 4
_ZN2v88internal4wasm12_GLOBAL__N_1L12kTypes_s_sssE:
	.byte	5
	.byte	5
	.byte	5
	.byte	5
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_isE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_isE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_isE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_v_isE:
	.quad	0
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_isE+1
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_isE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_isE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_isE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_v_isE:
	.byte	0
	.byte	1
	.byte	5
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_sE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_sE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_sE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_i_sE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_sE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_sE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_sE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_sE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_i_sE:
	.byte	1
	.byte	5
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_s_siE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_s_siE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_s_siE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_s_siE:
	.quad	1
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_s_siE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_s_siE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_s_siE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_s_siE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_s_siE:
	.byte	5
	.byte	5
	.byte	1
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_lE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_lE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_lE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_lE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_lE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_lE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_lE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_lE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_lE:
	.byte	5
	.byte	2
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_iE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_iE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_iE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_iE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_iE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_iE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_iE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_iE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_iE:
	.byte	5
	.byte	1
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_s_ssE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_s_ssE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_s_ssE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L9kSig_s_ssE:
	.quad	1
	.quad	2
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_s_ssE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_s_ssE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_s_ssE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_s_ssE, 3
_ZN2v88internal4wasm12_GLOBAL__N_1L11kTypes_s_ssE:
	.byte	5
	.byte	5
	.byte	5
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_dE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_dE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_dE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_dE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_dE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_dE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_dE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_dE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_dE:
	.byte	5
	.byte	4
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_fE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_fE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_fE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_fE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_fE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_fE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_fE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_fE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_fE:
	.byte	5
	.byte	3
	.section	.data.rel.ro.local._ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_sE,"aw"
	.align 16
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_sE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_sE, 24
_ZN2v88internal4wasm12_GLOBAL__N_1L8kSig_s_sE:
	.quad	1
	.quad	1
	.quad	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_sE
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_sE,"a"
	.type	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_sE, @object
	.size	_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_sE, 2
_ZN2v88internal4wasm12_GLOBAL__N_1L10kTypes_s_sE:
	.byte	5
	.byte	5
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
