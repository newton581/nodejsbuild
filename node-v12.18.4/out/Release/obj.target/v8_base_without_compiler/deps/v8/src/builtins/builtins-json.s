	.file	"builtins-json.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internalL22Builtin_Impl_JsonParseENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22Builtin_Impl_JsonParseENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL22Builtin_Impl_JsonParseENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18397:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	cmpl	$5, %edi
	jg	.L6
	leaq	88(%rdx), %rsi
	movq	%rsi, %r13
.L9:
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L10
.L13:
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertToStringEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L37
.L12:
	movq	(%rsi), %rax
	movq	-1(%rax), %rdx
	movq	%rax, %r15
	cmpw	$63, 11(%rdx)
	jbe	.L62
.L16:
	movq	-1(%r15), %rax
	cmpw	$63, 11(%rax)
	jbe	.L25
.L58:
	movq	(%rsi), %r15
.L33:
	movq	-1(%r15), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	je	.L31
.L63:
	cmpw	$8, %ax
	je	.L32
	movq	15(%r15), %r15
	movq	-1(%r15), %rax
	movzwl	11(%rax), %eax
	andw	$9, %ax
	jne	.L63
.L31:
	leaq	-128(%rbp), %rdi
	movq	%rsi, %rdx
	movq	%r12, %rsi
	movq	%rdi, -136(%rbp)
	call	_ZN2v88internal10JsonParserItEC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal10JsonParserItE9ParseJsonEv@PLT
	movq	-136(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L64
	call	_ZN2v88internal10JsonParserItED1Ev@PLT
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L61
.L41:
	testq	%r15, %r15
	je	.L37
	movq	(%r15), %r13
.L14:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L47
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L47:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	%rsi, %r13
	leaq	-8(%rsi), %rsi
	cmpl	$6, %edi
	je	.L66
	movq	(%rsi), %rax
	subq	$16, %r13
	testb	$1, %al
	je	.L13
.L10:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L62:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L16
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L19
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L19
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %r15
	movq	%rax, %rsi
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L25:
	movq	-1(%r15), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L58
	movq	(%rsi), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %r15
	testq	%rdi, %rdi
	je	.L28
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	-128(%rbp), %rdi
	movq	%rsi, %rdx
	movq	%r12, %rsi
	movq	%rdi, -136(%rbp)
	call	_ZN2v88internal10JsonParserIhEC1EPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal10JsonParserIhE9ParseJsonEv@PLT
	movq	-136(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L67
	call	_ZN2v88internal10JsonParserIhED1Ev@PLT
	movq	0(%r13), %rax
	testb	$1, %al
	je	.L41
.L61:
	movq	-1(%rax), %rax
	testb	$2, 13(%rax)
	je	.L41
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal21JsonParseInternalizer11InternalizeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_@PLT
	movq	%rax, %r15
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L67:
	call	_ZN2v88internal10JsonParserIhED1Ev@PLT
.L37:
	movq	312(%r12), %r13
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L64:
	call	_ZN2v88internal10JsonParserItED1Ev@PLT
	movq	312(%r12), %r13
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L19:
	movq	41112(%r12), %rdi
	movq	15(%rax), %r15
	testq	%rdi, %rdi
	je	.L21
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r15
	movq	%rax, %rsi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L28:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L68
.L30:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L21:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L69
.L23:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L30
.L69:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L23
.L65:
	call	__stack_chk_fail@PLT
.L66:
	leaq	88(%rdx), %r13
	jmp	.L9
	.cfi_endproc
.LFE18397:
	.size	_ZN2v88internalL22Builtin_Impl_JsonParseENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL22Builtin_Impl_JsonParseENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L76
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L79
.L70:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L70
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"V8.Builtin_JsonStringify"
	.section	.text._ZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateE:
.LFB18398:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L119
.L81:
	movq	_ZZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateEE27trace_event_unique_atomic31(%rip), %rbx
	testq	%rbx, %rbx
	je	.L120
.L83:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L121
.L85:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r15
	addl	$1, 41104(%r12)
	cmpl	$5, %r14d
	jg	.L89
	leaq	88(%r12), %rcx
	movq	%rcx, %rsi
.L90:
	movq	%rcx, %rdx
.L94:
	movq	%r12, %rdi
	call	_ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	testq	%rax, %rax
	je	.L122
.L95:
	movq	(%rax), %r13
.L96:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L99
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L99:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L123
.L80:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	leaq	-8(%r13), %rsi
	cmpl	$6, %r14d
	je	.L125
	leaq	-16(%r13), %rdx
	cmpl	$7, %r14d
	je	.L126
	leaq	-24(%r13), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	testq	%rax, %rax
	jne	.L95
.L122:
	movq	312(%r12), %r13
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L121:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L127
.L86:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L87
	movq	(%rdi), %rax
	call	*8(%rax)
.L87:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L88
	movq	(%rdi), %rax
	call	*8(%rax)
.L88:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L120:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L128
.L84:
	movq	%rbx, _ZZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateEE27trace_event_unique_atomic31(%rip)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L119:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$786, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L128:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L127:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L86
.L124:
	call	__stack_chk_fail@PLT
.L126:
	leaq	88(%r12), %rcx
	jmp	.L94
.L125:
	leaq	88(%r12), %rcx
	jmp	.L90
	.cfi_endproc
.LFE18398:
	.size	_ZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL28Builtin_Impl_Stats_JsonParseEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"V8.Builtin_JsonParse"
	.section	.text._ZN2v88internalL28Builtin_Impl_Stats_JsonParseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Builtin_Impl_Stats_JsonParseEiPmPNS0_7IsolateE, @function
_ZN2v88internalL28Builtin_Impl_Stats_JsonParseEiPmPNS0_7IsolateE:
.LFB18395:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L158
.L130:
	movq	_ZZN2v88internalL28Builtin_Impl_Stats_JsonParseEiPmPNS0_7IsolateEE27trace_event_unique_atomic16(%rip), %rbx
	testq	%rbx, %rbx
	je	.L159
.L132:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L160
.L134:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL22Builtin_Impl_JsonParseENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L161
.L138:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L162
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L163
.L133:
	movq	%rbx, _ZZN2v88internalL28Builtin_Impl_Stats_JsonParseEiPmPNS0_7IsolateEE27trace_event_unique_atomic16(%rip)
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L160:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L164
.L135:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L136
	movq	(%rdi), %rax
	call	*8(%rax)
.L136:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L137
	movq	(%rdi), %rax
	call	*8(%rax)
.L137:
	leaq	.LC2(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L158:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$785, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L164:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L133
.L162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18395:
	.size	_ZN2v88internalL28Builtin_Impl_Stats_JsonParseEiPmPNS0_7IsolateE, .-_ZN2v88internalL28Builtin_Impl_Stats_JsonParseEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal17Builtin_JsonParseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Builtin_JsonParseEiPmPNS0_7IsolateE
	.type	_ZN2v88internal17Builtin_JsonParseEiPmPNS0_7IsolateE, @function
_ZN2v88internal17Builtin_JsonParseEiPmPNS0_7IsolateE:
.LFB18396:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L169
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL22Builtin_Impl_JsonParseENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore 6
	jmp	_ZN2v88internalL28Builtin_Impl_Stats_JsonParseEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18396:
	.size	_ZN2v88internal17Builtin_JsonParseEiPmPNS0_7IsolateE, .-_ZN2v88internal17Builtin_JsonParseEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Builtin_JsonStringifyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Builtin_JsonStringifyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Builtin_JsonStringifyEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Builtin_JsonStringifyEiPmPNS0_7IsolateE:
.LFB18399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L183
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	cmpl	$5, %edi
	jle	.L184
	leaq	-8(%rsi), %r8
	cmpl	$6, %edi
	je	.L185
	leaq	-16(%rsi), %rdx
	cmpl	$7, %edi
	je	.L186
	leaq	-24(%rsi), %rcx
.L177:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	testq	%rax, %rax
	je	.L187
.L178:
	movq	(%rax), %r14
.L179:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L170
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L170:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	leaq	88(%rdx), %rcx
	movq	%rcx, %r8
.L173:
	movq	%rcx, %rdx
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13JsonStringifyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	testq	%rax, %rax
	jne	.L178
.L187:
	movq	312(%r12), %r14
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L183:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateE
.L186:
	.cfi_restore_state
	leaq	88(%r12), %rcx
	jmp	.L177
.L185:
	leaq	88(%rdx), %rcx
	jmp	.L173
	.cfi_endproc
.LFE18399:
	.size	_ZN2v88internal21Builtin_JsonStringifyEiPmPNS0_7IsolateE, .-_ZN2v88internal21Builtin_JsonStringifyEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal17Builtin_JsonParseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal17Builtin_JsonParseEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal17Builtin_JsonParseEiPmPNS0_7IsolateE:
.LFB22119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22119:
	.size	_GLOBAL__sub_I__ZN2v88internal17Builtin_JsonParseEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal17Builtin_JsonParseEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal17Builtin_JsonParseEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateEE27trace_event_unique_atomic31,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateEE27trace_event_unique_atomic31, @object
	.size	_ZZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateEE27trace_event_unique_atomic31, 8
_ZZN2v88internalL32Builtin_Impl_Stats_JsonStringifyEiPmPNS0_7IsolateEE27trace_event_unique_atomic31:
	.zero	8
	.section	.bss._ZZN2v88internalL28Builtin_Impl_Stats_JsonParseEiPmPNS0_7IsolateEE27trace_event_unique_atomic16,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Builtin_Impl_Stats_JsonParseEiPmPNS0_7IsolateEE27trace_event_unique_atomic16, @object
	.size	_ZZN2v88internalL28Builtin_Impl_Stats_JsonParseEiPmPNS0_7IsolateEE27trace_event_unique_atomic16, 8
_ZZN2v88internalL28Builtin_Impl_Stats_JsonParseEiPmPNS0_7IsolateEE27trace_event_unique_atomic16:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
