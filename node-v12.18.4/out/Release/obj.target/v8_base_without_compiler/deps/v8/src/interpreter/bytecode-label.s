	.file	"bytecode-label.cc"
	.text
	.section	.text._ZN2v88internal11interpreter14BytecodeLabels3NewEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter14BytecodeLabels3NewEv
	.type	_ZN2v88internal11interpreter14BytecodeLabels3NewEv, @function
_ZN2v88internal11interpreter14BytecodeLabels3NewEv:
.LFB19211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %r8
	movq	%rdi, %rbx
	movq	24(%r8), %rax
	movq	16(%r8), %rdi
	subq	%rdi, %rax
	cmpq	$31, %rax
	jbe	.L6
	leaq	32(%rdi), %rax
	movq	%rax, 16(%r8)
.L3:
	movb	$0, 16(%rdi)
	movq	%r12, %rsi
	movq	$-1, 24(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	movq	16(%rbx), %rax
	addq	$1, 24(%rbx)
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	%r8, %rdi
	movl	$32, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L3
	.cfi_endproc
.LFE19211:
	.size	_ZN2v88internal11interpreter14BytecodeLabels3NewEv, .-_ZN2v88internal11interpreter14BytecodeLabels3NewEv
	.section	.text._ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE
	.type	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE, @function
_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE:
.LFB19212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	8(%rdi), %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movb	$1, 32(%rdi)
	cmpq	%rbx, %r12
	je	.L7
	movq	%rsi, %r13
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	16(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	(%rbx), %rbx
	cmpq	%rbx, %r12
	jne	.L9
.L7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19212:
	.size	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE, .-_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter14BytecodeLabels3NewEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter14BytecodeLabels3NewEv, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter14BytecodeLabels3NewEv:
.LFB23471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23471:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter14BytecodeLabels3NewEv, .-_GLOBAL__sub_I__ZN2v88internal11interpreter14BytecodeLabels3NewEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter14BytecodeLabels3NewEv
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
