	.file	"runtime-test.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1365:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1365:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB2153:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2153:
	.size	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4479:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4479:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4480:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4480:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4482:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4482:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZNK2v88internal29NativesExternalStringResource4dataEv,"axG",@progbits,_ZNK2v88internal29NativesExternalStringResource4dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal29NativesExternalStringResource4dataEv
	.type	_ZNK2v88internal29NativesExternalStringResource4dataEv, @function
_ZNK2v88internal29NativesExternalStringResource4dataEv:
.LFB22194:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	ret
	.cfi_endproc
.LFE22194:
	.size	_ZNK2v88internal29NativesExternalStringResource4dataEv, .-_ZNK2v88internal29NativesExternalStringResource4dataEv
	.section	.text._ZN2v88internalL10ReturnThisERKNS_20FunctionCallbackInfoINS_5ValueEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL10ReturnThisERKNS_20FunctionCallbackInfoINS_5ValueEEE, @function
_ZN2v88internalL10ReturnThisERKNS_20FunctionCallbackInfoINS_5ValueEEE:
.LFB22475:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	movq	(%rdi), %rax
	movq	8(%rdx), %rdx
	movq	%rdx, 24(%rax)
	ret
	.cfi_endproc
.LFE22475:
	.size	_ZN2v88internalL10ReturnThisERKNS_20FunctionCallbackInfoINS_5ValueEEE, .-_ZN2v88internalL10ReturnThisERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_134DisallowCodegenFromStringsCallbackENS_5LocalINS_7ContextEEENS2_INS_6StringEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_134DisallowCodegenFromStringsCallbackENS_5LocalINS_7ContextEEENS2_INS_6StringEEE, @function
_ZN2v88internal12_GLOBAL__N_134DisallowCodegenFromStringsCallbackENS_5LocalINS_7ContextEEENS2_INS_6StringEEE:
.LFB22552:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22552:
	.size	_ZN2v88internal12_GLOBAL__N_134DisallowCodegenFromStringsCallbackENS_5LocalINS_7ContextEEENS2_INS_6StringEEE, .-_ZN2v88internal12_GLOBAL__N_134DisallowCodegenFromStringsCallbackENS_5LocalINS_7ContextEEENS2_INS_6StringEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_117EnableWasmThreadsENS_5LocalINS_7ContextEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_117EnableWasmThreadsENS_5LocalINS_7ContextEEE, @function
_ZN2v88internal12_GLOBAL__N_117EnableWasmThreadsENS_5LocalINS_7ContextEEE:
.LFB22577:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE22577:
	.size	_ZN2v88internal12_GLOBAL__N_117EnableWasmThreadsENS_5LocalINS_7ContextEEE, .-_ZN2v88internal12_GLOBAL__N_117EnableWasmThreadsENS_5LocalINS_7ContextEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_118DisableWasmThreadsENS_5LocalINS_7ContextEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118DisableWasmThreadsENS_5LocalINS_7ContextEEE, @function
_ZN2v88internal12_GLOBAL__N_118DisableWasmThreadsENS_5LocalINS_7ContextEEE:
.LFB22578:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22578:
	.size	_ZN2v88internal12_GLOBAL__N_118DisableWasmThreadsENS_5LocalINS_7ContextEEE, .-_ZN2v88internal12_GLOBAL__N_118DisableWasmThreadsENS_5LocalINS_7ContextEEE
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc:
.LFB22717:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22717:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE:
.LFB22718:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22718:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE:
.LFB22719:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22719:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii:
.LFB22720:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22720:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE:
.LFB22721:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22721:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CallbackEventENS0_4NameEm,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CallbackEventENS0_4NameEm, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CallbackEventENS0_4NameEm:
.LFB22722:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22722:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CallbackEventENS0_4NameEm, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CallbackEventENS0_4NameEm
	.set	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener19SetterCallbackEventENS0_4NameEm,_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CallbackEventENS0_4NameEm
	.set	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener19GetterCallbackEventENS0_4NameEm,_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CallbackEventENS0_4NameEm
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE:
.LFB22725:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22725:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CodeMoveEventENS0_12AbstractCodeES5_,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CodeMoveEventENS0_12AbstractCodeES5_, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CodeMoveEventENS0_12AbstractCodeES5_:
.LFB22726:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22726:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CodeMoveEventENS0_12AbstractCodeES5_, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CodeMoveEventENS0_12AbstractCodeES5_
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener27SharedFunctionInfoMoveEventEmm,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener27SharedFunctionInfoMoveEventEmm, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener27SharedFunctionInfoMoveEventEmm:
.LFB22727:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22727:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener27SharedFunctionInfoMoveEventEmm, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener27SharedFunctionInfoMoveEventEmm
	.set	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener22NativeContextMoveEventEmm,_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener27SharedFunctionInfoMoveEventEmm
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener17CodeMovingGCEventEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener17CodeMovingGCEventEv, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener17CodeMovingGCEventEv:
.LFB22729:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22729:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener17CodeMovingGCEventEv, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener17CodeMovingGCEventEv
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE:
.LFB22730:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22730:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi:
.LFB22731:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22731:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener27is_listening_to_code_eventsEv,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener27is_listening_to_code_eventsEv, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener27is_listening_to_code_eventsEv:
.LFB22732:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE22732:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener27is_listening_to_code_eventsEv, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener27is_listening_to_code_eventsEv
	.section	.text._ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB26884:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE26884:
	.size	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB28728:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE28728:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB28729:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L28
	cmpl	$3, %edx
	je	.L29
	cmpl	$1, %edx
	je	.L33
.L29:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE28729:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD2Ev, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD2Ev:
.LFB28945:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28945:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD2Ev, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD2Ev
	.set	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD1Ev,_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD2Ev
	.section	.text._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD0Ev, @function
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD0Ev:
.LFB28947:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28947:
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD0Ev, .-_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD0Ev
	.section	.text._ZN2v88internalL16call_as_functionERKNS_20FunctionCallbackInfoINS_5ValueEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL16call_as_functionERKNS_20FunctionCallbackInfoINS_5ValueEEE, @function
_ZN2v88internalL16call_as_functionERKNS_20FunctionCallbackInfoINS_5ValueEEE:
.LFB22479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	movl	16(%rbx), %eax
	leaq	88(%rdi), %r12
	testl	%eax, %eax
	jle	.L38
	movq	8(%rbx), %r12
.L38:
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value11NumberValueENS_5LocalINS_7ContextEEE@PLT
	movsd	%xmm0, -24(%rbp)
	testb	%al, %al
	je	.L46
.L39:
	movq	(%rbx), %rax
	cmpl	$1, 16(%rbx)
	movq	8(%rax), %rdi
	leaq	88(%rdi), %r12
	jle	.L41
	movq	8(%rbx), %rax
	leaq	-8(%rax), %r12
.L41:
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value11NumberValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L47
.L42:
	movsd	-24(%rbp), %xmm1
	movq	(%rbx), %rbx
	subsd	%xmm0, %xmm1
	movq	8(%rbx), %rdi
	movapd	%xmm1, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L48
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L47:
	movsd	%xmm0, -32(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movsd	-32(%rbp), %xmm0
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L48:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22479:
	.size	_ZN2v88internalL16call_as_functionERKNS_20FunctionCallbackInfoINS_5ValueEEE, .-_ZN2v88internalL16call_as_functionERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv:
.LFB27729:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base5MutexC1Ev@PLT
	.cfi_endproc
.LFE27729:
	.size	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB28954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE28954:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZNK2v88internal10HeapObject8map_wordEPNS0_7IsolateE.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal10HeapObject8map_wordEPNS0_7IsolateE.isra.0, @function
_ZNK2v88internal10HeapObject8map_wordEPNS0_7IsolateE.isra.0:
.LFB29257:
	.cfi_startproc
	movq	(%rdi), %rax
	movq	-1(%rax), %rax
	ret
	.cfi_endproc
.LFE29257:
	.size	_ZNK2v88internal10HeapObject8map_wordEPNS0_7IsolateE.isra.0, .-_ZNK2v88internal10HeapObject8map_wordEPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal12_GLOBAL__N_19StackSizeEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_19StackSizeEPNS0_7IsolateE, @function
_ZN2v88internal12_GLOBAL__N_19StackSizeEPNS0_7IsolateE:
.LFB22532:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	-1472(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$1456, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -56(%rbp)
	jne	.L60
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L55:
	addl	$1, %r12d
.L60:
	movq	%rbx, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	cmpq	$0, -56(%rbp)
	jne	.L55
.L53:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$1456, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L61:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22532:
	.size	_ZN2v88internal12_GLOBAL__N_19StackSizeEPNS0_7IsolateE, .-_ZN2v88internal12_GLOBAL__N_19StackSizeEPNS0_7IsolateE
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB29405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29405:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB28955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28955:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB29406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE29406:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.rodata._ZN2v88internal12_GLOBAL__N_120IsWasmCompileAllowedEPNS_7IsolateENS_5LocalINS_5ValueEEEb.str1.1,"aMS",@progbits,1
.LC0:
	.string	"map::at"
	.section	.text._ZN2v88internal12_GLOBAL__N_120IsWasmCompileAllowedEPNS_7IsolateENS_5LocalINS_5ValueEEEb,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120IsWasmCompileAllowedEPNS_7IsolateENS_5LocalINS_5ValueEEEb, @function
_ZN2v88internal12_GLOBAL__N_120IsWasmCompileAllowedEPNS_7IsolateENS_5LocalINS_5ValueEEEb:
.LFB22411:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %eax
	cmpb	$2, %al
	jne	.L100
.L69:
	leaq	8+_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %eax
	testb	%al, %al
	je	.L101
.L72:
	movq	16+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rax
	testq	%rax, %rax
	je	.L74
.L104:
	leaq	8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdx
	movq	%rdx, %rbx
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%rax, %rbx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L76
.L75:
	cmpq	%r12, 32(%rax)
	jnb	.L102
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L75
.L76:
	cmpq	%rdx, %rbx
	je	.L74
	cmpq	%r12, 32(%rbx)
	ja	.L74
	testb	%r14b, %r14b
	je	.L80
	cmpb	$0, 44(%rbx)
	je	.L80
.L84:
	movl	$1, %r12d
.L81:
	leaq	8+_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	je	.L85
	movq	%r13, %rdi
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	movq	%rax, %r8
	movl	40(%rbx), %eax
	cmpq	%rax, %r8
	jbe	.L84
.L85:
	movq	%r13, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L81
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, %r8
	movl	40(%rbx), %eax
	cmpq	%rax, %r8
	setbe	%r12b
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L100:
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rax
	leaq	-96(%rbp), %r15
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r15, %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L69
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L72
	leaq	8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rax
	leaq	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdi
	movl	$0, 8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	%rax, 24+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	%rax, 32+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	$0, 16+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	$0, 40+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	call	__cxa_guard_release@PLT
	movq	16+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rax
	testq	%rax, %rax
	jne	.L104
.L74:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_out_of_rangePKc@PLT
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22411:
	.size	_ZN2v88internal12_GLOBAL__N_120IsWasmCompileAllowedEPNS_7IsolateENS_5LocalINS_5ValueEEEb, .-_ZN2v88internal12_GLOBAL__N_120IsWasmCompileAllowedEPNS_7IsolateENS_5LocalINS_5ValueEEEb
	.section	.rodata._ZN2v88internal12_GLOBAL__N_118WasmModuleOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Sync compile not allowed"
	.section	.text._ZN2v88internal12_GLOBAL__N_118WasmModuleOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_118WasmModuleOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal12_GLOBAL__N_118WasmModuleOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE:
.LFB22415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	movl	16(%rbx), %eax
	leaq	88(%rdi), %rsi
	testl	%eax, %eax
	jle	.L107
	movq	8(%rbx), %rsi
.L107:
	xorl	%edx, %edx
	call	_ZN2v88internal12_GLOBAL__N_120IsWasmCompileAllowedEPNS_7IsolateENS_5LocalINS_5ValueEEEb
	movl	%eax, %r8d
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L112
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	(%rbx), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L113
.L109:
	call	_ZN2v89Exception10RangeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rdi
	jmp	.L109
	.cfi_endproc
.LFE22415:
	.size	_ZN2v88internal12_GLOBAL__N_118WasmModuleOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE, .-_ZN2v88internal12_GLOBAL__N_118WasmModuleOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_120WasmInstanceOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Sync instantiate not allowed"
	.section	.text._ZN2v88internal12_GLOBAL__N_120WasmInstanceOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120WasmInstanceOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal12_GLOBAL__N_120WasmInstanceOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE:
.LFB22416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	8(%rax), %r13
	movl	16(%rdi), %eax
	leaq	88(%r13), %r14
	testl	%eax, %eax
	jle	.L116
	movq	8(%rdi), %r14
.L116:
	movzbl	_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %eax
	cmpb	$2, %al
	jne	.L156
.L117:
	leaq	8+_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %eax
	testb	%al, %al
	je	.L157
.L120:
	movq	16+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rax
	testq	%rax, %rax
	je	.L122
.L162:
	leaq	8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdx
	movq	%rdx, %r12
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%rax, %r12
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L124
.L123:
	cmpq	%r13, 32(%rax)
	jnb	.L158
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L123
.L124:
	cmpq	%rdx, %r12
	je	.L122
	cmpq	%r13, 32(%r12)
	ja	.L122
	movq	%r14, %rdi
	call	_ZNK2v85Value27IsWebAssemblyCompiledModuleEv@PLT
	testb	%al, %al
	jne	.L128
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120IsWasmCompileAllowedEPNS_7IsolateENS_5LocalINS_5ValueEEEb
	movl	%eax, %r12d
.L129:
	leaq	8+_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	xorl	%eax, %eax
	testb	%r12b, %r12b
	jne	.L114
	movq	(%rbx), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L159
.L140:
	call	_ZN2v89Exception10RangeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movl	$1, %eax
.L114:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L160
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	leaq	-96(%rbp), %r13
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v816WasmModuleObject17GetCompiledModuleEv@PLT
	movq	%r13, %rdi
	call	_ZN2v818CompiledWasmModule15GetWireBytesRefEv@PLT
	movq	-88(%rbp), %r13
	cmpl	%edx, 40(%r12)
	setnb	%r12b
	testq	%r13, %r13
	je	.L129
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L132
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L133:
	cmpl	$1, %eax
	jne	.L129
	movq	0(%r13), %rax
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L161
.L135:
	testq	%r14, %r14
	je	.L136
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L137:
	cmpl	$1, %eax
	jne	.L129
	movq	0(%r13), %rax
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L138
	call	*8(%rax)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L156:
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rax
	leaq	-80(%rbp), %r12
	movq	%rax, -72(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L117
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L157:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L120
	leaq	8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rax
	leaq	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdi
	movl	$0, 8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	%rax, 24+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	%rax, 32+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	$0, 16+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	$0, 40+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	call	__cxa_guard_release@PLT
	movq	16+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rax
	testq	%rax, %rax
	jne	.L162
.L122:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_out_of_rangePKc@PLT
	.p2align 4,,10
	.p2align 3
.L132:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L136:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rdi
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L135
.L138:
	call	*%rdx
	jmp	.L129
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22416:
	.size	_ZN2v88internal12_GLOBAL__N_120WasmInstanceOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE, .-_ZN2v88internal12_GLOBAL__N_120WasmInstanceOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_120EnsureFeedbackVectorENS0_6HandleINS0_10JSFunctionEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120EnsureFeedbackVectorENS0_6HandleINS0_10JSFunctionEEE, @function
_ZN2v88internal12_GLOBAL__N_120EnsureFeedbackVectorENS0_6HandleINS0_10JSFunctionEEE:
.LFB22454:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andb	$16, %dh
	jne	.L164
.L199:
	xorl	%eax, %eax
.L163:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L210
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	movq	23(%rax), %rdx
	movq	%rdi, %r12
	movabsq	$287762808832, %rcx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L171
	testb	$1, %dl
	jne	.L169
.L173:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	movl	$1, %eax
	cmpw	$155, 11(%rdx)
	je	.L163
.L171:
	movq	(%r12), %rax
	movq	23(%rax), %rbx
	movq	%rbx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdx
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L211
.L167:
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L212
.L174:
	xorl	%eax, %eax
.L184:
	movabsq	$287762808832, %rcx
	movq	7(%rbx), %rdx
	cmpq	%rcx, %rdx
	je	.L187
	movl	$1, %ecx
	testb	$1, %dl
	jne	.L213
.L186:
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movb	%cl, -56(%rbp)
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	jne	.L214
.L188:
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L196
	testb	$1, %dl
	jne	.L194
.L197:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$125, 11(%rax)
	je	.L198
.L196:
	leaq	-64(%rbp), %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE@PLT
	testb	%al, %al
	jne	.L198
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L212:
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L174
.L178:
	movq	31(%rbx), %rax
	testb	$1, %al
	jne	.L215
.L176:
	movq	7(%rbx), %rax
	testb	$1, %al
	jne	.L216
.L180:
	movq	7(%rbx), %rax
	movq	7(%rax), %rsi
.L179:
	movq	3520(%rdx), %rdi
	leaq	-37592(%rdx), %r13
	testq	%rdi, %rdi
	je	.L181
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L214:
	movabsq	$287762808832, %rdx
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L209
	testb	$1, %al
	jne	.L217
.L198:
	movq	%r12, %rdi
	call	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE@PLT
	movl	$1, %eax
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L213:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	jne	.L218
.L187:
	xorl	%ecx, %ecx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L169:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L171
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L173
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L211:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L167
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L217:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L209
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L198
	.p2align 4,,10
	.p2align 3
.L209:
	movq	(%r12), %rax
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L181:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L219
.L183:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L218:
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	setne	%cl
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L194:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L196
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L197
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L215:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L176
	movq	39(%rax), %rcx
	testb	$1, %cl
	je	.L176
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L176
	movq	31(%rax), %rsi
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L216:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L180
	movq	7(%rbx), %rsi
	jmp	.L179
.L219:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L183
.L210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22454:
	.size	_ZN2v88internal12_GLOBAL__N_120EnsureFeedbackVectorENS0_6HandleINS0_10JSFunctionEEE, .-_ZN2v88internal12_GLOBAL__N_120EnsureFeedbackVectorENS0_6HandleINS0_10JSFunctionEEE
	.section	.text._ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_NS0_8internal12_GLOBAL__N_119WasmCompileControlsEESt10_Select1stIS8_ESt4lessIS2_ESaIS8_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESJ_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_NS0_8internal12_GLOBAL__N_119WasmCompileControlsEESt10_Select1stIS8_ESt4lessIS2_ESaIS8_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESJ_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0, @function
_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_NS0_8internal12_GLOBAL__N_119WasmCompileControlsEESt10_Select1stIS8_ESt4lessIS2_ESaIS8_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESJ_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0:
.LFB29393:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$48, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	leaq	8(%rbx), %rcx
	movq	%rax, %r13
	movq	(%r14), %rax
	movl	$-1, 40(%r13)
	movq	(%rax), %r14
	movb	$1, 44(%r13)
	movq	%r14, 32(%r13)
	cmpq	%rcx, %r12
	je	.L280
	movq	32(%r12), %r15
	cmpq	%r15, %r14
	jnb	.L233
	movq	24(%rbx), %r15
	cmpq	%r15, %r12
	je	.L236
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	cmpq	%r14, 32(%rax)
	jnb	.L235
	cmpq	$0, 24(%rax)
	movq	%r12, %r15
	je	.L281
.L236:
	testq	%r15, %r15
	setne	%al
.L255:
	cmpq	%r12, %rcx
	je	.L268
	testb	%al, %al
	je	.L282
.L268:
	movl	$1, %edi
.L246:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	jbe	.L232
	cmpq	32(%rbx), %r12
	je	.L277
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	cmpq	%r14, 32(%rax)
	jbe	.L244
	cmpq	$0, 24(%r12)
	je	.L245
	movq	%rax, %r12
	movl	$1, %edi
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L226:
	movq	%r15, %r12
	testb	%sil, %sil
	jne	.L224
.L229:
	cmpq	%r14, %rdx
	jb	.L231
	movq	%r15, %r12
.L232:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	cmpq	$0, 40(%rbx)
	je	.L222
	movq	32(%rbx), %r12
	cmpq	%r14, 32(%r12)
	jb	.L223
.L222:
	movq	16(%rbx), %r15
	testq	%r15, %r15
	jne	.L225
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L284:
	movq	16(%r15), %rax
	movl	$1, %esi
.L228:
	testq	%rax, %rax
	je	.L226
	movq	%rax, %r15
.L225:
	movq	32(%r15), %rdx
	cmpq	%r14, %rdx
	ja	.L284
	movq	24(%r15), %rax
	xorl	%esi, %esi
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%rax, %r12
.L223:
	xorl	%eax, %eax
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L235:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	jne	.L238
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L286:
	movq	16(%r12), %rax
	movl	$1, %esi
.L241:
	testq	%rax, %rax
	je	.L239
	movq	%rax, %r12
.L238:
	movq	32(%r12), %rdx
	cmpq	%rdx, %r14
	jb	.L286
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L258:
	movq	%r15, %r12
.L277:
	xorl	%r15d, %r15d
	jmp	.L236
.L285:
	movq	%rcx, %r12
.L237:
	cmpq	%r12, %r15
	je	.L263
.L279:
	movq	%r12, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	movq	%r12, %rdi
	movq	32(%rax), %rdx
	movq	%rax, %r12
.L252:
	cmpq	%rdx, %r14
	jbe	.L232
.L253:
	movq	%rdi, %r12
.L231:
	testq	%r12, %r12
	je	.L232
	xorl	%r15d, %r15d
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L282:
	movq	32(%r12), %r15
.L245:
	xorl	%edi, %edi
	cmpq	%r14, %r15
	seta	%dil
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%rcx, %r15
.L224:
	cmpq	%r15, 24(%rbx)
	je	.L258
	movq	%r15, %rdi
	movq	%rcx, -56(%rbp)
	movq	%r15, %r12
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rcx
	movq	32(%rax), %rdx
	movq	%rax, %r15
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L239:
	movq	%r12, %rdi
	testb	%sil, %sil
	je	.L252
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L244:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	jne	.L248
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L288:
	movq	16(%r12), %rax
	movl	$1, %esi
.L251:
	testq	%rax, %rax
	je	.L249
	movq	%rax, %r12
.L248:
	movq	32(%r12), %rdx
	cmpq	%rdx, %r14
	jb	.L288
	movq	24(%r12), %rax
	xorl	%esi, %esi
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L249:
	movq	%r12, %rdi
	testb	%sil, %sil
	je	.L252
.L247:
	cmpq	%r12, 24(%rbx)
	jne	.L279
	movq	%r12, %rdi
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L263:
	movq	%r15, %rdi
	jmp	.L253
.L287:
	movq	%rcx, %r12
	jmp	.L247
	.cfi_endproc
.LFE29393:
	.size	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_NS0_8internal12_GLOBAL__N_119WasmCompileControlsEESt10_Select1stIS8_ESt4lessIS2_ESaIS8_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESJ_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0, .-_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_NS0_8internal12_GLOBAL__N_119WasmCompileControlsEESt10_Select1stIS8_ESt4lessIS2_ESaIS8_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESJ_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB19732:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L295
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L298
.L289:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L289
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE19732:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC4:
	.string	"V8.Runtime_Runtime_ConstructDouble"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC5:
	.string	"args[0].IsNumber()"
.LC6:
	.string	"Check failed: %s."
.LC11:
	.string	"args[1].IsNumber()"
	.section	.text._ZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateE.isra.0:
.LFB29152:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L376
.L300:
	movq	_ZZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateEE28trace_event_unique_atomic124(%rip), %rbx
	testq	%rbx, %rbx
	je	.L377
.L302:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L378
.L304:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L379
	shrq	$32, %rax
.L312:
	movq	-8(%r14), %rdx
	testb	$1, %dl
	jne	.L380
.L373:
	shrq	$32, %rdx
.L325:
	salq	$32, %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	orq	%rdx, %rax
	movq	%rax, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L336
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L336:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L381
.L299:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L382
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L380:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L383
	movq	7(%rdx), %rdi
	movsd	.LC8(%rip), %xmm2
	movq	%rdi, %xmm1
	andpd	.LC7(%rip), %xmm1
	movq	%rdi, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L326
	movsd	.LC9(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L384
.L326:
	movabsq	$9218868437227405312, %rdx
	andq	%rdi, %rdx
	je	.L325
	movq	%rdi, %rsi
	xorl	%edx, %edx
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L385
	cmpl	$31, %ecx
	jg	.L325
	movabsq	$4503599627370495, %rdx
	movq	%rdx, %rsi
	addq	$1, %rdx
	andq	%rdi, %rsi
	addq	%rsi, %rdx
	salq	%cl, %rdx
	movl	%edx, %edx
.L332:
	sarq	$63, %rdi
	orl	$1, %edi
	imull	%edi, %edx
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L379:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L386
	movq	7(%rax), %rdx
	movsd	.LC8(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC7(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L313
	movsd	.LC9(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L387
.L313:
	movabsq	$9218868437227405312, %rax
	testq	%rax, %rdx
	je	.L340
	movq	%rdx, %rsi
	xorl	%eax, %eax
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L388
	cmpl	$31, %ecx
	jg	.L312
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %rax
	addq	%rsi, %rax
	salq	%cl, %rax
	movl	%eax, %ecx
.L319:
	movq	%rdx, %rax
	movq	-8(%r14), %rdx
	sarq	$63, %rax
	orl	$1, %eax
	imull	%ecx, %eax
	testb	$1, %dl
	je	.L373
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L378:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L389
.L305:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L306
	movq	(%rdi), %rax
	call	*8(%rax)
.L306:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L307
	movq	(%rdi), %rax
	call	*8(%rax)
.L307:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L377:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L390
.L303:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateEE28trace_event_unique_atomic124(%rip)
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L387:
	comisd	.LC10(%rip), %xmm0
	jb	.L313
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L313
	je	.L312
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L384:
	comisd	.LC10(%rip), %xmm0
	jb	.L326
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L326
	jne	.L326
	movl	%edx, %edx
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L388:
	cmpl	$-52, %ecx
	jl	.L312
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rax
	addq	%rcx, %rax
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rax
	movq	%rax, %rcx
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L385:
	cmpl	$-52, %ecx
	jl	.L325
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rcx
	andq	%rdi, %rdx
	addq	%rcx, %rdx
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rdx
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L386:
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L376:
	movq	40960(%rsi), %rax
	movl	$559, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L383:
	leaq	.LC11(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L381:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L389:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L305
.L340:
	xorl	%eax, %eax
	jmp	.L312
.L382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29152:
	.size	_ZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL27Stats_Runtime_ICsAreEnabledEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"V8.Runtime_Runtime_ICsAreEnabled"
	.section	.text._ZN2v88internalL27Stats_Runtime_ICsAreEnabledEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_ICsAreEnabledEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_ICsAreEnabledEiPmPNS0_7IsolateE.isra.0:
.LFB29153:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L422
.L392:
	movq	_ZZN2v88internalL27Stats_Runtime_ICsAreEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic211(%rip), %rbx
	testq	%rbx, %rbx
	je	.L423
.L394:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L424
.L396:
	cmpb	$0, _ZN2v88internal11FLAG_use_icE(%rip)
	je	.L400
	movq	112(%r12), %r12
.L401:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L425
.L391:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L426
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L423:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L427
.L395:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_ICsAreEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic211(%rip)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L424:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L428
.L397:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L398
	movq	(%rdi), %rax
	call	*8(%rax)
.L398:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L399
	movq	(%rdi), %rax
	call	*8(%rax)
.L399:
	leaq	.LC12(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L425:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L422:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$604, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L428:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L427:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L395
.L426:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29153:
	.size	_ZN2v88internalL27Stats_Runtime_ICsAreEnabledEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_ICsAreEnabledEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL48Stats_Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8.Runtime_Runtime_IsConcurrentRecompilationSupported"
	.section	.text._ZN2v88internalL48Stats_Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Stats_Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL48Stats_Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateE.isra.0:
.LFB29154:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L460
.L430:
	movq	_ZZN2v88internalL48Stats_Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateEE28trace_event_unique_atomic217(%rip), %rbx
	testq	%rbx, %rbx
	je	.L461
.L432:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L462
.L434:
	cmpq	$0, 45416(%r12)
	je	.L438
	movq	112(%r12), %r12
.L439:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L463
.L429:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L464
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L461:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L465
.L433:
	movq	%rbx, _ZZN2v88internalL48Stats_Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateEE28trace_event_unique_atomic217(%rip)
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L462:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L466
.L435:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L436
	movq	(%rdi), %rax
	call	*8(%rax)
.L436:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L437
	movq	(%rdi), %rax
	call	*8(%rax)
.L437:
	leaq	.LC13(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L463:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L460:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$607, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L466:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L465:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L433
.L464:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29154:
	.size	_ZN2v88internalL48Stats_Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL48Stats_Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL24Stats_Runtime_DebugPrintEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC14:
	.string	"V8.Runtime_Runtime_DebugPrint"
.LC15:
	.string	"[weak cleared]"
.LC16:
	.string	"[weak] "
	.section	.text._ZN2v88internalL24Stats_Runtime_DebugPrintEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_DebugPrintEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_DebugPrintEiPmPNS0_7IsolateE.isra.0:
.LFB29155:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -448(%rbp)
	movq	$0, -416(%rbp)
	movaps	%xmm0, -432(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L505
.L468:
	movq	_ZZN2v88internalL24Stats_Runtime_DebugPrintEiPmPNS0_7IsolateEE28trace_event_unique_atomic680(%rip), %rbx
	testq	%rbx, %rbx
	je	.L506
.L470:
	movq	$0, -480(%rbp)
	movzbl	(%rbx), %eax
	leaq	-400(%rbp), %r13
	testb	$5, %al
	jne	.L507
.L472:
	leaq	-320(%rbp), %r14
	movq	(%r12), %rbx
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r15
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -96(%rbp)
	movq	%r15, -320(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	cmpl	$3, %ebx
	jne	.L476
	movl	$14, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L477:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %rbx
	testq	%rbx, %rbx
	je	.L508
	cmpb	$0, 56(%rbx)
	je	.L481
	movsbl	67(%rbx), %esi
.L482:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	(%r12), %r12
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%r15, -320(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	leaq	-480(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-448(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L509
.L467:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L510
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	movq	%rbx, %rcx
	andq	$-3, %rcx
	testb	$1, %bl
	cmove	%rbx, %rcx
	andl	$3, %ebx
	cmpq	$3, %rbx
	je	.L511
.L479:
	leaq	-488(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rcx, -488(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L481:
	movq	%rbx, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%rbx), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L482
	movq	%rbx, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L507:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -400(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L512
.L473:
	movq	-392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L474
	movq	(%rdi), %rax
	call	*8(%rax)
.L474:
	movq	-400(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L475
	movq	(%rdi), %rax
	call	*8(%rax)
.L475:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -472(%rbp)
	movq	%rax, -464(%rbp)
	leaq	-472(%rbp), %rax
	movq	%r14, -456(%rbp)
	movq	%rax, -480(%rbp)
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L506:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L513
.L471:
	movq	%rbx, _ZZN2v88internalL24Stats_Runtime_DebugPrintEiPmPNS0_7IsolateEE28trace_event_unique_atomic680(%rip)
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L511:
	movl	$7, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r13, %rdi
	movq	%rcx, -504(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-504(%rbp), %rcx
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L509:
	leaq	-440(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L505:
	movq	40960(%rsi), %rdi
	movl	$561, %edx
	leaq	-440(%rbp), %rsi
	addq	$23240, %rdi
	movq	%rdi, -448(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L513:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L512:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%r13
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L473
.L508:
	call	_ZSt16__throw_bad_castv@PLT
.L510:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29155:
	.size	_ZN2v88internalL24Stats_Runtime_DebugPrintEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_DebugPrintEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Runtime_Runtime_GlobalPrint"
	.section	.rodata._ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC18:
	.string	"args[0].IsString()"
.LC19:
	.string	"unreachable code"
.LC20:
	.string	"%c"
	.section	.text._ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0:
.LFB29156:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -416(%rbp)
	movq	$0, -384(%rbp)
	movaps	%xmm0, -400(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L618
.L515:
	movq	_ZZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateEE28trace_event_unique_atomic781(%rip), %rax
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L619
.L517:
	movq	$0, -448(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L620
.L519:
	movq	(%r12), %r12
	testb	$1, %r12b
	jne	.L523
.L524:
	leaq	.LC18(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L619:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L621
.L518:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateEE28trace_event_unique_atomic781(%rip)
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L523:
	movq	-1(%r12), %rax
	cmpw	$63, 11(%rax)
	ja	.L524
	leaq	-368(%rbp), %r13
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movl	$32, %ecx
	movq	%r13, %rdi
	leaq	.L528(%rip), %rdx
	xorl	%r14d, %r14d
	rep stosq
	movb	$0, -88(%rbp)
	movq	$0, -112(%rbp)
	movl	$0, -460(%rbp)
	movaps	%xmm0, -80(%rbp)
	movslq	11(%r12), %rbx
.L525:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L526
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0,"a",@progbits
	.align 4
	.align 4
.L528:
	.long	.L534-.L528
	.long	.L531-.L528
	.long	.L533-.L528
	.long	.L529-.L528
	.long	.L526-.L528
	.long	.L527-.L528
	.long	.L526-.L528
	.long	.L526-.L528
	.long	.L532-.L528
	.long	.L531-.L528
	.long	.L530-.L528
	.long	.L529-.L528
	.long	.L526-.L528
	.long	.L527-.L528
	.section	.text._ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L531:
	movl	$0, -104(%rbp)
	movl	-460(%rbp), %edx
	testq	%rsi, %rsi
	je	.L548
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi@PLT
	movl	-104(%rbp), %esi
	movl	$0, -460(%rbp)
	testl	%esi, %esi
	jne	.L541
.L548:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
.L540:
	leaq	.L561(%rip), %rbx
	leaq	.L580(%rip), %r14
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %r15
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L616:
	movzbl	-88(%rbp), %edx
.L557:
	testb	%dl, %dl
	je	.L588
.L587:
	leaq	1(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzbl	(%rax), %esi
.L592:
	xorl	%eax, %eax
	leaq	.LC20(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
.L593:
	cmpq	%rdx, %rax
	jne	.L616
	movl	$0, -456(%rbp)
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L622
.L558:
	leaq	-448(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-416(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L623
.L514:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L624
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L586:
	.cfi_restore_state
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L588:
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %esi
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L622:
	leaq	-456(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L558
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L559:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L526
	movzwl	%dx, %edx
	movslq	(%rbx,%rdx,4), %rdx
	addq	%rbx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0
	.align 4
	.align 4
.L561:
	.long	.L567-.L561
	.long	.L564-.L561
	.long	.L566-.L561
	.long	.L562-.L561
	.long	.L526-.L561
	.long	.L560-.L561
	.long	.L526-.L561
	.long	.L526-.L561
	.long	.L565-.L561
	.long	.L564-.L561
	.long	.L563-.L561
	.long	.L562-.L561
	.long	.L526-.L561
	.long	.L560-.L561
	.section	.text._ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L560:
	movq	15(%rax), %rax
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L562:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L564:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L568:
	cmpq	%rcx, %rax
	jne	.L616
	movl	$0, -452(%rbp)
	movl	-104(%rbp), %edx
	testl	%edx, %edx
	je	.L616
	leaq	-452(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L625
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L578:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L526
	movzwl	%dx, %edx
	movslq	(%r14,%rdx,4), %rdx
	addq	%r14, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0
	.align 4
	.align 4
.L580:
	.long	.L586-.L580
	.long	.L583-.L580
	.long	.L585-.L580
	.long	.L581-.L580
	.long	.L526-.L580
	.long	.L579-.L580
	.long	.L526-.L580
	.long	.L526-.L580
	.long	.L584-.L580
	.long	.L583-.L580
	.long	.L582-.L580
	.long	.L581-.L580
	.long	.L526-.L580
	.long	.L579-.L580
	.section	.text._ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L566:
	movq	15(%rax), %rdi
	movl	%esi, -472(%rbp)
	movl	%ecx, -468(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-472(%rbp), %rsi
	movslq	-468(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L567:
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L563:
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%r15, %rax
	jne	.L569
	movq	8(%rdi), %rax
.L570:
	movslq	%esi, %rsi
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L565:
	movslq	%esi, %rsi
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L620:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -368(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L626
.L520:
	movq	-360(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L521
	movq	(%rdi), %rax
	call	*8(%rax)
.L521:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L522
	movq	(%rdi), %rax
	call	*8(%rax)
.L522:
	leaq	.LC17(%rip), %rax
	movq	%rbx, -440(%rbp)
	movq	%rax, -432(%rbp)
	leaq	-440(%rbp), %rax
	movq	%r13, -424(%rbp)
	movq	%rax, -448(%rbp)
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L527:
	movq	15(%rsi), %rsi
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L529:
	addl	27(%rsi), %r14d
	movq	15(%rsi), %rsi
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L579:
	movq	15(%rax), %rax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L581:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L583:
	movzbl	-88(%rbp), %edx
	movq	-80(%rbp), %rax
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L533:
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, -88(%rbp)
	movq	%rax, %r8
	movslq	%r14d, %rax
	leaq	(%r8,%rax,2), %rax
	leaq	(%rax,%rbx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
.L535:
	movl	$0, -104(%rbp)
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L534:
	movslq	%r14d, %rax
	movb	$0, -88(%rbp)
	leaq	15(%rsi,%rax,2), %rax
	leaq	(%rax,%rbx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L530:
	movq	15(%rsi), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L536
	movq	8(%rdi), %rdx
.L537:
	movslq	%r14d, %rax
	addq	%rdx, %rax
.L617:
	movslq	%ebx, %rdx
	movb	$1, -88(%rbp)
	addq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L532:
	movslq	%r14d, %rax
	leaq	15(%rsi,%rax), %rax
	jmp	.L617
.L585:
	movq	15(%rax), %rdi
	movl	%ecx, -472(%rbp)
	movl	%esi, -468(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-468(%rbp), %rsi
	movslq	-472(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %esi
	jmp	.L592
.L584:
	movslq	%esi, %rsi
	movslq	%ecx, %rdx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L587
.L582:
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%r15, %rax
	jne	.L589
	movq	8(%rdi), %rax
.L590:
	movslq	%esi, %rsi
	movslq	%ecx, %rdx
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L541:
	leaq	-460(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L548
	movl	-460(%rbp), %edi
	movl	11(%rax), %edx
	leaq	.L545(%rip), %rsi
	movl	%edi, %r14d
.L543:
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$15, %ecx
	cmpw	$13, %cx
	ja	.L526
	movzwl	%cx, %ecx
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0
	.align 4
	.align 4
.L545:
	.long	.L551-.L545
	.long	.L548-.L545
	.long	.L550-.L545
	.long	.L546-.L545
	.long	.L526-.L545
	.long	.L544-.L545
	.long	.L526-.L545
	.long	.L526-.L545
	.long	.L549-.L545
	.long	.L548-.L545
	.long	.L547-.L545
	.long	.L546-.L545
	.long	.L526-.L545
	.long	.L544-.L545
	.section	.text._ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L618:
	movq	40960(%rsi), %rdi
	movl	$579, %edx
	leaq	-408(%rbp), %rsi
	addq	$23240, %rdi
	movq	%rdi, -416(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L623:
	leaq	-408(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L625:
	movq	-80(%rbp), %rax
	jmp	.L616
.L569:
	movl	%esi, -472(%rbp)
	movl	%ecx, -468(%rbp)
	call	*%rax
	movl	-472(%rbp), %esi
	movslq	-468(%rbp), %rcx
	jmp	.L570
.L621:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L518
.L626:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$88, %esi
	leaq	-368(%rbp), %rdx
	pushq	$0
	leaq	.LC17(%rip), %rcx
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L526:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L544:
	movq	15(%rax), %rax
	jmp	.L543
.L546:
	addl	27(%rax), %r14d
	movq	15(%rax), %rax
	jmp	.L543
.L547:
	movl	%edx, %ebx
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	subl	%edi, %ebx
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L553
	movq	8(%rdi), %rax
.L554:
	movslq	%r14d, %r14
	movslq	%ebx, %rdx
	movb	$1, -88(%rbp)
	addq	%r14, %rax
	addq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L540
.L549:
	movslq	%r14d, %r14
	subl	%edi, %edx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%r14), %rax
	movslq	%edx, %rdx
	addq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L540
.L550:
	movl	%edx, %ebx
	movslq	%r14d, %r14
	subl	%edi, %ebx
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%ebx, %rdx
	movb	$0, -88(%rbp)
	leaq	(%rax,%r14,2), %rax
	leaq	(%rax,%rdx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L540
.L551:
	movslq	%r14d, %r14
	subl	%edi, %edx
	movb	$0, -88(%rbp)
	leaq	15(%rax,%r14,2), %rax
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L540
.L536:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L537
.L589:
	movl	%ecx, -472(%rbp)
	movl	%esi, -468(%rbp)
	call	*%rax
	movl	-472(%rbp), %ecx
	movl	-468(%rbp), %esi
	jmp	.L590
.L553:
	call	*%rax
	jmp	.L554
.L624:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29156:
	.size	_ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL23Stats_Runtime_TraceExitEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC21:
	.string	"V8.Runtime_Runtime_TraceExit"
.LC22:
	.string	""
.LC23:
	.string	"%4d:%*s"
.LC24:
	.string	"..."
.LC25:
	.string	"} -> "
.LC26:
	.string	"\n"
	.section	.text._ZN2v88internalL23Stats_Runtime_TraceExitEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL23Stats_Runtime_TraceExitEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL23Stats_Runtime_TraceExitEiPmPNS0_7IsolateE.isra.0:
.LFB29158:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L658
.L628:
	movq	_ZZN2v88internalL23Stats_Runtime_TraceExitEiPmPNS0_7IsolateEE28trace_event_unique_atomic902(%rip), %rbx
	testq	%rbx, %rbx
	je	.L659
.L630:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L660
.L632:
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_19StackSizeEPNS0_7IsolateE
	movl	%eax, %esi
	cmpl	$80, %eax
	jg	.L636
	movl	%eax, %edx
	leaq	.LC22(%rip), %rcx
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L637:
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	-152(%rbp), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	-144(%rbp), %rdi
	movq	-152(%rbp), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L661
.L627:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L662
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L636:
	.cfi_restore_state
	leaq	.LC24(%rip), %rcx
	movl	$80, %edx
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L660:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L663
.L633:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L634
	movq	(%rdi), %rax
	call	*8(%rax)
.L634:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L635
	movq	(%rdi), %rax
	call	*8(%rax)
.L635:
	leaq	.LC21(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L659:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L664
.L631:
	movq	%rbx, _ZZN2v88internalL23Stats_Runtime_TraceExitEiPmPNS0_7IsolateEE28trace_event_unique_atomic902(%rip)
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L658:
	movq	40960(%rsi), %rax
	movl	$633, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L661:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L664:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L663:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC21(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L633
.L662:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29158:
	.size	_ZN2v88internalL23Stats_Runtime_TraceExitEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL23Stats_Runtime_TraceExitEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"V8.Runtime_Runtime_HaveSameMap"
	.section	.rodata._ZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC28:
	.string	"args[0].IsJSObject()"
.LC29:
	.string	"args[1].IsJSObject()"
	.section	.text._ZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateE.isra.0:
.LFB29159:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L700
.L666:
	movq	_ZZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic913(%rip), %rbx
	testq	%rbx, %rbx
	je	.L701
.L668:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L702
.L670:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L674
.L675:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L701:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L703
.L669:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic913(%rip)
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L674:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L675
	movq	-8(%r13), %rdx
	testb	$1, %dl
	jne	.L704
.L676:
	leaq	.LC29(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L702:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L705
.L671:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L672
	movq	(%rdi), %rax
	call	*8(%rax)
.L672:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L673
	movq	(%rdi), %rax
	call	*8(%rax)
.L673:
	leaq	.LC27(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L704:
	movq	-1(%rdx), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L676
	movq	-1(%rdx), %rdx
	movq	-1(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L678
	movq	112(%r12), %r12
.L679:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L706
.L665:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L707
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L678:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L700:
	movq	40960(%rsi), %rax
	movl	$602, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L706:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L705:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC27(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L703:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L669
.L707:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29159:
	.size	_ZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC30:
	.string	"V8.Runtime_Runtime_HasElementsInALargeObjectSpace"
	.section	.rodata._ZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC31:
	.string	"args[0].IsJSArray()"
	.section	.text._ZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE.isra.0:
.LFB29160:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L741
.L709:
	movq	_ZZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateEE28trace_event_unique_atomic921(%rip), %rbx
	testq	%rbx, %rbx
	je	.L742
.L711:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L743
.L713:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L717
.L718:
	leaq	.LC31(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L742:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L744
.L712:
	movq	%rbx, _ZZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateEE28trace_event_unique_atomic921(%rip)
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L717:
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	jne	.L718
	movq	15(%rax), %r13
	movq	37888(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal16LargeObjectSpace8ContainsENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L745
.L719:
	movq	112(%r12), %r12
.L720:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L746
.L708:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L747
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L743:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L748
.L714:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L715
	movq	(%rdi), %rax
	call	*8(%rax)
.L715:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L716
	movq	(%rdi), %rax
	call	*8(%rax)
.L716:
	leaq	.LC30(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L745:
	movq	37872(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal16LargeObjectSpace8ContainsENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L719
	movq	120(%r12), %r12
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L741:
	movq	40960(%rsi), %rax
	movl	$582, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L746:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L744:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L748:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC30(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L714
.L747:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29160:
	.size	_ZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_InYoungGenerationEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"V8.Runtime_Runtime_InYoungGeneration"
	.section	.text._ZN2v88internalL31Stats_Runtime_InYoungGenerationEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_InYoungGenerationEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_InYoungGenerationEiPmPNS0_7IsolateE.isra.0:
.LFB29161:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L783
.L750:
	movq	_ZZN2v88internalL31Stats_Runtime_InYoungGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic931(%rip), %rbx
	testq	%rbx, %rbx
	je	.L784
.L752:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L785
.L754:
	movq	0(%r13), %rax
	testb	$1, %al
	je	.L758
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L758
	movq	112(%r12), %r12
.L759:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L786
.L749:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L787
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L758:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L785:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L788
.L755:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L756
	movq	(%rdi), %rax
	call	*8(%rax)
.L756:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L757
	movq	(%rdi), %rax
	call	*8(%rax)
.L757:
	leaq	.LC32(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L784:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L789
.L753:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_InYoungGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic931(%rip)
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L783:
	movq	40960(%rsi), %rax
	movl	$605, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L786:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L789:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L788:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC32(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L755
.L787:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29161:
	.size	_ZN2v88internalL31Stats_Runtime_InYoungGenerationEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_InYoungGenerationEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL24Stats_Runtime_IsWasmCodeEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC33:
	.string	"V8.Runtime_Runtime_IsWasmCode"
.LC34:
	.string	"args[0].IsJSFunction()"
	.section	.text._ZN2v88internalL24Stats_Runtime_IsWasmCodeEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_IsWasmCodeEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_IsWasmCodeEiPmPNS0_7IsolateE.isra.0:
.LFB29162:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L823
.L791:
	movq	_ZZN2v88internalL24Stats_Runtime_IsWasmCodeEiPmPNS0_7IsolateEE28trace_event_unique_atomic981(%rip), %rbx
	testq	%rbx, %rbx
	je	.L824
.L793:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L825
.L795:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L799
.L800:
	leaq	.LC34(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L824:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L826
.L794:
	movq	%rbx, _ZZN2v88internalL24Stats_Runtime_IsWasmCodeEiPmPNS0_7IsolateEE28trace_event_unique_atomic981(%rip)
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L799:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L800
	movq	47(%rax), %rax
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$8, %eax
	jne	.L801
	movq	112(%r12), %r12
.L802:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L827
.L790:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L828
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L825:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L829
.L796:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L797
	movq	(%rdi), %rax
	call	*8(%rax)
.L797:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L798
	movq	(%rdi), %rax
	call	*8(%rax)
.L798:
	leaq	.LC33(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L823:
	movq	40960(%rsi), %rax
	movl	$610, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L827:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L826:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L829:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC33(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L796
.L828:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29162:
	.size	_ZN2v88internalL24Stats_Runtime_IsWasmCodeEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_IsWasmCodeEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL38Stats_Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC35:
	.string	"V8.Runtime_Runtime_IsWasmTrapHandlerEnabled"
	.section	.text._ZN2v88internalL38Stats_Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL38Stats_Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateE.isra.0:
.LFB29163:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L861
.L831:
	movq	_ZZN2v88internalL38Stats_Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic989(%rip), %rbx
	testq	%rbx, %rbx
	je	.L862
.L833:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L863
.L835:
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L839
	movq	112(%r12), %r12
.L840:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L864
.L830:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L865
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L839:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L862:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L866
.L834:
	movq	%rbx, _ZZN2v88internalL38Stats_Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic989(%rip)
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L863:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L867
.L836:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L837
	movq	(%rdi), %rax
	call	*8(%rax)
.L837:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L838
	movq	(%rdi), %rax
	call	*8(%rax)
.L838:
	leaq	.LC35(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L864:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L861:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$611, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L867:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC35(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L866:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L834
.L865:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29163:
	.size	_ZN2v88internalL38Stats_Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL38Stats_Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_IsThreadInWasmEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"V8.Runtime_Runtime_IsThreadInWasm"
	.section	.text._ZN2v88internalL28Stats_Runtime_IsThreadInWasmEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_IsThreadInWasmEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_IsThreadInWasmEiPmPNS0_7IsolateE.isra.0:
.LFB29164:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L899
.L869:
	movq	_ZZN2v88internalL28Stats_Runtime_IsThreadInWasmEiPmPNS0_7IsolateEE28trace_event_unique_atomic995(%rip), %rbx
	testq	%rbx, %rbx
	je	.L900
.L871:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L901
.L873:
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	testl	%eax, %eax
	je	.L877
	movq	112(%r12), %r12
.L878:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L902
.L868:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L903
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L877:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L900:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L904
.L872:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_IsThreadInWasmEiPmPNS0_7IsolateEE28trace_event_unique_atomic995(%rip)
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L901:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L905
.L874:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L875
	movq	(%rdi), %rax
	call	*8(%rax)
.L875:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L876
	movq	(%rdi), %rax
	call	*8(%rax)
.L876:
	leaq	.LC36(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L902:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L899:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$609, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L905:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC36(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L904:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L872
.L903:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29164:
	.size	_ZN2v88internalL28Stats_Runtime_IsThreadInWasmEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_IsThreadInWasmEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL39Stats_Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"V8.Runtime_Runtime_GetWasmRecoveredTrapCount"
	.section	.text._ZN2v88internalL39Stats_Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL39Stats_Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateE.isra.0:
.LFB29165:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L944
.L907:
	movq	_ZZN2v88internalL39Stats_Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateEE29trace_event_unique_atomic1001(%rip), %rbx
	testq	%rbx, %rbx
	je	.L945
.L909:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L946
.L911:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	call	_ZN2v88internal12trap_handler21GetRecoveredTrapCountEv@PLT
	cmpq	$2147483647, %rax
	ja	.L915
	movq	41112(%r12), %rdi
	salq	$32, %rax
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L916
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L919:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L924
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L924:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L947
.L906:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L948
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L916:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L949
.L918:
	movq	%r13, (%rax)
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L915:
	testq	%rax, %rax
	js	.L920
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L921:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L946:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L950
.L912:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L913
	movq	(%rdi), %rax
	call	*8(%rax)
.L913:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L914
	movq	(%rdi), %rax
	call	*8(%rax)
.L914:
	leaq	.LC37(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L945:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L951
.L910:
	movq	%rbx, _ZZN2v88internalL39Stats_Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateEE29trace_event_unique_atomic1001(%rip)
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L920:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L947:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L944:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$578, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L951:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L950:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC37(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L949:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L918
.L948:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29165:
	.size	_ZN2v88internalL39Stats_Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL39Stats_Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL29Stats_Runtime_HasFastElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC38:
	.string	"V8.Runtime_Runtime_HasFastElements"
	.section	.text._ZN2v88internalL29Stats_Runtime_HasFastElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_HasFastElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_HasFastElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29166:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L985
.L953:
	movq	_ZZN2v88internalL29Stats_Runtime_HasFastElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1076(%rip), %rbx
	testq	%rbx, %rbx
	je	.L986
.L955:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L987
.L957:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L961
.L962:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L986:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L988
.L956:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_HasFastElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1076(%rip)
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L961:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L962
	movq	-1(%rax), %rax
	cmpb	$47, 14(%rax)
	ja	.L963
	movq	112(%r12), %r12
.L964:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L989
.L952:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L990
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L963:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L987:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L991
.L958:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L959
	movq	(%rdi), %rax
	call	*8(%rax)
.L959:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L960
	movq	(%rdi), %rax
	call	*8(%rax)
.L960:
	leaq	.LC38(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L985:
	movq	40960(%rsi), %rax
	movl	$583, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L989:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L988:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L991:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC38(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L958
.L990:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29166:
	.size	_ZN2v88internalL29Stats_Runtime_HasFastElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_HasFastElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_HasSmiElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC39:
	.string	"V8.Runtime_Runtime_HasSmiElements"
	.section	.text._ZN2v88internalL28Stats_Runtime_HasSmiElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_HasSmiElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_HasSmiElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29167:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1025
.L993:
	movq	_ZZN2v88internalL28Stats_Runtime_HasSmiElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1077(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1026
.L995:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1027
.L997:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1001
.L1002:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1026:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1028
.L996:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_HasSmiElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1077(%rip)
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1002
	movq	-1(%rax), %rax
	cmpb	$15, 14(%rax)
	ja	.L1003
	movq	112(%r12), %r12
.L1004:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1029
.L992:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1030
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1003:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1027:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1031
.L998:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L999
	movq	(%rdi), %rax
	call	*8(%rax)
.L999:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1000
	movq	(%rdi), %rax
	call	*8(%rax)
.L1000:
	leaq	.LC39(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1025:
	movq	40960(%rsi), %rax
	movl	$600, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1029:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1028:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1031:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC39(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L998
.L1030:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29167:
	.size	_ZN2v88internalL28Stats_Runtime_HasSmiElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_HasSmiElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_HasObjectElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC40:
	.string	"V8.Runtime_Runtime_HasObjectElements"
	.section	.text._ZN2v88internalL31Stats_Runtime_HasObjectElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_HasObjectElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_HasObjectElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29168:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1065
.L1033:
	movq	_ZZN2v88internalL31Stats_Runtime_HasObjectElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1078(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1066
.L1035:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1067
.L1037:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1041
.L1042:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1066:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1068
.L1036:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_HasObjectElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1078(%rip)
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1041:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1042
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$2, %eax
	cmpb	$1, %al
	ja	.L1043
	movq	112(%r12), %r12
.L1044:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1069
.L1032:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1070
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1043:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1067:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1071
.L1038:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1039
	movq	(%rdi), %rax
	call	*8(%rax)
.L1039:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1040
	movq	(%rdi), %rax
	call	*8(%rax)
.L1040:
	leaq	.LC40(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	40960(%rsi), %rax
	movl	$597, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1069:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1068:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1071:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC40(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1038
.L1070:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29168:
	.size	_ZN2v88internalL31Stats_Runtime_HasObjectElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_HasObjectElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC41:
	.string	"V8.Runtime_Runtime_HasSmiOrObjectElements"
	.section	.text._ZN2v88internalL36Stats_Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29169:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1105
.L1073:
	movq	_ZZN2v88internalL36Stats_Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1079(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1106
.L1075:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1107
.L1077:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1081
.L1082:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1106:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1108
.L1076:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1079(%rip)
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1082
	movq	-1(%rax), %rax
	cmpb	$31, 14(%rax)
	ja	.L1083
	movq	112(%r12), %r12
.L1084:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1109
.L1072:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1110
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1083:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1107:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1111
.L1078:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1079
	movq	(%rdi), %rax
	call	*8(%rax)
.L1079:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1080
	movq	(%rdi), %rax
	call	*8(%rax)
.L1080:
	leaq	.LC41(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1105:
	movq	40960(%rsi), %rax
	movl	$601, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1109:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1108:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1076
	.p2align 4,,10
	.p2align 3
.L1111:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC41(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1078
.L1110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29169:
	.size	_ZN2v88internalL36Stats_Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_HasDoubleElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC42:
	.string	"V8.Runtime_Runtime_HasDoubleElements"
	.section	.text._ZN2v88internalL31Stats_Runtime_HasDoubleElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_HasDoubleElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_HasDoubleElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29170:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1145
.L1113:
	movq	_ZZN2v88internalL31Stats_Runtime_HasDoubleElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1080(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1146
.L1115:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1147
.L1117:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1121
.L1122:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1146:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1148
.L1116:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_HasDoubleElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1080(%rip)
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1121:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1122
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$4, %eax
	cmpb	$1, %al
	ja	.L1123
	movq	112(%r12), %r12
.L1124:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1149
.L1112:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1150
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1123:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1147:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1151
.L1118:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1119
	movq	(%rdi), %rax
	call	*8(%rax)
.L1119:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1120
	movq	(%rdi), %rax
	call	*8(%rax)
.L1120:
	leaq	.LC42(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1145:
	movq	40960(%rsi), %rax
	movl	$581, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1149:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1148:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1151:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC42(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1118
.L1150:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29170:
	.size	_ZN2v88internalL31Stats_Runtime_HasDoubleElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_HasDoubleElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL30Stats_Runtime_HasHoleyElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC43:
	.string	"V8.Runtime_Runtime_HasHoleyElements"
	.section	.text._ZN2v88internalL30Stats_Runtime_HasHoleyElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_HasHoleyElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL30Stats_Runtime_HasHoleyElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29171:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1190
.L1153:
	movq	_ZZN2v88internalL30Stats_Runtime_HasHoleyElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1081(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1191
.L1155:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1192
.L1157:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1161
.L1162:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1191:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1193
.L1156:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_HasHoleyElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1081(%rip)
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1162
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpb	$5, %al
	ja	.L1170
	testb	$1, %al
	jne	.L1163
.L1170:
	movq	120(%r12), %r12
.L1165:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1194
.L1152:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1195
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1163:
	.cfi_restore_state
	movq	112(%r12), %r12
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1192:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1196
.L1158:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1159
	movq	(%rdi), %rax
	call	*8(%rax)
.L1159:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1160
	movq	(%rdi), %rax
	call	*8(%rax)
.L1160:
	leaq	.LC43(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1190:
	movq	40960(%rsi), %rax
	movl	$596, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1194:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1193:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1196:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC43(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1158
.L1195:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29171:
	.size	_ZN2v88internalL30Stats_Runtime_HasHoleyElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL30Stats_Runtime_HasHoleyElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_HasDictionaryElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC44:
	.string	"V8.Runtime_Runtime_HasDictionaryElements"
	.section	.text._ZN2v88internalL35Stats_Runtime_HasDictionaryElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_HasDictionaryElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_HasDictionaryElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29172:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1230
.L1198:
	movq	_ZZN2v88internalL35Stats_Runtime_HasDictionaryElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1082(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1231
.L1200:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1232
.L1202:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1206
.L1207:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1231:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1233
.L1201:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_HasDictionaryElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1082(%rip)
	jmp	.L1200
	.p2align 4,,10
	.p2align 3
.L1206:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1207
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$12, %eax
	jne	.L1208
	movq	112(%r12), %r12
.L1209:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1234
.L1197:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1235
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1208:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1232:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1236
.L1203:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1204
	movq	(%rdi), %rax
	call	*8(%rax)
.L1204:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1205
	movq	(%rdi), %rax
	call	*8(%rax)
.L1205:
	leaq	.LC44(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	40960(%rsi), %rax
	movl	$580, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1234:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1233:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1236:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC44(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1203
.L1235:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29172:
	.size	_ZN2v88internalL35Stats_Runtime_HasDictionaryElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_HasDictionaryElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_HasPackedElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC45:
	.string	"V8.Runtime_Runtime_HasPackedElements"
	.section	.text._ZN2v88internalL31Stats_Runtime_HasPackedElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_HasPackedElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_HasPackedElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29173:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1270
.L1238:
	movq	_ZZN2v88internalL31Stats_Runtime_HasPackedElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1083(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1271
.L1240:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1272
.L1242:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1246
.L1247:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1271:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1273
.L1241:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_HasPackedElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1083(%rip)
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1246:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1247
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$2, %eax
	jne	.L1248
	movq	112(%r12), %r12
.L1249:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1274
.L1237:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1275
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1248:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1272:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1276
.L1243:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1244
	movq	(%rdi), %rax
	call	*8(%rax)
.L1244:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1245
	movq	(%rdi), %rax
	call	*8(%rax)
.L1245:
	leaq	.LC45(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1270:
	movq	40960(%rsi), %rax
	movl	$598, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1274:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1273:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1276:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC45(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1243
.L1275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29173:
	.size	_ZN2v88internalL31Stats_Runtime_HasPackedElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_HasPackedElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL40Stats_Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC46:
	.string	"V8.Runtime_Runtime_HasSloppyArgumentsElements"
	.section	.text._ZN2v88internalL40Stats_Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29174:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1310
.L1278:
	movq	_ZZN2v88internalL40Stats_Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1084(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1311
.L1280:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1312
.L1282:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1286
.L1287:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1311:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1313
.L1281:
	movq	%rbx, _ZZN2v88internalL40Stats_Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1084(%rip)
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1286:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1287
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$13, %eax
	cmpb	$1, %al
	ja	.L1288
	movq	112(%r12), %r12
.L1289:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1314
.L1277:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1315
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1288:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1312:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1316
.L1283:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1284
	movq	(%rdi), %rax
	call	*8(%rax)
.L1284:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1285
	movq	(%rdi), %rax
	call	*8(%rax)
.L1285:
	leaq	.LC46(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1282
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	40960(%rsi), %rax
	movl	$599, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1278
	.p2align 4,,10
	.p2align 3
.L1314:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1313:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1316:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC46(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1283
.L1315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29174:
	.size	_ZN2v88internalL40Stats_Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_HasFastPropertiesEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC47:
	.string	"V8.Runtime_Runtime_HasFastProperties"
	.section	.text._ZN2v88internalL31Stats_Runtime_HasFastPropertiesEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_HasFastPropertiesEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_HasFastPropertiesEiPmPNS0_7IsolateE.isra.0:
.LFB29175:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1350
.L1318:
	movq	_ZZN2v88internalL31Stats_Runtime_HasFastPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1086(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1351
.L1320:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1352
.L1322:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1326
.L1327:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1351:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1353
.L1321:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_HasFastPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1086(%rip)
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1326:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1327
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	jne	.L1328
	movq	112(%r12), %r12
.L1329:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1354
.L1317:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1355
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1328:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1352:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1356
.L1323:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1324
	movq	(%rdi), %rax
	call	*8(%rax)
.L1324:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1325
	movq	(%rdi), %rax
	call	*8(%rax)
.L1325:
	leaq	.LC47(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1350:
	movq	40960(%rsi), %rax
	movl	$584, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1354:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1353:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1321
	.p2align 4,,10
	.p2align 3
.L1356:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC47(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1323
.L1355:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29175:
	.size	_ZN2v88internalL31Stats_Runtime_HasFastPropertiesEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_HasFastPropertiesEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC48:
	.string	"V8.Runtime_Runtime_HasFixedUint8Elements"
	.section	.text._ZN2v88internalL35Stats_Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29176:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1390
.L1358:
	movq	_ZZN2v88internalL35Stats_Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1391
.L1360:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1392
.L1362:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1366
.L1367:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1391:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1393
.L1361:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip)
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1366:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1367
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$17, %eax
	jne	.L1368
	movq	112(%r12), %r12
.L1369:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1394
.L1357:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1395
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1368:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1392:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1396
.L1363:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1364
	movq	(%rdi), %rax
	call	*8(%rax)
.L1364:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1365
	movq	(%rdi), %rax
	call	*8(%rax)
.L1365:
	leaq	.LC48(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1390:
	movq	40960(%rsi), %rax
	movl	$595, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1358
	.p2align 4,,10
	.p2align 3
.L1394:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1393:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1361
	.p2align 4,,10
	.p2align 3
.L1396:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC48(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1363
.L1395:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29176:
	.size	_ZN2v88internalL35Stats_Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL34Stats_Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC49:
	.string	"V8.Runtime_Runtime_HasFixedInt8Elements"
	.section	.text._ZN2v88internalL34Stats_Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29177:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1430
.L1398:
	movq	_ZZN2v88internalL34Stats_Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1431
.L1400:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1432
.L1402:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1406
.L1407:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1431:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1433
.L1401:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip)
	jmp	.L1400
	.p2align 4,,10
	.p2align 3
.L1406:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1407
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$18, %eax
	jne	.L1408
	movq	112(%r12), %r12
.L1409:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1434
.L1397:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1435
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1408:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1432:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1436
.L1403:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1404
	movq	(%rdi), %rax
	call	*8(%rax)
.L1404:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1405
	movq	(%rdi), %rax
	call	*8(%rax)
.L1405:
	leaq	.LC49(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1430:
	movq	40960(%rsi), %rax
	movl	$591, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1434:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1397
	.p2align 4,,10
	.p2align 3
.L1433:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1401
	.p2align 4,,10
	.p2align 3
.L1436:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC49(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1403
.L1435:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29177:
	.size	_ZN2v88internalL34Stats_Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC50:
	.string	"V8.Runtime_Runtime_HasFixedUint16Elements"
	.section	.text._ZN2v88internalL36Stats_Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29178:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1470
.L1438:
	movq	_ZZN2v88internalL36Stats_Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1471
.L1440:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1472
.L1442:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1446
.L1447:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1471:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1473
.L1441:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip)
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1447
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$19, %eax
	jne	.L1448
	movq	112(%r12), %r12
.L1449:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1474
.L1437:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1475
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1448:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1472:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1476
.L1443:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1444
	movq	(%rdi), %rax
	call	*8(%rax)
.L1444:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1445
	movq	(%rdi), %rax
	call	*8(%rax)
.L1445:
	leaq	.LC50(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1470:
	movq	40960(%rsi), %rax
	movl	$592, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1474:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1437
	.p2align 4,,10
	.p2align 3
.L1473:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1476:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC50(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1443
.L1475:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29178:
	.size	_ZN2v88internalL36Stats_Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC51:
	.string	"V8.Runtime_Runtime_HasFixedInt16Elements"
	.section	.text._ZN2v88internalL35Stats_Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29179:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1510
.L1478:
	movq	_ZZN2v88internalL35Stats_Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1511
.L1480:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1512
.L1482:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1486
.L1487:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1511:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1513
.L1481:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip)
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1487
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$20, %eax
	jne	.L1488
	movq	112(%r12), %r12
.L1489:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1514
.L1477:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1515
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1488:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1512:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1516
.L1483:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1484
	movq	(%rdi), %rax
	call	*8(%rax)
.L1484:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1485
	movq	(%rdi), %rax
	call	*8(%rax)
.L1485:
	leaq	.LC51(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1482
	.p2align 4,,10
	.p2align 3
.L1510:
	movq	40960(%rsi), %rax
	movl	$589, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1514:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1513:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1481
	.p2align 4,,10
	.p2align 3
.L1516:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC51(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1483
.L1515:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29179:
	.size	_ZN2v88internalL35Stats_Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC52:
	.string	"V8.Runtime_Runtime_HasFixedUint32Elements"
	.section	.text._ZN2v88internalL36Stats_Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29180:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1550
.L1518:
	movq	_ZZN2v88internalL36Stats_Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1551
.L1520:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1552
.L1522:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1526
.L1527:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1551:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1553
.L1521:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip)
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L1526:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1527
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$21, %eax
	jne	.L1528
	movq	112(%r12), %r12
.L1529:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1554
.L1517:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1555
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1528:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1529
	.p2align 4,,10
	.p2align 3
.L1552:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1556
.L1523:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1524
	movq	(%rdi), %rax
	call	*8(%rax)
.L1524:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1525
	movq	(%rdi), %rax
	call	*8(%rax)
.L1525:
	leaq	.LC52(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	40960(%rsi), %rax
	movl	$593, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1518
	.p2align 4,,10
	.p2align 3
.L1554:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1517
	.p2align 4,,10
	.p2align 3
.L1553:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1521
	.p2align 4,,10
	.p2align 3
.L1556:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC52(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1523
.L1555:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29180:
	.size	_ZN2v88internalL36Stats_Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC53:
	.string	"V8.Runtime_Runtime_HasFixedInt32Elements"
	.section	.text._ZN2v88internalL35Stats_Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29181:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1590
.L1558:
	movq	_ZZN2v88internalL35Stats_Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1591
.L1560:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1592
.L1562:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1566
.L1567:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1591:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1593
.L1561:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip)
	jmp	.L1560
	.p2align 4,,10
	.p2align 3
.L1566:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1567
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$22, %eax
	jne	.L1568
	movq	112(%r12), %r12
.L1569:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1594
.L1557:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1595
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1568:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1592:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1596
.L1563:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1564
	movq	(%rdi), %rax
	call	*8(%rax)
.L1564:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1565
	movq	(%rdi), %rax
	call	*8(%rax)
.L1565:
	leaq	.LC53(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1590:
	movq	40960(%rsi), %rax
	movl	$590, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1558
	.p2align 4,,10
	.p2align 3
.L1594:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1593:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1561
	.p2align 4,,10
	.p2align 3
.L1596:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC53(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1563
.L1595:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29181:
	.size	_ZN2v88internalL35Stats_Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL37Stats_Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC54:
	.string	"V8.Runtime_Runtime_HasFixedFloat32Elements"
	.section	.text._ZN2v88internalL37Stats_Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29182:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1630
.L1598:
	movq	_ZZN2v88internalL37Stats_Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1631
.L1600:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1632
.L1602:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1606
.L1607:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1631:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1633
.L1601:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip)
	jmp	.L1600
	.p2align 4,,10
	.p2align 3
.L1606:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1607
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$23, %eax
	jne	.L1608
	movq	112(%r12), %r12
.L1609:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1634
.L1597:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1635
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1608:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1609
	.p2align 4,,10
	.p2align 3
.L1632:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1636
.L1603:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1604
	movq	(%rdi), %rax
	call	*8(%rax)
.L1604:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1605
	movq	(%rdi), %rax
	call	*8(%rax)
.L1605:
	leaq	.LC54(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L1630:
	movq	40960(%rsi), %rax
	movl	$587, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1634:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1633:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1636:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC54(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1603
.L1635:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29182:
	.size	_ZN2v88internalL37Stats_Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL37Stats_Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC55:
	.string	"V8.Runtime_Runtime_HasFixedFloat64Elements"
	.section	.text._ZN2v88internalL37Stats_Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29183:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1670
.L1638:
	movq	_ZZN2v88internalL37Stats_Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1671
.L1640:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1672
.L1642:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1646
.L1647:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1671:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1673
.L1641:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip)
	jmp	.L1640
	.p2align 4,,10
	.p2align 3
.L1646:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1647
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$24, %eax
	jne	.L1648
	movq	112(%r12), %r12
.L1649:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1674
.L1637:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1675
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1648:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1649
	.p2align 4,,10
	.p2align 3
.L1672:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1676
.L1643:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1644
	movq	(%rdi), %rax
	call	*8(%rax)
.L1644:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1645
	movq	(%rdi), %rax
	call	*8(%rax)
.L1645:
	leaq	.LC55(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1642
	.p2align 4,,10
	.p2align 3
.L1670:
	movq	40960(%rsi), %rax
	movl	$588, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1638
	.p2align 4,,10
	.p2align 3
.L1674:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1637
	.p2align 4,,10
	.p2align 3
.L1673:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1676:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC55(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1643
.L1675:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29183:
	.size	_ZN2v88internalL37Stats_Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL42Stats_Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC56:
	.string	"V8.Runtime_Runtime_HasFixedUint8ClampedElements"
	.section	.text._ZN2v88internalL42Stats_Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Stats_Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL42Stats_Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29184:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1710
.L1678:
	movq	_ZZN2v88internalL42Stats_Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1711
.L1680:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1712
.L1682:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1686
.L1687:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1711:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1713
.L1681:
	movq	%rbx, _ZZN2v88internalL42Stats_Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip)
	jmp	.L1680
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1687
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$25, %eax
	jne	.L1688
	movq	112(%r12), %r12
.L1689:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1714
.L1677:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1715
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1688:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1712:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1716
.L1683:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1684
	movq	(%rdi), %rax
	call	*8(%rax)
.L1684:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1685
	movq	(%rdi), %rax
	call	*8(%rax)
.L1685:
	leaq	.LC56(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1682
	.p2align 4,,10
	.p2align 3
.L1710:
	movq	40960(%rsi), %rax
	movl	$594, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1714:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1677
	.p2align 4,,10
	.p2align 3
.L1713:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1681
	.p2align 4,,10
	.p2align 3
.L1716:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC56(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1683
.L1715:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29184:
	.size	_ZN2v88internalL42Stats_Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL42Stats_Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL39Stats_Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC57:
	.string	"V8.Runtime_Runtime_HasFixedBigUint64Elements"
	.section	.text._ZN2v88internalL39Stats_Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL39Stats_Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29185:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1750
.L1718:
	movq	_ZZN2v88internalL39Stats_Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1751
.L1720:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1752
.L1722:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1726
.L1727:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1751:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1753
.L1721:
	movq	%rbx, _ZZN2v88internalL39Stats_Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip)
	jmp	.L1720
	.p2align 4,,10
	.p2align 3
.L1726:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1727
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$26, %eax
	jne	.L1728
	movq	112(%r12), %r12
.L1729:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1754
.L1717:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1755
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1728:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1752:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1756
.L1723:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1724
	movq	(%rdi), %rax
	call	*8(%rax)
.L1724:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1725
	movq	(%rdi), %rax
	call	*8(%rax)
.L1725:
	leaq	.LC57(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1750:
	movq	40960(%rsi), %rax
	movl	$586, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1718
	.p2align 4,,10
	.p2align 3
.L1754:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1753:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1721
	.p2align 4,,10
	.p2align 3
.L1756:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC57(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1723
.L1755:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29185:
	.size	_ZN2v88internalL39Stats_Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL39Stats_Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL38Stats_Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC58:
	.string	"V8.Runtime_Runtime_HasFixedBigInt64Elements"
	.section	.text._ZN2v88internalL38Stats_Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL38Stats_Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateE.isra.0:
.LFB29186:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1790
.L1758:
	movq	_ZZN2v88internalL38Stats_Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1791
.L1760:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1792
.L1762:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L1766
.L1767:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1791:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1793
.L1761:
	movq	%rbx, _ZZN2v88internalL38Stats_Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096(%rip)
	jmp	.L1760
	.p2align 4,,10
	.p2align 3
.L1766:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L1767
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$27, %eax
	jne	.L1768
	movq	112(%r12), %r12
.L1769:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1794
.L1757:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1795
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1768:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1769
	.p2align 4,,10
	.p2align 3
.L1792:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1796
.L1763:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1764
	movq	(%rdi), %rax
	call	*8(%rax)
.L1764:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1765
	movq	(%rdi), %rax
	call	*8(%rax)
.L1765:
	leaq	.LC58(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1762
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	40960(%rsi), %rax
	movl	$585, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1794:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1757
	.p2align 4,,10
	.p2align 3
.L1793:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1796:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC58(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1763
.L1795:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29186:
	.size	_ZN2v88internalL38Stats_Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL38Stats_Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC59:
	.string	"V8.Runtime_Runtime_ArraySpeciesProtector"
	.section	.text._ZN2v88internalL35Stats_Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateE.isra.0:
.LFB29187:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1831
.L1798:
	movq	_ZZN2v88internalL35Stats_Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1100(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1832
.L1800:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1833
.L1802:
	movq	4520(%r12), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L1806
	sarq	$32, %rax
	cmpq	$1, %rax
	je	.L1834
.L1806:
	movq	120(%r12), %r12
.L1807:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1835
.L1797:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1836
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1834:
	.cfi_restore_state
	movq	112(%r12), %r12
	jmp	.L1807
	.p2align 4,,10
	.p2align 3
.L1832:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1837
.L1801:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1100(%rip)
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1833:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1838
.L1803:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1804
	movq	(%rdi), %rax
	call	*8(%rax)
.L1804:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1805
	movq	(%rdi), %rax
	call	*8(%rax)
.L1805:
	leaq	.LC59(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1802
	.p2align 4,,10
	.p2align 3
.L1835:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1797
	.p2align 4,,10
	.p2align 3
.L1831:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$553, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1798
	.p2align 4,,10
	.p2align 3
.L1838:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC59(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L1837:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1801
.L1836:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29187:
	.size	_ZN2v88internalL35Stats_Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL34Stats_Runtime_MapIteratorProtectorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC60:
	.string	"V8.Runtime_Runtime_MapIteratorProtector"
	.section	.text._ZN2v88internalL34Stats_Runtime_MapIteratorProtectorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_MapIteratorProtectorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_MapIteratorProtectorEiPmPNS0_7IsolateE.isra.0:
.LFB29188:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1870
.L1840:
	movq	_ZZN2v88internalL34Stats_Runtime_MapIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1107(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1871
.L1842:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1872
.L1844:
	movq	4584(%r12), %rdx
	movabsq	$4294967296, %rax
	cmpq	%rax, 23(%rdx)
	jne	.L1848
	movq	112(%r12), %r12
.L1849:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1873
.L1839:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1874
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1848:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1849
	.p2align 4,,10
	.p2align 3
.L1871:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1875
.L1843:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_MapIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1107(%rip)
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1872:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1876
.L1845:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1846
	movq	(%rdi), %rax
	call	*8(%rax)
.L1846:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1847
	movq	(%rdi), %rax
	call	*8(%rax)
.L1847:
	leaq	.LC60(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1844
	.p2align 4,,10
	.p2align 3
.L1873:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1839
	.p2align 4,,10
	.p2align 3
.L1870:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$614, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1840
	.p2align 4,,10
	.p2align 3
.L1876:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC60(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1875:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1843
.L1874:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29188:
	.size	_ZN2v88internalL34Stats_Runtime_MapIteratorProtectorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_MapIteratorProtectorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL34Stats_Runtime_SetIteratorProtectorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC61:
	.string	"V8.Runtime_Runtime_SetIteratorProtector"
	.section	.text._ZN2v88internalL34Stats_Runtime_SetIteratorProtectorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_SetIteratorProtectorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_SetIteratorProtectorEiPmPNS0_7IsolateE.isra.0:
.LFB29189:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1908
.L1878:
	movq	_ZZN2v88internalL34Stats_Runtime_SetIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1113(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1909
.L1880:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1910
.L1882:
	movq	4600(%r12), %rdx
	movabsq	$4294967296, %rax
	cmpq	%rax, 23(%rdx)
	jne	.L1886
	movq	112(%r12), %r12
.L1887:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1911
.L1877:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1912
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1886:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1887
	.p2align 4,,10
	.p2align 3
.L1909:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1913
.L1881:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_SetIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1113(%rip)
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1910:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1914
.L1883:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1884
	movq	(%rdi), %rax
	call	*8(%rax)
.L1884:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1885
	movq	(%rdi), %rax
	call	*8(%rax)
.L1885:
	leaq	.LC61(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1882
	.p2align 4,,10
	.p2align 3
.L1911:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1877
	.p2align 4,,10
	.p2align 3
.L1908:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$626, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1878
	.p2align 4,,10
	.p2align 3
.L1914:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC61(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1883
	.p2align 4,,10
	.p2align 3
.L1913:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1881
.L1912:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29189:
	.size	_ZN2v88internalL34Stats_Runtime_SetIteratorProtectorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_SetIteratorProtectorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL37Stats_Runtime_StringIteratorProtectorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC62:
	.string	"V8.Runtime_Runtime_StringIteratorProtector"
	.section	.text._ZN2v88internalL37Stats_Runtime_StringIteratorProtectorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_StringIteratorProtectorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL37Stats_Runtime_StringIteratorProtectorEiPmPNS0_7IsolateE.isra.0:
.LFB29190:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1946
.L1916:
	movq	_ZZN2v88internalL37Stats_Runtime_StringIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1119(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1947
.L1918:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L1948
.L1920:
	movq	4608(%r12), %rdx
	movabsq	$4294967296, %rax
	cmpq	%rax, 23(%rdx)
	jne	.L1924
	movq	112(%r12), %r12
.L1925:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1949
.L1915:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1950
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1924:
	.cfi_restore_state
	movq	120(%r12), %r12
	jmp	.L1925
	.p2align 4,,10
	.p2align 3
.L1947:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1951
.L1919:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_StringIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1119(%rip)
	jmp	.L1918
	.p2align 4,,10
	.p2align 3
.L1948:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1952
.L1921:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1922
	movq	(%rdi), %rax
	call	*8(%rax)
.L1922:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1923
	movq	(%rdi), %rax
	call	*8(%rax)
.L1923:
	leaq	.LC62(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1920
	.p2align 4,,10
	.p2align 3
.L1949:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1915
	.p2align 4,,10
	.p2align 3
.L1946:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$630, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1916
	.p2align 4,,10
	.p2align 3
.L1952:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC62(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L1921
	.p2align 4,,10
	.p2align 3
.L1951:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1919
.L1950:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29190:
	.size	_ZN2v88internalL37Stats_Runtime_StringIteratorProtectorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL37Stats_Runtime_StringIteratorProtectorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL30Stats_Runtime_HeapObjectVerifyEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC63:
	.string	"V8.Runtime_Runtime_HeapObjectVerify"
	.align 8
.LC64:
	.string	"HeapObject::cast(*object).map().IsMap()"
	.section	.text._ZN2v88internalL30Stats_Runtime_HeapObjectVerifyEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_HeapObjectVerifyEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL30Stats_Runtime_HeapObjectVerifyEiPmPNS0_7IsolateE.isra.0:
.LFB29191:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1986
.L1954:
	movq	_ZZN2v88internalL30Stats_Runtime_HeapObjectVerifyEiPmPNS0_7IsolateEE29trace_event_unique_atomic1191(%rip), %r12
	testq	%r12, %r12
	je	.L1987
.L1956:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L1988
.L1958:
	movl	41104(%rbx), %eax
	leal	1(%rax), %edx
	movl	%edx, 41104(%rbx)
	movq	0(%r13), %rdx
	testb	$1, %dl
	je	.L1962
	movq	-1(%rdx), %rdx
	movq	-1(%rdx), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L1989
.L1962:
	movl	%eax, 41104(%rbx)
	leaq	-144(%rbp), %rdi
	movq	112(%rbx), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1990
.L1953:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1991
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1988:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1992
.L1959:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1960
	movq	(%rdi), %rax
	call	*8(%rax)
.L1960:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1961
	movq	(%rdi), %rax
	call	*8(%rax)
.L1961:
	leaq	.LC63(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L1987:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1993
.L1957:
	movq	%r12, _ZZN2v88internalL30Stats_Runtime_HeapObjectVerifyEiPmPNS0_7IsolateEE29trace_event_unique_atomic1191(%rip)
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L1986:
	movq	40960(%rsi), %rax
	movl	$603, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1954
	.p2align 4,,10
	.p2align 3
.L1990:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1953
	.p2align 4,,10
	.p2align 3
.L1993:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1957
	.p2align 4,,10
	.p2align 3
.L1992:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC63(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1959
	.p2align 4,,10
	.p2align 3
.L1989:
	leaq	.LC64(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1991:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29191:
	.size	_ZN2v88internalL30Stats_Runtime_HeapObjectVerifyEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL30Stats_Runtime_HeapObjectVerifyEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC65:
	.string	"V8.Runtime_Runtime_GetWasmExceptionValues"
	.section	.rodata._ZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC66:
	.string	"args[0].IsJSReceiver()"
.LC67:
	.string	"values_obj->IsFixedArray()"
	.section	.text._ZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE.isra.0:
.LFB29195:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2028
.L1995:
	movq	_ZZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1024(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2029
.L1997:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2030
.L1999:
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L2003
.L2004:
	leaq	.LC66(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2029:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2031
.L1998:
	movq	%rbx, _ZZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1024(%rip)
	jmp	.L1997
	.p2align 4,,10
	.p2align 3
.L2003:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2004
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal20WasmExceptionPackage18GetExceptionValuesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %rsi
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L2032
.L2005:
	leaq	.LC67(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2030:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2033
.L2000:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2001
	movq	(%rdi), %rax
	call	*8(%rax)
.L2001:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2002
	movq	(%rdi), %rax
	call	*8(%rax)
.L2002:
	leaq	.LC65(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L1999
	.p2align 4,,10
	.p2align 3
.L2032:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	subl	$123, %edx
	cmpw	$14, %dx
	ja	.L2005
	movslq	11(%rax), %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2007
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2007:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2034
.L1994:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2035
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2028:
	.cfi_restore_state
	movq	40960(%rsi), %rax
	movl	$577, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L2034:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1994
	.p2align 4,,10
	.p2align 3
.L2033:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC65(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2000
	.p2align 4,,10
	.p2align 3
.L2031:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1998
.L2035:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29195:
	.size	_ZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC68:
	.string	"V8.Runtime_Runtime_WasmGetNumberOfInstances"
	.section	.rodata._ZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC69:
	.string	"args[0].IsWasmModuleObject()"
	.section	.text._ZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE, @function
_ZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE:
.LFB22684:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2074
.L2037:
	movq	_ZZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1208(%rip), %r12
	testq	%r12, %r12
	je	.L2075
.L2039:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L2076
.L2041:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L2045
.L2046:
	leaq	.LC69(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2075:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2077
.L2040:
	movq	%r12, _ZZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1208(%rip)
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2045:
	movq	-1(%rax), %rdx
	cmpw	$1102, 11(%rdx)
	jne	.L2046
	movq	47(%rax), %rdx
	movl	19(%rdx), %eax
	leaq	15(%rdx), %rsi
	testl	%eax, %eax
	jle	.L2056
	movq	(%rsi), %r8
	addq	$23, %rdx
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	sarq	$32, %r8
	.p2align 4,,10
	.p2align 3
.L2051:
	movq	(%rdx), %rsi
	movq	%rsi, %rdi
	andl	$3, %edi
	cmpq	$3, %rdi
	jne	.L2049
	cmpl	$3, %esi
	setne	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
.L2049:
	addl	$1, %ecx
	addq	$8, %rdx
	cmpl	%r8d, %ecx
	jl	.L2051
	movq	%rax, %r12
	salq	$32, %r12
.L2047:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2078
.L2036:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2079
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2076:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2080
.L2042:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2043
	movq	(%rdi), %rax
	call	*8(%rax)
.L2043:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2044
	movq	(%rdi), %rax
	call	*8(%rax)
.L2044:
	leaq	.LC68(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2041
	.p2align 4,,10
	.p2align 3
.L2074:
	movq	40960(%rdx), %rdi
	leaq	-104(%rbp), %rsi
	movl	$636, %edx
	addq	$23240, %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2037
	.p2align 4,,10
	.p2align 3
.L2056:
	xorl	%r12d, %r12d
	jmp	.L2047
	.p2align 4,,10
	.p2align 3
.L2078:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2036
.L2080:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC68(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2042
.L2077:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L2040
.L2079:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22684:
	.size	_ZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE, .-_ZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Stats_Runtime_CloneWasmModuleEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC70:
	.string	"V8.Runtime_Runtime_CloneWasmModule"
	.section	.text._ZN2v88internalL29Stats_Runtime_CloneWasmModuleEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_CloneWasmModuleEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Stats_Runtime_CloneWasmModuleEiPmPNS0_7IsolateE:
.LFB22672:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2139
.L2082:
	movq	_ZZN2v88internalL29Stats_Runtime_CloneWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1179(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2140
.L2084:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2141
.L2086:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2090
.L2091:
	leaq	.LC69(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2140:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2142
.L2085:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_CloneWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1179(%rip)
	jmp	.L2084
	.p2align 4,,10
	.p2align 3
.L2090:
	movq	-1(%rax), %rax
	cmpw	$1102, 11(%rax)
	jne	.L2091
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal4wasm10WasmEngine13GetWasmEngineEv@PLT
	movq	0(%r13), %rax
	movq	-176(%rbp), %rdi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	%rdx, -192(%rbp)
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L2092
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2093
	lock addl	$1, 8(%rax)
.L2092:
	leaq	-192(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10WasmEngine18ImportNativeModuleEPNS0_7IsolateESt10shared_ptrINS1_12NativeModuleEE@PLT
	movq	-184(%rbp), %r13
	testq	%r13, %r13
	je	.L2095
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L2096
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r13)
	cmpl	$1, %edx
	je	.L2143
	.p2align 4,,10
	.p2align 3
.L2095:
	movq	-168(%rbp), %r13
	testq	%r13, %r13
	je	.L2110
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L2104
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r13)
	cmpl	$1, %edx
	je	.L2144
	.p2align 4,,10
	.p2align 3
.L2110:
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2113
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2113:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2145
.L2081:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2146
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2141:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2147
.L2087:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2088
	movq	(%rdi), %rax
	call	*8(%rax)
.L2088:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2089
	movq	(%rdi), %rax
	call	*8(%rax)
.L2089:
	leaq	.LC70(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L2086
	.p2align 4,,10
	.p2align 3
.L2093:
	addl	$1, 8(%rax)
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2096:
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %edx
	jne	.L2095
.L2143:
	movq	0(%r13), %rdx
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rcx
	movq	16(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L2148
.L2099:
	testq	%r15, %r15
	je	.L2100
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r13)
.L2101:
	cmpl	$1, %edx
	jne	.L2095
	movq	0(%r13), %rdx
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rsi
	movq	%rax, -200(%rbp)
	movq	%r13, %rdi
	movq	24(%rdx), %rcx
	cmpq	%rsi, %rcx
	jne	.L2102
	call	*8(%rdx)
	movq	-200(%rbp), %rax
	jmp	.L2095
	.p2align 4,,10
	.p2align 3
.L2104:
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %edx
	jne	.L2110
.L2144:
	movq	0(%r13), %rdx
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rcx
	movq	16(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L2149
.L2106:
	testq	%r15, %r15
	je	.L2107
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r13)
.L2108:
	cmpl	$1, %edx
	jne	.L2110
	movq	0(%r13), %rdx
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rsi
	movq	%rax, -200(%rbp)
	movq	%r13, %rdi
	movq	24(%rdx), %rcx
	cmpq	%rsi, %rcx
	jne	.L2109
	call	*8(%rdx)
	movq	-200(%rbp), %rax
	jmp	.L2110
	.p2align 4,,10
	.p2align 3
.L2139:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$556, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2145:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2100:
	movl	12(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r13)
	jmp	.L2101
	.p2align 4,,10
	.p2align 3
.L2107:
	movl	12(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r13)
	jmp	.L2108
	.p2align 4,,10
	.p2align 3
.L2142:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2085
	.p2align 4,,10
	.p2align 3
.L2147:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC70(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2087
	.p2align 4,,10
	.p2align 3
.L2102:
	call	*%rcx
	movq	-200(%rbp), %rax
	jmp	.L2095
	.p2align 4,,10
	.p2align 3
.L2109:
	call	*%rcx
	movq	-200(%rbp), %rax
	jmp	.L2110
	.p2align 4,,10
	.p2align 3
.L2148:
	movq	%rax, -200(%rbp)
	movq	%r13, %rdi
	call	*%rdx
	movq	-200(%rbp), %rax
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2149:
	movq	%rax, -200(%rbp)
	movq	%r13, %rdi
	call	*%rdx
	movq	-200(%rbp), %rax
	jmp	.L2106
.L2146:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22672:
	.size	_ZN2v88internalL29Stats_Runtime_CloneWasmModuleEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Stats_Runtime_CloneWasmModuleEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Stats_Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC71:
	.string	"V8.Runtime_Runtime_ClearMegamorphicStubCache"
	.section	.text._ZN2v88internalL39Stats_Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL39Stats_Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE.isra.0:
.LFB29199:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2181
.L2151:
	movq	_ZZN2v88internalL39Stats_Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateEE28trace_event_unique_atomic116(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2182
.L2153:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2183
.L2155:
	movq	41024(%r12), %rdi
	movq	41088(%r12), %r14
	addl	$1, 41104(%r12)
	movq	41096(%r12), %rbx
	call	_ZN2v88internal9StubCache5ClearEv@PLT
	movq	41032(%r12), %rdi
	call	_ZN2v88internal9StubCache5ClearEv@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2161
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2161:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2184
.L2150:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2185
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2183:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2186
.L2156:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2157
	movq	(%rdi), %rax
	call	*8(%rax)
.L2157:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2158
	movq	(%rdi), %rax
	call	*8(%rax)
.L2158:
	leaq	.LC71(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2155
	.p2align 4,,10
	.p2align 3
.L2182:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2187
.L2154:
	movq	%rbx, _ZZN2v88internalL39Stats_Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateEE28trace_event_unique_atomic116(%rip)
	jmp	.L2153
	.p2align 4,,10
	.p2align 3
.L2181:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$555, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2151
	.p2align 4,,10
	.p2align 3
.L2184:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2187:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2154
	.p2align 4,,10
	.p2align 3
.L2186:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC71(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2156
.L2185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29199:
	.size	_ZN2v88internalL39Stats_Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL39Stats_Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC72:
	.string	"V8.Runtime_Runtime_DeoptimizeFunction"
	.section	.text._ZN2v88internalL32Stats_Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateE.isra.0:
.LFB29200:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2231
.L2189:
	movq	_ZZN2v88internalL32Stats_Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic162(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2232
.L2191:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2233
.L2193:
	movl	41104(%r12), %eax
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L2197
.L2230:
	movq	88(%r12), %r13
	movq	%rbx, %rdx
.L2207:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L2210
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2210:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2234
.L2188:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2235
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2197:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L2230
	movq	47(%rdx), %rax
	cmpl	$67, 59(%rax)
	je	.L2205
	movabsq	$287762808832, %rcx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2205
	testb	$1, %al
	jne	.L2236
.L2202:
	movq	47(%rdx), %rax
	testb	$62, 43(%rax)
	jne	.L2205
	movq	47(%rdx), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	jne	.L2205
	movq	0(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE@PLT
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L2207
	.p2align 4,,10
	.p2align 3
.L2233:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2237
.L2194:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2195
	movq	(%rdi), %rax
	call	*8(%rax)
.L2195:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2196
	movq	(%rdi), %rax
	call	*8(%rax)
.L2196:
	leaq	.LC72(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2193
	.p2align 4,,10
	.p2align 3
.L2232:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2238
.L2192:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic162(%rip)
	jmp	.L2191
	.p2align 4,,10
	.p2align 3
.L2236:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L2205
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L2202
	.p2align 4,,10
	.p2align 3
.L2205:
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L2207
	.p2align 4,,10
	.p2align 3
.L2234:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2231:
	movq	40960(%rsi), %rax
	movl	$564, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2189
	.p2align 4,,10
	.p2align 3
.L2238:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2237:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC72(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2194
.L2235:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29200:
	.size	_ZN2v88internalL32Stats_Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL27Stats_Runtime_DeoptimizeNowEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC73:
	.string	"V8.Runtime_Runtime_DeoptimizeNow"
	.section	.text._ZN2v88internalL27Stats_Runtime_DeoptimizeNowEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_DeoptimizeNowEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_DeoptimizeNowEiPmPNS0_7IsolateE.isra.0:
.LFB29201:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1568, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1552(%rbp)
	movq	$0, -1520(%rbp)
	movaps	%xmm0, -1536(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2291
.L2240:
	movq	_ZZN2v88internalL27Stats_Runtime_DeoptimizeNowEiPmPNS0_7IsolateEE28trace_event_unique_atomic182(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2292
.L2242:
	movq	$0, -1584(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2293
.L2244:
	addl	$1, 41104(%r12)
	leaq	-1504(%rbp), %r14
	movq	%r12, %rsi
	movq	41088(%r12), %r13
	movq	%r14, %rdi
	movq	41096(%r12), %rbx
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L2290
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2290
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2249
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	testq	%rax, %rax
	jne	.L2294
	.p2align 4,,10
	.p2align 3
.L2290:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r14
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2263
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2263:
	leaq	-1584(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1552(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2295
.L2239:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2296
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2249:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2297
.L2251:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
.L2252:
	movq	47(%rsi), %rdx
	leaq	47(%rsi), %rcx
	cmpl	$67, 59(%rdx)
	je	.L2290
	movq	23(%rsi), %rdx
	movabsq	$287762808832, %rsi
	movq	7(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L2290
	testb	$1, %dl
	jne	.L2298
.L2256:
	movq	(%rcx), %rdx
	testb	$62, 43(%rdx)
	jne	.L2290
	movq	(%rcx), %rdx
	movq	31(%rdx), %rdx
	movl	15(%rdx), %edx
	andl	$1, %edx
	jne	.L2290
	movq	(%rax), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE@PLT
	jmp	.L2290
	.p2align 4,,10
	.p2align 3
.L2293:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2299
.L2245:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2246
	movq	(%rdi), %rax
	call	*8(%rax)
.L2246:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2247
	movq	(%rdi), %rax
	call	*8(%rax)
.L2247:
	leaq	.LC73(%rip), %rax
	movq	%rbx, -1576(%rbp)
	movq	%rax, -1568(%rbp)
	leaq	-1576(%rbp), %rax
	movq	%r13, -1560(%rbp)
	movq	%rax, -1584(%rbp)
	jmp	.L2244
	.p2align 4,,10
	.p2align 3
.L2292:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2300
.L2243:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_DeoptimizeNowEiPmPNS0_7IsolateEE28trace_event_unique_atomic182(%rip)
	jmp	.L2242
	.p2align 4,,10
	.p2align 3
.L2294:
	movq	(%rax), %rsi
	jmp	.L2252
	.p2align 4,,10
	.p2align 3
.L2291:
	movq	40960(%rdi), %rax
	leaq	-1544(%rbp), %rsi
	movl	$640, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -1552(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2240
	.p2align 4,,10
	.p2align 3
.L2295:
	leaq	-1544(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2239
	.p2align 4,,10
	.p2align 3
.L2300:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2243
	.p2align 4,,10
	.p2align 3
.L2299:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC73(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2245
	.p2align 4,,10
	.p2align 3
.L2297:
	movq	%r12, %rdi
	movq	%rsi, -1592(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1592(%rbp), %rsi
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2298:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L2290
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L2256
	jmp	.L2290
.L2296:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29201:
	.size	_ZN2v88internalL27Stats_Runtime_DeoptimizeNowEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_DeoptimizeNowEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL45Stats_Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC74:
	.string	"V8.Runtime_Runtime_EnsureFeedbackVectorForFunction"
	.section	.text._ZN2v88internalL45Stats_Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Stats_Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL45Stats_Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateE.isra.0:
.LFB29202:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2340
.L2302:
	movq	_ZZN2v88internalL45Stats_Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic341(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2341
.L2304:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2342
.L2306:
	movl	41104(%r12), %eax
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L2310
.L2339:
	movq	88(%r12), %r13
	movq	%rbx, %rdx
.L2314:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L2318
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2318:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2343
.L2301:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2344
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2310:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L2339
	movq	-1(%rdx), %rax
	cmpw	$1105, 11(%rax)
	je	.L2337
	leaq	.LC34(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2342:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2345
.L2307:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2308
	movq	(%rdi), %rax
	call	*8(%rax)
.L2308:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2309
	movq	(%rdi), %rax
	call	*8(%rax)
.L2309:
	leaq	.LC74(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2306
	.p2align 4,,10
	.p2align 3
.L2341:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2346
.L2305:
	movq	%rbx, _ZZN2v88internalL45Stats_Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic341(%rip)
	jmp	.L2304
	.p2align 4,,10
	.p2align 3
.L2337:
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120EnsureFeedbackVectorENS0_6HandleINS0_10JSFunctionEEE
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2340:
	movq	40960(%rsi), %rax
	movl	$570, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2302
	.p2align 4,,10
	.p2align 3
.L2343:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2301
	.p2align 4,,10
	.p2align 3
.L2346:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2305
	.p2align 4,,10
	.p2align 3
.L2345:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC74(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2307
.L2344:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29202:
	.size	_ZN2v88internalL45Stats_Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL45Stats_Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC75:
	.string	"V8.Runtime_Runtime_OptimizeOsr"
	.section	.rodata._ZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC76:
	.string	"[OSR - OptimizeOsr marking "
	.section	.rodata._ZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateE.str1.8
	.align 8
.LC77:
	.string	" for non-concurrent optimization]\n"
	.section	.text._ZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateE, @function
_ZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateE:
.LFB22462:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1592, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -1624(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2441
.L2348:
	movq	_ZZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateEE28trace_event_unique_atomic399(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2442
.L2350:
	movq	$0, -1600(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2443
.L2352:
	addl	$1, 41104(%r12)
	movq	41088(%r12), %r15
	xorl	%ebx, %ebx
	movq	41096(%r12), %r14
	cmpl	$1, %r13d
	je	.L2444
.L2356:
	leaq	-1520(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -104(%rbp)
	je	.L2385
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2385
	testl	%ebx, %ebx
	jne	.L2359
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2438:
	subl	$1, %ebx
	je	.L2358
.L2359:
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2438
.L2385:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L2388
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2388:
	leaq	-1600(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2445
.L2347:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2446
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2358:
	.cfi_restore_state
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2447
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L2361
	jmp	.L2385
	.p2align 4,,10
	.p2align 3
.L2447:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L2448
.L2362:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
.L2361:
	cmpb	$0, _ZN2v88internal8FLAG_optE(%rip)
	je	.L2385
	movq	0(%r13), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$15728640, %edx
	jne	.L2449
.L2365:
	cmpb	$0, _ZN2v88internal27FLAG_testing_d8_test_runnerE(%rip)
	jne	.L2450
.L2367:
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	jne	.L2451
.L2370:
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L2382
	testb	$1, %dl
	jne	.L2376
.L2379:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	jne	.L2382
	movq	15(%rax), %rax
	testb	$1, %al
	je	.L2382
	cmpl	$3, %eax
	je	.L2382
	andq	$-3, %rax
	je	.L2382
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	jne	.L2382
.L2374:
	cmpb	$0, _ZN2v88internal27FLAG_testing_d8_test_runnerE(%rip)
	je	.L2385
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PendingOptimizationTable20FunctionWasOptimizedEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE@PLT
	jmp	.L2385
	.p2align 4,,10
	.p2align 3
.L2443:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2452
.L2353:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2354
	movq	(%rdi), %rax
	call	*8(%rax)
.L2354:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2355
	movq	(%rdi), %rax
	call	*8(%rax)
.L2355:
	leaq	.LC75(%rip), %rax
	movq	%rbx, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%r14, -1576(%rbp)
	movq	%rax, -1600(%rbp)
	jmp	.L2352
	.p2align 4,,10
	.p2align 3
.L2442:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2453
.L2351:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateEE28trace_event_unique_atomic399(%rip)
	jmp	.L2350
	.p2align 4,,10
	.p2align 3
.L2444:
	movq	-1624(%rbp), %rax
	movq	(%rax), %rbx
	shrq	$32, %rbx
	jmp	.L2356
.L2449:
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	shrl	$20, %edx
	andl	$15, %edx
	cmpb	$11, %dl
	jne	.L2365
	jmp	.L2385
.L2376:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L2382
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L2379
.L2382:
	cmpb	$0, _ZN2v88internal14FLAG_trace_osrE(%rip)
	leaq	-1608(%rbp), %rbx
	jne	.L2454
.L2384:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -1608(%rbp)
	call	_ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE@PLT
	movq	-104(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$12, %eax
	jne	.L2385
	movq	40944(%r12), %rdi
	movq	-104(%rbp), %rsi
	movl	$6, %edx
	call	_ZN2v88internal15RuntimeProfiler25AttemptOnStackReplacementEPNS0_16InterpretedFrameEi@PLT
	jmp	.L2385
	.p2align 4,,10
	.p2align 3
.L2441:
	movq	40960(%rdx), %rax
	leaq	-1560(%rbp), %rsi
	movl	$618, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -1568(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L2445:
	leaq	-1560(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2347
.L2451:
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L2370
	testb	$1, %dl
	jne	.L2455
.L2371:
	movq	47(%rax), %rdx
	testb	$62, 43(%rdx)
	jne	.L2370
	movq	47(%rax), %rdx
	movq	31(%rdx), %rdx
	movl	15(%rdx), %edx
	andl	$1, %edx
	je	.L2374
	jmp	.L2370
.L2453:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2351
.L2452:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC75(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2353
.L2450:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PendingOptimizationTable21MarkedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	0(%r13), %rax
	jmp	.L2367
.L2448:
	movq	%r12, %rdi
	movq	%rsi, -1624(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1624(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2362
.L2455:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L2370
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L2371
	jmp	.L2370
.L2454:
	xorl	%eax, %eax
	leaq	.LC76(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	0(%r13), %rax
	movq	stdout(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, -1608(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC77(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2384
.L2446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22462:
	.size	_ZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateE, .-_ZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Stats_Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC78:
	.string	"V8.Runtime_Runtime_NeverOptimizeFunction"
	.section	.text._ZN2v88internalL35Stats_Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateE.isra.0:
.LFB29203:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2492
.L2457:
	movq	_ZZN2v88internalL35Stats_Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic455(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2493
.L2459:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2494
.L2461:
	movl	41104(%r12), %eax
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L2465
.L2491:
	movq	88(%r12), %r13
	movq	%rbx, %rdx
.L2468:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L2471
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2471:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2495
.L2456:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2496
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2465:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L2491
	movq	23(%rdx), %rax
	leaq	-152(%rbp), %rdi
	movl	$11, %esi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo19DisableOptimizationENS0_13BailoutReasonE@PLT
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L2468
	.p2align 4,,10
	.p2align 3
.L2494:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2497
.L2462:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2463
	movq	(%rdi), %rax
	call	*8(%rax)
.L2463:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2464
	movq	(%rdi), %rax
	call	*8(%rax)
.L2464:
	leaq	.LC78(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2461
	.p2align 4,,10
	.p2align 3
.L2493:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2498
.L2460:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic455(%rip)
	jmp	.L2459
	.p2align 4,,10
	.p2align 3
.L2495:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L2492:
	movq	40960(%rsi), %rax
	movl	$615, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2457
	.p2align 4,,10
	.p2align 3
.L2498:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2460
	.p2align 4,,10
	.p2align 3
.L2497:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC78(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2462
.L2496:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29203:
	.size	_ZN2v88internalL35Stats_Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL44Stats_Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC79:
	.string	"V8.Runtime_Runtime_UnblockConcurrentRecompilation"
	.section	.text._ZN2v88internalL44Stats_Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Stats_Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL44Stats_Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateE.isra.0:
.LFB29204:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2532
.L2500:
	movq	_ZZN2v88internalL44Stats_Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateEE28trace_event_unique_atomic561(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2533
.L2502:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2534
.L2504:
	cmpb	$0, _ZN2v88internal35FLAG_block_concurrent_recompilationE(%rip)
	jne	.L2535
.L2508:
	leaq	-144(%rbp), %rdi
	movq	88(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2536
.L2499:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2537
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2533:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2538
.L2503:
	movq	%rbx, _ZZN2v88internalL44Stats_Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateEE28trace_event_unique_atomic561(%rip)
	jmp	.L2502
	.p2align 4,,10
	.p2align 3
.L2535:
	movq	45416(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2508
	call	_ZN2v88internal27OptimizingCompileDispatcher7UnblockEv@PLT
	jmp	.L2508
	.p2align 4,,10
	.p2align 3
.L2534:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2539
.L2505:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2506
	movq	(%rdi), %rax
	call	*8(%rax)
.L2506:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2507
	movq	(%rdi), %rax
	call	*8(%rax)
.L2507:
	leaq	.LC79(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2504
	.p2align 4,,10
	.p2align 3
.L2536:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2499
	.p2align 4,,10
	.p2align 3
.L2532:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$635, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2500
	.p2align 4,,10
	.p2align 3
.L2539:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC79(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2505
	.p2align 4,,10
	.p2align 3
.L2538:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2503
.L2537:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29204:
	.size	_ZN2v88internalL44Stats_Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL44Stats_Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC80:
	.string	"V8.Runtime_Runtime_ClearFunctionFeedback"
	.section	.text._ZN2v88internalL35Stats_Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateE.isra.0:
.LFB29205:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2579
.L2541:
	movq	_ZZN2v88internalL35Stats_Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateEE28trace_event_unique_atomic615(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2580
.L2543:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2581
.L2545:
	movl	41104(%r12), %eax
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L2549
.L2578:
	movq	88(%r12), %r13
	movq	%rbx, %rdx
.L2553:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L2557
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2557:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2582
.L2540:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2583
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2549:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L2578
	movq	-1(%rdx), %rax
	cmpw	$1105, 11(%rax)
	je	.L2576
	leaq	.LC34(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2581:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2584
.L2546:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2547
	movq	(%rdi), %rax
	call	*8(%rax)
.L2547:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2548
	movq	(%rdi), %rax
	call	*8(%rax)
.L2548:
	leaq	.LC80(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2545
	.p2align 4,,10
	.p2align 3
.L2580:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2585
.L2544:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateEE28trace_event_unique_atomic615(%rip)
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2576:
	leaq	-152(%rbp), %rdi
	movq	%rdx, -152(%rbp)
	call	_ZN2v88internal10JSFunction21ClearTypeFeedbackInfoEv@PLT
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L2553
	.p2align 4,,10
	.p2align 3
.L2579:
	movq	40960(%rsi), %rax
	movl	$554, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2541
	.p2align 4,,10
	.p2align 3
.L2582:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2540
	.p2align 4,,10
	.p2align 3
.L2585:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2544
	.p2align 4,,10
	.p2align 3
.L2584:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC80(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2546
.L2583:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29205:
	.size	_ZN2v88internalL35Stats_Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC81:
	.string	"V8.Runtime_Runtime_SetWasmInstantiateControls"
	.section	.rodata._ZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC82:
	.string	"args.length() == 0"
	.section	.text._ZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE.isra.0:
.LFB29206:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2617
.L2587:
	movq	_ZZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateEE28trace_event_unique_atomic640(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2618
.L2589:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2619
.L2591:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	testl	%r13d, %r13d
	jne	.L2620
	leaq	_ZN2v88internal12_GLOBAL__N_120WasmInstanceOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate23SetWasmInstanceCallbackEPFbRKNS_20FunctionCallbackInfoINS_5ValueEEEE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2596
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2596:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2621
.L2586:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2622
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2619:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2623
.L2592:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2593
	movq	(%rdi), %rax
	call	*8(%rax)
.L2593:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2594
	movq	(%rdi), %rax
	call	*8(%rax)
.L2594:
	leaq	.LC81(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2591
	.p2align 4,,10
	.p2align 3
.L2618:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2624
.L2590:
	movq	%rbx, _ZZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateEE28trace_event_unique_atomic640(%rip)
	jmp	.L2589
	.p2align 4,,10
	.p2align 3
.L2620:
	leaq	.LC82(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2617:
	movq	40960(%rsi), %rax
	movl	$628, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2587
	.p2align 4,,10
	.p2align 3
.L2621:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2586
	.p2align 4,,10
	.p2align 3
.L2624:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2590
	.p2align 4,,10
	.p2align 3
.L2623:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC81(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2592
.L2622:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29206:
	.size	_ZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_NotifyContextDisposedEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC83:
	.string	"V8.Runtime_Runtime_NotifyContextDisposed"
	.section	.text._ZN2v88internalL35Stats_Runtime_NotifyContextDisposedEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_NotifyContextDisposedEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_NotifyContextDisposedEiPmPNS0_7IsolateE.isra.0:
.LFB29207:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2656
.L2626:
	movq	_ZZN2v88internalL35Stats_Runtime_NotifyContextDisposedEiPmPNS0_7IsolateEE28trace_event_unique_atomic648(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2657
.L2628:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2658
.L2630:
	addl	$1, 41104(%r12)
	movq	41088(%r12), %r14
	leaq	37592(%r12), %rdi
	movl	$1, %esi
	movq	41096(%r12), %rbx
	call	_ZN2v88internal4Heap21NotifyContextDisposedEb@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2636
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2636:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2659
.L2625:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2660
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2658:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2661
.L2631:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2632
	movq	(%rdi), %rax
	call	*8(%rax)
.L2632:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2633
	movq	(%rdi), %rax
	call	*8(%rax)
.L2633:
	leaq	.LC83(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2630
	.p2align 4,,10
	.p2align 3
.L2657:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2662
.L2629:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_NotifyContextDisposedEiPmPNS0_7IsolateEE28trace_event_unique_atomic648(%rip)
	jmp	.L2628
	.p2align 4,,10
	.p2align 3
.L2656:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$616, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2626
	.p2align 4,,10
	.p2align 3
.L2659:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2625
	.p2align 4,,10
	.p2align 3
.L2662:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2629
	.p2align 4,,10
	.p2align 3
.L2661:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC83(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2631
.L2660:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29207:
	.size	_ZN2v88internalL35Stats_Runtime_NotifyContextDisposedEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_NotifyContextDisposedEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL34Stats_Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC84:
	.string	"V8.Runtime_Runtime_SetAllocationTimeout"
	.section	.text._ZN2v88internalL34Stats_Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateE.isra.0:
.LFB29208:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2692
.L2664:
	movq	_ZZN2v88internalL34Stats_Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateEE28trace_event_unique_atomic656(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2693
.L2666:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2694
.L2668:
	leaq	-144(%rbp), %rdi
	movq	88(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2695
.L2663:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2696
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2693:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2697
.L2667:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateEE28trace_event_unique_atomic656(%rip)
	jmp	.L2666
	.p2align 4,,10
	.p2align 3
.L2694:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2698
.L2669:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2670
	movq	(%rdi), %rax
	call	*8(%rax)
.L2670:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2671
	movq	(%rdi), %rax
	call	*8(%rax)
.L2671:
	leaq	.LC84(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2668
	.p2align 4,,10
	.p2align 3
.L2695:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2663
	.p2align 4,,10
	.p2align 3
.L2692:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$624, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2664
	.p2align 4,,10
	.p2align 3
.L2698:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC84(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2669
	.p2align 4,,10
	.p2align 3
.L2697:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2667
.L2696:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29208:
	.size	_ZN2v88internalL34Stats_Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC85:
	.string	"V8.Runtime_Runtime_PrintWithNameForAssert"
	.section	.rodata._ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC86:
	.string	" * "
.LC87:
	.string	": "
	.section	.text._ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0:
.LFB29209:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$440, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -416(%rbp)
	movq	$0, -384(%rbp)
	movaps	%xmm0, -400(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2803
.L2700:
	movq	_ZZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic728(%rip), %rax
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2804
.L2702:
	movq	$0, -448(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L2805
.L2704:
	movq	(%rbx), %r15
	testb	$1, %r15b
	jne	.L2708
.L2709:
	leaq	.LC18(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2804:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2806
.L2703:
	movq	%r13, _ZZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic728(%rip)
	jmp	.L2702
	.p2align 4,,10
	.p2align 3
.L2708:
	movq	-1(%r15), %rax
	cmpw	$63, 11(%rax)
	ja	.L2709
	leaq	.LC86(%rip), %rdi
	xorl	%eax, %eax
	leaq	-368(%rbp), %r14
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	$32, %ecx
	xorl	%eax, %eax
	movq	%r14, %rdi
	rep stosq
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	leaq	.L2713(%rip), %rdx
	movb	$0, -88(%rbp)
	movl	$0, -464(%rbp)
	movaps	%xmm0, -80(%rbp)
	movslq	11(%r15), %r13
.L2710:
	movq	-1(%r15), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L2711
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0,"a",@progbits
	.align 4
	.align 4
.L2713:
	.long	.L2719-.L2713
	.long	.L2716-.L2713
	.long	.L2718-.L2713
	.long	.L2714-.L2713
	.long	.L2711-.L2713
	.long	.L2712-.L2713
	.long	.L2711-.L2713
	.long	.L2711-.L2713
	.long	.L2717-.L2713
	.long	.L2716-.L2713
	.long	.L2715-.L2713
	.long	.L2714-.L2713
	.long	.L2711-.L2713
	.long	.L2712-.L2713
	.section	.text._ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2716:
	movl	$0, -104(%rbp)
	movl	-464(%rbp), %edx
	testq	%r15, %r15
	je	.L2733
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi@PLT
	movl	-104(%rbp), %esi
	movl	$0, -464(%rbp)
	testl	%esi, %esi
	jne	.L2726
.L2733:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
.L2725:
	leaq	.L2746(%rip), %r13
	leaq	.L2765(%rip), %r15
	jmp	.L2778
	.p2align 4,,10
	.p2align 3
.L2801:
	movzbl	-88(%rbp), %edx
.L2742:
	testb	%dl, %dl
	je	.L2773
.L2772:
	leaq	1(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzbl	(%rax), %esi
.L2777:
	xorl	%eax, %eax
	leaq	.LC20(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
.L2778:
	cmpq	%rdx, %rax
	jne	.L2801
	movl	$0, -460(%rbp)
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L2807
.L2743:
	leaq	.LC87(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-8(%rbx), %rax
	movq	stdout(%rip), %rsi
	leaq	-456(%rbp), %rdi
	movq	%rax, -456(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	-448(%rbp), %rdi
	movq	88(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-416(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2808
.L2699:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2809
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2771:
	.cfi_restore_state
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L2773:
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %esi
	jmp	.L2777
	.p2align 4,,10
	.p2align 3
.L2807:
	leaq	-460(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L2743
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L2744:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L2711
	movzwl	%dx, %edx
	movslq	0(%r13,%rdx,4), %rdx
	addq	%r13, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0
	.align 4
	.align 4
.L2746:
	.long	.L2752-.L2746
	.long	.L2749-.L2746
	.long	.L2751-.L2746
	.long	.L2747-.L2746
	.long	.L2711-.L2746
	.long	.L2745-.L2746
	.long	.L2711-.L2746
	.long	.L2711-.L2746
	.long	.L2750-.L2746
	.long	.L2749-.L2746
	.long	.L2748-.L2746
	.long	.L2747-.L2746
	.long	.L2711-.L2746
	.long	.L2745-.L2746
	.section	.text._ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2745:
	movq	15(%rax), %rax
	jmp	.L2744
	.p2align 4,,10
	.p2align 3
.L2747:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L2744
	.p2align 4,,10
	.p2align 3
.L2749:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L2753:
	cmpq	%rcx, %rax
	jne	.L2801
	movl	$0, -456(%rbp)
	movl	-104(%rbp), %edx
	testl	%edx, %edx
	je	.L2801
	leaq	-456(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L2810
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L2763:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L2711
	movzwl	%dx, %edx
	movslq	(%r15,%rdx,4), %rdx
	addq	%r15, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0
	.align 4
	.align 4
.L2765:
	.long	.L2771-.L2765
	.long	.L2768-.L2765
	.long	.L2770-.L2765
	.long	.L2766-.L2765
	.long	.L2711-.L2765
	.long	.L2764-.L2765
	.long	.L2711-.L2765
	.long	.L2711-.L2765
	.long	.L2769-.L2765
	.long	.L2768-.L2765
	.long	.L2767-.L2765
	.long	.L2766-.L2765
	.long	.L2711-.L2765
	.long	.L2764-.L2765
	.section	.text._ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2751:
	movq	15(%rax), %rdi
	movl	%esi, -472(%rbp)
	movl	%ecx, -468(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-472(%rbp), %rsi
	movslq	-468(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2752:
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2748:
	movq	15(%rax), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2754
	movq	8(%rdi), %rax
.L2755:
	movslq	%esi, %rsi
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2750:
	movslq	%esi, %rsi
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2805:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -368(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2811
.L2705:
	movq	-360(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2706
	movq	(%rdi), %rax
	call	*8(%rax)
.L2706:
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2707
	movq	(%rdi), %rax
	call	*8(%rax)
.L2707:
	leaq	.LC85(%rip), %rax
	movq	%r13, -440(%rbp)
	movq	%rax, -432(%rbp)
	leaq	-440(%rbp), %rax
	movq	%r14, -424(%rbp)
	movq	%rax, -448(%rbp)
	jmp	.L2704
	.p2align 4,,10
	.p2align 3
.L2712:
	movq	15(%r15), %r15
	jmp	.L2710
	.p2align 4,,10
	.p2align 3
.L2714:
	addl	27(%r15), %ecx
	movq	15(%r15), %r15
	jmp	.L2710
	.p2align 4,,10
	.p2align 3
.L2764:
	movq	15(%rax), %rax
	jmp	.L2763
	.p2align 4,,10
	.p2align 3
.L2766:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L2763
	.p2align 4,,10
	.p2align 3
.L2768:
	movzbl	-88(%rbp), %edx
	movq	-80(%rbp), %rax
	jmp	.L2742
	.p2align 4,,10
	.p2align 3
.L2718:
	movq	15(%r15), %rdi
	movl	%ecx, -468(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-468(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rcx,2), %rax
	leaq	(%rax,%r13,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
.L2720:
	movl	$0, -104(%rbp)
	jmp	.L2725
	.p2align 4,,10
	.p2align 3
.L2719:
	movslq	%ecx, %rcx
	movb	$0, -88(%rbp)
	leaq	15(%r15,%rcx,2), %rax
	leaq	(%rax,%r13,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L2720
	.p2align 4,,10
	.p2align 3
.L2715:
	movq	15(%r15), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2721
	movq	8(%rdi), %rax
.L2722:
	movslq	%ecx, %rcx
	addq	%rcx, %rax
.L2802:
	movslq	%r13d, %rdx
	movb	$1, -88(%rbp)
	addq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L2720
	.p2align 4,,10
	.p2align 3
.L2717:
	movslq	%ecx, %rcx
	leaq	15(%r15,%rcx), %rax
	jmp	.L2802
.L2770:
	movq	15(%rax), %rdi
	movl	%ecx, -472(%rbp)
	movl	%esi, -468(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-468(%rbp), %rsi
	movslq	-472(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %esi
	jmp	.L2777
.L2769:
	movslq	%esi, %rsi
	movslq	%ecx, %rdx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L2772
.L2767:
	movq	15(%rax), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2774
	movq	8(%rdi), %rax
.L2775:
	movslq	%esi, %rsi
	movslq	%ecx, %rdx
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L2772
	.p2align 4,,10
	.p2align 3
.L2726:
	leaq	-464(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L2733
	movl	-464(%rbp), %edi
	movl	11(%rax), %edx
	leaq	.L2730(%rip), %rsi
	movl	%edi, %r15d
.L2728:
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$15, %ecx
	cmpw	$13, %cx
	ja	.L2711
	movzwl	%cx, %ecx
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0
	.align 4
	.align 4
.L2730:
	.long	.L2736-.L2730
	.long	.L2733-.L2730
	.long	.L2735-.L2730
	.long	.L2731-.L2730
	.long	.L2711-.L2730
	.long	.L2729-.L2730
	.long	.L2711-.L2730
	.long	.L2711-.L2730
	.long	.L2734-.L2730
	.long	.L2733-.L2730
	.long	.L2732-.L2730
	.long	.L2731-.L2730
	.long	.L2711-.L2730
	.long	.L2729-.L2730
	.section	.text._ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L2803:
	movq	40960(%rsi), %rax
	movl	$620, %edx
	leaq	-408(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -416(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2700
	.p2align 4,,10
	.p2align 3
.L2808:
	leaq	-408(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2699
	.p2align 4,,10
	.p2align 3
.L2810:
	movq	-80(%rbp), %rax
	jmp	.L2801
.L2754:
	movl	%esi, -472(%rbp)
	movl	%ecx, -468(%rbp)
	call	*%rax
	movl	-472(%rbp), %esi
	movslq	-468(%rbp), %rcx
	jmp	.L2755
.L2806:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L2703
.L2811:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$88, %esi
	leaq	-368(%rbp), %rdx
	pushq	$0
	leaq	.LC85(%rip), %rcx
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2705
	.p2align 4,,10
	.p2align 3
.L2711:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2729:
	movq	15(%rax), %rax
	jmp	.L2728
.L2731:
	addl	27(%rax), %r15d
	movq	15(%rax), %rax
	jmp	.L2728
.L2732:
	subl	%edi, %edx
	movq	15(%rax), %rdi
	movl	%edx, %r13d
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2738
	movq	8(%rdi), %rax
.L2739:
	movslq	%r15d, %r15
	movslq	%r13d, %rdx
	movb	$1, -88(%rbp)
	addq	%r15, %rax
	addq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L2725
.L2734:
	movslq	%r15d, %r15
	subl	%edi, %edx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%r15), %rax
	movslq	%edx, %rdx
	addq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L2725
.L2735:
	subl	%edi, %edx
	movq	15(%rax), %rdi
	movslq	%r15d, %r15
	movl	%edx, %r13d
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%r13d, %rdx
	movb	$0, -88(%rbp)
	leaq	(%rax,%r15,2), %rax
	leaq	(%rax,%rdx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L2725
.L2736:
	movslq	%r15d, %r15
	subl	%edi, %edx
	movb	$0, -88(%rbp)
	leaq	15(%rax,%r15,2), %rax
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L2725
.L2721:
	movl	%ecx, -468(%rbp)
	call	*%rax
	movl	-468(%rbp), %ecx
	jmp	.L2722
.L2774:
	movl	%ecx, -472(%rbp)
	movl	%esi, -468(%rbp)
	call	*%rax
	movl	-472(%rbp), %ecx
	movl	-468(%rbp), %esi
	jmp	.L2775
.L2738:
	call	*%rax
	jmp	.L2739
.L2809:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29209:
	.size	_ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL24Stats_Runtime_DebugTraceEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC88:
	.string	"V8.Runtime_Runtime_DebugTrace"
	.section	.text._ZN2v88internalL24Stats_Runtime_DebugTraceEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_DebugTraceEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_DebugTraceEiPmPNS0_7IsolateE.isra.0:
.LFB29210:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2841
.L2813:
	movq	_ZZN2v88internalL24Stats_Runtime_DebugTraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic747(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2842
.L2815:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2843
.L2817:
	movq	stdout(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	leaq	-144(%rbp), %rdi
	movq	88(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2844
.L2812:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2845
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2842:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2846
.L2816:
	movq	%rbx, _ZZN2v88internalL24Stats_Runtime_DebugTraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic747(%rip)
	jmp	.L2815
	.p2align 4,,10
	.p2align 3
.L2843:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2847
.L2818:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2819
	movq	(%rdi), %rax
	call	*8(%rax)
.L2819:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2820
	movq	(%rdi), %rax
	call	*8(%rax)
.L2820:
	leaq	.LC88(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2817
	.p2align 4,,10
	.p2align 3
.L2844:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2812
	.p2align 4,,10
	.p2align 3
.L2841:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$562, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2813
	.p2align 4,,10
	.p2align 3
.L2847:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC88(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2818
	.p2align 4,,10
	.p2align 3
.L2846:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2816
.L2845:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29210:
	.size	_ZN2v88internalL24Stats_Runtime_DebugTraceEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_DebugTraceEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL25Stats_Runtime_SystemBreakEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC89:
	.string	"V8.Runtime_Runtime_SystemBreak"
	.section	.text._ZN2v88internalL25Stats_Runtime_SystemBreakEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_SystemBreakEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL25Stats_Runtime_SystemBreakEiPmPNS0_7IsolateE.isra.0:
.LFB29211:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2879
.L2849:
	movq	_ZZN2v88internalL25Stats_Runtime_SystemBreakEiPmPNS0_7IsolateEE28trace_event_unique_atomic795(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2880
.L2851:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2881
.L2853:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	call	_ZN2v84base2OS10DebugBreakEv@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2859
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2859:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2882
.L2848:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2883
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2881:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2884
.L2854:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2855
	movq	(%rdi), %rax
	call	*8(%rax)
.L2855:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2856
	movq	(%rdi), %rax
	call	*8(%rax)
.L2856:
	leaq	.LC89(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2853
	.p2align 4,,10
	.p2align 3
.L2880:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2885
.L2852:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_SystemBreakEiPmPNS0_7IsolateEE28trace_event_unique_atomic795(%rip)
	jmp	.L2851
	.p2align 4,,10
	.p2align 3
.L2879:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$631, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2849
	.p2align 4,,10
	.p2align 3
.L2882:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2848
	.p2align 4,,10
	.p2align 3
.L2885:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2852
	.p2align 4,,10
	.p2align 3
.L2884:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC89(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2854
.L2883:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29211:
	.size	_ZN2v88internalL25Stats_Runtime_SystemBreakEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL25Stats_Runtime_SystemBreakEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_DisassembleFunctionEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC90:
	.string	"V8.Runtime_Runtime_DisassembleFunction"
	.section	.text._ZN2v88internalL33Stats_Runtime_DisassembleFunctionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_DisassembleFunctionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_DisassembleFunctionEiPmPNS0_7IsolateE.isra.0:
.LFB29212:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2913
.L2887:
	movq	_ZZN2v88internalL33Stats_Runtime_DisassembleFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic854(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2914
.L2889:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L2915
.L2891:
	leaq	-144(%rbp), %rdi
	movq	88(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2895
.L2886:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2916
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2914:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2917
.L2890:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_DisassembleFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic854(%rip)
	jmp	.L2889
	.p2align 4,,10
	.p2align 3
.L2915:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2918
.L2892:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2893
	movq	(%rdi), %rax
	call	*8(%rax)
.L2893:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2894
	movq	(%rdi), %rax
	call	*8(%rax)
.L2894:
	leaq	.LC90(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2891
	.p2align 4,,10
	.p2align 3
.L2895:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2886
	.p2align 4,,10
	.p2align 3
.L2913:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$568, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2887
	.p2align 4,,10
	.p2align 3
.L2918:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC90(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2892
	.p2align 4,,10
	.p2align 3
.L2917:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2890
.L2916:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29212:
	.size	_ZN2v88internalL33Stats_Runtime_DisassembleFunctionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_DisassembleFunctionEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL24Stats_Runtime_TraceEnterEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC91:
	.string	"V8.Runtime_Runtime_TraceEnter"
.LC92:
	.string	" {\n"
	.section	.text._ZN2v88internalL24Stats_Runtime_TraceEnterEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_TraceEnterEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL24Stats_Runtime_TraceEnterEiPmPNS0_7IsolateE.isra.0:
.LFB29213:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2950
.L2920:
	movq	_ZZN2v88internalL24Stats_Runtime_TraceEnterEiPmPNS0_7IsolateEE28trace_event_unique_atomic892(%rip), %r12
	testq	%r12, %r12
	je	.L2951
.L2922:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L2952
.L2924:
	movq	%rbx, %rdi
	call	_ZN2v88internal12_GLOBAL__N_19StackSizeEPNS0_7IsolateE
	movl	%eax, %esi
	cmpl	$80, %eax
	jg	.L2928
	movl	%eax, %edx
	leaq	.LC22(%rip), %rcx
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L2929:
	movq	stdout(%rip), %rsi
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%rbx, %rdi
	call	_ZN2v88internal15JavaScriptFrame8PrintTopEPNS0_7IsolateEP8_IO_FILEbb@PLT
	leaq	.LC92(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	leaq	-144(%rbp), %rdi
	movq	88(%rbx), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2953
.L2919:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2954
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2928:
	.cfi_restore_state
	leaq	.LC24(%rip), %rcx
	movl	$80, %edx
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L2929
	.p2align 4,,10
	.p2align 3
.L2951:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2955
.L2923:
	movq	%r12, _ZZN2v88internalL24Stats_Runtime_TraceEnterEiPmPNS0_7IsolateEE28trace_event_unique_atomic892(%rip)
	jmp	.L2922
	.p2align 4,,10
	.p2align 3
.L2952:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2956
.L2925:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2926
	movq	(%rdi), %rax
	call	*8(%rax)
.L2926:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2927
	movq	(%rdi), %rax
	call	*8(%rax)
.L2927:
	leaq	.LC91(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L2924
	.p2align 4,,10
	.p2align 3
.L2953:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2919
	.p2align 4,,10
	.p2align 3
.L2950:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$632, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L2956:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC91(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L2925
	.p2align 4,,10
	.p2align 3
.L2955:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L2923
.L2954:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29213:
	.size	_ZN2v88internalL24Stats_Runtime_TraceEnterEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL24Stats_Runtime_TraceEnterEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_SerializeWasmModuleEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC93:
	.string	"V8.Runtime_Runtime_SerializeWasmModule"
	.section	.text._ZN2v88internalL33Stats_Runtime_SerializeWasmModuleEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_SerializeWasmModuleEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Stats_Runtime_SerializeWasmModuleEiPmPNS0_7IsolateE:
.LFB22663:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3000
.L2958:
	movq	_ZZN2v88internalL33Stats_Runtime_SerializeWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1128(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3001
.L2960:
	movq	$0, -192(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3002
.L2962:
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movq	%rax, -208(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -200(%rbp)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L2966
.L2967:
	leaq	.LC69(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3001:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3003
.L2961:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_SerializeWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1128(%rip)
	jmp	.L2960
	.p2align 4,,10
	.p2align 3
.L2966:
	movq	-1(%rax), %rdx
	cmpw	$1102, 11(%rdx)
	jne	.L2967
	movq	23(%rax), %rax
	leaq	-160(%rbp), %r15
	movq	%r15, %rdi
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal4wasm14WasmSerializerC1EPNS1_12NativeModuleE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal4wasm14WasmSerializer29GetSerializedNativeModuleSizeEv@PLT
	movq	45544(%r12), %rdi
	movq	%rax, %r13
	movq	(%rdi), %rax
	movq	%r13, %rsi
	call	*16(%rax)
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE@PLT
	subq	$8, %rsp
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	pushq	$0
	movq	%rax, %rdi
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb@PLT
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L2970
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZNK2v88internal4wasm14WasmSerializer21SerializeNativeModuleENS0_6VectorIhEE@PLT
	testb	%al, %al
	jne	.L3004
.L2970:
	movq	88(%r12), %r13
.L2969:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2971
	call	_ZdlPv@PLT
.L2971:
	subl	$1, 41104(%r12)
	movq	-208(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-200(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L2974
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2974:
	leaq	-192(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3005
.L2957:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3006
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3002:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3007
.L2963:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2964
	movq	(%rdi), %rax
	call	*8(%rax)
.L2964:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2965
	movq	(%rdi), %rax
	call	*8(%rax)
.L2965:
	leaq	.LC93(%rip), %rax
	movq	%rbx, -184(%rbp)
	movq	%rax, -176(%rbp)
	leaq	-184(%rbp), %rax
	movq	%r14, -168(%rbp)
	movq	%rax, -192(%rbp)
	jmp	.L2962
	.p2align 4,,10
	.p2align 3
.L3004:
	movq	(%rbx), %r13
	jmp	.L2969
	.p2align 4,,10
	.p2align 3
.L3000:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$623, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2958
	.p2align 4,,10
	.p2align 3
.L3005:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2957
	.p2align 4,,10
	.p2align 3
.L3003:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L2961
	.p2align 4,,10
	.p2align 3
.L3007:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC93(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L2963
.L3006:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22663:
	.size	_ZN2v88internalL33Stats_Runtime_SerializeWasmModuleEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Stats_Runtime_SerializeWasmModuleEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC94:
	.string	"V8.Runtime_Runtime_RedirectToWasmInterpreter"
	.align 8
.LC95:
	.string	"args[0].IsWasmInstanceObject()"
	.section	.rodata._ZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC96:
	.string	"args[1].IsSmi()"
	.section	.text._ZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE, @function
_ZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE:
.LFB22690:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3041
.L3009:
	movq	_ZZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1229(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3042
.L3011:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3043
.L3013:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3017
.L3018:
	leaq	.LC95(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3042:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3044
.L3012:
	movq	%rbx, _ZZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1229(%rip)
	jmp	.L3011
	.p2align 4,,10
	.p2align 3
.L3017:
	movq	-1(%rax), %rax
	cmpw	$1100, 11(%rax)
	jne	.L3018
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L3045
	movq	%r13, %rdi
	sarq	$32, %rax
	movl	%eax, -148(%rbp)
	call	_ZN2v88internal18WasmInstanceObject20GetOrCreateDebugInfoENS0_6HandleIS1_EE@PLT
	leaq	-148(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZN2v88internal13WasmDebugInfo21RedirectToInterpreterENS0_6HandleIS1_EENS0_6VectorIiEE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3020
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3020:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3046
.L3008:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3047
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3043:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3048
.L3014:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3015
	movq	(%rdi), %rax
	call	*8(%rax)
.L3015:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3016
	movq	(%rdi), %rax
	call	*8(%rax)
.L3016:
	leaq	.LC94(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3013
	.p2align 4,,10
	.p2align 3
.L3041:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$621, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3009
	.p2align 4,,10
	.p2align 3
.L3045:
	leaq	.LC96(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3046:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3008
	.p2align 4,,10
	.p2align 3
.L3048:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC94(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3014
	.p2align 4,,10
	.p2align 3
.L3044:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3012
.L3047:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22690:
	.size	_ZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE, .-_ZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC97:
	.string	"V8.Runtime_Runtime_WasmTraceMemory"
	.section	.rodata._ZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC98:
	.string	"args[0].IsSmi()"
	.section	.text._ZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE.isra.0:
.LFB29214:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$1672, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1632(%rbp)
	movq	$0, -1600(%rbp)
	movaps	%xmm0, -1616(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3084
.L3050:
	movq	_ZZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateEE29trace_event_unique_atomic1241(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3085
.L3052:
	movq	$0, -1664(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3086
.L3054:
	movq	41088(%r12), %r11
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %r14
	testb	$1, %r14b
	jne	.L3087
	leaq	-1584(%rbp), %r9
	movq	%r11, -1712(%rbp)
	movq	%r9, %rdi
	movq	%r9, -1704(%rbp)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	%r12, %rsi
	leaq	-1520(%rbp), %rdi
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-104(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	movq	%r13, %rdi
	movq	159(%rax), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rsi
	movq	%rsi, -1696(%rbp)
	call	_ZNK2v88internal17WasmCompiledFrame14function_indexEv@PLT
	movq	%r13, %rdi
	movl	%eax, -1688(%rbp)
	movq	0(%r13), %rax
	call	*104(%rax)
	movq	%r13, %rdi
	movl	%eax, -1684(%rbp)
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	leaq	-1672(%rbp), %rdi
	movq	%rax, -1672(%rbp)
	call	_ZN2v88internal18WasmInstanceObject6moduleEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	movslq	-1688(%rbp), %rax
	salq	$5, %rax
	addq	136(%r15), %rax
	movl	16(%rax), %r15d
	call	_ZNK2v88internal17WasmCompiledFrame9wasm_codeEv@PLT
	movq	-1696(%rbp), %rsi
	movl	-1684(%rbp), %ecx
	movzbl	136(%rax), %edi
	movl	$3, %eax
	movl	-1688(%rbp), %edx
	movq	%rsi, %r8
	movq	%r14, %rsi
	cmpb	$2, %dil
	cmovne	%eax, %edi
	subl	%r15d, %ecx
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movq	-1704(%rbp), %r9
	movq	88(%r12), %r13
	movq	%r9, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	-1712(%rbp), %r11
	subl	$1, 41104(%r12)
	movq	%r11, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3062
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3062:
	leaq	-1664(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1632(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3088
.L3049:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3089
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3086:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3090
.L3055:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3056
	movq	(%rdi), %rax
	call	*8(%rax)
.L3056:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3057
	movq	(%rdi), %rax
	call	*8(%rax)
.L3057:
	leaq	.LC97(%rip), %rax
	movq	%rbx, -1656(%rbp)
	movq	%rax, -1648(%rbp)
	leaq	-1656(%rbp), %rax
	movq	%r14, -1640(%rbp)
	movq	%rax, -1664(%rbp)
	jmp	.L3054
	.p2align 4,,10
	.p2align 3
.L3085:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3091
.L3053:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateEE29trace_event_unique_atomic1241(%rip)
	jmp	.L3052
	.p2align 4,,10
	.p2align 3
.L3087:
	leaq	.LC98(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3084:
	movq	40960(%rsi), %rax
	movl	$639, %edx
	leaq	-1624(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -1632(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3050
	.p2align 4,,10
	.p2align 3
.L3088:
	leaq	-1624(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3049
	.p2align 4,,10
	.p2align 3
.L3091:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3053
	.p2align 4,,10
	.p2align 3
.L3090:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC97(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3055
.L3089:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29214:
	.size	_ZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL39Stats_Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC99:
	.string	"V8.Runtime_Runtime_FreezeWasmLazyCompilation"
	.section	.text._ZN2v88internalL39Stats_Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL39Stats_Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateE.isra.0:
.LFB29215:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3123
.L3093:
	movq	_ZZN2v88internalL39Stats_Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateEE29trace_event_unique_atomic1308(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3124
.L3095:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3125
.L3097:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3101
.L3102:
	leaq	.LC95(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3124:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3126
.L3096:
	movq	%rbx, _ZZN2v88internalL39Stats_Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateEE29trace_event_unique_atomic1308(%rip)
	jmp	.L3095
	.p2align 4,,10
	.p2align 3
.L3101:
	movq	-1(%rax), %rdx
	cmpw	$1100, 11(%rdx)
	jne	.L3102
	movq	135(%rax), %rax
	leaq	-144(%rbp), %rdi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movb	$1, 677(%rax)
	movq	88(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3127
.L3092:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3128
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3125:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3129
.L3098:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3099
	movq	(%rdi), %rax
	call	*8(%rax)
.L3099:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3100
	movq	(%rdi), %rax
	call	*8(%rax)
.L3100:
	leaq	.LC99(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3097
	.p2align 4,,10
	.p2align 3
.L3123:
	movq	40960(%rsi), %rax
	movl	$571, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3093
	.p2align 4,,10
	.p2align 3
.L3127:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3092
	.p2align 4,,10
	.p2align 3
.L3126:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3096
	.p2align 4,,10
	.p2align 3
.L3129:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC99(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3098
.L3128:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29215:
	.size	_ZN2v88internalL39Stats_Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL39Stats_Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL34Stats_Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC100:
	.string	"V8.Runtime_Runtime_TurbofanStaticAssert"
	.section	.text._ZN2v88internalL34Stats_Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL34Stats_Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL34Stats_Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateE.isra.0:
.LFB29216:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3159
.L3131:
	movq	_ZZN2v88internalL34Stats_Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateEE29trace_event_unique_atomic1317(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3160
.L3133:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3161
.L3135:
	leaq	-144(%rbp), %rdi
	movq	88(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3162
.L3130:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3163
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3160:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3164
.L3134:
	movq	%rbx, _ZZN2v88internalL34Stats_Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateEE29trace_event_unique_atomic1317(%rip)
	jmp	.L3133
	.p2align 4,,10
	.p2align 3
.L3161:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3165
.L3136:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3137
	movq	(%rdi), %rax
	call	*8(%rax)
.L3137:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3138
	movq	(%rdi), %rax
	call	*8(%rax)
.L3138:
	leaq	.LC100(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3135
	.p2align 4,,10
	.p2align 3
.L3162:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3130
	.p2align 4,,10
	.p2align 3
.L3159:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$634, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L3165:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC100(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L3136
	.p2align 4,,10
	.p2align 3
.L3164:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3134
.L3163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29216:
	.size	_ZN2v88internalL34Stats_Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL34Stats_Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_RunningInSimulatorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC101:
	.string	"V8.Runtime_Runtime_RunningInSimulator"
	.section	.text._ZN2v88internalL32Stats_Runtime_RunningInSimulatorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_RunningInSimulatorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_RunningInSimulatorEiPmPNS0_7IsolateE.isra.0:
.LFB29218:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3195
.L3167:
	movq	_ZZN2v88internalL32Stats_Runtime_RunningInSimulatorEiPmPNS0_7IsolateEE28trace_event_unique_atomic201(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3196
.L3169:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3197
.L3171:
	leaq	-144(%rbp), %rdi
	movq	120(%r12), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3198
.L3166:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3199
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3196:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3200
.L3170:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_RunningInSimulatorEiPmPNS0_7IsolateEE28trace_event_unique_atomic201(%rip)
	jmp	.L3169
	.p2align 4,,10
	.p2align 3
.L3197:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3201
.L3172:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3173
	movq	(%rdi), %rax
	call	*8(%rax)
.L3173:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3174
	movq	(%rdi), %rax
	call	*8(%rax)
.L3174:
	leaq	.LC101(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3171
	.p2align 4,,10
	.p2align 3
.L3198:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3166
	.p2align 4,,10
	.p2align 3
.L3195:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$622, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3167
	.p2align 4,,10
	.p2align 3
.L3201:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC101(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L3172
	.p2align 4,,10
	.p2align 3
.L3200:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3170
.L3199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29218:
	.size	_ZN2v88internalL32Stats_Runtime_RunningInSimulatorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_RunningInSimulatorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL30Stats_Runtime_SetForceSlowPathEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC102:
	.string	"V8.Runtime_Runtime_SetForceSlowPath"
	.section	.text._ZN2v88internalL30Stats_Runtime_SetForceSlowPathEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_SetForceSlowPathEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL30Stats_Runtime_SetForceSlowPathEiPmPNS0_7IsolateE.isra.0:
.LFB29219:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3231
.L3203:
	movq	_ZZN2v88internalL30Stats_Runtime_SetForceSlowPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic805(%rip), %r12
	testq	%r12, %r12
	je	.L3232
.L3205:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L3233
.L3207:
	movq	0(%r13), %rax
	cmpq	%rax, 112(%rbx)
	leaq	-144(%rbp), %rdi
	sete	45428(%rbx)
	movq	88(%rbx), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3234
.L3202:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3235
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3232:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3236
.L3206:
	movq	%r12, _ZZN2v88internalL30Stats_Runtime_SetForceSlowPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic805(%rip)
	jmp	.L3205
	.p2align 4,,10
	.p2align 3
.L3233:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3237
.L3208:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3209
	movq	(%rdi), %rax
	call	*8(%rax)
.L3209:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3210
	movq	(%rdi), %rax
	call	*8(%rax)
.L3210:
	leaq	.LC102(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3207
	.p2align 4,,10
	.p2align 3
.L3234:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3202
	.p2align 4,,10
	.p2align 3
.L3231:
	movq	40960(%rsi), %rax
	movl	$625, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3203
	.p2align 4,,10
	.p2align 3
.L3237:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC102(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3208
	.p2align 4,,10
	.p2align 3
.L3236:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L3206
.L3235:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29219:
	.size	_ZN2v88internalL30Stats_Runtime_SetForceSlowPathEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL30Stats_Runtime_SetForceSlowPathEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC103:
	.string	"V8.Runtime_Runtime_DisallowCodegenFromStrings"
	.section	.rodata._ZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC104:
	.string	"args[0].IsBoolean()"
	.section	.text._ZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE.isra.0:
.LFB29220:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3275
.L3239:
	movq	_ZZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateEE28trace_event_unique_atomic961(%rip), %r12
	testq	%r12, %r12
	je	.L3276
.L3241:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L3277
.L3243:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3278
.L3248:
	leaq	.LC104(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3276:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3279
.L3242:
	movq	%r12, _ZZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateEE28trace_event_unique_atomic961(%rip)
	jmp	.L3241
	.p2align 4,,10
	.p2align 3
.L3278:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L3248
	testb	$-2, 43(%rax)
	jne	.L3248
	cmpq	112(%rbx), %rax
	leaq	_ZN2v88internal12_GLOBAL__N_134DisallowCodegenFromStringsCallbackENS_5LocalINS_7ContextEEENS2_INS_6StringEEE(%rip), %rsi
	movl	$0, %eax
	movq	%rbx, %rdi
	cmovne	%rax, %rsi
	call	_ZN2v87Isolate41SetAllowCodeGenerationFromStringsCallbackEPFbNS_5LocalINS_7ContextEEENS1_INS_6StringEEEE@PLT
	leaq	-144(%rbp), %rdi
	movq	88(%rbx), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3280
.L3238:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3281
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3277:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3282
.L3244:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3245
	movq	(%rdi), %rax
	call	*8(%rax)
.L3245:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3246
	movq	(%rdi), %rax
	call	*8(%rax)
.L3246:
	leaq	.LC103(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3243
	.p2align 4,,10
	.p2align 3
.L3275:
	movq	40960(%rsi), %rax
	movl	$566, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3239
	.p2align 4,,10
	.p2align 3
.L3280:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3238
	.p2align 4,,10
	.p2align 3
.L3279:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L3242
	.p2align 4,,10
	.p2align 3
.L3282:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC103(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3244
.L3281:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29220:
	.size	_ZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC105:
	.string	"V8.Runtime_Runtime_DisallowWasmCodegen"
	.section	.text._ZN2v88internalL33Stats_Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateE.isra.0:
.LFB29221:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3320
.L3284:
	movq	_ZZN2v88internalL33Stats_Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateEE28trace_event_unique_atomic971(%rip), %r12
	testq	%r12, %r12
	je	.L3321
.L3286:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L3322
.L3288:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3323
.L3293:
	leaq	.LC104(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3321:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3324
.L3287:
	movq	%r12, _ZZN2v88internalL33Stats_Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateEE28trace_event_unique_atomic971(%rip)
	jmp	.L3286
	.p2align 4,,10
	.p2align 3
.L3323:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L3293
	testb	$-2, 43(%rax)
	jne	.L3293
	cmpq	112(%rbx), %rax
	leaq	_ZN2v88internal12_GLOBAL__N_134DisallowCodegenFromStringsCallbackENS_5LocalINS_7ContextEEENS2_INS_6StringEEE(%rip), %rsi
	movl	$0, %eax
	movq	%rbx, %rdi
	cmovne	%rax, %rsi
	call	_ZN2v87Isolate34SetAllowWasmCodeGenerationCallbackEPFbNS_5LocalINS_7ContextEEENS1_INS_6StringEEEE@PLT
	leaq	-144(%rbp), %rdi
	movq	88(%rbx), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3325
.L3283:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3326
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3322:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3327
.L3289:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3290
	movq	(%rdi), %rax
	call	*8(%rax)
.L3290:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3291
	movq	(%rdi), %rax
	call	*8(%rax)
.L3291:
	leaq	.LC105(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3288
	.p2align 4,,10
	.p2align 3
.L3320:
	movq	40960(%rsi), %rax
	movl	$567, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3284
	.p2align 4,,10
	.p2align 3
.L3325:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3283
	.p2align 4,,10
	.p2align 3
.L3324:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L3287
	.p2align 4,,10
	.p2align 3
.L3327:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC105(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3289
.L3326:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29221:
	.size	_ZN2v88internalL33Stats_Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC106:
	.string	"V8.Runtime_Runtime_SetWasmThreadsEnabled"
	.section	.text._ZN2v88internalL35Stats_Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateE.isra.0:
.LFB29222:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3365
.L3329:
	movq	_ZZN2v88internalL35Stats_Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateEE29trace_event_unique_atomic1043(%rip), %r12
	testq	%r12, %r12
	je	.L3366
.L3331:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L3367
.L3333:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3368
.L3338:
	leaq	.LC104(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3366:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3369
.L3332:
	movq	%r12, _ZZN2v88internalL35Stats_Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateEE29trace_event_unique_atomic1043(%rip)
	jmp	.L3331
	.p2align 4,,10
	.p2align 3
.L3368:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L3338
	testb	$-2, 43(%rax)
	jne	.L3338
	cmpq	112(%rbx), %rax
	leaq	_ZN2v88internal12_GLOBAL__N_117EnableWasmThreadsENS_5LocalINS_7ContextEEE(%rip), %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_118DisableWasmThreadsENS_5LocalINS_7ContextEEE(%rip), %rax
	movq	%rbx, %rdi
	cmovne	%rax, %rsi
	call	_ZN2v87Isolate29SetWasmThreadsEnabledCallbackEPFbNS_5LocalINS_7ContextEEEE@PLT
	leaq	-144(%rbp), %rdi
	movq	88(%rbx), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3370
.L3328:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3371
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3367:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3372
.L3334:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3335
	movq	(%rdi), %rax
	call	*8(%rax)
.L3335:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3336
	movq	(%rdi), %rax
	call	*8(%rax)
.L3336:
	leaq	.LC106(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3333
	.p2align 4,,10
	.p2align 3
.L3365:
	movq	40960(%rsi), %rax
	movl	$629, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3329
	.p2align 4,,10
	.p2align 3
.L3370:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3328
	.p2align 4,,10
	.p2align 3
.L3369:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L3332
	.p2align 4,,10
	.p2align 3
.L3372:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC106(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3334
.L3371:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29222:
	.size	_ZN2v88internalL35Stats_Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC107:
	.string	"V8.Runtime_Runtime_RegexpHasBytecode"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC108:
	.string	"args[0].IsJSRegExp()"
.LC109:
	.string	"args[1].IsBoolean()"
	.section	.text._ZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE.isra.0:
.LFB29223:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3412
.L3374:
	movq	_ZZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1052(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3413
.L3376:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3414
.L3378:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3382
.L3383:
	leaq	.LC108(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3413:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3415
.L3377:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1052(%rip)
	jmp	.L3376
	.p2align 4,,10
	.p2align 3
.L3382:
	movq	-1(%rax), %rdx
	cmpw	$1075, 11(%rdx)
	jne	.L3383
	movq	%rax, -152(%rbp)
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L3416
.L3386:
	leaq	.LC109(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3414:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3417
.L3379:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3380
	movq	(%rdi), %rax
	call	*8(%rax)
.L3380:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3381
	movq	(%rdi), %rax
	call	*8(%rax)
.L3381:
	leaq	.LC107(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3378
	.p2align 4,,10
	.p2align 3
.L3416:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L3386
	testb	$-2, 43(%rax)
	jne	.L3386
	xorl	%esi, %esi
	cmpq	112(%r12), %rax
	leaq	-152(%rbp), %rdi
	sete	%sil
	call	_ZNK2v88internal8JSRegExp8BytecodeEb@PLT
	testb	$1, %al
	jne	.L3418
.L3388:
	movq	120(%r12), %r12
.L3390:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3419
.L3373:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3420
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3418:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$70, 11(%rax)
	jne	.L3388
	movq	112(%r12), %r12
	jmp	.L3390
	.p2align 4,,10
	.p2align 3
.L3412:
	movq	40960(%rsi), %rax
	movl	$612, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3374
	.p2align 4,,10
	.p2align 3
.L3419:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3373
	.p2align 4,,10
	.p2align 3
.L3415:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3377
	.p2align 4,,10
	.p2align 3
.L3417:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC107(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3379
.L3420:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29223:
	.size	_ZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL33Stats_Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC110:
	.string	"V8.Runtime_Runtime_RegexpHasNativeCode"
	.section	.text._ZN2v88internalL33Stats_Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateE.isra.0:
.LFB29224:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3460
.L3422:
	movq	_ZZN2v88internalL33Stats_Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1061(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3461
.L3424:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3462
.L3426:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3430
.L3431:
	leaq	.LC108(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3461:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3463
.L3425:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1061(%rip)
	jmp	.L3424
	.p2align 4,,10
	.p2align 3
.L3430:
	movq	-1(%rax), %rdx
	cmpw	$1075, 11(%rdx)
	jne	.L3431
	movq	%rax, -152(%rbp)
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L3464
.L3434:
	leaq	.LC109(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3462:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3465
.L3427:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3428
	movq	(%rdi), %rax
	call	*8(%rax)
.L3428:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3429
	movq	(%rdi), %rax
	call	*8(%rax)
.L3429:
	leaq	.LC110(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3426
	.p2align 4,,10
	.p2align 3
.L3464:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L3434
	testb	$-2, 43(%rax)
	jne	.L3434
	xorl	%esi, %esi
	cmpq	112(%r12), %rax
	leaq	-152(%rbp), %rdi
	sete	%sil
	call	_ZNK2v88internal8JSRegExp4CodeEb@PLT
	testb	$1, %al
	jne	.L3466
.L3436:
	movq	120(%r12), %r12
.L3438:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3467
.L3421:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3468
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3466:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$69, 11(%rax)
	jne	.L3436
	movq	112(%r12), %r12
	jmp	.L3438
	.p2align 4,,10
	.p2align 3
.L3460:
	movq	40960(%rsi), %rax
	movl	$613, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3422
	.p2align 4,,10
	.p2align 3
.L3467:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3421
	.p2align 4,,10
	.p2align 3
.L3463:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3425
	.p2align 4,,10
	.p2align 3
.L3465:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC110(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3427
.L3468:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29224:
	.size	_ZN2v88internalL33Stats_Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL27Stats_Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC111:
	.string	"V8.Runtime_Runtime_IsAsmWasmCode"
	.section	.text._ZN2v88internalL27Stats_Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateE.isra.0:
.LFB29225:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3508
.L3470:
	movq	_ZZN2v88internalL27Stats_Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateEE28trace_event_unique_atomic938(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3509
.L3472:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3510
.L3474:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3478
.L3479:
	leaq	.LC34(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3509:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3511
.L3473:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateEE28trace_event_unique_atomic938(%rip)
	jmp	.L3472
	.p2align 4,,10
	.p2align 3
.L3478:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L3479
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	testb	$1, %dl
	jne	.L3512
.L3507:
	movq	120(%r12), %r12
.L3482:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3513
.L3469:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3514
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3510:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3515
.L3475:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3476
	movq	(%rdi), %rax
	call	*8(%rax)
.L3476:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3477
	movq	(%rdi), %rax
	call	*8(%rax)
.L3477:
	leaq	.LC111(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3474
	.p2align 4,,10
	.p2align 3
.L3512:
	movq	-1(%rdx), %rdx
	cmpw	$83, 11(%rdx)
	jne	.L3507
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	andl	$1, %edx
	jne	.L3483
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$69, %rax
	je	.L3507
.L3483:
	movq	112(%r12), %r12
	jmp	.L3482
	.p2align 4,,10
	.p2align 3
.L3508:
	movq	40960(%rsi), %rax
	movl	$606, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3470
	.p2align 4,,10
	.p2align 3
.L3513:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3469
	.p2align 4,,10
	.p2align 3
.L3511:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3473
	.p2align 4,,10
	.p2align 3
.L3515:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC111(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3475
.L3514:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29225:
	.size	_ZN2v88internalL27Stats_Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC112:
	.string	"concurrent"
.LC113:
	.string	"non-concurrent"
	.section	.rodata._ZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC114:
	.string	"V8.Runtime_Runtime_OptimizeFunctionOnNextCall"
	.section	.rodata._ZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE.str1.1
.LC115:
	.string	" for %s optimization]\n"
.LC116:
	.string	"[manually marking "
	.section	.text._ZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE, @function
_ZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE:
.LFB22447:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3642
.L3517:
	movq	_ZZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic224(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3643
.L3519:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3644
.L3521:
	movl	41104(%r12), %eax
	movq	41088(%r12), %rcx
	movq	41096(%r12), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	leal	-1(%r13), %edx
	cmpl	$1, %edx
	ja	.L3645
	movq	(%r14), %rdx
	movq	%rbx, %rsi
	testb	$1, %dl
	jne	.L3646
.L3529:
	movq	88(%r12), %r13
.L3532:
	movq	%rcx, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rsi, %rbx
	je	.L3526
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3526:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3647
.L3516:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3648
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3646:
	.cfi_restore_state
	movq	-1(%rdx), %rsi
	cmpw	$1105, 11(%rsi)
	je	.L3528
	movq	%rbx, %rsi
	jmp	.L3529
	.p2align 4,,10
	.p2align 3
.L3645:
	movl	%eax, 41104(%r12)
	movq	88(%r12), %r13
	jmp	.L3526
	.p2align 4,,10
	.p2align 3
.L3644:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3649
.L3522:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3523
	movq	(%rdi), %rax
	call	*8(%rax)
.L3523:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3524
	movq	(%rdi), %rax
	call	*8(%rax)
.L3524:
	leaq	.LC114(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3521
	.p2align 4,,10
	.p2align 3
.L3643:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3650
.L3520:
	movq	%rbx, _ZZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic224(%rip)
	jmp	.L3519
	.p2align 4,,10
	.p2align 3
.L3528:
	movq	23(%rdx), %rsi
	movl	47(%rsi), %esi
	andl	$4096, %esi
	je	.L3651
	movq	23(%rdx), %r15
	movq	%r15, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	7(%r15), %rdx
	testb	$1, %dl
	jne	.L3533
.L3536:
	movq	7(%r15), %rdx
	testb	$1, %dl
	jne	.L3652
.L3534:
	xorl	%eax, %eax
.L3545:
	movabsq	$287762808832, %rsi
	movq	7(%r15), %rdx
	cmpq	%rsi, %rdx
	je	.L3546
	testb	$1, %dl
	jne	.L3653
	movq	%rax, -176(%rbp)
.L3640:
	movb	$1, -168(%rbp)
.L3551:
	cmpb	$0, _ZN2v88internal8FLAG_optE(%rip)
	je	.L3577
	movq	(%r14), %r8
	movq	23(%r8), %rax
	movl	47(%rax), %eax
	testl	$15728640, %eax
	je	.L3554
	movq	23(%r8), %rax
	movl	47(%rax), %eax
	shrl	$20, %eax
	andl	$15, %eax
	cmpb	$11, %al
	je	.L3577
.L3554:
	movq	23(%r8), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L3654
.L3556:
	cmpb	$0, _ZN2v88internal27FLAG_testing_d8_test_runnerE(%rip)
	jne	.L3655
.L3559:
	movq	(%r14), %rax
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	jne	.L3656
.L3562:
	movabsq	$287762808832, %rsi
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L3574
	testb	$1, %dl
	jne	.L3568
.L3571:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L3657
.L3574:
	leaq	-184(%rbp), %r15
	cmpl	$2, %r13d
	je	.L3658
.L3576:
	xorl	%r13d, %r13d
	cmpb	$0, _ZN2v88internal14FLAG_trace_optE(%rip)
	jne	.L3659
.L3582:
	movq	(%r14), %r8
	movq	47(%r8), %rax
	cmpl	$67, 59(%rax)
	jne	.L3660
.L3584:
	movl	$57, %esi
	leaq	41184(%r12), %rdi
	movq	%r8, -208(%rbp)
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	-208(%rbp), %r8
	movq	(%rax), %rdx
	leaq	47(%r8), %rsi
	movq	%rdx, 47(%r8)
	testb	$1, %dl
	movq	-200(%rbp), %rcx
	je	.L3588
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L3588
	movq	%r8, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-200(%rbp), %rcx
.L3588:
	movq	%r14, %rdi
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE@PLT
	movq	(%r14), %rax
	movl	%r13d, %esi
	movq	%r15, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE@PLT
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rsi
	movq	-200(%rbp), %rcx
	subl	$1, %eax
	jmp	.L3532
	.p2align 4,,10
	.p2align 3
.L3647:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3516
	.p2align 4,,10
	.p2align 3
.L3642:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$617, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3517
	.p2align 4,,10
	.p2align 3
.L3650:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3520
	.p2align 4,,10
	.p2align 3
.L3649:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC114(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3522
	.p2align 4,,10
	.p2align 3
.L3651:
	movq	88(%r12), %r13
	movq	%rbx, %rsi
	jmp	.L3532
	.p2align 4,,10
	.p2align 3
.L3652:
	movq	-1(%rdx), %rdx
	cmpw	$91, 11(%rdx)
	jne	.L3534
.L3539:
	movq	31(%r15), %rdx
	testb	$1, %dl
	jne	.L3661
.L3537:
	movq	7(%r15), %rdx
	testb	$1, %dl
	jne	.L3662
.L3541:
	movq	7(%r15), %rdx
	movq	7(%rdx), %rsi
.L3540:
	movq	3520(%rax), %rdi
	leaq	-37592(%rax), %rdx
	testq	%rdi, %rdi
	je	.L3542
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-200(%rbp), %rcx
	jmp	.L3545
.L3656:
	movabsq	$287762808832, %rsi
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L3562
	testb	$1, %dl
	jne	.L3663
.L3563:
	movq	47(%rax), %rdx
	testb	$62, 43(%rdx)
	jne	.L3562
	movq	47(%rax), %rdx
	movq	31(%rdx), %rdx
	movl	15(%rdx), %edx
	andb	$1, %dl
	jne	.L3562
.L3566:
	cmpb	$0, _ZN2v88internal27FLAG_testing_d8_test_runnerE(%rip)
	je	.L3577
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal24PendingOptimizationTable20FunctionWasOptimizedEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	-200(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L3577:
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rsi
	subl	$1, %eax
	jmp	.L3532
	.p2align 4,,10
	.p2align 3
.L3546:
	movq	%rax, -176(%rbp)
.L3639:
	leaq	-176(%rbp), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rcx, -200(%rbp)
	movb	$0, -168(%rbp)
	call	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE@PLT
	movq	-200(%rbp), %rcx
	testb	%al, %al
	jne	.L3551
	jmp	.L3577
	.p2align 4,,10
	.p2align 3
.L3533:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L3536
	jmp	.L3539
.L3653:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L3546
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	movq	%rax, -176(%rbp)
	jne	.L3640
	jmp	.L3639
.L3542:
	movq	41088(%rdx), %rax
	cmpq	41096(%rdx), %rax
	je	.L3664
.L3544:
	leaq	8(%rax), %rdi
	movq	%rdi, 41088(%rdx)
	movq	%rsi, (%rax)
	jmp	.L3545
.L3654:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L3556
	jmp	.L3577
.L3661:
	movq	-1(%rdx), %rsi
	cmpw	$86, 11(%rsi)
	jne	.L3537
	movq	39(%rdx), %rsi
	testb	$1, %sil
	je	.L3537
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L3537
	movq	31(%rdx), %rsi
	jmp	.L3540
.L3662:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L3541
	movq	7(%r15), %rsi
	jmp	.L3540
.L3655:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal24PendingOptimizationTable21MarkedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	-200(%rbp), %rcx
	jmp	.L3559
.L3660:
	movabsq	$287762808832, %rdx
	movq	23(%r8), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3641
	testb	$1, %al
	je	.L3588
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L3641
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L3588
.L3641:
	movq	(%r14), %r8
	jmp	.L3584
.L3657:
	movq	15(%rax), %rax
	testb	$1, %al
	je	.L3574
	cmpl	$3, %eax
	je	.L3574
	andq	$-3, %rax
	je	.L3574
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	jne	.L3574
	jmp	.L3566
.L3663:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L3562
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L3563
	jmp	.L3562
.L3664:
	movq	%rdx, %rdi
	movq	%rsi, -216(%rbp)
	movq	%rcx, -208(%rbp)
	movq	%rdx, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	-208(%rbp), %rcx
	movq	-200(%rbp), %rdx
	jmp	.L3544
.L3658:
	movq	-8(%r14), %rax
	testb	$1, %al
	je	.L3577
	movq	%r15, %rdi
	movq	%rcx, -200(%rbp)
	movq	%rax, -184(%rbp)
	call	_ZNK2v88internal10HeapObject8map_wordEPNS0_7IsolateE.isra.0
	movq	-200(%rbp), %rcx
	cmpw	$63, 11(%rax)
	ja	.L3577
	movq	-8(%r14), %rax
	leaq	.LC112(%rip), %rsi
	movl	$10, %edx
	movq	%rcx, -200(%rbp)
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movq	-200(%rbp), %rcx
	testb	%al, %al
	je	.L3576
	cmpq	$0, 45416(%r12)
	je	.L3576
	cmpb	$0, _ZN2v88internal14FLAG_trace_optE(%rip)
	movl	$1, %r13d
	je	.L3582
	leaq	.LC116(%rip), %rdi
	xorl	%eax, %eax
	movq	%rcx, -200(%rbp)
	movl	$1, %r13d
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r14), %rax
	movq	stdout(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -184(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movq	-200(%rbp), %rcx
	leaq	.LC112(%rip), %rsi
.L3593:
	leaq	.LC115(%rip), %rdi
	xorl	%eax, %eax
	movq	%rcx, -200(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-200(%rbp), %rcx
	jmp	.L3582
.L3659:
	leaq	.LC116(%rip), %rdi
	xorl	%eax, %eax
	movq	%rcx, -200(%rbp)
	xorl	%r13d, %r13d
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r14), %rax
	movq	stdout(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -184(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movq	-200(%rbp), %rcx
	leaq	.LC113(%rip), %rsi
	jmp	.L3593
.L3568:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L3574
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L3571
	jmp	.L3574
.L3648:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22447:
	.size	_ZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE, .-_ZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC117:
	.string	"V8.Runtime_Runtime_PrepareFunctionForOptimization"
	.section	.rodata._ZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC118:
	.string	"allow heuristic optimization"
	.section	.text._ZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE, @function
_ZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE:
.LFB22458:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3717
.L3666:
	movq	_ZZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateEE28trace_event_unique_atomic352(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3718
.L3668:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3719
.L3670:
	movl	41104(%r12), %eax
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	0(%r13), %rdx
	testb	$1, %dl
	jne	.L3674
.L3716:
	movq	88(%r12), %r13
	movq	%rbx, %rdx
.L3678:
	movq	%r15, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L3694
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3694:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3720
.L3665:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3721
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3674:
	.cfi_restore_state
	movq	-1(%rdx), %rsi
	cmpw	$1105, 11(%rsi)
	jne	.L3716
	movb	$0, -177(%rbp)
	movq	-1(%rdx), %rax
	cmpw	$1105, 11(%rax)
	je	.L3713
	leaq	.LC34(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3719:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3722
.L3671:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3672
	movq	(%rdi), %rax
	call	*8(%rax)
.L3672:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3673
	movq	(%rdi), %rax
	call	*8(%rax)
.L3673:
	leaq	.LC117(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3670
	.p2align 4,,10
	.p2align 3
.L3718:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3723
.L3669:
	movq	%rbx, _ZZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateEE28trace_event_unique_atomic352(%rip)
	jmp	.L3668
	.p2align 4,,10
	.p2align 3
.L3713:
	cmpl	$2, %r14d
	je	.L3724
.L3680:
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120EnsureFeedbackVectorENS0_6HandleINS0_10JSFunctionEEE
	testb	%al, %al
	je	.L3691
	movq	0(%r13), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$15728640, %edx
	je	.L3688
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	shrl	$20, %edx
	andl	$15, %edx
	cmpb	$11, %dl
	je	.L3691
.L3688:
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L3725
.L3685:
	cmpb	$0, _ZN2v88internal27FLAG_testing_d8_test_runnerE(%rip)
	je	.L3691
	movzbl	-177(%rbp), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb@PLT
	.p2align 4,,10
	.p2align 3
.L3691:
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L3678
	.p2align 4,,10
	.p2align 3
.L3717:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$619, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3666
	.p2align 4,,10
	.p2align 3
.L3720:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3665
	.p2align 4,,10
	.p2align 3
.L3723:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3669
	.p2align 4,,10
	.p2align 3
.L3722:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC117(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3671
	.p2align 4,,10
	.p2align 3
.L3724:
	movq	-8(%r13), %rax
	testb	$1, %al
	je	.L3691
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L3691
	leaq	-168(%rbp), %rdi
	leaq	.LC118(%rip), %rsi
	movl	$28, %edx
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movb	%al, -177(%rbp)
	jmp	.L3680
	.p2align 4,,10
	.p2align 3
.L3725:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L3685
	jmp	.L3691
.L3721:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22458:
	.size	_ZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE, .-_ZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC119:
	.string	"V8.Runtime_Runtime_GetOptimizationStatus"
	.section	.rodata._ZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC120:
	.string	"no sync"
	.section	.text._ZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE, @function
_ZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE:
.LFB22468:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1592, %rsp
	movl	%edi, -1612(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -1568(%rbp)
	movq	$0, -1536(%rbp)
	movaps	%xmm0, -1552(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3864
.L3727:
	movq	_ZZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateEE28trace_event_unique_atomic469(%rip), %r12
	testq	%r12, %r12
	je	.L3865
.L3729:
	movq	$0, -1600(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L3866
.L3731:
	movq	41088(%r13), %rax
	addl	$1, 41104(%r13)
	cmpb	$0, _ZN2v88internal14FLAG_lite_modeE(%rip)
	movq	41096(%r13), %r12
	movq	%rax, -1608(%rbp)
	jne	.L3816
	cmpb	$1, _ZN2v88internal12FLAG_jitlessE(%rip)
	sbbl	%r15d, %r15d
	andl	$-4096, %r15d
	addl	$4098, %r15d
	cmpb	$1, _ZN2v88internal12FLAG_jitlessE(%rip)
	sbbl	%r14d, %r14d
	notl	%r14d
	andl	$4096, %r14d
.L3735:
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate13use_optimizerEv@PLT
	testb	%al, %al
	cmove	%r15d, %r14d
	cmpb	$0, _ZN2v88internal15FLAG_always_optE(%rip)
	jne	.L3737
	cmpb	$0, _ZN2v88internal23FLAG_prepare_always_optE(%rip)
	jne	.L3737
.L3738:
	movl	_ZN2v88internal24FLAG_deopt_every_n_timesE(%rip), %eax
	testl	%eax, %eax
	je	.L3739
	orl	$8, %r14d
.L3739:
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L3740
.L3863:
	salq	$32, %r14
.L3741:
	movq	-1608(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	cmpq	41096(%r13), %r12
	je	.L3812
	movq	%r12, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3812:
	leaq	-1600(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3867
.L3726:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3868
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3737:
	.cfi_restore_state
	orl	$4, %r14d
	jmp	.L3738
	.p2align 4,,10
	.p2align 3
.L3816:
	movl	$4098, %r15d
	movl	$4096, %r14d
	jmp	.L3735
	.p2align 4,,10
	.p2align 3
.L3740:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L3863
	movl	%r14d, %ecx
	orl	$1, %ecx
	cmpl	$2, -1612(%rbp)
	movl	%ecx, -1624(%rbp)
	je	.L3743
	cmpq	$0, 45416(%r13)
	leaq	-1520(%rbp), %r15
	je	.L3745
	.p2align 4,,10
	.p2align 3
.L3746:
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L3860
	testb	$1, %dl
	jne	.L3751
.L3754:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L3752
.L3860:
	movq	(%rbx), %rax
.L3745:
	movabsq	$287762808832, %rsi
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L3764
	testb	$1, %dl
	jne	.L3759
.L3762:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	jne	.L3764
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L3764
	sarq	$32, %rax
	cmpq	$2, %rax
	jne	.L3764
	orb	$-127, %r14b
	movl	%r14d, -1624(%rbp)
	jmp	.L3768
	.p2align 4,,10
	.p2align 3
.L3866:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3869
.L3732:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3733
	movq	(%rdi), %rax
	call	*8(%rax)
.L3733:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3734
	movq	(%rdi), %rax
	call	*8(%rax)
.L3734:
	leaq	.LC119(%rip), %rax
	movq	%r12, -1592(%rbp)
	movq	%rax, -1584(%rbp)
	leaq	-1592(%rbp), %rax
	movq	%r14, -1576(%rbp)
	movq	%rax, -1600(%rbp)
	jmp	.L3731
	.p2align 4,,10
	.p2align 3
.L3865:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3870
.L3730:
	movq	%r12, _ZZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateEE28trace_event_unique_atomic469(%rip)
	jmp	.L3729
	.p2align 4,,10
	.p2align 3
.L3867:
	leaq	-1560(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3726
	.p2align 4,,10
	.p2align 3
.L3864:
	movq	40960(%rdx), %rax
	leaq	-1560(%rbp), %rsi
	movl	$574, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -1568(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3727
	.p2align 4,,10
	.p2align 3
.L3752:
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L3860
	sarq	$32, %rax
	cmpq	$4, %rax
	jne	.L3860
	movq	45416(%r13), %rdi
	call	_ZN2v88internal27OptimizingCompileDispatcher25InstallOptimizedFunctionsEv@PLT
	movl	$50000, %edi
	call	_ZN2v84base2OS5SleepENS0_9TimeDeltaE@PLT
	movq	(%rbx), %rax
	jmp	.L3746
	.p2align 4,,10
	.p2align 3
.L3759:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L3764
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L3762
	.p2align 4,,10
	.p2align 3
.L3764:
	movabsq	$287762808832, %rsi
	movq	(%rbx), %rdx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3774
	testb	$1, %al
	jne	.L3769
.L3772:
	movq	39(%rdx), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L3871
	.p2align 4,,10
	.p2align 3
.L3774:
	movabsq	$287762808832, %rsi
	movq	(%rbx), %rdx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3768
	testb	$1, %al
	jne	.L3778
.L3781:
	movq	39(%rdx), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L3872
	.p2align 4,,10
	.p2align 3
.L3768:
	movq	(%rbx), %rax
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	je	.L3791
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L3861
	testb	$1, %dl
	jne	.L3873
.L3787:
	movq	47(%rax), %rdx
	testb	$62, 43(%rdx)
	je	.L3789
.L3861:
	movq	(%rbx), %rax
.L3791:
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	je	.L3801
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L3801
	testb	$1, %dl
	jne	.L3874
.L3797:
	movq	47(%rax), %rdx
	movl	59(%rdx), %edx
	cmpl	$57, %edx
	je	.L3802
	subl	$64, %edx
	cmpl	$1, %edx
	jbe	.L3802
	movq	47(%rax), %rdx
	testb	$62, 43(%rdx)
	je	.L3875
	.p2align 4,,10
	.p2align 3
.L3801:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -104(%rbp)
	je	.L3805
	.p2align 4,,10
	.p2align 3
.L3862:
	movq	%r15, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3805
	movq	(%rdi), %rax
	movq	(%rbx), %r14
	call	*152(%rax)
	cmpq	%rax, %r14
	jne	.L3862
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3805
	movq	(%rdi), %rax
	call	*8(%rax)
	movl	-1624(%rbp), %ecx
	movl	%eax, %r8d
	movl	%ecx, %eax
	orb	$12, %ch
	orb	$4, %ah
	cmpl	$4, %r8d
	cmove	%ecx, %eax
	movl	%eax, -1624(%rbp)
.L3805:
	movq	-1624(%rbp), %r14
	jmp	.L3863
	.p2align 4,,10
	.p2align 3
.L3870:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L3730
	.p2align 4,,10
	.p2align 3
.L3869:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC119(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3732
.L3875:
	movq	47(%rax), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L3801
	.p2align 4,,10
	.p2align 3
.L3802:
	orl	$64, -1624(%rbp)
	jmp	.L3801
	.p2align 4,,10
	.p2align 3
.L3751:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L3860
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L3754
	jmp	.L3860
	.p2align 4,,10
	.p2align 3
.L3743:
	movq	-8(%rbx), %rax
	testb	$1, %al
	jne	.L3876
.L3747:
	movq	88(%r13), %r14
	jmp	.L3741
.L3871:
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L3774
	sarq	$32, %rax
	cmpq	$4, %rax
	jne	.L3774
	orl	$257, %r14d
	movl	%r14d, -1624(%rbp)
	jmp	.L3768
	.p2align 4,,10
	.p2align 3
.L3876:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L3747
	leaq	-1520(%rbp), %r15
	leaq	.LC120(%rip), %rsi
	movl	$7, %edx
	movq	%rax, -1520(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	testb	%al, %al
	jne	.L3860
	cmpq	$0, 45416(%r13)
	movq	(%rbx), %rax
	jne	.L3746
	jmp	.L3745
.L3789:
	movq	47(%rax), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	movq	(%rbx), %rax
	jne	.L3791
	movq	47(%rax), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %ecx
	movl	-1624(%rbp), %esi
	movl	%esi, %eax
	orl	$16, %esi
	orb	$32, %ah
	andl	$1, %ecx
	cmove	%esi, %eax
	movl	%eax, %esi
	movq	(%rbx), %rax
	movl	%esi, %edx
	movq	47(%rax), %rcx
	orl	$32, %edx
	testb	$64, 43(%rcx)
	cmove	%esi, %edx
	movl	%edx, -1624(%rbp)
	jmp	.L3791
.L3873:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L3861
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L3787
	jmp	.L3861
.L3874:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L3801
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L3797
	jmp	.L3801
.L3769:
	movq	-1(%rax), %rsi
	cmpw	$165, 11(%rsi)
	je	.L3774
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L3772
	jmp	.L3774
.L3872:
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L3768
	sarq	$32, %rax
	movl	%r14d, %ecx
	orl	$513, %ecx
	cmpq	$4, %rax
	cmovne	-1624(%rbp), %ecx
	movl	%ecx, -1624(%rbp)
	jmp	.L3768
.L3778:
	movq	-1(%rax), %rsi
	cmpw	$165, 11(%rsi)
	je	.L3768
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L3781
	jmp	.L3768
.L3868:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22468:
	.size	_ZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE, .-_ZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC121:
	.string	"V8.Runtime_Runtime_DebugTrackRetainingPath"
	.align 8
.LC122:
	.string	"DebugTrackRetainingPath requires --track-retaining-path flag.\n"
	.section	.rodata._ZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC123:
	.string	"args[0].IsHeapObject()"
.LC124:
	.string	"args[1].IsString()"
	.section	.rodata._ZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE.str1.8
	.align 8
.LC125:
	.string	"Unexpected second argument of DebugTrackRetainingPath.\n"
	.align 8
.LC126:
	.string	"Expected an empty string or '%s', got '%s'.\n"
	.section	.text._ZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE, @function
_ZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE:
.LFB22507:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -112(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3924
.L3878:
	movq	_ZZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic754(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3925
.L3880:
	movq	$0, -176(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3926
.L3882:
	movq	41088(%r12), %r15
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	cmpb	$0, _ZN2v88internal25FLAG_track_retaining_pathE(%rip)
	jne	.L3886
	leaq	.LC122(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L3887:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3896
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3896:
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3927
.L3877:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3928
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3886:
	.cfi_restore_state
	testb	$1, 0(%r13)
	je	.L3929
	xorl	%edx, %edx
	cmpl	$2, %r14d
	je	.L3930
.L3889:
	leaq	37592(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4Heap22AddRetainingPathTargetENS0_6HandleINS0_10HeapObjectEEENS0_19RetainingPathOptionE@PLT
	jmp	.L3887
	.p2align 4,,10
	.p2align 3
.L3926:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3931
.L3883:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3884
	movq	(%rdi), %rax
	call	*8(%rax)
.L3884:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3885
	movq	(%rdi), %rax
	call	*8(%rax)
.L3885:
	leaq	.LC121(%rip), %rax
	movq	%rbx, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%r15, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L3882
	.p2align 4,,10
	.p2align 3
.L3925:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3932
.L3881:
	movq	%rbx, _ZZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic754(%rip)
	jmp	.L3880
	.p2align 4,,10
	.p2align 3
.L3929:
	leaq	.LC123(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3930:
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L3890
.L3891:
	leaq	.LC124(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3924:
	movq	40960(%rdx), %rax
	leaq	-136(%rbp), %rsi
	movl	$563, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3878
	.p2align 4,,10
	.p2align 3
.L3927:
	leaq	-136(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3877
	.p2align 4,,10
	.p2align 3
.L3932:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3881
	.p2align 4,,10
	.p2align 3
.L3931:
	subq	$8, %rsp
	leaq	-96(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC121(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L3883
	.p2align 4,,10
	.p2align 3
.L3890:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L3891
	leaq	-184(%rbp), %r9
	leaq	-80(%rbp), %r14
	movb	$0, -60(%rbp)
	movdqa	.LC127(%rip), %xmm0
	movl	$20, %edx
	movq	%r14, %rsi
	movq	%r9, %rdi
	movl	$1752457584, -64(%rbp)
	movq	%rax, -184(%rbp)
	movq	%r9, -200(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	$1, %edx
	testb	%al, %al
	jne	.L3889
	movq	-8(%r13), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	je	.L3889
	leaq	.LC125(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-200(%rbp), %r9
	movq	-8(%r13), %rax
	xorl	%r8d, %r8d
	leaq	-192(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r9, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-184(%rbp), %rdx
	leaq	.LC126(%rip), %rdi
	xorl	%eax, %eax
	movq	%r14, %rsi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-184(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3893
	call	_ZdaPv@PLT
.L3893:
	xorl	%edx, %edx
	jmp	.L3889
.L3928:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22507:
	.size	_ZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE, .-_ZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Stats_Runtime_ConstructConsStringEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC128:
	.string	"V8.Runtime_Runtime_ConstructConsString"
	.align 8
.LC129:
	.string	"left->IsOneByteRepresentation()"
	.align 8
.LC130:
	.string	"right->IsOneByteRepresentation()"
	.section	.text._ZN2v88internalL33Stats_Runtime_ConstructConsStringEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_ConstructConsStringEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_ConstructConsStringEiPmPNS0_7IsolateE.isra.0:
.LFB29228:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3969
.L3934:
	movq	_ZZN2v88internalL33Stats_Runtime_ConstructConsStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic133(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3970
.L3936:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3971
.L3938:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3942
.L3943:
	leaq	.LC18(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3970:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3972
.L3937:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_ConstructConsStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic133(%rip)
	jmp	.L3936
	.p2align 4,,10
	.p2align 3
.L3942:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L3943
	movq	-8(%r13), %rdx
	leaq	-8(%r13), %r9
	testb	$1, %dl
	jne	.L3973
.L3944:
	leaq	.LC124(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3971:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3974
.L3939:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3940
	movq	(%rdi), %rax
	call	*8(%rax)
.L3940:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3941
	movq	(%rdi), %rax
	call	*8(%rax)
.L3941:
	leaq	.LC128(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L3938
	.p2align 4,,10
	.p2align 3
.L3973:
	movq	-1(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L3944
	movq	-1(%rax), %rcx
	testb	$8, 11(%rcx)
	je	.L3975
	movq	-1(%rdx), %rcx
	testb	$8, 11(%rcx)
	je	.L3976
	movl	11(%rdx), %ecx
	movq	%r13, %rsi
	addl	11(%rax), %ecx
	movl	$1, %r8d
	movq	%r9, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_ib@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L3948
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3948:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3977
.L3933:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3978
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3969:
	.cfi_restore_state
	movq	40960(%rsi), %rax
	movl	$558, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3934
	.p2align 4,,10
	.p2align 3
.L3975:
	leaq	.LC129(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3976:
	leaq	.LC130(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3974:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC128(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3939
	.p2align 4,,10
	.p2align 3
.L3972:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3937
	.p2align 4,,10
	.p2align 3
.L3977:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3933
.L3978:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29228:
	.size	_ZN2v88internalL33Stats_Runtime_ConstructConsStringEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_ConstructConsStringEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC131:
	.string	"V8.Runtime_Runtime_GetWasmExceptionId"
	.align 8
.LC132:
	.string	"args[1].IsWasmInstanceObject()"
	.section	.text._ZN2v88internalL32Stats_Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Stats_Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateE:
.LFB22571:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4026
.L3980:
	movq	_ZZN2v88internalL32Stats_Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateEE29trace_event_unique_atomic1008(%rip), %r13
	testq	%r13, %r13
	je	.L4027
.L3982:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L4028
.L3984:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r15
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L3988
.L3989:
	leaq	.LC66(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4027:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4029
.L3983:
	movq	%r13, _ZZN2v88internalL32Stats_Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateEE29trace_event_unique_atomic1008(%rip)
	jmp	.L3982
	.p2align 4,,10
	.p2align 3
.L3988:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3989
	movq	-8(%rbx), %rax
	testb	$1, %al
	jne	.L4030
.L3990:
	leaq	.LC132(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4028:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4031
.L3985:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3986
	movq	(%rdi), %rax
	call	*8(%rax)
.L3986:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3987
	movq	(%rdi), %rax
	call	*8(%rax)
.L3987:
	leaq	.LC131(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L3984
	.p2align 4,,10
	.p2align 3
.L4030:
	movq	-1(%rax), %rax
	cmpw	$1100, 11(%rax)
	jne	.L3990
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal20WasmExceptionPackage15GetExceptionTagEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L4032
.L3993:
	movq	88(%r12), %r13
.L4000:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L4004
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4004:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4033
.L3979:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4034
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4032:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$106, 11(%rax)
	jne	.L3993
	movq	-8(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	223(%rax), %rsi
	testq	%rdi, %rdi
	je	.L3995
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3996:
	movl	11(%rsi), %edx
	testl	%edx, %edx
	jle	.L3993
	movq	0(%r13), %rdi
	xorl	%edx, %edx
	jmp	.L3998
	.p2align 4,,10
	.p2align 3
.L3999:
	movq	(%rax), %rsi
	addq	$1, %rdx
	cmpl	%edx, 11(%rsi)
	jle	.L3993
.L3998:
	movq	15(%rsi,%rdx,8), %rcx
	movl	%edx, %r13d
	cmpq	%rcx, %rdi
	jne	.L3999
	salq	$32, %r13
	jmp	.L4000
	.p2align 4,,10
	.p2align 3
.L4026:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$576, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3980
	.p2align 4,,10
	.p2align 3
.L4033:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3979
	.p2align 4,,10
	.p2align 3
.L4031:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC131(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L3985
	.p2align 4,,10
	.p2align 3
.L4029:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L3983
	.p2align 4,,10
	.p2align 3
.L3995:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L4035
.L3997:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L3996
.L4035:
	movq	%r12, %rdi
	movq	%rsi, -168(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-168(%rbp), %rsi
	jmp	.L3997
.L4034:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22571:
	.size	_ZN2v88internalL32Stats_Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Stats_Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL37Stats_Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC133:
	.string	"V8.Runtime_Runtime_WasmNumInterpretedCalls"
	.section	.text._ZN2v88internalL37Stats_Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL37Stats_Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL37Stats_Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateE:
.LFB22687:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4078
.L4037:
	movq	_ZZN2v88internalL37Stats_Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1220(%rip), %r13
	testq	%r13, %r13
	je	.L4079
.L4039:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L4080
.L4041:
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L4045
.L4046:
	leaq	.LC95(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4079:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4081
.L4040:
	movq	%r13, _ZZN2v88internalL37Stats_Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1220(%rip)
	jmp	.L4039
	.p2align 4,,10
	.p2align 3
.L4045:
	movq	-1(%rax), %rdx
	cmpw	$1100, 11(%rdx)
	jne	.L4046
	movq	191(%rax), %rdx
	andq	$-262144, %rax
	xorl	%r13d, %r13d
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rdx
	je	.L4047
	leaq	-168(%rbp), %rdi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal13WasmDebugInfo19NumInterpretedCallsEv@PLT
	movq	%rax, %r13
	cmpq	$2147483647, %rax
	ja	.L4048
	movq	41112(%r12), %rdi
	salq	$32, %r13
	testq	%rdi, %rdi
	je	.L4049
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	.p2align 4,,10
	.p2align 3
.L4047:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L4057
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4057:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4082
.L4036:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4083
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4080:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4084
.L4042:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4043
	movq	(%rdi), %rax
	call	*8(%rax)
.L4043:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4044
	movq	(%rdi), %rax
	call	*8(%rax)
.L4044:
	leaq	.LC133(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L4041
	.p2align 4,,10
	.p2align 3
.L4049:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L4085
.L4051:
	movq	%r13, (%rax)
	jmp	.L4047
	.p2align 4,,10
	.p2align 3
.L4048:
	testq	%rax, %rax
	js	.L4053
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L4054:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L4047
	.p2align 4,,10
	.p2align 3
.L4078:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$637, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4037
	.p2align 4,,10
	.p2align 3
.L4082:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4036
	.p2align 4,,10
	.p2align 3
.L4084:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC133(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L4042
	.p2align 4,,10
	.p2align 3
.L4081:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L4040
	.p2align 4,,10
	.p2align 3
.L4053:
	shrq	%rax
	andl	$1, %r13d
	pxor	%xmm0, %xmm0
	orq	%r13, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L4054
	.p2align 4,,10
	.p2align 3
.L4085:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L4051
.L4083:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22687:
	.size	_ZN2v88internalL37Stats_Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateE, .-_ZN2v88internalL37Stats_Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Stats_Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC134:
	.string	"V8.Runtime_Runtime_WasmTierUpFunction"
	.align 8
.LC135:
	.string	"!native_module->compilation_state()->failed()"
	.section	.text._ZN2v88internalL32Stats_Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Stats_Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateE:
.LFB22699:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4120
.L4087:
	movq	_ZZN2v88internalL32Stats_Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1271(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4121
.L4089:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L4122
.L4091:
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L4095
.L4096:
	leaq	.LC95(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4121:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4123
.L4090:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1271(%rip)
	jmp	.L4089
	.p2align 4,,10
	.p2align 3
.L4095:
	movq	-1(%rax), %rdx
	cmpw	$1100, 11(%rdx)
	jne	.L4096
	movq	-8(%r13), %rcx
	testb	$1, %cl
	jne	.L4124
	movq	135(%rax), %rax
	sarq	$32, %rcx
	movl	$3, %r8d
	movq	%r12, %rsi
	movq	45752(%r12), %rdi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rbx
	movq	%rbx, %rdx
	call	_ZN2v88internal4wasm10WasmEngine15CompileFunctionEPNS0_7IsolateEPNS1_12NativeModuleEjNS1_13ExecutionTierE@PLT
	movq	520(%rbx), %rdi
	call	_ZNK2v88internal4wasm16CompilationState6failedEv@PLT
	testb	%al, %al
	jne	.L4125
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L4099
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4099:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4126
.L4086:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4127
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4122:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4128
.L4092:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4093
	movq	(%rdi), %rax
	call	*8(%rax)
.L4093:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4094
	movq	(%rdi), %rax
	call	*8(%rax)
.L4094:
	leaq	.LC134(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L4091
	.p2align 4,,10
	.p2align 3
.L4120:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$638, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4087
	.p2align 4,,10
	.p2align 3
.L4124:
	leaq	.LC96(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4125:
	leaq	.LC135(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4126:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4086
	.p2align 4,,10
	.p2align 3
.L4123:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L4090
	.p2align 4,,10
	.p2align 3
.L4128:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC134(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L4092
.L4127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22699:
	.size	_ZN2v88internalL32Stats_Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Stats_Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL25Stats_Runtime_GetCallableEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC136:
	.string	"V8.Runtime_Runtime_GetCallable"
	.section	.text._ZN2v88internalL25Stats_Runtime_GetCallableEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_GetCallableEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL25Stats_Runtime_GetCallableEiPmPNS0_7IsolateE.isra.0:
.LFB29282:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4162
.L4130:
	movq	_ZZN2v88internalL25Stats_Runtime_GetCallableEiPmPNS0_7IsolateEE28trace_event_unique_atomic599(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4163
.L4132:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L4164
.L4134:
	subq	$8, %rsp
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	addl	$1, 41104(%r12)
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	pushq	$0
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	xorl	%edx, %edx
	leaq	_ZN2v88internalL16call_as_functionERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate24SetCallAsFunctionHandlerEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS2_EE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L4165
.L4138:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L4166
.L4139:
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4142
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4142:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4167
.L4129:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4168
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4164:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4169
.L4135:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4136
	movq	(%rdi), %rax
	call	*8(%rax)
.L4136:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4137
	movq	(%rdi), %rax
	call	*8(%rax)
.L4137:
	leaq	.LC136(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r13, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L4134
	.p2align 4,,10
	.p2align 3
.L4163:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4170
.L4133:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_GetCallableEiPmPNS0_7IsolateEE28trace_event_unique_atomic599(%rip)
	jmp	.L4132
	.p2align 4,,10
	.p2align 3
.L4162:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$572, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4130
	.p2align 4,,10
	.p2align 3
.L4167:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4129
	.p2align 4,,10
	.p2align 3
.L4165:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L4138
	.p2align 4,,10
	.p2align 3
.L4166:
	movq	%rax, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rax
	jmp	.L4139
	.p2align 4,,10
	.p2align 3
.L4170:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L4133
	.p2align 4,,10
	.p2align 3
.L4169:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC136(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L4135
.L4168:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29282:
	.size	_ZN2v88internalL25Stats_Runtime_GetCallableEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL25Stats_Runtime_GetCallableEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_ConstructSlicedStringEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC137:
	.string	"V8.Runtime_Runtime_ConstructSlicedString"
	.align 8
.LC138:
	.string	"string->IsOneByteRepresentation()"
	.align 8
.LC139:
	.string	"index->value() < string->length()"
	.align 8
.LC140:
	.string	"sliced_string->IsSlicedString()"
	.section	.text._ZN2v88internalL35Stats_Runtime_ConstructSlicedStringEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_ConstructSlicedStringEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_ConstructSlicedStringEiPmPNS0_7IsolateE.isra.0:
.LFB29286:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4212
.L4172:
	movq	_ZZN2v88internalL35Stats_Runtime_ConstructSlicedStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic147(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4213
.L4174:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L4214
.L4176:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L4215
.L4180:
	leaq	.LC18(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4213:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4216
.L4175:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_ConstructSlicedStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic147(%rip)
	jmp	.L4174
	.p2align 4,,10
	.p2align 3
.L4215:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L4180
	movq	-8(%r13), %rdx
	testb	$1, %dl
	jne	.L4217
	movq	-1(%rax), %rcx
	testb	$8, 11(%rcx)
	je	.L4218
	movl	11(%rax), %ecx
	sarq	$32, %rdx
	cmpl	%edx, %ecx
	jle	.L4219
	testq	%rdx, %rdx
	je	.L4186
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	%rax, %r13
	movq	(%rax), %rax
.L4186:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L4220
.L4188:
	leaq	.LC140(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4214:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4221
.L4177:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4178
	movq	(%rdi), %rax
	call	*8(%rax)
.L4178:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4179
	movq	(%rdi), %rax
	call	*8(%rax)
.L4179:
	leaq	.LC137(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L4176
	.p2align 4,,10
	.p2align 3
.L4220:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$3, %ax
	jne	.L4188
	movq	0(%r13), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4190
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4190:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4222
.L4171:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4223
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4212:
	.cfi_restore_state
	movq	40960(%rsi), %rax
	movl	$560, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4172
	.p2align 4,,10
	.p2align 3
.L4217:
	leaq	.LC96(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4218:
	leaq	.LC138(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4219:
	leaq	.LC139(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4221:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC137(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L4177
	.p2align 4,,10
	.p2align 3
.L4216:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L4175
	.p2align 4,,10
	.p2align 3
.L4222:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4171
.L4223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29286:
	.size	_ZN2v88internalL35Stats_Runtime_ConstructSlicedStringEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_ConstructSlicedStringEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC141:
	.string	"V8.Runtime_Runtime_SetWasmCompileControls"
	.section	.rodata._ZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC142:
	.string	"args.length() == 2"
	.section	.text._ZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE:
.LFB22486:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -112(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4278
.L4225:
	movq	_ZZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateEE28trace_event_unique_atomic626(%rip), %r13
	testq	%r13, %r13
	je	.L4279
.L4227:
	movq	$0, -176(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L4280
.L4229:
	movq	%r12, -192(%rbp)
	movq	41088(%r12), %r14
	addl	$1, 41104(%r12)
	movq	41096(%r12), %r13
	cmpl	$2, %r15d
	jne	.L4281
	testb	$1, (%rbx)
	jne	.L4282
	movq	-8(%rbx), %rax
	testb	$1, %al
	jne	.L4283
.L4235:
	leaq	.LC109(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4279:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4284
.L4228:
	movq	%r13, _ZZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateEE28trace_event_unique_atomic626(%rip)
	jmp	.L4227
	.p2align 4,,10
	.p2align 3
.L4280:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4285
.L4230:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4231
	movq	(%rdi), %rax
	call	*8(%rax)
.L4231:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4232
	movq	(%rdi), %rax
	call	*8(%rax)
.L4232:
	leaq	.LC141(%rip), %rax
	movq	%r13, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-168(%rbp), %rax
	movq	%r14, -152(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L4229
	.p2align 4,,10
	.p2align 3
.L4283:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L4235
	testb	$-2, 43(%rax)
	jne	.L4235
	cmpq	%rax, 112(%r12)
	movzbl	_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %eax
	sete	%r15b
	cmpb	$2, %al
	je	.L4239
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %r8
	leaq	8+_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r8, %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rdi
	movq	%rax, %xmm1
	movq	%r8, -200(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L4239
	movq	-200(%rbp), %r8
	movl	$3, %edx
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	*%rax
.L4239:
	leaq	8+_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %eax
	testb	%al, %al
	je	.L4241
	leaq	8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rax
.L4242:
	movq	16+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdx
	testq	%rdx, %rdx
	je	.L4255
	movq	-192(%rbp), %rdi
	leaq	8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rsi
	jmp	.L4245
	.p2align 4,,10
	.p2align 3
.L4286:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L4246
.L4245:
	cmpq	%rdi, 32(%rdx)
	jnb	.L4286
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L4245
.L4246:
	cmpq	%rax, %rsi
	je	.L4244
	cmpq	%rdi, 32(%rsi)
	jbe	.L4249
.L4244:
	leaq	-192(%rbp), %rax
	leaq	_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdi
	leaq	-184(%rbp), %rdx
	movq	%rax, -184(%rbp)
	call	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_NS0_8internal12_GLOBAL__N_119WasmCompileControlsEESt10_Select1stIS8_ESt4lessIS2_ESaIS8_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESJ_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0
	movq	-192(%rbp), %rdi
	movq	%rax, %rsi
.L4249:
	movb	%r15b, 44(%rsi)
	movq	(%rbx), %rax
	sarq	$32, %rax
	movl	%eax, 40(%rsi)
	leaq	_ZN2v88internal12_GLOBAL__N_118WasmModuleOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	call	_ZN2v87Isolate21SetWasmModuleCallbackEPFbRKNS_20FunctionCallbackInfoINS_5ValueEEEE@PLT
	leaq	8+_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rdi
	movq	88(%r12), %r15
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L4250
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4250:
	leaq	-176(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4287
.L4224:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4288
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4241:
	.cfi_restore_state
	leaq	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	movl	%eax, %r8d
	leaq	8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rax
	testl	%r8d, %r8d
	je	.L4242
	leaq	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdi
	movq	%rax, 24+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	%rax, 32+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	%rax, -200(%rbp)
	movl	$0, 8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	$0, 16+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	$0, 40+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	call	__cxa_guard_release@PLT
	movq	-200(%rbp), %rax
	jmp	.L4242
	.p2align 4,,10
	.p2align 3
.L4281:
	leaq	.LC142(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4278:
	movq	40960(%rdx), %rax
	leaq	-136(%rbp), %rsi
	movl	$627, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4225
	.p2align 4,,10
	.p2align 3
.L4282:
	leaq	.LC98(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4255:
	leaq	8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rsi
	jmp	.L4244
	.p2align 4,,10
	.p2align 3
.L4287:
	leaq	-136(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4224
	.p2align 4,,10
	.p2align 3
.L4285:
	subq	$8, %rsp
	leaq	-96(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC141(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L4230
	.p2align 4,,10
	.p2align 3
.L4284:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L4228
.L4288:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22486:
	.size	_ZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Stats_Runtime_GetUndetectableEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC143:
	.string	"V8.Runtime_Runtime_GetUndetectable"
	.section	.text._ZN2v88internalL29Stats_Runtime_GetUndetectableEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_GetUndetectableEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_GetUndetectableEiPmPNS0_7IsolateE.isra.0:
.LFB29287:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4325
.L4290:
	movq	_ZZN2v88internalL29Stats_Runtime_GetUndetectableEiPmPNS0_7IsolateEE28trace_event_unique_atomic574(%rip), %r13
	testq	%r13, %r13
	je	.L4326
.L4292:
	movq	$0, -144(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L4327
.L4294:
	addl	$1, 41104(%r12)
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v814ObjectTemplate3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate18MarkAsUndetectableEv@PLT
	xorl	%edx, %edx
	leaq	_ZN2v88internalL10ReturnThisERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v814ObjectTemplate24SetCallAsFunctionHandlerEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS2_EE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movq	%rax, %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L4298
	movq	(%rax), %r13
.L4298:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L4302
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4302:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4328
.L4289:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4329
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4327:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%ebx, %ebx
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4330
.L4295:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4296
	movq	(%rdi), %rax
	call	*8(%rax)
.L4296:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4297
	movq	(%rdi), %rax
	call	*8(%rax)
.L4297:
	leaq	.LC143(%rip), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%rbx, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L4294
	.p2align 4,,10
	.p2align 3
.L4326:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4331
.L4293:
	movq	%r13, _ZZN2v88internalL29Stats_Runtime_GetUndetectableEiPmPNS0_7IsolateEE28trace_event_unique_atomic574(%rip)
	jmp	.L4292
	.p2align 4,,10
	.p2align 3
.L4325:
	movq	40960(%rdi), %rax
	leaq	-104(%rbp), %rsi
	movl	$575, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4290
	.p2align 4,,10
	.p2align 3
.L4328:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4289
	.p2align 4,,10
	.p2align 3
.L4331:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L4293
	.p2align 4,,10
	.p2align 3
.L4330:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC143(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %rbx
	addq	$64, %rsp
	jmp	.L4295
.L4329:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29287:
	.size	_ZN2v88internalL29Stats_Runtime_GetUndetectableEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_GetUndetectableEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC144:
	.string	"V8.Runtime_Runtime_DeserializeWasmModule"
	.section	.rodata._ZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC145:
	.string	"args[0].IsJSArrayBuffer()"
.LC146:
	.string	"args[1].IsJSTypedArray()"
.LC147:
	.string	"!buffer->was_detached()"
.LC148:
	.string	"!wire_bytes->WasDetached()"
	.section	.text._ZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE.isra.0:
.LFB29289:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4373
.L4333:
	movq	_ZZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1150(%rip), %r13
	testq	%r13, %r13
	je	.L4374
.L4335:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L4375
.L4337:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rdx
	testb	$1, %dl
	jne	.L4376
.L4341:
	leaq	.LC145(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4374:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4377
.L4336:
	movq	%r13, _ZZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1150(%rip)
	jmp	.L4335
	.p2align 4,,10
	.p2align 3
.L4376:
	movq	-1(%rdx), %rax
	cmpw	$1059, 11(%rax)
	jne	.L4341
	movq	-8(%rbx), %rax
	testb	$1, %al
	jne	.L4378
.L4342:
	leaq	.LC146(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4375:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4379
.L4338:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4339
	movq	(%rdi), %rax
	call	*8(%rax)
.L4339:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4340
	movq	(%rdi), %rax
	call	*8(%rax)
.L4340:
	leaq	.LC144(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L4337
	.p2align 4,,10
	.p2align 3
.L4378:
	movq	-1(%rax), %rcx
	cmpw	$1086, 11(%rcx)
	jne	.L4342
	movl	39(%rdx), %edx
	andl	$4, %edx
	jne	.L4380
	movq	23(%rax), %rdx
	movl	39(%rdx), %edx
	andl	$4, %edx
	jne	.L4381
	leaq	-168(%rbp), %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal12JSTypedArray9GetBufferEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r8
	movq	-8(%rbx), %rax
	movq	(%r8), %rdx
	movq	31(%rax), %rcx
	movq	39(%rax), %r8
	addq	31(%rdx), %rcx
	movq	(%rbx), %rdx
	movslq	23(%rdx), %r9
	movq	31(%rdx), %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal4wasm23DeserializeNativeModuleEPNS0_7IsolateENS0_6VectorIKhEES6_@PLT
	testq	%rax, %rax
	je	.L4382
	movq	(%rax), %r15
.L4348:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L4351
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4351:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4383
.L4332:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4384
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4382:
	.cfi_restore_state
	movq	88(%r12), %r15
	jmp	.L4348
	.p2align 4,,10
	.p2align 3
.L4373:
	movq	40960(%rsi), %rax
	movl	$565, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4333
	.p2align 4,,10
	.p2align 3
.L4380:
	leaq	.LC147(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4381:
	leaq	.LC148(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4379:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC144(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L4338
	.p2align 4,,10
	.p2align 3
.L4377:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L4336
	.p2align 4,,10
	.p2align 3
.L4383:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4332
.L4384:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29289:
	.size	_ZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC149:
	.string	"V8.Runtime_Runtime_IsLiftoffFunction"
	.align 8
.LC150:
	.string	"WasmExportedFunction::IsWasmExportedFunction(*function)"
	.section	.text._ZN2v88internalL31Stats_Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateE.isra.0:
.LFB29290:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -192(%rbp)
	movq	$0, -160(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4425
.L4386:
	movq	_ZZN2v88internalL31Stats_Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1283(%rip), %r13
	testq	%r13, %r13
	je	.L4426
.L4388:
	movq	$0, -224(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L4427
.L4390:
	movq	41088(%r12), %rax
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	movq	(%rbx), %rdi
	movq	%rax, -232(%rbp)
	testb	$1, %dil
	jne	.L4428
.L4394:
	leaq	.LC34(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4426:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4429
.L4389:
	movq	%r13, _ZZN2v88internalL31Stats_Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1283(%rip)
	jmp	.L4388
	.p2align 4,,10
	.p2align 3
.L4428:
	movq	-1(%rdi), %rax
	cmpw	$1105, 11(%rax)
	jne	.L4394
	call	_ZN2v88internal20WasmExportedFunction22IsWasmExportedFunctionENS0_6ObjectE@PLT
	testb	%al, %al
	je	.L4430
	leaq	-144(%rbp), %r14
	movq	(%rbx), %rax
	movq	%r14, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal20WasmExportedFunction8instanceEv@PLT
	movq	%r14, %rdi
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r9
	movq	(%rbx), %rax
	movq	%r9, -240(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal20WasmExportedFunction14function_indexEv@PLT
	movq	%r14, %rdi
	movl	%eax, %r15d
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	-240(%rbp), %r9
	movl	%r15d, %esi
	movq	%r9, %rdi
	call	_ZNK2v88internal4wasm12NativeModule7GetCodeEj@PLT
	testq	%rax, %rax
	je	.L4397
	cmpb	$2, 136(%rax)
	jne	.L4397
	movq	112(%r12), %r15
.L4398:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	-232(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L4401
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4401:
	leaq	-224(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4431
.L4385:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4432
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4397:
	.cfi_restore_state
	movq	120(%r12), %r15
	jmp	.L4398
	.p2align 4,,10
	.p2align 3
.L4427:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4433
.L4391:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4392
	movq	(%rdi), %rax
	call	*8(%rax)
.L4392:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4393
	movq	(%rdi), %rax
	call	*8(%rax)
.L4393:
	leaq	.LC149(%rip), %rax
	movq	%r13, -216(%rbp)
	movq	%rax, -208(%rbp)
	leaq	-216(%rbp), %rax
	movq	%r14, -200(%rbp)
	movq	%rax, -224(%rbp)
	jmp	.L4390
	.p2align 4,,10
	.p2align 3
.L4425:
	movq	40960(%rsi), %rax
	movl	$608, %edx
	leaq	-184(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -192(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4386
	.p2align 4,,10
	.p2align 3
.L4430:
	leaq	.LC150(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4431:
	leaq	-184(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4385
	.p2align 4,,10
	.p2align 3
.L4433:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC149(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L4391
	.p2align 4,,10
	.p2align 3
.L4429:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L4389
.L4432:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29290:
	.size	_ZN2v88internalL31Stats_Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL43Stats_Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC151:
	.string	"V8.Runtime_Runtime_CompleteInobjectSlackTracking"
	.section	.text._ZN2v88internalL43Stats_Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Stats_Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL43Stats_Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateE.isra.0:
.LFB29301:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4466
.L4435:
	movq	_ZZN2v88internalL43Stats_Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1298(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4467
.L4437:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L4468
.L4439:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L4469
.L4443:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4467:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4470
.L4438:
	movq	%rbx, _ZZN2v88internalL43Stats_Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1298(%rip)
	jmp	.L4437
	.p2align 4,,10
	.p2align 3
.L4469:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L4443
	movq	-1(%rax), %rax
	leaq	-152(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal3Map29CompleteInobjectSlackTrackingEPNS0_7IsolateE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4445
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4445:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4471
.L4434:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4472
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4468:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4473
.L4440:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4441
	movq	(%rdi), %rax
	call	*8(%rax)
.L4441:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4442
	movq	(%rdi), %rax
	call	*8(%rax)
.L4442:
	leaq	.LC151(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L4439
	.p2align 4,,10
	.p2align 3
.L4466:
	movq	40960(%rsi), %rax
	movl	$557, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4435
	.p2align 4,,10
	.p2align 3
.L4471:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4434
	.p2align 4,,10
	.p2align 3
.L4470:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L4438
	.p2align 4,,10
	.p2align 3
.L4473:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC151(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L4440
.L4472:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29301:
	.size	_ZN2v88internalL43Stats_Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL43Stats_Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE,"axG",@progbits,_ZN2v88internal21RuntimeCallTimerScopeC5EPNS0_7IsolateENS0_20RuntimeCallCounterIdE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.type	_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE, @function
_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE:
.LFB20106:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4476
	ret
	.p2align 4,,10
	.p2align 3
.L4476:
	movq	40960(%rsi), %r8
	leaq	8(%rdi), %rsi
	addq	$23240, %r8
	movq	%r8, (%rdi)
	movq	%r8, %rdi
	jmp	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	.cfi_endproc
.LFE20106:
	.size	_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE, .-_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.weak	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.set	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE,_ZN2v88internal21RuntimeCallTimerScopeC2EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	.section	.text._ZN2v88internal33Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE:
.LFB22418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4481
	addl	$1, 41104(%rdx)
	movq	41024(%rdx), %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal9StubCache5ClearEv@PLT
	movq	41032(%r12), %rdi
	call	_ZN2v88internal9StubCache5ClearEv@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4477
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4477:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4481:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Stats_Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22418:
	.size	_ZN2v88internal33Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_ConstructDoubleEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_ConstructDoubleEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_ConstructDoubleEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_ConstructDoubleEiPmPNS0_7IsolateE:
.LFB22421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4533
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L4534
	shrq	$32, %rdx
	movq	%rdx, %rax
.L4488:
	movq	-8(%rdi), %rdx
	testb	$1, %dl
	je	.L4530
.L4541:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L4535
	movq	7(%rdx), %rdi
	movsd	.LC8(%rip), %xmm2
	movq	%rdi, %xmm1
	andpd	.LC7(%rip), %xmm1
	movq	%rdi, %xmm0
	ucomisd	%xmm1, %xmm2
	jnb	.L4536
.L4502:
	movabsq	$9218868437227405312, %rdx
	andq	%rdi, %rdx
	je	.L4501
	movq	%rdi, %rsi
	xorl	%edx, %edx
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L4537
	cmpl	$31, %ecx
	jg	.L4501
	movabsq	$4503599627370495, %rdx
	movq	%rdx, %rsi
	addq	$1, %rdx
	andq	%rdi, %rsi
	addq	%rsi, %rdx
	salq	%cl, %rdx
	movl	%edx, %edx
.L4508:
	sarq	$63, %rdi
	orl	$1, %edi
	imull	%edi, %edx
.L4501:
	salq	$32, %rax
	xorl	%esi, %esi
	movq	%r12, %rdi
	orq	%rdx, %rax
	movq	%rax, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4482
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4482:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4534:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L4538
	movq	7(%rdx), %rdx
	movsd	.LC8(%rip), %xmm2
	movq	%rdx, %xmm1
	andpd	.LC7(%rip), %xmm1
	movq	%rdx, %xmm0
	ucomisd	%xmm1, %xmm2
	jb	.L4489
	movsd	.LC9(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jnb	.L4539
.L4489:
	movabsq	$9218868437227405312, %rcx
	testq	%rcx, %rdx
	je	.L4488
	movq	%rdx, %rsi
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %ecx
	subl	$1075, %ecx
	js	.L4540
	cmpl	$31, %ecx
	jg	.L4488
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rsi
	andq	%rdx, %rax
	addq	%rsi, %rax
	salq	%cl, %rax
	movl	%eax, %eax
.L4495:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %eax
	movq	-8(%rdi), %rdx
	testb	$1, %dl
	jne	.L4541
.L4530:
	shrq	$32, %rdx
	jmp	.L4501
	.p2align 4,,10
	.p2align 3
.L4536:
	movsd	.LC9(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L4502
	comisd	.LC10(%rip), %xmm0
	jb	.L4502
	cvttsd2sil	%xmm0, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L4502
	jne	.L4502
	movl	%edx, %edx
	jmp	.L4501
	.p2align 4,,10
	.p2align 3
.L4539:
	comisd	.LC10(%rip), %xmm0
	jb	.L4489
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L4489
	jne	.L4489
	movl	%ecx, %eax
	jmp	.L4488
	.p2align 4,,10
	.p2align 3
.L4540:
	cmpl	$-52, %ecx
	jl	.L4488
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rax
	addq	%rcx, %rax
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rax
	jmp	.L4495
	.p2align 4,,10
	.p2align 3
.L4537:
	cmpl	$-52, %ecx
	jl	.L4501
	movabsq	$4503599627370495, %rdx
	movabsq	$4503599627370496, %rcx
	andq	%rdi, %rdx
	addq	%rcx, %rdx
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rdx
	jmp	.L4508
	.p2align 4,,10
	.p2align 3
.L4533:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L4538:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4535:
	leaq	.LC11(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22421:
	.size	_ZN2v88internal23Runtime_ConstructDoubleEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_ConstructDoubleEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_ConstructConsStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_ConstructConsStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_ConstructConsStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_ConstructConsStringEiPmPNS0_7IsolateE:
.LFB22424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4552
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L4544
.L4545:
	leaq	.LC18(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4544:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L4545
	movq	-8(%rsi), %rdx
	leaq	-8(%rsi), %r10
	testb	$1, %dl
	jne	.L4553
.L4546:
	leaq	.LC124(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4553:
	movq	-1(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L4546
	movq	-1(%rax), %rcx
	testb	$8, 11(%rcx)
	je	.L4554
	movq	-1(%rdx), %rcx
	testb	$8, 11(%rcx)
	je	.L4555
	movl	11(%rax), %ecx
	movl	$1, %r8d
	addl	11(%rdx), %ecx
	movq	%r12, %rdi
	movq	%r10, %rdx
	call	_ZN2v88internal7Factory13NewConsStringENS0_6HandleINS0_6StringEEES4_ib@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4542
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4542:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4552:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r9, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_ConstructConsStringEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L4554:
	.cfi_restore_state
	leaq	.LC129(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4555:
	leaq	.LC130(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22424:
	.size	_ZN2v88internal27Runtime_ConstructConsStringEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_ConstructConsStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_ConstructSlicedStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_ConstructSlicedStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_ConstructSlicedStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_ConstructSlicedStringEiPmPNS0_7IsolateE:
.LFB22427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testl	%edx, %edx
	jne	.L4571
	movq	41088(%r12), %r13
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	(%rsi), %rcx
	testb	$1, %cl
	jne	.L4572
.L4558:
	leaq	.LC18(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4572:
	movq	-1(%rcx), %rax
	cmpw	$63, 11(%rax)
	ja	.L4558
	movq	-8(%rsi), %rdx
	testb	$1, %dl
	jne	.L4573
	movq	-1(%rcx), %rax
	testb	$8, 11(%rax)
	je	.L4574
	movl	11(%rcx), %r9d
	sarq	$32, %rdx
	cmpl	%r9d, %edx
	jge	.L4575
	testq	%rdx, %rdx
	je	.L4564
	movl	%r9d, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewProperSubStringENS0_6HandleINS0_6StringEEEii@PLT
	movq	(%rax), %rcx
	movq	%rax, %r8
.L4564:
	movq	-1(%rcx), %rax
	cmpw	$63, 11(%rax)
	jbe	.L4576
.L4566:
	leaq	.LC140(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4576:
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$3, %ax
	jne	.L4566
	movq	(%r8), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4556
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4556:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4571:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rsi
	movq	%r8, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL35Stats_Runtime_ConstructSlicedStringEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L4573:
	.cfi_restore_state
	leaq	.LC96(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4574:
	leaq	.LC138(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4575:
	leaq	.LC139(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22427:
	.size	_ZN2v88internal29Runtime_ConstructSlicedStringEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_ConstructSlicedStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateE:
.LFB22430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4594
	movl	41104(%rdx), %eax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L4579
.L4593:
	movq	88(%r12), %r13
	movq	%rbx, %rdx
.L4589:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L4577
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4577:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4579:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L4593
	movq	47(%rdx), %rax
	cmpl	$67, 59(%rax)
	jne	.L4595
.L4587:
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L4589
	.p2align 4,,10
	.p2align 3
.L4594:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L4595:
	.cfi_restore_state
	movabsq	$287762808832, %rcx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L4587
	testb	$1, %al
	jne	.L4596
.L4584:
	movq	47(%rdx), %rax
	testb	$62, 43(%rax)
	jne	.L4587
	movq	47(%rdx), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	jne	.L4587
	movq	(%rdi), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE@PLT
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L4589
	.p2align 4,,10
	.p2align 3
.L4596:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L4587
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L4584
	jmp	.L4587
	.cfi_endproc
.LFE22430:
	.size	_ZN2v88internal26Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_DeoptimizeNowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_DeoptimizeNowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_DeoptimizeNowEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_DeoptimizeNowEiPmPNS0_7IsolateE:
.LFB22433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1472, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4625
	addl	$1, 41104(%rdx)
	leaq	-1488(%rbp), %r14
	movq	%rdx, %rsi
	movq	41088(%rdx), %r13
	movq	%r14, %rdi
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -72(%rbp)
	je	.L4624
	movq	%r14, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4624
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L4601
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	testq	%rax, %rax
	jne	.L4626
	.p2align 4,,10
	.p2align 3
.L4624:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r14
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4597
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4597:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4627
	addq	$1472, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4601:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L4628
.L4603:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
.L4604:
	movq	47(%rsi), %rdx
	leaq	47(%rsi), %rcx
	cmpl	$67, 59(%rdx)
	je	.L4624
	movq	23(%rsi), %rdx
	movabsq	$287762808832, %rsi
	movq	7(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L4624
	testb	$1, %dl
	jne	.L4629
.L4608:
	movq	(%rcx), %rdx
	testb	$62, 43(%rdx)
	jne	.L4624
	movq	(%rcx), %rdx
	movq	31(%rdx), %rdx
	movl	15(%rdx), %edx
	andl	$1, %edx
	jne	.L4624
	movq	(%rax), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal11Deoptimizer18DeoptimizeFunctionENS0_10JSFunctionENS0_4CodeE@PLT
	jmp	.L4624
	.p2align 4,,10
	.p2align 3
.L4626:
	movq	(%rax), %rsi
	jmp	.L4604
	.p2align 4,,10
	.p2align 3
.L4625:
	movq	%rdx, %rdi
	call	_ZN2v88internalL27Stats_Runtime_DeoptimizeNowEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r14
	jmp	.L4597
	.p2align 4,,10
	.p2align 3
.L4628:
	movq	%r12, %rdi
	movq	%rsi, -1496(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1496(%rbp), %rsi
	jmp	.L4603
	.p2align 4,,10
	.p2align 3
.L4629:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L4624
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L4608
	jmp	.L4624
.L4627:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22433:
	.size	_ZN2v88internal21Runtime_DeoptimizeNowEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_DeoptimizeNowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_RunningInSimulatorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_RunningInSimulatorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_RunningInSimulatorEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_RunningInSimulatorEiPmPNS0_7IsolateE:
.LFB22439:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4632
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4632:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL32Stats_Runtime_RunningInSimulatorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22439:
	.size	_ZN2v88internal26Runtime_RunningInSimulatorEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_RunningInSimulatorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_ICsAreEnabledEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_ICsAreEnabledEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_ICsAreEnabledEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_ICsAreEnabledEiPmPNS0_7IsolateE:
.LFB22442:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4637
	cmpb	$0, _ZN2v88internal11FLAG_use_icE(%rip)
	je	.L4635
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4635:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4637:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL27Stats_Runtime_ICsAreEnabledEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22442:
	.size	_ZN2v88internal21Runtime_ICsAreEnabledEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_ICsAreEnabledEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal42Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal42Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal42Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateE, @function
_ZN2v88internal42Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateE:
.LFB22445:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4642
	cmpq	$0, 45416(%rdx)
	je	.L4640
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4640:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4642:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL48Stats_Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22445:
	.size	_ZN2v88internal42Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateE, .-_ZN2v88internal42Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE:
.LFB22448:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4745
	movq	41088(%rdx), %rcx
	movq	41096(%rdx), %rbx
	movl	41104(%rdx), %edx
	leal	1(%rdx), %eax
	movl	%eax, 41104(%r12)
	leal	-1(%rdi), %eax
	cmpl	$1, %eax
	ja	.L4746
	movq	(%rsi), %rax
	movq	%rbx, %rsi
	testb	$1, %al
	jne	.L4747
.L4650:
	movq	88(%r12), %r13
.L4653:
	movq	%rcx, 41088(%r12)
	movl	%edx, 41104(%r12)
	cmpq	%rsi, %rbx
	je	.L4643
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4643:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4748
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4746:
	.cfi_restore_state
	movl	%edx, 41104(%r12)
	movq	88(%r12), %r13
	jmp	.L4643
	.p2align 4,,10
	.p2align 3
.L4747:
	movq	-1(%rax), %rsi
	cmpw	$1105, 11(%rsi)
	je	.L4649
	movq	%rbx, %rsi
	jmp	.L4650
	.p2align 4,,10
	.p2align 3
.L4745:
	call	_ZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L4643
	.p2align 4,,10
	.p2align 3
.L4649:
	movq	23(%rax), %rsi
	movl	47(%rsi), %esi
	andl	$4096, %esi
	je	.L4749
	movq	23(%rax), %r15
	movq	%r15, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	7(%r15), %rdx
	testb	$1, %dl
	jne	.L4654
.L4657:
	movq	7(%r15), %rdx
	testb	$1, %dl
	jne	.L4750
.L4655:
	xorl	%edx, %edx
.L4666:
	movabsq	$287762808832, %rsi
	movq	7(%r15), %rax
	cmpq	%rsi, %rax
	je	.L4667
	testb	$1, %al
	jne	.L4751
	movq	%rdx, -80(%rbp)
.L4743:
	movb	$1, -72(%rbp)
.L4672:
	cmpb	$0, _ZN2v88internal8FLAG_optE(%rip)
	je	.L4698
	movq	(%r14), %r8
	movq	23(%r8), %rax
	movl	47(%rax), %eax
	testl	$15728640, %eax
	je	.L4675
	movq	23(%r8), %rax
	movl	47(%rax), %eax
	shrl	$20, %eax
	andl	$15, %eax
	cmpb	$11, %al
	je	.L4698
.L4675:
	movq	23(%r8), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L4752
.L4677:
	cmpb	$0, _ZN2v88internal27FLAG_testing_d8_test_runnerE(%rip)
	jne	.L4753
.L4680:
	movq	(%r14), %rax
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	jne	.L4754
.L4683:
	movabsq	$287762808832, %rsi
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L4695
	testb	$1, %dl
	jne	.L4689
.L4692:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L4755
.L4695:
	leaq	-88(%rbp), %r15
	cmpl	$2, %r13d
	je	.L4756
.L4697:
	xorl	%r13d, %r13d
	cmpb	$0, _ZN2v88internal14FLAG_trace_optE(%rip)
	jne	.L4757
.L4703:
	movq	(%r14), %r8
	movq	47(%r8), %rax
	cmpl	$67, 59(%rax)
	jne	.L4758
.L4705:
	movl	$57, %esi
	leaq	41184(%r12), %rdi
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	-112(%rbp), %r8
	movq	(%rax), %rdx
	leaq	47(%r8), %rsi
	movq	%rdx, 47(%r8)
	testb	$1, %dl
	movq	-104(%rbp), %rcx
	je	.L4709
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L4709
	movq	%r8, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-104(%rbp), %rcx
.L4709:
	movq	%r14, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE@PLT
	movq	(%r14), %rax
	movl	%r13d, %esi
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE@PLT
	movq	88(%r12), %r13
	movq	-104(%rbp), %rcx
	movl	41104(%r12), %eax
	movq	41096(%r12), %rsi
	leal	-1(%rax), %edx
	jmp	.L4653
	.p2align 4,,10
	.p2align 3
.L4749:
	movq	88(%r12), %r13
	movq	%rbx, %rsi
	jmp	.L4653
.L4750:
	movq	-1(%rdx), %rdx
	cmpw	$91, 11(%rdx)
	jne	.L4655
.L4660:
	movq	31(%r15), %rdx
	testb	$1, %dl
	jne	.L4759
.L4658:
	movq	7(%r15), %rdx
	testb	$1, %dl
	jne	.L4760
.L4662:
	movq	7(%r15), %rdx
	movq	7(%rdx), %r8
.L4661:
	movq	3520(%rax), %rdi
	leaq	-37592(%rax), %rsi
	testq	%rdi, %rdi
	je	.L4663
	movq	%r8, %rsi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L4666
.L4754:
	movabsq	$287762808832, %rsi
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L4683
	testb	$1, %dl
	jne	.L4761
.L4684:
	movq	47(%rax), %rdx
	testb	$62, 43(%rdx)
	jne	.L4683
	movq	47(%rax), %rdx
	movq	31(%rdx), %rdx
	movl	15(%rdx), %edx
	andb	$1, %dl
	jne	.L4683
.L4687:
	cmpb	$0, _ZN2v88internal27FLAG_testing_d8_test_runnerE(%rip)
	je	.L4698
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal24PendingOptimizationTable20FunctionWasOptimizedEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	-104(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L4698:
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rsi
	leal	-1(%rax), %edx
	jmp	.L4653
	.p2align 4,,10
	.p2align 3
.L4667:
	movq	%rdx, -80(%rbp)
.L4742:
	leaq	-80(%rbp), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rcx, -104(%rbp)
	movb	$0, -72(%rbp)
	call	_ZN2v88internal8Compiler7CompileENS0_6HandleINS0_10JSFunctionEEENS1_18ClearExceptionFlagEPNS0_15IsCompiledScopeE@PLT
	movq	-104(%rbp), %rcx
	testb	%al, %al
	jne	.L4672
	jmp	.L4698
	.p2align 4,,10
	.p2align 3
.L4654:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L4657
	jmp	.L4660
.L4751:
	movq	-1(%rax), %rsi
	cmpw	$165, 11(%rsi)
	je	.L4667
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	movq	%rdx, -80(%rbp)
	jne	.L4743
	jmp	.L4742
.L4663:
	movq	41088(%rsi), %rdx
	cmpq	41096(%rsi), %rdx
	je	.L4762
.L4665:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rsi)
	movq	%r8, (%rdx)
	jmp	.L4666
.L4752:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L4677
	jmp	.L4698
.L4759:
	movq	-1(%rdx), %rsi
	cmpw	$86, 11(%rsi)
	jne	.L4658
	movq	39(%rdx), %rsi
	testb	$1, %sil
	je	.L4658
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L4658
	movq	31(%rdx), %r8
	jmp	.L4661
.L4760:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L4662
	movq	7(%r15), %r8
	jmp	.L4661
.L4753:
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal24PendingOptimizationTable21MarkedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	-104(%rbp), %rcx
	jmp	.L4680
.L4758:
	movabsq	$287762808832, %rdx
	movq	23(%r8), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L4744
	testb	$1, %al
	je	.L4709
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L4744
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L4709
.L4744:
	movq	(%r14), %r8
	jmp	.L4705
.L4755:
	movq	15(%rax), %rax
	testb	$1, %al
	je	.L4695
	cmpl	$3, %eax
	je	.L4695
	andq	$-3, %rax
	je	.L4695
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	jne	.L4695
	jmp	.L4687
.L4761:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L4683
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L4684
	jmp	.L4683
.L4762:
	movq	%rsi, %rdi
	movq	%r8, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %r8
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L4665
.L4756:
	movq	-8(%r14), %rax
	testb	$1, %al
	je	.L4698
	movq	%r15, %rdi
	movq	%rcx, -104(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal10HeapObject8map_wordEPNS0_7IsolateE.isra.0
	movq	-104(%rbp), %rcx
	cmpw	$63, 11(%rax)
	ja	.L4698
	movq	-8(%r14), %rax
	leaq	.LC112(%rip), %rsi
	movl	$10, %edx
	movq	%rcx, -104(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movq	-104(%rbp), %rcx
	testb	%al, %al
	je	.L4697
	cmpq	$0, 45416(%r12)
	je	.L4697
	cmpb	$0, _ZN2v88internal14FLAG_trace_optE(%rip)
	movl	$1, %r13d
	je	.L4703
	leaq	.LC116(%rip), %rdi
	xorl	%eax, %eax
	movq	%rcx, -104(%rbp)
	movl	$1, %r13d
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r14), %rax
	movq	stdout(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movq	-104(%rbp), %rcx
	leaq	.LC112(%rip), %rsi
.L4713:
	leaq	.LC115(%rip), %rdi
	xorl	%eax, %eax
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-104(%rbp), %rcx
	jmp	.L4703
.L4757:
	leaq	.LC116(%rip), %rdi
	xorl	%eax, %eax
	movq	%rcx, -104(%rbp)
	xorl	%r13d, %r13d
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	(%r14), %rax
	movq	stdout(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movq	-104(%rbp), %rcx
	leaq	.LC113(%rip), %rsi
	jmp	.L4713
.L4689:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L4695
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L4692
	jmp	.L4695
.L4748:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22448:
	.size	_ZN2v88internal34Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal39Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal39Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal39Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal39Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateE:
.LFB22456:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4776
	movl	41104(%rdx), %eax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	leal	1(%rax), %edx
	movl	%edx, 41104(%r12)
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L4765
.L4775:
	movq	88(%r12), %r13
	movq	%rbx, %rdx
.L4769:
	movq	%r14, 41088(%r12)
	movl	%eax, 41104(%r12)
	cmpq	%rdx, %rbx
	je	.L4763
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4763:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4765:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L4775
	movq	-1(%rdx), %rax
	cmpw	$1105, 11(%rax)
	je	.L4773
	leaq	.LC34(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4776:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL45Stats_Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L4773:
	.cfi_restore_state
	call	_ZN2v88internal12_GLOBAL__N_120EnsureFeedbackVectorENS0_6HandleINS0_10JSFunctionEEE
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L4769
	.cfi_endproc
.LFE22456:
	.size	_ZN2v88internal39Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal39Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE:
.LFB22459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4805
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %r14
	movl	41104(%rdx), %edx
	leal	1(%rdx), %eax
	movl	%eax, 41104(%r12)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L4780
.L4804:
	movq	88(%r12), %r13
	movq	%r14, %rax
.L4784:
	movq	%r15, 41088(%r12)
	movl	%edx, 41104(%r12)
	cmpq	%rax, %r14
	je	.L4777
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4777:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4806
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4780:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L4804
	movq	-1(%rax), %rax
	xorl	%ebx, %ebx
	cmpw	$1105, 11(%rax)
	je	.L4801
	leaq	.LC34(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4805:
	call	_ZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L4777
	.p2align 4,,10
	.p2align 3
.L4801:
	cmpl	$2, %edi
	je	.L4807
.L4786:
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_120EnsureFeedbackVectorENS0_6HandleINS0_10JSFunctionEEE
	testb	%al, %al
	je	.L4797
	movq	0(%r13), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$15728640, %edx
	je	.L4794
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	shrl	$20, %edx
	andl	$15, %edx
	cmpb	$11, %dl
	je	.L4797
.L4794:
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L4808
.L4791:
	cmpb	$0, _ZN2v88internal27FLAG_testing_d8_test_runnerE(%rip)
	je	.L4797
	movzbl	%bl, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PendingOptimizationTable23PreparedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEEb@PLT
	.p2align 4,,10
	.p2align 3
.L4797:
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	leal	-1(%rax), %edx
	movq	41096(%r12), %rax
	jmp	.L4784
	.p2align 4,,10
	.p2align 3
.L4807:
	movq	-8(%rsi), %rax
	testb	$1, %al
	je	.L4797
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L4797
	leaq	-64(%rbp), %rdi
	leaq	.LC118(%rip), %rsi
	movl	$28, %edx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %ebx
	jmp	.L4786
.L4808:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L4791
	jmp	.L4797
.L4806:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22459:
	.size	_ZN2v88internal38Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE, .-_ZN2v88internal38Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Runtime_OptimizeOsrEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_OptimizeOsrEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_OptimizeOsrEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_OptimizeOsrEiPmPNS0_7IsolateE:
.LFB22463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1496, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4879
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r15
	xorl	%ebx, %ebx
	movq	41096(%rdx), %r14
	cmpl	$1, %edi
	je	.L4880
.L4812:
	leaq	-1504(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L4841
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4841
	testl	%ebx, %ebx
	jne	.L4815
	jmp	.L4814
	.p2align 4,,10
	.p2align 3
.L4876:
	subl	$1, %ebx
	je	.L4814
.L4815:
	movq	%r13, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4876
.L4841:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L4809
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4809:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4881
	addq	$1496, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4814:
	.cfi_restore_state
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L4882
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4841
	cmpb	$0, _ZN2v88internal8FLAG_optE(%rip)
	je	.L4841
.L4820:
	movq	0(%r13), %rax
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$15728640, %edx
	jne	.L4883
.L4821:
	cmpb	$0, _ZN2v88internal27FLAG_testing_d8_test_runnerE(%rip)
	jne	.L4884
.L4823:
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	jne	.L4885
.L4826:
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L4838
	testb	$1, %dl
	jne	.L4832
.L4835:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	jne	.L4838
	movq	15(%rax), %rax
	testb	$1, %al
	je	.L4838
	cmpl	$3, %eax
	je	.L4838
	andq	$-3, %rax
	je	.L4838
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	jne	.L4838
.L4830:
	cmpb	$0, _ZN2v88internal27FLAG_testing_d8_test_runnerE(%rip)
	je	.L4841
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PendingOptimizationTable20FunctionWasOptimizedEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE@PLT
	jmp	.L4841
	.p2align 4,,10
	.p2align 3
.L4882:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L4886
.L4818:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	cmpb	$0, _ZN2v88internal8FLAG_optE(%rip)
	jne	.L4820
	jmp	.L4841
.L4832:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L4838
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L4835
.L4838:
	cmpb	$0, _ZN2v88internal14FLAG_trace_osrE(%rip)
	leaq	-1512(%rbp), %rbx
	jne	.L4887
.L4840:
	movq	%r13, %rdi
	call	_ZN2v88internal10JSFunction20EnsureFeedbackVectorENS0_6HandleIS1_EE@PLT
	movq	0(%r13), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, -1512(%rbp)
	call	_ZN2v88internal10JSFunction19MarkForOptimizationENS0_15ConcurrencyModeE@PLT
	movq	-88(%rbp), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$12, %eax
	jne	.L4841
	movq	40944(%r12), %rdi
	movq	-88(%rbp), %rsi
	movl	$6, %edx
	call	_ZN2v88internal15RuntimeProfiler25AttemptOnStackReplacementEPNS0_16InterpretedFrameEi@PLT
	jmp	.L4841
	.p2align 4,,10
	.p2align 3
.L4880:
	movq	(%rsi), %rbx
	shrq	$32, %rbx
	jmp	.L4812
	.p2align 4,,10
	.p2align 3
.L4879:
	call	_ZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L4809
.L4885:
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L4826
	testb	$1, %dl
	jne	.L4888
.L4827:
	movq	47(%rax), %rdx
	testb	$62, 43(%rdx)
	jne	.L4826
	movq	47(%rax), %rdx
	movq	31(%rdx), %rdx
	movl	15(%rdx), %edx
	andl	$1, %edx
	je	.L4830
	jmp	.L4826
.L4883:
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	shrl	$20, %edx
	andl	$15, %edx
	cmpb	$11, %dl
	jne	.L4821
	jmp	.L4841
.L4884:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal24PendingOptimizationTable21MarkedForOptimizationEPNS0_7IsolateENS0_6HandleINS0_10JSFunctionEEE@PLT
	movq	0(%r13), %rax
	jmp	.L4823
.L4886:
	movq	%r12, %rdi
	movq	%rsi, -1528(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1528(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L4818
.L4888:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L4826
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L4827
	jmp	.L4826
.L4887:
	xorl	%eax, %eax
	leaq	.LC76(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	0(%r13), %rax
	movq	stdout(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, -1512(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	leaq	.LC77(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L4840
.L4881:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22463:
	.size	_ZN2v88internal19Runtime_OptimizeOsrEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_OptimizeOsrEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateE:
.LFB22466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4901
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	41104(%rdx), %edx
	leal	1(%rdx), %eax
	movl	%eax, 41104(%r12)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L4892
.L4900:
	movq	88(%r12), %r13
	movq	%rbx, %rax
.L4895:
	movq	%r14, 41088(%r12)
	movl	%edx, 41104(%r12)
	cmpq	%rax, %rbx
	je	.L4889
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4889:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4902
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4892:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L4900
	movq	23(%rax), %rax
	leaq	-48(%rbp), %rdi
	movl	$11, %esi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo19DisableOptimizationENS0_13BailoutReasonE@PLT
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	leal	-1(%rax), %edx
	movq	41096(%r12), %rax
	jmp	.L4895
	.p2align 4,,10
	.p2align 3
.L4901:
	movq	%rdx, %rsi
	call	_ZN2v88internalL35Stats_Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L4889
.L4902:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22466:
	.size	_ZN2v88internal29Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE:
.LFB22469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$1496, %rsp
	.cfi_offset 3, -56
	movl	%edi, -1516(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5016
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	cmpb	$0, _ZN2v88internal14FLAG_lite_modeE(%rip)
	movq	41096(%rdx), %rbx
	movq	%rax, -1512(%rbp)
	jne	.L4984
	cmpb	$1, _ZN2v88internal12FLAG_jitlessE(%rip)
	sbbl	%r15d, %r15d
	andl	$-4096, %r15d
	addl	$4098, %r15d
	cmpb	$1, _ZN2v88internal12FLAG_jitlessE(%rip)
	sbbl	%r14d, %r14d
	notl	%r14d
	andl	$4096, %r14d
.L4906:
	movq	%r13, %rdi
	call	_ZN2v88internal7Isolate13use_optimizerEv@PLT
	testb	%al, %al
	cmove	%r15d, %r14d
	cmpb	$0, _ZN2v88internal15FLAG_always_optE(%rip)
	jne	.L4908
	cmpb	$0, _ZN2v88internal23FLAG_prepare_always_optE(%rip)
	je	.L4909
.L4908:
	orl	$4, %r14d
.L4909:
	movl	_ZN2v88internal24FLAG_deopt_every_n_timesE(%rip), %eax
	testl	%eax, %eax
	je	.L4910
	orl	$8, %r14d
.L4910:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L4911
.L4913:
	movq	%r14, %r12
	salq	$32, %r12
.L4912:
	movq	-1512(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	cmpq	41096(%r13), %rbx
	je	.L4903
	movq	%rbx, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4903:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5017
	addq	$1496, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4984:
	.cfi_restore_state
	movl	$4098, %r15d
	movl	$4096, %r14d
	jmp	.L4906
	.p2align 4,,10
	.p2align 3
.L4911:
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L4913
	movl	%r14d, %ecx
	orl	$1, %ecx
	cmpl	$2, -1516(%rbp)
	movl	%ecx, -1528(%rbp)
	je	.L4914
	cmpq	$0, 45416(%r13)
	leaq	-1504(%rbp), %r15
	je	.L4916
	.p2align 4,,10
	.p2align 3
.L4917:
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L5013
	testb	$1, %dl
	jne	.L4922
.L4925:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L4923
.L5013:
	movq	(%r12), %rax
.L4916:
	movabsq	$287762808832, %rsi
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rsi, %rdx
	je	.L4935
	testb	$1, %dl
	jne	.L4930
.L4933:
	movq	39(%rax), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	jne	.L4935
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L4935
	sarq	$32, %rax
	cmpq	$2, %rax
	jne	.L4935
	orb	$-127, %r14b
	movl	%r14d, -1528(%rbp)
.L4939:
	movq	(%r12), %rax
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	je	.L4962
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L5014
	testb	$1, %dl
	jne	.L5018
.L4958:
	movq	47(%rax), %rdx
	testb	$62, 43(%rdx)
	je	.L4960
.L5014:
	movq	(%r12), %rax
.L4962:
	movq	47(%rax), %rdx
	cmpl	$67, 59(%rdx)
	je	.L4972
	movabsq	$287762808832, %rcx
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L4972
	testb	$1, %dl
	jne	.L5019
.L4968:
	movq	47(%rax), %rdx
	movl	59(%rdx), %edx
	cmpl	$57, %edx
	je	.L4973
	subl	$64, %edx
	cmpl	$1, %edx
	jbe	.L4973
	movq	47(%rax), %rdx
	testb	$62, 43(%rdx)
	je	.L5020
	.p2align 4,,10
	.p2align 3
.L4972:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	cmpq	$0, -88(%rbp)
	je	.L4976
	.p2align 4,,10
	.p2align 3
.L5015:
	movq	%r15, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4976
	movq	(%rdi), %rax
	movq	(%r12), %r14
	call	*152(%rax)
	cmpq	%rax, %r14
	jne	.L5015
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4976
	movq	(%rdi), %rax
	call	*8(%rax)
	movl	%eax, %r9d
	movl	-1528(%rbp), %eax
	movl	%eax, %edx
	orb	$12, %ah
	orb	$4, %dh
	cmpl	$4, %r9d
	cmove	%eax, %edx
	movl	%edx, -1528(%rbp)
.L4976:
	movq	-1528(%rbp), %r12
	salq	$32, %r12
	jmp	.L4912
	.p2align 4,,10
	.p2align 3
.L4930:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L4935
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L4933
	.p2align 4,,10
	.p2align 3
.L4935:
	movabsq	$287762808832, %rsi
	movq	(%r12), %rdx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rsi, %rax
	je	.L4945
	testb	$1, %al
	jne	.L4940
.L4943:
	movq	39(%rdx), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	je	.L5021
	.p2align 4,,10
	.p2align 3
.L4945:
	movabsq	$287762808832, %rsi
	movq	(%r12), %rdx
	movq	23(%rdx), %rax
	movq	7(%rax), %rax
	cmpq	%rsi, %rax
	je	.L4939
	testb	$1, %al
	jne	.L4949
.L4952:
	movq	39(%rdx), %rax
	movq	7(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$155, 11(%rdx)
	jne	.L4939
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L4939
	sarq	$32, %rax
	movl	%r14d, %ecx
	orl	$513, %ecx
	cmpq	$4, %rax
	cmovne	-1528(%rbp), %ecx
	movl	%ecx, -1528(%rbp)
	jmp	.L4939
	.p2align 4,,10
	.p2align 3
.L5016:
	call	_ZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE
	movq	%rax, %r12
	jmp	.L4903
	.p2align 4,,10
	.p2align 3
.L4923:
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L5013
	sarq	$32, %rax
	cmpq	$4, %rax
	jne	.L5013
	movq	45416(%r13), %rdi
	call	_ZN2v88internal27OptimizingCompileDispatcher25InstallOptimizedFunctionsEv@PLT
	movl	$50000, %edi
	call	_ZN2v84base2OS5SleepENS0_9TimeDeltaE@PLT
	movq	(%r12), %rax
	jmp	.L4917
.L5020:
	movq	47(%rax), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L4972
	.p2align 4,,10
	.p2align 3
.L4973:
	orl	$64, -1528(%rbp)
	jmp	.L4972
	.p2align 4,,10
	.p2align 3
.L4922:
	movq	-1(%rdx), %rsi
	cmpw	$165, 11(%rsi)
	je	.L5013
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L4925
	jmp	.L5013
	.p2align 4,,10
	.p2align 3
.L4914:
	movq	-8(%r12), %rax
	testb	$1, %al
	jne	.L5022
.L4918:
	movq	88(%r13), %r12
	jmp	.L4912
.L5021:
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L4945
	sarq	$32, %rax
	cmpq	$4, %rax
	jne	.L4945
	orl	$257, %r14d
	movl	%r14d, -1528(%rbp)
	jmp	.L4939
	.p2align 4,,10
	.p2align 3
.L5022:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L4918
	leaq	-1504(%rbp), %r15
	leaq	.LC120(%rip), %rsi
	movl	$7, %edx
	movq	%rax, -1504(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	testb	%al, %al
	jne	.L5013
	cmpq	$0, 45416(%r13)
	movq	(%r12), %rax
	jne	.L4917
	jmp	.L4916
.L4960:
	movq	47(%rax), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	movq	(%r12), %rax
	jne	.L4962
	movq	47(%rax), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %ecx
	movl	-1528(%rbp), %eax
	movl	%eax, %edx
	orl	$16, %eax
	orb	$32, %dh
	andl	$1, %ecx
	cmove	%eax, %edx
	movq	(%r12), %rax
	movq	47(%rax), %rcx
	movl	%edx, %esi
	orl	$32, %edx
	testb	$64, 43(%rcx)
	cmove	%esi, %edx
	movl	%edx, -1528(%rbp)
	jmp	.L4962
.L5018:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L5014
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L4958
	jmp	.L5014
.L5019:
	movq	-1(%rdx), %rcx
	cmpw	$165, 11(%rcx)
	je	.L4972
	movq	-1(%rdx), %rdx
	cmpw	$166, 11(%rdx)
	jne	.L4968
	jmp	.L4972
.L4940:
	movq	-1(%rax), %rsi
	cmpw	$165, 11(%rsi)
	je	.L4945
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L4943
	jmp	.L4945
.L4949:
	movq	-1(%rax), %rsi
	cmpw	$165, 11(%rsi)
	je	.L4939
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L4952
	jmp	.L4939
.L5017:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22469:
	.size	_ZN2v88internal29Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_GetOptimizationStatusEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateE:
.LFB22473:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5033
	cmpb	$0, _ZN2v88internal35FLAG_block_concurrent_recompilationE(%rip)
	jne	.L5034
.L5030:
	movq	88(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5034:
	movq	45416(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L5030
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdx, -8(%rbp)
	call	_ZN2v88internal27OptimizingCompileDispatcher7UnblockEv@PLT
	movq	-8(%rbp), %rdx
	movq	88(%rdx), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5033:
	.cfi_restore 6
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL44Stats_Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22473:
	.size	_ZN2v88internal38Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateE, .-_ZN2v88internal38Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_GetUndetectableEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_GetUndetectableEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_GetUndetectableEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_GetUndetectableEiPmPNS0_7IsolateE:
.LFB22477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5045
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	xorl	%esi, %esi
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	call	_ZN2v814ObjectTemplate3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate18MarkAsUndetectableEv@PLT
	xorl	%edx, %edx
	leaq	_ZN2v88internalL10ReturnThisERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v814ObjectTemplate24SetCallAsFunctionHandlerEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS2_EE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	movq	%rax, %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L5037
	movq	(%rax), %r13
.L5037:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L5035
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5035:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5045:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL29Stats_Runtime_GetUndetectableEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22477:
	.size	_ZN2v88internal23Runtime_GetUndetectableEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_GetUndetectableEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Runtime_GetCallableEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_GetCallableEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_GetCallableEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_GetCallableEiPmPNS0_7IsolateE:
.LFB22481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5053
	subq	$8, %rsp
	addl	$1, 41104(%rdx)
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	pushq	$0
	xorl	%edx, %edx
	movl	$1, %r9d
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	xorl	%edx, %edx
	leaq	_ZN2v88internalL16call_as_functionERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate24SetCallAsFunctionHandlerEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS2_EE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L5054
.L5048:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L5055
.L5049:
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5046
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5046:
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5053:
	.cfi_restore_state
	leaq	-32(%rbp), %rsp
	movq	%rdx, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL25Stats_Runtime_GetCallableEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L5054:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L5048
	.p2align 4,,10
	.p2align 3
.L5055:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rax
	jmp	.L5049
	.cfi_endproc
.LFE22481:
	.size	_ZN2v88internal19Runtime_GetCallableEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_GetCallableEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateE:
.LFB22484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5071
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movl	41104(%rdx), %edx
	leal	1(%rdx), %eax
	movl	%eax, 41104(%r12)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5059
.L5070:
	movq	88(%r12), %r13
	movq	%rbx, %rax
.L5063:
	movq	%r14, 41088(%r12)
	movl	%edx, 41104(%r12)
	cmpq	%rax, %rbx
	je	.L5056
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5056:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5072
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5059:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L5070
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	je	.L5068
	leaq	.LC34(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5071:
	movq	%rdx, %rsi
	call	_ZN2v88internalL35Stats_Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L5056
	.p2align 4,,10
	.p2align 3
.L5068:
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal10JSFunction21ClearTypeFeedbackInfoEv@PLT
	movl	41104(%r12), %eax
	movq	88(%r12), %r13
	leal	-1(%rax), %edx
	movq	41096(%r12), %rax
	jmp	.L5063
.L5072:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22484:
	.size	_ZN2v88internal29Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE:
.LFB22487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5103
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	%rdx, -112(%rbp)
	movq	41096(%rdx), %rbx
	cmpl	$2, %edi
	jne	.L5104
	testb	$1, (%rsi)
	jne	.L5105
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L5106
.L5078:
	leaq	.LC109(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5106:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L5078
	testb	$-2, 43(%rax)
	jne	.L5078
	cmpq	%rax, 112(%r12)
	movzbl	_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %eax
	sete	%r14b
	cmpb	$2, %al
	je	.L5082
	leaq	_ZN2v84base16LazyInstanceImplINS0_5MutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %r8
	leaq	8+_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r8, %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rdi
	movq	%rax, %xmm1
	movq	%r8, -120(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L5082
	movq	-120(%rbp), %r8
	movl	$3, %edx
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	*%rax
.L5082:
	leaq	8+_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %eax
	testb	%al, %al
	je	.L5084
	leaq	8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rax
.L5085:
	movq	16+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdx
	testq	%rdx, %rdx
	je	.L5095
	movq	-112(%rbp), %rdi
	leaq	8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rsi
	jmp	.L5088
	.p2align 4,,10
	.p2align 3
.L5107:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L5089
.L5088:
	cmpq	%rdi, 32(%rdx)
	jnb	.L5107
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L5088
.L5089:
	cmpq	%rax, %rsi
	je	.L5087
	cmpq	%rdi, 32(%rsi)
	jbe	.L5092
.L5087:
	leaq	-112(%rbp), %rax
	leaq	_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdi
	leaq	-104(%rbp), %rdx
	movq	%rax, -104(%rbp)
	call	_ZNSt8_Rb_treeIPN2v87IsolateESt4pairIKS2_NS0_8internal12_GLOBAL__N_119WasmCompileControlsEESt10_Select1stIS8_ESt4lessIS2_ESaIS8_EE22_M_emplace_hint_uniqueIJRKSt21piecewise_construct_tSt5tupleIJRS4_EESJ_IJEEEEESt17_Rb_tree_iteratorIS8_ESt23_Rb_tree_const_iteratorIS8_EDpOT_.isra.0
	movq	-112(%rbp), %rdi
	movq	%rax, %rsi
.L5092:
	movb	%r14b, 44(%rsi)
	movq	(%r15), %rax
	sarq	$32, %rax
	movl	%eax, 40(%rsi)
	leaq	_ZN2v88internal12_GLOBAL__N_118WasmModuleOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	call	_ZN2v87Isolate21SetWasmModuleCallbackEPFbRKNS_20FunctionCallbackInfoINS_5ValueEEEE@PLT
	leaq	8+_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE(%rip), %rdi
	movq	88(%r12), %r14
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5073
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L5073
	.p2align 4,,10
	.p2align 3
.L5103:
	call	_ZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L5073:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5108
	addq	$88, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5104:
	.cfi_restore_state
	leaq	.LC142(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5105:
	leaq	.LC98(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5084:
	leaq	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	movl	%eax, %r8d
	leaq	8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rax
	testl	%r8d, %r8d
	je	.L5085
	leaq	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rdi
	movq	%rax, 24+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	%rax, 32+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	%rax, -120(%rbp)
	movl	$0, 8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	$0, 16+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	movq	$0, 40+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip)
	call	__cxa_guard_release@PLT
	movq	-120(%rbp), %rax
	jmp	.L5085
	.p2align 4,,10
	.p2align 3
.L5095:
	leaq	8+_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object(%rip), %rsi
	jmp	.L5087
.L5108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22487:
	.size	_ZN2v88internal30Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE:
.LFB22490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5114
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testl	%edi, %edi
	jne	.L5115
	leaq	_ZN2v88internal12_GLOBAL__N_120WasmInstanceOverrideERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	movq	%rdx, %rdi
	call	_ZN2v87Isolate23SetWasmInstanceCallbackEPFbRKNS_20FunctionCallbackInfoINS_5ValueEEEE@PLT
	movq	%r13, 41088(%r12)
	movq	88(%r12), %r14
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5109
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5109:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5114:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L5115:
	.cfi_restore_state
	leaq	.LC82(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22490:
	.size	_ZN2v88internal34Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_NotifyContextDisposedEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_NotifyContextDisposedEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_NotifyContextDisposedEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_NotifyContextDisposedEiPmPNS0_7IsolateE:
.LFB22493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5120
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	leaq	37592(%rdx), %rdi
	movl	$1, %esi
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal4Heap21NotifyContextDisposedEb@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5116
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5116:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5120:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL35Stats_Runtime_NotifyContextDisposedEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22493:
	.size	_ZN2v88internal29Runtime_NotifyContextDisposedEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_NotifyContextDisposedEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateE:
.LFB22496:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5123
	movq	88(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5123:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL34Stats_Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22496:
	.size	_ZN2v88internal28Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Runtime_DebugPrintEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_DebugPrintEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_DebugPrintEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_DebugPrintEiPmPNS0_7IsolateE:
.LFB22499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5138
	leaq	-320(%rbp), %r14
	movq	(%rsi), %rbx
	leaq	-400(%rbp), %r13
	movq	%r14, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r15
	call	_ZNSt8ios_baseC2Ev@PLT
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	stdout(%rip), %rdx
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movw	%ax, -96(%rbp)
	movq	%r15, -320(%rbp)
	movq	$0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%rax, -400(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	cmpl	$3, %ebx
	je	.L5139
	movq	%rbx, %rcx
	andq	$-3, %rcx
	testb	$1, %bl
	cmove	%rbx, %rcx
	andl	$3, %ebx
	cmpq	$3, %rbx
	je	.L5140
.L5130:
	leaq	-408(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rcx, -408(%rbp)
	call	_ZN2v88internallsERSoRKNS0_5BriefE@PLT
.L5128:
	movq	-400(%rbp), %rax
	movq	-24(%rax), %rax
	movq	-160(%rbp,%rax), %rbx
	testq	%rbx, %rbx
	je	.L5141
	cmpb	$0, 56(%rbx)
	je	.L5132
	movsbl	67(%rbx), %esi
.L5133:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rcx
	movq	(%r12), %r12
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%r15, -320(%rbp)
	movq	%rax, -400(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L5124:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5142
	addq	$392, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5139:
	.cfi_restore_state
	movl	$14, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L5128
	.p2align 4,,10
	.p2align 3
.L5132:
	movq	%rbx, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%rbx), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5133
	movq	%rbx, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L5133
	.p2align 4,,10
	.p2align 3
.L5140:
	movl	$7, %edx
	leaq	.LC16(%rip), %rsi
	movq	%r13, %rdi
	movq	%rcx, -424(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-424(%rbp), %rcx
	jmp	.L5130
	.p2align 4,,10
	.p2align 3
.L5138:
	movq	%r12, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL24Stats_Runtime_DebugPrintEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r12
	jmp	.L5124
.L5142:
	call	__stack_chk_fail@PLT
.L5141:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE22499:
	.size	_ZN2v88internal18Runtime_DebugPrintEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_DebugPrintEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE:
.LFB22502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5225
	movq	(%rsi), %r15
	testb	$1, %r15b
	jne	.L5146
.L5147:
	leaq	.LC18(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5146:
	movq	-1(%r15), %rax
	cmpw	$63, 11(%rax)
	ja	.L5147
	leaq	.LC86(%rip), %rdi
	xorl	%eax, %eax
	leaq	-368(%rbp), %r14
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movl	$32, %ecx
	xorl	%eax, %eax
	movq	%r14, %rdi
	rep stosq
	pxor	%xmm0, %xmm0
	movb	$0, -88(%rbp)
	xorl	%edx, %edx
	movq	$0, -112(%rbp)
	leaq	.L5151(%rip), %rcx
	movl	$0, -384(%rbp)
	movaps	%xmm0, -80(%rbp)
	movslq	11(%r15), %rbx
.L5148:
	movq	-1(%r15), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L5149
	movzwl	%ax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L5151:
	.long	.L5157-.L5151
	.long	.L5154-.L5151
	.long	.L5156-.L5151
	.long	.L5152-.L5151
	.long	.L5149-.L5151
	.long	.L5150-.L5151
	.long	.L5149-.L5151
	.long	.L5149-.L5151
	.long	.L5155-.L5151
	.long	.L5154-.L5151
	.long	.L5153-.L5151
	.long	.L5152-.L5151
	.long	.L5149-.L5151
	.long	.L5150-.L5151
	.section	.text._ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L5154:
	movl	$0, -104(%rbp)
	movl	-384(%rbp), %edx
	testq	%r15, %r15
	je	.L5171
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi@PLT
	movl	-104(%rbp), %esi
	movl	$0, -384(%rbp)
	testl	%esi, %esi
	jne	.L5164
.L5171:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
.L5163:
	leaq	.L5184(%rip), %rbx
	leaq	.L5201(%rip), %r15
	jmp	.L5214
	.p2align 4,,10
	.p2align 3
.L5223:
	movzbl	-88(%rbp), %edx
.L5180:
	testb	%dl, %dl
	je	.L5209
.L5208:
	leaq	1(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzbl	(%rax), %esi
.L5213:
	xorl	%eax, %eax
	leaq	.LC20(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
.L5214:
	cmpq	%rax, %rdx
	jne	.L5223
	movl	$0, -380(%rbp)
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L5226
.L5181:
	leaq	.LC87(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-8(%r12), %rax
	movq	stdout(%rip), %rsi
	leaq	-376(%rbp), %rdi
	movq	%rax, -376(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	88(%r13), %rax
.L5143:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L5227
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5207:
	.cfi_restore_state
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L5209:
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %esi
	jmp	.L5213
	.p2align 4,,10
	.p2align 3
.L5226:
	leaq	-380(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L5181
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L5182:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L5149
	movzwl	%dx, %edx
	movslq	(%rbx,%rdx,4), %rdx
	addq	%rbx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE
	.align 4
	.align 4
.L5184:
	.long	.L5190-.L5184
	.long	.L5187-.L5184
	.long	.L5189-.L5184
	.long	.L5185-.L5184
	.long	.L5149-.L5184
	.long	.L5183-.L5184
	.long	.L5149-.L5184
	.long	.L5149-.L5184
	.long	.L5188-.L5184
	.long	.L5187-.L5184
	.long	.L5186-.L5184
	.long	.L5185-.L5184
	.long	.L5149-.L5184
	.long	.L5183-.L5184
	.section	.text._ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L5183:
	movq	15(%rax), %rax
	jmp	.L5182
	.p2align 4,,10
	.p2align 3
.L5185:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L5182
	.p2align 4,,10
	.p2align 3
.L5187:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L5191:
	cmpq	%rcx, %rax
	jne	.L5223
	movl	$0, -376(%rbp)
	movl	-104(%rbp), %edx
	testl	%edx, %edx
	je	.L5223
	leaq	-376(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L5228
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L5199:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L5149
	movzwl	%dx, %edx
	movslq	(%r15,%rdx,4), %rdx
	addq	%r15, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE
	.align 4
	.align 4
.L5201:
	.long	.L5207-.L5201
	.long	.L5204-.L5201
	.long	.L5206-.L5201
	.long	.L5202-.L5201
	.long	.L5149-.L5201
	.long	.L5200-.L5201
	.long	.L5149-.L5201
	.long	.L5149-.L5201
	.long	.L5205-.L5201
	.long	.L5204-.L5201
	.long	.L5203-.L5201
	.long	.L5202-.L5201
	.long	.L5149-.L5201
	.long	.L5200-.L5201
	.section	.text._ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L5189:
	movq	15(%rax), %rdi
	movl	%esi, -392(%rbp)
	movl	%ecx, -388(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-392(%rbp), %rsi
	movslq	-388(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L5191
	.p2align 4,,10
	.p2align 3
.L5190:
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L5191
	.p2align 4,,10
	.p2align 3
.L5186:
	movq	15(%rax), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5192
	movq	8(%rdi), %rax
.L5193:
	movslq	%esi, %rsi
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L5191
	.p2align 4,,10
	.p2align 3
.L5188:
	movslq	%esi, %rsi
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L5191
	.p2align 4,,10
	.p2align 3
.L5150:
	movq	15(%r15), %r15
	jmp	.L5148
	.p2align 4,,10
	.p2align 3
.L5152:
	addl	27(%r15), %edx
	movq	15(%r15), %r15
	jmp	.L5148
	.p2align 4,,10
	.p2align 3
.L5200:
	movq	15(%rax), %rax
	jmp	.L5199
	.p2align 4,,10
	.p2align 3
.L5202:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L5199
	.p2align 4,,10
	.p2align 3
.L5204:
	movzbl	-88(%rbp), %edx
	movq	-80(%rbp), %rax
	jmp	.L5180
	.p2align 4,,10
	.p2align 3
.L5156:
	movq	15(%r15), %rdi
	movl	%edx, -388(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rdx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rdx,2), %rax
	leaq	(%rax,%rbx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
.L5158:
	movl	$0, -104(%rbp)
	jmp	.L5163
	.p2align 4,,10
	.p2align 3
.L5157:
	movslq	%edx, %rdx
	movb	$0, -88(%rbp)
	leaq	15(%r15,%rdx,2), %rax
	leaq	(%rax,%rbx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L5158
	.p2align 4,,10
	.p2align 3
.L5153:
	movq	15(%r15), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rcx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L5159
	movq	8(%rdi), %rax
.L5160:
	movslq	%edx, %rdx
	addq	%rdx, %rax
.L5224:
	movslq	%ebx, %rdx
	movb	$1, -88(%rbp)
	addq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L5158
	.p2align 4,,10
	.p2align 3
.L5155:
	movslq	%edx, %rdx
	leaq	15(%r15,%rdx), %rax
	jmp	.L5224
.L5206:
	movq	15(%rax), %rdi
	movl	%ecx, -392(%rbp)
	movl	%esi, -388(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rsi
	movslq	-392(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %esi
	jmp	.L5213
.L5203:
	movq	15(%rax), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5210
	movq	8(%rdi), %rax
.L5211:
	movslq	%esi, %rsi
	movslq	%ecx, %rdx
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L5208
.L5205:
	movslq	%esi, %rsi
	movslq	%ecx, %rdx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L5208
	.p2align 4,,10
	.p2align 3
.L5164:
	leaq	-384(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L5171
	movl	-384(%rbp), %edi
	movl	11(%rax), %edx
	leaq	.L5168(%rip), %rsi
	movl	%edi, %r15d
.L5166:
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$15, %ecx
	cmpw	$13, %cx
	ja	.L5149
	movzwl	%cx, %ecx
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE
	.align 4
	.align 4
.L5168:
	.long	.L5174-.L5168
	.long	.L5171-.L5168
	.long	.L5173-.L5168
	.long	.L5169-.L5168
	.long	.L5149-.L5168
	.long	.L5167-.L5168
	.long	.L5149-.L5168
	.long	.L5149-.L5168
	.long	.L5172-.L5168
	.long	.L5171-.L5168
	.long	.L5170-.L5168
	.long	.L5169-.L5168
	.long	.L5149-.L5168
	.long	.L5167-.L5168
	.section	.text._ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L5225:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE.isra.0
	jmp	.L5143
	.p2align 4,,10
	.p2align 3
.L5228:
	movq	-80(%rbp), %rax
	jmp	.L5223
.L5192:
	movl	%esi, -392(%rbp)
	movl	%ecx, -388(%rbp)
	call	*%rax
	movl	-392(%rbp), %esi
	movslq	-388(%rbp), %rcx
	jmp	.L5193
	.p2align 4,,10
	.p2align 3
.L5149:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5167:
	movq	15(%rax), %rax
	jmp	.L5166
.L5169:
	addl	27(%rax), %r15d
	movq	15(%rax), %rax
	jmp	.L5166
.L5170:
	movl	%edx, %ebx
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	subl	%edi, %ebx
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5176
	movq	8(%rdi), %rax
.L5177:
	movslq	%r15d, %r15
	movslq	%ebx, %rdx
	movb	$1, -88(%rbp)
	addq	%r15, %rax
	addq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L5163
.L5172:
	movslq	%r15d, %r15
	subl	%edi, %edx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%r15), %rax
	movslq	%edx, %rdx
	addq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L5163
.L5173:
	movl	%edx, %ebx
	movslq	%r15d, %r15
	subl	%edi, %ebx
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%ebx, %rdx
	movb	$0, -88(%rbp)
	leaq	(%rax,%r15,2), %rax
	leaq	(%rax,%rdx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L5163
.L5174:
	movslq	%r15d, %r15
	subl	%edi, %edx
	movb	$0, -88(%rbp)
	leaq	15(%rax,%r15,2), %rax
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L5163
.L5159:
	movl	%edx, -388(%rbp)
	call	*%rax
	movl	-388(%rbp), %edx
	jmp	.L5160
.L5210:
	movl	%ecx, -392(%rbp)
	movl	%esi, -388(%rbp)
	call	*%rax
	movl	-392(%rbp), %ecx
	movl	-388(%rbp), %esi
	jmp	.L5211
.L5176:
	call	*%rax
	jmp	.L5177
.L5227:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22502:
	.size	_ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Runtime_DebugTraceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_DebugTraceEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_DebugTraceEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_DebugTraceEiPmPNS0_7IsolateE:
.LFB22505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5232
	movq	stdout(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	movq	88(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5232:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rdx, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL24Stats_Runtime_DebugTraceEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22505:
	.size	_ZN2v88internal18Runtime_DebugTraceEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_DebugTraceEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE:
.LFB22508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5256
	addl	$1, 41104(%rdx)
	cmpb	$0, _ZN2v88internal25FLAG_track_retaining_pathE(%rip)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	je	.L5257
	testb	$1, (%rsi)
	je	.L5258
	xorl	%edx, %edx
	cmpl	$2, %edi
	je	.L5259
.L5239:
	leaq	37592(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4Heap22AddRetainingPathTargetENS0_6HandleINS0_10HeapObjectEEENS0_19RetainingPathOptionE@PLT
.L5237:
	subl	$1, 41104(%r12)
	movq	88(%r12), %r13
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5233
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5233:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5260
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5257:
	.cfi_restore_state
	leaq	.LC122(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5237
	.p2align 4,,10
	.p2align 3
.L5259:
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L5240
.L5241:
	leaq	.LC124(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5256:
	call	_ZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L5233
	.p2align 4,,10
	.p2align 3
.L5240:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L5241
	leaq	-88(%rbp), %r9
	leaq	-80(%rbp), %r15
	movb	$0, -60(%rbp)
	movdqa	.LC127(%rip), %xmm0
	movl	$20, %edx
	movq	%r15, %rsi
	movq	%r9, %rdi
	movq	%rax, -88(%rbp)
	movl	$1752457584, -64(%rbp)
	movq	%r9, -104(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	$1, %edx
	testb	%al, %al
	jne	.L5239
	movq	-8(%r13), %rax
	movl	11(%rax), %edx
	testl	%edx, %edx
	je	.L5239
	leaq	.LC125(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-104(%rbp), %r9
	movq	-8(%r13), %rax
	xorl	%r8d, %r8d
	leaq	-96(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r9, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-88(%rbp), %rdx
	movq	%r15, %rsi
	xorl	%eax, %eax
	leaq	.LC126(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5243
	call	_ZdaPv@PLT
.L5243:
	xorl	%edx, %edx
	jmp	.L5239
	.p2align 4,,10
	.p2align 3
.L5258:
	leaq	.LC123(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L5260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22508:
	.size	_ZN2v88internal31Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE:
.LFB22512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5343
	movq	(%rsi), %r12
	testb	$1, %r12b
	jne	.L5264
.L5265:
	leaq	.LC18(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5264:
	movq	-1(%r12), %rax
	cmpw	$63, 11(%rax)
	ja	.L5265
	leaq	-368(%rbp), %r13
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movl	$32, %ecx
	movq	%r13, %rdi
	movb	$0, -88(%rbp)
	xorl	%r14d, %r14d
	rep stosq
	movaps	%xmm0, -80(%rbp)
	leaq	.L5269(%rip), %rdx
	movq	$0, -112(%rbp)
	movl	$0, -380(%rbp)
	movslq	11(%r12), %rbx
.L5266:
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$15, %eax
	cmpw	$13, %ax
	ja	.L5267
	movzwl	%ax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L5269:
	.long	.L5275-.L5269
	.long	.L5272-.L5269
	.long	.L5274-.L5269
	.long	.L5270-.L5269
	.long	.L5267-.L5269
	.long	.L5268-.L5269
	.long	.L5267-.L5269
	.long	.L5267-.L5269
	.long	.L5273-.L5269
	.long	.L5272-.L5269
	.long	.L5271-.L5269
	.long	.L5270-.L5269
	.long	.L5267-.L5269
	.long	.L5268-.L5269
	.section	.text._ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L5272:
	movl	$0, -104(%rbp)
	movl	-380(%rbp), %edx
	testq	%rsi, %rsi
	je	.L5289
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator10InitializeENS0_10ConsStringEi@PLT
	movl	-104(%rbp), %esi
	movl	$0, -380(%rbp)
	testl	%esi, %esi
	jne	.L5282
.L5289:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
.L5281:
	leaq	.L5302(%rip), %rbx
	leaq	.L5319(%rip), %r14
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %r15
	jmp	.L5332
	.p2align 4,,10
	.p2align 3
.L5341:
	movzbl	-88(%rbp), %edx
.L5298:
	testb	%dl, %dl
	je	.L5327
.L5326:
	leaq	1(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzbl	(%rax), %esi
.L5331:
	xorl	%eax, %eax
	leaq	.LC20(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rdx
.L5332:
	cmpq	%rax, %rdx
	jne	.L5341
	movl	$0, -376(%rbp)
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L5344
.L5261:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5345
	addq	$360, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5325:
	.cfi_restore_state
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L5327:
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %esi
	jmp	.L5331
	.p2align 4,,10
	.p2align 3
.L5344:
	leaq	-376(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L5261
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L5300:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L5267
	movzwl	%dx, %edx
	movslq	(%rbx,%rdx,4), %rdx
	addq	%rbx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE
	.align 4
	.align 4
.L5302:
	.long	.L5308-.L5302
	.long	.L5305-.L5302
	.long	.L5307-.L5302
	.long	.L5303-.L5302
	.long	.L5267-.L5302
	.long	.L5301-.L5302
	.long	.L5267-.L5302
	.long	.L5267-.L5302
	.long	.L5306-.L5302
	.long	.L5305-.L5302
	.long	.L5304-.L5302
	.long	.L5303-.L5302
	.long	.L5267-.L5302
	.long	.L5301-.L5302
	.section	.text._ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L5301:
	movq	15(%rax), %rax
	jmp	.L5300
	.p2align 4,,10
	.p2align 3
.L5303:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L5300
	.p2align 4,,10
	.p2align 3
.L5305:
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L5309:
	cmpq	%rcx, %rax
	jne	.L5341
	movl	$0, -372(%rbp)
	movl	-104(%rbp), %edx
	testl	%edx, %edx
	je	.L5341
	leaq	-372(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L5346
	movslq	11(%rax), %rcx
	xorl	%esi, %esi
.L5317:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$15, %edx
	cmpw	$13, %dx
	ja	.L5267
	movzwl	%dx, %edx
	movslq	(%r14,%rdx,4), %rdx
	addq	%r14, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE
	.align 4
	.align 4
.L5319:
	.long	.L5325-.L5319
	.long	.L5322-.L5319
	.long	.L5324-.L5319
	.long	.L5320-.L5319
	.long	.L5267-.L5319
	.long	.L5318-.L5319
	.long	.L5267-.L5319
	.long	.L5267-.L5319
	.long	.L5323-.L5319
	.long	.L5322-.L5319
	.long	.L5321-.L5319
	.long	.L5320-.L5319
	.long	.L5267-.L5319
	.long	.L5318-.L5319
	.section	.text._ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L5307:
	movq	15(%rax), %rdi
	movl	%esi, -392(%rbp)
	movl	%ecx, -388(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-392(%rbp), %rsi
	movslq	-388(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L5309
	.p2align 4,,10
	.p2align 3
.L5308:
	movslq	%esi, %rsi
	movb	$0, -88(%rbp)
	leaq	15(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L5309
	.p2align 4,,10
	.p2align 3
.L5304:
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%r15, %rax
	jne	.L5310
	movq	8(%rdi), %rax
.L5311:
	movslq	%esi, %rsi
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L5309
	.p2align 4,,10
	.p2align 3
.L5306:
	movslq	%esi, %rsi
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	jmp	.L5309
	.p2align 4,,10
	.p2align 3
.L5268:
	movq	15(%rsi), %rsi
	jmp	.L5266
	.p2align 4,,10
	.p2align 3
.L5270:
	addl	27(%rsi), %r14d
	movq	15(%rsi), %rsi
	jmp	.L5266
	.p2align 4,,10
	.p2align 3
.L5318:
	movq	15(%rax), %rax
	jmp	.L5317
	.p2align 4,,10
	.p2align 3
.L5320:
	addl	27(%rax), %esi
	movq	15(%rax), %rax
	jmp	.L5317
	.p2align 4,,10
	.p2align 3
.L5322:
	movzbl	-88(%rbp), %edx
	movq	-80(%rbp), %rax
	jmp	.L5298
	.p2align 4,,10
	.p2align 3
.L5274:
	movq	15(%rsi), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movb	$0, -88(%rbp)
	movq	%rax, %r8
	movslq	%r14d, %rax
	leaq	(%r8,%rax,2), %rax
	leaq	(%rax,%rbx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
.L5276:
	movl	$0, -104(%rbp)
	jmp	.L5281
	.p2align 4,,10
	.p2align 3
.L5275:
	movslq	%r14d, %rax
	movb	$0, -88(%rbp)
	leaq	15(%rsi,%rax,2), %rax
	leaq	(%rax,%rbx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L5276
	.p2align 4,,10
	.p2align 3
.L5271:
	movq	15(%rsi), %rdi
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5277
	movq	8(%rdi), %rdx
.L5278:
	movslq	%r14d, %rax
	addq	%rdx, %rax
.L5342:
	movslq	%ebx, %rdx
	movb	$1, -88(%rbp)
	addq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L5276
	.p2align 4,,10
	.p2align 3
.L5273:
	movslq	%r14d, %rax
	leaq	15(%rsi,%rax), %rax
	jmp	.L5342
.L5324:
	movq	15(%rax), %rdi
	movl	%ecx, -392(%rbp)
	movl	%esi, -388(%rbp)
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	-388(%rbp), %rsi
	movslq	-392(%rbp), %rcx
	movb	$0, -88(%rbp)
	leaq	(%rax,%rsi,2), %rax
	leaq	(%rax,%rcx,2), %rdx
	movq	%rdx, -72(%rbp)
	leaq	2(%rax), %rdx
	movq	%rdx, -80(%rbp)
	movzwl	(%rax), %esi
	jmp	.L5331
.L5321:
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%r15, %rax
	jne	.L5328
	movq	8(%rdi), %rax
.L5329:
	movslq	%esi, %rsi
	movslq	%ecx, %rdx
	movb	$1, -88(%rbp)
	addq	%rsi, %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L5326
.L5323:
	movslq	%esi, %rsi
	movslq	%ecx, %rdx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%rsi), %rax
	addq	%rax, %rdx
	movq	%rdx, -72(%rbp)
	jmp	.L5326
	.p2align 4,,10
	.p2align 3
.L5282:
	leaq	-380(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal18ConsStringIterator8ContinueEPi@PLT
	testq	%rax, %rax
	je	.L5289
	movl	-380(%rbp), %edi
	movl	11(%rax), %edx
	leaq	.L5286(%rip), %rsi
	movl	%edi, %r14d
.L5284:
	movq	-1(%rax), %rcx
	movzwl	11(%rcx), %ecx
	andl	$15, %ecx
	cmpw	$13, %cx
	ja	.L5267
	movzwl	%cx, %ecx
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE
	.align 4
	.align 4
.L5286:
	.long	.L5292-.L5286
	.long	.L5289-.L5286
	.long	.L5291-.L5286
	.long	.L5287-.L5286
	.long	.L5267-.L5286
	.long	.L5285-.L5286
	.long	.L5267-.L5286
	.long	.L5267-.L5286
	.long	.L5290-.L5286
	.long	.L5289-.L5286
	.long	.L5288-.L5286
	.long	.L5287-.L5286
	.long	.L5267-.L5286
	.long	.L5285-.L5286
	.section	.text._ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L5343:
	movq	%rdx, %rsi
	call	_ZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r12
	jmp	.L5261
	.p2align 4,,10
	.p2align 3
.L5346:
	movq	-80(%rbp), %rax
	jmp	.L5341
.L5310:
	movl	%esi, -392(%rbp)
	movl	%ecx, -388(%rbp)
	call	*%rax
	movl	-392(%rbp), %esi
	movslq	-388(%rbp), %rcx
	jmp	.L5311
	.p2align 4,,10
	.p2align 3
.L5267:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5285:
	movq	15(%rax), %rax
	jmp	.L5284
.L5287:
	addl	27(%rax), %r14d
	movq	15(%rax), %rax
	jmp	.L5284
.L5288:
	movl	%edx, %ebx
	leaq	_ZNK2v88internal29NativesExternalStringResource4dataEv(%rip), %rdx
	subl	%edi, %ebx
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5294
	movq	8(%rdi), %rax
.L5295:
	movslq	%r14d, %r14
	movslq	%ebx, %rdx
	movb	$1, -88(%rbp)
	addq	%r14, %rax
	addq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L5281
.L5290:
	movslq	%r14d, %r14
	subl	%edi, %edx
	movb	$1, -88(%rbp)
	leaq	15(%rax,%r14), %rax
	movslq	%edx, %rdx
	addq	%rax, %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L5281
.L5291:
	movl	%edx, %ebx
	movslq	%r14d, %r14
	subl	%edi, %ebx
	movq	15(%rax), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movslq	%ebx, %rdx
	movb	$0, -88(%rbp)
	leaq	(%rax,%r14,2), %rax
	leaq	(%rax,%rdx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L5281
.L5292:
	movslq	%r14d, %r14
	subl	%edi, %edx
	movb	$0, -88(%rbp)
	leaq	15(%rax,%r14,2), %rax
	movslq	%edx, %rdx
	leaq	(%rax,%rdx,2), %rdx
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L5281
.L5277:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L5278
.L5328:
	movl	%ecx, -392(%rbp)
	movl	%esi, -388(%rbp)
	call	*%rax
	movl	-392(%rbp), %ecx
	movl	-388(%rbp), %esi
	jmp	.L5329
.L5294:
	call	*%rax
	jmp	.L5295
.L5345:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22512:
	.size	_ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_GlobalPrintEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Runtime_SystemBreakEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_SystemBreakEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_SystemBreakEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_SystemBreakEiPmPNS0_7IsolateE:
.LFB22515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5351
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v84base2OS10DebugBreakEv@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5347
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5347:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5351:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL25Stats_Runtime_SystemBreakEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22515:
	.size	_ZN2v88internal19Runtime_SystemBreakEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_SystemBreakEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_SetForceSlowPathEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_SetForceSlowPathEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_SetForceSlowPathEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_SetForceSlowPathEiPmPNS0_7IsolateE:
.LFB22518:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5354
	movq	(%rsi), %rax
	cmpq	%rax, 112(%rdx)
	sete	45428(%rdx)
	movq	88(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5354:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL30Stats_Runtime_SetForceSlowPathEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22518:
	.size	_ZN2v88internal24Runtime_SetForceSlowPathEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_SetForceSlowPathEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_DisassembleFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_DisassembleFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_DisassembleFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_DisassembleFunctionEiPmPNS0_7IsolateE:
.LFB22530:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5357
	movq	88(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5357:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL33Stats_Runtime_DisassembleFunctionEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22530:
	.size	_ZN2v88internal27Runtime_DisassembleFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_DisassembleFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Runtime_TraceEnterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_TraceEnterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_TraceEnterEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_TraceEnterEiPmPNS0_7IsolateE:
.LFB22535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5363
	call	_ZN2v88internal12_GLOBAL__N_19StackSizeEPNS0_7IsolateE
	movl	%eax, %esi
	cmpl	$80, %eax
	jle	.L5364
	leaq	.LC24(%rip), %rcx
	movl	$80, %edx
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L5361:
	movq	stdout(%rip), %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movl	$1, %edx
	call	_ZN2v88internal15JavaScriptFrame8PrintTopEPNS0_7IsolateEP8_IO_FILEbb@PLT
	xorl	%eax, %eax
	leaq	.LC92(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	88(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5364:
	.cfi_restore_state
	movl	%eax, %edx
	leaq	.LC22(%rip), %rcx
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5361
	.p2align 4,,10
	.p2align 3
.L5363:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL24Stats_Runtime_TraceEnterEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22535:
	.size	_ZN2v88internal18Runtime_TraceEnterEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_TraceEnterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal17Runtime_TraceExitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal17Runtime_TraceExitEiPmPNS0_7IsolateE
	.type	_ZN2v88internal17Runtime_TraceExitEiPmPNS0_7IsolateE, @function
_ZN2v88internal17Runtime_TraceExitEiPmPNS0_7IsolateE:
.LFB22538:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5372
	movq	(%rsi), %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_19StackSizeEPNS0_7IsolateE
	movl	%eax, %esi
	cmpl	$80, %eax
	jle	.L5373
	leaq	.LC24(%rip), %rcx
	movl	$80, %edx
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
.L5369:
	xorl	%eax, %eax
	leaq	.LC25(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	stdout(%rip), %rsi
	leaq	-16(%rbp), %rdi
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	xorl	%eax, %eax
	leaq	.LC26(%rip), %rdi
	call	_ZN2v88internal6PrintFEPKcz@PLT
	movq	-16(%rbp), %rax
.L5365:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5374
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5373:
	.cfi_restore_state
	movl	%eax, %edx
	leaq	.LC22(%rip), %rcx
	leaq	.LC23(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEPKcz@PLT
	jmp	.L5369
	.p2align 4,,10
	.p2align 3
.L5372:
	movq	%rdx, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internalL23Stats_Runtime_TraceExitEiPmPNS0_7IsolateE.isra.0
	jmp	.L5365
.L5374:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22538:
	.size	_ZN2v88internal17Runtime_TraceExitEiPmPNS0_7IsolateE, .-_ZN2v88internal17Runtime_TraceExitEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Runtime_HaveSameMapEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_HaveSameMapEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_HaveSameMapEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_HaveSameMapEiPmPNS0_7IsolateE:
.LFB22541:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5385
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	$1, %al
	jne	.L5377
.L5378:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5377:
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5378
	movq	-8(%rsi), %rcx
	testb	$1, %cl
	jne	.L5386
.L5379:
	leaq	.LC29(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5386:
	movq	-1(%rcx), %rsi
	cmpw	$1024, 11(%rsi)
	jbe	.L5379
	movq	-1(%rcx), %rcx
	movq	-1(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L5381
	movq	112(%rdx), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5381:
	.cfi_restore_state
	movq	120(%rdx), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5385:
	.cfi_restore 6
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22541:
	.size	_ZN2v88internal19Runtime_HaveSameMapEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_HaveSameMapEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE:
.LFB22544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rdx, %r12
	testl	%eax, %eax
	jne	.L5394
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5389
.L5390:
	leaq	.LC31(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5389:
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	jne	.L5390
	movq	15(%rax), %r13
	movq	37888(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal16LargeObjectSpace8ContainsENS0_10HeapObjectE@PLT
	testb	%al, %al
	je	.L5395
.L5391:
	movq	112(%r12), %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5395:
	.cfi_restore_state
	movq	37872(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal16LargeObjectSpace8ContainsENS0_10HeapObjectE@PLT
	testb	%al, %al
	jne	.L5391
	movq	120(%r12), %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5394:
	.cfi_restore_state
	popq	%r12
	movq	%rdx, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22544:
	.size	_ZN2v88internal38Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE, .-_ZN2v88internal38Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_InYoungGenerationEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_InYoungGenerationEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_InYoungGenerationEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_InYoungGenerationEiPmPNS0_7IsolateE:
.LFB22547:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5403
	movq	(%rsi), %rax
	testb	$1, %al
	je	.L5398
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L5398
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5398:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5403:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL31Stats_Runtime_InYoungGenerationEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22547:
	.size	_ZN2v88internal25Runtime_InYoungGenerationEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_InYoungGenerationEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateE:
.LFB22550:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	testl	%eax, %eax
	jne	.L5418
	movq	(%rdi), %rax
	testb	$1, %al
	jne	.L5406
.L5407:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC34(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5406:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L5407
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	testb	$1, %dl
	jne	.L5419
.L5417:
	movq	120(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5419:
	movq	-1(%rdx), %rdx
	cmpw	$83, 11(%rdx)
	jne	.L5417
	movq	23(%rax), %rdx
	movq	7(%rdx), %rdx
	andl	$1, %edx
	jne	.L5411
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	sarq	$32, %rax
	cmpq	$69, %rax
	je	.L5417
.L5411:
	movq	112(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5418:
	jmp	_ZN2v88internalL27Stats_Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22550:
	.size	_ZN2v88internal21Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE:
.LFB22554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5432
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5433
.L5423:
	leaq	.LC104(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5433:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L5423
	testb	$-2, 43(%rax)
	jne	.L5423
	cmpq	%rax, 112(%r12)
	leaq	_ZN2v88internal12_GLOBAL__N_134DisallowCodegenFromStringsCallbackENS_5LocalINS_7ContextEEENS2_INS_6StringEEE(%rip), %rsi
	movl	$0, %eax
	movq	%r12, %rdi
	cmovne	%rax, %rsi
	call	_ZN2v87Isolate41SetAllowCodeGenerationFromStringsCallbackEPFbNS_5LocalINS_7ContextEEENS1_INS_6StringEEEE@PLT
	movq	88(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5432:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rdx, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22554:
	.size	_ZN2v88internal34Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateE:
.LFB22557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5446
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5447
.L5437:
	leaq	.LC104(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5447:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L5437
	testb	$-2, 43(%rax)
	jne	.L5437
	cmpq	%rax, 112(%r12)
	leaq	_ZN2v88internal12_GLOBAL__N_134DisallowCodegenFromStringsCallbackENS_5LocalINS_7ContextEEENS2_INS_6StringEEE(%rip), %rsi
	movl	$0, %eax
	movq	%r12, %rdi
	cmovne	%rax, %rsi
	call	_ZN2v87Isolate34SetAllowWasmCodeGenerationCallbackEPFbNS_5LocalINS_7ContextEEENS1_INS_6StringEEEE@PLT
	movq	88(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5446:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rdx, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22557:
	.size	_ZN2v88internal27Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal18Runtime_IsWasmCodeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_IsWasmCodeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_IsWasmCodeEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_IsWasmCodeEiPmPNS0_7IsolateE:
.LFB22560:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5456
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5450
.L5451:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC34(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5450:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L5451
	movq	47(%rax), %rax
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$8, %eax
	jne	.L5452
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5452:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5456:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL24Stats_Runtime_IsWasmCodeEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22560:
	.size	_ZN2v88internal18Runtime_IsWasmCodeEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_IsWasmCodeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateE:
.LFB22563:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5461
	cmpb	$0, _ZN2v88internal12trap_handler25g_is_trap_handler_enabledE(%rip)
	je	.L5459
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5459:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5461:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL38Stats_Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22563:
	.size	_ZN2v88internal32Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_IsThreadInWasmEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_IsThreadInWasmEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_IsThreadInWasmEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_IsThreadInWasmEiPmPNS0_7IsolateE:
.LFB22566:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5466
	movq	_ZN2v88internal12trap_handler21g_thread_in_wasm_codeE@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	testl	%eax, %eax
	je	.L5464
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5464:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5466:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL28Stats_Runtime_IsThreadInWasmEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22566:
	.size	_ZN2v88internal22Runtime_IsThreadInWasmEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_IsThreadInWasmEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateE:
.LFB22569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5479
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	call	_ZN2v88internal12trap_handler21GetRecoveredTrapCountEv@PLT
	cmpq	$2147483647, %rax
	ja	.L5469
	movq	41112(%r12), %rdi
	salq	$32, %rax
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L5470
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
.L5473:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5467
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5467:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5470:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L5480
.L5472:
	movq	%r13, (%rax)
	jmp	.L5473
	.p2align 4,,10
	.p2align 3
.L5469:
	testq	%rax, %rax
	js	.L5474
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L5475:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L5473
	.p2align 4,,10
	.p2align 3
.L5474:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L5475
	.p2align 4,,10
	.p2align 3
.L5479:
	popq	%rbx
	movq	%rdx, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL39Stats_Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L5480:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L5472
	.cfi_endproc
.LFE22569:
	.size	_ZN2v88internal33Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateE:
.LFB22572:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5502
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r15
	testb	$1, %al
	jne	.L5483
.L5484:
	leaq	.LC66(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5483:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L5484
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L5503
.L5485:
	leaq	.LC132(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5503:
	movq	-1(%rax), %rax
	cmpw	$1100, 11(%rax)
	jne	.L5485
	movq	%rdx, %rdi
	call	_ZN2v88internal20WasmExceptionPackage15GetExceptionTagEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L5504
.L5488:
	movq	88(%r12), %r13
.L5495:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L5481
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5481:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5504:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$106, 11(%rax)
	jne	.L5488
	movq	-8(%r13), %rax
	movq	41112(%r12), %rdi
	movq	223(%rax), %rsi
	testq	%rdi, %rdi
	je	.L5490
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L5491:
	movl	11(%rsi), %edx
	testl	%edx, %edx
	jle	.L5488
	movq	(%rbx), %rdi
	xorl	%edx, %edx
	jmp	.L5493
	.p2align 4,,10
	.p2align 3
.L5494:
	movq	(%rax), %rsi
	addq	$1, %rdx
	cmpl	%edx, 11(%rsi)
	jle	.L5488
.L5493:
	movq	15(%rsi,%rdx,8), %rcx
	movl	%edx, %r13d
	cmpq	%rcx, %rdi
	jne	.L5494
	salq	$32, %r13
	jmp	.L5495
	.p2align 4,,10
	.p2align 3
.L5502:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L5490:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L5505
.L5492:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L5491
.L5505:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L5492
	.cfi_endproc
.LFE22572:
	.size	_ZN2v88internal26Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE:
.LFB22575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5514
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L5508
.L5509:
	leaq	.LC66(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5508:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L5509
	movq	%rdx, %rdi
	call	_ZN2v88internal20WasmExceptionPackage18GetExceptionValuesEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %rsi
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L5515
.L5510:
	leaq	.LC67(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5515:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	subl	$123, %edx
	cmpw	$14, %dx
	ja	.L5510
	movslq	11(%rax), %rcx
	xorl	%r8d, %r8d
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory22NewJSArrayWithElementsENS0_6HandleINS0_14FixedArrayBaseEEENS0_12ElementsKindEiNS0_14AllocationTypeE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5506
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5506:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5514:
	.cfi_restore_state
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22575:
	.size	_ZN2v88internal30Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateE:
.LFB22580:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5528
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5529
.L5519:
	leaq	.LC104(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5529:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L5519
	testb	$-2, 43(%rax)
	jne	.L5519
	cmpq	%rax, 112(%r12)
	leaq	_ZN2v88internal12_GLOBAL__N_117EnableWasmThreadsENS_5LocalINS_7ContextEEE(%rip), %rsi
	leaq	_ZN2v88internal12_GLOBAL__N_118DisableWasmThreadsENS_5LocalINS_7ContextEEE(%rip), %rax
	movq	%r12, %rdi
	cmovne	%rax, %rsi
	call	_ZN2v87Isolate29SetWasmThreadsEnabledCallbackEPFbNS_5LocalINS_7ContextEEEE@PLT
	movq	88(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5528:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%rdx, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL35Stats_Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22580:
	.size	_ZN2v88internal29Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE:
.LFB22583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5545
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5533
.L5534:
	leaq	.LC108(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5533:
	movq	-1(%rax), %rdx
	cmpw	$1075, 11(%rdx)
	jne	.L5534
	movq	%rax, -32(%rbp)
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L5546
.L5537:
	leaq	.LC109(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5546:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L5537
	testb	$-2, 43(%rax)
	jne	.L5537
	xorl	%esi, %esi
	cmpq	%rax, 112(%r12)
	leaq	-32(%rbp), %rdi
	sete	%sil
	call	_ZNK2v88internal8JSRegExp8BytecodeEb@PLT
	testb	$1, %al
	jne	.L5547
.L5539:
	movq	120(%r12), %rax
.L5530:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5548
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5547:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$70, 11(%rax)
	jne	.L5539
	movq	112(%r12), %rax
	jmp	.L5530
	.p2align 4,,10
	.p2align 3
.L5545:
	movq	%rdx, %rsi
	call	_ZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE.isra.0
	jmp	.L5530
.L5548:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22583:
	.size	_ZN2v88internal25Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateE:
.LFB22586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5564
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5552
.L5553:
	leaq	.LC108(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5552:
	movq	-1(%rax), %rdx
	cmpw	$1075, 11(%rdx)
	jne	.L5553
	movq	%rax, -32(%rbp)
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L5565
.L5556:
	leaq	.LC109(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5565:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L5556
	testb	$-2, 43(%rax)
	jne	.L5556
	xorl	%esi, %esi
	cmpq	%rax, 112(%r12)
	leaq	-32(%rbp), %rdi
	sete	%sil
	call	_ZNK2v88internal8JSRegExp4CodeEb@PLT
	testb	$1, %al
	jne	.L5566
.L5558:
	movq	120(%r12), %rax
.L5549:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5567
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5566:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$69, 11(%rax)
	jne	.L5558
	movq	112(%r12), %rax
	jmp	.L5549
	.p2align 4,,10
	.p2align 3
.L5564:
	movq	%rdx, %rsi
	call	_ZN2v88internalL33Stats_Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateE.isra.0
	jmp	.L5549
.L5567:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22586:
	.size	_ZN2v88internal27Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_HasFastElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_HasFastElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_HasFastElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_HasFastElementsEiPmPNS0_7IsolateE:
.LFB22589:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5576
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5570
.L5571:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5570:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5571
	movq	-1(%rax), %rax
	cmpb	$47, 14(%rax)
	ja	.L5572
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5572:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5576:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL29Stats_Runtime_HasFastElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22589:
	.size	_ZN2v88internal23Runtime_HasFastElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_HasFastElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_HasSmiElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_HasSmiElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_HasSmiElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_HasSmiElementsEiPmPNS0_7IsolateE:
.LFB22592:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5585
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5579
.L5580:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5579:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5580
	movq	-1(%rax), %rax
	cmpb	$15, 14(%rax)
	ja	.L5581
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5581:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5585:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL28Stats_Runtime_HasSmiElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22592:
	.size	_ZN2v88internal22Runtime_HasSmiElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_HasSmiElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_HasObjectElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_HasObjectElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_HasObjectElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_HasObjectElementsEiPmPNS0_7IsolateE:
.LFB22595:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5594
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5588
.L5589:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5588:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5589
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$2, %eax
	cmpb	$1, %al
	ja	.L5590
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5590:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5594:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL31Stats_Runtime_HasObjectElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22595:
	.size	_ZN2v88internal25Runtime_HasObjectElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_HasObjectElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateE:
.LFB22598:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5603
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5597
.L5598:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5597:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5598
	movq	-1(%rax), %rax
	cmpb	$31, 14(%rax)
	ja	.L5599
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5599:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5603:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL36Stats_Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22598:
	.size	_ZN2v88internal30Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_HasDoubleElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_HasDoubleElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_HasDoubleElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_HasDoubleElementsEiPmPNS0_7IsolateE:
.LFB22601:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5612
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5606
.L5607:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5606:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5607
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$4, %eax
	cmpb	$1, %al
	ja	.L5608
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5608:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5612:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL31Stats_Runtime_HasDoubleElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22601:
	.size	_ZN2v88internal25Runtime_HasDoubleElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_HasDoubleElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_HasHoleyElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_HasHoleyElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_HasHoleyElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_HasHoleyElementsEiPmPNS0_7IsolateE:
.LFB22604:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	testl	%eax, %eax
	jne	.L5626
	movq	(%rdi), %rax
	testb	$1, %al
	jne	.L5615
.L5616:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5615:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L5616
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpb	$5, %al
	ja	.L5620
	testb	$1, %al
	jne	.L5617
.L5620:
	movq	120(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5617:
	movq	112(%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5626:
	jmp	_ZN2v88internalL30Stats_Runtime_HasHoleyElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22604:
	.size	_ZN2v88internal24Runtime_HasHoleyElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_HasHoleyElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_HasDictionaryElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_HasDictionaryElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_HasDictionaryElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_HasDictionaryElementsEiPmPNS0_7IsolateE:
.LFB22607:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5635
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5629
.L5630:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5629:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5630
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$12, %eax
	jne	.L5631
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5631:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5635:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL35Stats_Runtime_HasDictionaryElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22607:
	.size	_ZN2v88internal29Runtime_HasDictionaryElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_HasDictionaryElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_HasPackedElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_HasPackedElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_HasPackedElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_HasPackedElementsEiPmPNS0_7IsolateE:
.LFB22610:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5644
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5638
.L5639:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5638:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5639
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$2, %eax
	jne	.L5640
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5640:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5644:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL31Stats_Runtime_HasPackedElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22610:
	.size	_ZN2v88internal25Runtime_HasPackedElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_HasPackedElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateE:
.LFB22613:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5653
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5647
.L5648:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5647:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5648
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$13, %eax
	cmpb	$1, %al
	ja	.L5649
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5649:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5653:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL40Stats_Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22613:
	.size	_ZN2v88internal34Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_HasFastPropertiesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_HasFastPropertiesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_HasFastPropertiesEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_HasFastPropertiesEiPmPNS0_7IsolateE:
.LFB22616:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5662
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5656
.L5657:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5656:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5657
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	jne	.L5658
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5658:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5662:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL31Stats_Runtime_HasFastPropertiesEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22616:
	.size	_ZN2v88internal25Runtime_HasFastPropertiesEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_HasFastPropertiesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateE:
.LFB22619:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5671
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5665
.L5666:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5665:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5666
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$17, %eax
	jne	.L5667
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5667:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5671:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL35Stats_Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22619:
	.size	_ZN2v88internal29Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateE:
.LFB22622:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5680
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5674
.L5675:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5674:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5675
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$18, %eax
	jne	.L5676
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5676:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5680:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL34Stats_Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22622:
	.size	_ZN2v88internal28Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateE:
.LFB22625:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5689
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5683
.L5684:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5683:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5684
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$19, %eax
	jne	.L5685
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5685:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5689:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL36Stats_Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22625:
	.size	_ZN2v88internal30Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateE:
.LFB22628:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5698
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5692
.L5693:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5692:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5693
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$20, %eax
	jne	.L5694
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5694:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5698:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL35Stats_Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22628:
	.size	_ZN2v88internal29Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateE:
.LFB22631:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5707
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5701
.L5702:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5701:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5702
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$21, %eax
	jne	.L5703
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5703:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5707:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL36Stats_Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22631:
	.size	_ZN2v88internal30Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal30Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateE:
.LFB22634:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5716
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5710
.L5711:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5710:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5711
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$22, %eax
	jne	.L5712
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5712:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5716:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL35Stats_Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22634:
	.size	_ZN2v88internal29Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateE:
.LFB22637:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5725
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5719
.L5720:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5719:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5720
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$23, %eax
	jne	.L5721
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5721:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5725:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL37Stats_Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22637:
	.size	_ZN2v88internal31Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateE:
.LFB22640:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5734
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5728
.L5729:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5728:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5729
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$24, %eax
	jne	.L5730
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5730:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5734:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL37Stats_Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22640:
	.size	_ZN2v88internal31Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateE:
.LFB22643:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5743
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5737
.L5738:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5737:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5738
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$25, %eax
	jne	.L5739
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5739:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5743:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL42Stats_Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22643:
	.size	_ZN2v88internal36Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal36Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateE:
.LFB22646:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5752
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5746
.L5747:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5746:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5747
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$26, %eax
	jne	.L5748
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5748:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5752:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL39Stats_Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22646:
	.size	_ZN2v88internal33Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateE:
.LFB22649:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5761
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5755
.L5756:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5755:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5756
	movq	-1(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	cmpl	$27, %eax
	jne	.L5757
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5757:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5761:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL38Stats_Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22649:
	.size	_ZN2v88internal32Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateE:
.LFB22652:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5769
	movq	4520(%rdx), %rax
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L5764
	sarq	$32, %rax
	cmpq	$1, %rax
	je	.L5770
.L5764:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5770:
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5769:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL35Stats_Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22652:
	.size	_ZN2v88internal29Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_MapIteratorProtectorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_MapIteratorProtectorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_MapIteratorProtectorEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_MapIteratorProtectorEiPmPNS0_7IsolateE:
.LFB22655:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5775
	movq	4584(%rdx), %rcx
	movabsq	$4294967296, %rax
	cmpq	%rax, 23(%rcx)
	jne	.L5773
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5773:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5775:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL34Stats_Runtime_MapIteratorProtectorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22655:
	.size	_ZN2v88internal28Runtime_MapIteratorProtectorEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_MapIteratorProtectorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_SetIteratorProtectorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_SetIteratorProtectorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_SetIteratorProtectorEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_SetIteratorProtectorEiPmPNS0_7IsolateE:
.LFB22658:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5780
	movq	4600(%rdx), %rcx
	movabsq	$4294967296, %rax
	cmpq	%rax, 23(%rcx)
	jne	.L5778
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5778:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5780:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL34Stats_Runtime_SetIteratorProtectorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22658:
	.size	_ZN2v88internal28Runtime_SetIteratorProtectorEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_SetIteratorProtectorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_StringIteratorProtectorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_StringIteratorProtectorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_StringIteratorProtectorEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_StringIteratorProtectorEiPmPNS0_7IsolateE:
.LFB22661:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5785
	movq	4608(%rdx), %rcx
	movabsq	$4294967296, %rax
	cmpq	%rax, 23(%rcx)
	jne	.L5783
	movq	112(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5783:
	movq	120(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5785:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL37Stats_Runtime_StringIteratorProtectorEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22661:
	.size	_ZN2v88internal31Runtime_StringIteratorProtectorEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_StringIteratorProtectorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_SerializeWasmModuleEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_SerializeWasmModuleEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_SerializeWasmModuleEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_SerializeWasmModuleEiPmPNS0_7IsolateE:
.LFB22664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5805
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -112(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -104(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5789
.L5790:
	leaq	.LC69(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5789:
	movq	-1(%rax), %rdx
	cmpw	$1102, 11(%rdx)
	jne	.L5790
	movq	23(%rax), %rax
	leaq	-96(%rbp), %r15
	movq	%r15, %rdi
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rsi
	call	_ZN2v88internal4wasm14WasmSerializerC1EPNS1_12NativeModuleE@PLT
	movq	%r15, %rdi
	call	_ZNK2v88internal4wasm14WasmSerializer29GetSerializedNativeModuleSizeEv@PLT
	movq	45544(%r12), %rdi
	movq	%rax, %r13
	movq	(%rdi), %rax
	movq	%r13, %rsi
	call	*16(%rax)
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88internal7Factory16NewJSArrayBufferENS0_10SharedFlagENS0_14AllocationTypeE@PLT
	subq	$8, %rsp
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	pushq	$0
	movq	%rax, %rdi
	movq	%r13, %r8
	movq	%r14, %rcx
	movq	%r12, %rsi
	movq	%rax, %rbx
	call	_ZN2v88internal13JSArrayBuffer5SetupENS0_6HandleIS1_EEPNS0_7IsolateEbPvmNS0_10SharedFlagEb@PLT
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L5793
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZNK2v88internal4wasm14WasmSerializer21SerializeNativeModuleENS0_6VectorIhEE@PLT
	testb	%al, %al
	jne	.L5806
.L5793:
	movq	88(%r12), %r13
.L5792:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5794
	call	_ZdlPv@PLT
.L5794:
	subl	$1, 41104(%r12)
	movq	-112(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-104(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L5786
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5786:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5807
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5806:
	.cfi_restore_state
	movq	(%rbx), %r13
	jmp	.L5792
	.p2align 4,,10
	.p2align 3
.L5805:
	call	_ZN2v88internalL33Stats_Runtime_SerializeWasmModuleEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L5786
.L5807:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22664:
	.size	_ZN2v88internal27Runtime_SerializeWasmModuleEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_SerializeWasmModuleEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE:
.LFB22670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5825
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L5826
.L5811:
	leaq	.LC145(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5826:
	movq	-1(%rdx), %rax
	cmpw	$1059, 11(%rax)
	jne	.L5811
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L5827
.L5812:
	leaq	.LC146(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5827:
	movq	-1(%rax), %rcx
	cmpw	$1086, 11(%rcx)
	jne	.L5812
	movl	39(%rdx), %edx
	andl	$4, %edx
	jne	.L5828
	movq	23(%rax), %rdx
	movl	39(%rdx), %edx
	andl	$4, %edx
	jne	.L5829
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal12JSTypedArray9GetBufferEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r8
	movq	-8(%r13), %rax
	movq	(%r8), %rdx
	movq	31(%rax), %rcx
	movq	39(%rax), %r8
	addq	31(%rdx), %rcx
	movq	0(%r13), %rdx
	movslq	23(%rdx), %r9
	movq	31(%rdx), %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal4wasm23DeserializeNativeModuleEPNS0_7IsolateENS0_6VectorIKhEES6_@PLT
	testq	%rax, %rax
	je	.L5830
	movq	(%rax), %r13
.L5818:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5808
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5808:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5831
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5830:
	.cfi_restore_state
	movq	88(%r12), %r13
	jmp	.L5818
	.p2align 4,,10
	.p2align 3
.L5825:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L5808
	.p2align 4,,10
	.p2align 3
.L5828:
	leaq	.LC147(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5829:
	leaq	.LC148(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L5831:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22670:
	.size	_ZN2v88internal29Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_CloneWasmModuleEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_CloneWasmModuleEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_CloneWasmModuleEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_CloneWasmModuleEiPmPNS0_7IsolateE:
.LFB22673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5866
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L5835
.L5836:
	leaq	.LC69(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5835:
	movq	-1(%rax), %rax
	cmpw	$1102, 11(%rax)
	jne	.L5836
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal4wasm10WasmEngine13GetWasmEngineEv@PLT
	movq	0(%r13), %rax
	movq	-80(%rbp), %rdi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	%rdx, -96(%rbp)
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L5837
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L5838
	lock addl	$1, 8(%rax)
.L5837:
	leaq	-96(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm10WasmEngine18ImportNativeModuleEPNS0_7IsolateESt10shared_ptrINS1_12NativeModuleEE@PLT
	movq	-88(%rbp), %r13
	testq	%r13, %r13
	je	.L5840
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L5841
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r13)
	cmpl	$1, %edx
	je	.L5867
	.p2align 4,,10
	.p2align 3
.L5840:
	movq	-72(%rbp), %r13
	testq	%r13, %r13
	je	.L5855
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L5849
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r13)
	cmpl	$1, %edx
	je	.L5868
	.p2align 4,,10
	.p2align 3
.L5855:
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5832
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5832:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5869
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5838:
	.cfi_restore_state
	addl	$1, 8(%rax)
	jmp	.L5837
	.p2align 4,,10
	.p2align 3
.L5841:
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %edx
	jne	.L5840
.L5867:
	movq	0(%r13), %rdx
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rcx
	movq	16(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L5870
.L5844:
	testq	%r15, %r15
	je	.L5845
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r13)
.L5846:
	cmpl	$1, %edx
	jne	.L5840
	movq	0(%r13), %rdx
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rsi
	movq	%rax, -104(%rbp)
	movq	%r13, %rdi
	movq	24(%rdx), %rcx
	cmpq	%rsi, %rcx
	jne	.L5847
	call	*8(%rdx)
	movq	-104(%rbp), %rax
	jmp	.L5840
	.p2align 4,,10
	.p2align 3
.L5849:
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %edx
	jne	.L5855
.L5868:
	movq	0(%r13), %rdx
	leaq	_ZNSt15_Sp_counted_ptrIDnLN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv(%rip), %rcx
	movq	16(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L5871
.L5851:
	testq	%r15, %r15
	je	.L5852
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r13)
.L5853:
	cmpl	$1, %edx
	jne	.L5855
	movq	0(%r13), %rdx
	leaq	_ZNSt16_Sp_counted_baseILN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv(%rip), %rsi
	movq	%rax, -104(%rbp)
	movq	%r13, %rdi
	movq	24(%rdx), %rcx
	cmpq	%rsi, %rcx
	jne	.L5854
	call	*8(%rdx)
	movq	-104(%rbp), %rax
	jmp	.L5855
	.p2align 4,,10
	.p2align 3
.L5866:
	call	_ZN2v88internalL29Stats_Runtime_CloneWasmModuleEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L5832
	.p2align 4,,10
	.p2align 3
.L5852:
	movl	12(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r13)
	jmp	.L5853
	.p2align 4,,10
	.p2align 3
.L5845:
	movl	12(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r13)
	jmp	.L5846
	.p2align 4,,10
	.p2align 3
.L5847:
	call	*%rcx
	movq	-104(%rbp), %rax
	jmp	.L5840
	.p2align 4,,10
	.p2align 3
.L5854:
	call	*%rcx
	movq	-104(%rbp), %rax
	jmp	.L5855
	.p2align 4,,10
	.p2align 3
.L5870:
	movq	%rax, -104(%rbp)
	movq	%r13, %rdi
	call	*%rdx
	movq	-104(%rbp), %rax
	jmp	.L5844
	.p2align 4,,10
	.p2align 3
.L5871:
	movq	%rax, -104(%rbp)
	movq	%r13, %rdi
	call	*%rdx
	movq	-104(%rbp), %rax
	jmp	.L5851
.L5869:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22673:
	.size	_ZN2v88internal23Runtime_CloneWasmModuleEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_CloneWasmModuleEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal24Runtime_HeapObjectVerifyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_HeapObjectVerifyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_HeapObjectVerifyEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_HeapObjectVerifyEiPmPNS0_7IsolateE:
.LFB22682:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	testl	%eax, %eax
	jne	.L5881
	movl	41104(%rdx), %eax
	leal	1(%rax), %edx
	movl	%edx, 41104(%rsi)
	movq	(%rdi), %rdx
	testb	$1, %dl
	je	.L5874
	movq	-1(%rdx), %rdx
	movq	-1(%rdx), %rdx
	cmpw	$68, 11(%rdx)
	jne	.L5882
.L5874:
	movq	112(%rsi), %r8
	movl	%eax, 41104(%rsi)
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5881:
	jmp	_ZN2v88internalL30Stats_Runtime_HeapObjectVerifyEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L5882:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC64(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22682:
	.size	_ZN2v88internal24Runtime_HeapObjectVerifyEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_HeapObjectVerifyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal32Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal32Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal32Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE, @function
_ZN2v88internal32Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE:
.LFB22685:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5896
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5885
.L5886:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC69(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5885:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rdx
	cmpw	$1102, 11(%rdx)
	jne	.L5886
	movq	47(%rax), %rdx
	movl	19(%rdx), %eax
	leaq	15(%rdx), %rsi
	testl	%eax, %eax
	jle	.L5892
	movq	(%rsi), %r8
	addq	$23, %rdx
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	sarq	$32, %r8
	.p2align 4,,10
	.p2align 3
.L5891:
	movq	(%rdx), %rsi
	movq	%rsi, %rdi
	andl	$3, %edi
	cmpq	$3, %rdi
	jne	.L5889
	cmpl	$3, %esi
	setne	%sil
	movzbl	%sil, %esi
	addl	%esi, %eax
.L5889:
	addl	$1, %ecx
	addq	$8, %rdx
	cmpl	%r8d, %ecx
	jl	.L5891
	salq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5896:
	jmp	_ZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L5892:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22685:
	.size	_ZN2v88internal32Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE, .-_ZN2v88internal32Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateE:
.LFB22688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5915
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L5900
.L5901:
	leaq	.LC95(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5900:
	movq	-1(%rax), %rdx
	cmpw	$1100, 11(%rdx)
	jne	.L5901
	movq	191(%rax), %rdx
	andq	$-262144, %rax
	xorl	%r13d, %r13d
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rdx
	je	.L5902
	leaq	-48(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	call	_ZN2v88internal13WasmDebugInfo19NumInterpretedCallsEv@PLT
	movq	%rax, %r13
	cmpq	$2147483647, %rax
	ja	.L5903
	movq	41112(%r12), %rdi
	salq	$32, %r13
	testq	%rdi, %rdi
	je	.L5904
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	.p2align 4,,10
	.p2align 3
.L5902:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5897
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5897:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5916
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5904:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L5917
.L5906:
	movq	%r13, (%rax)
	jmp	.L5902
	.p2align 4,,10
	.p2align 3
.L5903:
	testq	%rax, %rax
	js	.L5908
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L5909:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L5902
	.p2align 4,,10
	.p2align 3
.L5915:
	call	_ZN2v88internalL37Stats_Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L5897
	.p2align 4,,10
	.p2align 3
.L5908:
	shrq	%rax
	andl	$1, %r13d
	pxor	%xmm0, %xmm0
	orq	%r13, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L5909
	.p2align 4,,10
	.p2align 3
.L5917:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L5906
.L5916:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22688:
	.size	_ZN2v88internal31Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateE, .-_ZN2v88internal31Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE:
.LFB22691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5927
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L5921
.L5922:
	leaq	.LC95(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5921:
	movq	-1(%rax), %rax
	cmpw	$1100, 11(%rax)
	jne	.L5922
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L5928
	sarq	$32, %rax
	movl	%eax, -44(%rbp)
	call	_ZN2v88internal18WasmInstanceObject20GetOrCreateDebugInfoENS0_6HandleIS1_EE@PLT
	leaq	-44(%rbp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	call	_ZN2v88internal13WasmDebugInfo21RedirectToInterpreterENS0_6HandleIS1_EENS0_6VectorIiEE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5918
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5918:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5929
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5927:
	.cfi_restore_state
	movl	%r8d, %edi
	call	_ZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L5918
	.p2align 4,,10
	.p2align 3
.L5928:
	leaq	.LC96(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L5929:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22691:
	.size	_ZN2v88internal33Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE:
.LFB22694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$1576, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5941
	addl	$1, 41104(%rdx)
	movq	(%rsi), %r14
	movq	41088(%rdx), %r11
	movq	41096(%rdx), %rbx
	testb	$1, %r14b
	jne	.L5942
	leaq	-1568(%rbp), %r9
	movq	%r11, -1616(%rbp)
	movq	%r9, %rdi
	movq	%r9, -1608(%rbp)
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	%r12, %rsi
	leaq	-1504(%rbp), %rdi
	call	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-88(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	movq	%r13, %rdi
	movq	159(%rax), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rsi
	movq	%rsi, -1600(%rbp)
	call	_ZNK2v88internal17WasmCompiledFrame14function_indexEv@PLT
	movq	%r13, %rdi
	movl	%eax, -1592(%rbp)
	movq	0(%r13), %rax
	call	*104(%rax)
	movq	%r13, %rdi
	movl	%eax, -1588(%rbp)
	call	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv@PLT
	leaq	-1576(%rbp), %rdi
	movq	%rax, -1576(%rbp)
	call	_ZN2v88internal18WasmInstanceObject6moduleEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	movslq	-1592(%rbp), %rax
	salq	$5, %rax
	addq	136(%r15), %rax
	movl	16(%rax), %r15d
	call	_ZNK2v88internal17WasmCompiledFrame9wasm_codeEv@PLT
	movq	-1600(%rbp), %rsi
	movl	-1588(%rbp), %ecx
	movzbl	136(%rax), %edi
	movl	$3, %eax
	movl	-1592(%rbp), %edx
	movq	%rsi, %r8
	movq	%r14, %rsi
	cmpb	$2, %dil
	cmovne	%eax, %edi
	subl	%r15d, %ecx
	call	_ZN2v88internal4wasm20TraceMemoryOperationENS1_13ExecutionTierEPKNS1_17MemoryTracingInfoEiiPh@PLT
	movq	-1608(%rbp), %r9
	movq	88(%r12), %r13
	movq	%r9, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	-1616(%rbp), %r11
	subl	$1, 41104(%r12)
	movq	%r11, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5930
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5930:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5943
	addq	$1576, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5941:
	.cfi_restore_state
	movq	%rdx, %rsi
	call	_ZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L5930
	.p2align 4,,10
	.p2align 3
.L5942:
	leaq	.LC98(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L5943:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22694:
	.size	_ZN2v88internal23Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_WasmTraceMemoryEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateE:
.LFB22700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5952
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %r13
	testb	$1, %al
	jne	.L5946
.L5947:
	leaq	.LC95(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5946:
	movq	-1(%rax), %rdx
	cmpw	$1100, 11(%rdx)
	jne	.L5947
	movq	-8(%rsi), %rcx
	testb	$1, %cl
	jne	.L5953
	movq	135(%rax), %rax
	sarq	$32, %rcx
	movl	$3, %r8d
	movq	%r12, %rsi
	movq	45752(%r12), %rdi
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rbx
	movq	%rbx, %rdx
	call	_ZN2v88internal4wasm10WasmEngine15CompileFunctionEPNS0_7IsolateEPNS1_12NativeModuleEjNS1_13ExecutionTierE@PLT
	movq	520(%rbx), %rdi
	call	_ZNK2v88internal4wasm16CompilationState6failedEv@PLT
	testb	%al, %al
	jne	.L5954
	subl	$1, 41104(%r12)
	movq	88(%r12), %r15
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L5944
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5944:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5952:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Stats_Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L5953:
	.cfi_restore_state
	leaq	.LC96(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5954:
	leaq	.LC135(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22700:
	.size	_ZN2v88internal26Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateE:
.LFB22703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5971
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rdi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %dil
	jne	.L5972
.L5958:
	leaq	.LC34(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5972:
	movq	-1(%rdi), %rax
	cmpw	$1105, 11(%rax)
	jne	.L5958
	call	_ZN2v88internal20WasmExportedFunction22IsWasmExportedFunctionENS0_6ObjectE@PLT
	testb	%al, %al
	je	.L5973
	movq	0(%r13), %rax
	leaq	-128(%rbp), %r15
	movq	%r15, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal20WasmExportedFunction8instanceEv@PLT
	movq	%r15, %rdi
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %r8
	movq	0(%r13), %rax
	movq	%r8, -136(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZN2v88internal20WasmExportedFunction14function_indexEv@PLT
	movq	%r15, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	-136(%rbp), %r8
	movl	%r13d, %esi
	movq	%r8, %rdi
	call	_ZNK2v88internal4wasm12NativeModule7GetCodeEj@PLT
	testq	%rax, %rax
	je	.L5961
	cmpb	$2, 136(%rax)
	jne	.L5961
	movq	112(%r12), %r13
.L5962:
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5955
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5955:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5974
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5961:
	.cfi_restore_state
	movq	120(%r12), %r13
	jmp	.L5962
	.p2align 4,,10
	.p2align 3
.L5971:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL31Stats_Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L5955
	.p2align 4,,10
	.p2align 3
.L5973:
	leaq	.LC150(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L5974:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22703:
	.size	_ZN2v88internal25Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateE:
.LFB22706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5983
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L5984
.L5978:
	leaq	.LC28(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5984:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L5978
	movq	-1(%rax), %rax
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal3Map29CompleteInobjectSlackTrackingEPNS0_7IsolateE@PLT
	movq	%r14, 41088(%r12)
	movq	88(%r12), %r13
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L5975
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5975:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5985
	addq	$16, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5983:
	.cfi_restore_state
	movq	%rdx, %rsi
	call	_ZN2v88internalL43Stats_Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L5975
.L5985:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22706:
	.size	_ZN2v88internal37Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateE, .-_ZN2v88internal37Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateE:
.LFB22709:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	testl	%eax, %eax
	jne	.L5992
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5988
.L5989:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC95(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L5988:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rcx
	cmpw	$1100, 11(%rcx)
	jne	.L5989
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movb	$1, 677(%rax)
	movq	88(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5992:
	movq	%rdx, %rsi
	jmp	_ZN2v88internalL39Stats_Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22709:
	.size	_ZN2v88internal33Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal28Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal28Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateE
	.type	_ZN2v88internal28Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateE, @function
_ZN2v88internal28Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateE:
.LFB22712:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5995
	movq	88(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5995:
	movq	%rdx, %rdi
	jmp	_ZN2v88internalL34Stats_Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22712:
	.size	_ZN2v88internal28Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateE, .-_ZN2v88internal28Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateE
	.section	.text._ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev,"axG",@progbits,_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.type	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev, @function
_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev:
.LFB24860:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5996
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L5996:
	ret
	.cfi_endproc
.LFE24860:
	.size	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev, .-_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.weak	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	.set	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev,_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED2Ev
	.section	.rodata._ZN2v88internalL19Stats_Runtime_AbortEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC152:
	.string	"V8.Runtime_Runtime_Abort"
.LC153:
	.string	"abort: %s\n"
	.section	.text._ZN2v88internalL19Stats_Runtime_AbortEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL19Stats_Runtime_AbortEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL19Stats_Runtime_AbortEiPmPNS0_7IsolateE.isra.0:
.LFB29157:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$550, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-128(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL19Stats_Runtime_AbortEiPmPNS0_7IsolateEE28trace_event_unique_atomic818(%rip), %rbx
	testq	%rbx, %rbx
	je	.L6008
.L6000:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L6009
.L6001:
	movq	0(%r13), %rdi
	testb	$1, %dil
	jne	.L6010
	sarq	$32, %rdi
	call	_ZN2v88internal14GetAbortReasonENS0_11AbortReasonE@PLT
	leaq	.LC153(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	stderr(%rip), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	call	_ZN2v84base2OS5AbortEv@PLT
.L6008:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL19Stats_Runtime_AbortEiPmPNS0_7IsolateEE28trace_event_unique_atomic818(%rip)
	movq	%rax, %rbx
	jmp	.L6000
.L6009:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r15
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC152(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r15
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-72(%rbp), %rdi
	movq	%rax, %r14
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r15, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	leaq	.LC152(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L6001
.L6010:
	leaq	.LC98(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE29157:
	.size	_ZN2v88internalL19Stats_Runtime_AbortEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL19Stats_Runtime_AbortEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal13Runtime_AbortEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal13Runtime_AbortEiPmPNS0_7IsolateE
	.type	_ZN2v88internal13Runtime_AbortEiPmPNS0_7IsolateE, @function
_ZN2v88internal13Runtime_AbortEiPmPNS0_7IsolateE:
.LFB22521:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L6015
	movq	(%rsi), %rdi
	testb	$1, %dil
	jne	.L6016
	sarq	$32, %rdi
	call	_ZN2v88internal14GetAbortReasonENS0_11AbortReasonE@PLT
	leaq	.LC153(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	stderr(%rip), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	call	_ZN2v84base2OS5AbortEv@PLT
.L6015:
	movq	%rdx, %rsi
	call	_ZN2v88internalL19Stats_Runtime_AbortEiPmPNS0_7IsolateE.isra.0
.L6016:
	leaq	.LC98(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22521:
	.size	_ZN2v88internal13Runtime_AbortEiPmPNS0_7IsolateE, .-_ZN2v88internal13Runtime_AbortEiPmPNS0_7IsolateE
	.section	.text._ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED2Ev,"axG",@progbits,_ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED2Ev
	.type	_ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED2Ev, @function
_ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED2Ev:
.LFB25235:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L6017
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L6017:
	ret
	.cfi_endproc
.LFE25235:
	.size	_ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED2Ev, .-_ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED2Ev
	.weak	_ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED1Ev
	.set	_ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED1Ev,_ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED2Ev
	.section	.rodata._ZN2v88internalL21Stats_Runtime_AbortJSEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC154:
	.string	"V8.Runtime_Runtime_AbortJS"
.LC155:
	.string	"[disabled] abort: %s\n"
	.section	.text._ZN2v88internalL21Stats_Runtime_AbortJSEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL21Stats_Runtime_AbortJSEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL21Stats_Runtime_AbortJSEiPmPNS0_7IsolateE.isra.0:
.LFB29226:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L6056
.L6020:
	movq	_ZZN2v88internalL21Stats_Runtime_AbortJSEiPmPNS0_7IsolateEE28trace_event_unique_atomic829(%rip), %rbx
	testq	%rbx, %rbx
	je	.L6057
.L6022:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L6058
.L6024:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L6028
.L6029:
	leaq	.LC18(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L6057:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L6059
.L6023:
	movq	%rbx, _ZZN2v88internalL21Stats_Runtime_AbortJSEiPmPNS0_7IsolateEE28trace_event_unique_atomic829(%rip)
	jmp	.L6022
	.p2align 4,,10
	.p2align 3
.L6028:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L6029
	cmpb	$0, _ZN2v88internal20FLAG_disable_abortjsE(%rip)
	movq	%rax, -152(%rbp)
	je	.L6030
	leaq	-160(%rbp), %rdi
	leaq	-152(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-160(%rbp), %rsi
	leaq	.LC155(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6031
	call	_ZdaPv@PLT
.L6031:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L6035
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L6035:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L6060
.L6033:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6061
	leaq	-32(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6058:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L6062
.L6025:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6026
	movq	(%rdi), %rax
	call	*8(%rax)
.L6026:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6027
	movq	(%rdi), %rax
	call	*8(%rax)
.L6027:
	leaq	.LC154(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L6024
	.p2align 4,,10
	.p2align 3
.L6056:
	movq	40960(%rsi), %rax
	movl	$551, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L6020
	.p2align 4,,10
	.p2align 3
.L6060:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L6033
	.p2align 4,,10
	.p2align 3
.L6062:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC154(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L6025
	.p2align 4,,10
	.p2align 3
.L6059:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L6023
.L6061:
	call	__stack_chk_fail@PLT
.L6030:
	leaq	-160(%rbp), %r13
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	leaq	-152(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-160(%rbp), %rsi
	xorl	%eax, %eax
	leaq	.LC153(%rip), %rdi
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED1Ev
	movq	stderr(%rip), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	call	_ZN2v84base2OS5AbortEv@PLT
	.cfi_endproc
.LFE29226:
	.size	_ZN2v88internalL21Stats_Runtime_AbortJSEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL21Stats_Runtime_AbortJSEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal15Runtime_AbortJSEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal15Runtime_AbortJSEiPmPNS0_7IsolateE
	.type	_ZN2v88internal15Runtime_AbortJSEiPmPNS0_7IsolateE, @function
_ZN2v88internal15Runtime_AbortJSEiPmPNS0_7IsolateE:
.LFB22524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L6077
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L6066
.L6067:
	leaq	.LC18(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L6066:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L6067
	cmpb	$0, _ZN2v88internal20FLAG_disable_abortjsE(%rip)
	movq	%rax, -56(%rbp)
	je	.L6068
	leaq	-48(%rbp), %rdi
	leaq	-56(%rbp), %rsi
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-48(%rbp), %rsi
	leaq	.LC155(%rip), %rdi
	xorl	%eax, %eax
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6069
	call	_ZdaPv@PLT
.L6069:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L6071
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L6071:
	xorl	%eax, %eax
.L6063:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L6078
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6077:
	.cfi_restore_state
	movq	%rdx, %rsi
	call	_ZN2v88internalL21Stats_Runtime_AbortJSEiPmPNS0_7IsolateE.isra.0
	jmp	.L6063
.L6078:
	call	__stack_chk_fail@PLT
.L6068:
	leaq	-48(%rbp), %r13
	xorl	%r8d, %r8d
	leaq	-56(%rbp), %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-48(%rbp), %rsi
	xorl	%eax, %eax
	leaq	.LC153(%rip), %rdi
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED1Ev
	movq	stderr(%rip), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	call	_ZN2v84base2OS5AbortEv@PLT
	.cfi_endproc
.LFE22524:
	.size	_ZN2v88internal15Runtime_AbortJSEiPmPNS0_7IsolateE, .-_ZN2v88internal15Runtime_AbortJSEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC156:
	.string	"V8.Runtime_Runtime_AbortCSAAssert"
	.section	.rodata._ZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC157:
	.string	"abort: CSA_ASSERT failed: %s\n"
	.section	.text._ZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateE.isra.0:
.LFB29227:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$552, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	leaq	-128(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal21RuntimeCallTimerScopeC1EPNS0_7IsolateENS0_20RuntimeCallCounterIdE
	movq	_ZZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic843(%rip), %rbx
	testq	%rbx, %rbx
	je	.L6090
.L6081:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L6091
.L6082:
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L6083
.L6084:
	leaq	.LC18(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L6090:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*16(%rax)
	movq	%rax, _ZZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic843(%rip)
	movq	%rax, %rbx
	jmp	.L6081
.L6083:
	leaq	-168(%rbp), %rsi
	movq	%rax, -168(%rbp)
	movq	%rsi, %rdi
	call	_ZNK2v88internal10HeapObject8map_wordEPNS0_7IsolateE.isra.0
	cmpw	$63, 11(%rax)
	ja	.L6084
	movq	0(%r13), %rax
	leaq	-176(%rbp), %r13
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-176(%rbp), %rsi
	xorl	%eax, %eax
	leaq	.LC157(%rip), %rdi
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED1Ev
	movq	stderr(%rip), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	call	_ZN2v84base2OS5AbortEv@PLT
.L6091:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r15
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%rbx, %rdx
	movq	%rax, %rdi
	pushq	%rax
	leaq	.LC156(%rip), %rcx
	movl	$88, %esi
	movq	(%rdi), %rax
	pushq	$0
	pushq	%r15
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	leaq	-72(%rbp), %rdi
	movq	%rax, %r14
	addq	$64, %rsp
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	movq	%r15, %rdi
	call	_ZNSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteIS1_EED1Ev
	leaq	.LC156(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L6082
	.cfi_endproc
.LFE29227:
	.size	_ZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal22Runtime_AbortCSAAssertEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_AbortCSAAssertEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_AbortCSAAssertEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_AbortCSAAssertEiPmPNS0_7IsolateE:
.LFB22527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L6098
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L6094
.L6095:
	leaq	.LC18(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L6094:
	leaq	-32(%rbp), %r13
	movq	%rax, -32(%rbp)
	movq	%r13, %rdi
	call	_ZNK2v88internal10HeapObject8map_wordEPNS0_7IsolateE.isra.0
	cmpw	$63, 11(%rax)
	ja	.L6095
	movq	(%rsi), %rax
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$1, %edx
	leaq	-40(%rbp), %rsi
	movq	%rax, -40(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-32(%rbp), %rsi
	xorl	%eax, %eax
	leaq	.LC157(%rip), %rdi
	call	_ZN2v84base2OS10PrintErrorEPKcz@PLT
	movq	%r13, %rdi
	call	_ZNSt10unique_ptrIA_cSt14default_deleteIS0_EED1Ev
	movq	stderr(%rip), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate10PrintStackEP8_IO_FILENS1_14PrintStackModeE@PLT
	call	_ZN2v84base2OS5AbortEv@PLT
.L6098:
	movq	%rdx, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22527:
	.size	_ZN2v88internal22Runtime_AbortCSAAssertEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_AbortCSAAssertEiPmPNS0_7IsolateE
	.section	.text._ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.type	_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, @function
_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm:
.LFB27928:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L6100
	movq	(%rbx), %r8
.L6101:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L6110
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L6111:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6100:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L6124
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L6125
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L6103:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L6105
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L6106
	.p2align 4,,10
	.p2align 3
.L6107:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L6108:
	testq	%rsi, %rsi
	je	.L6105
.L6106:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L6107
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L6113
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L6106
	.p2align 4,,10
	.p2align 3
.L6105:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L6109
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L6109:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L6101
	.p2align 4,,10
	.p2align 3
.L6110:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L6112
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L6112:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L6111
	.p2align 4,,10
	.p2align 3
.L6113:
	movq	%rdx, %rdi
	jmp	.L6108
	.p2align 4,,10
	.p2align 3
.L6124:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L6103
.L6125:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE27928:
	.size	_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, .-_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.section	.rodata._ZN2v88internalL41Stats_Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC158:
	.string	"V8.Runtime_Runtime_EnableCodeLoggingForTesting"
	.section	.text._ZN2v88internalL41Stats_Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Stats_Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL41Stats_Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateE.isra.0:
.LFB29217:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L6171
.L6127:
	movq	_ZZN2v88internalL41Stats_Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1324(%rip), %r12
	testq	%r12, %r12
	je	.L6172
.L6129:
	movq	$0, -160(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L6173
.L6131:
	movzbl	_ZGVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener(%rip), %eax
	testb	%al, %al
	je	.L6174
.L6136:
	movq	45752(%rbx), %rdi
	movq	%rbx, %rsi
	leaq	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener(%rip), %r12
	call	_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE@PLT
	movq	41488(%rbx), %r13
	leaq	56(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%r13), %rsi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L6138
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L6140
	.p2align 4,,10
	.p2align 3
.L6175:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L6138
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r15
	jne	.L6138
.L6140:
	cmpq	%r12, %rdi
	jne	.L6175
.L6139:
	movq	%r14, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	leaq	-160(%rbp), %rdi
	movq	88(%rbx), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L6176
.L6126:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6177
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6172:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L6178
.L6130:
	movq	%r12, _ZZN2v88internalL41Stats_Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1324(%rip)
	jmp	.L6129
	.p2align 4,,10
	.p2align 3
.L6174:
	leaq	_ZGVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L6136
	leaq	16+_ZTVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE12NoopListener(%rip), %rax
	leaq	_ZGVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener(%rip), %rdi
	movq	%rax, _ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L6136
	.p2align 4,,10
	.p2align 3
.L6173:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L6179
.L6132:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6133
	movq	(%rdi), %rax
	call	*8(%rax)
.L6133:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L6134
	movq	(%rdi), %rax
	call	*8(%rax)
.L6134:
	leaq	.LC158(%rip), %rax
	movq	%r12, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L6131
	.p2align 4,,10
	.p2align 3
.L6138:
	movl	$16, %edi
	call	_Znwm@PLT
	movl	$1, %r8d
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r12, 8(%rax)
	movq	%rax, %rcx
	leaq	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener(%rip), %rdx
	movq	$0, (%rax)
	call	_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	jmp	.L6139
	.p2align 4,,10
	.p2align 3
.L6171:
	movq	40960(%rdi), %rax
	leaq	-120(%rbp), %rsi
	movl	$569, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L6127
	.p2align 4,,10
	.p2align 3
.L6176:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L6126
	.p2align 4,,10
	.p2align 3
.L6179:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC158(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L6132
	.p2align 4,,10
	.p2align 3
.L6178:
	leaq	.LC3(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L6130
.L6177:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29217:
	.size	_ZN2v88internalL41Stats_Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL41Stats_Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal35Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateE:
.LFB22715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L6199
	movzbl	_ZGVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener(%rip), %eax
	testb	%al, %al
	je	.L6200
.L6183:
	movq	45752(%r12), %rdi
	movq	%r12, %rsi
	leaq	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener(%rip), %rbx
	call	_ZN2v88internal4wasm10WasmEngine17EnableCodeLoggingEPNS0_7IsolateE@PLT
	movq	41488(%r12), %r13
	leaq	56(%r13), %r15
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	movq	8(%r13), %rdi
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L6185
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L6187
	.p2align 4,,10
	.p2align 3
.L6201:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L6185
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L6185
.L6187:
	cmpq	%rbx, %rsi
	jne	.L6201
.L6186:
	movq	%r15, %rdi
	call	_ZN2v84base5Mutex6UnlockEv@PLT
	movq	88(%r12), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6200:
	.cfi_restore_state
	leaq	_ZGVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L6183
	leaq	16+_ZTVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE12NoopListener(%rip), %rax
	leaq	_ZGVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener(%rip), %rdi
	movq	%rax, _ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener(%rip)
	call	__cxa_guard_release@PLT
	jmp	.L6183
	.p2align 4,,10
	.p2align 3
.L6185:
	movl	$16, %edi
	call	_Znwm@PLT
	movl	$1, %r8d
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rbx, 8(%rax)
	movq	%rax, %rcx
	leaq	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener(%rip), %rdx
	movq	$0, (%rax)
	call	_ZNSt10_HashtableIPN2v88internal17CodeEventListenerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	jmp	.L6186
	.p2align 4,,10
	.p2align 3
.L6199:
	addq	$8, %rsp
	movq	%rdx, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL41Stats_Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE22715:
	.size	_ZN2v88internal35Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateE, .-_ZN2v88internal35Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal33Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal33Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal33Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE:
.LFB29077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29077:
	.size	_GLOBAL__sub_I__ZN2v88internal33Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal33Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal33Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateE
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.data.rel.ro.local._ZTVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE12NoopListener,"aw"
	.align 8
	.type	_ZTVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE12NoopListener, @object
	.size	_ZTVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE12NoopListener, 160
_ZTVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE12NoopListener:
	.quad	0
	.quad	0
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD1Ev
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListenerD0Ev
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CallbackEventENS0_4NameEm
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener19GetterCallbackEventENS0_4NameEm
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener19SetterCallbackEventENS0_4NameEm
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener13CodeMoveEventENS0_12AbstractCodeES5_
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener27SharedFunctionInfoMoveEventEmm
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener22NativeContextMoveEventEmm
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener17CodeMovingGCEventEv
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.quad	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEEN12NoopListener27is_listening_to_code_eventsEv
	.section	.bss._ZGVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener, @object
	.size	_ZGVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener, 8
_ZGVZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener:
	.zero	8
	.section	.bss._ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener, @object
	.size	_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener, 8
_ZZN2v88internalL45__RT_impl_Runtime_EnableCodeLoggingForTestingENS0_9ArgumentsEPNS0_7IsolateEE13noop_listener:
	.zero	8
	.section	.bss._ZZN2v88internalL41Stats_Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1324,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Stats_Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1324, @object
	.size	_ZZN2v88internalL41Stats_Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1324, 8
_ZZN2v88internalL41Stats_Runtime_EnableCodeLoggingForTestingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1324:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateEE29trace_event_unique_atomic1317,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateEE29trace_event_unique_atomic1317, @object
	.size	_ZZN2v88internalL34Stats_Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateEE29trace_event_unique_atomic1317, 8
_ZZN2v88internalL34Stats_Runtime_TurbofanStaticAssertEiPmPNS0_7IsolateEE29trace_event_unique_atomic1317:
	.zero	8
	.section	.bss._ZZN2v88internalL39Stats_Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateEE29trace_event_unique_atomic1308,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateEE29trace_event_unique_atomic1308, @object
	.size	_ZZN2v88internalL39Stats_Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateEE29trace_event_unique_atomic1308, 8
_ZZN2v88internalL39Stats_Runtime_FreezeWasmLazyCompilationEiPmPNS0_7IsolateEE29trace_event_unique_atomic1308:
	.zero	8
	.section	.bss._ZZN2v88internalL43Stats_Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1298,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL43Stats_Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1298, @object
	.size	_ZZN2v88internalL43Stats_Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1298, 8
_ZZN2v88internalL43Stats_Runtime_CompleteInobjectSlackTrackingEiPmPNS0_7IsolateEE29trace_event_unique_atomic1298:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1283,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1283, @object
	.size	_ZZN2v88internalL31Stats_Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1283, 8
_ZZN2v88internalL31Stats_Runtime_IsLiftoffFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1283:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1271,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1271, @object
	.size	_ZZN2v88internalL32Stats_Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1271, 8
_ZZN2v88internalL32Stats_Runtime_WasmTierUpFunctionEiPmPNS0_7IsolateEE29trace_event_unique_atomic1271:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateEE29trace_event_unique_atomic1241,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateEE29trace_event_unique_atomic1241, @object
	.size	_ZZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateEE29trace_event_unique_atomic1241, 8
_ZZN2v88internalL29Stats_Runtime_WasmTraceMemoryEiPmPNS0_7IsolateEE29trace_event_unique_atomic1241:
	.zero	8
	.section	.bss._ZZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1229,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1229, @object
	.size	_ZZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1229, 8
_ZZN2v88internalL39Stats_Runtime_RedirectToWasmInterpreterEiPmPNS0_7IsolateEE29trace_event_unique_atomic1229:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1220,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1220, @object
	.size	_ZZN2v88internalL37Stats_Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1220, 8
_ZZN2v88internalL37Stats_Runtime_WasmNumInterpretedCallsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1220:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1208,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1208, @object
	.size	_ZZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1208, 8
_ZZN2v88internalL38Stats_Runtime_WasmGetNumberOfInstancesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1208:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_HeapObjectVerifyEiPmPNS0_7IsolateEE29trace_event_unique_atomic1191,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_HeapObjectVerifyEiPmPNS0_7IsolateEE29trace_event_unique_atomic1191, @object
	.size	_ZZN2v88internalL30Stats_Runtime_HeapObjectVerifyEiPmPNS0_7IsolateEE29trace_event_unique_atomic1191, 8
_ZZN2v88internalL30Stats_Runtime_HeapObjectVerifyEiPmPNS0_7IsolateEE29trace_event_unique_atomic1191:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_CloneWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1179,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_CloneWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1179, @object
	.size	_ZZN2v88internalL29Stats_Runtime_CloneWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1179, 8
_ZZN2v88internalL29Stats_Runtime_CloneWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1179:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1150,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1150, @object
	.size	_ZZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1150, 8
_ZZN2v88internalL35Stats_Runtime_DeserializeWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1150:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_SerializeWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1128,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_SerializeWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1128, @object
	.size	_ZZN2v88internalL33Stats_Runtime_SerializeWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1128, 8
_ZZN2v88internalL33Stats_Runtime_SerializeWasmModuleEiPmPNS0_7IsolateEE29trace_event_unique_atomic1128:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_StringIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1119,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_StringIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1119, @object
	.size	_ZZN2v88internalL37Stats_Runtime_StringIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1119, 8
_ZZN2v88internalL37Stats_Runtime_StringIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1119:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_SetIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1113,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_SetIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1113, @object
	.size	_ZZN2v88internalL34Stats_Runtime_SetIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1113, 8
_ZZN2v88internalL34Stats_Runtime_SetIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1113:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_MapIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1107,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_MapIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1107, @object
	.size	_ZZN2v88internalL34Stats_Runtime_MapIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1107, 8
_ZZN2v88internalL34Stats_Runtime_MapIteratorProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1107:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1100,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1100, @object
	.size	_ZZN2v88internalL35Stats_Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1100, 8
_ZZN2v88internalL35Stats_Runtime_ArraySpeciesProtectorEiPmPNS0_7IsolateEE29trace_event_unique_atomic1100:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, @object
	.size	_ZZN2v88internalL38Stats_Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, 8
_ZZN2v88internalL38Stats_Runtime_HasFixedBigInt64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096:
	.zero	8
	.section	.bss._ZZN2v88internalL39Stats_Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, @object
	.size	_ZZN2v88internalL39Stats_Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, 8
_ZZN2v88internalL39Stats_Runtime_HasFixedBigUint64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096:
	.zero	8
	.section	.bss._ZZN2v88internalL42Stats_Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Stats_Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, @object
	.size	_ZZN2v88internalL42Stats_Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, 8
_ZZN2v88internalL42Stats_Runtime_HasFixedUint8ClampedElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, @object
	.size	_ZZN2v88internalL37Stats_Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, 8
_ZZN2v88internalL37Stats_Runtime_HasFixedFloat64ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, @object
	.size	_ZZN2v88internalL37Stats_Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, 8
_ZZN2v88internalL37Stats_Runtime_HasFixedFloat32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, @object
	.size	_ZZN2v88internalL35Stats_Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, 8
_ZZN2v88internalL35Stats_Runtime_HasFixedInt32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, @object
	.size	_ZZN2v88internalL36Stats_Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, 8
_ZZN2v88internalL36Stats_Runtime_HasFixedUint32ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, @object
	.size	_ZZN2v88internalL35Stats_Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, 8
_ZZN2v88internalL35Stats_Runtime_HasFixedInt16ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, @object
	.size	_ZZN2v88internalL36Stats_Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, 8
_ZZN2v88internalL36Stats_Runtime_HasFixedUint16ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, @object
	.size	_ZZN2v88internalL34Stats_Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, 8
_ZZN2v88internalL34Stats_Runtime_HasFixedInt8ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, @object
	.size	_ZZN2v88internalL35Stats_Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096, 8
_ZZN2v88internalL35Stats_Runtime_HasFixedUint8ElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1096:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_HasFastPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1086,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_HasFastPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1086, @object
	.size	_ZZN2v88internalL31Stats_Runtime_HasFastPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1086, 8
_ZZN2v88internalL31Stats_Runtime_HasFastPropertiesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1086:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1084,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1084, @object
	.size	_ZZN2v88internalL40Stats_Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1084, 8
_ZZN2v88internalL40Stats_Runtime_HasSloppyArgumentsElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1084:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_HasPackedElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1083,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_HasPackedElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1083, @object
	.size	_ZZN2v88internalL31Stats_Runtime_HasPackedElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1083, 8
_ZZN2v88internalL31Stats_Runtime_HasPackedElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1083:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_HasDictionaryElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1082,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_HasDictionaryElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1082, @object
	.size	_ZZN2v88internalL35Stats_Runtime_HasDictionaryElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1082, 8
_ZZN2v88internalL35Stats_Runtime_HasDictionaryElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1082:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_HasHoleyElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1081,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_HasHoleyElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1081, @object
	.size	_ZZN2v88internalL30Stats_Runtime_HasHoleyElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1081, 8
_ZZN2v88internalL30Stats_Runtime_HasHoleyElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1081:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_HasDoubleElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1080,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_HasDoubleElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1080, @object
	.size	_ZZN2v88internalL31Stats_Runtime_HasDoubleElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1080, 8
_ZZN2v88internalL31Stats_Runtime_HasDoubleElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1080:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1079,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1079, @object
	.size	_ZZN2v88internalL36Stats_Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1079, 8
_ZZN2v88internalL36Stats_Runtime_HasSmiOrObjectElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1079:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_HasObjectElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1078,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_HasObjectElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1078, @object
	.size	_ZZN2v88internalL31Stats_Runtime_HasObjectElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1078, 8
_ZZN2v88internalL31Stats_Runtime_HasObjectElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1078:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_HasSmiElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1077,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_HasSmiElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1077, @object
	.size	_ZZN2v88internalL28Stats_Runtime_HasSmiElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1077, 8
_ZZN2v88internalL28Stats_Runtime_HasSmiElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1077:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_HasFastElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1076,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_HasFastElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1076, @object
	.size	_ZZN2v88internalL29Stats_Runtime_HasFastElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1076, 8
_ZZN2v88internalL29Stats_Runtime_HasFastElementsEiPmPNS0_7IsolateEE29trace_event_unique_atomic1076:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1061,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1061, @object
	.size	_ZZN2v88internalL33Stats_Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1061, 8
_ZZN2v88internalL33Stats_Runtime_RegexpHasNativeCodeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1061:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1052,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1052, @object
	.size	_ZZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1052, 8
_ZZN2v88internalL31Stats_Runtime_RegexpHasBytecodeEiPmPNS0_7IsolateEE29trace_event_unique_atomic1052:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateEE29trace_event_unique_atomic1043,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateEE29trace_event_unique_atomic1043, @object
	.size	_ZZN2v88internalL35Stats_Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateEE29trace_event_unique_atomic1043, 8
_ZZN2v88internalL35Stats_Runtime_SetWasmThreadsEnabledEiPmPNS0_7IsolateEE29trace_event_unique_atomic1043:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1024,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1024, @object
	.size	_ZZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1024, 8
_ZZN2v88internalL36Stats_Runtime_GetWasmExceptionValuesEiPmPNS0_7IsolateEE29trace_event_unique_atomic1024:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateEE29trace_event_unique_atomic1008,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateEE29trace_event_unique_atomic1008, @object
	.size	_ZZN2v88internalL32Stats_Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateEE29trace_event_unique_atomic1008, 8
_ZZN2v88internalL32Stats_Runtime_GetWasmExceptionIdEiPmPNS0_7IsolateEE29trace_event_unique_atomic1008:
	.zero	8
	.section	.bss._ZZN2v88internalL39Stats_Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateEE29trace_event_unique_atomic1001,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateEE29trace_event_unique_atomic1001, @object
	.size	_ZZN2v88internalL39Stats_Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateEE29trace_event_unique_atomic1001, 8
_ZZN2v88internalL39Stats_Runtime_GetWasmRecoveredTrapCountEiPmPNS0_7IsolateEE29trace_event_unique_atomic1001:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_IsThreadInWasmEiPmPNS0_7IsolateEE28trace_event_unique_atomic995,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_IsThreadInWasmEiPmPNS0_7IsolateEE28trace_event_unique_atomic995, @object
	.size	_ZZN2v88internalL28Stats_Runtime_IsThreadInWasmEiPmPNS0_7IsolateEE28trace_event_unique_atomic995, 8
_ZZN2v88internalL28Stats_Runtime_IsThreadInWasmEiPmPNS0_7IsolateEE28trace_event_unique_atomic995:
	.zero	8
	.section	.bss._ZZN2v88internalL38Stats_Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic989,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL38Stats_Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic989, @object
	.size	_ZZN2v88internalL38Stats_Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic989, 8
_ZZN2v88internalL38Stats_Runtime_IsWasmTrapHandlerEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic989:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_IsWasmCodeEiPmPNS0_7IsolateEE28trace_event_unique_atomic981,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_IsWasmCodeEiPmPNS0_7IsolateEE28trace_event_unique_atomic981, @object
	.size	_ZZN2v88internalL24Stats_Runtime_IsWasmCodeEiPmPNS0_7IsolateEE28trace_event_unique_atomic981, 8
_ZZN2v88internalL24Stats_Runtime_IsWasmCodeEiPmPNS0_7IsolateEE28trace_event_unique_atomic981:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateEE28trace_event_unique_atomic971,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateEE28trace_event_unique_atomic971, @object
	.size	_ZZN2v88internalL33Stats_Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateEE28trace_event_unique_atomic971, 8
_ZZN2v88internalL33Stats_Runtime_DisallowWasmCodegenEiPmPNS0_7IsolateEE28trace_event_unique_atomic971:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateEE28trace_event_unique_atomic961,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateEE28trace_event_unique_atomic961, @object
	.size	_ZZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateEE28trace_event_unique_atomic961, 8
_ZZN2v88internalL40Stats_Runtime_DisallowCodegenFromStringsEiPmPNS0_7IsolateEE28trace_event_unique_atomic961:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateEE28trace_event_unique_atomic938,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateEE28trace_event_unique_atomic938, @object
	.size	_ZZN2v88internalL27Stats_Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateEE28trace_event_unique_atomic938, 8
_ZZN2v88internalL27Stats_Runtime_IsAsmWasmCodeEiPmPNS0_7IsolateEE28trace_event_unique_atomic938:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_InYoungGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic931,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_InYoungGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic931, @object
	.size	_ZZN2v88internalL31Stats_Runtime_InYoungGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic931, 8
_ZZN2v88internalL31Stats_Runtime_InYoungGenerationEiPmPNS0_7IsolateEE28trace_event_unique_atomic931:
	.zero	8
	.section	.bss._ZZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateEE28trace_event_unique_atomic921,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateEE28trace_event_unique_atomic921, @object
	.size	_ZZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateEE28trace_event_unique_atomic921, 8
_ZZN2v88internalL44Stats_Runtime_HasElementsInALargeObjectSpaceEiPmPNS0_7IsolateEE28trace_event_unique_atomic921:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic913,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic913, @object
	.size	_ZZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic913, 8
_ZZN2v88internalL25Stats_Runtime_HaveSameMapEiPmPNS0_7IsolateEE28trace_event_unique_atomic913:
	.zero	8
	.section	.bss._ZZN2v88internalL23Stats_Runtime_TraceExitEiPmPNS0_7IsolateEE28trace_event_unique_atomic902,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL23Stats_Runtime_TraceExitEiPmPNS0_7IsolateEE28trace_event_unique_atomic902, @object
	.size	_ZZN2v88internalL23Stats_Runtime_TraceExitEiPmPNS0_7IsolateEE28trace_event_unique_atomic902, 8
_ZZN2v88internalL23Stats_Runtime_TraceExitEiPmPNS0_7IsolateEE28trace_event_unique_atomic902:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_TraceEnterEiPmPNS0_7IsolateEE28trace_event_unique_atomic892,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_TraceEnterEiPmPNS0_7IsolateEE28trace_event_unique_atomic892, @object
	.size	_ZZN2v88internalL24Stats_Runtime_TraceEnterEiPmPNS0_7IsolateEE28trace_event_unique_atomic892, 8
_ZZN2v88internalL24Stats_Runtime_TraceEnterEiPmPNS0_7IsolateEE28trace_event_unique_atomic892:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_DisassembleFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic854,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_DisassembleFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic854, @object
	.size	_ZZN2v88internalL33Stats_Runtime_DisassembleFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic854, 8
_ZZN2v88internalL33Stats_Runtime_DisassembleFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic854:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic843,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic843, @object
	.size	_ZZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic843, 8
_ZZN2v88internalL28Stats_Runtime_AbortCSAAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic843:
	.zero	8
	.section	.bss._ZZN2v88internalL21Stats_Runtime_AbortJSEiPmPNS0_7IsolateEE28trace_event_unique_atomic829,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL21Stats_Runtime_AbortJSEiPmPNS0_7IsolateEE28trace_event_unique_atomic829, @object
	.size	_ZZN2v88internalL21Stats_Runtime_AbortJSEiPmPNS0_7IsolateEE28trace_event_unique_atomic829, 8
_ZZN2v88internalL21Stats_Runtime_AbortJSEiPmPNS0_7IsolateEE28trace_event_unique_atomic829:
	.zero	8
	.section	.bss._ZZN2v88internalL19Stats_Runtime_AbortEiPmPNS0_7IsolateEE28trace_event_unique_atomic818,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL19Stats_Runtime_AbortEiPmPNS0_7IsolateEE28trace_event_unique_atomic818, @object
	.size	_ZZN2v88internalL19Stats_Runtime_AbortEiPmPNS0_7IsolateEE28trace_event_unique_atomic818, 8
_ZZN2v88internalL19Stats_Runtime_AbortEiPmPNS0_7IsolateEE28trace_event_unique_atomic818:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_SetForceSlowPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic805,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_SetForceSlowPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic805, @object
	.size	_ZZN2v88internalL30Stats_Runtime_SetForceSlowPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic805, 8
_ZZN2v88internalL30Stats_Runtime_SetForceSlowPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic805:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_SystemBreakEiPmPNS0_7IsolateEE28trace_event_unique_atomic795,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_SystemBreakEiPmPNS0_7IsolateEE28trace_event_unique_atomic795, @object
	.size	_ZZN2v88internalL25Stats_Runtime_SystemBreakEiPmPNS0_7IsolateEE28trace_event_unique_atomic795, 8
_ZZN2v88internalL25Stats_Runtime_SystemBreakEiPmPNS0_7IsolateEE28trace_event_unique_atomic795:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateEE28trace_event_unique_atomic781,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateEE28trace_event_unique_atomic781, @object
	.size	_ZZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateEE28trace_event_unique_atomic781, 8
_ZZN2v88internalL25Stats_Runtime_GlobalPrintEiPmPNS0_7IsolateEE28trace_event_unique_atomic781:
	.zero	8
	.section	.bss._ZZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic754,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic754, @object
	.size	_ZZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic754, 8
_ZZN2v88internalL37Stats_Runtime_DebugTrackRetainingPathEiPmPNS0_7IsolateEE28trace_event_unique_atomic754:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_DebugTraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic747,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_DebugTraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic747, @object
	.size	_ZZN2v88internalL24Stats_Runtime_DebugTraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic747, 8
_ZZN2v88internalL24Stats_Runtime_DebugTraceEiPmPNS0_7IsolateEE28trace_event_unique_atomic747:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic728,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic728, @object
	.size	_ZZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic728, 8
_ZZN2v88internalL36Stats_Runtime_PrintWithNameForAssertEiPmPNS0_7IsolateEE28trace_event_unique_atomic728:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_DebugPrintEiPmPNS0_7IsolateEE28trace_event_unique_atomic680,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_DebugPrintEiPmPNS0_7IsolateEE28trace_event_unique_atomic680, @object
	.size	_ZZN2v88internalL24Stats_Runtime_DebugPrintEiPmPNS0_7IsolateEE28trace_event_unique_atomic680, 8
_ZZN2v88internalL24Stats_Runtime_DebugPrintEiPmPNS0_7IsolateEE28trace_event_unique_atomic680:
	.zero	8
	.section	.bss._ZZN2v88internalL34Stats_Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateEE28trace_event_unique_atomic656,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL34Stats_Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateEE28trace_event_unique_atomic656, @object
	.size	_ZZN2v88internalL34Stats_Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateEE28trace_event_unique_atomic656, 8
_ZZN2v88internalL34Stats_Runtime_SetAllocationTimeoutEiPmPNS0_7IsolateEE28trace_event_unique_atomic656:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_NotifyContextDisposedEiPmPNS0_7IsolateEE28trace_event_unique_atomic648,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_NotifyContextDisposedEiPmPNS0_7IsolateEE28trace_event_unique_atomic648, @object
	.size	_ZZN2v88internalL35Stats_Runtime_NotifyContextDisposedEiPmPNS0_7IsolateEE28trace_event_unique_atomic648, 8
_ZZN2v88internalL35Stats_Runtime_NotifyContextDisposedEiPmPNS0_7IsolateEE28trace_event_unique_atomic648:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateEE28trace_event_unique_atomic640,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateEE28trace_event_unique_atomic640, @object
	.size	_ZZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateEE28trace_event_unique_atomic640, 8
_ZZN2v88internalL40Stats_Runtime_SetWasmInstantiateControlsEiPmPNS0_7IsolateEE28trace_event_unique_atomic640:
	.zero	8
	.section	.bss._ZZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateEE28trace_event_unique_atomic626,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateEE28trace_event_unique_atomic626, @object
	.size	_ZZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateEE28trace_event_unique_atomic626, 8
_ZZN2v88internalL36Stats_Runtime_SetWasmCompileControlsEiPmPNS0_7IsolateEE28trace_event_unique_atomic626:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateEE28trace_event_unique_atomic615,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateEE28trace_event_unique_atomic615, @object
	.size	_ZZN2v88internalL35Stats_Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateEE28trace_event_unique_atomic615, 8
_ZZN2v88internalL35Stats_Runtime_ClearFunctionFeedbackEiPmPNS0_7IsolateEE28trace_event_unique_atomic615:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_GetCallableEiPmPNS0_7IsolateEE28trace_event_unique_atomic599,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_GetCallableEiPmPNS0_7IsolateEE28trace_event_unique_atomic599, @object
	.size	_ZZN2v88internalL25Stats_Runtime_GetCallableEiPmPNS0_7IsolateEE28trace_event_unique_atomic599, 8
_ZZN2v88internalL25Stats_Runtime_GetCallableEiPmPNS0_7IsolateEE28trace_event_unique_atomic599:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_GetUndetectableEiPmPNS0_7IsolateEE28trace_event_unique_atomic574,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_GetUndetectableEiPmPNS0_7IsolateEE28trace_event_unique_atomic574, @object
	.size	_ZZN2v88internalL29Stats_Runtime_GetUndetectableEiPmPNS0_7IsolateEE28trace_event_unique_atomic574, 8
_ZZN2v88internalL29Stats_Runtime_GetUndetectableEiPmPNS0_7IsolateEE28trace_event_unique_atomic574:
	.zero	8
	.section	.bss._ZZN2v88internalL44Stats_Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateEE28trace_event_unique_atomic561,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Stats_Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateEE28trace_event_unique_atomic561, @object
	.size	_ZZN2v88internalL44Stats_Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateEE28trace_event_unique_atomic561, 8
_ZZN2v88internalL44Stats_Runtime_UnblockConcurrentRecompilationEiPmPNS0_7IsolateEE28trace_event_unique_atomic561:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateEE28trace_event_unique_atomic469,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateEE28trace_event_unique_atomic469, @object
	.size	_ZZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateEE28trace_event_unique_atomic469, 8
_ZZN2v88internalL35Stats_Runtime_GetOptimizationStatusEiPmPNS0_7IsolateEE28trace_event_unique_atomic469:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic455,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic455, @object
	.size	_ZZN2v88internalL35Stats_Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic455, 8
_ZZN2v88internalL35Stats_Runtime_NeverOptimizeFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic455:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateEE28trace_event_unique_atomic399,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateEE28trace_event_unique_atomic399, @object
	.size	_ZZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateEE28trace_event_unique_atomic399, 8
_ZZN2v88internalL25Stats_Runtime_OptimizeOsrEiPmPNS0_7IsolateEE28trace_event_unique_atomic399:
	.zero	8
	.section	.bss._ZZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateEE28trace_event_unique_atomic352,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateEE28trace_event_unique_atomic352, @object
	.size	_ZZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateEE28trace_event_unique_atomic352, 8
_ZZN2v88internalL44Stats_Runtime_PrepareFunctionForOptimizationEiPmPNS0_7IsolateEE28trace_event_unique_atomic352:
	.zero	8
	.section	.bss._ZZN2v88internalL45Stats_Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic341,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Stats_Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic341, @object
	.size	_ZZN2v88internalL45Stats_Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic341, 8
_ZZN2v88internalL45Stats_Runtime_EnsureFeedbackVectorForFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic341:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic224,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic224, @object
	.size	_ZZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic224, 8
_ZZN2v88internalL40Stats_Runtime_OptimizeFunctionOnNextCallEiPmPNS0_7IsolateEE28trace_event_unique_atomic224:
	.zero	8
	.section	.bss._ZZN2v88internalL48Stats_Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateEE28trace_event_unique_atomic217,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL48Stats_Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateEE28trace_event_unique_atomic217, @object
	.size	_ZZN2v88internalL48Stats_Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateEE28trace_event_unique_atomic217, 8
_ZZN2v88internalL48Stats_Runtime_IsConcurrentRecompilationSupportedEiPmPNS0_7IsolateEE28trace_event_unique_atomic217:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_ICsAreEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic211,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_ICsAreEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic211, @object
	.size	_ZZN2v88internalL27Stats_Runtime_ICsAreEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic211, 8
_ZZN2v88internalL27Stats_Runtime_ICsAreEnabledEiPmPNS0_7IsolateEE28trace_event_unique_atomic211:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_RunningInSimulatorEiPmPNS0_7IsolateEE28trace_event_unique_atomic201,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_RunningInSimulatorEiPmPNS0_7IsolateEE28trace_event_unique_atomic201, @object
	.size	_ZZN2v88internalL32Stats_Runtime_RunningInSimulatorEiPmPNS0_7IsolateEE28trace_event_unique_atomic201, 8
_ZZN2v88internalL32Stats_Runtime_RunningInSimulatorEiPmPNS0_7IsolateEE28trace_event_unique_atomic201:
	.zero	8
	.section	.bss._ZZN2v88internalL27Stats_Runtime_DeoptimizeNowEiPmPNS0_7IsolateEE28trace_event_unique_atomic182,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_DeoptimizeNowEiPmPNS0_7IsolateEE28trace_event_unique_atomic182, @object
	.size	_ZZN2v88internalL27Stats_Runtime_DeoptimizeNowEiPmPNS0_7IsolateEE28trace_event_unique_atomic182, 8
_ZZN2v88internalL27Stats_Runtime_DeoptimizeNowEiPmPNS0_7IsolateEE28trace_event_unique_atomic182:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic162,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic162, @object
	.size	_ZZN2v88internalL32Stats_Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic162, 8
_ZZN2v88internalL32Stats_Runtime_DeoptimizeFunctionEiPmPNS0_7IsolateEE28trace_event_unique_atomic162:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_ConstructSlicedStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic147,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_ConstructSlicedStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic147, @object
	.size	_ZZN2v88internalL35Stats_Runtime_ConstructSlicedStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic147, 8
_ZZN2v88internalL35Stats_Runtime_ConstructSlicedStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic147:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_ConstructConsStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic133,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_ConstructConsStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic133, @object
	.size	_ZZN2v88internalL33Stats_Runtime_ConstructConsStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic133, 8
_ZZN2v88internalL33Stats_Runtime_ConstructConsStringEiPmPNS0_7IsolateEE28trace_event_unique_atomic133:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateEE28trace_event_unique_atomic124,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateEE28trace_event_unique_atomic124, @object
	.size	_ZZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateEE28trace_event_unique_atomic124, 8
_ZZN2v88internalL29Stats_Runtime_ConstructDoubleEiPmPNS0_7IsolateEE28trace_event_unique_atomic124:
	.zero	8
	.section	.bss._ZZN2v88internalL39Stats_Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateEE28trace_event_unique_atomic116,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateEE28trace_event_unique_atomic116, @object
	.size	_ZZN2v88internalL39Stats_Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateEE28trace_event_unique_atomic116, 8
_ZZN2v88internalL39Stats_Runtime_ClearMegamorphicStubCacheEiPmPNS0_7IsolateEE28trace_event_unique_atomic116:
	.zero	8
	.section	.bss._ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE,"aw",@nobits
	.align 32
	.type	_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE, @object
	.size	_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE, 48
_ZN2v88internal12_GLOBAL__N_129g_PerIsolateWasmControlsMutexE:
	.zero	48
	.section	.bss._ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object, @object
	.size	_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object, 8
_ZGVZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object,"aw",@nobits
	.align 32
	.type	_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object, @object
	.size	_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object, 48
_ZZN2v88internal12_GLOBAL__N_125GetPerIsolateWasmControlsEvE6object:
	.zero	48
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC7:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC8:
	.long	4294967295
	.long	2146435071
	.align 8
.LC9:
	.long	4290772992
	.long	1105199103
	.align 8
.LC10:
	.long	0
	.long	-1042284544
	.section	.rodata.cst16
	.align 16
.LC127:
	.quad	8098929444147786356
	.quad	3273676516263552360
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
