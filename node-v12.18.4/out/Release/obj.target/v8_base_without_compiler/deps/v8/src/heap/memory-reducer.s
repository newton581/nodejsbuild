	.file	"memory-reducer.cc"
	.text
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14CancelableTask3RunEv
	.type	_ZN2v88internal14CancelableTask3RunEv, @function
_ZN2v88internal14CancelableTask3RunEv:
.LFB3729:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, 16(%rdi)
	jne	.L1
	movq	(%rdi), %rax
	jmp	*24(%rax)
.L1:
	ret
	.cfi_endproc
.LFE3729:
	.size	_ZN2v88internal14CancelableTask3RunEv, .-_ZN2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal13MemoryReducer9TimerTaskD2Ev,"axG",@progbits,_ZN2v88internal13MemoryReducer9TimerTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13MemoryReducer9TimerTaskD2Ev
	.type	_ZN2v88internal13MemoryReducer9TimerTaskD2Ev, @function
_ZN2v88internal13MemoryReducer9TimerTaskD2Ev:
.LFB23413:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE23413:
	.size	_ZN2v88internal13MemoryReducer9TimerTaskD2Ev, .-_ZN2v88internal13MemoryReducer9TimerTaskD2Ev
	.weak	_ZN2v88internal13MemoryReducer9TimerTaskD1Ev
	.set	_ZN2v88internal13MemoryReducer9TimerTaskD1Ev,_ZN2v88internal13MemoryReducer9TimerTaskD2Ev
	.section	.text._ZN2v88internal13MemoryReducer9TimerTaskD0Ev,"axG",@progbits,_ZN2v88internal13MemoryReducer9TimerTaskD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13MemoryReducer9TimerTaskD0Ev
	.type	_ZN2v88internal13MemoryReducer9TimerTaskD0Ev, @function
_ZN2v88internal13MemoryReducer9TimerTaskD0Ev:
.LFB23415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23415:
	.size	_ZN2v88internal13MemoryReducer9TimerTaskD0Ev, .-_ZN2v88internal13MemoryReducer9TimerTaskD0Ev
	.section	.text._ZN2v88internal13MemoryReducer13ScheduleTimerEd.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal13MemoryReducer13ScheduleTimerEd.part.0, @function
_ZN2v88internal13MemoryReducer13ScheduleTimerEd.part.0:
.LFB25118:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	8(%rdi), %r13
	movsd	%xmm0, -56(%rbp)
	movl	$48, %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	0(%r13), %rax
	movq	16(%rax), %r14
	call	_Znwm@PLT
	movq	%rax, %rbx
	movq	(%r12), %rax
	movq	%rbx, %rdi
	addq	$32, %rbx
	leaq	-37592(%rax), %rsi
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	movq	%r12, 8(%rbx)
	movq	%r13, %rdi
	leaq	-48(%rbp), %rsi
	leaq	16+_ZTVN2v88internal13MemoryReducer9TimerTaskE(%rip), %rax
	movsd	-56(%rbp), %xmm0
	addsd	.LC0(%rip), %xmm0
	divsd	.LC1(%rip), %xmm0
	movq	%rax, -32(%rbx)
	addq	$48, %rax
	movq	%rax, (%rbx)
	movq	%rbx, -48(%rbp)
	call	*%r14
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L7
	movq	(%rdi), %rax
	call	*8(%rax)
.L7:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L14
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE25118:
	.size	_ZN2v88internal13MemoryReducer13ScheduleTimerEd.part.0, .-_ZN2v88internal13MemoryReducer13ScheduleTimerEd.part.0
	.section	.text._ZN2v88internal13MemoryReducer9TimerTaskD2Ev,"axG",@progbits,_ZN2v88internal13MemoryReducer9TimerTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal13MemoryReducer9TimerTaskD1Ev
	.type	_ZThn32_N2v88internal13MemoryReducer9TimerTaskD1Ev, @function
_ZThn32_N2v88internal13MemoryReducer9TimerTaskD1Ev:
.LFB25120:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	subq	$32, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN2v88internal10CancelableD2Ev@PLT
	.cfi_endproc
.LFE25120:
	.size	_ZThn32_N2v88internal13MemoryReducer9TimerTaskD1Ev, .-_ZThn32_N2v88internal13MemoryReducer9TimerTaskD1Ev
	.section	.text._ZN2v88internal13MemoryReducer9TimerTaskD0Ev,"axG",@progbits,_ZN2v88internal13MemoryReducer9TimerTaskD5Ev,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal13MemoryReducer9TimerTaskD0Ev
	.type	_ZThn32_N2v88internal13MemoryReducer9TimerTaskD0Ev, @function
_ZThn32_N2v88internal13MemoryReducer9TimerTaskD0Ev:
.LFB25121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal14CancelableTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -32(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88internal10CancelableD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE25121:
	.size	_ZThn32_N2v88internal13MemoryReducer9TimerTaskD0Ev, .-_ZThn32_N2v88internal13MemoryReducer9TimerTaskD0Ev
	.section	.text._ZN2v88internal14CancelableTask3RunEv,"axG",@progbits,_ZN2v88internal14CancelableTask3RunEv,comdat
	.p2align 4
	.weak	_ZThn32_N2v88internal14CancelableTask3RunEv
	.type	_ZThn32_N2v88internal14CancelableTask3RunEv, @function
_ZThn32_N2v88internal14CancelableTask3RunEv:
.LFB25122:
	.cfi_startproc
	endbr64
	leaq	-32(%rdi), %r8
	xorl	%eax, %eax
	movl	$2, %edx
	lock cmpxchgl	%edx, -16(%rdi)
	jne	.L18
	movq	-32(%rdi), %rax
	movq	%r8, %rdi
	movq	24(%rax), %rax
	jmp	*%rax
.L18:
	ret
	.cfi_endproc
.LFE25122:
	.size	_ZThn32_N2v88internal14CancelableTask3RunEv, .-_ZThn32_N2v88internal14CancelableTask3RunEv
	.section	.text._ZN2v88internal13MemoryReducerC2EPNS0_4HeapE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MemoryReducerC2EPNS0_4HeapE
	.type	_ZN2v88internal13MemoryReducerC2EPNS0_4HeapE, @function
_ZN2v88internal13MemoryReducerC2EPNS0_4HeapE:
.LFB20059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rsi, (%rdi)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	leaq	8(%rbx), %rdi
	leaq	-37592(%r12), %rdx
	movq	%rax, %rsi
	movq	(%rax), %rax
	call	*48(%rax)
	movq	$0, 24(%rbx)
	pxor	%xmm0, %xmm0
	movq	$0, 48(%rbx)
	movl	$0, 56(%rbx)
	movq	$0x000000000, 64(%rbx)
	movups	%xmm0, 32(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L23
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L23:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20059:
	.size	_ZN2v88internal13MemoryReducerC2EPNS0_4HeapE, .-_ZN2v88internal13MemoryReducerC2EPNS0_4HeapE
	.globl	_ZN2v88internal13MemoryReducerC1EPNS0_4HeapE
	.set	_ZN2v88internal13MemoryReducerC1EPNS0_4HeapE,_ZN2v88internal13MemoryReducerC2EPNS0_4HeapE
	.section	.text._ZN2v88internal13MemoryReducer9TimerTaskC2EPS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MemoryReducer9TimerTaskC2EPS1_
	.type	_ZN2v88internal13MemoryReducer9TimerTaskC2EPS1_, @function
_ZN2v88internal13MemoryReducer9TimerTaskC2EPS1_:
.LFB20070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rsi), %rax
	movq	%rdi, %rbx
	leaq	-37592(%rax), %rsi
	call	_ZN2v88internal14CancelableTaskC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal13MemoryReducer9TimerTaskE(%rip), %rax
	movq	%r12, 40(%rbx)
	movq	%rax, (%rbx)
	addq	$48, %rax
	movq	%rax, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20070:
	.size	_ZN2v88internal13MemoryReducer9TimerTaskC2EPS1_, .-_ZN2v88internal13MemoryReducer9TimerTaskC2EPS1_
	.globl	_ZN2v88internal13MemoryReducer9TimerTaskC1EPS1_
	.set	_ZN2v88internal13MemoryReducer9TimerTaskC1EPS1_,_ZN2v88internal13MemoryReducer9TimerTaskC2EPS1_
	.section	.rodata._ZN2v88internal13MemoryReducer11NotifyTimerERKNS1_5EventE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"unreachable code"
	.section	.rodata._ZN2v88internal13MemoryReducer11NotifyTimerERKNS1_5EventE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"Memory reducer: started GC #%d\n"
	.align 8
.LC10:
	.string	"Memory reducer: waiting for %.f ms\n"
	.section	.text._ZN2v88internal13MemoryReducer11NotifyTimerERKNS1_5EventE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MemoryReducer11NotifyTimerERKNS1_5EventE
	.type	_ZN2v88internal13MemoryReducer11NotifyTimerERKNS1_5EventE, @function
_ZN2v88internal13MemoryReducer11NotifyTimerERKNS1_5EventE:
.LFB20073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, _ZN2v88internal24FLAG_incremental_markingE(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	je	.L27
	cmpb	$0, _ZN2v88internal19FLAG_memory_reducerE(%rip)
	jne	.L28
.L27:
	movsd	40(%rbx), %xmm1
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
.L29:
	movl	$0, 24(%rbx)
	unpcklpd	%xmm1, %xmm0
	movl	%edx, 28(%rbx)
	movq	%rax, 48(%rbx)
	movups	%xmm0, 32(%rbx)
.L26:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movl	24(%rdi), %eax
	movq	%rsi, %r12
	cmpl	$1, %eax
	je	.L30
	cmpl	$2, %eax
	je	.L31
	testl	%eax, %eax
	je	.L74
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L74:
	movl	(%rsi), %eax
	testl	%eax, %eax
	je	.L75
	cmpl	$1, %eax
	je	.L76
	movsd	.LC5(%rip), %xmm0
	addsd	8(%rsi), %xmm0
	xorl	%eax, %eax
	xorl	%edx, %edx
	movsd	40(%rdi), %xmm1
.L40:
	movapd	%xmm0, %xmm3
	movl	$1, 24(%rbx)
	unpcklpd	%xmm1, %xmm3
	movl	%edx, 28(%rbx)
	movq	%rax, 48(%rbx)
	movups	%xmm3, 32(%rbx)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L31:
	cmpl	$1, (%rsi)
	movl	28(%rdi), %edx
	jne	.L52
	movsd	8(%rsi), %xmm1
	cmpl	$2, %edx
	jg	.L55
	cmpb	$0, 24(%rsi)
	jne	.L65
	cmpl	$1, %edx
	jne	.L55
.L65:
	movsd	.LC7(%rip), %xmm0
	xorl	%eax, %eax
	addsd	%xmm1, %xmm0
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L30:
	movl	(%rsi), %eax
	movl	28(%rdi), %edx
	cmpl	$1, %eax
	je	.L41
	cmpl	$2, %eax
	je	.L42
	testl	%eax, %eax
	je	.L77
	movsd	32(%rdi), %xmm0
	movapd	%xmm0, %xmm1
	movhpd	40(%rdi), %xmm1
	movups	%xmm1, 32(%rdi)
.L58:
	movq	(%rbx), %rdi
	movq	2064(%rdi), %rax
	movl	80(%rax), %eax
	testl	%eax, %eax
	jne	.L78
.L60:
	cmpl	$4, 392(%rdi)
	je	.L62
	subsd	8(%r12), %xmm0
	movq	%rbx, %rdi
	call	_ZN2v88internal13MemoryReducer13ScheduleTimerEd.part.0
.L62:
	cmpb	$0, _ZN2v88internal21FLAG_trace_gc_verboseE(%rip)
	je	.L26
	movsd	32(%rbx), %xmm0
	movq	(%rbx), %rdi
	movl	$1, %eax
	leaq	.LC10(%rip), %rsi
	subsd	8(%r12), %xmm0
	popq	%rbx
	subq	$37592, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movsd	40(%rdi), %xmm1
	cmpl	$2, %edx
	jg	.L55
	cmpb	$0, 26(%rsi)
	movsd	8(%rsi), %xmm2
	je	.L46
	cmpb	$0, 25(%rsi)
	jne	.L47
	ucomisd	.LC2(%rip), %xmm1
	jp	.L64
	je	.L46
.L64:
	movsd	.LC6(%rip), %xmm0
	addsd	%xmm1, %xmm0
	comisd	%xmm0, %xmm2
	jbe	.L46
.L47:
	movsd	32(%rbx), %xmm0
	comisd	%xmm0, %xmm2
	jb	.L79
	addl	$1, %edx
	pxor	%xmm4, %xmm4
	movl	$2, 24(%rbx)
	movl	%edx, 28(%rbx)
	unpcklpd	%xmm1, %xmm4
	movq	$0, 48(%rbx)
	movups	%xmm4, 32(%rbx)
	.p2align 4,,10
	.p2align 3
.L52:
	cmpb	$0, _ZN2v88internal21FLAG_trace_gc_verboseE(%rip)
	movq	(%rbx), %rdi
	jne	.L80
.L57:
	popq	%rbx
	movl	$32, %edx
	popq	%r12
	movl	$17, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4Heap27StartIdleIncrementalMarkingENS0_23GarbageCollectionReasonENS_15GCCallbackFlagsE@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	subq	$37592, %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	movq	(%rbx), %rdi
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L78:
	call	_ZN2v88internal4Heap28ShouldOptimizeForMemoryUsageEv@PLT
	testb	%al, %al
	jne	.L61
.L73:
	movsd	32(%rbx), %xmm0
	movq	(%rbx), %rdi
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L75:
	movl	28(%rdi), %edx
	movsd	32(%rdi), %xmm0
	movsd	40(%rdi), %xmm1
	movq	48(%rdi), %rax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L61:
	movq	(%rbx), %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	(%rbx), %rax
	movl	$1, %edx
	addsd	.LC7(%rip), %xmm0
	movl	$1, %esi
	movq	2064(%rax), %rdi
	call	_ZN2v88internal18IncrementalMarking19AdvanceWithDeadlineEdNS1_16CompletionActionENS0_10StepOriginE@PLT
	movq	(%rbx), %rdi
	movl	$9, %esi
	call	_ZN2v88internal4Heap36FinalizeIncrementalMarkingIfCompleteENS0_23GarbageCollectionReasonE@PLT
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L55:
	movq	16(%r12), %rax
	pxor	%xmm0, %xmm0
	movl	$3, %edx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L42:
	movsd	32(%rdi), %xmm0
	movsd	40(%rdi), %xmm1
	movq	48(%rdi), %rax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L41:
	movsd	8(%rsi), %xmm1
	movsd	.LC5(%rip), %xmm0
	xorl	%eax, %eax
	addsd	%xmm1, %xmm0
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L76:
	movq	48(%rdi), %rax
	testq	%rax, %rax
	js	.L36
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L37:
	mulsd	.LC3(%rip), %xmm0
	movsd	.LC4(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L38
	cvttsd2siq	%xmm0, %rdx
.L39:
	leaq	10485760(%rax), %rcx
	cmpq	%rcx, %rdx
	cmovb	%rcx, %rdx
	cmpq	%rdx, 16(%r12)
	jnb	.L35
	movl	28(%rbx), %edx
	movsd	32(%rbx), %xmm0
	movsd	40(%rbx), %xmm1
	jmp	.L29
.L46:
	addsd	.LC5(%rip), %xmm2
	xorl	%eax, %eax
	movapd	%xmm2, %xmm0
	jmp	.L40
.L35:
	movsd	8(%r12), %xmm1
	movsd	.LC5(%rip), %xmm0
	xorl	%eax, %eax
	xorl	%edx, %edx
	addsd	%xmm1, %xmm0
	jmp	.L40
.L38:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rdx
	btcq	$63, %rdx
	jmp	.L39
.L36:
	movq	%rax, %rdx
	movq	%rax, %rcx
	pxor	%xmm0, %xmm0
	shrq	%rdx
	andl	$1, %ecx
	orq	%rcx, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L37
.L79:
	movq	48(%rbx), %rax
	jmp	.L40
	.cfi_endproc
.LFE20073:
	.size	_ZN2v88internal13MemoryReducer11NotifyTimerERKNS1_5EventE, .-_ZN2v88internal13MemoryReducer11NotifyTimerERKNS1_5EventE
	.section	.rodata._ZN2v88internal13MemoryReducer9TimerTask11RunInternalEv.str1.1,"aMS",@progbits,1
.LC11:
	.string	"background"
.LC12:
	.string	"foreground"
.LC13:
	.string	"low alloc"
.LC14:
	.string	"high alloc"
.LC15:
	.string	"Memory reducer: %s, %s\n"
	.section	.text._ZN2v88internal13MemoryReducer9TimerTask11RunInternalEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MemoryReducer9TimerTask11RunInternalEv
	.type	_ZN2v88internal13MemoryReducer9TimerTask11RunInternalEv, @function
_ZN2v88internal13MemoryReducer9TimerTask11RunInternalEv:
.LFB20072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap31MonotonicallyIncreasingTimeInMsEv@PLT
	movq	%r12, %rdi
	movq	2008(%r12), %r15
	movsd	%xmm0, -104(%rbp)
	call	_ZNK2v88internal4Heap25EmbedderAllocationCounterEv@PLT
	movq	%r12, %rdi
	movq	2152(%r12), %r13
	movq	%rax, %r14
	call	_ZN2v88internal4Heap26OldGenerationSizeOfObjectsEv@PLT
	movq	248(%r12), %rdi
	subq	2160(%r12), %r13
	addq	%rax, %r13
	movq	2144(%r12), %rax
	movq	336(%rdi), %rdx
	movq	104(%rdi), %r9
	movq	%rax, -112(%rbp)
	leaq	-8(%r9), %rcx
	leaq	-8(%rdx), %rax
	movq	%r9, %rsi
	andq	$-262144, %rax
	andq	$-262144, %rcx
	subq	%rdx, %rsi
	cmpq	%rcx, %rax
	je	.L85
	movq	48(%rax), %rsi
	subq	%rdx, %rsi
	movq	224(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L83
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%rsi, -144(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v88internal17MemoryChunkLayout27AllocatableMemoryInDataPageEv@PLT
	movq	-128(%rbp), %rdx
	movq	-144(%rbp), %rsi
	movq	-136(%rbp), %rcx
	movq	-120(%rbp), %rdi
	movq	224(%rdx), %rdx
	addq	%rax, %rsi
	cmpq	%rdx, %rcx
	jne	.L84
	movq	104(%rdi), %r9
.L83:
	addq	%r9, %rsi
	subq	40(%rdx), %rsi
.L85:
	movsd	-104(%rbp), %xmm0
	addq	-112(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal8GCTracer16SampleAllocationEdmmm@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal4Heap20HasLowAllocationRateEv@PLT
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal4Heap28ShouldOptimizeForMemoryUsageEv@PLT
	cmpb	$0, _ZN2v88internal21FLAG_trace_gc_verboseE(%rip)
	movl	%eax, %r14d
	je	.L86
	testb	%al, %al
	leaq	.LC11(%rip), %rcx
	leaq	.LC12(%rip), %rax
	cmove	%rax, %rcx
	leaq	.LC13(%rip), %rdx
	testb	%r13b, %r13b
	leaq	.LC14(%rip), %rax
	leaq	-37592(%r12), %rdi
	cmove	%rax, %rdx
	leaq	.LC15(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
.L86:
	movq	2064(%r12), %rdi
	movsd	-104(%rbp), %xmm1
	orl	%r14d, %r13d
	xorl	%eax, %eax
	movl	$0, -96(%rbp)
	movl	80(%rdi), %edx
	movb	%r13b, -71(%rbp)
	movsd	%xmm1, -88(%rbp)
	testl	%edx, %edx
	je	.L96
.L89:
	movq	%r12, %rdi
	movb	%al, -70(%rbp)
	call	_ZN2v88internal4Heap28CommittedOldGenerationMemoryEv@PLT
	movq	40(%rbx), %rdi
	leaq	-96(%rbp), %rsi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal13MemoryReducer11NotifyTimerERKNS1_5EventE
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	call	_ZN2v88internal18IncrementalMarking14CanBeActivatedEv@PLT
	orl	%r14d, %eax
	jmp	.L89
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20072:
	.size	_ZN2v88internal13MemoryReducer9TimerTask11RunInternalEv, .-_ZN2v88internal13MemoryReducer9TimerTask11RunInternalEv
	.section	.rodata._ZN2v88internal13MemoryReducer17NotifyMarkCompactERKNS1_5EventE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"will do more"
.LC17:
	.string	"done"
	.section	.rodata._ZN2v88internal13MemoryReducer17NotifyMarkCompactERKNS1_5EventE.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"Memory reducer: finished GC #%d (%s)\n"
	.section	.text._ZN2v88internal13MemoryReducer17NotifyMarkCompactERKNS1_5EventE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MemoryReducer17NotifyMarkCompactERKNS1_5EventE
	.type	_ZN2v88internal13MemoryReducer17NotifyMarkCompactERKNS1_5EventE, @function
_ZN2v88internal13MemoryReducer17NotifyMarkCompactERKNS1_5EventE:
.LFB20074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, _ZN2v88internal24FLAG_incremental_markingE(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	24(%rdi), %r12d
	je	.L99
	cmpb	$0, _ZN2v88internal19FLAG_memory_reducerE(%rip)
	jne	.L100
.L99:
	pxor	%xmm0, %xmm0
	movq	$0, 24(%rbx)
	movhpd	40(%rbx), %xmm0
	movq	$0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	cmpl	$1, %r12d
	jne	.L132
.L98:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	cmpl	$1, %r12d
	je	.L103
	cmpl	$2, %r12d
	je	.L104
	testl	%r12d, %r12d
	jne	.L105
	movl	(%rsi), %eax
	testl	%eax, %eax
	je	.L145
	cmpl	$1, %eax
	je	.L146
	movsd	.LC5(%rip), %xmm0
	addsd	8(%rsi), %xmm0
	movsd	40(%rdi), %xmm1
.L114:
	movapd	%xmm0, %xmm3
	movq	$1, 24(%rbx)
	movq	$0, 48(%rbx)
	unpcklpd	%xmm1, %xmm3
	movups	%xmm3, 32(%rbx)
.L130:
	movq	(%rbx), %rax
	cmpl	$4, 392(%rax)
	je	.L132
	subsd	8(%rsi), %xmm0
	movq	%rbx, %rdi
	call	_ZN2v88internal13MemoryReducer13ScheduleTimerEd.part.0
.L132:
	cmpl	$2, %r12d
	jne	.L98
.L143:
	cmpb	$0, _ZN2v88internal21FLAG_trace_gc_verboseE(%rip)
	je	.L98
	movq	(%rbx), %rax
	cmpl	$1, 24(%rbx)
	leaq	.LC16(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	movl	28(%rbx), %edx
	popq	%rbx
	leaq	-37592(%rax), %rdi
	leaq	.LC17(%rip), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	cmovne	%rax, %rcx
	xorl	%eax, %eax
	jmp	_ZN2v88internal7Isolate18PrintWithTimestampEPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	cmpl	$1, (%rsi)
	movl	28(%rdi), %eax
	jne	.L143
	movsd	8(%rsi), %xmm1
	cmpl	$2, %eax
	jg	.L128
	cmpb	$0, 24(%rsi)
	jne	.L137
	cmpl	$1, %eax
	jne	.L128
.L137:
	movsd	.LC7(%rip), %xmm0
	movl	$1, 24(%rbx)
	movl	%eax, 28(%rbx)
	addsd	%xmm1, %xmm0
	movq	$0, 48(%rbx)
	movapd	%xmm0, %xmm5
	unpcklpd	%xmm1, %xmm5
	movups	%xmm5, 32(%rbx)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L103:
	movl	(%rsi), %eax
	movl	28(%rdi), %edx
	cmpl	$1, %eax
	je	.L115
	cmpl	$2, %eax
	je	.L116
	testl	%eax, %eax
	jne	.L98
	movsd	40(%rdi), %xmm1
	cmpl	$2, %edx
	jle	.L120
	movq	16(%rsi), %rcx
	pxor	%xmm0, %xmm0
	movl	$3, %edx
	.p2align 4,,10
	.p2align 3
.L119:
	unpcklpd	%xmm1, %xmm0
	movl	%eax, 24(%rbx)
	movl	%edx, 28(%rbx)
	movq	%rcx, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	jmp	.L98
.L105:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	movl	28(%rdi), %eax
	movsd	32(%rdi), %xmm0
	movsd	40(%rdi), %xmm1
	movq	48(%rdi), %rdx
.L107:
	unpcklpd	%xmm1, %xmm0
	movl	$0, 24(%rbx)
	movl	%eax, 28(%rbx)
	movq	%rdx, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L128:
	movq	16(%rsi), %rax
	pxor	%xmm4, %xmm4
	movabsq	$12884901888, %rdx
	unpcklpd	%xmm1, %xmm4
	movq	%rdx, 24(%rbx)
	movq	%rax, 48(%rbx)
	movups	%xmm4, 32(%rbx)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L116:
	movsd	32(%rdi), %xmm0
	movsd	40(%rdi), %xmm1
	movl	$1, %eax
	movq	48(%rdi), %rcx
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L115:
	movsd	8(%rsi), %xmm1
	movsd	.LC5(%rip), %xmm0
	xorl	%ecx, %ecx
	addsd	%xmm1, %xmm0
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L146:
	movq	48(%rdi), %rdx
	testq	%rdx, %rdx
	js	.L110
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L111:
	mulsd	.LC3(%rip), %xmm0
	movsd	.LC4(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L112
	cvttsd2siq	%xmm0, %rax
.L113:
	leaq	10485760(%rdx), %rcx
	cmpq	%rcx, %rax
	cmovb	%rcx, %rax
	cmpq	%rax, 16(%rsi)
	jnb	.L109
	movl	28(%rbx), %eax
	movsd	32(%rbx), %xmm0
	movsd	40(%rbx), %xmm1
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L120:
	cmpb	$0, 26(%rsi)
	movsd	8(%rsi), %xmm2
	je	.L121
	cmpb	$0, 25(%rsi)
	jne	.L122
	ucomisd	.LC2(%rip), %xmm1
	jp	.L136
	je	.L121
.L136:
	movsd	.LC6(%rip), %xmm0
	addsd	%xmm1, %xmm0
	comisd	%xmm0, %xmm2
	jbe	.L121
.L122:
	movsd	32(%rbx), %xmm0
	comisd	%xmm0, %xmm2
	jb	.L147
	addl	$1, %edx
	xorl	%ecx, %ecx
	pxor	%xmm0, %xmm0
	movl	$2, %eax
	jmp	.L119
.L121:
	addsd	.LC5(%rip), %xmm2
	movl	$1, %eax
	xorl	%ecx, %ecx
	movapd	%xmm2, %xmm0
	jmp	.L119
.L112:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	btcq	$63, %rax
	jmp	.L113
.L109:
	movsd	8(%rsi), %xmm1
	movsd	.LC5(%rip), %xmm0
	addsd	%xmm1, %xmm0
	jmp	.L114
.L110:
	movq	%rdx, %rax
	movq	%rdx, %rcx
	pxor	%xmm0, %xmm0
	shrq	%rax
	andl	$1, %ecx
	orq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L111
.L147:
	movq	48(%rbx), %rcx
	movl	$1, %eax
	jmp	.L119
	.cfi_endproc
.LFE20074:
	.size	_ZN2v88internal13MemoryReducer17NotifyMarkCompactERKNS1_5EventE, .-_ZN2v88internal13MemoryReducer17NotifyMarkCompactERKNS1_5EventE
	.section	.text._ZN2v88internal13MemoryReducer21NotifyPossibleGarbageERKNS1_5EventE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MemoryReducer21NotifyPossibleGarbageERKNS1_5EventE
	.type	_ZN2v88internal13MemoryReducer21NotifyPossibleGarbageERKNS1_5EventE, @function
_ZN2v88internal13MemoryReducer21NotifyPossibleGarbageERKNS1_5EventE:
.LFB20075:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal24FLAG_incremental_markingE(%rip)
	movl	24(%rdi), %eax
	je	.L149
	cmpb	$0, _ZN2v88internal19FLAG_memory_reducerE(%rip)
	jne	.L150
.L149:
	movsd	40(%rdi), %xmm1
	xorl	%r8d, %r8d
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L151:
	movapd	%xmm0, %xmm3
	movl	%edx, 24(%rdi)
	unpcklpd	%xmm1, %xmm3
	movl	%ecx, 28(%rdi)
	movq	%r8, 48(%rdi)
	movups	%xmm3, 32(%rdi)
	cmpl	$1, %eax
	je	.L148
	cmpl	$1, %edx
	je	.L163
.L148:
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	cmpl	$1, %eax
	je	.L152
	cmpl	$2, %eax
	je	.L153
	testl	%eax, %eax
	je	.L190
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	cmpl	$1, (%rsi)
	movl	28(%rdi), %ecx
	jne	.L167
	movsd	8(%rsi), %xmm1
	cmpl	$2, %ecx
	jg	.L177
	cmpb	$0, 24(%rsi)
	jne	.L183
	cmpl	$1, %ecx
	jne	.L177
.L183:
	movsd	.LC7(%rip), %xmm0
	movl	$1, 24(%rdi)
	movl	%ecx, 28(%rdi)
	addsd	%xmm1, %xmm0
	movq	$0, 48(%rdi)
	movapd	%xmm0, %xmm5
	unpcklpd	%xmm1, %xmm5
	movups	%xmm5, 32(%rdi)
	.p2align 4,,10
	.p2align 3
.L163:
	movq	(%rdi), %rax
	cmpl	$4, 392(%rax)
	je	.L148
	subsd	8(%rsi), %xmm0
	jmp	_ZN2v88internal13MemoryReducer13ScheduleTimerEd.part.0
	.p2align 4,,10
	.p2align 3
.L190:
	movl	(%rsi), %eax
	testl	%eax, %eax
	jne	.L155
	movl	28(%rdi), %eax
	movsd	32(%rdi), %xmm0
	movsd	40(%rdi), %xmm1
	movq	48(%rdi), %rdx
.L156:
	unpcklpd	%xmm1, %xmm0
	movl	$0, 24(%rdi)
	movl	%eax, 28(%rdi)
	movq	%rdx, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	movl	(%rsi), %edx
	movl	28(%rdi), %ecx
	cmpl	$1, %edx
	je	.L164
	cmpl	$2, %edx
	je	.L165
	testl	%edx, %edx
	je	.L191
.L167:
	movsd	32(%rdi), %xmm0
	movsd	40(%rdi), %xmm1
	movl	%eax, %edx
	movq	48(%rdi), %r8
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L155:
	cmpl	$1, %eax
	je	.L192
	movsd	.LC5(%rip), %xmm0
	addsd	8(%rsi), %xmm0
	movq	$1, 24(%rdi)
	movq	$0, 48(%rdi)
	movapd	%xmm0, %xmm1
	movhpd	40(%rdi), %xmm1
	movups	%xmm1, 32(%rdi)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L177:
	movq	16(%rsi), %rax
	pxor	%xmm4, %xmm4
	movabsq	$12884901888, %rcx
	unpcklpd	%xmm1, %xmm4
	movq	%rcx, 24(%rdi)
	movq	%rax, 48(%rdi)
	movups	%xmm4, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	movsd	32(%rdi), %xmm0
	movsd	40(%rdi), %xmm1
	movl	$1, %edx
	movq	48(%rdi), %rax
.L168:
	unpcklpd	%xmm1, %xmm0
	movl	%edx, 24(%rdi)
	movl	%ecx, 28(%rdi)
	movq	%rax, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	movsd	8(%rsi), %xmm1
	movsd	.LC5(%rip), %xmm0
	xorl	%eax, %eax
	addsd	%xmm1, %xmm0
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L191:
	movsd	40(%rdi), %xmm1
	cmpl	$2, %ecx
	jle	.L169
	movq	16(%rsi), %rax
	pxor	%xmm0, %xmm0
	movl	$3, %ecx
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L192:
	movq	48(%rdi), %rdx
	testq	%rdx, %rdx
	js	.L159
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L160:
	mulsd	.LC3(%rip), %xmm0
	movsd	.LC4(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L161
	cvttsd2siq	%xmm0, %rax
.L162:
	leaq	10485760(%rdx), %rcx
	cmpq	%rcx, %rax
	cmovb	%rcx, %rax
	cmpq	%rax, 16(%rsi)
	jnb	.L158
	movl	28(%rdi), %eax
	movsd	32(%rdi), %xmm0
	movsd	40(%rdi), %xmm1
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L169:
	cmpb	$0, 26(%rsi)
	movsd	8(%rsi), %xmm2
	je	.L170
	cmpb	$0, 25(%rsi)
	jne	.L171
	ucomisd	.LC2(%rip), %xmm1
	jp	.L182
	je	.L170
.L182:
	movsd	.LC6(%rip), %xmm0
	addsd	%xmm1, %xmm0
	comisd	%xmm0, %xmm2
	jbe	.L170
.L171:
	movsd	32(%rdi), %xmm0
	comisd	%xmm0, %xmm2
	jb	.L193
	addl	$1, %ecx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movl	$2, %edx
	jmp	.L168
.L170:
	addsd	.LC5(%rip), %xmm2
	movl	$1, %edx
	xorl	%eax, %eax
	movapd	%xmm2, %xmm0
	jmp	.L168
.L161:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	btcq	$63, %rax
	jmp	.L162
.L158:
	movsd	8(%rsi), %xmm1
	movsd	.LC5(%rip), %xmm0
	movq	$1, 24(%rdi)
	movq	$0, 48(%rdi)
	addsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm6
	unpcklpd	%xmm1, %xmm6
	movups	%xmm6, 32(%rdi)
	jmp	.L163
.L159:
	movq	%rdx, %rax
	movq	%rdx, %rcx
	pxor	%xmm0, %xmm0
	shrq	%rax
	andl	$1, %ecx
	orq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L160
.L193:
	movq	48(%rdi), %rax
	movl	$1, %edx
	jmp	.L168
	.cfi_endproc
.LFE20075:
	.size	_ZN2v88internal13MemoryReducer21NotifyPossibleGarbageERKNS1_5EventE, .-_ZN2v88internal13MemoryReducer21NotifyPossibleGarbageERKNS1_5EventE
	.section	.text._ZN2v88internal13MemoryReducer10WatchdogGCERKNS1_5StateERKNS1_5EventE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MemoryReducer10WatchdogGCERKNS1_5StateERKNS1_5EventE
	.type	_ZN2v88internal13MemoryReducer10WatchdogGCERKNS1_5StateERKNS1_5EventE, @function
_ZN2v88internal13MemoryReducer10WatchdogGCERKNS1_5StateERKNS1_5EventE:
.LFB20076:
	.cfi_startproc
	endbr64
	movsd	16(%rdi), %xmm0
	ucomisd	.LC2(%rip), %xmm0
	jp	.L198
	jne	.L198
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	addsd	.LC6(%rip), %xmm0
	movsd	8(%rsi), %xmm1
	comisd	%xmm0, %xmm1
	seta	%al
	ret
	.cfi_endproc
.LFE20076:
	.size	_ZN2v88internal13MemoryReducer10WatchdogGCERKNS1_5StateERKNS1_5EventE, .-_ZN2v88internal13MemoryReducer10WatchdogGCERKNS1_5StateERKNS1_5EventE
	.section	.text._ZN2v88internal13MemoryReducer4StepERKNS1_5StateERKNS1_5EventE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MemoryReducer4StepERKNS1_5StateERKNS1_5EventE
	.type	_ZN2v88internal13MemoryReducer4StepERKNS1_5StateERKNS1_5EventE, @function
_ZN2v88internal13MemoryReducer4StepERKNS1_5StateERKNS1_5EventE:
.LFB20077:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal24FLAG_incremental_markingE(%rip)
	movq	%rdi, %rax
	je	.L201
	cmpb	$0, _ZN2v88internal19FLAG_memory_reducerE(%rip)
	jne	.L202
.L201:
	movsd	16(%rsi), %xmm0
	movq	$0, (%rax)
	movq	$0x000000000, 8(%rax)
	movq	$0, 24(%rax)
	movsd	%xmm0, 16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	movl	(%rsi), %ecx
	cmpl	$1, %ecx
	je	.L204
	cmpl	$2, %ecx
	je	.L205
	testl	%ecx, %ecx
	je	.L235
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	je	.L209
	cmpl	$1, %ecx
	je	.L236
	movsd	.LC5(%rip), %xmm0
	addsd	8(%rdx), %xmm0
	movq	$1, (%rdi)
	movhpd	16(%rsi), %xmm0
	movq	$0, 24(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	cmpl	$1, (%rdx)
	jne	.L209
	movl	4(%rsi), %ecx
	movsd	8(%rdx), %xmm0
	cmpl	$2, %ecx
	jg	.L224
	cmpb	$0, 24(%rdx)
	jne	.L227
	cmpl	$1, %ecx
	jne	.L224
.L227:
	movsd	.LC7(%rip), %xmm1
	movl	$1, (%rax)
	movl	%ecx, 4(%rax)
	addsd	%xmm0, %xmm1
	movq	$0, 24(%rax)
	movsd	%xmm0, 16(%rax)
	movsd	%xmm1, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	movl	(%rdx), %ecx
	cmpl	$1, %ecx
	je	.L214
	cmpl	$2, %ecx
	je	.L215
	testl	%ecx, %ecx
	je	.L237
.L209:
	movdqu	(%rsi), %xmm3
	movdqu	16(%rsi), %xmm4
	movups	%xmm3, (%rax)
	movups	%xmm4, 16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	movq	16(%rdx), %rdx
.L234:
	movabsq	$12884901888, %rcx
	movq	$0x000000000, 8(%rax)
	movq	%rcx, (%rax)
	movq	%rdx, 24(%rax)
	movsd	%xmm0, 16(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	movl	4(%rsi), %ecx
	cmpl	$2, %ecx
	jle	.L217
	movq	16(%rdx), %rdx
	movsd	16(%rsi), %xmm0
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L215:
	movdqu	(%rsi), %xmm5
	movdqu	16(%rsi), %xmm6
	movups	%xmm5, (%rdi)
	movups	%xmm6, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	movl	4(%rsi), %ecx
	movl	$1, (%rdi)
	movl	%ecx, 4(%rdi)
.L233:
	movsd	.LC5(%rip), %xmm0
	addsd	8(%rdx), %xmm0
	movhpd	8(%rdx), %xmm0
	movq	$0, 24(%rax)
	movups	%xmm0, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	movq	24(%rsi), %rdi
	testq	%rdi, %rdi
	js	.L210
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdi, %xmm0
.L211:
	mulsd	.LC3(%rip), %xmm0
	movsd	.LC4(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L212
	cvttsd2siq	%xmm0, %rcx
.L213:
	addq	$10485760, %rdi
	cmpq	%rdi, %rcx
	cmovb	%rdi, %rcx
	cmpq	%rcx, 16(%rdx)
	jb	.L209
	movq	$1, (%rax)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L217:
	cmpb	$0, 26(%rdx)
	movsd	8(%rdx), %xmm0
	je	.L218
	cmpb	$0, 25(%rdx)
	jne	.L219
	movsd	16(%rsi), %xmm1
	ucomisd	.LC2(%rip), %xmm1
	jp	.L226
	je	.L220
.L226:
	movsd	.LC6(%rip), %xmm2
	addsd	%xmm1, %xmm2
	comisd	%xmm2, %xmm0
	jbe	.L220
.L219:
	comisd	8(%rsi), %xmm0
	jb	.L209
	movsd	16(%rsi), %xmm0
	addl	$1, %ecx
	movl	$2, (%rax)
	movl	%ecx, 4(%rax)
	movq	$0x000000000, 8(%rax)
	movq	$0, 24(%rax)
	movsd	%xmm0, 16(%rax)
	ret
.L218:
	movsd	16(%rsi), %xmm1
.L220:
	addsd	.LC5(%rip), %xmm0
	movl	$1, (%rax)
	movl	%ecx, 4(%rax)
	movq	$0, 24(%rax)
	movsd	%xmm0, 8(%rax)
	movsd	%xmm1, 16(%rax)
	ret
.L212:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rcx
	btcq	$63, %rcx
	jmp	.L213
.L210:
	movq	%rdi, %rcx
	movq	%rdi, %r8
	pxor	%xmm0, %xmm0
	shrq	%rcx
	andl	$1, %r8d
	orq	%r8, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L211
	.cfi_endproc
.LFE20077:
	.size	_ZN2v88internal13MemoryReducer4StepERKNS1_5StateERKNS1_5EventE, .-_ZN2v88internal13MemoryReducer4StepERKNS1_5StateERKNS1_5EventE
	.section	.text._ZN2v88internal13MemoryReducer13ScheduleTimerEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MemoryReducer13ScheduleTimerEd
	.type	_ZN2v88internal13MemoryReducer13ScheduleTimerEd, @function
_ZN2v88internal13MemoryReducer13ScheduleTimerEd:
.LFB20079:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	cmpl	$4, 392(%rax)
	je	.L238
	jmp	_ZN2v88internal13MemoryReducer13ScheduleTimerEd.part.0
	.p2align 4,,10
	.p2align 3
.L238:
	ret
	.cfi_endproc
.LFE20079:
	.size	_ZN2v88internal13MemoryReducer13ScheduleTimerEd, .-_ZN2v88internal13MemoryReducer13ScheduleTimerEd
	.section	.text._ZN2v88internal13MemoryReducer8TearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13MemoryReducer8TearDownEv
	.type	_ZN2v88internal13MemoryReducer8TearDownEv, @function
_ZN2v88internal13MemoryReducer8TearDownEv:
.LFB20082:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0, 24(%rdi)
	movq	$0, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.cfi_endproc
.LFE20082:
	.size	_ZN2v88internal13MemoryReducer8TearDownEv, .-_ZN2v88internal13MemoryReducer8TearDownEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal13MemoryReducer12kLongDelayMsE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal13MemoryReducer12kLongDelayMsE, @function
_GLOBAL__sub_I__ZN2v88internal13MemoryReducer12kLongDelayMsE:
.LFB25104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25104:
	.size	_GLOBAL__sub_I__ZN2v88internal13MemoryReducer12kLongDelayMsE, .-_GLOBAL__sub_I__ZN2v88internal13MemoryReducer12kLongDelayMsE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal13MemoryReducer12kLongDelayMsE
	.weak	_ZTVN2v88internal14CancelableTaskE
	.section	.data.rel.ro._ZTVN2v88internal14CancelableTaskE,"awG",@progbits,_ZTVN2v88internal14CancelableTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal14CancelableTaskE, @object
	.size	_ZTVN2v88internal14CancelableTaskE, 88
_ZTVN2v88internal14CancelableTaskE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	__cxa_pure_virtual
	.quad	-32
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.weak	_ZTVN2v88internal13MemoryReducer9TimerTaskE
	.section	.data.rel.ro.local._ZTVN2v88internal13MemoryReducer9TimerTaskE,"awG",@progbits,_ZTVN2v88internal13MemoryReducer9TimerTaskE,comdat
	.align 8
	.type	_ZTVN2v88internal13MemoryReducer9TimerTaskE, @object
	.size	_ZTVN2v88internal13MemoryReducer9TimerTaskE, 88
_ZTVN2v88internal13MemoryReducer9TimerTaskE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal13MemoryReducer9TimerTaskD1Ev
	.quad	_ZN2v88internal13MemoryReducer9TimerTaskD0Ev
	.quad	_ZN2v88internal14CancelableTask3RunEv
	.quad	_ZN2v88internal13MemoryReducer9TimerTask11RunInternalEv
	.quad	-32
	.quad	0
	.quad	_ZThn32_N2v88internal13MemoryReducer9TimerTaskD1Ev
	.quad	_ZThn32_N2v88internal13MemoryReducer9TimerTaskD0Ev
	.quad	_ZThn32_N2v88internal14CancelableTask3RunEv
	.globl	_ZN2v88internal13MemoryReducer21kCommittedMemoryDeltaE
	.section	.rodata._ZN2v88internal13MemoryReducer21kCommittedMemoryDeltaE,"a"
	.align 8
	.type	_ZN2v88internal13MemoryReducer21kCommittedMemoryDeltaE, @object
	.size	_ZN2v88internal13MemoryReducer21kCommittedMemoryDeltaE, 8
_ZN2v88internal13MemoryReducer21kCommittedMemoryDeltaE:
	.quad	10485760
	.globl	_ZN2v88internal13MemoryReducer22kCommittedMemoryFactorE
	.section	.rodata._ZN2v88internal13MemoryReducer22kCommittedMemoryFactorE,"a"
	.align 8
	.type	_ZN2v88internal13MemoryReducer22kCommittedMemoryFactorE, @object
	.size	_ZN2v88internal13MemoryReducer22kCommittedMemoryFactorE, 8
_ZN2v88internal13MemoryReducer22kCommittedMemoryFactorE:
	.long	2576980378
	.long	1072798105
	.globl	_ZN2v88internal13MemoryReducer15kMaxNumberOfGCsE
	.section	.rodata._ZN2v88internal13MemoryReducer15kMaxNumberOfGCsE,"a"
	.align 4
	.type	_ZN2v88internal13MemoryReducer15kMaxNumberOfGCsE, @object
	.size	_ZN2v88internal13MemoryReducer15kMaxNumberOfGCsE, 4
_ZN2v88internal13MemoryReducer15kMaxNumberOfGCsE:
	.long	3
	.globl	_ZN2v88internal13MemoryReducer16kWatchdogDelayMsE
	.section	.rodata._ZN2v88internal13MemoryReducer16kWatchdogDelayMsE,"a"
	.align 4
	.type	_ZN2v88internal13MemoryReducer16kWatchdogDelayMsE, @object
	.size	_ZN2v88internal13MemoryReducer16kWatchdogDelayMsE, 4
_ZN2v88internal13MemoryReducer16kWatchdogDelayMsE:
	.long	100000
	.globl	_ZN2v88internal13MemoryReducer13kShortDelayMsE
	.section	.rodata._ZN2v88internal13MemoryReducer13kShortDelayMsE,"a"
	.align 4
	.type	_ZN2v88internal13MemoryReducer13kShortDelayMsE, @object
	.size	_ZN2v88internal13MemoryReducer13kShortDelayMsE, 4
_ZN2v88internal13MemoryReducer13kShortDelayMsE:
	.long	500
	.globl	_ZN2v88internal13MemoryReducer12kLongDelayMsE
	.section	.rodata._ZN2v88internal13MemoryReducer12kLongDelayMsE,"a"
	.align 4
	.type	_ZN2v88internal13MemoryReducer12kLongDelayMsE, @object
	.size	_ZN2v88internal13MemoryReducer12kLongDelayMsE, 4
_ZN2v88internal13MemoryReducer12kLongDelayMsE:
	.long	8000
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1079574528
	.align 8
.LC1:
	.long	0
	.long	1083129856
	.align 8
.LC2:
	.long	0
	.long	0
	.align 8
.LC3:
	.long	2576980378
	.long	1072798105
	.align 8
.LC4:
	.long	0
	.long	1138753536
	.align 8
.LC5:
	.long	0
	.long	1086275584
	.align 8
.LC6:
	.long	0
	.long	1090021888
	.align 8
.LC7:
	.long	0
	.long	1082081280
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
