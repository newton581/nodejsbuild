	.file	"handler-table.cc"
	.text
	.section	.text._ZN2v88internal12HandlerTableC2ENS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTableC2ENS0_4CodeE
	.type	_ZN2v88internal12HandlerTableC2ENS0_4CodeE, @function
_ZN2v88internal12HandlerTableC2ENS0_4CodeE:
.LFB19253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-40(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$24, %rsp
	movq	%rsi, -40(%rbp)
	call	_ZNK2v88internal4Code18handler_table_sizeEv@PLT
	movq	-40(%rbp), %rdx
	movl	%eax, %r12d
	movl	43(%rdx), %eax
	leaq	63(%rdx), %rcx
	testl	%eax, %eax
	js	.L7
.L3:
	movslq	51(%rdx), %rax
	movl	%r12d, %edx
	shrl	$31, %edx
	addl	%r12d, %edx
	addq	%rcx, %rax
	sarl	%edx
	movq	%rax, 8(%rbx)
	movslq	%edx, %rdx
	shrq	$2, %rdx
	movl	%edx, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-40(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L3
	.cfi_endproc
.LFE19253:
	.size	_ZN2v88internal12HandlerTableC2ENS0_4CodeE, .-_ZN2v88internal12HandlerTableC2ENS0_4CodeE
	.globl	_ZN2v88internal12HandlerTableC1ENS0_4CodeE
	.set	_ZN2v88internal12HandlerTableC1ENS0_4CodeE,_ZN2v88internal12HandlerTableC2ENS0_4CodeE
	.section	.text._ZN2v88internal12HandlerTableC2ENS0_13BytecodeArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTableC2ENS0_13BytecodeArrayE
	.type	_ZN2v88internal12HandlerTableC2ENS0_13BytecodeArrayE, @function
_ZN2v88internal12HandlerTableC2ENS0_13BytecodeArrayE:
.LFB19256:
	.cfi_startproc
	endbr64
	movq	23(%rsi), %rdx
	movslq	11(%rdx), %rcx
	testl	%ecx, %ecx
	leal	3(%rcx), %eax
	cmovns	%ecx, %eax
	addq	$15, %rdx
	movq	%rdx, 8(%rdi)
	sarl	$2, %eax
	cltq
	shrq	$2, %rax
	movl	%eax, (%rdi)
	ret
	.cfi_endproc
.LFE19256:
	.size	_ZN2v88internal12HandlerTableC2ENS0_13BytecodeArrayE, .-_ZN2v88internal12HandlerTableC2ENS0_13BytecodeArrayE
	.globl	_ZN2v88internal12HandlerTableC1ENS0_13BytecodeArrayE
	.set	_ZN2v88internal12HandlerTableC1ENS0_13BytecodeArrayE,_ZN2v88internal12HandlerTableC2ENS0_13BytecodeArrayE
	.section	.text._ZN2v88internal12HandlerTableC2ENS0_9ByteArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTableC2ENS0_9ByteArrayE
	.type	_ZN2v88internal12HandlerTableC2ENS0_9ByteArrayE, @function
_ZN2v88internal12HandlerTableC2ENS0_9ByteArrayE:
.LFB19259:
	.cfi_startproc
	endbr64
	movslq	11(%rsi), %rdx
	testl	%edx, %edx
	leal	3(%rdx), %eax
	cmovns	%edx, %eax
	addq	$15, %rsi
	movq	%rsi, 8(%rdi)
	sarl	$2, %eax
	cltq
	shrq	$2, %rax
	movl	%eax, (%rdi)
	ret
	.cfi_endproc
.LFE19259:
	.size	_ZN2v88internal12HandlerTableC2ENS0_9ByteArrayE, .-_ZN2v88internal12HandlerTableC2ENS0_9ByteArrayE
	.globl	_ZN2v88internal12HandlerTableC1ENS0_9ByteArrayE
	.set	_ZN2v88internal12HandlerTableC1ENS0_9ByteArrayE,_ZN2v88internal12HandlerTableC2ENS0_9ByteArrayE
	.section	.rodata._ZN2v88internal12HandlerTableC2EmiNS1_12EncodingModeE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal12HandlerTableC2EmiNS1_12EncodingModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTableC2EmiNS1_12EncodingModeE
	.type	_ZN2v88internal12HandlerTableC2EmiNS1_12EncodingModeE, @function
_ZN2v88internal12HandlerTableC2EmiNS1_12EncodingModeE:
.LFB19262:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	testl	%ecx, %ecx
	je	.L12
	cmpl	$1, %ecx
	jne	.L17
	movl	$2, %ecx
	cltd
	movq	%rsi, 8(%rdi)
	idivl	%ecx
	cltq
	shrq	$2, %rax
	movl	%eax, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$4, %ecx
	cltd
	movq	%rsi, 8(%rdi)
	idivl	%ecx
	cltq
	shrq	$2, %rax
	movl	%eax, (%rdi)
	ret
.L17:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19262:
	.size	_ZN2v88internal12HandlerTableC2EmiNS1_12EncodingModeE, .-_ZN2v88internal12HandlerTableC2EmiNS1_12EncodingModeE
	.globl	_ZN2v88internal12HandlerTableC1EmiNS1_12EncodingModeE
	.set	_ZN2v88internal12HandlerTableC1EmiNS1_12EncodingModeE,_ZN2v88internal12HandlerTableC2EmiNS1_12EncodingModeE
	.section	.text._ZN2v88internal12HandlerTable17EntrySizeFromModeENS1_12EncodingModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTable17EntrySizeFromModeENS1_12EncodingModeE
	.type	_ZN2v88internal12HandlerTable17EntrySizeFromModeENS1_12EncodingModeE, @function
_ZN2v88internal12HandlerTable17EntrySizeFromModeENS1_12EncodingModeE:
.LFB19264:
	.cfi_startproc
	endbr64
	testl	%edi, %edi
	je	.L20
	cmpl	$1, %edi
	jne	.L25
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$4, %eax
	ret
.L25:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE19264:
	.size	_ZN2v88internal12HandlerTable17EntrySizeFromModeENS1_12EncodingModeE, .-_ZN2v88internal12HandlerTable17EntrySizeFromModeENS1_12EncodingModeE
	.section	.text._ZNK2v88internal12HandlerTable13GetRangeStartEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12HandlerTable13GetRangeStartEi
	.type	_ZNK2v88internal12HandlerTable13GetRangeStartEi, @function
_ZNK2v88internal12HandlerTable13GetRangeStartEi:
.LFB19265:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	sall	$2, %esi
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %eax
	ret
	.cfi_endproc
.LFE19265:
	.size	_ZNK2v88internal12HandlerTable13GetRangeStartEi, .-_ZNK2v88internal12HandlerTable13GetRangeStartEi
	.section	.text._ZNK2v88internal12HandlerTable11GetRangeEndEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12HandlerTable11GetRangeEndEi
	.type	_ZNK2v88internal12HandlerTable11GetRangeEndEi, @function
_ZNK2v88internal12HandlerTable11GetRangeEndEi:
.LFB19266:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	leal	1(,%rsi,4), %eax
	cltq
	movl	(%rdx,%rax,4), %eax
	ret
	.cfi_endproc
.LFE19266:
	.size	_ZNK2v88internal12HandlerTable11GetRangeEndEi, .-_ZNK2v88internal12HandlerTable11GetRangeEndEi
	.section	.text._ZNK2v88internal12HandlerTable15GetRangeHandlerEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12HandlerTable15GetRangeHandlerEi
	.type	_ZNK2v88internal12HandlerTable15GetRangeHandlerEi, @function
_ZNK2v88internal12HandlerTable15GetRangeHandlerEi:
.LFB19267:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	leal	2(,%rsi,4), %eax
	cltq
	movl	(%rdx,%rax,4), %eax
	shrl	$3, %eax
	ret
	.cfi_endproc
.LFE19267:
	.size	_ZNK2v88internal12HandlerTable15GetRangeHandlerEi, .-_ZNK2v88internal12HandlerTable15GetRangeHandlerEi
	.section	.text._ZNK2v88internal12HandlerTable12GetRangeDataEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12HandlerTable12GetRangeDataEi
	.type	_ZNK2v88internal12HandlerTable12GetRangeDataEi, @function
_ZNK2v88internal12HandlerTable12GetRangeDataEi:
.LFB19268:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	leal	3(,%rsi,4), %eax
	cltq
	movl	(%rdx,%rax,4), %eax
	ret
	.cfi_endproc
.LFE19268:
	.size	_ZNK2v88internal12HandlerTable12GetRangeDataEi, .-_ZNK2v88internal12HandlerTable12GetRangeDataEi
	.section	.text._ZNK2v88internal12HandlerTable18GetRangePredictionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12HandlerTable18GetRangePredictionEi
	.type	_ZNK2v88internal12HandlerTable18GetRangePredictionEi, @function
_ZNK2v88internal12HandlerTable18GetRangePredictionEi:
.LFB19269:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	leal	2(,%rsi,4), %eax
	cltq
	movl	(%rdx,%rax,4), %eax
	andl	$7, %eax
	ret
	.cfi_endproc
.LFE19269:
	.size	_ZNK2v88internal12HandlerTable18GetRangePredictionEi, .-_ZNK2v88internal12HandlerTable18GetRangePredictionEi
	.section	.text._ZNK2v88internal12HandlerTable15GetReturnOffsetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12HandlerTable15GetReturnOffsetEi
	.type	_ZNK2v88internal12HandlerTable15GetReturnOffsetEi, @function
_ZNK2v88internal12HandlerTable15GetReturnOffsetEi:
.LFB19270:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	addl	%esi, %esi
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %eax
	ret
	.cfi_endproc
.LFE19270:
	.size	_ZNK2v88internal12HandlerTable15GetReturnOffsetEi, .-_ZNK2v88internal12HandlerTable15GetReturnOffsetEi
	.section	.text._ZNK2v88internal12HandlerTable16GetReturnHandlerEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12HandlerTable16GetReturnHandlerEi
	.type	_ZNK2v88internal12HandlerTable16GetReturnHandlerEi, @function
_ZNK2v88internal12HandlerTable16GetReturnHandlerEi:
.LFB19271:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	leal	1(%rsi,%rsi), %eax
	cltq
	movl	(%rdx,%rax,4), %eax
	shrl	$3, %eax
	ret
	.cfi_endproc
.LFE19271:
	.size	_ZNK2v88internal12HandlerTable16GetReturnHandlerEi, .-_ZNK2v88internal12HandlerTable16GetReturnHandlerEi
	.section	.text._ZN2v88internal12HandlerTable13SetRangeStartEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTable13SetRangeStartEii
	.type	_ZN2v88internal12HandlerTable13SetRangeStartEii, @function
_ZN2v88internal12HandlerTable13SetRangeStartEii:
.LFB19272:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	sall	$2, %esi
	movslq	%esi, %rsi
	movl	%edx, (%rax,%rsi,4)
	ret
	.cfi_endproc
.LFE19272:
	.size	_ZN2v88internal12HandlerTable13SetRangeStartEii, .-_ZN2v88internal12HandlerTable13SetRangeStartEii
	.section	.text._ZN2v88internal12HandlerTable11SetRangeEndEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTable11SetRangeEndEii
	.type	_ZN2v88internal12HandlerTable11SetRangeEndEii, @function
_ZN2v88internal12HandlerTable11SetRangeEndEii:
.LFB19273:
	.cfi_startproc
	endbr64
	movl	%edx, %r8d
	leal	1(,%rsi,4), %eax
	movq	8(%rdi), %rdx
	cltq
	movl	%r8d, (%rdx,%rax,4)
	ret
	.cfi_endproc
.LFE19273:
	.size	_ZN2v88internal12HandlerTable11SetRangeEndEii, .-_ZN2v88internal12HandlerTable11SetRangeEndEii
	.section	.text._ZN2v88internal12HandlerTable15SetRangeHandlerEiiNS1_15CatchPredictionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTable15SetRangeHandlerEiiNS1_15CatchPredictionE
	.type	_ZN2v88internal12HandlerTable15SetRangeHandlerEiiNS1_15CatchPredictionE, @function
_ZN2v88internal12HandlerTable15SetRangeHandlerEiiNS1_15CatchPredictionE:
.LFB19274:
	.cfi_startproc
	endbr64
	leal	2(,%rsi,4), %eax
	sall	$3, %edx
	movq	8(%rdi), %rsi
	cltq
	orl	%ecx, %edx
	movl	%edx, (%rsi,%rax,4)
	ret
	.cfi_endproc
.LFE19274:
	.size	_ZN2v88internal12HandlerTable15SetRangeHandlerEiiNS1_15CatchPredictionE, .-_ZN2v88internal12HandlerTable15SetRangeHandlerEiiNS1_15CatchPredictionE
	.section	.text._ZN2v88internal12HandlerTable12SetRangeDataEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTable12SetRangeDataEii
	.type	_ZN2v88internal12HandlerTable12SetRangeDataEii, @function
_ZN2v88internal12HandlerTable12SetRangeDataEii:
.LFB19277:
	.cfi_startproc
	endbr64
	movl	%edx, %r8d
	leal	3(,%rsi,4), %eax
	movq	8(%rdi), %rdx
	cltq
	movl	%r8d, (%rdx,%rax,4)
	ret
	.cfi_endproc
.LFE19277:
	.size	_ZN2v88internal12HandlerTable12SetRangeDataEii, .-_ZN2v88internal12HandlerTable12SetRangeDataEii
	.section	.text._ZN2v88internal12HandlerTable14LengthForRangeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTable14LengthForRangeEi
	.type	_ZN2v88internal12HandlerTable14LengthForRangeEi, @function
_ZN2v88internal12HandlerTable14LengthForRangeEi:
.LFB19278:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	sall	$4, %eax
	ret
	.cfi_endproc
.LFE19278:
	.size	_ZN2v88internal12HandlerTable14LengthForRangeEi, .-_ZN2v88internal12HandlerTable14LengthForRangeEi
	.section	.text._ZN2v88internal12HandlerTable20EmitReturnTableStartEPNS0_9AssemblerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTable20EmitReturnTableStartEPNS0_9AssemblerE
	.type	_ZN2v88internal12HandlerTable20EmitReturnTableStartEPNS0_9AssemblerE, @function
_ZN2v88internal12HandlerTable20EmitReturnTableStartEPNS0_9AssemblerE:
.LFB19279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal9Assembler9DataAlignEi@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L43
.L39:
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L44
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	leaq	-80(%rbp), %r13
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
	movq	$28, -88(%rbp)
	movq	%r13, %rdi
	leaq	-64(%rbp), %r12
	movq	%r12, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-88(%rbp), %rdx
	leaq	40(%rbx), %rdi
	movdqa	.LC1(%rip), %xmm0
	movabsq	$7022273394677408878, %rcx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movl	$778398818, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-80(%rbp), %rdx
	movq	%rcx, 16(%rax)
	movq	-88(%rbp), %rax
	movq	%rax, -72(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r13, %rdx
	movq	32(%rbx), %rsi
	subq	16(%rbx), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-80(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L39
	call	_ZdlPv@PLT
	jmp	.L39
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19279:
	.size	_ZN2v88internal12HandlerTable20EmitReturnTableStartEPNS0_9AssemblerE, .-_ZN2v88internal12HandlerTable20EmitReturnTableStartEPNS0_9AssemblerE
	.section	.text._ZN2v88internal12HandlerTable15EmitReturnEntryEPNS0_9AssemblerEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTable15EmitReturnEntryEPNS0_9AssemblerEii
	.type	_ZN2v88internal12HandlerTable15EmitReturnEntryEPNS0_9AssemblerEii, @function
_ZN2v88internal12HandlerTable15EmitReturnEntryEPNS0_9AssemblerEii:
.LFB19280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	call	_ZN2v88internal9Assembler2ddEj@PLT
	leal	0(,%rbx,8), %esi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler2ddEj@PLT
	.cfi_endproc
.LFE19280:
	.size	_ZN2v88internal12HandlerTable15EmitReturnEntryEPNS0_9AssemblerEii, .-_ZN2v88internal12HandlerTable15EmitReturnEntryEPNS0_9AssemblerEii
	.section	.text._ZNK2v88internal12HandlerTable20NumberOfRangeEntriesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12HandlerTable20NumberOfRangeEntriesEv
	.type	_ZNK2v88internal12HandlerTable20NumberOfRangeEntriesEv, @function
_ZNK2v88internal12HandlerTable20NumberOfRangeEntriesEv:
.LFB19281:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE19281:
	.size	_ZNK2v88internal12HandlerTable20NumberOfRangeEntriesEv, .-_ZNK2v88internal12HandlerTable20NumberOfRangeEntriesEv
	.section	.text._ZNK2v88internal12HandlerTable21NumberOfReturnEntriesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12HandlerTable21NumberOfReturnEntriesEv
	.type	_ZNK2v88internal12HandlerTable21NumberOfReturnEntriesEv, @function
_ZNK2v88internal12HandlerTable21NumberOfReturnEntriesEv:
.LFB23046:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	ret
	.cfi_endproc
.LFE23046:
	.size	_ZNK2v88internal12HandlerTable21NumberOfReturnEntriesEv, .-_ZNK2v88internal12HandlerTable21NumberOfReturnEntriesEv
	.section	.text._ZN2v88internal12HandlerTable11LookupRangeEiPiPNS1_15CatchPredictionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTable11LookupRangeEiPiPNS1_15CatchPredictionE
	.type	_ZN2v88internal12HandlerTable11LookupRangeEiPiPNS1_15CatchPredictionE, @function
_ZN2v88internal12HandlerTable11LookupRangeEiPiPNS1_15CatchPredictionE:
.LFB19283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	(%rdi), %ebx
	testl	%ebx, %ebx
	jle	.L65
	testq	%rcx, %rcx
	je	.L101
	testq	%rdx, %rdx
	je	.L102
	movq	8(%rdi), %rax
	xorl	%r8d, %r8d
	xorl	%r10d, %r10d
	movl	$-1, %r12d
	.p2align 4,,10
	.p2align 3
.L59:
	movl	8(%rax,%r8), %r11d
	movl	%r11d, %r9d
	shrl	$3, %r9d
	cmpl	4(%r8,%rax), %esi
	jge	.L63
	cmpl	(%rax,%r8), %esi
	jl	.L63
	leal	3(,%r10,4), %ebx
	andl	$7, %r11d
	addl	$1, %r10d
	addq	$16, %r8
	movl	(%rax,%rbx,4), %eax
	movl	%eax, (%rdx)
	movl	%r11d, (%rcx)
	movl	(%rdi), %ebx
	cmpl	%r10d, %ebx
	jle	.L49
	movq	8(%rdi), %rax
	movl	%r9d, %r12d
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L63:
	addl	$1, %r10d
	addq	$16, %r8
	cmpl	%ebx, %r10d
	jl	.L59
	movl	%r12d, %r9d
.L49:
	popq	%rbx
	movl	%r9d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L103
	movq	8(%rdi), %rax
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$-1, %r10d
	.p2align 4,,10
	.p2align 3
.L52:
	movl	8(%rcx,%rax), %r9d
	shrl	$3, %r9d
	cmpl	4(%rcx,%rax), %esi
	jge	.L57
.L104:
	cmpl	(%rcx,%rax), %esi
	jl	.L57
	leal	3(,%r8,4), %r10d
	addl	$1, %r8d
	addq	$16, %rcx
	movl	(%rax,%r10,4), %eax
	movl	%eax, (%rdx)
	movl	(%rdi), %ebx
	cmpl	%r8d, %ebx
	jle	.L49
	movq	8(%rdi), %rax
	movl	%r9d, %r10d
	movl	8(%rcx,%rax), %r9d
	shrl	$3, %r9d
	cmpl	4(%rcx,%rax), %esi
	jl	.L104
.L57:
	addl	$1, %r8d
	addq	$16, %rcx
	cmpl	%ebx, %r8d
	jl	.L52
	movl	%r10d, %r9d
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L102:
	movq	8(%rdi), %rax
	xorl	%r10d, %r10d
	movl	$-1, %r11d
	.p2align 4,,10
	.p2align 3
.L62:
	movl	8(%rdx,%rax), %r8d
	movl	%r8d, %r9d
	shrl	$3, %r9d
	cmpl	4(%rdx,%rax), %esi
	jge	.L60
	cmpl	(%rdx,%rax), %esi
	jl	.L60
	andl	$7, %r8d
	addl	$1, %r10d
	addq	$16, %rdx
	movl	%r8d, (%rcx)
	movl	(%rdi), %ebx
	cmpl	%ebx, %r10d
	jge	.L49
	movq	8(%rdi), %rax
	movl	%r9d, %r11d
	jmp	.L62
.L103:
	movq	8(%rdi), %rax
	movl	$-1, %ecx
	.p2align 4,,10
	.p2align 3
.L56:
	movl	8(%rax), %r9d
	addl	$1, %edx
	shrl	$3, %r9d
	cmpl	4(%rax), %esi
	jge	.L53
.L105:
	cmpl	(%rax), %esi
	jl	.L53
	addq	$16, %rax
	cmpl	%edx, %ebx
	jle	.L49
	movl	%r9d, %ecx
	movl	8(%rax), %r9d
	addl	$1, %edx
	shrl	$3, %r9d
	cmpl	4(%rax), %esi
	jl	.L105
.L53:
	addq	$16, %rax
	cmpl	%edx, %ebx
	jg	.L56
	movl	%ecx, %r9d
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L60:
	addl	$1, %r10d
	addq	$16, %rdx
	cmpl	%r10d, %ebx
	jg	.L62
	movl	%r11d, %r9d
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$-1, %r9d
	popq	%rbx
	popq	%r12
	movl	%r9d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19283:
	.size	_ZN2v88internal12HandlerTable11LookupRangeEiPiPNS1_15CatchPredictionE, .-_ZN2v88internal12HandlerTable11LookupRangeEiPiPNS1_15CatchPredictionE
	.section	.text._ZN2v88internal12HandlerTable12LookupReturnEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTable12LookupReturnEi
	.type	_ZN2v88internal12HandlerTable12LookupReturnEi, @function
_ZN2v88internal12HandlerTable12LookupReturnEi:
.LFB19284:
	.cfi_startproc
	endbr64
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L110
	movq	8(%rdi), %rdi
	addl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%rdi, %rdx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L108:
	addl	$2, %eax
	addq	$8, %rdx
	cmpl	%eax, %ecx
	je	.L110
.L109:
	cmpl	(%rdx), %esi
	jne	.L108
	addl	$1, %eax
	cltq
	movl	(%rdi,%rax,4), %eax
	shrl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE19284:
	.size	_ZN2v88internal12HandlerTable12LookupReturnEi, .-_ZN2v88internal12HandlerTable12LookupReturnEi
	.section	.rodata._ZN2v88internal12HandlerTable22HandlerTableRangePrintERSo.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"   from   to       hdlr (prediction,   data)\n"
	.section	.rodata._ZN2v88internal12HandlerTable22HandlerTableRangePrintERSo.str1.1,"aMS",@progbits,1
.LC3:
	.string	"  ("
.LC4:
	.string	","
.LC5:
	.string	")  ->  "
.LC6:
	.string	" (prediction="
.LC7:
	.string	", data="
.LC8:
	.string	")\n"
	.section	.text._ZN2v88internal12HandlerTable22HandlerTableRangePrintERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTable22HandlerTableRangePrintERSo
	.type	_ZN2v88internal12HandlerTable22HandlerTableRangePrintERSo, @function
_ZN2v88internal12HandlerTable22HandlerTableRangePrintERSo:
.LFB19285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$45, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	leaq	.LC2(%rip), %rsi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-56(%rbp), %r8
	movl	(%r8), %eax
	testl	%eax, %eax
	jle	.L112
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L114:
	movq	8(%r8), %rax
	movl	$3, %edx
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	leaq	.LC3(%rip), %rsi
	addl	$1, %r13d
	movl	12(%rax,%r14), %r9d
	movl	4(%rax,%r14), %r10d
	movl	(%rax,%r14), %r12d
	movl	8(%rax,%r14), %ebx
	addq	$16, %r14
	movl	%r9d, -56(%rbp)
	movl	%r10d, -60(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rax
	movl	%r12d, %esi
	movq	%r15, %rdi
	movq	-24(%rax), %rax
	movq	$4, 16(%r15,%rax)
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movl	-60(%rbp), %r10d
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movl	%r10d, %esi
	movq	$4, 16(%r12,%rax)
	call	_ZNSolsEi@PLT
	movl	$7, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movl	%ebx, %esi
	movq	%r12, %rdi
	shrl	$3, %esi
	andl	$7, %ebx
	movq	-24(%rax), %rax
	movq	$4, 16(%r12,%rax)
	call	_ZNSolsEi@PLT
	movl	$13, %edx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZNSolsEi@PLT
	movl	$7, %edx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-56(%rbp), %r9d
	movq	%r12, %rdi
	movl	%r9d, %esi
	call	_ZNSolsEi@PLT
	movl	$2, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-72(%rbp), %r8
	cmpl	%r13d, (%r8)
	jg	.L114
.L112:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19285:
	.size	_ZN2v88internal12HandlerTable22HandlerTableRangePrintERSo, .-_ZN2v88internal12HandlerTable22HandlerTableRangePrintERSo
	.section	.rodata._ZN2v88internal12HandlerTable23HandlerTableReturnPrintERSo.str1.1,"aMS",@progbits,1
.LC9:
	.string	"  offset   handler\n"
.LC10:
	.string	"    "
.LC11:
	.string	"  ->  "
.LC12:
	.string	"\n"
	.section	.text._ZN2v88internal12HandlerTable23HandlerTableReturnPrintERSo,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12HandlerTable23HandlerTableReturnPrintERSo
	.type	_ZN2v88internal12HandlerTable23HandlerTableReturnPrintERSo, @function
_ZN2v88internal12HandlerTable23HandlerTableReturnPrintERSo:
.LFB19286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$19, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	subq	$24, %rsp
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	(%r15), %eax
	testl	%eax, %eax
	jle	.L117
	xorl	%r14d, %r14d
	leaq	.LC10(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L119:
	movq	8(%r15), %rax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	(%rax,%r14,8), %r12d
	movl	4(%rax,%r14,8), %r8d
	addq	$1, %r14
	movq	(%rbx), %rax
	shrl	$3, %r8d
	movq	-24(%rax), %rdx
	movl	%r8d, -52(%rbp)
	addq	%rbx, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	movl	$4, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rax
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movq	-24(%rax), %rax
	movq	$4, 16(%rbx,%rax)
	call	_ZNSolsEi@PLT
	movl	$6, %edx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rax
	movl	-52(%rbp), %r8d
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movl	%r8d, %esi
	movq	$4, 16(%r12,%rax)
	call	_ZNSolsEi@PLT
	leaq	.LC12(%rip), %rsi
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	-24(%rax), %rdx
	addq	%rdi, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$2, %eax
	movl	%eax, 24(%rdx)
	movl	$1, %edx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, (%r15)
	jg	.L119
.L117:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19286:
	.size	_ZN2v88internal12HandlerTable23HandlerTableReturnPrintERSo, .-_ZN2v88internal12HandlerTable23HandlerTableReturnPrintERSo
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal12HandlerTableC2ENS0_4CodeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal12HandlerTableC2ENS0_4CodeE, @function
_GLOBAL__sub_I__ZN2v88internal12HandlerTableC2ENS0_4CodeE:
.LFB23018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23018:
	.size	_GLOBAL__sub_I__ZN2v88internal12HandlerTableC2ENS0_4CodeE, .-_GLOBAL__sub_I__ZN2v88internal12HandlerTableC2ENS0_4CodeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal12HandlerTableC2ENS0_4CodeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	7305815258813905723
	.quad	7018895677944591472
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
