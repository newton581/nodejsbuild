	.file	"unoptimized-compilation-info.cc"
	.text
	.section	.text._ZN2v88internal26UnoptimizedCompilationInfoC2EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26UnoptimizedCompilationInfoC2EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE
	.type	_ZN2v88internal26UnoptimizedCompilationInfoC2EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE, @function
_ZN2v88internal26UnoptimizedCompilationInfoC2EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE:
.LFB19461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$24, %rsp
	movq	$0, 48(%rbx)
	movq	%rsi, 56(%rbx)
	movq	$0, 80(%rbx)
	movl	$0, 88(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 64(%rbx)
	movq	16(%rsi), %rax
	movq	%rsi, 8(%rbx)
	movq	24(%rsi), %rsi
	movl	$0, (%rbx)
	subq	%rax, %rsi
	cmpq	$15, %rsi
	jbe	.L22
	leaq	16(%rax), %rsi
	movq	%rsi, 16(%rdi)
.L3:
	movq	%rax, 64(%rbx)
	movq	%rax, 72(%rbx)
	movq	152(%rdx), %rax
	movq	%rsi, 80(%rbx)
	movq	%rax, 24(%rbx)
	movl	8(%rdx), %eax
	movq	%rcx, 16(%rbx)
	testb	$4, %al
	je	.L4
	orl	$1, (%rbx)
.L4:
	testb	$2, %ah
	je	.L5
	orl	$2, (%rbx)
.L5:
	testl	$131072, %eax
	je	.L6
	orl	$4, (%rbx)
.L6:
	testl	$268435456, %eax
	je	.L1
	orl	$8, (%rbx)
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	$16, %esi
	movq	%rcx, -32(%rbp)
	movq	%rdx, -24(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rcx
	leaq	16(%rax), %rsi
	jmp	.L3
	.cfi_endproc
.LFE19461:
	.size	_ZN2v88internal26UnoptimizedCompilationInfoC2EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE, .-_ZN2v88internal26UnoptimizedCompilationInfoC2EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE
	.globl	_ZN2v88internal26UnoptimizedCompilationInfoC1EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE
	.set	_ZN2v88internal26UnoptimizedCompilationInfoC1EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE,_ZN2v88internal26UnoptimizedCompilationInfoC2EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE
	.section	.text._ZNK2v88internal26UnoptimizedCompilationInfo5scopeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal26UnoptimizedCompilationInfo5scopeEv
	.type	_ZNK2v88internal26UnoptimizedCompilationInfo5scopeEv, @function
_ZNK2v88internal26UnoptimizedCompilationInfo5scopeEv:
.LFB19463:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	40(%rax), %rax
	ret
	.cfi_endproc
.LFE19463:
	.size	_ZNK2v88internal26UnoptimizedCompilationInfo5scopeEv, .-_ZNK2v88internal26UnoptimizedCompilationInfo5scopeEv
	.section	.text._ZNK2v88internal26UnoptimizedCompilationInfo14num_parametersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal26UnoptimizedCompilationInfo14num_parametersEv
	.type	_ZNK2v88internal26UnoptimizedCompilationInfo14num_parametersEv, @function
_ZNK2v88internal26UnoptimizedCompilationInfo14num_parametersEv:
.LFB19464:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	40(%rax), %rax
	movl	136(%rax), %eax
	ret
	.cfi_endproc
.LFE19464:
	.size	_ZNK2v88internal26UnoptimizedCompilationInfo14num_parametersEv, .-_ZNK2v88internal26UnoptimizedCompilationInfo14num_parametersEv
	.section	.text._ZNK2v88internal26UnoptimizedCompilationInfo29num_parameters_including_thisEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal26UnoptimizedCompilationInfo29num_parameters_including_thisEv
	.type	_ZNK2v88internal26UnoptimizedCompilationInfo29num_parameters_including_thisEv, @function
_ZNK2v88internal26UnoptimizedCompilationInfo29num_parameters_including_thisEv:
.LFB19465:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	40(%rax), %rax
	movl	136(%rax), %eax
	addl	$1, %eax
	ret
	.cfi_endproc
.LFE19465:
	.size	_ZNK2v88internal26UnoptimizedCompilationInfo29num_parameters_including_thisEv, .-_ZNK2v88internal26UnoptimizedCompilationInfo29num_parameters_including_thisEv
	.section	.text._ZNK2v88internal26UnoptimizedCompilationInfo27SourcePositionRecordingModeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal26UnoptimizedCompilationInfo27SourcePositionRecordingModeEv
	.type	_ZNK2v88internal26UnoptimizedCompilationInfo27SourcePositionRecordingModeEv, @function
_ZNK2v88internal26UnoptimizedCompilationInfo27SourcePositionRecordingModeEv:
.LFB19466:
	.cfi_startproc
	endbr64
	testb	$8, (%rdi)
	jne	.L36
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal15FunctionLiteral21AllowsLazyCompilationEv@PLT
	testb	%al, %al
	je	.L29
	movl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movl	$2, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore 6
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE19466:
	.size	_ZNK2v88internal26UnoptimizedCompilationInfo27SourcePositionRecordingModeEv, .-_ZNK2v88internal26UnoptimizedCompilationInfo27SourcePositionRecordingModeEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal26UnoptimizedCompilationInfoC2EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal26UnoptimizedCompilationInfoC2EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE, @function
_GLOBAL__sub_I__ZN2v88internal26UnoptimizedCompilationInfoC2EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE:
.LFB23828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23828:
	.size	_GLOBAL__sub_I__ZN2v88internal26UnoptimizedCompilationInfoC2EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE, .-_GLOBAL__sub_I__ZN2v88internal26UnoptimizedCompilationInfoC2EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal26UnoptimizedCompilationInfoC2EPNS0_4ZoneEPNS0_9ParseInfoEPNS0_15FunctionLiteralE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
