	.file	"bytecode-decoder.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1527:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1527:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE
	.type	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE:
.LFB17992:
	.cfi_startproc
	endbr64
	salq	$3, %rdx
	leaq	_ZN2v88internal11interpreter9Bytecodes17kOperandKindSizesE(%rip), %rax
	movzbl	%sil, %esi
	andl	$2032, %edx
	addq	%rax, %rdx
	movzbl	(%rdx,%rsi), %eax
	cmpb	$2, %al
	je	.L4
	ja	.L5
	testb	%al, %al
	je	.L14
	movsbl	(%rdi), %edx
	movl	$-5, %eax
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	cmpb	$4, %al
	movl	$-5, %eax
	jne	.L15
	subl	(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movswl	(%rdi), %edx
	movl	$-5, %eax
	subl	%edx, %eax
	ret
.L14:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17992:
	.size	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE, .-_ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE
	.section	.text._ZN2v88internal11interpreter15BytecodeDecoder25DecodeRegisterListOperandEmjNS1_11OperandTypeENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter15BytecodeDecoder25DecodeRegisterListOperandEmjNS1_11OperandTypeENS1_12OperandScaleE
	.type	_ZN2v88internal11interpreter15BytecodeDecoder25DecodeRegisterListOperandEmjNS1_11OperandTypeENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter15BytecodeDecoder25DecodeRegisterListOperandEmjNS1_11OperandTypeENS1_12OperandScaleE:
.LFB17993:
	.cfi_startproc
	endbr64
	salq	$3, %rcx
	movl	%esi, %eax
	leaq	_ZN2v88internal11interpreter9Bytecodes17kOperandKindSizesE(%rip), %rsi
	movzbl	%dl, %edx
	andl	$2032, %ecx
	addq	%rsi, %rcx
	movzbl	(%rcx,%rdx), %edx
	cmpb	$2, %dl
	je	.L17
	ja	.L18
	testb	%dl, %dl
	je	.L27
	movsbl	(%rdi), %ecx
	movl	$-5, %edx
	subl	%ecx, %edx
.L22:
	movq	%rax, %rsi
	movl	%edx, %eax
	salq	$32, %rsi
	orq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	cmpb	$4, %dl
	movl	$-5, %edx
	jne	.L22
	subl	(%rdi), %edx
	movq	%rax, %rsi
	salq	$32, %rsi
	movl	%edx, %eax
	orq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movswl	(%rdi), %ecx
	movl	$-5, %edx
	movq	%rax, %rsi
	salq	$32, %rsi
	subl	%ecx, %edx
	movl	%edx, %eax
	orq	%rsi, %rax
	ret
.L27:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17993:
	.size	_ZN2v88internal11interpreter15BytecodeDecoder25DecodeRegisterListOperandEmjNS1_11OperandTypeENS1_12OperandScaleE, .-_ZN2v88internal11interpreter15BytecodeDecoder25DecodeRegisterListOperandEmjNS1_11OperandTypeENS1_12OperandScaleE
	.section	.text._ZN2v88internal11interpreter15BytecodeDecoder19DecodeSignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter15BytecodeDecoder19DecodeSignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE
	.type	_ZN2v88internal11interpreter15BytecodeDecoder19DecodeSignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter15BytecodeDecoder19DecodeSignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE:
.LFB17994:
	.cfi_startproc
	endbr64
	salq	$3, %rdx
	leaq	_ZN2v88internal11interpreter9Bytecodes17kOperandKindSizesE(%rip), %rax
	movzbl	%sil, %esi
	andl	$2032, %edx
	addq	%rax, %rdx
	movzbl	(%rdx,%rsi), %eax
	cmpb	$2, %al
	je	.L29
	ja	.L30
	testb	%al, %al
	je	.L39
	movsbl	(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	cmpb	$4, %al
	jne	.L40
	movl	(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movswl	(%rdi), %eax
	ret
.L39:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17994:
	.size	_ZN2v88internal11interpreter15BytecodeDecoder19DecodeSignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE, .-_ZN2v88internal11interpreter15BytecodeDecoder19DecodeSignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE
	.section	.text._ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE
	.type	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE, @function
_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE:
.LFB17995:
	.cfi_startproc
	endbr64
	salq	$3, %rdx
	leaq	_ZN2v88internal11interpreter9Bytecodes17kOperandKindSizesE(%rip), %rax
	movzbl	%sil, %esi
	andl	$2032, %edx
	addq	%rax, %rdx
	movzbl	(%rdx,%rsi), %eax
	cmpb	$2, %al
	je	.L42
	ja	.L43
	testb	%al, %al
	je	.L52
	movzbl	(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	cmpb	$4, %al
	jne	.L53
	movl	(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	movzwl	(%rdi), %eax
	ret
.L52:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE17995:
	.size	_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE, .-_ZN2v88internal11interpreter15BytecodeDecoder21DecodeUnsignedOperandEmNS1_11OperandTypeENS1_12OperandScaleE
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1,"aMS",@progbits,1
.LC1:
	.string	"promise_internal_constructor"
.LC2:
	.string	"is_promise"
.LC3:
	.string	"promise_then"
.LC4:
	.string	"embedder_data"
.LC5:
	.string	"global_proxy_object"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"accessor_property_descriptor_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC7:
	.string	"allow_code_gen_from_strings"
.LC8:
	.string	"array_buffer_map"
.LC9:
	.string	"array_buffer_noinit_fun"
.LC10:
	.string	"array_function"
.LC11:
	.string	"array_join_stack"
.LC12:
	.string	"async_from_sync_iterator_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC13:
	.string	"async_function_await_reject_shared_fun"
	.align 8
.LC14:
	.string	"async_function_await_resolve_shared_fun"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC15:
	.string	"async_function_constructor"
.LC16:
	.string	"async_function_object_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC17:
	.string	"async_generator_function_function"
	.align 8
.LC18:
	.string	"async_iterator_value_unwrap_shared_fun"
	.align 8
.LC19:
	.string	"async_generator_await_reject_shared_fun"
	.align 8
.LC20:
	.string	"async_generator_await_resolve_shared_fun"
	.align 8
.LC21:
	.string	"async_generator_yield_resolve_shared_fun"
	.align 8
.LC22:
	.string	"async_generator_return_resolve_shared_fun"
	.align 8
.LC23:
	.string	"async_generator_return_closed_resolve_shared_fun"
	.align 8
.LC24:
	.string	"async_generator_return_closed_reject_shared_fun"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC25:
	.string	"atomics_object"
.LC26:
	.string	"bigint_function"
.LC27:
	.string	"bigint64_array_fun"
.LC28:
	.string	"biguint64_array_fun"
.LC29:
	.string	"boolean_function"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC30:
	.string	"bound_function_with_constructor_map"
	.align 8
.LC31:
	.string	"bound_function_without_constructor_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC32:
	.string	"call_as_constructor_delegate"
.LC33:
	.string	"call_as_function_delegate"
.LC34:
	.string	"callsite_function"
.LC35:
	.string	"context_extension_function"
.LC36:
	.string	"data_property_descriptor_map"
.LC37:
	.string	"data_view_fun"
.LC38:
	.string	"date_function"
.LC39:
	.string	"debug_context_id"
.LC40:
	.string	"empty_function"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC41:
	.string	"error_message_for_code_gen_from_strings"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC42:
	.string	"errors_thrown"
.LC43:
	.string	"extras_binding_object"
.LC44:
	.string	"extras_utils_object"
.LC45:
	.string	"fast_aliased_arguments_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC46:
	.string	"fast_template_instantiations_cache"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC47:
	.string	"float32_array_fun"
.LC48:
	.string	"float64_array_fun"
.LC49:
	.string	"function_function"
.LC50:
	.string	"generator_function_function"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC51:
	.string	"generator_object_prototype_map"
	.align 8
.LC52:
	.string	"async_generator_object_prototype_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC53:
	.string	"initial_array_iterator_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC54:
	.string	"initial_array_iterator_prototype"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC55:
	.string	"initial_array_prototype"
.LC56:
	.string	"initial_error_prototype"
.LC57:
	.string	"initial_generator_prototype"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC58:
	.string	"initial_async_generator_prototype"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC59:
	.string	"initial_iterator_prototype"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC60:
	.string	"initial_map_iterator_prototype"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC61:
	.string	"initial_map_prototype_map"
.LC62:
	.string	"initial_object_prototype"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC63:
	.string	"initial_set_iterator_prototype"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC64:
	.string	"initial_set_prototype"
.LC65:
	.string	"initial_set_prototype_map"
.LC66:
	.string	"initial_string_iterator_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC67:
	.string	"initial_string_iterator_prototype"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC68:
	.string	"initial_string_prototype"
.LC69:
	.string	"initial_weakmap_prototype_map"
.LC70:
	.string	"initial_weakset_prototype_map"
.LC71:
	.string	"int16_array_fun"
.LC72:
	.string	"int32_array_fun"
.LC73:
	.string	"int8_array_fun"
.LC74:
	.string	"intl_collator_function"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC75:
	.string	"intl_date_time_format_function"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC76:
	.string	"intl_number_format_function"
.LC77:
	.string	"intl_locale_function"
.LC78:
	.string	"intl_segment_iterator_map"
.LC79:
	.string	"iterator_result_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC80:
	.string	"js_array_packed_smi_elements_map"
	.align 8
.LC81:
	.string	"js_array_holey_smi_elements_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC82:
	.string	"js_array_packed_elements_map"
.LC83:
	.string	"js_array_holey_elements_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC84:
	.string	"js_array_packed_double_elements_map"
	.align 8
.LC85:
	.string	"js_array_holey_double_elements_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC86:
	.string	"js_map_fun"
.LC87:
	.string	"js_map_map"
.LC88:
	.string	"js_module_namespace_map"
.LC89:
	.string	"js_set_fun"
.LC90:
	.string	"js_set_map"
.LC91:
	.string	"weak_cell_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC92:
	.string	"js_finalization_group_cleanup_iterator_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC93:
	.string	"js_weak_map_fun"
.LC94:
	.string	"js_weak_set_fun"
.LC95:
	.string	"js_weak_ref_fun"
.LC96:
	.string	"js_finalization_group_fun"
.LC97:
	.string	"map_cache"
.LC98:
	.string	"map_key_iterator_map"
.LC99:
	.string	"map_key_value_iterator_map"
.LC100:
	.string	"map_value_iterator_map"
.LC101:
	.string	"math_random_index"
.LC102:
	.string	"math_random_state"
.LC103:
	.string	"math_random_cache"
.LC104:
	.string	"message_listeners"
.LC105:
	.string	"normalized_map_cache"
.LC106:
	.string	"number_function"
.LC107:
	.string	"object_function"
.LC108:
	.string	"object_function_prototype_map"
.LC109:
	.string	"opaque_reference_function"
.LC110:
	.string	"proxy_callable_map"
.LC111:
	.string	"proxy_constructor_map"
.LC112:
	.string	"proxy_function"
.LC113:
	.string	"proxy_map"
.LC114:
	.string	"proxy_revocable_result_map"
.LC115:
	.string	"proxy_revoke_shared_fun"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC116:
	.string	"promise_get_capabilities_executor_shared_fun"
	.align 8
.LC117:
	.string	"promise_capability_default_reject_shared_fun"
	.align 8
.LC118:
	.string	"promise_capability_default_resolve_shared_fun"
	.align 8
.LC119:
	.string	"promise_then_finally_shared_fun"
	.align 8
.LC120:
	.string	"promise_catch_finally_shared_fun"
	.align 8
.LC121:
	.string	"promise_value_thunk_finally_shared_fun"
	.align 8
.LC122:
	.string	"promise_thrower_finally_shared_fun"
	.align 8
.LC123:
	.string	"promise_all_resolve_element_shared_fun"
	.align 8
.LC124:
	.string	"promise_all_settled_resolve_element_shared_fun"
	.align 8
.LC125:
	.string	"promise_all_settled_reject_element_shared_fun"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC126:
	.string	"promise_prototype"
.LC127:
	.string	"regexp_exec_function"
.LC128:
	.string	"regexp_function"
.LC129:
	.string	"regexp_last_match_info"
.LC130:
	.string	"regexp_match_all_function"
.LC131:
	.string	"regexp_match_function"
.LC132:
	.string	"regexp_prototype"
.LC133:
	.string	"regexp_prototype_map"
.LC134:
	.string	"regexp_replace_function"
.LC135:
	.string	"regexp_result_map"
.LC136:
	.string	"regexp_search_function"
.LC137:
	.string	"regexp_split_function"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC138:
	.string	"initial_regexp_string_iterator_prototype_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC139:
	.string	"script_context_table"
.LC140:
	.string	"script_execution_callback"
.LC141:
	.string	"security_token"
.LC142:
	.string	"serialized_objects"
.LC143:
	.string	"set_value_iterator_map"
.LC144:
	.string	"set_key_value_iterator_map"
.LC145:
	.string	"shared_array_buffer_fun"
.LC146:
	.string	"sloppy_arguments_map"
.LC147:
	.string	"slow_aliased_arguments_map"
.LC148:
	.string	"strict_arguments_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC149:
	.string	"slow_object_with_null_prototype_map"
	.align 8
.LC150:
	.string	"slow_object_with_object_prototype_map"
	.align 8
.LC151:
	.string	"slow_template_instantiations_cache"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC152:
	.string	"regexp_species_protector"
.LC153:
	.string	"sloppy_function_map"
.LC154:
	.string	"sloppy_function_with_name_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC155:
	.string	"sloppy_function_without_prototype_map"
	.align 8
.LC156:
	.string	"sloppy_function_with_readonly_prototype_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC157:
	.string	"strict_function_map"
.LC158:
	.string	"strict_function_with_name_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC159:
	.string	"strict_function_with_readonly_prototype_map"
	.align 8
.LC160:
	.string	"strict_function_without_prototype_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC161:
	.string	"method_with_name_map"
.LC162:
	.string	"method_with_home_object_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC163:
	.string	"method_with_name_and_home_object_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC164:
	.string	"async_function_map"
.LC165:
	.string	"async_function_with_name_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC166:
	.string	"async_function_with_home_object_map"
	.align 8
.LC167:
	.string	"async_function_with_name_and_home_object_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC168:
	.string	"generator_function_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC169:
	.string	"generator_function_with_name_map"
	.align 8
.LC170:
	.string	"generator_function_with_home_object_map"
	.align 8
.LC171:
	.string	"generator_function_with_name_and_home_object_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC172:
	.string	"async_generator_function_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.8
	.align 8
.LC173:
	.string	"async_generator_function_with_name_map"
	.align 8
.LC174:
	.string	"async_generator_function_with_home_object_map"
	.align 8
.LC175:
	.string	"async_generator_function_with_name_and_home_object_map"
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi.str1.1
.LC176:
	.string	"class_function_map"
.LC177:
	.string	"string_function"
.LC178:
	.string	"string_function_prototype_map"
.LC179:
	.string	"symbol_function"
.LC180:
	.string	"wasm_exported_function_map"
.LC181:
	.string	"wasm_exception_constructor"
.LC182:
	.string	"wasm_global_constructor"
.LC183:
	.string	"wasm_instance_constructor"
.LC184:
	.string	"wasm_memory_constructor"
.LC185:
	.string	"wasm_module_constructor"
.LC186:
	.string	"wasm_table_constructor"
.LC187:
	.string	"template_weakmap"
.LC188:
	.string	"typed_array_function"
.LC189:
	.string	"typed_array_prototype"
.LC190:
	.string	"uint16_array_fun"
.LC191:
	.string	"uint32_array_fun"
.LC192:
	.string	"uint8_array_fun"
.LC193:
	.string	"uint8_clamped_array_fun"
.LC194:
	.string	"array_entries_iterator"
.LC195:
	.string	"array_for_each_iterator"
.LC196:
	.string	"array_keys_iterator"
.LC197:
	.string	"array_values_iterator"
.LC198:
	.string	"error_function"
.LC199:
	.string	"error_to_string"
.LC200:
	.string	"eval_error_function"
.LC201:
	.string	"global_eval_fun"
.LC202:
	.string	"global_proxy_function"
.LC203:
	.string	"map_delete"
.LC204:
	.string	"map_get"
.LC205:
	.string	"map_has"
.LC206:
	.string	"map_set"
.LC207:
	.string	"function_has_instance"
.LC208:
	.string	"object_to_string"
.LC209:
	.string	"promise_all"
.LC210:
	.string	"promise_catch"
.LC211:
	.string	"promise_function"
.LC212:
	.string	"range_error_function"
.LC213:
	.string	"reference_error_function"
.LC214:
	.string	"set_add"
.LC215:
	.string	"set_delete"
.LC216:
	.string	"set_has"
.LC217:
	.string	"syntax_error_function"
.LC218:
	.string	"type_error_function"
.LC219:
	.string	"uri_error_function"
.LC220:
	.string	"wasm_compile_error_function"
.LC221:
	.string	"wasm_link_error_function"
.LC222:
	.string	"wasm_runtime_error_function"
.LC223:
	.string	"weakmap_set"
.LC224:
	.string	"weakmap_get"
.LC225:
	.string	"weakset_add"
.LC226:
	.string	"generator_next_internal"
.LC227:
	.string	"make_error"
.LC228:
	.string	"make_range_error"
.LC229:
	.string	"make_syntax_error"
.LC230:
	.string	"make_type_error"
.LC231:
	.string	"make_uri_error"
.LC232:
	.string	"object_create"
.LC233:
	.string	"reflect_apply"
.LC234:
	.string	"reflect_construct"
.LC235:
	.string	"math_floor"
.LC236:
	.string	"math_pow"
.LC237:
	.string	"array_buffer_fun"
.LC238:
	.string	"."
.LC239:
	.string	" "
.LC240:
	.string	"   "
.LC241:
	.string	"["
.LC242:
	.string	"]"
.LC243:
	.string	"#"
.LC244:
	.string	"-"
.LC245:
	.string	", "
	.section	.text._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi
	.type	_ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi, @function
_ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi:
.LFB17998:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$440, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -416(%rbp)
	movzbl	(%rsi), %r15d
	movl	%edx, -424(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$3, %r15b
	ja	.L398
	cmpb	$2, %r15b
	je	.L401
	cmpb	$3, %r15b
	je	.L402
	testb	%r15b, %r15b
	je	.L401
	cmpb	$1, %r15b
	jne	.L57
.L402:
	movb	$4, -401(%rbp)
	movq	%rsi, %rax
.L56:
	movl	$1, -408(%rbp)
	movzbl	1(%rax), %r15d
.L55:
	leaq	-320(%rbp), %rbx
	movq	%rbx, %rdi
	movq	%rbx, -464(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE7copyfmtERKS2_@PLT
	movq	(%r12), %rax
	movq	-24(%rax), %rbx
	addq	%r12, %rbx
	cmpb	$0, 225(%rbx)
	je	.L469
.L58:
	movb	$48, 224(%rbx)
	movq	(%r12), %rax
	movzbl	%r15b, %ecx
	leaq	_ZN2v88internal11interpreter9Bytecodes14kBytecodeSizesE(%rip), %rdx
	movl	-408(%rbp), %ebx
	movq	%rcx, -432(%rbp)
	movq	-24(%rax), %rax
	movl	$8, 24(%r12,%rax)
	movzbl	-401(%rbp), %eax
	sarl	%eax
	cltq
	movq	%rax, -440(%rbp)
	imulq	$183, %rax, %rax
	addq	%rcx, %rax
	addl	(%rdx,%rax,4), %ebx
	testl	%ebx, %ebx
	jle	.L470
	movq	-416(%rbp), %rcx
	leal	-1(%rbx), %eax
	leaq	-388(%rbp), %r13
	movl	%ebx, -420(%rbp)
	movq	%r13, %rbx
	leaq	1(%rcx,%rax), %rax
	movq	%rcx, %r14
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L68:
	movq	(%r12), %rax
	movq	%r12, %rdi
	addq	$1, %r14
	movq	-24(%rax), %rax
	movq	$2, 16(%r12,%rax)
	movzbl	-1(%r14), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	movq	%rbx, %rsi
	movb	$32, -388(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	%r14, %r13
	jne	.L68
	movq	(%r12), %rax
	movl	-420(%rbp), %ebx
	movq	-464(%rbp), %rsi
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE7copyfmtERKS2_@PLT
	cmpl	$5, %ebx
	jg	.L65
.L66:
	leaq	.LC240(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	$6, %ebx
	jne	.L71
.L65:
	leaq	-352(%rbp), %rax
	movzbl	-401(%rbp), %edx
	leaq	.LC238(%rip), %rcx
	movl	%r15d, %esi
	movq	%rax, %rdi
	movq	%rax, -448(%rbp)
	call	_ZN2v88internal11interpreter9Bytecodes8ToStringB5cxx11ENS1_8BytecodeENS1_12OperandScaleEPKc@PLT
	movq	-344(%rbp), %rdx
	movq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC239(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rdi
	leaq	-336(%rbp), %rax
	movq	%rax, -456(%rbp)
	cmpq	%rax, %rdi
	je	.L70
	call	_ZdlPv@PLT
.L70:
	movl	%r15d, %edi
	call	_ZN2v88internal11interpreter9Bytecodes12IsDebugBreakENS1_8BytecodeE@PLT
	testb	%al, %al
	jne	.L73
	movq	-432(%rbp), %rcx
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandCountE(%rip), %rax
	movl	(%rax,%rcx,4), %eax
	movl	%eax, -432(%rbp)
	testl	%eax, %eax
	jle	.L73
	movq	-440(%rbp), %rdx
	leaq	_ZN2v88internal11interpreter9Bytecodes17kOperandKindSizesE(%rip), %rax
	movb	%r15b, -420(%rbp)
	xorl	%ebx, %ebx
	leaq	_ZN2v88internal11interpreter9Bytecodes13kOperandTypesE(%rip), %r14
	movq	%rcx, %r15
	salq	$4, %rdx
	addq	%rdx, %rax
	movq	%rax, -440(%rbp)
	.p2align 4,,10
	.p2align 3
.L390:
	movq	(%r14,%r15,8), %rdx
	movslq	%ebx, %rax
	movzbl	-420(%rbp), %edi
	movl	%ebx, %esi
	movzbl	(%rdx,%rax), %r13d
	movzbl	-401(%rbp), %edx
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	-416(%rbp), %rcx
	addl	-408(%rbp), %eax
	cltq
	leaq	(%rcx,%rax), %r8
	cmpb	$15, %r13b
	ja	.L74
	leaq	.L76(%rip), %rdx
	movzbl	%r13b, %ecx
	movslq	(%rdx,%rcx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi,"a",@progbits
	.align 4
	.align 4
.L76:
	.long	.L57-.L76
	.long	.L85-.L76
	.long	.L84-.L76
	.long	.L83-.L76
	.long	.L82-.L76
	.long	.L81-.L76
	.long	.L81-.L76
	.long	.L57-.L76
	.long	.L80-.L76
	.long	.L79-.L76
	.long	.L78-.L76
	.long	.L77-.L76
	.long	.L79-.L76
	.long	.L78-.L76
	.long	.L77-.L76
	.long	.L75-.L76
	.section	.text._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi
	.p2align 4,,10
	.p2align 3
.L77:
	movq	-440(%rbp), %rax
	movzbl	(%rax,%rcx), %eax
	cmpb	$2, %al
	je	.L367
	ja	.L368
	testb	%al, %al
	je	.L57
	movsbl	(%r8), %edx
	movl	$-4, %r13d
	movl	$-5, %eax
	subl	%edx, %r13d
	subl	%edx, %eax
	.p2align 4,,10
	.p2align 3
.L371:
	movl	-424(%rbp), %edx
	leaq	-384(%rbp), %rdi
	leaq	-392(%rbp), %rsi
	movl	%eax, -392(%rbp)
	call	_ZNK2v88internal11interpreter8Register8ToStringB5cxx11Ei@PLT
	movq	-376(%rbp), %rdx
	movq	-384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC244(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -472(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L466:
	movl	-424(%rbp), %edx
	movq	-448(%rbp), %rdi
	leaq	-388(%rbp), %rsi
	movl	%r13d, -388(%rbp)
	call	_ZNK2v88internal11interpreter8Register8ToStringB5cxx11Ei@PLT
	movq	-472(%rbp), %r8
	movq	-344(%rbp), %rdx
	movq	-352(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rdi
	cmpq	-456(%rbp), %rdi
	je	.L386
	call	_ZdlPv@PLT
.L386:
	movq	-384(%rbp), %rdi
	leaq	-368(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L74
.L459:
	call	_ZdlPv@PLT
.L74:
	movl	-432(%rbp), %r13d
	leal	-1(%r13), %eax
	cmpl	%ebx, %eax
	jne	.L471
.L73:
	movq	-464(%rbp), %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L472
	addq	$440, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movzbl	-401(%rbp), %edx
	addl	$1, %ebx
	movzbl	-420(%rbp), %edi
	movq	%rcx, -480(%rbp)
	movl	%ebx, %esi
	movq	%r8, -472(%rbp)
	call	_ZN2v88internal11interpreter9Bytecodes16GetOperandOffsetENS1_8BytecodeEiNS1_12OperandScaleE@PLT
	movq	-440(%rbp), %rcx
	addl	-408(%rbp), %eax
	cltq
	addq	-416(%rbp), %rax
	movq	-472(%rbp), %r8
	movzbl	7(%rcx), %edx
	movq	-480(%rbp), %rcx
	cmpb	$2, %dl
	je	.L374
	ja	.L375
	testb	%dl, %dl
	je	.L57
	movzbl	(%rax), %edx
.L378:
	movq	-440(%rbp), %rax
	movzbl	(%rax,%rcx), %eax
	cmpb	$2, %al
	je	.L379
.L477:
	ja	.L380
	testb	%al, %al
	je	.L57
	movsbl	(%r8), %eax
	movl	$-5, %ecx
	subl	%eax, %ecx
.L383:
	movl	%edx, %r13d
	testl	%edx, %edx
	jne	.L473
.L384:
	movl	-424(%rbp), %edx
	leaq	-384(%rbp), %rdi
	leaq	-392(%rbp), %rsi
	movl	$0, -392(%rbp)
	call	_ZNK2v88internal11interpreter8Register8ToStringB5cxx11Ei@PLT
	movq	-376(%rbp), %rdx
	movq	-384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC244(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -472(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-472(%rbp), %r8
.L396:
	movq	%r8, -472(%rbp)
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L79:
	movq	-440(%rbp), %rax
	movzbl	(%rax,%rcx), %eax
	cmpb	$2, %al
	je	.L354
	ja	.L355
	testb	%al, %al
	je	.L57
	movsbl	(%r8), %edx
	movl	$-5, %eax
	subl	%edx, %eax
.L358:
	movl	-424(%rbp), %edx
	movq	-448(%rbp), %rdi
	leaq	-388(%rbp), %rsi
	movl	%eax, -388(%rbp)
	call	_ZNK2v88internal11interpreter8Register8ToStringB5cxx11Ei@PLT
	movq	-344(%rbp), %rdx
	movq	-352(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rdi
	cmpq	-456(%rbp), %rdi
	jne	.L459
	movl	-432(%rbp), %r13d
	leal	-1(%r13), %eax
	cmpl	%ebx, %eax
	je	.L73
	.p2align 4,,10
	.p2align 3
.L471:
	movl	$2, %edx
	leaq	.LC245(%rip), %rsi
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, %ebx
	jl	.L390
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$1, %edx
	leaq	.LC241(%rip), %rsi
	movq	%r12, %rdi
	movq	%rcx, -480(%rbp)
	movq	%r8, -472(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-440(%rbp), %rax
	movq	-480(%rbp), %rcx
	movq	-472(%rbp), %r8
	movzbl	(%rax,%rcx), %eax
	cmpb	$2, %al
	je	.L86
	ja	.L87
	testb	%al, %al
	je	.L57
	movzbl	(%r8), %esi
.L90:
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, %edx
	leaq	.LC242(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$1, %edx
	leaq	.LC241(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -472(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-440(%rbp), %rax
	movq	-472(%rbp), %r8
	movzbl	8(%rax), %eax
	cmpb	$2, %al
	je	.L344
	ja	.L345
	testb	%al, %al
	je	.L57
	movsbl	(%r8), %esi
.L348:
	movq	%r12, %rdi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC242(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L82:
	movq	-440(%rbp), %rax
	movzbl	4(%rax), %eax
	cmpb	$2, %al
	je	.L98
	ja	.L99
	testb	%al, %al
	je	.L57
	movzbl	(%r8), %r13d
.L103:
	movl	$1, %edx
	leaq	.LC241(%rip), %rsi
	movq	%r12, %rdi
	subl	$4, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	$236, %r13d
	ja	.L57
	leaq	.L392(%rip), %rdx
	movslq	(%rdx,%r13,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi
	.align 4
	.align 4
.L392:
	.long	.L336-.L392
	.long	.L391-.L392
	.long	.L395-.L392
	.long	.L394-.L392
	.long	.L393-.L392
	.long	.L403-.L392
	.long	.L104-.L392
	.long	.L106-.L392
	.long	.L107-.L392
	.long	.L108-.L392
	.long	.L109-.L392
	.long	.L110-.L392
	.long	.L111-.L392
	.long	.L112-.L392
	.long	.L113-.L392
	.long	.L114-.L392
	.long	.L115-.L392
	.long	.L116-.L392
	.long	.L117-.L392
	.long	.L118-.L392
	.long	.L119-.L392
	.long	.L120-.L392
	.long	.L121-.L392
	.long	.L122-.L392
	.long	.L123-.L392
	.long	.L124-.L392
	.long	.L125-.L392
	.long	.L126-.L392
	.long	.L127-.L392
	.long	.L128-.L392
	.long	.L129-.L392
	.long	.L130-.L392
	.long	.L131-.L392
	.long	.L132-.L392
	.long	.L133-.L392
	.long	.L134-.L392
	.long	.L135-.L392
	.long	.L136-.L392
	.long	.L137-.L392
	.long	.L138-.L392
	.long	.L139-.L392
	.long	.L140-.L392
	.long	.L141-.L392
	.long	.L142-.L392
	.long	.L143-.L392
	.long	.L144-.L392
	.long	.L145-.L392
	.long	.L146-.L392
	.long	.L147-.L392
	.long	.L148-.L392
	.long	.L149-.L392
	.long	.L150-.L392
	.long	.L151-.L392
	.long	.L152-.L392
	.long	.L153-.L392
	.long	.L154-.L392
	.long	.L155-.L392
	.long	.L156-.L392
	.long	.L157-.L392
	.long	.L158-.L392
	.long	.L159-.L392
	.long	.L160-.L392
	.long	.L161-.L392
	.long	.L162-.L392
	.long	.L163-.L392
	.long	.L164-.L392
	.long	.L165-.L392
	.long	.L166-.L392
	.long	.L167-.L392
	.long	.L168-.L392
	.long	.L169-.L392
	.long	.L170-.L392
	.long	.L171-.L392
	.long	.L172-.L392
	.long	.L173-.L392
	.long	.L174-.L392
	.long	.L175-.L392
	.long	.L176-.L392
	.long	.L177-.L392
	.long	.L178-.L392
	.long	.L179-.L392
	.long	.L180-.L392
	.long	.L181-.L392
	.long	.L182-.L392
	.long	.L183-.L392
	.long	.L184-.L392
	.long	.L185-.L392
	.long	.L186-.L392
	.long	.L187-.L392
	.long	.L188-.L392
	.long	.L189-.L392
	.long	.L190-.L392
	.long	.L191-.L392
	.long	.L192-.L392
	.long	.L193-.L392
	.long	.L194-.L392
	.long	.L195-.L392
	.long	.L196-.L392
	.long	.L197-.L392
	.long	.L198-.L392
	.long	.L199-.L392
	.long	.L200-.L392
	.long	.L201-.L392
	.long	.L202-.L392
	.long	.L203-.L392
	.long	.L204-.L392
	.long	.L205-.L392
	.long	.L206-.L392
	.long	.L207-.L392
	.long	.L208-.L392
	.long	.L209-.L392
	.long	.L210-.L392
	.long	.L211-.L392
	.long	.L212-.L392
	.long	.L213-.L392
	.long	.L214-.L392
	.long	.L215-.L392
	.long	.L216-.L392
	.long	.L217-.L392
	.long	.L218-.L392
	.long	.L219-.L392
	.long	.L220-.L392
	.long	.L221-.L392
	.long	.L222-.L392
	.long	.L223-.L392
	.long	.L224-.L392
	.long	.L225-.L392
	.long	.L226-.L392
	.long	.L227-.L392
	.long	.L228-.L392
	.long	.L229-.L392
	.long	.L230-.L392
	.long	.L231-.L392
	.long	.L232-.L392
	.long	.L233-.L392
	.long	.L234-.L392
	.long	.L235-.L392
	.long	.L236-.L392
	.long	.L237-.L392
	.long	.L238-.L392
	.long	.L239-.L392
	.long	.L240-.L392
	.long	.L241-.L392
	.long	.L242-.L392
	.long	.L243-.L392
	.long	.L244-.L392
	.long	.L245-.L392
	.long	.L246-.L392
	.long	.L247-.L392
	.long	.L248-.L392
	.long	.L249-.L392
	.long	.L250-.L392
	.long	.L251-.L392
	.long	.L252-.L392
	.long	.L253-.L392
	.long	.L254-.L392
	.long	.L255-.L392
	.long	.L256-.L392
	.long	.L257-.L392
	.long	.L258-.L392
	.long	.L259-.L392
	.long	.L260-.L392
	.long	.L261-.L392
	.long	.L262-.L392
	.long	.L263-.L392
	.long	.L264-.L392
	.long	.L265-.L392
	.long	.L266-.L392
	.long	.L267-.L392
	.long	.L268-.L392
	.long	.L269-.L392
	.long	.L270-.L392
	.long	.L271-.L392
	.long	.L272-.L392
	.long	.L273-.L392
	.long	.L274-.L392
	.long	.L275-.L392
	.long	.L276-.L392
	.long	.L277-.L392
	.long	.L278-.L392
	.long	.L279-.L392
	.long	.L280-.L392
	.long	.L281-.L392
	.long	.L282-.L392
	.long	.L283-.L392
	.long	.L284-.L392
	.long	.L285-.L392
	.long	.L286-.L392
	.long	.L287-.L392
	.long	.L288-.L392
	.long	.L289-.L392
	.long	.L290-.L392
	.long	.L291-.L392
	.long	.L292-.L392
	.long	.L293-.L392
	.long	.L294-.L392
	.long	.L295-.L392
	.long	.L296-.L392
	.long	.L297-.L392
	.long	.L298-.L392
	.long	.L299-.L392
	.long	.L300-.L392
	.long	.L301-.L392
	.long	.L302-.L392
	.long	.L303-.L392
	.long	.L304-.L392
	.long	.L305-.L392
	.long	.L306-.L392
	.long	.L307-.L392
	.long	.L308-.L392
	.long	.L309-.L392
	.long	.L310-.L392
	.long	.L311-.L392
	.long	.L312-.L392
	.long	.L313-.L392
	.long	.L314-.L392
	.long	.L315-.L392
	.long	.L316-.L392
	.long	.L317-.L392
	.long	.L318-.L392
	.long	.L319-.L392
	.long	.L320-.L392
	.long	.L321-.L392
	.long	.L322-.L392
	.long	.L323-.L392
	.long	.L324-.L392
	.long	.L325-.L392
	.long	.L326-.L392
	.long	.L327-.L392
	.long	.L328-.L392
	.long	.L329-.L392
	.long	.L330-.L392
	.long	.L331-.L392
	.long	.L332-.L392
	.long	.L333-.L392
	.long	.L334-.L392
	.long	.L335-.L392
	.section	.text._ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi
	.p2align 4,,10
	.p2align 3
.L83:
	movl	$1, %edx
	leaq	.LC241(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -472(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-440(%rbp), %rax
	movq	-472(%rbp), %r8
	movzbl	3(%rax), %eax
	cmpb	$2, %al
	je	.L337
	ja	.L338
	testb	%al, %al
	je	.L57
	movzbl	(%r8), %edi
.L341:
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	movq	8(%rax), %r13
	testq	%r13, %r13
	je	.L461
.L342:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%rax, %rdx
.L460:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L343:
	movl	$1, %edx
	leaq	.LC242(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L75:
	movq	-440(%rbp), %rax
	movzbl	15(%rax), %eax
	cmpb	$2, %al
	je	.L360
	ja	.L361
	testb	%al, %al
	je	.L57
	movsbl	(%r8), %edx
	movl	$-3, %r13d
	movl	$-5, %eax
	subl	%edx, %r13d
	subl	%edx, %eax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L84:
	movq	-440(%rbp), %rax
	movzbl	2(%rax), %eax
	cmpb	$2, %al
	je	.L91
	ja	.L92
	testb	%al, %al
	je	.L57
	movzbl	(%r8), %r13d
.L95:
	movl	$1, %edx
	leaq	.LC241(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	%r13d, %edi
	call	_ZN2v88internal11interpreter16IntrinsicsHelper11ToRuntimeIdENS2_11IntrinsicIdE@PLT
	movl	%eax, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	movq	8(%rax), %r13
	testq	%r13, %r13
	je	.L461
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%rax, %rdx
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$1, %edx
	leaq	.LC243(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -472(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-440(%rbp), %rax
	movq	-472(%rbp), %r8
	movzbl	1(%rax), %eax
	cmpb	$2, %al
	je	.L349
	ja	.L350
	testb	%al, %al
	je	.L57
	movzbl	(%r8), %esi
.L353:
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L401:
	movb	$2, -401(%rbp)
	movq	%rsi, %rax
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L361:
	cmpb	$4, %al
	jne	.L474
	movl	(%r8), %edx
	movl	$-3, %r13d
	movl	$-5, %eax
	subl	%edx, %r13d
	subl	%edx, %eax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L338:
	xorl	%edi, %edi
	cmpb	$4, %al
	jne	.L341
	movl	(%r8), %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	movq	8(%rax), %r13
	testq	%r13, %r13
	jne	.L342
	.p2align 4,,10
	.p2align 3
.L461:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L350:
	xorl	%esi, %esi
	cmpb	$4, %al
	jne	.L353
	movl	(%r8), %esi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L345:
	xorl	%esi, %esi
	cmpb	$4, %al
	jne	.L348
	movl	(%r8), %esi
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L92:
	xorl	%r13d, %r13d
	cmpb	$4, %al
	jne	.L95
	movl	(%r8), %r13d
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L99:
	cmpb	$4, %al
	jne	.L475
	movl	(%r8), %r13d
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L87:
	xorl	%esi, %esi
	cmpb	$4, %al
	jne	.L90
	movl	(%r8), %esi
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L375:
	cmpb	$4, %dl
	jne	.L476
	movl	(%rax), %edx
	movq	-440(%rbp), %rax
	movzbl	(%rax,%rcx), %eax
	cmpb	$2, %al
	jne	.L477
.L379:
	movswl	(%r8), %eax
	movl	$-5, %ecx
	movl	%edx, %r13d
	subl	%eax, %ecx
	testl	%edx, %edx
	je	.L384
.L473:
	movl	-424(%rbp), %edx
	leaq	-384(%rbp), %rdi
	leaq	-392(%rbp), %rsi
	movl	%ecx, -392(%rbp)
	movl	%ecx, -480(%rbp)
	call	_ZNK2v88internal11interpreter8Register8ToStringB5cxx11Ei@PLT
	movq	-376(%rbp), %rdx
	movq	-384(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	leaq	.LC244(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -472(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-480(%rbp), %ecx
	movq	-472(%rbp), %r8
	leal	-1(%r13,%rcx), %r13d
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L368:
	cmpb	$4, %al
	jne	.L478
	movl	(%r8), %edx
	movl	$-4, %r13d
	movl	$-5, %eax
	subl	%edx, %r13d
	subl	%edx, %eax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L355:
	cmpb	$4, %al
	movl	$-5, %eax
	jne	.L358
	subl	(%r8), %eax
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L380:
	movl	$-5, %ecx
	cmpb	$4, %al
	jne	.L383
	subl	(%r8), %ecx
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L474:
	movl	$-5, %eax
	movl	$-3, %r13d
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L478:
	movl	$-5, %eax
	movl	$-4, %r13d
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L476:
	xorl	%edx, %edx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L398:
	movb	$1, -401(%rbp)
	movl	$0, -408(%rbp)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L469:
	movq	240(%rbx), %r13
	testq	%r13, %r13
	je	.L479
	cmpb	$0, 56(%r13)
	je	.L480
.L61:
	movb	$1, 225(%rbx)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L349:
	movzwl	(%r8), %esi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L91:
	movzwl	(%r8), %r13d
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L360:
	movswl	(%r8), %edx
	movl	$-3, %r13d
	movl	$-5, %eax
	subl	%edx, %r13d
	subl	%edx, %eax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L337:
	movzwl	(%r8), %edi
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L475:
	movl	$1, %edx
	leaq	.LC241(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L374:
	movzwl	(%rax), %edx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L367:
	movswl	(%r8), %edx
	movl	$-4, %r13d
	movl	$-5, %eax
	subl	%edx, %r13d
	subl	%edx, %eax
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L98:
	movzwl	(%r8), %r13d
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L344:
	movswl	(%r8), %esi
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L86:
	movzwl	(%r8), %esi
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L354:
	movswl	(%r8), %edx
	movl	$-5, %eax
	subl	%edx, %eax
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L61
	movl	$32, %esi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L61
.L470:
	movq	(%r12), %rax
	movq	-464(%rbp), %rsi
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE7copyfmtERKS2_@PLT
	jmp	.L66
.L334:
	movl	$10, %edx
	leaq	.LC2(%rip), %rsi
	jmp	.L460
.L333:
	movl	$28, %edx
	leaq	.LC1(%rip), %rsi
	jmp	.L460
.L332:
	movl	$8, %edx
	leaq	.LC236(%rip), %rsi
	jmp	.L460
.L331:
	movl	$10, %edx
	leaq	.LC235(%rip), %rsi
	jmp	.L460
.L330:
	movl	$17, %edx
	leaq	.LC234(%rip), %rsi
	jmp	.L460
.L329:
	movl	$13, %edx
	leaq	.LC233(%rip), %rsi
	jmp	.L460
.L328:
	movl	$13, %edx
	leaq	.LC232(%rip), %rsi
	jmp	.L460
.L327:
	movl	$14, %edx
	leaq	.LC231(%rip), %rsi
	jmp	.L460
.L326:
	movl	$15, %edx
	leaq	.LC230(%rip), %rsi
	jmp	.L460
.L325:
	movl	$17, %edx
	leaq	.LC229(%rip), %rsi
	jmp	.L460
.L324:
	movl	$16, %edx
	leaq	.LC228(%rip), %rsi
	jmp	.L460
.L323:
	movl	$10, %edx
	leaq	.LC227(%rip), %rsi
	jmp	.L460
.L322:
	movl	$23, %edx
	leaq	.LC226(%rip), %rsi
	jmp	.L460
.L321:
	movl	$11, %edx
	leaq	.LC225(%rip), %rsi
	jmp	.L460
.L320:
	movl	$11, %edx
	leaq	.LC224(%rip), %rsi
	jmp	.L460
.L319:
	movl	$11, %edx
	leaq	.LC223(%rip), %rsi
	jmp	.L460
.L318:
	movl	$27, %edx
	leaq	.LC222(%rip), %rsi
	jmp	.L460
.L317:
	movl	$24, %edx
	leaq	.LC221(%rip), %rsi
	jmp	.L460
.L316:
	movl	$27, %edx
	leaq	.LC220(%rip), %rsi
	jmp	.L460
.L315:
	movl	$18, %edx
	leaq	.LC219(%rip), %rsi
	jmp	.L460
.L314:
	movl	$19, %edx
	leaq	.LC218(%rip), %rsi
	jmp	.L460
.L313:
	movl	$21, %edx
	leaq	.LC217(%rip), %rsi
	jmp	.L460
.L312:
	movl	$7, %edx
	leaq	.LC216(%rip), %rsi
	jmp	.L460
.L311:
	movl	$10, %edx
	leaq	.LC215(%rip), %rsi
	jmp	.L460
.L310:
	movl	$7, %edx
	leaq	.LC214(%rip), %rsi
	jmp	.L460
.L309:
	movl	$24, %edx
	leaq	.LC213(%rip), %rsi
	jmp	.L460
.L308:
	movl	$20, %edx
	leaq	.LC212(%rip), %rsi
	jmp	.L460
.L307:
	movl	$16, %edx
	leaq	.LC211(%rip), %rsi
	jmp	.L460
.L306:
	movl	$13, %edx
	leaq	.LC210(%rip), %rsi
	jmp	.L460
.L305:
	movl	$11, %edx
	leaq	.LC209(%rip), %rsi
	jmp	.L460
.L304:
	movl	$16, %edx
	leaq	.LC208(%rip), %rsi
	jmp	.L460
.L303:
	movl	$21, %edx
	leaq	.LC207(%rip), %rsi
	jmp	.L460
.L302:
	movl	$7, %edx
	leaq	.LC206(%rip), %rsi
	jmp	.L460
.L301:
	movl	$7, %edx
	leaq	.LC205(%rip), %rsi
	jmp	.L460
.L300:
	movl	$7, %edx
	leaq	.LC204(%rip), %rsi
	jmp	.L460
.L299:
	movl	$10, %edx
	leaq	.LC203(%rip), %rsi
	jmp	.L460
.L298:
	movl	$21, %edx
	leaq	.LC202(%rip), %rsi
	jmp	.L460
.L297:
	movl	$15, %edx
	leaq	.LC201(%rip), %rsi
	jmp	.L460
.L296:
	movl	$19, %edx
	leaq	.LC200(%rip), %rsi
	jmp	.L460
.L295:
	movl	$15, %edx
	leaq	.LC199(%rip), %rsi
	jmp	.L460
.L294:
	movl	$14, %edx
	leaq	.LC198(%rip), %rsi
	jmp	.L460
.L293:
	movl	$21, %edx
	leaq	.LC197(%rip), %rsi
	jmp	.L460
.L292:
	movl	$19, %edx
	leaq	.LC196(%rip), %rsi
	jmp	.L460
.L291:
	movl	$23, %edx
	leaq	.LC195(%rip), %rsi
	jmp	.L460
.L290:
	movl	$22, %edx
	leaq	.LC194(%rip), %rsi
	jmp	.L460
.L289:
	movl	$23, %edx
	leaq	.LC193(%rip), %rsi
	jmp	.L460
.L288:
	movl	$15, %edx
	leaq	.LC192(%rip), %rsi
	jmp	.L460
.L287:
	movl	$16, %edx
	leaq	.LC191(%rip), %rsi
	jmp	.L460
.L286:
	movl	$16, %edx
	leaq	.LC190(%rip), %rsi
	jmp	.L460
.L285:
	movl	$21, %edx
	leaq	.LC189(%rip), %rsi
	jmp	.L460
.L284:
	movl	$20, %edx
	leaq	.LC188(%rip), %rsi
	jmp	.L460
.L283:
	movl	$16, %edx
	leaq	.LC187(%rip), %rsi
	jmp	.L460
.L282:
	movl	$22, %edx
	leaq	.LC186(%rip), %rsi
	jmp	.L460
.L281:
	movl	$23, %edx
	leaq	.LC185(%rip), %rsi
	jmp	.L460
.L280:
	movl	$23, %edx
	leaq	.LC184(%rip), %rsi
	jmp	.L460
.L279:
	movl	$25, %edx
	leaq	.LC183(%rip), %rsi
	jmp	.L460
.L278:
	movl	$23, %edx
	leaq	.LC182(%rip), %rsi
	jmp	.L460
.L277:
	movl	$26, %edx
	leaq	.LC181(%rip), %rsi
	jmp	.L460
.L276:
	movl	$26, %edx
	leaq	.LC180(%rip), %rsi
	jmp	.L460
.L275:
	movl	$15, %edx
	leaq	.LC179(%rip), %rsi
	jmp	.L460
.L274:
	movl	$29, %edx
	leaq	.LC178(%rip), %rsi
	jmp	.L460
.L273:
	movl	$15, %edx
	leaq	.LC177(%rip), %rsi
	jmp	.L460
.L272:
	movl	$18, %edx
	leaq	.LC176(%rip), %rsi
	jmp	.L460
.L271:
	movl	$54, %edx
	leaq	.LC175(%rip), %rsi
	jmp	.L460
.L270:
	movl	$45, %edx
	leaq	.LC174(%rip), %rsi
	jmp	.L460
.L269:
	movl	$38, %edx
	leaq	.LC173(%rip), %rsi
	jmp	.L460
.L268:
	movl	$28, %edx
	leaq	.LC172(%rip), %rsi
	jmp	.L460
.L267:
	movl	$48, %edx
	leaq	.LC171(%rip), %rsi
	jmp	.L460
.L266:
	movl	$39, %edx
	leaq	.LC170(%rip), %rsi
	jmp	.L460
.L265:
	movl	$32, %edx
	leaq	.LC169(%rip), %rsi
	jmp	.L460
.L264:
	movl	$22, %edx
	leaq	.LC168(%rip), %rsi
	jmp	.L460
.L263:
	movl	$44, %edx
	leaq	.LC167(%rip), %rsi
	jmp	.L460
.L262:
	movl	$35, %edx
	leaq	.LC166(%rip), %rsi
	jmp	.L460
.L261:
	movl	$28, %edx
	leaq	.LC165(%rip), %rsi
	jmp	.L460
.L260:
	movl	$18, %edx
	leaq	.LC164(%rip), %rsi
	jmp	.L460
.L259:
	movl	$36, %edx
	leaq	.LC163(%rip), %rsi
	jmp	.L460
.L258:
	movl	$27, %edx
	leaq	.LC162(%rip), %rsi
	jmp	.L460
.L257:
	movl	$20, %edx
	leaq	.LC161(%rip), %rsi
	jmp	.L460
.L256:
	movl	$37, %edx
	leaq	.LC160(%rip), %rsi
	jmp	.L460
.L255:
	movl	$43, %edx
	leaq	.LC159(%rip), %rsi
	jmp	.L460
.L254:
	movl	$29, %edx
	leaq	.LC158(%rip), %rsi
	jmp	.L460
.L253:
	movl	$19, %edx
	leaq	.LC157(%rip), %rsi
	jmp	.L460
.L252:
	movl	$43, %edx
	leaq	.LC156(%rip), %rsi
	jmp	.L460
.L251:
	movl	$37, %edx
	leaq	.LC155(%rip), %rsi
	jmp	.L460
.L250:
	movl	$29, %edx
	leaq	.LC154(%rip), %rsi
	jmp	.L460
.L249:
	movl	$19, %edx
	leaq	.LC153(%rip), %rsi
	jmp	.L460
.L248:
	movl	$24, %edx
	leaq	.LC152(%rip), %rsi
	jmp	.L460
.L247:
	movl	$34, %edx
	leaq	.LC151(%rip), %rsi
	jmp	.L460
.L246:
	movl	$37, %edx
	leaq	.LC150(%rip), %rsi
	jmp	.L460
.L245:
	movl	$35, %edx
	leaq	.LC149(%rip), %rsi
	jmp	.L460
.L244:
	movl	$20, %edx
	leaq	.LC148(%rip), %rsi
	jmp	.L460
.L243:
	movl	$26, %edx
	leaq	.LC147(%rip), %rsi
	jmp	.L460
.L242:
	movl	$20, %edx
	leaq	.LC146(%rip), %rsi
	jmp	.L460
.L241:
	movl	$23, %edx
	leaq	.LC145(%rip), %rsi
	jmp	.L460
.L240:
	movl	$26, %edx
	leaq	.LC144(%rip), %rsi
	jmp	.L460
.L239:
	movl	$22, %edx
	leaq	.LC143(%rip), %rsi
	jmp	.L460
.L238:
	movl	$18, %edx
	leaq	.LC142(%rip), %rsi
	jmp	.L460
.L237:
	movl	$14, %edx
	leaq	.LC141(%rip), %rsi
	jmp	.L460
.L236:
	movl	$25, %edx
	leaq	.LC140(%rip), %rsi
	jmp	.L460
.L235:
	movl	$20, %edx
	leaq	.LC139(%rip), %rsi
	jmp	.L460
.L234:
	movl	$44, %edx
	leaq	.LC138(%rip), %rsi
	jmp	.L460
.L233:
	movl	$21, %edx
	leaq	.LC137(%rip), %rsi
	jmp	.L460
.L232:
	movl	$22, %edx
	leaq	.LC136(%rip), %rsi
	jmp	.L460
.L231:
	movl	$17, %edx
	leaq	.LC135(%rip), %rsi
	jmp	.L460
.L230:
	movl	$23, %edx
	leaq	.LC134(%rip), %rsi
	jmp	.L460
.L229:
	movl	$20, %edx
	leaq	.LC133(%rip), %rsi
	jmp	.L460
.L228:
	movl	$16, %edx
	leaq	.LC132(%rip), %rsi
	jmp	.L460
.L227:
	movl	$21, %edx
	leaq	.LC131(%rip), %rsi
	jmp	.L460
.L226:
	movl	$25, %edx
	leaq	.LC130(%rip), %rsi
	jmp	.L460
.L225:
	movl	$22, %edx
	leaq	.LC129(%rip), %rsi
	jmp	.L460
.L224:
	movl	$15, %edx
	leaq	.LC128(%rip), %rsi
	jmp	.L460
.L223:
	movl	$20, %edx
	leaq	.LC127(%rip), %rsi
	jmp	.L460
.L222:
	movl	$17, %edx
	leaq	.LC126(%rip), %rsi
	jmp	.L460
.L221:
	movl	$45, %edx
	leaq	.LC125(%rip), %rsi
	jmp	.L460
.L220:
	movl	$46, %edx
	leaq	.LC124(%rip), %rsi
	jmp	.L460
.L219:
	movl	$38, %edx
	leaq	.LC123(%rip), %rsi
	jmp	.L460
.L218:
	movl	$34, %edx
	leaq	.LC122(%rip), %rsi
	jmp	.L460
.L217:
	movl	$38, %edx
	leaq	.LC121(%rip), %rsi
	jmp	.L460
.L216:
	movl	$32, %edx
	leaq	.LC120(%rip), %rsi
	jmp	.L460
.L215:
	movl	$31, %edx
	leaq	.LC119(%rip), %rsi
	jmp	.L460
.L214:
	movl	$45, %edx
	leaq	.LC118(%rip), %rsi
	jmp	.L460
.L213:
	movl	$44, %edx
	leaq	.LC117(%rip), %rsi
	jmp	.L460
.L212:
	movl	$44, %edx
	leaq	.LC116(%rip), %rsi
	jmp	.L460
.L211:
	movl	$23, %edx
	leaq	.LC115(%rip), %rsi
	jmp	.L460
.L210:
	movl	$26, %edx
	leaq	.LC114(%rip), %rsi
	jmp	.L460
.L209:
	movl	$9, %edx
	leaq	.LC113(%rip), %rsi
	jmp	.L460
.L208:
	movl	$14, %edx
	leaq	.LC112(%rip), %rsi
	jmp	.L460
.L207:
	movl	$21, %edx
	leaq	.LC111(%rip), %rsi
	jmp	.L460
.L206:
	movl	$18, %edx
	leaq	.LC110(%rip), %rsi
	jmp	.L460
.L205:
	movl	$25, %edx
	leaq	.LC109(%rip), %rsi
	jmp	.L460
.L204:
	movl	$29, %edx
	leaq	.LC108(%rip), %rsi
	jmp	.L460
.L203:
	movl	$15, %edx
	leaq	.LC107(%rip), %rsi
	jmp	.L460
.L202:
	movl	$15, %edx
	leaq	.LC106(%rip), %rsi
	jmp	.L460
.L201:
	movl	$20, %edx
	leaq	.LC105(%rip), %rsi
	jmp	.L460
.L200:
	movl	$17, %edx
	leaq	.LC104(%rip), %rsi
	jmp	.L460
.L199:
	movl	$17, %edx
	leaq	.LC103(%rip), %rsi
	jmp	.L460
.L198:
	movl	$17, %edx
	leaq	.LC102(%rip), %rsi
	jmp	.L460
.L197:
	movl	$17, %edx
	leaq	.LC101(%rip), %rsi
	jmp	.L460
.L196:
	movl	$22, %edx
	leaq	.LC100(%rip), %rsi
	jmp	.L460
.L195:
	movl	$26, %edx
	leaq	.LC99(%rip), %rsi
	jmp	.L460
.L194:
	movl	$20, %edx
	leaq	.LC98(%rip), %rsi
	jmp	.L460
.L193:
	movl	$9, %edx
	leaq	.LC97(%rip), %rsi
	jmp	.L460
.L192:
	movl	$25, %edx
	leaq	.LC96(%rip), %rsi
	jmp	.L460
.L191:
	movl	$15, %edx
	leaq	.LC95(%rip), %rsi
	jmp	.L460
.L190:
	movl	$15, %edx
	leaq	.LC94(%rip), %rsi
	jmp	.L460
.L189:
	movl	$15, %edx
	leaq	.LC93(%rip), %rsi
	jmp	.L460
.L188:
	movl	$42, %edx
	leaq	.LC92(%rip), %rsi
	jmp	.L460
.L187:
	movl	$13, %edx
	leaq	.LC91(%rip), %rsi
	jmp	.L460
.L186:
	movl	$10, %edx
	leaq	.LC90(%rip), %rsi
	jmp	.L460
.L185:
	movl	$10, %edx
	leaq	.LC89(%rip), %rsi
	jmp	.L460
.L184:
	movl	$23, %edx
	leaq	.LC88(%rip), %rsi
	jmp	.L460
.L183:
	movl	$10, %edx
	leaq	.LC87(%rip), %rsi
	jmp	.L460
.L182:
	movl	$10, %edx
	leaq	.LC86(%rip), %rsi
	jmp	.L460
.L181:
	movl	$34, %edx
	leaq	.LC85(%rip), %rsi
	jmp	.L460
.L180:
	movl	$35, %edx
	leaq	.LC84(%rip), %rsi
	jmp	.L460
.L179:
	movl	$27, %edx
	leaq	.LC83(%rip), %rsi
	jmp	.L460
.L178:
	movl	$28, %edx
	leaq	.LC82(%rip), %rsi
	jmp	.L460
.L177:
	movl	$31, %edx
	leaq	.LC81(%rip), %rsi
	jmp	.L460
.L176:
	movl	$32, %edx
	leaq	.LC80(%rip), %rsi
	jmp	.L460
.L175:
	movl	$19, %edx
	leaq	.LC79(%rip), %rsi
	jmp	.L460
.L174:
	movl	$25, %edx
	leaq	.LC78(%rip), %rsi
	jmp	.L460
.L173:
	movl	$20, %edx
	leaq	.LC77(%rip), %rsi
	jmp	.L460
.L172:
	movl	$27, %edx
	leaq	.LC76(%rip), %rsi
	jmp	.L460
.L171:
	movl	$30, %edx
	leaq	.LC75(%rip), %rsi
	jmp	.L460
.L170:
	movl	$22, %edx
	leaq	.LC74(%rip), %rsi
	jmp	.L460
.L169:
	movl	$14, %edx
	leaq	.LC73(%rip), %rsi
	jmp	.L460
.L168:
	movl	$15, %edx
	leaq	.LC72(%rip), %rsi
	jmp	.L460
.L167:
	movl	$15, %edx
	leaq	.LC71(%rip), %rsi
	jmp	.L460
.L166:
	movl	$29, %edx
	leaq	.LC70(%rip), %rsi
	jmp	.L460
.L165:
	movl	$29, %edx
	leaq	.LC69(%rip), %rsi
	jmp	.L460
.L164:
	movl	$24, %edx
	leaq	.LC68(%rip), %rsi
	jmp	.L460
.L163:
	movl	$33, %edx
	leaq	.LC67(%rip), %rsi
	jmp	.L460
.L162:
	movl	$27, %edx
	leaq	.LC66(%rip), %rsi
	jmp	.L460
.L161:
	movl	$25, %edx
	leaq	.LC65(%rip), %rsi
	jmp	.L460
.L160:
	movl	$21, %edx
	leaq	.LC64(%rip), %rsi
	jmp	.L460
.L159:
	movl	$30, %edx
	leaq	.LC63(%rip), %rsi
	jmp	.L460
.L158:
	movl	$24, %edx
	leaq	.LC62(%rip), %rsi
	jmp	.L460
.L157:
	movl	$25, %edx
	leaq	.LC61(%rip), %rsi
	jmp	.L460
.L156:
	movl	$30, %edx
	leaq	.LC60(%rip), %rsi
	jmp	.L460
.L155:
	movl	$26, %edx
	leaq	.LC59(%rip), %rsi
	jmp	.L460
.L154:
	movl	$33, %edx
	leaq	.LC58(%rip), %rsi
	jmp	.L460
.L153:
	movl	$27, %edx
	leaq	.LC57(%rip), %rsi
	jmp	.L460
.L152:
	movl	$23, %edx
	leaq	.LC56(%rip), %rsi
	jmp	.L460
.L151:
	movl	$23, %edx
	leaq	.LC55(%rip), %rsi
	jmp	.L460
.L150:
	movl	$32, %edx
	leaq	.LC54(%rip), %rsi
	jmp	.L460
.L149:
	movl	$26, %edx
	leaq	.LC53(%rip), %rsi
	jmp	.L460
.L148:
	movl	$36, %edx
	leaq	.LC52(%rip), %rsi
	jmp	.L460
.L147:
	movl	$30, %edx
	leaq	.LC51(%rip), %rsi
	jmp	.L460
.L146:
	movl	$27, %edx
	leaq	.LC50(%rip), %rsi
	jmp	.L460
.L145:
	movl	$17, %edx
	leaq	.LC49(%rip), %rsi
	jmp	.L460
.L144:
	movl	$17, %edx
	leaq	.LC48(%rip), %rsi
	jmp	.L460
.L143:
	movl	$17, %edx
	leaq	.LC47(%rip), %rsi
	jmp	.L460
.L142:
	movl	$34, %edx
	leaq	.LC46(%rip), %rsi
	jmp	.L460
.L141:
	movl	$26, %edx
	leaq	.LC45(%rip), %rsi
	jmp	.L460
.L140:
	movl	$19, %edx
	leaq	.LC44(%rip), %rsi
	jmp	.L460
.L139:
	movl	$21, %edx
	leaq	.LC43(%rip), %rsi
	jmp	.L460
.L138:
	movl	$13, %edx
	leaq	.LC42(%rip), %rsi
	jmp	.L460
.L137:
	movl	$39, %edx
	leaq	.LC41(%rip), %rsi
	jmp	.L460
.L136:
	movl	$14, %edx
	leaq	.LC40(%rip), %rsi
	jmp	.L460
.L135:
	movl	$16, %edx
	leaq	.LC39(%rip), %rsi
	jmp	.L460
.L134:
	movl	$13, %edx
	leaq	.LC38(%rip), %rsi
	jmp	.L460
.L133:
	movl	$13, %edx
	leaq	.LC37(%rip), %rsi
	jmp	.L460
.L132:
	movl	$28, %edx
	leaq	.LC36(%rip), %rsi
	jmp	.L460
.L131:
	movl	$26, %edx
	leaq	.LC35(%rip), %rsi
	jmp	.L460
.L130:
	movl	$17, %edx
	leaq	.LC34(%rip), %rsi
	jmp	.L460
.L129:
	movl	$25, %edx
	leaq	.LC33(%rip), %rsi
	jmp	.L460
.L128:
	movl	$28, %edx
	leaq	.LC32(%rip), %rsi
	jmp	.L460
.L127:
	movl	$38, %edx
	leaq	.LC31(%rip), %rsi
	jmp	.L460
.L126:
	movl	$35, %edx
	leaq	.LC30(%rip), %rsi
	jmp	.L460
.L125:
	movl	$16, %edx
	leaq	.LC29(%rip), %rsi
	jmp	.L460
.L124:
	movl	$19, %edx
	leaq	.LC28(%rip), %rsi
	jmp	.L460
.L123:
	movl	$18, %edx
	leaq	.LC27(%rip), %rsi
	jmp	.L460
.L122:
	movl	$15, %edx
	leaq	.LC26(%rip), %rsi
	jmp	.L460
.L121:
	movl	$14, %edx
	leaq	.LC25(%rip), %rsi
	jmp	.L460
.L120:
	movl	$47, %edx
	leaq	.LC24(%rip), %rsi
	jmp	.L460
.L119:
	movl	$48, %edx
	leaq	.LC23(%rip), %rsi
	jmp	.L460
.L118:
	movl	$41, %edx
	leaq	.LC22(%rip), %rsi
	jmp	.L460
.L117:
	movl	$40, %edx
	leaq	.LC21(%rip), %rsi
	jmp	.L460
.L116:
	movl	$40, %edx
	leaq	.LC20(%rip), %rsi
	jmp	.L460
.L115:
	movl	$39, %edx
	leaq	.LC19(%rip), %rsi
	jmp	.L460
.L114:
	movl	$38, %edx
	leaq	.LC18(%rip), %rsi
	jmp	.L460
.L113:
	movl	$33, %edx
	leaq	.LC17(%rip), %rsi
	jmp	.L460
.L112:
	movl	$25, %edx
	leaq	.LC16(%rip), %rsi
	jmp	.L460
.L111:
	movl	$26, %edx
	leaq	.LC15(%rip), %rsi
	jmp	.L460
.L110:
	movl	$39, %edx
	leaq	.LC14(%rip), %rsi
	jmp	.L460
.L109:
	movl	$38, %edx
	leaq	.LC13(%rip), %rsi
	jmp	.L460
.L108:
	movl	$28, %edx
	leaq	.LC12(%rip), %rsi
	jmp	.L460
.L107:
	movl	$16, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L460
.L106:
	movl	$14, %edx
	leaq	.LC10(%rip), %rsi
	jmp	.L460
.L104:
	movl	$23, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L460
.L335:
	movl	$12, %edx
	leaq	.LC3(%rip), %rsi
	jmp	.L460
.L394:
	movl	$27, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L460
.L395:
	movl	$32, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L460
.L391:
	movl	$13, %edx
	leaq	.LC4(%rip), %rsi
	jmp	.L460
.L403:
	movl	$16, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L460
.L393:
	movl	$16, %edx
	leaq	.LC237(%rip), %rsi
	jmp	.L460
.L336:
	movl	$19, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L460
.L472:
	call	__stack_chk_fail@PLT
.L479:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE17998:
	.size	_ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi, .-_ZN2v88internal11interpreter15BytecodeDecoder6DecodeERSoPKhi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE:
.LFB21684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21684:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE, .-_GLOBAL__sub_I__ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter15BytecodeDecoder21DecodeRegisterOperandEmNS1_11OperandTypeENS1_12OperandScaleE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
