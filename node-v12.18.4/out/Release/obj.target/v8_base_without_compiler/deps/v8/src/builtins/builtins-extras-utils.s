	.file	"builtins-extras-utils.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internalL40Builtin_Impl_ExtrasUtilsCallReflectApplyENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_ExtrasUtilsCallReflectApplyENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_ExtrasUtilsCallReflectApplyENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18355:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	41112(%rdx), %rdi
	movq	41096(%rdx), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	12464(%rdx), %rsi
	movq	%rax, -104(%rbp)
	testq	%rdi, %rdi
	je	.L6
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L7:
	movq	12464(%r14), %rax
	movq	39(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L9
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-112(%rbp), %rdx
	movq	%rax, %r13
.L10:
	movq	(%rdx), %rax
	movq	47(%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r11
.L13:
	subl	$6, %ebx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movslq	%ebx, %rbx
	leaq	-16(%r12), %rax
	movl	$3, %esi
	movq	%r14, %rdi
	movl	$1, %r8d
	movq	%r11, -112(%rbp)
	movq	%rbx, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v88internal7Factory10NewJSArrayENS0_12ElementsKindEiiNS0_26ArrayStorageAllocationModeENS0_14AllocationTypeE@PLT
	leaq	-96(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal32ArrayConstructInitializeElementsENS0_6HandleINS0_7JSArrayEEEPNS0_9ArgumentsE@PLT
	movq	-112(%rbp), %r11
	testq	%rax, %rax
	je	.L27
	subq	$8, %r12
	movq	%r11, -80(%rbp)
	movq	%r12, -72(%rbp)
	movq	%rbx, -64(%rbp)
	movq	0(%r13), %rax
	movq	1887(%rax), %r12
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L17
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L18:
	leaq	88(%r14), %rdx
	leaq	-80(%rbp), %r8
	movl	$3, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal9Execution4CallEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_iPS6_@PLT
	testq	%rax, %rax
	je	.L27
	movq	(%rax), %r12
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L27:
	movq	312(%r14), %r12
.L16:
	movq	-104(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	cmpq	41096(%r14), %r15
	je	.L22
	movq	%r15, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L22:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	41088(%r14), %rsi
	cmpq	41096(%r14), %rsi
	je	.L29
.L19:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r14)
	movq	%r12, (%rsi)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L12:
	movq	41088(%r14), %r11
	cmpq	%r11, 41096(%r14)
	je	.L30
.L14:
	leaq	8(%r11), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r11)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L9:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L31
.L11:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, 0(%r13)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rax, %rdx
	cmpq	%r15, %rax
	je	.L32
.L8:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r14, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %r11
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%r14, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r14, %rdi
	movq	%rsi, -120(%rbp)
	movq	%rdx, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L19
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18355:
	.size	_ZN2v88internalL40Builtin_Impl_ExtrasUtilsCallReflectApplyENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_ExtrasUtilsCallReflectApplyENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internalL35Builtin_Impl_ExtrasUtilsUncurryThisENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Builtin_Impl_ExtrasUtilsUncurryThisENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL35Builtin_Impl_ExtrasUtilsUncurryThisENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18352:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	12464(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r15
	movq	39(%rax), %r14
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L34
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L35:
	movq	%r12, %rdi
	movl	$5, %edx
	call	_ZN2v88internal7Factory17NewBuiltinContextENS0_6HandleINS0_13NativeContextEEEi@PLT
	movq	-8(%r13), %r13
	movq	(%rax), %rdi
	movq	%rax, %r14
	movq	%r13, 47(%rdi)
	leaq	47(%rdi), %rsi
	testb	$1, %r13b
	je	.L45
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	testl	$262144, %eax
	jne	.L54
	testb	$24, %al
	je	.L45
.L56:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L55
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$342, %edx
	movq	%r12, %rdi
	leaq	128(%r12), %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory31NewSharedFunctionInfoForBuiltinENS0_11MaybeHandleINS0_6StringEEEiNS0_12FunctionKindE@PLT
	movl	$-1, %edx
	movq	%rax, %r13
	movq	(%rax), %rax
	movw	%dx, 41(%rax)
	movq	12464(%r12), %rax
	movq	39(%rax), %rax
	movq	1303(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L40
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L41:
	movq	%r13, %rdx
	movl	$1, %r8d
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory33NewFunctionFromSharedFunctionInfoENS0_6HandleINS0_3MapEEENS2_INS0_18SharedFunctionInfoEEENS2_INS0_7ContextEEENS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	movq	%rbx, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r15
	je	.L52
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L52:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%rsi, -72(%rbp)
	movq	%rdi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L56
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L40:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L57
.L42:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%rbx, %rsi
	cmpq	41096(%rdx), %rbx
	je	.L58
.L36:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%r13, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rdx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L42
	.cfi_endproc
.LFE18352:
	.size	_ZN2v88internalL35Builtin_Impl_ExtrasUtilsUncurryThisENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL35Builtin_Impl_ExtrasUtilsUncurryThisENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L65
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L68
.L59:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L59
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL46Builtin_Impl_Stats_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Builtin_ExtrasUtilsCallReflectApply"
	.section	.text._ZN2v88internalL46Builtin_Impl_Stats_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_Stats_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_Stats_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE:
.LFB18353:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L98
.L70:
	movq	_ZZN2v88internalL46Builtin_Impl_Stats_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateEE27trace_event_unique_atomic64(%rip), %rbx
	testq	%rbx, %rbx
	je	.L99
.L72:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L100
.L74:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL40Builtin_Impl_ExtrasUtilsCallReflectApplyENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L101
.L78:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L103
.L73:
	movq	%rbx, _ZZN2v88internalL46Builtin_Impl_Stats_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateEE27trace_event_unique_atomic64(%rip)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L100:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L104
.L75:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L76
	movq	(%rdi), %rax
	call	*8(%rax)
.L76:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	movq	(%rdi), %rax
	call	*8(%rax)
.L77:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L98:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$772, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L104:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L103:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L73
.L102:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18353:
	.size	_ZN2v88internalL46Builtin_Impl_Stats_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_Stats_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL41Builtin_Impl_Stats_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"V8.Builtin_ExtrasUtilsUncurryThis"
	.section	.text._ZN2v88internalL41Builtin_Impl_Stats_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Builtin_Impl_Stats_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE, @function
_ZN2v88internalL41Builtin_Impl_Stats_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE:
.LFB18350:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L134
.L106:
	movq	_ZZN2v88internalL41Builtin_Impl_Stats_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateEE27trace_event_unique_atomic37(%rip), %rbx
	testq	%rbx, %rbx
	je	.L135
.L108:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L136
.L110:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL35Builtin_Impl_ExtrasUtilsUncurryThisENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L137
.L114:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L138
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L139
.L109:
	movq	%rbx, _ZZN2v88internalL41Builtin_Impl_Stats_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateEE27trace_event_unique_atomic37(%rip)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L136:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L140
.L111:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L112
	movq	(%rdi), %rax
	call	*8(%rax)
.L112:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L113
	movq	(%rdi), %rax
	call	*8(%rax)
.L113:
	leaq	.LC2(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L134:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$771, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L140:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC2(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L139:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L109
.L138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18350:
	.size	_ZN2v88internalL41Builtin_Impl_Stats_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE, .-_ZN2v88internalL41Builtin_Impl_Stats_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal30Builtin_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal30Builtin_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE
	.type	_ZN2v88internal30Builtin_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE, @function
_ZN2v88internal30Builtin_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE:
.LFB18351:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L145
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL35Builtin_Impl_ExtrasUtilsUncurryThisENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore 6
	jmp	_ZN2v88internalL41Builtin_Impl_Stats_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18351:
	.size	_ZN2v88internal30Builtin_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE, .-_ZN2v88internal30Builtin_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Builtin_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Builtin_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Builtin_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Builtin_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE:
.LFB18354:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L150
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL40Builtin_Impl_ExtrasUtilsCallReflectApplyENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore 6
	jmp	_ZN2v88internalL46Builtin_Impl_Stats_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18354:
	.size	_ZN2v88internal35Builtin_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE, .-_ZN2v88internal35Builtin_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal30Builtin_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal30Builtin_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal30Builtin_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE:
.LFB22094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22094:
	.size	_GLOBAL__sub_I__ZN2v88internal30Builtin_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal30Builtin_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal30Builtin_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL46Builtin_Impl_Stats_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateEE27trace_event_unique_atomic64,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL46Builtin_Impl_Stats_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateEE27trace_event_unique_atomic64, @object
	.size	_ZZN2v88internalL46Builtin_Impl_Stats_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateEE27trace_event_unique_atomic64, 8
_ZZN2v88internalL46Builtin_Impl_Stats_ExtrasUtilsCallReflectApplyEiPmPNS0_7IsolateEE27trace_event_unique_atomic64:
	.zero	8
	.section	.bss._ZZN2v88internalL41Builtin_Impl_Stats_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateEE27trace_event_unique_atomic37,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Builtin_Impl_Stats_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateEE27trace_event_unique_atomic37, @object
	.size	_ZZN2v88internalL41Builtin_Impl_Stats_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateEE27trace_event_unique_atomic37, 8
_ZZN2v88internalL41Builtin_Impl_Stats_ExtrasUtilsUncurryThisEiPmPNS0_7IsolateEE27trace_event_unique_atomic37:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
