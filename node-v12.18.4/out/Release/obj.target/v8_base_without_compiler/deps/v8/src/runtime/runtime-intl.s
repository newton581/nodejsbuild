	.file	"runtime-intl.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5089:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5089:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5090:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5090:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5092:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5092:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB18214:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE18214:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"V8.Runtime_Runtime_FormatList"
.LC2:
	.string	"args[0].IsJSListFormat()"
.LC3:
	.string	"Check failed: %s."
.LC4:
	.string	"args[1].IsJSArray()"
	.section	.text._ZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateE, @function
_ZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateE:
.LFB19056:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L52
.L16:
	movq	_ZZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateEE27trace_event_unique_atomic36(%rip), %rbx
	testq	%rbx, %rbx
	je	.L53
.L18:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L54
.L20:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L24
.L25:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L55
.L19:
	movq	%rbx, _ZZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateEE27trace_event_unique_atomic36(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L24:
	movq	-1(%rax), %rax
	cmpw	$1091, 11(%rax)
	jne	.L25
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	testb	$1, %al
	jne	.L56
.L26:
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L57
.L21:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L56:
	movq	-1(%rax), %rax
	cmpw	$1061, 11(%rax)
	jne	.L26
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12JSListFormat10FormatListEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE@PLT
	testq	%rax, %rax
	je	.L58
	movq	(%rax), %r13
.L29:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L32
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L32:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L59
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L52:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$384, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L57:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L19
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19056:
	.size	_ZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateE, .-_ZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL31Stats_Runtime_FormatListToPartsEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"V8.Runtime_Runtime_FormatListToParts"
	.section	.text._ZN2v88internalL31Stats_Runtime_FormatListToPartsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_FormatListToPartsEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_FormatListToPartsEiPmPNS0_7IsolateE:
.LFB19059:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L98
.L62:
	movq	_ZZN2v88internalL31Stats_Runtime_FormatListToPartsEiPmPNS0_7IsolateEE27trace_event_unique_atomic46(%rip), %rbx
	testq	%rbx, %rbx
	je	.L99
.L64:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L100
.L66:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L70
.L71:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L99:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L101
.L65:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_FormatListToPartsEiPmPNS0_7IsolateEE27trace_event_unique_atomic46(%rip)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L70:
	movq	-1(%rax), %rax
	cmpw	$1091, 11(%rax)
	jne	.L71
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	testb	$1, %al
	jne	.L102
.L72:
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L100:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L103
.L67:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L68
	movq	(%rdi), %rax
	call	*8(%rax)
.L68:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	movq	(%rdi), %rax
	call	*8(%rax)
.L69:
	leaq	.LC5(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L102:
	movq	-1(%rax), %rax
	cmpw	$1061, 11(%rax)
	jne	.L72
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12JSListFormat17FormatListToPartsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE@PLT
	testq	%rax, %rax
	je	.L104
	movq	(%rax), %r13
.L75:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L78
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L78:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L105
.L61:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	312(%r12), %r13
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L98:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$385, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L103:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC5(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L65
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19059:
	.size	_ZN2v88internalL31Stats_Runtime_FormatListToPartsEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_FormatListToPartsEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Runtime_Runtime_StringToLowerCaseIntl"
	.section	.rodata._ZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC7:
	.string	"args[0].IsString()"
	.section	.text._ZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE.isra.0:
.LFB23320:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L157
.L108:
	movq	_ZZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic55(%rip), %rbx
	testq	%rbx, %rbx
	je	.L158
.L110:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L159
.L112:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L116
.L117:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L158:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L160
.L111:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic55(%rip)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L116:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L117
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	ja	.L119
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L161
.L119:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L127
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L127
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L130
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl14ConvertToLowerEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L162
	movq	(%rax), %r13
.L134:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L137
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L137:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L163
.L107:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L165
.L113:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L114
	movq	(%rdi), %rax
	call	*8(%rax)
.L114:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L115
	movq	(%rdi), %rax
	call	*8(%rax)
.L115:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L130:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L166
.L132:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L162:
	movq	312(%r12), %r13
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L161:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L122
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L122
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L122:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L124
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L157:
	movq	40960(%rsi), %rax
	movl	$386, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L165:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L124:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L167
.L126:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L132
.L167:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L126
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23320:
	.size	_ZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL35Stats_Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"V8.Runtime_Runtime_StringToUpperCaseIntl"
	.section	.text._ZN2v88internalL35Stats_Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateE.isra.0:
.LFB23321:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L218
.L169:
	movq	_ZZN2v88internalL35Stats_Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic63(%rip), %rbx
	testq	%rbx, %rbx
	je	.L219
.L171:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L220
.L173:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L177
.L178:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L219:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L221
.L172:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic63(%rip)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L177:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L178
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	ja	.L180
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L222
.L180:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L188
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	jne	.L188
	movq	0(%r13), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L191
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl14ConvertToUpperEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L223
	movq	(%rax), %r13
.L195:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L198
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L198:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L224
.L168:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L225
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L226
.L174:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L175
	movq	(%rdi), %rax
	call	*8(%rax)
.L175:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L176
	movq	(%rdi), %rax
	call	*8(%rax)
.L176:
	leaq	.LC8(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L191:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L227
.L193:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L223:
	movq	312(%r12), %r13
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L222:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L183
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L183
	movq	%r13, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L183:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L185
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L218:
	movq	40960(%rsi), %rax
	movl	$387, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L226:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC8(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L185:
	movq	41088(%r12), %r13
	cmpq	41096(%r12), %r13
	je	.L228
.L187:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, 0(%r13)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L227:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L193
.L228:
	movq	%r12, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L187
.L225:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23321:
	.size	_ZN2v88internalL35Stats_Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal18Runtime_FormatListEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal18Runtime_FormatListEiPmPNS0_7IsolateE
	.type	_ZN2v88internal18Runtime_FormatListEiPmPNS0_7IsolateE, @function
_ZN2v88internal18Runtime_FormatListEiPmPNS0_7IsolateE:
.LFB19057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L240
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L231
.L232:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L231:
	movq	-1(%rax), %rax
	cmpw	$1091, 11(%rax)
	jne	.L232
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L241
.L233:
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L241:
	movq	-1(%rax), %rax
	cmpw	$1061, 11(%rax)
	jne	.L233
	movq	%r12, %rdi
	call	_ZN2v88internal12JSListFormat10FormatListEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE@PLT
	testq	%rax, %rax
	je	.L242
	movq	(%rax), %r14
.L236:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L229
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L229:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L240:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19057:
	.size	_ZN2v88internal18Runtime_FormatListEiPmPNS0_7IsolateE, .-_ZN2v88internal18Runtime_FormatListEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_FormatListToPartsEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_FormatListToPartsEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_FormatListToPartsEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_FormatListToPartsEiPmPNS0_7IsolateE:
.LFB19060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L254
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L245
.L246:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L245:
	movq	-1(%rax), %rax
	cmpw	$1091, 11(%rax)
	jne	.L246
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L255
.L247:
	leaq	.LC4(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L255:
	movq	-1(%rax), %rax
	cmpw	$1061, 11(%rax)
	jne	.L247
	movq	%r12, %rdi
	call	_ZN2v88internal12JSListFormat17FormatListToPartsEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_7JSArrayEEE@PLT
	testq	%rax, %rax
	je	.L256
	movq	(%rax), %r14
.L250:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L243
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L243:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L254:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_FormatListToPartsEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE19060:
	.size	_ZN2v88internal25Runtime_FormatListToPartsEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_FormatListToPartsEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE:
.LFB19063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L281
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L259
.L260:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L259:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L260
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	ja	.L262
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L282
.L262:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L270
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L283
.L270:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl14ConvertToLowerEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L284
.L276:
	movq	(%rax), %r14
.L277:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L257
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L257:
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	movq	(%r8), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L273
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%r12, %rdi
	movq	%rax, %r8
	movq	%r8, %rsi
	call	_ZN2v88internal4Intl14ConvertToLowerEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	jne	.L276
	.p2align 4,,10
	.p2align 3
.L284:
	movq	312(%r12), %r14
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L282:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L265
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L265
	movq	%r8, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r8
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L265:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L267
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L281:
	addq	$16, %rsp
	movq	%rdx, %rsi
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L285
.L275:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L267:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L286
.L269:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L275
.L286:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L269
	.cfi_endproc
.LFE19063:
	.size	_ZN2v88internal29Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateE:
.LFB19066:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L311
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L289
.L290:
	leaq	.LC7(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L289:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L290
	movq	-1(%rax), %rdx
	movq	%rax, %rsi
	cmpw	$63, 11(%rdx)
	ja	.L292
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	je	.L312
.L292:
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L300
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	andl	$7, %eax
	cmpw	$5, %ax
	je	.L313
.L300:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl14ConvertToUpperEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	je	.L314
.L306:
	movq	(%rax), %r14
.L307:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L287
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L287:
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movq	(%r8), %rax
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L303
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%r12, %rdi
	movq	%rax, %r8
	movq	%r8, %rsi
	call	_ZN2v88internal4Intl14ConvertToUpperEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	testq	%rax, %rax
	jne	.L306
	.p2align 4,,10
	.p2align 3
.L314:
	movq	312(%r12), %r14
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L312:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$1, %dx
	jne	.L295
	movq	23(%rax), %rdx
	movl	11(%rdx), %edx
	testl	%edx, %edx
	je	.L295
	movq	%r8, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6String11SlowFlattenEPNS0_7IsolateENS0_6HandleINS0_10ConsStringEEENS0_14AllocationTypeE@PLT
	movq	%rax, %r8
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L295:
	movq	41112(%r12), %rdi
	movq	15(%rax), %rsi
	testq	%rdi, %rdi
	je	.L297
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L311:
	addq	$16, %rsp
	movq	%rdx, %rsi
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL35Stats_Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L315
.L305:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L297:
	movq	41088(%r12), %r8
	cmpq	41096(%r12), %r8
	je	.L316
.L299:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r8)
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L305
.L316:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L299
	.cfi_endproc
.LFE19066:
	.size	_ZN2v88internal29Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal18Runtime_FormatListEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal18Runtime_FormatListEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal18Runtime_FormatListEiPmPNS0_7IsolateE:
.LFB23308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23308:
	.size	_GLOBAL__sub_I__ZN2v88internal18Runtime_FormatListEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal18Runtime_FormatListEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal18Runtime_FormatListEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL35Stats_Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic63,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic63, @object
	.size	_ZZN2v88internalL35Stats_Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic63, 8
_ZZN2v88internalL35Stats_Runtime_StringToUpperCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic63:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic55,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic55, @object
	.size	_ZZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic55, 8
_ZZN2v88internalL35Stats_Runtime_StringToLowerCaseIntlEiPmPNS0_7IsolateEE27trace_event_unique_atomic55:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_FormatListToPartsEiPmPNS0_7IsolateEE27trace_event_unique_atomic46,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_FormatListToPartsEiPmPNS0_7IsolateEE27trace_event_unique_atomic46, @object
	.size	_ZZN2v88internalL31Stats_Runtime_FormatListToPartsEiPmPNS0_7IsolateEE27trace_event_unique_atomic46, 8
_ZZN2v88internalL31Stats_Runtime_FormatListToPartsEiPmPNS0_7IsolateEE27trace_event_unique_atomic46:
	.zero	8
	.section	.bss._ZZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateEE27trace_event_unique_atomic36,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateEE27trace_event_unique_atomic36, @object
	.size	_ZZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateEE27trace_event_unique_atomic36, 8
_ZZN2v88internalL24Stats_Runtime_FormatListEiPmPNS0_7IsolateEE27trace_event_unique_atomic36:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
