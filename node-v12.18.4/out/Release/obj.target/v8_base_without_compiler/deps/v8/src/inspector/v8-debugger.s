	.file	"v8-debugger.cc"
	.text
	.section	.text._ZN12v8_inspector17V8InspectorClient21runMessageLoopOnPauseEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient21runMessageLoopOnPauseEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient21runMessageLoopOnPauseEi
	.type	_ZN12v8_inspector17V8InspectorClient21runMessageLoopOnPauseEi, @function
_ZN12v8_inspector17V8InspectorClient21runMessageLoopOnPauseEi:
.LFB4307:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4307:
	.size	_ZN12v8_inspector17V8InspectorClient21runMessageLoopOnPauseEi, .-_ZN12v8_inspector17V8InspectorClient21runMessageLoopOnPauseEi
	.section	.text._ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv
	.type	_ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv, @function
_ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv:
.LFB4308:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4308:
	.size	_ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv, .-_ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv
	.section	.text._ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE
	.type	_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE, @function
_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE:
.LFB4318:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4318:
	.size	_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE, .-_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE
	.section	.text._ZN12v8_inspector17V8InspectorClient29maxAsyncCallStackDepthChangedEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient29maxAsyncCallStackDepthChangedEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient29maxAsyncCallStackDepthChangedEi
	.type	_ZN12v8_inspector17V8InspectorClient29maxAsyncCallStackDepthChangedEi, @function
_ZN12v8_inspector17V8InspectorClient29maxAsyncCallStackDepthChangedEi:
.LFB4333:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4333:
	.size	_ZN12v8_inspector17V8InspectorClient29maxAsyncCallStackDepthChangedEi, .-_ZN12v8_inspector17V8InspectorClient29maxAsyncCallStackDepthChangedEi
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger7disableEvEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger7disableEvEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger7disableEvEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation:
.LFB10845:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L7
	cmpl	$3, %edx
	je	.L8
	cmpl	$1, %edx
	je	.L12
.L8:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE10845:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger7disableEvEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger7disableEvEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation:
.LFB10932:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L14
	cmpl	$3, %edx
	je	.L15
	cmpl	$1, %edx
	je	.L19
.L15:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE10932:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE1_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE1_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE1_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation:
.LFB10940:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L22
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE10940:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE1_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE1_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS3_5debug6ScriptEEEbbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS3_5debug6ScriptEEEbbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS3_5debug6ScriptEEEbbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation:
.LFB10945:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L24
	cmpl	$3, %edx
	je	.L25
	cmpl	$1, %edx
	je	.L29
.L25:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE10945:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS3_5debug6ScriptEEEbbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS3_5debug6ScriptEEEbbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger17captureStackTraceEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger17captureStackTraceEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger17captureStackTraceEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB11086:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	184(%rax), %rax
	cmpb	$0, 40(%rax)
	je	.L30
	movq	(%rdi), %rax
	movl	_ZN12v8_inspector16V8StackTraceImpl25maxCallStackSizeToCaptureE(%rip), %edx
	movl	%edx, (%rax)
.L30:
	ret
	.cfi_endproc
.LFE11086:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger17captureStackTraceEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger17captureStackTraceEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger17captureStackTraceEbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger17captureStackTraceEbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger17captureStackTraceEbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation:
.LFB11087:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L33
	cmpl	$3, %edx
	je	.L34
	cmpl	$1, %edx
	je	.L38
.L34:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11087:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger17captureStackTraceEbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger17captureStackTraceEbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB15038:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE15038:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD2Ev, @function
_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD2Ev:
.LFB15042:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE15042:
	.size	_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD2Ev, .-_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD2Ev
	.set	_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD1Ev,_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB15059:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE15059:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD0Ev, @function
_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD0Ev:
.LFB15044:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE15044:
	.size	_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD0Ev, .-_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB15040:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE15040:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB15058:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE15058:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB15057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L45
	movq	80(%r12), %rdi
	leaq	96(%r12), %rax
	cmpq	%rax, %rdi
	je	.L47
	call	_ZdlPv@PLT
.L47:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15057:
	.size	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger7disableEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger7disableEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger7disableEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB10843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movzbl	(%rax), %esi
	movq	(%r8), %rax
	movq	192(%rax), %rdi
	call	_ZNK12v8_inspector19V8DebuggerAgentImpl12acceptsPauseEb@PLT
	testb	%al, %al
	je	.L51
	movq	8(%rbx), %rax
	movb	$1, (%rax)
.L51:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10843:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger7disableEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger7disableEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZZN12v8_inspector10V8Debugger17interruptAndBreakEiENUlPN2v87IsolateEPvE_4_FUNES3_S4_,"ax",@progbits
	.p2align 4
	.type	_ZZN12v8_inspector10V8Debugger17interruptAndBreakEiENUlPN2v87IsolateEPvE_4_FUNES3_S4_, @function
_ZZN12v8_inspector10V8Debugger17interruptAndBreakEiENUlPN2v87IsolateEPvE_4_FUNES3_S4_:
.LFB7756:
	.cfi_startproc
	endbr64
	jmp	_ZN2v85debug13BreakRightNowEPNS_7IsolateE@PLT
	.cfi_endproc
.LFE7756:
	.size	_ZZN12v8_inspector10V8Debugger17interruptAndBreakEiENUlPN2v87IsolateEPvE_4_FUNES3_S4_, .-_ZZN12v8_inspector10V8Debugger17interruptAndBreakEiENUlPN2v87IsolateEPvE_4_FUNES3_S4_
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB10935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rbx
	movq	(%rsi), %r12
	movq	40(%rbx), %rax
	movq	192(%r12), %rdi
	movzbl	(%rax), %esi
	call	_ZNK12v8_inspector19V8DebuggerAgentImpl12acceptsPauseEb@PLT
	testb	%al, %al
	jne	.L64
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movq	48(%rbx), %rax
	movq	16(%rbx), %r14
	movq	8(%rbx), %r13
	movq	192(%r12), %r12
	movzbl	(%rax), %ecx
	movq	40(%rbx), %rax
	movzbl	(%rax), %edx
	movq	32(%rbx), %rax
	movl	%ecx, -60(%rbp)
	movzbl	(%rax), %r9d
	movq	24(%rbx), %rax
	movl	%edx, -56(%rbp)
	movl	(%rax), %r15d
	movq	(%rbx), %rax
	movl	%r9d, -52(%rbp)
	movq	(%rax), %rdi
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movl	-60(%rbp), %ecx
	movl	-56(%rbp), %edx
	movl	%r15d, %r8d
	movl	-52(%rbp), %r9d
	movl	%eax, %esi
	movq	%r12, %rdi
	pushq	%rcx
	movq	%r14, %rcx
	pushq	%rdx
	movq	0(%r13), %rdx
	call	_ZN12v8_inspector19V8DebuggerAgentImpl8didPauseEiN2v85LocalINS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEbbb@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10935:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E1_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E1_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E1_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB10939:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	192(%rax), %rdi
	cmpb	$0, 32(%rdi)
	jne	.L67
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	jmp	_ZN12v8_inspector19V8DebuggerAgentImpl11didContinueEv@PLT
	.cfi_endproc
.LFE10939:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E1_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E1_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmm
	.type	_ZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmm, @function
_ZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmm:
.LFB7797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, 48(%rdi)
	movb	$1, 56(%rdi)
	movq	16(%rdi), %rdi
	call	_ZN2v87Isolate28GetEnteredOrMicrotaskContextEv@PLT
	movq	%rax, %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L69
	movq	24(%rbx), %rdi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE@PLT
.L69:
	movl	%eax, 60(%rbx)
	movq	16(%rbx), %rdi
	xorl	%edx, %edx
	leaq	_ZZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmmENUlPN2v87IsolateES1_E_4_FUNES4_S1_(%rip), %rsi
	call	_ZN2v87Isolate16RequestInterruptEPFvPS0_PvES2_@PLT
	leaq	0(,%r12,4), %rax
	popq	%rbx
	popq	%r12
	movabsq	$4611686018427387902, %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpq	%rdx, %rax
	leaq	1(%rdx), %rdx
	cmova	%rdx, %rax
	ret
	.cfi_endproc
.LFE7797:
	.size	_ZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmm, .-_ZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmm
	.section	.text._ZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS1_5debug6ScriptEEEbb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS1_5debug6ScriptEEEbb
	.type	_ZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS1_5debug6ScriptEEEbb, @function
_ZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS1_5debug6ScriptEEEbb:
.LFB7801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -40
	movq	%rsi, -104(%rbp)
	movb	%dl, -108(%rbp)
	movb	%cl, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85debug6Script9ContextIdEv@PLT
	testb	%al, %al
	je	.L75
	movq	-104(%rbp), %rdi
	sarq	$32, %rax
	movq	%rax, %rbx
	call	_ZNK2v85debug6Script6IsWasmEv@PLT
	testb	%al, %al
	jne	.L93
.L77:
	movl	40(%r12), %eax
	testl	%eax, %eax
	je	.L94
.L75:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	leaq	-96(%rbp), %rax
	movq	24(%r12), %r13
	movl	$40, %edi
	movq	%rax, %xmm1
	leaq	-104(%rbp), %rax
	movq	%rax, %xmm2
	leaq	-112(%rbp), %rax
	movq	%rax, %xmm0
	leaq	-108(%rbp), %rax
	punpcklqdq	%xmm2, %xmm1
	movq	%rax, %xmm3
	movq	16(%r12), %rax
	movaps	%xmm1, -144(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, -96(%rbp)
	movq	16(%r13), %rax
	movaps	%xmm0, -128(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -64(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm0
	leaq	-88(%rbp), %rdx
	movdqa	-144(%rbp), %xmm1
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS3_5debug6ScriptEEEbbEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation(%rip), %rsi
	movq	%rdx, 32(%rax)
	movq	24(%r12), %rdi
	movups	%xmm0, 16(%rax)
	movq	%rsi, %xmm0
	movl	%ebx, %esi
	movq	%rax, -80(%rbp)
	movups	%xmm1, (%rax)
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger14ScriptCompiledEN2v85LocalINS5_5debug6ScriptEEEbbEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -64(%rbp)
.L92:
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	-80(%rbp), %r12
	movq	%r13, %rdi
	movl	%eax, %esi
	movq	%r12, %rdx
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE@PLT
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L75
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L93:
	movq	-104(%rbp), %rdi
	call	_ZNK2v85debug6Script16SourceMappingURLEv@PLT
	testq	%rax, %rax
	jne	.L77
	leaq	728(%r12), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS3_5debug6ScriptEEEbbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation(%rip), %rcx
	movq	24(%r12), %r13
	movl	%ebx, %esi
	movq	%rax, -88(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, %xmm0
	leaq	-88(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm5
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger14ScriptCompiledEN2v85LocalINS5_5debug6ScriptEEEbbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, %xmm6
	movaps	%xmm0, -80(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -64(%rbp)
	jmp	.L92
.L95:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7801:
	.size	_ZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS1_5debug6ScriptEEEbb, .-_ZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS1_5debug6ScriptEEEbb
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger14ScriptCompiledEN2v85LocalINS5_5debug6ScriptEEEbbEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger14ScriptCompiledEN2v85LocalINS5_5debug6ScriptEEEbbEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger14ScriptCompiledEN2v85LocalINS5_5debug6ScriptEEEbbEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB10955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	192(%rax), %r12
	cmpb	$0, 32(%r12)
	jne	.L104
.L96:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	-48(%rbp), %r14
	movq	%r12, %r8
	movq	%r14, %rdi
	movq	16(%rax), %rdx
	movq	32(%rax), %rsi
	movzbl	(%rdx), %r13d
	movq	24(%rax), %rdx
	movq	(%rsi), %r9
	movzbl	(%rdx), %ecx
	movq	8(%rax), %rdx
	xorl	$1, %r13d
	movq	(%rax), %rax
	movzbl	%r13b, %r13d
	movq	(%rdx), %rdx
	movq	(%rax), %rsi
	call	_ZN12v8_inspector16V8DebuggerScript6CreateEPN2v87IsolateENS1_5LocalINS1_5debug6ScriptEEEbPNS_19V8DebuggerAgentImplEPNS_17V8InspectorClientE@PLT
	movq	%r12, %rdi
	movl	%r13d, %edx
	movq	%r14, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl14didParseSourceESt10unique_ptrINS_16V8DebuggerScriptESt14default_deleteIS2_EEb@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L96
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L96
.L105:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10955:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger14ScriptCompiledEN2v85LocalINS5_5debug6ScriptEEEbbEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger14ScriptCompiledEN2v85LocalINS5_5debug6ScriptEEEbbEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN12v8_inspector10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS1_5debug6ScriptEEERKNS3_8LocationES8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS1_5debug6ScriptEEERKNS3_8LocationES8_
	.type	_ZN12v8_inspector10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS1_5debug6ScriptEEERKNS3_8LocationES8_, @function
_ZN12v8_inspector10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS1_5debug6ScriptEEERKNS3_8LocationES8_:
.LFB7815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85debug6Script9ContextIdEv@PLT
	testb	%al, %al
	je	.L106
	movq	%rax, %rbx
	leaq	-130(%rbp), %rax
	movq	%r15, %rdi
	movb	$0, -130(%rbp)
	movq	%rax, %xmm0
	leaq	-129(%rbp), %rax
	movb	$1, -129(%rbp)
	sarq	$32, %rbx
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -176(%rbp)
	call	_ZNK2v85debug6Script2IdEv@PLT
	leaq	-96(%rbp), %rdx
	movq	%rdx, %rdi
	movl	%eax, %esi
	movq	%rdx, -152(%rbp)
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	movl	$40, %edi
	movq	24(%r12), %r15
	movq	$0, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-176(%rbp), %xmm0
	movq	24(%r12), %rdi
	movl	%ebx, %esi
	movq	-152(%rbp), %rdx
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS3_5debug6ScriptEEERKNS5_8LocationESA_EUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation(%rip), %rcx
	movq	%r14, 24(%rax)
	leaq	-128(%rbp), %r12
	movups	%xmm0, (%rax)
	movq	%rcx, %xmm0
	movq	%rdx, 16(%rax)
	movq	%r13, 32(%rax)
	movq	%rax, -128(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS5_5debug6ScriptEEERKNS7_8LocationESC_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE@PLT
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L108
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L108:
	movzbl	-130(%rbp), %eax
	movzbl	-129(%rbp), %edx
	movq	-96(%rbp), %rdi
	testb	%al, %al
	cmovne	%edx, %eax
	leaq	-80(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L106
	movb	%al, -152(%rbp)
	call	_ZdlPv@PLT
	movzbl	-152(%rbp), %eax
.L106:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L118
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L118:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7815:
	.size	_ZN12v8_inspector10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS1_5debug6ScriptEEERKNS3_8LocationES8_, .-_ZN12v8_inspector10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS1_5debug6ScriptEEERKNS3_8LocationES8_
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS5_5debug6ScriptEEERKNS7_8LocationESC_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS5_5debug6ScriptEEERKNS7_8LocationESC_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS5_5debug6ScriptEEERKNS7_8LocationESC_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB10971:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	192(%rax), %r8
	cmpb	$0, 32(%r8)
	jne	.L125
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rbx
	movq	%r8, %rdi
	movq	(%rbx), %rax
	movb	$1, (%rax)
	movq	32(%rbx), %rcx
	movq	24(%rbx), %rdx
	movq	16(%rbx), %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl20isFunctionBlackboxedERKNS_8String16ERKN2v85debug8LocationES8_@PLT
	movl	%eax, %r8d
	movq	8(%rbx), %rax
	andb	%r8b, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10971:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS5_5debug6ScriptEEERKNS7_8LocationESC_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS5_5debug6ScriptEEERKNS7_8LocationESC_EUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation:
.LFB10936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L127
	cmpl	$3, %edx
	je	.L128
	cmpl	$1, %edx
	je	.L134
.L129:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$56, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movdqu	32(%r12), %xmm2
	movups	%xmm2, 32(%rax)
	movq	48(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 48(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L129
	movl	$56, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10936:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS3_5debug6ScriptEEEbbEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS3_5debug6ScriptEEEbbEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS3_5debug6ScriptEEEbbEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation:
.LFB10956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L136
	cmpl	$3, %edx
	je	.L137
	cmpl	$1, %edx
	je	.L143
.L138:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$40, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movq	32(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L138
	movl	$40, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10956:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS3_5debug6ScriptEEEbbEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS3_5debug6ScriptEEEbbEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKSC_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS3_5debug6ScriptEEERKNS5_8LocationESA_EUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS3_5debug6ScriptEEERKNS5_8LocationESA_EUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS3_5debug6ScriptEEERKNS5_8LocationESA_EUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation:
.LFB10972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L145
	cmpl	$3, %edx
	je	.L146
	cmpl	$1, %edx
	je	.L152
.L147:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$40, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movq	32(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L147
	movl	$40, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10972:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS3_5debug6ScriptEEERKNS5_8LocationESA_EUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS3_5debug6ScriptEEERKNS5_8LocationESA_EUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger14ScriptCompiledEN2v85LocalINS5_5debug6ScriptEEEbbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger14ScriptCompiledEN2v85LocalINS5_5debug6ScriptEEEbbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger14ScriptCompiledEN2v85LocalINS5_5debug6ScriptEEEbbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB10944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rsi), %rax
	movq	192(%rax), %r12
	cmpb	$0, 32(%r12)
	jne	.L156
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movq	8(%rdi), %rax
	movq	(%rax), %r13
	movq	(%rdi), %rax
	movq	(%rax), %rdi
	call	_ZN2v85debug10WasmScript4CastEPNS0_6ScriptE@PLT
	movq	%r12, %rdx
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector15WasmTranslation9AddScriptEN2v85LocalINS1_5debug10WasmScriptEEEPNS_19V8DebuggerAgentImplE@PLT
	.cfi_endproc
.LFE10944:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger14ScriptCompiledEN2v85LocalINS5_5debug6ScriptEEEbbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger14ScriptCompiledEN2v85LocalINS5_5debug6ScriptEEEbbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN12v8_inspector10V8Debugger35terminateExecutionCompletedCallbackEPN2v87IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger35terminateExecutionCompletedCallbackEPN2v87IsolateE
	.type	_ZN12v8_inspector10V8Debugger35terminateExecutionCompletedCallbackEPN2v87IsolateE, @function
_ZN12v8_inspector10V8Debugger35terminateExecutionCompletedCallbackEPN2v87IsolateE:
.LFB7785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	call	_ZN2v85debug12GetInspectorEPNS_7IsolateE@PLT
	movq	24(%rax), %rbx
	cmpq	$0, 720(%rbx)
	je	.L157
	movq	16(%rbx), %rdi
	leaq	_ZN12v8_inspector10V8Debugger35terminateExecutionCompletedCallbackEPN2v87IsolateE(%rip), %rsi
	call	_ZN2v87Isolate27RemoveCallCompletedCallbackEPFvPS0_E@PLT
	movq	16(%rbx), %rdi
	xorl	%edx, %edx
	leaq	_ZN12v8_inspector10V8Debugger47terminateExecutionCompletedCallbackIgnoringDataEPN2v87IsolateEPv(%rip), %rsi
	call	_ZN2v87Isolate33RemoveMicrotasksCompletedCallbackEPFvPS0_PvES2_@PLT
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate24CancelTerminateExecutionEv@PLT
	movq	720(%rbx), %rdi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	720(%rbx), %rdi
	movq	$0, 720(%rbx)
	testq	%rdi, %rdi
	je	.L157
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7785:
	.size	_ZN12v8_inspector10V8Debugger35terminateExecutionCompletedCallbackEPN2v87IsolateE, .-_ZN12v8_inspector10V8Debugger35terminateExecutionCompletedCallbackEPN2v87IsolateE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicate6FilterEN2v85LocalINS2_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicate6FilterEN2v85LocalINS2_6ObjectEEE, @function
_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicate6FilterEN2v85LocalINS2_6ObjectEEE:
.LFB7512:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	call	_ZNK2v85Value23IsModuleNamespaceObjectEv@PLT
	testb	%al, %al
	je	.L192
.L166:
	xorl	%eax, %eax
.L165:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v85debug18GetCreationContextENS_5LocalINS_6ObjectEEE@PLT
	movq	16(%rbx), %rdx
	testq	%rax, %rax
	je	.L193
	testq	%rdx, %rdx
	je	.L166
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	sete	%al
.L168:
	testb	%al, %al
	je	.L166
	movq	8(%rbx), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient23isInspectableHeapObjectEN2v85LocalINS1_6ObjectEEE(%rip), %rdx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L194
	.p2align 4,,10
	.p2align 3
.L170:
	movq	%r12, %rdi
	call	_ZN2v86Object12GetPrototypeEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L166
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L195
	testq	%r12, %r12
	je	.L170
	movq	(%r12), %rcx
	cmpq	%rcx, (%rax)
	sete	%al
.L172:
	testb	%al, %al
	je	.L170
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L195:
	testq	%r12, %r12
	sete	%al
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L193:
	testq	%rdx, %rdx
	sete	%al
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r12, %rsi
	call	*%rax
	testb	%al, %al
	je	.L166
	jmp	.L170
	.cfi_endproc
.LFE7512:
	.size	_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicate6FilterEN2v85LocalINS2_6ObjectEEE, .-_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicate6FilterEN2v85LocalINS2_6ObjectEEE
	.section	.text._ZZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmmENUlPN2v87IsolateES1_E_4_FUNES4_S1_,"ax",@progbits
	.p2align 4
	.type	_ZZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmmENUlPN2v87IsolateES1_E_4_FUNES4_S1_, @function
_ZZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmmENUlPN2v87IsolateES1_E_4_FUNES4_S1_:
.LFB15601:
	.cfi_startproc
	endbr64
	jmp	_ZN2v85debug13BreakRightNowEPNS_7IsolateE@PLT
	.cfi_endproc
.LFE15601:
	.size	_ZZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmmENUlPN2v87IsolateES1_E_4_FUNES4_S1_, .-_ZZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmmENUlPN2v87IsolateES1_E_4_FUNES4_S1_
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB15603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movzbl	(%rax), %esi
	movq	(%r8), %rax
	movq	192(%rax), %rdi
	call	_ZNK12v8_inspector19V8DebuggerAgentImpl12acceptsPauseEb@PLT
	testb	%al, %al
	je	.L197
	movq	8(%rbx), %rax
	movb	$1, (%rax)
.L197:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15603:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN12v8_inspector10V8DebuggerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8DebuggerD2Ev
	.type	_ZN12v8_inspector10V8DebuggerD2Ev, @function
_ZN12v8_inspector10V8DebuggerD2Ev:
.LFB7717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	88+_ZTVN12v8_inspector10V8DebuggerE(%rip), %rax
	leaq	_ZN12v8_inspector10V8Debugger35terminateExecutionCompletedCallbackEPN2v87IsolateE(%rip), %rsi
	leaq	-72(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movups	%xmm0, (%rdi)
	movq	16(%rdi), %rdi
	call	_ZN2v87Isolate27RemoveCallCompletedCallbackEPFvPS0_E@PLT
	movq	16(%rbx), %rdi
	xorl	%edx, %edx
	leaq	_ZN12v8_inspector10V8Debugger47terminateExecutionCompletedCallbackIgnoringDataEPN2v87IsolateEPv(%rip), %rsi
	call	_ZN2v87Isolate33RemoveMicrotasksCompletedCallbackEPFvPS0_PvES2_@PLT
	leaq	728(%rbx), %rdi
	call	_ZN12v8_inspector15WasmTranslationD1Ev@PLT
	movq	720(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L204
	movq	(%rdi), %rax
	call	*32(%rax)
.L204:
	movq	680(%rbx), %r12
	testq	%r12, %r12
	jne	.L205
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L355:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L209
.L210:
	movq	%r13, %r12
.L205:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	(%r12), %r13
	cmpq	%rax, %rdi
	jne	.L355
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L210
.L209:
	movq	672(%rbx), %rax
	movq	664(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	664(%rbx), %rdi
	leaq	712(%rbx), %rax
	movq	$0, 688(%rbx)
	movq	$0, 680(%rbx)
	cmpq	%rax, %rdi
	je	.L206
	call	_ZdlPv@PLT
.L206:
	movq	624(%rbx), %r12
	testq	%r12, %r12
	je	.L214
	.p2align 4,,10
	.p2align 3
.L211:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L211
.L214:
	movq	616(%rbx), %rax
	movq	608(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	608(%rbx), %rdi
	leaq	656(%rbx), %rax
	movq	$0, 632(%rbx)
	movq	$0, 624(%rbx)
	cmpq	%rax, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	movq	560(%rbx), %r12
	testq	%r12, %r12
	je	.L228
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L224
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%r12, %r13
	movq	(%r12), %r12
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L226
	lock subl	$1, 12(%rdi)
	jne	.L226
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L226:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L218
.L228:
	movq	552(%rbx), %rax
	movq	544(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	544(%rbx), %rdi
	leaq	592(%rbx), %rax
	movq	$0, 568(%rbx)
	movq	$0, 560(%rbx)
	cmpq	%rax, %rdi
	je	.L216
	call	_ZdlPv@PLT
.L216:
	movq	464(%rbx), %rdi
	leaq	480(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L229
	call	_ZdlPv@PLT
.L229:
	movq	416(%rbx), %r12
	testq	%r12, %r12
	je	.L233
	.p2align 4,,10
	.p2align 3
.L230:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L230
.L233:
	movq	408(%rbx), %rax
	movq	400(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	400(%rbx), %rdi
	leaq	448(%rbx), %rax
	movq	$0, 424(%rbx)
	movq	$0, 416(%rbx)
	cmpq	%rax, %rdi
	je	.L231
	call	_ZdlPv@PLT
.L231:
	movq	360(%rbx), %r12
	testq	%r12, %r12
	je	.L247
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L243
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%r12, %r13
	movq	(%r12), %r12
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L245
	lock subl	$1, 12(%rdi)
	jne	.L245
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L237
.L247:
	movq	352(%rbx), %rax
	movq	344(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	344(%rbx), %rdi
	leaq	392(%rbx), %rax
	movq	$0, 368(%rbx)
	movq	$0, 360(%rbx)
	cmpq	%rax, %rdi
	je	.L235
	call	_ZdlPv@PLT
.L235:
	movq	320(%rbx), %r12
	leaq	320(%rbx), %r14
	cmpq	%r12, %r14
	je	.L261
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L257
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r13
	testq	%r13, %r13
	jne	.L356
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	cmpq	%r12, %r14
	je	.L261
.L251:
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r13
	testq	%r13, %r13
	je	.L259
.L356:
	lock subl	$1, 8(%r13)
	jne	.L259
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L259
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	cmpq	%r12, %r14
	jne	.L251
	.p2align 4,,10
	.p2align 3
.L261:
	movq	288(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L250
	call	_ZdlPv@PLT
.L250:
	movq	272(%rbx), %r14
	movq	264(%rbx), %r12
	cmpq	%r12, %r14
	je	.L262
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L269
	movq	8(%r12), %r13
	testq	%r13, %r13
	jne	.L357
	.p2align 4,,10
	.p2align 3
.L271:
	addq	$16, %r12
	cmpq	%r12, %r14
	je	.L268
.L263:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L271
.L357:
	lock subl	$1, 8(%r13)
	jne	.L271
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L271
	movq	0(%r13), %rax
	addq	$16, %r12
	movq	%r13, %rdi
	call	*24(%rax)
	cmpq	%r12, %r14
	jne	.L263
	.p2align 4,,10
	.p2align 3
.L268:
	movq	264(%rbx), %r12
.L262:
	testq	%r12, %r12
	je	.L273
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L273:
	movq	240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L274
	call	_ZdlPv@PLT
.L274:
	movq	192(%rbx), %r12
	testq	%r12, %r12
	je	.L278
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L275
.L278:
	movq	184(%rbx), %rax
	movq	176(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	176(%rbx), %rdi
	leaq	224(%rbx), %rax
	movq	$0, 200(%rbx)
	movq	$0, 192(%rbx)
	cmpq	%rax, %rdi
	je	.L276
	call	_ZdlPv@PLT
.L276:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L292
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L288
	.p2align 4,,10
	.p2align 3
.L282:
	movq	%r12, %r13
	movq	(%r12), %r12
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L290
	lock subl	$1, 12(%rdi)
	jne	.L290
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L290:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L282
.L292:
	movq	128(%rbx), %rax
	movq	120(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	120(%rbx), %rdi
	leaq	168(%rbx), %rax
	movq	$0, 144(%rbx)
	movq	$0, 136(%rbx)
	cmpq	%rax, %rdi
	je	.L280
	call	_ZdlPv@PLT
.L280:
	movq	112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L293
	movq	(%rdi), %rax
	call	*64(%rax)
.L293:
	movq	72(%rbx), %rdi
	addq	$88, %rbx
	cmpq	%rbx, %rdi
	je	.L203
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movq	%r12, %r13
	movq	(%r12), %r12
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L221
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L221
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L224
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%r12, %r13
	movq	(%r12), %r12
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L240
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L240
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L243
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L255:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L254
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	cmpq	%r12, %r14
	je	.L261
.L257:
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r13
	testq	%r13, %r13
	je	.L254
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L254
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L267:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L266
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L266:
	addq	$16, %r12
	cmpq	%r14, %r12
	je	.L268
.L269:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L266
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L266
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L288:
	movq	%r12, %r13
	movq	(%r12), %r12
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L285
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L285
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L288
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L203:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7717:
	.size	_ZN12v8_inspector10V8DebuggerD2Ev, .-_ZN12v8_inspector10V8DebuggerD2Ev
	.set	.LTHUNK10,_ZN12v8_inspector10V8DebuggerD2Ev
	.section	.text._ZThn8_N12v8_inspector10V8DebuggerD1Ev,"ax",@progbits
	.p2align 4
	.globl	_ZThn8_N12v8_inspector10V8DebuggerD1Ev
	.type	_ZThn8_N12v8_inspector10V8DebuggerD1Ev, @function
_ZThn8_N12v8_inspector10V8DebuggerD1Ev:
.LFB15614:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK10
	.cfi_endproc
.LFE15614:
	.size	_ZThn8_N12v8_inspector10V8DebuggerD1Ev, .-_ZThn8_N12v8_inspector10V8DebuggerD1Ev
	.globl	_ZN12v8_inspector10V8DebuggerD1Ev
	.set	_ZN12v8_inspector10V8DebuggerD1Ev,_ZN12v8_inspector10V8DebuggerD2Ev
	.section	.text._ZN12v8_inspector10V8DebuggerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8DebuggerD0Ev
	.type	_ZN12v8_inspector10V8DebuggerD0Ev, @function
_ZN12v8_inspector10V8DebuggerD0Ev:
.LFB7719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN12v8_inspector10V8DebuggerD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$848, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7719:
	.size	_ZN12v8_inspector10V8DebuggerD0Ev, .-_ZN12v8_inspector10V8DebuggerD0Ev
	.section	.text._ZThn8_N12v8_inspector10V8DebuggerD0Ev,"ax",@progbits
	.p2align 4
	.globl	_ZThn8_N12v8_inspector10V8DebuggerD0Ev
	.type	_ZThn8_N12v8_inspector10V8DebuggerD0Ev, @function
_ZThn8_N12v8_inspector10V8DebuggerD0Ev:
.LFB15604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-8(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN12v8_inspector10V8DebuggerD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$848, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE15604:
	.size	_ZThn8_N12v8_inspector10V8DebuggerD0Ev, .-_ZThn8_N12v8_inspector10V8DebuggerD0Ev
	.section	.text._ZN12v8_inspector10V8Debugger47terminateExecutionCompletedCallbackIgnoringDataEPN2v87IsolateEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger47terminateExecutionCompletedCallbackIgnoringDataEPN2v87IsolateEPv
	.type	_ZN12v8_inspector10V8Debugger47terminateExecutionCompletedCallbackIgnoringDataEPN2v87IsolateEPv, @function
_ZN12v8_inspector10V8Debugger47terminateExecutionCompletedCallbackIgnoringDataEPN2v87IsolateEPv:
.LFB7786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	call	_ZN2v85debug12GetInspectorEPNS_7IsolateE@PLT
	movq	24(%rax), %rbx
	cmpq	$0, 720(%rbx)
	je	.L362
	movq	16(%rbx), %rdi
	leaq	_ZN12v8_inspector10V8Debugger35terminateExecutionCompletedCallbackEPN2v87IsolateE(%rip), %rsi
	call	_ZN2v87Isolate27RemoveCallCompletedCallbackEPFvPS0_E@PLT
	movq	16(%rbx), %rdi
	xorl	%edx, %edx
	leaq	_ZN12v8_inspector10V8Debugger47terminateExecutionCompletedCallbackIgnoringDataEPN2v87IsolateEPv(%rip), %rsi
	call	_ZN2v87Isolate33RemoveMicrotasksCompletedCallbackEPFvPS0_PvES2_@PLT
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate24CancelTerminateExecutionEv@PLT
	movq	720(%rbx), %rdi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	720(%rbx), %rdi
	movq	$0, 720(%rbx)
	testq	%rdi, %rdi
	je	.L362
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7786:
	.size	_ZN12v8_inspector10V8Debugger47terminateExecutionCompletedCallbackIgnoringDataEPN2v87IsolateEPv, .-_ZN12v8_inspector10V8Debugger47terminateExecutionCompletedCallbackIgnoringDataEPN2v87IsolateEPv
	.section	.text._ZN12v8_inspector10V8DebuggerC2EPN2v87IsolateEPNS_15V8InspectorImplE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8DebuggerC2EPN2v87IsolateEPNS_15V8InspectorImplE
	.type	_ZN12v8_inspector10V8DebuggerC2EPN2v87IsolateEPNS_15V8InspectorImplE, @function
_ZN12v8_inspector10V8DebuggerC2EPN2v87IsolateEPNS_15V8InspectorImplE:
.LFB7714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	88+_ZTVN12v8_inspector10V8DebuggerE(%rip), %rax
	movq	%rdx, %xmm4
	pxor	%xmm1, %xmm1
	leaq	-72(%rax), %rcx
	movq	%rax, %xmm3
	xorl	%eax, %eax
	xorl	%edx, %edx
	movq	%rcx, %xmm0
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	punpcklqdq	%xmm3, %xmm0
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movw	%ax, 56(%rdi)
	leaq	88(%rdi), %rax
	movq	%rax, 72(%rdi)
	leaq	168(%rdi), %rax
	movups	%xmm0, (%rdi)
	movq	%rsi, %xmm0
	movq	%rax, 120(%rdi)
	punpcklqdq	%xmm4, %xmm0
	leaq	224(%rdi), %rax
	movq	%rax, 176(%rdi)
	leaq	320(%rdi), %rax
	movups	%xmm0, 16(%rdi)
	movss	.LC0(%rip), %xmm0
	movq	%rax, %xmm2
	leaq	392(%rdi), %rax
	movq	$0, 32(%rdi)
	punpcklqdq	%xmm2, %xmm2
	movss	%xmm0, 152(%rdi)
	movss	%xmm0, 208(%rdi)
	movl	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movl	$0, 60(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 80(%rdi)
	movw	%dx, 88(%rdi)
	movq	$0, 104(%rdi)
	movq	$0, 112(%rdi)
	movq	$1, 128(%rdi)
	movq	$0, 136(%rdi)
	movq	$0, 144(%rdi)
	movq	$0, 160(%rdi)
	movq	$0, 168(%rdi)
	movq	$1, 184(%rdi)
	movq	$0, 192(%rdi)
	movq	$0, 200(%rdi)
	movq	$0, 216(%rdi)
	movq	$0, 224(%rdi)
	movq	$131072, 232(%rdi)
	movups	%xmm1, 240(%rdi)
	movq	%rax, 344(%rdi)
	leaq	448(%rdi), %rax
	movq	%rax, 400(%rdi)
	leaq	480(%rdi), %rax
	leaq	520(%rdi), %rdi
	movq	%rax, -56(%rdi)
	movq	$0, -216(%rdi)
	movl	$0, -208(%rdi)
	movq	$0, -184(%rdi)
	movq	$1, -168(%rdi)
	movq	$0, -160(%rdi)
	movq	$0, -152(%rdi)
	movq	$0, -136(%rdi)
	movq	$0, -128(%rdi)
	movq	$1, -112(%rdi)
	movq	$0, -104(%rdi)
	movq	$0, -96(%rdi)
	movq	$0, -80(%rdi)
	movq	$0, -48(%rdi)
	movw	%cx, -40(%rdi)
	movq	$0, -24(%rdi)
	movb	$0, -16(%rdi)
	movl	$0, -12(%rdi)
	movb	$0, -8(%rdi)
	movups	%xmm1, -264(%rdi)
	movups	%xmm1, -248(%rdi)
	movups	%xmm1, -232(%rdi)
	movss	%xmm0, -144(%rdi)
	movss	%xmm0, -88(%rdi)
	movups	%xmm1, -72(%rdi)
	movups	%xmm2, -200(%rdi)
	call	_ZN12v8_inspector14V8StackTraceIdC1Ev@PLT
	leaq	592(%rbx), %rax
	pxor	%xmm1, %xmm1
	movss	.LC0(%rip), %xmm0
	movq	%rax, 544(%rbx)
	movq	%r12, %rsi
	leaq	656(%rbx), %rax
	leaq	728(%rbx), %rdi
	movq	%rax, 608(%rbx)
	leaq	712(%rbx), %rax
	movq	$1, 552(%rbx)
	movq	$0, 560(%rbx)
	movq	$0, 568(%rbx)
	movq	$0, 584(%rbx)
	movq	$0, 592(%rbx)
	movq	$0, 600(%rbx)
	movq	$1, 616(%rbx)
	movq	$0, 624(%rbx)
	movq	$0, 632(%rbx)
	movq	$0, 648(%rbx)
	movq	$0, 656(%rbx)
	movq	%rax, 664(%rbx)
	movq	$1, 672(%rbx)
	movq	$0, 680(%rbx)
	movq	$0, 688(%rbx)
	movq	$0, 704(%rbx)
	movss	%xmm0, 576(%rbx)
	movss	%xmm0, 640(%rbx)
	movss	%xmm0, 696(%rbx)
	movups	%xmm1, 712(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector15WasmTranslationC1EPN2v87IsolateE@PLT
	.cfi_endproc
.LFE7714:
	.size	_ZN12v8_inspector10V8DebuggerC2EPN2v87IsolateEPNS_15V8InspectorImplE, .-_ZN12v8_inspector10V8DebuggerC2EPN2v87IsolateEPNS_15V8InspectorImplE
	.globl	_ZN12v8_inspector10V8DebuggerC1EPN2v87IsolateEPNS_15V8InspectorImplE
	.set	_ZN12v8_inspector10V8DebuggerC1EPN2v87IsolateEPNS_15V8InspectorImplE,_ZN12v8_inspector10V8DebuggerC2EPN2v87IsolateEPNS_15V8InspectorImplE
	.section	.text._ZN12v8_inspector10V8Debugger6enableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger6enableEv
	.type	_ZN12v8_inspector10V8Debugger6enableEv, @function
_ZN12v8_inspector10V8Debugger6enableEv:
.LFB7720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	32(%rdi), %eax
	leal	1(%rax), %edx
	movl	%edx, 32(%rdi)
	testl	%eax, %eax
	jne	.L372
	movq	16(%rdi), %rsi
	leaq	-48(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rdi
	movq	%rbx, %rsi
	call	_ZN2v85debug16SetDebugDelegateEPNS_7IsolateEPNS0_13DebugDelegateE@PLT
	movq	16(%rbx), %rdi
	movq	%rbx, %rdx
	leaq	_ZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmm(%rip), %rsi
	call	_ZN2v87Isolate24AddNearHeapLimitCallbackEPFmPvmmES1_@PLT
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v85debug22ChangeBreakOnExceptionEPNS_7IsolateENS0_19ExceptionBreakStateE@PLT
	movq	%r12, %rdi
	movl	$0, 508(%rbx)
	call	_ZN2v811HandleScopeD1Ev@PLT
.L372:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L377
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L377:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7720:
	.size	_ZN12v8_inspector10V8Debugger6enableEv, .-_ZN12v8_inspector10V8Debugger6enableEv
	.section	.text._ZN12v8_inspector10V8Debugger7disableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger7disableEv
	.type	_ZN12v8_inspector10V8Debugger7disableEv, @function
_ZN12v8_inspector10V8Debugger7disableEv:
.LFB7721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$112, %rsp
	movl	64(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.L399
.L379:
	subl	$1, 32(%rbx)
	je	.L400
.L378:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L401
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movl	68(%rbx), %esi
	testl	%esi, %esi
	je	.L384
	movq	16(%rbx), %rdi
	call	_ZN2v85debug16RemoveBreakpointEPNS_7IsolateEi@PLT
	movq	72(%rbx), %rdx
	xorl	%edi, %edi
	leaq	-48(%rbp), %rax
	xorl	%r8d, %r8d
	movq	%rax, -64(%rbp)
	xorl	%r9d, %r9d
	movw	%di, -48(%rbp)
	movq	$0, -32(%rbp)
	movl	$0, 68(%rbx)
	movq	$0, 80(%rbx)
	movw	%r8w, (%rdx)
	movq	-64(%rbp), %rdx
	movq	$0, -56(%rbp)
	movw	%r9w, (%rdx)
	movq	-32(%rbp), %rdx
	movq	-64(%rbp), %rdi
	movq	%rdx, 104(%rbx)
	cmpq	%rax, %rdi
	je	.L385
	call	_ZdlPv@PLT
.L385:
	movq	112(%rbx), %rdi
	movq	$0, 112(%rbx)
	testq	%rdi, %rdi
	je	.L384
	movq	(%rdi), %rax
	call	*64(%rax)
.L384:
	xorl	%edx, %edx
	leaq	-96(%rbp), %rax
	xorl	%ecx, %ecx
	movq	$0, -80(%rbp)
	movw	%dx, -96(%rbp)
	xorl	%esi, %esi
	movq	464(%rbx), %rdx
	movq	%rax, -112(%rbp)
	movq	$0, 456(%rbx)
	movq	$0, 472(%rbx)
	movw	%cx, (%rdx)
	movq	-112(%rbp), %rdx
	movq	$0, -104(%rbp)
	movw	%si, (%rdx)
	movq	-80(%rbp), %rdx
	movq	-112(%rbp), %rdi
	movq	%rdx, 496(%rbx)
	cmpq	%rax, %rdi
	je	.L387
	call	_ZdlPv@PLT
.L387:
	movb	$0, 512(%rbx)
	leaq	728(%rbx), %rdi
	call	_ZN12v8_inspector15WasmTranslation5ClearEv@PLT
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v85debug16SetDebugDelegateEPNS_7IsolateEPNS0_13DebugDelegateE@PLT
	movq	48(%rbx), %rdx
	movq	16(%rbx), %rdi
	leaq	_ZN12v8_inspector10V8Debugger21nearHeapLimitCallbackEPvmm(%rip), %rsi
	call	_ZN2v87Isolate27RemoveNearHeapLimitCallbackEPFmPvmmEm@PLT
	movq	$0, 48(%rbx)
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L399:
	movzbl	56(%rdi), %eax
	leaq	-64(%rbp), %r12
	movq	24(%rdi), %rdi
	movb	$0, -113(%rbp)
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger7disableEvEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation(%rip), %rcx
	movq	%r12, %rdx
	movb	%al, -114(%rbp)
	leaq	-114(%rbp), %rax
	movq	%rax, %xmm0
	leaq	-113(%rbp), %rax
	movq	%rax, %xmm1
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger7disableEvEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %xmm2
	movaps	%xmm0, -64(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L380
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L380:
	cmpb	$0, -113(%rbp)
	jne	.L379
	movq	24(%rbx), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv(%rip), %rdx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L379
	call	*%rax
	jmp	.L379
.L401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7721:
	.size	_ZN12v8_inspector10V8Debugger7disableEv, .-_ZN12v8_inspector10V8Debugger7disableEv
	.section	.text._ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi
	.type	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi, @function
_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi:
.LFB7730:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %edx
	cmpl	%edx, %esi
	sete	%al
	testl	%edx, %edx
	setne	%dl
	andl	%edx, %eax
	ret
	.cfi_endproc
.LFE7730:
	.size	_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi, .-_ZNK12v8_inspector10V8Debugger22isPausedInContextGroupEi
	.section	.text._ZNK12v8_inspector10V8Debugger7enabledEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector10V8Debugger7enabledEv
	.type	_ZNK12v8_inspector10V8Debugger7enabledEv, @function
_ZNK12v8_inspector10V8Debugger7enabledEv:
.LFB7731:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	testl	%eax, %eax
	setg	%al
	ret
	.cfi_endproc
.LFE7731:
	.size	_ZNK12v8_inspector10V8Debugger7enabledEv, .-_ZNK12v8_inspector10V8Debugger7enabledEv
	.section	.rodata._ZN12v8_inspector10V8Debugger20setBreakpointsActiveEb.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
	.section	.text._ZN12v8_inspector10V8Debugger20setBreakpointsActiveEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger20setBreakpointsActiveEb
	.type	_ZN12v8_inspector10V8Debugger20setBreakpointsActiveEb, @function
_ZN12v8_inspector10V8Debugger20setBreakpointsActiveEb:
.LFB7748:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %eax
	testl	%eax, %eax
	jle	.L411
	cmpb	$1, %sil
	sbbl	%eax, %eax
	xorl	%esi, %esi
	orl	$1, %eax
	addl	%eax, 36(%rdi)
	movq	16(%rdi), %rdi
	setne	%sil
	jmp	_ZN2v85debug20SetBreakPointsActiveEPNS_7IsolateEb@PLT
.L411:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE7748:
	.size	_ZN12v8_inspector10V8Debugger20setBreakpointsActiveEb, .-_ZN12v8_inspector10V8Debugger20setBreakpointsActiveEb
	.section	.text._ZN12v8_inspector10V8Debugger25getPauseOnExceptionsStateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger25getPauseOnExceptionsStateEv
	.type	_ZN12v8_inspector10V8Debugger25getPauseOnExceptionsStateEv, @function
_ZN12v8_inspector10V8Debugger25getPauseOnExceptionsStateEv:
.LFB7749:
	.cfi_startproc
	endbr64
	movl	508(%rdi), %eax
	ret
	.cfi_endproc
.LFE7749:
	.size	_ZN12v8_inspector10V8Debugger25getPauseOnExceptionsStateEv, .-_ZN12v8_inspector10V8Debugger25getPauseOnExceptionsStateEv
	.section	.text._ZN12v8_inspector10V8Debugger25setPauseOnExceptionsStateEN2v85debug19ExceptionBreakStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger25setPauseOnExceptionsStateEN2v85debug19ExceptionBreakStateE
	.type	_ZN12v8_inspector10V8Debugger25setPauseOnExceptionsStateEN2v85debug19ExceptionBreakStateE, @function
_ZN12v8_inspector10V8Debugger25setPauseOnExceptionsStateEN2v85debug19ExceptionBreakStateE:
.LFB7750:
	.cfi_startproc
	endbr64
	cmpl	%esi, 508(%rdi)
	je	.L416
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	_ZN2v85debug22ChangeBreakOnExceptionEPNS_7IsolateENS0_19ExceptionBreakStateE@PLT
	movl	%r12d, 508(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE7750:
	.size	_ZN12v8_inspector10V8Debugger25setPauseOnExceptionsStateEN2v85debug19ExceptionBreakStateE, .-_ZN12v8_inspector10V8Debugger25setPauseOnExceptionsStateEN2v85debug19ExceptionBreakStateE
	.section	.text._ZN12v8_inspector10V8Debugger18setPauseOnNextCallEbi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger18setPauseOnNextCallEbi
	.type	_ZN12v8_inspector10V8Debugger18setPauseOnNextCallEbi, @function
_ZN12v8_inspector10V8Debugger18setPauseOnNextCallEbi:
.LFB7751:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	testl	%eax, %eax
	jne	.L419
	testb	%sil, %sil
	jne	.L421
	movl	60(%rdi), %eax
	testl	%eax, %eax
	je	.L422
	cmpl	%edx, %eax
	jne	.L429
.L422:
	movl	%edx, 60(%rdi)
	movb	$0, 504(%rdi)
	movq	16(%rdi), %rdi
	jmp	_ZN2v85debug28ClearBreakOnNextFunctionCallEPNS_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L421:
	movl	%edx, 60(%rdi)
	movb	$1, 504(%rdi)
	movq	16(%rdi), %rdi
	jmp	_ZN2v85debug26SetBreakOnNextFunctionCallEPNS_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L419:
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	ret
	.cfi_endproc
.LFE7751:
	.size	_ZN12v8_inspector10V8Debugger18setPauseOnNextCallEbi, .-_ZN12v8_inspector10V8Debugger18setPauseOnNextCallEbi
	.section	.text._ZN12v8_inspector10V8Debugger15canBreakProgramEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger15canBreakProgramEv
	.type	_ZN12v8_inspector10V8Debugger15canBreakProgramEv, @function
_ZN12v8_inspector10V8Debugger15canBreakProgramEv:
.LFB7752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v85debug29AllFramesOnStackAreBlackboxedEPNS_7IsolateE@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE7752:
	.size	_ZN12v8_inspector10V8Debugger15canBreakProgramEv, .-_ZN12v8_inspector10V8Debugger15canBreakProgramEv
	.section	.text._ZN12v8_inspector10V8Debugger12breakProgramEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger12breakProgramEi
	.type	_ZN12v8_inspector10V8Debugger12breakProgramEi, @function
_ZN12v8_inspector10V8Debugger12breakProgramEi:
.LFB7753:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	testl	%eax, %eax
	je	.L434
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	movl	%esi, 60(%rdi)
	movq	16(%rdi), %rdi
	jmp	_ZN2v85debug13BreakRightNowEPNS_7IsolateE@PLT
	.cfi_endproc
.LFE7753:
	.size	_ZN12v8_inspector10V8Debugger12breakProgramEi, .-_ZN12v8_inspector10V8Debugger12breakProgramEi
	.section	.text._ZN12v8_inspector10V8Debugger17interruptAndBreakEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger17interruptAndBreakEi
	.type	_ZN12v8_inspector10V8Debugger17interruptAndBreakEi, @function
_ZN12v8_inspector10V8Debugger17interruptAndBreakEi:
.LFB7754:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	testl	%eax, %eax
	je	.L437
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	movl	%esi, 60(%rdi)
	movq	16(%rdi), %rdi
	xorl	%edx, %edx
	leaq	_ZZN12v8_inspector10V8Debugger17interruptAndBreakEiENUlPN2v87IsolateEPvE_4_FUNES3_S4_(%rip), %rsi
	jmp	_ZN2v87Isolate16RequestInterruptEPFvPS0_PvES2_@PLT
	.cfi_endproc
.LFE7754:
	.size	_ZN12v8_inspector10V8Debugger17interruptAndBreakEi, .-_ZN12v8_inspector10V8Debugger17interruptAndBreakEi
	.section	.text._ZN12v8_inspector10V8Debugger15continueProgramEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger15continueProgramEi
	.type	_ZN12v8_inspector10V8Debugger15continueProgramEi, @function
_ZN12v8_inspector10V8Debugger15continueProgramEi:
.LFB7758:
	.cfi_startproc
	endbr64
	movl	64(%rdi), %eax
	testl	%eax, %eax
	je	.L438
	cmpl	%esi, %eax
	je	.L446
.L438:
	ret
	.p2align 4,,10
	.p2align 3
.L446:
	movq	24(%rdi), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv(%rip), %rdx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L438
	jmp	*%rax
	.cfi_endproc
.LFE7758:
	.size	_ZN12v8_inspector10V8Debugger15continueProgramEi, .-_ZN12v8_inspector10V8Debugger15continueProgramEi
	.section	.text._ZN12v8_inspector10V8Debugger20breakProgramOnAssertEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger20breakProgramOnAssertEi
	.type	_ZN12v8_inspector10V8Debugger20breakProgramOnAssertEi, @function
_ZN12v8_inspector10V8Debugger20breakProgramOnAssertEi:
.LFB7759:
	.cfi_startproc
	endbr64
	movl	32(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L452
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	508(%rdi), %edx
	movq	%rdi, %rbx
	testl	%edx, %edx
	je	.L447
	movl	64(%rdi), %eax
	testl	%eax, %eax
	jne	.L447
	movq	16(%rdi), %rdi
	movl	%esi, %r12d
	call	_ZN2v85debug29AllFramesOnStackAreBlackboxedEPNS_7IsolateE@PLT
	testb	%al, %al
	je	.L455
.L447:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	%r12d, 60(%rbx)
	movq	16(%rbx), %rdi
	movb	$1, 57(%rbx)
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v85debug13BreakRightNowEPNS_7IsolateE@PLT
	.cfi_endproc
.LFE7759:
	.size	_ZN12v8_inspector10V8Debugger20breakProgramOnAssertEi, .-_ZN12v8_inspector10V8Debugger20breakProgramOnAssertEi
	.section	.rodata._ZN12v8_inspector10V8Debugger16pauseOnAsyncCallEimRKNS_8String16E.str1.1,"aMS",@progbits,1
.LC2:
	.string	"basic_string::_M_create"
	.section	.text._ZN12v8_inspector10V8Debugger16pauseOnAsyncCallEimRKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger16pauseOnAsyncCallEimRKNS_8String16E
	.type	_ZN12v8_inspector10V8Debugger16pauseOnAsyncCallEimRKNS_8String16E, @function
_ZN12v8_inspector10V8Debugger16pauseOnAsyncCallEimRKNS_8String16E:
.LFB7781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	464(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	%esi, 60(%rdi)
	movq	%rdx, 456(%rdi)
	cmpq	%rax, %rcx
	je	.L457
	movq	464(%rdi), %r14
	leaq	480(%rdi), %rdx
	movq	8(%rcx), %r13
	cmpq	%rdx, %r14
	je	.L466
	movq	480(%rdi), %rax
.L458:
	cmpq	%rax, %r13
	ja	.L476
	leaq	(%r13,%r13), %rdx
	testq	%r13, %r13
	je	.L462
.L479:
	movq	(%r12), %rsi
	cmpq	$1, %r13
	je	.L477
	testq	%rdx, %rdx
	je	.L462
	movq	%r14, %rdi
	call	memmove@PLT
	movq	464(%rbx), %r14
	.p2align 4,,10
	.p2align 3
.L462:
	xorl	%eax, %eax
	movq	%r13, 472(%rbx)
	movw	%ax, (%r14,%r13,2)
.L457:
	movq	32(%r12), %rax
	movq	%rax, 496(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r13
	ja	.L478
	addq	%rax, %rax
	movq	%r13, %r15
	cmpq	%rax, %r13
	jnb	.L461
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r15
.L461:
	leaq	2(%r15,%r15), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	464(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r14
	cmpq	%rdi, %rdx
	je	.L465
	call	_ZdlPv@PLT
.L465:
	movq	%r14, 464(%rbx)
	leaq	(%r13,%r13), %rdx
	movq	%r15, 480(%rbx)
	testq	%r13, %r13
	je	.L462
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L466:
	movl	$7, %eax
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L477:
	movzwl	(%rsi), %eax
	movw	%ax, (%r14)
	movq	464(%rbx), %r14
	jmp	.L462
.L478:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7781:
	.size	_ZN12v8_inspector10V8Debugger16pauseOnAsyncCallEimRKNS_8String16E, .-_ZN12v8_inspector10V8Debugger16pauseOnAsyncCallEimRKNS_8String16E
	.section	.rodata._ZN12v8_inspector10V8Debugger18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"There is current termination request in progress"
	.section	.text._ZN12v8_inspector10V8Debugger18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE
	.type	_ZN12v8_inspector10V8Debugger18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE, @function
_ZN12v8_inspector10V8Debugger18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE:
.LFB7782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 720(%rdi)
	je	.L481
	testq	%r12, %r12
	je	.L480
	movq	(%r12), %rax
	leaq	-144(%rbp), %r14
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	leaq	-96(%rbp), %r13
	movq	8(%rax), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	*%rbx
	movq	-88(%rbp), %rdi
	leaq	-72(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L483
	call	_ZdlPv@PLT
.L483:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L480
	call	_ZdlPv@PLT
.L480:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L490
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	movq	%r12, 720(%rdi)
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	movq	$0, (%rsi)
	leaq	_ZN12v8_inspector10V8Debugger35terminateExecutionCompletedCallbackEPN2v87IsolateE(%rip), %rsi
	call	_ZN2v87Isolate24AddCallCompletedCallbackEPFvPS0_E@PLT
	movq	16(%rbx), %rdi
	xorl	%edx, %edx
	leaq	_ZN12v8_inspector10V8Debugger47terminateExecutionCompletedCallbackIgnoringDataEPN2v87IsolateEPv(%rip), %rsi
	call	_ZN2v87Isolate30AddMicrotasksCompletedCallbackEPFvPS0_PvES2_@PLT
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate18TerminateExecutionEv@PLT
	jmp	.L480
.L490:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7782:
	.size	_ZN12v8_inspector10V8Debugger18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE, .-_ZN12v8_inspector10V8Debugger18terminateExecutionESt10unique_ptrINS_8protocol7Runtime7Backend26TerminateExecutionCallbackESt14default_deleteIS5_EE
	.section	.text._ZN12v8_inspector10V8Debugger17reportTerminationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger17reportTerminationEv
	.type	_ZN12v8_inspector10V8Debugger17reportTerminationEv, @function
_ZN12v8_inspector10V8Debugger17reportTerminationEv:
.LFB7784:
	.cfi_startproc
	endbr64
	cmpq	$0, 720(%rdi)
	je	.L499
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN12v8_inspector10V8Debugger35terminateExecutionCompletedCallbackEPN2v87IsolateE(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	call	_ZN2v87Isolate27RemoveCallCompletedCallbackEPFvPS0_E@PLT
	movq	16(%rbx), %rdi
	xorl	%edx, %edx
	leaq	_ZN12v8_inspector10V8Debugger47terminateExecutionCompletedCallbackIgnoringDataEPN2v87IsolateEPv(%rip), %rsi
	call	_ZN2v87Isolate33RemoveMicrotasksCompletedCallbackEPFvPS0_PvES2_@PLT
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate24CancelTerminateExecutionEv@PLT
	movq	720(%rbx), %rdi
	movq	(%rdi), %rax
	call	*(%rax)
	movq	720(%rbx), %rdi
	movq	$0, 720(%rbx)
	testq	%rdi, %rdi
	je	.L491
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L491:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE7784:
	.size	_ZN12v8_inspector10V8Debugger17reportTerminationEv, .-_ZN12v8_inspector10V8Debugger17reportTerminationEv
	.section	.text._ZN12v8_inspector10V8Debugger23clearContinueToLocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger23clearContinueToLocationEv
	.type	_ZN12v8_inspector10V8Debugger23clearContinueToLocationEv, @function
_ZN12v8_inspector10V8Debugger23clearContinueToLocationEv:
.LFB7789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -24
	movl	68(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.L512
.L502:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L513
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	_ZN2v85debug16RemoveBreakpointEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	leaq	-48(%rbp), %rax
	xorl	%ecx, %ecx
	movw	%dx, -48(%rbp)
	movq	72(%rbx), %rdx
	xorl	%esi, %esi
	movq	%rax, -64(%rbp)
	movq	$0, -32(%rbp)
	movl	$0, 68(%rbx)
	movq	$0, 80(%rbx)
	movw	%cx, (%rdx)
	movq	-64(%rbp), %rdx
	movq	$0, -56(%rbp)
	movw	%si, (%rdx)
	movq	-32(%rbp), %rdx
	movq	-64(%rbp), %rdi
	movq	%rdx, 104(%rbx)
	cmpq	%rax, %rdi
	je	.L505
	call	_ZdlPv@PLT
.L505:
	movq	112(%rbx), %rdi
	movq	$0, 112(%rbx)
	testq	%rdi, %rdi
	je	.L502
	movq	(%rdi), %rax
	call	*64(%rax)
	jmp	.L502
.L513:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7789:
	.size	_ZN12v8_inspector10V8Debugger23clearContinueToLocationEv, .-_ZN12v8_inspector10V8Debugger23clearContinueToLocationEv
	.section	.text._ZN12v8_inspector10V8Debugger18currentAsyncParentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger18currentAsyncParentEv
	.type	_ZN12v8_inspector10V8Debugger18currentAsyncParentEv, @function
_ZN12v8_inspector10V8Debugger18currentAsyncParentEv:
.LFB7818:
	.cfi_startproc
	endbr64
	movq	272(%rsi), %rdx
	movq	%rdi, %rax
	cmpq	%rdx, 264(%rsi)
	je	.L521
	movq	-16(%rdx), %rcx
	movq	%rcx, (%rdi)
	movq	-8(%rdx), %rdx
	movq	%rdx, 8(%rdi)
	testq	%rdx, %rdx
	je	.L514
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L517
	lock addl	$1, 8(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	addl	$1, 8(%rdx)
.L514:
	ret
	.p2align 4,,10
	.p2align 3
.L521:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	ret
	.cfi_endproc
.LFE7818:
	.size	_ZN12v8_inspector10V8Debugger18currentAsyncParentEv, .-_ZN12v8_inspector10V8Debugger18currentAsyncParentEv
	.section	.text._ZN12v8_inspector10V8Debugger21currentExternalParentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger21currentExternalParentEv
	.type	_ZN12v8_inspector10V8Debugger21currentExternalParentEv, @function
_ZN12v8_inspector10V8Debugger21currentExternalParentEv:
.LFB7825:
	.cfi_startproc
	endbr64
	movq	296(%rsi), %rdx
	movq	%rdi, %rax
	cmpq	%rdx, 288(%rsi)
	je	.L529
	movdqu	-24(%rdx), %xmm0
	movups	%xmm0, (%rdi)
	movq	-8(%rdx), %rdx
	movq	%rdx, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	call	_ZN12v8_inspector14V8StackTraceIdC1Ev@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7825:
	.size	_ZN12v8_inspector10V8Debugger21currentExternalParentEv, .-_ZN12v8_inspector10V8Debugger21currentExternalParentEv
	.section	.rodata._ZN12v8_inspector10V8Debugger18collectionsEntriesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"value"
.LC5:
	.string	"key"
	.section	.rodata._ZN12v8_inspector10V8Debugger18collectionsEntriesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"!isKeyValue || wrappedEntries->Length() % 2 == 0"
	.section	.rodata._ZN12v8_inspector10V8Debugger18collectionsEntriesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE.str1.1
.LC7:
	.string	"Check failed: %s."
	.section	.text._ZN12v8_inspector10V8Debugger18collectionsEntriesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger18collectionsEntriesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	.type	_ZN12v8_inspector10V8Debugger18collectionsEntriesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE, @function
_ZN12v8_inspector10V8Debugger18collectionsEntriesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE:
.LFB7856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rsi, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r12, %rdi
	movb	$0, -57(%rbp)
	movq	%rax, %r13
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L572
.L532:
	xorl	%eax, %eax
.L535:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L573
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	.cfi_restore_state
	movq	%r12, %rdi
	leaq	-57(%rbp), %rsi
	call	_ZN2v86Object14PreviewEntriesEPb@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L532
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	cmpb	$0, -57(%rbp)
	movq	%rax, -88(%rbp)
	jne	.L574
.L555:
	leaq	104(%r13), %rax
	movq	-88(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%rax, -96(%rbp)
	call	_ZN2v86Object12SetPrototypeENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L532
	shrw	$8, %ax
	je	.L532
	movq	%r13, -104(%rbp)
	xorl	%r14d, %r14d
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L569:
	cmpb	$0, -57(%rbp)
	movq	$0, -80(%rbp)
	je	.L552
	leal	1(%r14), %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L540
.L552:
	movq	-104(%rbp), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	-96(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v86Object12SetPrototypeENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movzbl	-57(%rbp), %edx
	testb	%al, %al
	je	.L549
	shrw	$8, %ax
	je	.L549
	testb	%dl, %dl
	leaq	.LC4(%rip), %rax
	leaq	.LC5(%rip), %rsi
	movq	-104(%rbp), %rdi
	cmove	%rax, %rsi
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_6ObjectEEENS1_INS0_4NameEEENS1_INS0_5ValueEEE@PLT
	cmpb	$0, -57(%rbp)
	jne	.L575
.L546:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movl	%eax, %esi
	movq	-72(%rbp), %rax
	movq	24(%rax), %rdi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L540
	movl	$1, %edx
	movq	%r15, %rsi
	call	_ZN12v8_inspector16InspectedContext17addInternalObjectEN2v85LocalINS1_6ObjectEEENS_19V8InternalValueTypeE@PLT
	testb	%al, %al
	je	.L540
	movq	-88(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	%eax, %edx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE@PLT
	movzbl	-57(%rbp), %edx
	.p2align 4,,10
	.p2align 3
.L549:
	xorl	%eax, %eax
	testb	%dl, %dl
	setne	%al
	leal	1(%r14,%rax), %r14d
.L551:
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%r14d, %eax
	jbe	.L539
	movl	%r14d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L569
.L540:
	movzbl	-57(%rbp), %edx
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L574:
	movq	%rax, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	testb	$1, %al
	je	.L555
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L539:
	movq	-88(%rbp), %rax
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L575:
	movq	-104(%rbp), %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	movq	-80(%rbp), %rcx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_6ObjectEEENS1_INS0_4NameEEENS1_INS0_5ValueEEE@PLT
	jmp	.L546
.L573:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7856:
	.size	_ZN12v8_inspector10V8Debugger18collectionsEntriesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE, .-_ZN12v8_inspector10V8Debugger18collectionsEntriesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	.section	.text._ZN12v8_inspector10V8Debugger12queryObjectsEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger12queryObjectsEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	.type	_ZN12v8_inspector10V8Debugger12queryObjectsEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE, @function
_ZN12v8_inspector10V8Debugger12queryObjectsEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE:
.LFB7858:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r14, -56(%rbp)
	leaq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %r13
	movq	%rax, -112(%rbp)
	movq	24(%rbx), %rax
	leaq	16+_ZTVN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateE(%rip), %rcx
	leaq	-80(%rbp), %rsi
	leaq	-144(%rbp), %r14
	movq	%rcx, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r12, -64(%rbp)
	call	_ZN2v85debug12QueryObjectsENS_5LocalINS_7ContextEEEPNS0_20QueryObjectPredicateEPNS_21PersistentValueVectorINS_6ObjectENS_34DefaultPersistentValueVectorTraitsEEE@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	movq	24(%rbx), %rax
	movq	-96(%rbp), %rsi
	xorl	%ebx, %ebx
	subq	-104(%rbp), %rsi
	movq	8(%rax), %rdi
	sarq	$3, %rsi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	-104(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdx, -96(%rbp)
	je	.L582
	.p2align 4,,10
	.p2align 3
.L577:
	movq	(%rdx,%rbx,8), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.L601
	movq	(%rax), %rsi
	movq	-112(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rcx
.L601:
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE@PLT
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rax
	subq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.L577
.L582:
	movq	%r14, %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %rdi
	movq	%rdx, %r12
	subq	%rdi, %r12
	sarq	$3, %r12
	je	.L578
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L579:
	movq	%rdx, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jnb	.L583
	movq	(%rdi,%rbx,8), %r8
	testq	%r8, %r8
	je	.L583
	movq	%r8, %rdi
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-104(%rbp), %rdi
	movq	-96(%rbp), %rdx
.L583:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	jne	.L579
.L578:
	cmpq	%rdx, %rdi
	je	.L584
	movq	%rdi, -96(%rbp)
.L584:
	testq	%rdi, %rdi
	je	.L585
	call	_ZdlPv@PLT
.L585:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L602
	addq	$112, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L602:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7858:
	.size	_ZN12v8_inspector10V8Debugger12queryObjectsEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE, .-_ZN12v8_inspector10V8Debugger12queryObjectsEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	.section	.text._ZN12v8_inspector10V8Debugger16createStackTraceEN2v85LocalINS1_10StackTraceEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger16createStackTraceEN2v85LocalINS1_10StackTraceEEE
	.type	_ZN12v8_inspector10V8Debugger16createStackTraceEN2v85LocalINS1_10StackTraceEEE, @function
_ZN12v8_inspector10V8Debugger16createStackTraceEN2v85LocalINS1_10StackTraceEEE:
.LFB7859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	16(%rsi), %rdi
	movl	_ZN12v8_inspector16V8StackTraceImpl25maxCallStackSizeToCaptureE(%rip), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L604
	leaq	-80(%rbp), %r8
	movq	16(%r12), %rsi
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rdi
	movq	24(%r12), %r15
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-88(%rbp), %r8
	movl	%eax, %r15d
	movq	%r8, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L604:
	movl	%ebx, %r8d
	movq	%r14, %rcx
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector16V8StackTraceImpl6createEPNS_10V8DebuggerEiN2v85LocalINS3_10StackTraceEEEi@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L610
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L610:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7859:
	.size	_ZN12v8_inspector10V8Debugger16createStackTraceEN2v85LocalINS1_10StackTraceEEE, .-_ZN12v8_inspector10V8Debugger16createStackTraceEN2v85LocalINS1_10StackTraceEEE
	.section	.text._ZN12v8_inspector10V8Debugger25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE
	.type	_ZN12v8_inspector10V8Debugger25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE, @function
_ZN12v8_inspector10V8Debugger25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE:
.LFB7866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	236(%rdi), %r8d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L611
	movq	296(%rdi), %rax
	movq	%rdi, %rbx
	cmpq	%rax, 288(%rdi)
	je	.L611
	subq	$24, %rax
	movq	%rax, 296(%rdi)
	movq	272(%rdi), %rax
	leaq	-16(%rax), %rdx
	movq	%rdx, 272(%rdi)
	movq	-8(%rax), %r12
	testq	%r12, %r12
	je	.L616
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L617
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L618:
	cmpl	$1, %eax
	jne	.L616
	movq	(%r12), %rax
	movq	%rsi, -88(%rbp)
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	movq	-88(%rbp), %rsi
	je	.L620
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L621:
	cmpl	$1, %eax
	je	.L636
	.p2align 4,,10
	.p2align 3
.L616:
	subq	$8, 248(%rbx)
	cmpq	$0, 472(%rbx)
	jne	.L637
	.p2align 4,,10
	.p2align 3
.L611:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L638
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	.cfi_restore_state
	movq	(%rsi), %rax
	cmpq	%rax, 456(%rbx)
	jne	.L611
	addq	$8, %rsi
	leaq	-80(%rbp), %rdi
	call	_ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE@PLT
	movq	472(%rbx), %rsi
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rdi
	movq	464(%rbx), %rdx
	cmpq	%r8, %rsi
	movq	%r8, %rcx
	cmovbe	%rsi, %rcx
	testq	%rcx, %rcx
	je	.L623
	xorl	%eax, %eax
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L639:
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L623
.L625:
	movzwl	(%rdi,%rax,2), %r9d
	cmpw	%r9w, (%rdx,%rax,2)
	je	.L639
.L624:
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L611
	call	_ZdlPv@PLT
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L617:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L636:
	movq	(%r12), %rax
	movq	%rsi, -88(%rbp)
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-88(%rbp), %rsi
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L620:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L623:
	subq	%r8, %rsi
	cmpq	$2147483647, %rsi
	jg	.L624
	cmpq	$-2147483648, %rsi
	jl	.L624
	testl	%esi, %esi
	jne	.L624
	leaq	-64(%rbp), %r12
	cmpq	%r12, %rdi
	je	.L627
	call	_ZdlPv@PLT
	movq	464(%rbx), %rdx
.L627:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	movq	%r12, -80(%rbp)
	xorl	%esi, %esi
	movw	%ax, -64(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, 456(%rbx)
	movq	$0, 472(%rbx)
	movw	%cx, (%rdx)
	movq	-80(%rbp), %rax
	movq	$0, -72(%rbp)
	movw	%si, (%rax)
	movq	-48(%rbp), %rax
	movq	-80(%rbp), %rdi
	movq	%rax, 496(%rbx)
	cmpq	%r12, %rdi
	je	.L628
	call	_ZdlPv@PLT
.L628:
	cmpb	$0, 504(%rbx)
	jne	.L611
	movq	16(%rbx), %rdi
	call	_ZN2v85debug28ClearBreakOnNextFunctionCallEPNS_7IsolateE@PLT
	jmp	.L611
.L638:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7866:
	.size	_ZN12v8_inspector10V8Debugger25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE, .-_ZN12v8_inspector10V8Debugger25externalAsyncTaskFinishedERKNS_14V8StackTraceIdE
	.section	.text._ZN12v8_inspector10V8Debugger27asyncTaskStartedForSteppingEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger27asyncTaskStartedForSteppingEPv
	.type	_ZN12v8_inspector10V8Debugger27asyncTaskStartedForSteppingEPv, @function
_ZN12v8_inspector10V8Debugger27asyncTaskStartedForSteppingEPv:
.LFB7897:
	.cfi_startproc
	endbr64
	cmpb	$0, 504(%rdi)
	jne	.L640
	cmpq	$0, 472(%rdi)
	jne	.L640
	cmpq	456(%rdi), %rsi
	je	.L642
.L640:
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	movq	16(%rdi), %rdi
	jmp	_ZN2v85debug26SetBreakOnNextFunctionCallEPNS_7IsolateE@PLT
	.cfi_endproc
.LFE7897:
	.size	_ZN12v8_inspector10V8Debugger27asyncTaskStartedForSteppingEPv, .-_ZN12v8_inspector10V8Debugger27asyncTaskStartedForSteppingEPv
	.section	.text._ZN12v8_inspector10V8Debugger28asyncTaskFinishedForSteppingEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger28asyncTaskFinishedForSteppingEPv
	.type	_ZN12v8_inspector10V8Debugger28asyncTaskFinishedForSteppingEPv, @function
_ZN12v8_inspector10V8Debugger28asyncTaskFinishedForSteppingEPv:
.LFB7898:
	.cfi_startproc
	endbr64
	cmpq	$0, 472(%rdi)
	jne	.L643
	cmpq	%rsi, 456(%rdi)
	je	.L647
.L643:
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	cmpb	$0, 504(%rdi)
	movq	$0, 456(%rdi)
	jne	.L643
	movq	16(%rdi), %rdi
	jmp	_ZN2v85debug28ClearBreakOnNextFunctionCallEPNS_7IsolateE@PLT
	.cfi_endproc
.LFE7898:
	.size	_ZN12v8_inspector10V8Debugger28asyncTaskFinishedForSteppingEPv, .-_ZN12v8_inspector10V8Debugger28asyncTaskFinishedForSteppingEPv
	.section	.text._ZN12v8_inspector10V8Debugger28asyncTaskCanceledForSteppingEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger28asyncTaskCanceledForSteppingEPv
	.type	_ZN12v8_inspector10V8Debugger28asyncTaskCanceledForSteppingEPv, @function
_ZN12v8_inspector10V8Debugger28asyncTaskCanceledForSteppingEPv:
.LFB7899:
	.cfi_startproc
	endbr64
	cmpq	$0, 472(%rdi)
	jne	.L648
	cmpq	%rsi, 456(%rdi)
	je	.L650
.L648:
	ret
	.p2align 4,,10
	.p2align 3
.L650:
	movq	$0, 456(%rdi)
	ret
	.cfi_endproc
.LFE7899:
	.size	_ZN12v8_inspector10V8Debugger28asyncTaskCanceledForSteppingEPv, .-_ZN12v8_inspector10V8Debugger28asyncTaskCanceledForSteppingEPv
	.section	.text._ZN12v8_inspector10V8Debugger21allAsyncTasksCanceledEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger21allAsyncTasksCanceledEv
	.type	_ZN12v8_inspector10V8Debugger21allAsyncTasksCanceledEv, @function
_ZN12v8_inspector10V8Debugger21allAsyncTasksCanceledEv:
.LFB7900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	136(%rdi), %r12
	testq	%r12, %r12
	je	.L652
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L658
	.p2align 4,,10
	.p2align 3
.L653:
	movq	%r12, %r13
	movq	(%r12), %r12
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L660
	lock subl	$1, 12(%rdi)
	jne	.L660
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L660:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L653
.L652:
	movq	128(%rbx), %rax
	movq	120(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	192(%rbx), %r12
	movq	$0, 144(%rbx)
	movq	$0, 136(%rbx)
	testq	%r12, %r12
	je	.L662
	.p2align 4,,10
	.p2align 3
.L663:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L663
.L662:
	movq	184(%rbx), %rax
	movq	176(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	264(%rbx), %r15
	movq	272(%rbx), %r14
	movq	$0, 200(%rbx)
	movq	$0, 192(%rbx)
	cmpq	%r14, %r15
	je	.L664
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%r15, %r13
	je	.L671
	movq	8(%r13), %r12
	testq	%r12, %r12
	jne	.L725
	.p2align 4,,10
	.p2align 3
.L673:
	addq	$16, %r13
	cmpq	%r13, %r14
	je	.L670
.L665:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L673
.L725:
	lock subl	$1, 8(%r12)
	jne	.L673
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r12)
	jne	.L673
	movq	(%r12), %rax
	addq	$16, %r13
	movq	%r12, %rdi
	call	*24(%rax)
	cmpq	%r13, %r14
	jne	.L665
	.p2align 4,,10
	.p2align 3
.L670:
	movq	%r15, 272(%rbx)
.L664:
	movq	288(%rbx), %rax
	cmpq	296(%rbx), %rax
	je	.L675
	movq	%rax, 296(%rbx)
.L675:
	movq	240(%rbx), %rax
	cmpq	248(%rbx), %rax
	je	.L676
	movq	%rax, 248(%rbx)
.L676:
	movq	360(%rbx), %r12
	testq	%r12, %r12
	je	.L677
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L683
	.p2align 4,,10
	.p2align 3
.L678:
	movq	%r12, %r13
	movq	(%r12), %r12
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L685
	lock subl	$1, 12(%rdi)
	jne	.L685
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L685:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L678
.L677:
	movq	352(%rbx), %rax
	movq	344(%rbx), %rdi
	xorl	%esi, %esi
	leaq	320(%rbx), %r14
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	320(%rbx), %r12
	movq	$0, 368(%rbx)
	movq	$0, 360(%rbx)
	cmpq	%r12, %r14
	je	.L687
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L693
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r13
	testq	%r13, %r13
	jne	.L726
	.p2align 4,,10
	.p2align 3
.L695:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	cmpq	%r12, %r14
	je	.L687
.L688:
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r13
	testq	%r13, %r13
	je	.L695
.L726:
	lock subl	$1, 8(%r13)
	jne	.L695
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L695
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	cmpq	%r12, %r14
	jne	.L688
.L687:
	movq	$0, 336(%rbx)
	movq	%r12, %xmm0
	movl	$0, 312(%rbx)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 320(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L658:
	.cfi_restore_state
	movq	%r12, %r13
	movq	(%r12), %r12
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L656
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L656
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L656:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L658
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L669:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	cmpl	$1, %eax
	jne	.L668
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L668:
	addq	$16, %r13
	cmpq	%r13, %r14
	je	.L670
.L671:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L668
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L668
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L683:
	movq	%r12, %r13
	movq	(%r12), %r12
	movq	24(%r13), %rdi
	testq	%rdi, %rdi
	je	.L681
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L681
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L681:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L683
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L692:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L691
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L691:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	cmpq	%r14, %r12
	je	.L687
.L693:
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r13
	testq	%r13, %r13
	je	.L691
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L691
	jmp	.L692
	.cfi_endproc
.LFE7900:
	.size	_ZN12v8_inspector10V8Debugger21allAsyncTasksCanceledEv, .-_ZN12v8_inspector10V8Debugger21allAsyncTasksCanceledEv
	.section	.text._ZN12v8_inspector10V8Debugger22muteScriptParsedEventsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger22muteScriptParsedEventsEv
	.type	_ZN12v8_inspector10V8Debugger22muteScriptParsedEventsEv, @function
_ZN12v8_inspector10V8Debugger22muteScriptParsedEventsEv:
.LFB7901:
	.cfi_startproc
	endbr64
	addl	$1, 40(%rdi)
	ret
	.cfi_endproc
.LFE7901:
	.size	_ZN12v8_inspector10V8Debugger22muteScriptParsedEventsEv, .-_ZN12v8_inspector10V8Debugger22muteScriptParsedEventsEv
	.section	.text._ZN12v8_inspector10V8Debugger24unmuteScriptParsedEventsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger24unmuteScriptParsedEventsEv
	.type	_ZN12v8_inspector10V8Debugger24unmuteScriptParsedEventsEv, @function
_ZN12v8_inspector10V8Debugger24unmuteScriptParsedEventsEv:
.LFB7902:
	.cfi_startproc
	endbr64
	subl	$1, 40(%rdi)
	ret
	.cfi_endproc
.LFE7902:
	.size	_ZN12v8_inspector10V8Debugger24unmuteScriptParsedEventsEv, .-_ZN12v8_inspector10V8Debugger24unmuteScriptParsedEventsEv
	.section	.text._ZN12v8_inspector10V8Debugger17captureStackTraceEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger17captureStackTraceEb
	.type	_ZN12v8_inspector10V8Debugger17captureStackTraceEb, @function
_ZN12v8_inspector10V8Debugger17captureStackTraceEb:
.LFB7903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$136, %rsp
	movq	16(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	jne	.L730
	movq	$0, 0(%r13)
.L729:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L746
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L730:
	.cfi_restore_state
	movq	16(%r12), %rsi
	leaq	-160(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	jne	.L732
.L734:
	movq	$0, 0(%r13)
.L733:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L732:
	leaq	-128(%rbp), %rdx
	movq	16(%r12), %rsi
	movq	%rdx, %rdi
	movq	%rdx, -168(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rdi
	movq	24(%r12), %r15
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-168(%rbp), %rdx
	movl	%eax, %r15d
	movq	%rdx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testl	%r15d, %r15d
	je	.L734
	testb	%bl, %bl
	movl	$1, -128(%rbp)
	movq	-168(%rbp), %rdx
	je	.L735
	movl	_ZN12v8_inspector16V8StackTraceImpl25maxCallStackSizeToCaptureE(%rip), %ecx
	movl	%ecx, -128(%rbp)
.L736:
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector16V8StackTraceImpl7captureEPNS_10V8DebuggerEii@PLT
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L735:
	movq	24(%r12), %rdi
	leaq	-96(%rbp), %rbx
	movq	%rdx, -96(%rbp)
	movl	%r15d, %esi
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger17captureStackTraceEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger17captureStackTraceEbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation(%rip), %rcx
	movq	%rbx, %rdx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L737
	movl	$3, %edx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L737:
	movl	-128(%rbp), %ecx
	jmp	.L736
.L746:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7903:
	.size	_ZN12v8_inspector10V8Debugger17captureStackTraceEb, .-_ZN12v8_inspector10V8Debugger17captureStackTraceEb
	.section	.text._ZN12v8_inspector10V8Debugger31shouldContinueToCurrentLocationEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger31shouldContinueToCurrentLocationEv
	.type	_ZN12v8_inspector10V8Debugger31shouldContinueToCurrentLocationEv, @function
_ZN12v8_inspector10V8Debugger31shouldContinueToCurrentLocationEv:
.LFB7788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	_ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum3AnyE(%rip), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	80(%r12), %rax
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %rdi
	movq	72(%r12), %r8
	cmpq	%rcx, %rax
	movq	%rcx, %rsi
	cmovbe	%rax, %rsi
	testq	%rsi, %rsi
	je	.L748
	xorl	%edx, %edx
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L794:
	ja	.L771
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	je	.L748
.L750:
	movzwl	(%rdi,%rdx,2), %ebx
	cmpw	%bx, (%r8,%rdx,2)
	jnb	.L794
	movl	$-1, %ebx
.L749:
	leaq	-80(%rbp), %r15
	cmpq	%r15, %rdi
	jne	.L766
.L764:
	movl	$1, %edx
	leaq	-104(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector10V8Debugger17captureStackTraceEb
	movq	_ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum7CurrentE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	80(%r12), %rdx
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	72(%r12), %r8
	cmpq	%rsi, %rdx
	movq	%rsi, %rcx
	cmovbe	%rdx, %rcx
	testq	%rcx, %rcx
	je	.L754
	xorl	%eax, %eax
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L795:
	ja	.L774
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L754
.L756:
	movzwl	(%rdi,%rax,2), %ebx
	cmpw	%bx, (%r8,%rax,2)
	jnb	.L795
	movl	$-1, %ebx
.L755:
	cmpq	%r15, %rdi
	jne	.L761
.L791:
	movq	-104(%rbp), %rdi
	movl	$1, %r14d
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L748:
	subq	%rcx, %rax
	cmpq	$2147483647, %rax
	jg	.L767
	cmpq	$-2147483648, %rax
	jl	.L767
	leaq	-80(%rbp), %r15
	movl	%eax, %ebx
	cmpq	%r15, %rdi
	je	.L752
.L766:
	call	_ZdlPv@PLT
.L752:
	movl	$1, %r14d
	testl	%ebx, %ebx
	jne	.L764
.L747:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L796
	addq	$72, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L771:
	.cfi_restore_state
	movl	$1, %ebx
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L754:
	subq	%rsi, %rdx
	cmpq	$2147483647, %rdx
	jg	.L757
	cmpq	$-2147483648, %rdx
	jl	.L757
	movl	%edx, %ebx
	cmpq	%r15, %rdi
	je	.L758
.L761:
	call	_ZdlPv@PLT
.L758:
	movq	-104(%rbp), %rdi
	movl	$1, %r14d
	testl	%ebx, %ebx
	jne	.L759
	movq	112(%r12), %r8
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZNK12v8_inspector16V8StackTraceImpl23isEqualIgnoringTopFrameEPS0_@PLT
	movq	-104(%rbp), %rdi
	movl	%eax, %r14d
.L759:
	testq	%rdi, %rdi
	je	.L747
	movq	(%rdi), %rax
	call	*64(%rax)
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L767:
	leaq	-80(%rbp), %r15
	cmpq	%r15, %rdi
	je	.L764
	call	_ZdlPv@PLT
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L757:
	cmpq	%r15, %rdi
	je	.L791
	call	_ZdlPv@PLT
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L774:
	movl	$1, %ebx
	jmp	.L755
.L796:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7788:
	.size	_ZN12v8_inspector10V8Debugger31shouldContinueToCurrentLocationEv, .-_ZN12v8_inspector10V8Debugger31shouldContinueToCurrentLocationEv
	.section	.text._ZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEb
	.type	_ZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEb, @function
_ZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEb:
.LFB7790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, -132(%rbp)
	movl	64(%rdi), %r8d
	movq	%rsi, -120(%rbp)
	movq	%rdx, -128(%rbp)
	movb	%r9b, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%r8d, %r8d
	je	.L827
.L797:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L828
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L827:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	24(%rdi), %rdi
	movq	%rcx, %r12
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE@PLT
	movl	%eax, %r13d
	movl	60(%rbx), %eax
	cmpl	%r13d, %eax
	je	.L799
	testl	%eax, %eax
	jne	.L829
.L799:
	xorl	%edx, %edx
	leaq	-80(%rbp), %rax
	xorl	%ecx, %ecx
	movq	$0, -64(%rbp)
	movw	%dx, -80(%rbp)
	movq	464(%rbx), %rdx
	xorl	%esi, %esi
	movq	%rax, -96(%rbp)
	movl	$0, 60(%rbx)
	movb	$0, 504(%rbx)
	movb	$0, 512(%rbx)
	movq	$0, 456(%rbx)
	movq	$0, 472(%rbx)
	movw	%cx, (%rdx)
	movq	-96(%rbp), %rdx
	movq	$0, -88(%rbp)
	movw	%si, (%rdx)
	movq	-64(%rbp), %rdx
	movq	-96(%rbp), %rdi
	movq	%rdx, 496(%rbx)
	cmpq	%rax, %rdi
	je	.L801
	call	_ZdlPv@PLT
.L801:
	movzbl	56(%rbx), %eax
	leaq	-99(%rbp), %r15
	movq	24(%rbx), %rdi
	leaq	-96(%rbp), %r14
	movq	%r15, %xmm0
	movq	%r14, %rdx
	movl	%r13d, %esi
	movb	$0, -97(%rbp)
	movb	%al, -99(%rbp)
	movzbl	57(%rbx), %eax
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation(%rip), %rcx
	movb	%al, -98(%rbp)
	leaq	-97(%rbp), %rax
	movq	%rax, %xmm1
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %xmm2
	movaps	%xmm0, -96(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L802
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L802:
	cmpb	$0, -97(%rbp)
	je	.L797
	movq	(%r12), %rdx
	movq	8(%r12), %rax
	subq	%rdx, %rax
	cmpq	$4, %rax
	jne	.L803
	movl	68(%rbx), %eax
	cmpl	%eax, (%rdx)
	je	.L830
.L803:
	leaq	-120(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, %xmm0
	leaq	-128(%rbp), %rax
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZN12v8_inspector10V8Debugger23clearContinueToLocationEv
	movl	%r13d, 64(%rbx)
	movq	24(%rbx), %r8
	movl	$56, %edi
	movq	$0, -80(%rbp)
	movq	%r8, -144(%rbp)
	call	_Znwm@PLT
	leaq	-132(%rbp), %rdx
	movdqa	-160(%rbp), %xmm0
	movq	-144(%rbp), %r8
	movq	%rdx, 24(%rax)
	leaq	-136(%rbp), %rdx
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE0_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation(%rip), %rsi
	movq	%rdx, 32(%rax)
	leaq	-98(%rbp), %rdx
	movq	%r8, %rdi
	movq	%rdx, 48(%rax)
	movq	%r14, %rdx
	movups	%xmm0, (%rax)
	movq	%rsi, %xmm0
	movl	%r13d, %esi
	movq	%r12, 16(%rax)
	movq	%r15, 40(%rax)
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E0_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L805
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L805:
	movq	-120(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	24(%rbx), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient21runMessageLoopOnPauseEi(%rip), %rdx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L831
.L806:
	movl	$0, 64(%rbx)
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_10V8Debugger18handleProgramBreakEN2v85LocalINS5_7ContextEEENS6_INS5_5ValueEEERKSt6vectorIiSaIiEENS5_5debug13ExceptionTypeEbEUlS2_E1_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	24(%rbx), %rdi
	movq	%r14, %rdx
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS3_7ContextEEENS4_INS3_5ValueEEERKSt6vectorIiSaIiEENS3_5debug13ExceptionTypeEbEUlPNS1_22V8InspectorSessionImplEE1_E10_M_managerERSt9_Any_dataRKSK_St18_Manager_operation(%rip), %rsi
	movq	%rax, %xmm5
	movq	%rsi, %xmm0
	movl	%r13d, %esi
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L807
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L807:
	cmpb	$0, 56(%rbx)
	jne	.L832
.L808:
	xorl	%eax, %eax
	movw	%ax, 56(%rbx)
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L829:
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v85debug11PrepareStepEPNS_7IsolateENS0_10StepActionE@PLT
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L832:
	movq	16(%rbx), %rdi
	call	_ZN2v87Isolate24RestoreOriginalHeapLimitEv@PLT
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L831:
	movl	%r13d, %esi
	call	*%rax
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L830:
	movq	-120(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -144(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	%rbx, %rdi
	call	_ZN12v8_inspector10V8Debugger31shouldContinueToCurrentLocationEv
	movq	-144(%rbp), %r8
	testb	%al, %al
	movq	%r8, %rdi
	je	.L833
	call	_ZN2v87Context4ExitEv@PLT
	jmp	.L803
.L833:
	call	_ZN2v87Context4ExitEv@PLT
	jmp	.L797
.L828:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7790:
	.size	_ZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEb, .-_ZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEb
	.section	.text._ZN12v8_inspector10V8Debugger21BreakProgramRequestedEN2v85LocalINS1_7ContextEEERKSt6vectorIiSaIiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger21BreakProgramRequestedEN2v85LocalINS1_7ContextEEERKSt6vectorIiSaIiEE
	.type	_ZN12v8_inspector10V8Debugger21BreakProgramRequestedEN2v85LocalINS1_7ContextEEERKSt6vectorIiSaIiEE, @function
_ZN12v8_inspector10V8Debugger21BreakProgramRequestedEN2v85LocalINS1_7ContextEEERKSt6vectorIiSaIiEE:
.LFB7804:
	.cfi_startproc
	endbr64
	movq	%rdx, %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	jmp	_ZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEb
	.cfi_endproc
.LFE7804:
	.size	_ZN12v8_inspector10V8Debugger21BreakProgramRequestedEN2v85LocalINS1_7ContextEEERKSt6vectorIiSaIiEE, .-_ZN12v8_inspector10V8Debugger21BreakProgramRequestedEN2v85LocalINS1_7ContextEEERKSt6vectorIiSaIiEE
	.section	.text._ZN12v8_inspector10V8Debugger15ExceptionThrownEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEES6_bNS1_5debug13ExceptionTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger15ExceptionThrownEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEES6_bNS1_5debug13ExceptionTypeE
	.type	_ZN12v8_inspector10V8Debugger15ExceptionThrownEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEES6_bNS1_5debug13ExceptionTypeE, @function
_ZN12v8_inspector10V8Debugger15ExceptionThrownEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEES6_bNS1_5debug13ExceptionTypeE:
.LFB7805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%r9d, %eax
	pxor	%xmm0, %xmm0
	movzbl	%r8b, %r9d
	movl	%eax, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rcx
	movq	%rcx, -8(%rbp)
	xorl	%ecx, %ecx
	leaq	-32(%rbp), %rcx
	movq	$0, -16(%rbp)
	movaps	%xmm0, -32(%rbp)
	call	_ZN12v8_inspector10V8Debugger18handleProgramBreakEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEERKSt6vectorIiSaIiEENS1_5debug13ExceptionTypeEb
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L835
	call	_ZdlPv@PLT
.L835:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L842
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L842:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7805:
	.size	_ZN12v8_inspector10V8Debugger15ExceptionThrownEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEES6_bNS1_5debug13ExceptionTypeE, .-_ZN12v8_inspector10V8Debugger15ExceptionThrownEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEES6_bNS1_5debug13ExceptionTypeE
	.section	.text._ZN12v8_inspector10V8Debugger21currentContextGroupIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger21currentContextGroupIdEv
	.type	_ZN12v8_inspector10V8Debugger21currentContextGroupIdEv, @function
_ZN12v8_inspector10V8Debugger21currentContextGroupIdEv:
.LFB7905:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L843
	movq	16(%rbx), %rsi
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rdi
	movq	24(%rbx), %r12
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZN2v811HandleScopeD1Ev@PLT
.L843:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L850
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L850:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7905:
	.size	_ZN12v8_inspector10V8Debugger21currentContextGroupIdEv, .-_ZN12v8_inspector10V8Debugger21currentContextGroupIdEv
	.section	.text._ZN12v8_inspector10V8Debugger13debuggerIdForERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger13debuggerIdForERKNS_8String16E
	.type	_ZN12v8_inspector10V8Debugger13debuggerIdForERKNS_8String16E, @function
_ZN12v8_inspector10V8Debugger13debuggerIdForERKNS_8String16E:
.LFB7953:
	.cfi_startproc
	endbr64
	movq	32(%rsi), %rcx
	testq	%rcx, %rcx
	jne	.L852
	movq	(%rsi), %rax
	movq	8(%rsi), %rdx
	leaq	(%rax,%rdx,2), %r8
	cmpq	%r8, %rax
	je	.L855
	.p2align 4,,10
	.p2align 3
.L854:
	movq	%rcx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	movsbq	-2(%rax), %rdx
	addq	%rdx, %rcx
	movq	%rcx, 32(%rsi)
	cmpq	%rax, %r8
	jne	.L854
	testq	%rcx, %rcx
	je	.L855
.L852:
	movq	672(%rdi), %r8
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	664(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L882
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2147483648, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rax), %rdi
	movabsq	$-2147483649, %rbx
	movq	64(%rdi), %r9
.L861:
	cmpq	%rcx, %r9
	je	.L885
.L857:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L856
	movq	64(%rdi), %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%r8
	cmpq	%rdx, %r10
	je	.L861
.L856:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	xorl	%edx, %edx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L885:
	.cfi_restore_state
	movq	8(%rsi), %rax
	movq	16(%rdi), %r9
	movq	8(%rdi), %r13
	movq	(%rsi), %r14
	cmpq	%r9, %rax
	movq	%r9, %r12
	cmovbe	%rax, %r12
	testq	%r12, %r12
	je	.L858
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L859:
	movzwl	0(%r13,%rdx,2), %r15d
	cmpw	%r15w, (%r14,%rdx,2)
	jne	.L857
	addq	$1, %rdx
	cmpq	%r12, %rdx
	jne	.L859
.L858:
	subq	%r9, %rax
	cmpq	%r11, %rax
	jge	.L857
	cmpq	%rbx, %rax
	jle	.L857
	testl	%eax, %eax
	jne	.L857
	popq	%rbx
	movq	48(%rdi), %rax
	popq	%r12
	movq	56(%rdi), %rdx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L855:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movq	$1, 32(%rsi)
	movl	$1, %ecx
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L882:
	xorl	%eax, %eax
	xorl	%edx, %edx
	ret
	.cfi_endproc
.LFE7953:
	.size	_ZN12v8_inspector10V8Debugger13debuggerIdForERKNS_8String16E, .-_ZN12v8_inspector10V8Debugger13debuggerIdForERKNS_8String16E
	.section	.text._ZN12v8_inspector10V8Debugger17addInternalObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS_19V8InternalValueTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger17addInternalObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS_19V8InternalValueTypeE
	.type	_ZN12v8_inspector10V8Debugger17addInternalObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS_19V8InternalValueTypeE, @function
_ZN12v8_inspector10V8Debugger17addInternalObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS_19V8InternalValueTypeE:
.LFB7954:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	24(%rbx), %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEi@PLT
	testq	%rax, %rax
	je	.L887
	addq	$8, %rsp
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector16InspectedContext17addInternalObjectEN2v85LocalINS1_6ObjectEEENS_19V8InternalValueTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L887:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7954:
	.size	_ZN12v8_inspector10V8Debugger17addInternalObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS_19V8InternalValueTypeE, .-_ZN12v8_inspector10V8Debugger17addInternalObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS_19V8InternalValueTypeE
	.section	.rodata._ZN12v8_inspector10V8Debugger31dumpAsyncTaskStacksStateForTestEv.str1.1,"aMS",@progbits,1
.LC8:
	.string	"Async stacks count: %d\n"
.LC9:
	.string	"Scheduled async tasks: %zu\n"
.LC10:
	.string	"Recurring async tasks: %zu\n"
	.section	.text._ZN12v8_inspector10V8Debugger31dumpAsyncTaskStacksStateForTestEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger31dumpAsyncTaskStacksStateForTestEv
	.type	_ZN12v8_inspector10V8Debugger31dumpAsyncTaskStacksStateForTestEv, @function
_ZN12v8_inspector10V8Debugger31dumpAsyncTaskStacksStateForTestEv:
.LFB7955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdx
	movl	$1, %esi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	312(%rdi), %ecx
	movq	stdout(%rip), %rdi
	call	__fprintf_chk@PLT
	movq	144(%rbx), %rcx
	movl	$1, %esi
	xorl	%eax, %eax
	movq	stdout(%rip), %rdi
	leaq	.LC9(%rip), %rdx
	call	__fprintf_chk@PLT
	movq	200(%rbx), %rcx
	xorl	%eax, %eax
	movq	stdout(%rip), %rdi
	movl	$1, %esi
	leaq	.LC10(%rip), %rdx
	call	__fprintf_chk@PLT
	movq	stdout(%rip), %rsi
	addq	$8, %rsp
	movl	$10, %edi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	fputc@PLT
	.cfi_endproc
.LFE7955:
	.size	_ZN12v8_inspector10V8Debugger31dumpAsyncTaskStacksStateForTestEv, .-_ZN12v8_inspector10V8Debugger31dumpAsyncTaskStacksStateForTestEv
	.section	.rodata._ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_.str1.1,"aMS",@progbits,1
.LC11:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB11045:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r14
	movq	%rsi, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	movabsq	$384307168202282325, %rcx
	cmpq	%rcx, %rax
	je	.L908
	movq	%r12, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L900
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L909
.L893:
	movq	%rbx, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %r8
	movq	%rax, %r13
	leaq	(%rax,%rbx), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	24(%r13), %rbx
.L899:
	movdqu	(%rdx), %xmm2
	movq	16(%rdx), %rax
	movups	%xmm2, 0(%r13,%r8)
	movq	%rax, 16(%r13,%r8)
	cmpq	%r14, %r12
	je	.L895
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L896:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L896
	leaq	-24(%r12), %rax
	subq	%r14, %rax
	shrq	$3, %rax
	leaq	48(%r13,%rax,8), %rbx
.L895:
	cmpq	%rsi, %r12
	je	.L897
	subq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-24(%rsi), %rax
	movq	%r12, %rsi
	shrq	$3, %rax
	leaq	24(,%rax,8), %rdx
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	addq	%rdx, %rbx
.L897:
	testq	%r14, %r14
	je	.L898
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L898:
	movq	-56(%rbp), %rax
	movq	%r13, %xmm0
	movq	%rbx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L894
	movq	$0, -56(%rbp)
	movl	$24, %ebx
	xorl	%r13d, %r13d
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L900:
	movl	$24, %ebx
	jmp	.L893
.L894:
	cmpq	%rcx, %rdi
	movq	%rcx, %rbx
	cmovbe	%rdi, %rbx
	imulq	$24, %rbx, %rbx
	jmp	.L893
.L908:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11045:
	.size	_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text._ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB11049:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	(%rdi), %r12
	movq	%r14, %rax
	subq	%r12, %rax
	sarq	$4, %rax
	cmpq	%rdx, %rax
	je	.L940
	movq	%rsi, %rcx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rcx
	testq	%rax, %rax
	je	.L930
	movabsq	$9223372036854775792, %r15
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L941
.L912:
	movq	%r15, %rdi
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	addq	%rax, %r15
	movq	%rax, -56(%rbp)
	addq	$16, %rax
	movq	%r15, -64(%rbp)
.L929:
	movq	-56(%rbp), %rsi
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rsi,%rcx)
	cmpq	%r12, %rbx
	je	.L914
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rsi, %rdx
	movq	%r12, %r15
	jne	.L915
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L919:
	movq	(%rdi), %rcx
	movq	%rdx, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	*16(%rcx)
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %rdx
	movl	12(%rdi), %ecx
	leal	-1(%rcx), %esi
	cmpl	$1, %ecx
	movl	%esi, 12(%rdi)
	jne	.L918
	movq	(%rdi), %rcx
	movq	%rdx, -72(%rbp)
	call	*24(%rcx)
	movq	-72(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L918:
	addq	$16, %r15
	addq	$16, %rdx
	cmpq	%r15, %rbx
	je	.L920
.L921:
	movq	(%r15), %rcx
	movq	$0, 8(%rdx)
	pxor	%xmm1, %xmm1
	movq	%rcx, (%rdx)
	movq	8(%r15), %rcx
	movq	%rcx, 8(%rdx)
	movups	%xmm1, (%r15)
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L918
	movl	8(%rdi), %ecx
	leal	-1(%rcx), %esi
	movl	%esi, 8(%rdi)
	cmpl	$1, %ecx
	je	.L919
	addq	$16, %r15
	addq	$16, %rdx
	cmpq	%r15, %rbx
	jne	.L921
	.p2align 4,,10
	.p2align 3
.L920:
	movq	%rbx, %rax
	subq	%r12, %rax
	addq	-56(%rbp), %rax
	addq	$16, %rax
.L914:
	cmpq	%r14, %rbx
	je	.L926
	subq	%rbx, %r14
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	-16(%r14), %rdi
	movq	%rdi, %rsi
	shrq	$4, %rsi
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L927:
	movdqu	(%rbx,%rdx), %xmm3
	addq	$1, %rcx
	movups	%xmm3, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rcx
	jb	.L927
	leaq	16(%rax,%rdi), %rax
.L926:
	testq	%r12, %r12
	je	.L928
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rax
.L928:
	movq	-56(%rbp), %xmm0
	movq	%rax, %xmm4
	movq	-64(%rbp), %rax
	punpcklqdq	%xmm4, %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L942:
	.cfi_restore_state
	movq	(%rdi), %rcx
	movq	%rdi, -72(%rbp)
	movq	%rdx, -80(%rbp)
	call	*16(%rcx)
	movq	-72(%rbp), %rdi
	movl	$-1, %ecx
	lock xaddl	%ecx, 12(%rdi)
	movq	-80(%rbp), %rdx
	cmpl	$1, %ecx
	jne	.L923
	movq	(%rdi), %rcx
	movq	%rdx, -72(%rbp)
	call	*24(%rcx)
	movq	-72(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L923:
	addq	$16, %r15
	addq	$16, %rdx
	cmpq	%r15, %rbx
	je	.L920
.L915:
	movq	(%r15), %rcx
	movq	$0, 8(%rdx)
	pxor	%xmm2, %xmm2
	movq	%rcx, (%rdx)
	movq	8(%r15), %rcx
	movq	%rcx, 8(%rdx)
	movups	%xmm2, (%r15)
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L923
	lock subl	$1, 8(%rdi)
	jne	.L923
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L941:
	testq	%rsi, %rsi
	jne	.L913
	movq	$0, -64(%rbp)
	movl	$16, %eax
	movq	$0, -56(%rbp)
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L930:
	movl	$16, %r15d
	jmp	.L912
.L940:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L913:
	cmpq	%rdx, %rsi
	cmovbe	%rsi, %rdx
	movq	%rdx, %r15
	salq	$4, %r15
	jmp	.L912
	.cfi_endproc
.LFE11049:
	.size	_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.type	_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, @function
_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_:
.LFB11070:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L957
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L953
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L958
.L945:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L952:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L959
	testq	%r13, %r13
	jg	.L948
	testq	%r9, %r9
	jne	.L951
.L949:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L959:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L948
.L951:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L958:
	testq	%rsi, %rsi
	jne	.L946
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L948:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L949
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L953:
	movl	$8, %r14d
	jmp	.L945
.L957:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L946:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L945
	.cfi_endproc
.LFE11070:
	.size	_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, .-_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.section	.text._ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.type	_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, @function
_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_:
.LFB11075:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	(%rdi), %r12
	movq	%r14, %rax
	subq	%r12, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L997
	movq	%rsi, %rbx
	movq	%rdi, %r13
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L983
	movabsq	$9223372036854775792, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L998
.L962:
	movq	%r15, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %rdx
	addq	%rax, %r15
	movq	%rax, -56(%rbp)
	addq	$16, %rax
	movq	%r15, -64(%rbp)
.L982:
	movdqu	(%rdx), %xmm4
	movq	8(%rdx), %rcx
	movq	-56(%rbp), %rdx
	movaps	%xmm4, -80(%rbp)
	movups	%xmm4, (%rdx,%rsi)
	testq	%rcx, %rcx
	je	.L964
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L965
	lock addl	$1, 8(%rcx)
.L964:
	cmpq	%r12, %rbx
	je	.L966
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	-56(%rbp), %rdx
	movq	%r12, %r15
	jne	.L968
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L972:
	movq	(%rdi), %rcx
	movq	%rdx, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	*16(%rcx)
	movq	-80(%rbp), %rdi
	movq	-88(%rbp), %rdx
	movl	12(%rdi), %ecx
	leal	-1(%rcx), %esi
	cmpl	$1, %ecx
	movl	%esi, 12(%rdi)
	jne	.L971
	movq	(%rdi), %rcx
	movq	%rdx, -80(%rbp)
	call	*24(%rcx)
	movq	-80(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L971:
	addq	$16, %r15
	addq	$16, %rdx
	cmpq	%r15, %rbx
	je	.L973
.L974:
	movq	(%r15), %rcx
	movq	$0, 8(%rdx)
	pxor	%xmm1, %xmm1
	movq	%rcx, (%rdx)
	movq	8(%r15), %rcx
	movq	%rcx, 8(%rdx)
	movups	%xmm1, (%r15)
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L971
	movl	8(%rdi), %ecx
	leal	-1(%rcx), %esi
	movl	%esi, 8(%rdi)
	cmpl	$1, %ecx
	je	.L972
	addq	$16, %r15
	addq	$16, %rdx
	cmpq	%r15, %rbx
	jne	.L974
	.p2align 4,,10
	.p2align 3
.L973:
	movq	%rbx, %rax
	subq	%r12, %rax
	addq	-56(%rbp), %rax
	addq	$16, %rax
.L966:
	cmpq	%r14, %rbx
	je	.L979
	subq	%rbx, %r14
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	-16(%r14), %rdi
	movq	%rdi, %rsi
	shrq	$4, %rsi
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L980:
	movdqu	(%rbx,%rdx), %xmm3
	addq	$1, %rcx
	movups	%xmm3, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rsi
	ja	.L980
	leaq	16(%rax,%rdi), %rax
.L979:
	testq	%r12, %r12
	je	.L981
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rax
.L981:
	movq	-56(%rbp), %xmm0
	movq	%rax, %xmm6
	movq	-64(%rbp), %rax
	punpcklqdq	%xmm6, %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L999:
	.cfi_restore_state
	movq	(%rdi), %rcx
	movq	%rdi, -80(%rbp)
	movq	%rdx, -88(%rbp)
	call	*16(%rcx)
	movq	-80(%rbp), %rdi
	movl	$-1, %ecx
	lock xaddl	%ecx, 12(%rdi)
	movq	-88(%rbp), %rdx
	cmpl	$1, %ecx
	jne	.L976
	movq	(%rdi), %rcx
	movq	%rdx, -80(%rbp)
	call	*24(%rcx)
	movq	-80(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L976:
	addq	$16, %r15
	addq	$16, %rdx
	cmpq	%r15, %rbx
	je	.L973
.L968:
	movq	(%r15), %rcx
	movq	$0, 8(%rdx)
	pxor	%xmm2, %xmm2
	movq	%rcx, (%rdx)
	movq	8(%r15), %rcx
	movq	%rcx, 8(%rdx)
	movups	%xmm2, (%r15)
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L976
	lock subl	$1, 8(%rdi)
	jne	.L976
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L965:
	addl	$1, 8(%rcx)
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L998:
	testq	%rdi, %rdi
	jne	.L963
	movq	$0, -64(%rbp)
	movl	$16, %eax
	movq	$0, -56(%rbp)
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L983:
	movl	$16, %r15d
	jmp	.L962
.L997:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L963:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$4, %r15
	jmp	.L962
	.cfi_endproc
.LFE11075:
	.size	_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_, .-_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	.section	.text._ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.type	_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, @function
_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_:
.LFB11077:
	.cfi_startproc
	endbr64
	movabsq	$-6148914691236517205, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movabsq	$384307168202282325, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r15
	movq	%r13, %rax
	subq	%r15, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L1017
	movq	%rsi, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L1009
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1018
.L1002:
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	-72(%rbp), %rdx
	movq	%rax, %rbx
	addq	%rax, %r14
	leaq	24(%rax), %rax
	movq	%rax, -56(%rbp)
.L1008:
	leaq	(%rbx,%rdx), %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN12v8_inspector14V8StackTraceIdC1Ev@PLT
	movq	-64(%rbp), %rsi
	cmpq	%r15, %rsi
	je	.L1004
	movq	%rbx, %rdx
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L1005:
	movdqu	(%rax), %xmm1
	addq	$24, %rax
	addq	$24, %rdx
	movups	%xmm1, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rsi
	jne	.L1005
	leaq	-24(%rsi), %rax
	subq	%r15, %rax
	shrq	$3, %rax
	leaq	48(%rbx,%rax,8), %rax
	movq	%rax, -56(%rbp)
.L1004:
	cmpq	%r13, %rsi
	je	.L1006
	subq	%rsi, %r13
	movq	-56(%rbp), %rdi
	leaq	-24(%r13), %rax
	shrq	$3, %rax
	leaq	24(,%rax,8), %r13
	movq	%r13, %rdx
	call	memcpy@PLT
	addq	%r13, -56(%rbp)
.L1006:
	testq	%r15, %r15
	je	.L1007
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1007:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1018:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L1003
	movq	$24, -56(%rbp)
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1009:
	movl	$24, %r14d
	jmp	.L1002
.L1003:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	imulq	$24, %rcx, %r14
	jmp	.L1002
.L1017:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11077:
	.size	_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_, .-_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	.section	.text.unlikely._ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv,"ax",@progbits
	.align 2
.LCOLDB12:
	.section	.text._ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv,"ax",@progbits
.LHOTB12:
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv
	.type	_ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv, @function
_ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv:
.LFB7873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	236(%rdi), %eax
	testl	%eax, %eax
	jne	.L1059
.L1019:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1060
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1059:
	.cfi_restore_state
	movq	248(%rdi), %rsi
	movq	%rdi, %rbx
	cmpq	256(%rdi), %rsi
	je	.L1021
	movq	-72(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, 248(%rdi)
.L1022:
	movq	-72(%rbp), %r8
	movq	128(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	movq	120(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1027
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1027
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L1027
.L1028:
	cmpq	%rsi, %r8
	jne	.L1061
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L1027
	movl	8(%rax), %edx
	testl	%edx, %edx
	je	.L1027
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rdx
	movl	8(%rax), %eax
.L1033:
	testl	%eax, %eax
	je	.L1056
	leal	1(%rax), %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L1033
	movq	16(%rcx), %rdi
	xorl	%esi, %esi
	movq	%rdi, -64(%rbp)
	call	_ZN12v8_inspector15AsyncStackTrace18setSuspendedTaskIdEPv@PLT
	movq	272(%rbx), %rsi
	cmpq	280(%rbx), %rsi
	je	.L1034
	movq	-64(%rbp), %rax
	movq	%rax, (%rsi)
	movq	-56(%rbp), %rax
	movq	%rax, 8(%rsi)
	testq	%rax, %rax
	je	.L1035
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1036
	lock addl	$1, 8(%rax)
.L1035:
	addq	$16, 272(%rbx)
.L1037:
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L1030
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L1040
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L1041:
	cmpl	$1, %eax
	jne	.L1030
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L1043
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1044:
	cmpl	$1, %eax
	jne	.L1030
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1027:
	movq	272(%rbx), %rsi
	cmpq	280(%rbx), %rsi
	je	.L1062
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	addq	$16, 272(%rbx)
.L1030:
	movq	296(%rbx), %rdi
	cmpq	304(%rbx), %rdi
	je	.L1045
	call	_ZN12v8_inspector14V8StackTraceIdC1Ev@PLT
	addq	$24, 296(%rbx)
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1045:
	leaq	288(%rbx), %r8
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1021:
	leaq	-72(%rbp), %rdx
	leaq	240(%rdi), %rdi
	call	_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1062:
	leaq	264(%rbx), %rdi
	call	_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1040:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1036:
	addl	$1, 8(%rax)
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1034:
	leaq	-64(%rbp), %rdx
	leaq	264(%rbx), %rdi
	call	_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJRKS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1037
.L1043:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1044
.L1060:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv
	.cfi_startproc
	.type	_ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv.cold, @function
_ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv.cold:
.LFSB7873:
.L1056:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	call	abort@PLT
	.cfi_endproc
.LFE7873:
	.section	.text._ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv
	.size	_ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv, .-_ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv
	.section	.text.unlikely._ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv
	.size	_ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv.cold, .-_ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv.cold
.LCOLDE12:
	.section	.text._ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv
.LHOTE12:
	.section	.text._ZN12v8_inspector10V8Debugger16asyncTaskStartedEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger16asyncTaskStartedEPv
	.type	_ZN12v8_inspector10V8Debugger16asyncTaskStartedEPv, @function
_ZN12v8_inspector10V8Debugger16asyncTaskStartedEPv:
.LFB7869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv
	cmpb	$0, 504(%rbx)
	jne	.L1063
	cmpq	$0, 472(%rbx)
	jne	.L1063
	cmpq	456(%rbx), %r12
	je	.L1066
.L1063:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1066:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v85debug26SetBreakOnNextFunctionCallEPNS_7IsolateE@PLT
	.cfi_endproc
.LFE7869:
	.size	_ZN12v8_inspector10V8Debugger16asyncTaskStartedEPv, .-_ZN12v8_inspector10V8Debugger16asyncTaskStartedEPv
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB11626:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L1086
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L1077
	movq	16(%rdi), %rax
.L1069:
	cmpq	%r15, %rax
	jb	.L1089
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L1073
.L1092:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L1090
	testq	%rdx, %rdx
	je	.L1073
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L1073:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1089:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L1091
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L1072
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L1072:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L1076
	call	_ZdlPv@PLT
.L1076:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L1073
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L1086:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L1077:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1090:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L1073
.L1091:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11626:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.text.unlikely._ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib,"ax",@progbits
	.align 2
.LCOLDB13:
	.section	.text._ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib,"ax",@progbits
.LHOTB13:
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib
	.type	_ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib, @function
_ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib:
.LFB7763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$152, %rsp
	movl	%esi, -164(%rbp)
	movq	16(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r14), %rsi
	xorl	%edx, %edx
	leaq	-152(%rbp), %rdi
	call	_ZN2v85debug18StackTraceIterator6CreateEPNS_7IsolateEi@PLT
	movq	-152(%rbp), %rdi
	movq	(%rdi), %rax
	call	*48(%rax)
	movq	-152(%rbp), %rdi
	movq	%rax, %r13
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-152(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movl	%eax, %r12d
	testb	%al, %al
	je	.L1094
	testq	%r13, %r13
	sete	%al
	testb	%bl, %al
	jne	.L1128
	movq	272(%r14), %rax
	cmpq	264(%r14), %rax
	je	.L1128
	movq	-8(%rax), %r13
	movq	-16(%rax), %rsi
	testq	%r13, %r13
	je	.L1096
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1097
	lock addl	$1, 8(%r13)
.L1098:
	testq	%rsi, %rsi
	je	.L1131
	leaq	-144(%rbp), %rdi
	call	_ZNK12v8_inspector15AsyncStackTrace6parentEv@PLT
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	jne	.L1124
.L1131:
	xorl	%r12d, %r12d
	testq	%rbx, %rbx
	je	.L1116
.L1165:
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1117:
	cmpl	$1, %eax
	jne	.L1094
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1118
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1119:
	cmpl	$1, %eax
	jne	.L1094
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1094:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1120
	movq	(%rdi), %rax
	call	*8(%rax)
.L1120:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1162
	addq	$152, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1096:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L1128
	leaq	-144(%rbp), %rdi
	call	_ZNK12v8_inspector15AsyncStackTrace6parentEv@PLT
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	jne	.L1124
	.p2align 4,,10
	.p2align 3
.L1128:
	xorl	%r12d, %r12d
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1124:
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L1111
	movq	-136(%rbp), %r8
	testq	%r8, %r8
	je	.L1101
	movl	8(%r8), %eax
	leaq	8(%r8), %rdx
.L1103:
	testl	%eax, %eax
	je	.L1101
	leal	1(%rax), %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L1103
	movq	-144(%rbp), %rdi
	movq	%r8, -176(%rbp)
	movq	%rdx, -184(%rbp)
	call	_ZNK12v8_inspector15AsyncStackTrace15suspendedTaskIdEv@PLT
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	movq	-176(%rbp), %r8
	testq	%rbx, %rbx
	je	.L1104
	movq	-184(%rbp), %rdx
	movl	$-1, %esi
	lock xaddl	%esi, (%rdx)
	movl	%esi, %edx
.L1105:
	cmpl	$1, %edx
	je	.L1163
.L1107:
	testq	%rax, %rax
	je	.L1111
	movl	-164(%rbp), %ecx
	xorl	%edx, %edx
	leaq	-96(%rbp), %rsi
	leaq	-80(%rbp), %rbx
	movq	%rax, 456(%r14)
	leaq	464(%r14), %rdi
	movl	%ecx, 60(%r14)
	movq	%rbx, -96(%rbp)
	movq	$0, -88(%rbp)
	movw	%dx, -80(%rbp)
	movq	$0, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 496(%r14)
	cmpq	%rbx, %rdi
	je	.L1112
	call	_ZdlPv@PLT
.L1112:
	movl	64(%r14), %edx
	testl	%edx, %edx
	setne	%al
	cmpl	%edx, -164(%rbp)
	sete	%dl
	andb	%dl, %al
	jne	.L1164
.L1100:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1113
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1114
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1115:
	cmpl	$1, %eax
	jne	.L1113
	movq	(%rdi), %rax
	call	*24(%rax)
.L1113:
	testq	%r13, %r13
	je	.L1094
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	jne	.L1165
.L1116:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1111:
	xorl	%r12d, %r12d
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1097:
	addl	$1, 8(%r13)
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1114:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1118:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1119
	.p2align 4,,10
	.p2align 3
.L1104:
	movl	8(%r8), %edx
	leal	-1(%rdx), %esi
	movl	%esi, 8(%r8)
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	(%r8), %rdx
	movq	%rax, -184(%rbp)
	movq	%r8, %rdi
	movq	%r8, -176(%rbp)
	call	*16(%rdx)
	testq	%rbx, %rbx
	movq	-176(%rbp), %r8
	movq	-184(%rbp), %rax
	je	.L1108
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r8)
.L1109:
	cmpl	$1, %edx
	jne	.L1107
	movq	(%r8), %rdx
	movq	%rax, -176(%rbp)
	movq	%r8, %rdi
	call	*24(%rdx)
	movq	-176(%rbp), %rax
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1164:
	movq	24(%r14), %rdx
	movl	%eax, %r12d
	leaq	_ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv(%rip), %rax
	movq	16(%rdx), %rdi
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1100
	call	*%rdx
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1108:
	movl	12(%r8), %edx
	leal	-1(%rdx), %esi
	movl	%esi, 12(%r8)
	jmp	.L1109
.L1162:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib
	.cfi_startproc
	.type	_ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib.cold, @function
_ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib.cold:
.LFSB7763:
.L1101:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE7763:
	.section	.text._ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib
	.size	_ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib, .-_ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib
	.section	.text.unlikely._ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib
	.size	_ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib.cold, .-_ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib.cold
.LCOLDE13:
	.section	.text._ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib
.LHOTE13:
	.section	.text._ZN12v8_inspector10V8Debugger17stepIntoStatementEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger17stepIntoStatementEib
	.type	_ZN12v8_inspector10V8Debugger17stepIntoStatementEib, @function
_ZN12v8_inspector10V8Debugger17stepIntoStatementEib:
.LFB7760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	movl	$1, %edx
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib
	testb	%al, %al
	je	.L1177
.L1166:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1177:
	.cfi_restore_state
	movl	%r12d, 60(%rbx)
	movq	16(%rbx), %rdi
	movl	$2, %esi
	movb	%r13b, 512(%rbx)
	call	_ZN2v85debug11PrepareStepEPNS_7IsolateENS0_10StepActionE@PLT
	movl	64(%rbx), %eax
	testl	%eax, %eax
	je	.L1166
	cmpl	%eax, %r12d
	jne	.L1166
	movq	24(%rbx), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv(%rip), %rdx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1166
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE7760:
	.size	_ZN12v8_inspector10V8Debugger17stepIntoStatementEib, .-_ZN12v8_inspector10V8Debugger17stepIntoStatementEib
	.section	.text._ZN12v8_inspector10V8Debugger17stepOverStatementEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger17stepOverStatementEi
	.type	_ZN12v8_inspector10V8Debugger17stepOverStatementEi, @function
_ZN12v8_inspector10V8Debugger17stepOverStatementEi:
.LFB7761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib
	testb	%al, %al
	je	.L1189
.L1178:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1189:
	.cfi_restore_state
	movl	%r12d, 60(%rbx)
	movq	16(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v85debug11PrepareStepEPNS_7IsolateENS0_10StepActionE@PLT
	movl	64(%rbx), %eax
	testl	%eax, %eax
	je	.L1178
	cmpl	%eax, %r12d
	jne	.L1178
	movq	24(%rbx), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv(%rip), %rdx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1178
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE7761:
	.size	_ZN12v8_inspector10V8Debugger17stepOverStatementEi, .-_ZN12v8_inspector10V8Debugger17stepOverStatementEi
	.section	.text._ZN12v8_inspector10V8Debugger17stepOutOfFunctionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger17stepOutOfFunctionEi
	.type	_ZN12v8_inspector10V8Debugger17stepOutOfFunctionEi, @function
_ZN12v8_inspector10V8Debugger17stepOutOfFunctionEi:
.LFB7762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN12v8_inspector10V8Debugger22asyncStepOutOfFunctionEib
	testb	%al, %al
	je	.L1201
.L1190:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1201:
	.cfi_restore_state
	movl	%r12d, 60(%rbx)
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v85debug11PrepareStepEPNS_7IsolateENS0_10StepActionE@PLT
	movl	64(%rbx), %eax
	testl	%eax, %eax
	je	.L1190
	cmpl	%eax, %r12d
	jne	.L1190
	movq	24(%rbx), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv(%rip), %rdx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1190
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE7762:
	.size	_ZN12v8_inspector10V8Debugger17stepOutOfFunctionEi, .-_ZN12v8_inspector10V8Debugger17stepOutOfFunctionEi
	.section	.rodata._ZN12v8_inspector10V8Debugger18continueToLocationEiPNS_16V8DebuggerScriptESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS6_EERKNS_8String16E.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"Cannot continue to specified location"
	.section	.text._ZN12v8_inspector10V8Debugger18continueToLocationEiPNS_16V8DebuggerScriptESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS6_EERKNS_8String16E,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger18continueToLocationEiPNS_16V8DebuggerScriptESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS6_EERKNS_8String16E
	.type	_ZN12v8_inspector10V8Debugger18continueToLocationEiPNS_16V8DebuggerScriptESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS6_EERKNS_8String16E, @function
_ZN12v8_inspector10V8Debugger18continueToLocationEiPNS_16V8DebuggerScriptESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS6_EERKNS_8String16E:
.LFB7787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movl	%edx, -148(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	movl	%edx, 60(%rsi)
	xorl	%edx, %edx
	cmpb	$0, 52(%rax)
	je	.L1203
	movl	56(%rax), %edx
.L1203:
	leaq	-108(%rbp), %r9
	movl	48(%rax), %esi
	movq	%r10, -144(%rbp)
	leaq	-96(%rbp), %r13
	movq	%r9, %rdi
	movq	%r9, -136(%rbp)
	leaq	-80(%rbp), %r14
	call	_ZN2v85debug8LocationC1Eii@PLT
	xorl	%edx, %edx
	leaq	68(%rbx), %rcx
	movq	%r13, %rsi
	movq	-144(%rbp), %r10
	movq	-136(%rbp), %r9
	movq	(%r10), %rax
	movq	%r10, %rdi
	movq	152(%rax), %rax
	movw	%dx, -80(%rbp)
	movq	%r9, %rdx
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -64(%rbp)
	call	*%rax
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1204
	movb	%al, -136(%rbp)
	call	_ZdlPv@PLT
	movzbl	-136(%rbp), %eax
.L1204:
	testb	%al, %al
	je	.L1205
	leaq	72(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	32(%r15), %rax
	movq	_ZN12v8_inspector8protocol8Debugger18ContinueToLocation20TargetCallFramesEnum3AnyE(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, 104(%rbx)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	80(%rbx), %rdx
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	72(%rbx), %r8
	cmpq	%rsi, %rdx
	movq	%rsi, %rcx
	cmovbe	%rdx, %rcx
	testq	%rcx, %rcx
	je	.L1206
	xorl	%eax, %eax
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1247:
	ja	.L1226
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L1206
.L1208:
	movzwl	(%rdi,%rax,2), %r11d
	cmpw	%r11w, (%r8,%rax,2)
	jnb	.L1247
	movl	$-1, %r15d
.L1207:
	cmpq	%r14, %rdi
	jne	.L1220
.L1218:
	leaq	-120(%rbp), %rdi
	movl	$1, %edx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector10V8Debugger17captureStackTraceEb
	movq	-120(%rbp), %rax
	movq	112(%rbx), %rdi
	movq	$0, -120(%rbp)
	movq	%rax, 112(%rbx)
	testq	%rdi, %rdi
	je	.L1211
	movq	(%rdi), %rax
	call	*64(%rax)
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1211
	movq	(%rdi), %rax
	call	*64(%rax)
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1205:
	leaq	.LC14(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector8protocol16DispatchResponse5ErrorERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1202
	call	_ZdlPv@PLT
.L1202:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1248
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1206:
	.cfi_restore_state
	subq	%rsi, %rdx
	cmpq	$2147483647, %rdx
	jg	.L1221
	cmpq	$-2147483648, %rdx
	jl	.L1221
	movl	%edx, %r15d
	cmpq	%r14, %rdi
	je	.L1210
.L1220:
	call	_ZdlPv@PLT
.L1210:
	testl	%r15d, %r15d
	jne	.L1218
.L1211:
	movl	64(%rbx), %eax
	testl	%eax, %eax
	je	.L1215
	cmpl	%eax, -148(%rbp)
	je	.L1249
.L1215:
	movq	%r12, %rdi
	call	_ZN12v8_inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1221:
	cmpq	%r14, %rdi
	je	.L1218
	call	_ZdlPv@PLT
	jmp	.L1218
	.p2align 4,,10
	.p2align 3
.L1226:
	movl	$1, %r15d
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1249:
	movq	24(%rbx), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient22quitMessageLoopOnPauseEv(%rip), %rdx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1215
	call	*%rax
	jmp	.L1215
.L1248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7787:
	.size	_ZN12v8_inspector10V8Debugger18continueToLocationEiPNS_16V8DebuggerScriptESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS6_EERKNS_8String16E, .-_ZN12v8_inspector10V8Debugger18continueToLocationEiPNS_16V8DebuggerScriptESt10unique_ptrINS_8protocol8Debugger8LocationESt14default_deleteIS6_EERKNS_8String16E
	.section	.text._ZNSt6vectorISt10unique_ptrIN12v8_inspector16V8DebuggerScriptESt14default_deleteIS2_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN12v8_inspector16V8DebuggerScriptESt14default_deleteIS2_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN12v8_inspector16V8DebuggerScriptESt14default_deleteIS2_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorISt10unique_ptrIN12v8_inspector16V8DebuggerScriptESt14default_deleteIS2_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorISt10unique_ptrIN12v8_inspector16V8DebuggerScriptESt14default_deleteIS2_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB12680:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r12
	movq	%rdi, -80(%rbp)
	movq	%rax, -72(%rbp)
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1274
	movq	%rsi, %r9
	movq	%rsi, %r15
	movq	%rsi, %rbx
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L1265
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1275
.L1252:
	movq	%r13, %rdi
	movq	%rdx, -96(%rbp)
	movq	%r9, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdx
	addq	%rax, %r13
	movq	%rax, -56(%rbp)
	leaq	8(%rax), %rcx
	movq	%r13, -64(%rbp)
.L1264:
	movq	(%rdx), %rax
	movq	-56(%rbp), %rsi
	movq	$0, (%rdx)
	movq	%rax, (%rsi,%r9)
	cmpq	%r12, %r15
	je	.L1254
	movq	%rsi, %r13
	movq	%r12, %r14
	.p2align 4,,10
	.p2align 3
.L1258:
	movq	(%r14), %rcx
	movq	$0, (%r14)
	movq	%rcx, 0(%r13)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1255
	movq	(%rdi), %rcx
	addq	$8, %r14
	addq	$8, %r13
	call	*8(%rcx)
	cmpq	%r14, %r15
	jne	.L1258
.L1256:
	movq	-56(%rbp), %rsi
	movq	%r15, %rax
	subq	%r12, %rax
	leaq	8(%rsi,%rax), %rcx
.L1254:
	movq	-72(%rbp), %rax
	cmpq	%rax, %r15
	je	.L1259
	movq	%rax, %r14
	subq	%r15, %r14
	leaq	-8(%r14), %r9
	movq	%r9, %rdi
	shrq	$3, %rdi
	addq	$1, %rdi
	testq	%r9, %r9
	je	.L1267
	movq	%rdi, %rdx
	xorl	%eax, %eax
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L1261:
	movdqu	(%r15,%rax), %xmm1
	movups	%xmm1, (%rcx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1261
	movq	%rdi, %rax
	andq	$-2, %rax
	leaq	0(,%rax,8), %r8
	leaq	(%rcx,%r8), %rdx
	leaq	(%r15,%r8), %rbx
	cmpq	%rax, %rdi
	je	.L1262
.L1260:
	movq	(%rbx), %rax
	movq	%rax, (%rdx)
.L1262:
	leaq	8(%rcx,%r9), %rcx
.L1259:
	testq	%r12, %r12
	je	.L1263
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L1263:
	movq	-56(%rbp), %xmm0
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm2
	movq	-64(%rbp), %rbx
	punpcklqdq	%xmm2, %xmm0
	movq	%rbx, 16(%rax)
	movups	%xmm0, (%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1255:
	.cfi_restore_state
	addq	$8, %r14
	addq	$8, %r13
	cmpq	%r14, %r15
	jne	.L1258
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1275:
	testq	%rdi, %rdi
	jne	.L1253
	movq	$0, -64(%rbp)
	movl	$8, %ecx
	movq	$0, -56(%rbp)
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1265:
	movl	$8, %r13d
	jmp	.L1252
.L1267:
	movq	%rcx, %rdx
	jmp	.L1260
.L1253:
	cmpq	%rcx, %rdi
	movq	%rcx, %rax
	cmovbe	%rdi, %rax
	leaq	0(,%rax,8), %r13
	jmp	.L1252
.L1274:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE12680:
	.size	_ZNSt6vectorISt10unique_ptrIN12v8_inspector16V8DebuggerScriptESt14default_deleteIS2_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorISt10unique_ptrIN12v8_inspector16V8DebuggerScriptESt14default_deleteIS2_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZN12v8_inspector10V8Debugger18getCompiledScriptsEiPNS_19V8DebuggerAgentImplE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger18getCompiledScriptsEiPNS_19V8DebuggerAgentImplE
	.type	_ZN12v8_inspector10V8Debugger18getCompiledScriptsEiPNS_19V8DebuggerAgentImplE, @function
_ZN12v8_inspector10V8Debugger18getCompiledScriptsEiPNS_19V8DebuggerAgentImplE:
.LFB7732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$136, %rsp
	movl	%edx, -156(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 16(%rdi)
	leaq	-128(%rbp), %rax
	movups	%xmm0, (%rdi)
	movq	16(%rsi), %rsi
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r13), %rdi
	leaq	-96(%rbp), %rsi
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	%rdi, -96(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v85debug16GetLoadedScriptsEPNS_7IsolateERNS_21PersistentValueVectorINS0_6ScriptENS_34DefaultPersistentValueVectorTraitsEEE@PLT
	movq	-88(%rbp), %rdi
	cmpq	%rdi, -80(%rbp)
	je	.L1278
	.p2align 4,,10
	.p2align 3
.L1277:
	movq	(%rdi,%rbx,8), %rax
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.L1281
	movq	(%rax), %rsi
	movq	-96(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r14
.L1281:
	movq	%r14, %rdi
	call	_ZNK2v85debug6Script11WasCompiledEv@PLT
	testb	%al, %al
	je	.L1283
	movq	%r14, %rdi
	call	_ZNK2v85debug6Script10IsEmbeddedEv@PLT
	testb	%al, %al
	jne	.L1289
	movq	%r14, %rdi
	call	_ZNK2v85debug6Script9ContextIdEv@PLT
	movq	%rax, %rsi
	testb	%al, %al
	je	.L1283
	movq	24(%r13), %rdi
	sarq	$32, %rsi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	cmpl	-156(%rbp), %eax
	jne	.L1283
.L1289:
	movq	24(%r13), %rax
	movq	16(%r13), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	-152(%rbp), %r8
	leaq	-136(%rbp), %r15
	movq	16(%rax), %r9
	movq	%r15, %rdi
	call	_ZN12v8_inspector16V8DebuggerScript6CreateEPN2v87IsolateENS1_5LocalINS1_5debug6ScriptEEEbPNS_19V8DebuggerAgentImplEPNS_17V8InspectorClientE@PLT
	movq	8(%r12), %rsi
	cmpq	16(%r12), %rsi
	je	.L1318
	movq	-136(%rbp), %rax
	movq	$0, -136(%rbp)
	movq	%rax, (%rsi)
	addq	$8, 8(%r12)
.L1290:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1283
	movq	(%rdi), %rax
	call	*8(%rax)
.L1283:
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdi
	addq	$1, %rbx
	movq	%rcx, %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	movq	%rax, %r15
	cmpq	%rax, %rbx
	jb	.L1277
	testq	%r15, %r15
	je	.L1279
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rbx
	jnb	.L1293
	movq	(%rdi,%rbx,8), %r8
	testq	%r8, %r8
	je	.L1293
	movq	%r8, %rdi
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-88(%rbp), %rdi
	movq	-80(%rbp), %rcx
.L1293:
	addq	$1, %rbx
	cmpq	%rbx, %r15
	jne	.L1280
.L1279:
	cmpq	%rcx, %rdi
	je	.L1278
	movq	%rdi, -80(%rbp)
.L1278:
	testq	%rdi, %rdi
	je	.L1294
	call	_ZdlPv@PLT
.L1294:
	movq	-168(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1319
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1318:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorISt10unique_ptrIN12v8_inspector16V8DebuggerScriptESt14default_deleteIS2_EESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L1290
.L1319:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7732:
	.size	_ZN12v8_inspector10V8Debugger18getCompiledScriptsEiPNS_19V8DebuggerAgentImplE, .-_ZN12v8_inspector10V8Debugger18getCompiledScriptsEiPNS_19V8DebuggerAgentImplE
	.section	.text._ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS4_,"axG",@progbits,_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS4_
	.type	_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS4_, @function
_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS4_:
.LFB12833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r9
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r9, %rax
	divq	%r8
	leaq	0(,%rdx,8), %r15
	leaq	0(%r13,%r15), %r14
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1331
	movq	%rdi, %rbx
	movq	(%r12), %rdi
	movq	%rdx, %r11
	movq	%r12, %r10
	movq	8(%rdi), %rcx
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1338:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L1331
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rcx, %rax
	divq	%r8
	cmpq	%r11, %rdx
	jne	.L1331
	movq	%rsi, %rdi
.L1323:
	cmpq	%rcx, %r9
	jne	.L1338
	movq	(%rdi), %rcx
	cmpq	%r10, %r12
	je	.L1339
	testq	%rcx, %rcx
	je	.L1325
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L1325
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rcx
.L1325:
	movq	%rcx, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1331:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1339:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L1332
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L1325
	movq	%r10, 0(%r13,%rdx,8)
	addq	(%rbx), %r15
	movq	(%r15), %rax
	movq	%r15, %r14
.L1324:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L1340
.L1326:
	movq	$0, (%r14)
	movq	(%rdi), %rcx
	jmp	.L1325
.L1332:
	movq	%r10, %rax
	jmp	.L1324
.L1340:
	movq	%rcx, 16(%rbx)
	jmp	.L1326
	.cfi_endproc
.LFE12833:
	.size	_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS4_, .-_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS4_
	.section	.text._ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	.type	_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm, @function
_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm:
.LFB12865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1342
	movq	(%rbx), %r8
.L1343:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L1352
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L1353:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1342:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1366
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1367
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L1345:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1347
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L1348
	.p2align 4,,10
	.p2align 3
.L1349:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1350:
	testq	%rsi, %rsi
	je	.L1347
.L1348:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1349
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1355
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1348
	.p2align 4,,10
	.p2align 3
.L1347:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L1351
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1351:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1352:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1354
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L1354:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1355:
	movq	%rdx, %rdi
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1366:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L1345
.L1367:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE12865:
	.size	_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm, .-_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	.section	.text._ZN12v8_inspector10V8Debugger22setAsyncCallStackDepthEPNS_19V8DebuggerAgentImplEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger22setAsyncCallStackDepthEPNS_19V8DebuggerAgentImplEi
	.type	_ZN12v8_inspector10V8Debugger22setAsyncCallStackDepthEPNS_19V8DebuggerAgentImplEi, @function
_ZN12v8_inspector10V8Debugger22setAsyncCallStackDepthEPNS_19V8DebuggerAgentImplEi:
.LFB7860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	400(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -56(%rbp)
	testl	%edx, %edx
	jle	.L1391
	movq	408(%rdi), %rdi
	movl	%edx, %ebx
	movq	%rsi, %rax
	xorl	%edx, %edx
	movq	%rsi, %r13
	divq	%rdi
	movq	400(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L1371
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1392:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1371
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r15
	jne	.L1371
.L1373:
	cmpq	%rsi, %r13
	jne	.L1392
	addq	$16, %rcx
.L1379:
	movl	%ebx, (%rcx)
.L1370:
	movq	416(%r12), %rax
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	.L1374
	.p2align 4,,10
	.p2align 3
.L1375:
	movl	16(%rax), %edx
	movq	(%rax), %rax
	cmpl	%edx, %ebx
	cmovl	%edx, %ebx
	testq	%rax, %rax
	jne	.L1375
.L1374:
	cmpl	%ebx, 236(%r12)
	je	.L1368
	movq	24(%r12), %rax
	movl	%ebx, 236(%r12)
	leaq	_ZN12v8_inspector17V8InspectorClient29maxAsyncCallStackDepthChangedEi(%rip), %rdx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	208(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1393
.L1377:
	leaq	8(%r12), %rsi
	testl	%ebx, %ebx
	je	.L1394
.L1380:
	movq	16(%r12), %rdi
	call	_ZN2v85debug21SetAsyncEventDelegateEPNS_7IsolateEPNS0_18AsyncEventDelegateE@PLT
.L1368:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1391:
	.cfi_restore_state
	leaq	-56(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS4_
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	%r12, %rdi
	call	_ZN12v8_inspector10V8Debugger21allAsyncTasksCanceledEv
	xorl	%esi, %esi
	jmp	.L1380
	.p2align 4,,10
	.p2align 3
.L1393:
	movl	%ebx, %esi
	call	*%rax
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1371:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movq	-56(%rbp), %rax
	movl	$1, %r8d
	movl	$0, 16(%rcx)
	movq	%rax, 8(%rcx)
	call	_ZNSt10_HashtableIPN12v8_inspector19V8DebuggerAgentImplESt4pairIKS2_iESaIS5_ENSt8__detail10_Select1stESt8equal_toIS2_ESt4hashIS2_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L1379
	.cfi_endproc
.LFE7860:
	.size	_ZN12v8_inspector10V8Debugger22setAsyncCallStackDepthEPNS_19V8DebuggerAgentImplEi, .-_ZN12v8_inspector10V8Debugger22setAsyncCallStackDepthEPNS_19V8DebuggerAgentImplEi
	.section	.text._ZNSt10_HashtableImSt4pairIKmSt8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableImSt4pairIKmSt8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableImSt4pairIKmSt8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.type	_ZNSt10_HashtableImSt4pairIKmSt8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, @function
_ZNSt10_HashtableImSt4pairIKmSt8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm:
.LFB12913:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1396
	movq	(%rbx), %r8
.L1397:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L1406
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L1407:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1396:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1420
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1421
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L1399:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1401
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1403:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1404:
	testq	%rsi, %rsi
	je	.L1401
.L1402:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1403
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1409
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1402
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r10
	je	.L1405
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1405:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L1397
	.p2align 4,,10
	.p2align 3
.L1406:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1408
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L1408:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1409:
	movq	%rdx, %rdi
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1420:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L1399
.L1421:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE12913:
	.size	_ZNSt10_HashtableImSt4pairIKmSt8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, .-_ZNSt10_HashtableImSt4pairIKmSt8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.section	.text._ZN12v8_inspector10V8Debugger15storeStackTraceESt10shared_ptrINS_15AsyncStackTraceEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger15storeStackTraceESt10shared_ptrINS_15AsyncStackTraceEE
	.type	_ZN12v8_inspector10V8Debugger15storeStackTraceESt10shared_ptrINS_15AsyncStackTraceEE, @function
_ZN12v8_inspector10V8Debugger15storeStackTraceESt10shared_ptrINS_15AsyncStackTraceEE:
.LFB7864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	600(%rdi), %rax
	movq	%rdi, %rbx
	movq	552(%rdi), %rsi
	leaq	1(%rax), %r12
	movq	%r12, %rax
	movq	%r12, 600(%rdi)
	divq	%rsi
	movq	544(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L1423
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1423
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r14
	jne	.L1423
.L1425:
	cmpq	%rdi, %r12
	jne	.L1446
	leaq	16(%rcx), %rbx
.L1433:
	movq	0(%r13), %rax
	movq	8(%r13), %r13
	movq	%rax, (%rbx)
	testq	%r13, %r13
	je	.L1426
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1427
	lock addl	$1, 12(%r13)
.L1426:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1429
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1430
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L1447
.L1429:
	movq	%r13, 8(%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1427:
	.cfi_restore_state
	addl	$1, 12(%r13)
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1430:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L1429
.L1447:
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%r13, 8(%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1423:
	.cfi_restore_state
	movl	$32, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	$0, (%rax)
	leaq	544(%rbx), %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r12, 8(%rax)
	movups	%xmm0, 16(%rax)
	call	_ZNSt10_HashtableImSt4pairIKmSt8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS6_ENSt8__detail10_Select1stESt8equal_toImESt4hashImENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	leaq	16(%rax), %rbx
	jmp	.L1433
	.cfi_endproc
.LFE7864:
	.size	_ZN12v8_inspector10V8Debugger15storeStackTraceESt10shared_ptrINS_15AsyncStackTraceEE, .-_ZN12v8_inspector10V8Debugger15storeStackTraceESt10shared_ptrINS_15AsyncStackTraceEE
	.section	.text._ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.type	_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, @function
_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_:
.LFB12933:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1462
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L1458
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1463
.L1450:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L1457:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L1464
	testq	%r13, %r13
	jg	.L1453
	testq	%r9, %r9
	jne	.L1456
.L1454:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1464:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L1453
.L1456:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1463:
	testq	%rsi, %rsi
	jne	.L1451
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1453:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L1454
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1458:
	movl	$8, %r14d
	jmp	.L1450
.L1462:
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1451:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L1450
	.cfi_endproc
.LFE12933:
	.size	_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, .-_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.section	.text._ZN12v8_inspector10V8Debugger24externalAsyncTaskStartedERKNS_14V8StackTraceIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger24externalAsyncTaskStartedERKNS_14V8StackTraceIdE
	.type	_ZN12v8_inspector10V8Debugger24externalAsyncTaskStartedERKNS_14V8StackTraceIdE, @function
_ZN12v8_inspector10V8Debugger24externalAsyncTaskStartedERKNS_14V8StackTraceIdE:
.LFB7865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	236(%rdi), %eax
	testl	%eax, %eax
	jne	.L1488
.L1465:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1489
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1488:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	_ZNK12v8_inspector14V8StackTraceId9IsInvalidEv@PLT
	testb	%al, %al
	jne	.L1465
	movq	296(%rbx), %rsi
	cmpq	304(%rbx), %rsi
	je	.L1469
	movdqu	(%r12), %xmm1
	movups	%xmm1, (%rsi)
	movq	16(%r12), %rax
	movq	%rax, 16(%rsi)
	addq	$24, 296(%rbx)
.L1470:
	movq	272(%rbx), %rsi
	cmpq	280(%rbx), %rsi
	je	.L1471
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	addq	$16, 272(%rbx)
.L1472:
	movq	(%r12), %rax
	movq	248(%rbx), %rsi
	movq	%rax, -72(%rbp)
	cmpq	256(%rbx), %rsi
	je	.L1473
	movq	%rax, (%rsi)
	addq	$8, 248(%rbx)
.L1474:
	cmpb	$0, 504(%rbx)
	jne	.L1465
	cmpq	$0, 472(%rbx)
	je	.L1465
	movq	(%r12), %rax
	cmpq	%rax, 456(%rbx)
	jne	.L1465
	leaq	-64(%rbp), %rdi
	leaq	8(%r12), %rsi
	call	_ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE@PLT
	movq	472(%rbx), %rsi
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rdi
	movq	464(%rbx), %rcx
	cmpq	%r8, %rsi
	movq	%r8, %rdx
	cmovbe	%rsi, %rdx
	testq	%rdx, %rdx
	je	.L1476
	xorl	%eax, %eax
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1490:
	addq	$1, %rax
	cmpq	%rdx, %rax
	je	.L1476
.L1478:
	movzwl	(%rdi,%rax,2), %r9d
	cmpw	%r9w, (%rcx,%rax,2)
	je	.L1490
.L1477:
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1465
	call	_ZdlPv@PLT
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1469:
	leaq	288(%rbx), %rdi
	movq	%r12, %rdx
	call	_ZNSt6vectorIN12v8_inspector14V8StackTraceIdESaIS1_EE17_M_realloc_insertIJRKS1_EEEvN9__gnu_cxx17__normal_iteratorIPS1_S3_EEDpOT_
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1473:
	leaq	-72(%rbp), %rdx
	leaq	240(%rbx), %rdi
	call	_ZNSt6vectorIPvSaIS0_EE17_M_realloc_insertIJS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	jmp	.L1474
	.p2align 4,,10
	.p2align 3
.L1471:
	leaq	264(%rbx), %rdi
	call	_ZNSt6vectorISt10shared_ptrIN12v8_inspector15AsyncStackTraceEESaIS3_EE17_M_realloc_insertIJEEEvN9__gnu_cxx17__normal_iteratorIPS3_S5_EEDpOT_
	jmp	.L1472
.L1476:
	subq	%r8, %rsi
	cmpq	$2147483647, %rsi
	jg	.L1477
	cmpq	$-2147483648, %rsi
	jl	.L1477
	testl	%esi, %esi
	jne	.L1477
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1480
	call	_ZdlPv@PLT
.L1480:
	movq	16(%rbx), %rdi
	call	_ZN2v85debug26SetBreakOnNextFunctionCallEPNS_7IsolateE@PLT
	jmp	.L1465
.L1489:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7865:
	.size	_ZN12v8_inspector10V8Debugger24externalAsyncTaskStartedERKNS_14V8StackTraceIdE, .-_ZN12v8_inspector10V8Debugger24externalAsyncTaskStartedERKNS_14V8StackTraceIdE
	.section	.text._ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	.type	_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm, @function
_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm:
.LFB12963:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1492
	movq	(%rbx), %r8
.L1493:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L1502
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L1503:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1492:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1516
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1517
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L1495:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1497
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1499:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1500:
	testq	%rsi, %rsi
	je	.L1497
.L1498:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1499
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1505
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1498
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L1501
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1501:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1502:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1504
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L1504:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1505:
	movq	%rdx, %rdi
	jmp	.L1500
	.p2align 4,,10
	.p2align 3
.L1516:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L1495
.L1517:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE12963:
	.size	_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm, .-_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt8weak_ptrIN12v8_inspector10StackFrameEEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt8weak_ptrIN12v8_inspector10StackFrameEEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt8weak_ptrIN12v8_inspector10StackFrameEEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt8weak_ptrIN12v8_inspector10StackFrameEEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt8weak_ptrIN12v8_inspector10StackFrameEEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm:
.LFB13059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1519
	movq	(%rbx), %r8
.L1520:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L1529
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L1530:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1519:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1543
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1544
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L1522:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1524
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L1525
	.p2align 4,,10
	.p2align 3
.L1526:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1527:
	testq	%rsi, %rsi
	je	.L1524
.L1525:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1526
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1532
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1525
	.p2align 4,,10
	.p2align 3
.L1524:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L1528
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1528:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L1529:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1531
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L1531:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1530
	.p2align 4,,10
	.p2align 3
.L1532:
	movq	%rdx, %rdi
	jmp	.L1527
	.p2align 4,,10
	.p2align 3
.L1543:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L1522
.L1544:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE13059:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt8weak_ptrIN12v8_inspector10StackFrameEEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt8weak_ptrIN12v8_inspector10StackFrameEEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.section	.text.unlikely._ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE,"ax",@progbits
	.align 2
.LCOLDB15:
	.section	.text._ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE,"ax",@progbits
.LHOTB15:
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE
	.type	_ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE, @function
_ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE:
.LFB7907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	236(%rsi), %r14d
	movq	$0, -64(%rbp)
	testl	%r14d, %r14d
	jne	.L1590
.L1546:
	movq	16(%rbx), %r13
	movl	$136, %edi
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN12v8_inspector10StackFrameC1EPN2v87IsolateENS1_5LocalINS1_10StackFrameEEE@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	movq	%r12, %rdi
	movabsq	$4294967297, %rsi
	movq	%rsi, 8(%rax)
	movq	%rax, %r13
	movq	%rcx, (%rax)
	movq	-56(%rbp), %rax
	movq	%rax, 16(%r13)
	call	_ZNK2v810StackFrame6IsWasmEv@PLT
	testb	%al, %al
	jne	.L1591
.L1551:
	movl	236(%rbx), %eax
	testl	%eax, %eax
	jne	.L1592
.L1556:
	movq	-56(%rbp), %xmm0
	movq	%r13, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r15)
.L1545:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1590:
	.cfi_restore_state
	movq	%rdx, %rdi
	call	_ZN2v85debug15GetStackFrameIdENS_5LocalINS_10StackFrameEEE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	%eax, %r14d
	cltq
	movq	%rax, -64(%rbp)
	divq	%rdi
	movq	344(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L1546
	movq	(%rax), %rcx
	movl	8(%rcx), %esi
	jmp	.L1548
	.p2align 4,,10
	.p2align 3
.L1593:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1546
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L1546
.L1548:
	cmpl	%esi, %r14d
	jne	.L1593
	movq	24(%rcx), %rax
	testq	%rax, %rax
	je	.L1546
	movl	8(%rax), %edx
	testl	%edx, %edx
	je	.L1546
	movq	%rax, 8(%r15)
	leaq	8(%rax), %rdx
	movl	8(%rax), %eax
.L1554:
	testl	%eax, %eax
	je	.L1587
	leal	1(%rax), %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L1554
	movq	16(%rcx), %rax
	movq	%rax, (%r15)
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	352(%rbx), %r9
	movq	-64(%rbp), %rax
	xorl	%edx, %edx
	divq	%r9
	movq	344(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r12
	testq	%rax, %rax
	je	.L1557
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1594:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1557
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r9
	cmpq	%rdx, %r12
	jne	.L1557
.L1559:
	cmpl	%edi, %r14d
	jne	.L1594
	leaq	16(%rcx), %rbx
.L1560:
	movq	-56(%rbp), %rax
	movq	%rax, (%rbx)
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L1595
	lock addl	$1, 12(%r13)
.L1561:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1563
	testq	%rax, %rax
	je	.L1564
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1565:
	cmpl	$1, %eax
	jne	.L1563
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1563:
	movq	%r13, 8(%rbx)
	jmp	.L1556
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	-56(%rbp), %rdi
	leaq	728(%rbx), %rsi
	call	_ZN12v8_inspector10StackFrame9translateEPNS_15WasmTranslationE@PLT
	jmp	.L1551
	.p2align 4,,10
	.p2align 3
.L1595:
	addl	$1, 12(%r13)
	jmp	.L1561
	.p2align 4,,10
	.p2align 3
.L1564:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1557:
	movl	$32, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	-64(%rbp), %rdx
	movq	%r12, %rsi
	movq	$0, (%rax)
	leaq	344(%rbx), %rdi
	movq	%rax, %rcx
	movl	$1, %r8d
	movl	%r14d, 8(%rax)
	movups	%xmm0, 16(%rax)
	call	_ZNSt10_HashtableIiSt4pairIKiSt8weak_ptrIN12v8_inspector10StackFrameEEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	leaq	16(%rax), %rbx
	jmp	.L1560
	.cfi_endproc
	.section	.text.unlikely._ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE
	.cfi_startproc
	.type	_ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE.cold, @function
_ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE.cold:
.LFSB7907:
.L1587:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE7907:
	.section	.text._ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE
	.size	_ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE, .-_ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE
	.section	.text.unlikely._ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE
	.size	_ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE.cold, .-_ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE.cold
.LCOLDE15:
	.section	.text._ZN12v8_inspector10V8Debugger9symbolizeEN2v85LocalINS1_10StackFrameEEE
.LHOTE15:
	.section	.text._ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S2_IllEESaIS5_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S4_EEEES2_INS7_14_Node_iteratorIS5_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S2_IllEESaIS5_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S4_EEEES2_INS7_14_Node_iteratorIS5_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S2_IllEESaIS5_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S4_EEEES2_INS7_14_Node_iteratorIS5_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S2_IllEESaIS5_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S4_EEEES2_INS7_14_Node_iteratorIS5_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S2_IllEESaIS5_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S4_EEEES2_INS7_14_Node_iteratorIS5_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB13069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movl	$72, %edi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_Znwm@PLT
	movq	0(%r13), %rdi
	leaq	24(%rax), %r10
	movq	$0, (%rax)
	movq	%rax, %r12
	movq	%r10, 8(%rax)
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1651
	movq	16(%r13), %rdx
	movq	%rdi, 8(%r12)
	movq	%rdx, 24(%r12)
.L1598:
	movq	8(%r13), %r9
	movq	32(%r13), %rbx
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	movdqu	40(%r13), %xmm0
	movq	$0, 8(%r13)
	movq	%r9, 16(%r12)
	movw	%ax, 16(%r13)
	movq	%rbx, 40(%r12)
	movups	%xmm0, 48(%r12)
	testq	%rbx, %rbx
	jne	.L1599
	leaq	(%rdi,%r9,2), %rcx
	cmpq	%rdi, %rcx
	je	.L1600
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L1601:
	movq	%rbx, %rdx
	addq	$2, %rax
	salq	$5, %rdx
	subq	%rbx, %rdx
	movsbq	-2(%rax), %rbx
	addq	%rdx, %rbx
	movq	%rbx, 40(%r12)
	cmpq	%rax, %rcx
	jne	.L1601
	testq	%rbx, %rbx
	je	.L1600
.L1599:
	movq	8(%r14), %rsi
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	leaq	0(,%rdx,8), %r15
	testq	%rax, %rax
	je	.L1602
	movq	(%rax), %r13
	movq	64(%r13), %rcx
.L1607:
	cmpq	%rbx, %rcx
	je	.L1652
.L1603:
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L1602
	movq	64(%r13), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	je	.L1607
.L1602:
	movq	24(%r14), %rdx
	leaq	32(%r14), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r13
	testb	%al, %al
	jne	.L1609
	movq	(%r14), %r8
.L1610:
	leaq	(%r8,%r15), %rax
	movq	%rbx, 64(%r12)
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1619
	movq	(%rdx), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rax
	movq	%r12, (%rax)
.L1620:
	addq	$1, 24(%r14)
	addq	$24, %rsp
	movq	%r12, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1652:
	.cfi_restore_state
	movq	16(%r13), %r11
	movq	8(%r13), %rcx
	cmpq	%r11, %r9
	movq	%r11, %rdx
	cmovbe	%r9, %rdx
	testq	%rdx, %rdx
	je	.L1604
	movq	%r11, -56(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1605:
	movzwl	(%rcx,%rax,2), %r11d
	cmpw	%r11w, (%rdi,%rax,2)
	jne	.L1603
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L1605
	movq	-56(%rbp), %r11
.L1604:
	movq	%r9, %rax
	movl	$2147483648, %ecx
	subq	%r11, %rax
	cmpq	%rcx, %rax
	jge	.L1603
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L1603
	testl	%eax, %eax
	jne	.L1603
	cmpq	%r10, %rdi
	je	.L1623
	call	_ZdlPv@PLT
.L1623:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r13, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1600:
	.cfi_restore_state
	movq	$1, 40(%r12)
	movl	$1, %ebx
	jmp	.L1599
	.p2align 4,,10
	.p2align 3
.L1651:
	movdqu	16(%r13), %xmm1
	movq	%r10, %rdi
	movups	%xmm1, 24(%r12)
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1609:
	cmpq	$1, %rdx
	je	.L1653
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1654
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r14), %r10
	movq	%rax, %r8
.L1612:
	movq	16(%r14), %rsi
	movq	$0, 16(%r14)
	testq	%rsi, %rsi
	je	.L1614
	xorl	%edi, %edi
	leaq	16(%r14), %r9
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1617:
	testq	%rsi, %rsi
	je	.L1614
.L1615:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	64(%rcx), %rax
	divq	%r13
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1616
	movq	16(%r14), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r14)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1625
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1615
	.p2align 4,,10
	.p2align 3
.L1614:
	movq	(%r14), %rdi
	cmpq	%r10, %rdi
	je	.L1618
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1618:
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	%r13, 8(%r14)
	divq	%r13
	movq	%r8, (%r14)
	leaq	0(,%rdx,8), %r15
	jmp	.L1610
	.p2align 4,,10
	.p2align 3
.L1619:
	movq	16(%r14), %rdx
	movq	%r12, 16(%r14)
	movq	%rdx, (%r12)
	testq	%rdx, %rdx
	je	.L1621
	movq	64(%rdx), %rax
	xorl	%edx, %edx
	divq	8(%r14)
	movq	%r12, (%r8,%rdx,8)
	movq	(%r14), %rax
	addq	%r15, %rax
.L1621:
	leaq	16(%r14), %rdx
	movq	%rdx, (%rax)
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1625:
	movq	%rdx, %rdi
	jmp	.L1617
.L1653:
	leaq	48(%r14), %r8
	movq	$0, 48(%r14)
	movq	%r8, %r10
	jmp	.L1612
.L1654:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE13069:
	.size	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S2_IllEESaIS5_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S4_EEEES2_INS7_14_Node_iteratorIS5_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S2_IllEESaIS5_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S4_EEEES2_INS7_14_Node_iteratorIS5_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm:
.LFB13145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	subq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, %r13
	leaq	(%r9,%rax), %r15
	subq	%rsi, %r13
	cmpq	(%rdi), %r14
	je	.L1668
	movq	16(%rdi), %rax
.L1656:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %r15
	ja	.L1689
	cmpq	%rax, %r15
	jbe	.L1658
	addq	%rax, %rax
	cmpq	%rax, %r15
	jnb	.L1658
	movabsq	$4611686018427387904, %rdi
	movq	%rdx, %r15
	cmpq	%rdx, %rax
	ja	.L1659
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L1658:
	leaq	2(%r15,%r15), %rdi
.L1659:
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	testq	%r12, %r12
	movq	(%rbx), %r11
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	%rax, %r10
	je	.L1661
	cmpq	$1, %r12
	je	.L1690
	movq	%r12, %rdx
	addq	%rdx, %rdx
	je	.L1661
	movq	%r11, %rsi
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r11
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L1661:
	testq	%rcx, %rcx
	je	.L1663
	testq	%r8, %r8
	je	.L1663
	leaq	(%r10,%r12,2), %rdi
	cmpq	$1, %r8
	je	.L1691
	movq	%r8, %rdx
	addq	%rdx, %rdx
	je	.L1663
	movq	%rcx, %rsi
	movq	%r8, -80(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
.L1663:
	testq	%r13, %r13
	jne	.L1692
.L1665:
	cmpq	%r11, %r14
	je	.L1667
	movq	%r11, %rdi
	movq	%r10, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r10
.L1667:
	movq	%r15, 16(%rbx)
	movq	%r10, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1692:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	addq	%r12, %r8
	leaq	(%r10,%r8,2), %rdi
	leaq	(%r11,%rax,2), %rsi
	cmpq	$1, %r13
	je	.L1693
	addq	%r13, %r13
	je	.L1665
	movq	%r13, %rdx
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1668:
	movl	$7, %eax
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1693:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	jmp	.L1665
	.p2align 4,,10
	.p2align 3
.L1690:
	movzwl	(%r11), %eax
	movw	%ax, (%r10)
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L1691:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	testq	%r13, %r13
	je	.L1665
	jmp	.L1692
.L1689:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13145:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.section	.rodata._ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE.str1.1,"aMS",@progbits,1
.LC16:
	.string	")"
.LC17:
	.string	" ("
	.section	.rodata._ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE.str1.1
.LC19:
	.string	"Global"
.LC20:
	.string	"Local"
.LC21:
	.string	"With Block"
.LC22:
	.string	"Closure"
.LC23:
	.string	"Catch"
.LC24:
	.string	"Block"
.LC25:
	.string	"Script"
.LC26:
	.string	"Eval"
.LC27:
	.string	"Module"
.LC28:
	.string	"description"
.LC29:
	.string	"object"
	.section	.text._ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE
	.type	_ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE, @function
_ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE:
.LFB7826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1096, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L1695
	cmpl	$1, %ecx
	jne	.L1697
	movq	%rdx, %rdi
	call	_ZN2v85debug15GeneratorObject4CastENS_5LocalINS_5ValueEEE@PLT
	movq	%rax, %rdi
	call	_ZN2v85debug15GeneratorObject11IsSuspendedEv@PLT
	testb	%al, %al
	jne	.L1699
.L1697:
	xorl	%r14d, %r14d
.L1700:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2205
	addq	$1096, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1695:
	.cfi_restore_state
	movq	16(%rbx), %rsi
	leaq	-1080(%rbp), %rdi
	call	_ZN2v85debug13ScopeIterator17CreateForFunctionEPNS_7IsolateENS_5LocalINS_8FunctionEEE@PLT
	movq	-1080(%rbp), %r12
.L1698:
	testq	%r12, %r12
	je	.L1697
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	%r13, %rsi
	movq	%rax, %r14
	movq	16(%rbx), %rax
	movq	%r14, %rdi
	leaq	104(%rax), %rdx
	call	_ZN2v86Object12SetPrototypeENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1707
	shrw	$8, %ax
	je	.L1707
	leaq	-720(%rbp), %rax
	movq	%rax, -1120(%rbp)
	leaq	-656(%rbp), %rax
	movq	%rax, -1096(%rbp)
	.p2align 4,,10
	.p2align 3
.L1704:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testb	%al, %al
	jne	.L1706
	movq	16(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	24(%rbx), %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1707
	movl	$2, %edx
	movq	%r15, %rsi
	call	_ZN12v8_inspector16InspectedContext17addInternalObjectEN2v85LocalINS1_6ObjectEEENS_19V8InternalValueTypeE@PLT
	testb	%al, %al
	je	.L1707
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*48(%rax)
	movq	16(%rbx), %rsi
	movq	-1120(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE@PLT
	movq	-1096(%rbp), %rax
	movq	$0, -664(%rbp)
	movq	$0, -640(%rbp)
	movq	%rax, -672(%rbp)
	xorl	%eax, %eax
	cmpq	$0, -712(%rbp)
	movw	%ax, -656(%rbp)
	jne	.L2206
.L1709:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*32(%rax)
	cmpl	$8, %eax
	ja	.L1741
	leaq	.L1743(%rip), %rcx
	movl	%eax, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE,"a",@progbits
	.align 4
	.align 4
.L1743:
	.long	.L1751-.L1743
	.long	.L1750-.L1743
	.long	.L1749-.L1743
	.long	.L1748-.L1743
	.long	.L1747-.L1743
	.long	.L1746-.L1743
	.long	.L1745-.L1743
	.long	.L1744-.L1743
	.long	.L1742-.L1743
	.section	.text._ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE
	.p2align 4,,10
	.p2align 3
.L1706:
	movq	%r13, %rdi
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	24(%rbx), %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1707
	movl	$3, %edx
	movq	%r14, %rsi
	call	_ZN12v8_inspector16InspectedContext17addInternalObjectEN2v85LocalINS1_6ObjectEEENS_19V8InternalValueTypeE@PLT
	testb	%al, %al
	jne	.L1705
	.p2align 4,,10
	.p2align 3
.L1707:
	xorl	%r14d, %r14d
.L1705:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L1699:
	movq	16(%rbx), %rsi
	movq	%r12, %rdx
	leaq	-1080(%rbp), %rdi
	call	_ZN2v85debug13ScopeIterator24CreateForGeneratorObjectEPNS_7IsolateENS_5LocalINS_6ObjectEEE@PLT
	movq	-1080(%rbp), %r12
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1744:
	leaq	.LC26(%rip), %rsi
	leaq	-144(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rsi
	leaq	-768(%rbp), %r9
	movq	%r9, -784(%rbp)
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L1950
	testq	%rsi, %rsi
	je	.L1721
.L1950:
	movq	%rdx, %rcx
	movq	%r9, %rdi
	sarq	%rcx
	cmpq	$14, %rdx
	ja	.L2207
.L1872:
	cmpq	$2, %rdx
	je	.L2208
	testq	%rdx, %rdx
	je	.L1874
	movq	%r9, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	%rdx, -1104(%rbp)
	call	memmove@PLT
	movq	-784(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %rcx
	movq	-1104(%rbp), %rdx
.L1874:
	xorl	%eax, %eax
	movq	%rcx, -776(%rbp)
	movw	%ax, (%rdi,%rdx)
	movq	-784(%rbp), %rdi
	movl	$7, %eax
	movq	-712(%rbp), %r8
	movq	-776(%rbp), %rsi
	cmpq	%r9, %rdi
	cmovne	-768(%rbp), %rax
	movq	-720(%rbp), %rcx
	leaq	(%r8,%rsi), %r10
	cmpq	%rax, %r10
	ja	.L1876
	leaq	-784(%rbp), %r11
	testq	%r8, %r8
	je	.L1877
	leaq	(%rdi,%rsi,2), %rax
	cmpq	$1, %r8
	je	.L2209
	addq	%r8, %r8
	je	.L1877
	movq	%rax, %rdi
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -1128(%rbp)
	movq	%r11, -1112(%rbp)
	movq	%r10, -1104(%rbp)
	call	memmove@PLT
	movq	-784(%rbp), %rdi
	movq	-1104(%rbp), %r10
	movq	-1112(%rbp), %r11
	movq	-1128(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1877:
	xorl	%eax, %eax
	movq	%r10, -776(%rbp)
	movq	%r11, %rsi
	movw	%ax, (%rdi,%r10,2)
	leaq	-96(%rbp), %rdi
	movq	%r9, -1104(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-784(%rbp), %rdi
	movq	-1104(%rbp), %r9
	cmpq	%r9, %rdi
	je	.L1879
	call	_ZdlPv@PLT
.L1879:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1880
	call	_ZdlPv@PLT
.L1880:
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	-672(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rcx, %rdx
	je	.L2198
	movq	-80(%rbp), %rsi
	cmpq	-1096(%rbp), %rdi
	je	.L2210
.L1885:
	movq	%rax, %xmm0
	movq	%rsi, %xmm5
	movq	-656(%rbp), %r8
	movq	%rdx, -672(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, -664(%rbp)
	testq	%rdi, %rdi
	jne	.L2174
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1745:
	leaq	.LC25(%rip), %rsi
	leaq	-192(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-184(%rbp), %rax
	movq	-192(%rbp), %rsi
	leaq	-800(%rbp), %r9
	movq	%r9, -816(%rbp)
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L1949
	testq	%rsi, %rsi
	je	.L1721
.L1949:
	movq	%rdx, %rcx
	movq	%r9, %rdi
	sarq	%rcx
	cmpq	$14, %rdx
	ja	.L2211
.L1855:
	cmpq	$2, %rdx
	je	.L2212
	testq	%rdx, %rdx
	je	.L1857
	movq	%r9, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	%rdx, -1104(%rbp)
	call	memmove@PLT
	movq	-816(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %rcx
	movq	-1104(%rbp), %rdx
.L1857:
	xorl	%eax, %eax
	movq	%rcx, -808(%rbp)
	movw	%ax, (%rdi,%rdx)
	movq	-816(%rbp), %rdi
	movl	$7, %eax
	movq	-712(%rbp), %r8
	movq	-808(%rbp), %rsi
	cmpq	%r9, %rdi
	cmovne	-800(%rbp), %rax
	movq	-720(%rbp), %rcx
	leaq	(%r8,%rsi), %r10
	cmpq	%rax, %r10
	ja	.L1859
	leaq	-816(%rbp), %r11
	testq	%r8, %r8
	je	.L1860
	leaq	(%rdi,%rsi,2), %rax
	cmpq	$1, %r8
	je	.L2213
	addq	%r8, %r8
	je	.L1860
	movq	%rax, %rdi
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -1128(%rbp)
	movq	%r11, -1112(%rbp)
	movq	%r10, -1104(%rbp)
	call	memmove@PLT
	movq	-816(%rbp), %rdi
	movq	-1104(%rbp), %r10
	movq	-1112(%rbp), %r11
	movq	-1128(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1860:
	xorl	%eax, %eax
	movq	%r10, -808(%rbp)
	movq	%r11, %rsi
	movw	%ax, (%rdi,%r10,2)
	leaq	-96(%rbp), %rdi
	movq	%r9, -1104(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-816(%rbp), %rdi
	movq	-1104(%rbp), %r9
	cmpq	%r9, %rdi
	je	.L1862
	call	_ZdlPv@PLT
.L1862:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1863
	call	_ZdlPv@PLT
.L1863:
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	-672(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rcx, %rdx
	je	.L2198
	movq	-80(%rbp), %rsi
	cmpq	-1096(%rbp), %rdi
	je	.L2214
.L1868:
	movq	%rax, %xmm0
	movq	%rsi, %xmm4
	movq	-656(%rbp), %r8
	movq	%rdx, -672(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -664(%rbp)
	testq	%rdi, %rdi
	je	.L1886
.L2174:
	movq	%rdi, -96(%rbp)
	movq	%r8, -80(%rbp)
.L1884:
	xorl	%r11d, %r11d
	movq	$0, -88(%rbp)
	movw	%r11w, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -640(%rbp)
	cmpq	%rcx, %rdi
	je	.L1741
.L2166:
	call	_ZdlPv@PLT
.L1741:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*40(%rax)
	movq	16(%rbx), %rdi
	leaq	-672(%rbp), %rsi
	movq	%rax, -1112(%rbp)
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	16(%rbx), %rdi
	leaq	.LC28(%rip), %rsi
	movq	%rax, -1104(%rbp)
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	movq	-1104(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_6ObjectEEENS1_INS0_4NameEEENS1_INS0_5ValueEEE@PLT
	movq	16(%rbx), %rdi
	leaq	.LC29(%rip), %rsi
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	movq	-1112(%rbp), %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r8, %rcx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_6ObjectEEENS1_INS0_4NameEEENS1_INS0_5ValueEEE@PLT
	movq	%r14, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	%r13, %rdi
	movq	%r15, %rcx
	movq	%r14, %rsi
	movl	%eax, %edx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE@PLT
	movq	-672(%rbp), %rdi
	cmpq	-1096(%rbp), %rdi
	je	.L1905
	call	_ZdlPv@PLT
.L1905:
	movq	-720(%rbp), %rdi
	leaq	-704(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1906
	call	_ZdlPv@PLT
.L1906:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1704
	.p2align 4,,10
	.p2align 3
.L1747:
	leaq	.LC23(%rip), %rsi
	leaq	-288(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %rsi
	leaq	-864(%rbp), %r9
	movq	%r9, -880(%rbp)
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L1947
	testq	%rsi, %rsi
	je	.L1721
.L1947:
	movq	%rdx, %rcx
	movq	%r9, %rdi
	sarq	%rcx
	cmpq	$14, %rdx
	ja	.L2215
.L1821:
	cmpq	$2, %rdx
	je	.L2216
	testq	%rdx, %rdx
	je	.L1823
	movq	%r9, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	%rdx, -1104(%rbp)
	call	memmove@PLT
	movq	-880(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %rcx
	movq	-1104(%rbp), %rdx
.L1823:
	xorl	%r8d, %r8d
	movq	%rcx, -872(%rbp)
	movl	$7, %eax
	movw	%r8w, (%rdi,%rdx)
	movq	-880(%rbp), %rdi
	movq	-712(%rbp), %r8
	movq	-872(%rbp), %rsi
	cmpq	%r9, %rdi
	cmovne	-864(%rbp), %rax
	movq	-720(%rbp), %rcx
	leaq	(%r8,%rsi), %r10
	cmpq	%rax, %r10
	ja	.L1825
	leaq	-880(%rbp), %r11
	testq	%r8, %r8
	je	.L1826
	leaq	(%rdi,%rsi,2), %rax
	cmpq	$1, %r8
	je	.L2217
	addq	%r8, %r8
	je	.L1826
	movq	%rax, %rdi
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -1128(%rbp)
	movq	%r11, -1112(%rbp)
	movq	%r10, -1104(%rbp)
	call	memmove@PLT
	movq	-880(%rbp), %rdi
	movq	-1104(%rbp), %r10
	movq	-1112(%rbp), %r11
	movq	-1128(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1826:
	xorl	%esi, %esi
	movq	%r10, -872(%rbp)
	movw	%si, (%rdi,%r10,2)
	leaq	-96(%rbp), %rdi
	movq	%r11, %rsi
	movq	%r9, -1104(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-880(%rbp), %rdi
	movq	-1104(%rbp), %r9
	cmpq	%r9, %rdi
	je	.L1828
	call	_ZdlPv@PLT
.L1828:
	movq	-288(%rbp), %rdi
	leaq	-272(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1829
	call	_ZdlPv@PLT
.L1829:
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	-672(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rcx, %rdx
	je	.L2198
	movq	-80(%rbp), %rsi
	cmpq	-1096(%rbp), %rdi
	je	.L2180
	movq	%rax, %xmm0
	movq	%rsi, %xmm7
	movq	-656(%rbp), %r8
	movq	%rdx, -672(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -664(%rbp)
	testq	%rdi, %rdi
	jne	.L2174
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1746:
	leaq	.LC24(%rip), %rsi
	leaq	-240(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-232(%rbp), %rax
	movq	-240(%rbp), %rsi
	leaq	-832(%rbp), %r9
	movq	%r9, -848(%rbp)
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L1948
	testq	%rsi, %rsi
	je	.L1721
.L1948:
	movq	%rdx, %rcx
	movq	%r9, %rdi
	sarq	%rcx
	cmpq	$14, %rdx
	ja	.L2218
.L1838:
	cmpq	$2, %rdx
	je	.L2219
	testq	%rdx, %rdx
	je	.L1840
	movq	%r9, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	%rdx, -1104(%rbp)
	call	memmove@PLT
	movq	-848(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %rcx
	movq	-1104(%rbp), %rdx
.L1840:
	movq	%rcx, -840(%rbp)
	xorl	%ecx, %ecx
	movl	$7, %eax
	movw	%cx, (%rdi,%rdx)
	movq	-848(%rbp), %rdi
	movq	-712(%rbp), %r8
	movq	-840(%rbp), %rsi
	cmpq	%r9, %rdi
	cmovne	-832(%rbp), %rax
	movq	-720(%rbp), %rcx
	leaq	(%r8,%rsi), %r10
	cmpq	%rax, %r10
	ja	.L1842
	leaq	-848(%rbp), %r11
	testq	%r8, %r8
	je	.L1843
	leaq	(%rdi,%rsi,2), %rax
	cmpq	$1, %r8
	je	.L2220
	addq	%r8, %r8
	je	.L1843
	movq	%rax, %rdi
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -1128(%rbp)
	movq	%r11, -1112(%rbp)
	movq	%r10, -1104(%rbp)
	call	memmove@PLT
	movq	-848(%rbp), %rdi
	movq	-1104(%rbp), %r10
	movq	-1112(%rbp), %r11
	movq	-1128(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1843:
	xorl	%edx, %edx
	movq	%r10, -840(%rbp)
	movq	%r11, %rsi
	movw	%dx, (%rdi,%r10,2)
	leaq	-96(%rbp), %rdi
	movq	%r9, -1104(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-848(%rbp), %rdi
	movq	-1104(%rbp), %r9
	cmpq	%r9, %rdi
	je	.L1845
	call	_ZdlPv@PLT
.L1845:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1846
	call	_ZdlPv@PLT
.L1846:
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	-672(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rcx, %rdx
	je	.L2198
	movq	-80(%rbp), %rsi
	cmpq	-1096(%rbp), %rdi
	je	.L2221
.L1851:
	movq	%rax, %xmm0
	movq	%rsi, %xmm3
	movq	-656(%rbp), %r8
	movq	%rdx, -672(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -664(%rbp)
	testq	%rdi, %rdi
	jne	.L2174
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1748:
	leaq	.LC22(%rip), %rsi
	leaq	-336(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-328(%rbp), %rax
	movq	-336(%rbp), %rsi
	leaq	-896(%rbp), %r9
	movq	%r9, -912(%rbp)
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L1946
	testq	%rsi, %rsi
	je	.L1721
.L1946:
	movq	%rdx, %rcx
	movq	%r9, %rdi
	sarq	%rcx
	cmpq	$14, %rdx
	ja	.L2222
.L1804:
	cmpq	$2, %rdx
	je	.L2223
	testq	%rdx, %rdx
	je	.L1806
	movq	%r9, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	%rdx, -1104(%rbp)
	call	memmove@PLT
	movq	-912(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %rcx
	movq	-1104(%rbp), %rdx
.L1806:
	xorl	%r10d, %r10d
	movq	%rcx, -904(%rbp)
	movl	$7, %eax
	movw	%r10w, (%rdi,%rdx)
	movq	-912(%rbp), %rdi
	movq	-712(%rbp), %r8
	movq	-904(%rbp), %rsi
	cmpq	%r9, %rdi
	cmovne	-896(%rbp), %rax
	movq	-720(%rbp), %rcx
	leaq	(%r8,%rsi), %r10
	cmpq	%rax, %r10
	ja	.L1808
	leaq	-912(%rbp), %r11
	testq	%r8, %r8
	je	.L1809
	leaq	(%rdi,%rsi,2), %rax
	cmpq	$1, %r8
	je	.L2224
	addq	%r8, %r8
	je	.L1809
	movq	%rax, %rdi
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -1128(%rbp)
	movq	%r11, -1112(%rbp)
	movq	%r10, -1104(%rbp)
	call	memmove@PLT
	movq	-912(%rbp), %rdi
	movq	-1104(%rbp), %r10
	movq	-1112(%rbp), %r11
	movq	-1128(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1809:
	movq	%r9, -1104(%rbp)
	xorl	%r9d, %r9d
	movq	%r11, %rsi
	movq	%r10, -904(%rbp)
	movw	%r9w, (%rdi,%r10,2)
	leaq	-96(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-912(%rbp), %rdi
	movq	-1104(%rbp), %r9
	cmpq	%r9, %rdi
	je	.L1811
	call	_ZdlPv@PLT
.L1811:
	movq	-336(%rbp), %rdi
	leaq	-320(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1812
	call	_ZdlPv@PLT
.L1812:
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	-672(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rcx, %rdx
	je	.L2198
	movq	-80(%rbp), %rsi
	cmpq	-1096(%rbp), %rdi
	je	.L2181
	movq	%rax, %xmm0
	movq	%rsi, %xmm6
	movq	-656(%rbp), %r8
	movq	%rdx, -672(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -664(%rbp)
	testq	%rdi, %rdi
	jne	.L2174
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1749:
	leaq	.LC21(%rip), %rsi
	leaq	-384(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-376(%rbp), %rax
	movq	-384(%rbp), %rsi
	leaq	-928(%rbp), %r9
	movq	%r9, -944(%rbp)
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L1945
	testq	%rsi, %rsi
	je	.L1721
.L1945:
	movq	%rdx, %rcx
	movq	%r9, %rdi
	sarq	%rcx
	cmpq	$14, %rdx
	ja	.L2225
.L1787:
	cmpq	$2, %rdx
	je	.L2226
	testq	%rdx, %rdx
	je	.L1789
	movq	%r9, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	%rdx, -1104(%rbp)
	call	memmove@PLT
	movq	-944(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %rcx
	movq	-1104(%rbp), %rdx
.L1789:
	xorl	%eax, %eax
	movq	%rcx, -936(%rbp)
	movw	%ax, (%rdi,%rdx)
	movq	-944(%rbp), %rdi
	movl	$7, %eax
	movq	-712(%rbp), %r8
	movq	-936(%rbp), %rsi
	cmpq	%r9, %rdi
	cmovne	-928(%rbp), %rax
	movq	-720(%rbp), %rcx
	leaq	(%r8,%rsi), %r10
	cmpq	%rax, %r10
	ja	.L1791
	leaq	-944(%rbp), %r11
	testq	%r8, %r8
	je	.L1792
	leaq	(%rdi,%rsi,2), %rax
	cmpq	$1, %r8
	je	.L2227
	addq	%r8, %r8
	je	.L1792
	movq	%rax, %rdi
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -1128(%rbp)
	movq	%r11, -1112(%rbp)
	movq	%r10, -1104(%rbp)
	call	memmove@PLT
	movq	-944(%rbp), %rdi
	movq	-1104(%rbp), %r10
	movq	-1112(%rbp), %r11
	movq	-1128(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1792:
	xorl	%eax, %eax
	movq	%r10, -936(%rbp)
	movq	%r11, %rsi
	movw	%ax, (%rdi,%r10,2)
	leaq	-96(%rbp), %rdi
	movq	%r9, -1104(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-944(%rbp), %rdi
	movq	-1104(%rbp), %r9
	cmpq	%r9, %rdi
	je	.L1794
	call	_ZdlPv@PLT
.L1794:
	movq	-384(%rbp), %rdi
	leaq	-368(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1795
	call	_ZdlPv@PLT
.L1795:
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	-672(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rcx, %rdx
	je	.L2198
	movq	-80(%rbp), %rsi
	cmpq	-1096(%rbp), %rdi
	jne	.L1885
.L2180:
	movq	%rax, %xmm0
	movq	%rsi, %xmm7
	movq	%rdx, -672(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -664(%rbp)
.L1886:
	movq	%rcx, -96(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1751:
	leaq	.LC19(%rip), %rsi
	leaq	-480(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-472(%rbp), %rax
	movq	-480(%rbp), %rsi
	leaq	-992(%rbp), %r9
	movq	%r9, -1008(%rbp)
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L1943
	testq	%rsi, %rsi
	je	.L1721
.L1943:
	movq	%rdx, %rcx
	movq	%r9, %rdi
	sarq	%rcx
	cmpq	$14, %rdx
	ja	.L2228
.L1753:
	cmpq	$2, %rdx
	je	.L2229
	testq	%rdx, %rdx
	je	.L1755
	movq	%r9, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	%rdx, -1104(%rbp)
	call	memmove@PLT
	movq	-1008(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %rcx
	movq	-1104(%rbp), %rdx
.L1755:
	movq	%rcx, -1000(%rbp)
	xorl	%ecx, %ecx
	movl	$7, %eax
	movw	%cx, (%rdi,%rdx)
	movq	-1008(%rbp), %rdi
	movq	-712(%rbp), %r8
	movq	-1000(%rbp), %rsi
	cmpq	%r9, %rdi
	cmovne	-992(%rbp), %rax
	movq	-720(%rbp), %rcx
	leaq	(%r8,%rsi), %r10
	cmpq	%rax, %r10
	ja	.L1757
	leaq	-1008(%rbp), %r11
	testq	%r8, %r8
	je	.L1758
	leaq	(%rdi,%rsi,2), %rax
	cmpq	$1, %r8
	je	.L2230
	addq	%r8, %r8
	je	.L1758
	movq	%rax, %rdi
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -1128(%rbp)
	movq	%r11, -1112(%rbp)
	movq	%r10, -1104(%rbp)
	call	memmove@PLT
	movq	-1008(%rbp), %rdi
	movq	-1104(%rbp), %r10
	movq	-1112(%rbp), %r11
	movq	-1128(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1758:
	xorl	%eax, %eax
	movq	%r10, -1000(%rbp)
	movq	%r11, %rsi
	movw	%ax, (%rdi,%r10,2)
	leaq	-96(%rbp), %rdi
	movq	%r9, -1104(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-1008(%rbp), %rdi
	movq	-1104(%rbp), %r9
	cmpq	%r9, %rdi
	je	.L1760
	call	_ZdlPv@PLT
.L1760:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1761
	call	_ZdlPv@PLT
.L1761:
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	-672(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rcx, %rdx
	je	.L2198
	movq	-80(%rbp), %rsi
	cmpq	-1096(%rbp), %rdi
	jne	.L1851
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L1750:
	leaq	.LC20(%rip), %rsi
	leaq	-432(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-424(%rbp), %rax
	movq	-432(%rbp), %rsi
	leaq	-960(%rbp), %r9
	movq	%r9, -976(%rbp)
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L1944
	testq	%rsi, %rsi
	je	.L1721
.L1944:
	movq	%rdx, %rcx
	movq	%r9, %rdi
	sarq	%rcx
	cmpq	$14, %rdx
	ja	.L2231
.L1770:
	cmpq	$2, %rdx
	je	.L2232
	testq	%rdx, %rdx
	je	.L1772
	movq	%r9, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	%rdx, -1104(%rbp)
	call	memmove@PLT
	movq	-976(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %rcx
	movq	-1104(%rbp), %rdx
.L1772:
	xorl	%eax, %eax
	movq	%rcx, -968(%rbp)
	movw	%ax, (%rdi,%rdx)
	movq	-976(%rbp), %rdi
	movl	$7, %eax
	movq	-712(%rbp), %r8
	movq	-968(%rbp), %rsi
	cmpq	%r9, %rdi
	cmovne	-960(%rbp), %rax
	movq	-720(%rbp), %rcx
	leaq	(%r8,%rsi), %r10
	cmpq	%rax, %r10
	ja	.L1774
	leaq	-976(%rbp), %r11
	testq	%r8, %r8
	je	.L1775
	leaq	(%rdi,%rsi,2), %rax
	cmpq	$1, %r8
	je	.L2233
	addq	%r8, %r8
	je	.L1775
	movq	%rax, %rdi
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -1128(%rbp)
	movq	%r11, -1112(%rbp)
	movq	%r10, -1104(%rbp)
	call	memmove@PLT
	movq	-976(%rbp), %rdi
	movq	-1104(%rbp), %r10
	movq	-1112(%rbp), %r11
	movq	-1128(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1775:
	xorl	%eax, %eax
	movq	%r10, -968(%rbp)
	movq	%r11, %rsi
	movw	%ax, (%rdi,%r10,2)
	leaq	-96(%rbp), %rdi
	movq	%r9, -1104(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-976(%rbp), %rdi
	movq	-1104(%rbp), %r9
	cmpq	%r9, %rdi
	je	.L1777
	call	_ZdlPv@PLT
.L1777:
	movq	-432(%rbp), %rdi
	leaq	-416(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1778
	call	_ZdlPv@PLT
.L1778:
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	-672(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rcx, %rdx
	je	.L2198
	movq	-80(%rbp), %rsi
	cmpq	-1096(%rbp), %rdi
	jne	.L1868
.L2181:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%rdx, -672(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -664(%rbp)
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1742:
	leaq	.LC27(%rip), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	-736(%rbp), %r9
	movq	%r9, -752(%rbp)
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L1951
	testq	%rsi, %rsi
	jne	.L1951
.L1721:
	leaq	.LC18(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L1951:
	movq	%rdx, %rcx
	movq	%r9, %rdi
	sarq	%rcx
	cmpq	$14, %rdx
	ja	.L2234
.L1889:
	cmpq	$2, %rdx
	je	.L2235
	testq	%rdx, %rdx
	je	.L1891
	movq	%r9, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	%rdx, -1104(%rbp)
	call	memmove@PLT
	movq	-752(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %rcx
	movq	-1104(%rbp), %rdx
.L1891:
	xorl	%r10d, %r10d
	movq	%rcx, -744(%rbp)
	movl	$7, %eax
	movw	%r10w, (%rdi,%rdx)
	movq	-752(%rbp), %rdi
	movq	-712(%rbp), %r8
	movq	-744(%rbp), %rsi
	cmpq	%r9, %rdi
	cmovne	-736(%rbp), %rax
	movq	-720(%rbp), %rcx
	leaq	(%r8,%rsi), %r10
	cmpq	%rax, %r10
	ja	.L1893
	leaq	-752(%rbp), %r11
	testq	%r8, %r8
	je	.L1894
	leaq	(%rdi,%rsi,2), %rax
	cmpq	$1, %r8
	je	.L2236
	addq	%r8, %r8
	je	.L1894
	movq	%rax, %rdi
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -1128(%rbp)
	movq	%r11, -1112(%rbp)
	movq	%r10, -1104(%rbp)
	call	memmove@PLT
	movq	-752(%rbp), %rdi
	movq	-1104(%rbp), %r10
	movq	-1112(%rbp), %r11
	movq	-1128(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1894:
	xorl	%r8d, %r8d
	movq	%r10, -744(%rbp)
	movq	%r11, %rsi
	movw	%r8w, (%rdi,%r10,2)
	leaq	-144(%rbp), %rdi
	movq	%r9, -1104(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-752(%rbp), %rdi
	movq	-1104(%rbp), %r9
	cmpq	%r9, %rdi
	je	.L1896
	call	_ZdlPv@PLT
.L1896:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1897
	call	_ZdlPv@PLT
.L1897:
	movq	-144(%rbp), %rdx
	leaq	-128(%rbp), %rcx
	movq	-672(%rbp), %rdi
	movq	-136(%rbp), %rax
	cmpq	%rcx, %rdx
	je	.L2237
	movq	-128(%rbp), %rsi
	cmpq	-1096(%rbp), %rdi
	je	.L2238
	movq	%rax, %xmm0
	movq	%rsi, %xmm6
	movq	-656(%rbp), %r8
	movq	%rdx, -672(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -664(%rbp)
	testq	%rdi, %rdi
	je	.L1903
	movq	%rdi, -144(%rbp)
	movq	%r8, -128(%rbp)
.L1901:
	movq	$0, -136(%rbp)
	xorl	%eax, %eax
	movw	%ax, (%rdi)
	movq	-112(%rbp), %rax
	movq	-144(%rbp), %rdi
	movq	%rax, -640(%rbp)
	cmpq	%rcx, %rdi
	jne	.L2166
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L2206:
	leaq	-576(%rbp), %rdi
	leaq	.LC16(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	.LC17(%rip), %rsi
	leaq	-528(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-520(%rbp), %rax
	movq	-528(%rbp), %rsi
	leaq	-1024(%rbp), %r9
	movq	%r9, -1040(%rbp)
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L1710
	testq	%rsi, %rsi
	je	.L1721
.L1710:
	movq	%rdx, %rcx
	movq	%r9, %rdi
	sarq	%rcx
	cmpq	$14, %rdx
	ja	.L2239
.L1711:
	cmpq	$2, %rdx
	je	.L2240
	testq	%rdx, %rdx
	je	.L1714
	movq	%r9, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	%rdx, -1104(%rbp)
	call	memmove@PLT
	movq	-1040(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %rcx
	movq	-1104(%rbp), %rdx
.L1714:
	xorl	%eax, %eax
	movq	%rcx, -1032(%rbp)
	movw	%ax, (%rdi,%rdx)
	movq	-1040(%rbp), %rdi
	movl	$7, %eax
	movq	-712(%rbp), %r8
	movq	-1032(%rbp), %rsi
	cmpq	%r9, %rdi
	cmovne	-1024(%rbp), %rax
	movq	-720(%rbp), %rcx
	leaq	(%r8,%rsi), %r10
	cmpq	%rax, %r10
	ja	.L1716
	leaq	-1040(%rbp), %r11
	testq	%r8, %r8
	je	.L1717
	leaq	(%rdi,%rsi,2), %rax
	cmpq	$1, %r8
	je	.L2241
	addq	%r8, %r8
	je	.L1717
	movq	%rax, %rdi
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r11, -1128(%rbp)
	movq	%r9, -1112(%rbp)
	movq	%r10, -1104(%rbp)
	call	memmove@PLT
	movq	-1040(%rbp), %rdi
	movq	-1104(%rbp), %r10
	movq	-1112(%rbp), %r9
	movq	-1128(%rbp), %r11
	.p2align 4,,10
	.p2align 3
.L1717:
	xorl	%eax, %eax
	movq	%r10, -1032(%rbp)
	movq	%r11, %rsi
	movw	%ax, (%rdi,%r10,2)
	leaq	-624(%rbp), %rdi
	movq	%r9, -1104(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-1040(%rbp), %rdi
	movq	-1104(%rbp), %r9
	cmpq	%r9, %rdi
	je	.L1719
	call	_ZdlPv@PLT
.L1719:
	movq	-528(%rbp), %rdi
	leaq	-512(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1720
	call	_ZdlPv@PLT
.L1720:
	movq	-616(%rbp), %rax
	movq	-624(%rbp), %rsi
	leaq	-1056(%rbp), %r9
	movq	%r9, -1072(%rbp)
	leaq	(%rax,%rax), %rdx
	movq	%rsi, %rax
	addq	%rdx, %rax
	je	.L1942
	testq	%rsi, %rsi
	je	.L1721
.L1942:
	movq	%rdx, %rcx
	movq	%r9, %rdi
	sarq	%rcx
	cmpq	$14, %rdx
	ja	.L2242
.L1723:
	cmpq	$2, %rdx
	je	.L2243
	testq	%rdx, %rdx
	je	.L1726
	movq	%r9, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	%rdx, -1104(%rbp)
	call	memmove@PLT
	movq	-1072(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %rcx
	movq	-1104(%rbp), %rdx
.L1726:
	xorl	%r10d, %r10d
	movq	%rcx, -1064(%rbp)
	movl	$7, %eax
	movw	%r10w, (%rdi,%rdx)
	movq	-1072(%rbp), %rdi
	movq	-568(%rbp), %r8
	movq	-1064(%rbp), %rsi
	cmpq	%r9, %rdi
	cmovne	-1056(%rbp), %rax
	movq	-576(%rbp), %rcx
	leaq	(%r8,%rsi), %r10
	cmpq	%rax, %r10
	ja	.L1728
	leaq	-1072(%rbp), %r11
	testq	%r8, %r8
	je	.L1729
	leaq	(%rdi,%rsi,2), %rax
	cmpq	$1, %r8
	je	.L2244
	addq	%r8, %r8
	je	.L1729
	movq	%rax, %rdi
	movq	%r8, %rdx
	movq	%rcx, %rsi
	movq	%r9, -1128(%rbp)
	movq	%r11, -1112(%rbp)
	movq	%r10, -1104(%rbp)
	call	memmove@PLT
	movq	-1072(%rbp), %rdi
	movq	-1104(%rbp), %r10
	movq	-1112(%rbp), %r11
	movq	-1128(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1729:
	movq	%r9, -1104(%rbp)
	xorl	%r9d, %r9d
	movq	%r11, %rsi
	movq	%r10, -1064(%rbp)
	movw	%r9w, (%rdi,%r10,2)
	leaq	-96(%rbp), %rdi
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-1072(%rbp), %rdi
	movq	-1104(%rbp), %r9
	cmpq	%r9, %rdi
	je	.L1731
	call	_ZdlPv@PLT
.L1731:
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	-720(%rbp), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rcx, %rdx
	je	.L2245
	leaq	-704(%rbp), %rsi
	movq	-80(%rbp), %r8
	cmpq	%rsi, %rdi
	je	.L2246
	movq	%rax, %xmm0
	movq	%r8, %xmm1
	movq	-704(%rbp), %rsi
	movq	%rdx, -720(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -712(%rbp)
	testq	%rdi, %rdi
	je	.L1737
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L1735:
	xorl	%esi, %esi
	movq	$0, -88(%rbp)
	movw	%si, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -688(%rbp)
	cmpq	%rcx, %rdi
	je	.L1738
	call	_ZdlPv@PLT
.L1738:
	movq	-624(%rbp), %rdi
	leaq	-608(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1739
	call	_ZdlPv@PLT
.L1739:
	movq	-576(%rbp), %rdi
	leaq	-560(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1709
	call	_ZdlPv@PLT
	jmp	.L1709
	.p2align 4,,10
	.p2align 3
.L2198:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L1882
	cmpq	$1, %rax
	je	.L2247
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L1882
	movq	%rcx, %rsi
	movq	%rcx, -1104(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	-672(%rbp), %rdi
	movq	-1104(%rbp), %rcx
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L1882:
	movq	%rax, -664(%rbp)
	xorl	%eax, %eax
	movw	%ax, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L2243:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-1072(%rbp), %rdi
	jmp	.L1726
	.p2align 4,,10
	.p2align 3
.L2240:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-1040(%rbp), %rdi
	jmp	.L1714
	.p2align 4,,10
	.p2align 3
.L1716:
	leaq	-1040(%rbp), %r11
	xorl	%edx, %edx
	movq	%r9, -1128(%rbp)
	movq	%r11, %rdi
	movq	%r10, -1112(%rbp)
	movq	%r11, -1104(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-1040(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %r10
	movq	-1104(%rbp), %r11
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L2239:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rcx
	ja	.L1724
	leaq	2(%rdx), %rdi
	movq	%r9, -1136(%rbp)
	movq	%rcx, -1128(%rbp)
	movq	%rsi, -1112(%rbp)
	movq	%rdx, -1104(%rbp)
	call	_Znwm@PLT
	movq	-1128(%rbp), %rcx
	movq	-1136(%rbp), %r9
	movq	%rax, -1040(%rbp)
	movq	-1112(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rcx, -1024(%rbp)
	movq	-1104(%rbp), %rdx
	jmp	.L1711
	.p2align 4,,10
	.p2align 3
.L2242:
	movabsq	$2305843009213693951, %rax
	movq	%r9, -1136(%rbp)
	cmpq	%rax, %rcx
	movq	%rsi, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	ja	.L1724
	leaq	2(%rdx), %rdi
	movq	%rdx, -1104(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rcx
	movq	-1136(%rbp), %r9
	movq	%rax, -1072(%rbp)
	movq	-1128(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rcx, -1056(%rbp)
	movq	-1104(%rbp), %rdx
	jmp	.L1723
	.p2align 4,,10
	.p2align 3
.L1728:
	leaq	-1072(%rbp), %r11
	xorl	%edx, %edx
	movq	%r9, -1128(%rbp)
	movq	%r11, %rdi
	movq	%r10, -1112(%rbp)
	movq	%r11, -1104(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-1072(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %r10
	movq	-1104(%rbp), %r11
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L2245:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L1733
	cmpq	$1, %rax
	je	.L2248
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L1733
	movq	%rcx, %rsi
	movq	%rcx, -1104(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	-720(%rbp), %rdi
	movq	-1104(%rbp), %rcx
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L1733:
	xorl	%r8d, %r8d
	movq	%rax, -712(%rbp)
	movw	%r8w, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L2246:
	movq	%rax, %xmm0
	movq	%r8, %xmm2
	movq	%rdx, -720(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -712(%rbp)
.L1737:
	movq	%rcx, -96(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L2221:
	movq	%rax, %xmm0
	movq	%rsi, %xmm3
	movq	%rdx, -672(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -664(%rbp)
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L2214:
	movq	%rax, %xmm0
	movq	%rsi, %xmm4
	movq	%rdx, -672(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -664(%rbp)
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L2210:
	movq	%rax, %xmm0
	movq	%rsi, %xmm5
	movq	%rdx, -672(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, -664(%rbp)
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L2238:
	movq	%rax, %xmm0
	movq	%rsi, %xmm6
	movq	%rdx, -672(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -664(%rbp)
.L1903:
	movq	%rcx, -144(%rbp)
	leaq	-128(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L2247:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	-672(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L1882
	.p2align 4,,10
	.p2align 3
.L2244:
	movzwl	(%rcx), %edx
	movw	%dx, (%rax)
	movq	-1072(%rbp), %rdi
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L2241:
	movzwl	(%rcx), %edx
	movw	%dx, (%rax)
	movq	-1040(%rbp), %rdi
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L2212:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-816(%rbp), %rdi
	jmp	.L1857
	.p2align 4,,10
	.p2align 3
.L2216:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-880(%rbp), %rdi
	jmp	.L1823
	.p2align 4,,10
	.p2align 3
.L2219:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-848(%rbp), %rdi
	jmp	.L1840
	.p2align 4,,10
	.p2align 3
.L2208:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-784(%rbp), %rdi
	jmp	.L1874
	.p2align 4,,10
	.p2align 3
.L2232:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-976(%rbp), %rdi
	jmp	.L1772
	.p2align 4,,10
	.p2align 3
.L2229:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-1008(%rbp), %rdi
	jmp	.L1755
	.p2align 4,,10
	.p2align 3
.L2226:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-944(%rbp), %rdi
	jmp	.L1789
	.p2align 4,,10
	.p2align 3
.L2223:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-912(%rbp), %rdi
	jmp	.L1806
	.p2align 4,,10
	.p2align 3
.L2235:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	movq	-752(%rbp), %rdi
	jmp	.L1891
	.p2align 4,,10
	.p2align 3
.L2211:
	movabsq	$2305843009213693951, %rax
	movq	%r9, -1136(%rbp)
	cmpq	%rax, %rcx
	movq	%rsi, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	ja	.L1724
	leaq	2(%rdx), %rdi
	movq	%rdx, -1104(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rcx
	movq	-1136(%rbp), %r9
	movq	%rax, -816(%rbp)
	movq	-1128(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rcx, -800(%rbp)
	movq	-1104(%rbp), %rdx
	jmp	.L1855
	.p2align 4,,10
	.p2align 3
.L1774:
	leaq	-976(%rbp), %r11
	xorl	%edx, %edx
	movq	%r9, -1128(%rbp)
	movq	%r11, %rdi
	movq	%r10, -1112(%rbp)
	movq	%r11, -1104(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-976(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %r10
	movq	-1104(%rbp), %r11
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L1876:
	leaq	-784(%rbp), %r11
	xorl	%edx, %edx
	movq	%r9, -1128(%rbp)
	movq	%r11, %rdi
	movq	%r10, -1112(%rbp)
	movq	%r11, -1104(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-784(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %r10
	movq	-1104(%rbp), %r11
	jmp	.L1877
	.p2align 4,,10
	.p2align 3
.L1859:
	leaq	-816(%rbp), %r11
	xorl	%edx, %edx
	movq	%r9, -1128(%rbp)
	movq	%r11, %rdi
	movq	%r10, -1112(%rbp)
	movq	%r11, -1104(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-816(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %r10
	movq	-1104(%rbp), %r11
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1842:
	leaq	-848(%rbp), %r11
	xorl	%edx, %edx
	movq	%r9, -1128(%rbp)
	movq	%r11, %rdi
	movq	%r10, -1112(%rbp)
	movq	%r11, -1104(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-848(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %r10
	movq	-1104(%rbp), %r11
	jmp	.L1843
	.p2align 4,,10
	.p2align 3
.L1825:
	leaq	-880(%rbp), %r11
	xorl	%edx, %edx
	movq	%r9, -1128(%rbp)
	movq	%r11, %rdi
	movq	%r10, -1112(%rbp)
	movq	%r11, -1104(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-880(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %r10
	movq	-1104(%rbp), %r11
	jmp	.L1826
	.p2align 4,,10
	.p2align 3
.L1757:
	leaq	-1008(%rbp), %r11
	xorl	%edx, %edx
	movq	%r9, -1128(%rbp)
	movq	%r11, %rdi
	movq	%r10, -1112(%rbp)
	movq	%r11, -1104(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-1008(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %r10
	movq	-1104(%rbp), %r11
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L1893:
	leaq	-752(%rbp), %r11
	xorl	%edx, %edx
	movq	%r9, -1128(%rbp)
	movq	%r11, %rdi
	movq	%r10, -1112(%rbp)
	movq	%r11, -1104(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-752(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %r10
	movq	-1104(%rbp), %r11
	jmp	.L1894
	.p2align 4,,10
	.p2align 3
.L2225:
	movabsq	$2305843009213693951, %rax
	movq	%r9, -1136(%rbp)
	cmpq	%rax, %rcx
	movq	%rsi, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	ja	.L1724
	leaq	2(%rdx), %rdi
	movq	%rdx, -1104(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rcx
	movq	-1136(%rbp), %r9
	movq	%rax, -944(%rbp)
	movq	-1128(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rcx, -928(%rbp)
	movq	-1104(%rbp), %rdx
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L2222:
	movabsq	$2305843009213693951, %rax
	movq	%r9, -1136(%rbp)
	cmpq	%rax, %rcx
	movq	%rsi, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	ja	.L1724
	leaq	2(%rdx), %rdi
	movq	%rdx, -1104(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rcx
	movq	-1136(%rbp), %r9
	movq	%rax, -912(%rbp)
	movq	-1128(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rcx, -896(%rbp)
	movq	-1104(%rbp), %rdx
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L2231:
	movabsq	$2305843009213693951, %rax
	movq	%r9, -1136(%rbp)
	cmpq	%rax, %rcx
	movq	%rsi, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	ja	.L1724
	leaq	2(%rdx), %rdi
	movq	%rdx, -1104(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rcx
	movq	-1136(%rbp), %r9
	movq	%rax, -976(%rbp)
	movq	-1128(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rcx, -960(%rbp)
	movq	-1104(%rbp), %rdx
	jmp	.L1770
	.p2align 4,,10
	.p2align 3
.L2228:
	movabsq	$2305843009213693951, %rax
	movq	%r9, -1136(%rbp)
	cmpq	%rax, %rcx
	movq	%rsi, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	ja	.L1724
	leaq	2(%rdx), %rdi
	movq	%rdx, -1104(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rcx
	movq	-1136(%rbp), %r9
	movq	%rax, -1008(%rbp)
	movq	-1128(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rcx, -992(%rbp)
	movq	-1104(%rbp), %rdx
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L2218:
	movabsq	$2305843009213693951, %rax
	movq	%r9, -1136(%rbp)
	cmpq	%rax, %rcx
	movq	%rsi, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	ja	.L1724
	leaq	2(%rdx), %rdi
	movq	%rdx, -1104(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rcx
	movq	-1136(%rbp), %r9
	movq	%rax, -848(%rbp)
	movq	-1128(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rcx, -832(%rbp)
	movq	-1104(%rbp), %rdx
	jmp	.L1838
	.p2align 4,,10
	.p2align 3
.L2207:
	movabsq	$2305843009213693951, %rax
	movq	%r9, -1136(%rbp)
	cmpq	%rax, %rcx
	movq	%rsi, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	ja	.L1724
	leaq	2(%rdx), %rdi
	movq	%rdx, -1104(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rcx
	movq	-1136(%rbp), %r9
	movq	%rax, -784(%rbp)
	movq	-1128(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rcx, -768(%rbp)
	movq	-1104(%rbp), %rdx
	jmp	.L1872
	.p2align 4,,10
	.p2align 3
.L2215:
	movabsq	$2305843009213693951, %rax
	movq	%r9, -1136(%rbp)
	cmpq	%rax, %rcx
	movq	%rsi, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	ja	.L1724
	leaq	2(%rdx), %rdi
	movq	%rdx, -1104(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rcx
	movq	-1136(%rbp), %r9
	movq	%rax, -880(%rbp)
	movq	-1128(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rcx, -864(%rbp)
	movq	-1104(%rbp), %rdx
	jmp	.L1821
	.p2align 4,,10
	.p2align 3
.L1808:
	leaq	-912(%rbp), %r11
	xorl	%edx, %edx
	movq	%r9, -1128(%rbp)
	movq	%r11, %rdi
	movq	%r10, -1112(%rbp)
	movq	%r11, -1104(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-912(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %r10
	movq	-1104(%rbp), %r11
	jmp	.L1809
	.p2align 4,,10
	.p2align 3
.L1791:
	leaq	-944(%rbp), %r11
	xorl	%edx, %edx
	movq	%r9, -1128(%rbp)
	movq	%r11, %rdi
	movq	%r10, -1112(%rbp)
	movq	%r11, -1104(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-944(%rbp), %rdi
	movq	-1128(%rbp), %r9
	movq	-1112(%rbp), %r10
	movq	-1104(%rbp), %r11
	jmp	.L1792
	.p2align 4,,10
	.p2align 3
.L2234:
	movabsq	$2305843009213693951, %rax
	movq	%r9, -1136(%rbp)
	cmpq	%rax, %rcx
	movq	%rsi, -1128(%rbp)
	movq	%rcx, -1112(%rbp)
	ja	.L1724
	leaq	2(%rdx), %rdi
	movq	%rdx, -1104(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rcx
	movq	-1136(%rbp), %r9
	movq	%rax, -752(%rbp)
	movq	-1128(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rcx, -736(%rbp)
	movq	-1104(%rbp), %rdx
	jmp	.L1889
	.p2align 4,,10
	.p2align 3
.L2237:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L1899
	cmpq	$1, %rax
	je	.L2249
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L1899
	movq	%rcx, %rsi
	movq	%rcx, -1104(%rbp)
	call	memmove@PLT
	movq	-136(%rbp), %rax
	movq	-672(%rbp), %rdi
	movq	-1104(%rbp), %rcx
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L1899:
	xorl	%esi, %esi
	movq	%rax, -664(%rbp)
	movw	%si, (%rdi,%rdx)
	movq	-144(%rbp), %rdi
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L2248:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	-720(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L1733
.L2213:
	movzwl	(%rcx), %edx
	movw	%dx, (%rax)
	movq	-816(%rbp), %rdi
	jmp	.L1860
.L2220:
	movzwl	(%rcx), %edx
	movw	%dx, (%rax)
	movq	-848(%rbp), %rdi
	jmp	.L1843
.L2217:
	movzwl	(%rcx), %edx
	movw	%dx, (%rax)
	movq	-880(%rbp), %rdi
	jmp	.L1826
.L2209:
	movzwl	(%rcx), %edx
	movw	%dx, (%rax)
	movq	-784(%rbp), %rdi
	jmp	.L1877
.L2233:
	movzwl	(%rcx), %edx
	movw	%dx, (%rax)
	movq	-976(%rbp), %rdi
	jmp	.L1775
.L2236:
	movzwl	(%rcx), %edx
	movw	%dx, (%rax)
	movq	-752(%rbp), %rdi
	jmp	.L1894
.L2230:
	movzwl	(%rcx), %edx
	movw	%dx, (%rax)
	movq	-1008(%rbp), %rdi
	jmp	.L1758
.L2227:
	movzwl	(%rcx), %edx
	movw	%dx, (%rax)
	movq	-944(%rbp), %rdi
	jmp	.L1792
.L2224:
	movzwl	(%rcx), %edx
	movw	%dx, (%rax)
	movq	-912(%rbp), %rdi
	jmp	.L1809
.L2249:
	movzwl	-128(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-136(%rbp), %rax
	movq	-672(%rbp), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L1899
.L2205:
	call	__stack_chk_fail@PLT
.L1724:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7826:
	.size	_ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE, .-_ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE
	.section	.text._ZN12v8_inspector10V8Debugger14functionScopesEN2v85LocalINS1_7ContextEEENS2_INS1_8FunctionEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger14functionScopesEN2v85LocalINS1_7ContextEEENS2_INS1_8FunctionEEE
	.type	_ZN12v8_inspector10V8Debugger14functionScopesEN2v85LocalINS1_7ContextEEENS2_INS1_8FunctionEEE, @function
_ZN12v8_inspector10V8Debugger14functionScopesEN2v85LocalINS1_7ContextEEENS2_INS1_8FunctionEEE:
.LFB7854:
	.cfi_startproc
	endbr64
	xorl	%ecx, %ecx
	jmp	_ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE
	.cfi_endproc
.LFE7854:
	.size	_ZN12v8_inspector10V8Debugger14functionScopesEN2v85LocalINS1_7ContextEEENS2_INS1_8FunctionEEE, .-_ZN12v8_inspector10V8Debugger14functionScopesEN2v85LocalINS1_7ContextEEENS2_INS1_8FunctionEEE
	.section	.text._ZN12v8_inspector10V8Debugger15generatorScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger15generatorScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	.type	_ZN12v8_inspector10V8Debugger15generatorScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE, @function
_ZN12v8_inspector10V8Debugger15generatorScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE:
.LFB7855:
	.cfi_startproc
	endbr64
	movl	$1, %ecx
	jmp	_ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE
	.cfi_endproc
.LFE7855:
	.size	_ZN12v8_inspector10V8Debugger15generatorScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE, .-_ZN12v8_inspector10V8Debugger15generatorScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	.section	.rodata._ZN12v8_inspector10V8Debugger18internalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"[[Scopes]]"
.LC31:
	.string	"[[Entries]]"
	.section	.text._ZN12v8_inspector10V8Debugger18internalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger18internalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	.type	_ZN12v8_inspector10V8Debugger18internalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE, @function
_ZN12v8_inspector10V8Debugger18internalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE:
.LFB7857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdi
	call	_ZN2v85debug21GetInternalPropertiesEPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L2270
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector10V8Debugger18collectionsEntriesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2261
	movq	16(%r13), %rdi
	leaq	.LC31(%rip), %rsi
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNK2v85Array6LengthEv@PLT
	movq	-56(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE@PLT
.L2261:
	movq	%r12, %rdi
	call	_ZNK2v85Value17IsGeneratorObjectEv@PLT
	testb	%al, %al
	jne	.L2278
.L2255:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L2279
.L2257:
	movq	%rbx, %rax
.L2270:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2279:
	.cfi_restore_state
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2257
	movq	16(%r13), %rdi
	leaq	.LC30(%rip), %rsi
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_ZNK2v85Array6LengthEv@PLT
	movq	%r13, %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE@PLT
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2278:
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector10V8Debugger15getTargetScopesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEENS0_15ScopeTargetKindE
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2255
	movq	16(%r13), %rdi
	leaq	.LC30(%rip), %rsi
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	call	_ZNK2v85Array6LengthEv@PLT
	movq	-56(%rbp), %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE@PLT
	movq	%rbx, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%eax, %edx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE@PLT
	jmp	.L2255
	.cfi_endproc
.LFE7857:
	.size	_ZN12v8_inspector10V8Debugger18internalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE, .-_ZN12v8_inspector10V8Debugger18internalPropertiesEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEE
	.section	.text._ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS2_10_Hash_nodeIS0_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS2_10_Hash_nodeIS0_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS2_10_Hash_nodeIS0_Lb0EEEm
	.type	_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS2_10_Hash_nodeIS0_Lb0EEEm, @function
_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS2_10_Hash_nodeIS0_Lb0EEEm:
.LFB14055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L2281
	movq	(%rbx), %r8
.L2282:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L2291
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L2292:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2281:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L2305
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2306
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L2284:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L2286
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L2287
	.p2align 4,,10
	.p2align 3
.L2288:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2289:
	testq	%rsi, %rsi
	je	.L2286
.L2287:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L2288
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L2294
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L2287
	.p2align 4,,10
	.p2align 3
.L2286:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L2290
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L2290:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L2282
	.p2align 4,,10
	.p2align 3
.L2291:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L2293
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L2293:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L2292
	.p2align 4,,10
	.p2align 3
.L2294:
	movq	%rdx, %rdi
	jmp	.L2289
	.p2align 4,,10
	.p2align 3
.L2305:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L2284
.L2306:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE14055:
	.size	_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS2_10_Hash_nodeIS0_Lb0EEEm, .-_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS2_10_Hash_nodeIS0_Lb0EEEm
	.section	.text._ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS9_15_Hash_node_baseEPNS9_10_Hash_nodeIS7_Lb0EEE,"axG",@progbits,_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS9_15_Hash_node_baseEPNS9_10_Hash_nodeIS7_Lb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS9_15_Hash_node_baseEPNS9_10_Hash_nodeIS7_Lb0EEE
	.type	_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS9_15_Hash_node_baseEPNS9_10_Hash_nodeIS7_Lb0EEE, @function
_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS9_15_Hash_node_baseEPNS9_10_Hash_nodeIS7_Lb0EEE:
.LFB14059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	0(,%rsi,8), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$8, %rsp
	movq	(%rbx), %rcx
	movq	(%r12), %r13
	leaq	(%rcx,%r8), %rax
	cmpq	%rdx, (%rax)
	je	.L2322
	testq	%r13, %r13
	je	.L2310
	movq	8(%r13), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	cmpq	%rdx, %rsi
	je	.L2310
	movq	%rdi, (%rcx,%rdx,8)
	movq	(%r12), %r13
.L2310:
	movq	%r13, (%rdi)
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2313
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2314
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L2323
.L2313:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2314:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L2313
.L2323:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2313
	.p2align 4,,10
	.p2align 3
.L2322:
	testq	%r13, %r13
	je	.L2309
	movq	8(%r13), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	cmpq	%rdx, %rsi
	je	.L2310
	movq	%rdi, (%rcx,%rdx,8)
	movq	(%rbx), %rax
	addq	%r8, %rax
	movq	(%rax), %rdx
.L2309:
	leaq	16(%rbx), %rcx
	cmpq	%rcx, %rdx
	je	.L2324
.L2311:
	movq	$0, (%rax)
	movq	(%r12), %r13
	jmp	.L2310
	.p2align 4,,10
	.p2align 3
.L2324:
	movq	%r13, 16(%rbx)
	jmp	.L2311
	.cfi_endproc
.LFE14059:
	.size	_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS9_15_Hash_node_baseEPNS9_10_Hash_nodeIS7_Lb0EEE, .-_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS9_15_Hash_node_baseEPNS9_10_Hash_nodeIS7_Lb0EEE
	.section	.text._ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS2_15_Hash_node_baseEPNS2_10_Hash_nodeIS0_Lb0EEE,"axG",@progbits,_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS2_15_Hash_node_baseEPNS2_10_Hash_nodeIS0_Lb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS2_15_Hash_node_baseEPNS2_10_Hash_nodeIS0_Lb0EEE
	.type	_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS2_15_Hash_node_baseEPNS2_10_Hash_nodeIS0_Lb0EEE, @function
_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS2_15_Hash_node_baseEPNS2_10_Hash_nodeIS0_Lb0EEE:
.LFB14061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	0(,%rsi,8), %r9
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	movq	(%rbx), %rcx
	movq	(%rdi), %r12
	leaq	(%rcx,%r9), %rax
	cmpq	%rdx, (%rax)
	je	.L2335
	testq	%r12, %r12
	je	.L2328
	movq	8(%r12), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	cmpq	%rdx, %rsi
	je	.L2328
	movq	%r8, (%rcx,%rdx,8)
	movq	(%rdi), %r12
.L2328:
	movq	%r12, (%r8)
	call	_ZdlPv@PLT
	movq	%r12, %rax
	subq	$1, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2335:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L2327
	movq	8(%r12), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	cmpq	%rdx, %rsi
	je	.L2328
	movq	%r8, (%rcx,%rdx,8)
	movq	(%rbx), %rax
	addq	%r9, %rax
	movq	(%rax), %rdx
.L2327:
	leaq	16(%rbx), %rcx
	cmpq	%rcx, %rdx
	je	.L2336
.L2329:
	movq	$0, (%rax)
	movq	(%rdi), %r12
	jmp	.L2328
	.p2align 4,,10
	.p2align 3
.L2336:
	movq	%r12, 16(%rbx)
	jmp	.L2329
	.cfi_endproc
.LFE14061:
	.size	_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS2_15_Hash_node_baseEPNS2_10_Hash_nodeIS0_Lb0EEE, .-_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS2_15_Hash_node_baseEPNS2_10_Hash_nodeIS0_Lb0EEE
	.section	.text._ZN12v8_inspector10V8Debugger25asyncTaskCanceledForStackEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger25asyncTaskCanceledForStackEPv
	.type	_ZN12v8_inspector10V8Debugger25asyncTaskCanceledForStackEPv, @function
_ZN12v8_inspector10V8Debugger25asyncTaskCanceledForStackEPv:
.LFB7872:
	.cfi_startproc
	endbr64
	movl	236(%rdi), %eax
	testl	%eax, %eax
	jne	.L2363
	ret
	.p2align 4,,10
	.p2align 3
.L2363:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	128(%rdi), %rdi
	divq	%rdi
	movq	120(%rbx), %rax
	movq	(%rax,%rdx,8), %r9
	movq	%rdx, %r10
	testq	%r9, %r9
	je	.L2339
	movq	(%r9), %r8
	movq	8(%r8), %rcx
	jmp	.L2341
	.p2align 4,,10
	.p2align 3
.L2364:
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	je	.L2339
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r8, %r9
	movq	%rcx, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L2339
	movq	%rsi, %r8
.L2341:
	cmpq	%rcx, %r12
	jne	.L2364
	leaq	120(%rbx), %rdi
	movq	%r8, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS9_15_Hash_node_baseEPNS9_10_Hash_nodeIS7_Lb0EEE
.L2339:
	movq	184(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	176(%rbx), %rax
	movq	(%rax,%rdx,8), %r9
	movq	%rdx, %r10
	testq	%r9, %r9
	je	.L2337
	movq	(%r9), %r8
	movq	8(%r8), %rcx
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2365:
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	je	.L2337
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r8, %r9
	movq	%rcx, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L2337
	movq	%rsi, %r8
.L2344:
	cmpq	%r12, %rcx
	jne	.L2365
	leaq	176(%rbx), %rdi
	movq	%r8, %rcx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r9, %rdx
	popq	%r12
	.cfi_restore 12
	movq	%r10, %rsi
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS2_15_Hash_node_baseEPNS2_10_Hash_nodeIS0_Lb0EEE
	.p2align 4,,10
	.p2align 3
.L2337:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7872:
	.size	_ZN12v8_inspector10V8Debugger25asyncTaskCanceledForStackEPv, .-_ZN12v8_inspector10V8Debugger25asyncTaskCanceledForStackEPv
	.section	.text._ZN12v8_inspector10V8Debugger17asyncTaskCanceledEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger17asyncTaskCanceledEPv
	.type	_ZN12v8_inspector10V8Debugger17asyncTaskCanceledEPv, @function
_ZN12v8_inspector10V8Debugger17asyncTaskCanceledEPv:
.LFB7868:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN12v8_inspector10V8Debugger25asyncTaskCanceledForStackEPv
	cmpq	$0, 472(%rbx)
	jne	.L2366
	cmpq	456(%rbx), %r12
	je	.L2369
.L2366:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2369:
	.cfi_restore_state
	movq	$0, 456(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7868:
	.size	_ZN12v8_inspector10V8Debugger17asyncTaskCanceledEPv, .-_ZN12v8_inspector10V8Debugger17asyncTaskCanceledEPv
	.section	.text._ZN12v8_inspector10V8Debugger25asyncTaskFinishedForStackEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger25asyncTaskFinishedForStackEPv
	.type	_ZN12v8_inspector10V8Debugger25asyncTaskFinishedForStackEPv, @function
_ZN12v8_inspector10V8Debugger25asyncTaskFinishedForStackEPv:
.LFB7874:
	.cfi_startproc
	endbr64
	movl	236(%rdi), %eax
	testl	%eax, %eax
	je	.L2391
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	248(%rdi), %rax
	cmpq	%rax, 240(%rdi)
	je	.L2370
	subq	$8, %rax
	movq	%rsi, %r13
	movq	%rax, 248(%rdi)
	movq	272(%rdi), %rax
	leaq	-16(%rax), %rdx
	movq	%rdx, 272(%rdi)
	movq	-8(%rax), %r14
	testq	%r14, %r14
	je	.L2375
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2376
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L2377:
	cmpl	$1, %eax
	jne	.L2375
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L2379
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L2380:
	cmpl	$1, %eax
	je	.L2394
	.p2align 4,,10
	.p2align 3
.L2375:
	movq	184(%r12), %rdi
	movq	%r13, %rax
	xorl	%edx, %edx
	subq	$24, 296(%r12)
	divq	%rdi
	movq	176(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L2381
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2382
	.p2align 4,,10
	.p2align 3
.L2395:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2381
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L2381
.L2382:
	cmpq	%r13, %rsi
	jne	.L2395
.L2370:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2391:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L2376:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L2377
	.p2align 4,,10
	.p2align 3
.L2381:
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector10V8Debugger25asyncTaskCanceledForStackEPv
	.p2align 4,,10
	.p2align 3
.L2394:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L2375
	.p2align 4,,10
	.p2align 3
.L2379:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L2380
	.cfi_endproc
.LFE7874:
	.size	_ZN12v8_inspector10V8Debugger25asyncTaskFinishedForStackEPv, .-_ZN12v8_inspector10V8Debugger25asyncTaskFinishedForStackEPv
	.section	.text._ZN12v8_inspector10V8Debugger17asyncTaskFinishedEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger17asyncTaskFinishedEPv
	.type	_ZN12v8_inspector10V8Debugger17asyncTaskFinishedEPv, @function
_ZN12v8_inspector10V8Debugger17asyncTaskFinishedEPv:
.LFB7870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	cmpq	$0, 472(%rdi)
	jne	.L2398
	cmpq	456(%rdi), %rsi
	je	.L2401
.L2398:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector10V8Debugger25asyncTaskFinishedForStackEPv
	.p2align 4,,10
	.p2align 3
.L2401:
	.cfi_restore_state
	cmpb	$0, 504(%rdi)
	movq	$0, 456(%rdi)
	jne	.L2398
	movq	16(%rdi), %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v85debug28ClearBreakOnNextFunctionCallEPNS_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector10V8Debugger25asyncTaskFinishedForStackEPv
	.cfi_endproc
.LFE7870:
	.size	_ZN12v8_inspector10V8Debugger17asyncTaskFinishedEPv, .-_ZN12v8_inspector10V8Debugger17asyncTaskFinishedEPv
	.section	.text._ZN12v8_inspector10V8Debugger29collectOldAsyncStacksIfNeededEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger29collectOldAsyncStacksIfNeededEv
	.type	_ZN12v8_inspector10V8Debugger29collectOldAsyncStacksIfNeededEv, @function
_ZN12v8_inspector10V8Debugger29collectOldAsyncStacksIfNeededEv:
.LFB7906:
	.cfi_startproc
	endbr64
	movl	312(%rdi), %edx
	movl	232(%rdi), %eax
	cmpl	%eax, %edx
	jle	.L2507
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%eax, %ecx
	shrl	$31, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%eax, %r14d
	shrl	$31, %r14d
	pushq	%r13
	addl	%eax, %r14d
	addl	%ecx, %eax
	pushq	%r12
	andl	$1, %eax
	pushq	%rbx
	sarl	%r14d
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subl	%ecx, %eax
	addl	%eax, %r14d
	subq	$8, %rsp
	cmpl	%r14d, %edx
	jg	.L2404
	jmp	.L2414
	.p2align 4,,10
	.p2align 3
.L2511:
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L2510
	.p2align 4,,10
	.p2align 3
.L2408:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	movl	312(%rbx), %eax
	subl	$1, %eax
	movl	%eax, 312(%rbx)
	cmpl	%r14d, %eax
	jle	.L2414
.L2404:
	subq	$1, 336(%rbx)
	movq	320(%rbx), %r13
	movq	%r13, %rdi
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	movq	24(%r13), %r12
	testq	%r12, %r12
	je	.L2408
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	jne	.L2511
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L2408
.L2510:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L2412
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L2413:
	cmpl	$1, %eax
	jne	.L2408
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L2408
	.p2align 4,,10
	.p2align 3
.L2414:
	movq	136(%rbx), %rcx
	leaq	120(%rbx), %r12
	testq	%rcx, %rcx
	jne	.L2405
	jmp	.L2406
	.p2align 4,,10
	.p2align 3
.L2415:
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L2417
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2406
.L2405:
	movq	24(%rcx), %rax
	testq	%rax, %rax
	jne	.L2415
.L2417:
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	128(%rbx)
	movq	120(%rbx), %rax
	movq	%rdx, %rsi
	movq	(%rax,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L2416:
	movq	%rdx, %r8
	movq	(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L2416
	movq	%r8, %rdx
	movq	%r12, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseEmPNS9_15_Hash_node_baseEPNS9_10_Hash_nodeIS7_Lb0EEE
	movq	%rax, %rcx
	testq	%rcx, %rcx
	jne	.L2405
.L2406:
	movq	560(%rbx), %r12
	leaq	560(%rbx), %r14
	testq	%r12, %r12
	jne	.L2420
	jmp	.L2421
	.p2align 4,,10
	.p2align 3
.L2422:
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L2424
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L2421
.L2420:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L2422
.L2424:
	movq	552(%rbx), %rcx
	movq	8(%r12), %rax
	xorl	%edx, %edx
	movq	544(%rbx), %r8
	divq	%rcx
	leaq	0(,%rdx,8), %r10
	movq	%rdx, %rdi
	leaq	(%r8,%r10), %r9
	movq	(%r9), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2423:
	movq	%rdx, %rsi
	movq	(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2423
	movq	(%r12), %r13
	cmpq	%rsi, %rax
	je	.L2512
	testq	%r13, %r13
	je	.L2428
	movq	8(%r13), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %rdi
	je	.L2428
	movq	%rsi, (%r8,%rdx,8)
	movq	(%r12), %r13
.L2428:
	movq	%r13, (%rsi)
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2431
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2432
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L2433:
	cmpl	$1, %eax
	jne	.L2431
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L2431:
	movq	%r12, %rdi
	movq	%r13, %r12
	call	_ZdlPv@PLT
	subq	$1, 568(%rbx)
	testq	%r12, %r12
	jne	.L2420
.L2421:
	movq	192(%rbx), %r8
	leaq	176(%rbx), %r12
	testq	%r8, %r8
	je	.L2437
	.p2align 4,,10
	.p2align 3
.L2436:
	movq	8(%r8), %rsi
	movq	128(%rbx), %r9
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r9
	movq	120(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2438
	movq	(%rax), %rcx
	movq	8(%rcx), %rdi
	jmp	.L2440
	.p2align 4,,10
	.p2align 3
.L2513:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2438
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%r9
	cmpq	%rdx, %r10
	jne	.L2438
.L2440:
	cmpq	%rdi, %rsi
	jne	.L2513
	movq	(%r8), %r8
.L2442:
	testq	%r8, %r8
	jne	.L2436
.L2437:
	movq	360(%rbx), %r12
	leaq	360(%rbx), %r14
	testq	%r12, %r12
	jne	.L2443
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2444:
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L2446
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L2402
.L2443:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L2444
.L2446:
	movq	352(%rbx), %rcx
	movslq	8(%r12), %rax
	xorl	%edx, %edx
	movq	344(%rbx), %r8
	divq	%rcx
	leaq	0(,%rdx,8), %r10
	movq	%rdx, %rdi
	leaq	(%r8,%r10), %r9
	movq	(%r9), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L2445:
	movq	%rdx, %rsi
	movq	(%rdx), %rdx
	cmpq	%rdx, %r12
	jne	.L2445
	movq	(%r12), %r13
	cmpq	%rsi, %rax
	je	.L2514
	testq	%r13, %r13
	je	.L2450
	movslq	8(%r13), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %rdi
	je	.L2450
	movq	%rsi, (%r8,%rdx,8)
	movq	(%r12), %r13
.L2450:
	movq	%r13, (%rsi)
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2453
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2454
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L2455:
	cmpl	$1, %eax
	jne	.L2453
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L2453:
	movq	%r12, %rdi
	movq	%r13, %r12
	call	_ZdlPv@PLT
	subq	$1, 368(%rbx)
	testq	%r12, %r12
	jne	.L2443
.L2402:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2512:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L2458
	movq	8(%r13), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %rdi
	je	.L2428
	movq	%rsi, (%r8,%rdx,8)
	addq	544(%rbx), %r10
	movq	(%r10), %rax
	movq	%r10, %r9
	cmpq	%r14, %rax
	je	.L2515
.L2429:
	movq	$0, (%r9)
	movq	(%r12), %r13
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2432:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L2433
	.p2align 4,,10
	.p2align 3
.L2514:
	testq	%r13, %r13
	je	.L2459
	movslq	8(%r13), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %rdi
	je	.L2450
	movq	%rsi, (%r8,%rdx,8)
	addq	344(%rbx), %r10
	movq	(%r10), %rax
	movq	%r10, %r9
	cmpq	%r14, %rax
	je	.L2516
.L2451:
	movq	$0, (%r9)
	movq	(%r12), %r13
	jmp	.L2450
	.p2align 4,,10
	.p2align 3
.L2454:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L2455
	.p2align 4,,10
	.p2align 3
.L2438:
	movq	%rsi, %rax
	xorl	%edx, %edx
	divq	184(%rbx)
	movq	176(%rbx), %rax
	movq	%rdx, %rsi
	movq	(%rax,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L2441:
	movq	%rdx, %r9
	movq	(%rdx), %rdx
	cmpq	%r8, %rdx
	jne	.L2441
	movq	%r8, %rcx
	movq	%r9, %rdx
	movq	%r12, %rdi
	call	_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseEmPNS2_15_Hash_node_baseEPNS2_10_Hash_nodeIS0_Lb0EEE
	movq	%rax, %r8
	jmp	.L2442
	.p2align 4,,10
	.p2align 3
.L2412:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L2413
	.p2align 4,,10
	.p2align 3
.L2459:
	movq	%rsi, %rax
	cmpq	%r14, %rax
	jne	.L2451
.L2516:
	movq	%r13, 360(%rbx)
	jmp	.L2451
	.p2align 4,,10
	.p2align 3
.L2458:
	movq	%rsi, %rax
	cmpq	%r14, %rax
	jne	.L2429
.L2515:
	movq	%r13, 560(%rbx)
	jmp	.L2429
	.p2align 4,,10
	.p2align 3
.L2507:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE7906:
	.size	_ZN12v8_inspector10V8Debugger29collectOldAsyncStacksIfNeededEv, .-_ZN12v8_inspector10V8Debugger29collectOldAsyncStacksIfNeededEv
	.section	.text._ZN12v8_inspector10V8Debugger26asyncTaskScheduledForStackERKNS_8String16EPvb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger26asyncTaskScheduledForStackERKNS_8String16EPvb
	.type	_ZN12v8_inspector10V8Debugger26asyncTaskScheduledForStackERKNS_8String16EPvb, @function
_ZN12v8_inspector10V8Debugger26asyncTaskScheduledForStackERKNS_8String16EPvb:
.LFB7871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	236(%rdi), %eax
	testl	%eax, %eax
	jne	.L2567
.L2517:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2568
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2567:
	.cfi_restore_state
	movq	%rsi, %rbx
	leaq	-112(%rbp), %r14
	movq	16(%rdi), %rsi
	movq	%rdi, %r12
	movq	%r14, %rdi
	movq	%rdx, %r15
	movl	%ecx, %r13d
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	_ZN12v8_inspector16V8StackTraceImpl25maxCallStackSizeToCaptureE(%rip), %r8d
	movq	16(%r12), %rdi
	movl	%r8d, -120(%rbp)
	call	_ZN2v87Isolate9InContextEv@PLT
	movl	-120(%rbp), %r8d
	testb	%al, %al
	je	.L2545
	leaq	-80(%rbp), %r9
	movq	16(%r12), %rsi
	movl	%r8d, -128(%rbp)
	movq	%r9, %rdi
	movq	%r9, -136(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	24(%r12), %r10
	movq	16(%r12), %rdi
	movq	%r10, -120(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-120(%rbp), %r10
	movq	%rax, %rsi
	movq	%r10, %rdi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-136(%rbp), %r9
	movl	%eax, -124(%rbp)
	movq	%r9, %rdi
	movq	%r9, -120(%rbp)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-120(%rbp), %r9
	movl	-124(%rbp), %edx
	movl	-128(%rbp), %r8d
.L2520:
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r9, %rdi
	call	_ZN12v8_inspector15AsyncStackTrace7captureEPNS_10V8DebuggerEiRKNS_8String16Ei@PLT
	movq	-80(%rbp), %r8
	testq	%r8, %r8
	je	.L2521
	movq	128(%r12), %rdi
	movq	%r15, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	120(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rbx
	testq	%rax, %rax
	je	.L2522
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2524
	.p2align 4,,10
	.p2align 3
.L2569:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2522
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %rbx
	jne	.L2522
.L2524:
	cmpq	%r15, %rsi
	jne	.L2569
	leaq	16(%rcx), %rbx
.L2543:
	movq	%r8, (%rbx)
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L2525
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2526
	lock addl	$1, 12(%rax)
.L2525:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2528
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2529
	movl	$-1, %edx
	lock xaddl	%edx, 12(%rdi)
.L2530:
	cmpl	$1, %edx
	je	.L2570
.L2528:
	movq	%rax, 8(%rbx)
	testb	%r13b, %r13b
	jne	.L2571
.L2532:
	movl	$32, %edi
	call	_Znwm@PLT
	movdqa	-80(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	leaq	320(%r12), %rsi
	movq	%rax, %rdi
	movaps	%xmm0, -80(%rbp)
	movups	%xmm1, 16(%rax)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	movq	%r12, %rdi
	addq	$1, 336(%r12)
	addl	$1, 312(%r12)
	call	_ZN12v8_inspector10V8Debugger29collectOldAsyncStacksIfNeededEv
.L2521:
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L2537
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L2538
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L2539:
	cmpl	$1, %eax
	jne	.L2537
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L2541
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L2542:
	cmpl	$1, %eax
	jne	.L2537
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L2537:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L2517
	.p2align 4,,10
	.p2align 3
.L2545:
	xorl	%edx, %edx
	leaq	-80(%rbp), %r9
	jmp	.L2520
	.p2align 4,,10
	.p2align 3
.L2538:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L2539
	.p2align 4,,10
	.p2align 3
.L2571:
	movq	184(%r12), %rdi
	movq	%r15, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	176(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L2533
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2535
	.p2align 4,,10
	.p2align 3
.L2572:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2533
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r13
	jne	.L2533
.L2535:
	cmpq	%r15, %rsi
	jne	.L2572
	jmp	.L2532
	.p2align 4,,10
	.p2align 3
.L2526:
	addl	$1, 12(%rax)
	jmp	.L2525
	.p2align 4,,10
	.p2align 3
.L2529:
	movl	12(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%rdi)
	jmp	.L2530
	.p2align 4,,10
	.p2align 3
.L2570:
	movq	(%rdi), %rdx
	movq	%rax, -120(%rbp)
	call	*24(%rdx)
	movq	-120(%rbp), %rax
	jmp	.L2528
	.p2align 4,,10
	.p2align 3
.L2541:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L2542
	.p2align 4,,10
	.p2align 3
.L2522:
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%rbx, %rsi
	leaq	120(%r12), %rdi
	movq	%r15, %rdx
	movq	$0, (%rax)
	movl	$1, %r8d
	movq	%rax, %rcx
	movq	%r15, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	call	_ZNSt10_HashtableIPvSt4pairIKS0_St8weak_ptrIN12v8_inspector15AsyncStackTraceEEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	movq	-80(%rbp), %r8
	leaq	16(%rax), %rbx
	jmp	.L2543
	.p2align 4,,10
	.p2align 3
.L2533:
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	176(%r12), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r15, 8(%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	$0, (%rax)
	call	_ZNSt10_HashtableIPvS0_SaIS0_ENSt8__detail9_IdentityESt8equal_toIS0_ESt4hashIS0_ENS2_18_Mod_range_hashingENS2_20_Default_ranged_hashENS2_20_Prime_rehash_policyENS2_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS2_10_Hash_nodeIS0_Lb0EEEm
	jmp	.L2532
.L2568:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7871:
	.size	_ZN12v8_inspector10V8Debugger26asyncTaskScheduledForStackERKNS_8String16EPvb, .-_ZN12v8_inspector10V8Debugger26asyncTaskScheduledForStackERKNS_8String16EPvb
	.section	.text._ZN12v8_inspector10V8Debugger28setMaxAsyncTaskStacksForTestEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger28setMaxAsyncTaskStacksForTestEi
	.type	_ZN12v8_inspector10V8Debugger28setMaxAsyncTaskStacksForTestEi, @function
_ZN12v8_inspector10V8Debugger28setMaxAsyncTaskStacksForTestEi:
.LFB7914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$0, 232(%rdi)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN12v8_inspector10V8Debugger29collectOldAsyncStacksIfNeededEv
	movl	%r12d, 232(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7914:
	.size	_ZN12v8_inspector10V8Debugger28setMaxAsyncTaskStacksForTestEi, .-_ZN12v8_inspector10V8Debugger28setMaxAsyncTaskStacksForTestEi
	.section	.text._ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm:
.LFB14584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L2576
	movq	(%rbx), %r8
.L2577:
	movq	(%r8,%r15,8), %rax
	leaq	0(,%r15,8), %rcx
	testq	%rax, %rax
	je	.L2586
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r13, (%rax)
.L2587:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2576:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L2600
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2601
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L2579:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L2581
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L2582
	.p2align 4,,10
	.p2align 3
.L2583:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2584:
	testq	%rsi, %rsi
	je	.L2581
.L2582:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L2583
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L2589
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L2582
	.p2align 4,,10
	.p2align 3
.L2581:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L2585
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L2585:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	divq	%r12
	movq	%r8, (%rbx)
	movq	%rdx, %r15
	jmp	.L2577
	.p2align 4,,10
	.p2align 3
.L2586:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L2588
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	(%rbx), %rax
	movq	%r13, (%rax,%rdx,8)
.L2588:
	movq	(%rbx), %rax
	leaq	16(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L2587
	.p2align 4,,10
	.p2align 3
.L2589:
	movq	%rdx, %rdi
	jmp	.L2584
	.p2align 4,,10
	.p2align 3
.L2600:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L2579
.L2601:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE14584:
	.size	_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.section	.text._ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS2_EEEES0_INS5_14_Node_iteratorIS3_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS2_EEEES0_INS5_14_Node_iteratorIS3_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS2_EEEES0_INS5_14_Node_iteratorIS3_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS2_EEEES0_INS5_14_Node_iteratorIS3_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS2_EEEES0_INS5_14_Node_iteratorIS3_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB14120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$32, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movslq	(%rbx), %r10
	movdqu	8(%rbx), %xmm0
	xorl	%edx, %edx
	movq	8(%r12), %rcx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movl	%r10d, 8(%rax)
	movups	%xmm0, 16(%rax)
	movq	%r10, %rax
	divq	%rcx
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2603
	movq	(%rax), %rbx
	movq	%r10, %rsi
	movl	8(%rbx), %r8d
	jmp	.L2605
	.p2align 4,,10
	.p2align 3
.L2616:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L2603
	movslq	8(%rbx), %rax
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L2603
.L2605:
	cmpl	%r8d, %esi
	jne	.L2616
	call	_ZdlPv@PLT
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2603:
	.cfi_restore_state
	movq	%rdi, %rcx
	movq	%r10, %rdx
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	%r9, %rsi
	call	_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	popq	%rbx
	movl	$1, %edx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14120:
	.size	_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS2_EEEES0_INS5_14_Node_iteratorIS3_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS2_EEEES0_INS5_14_Node_iteratorIS3_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZN12v8_inspector10V8Debugger13debuggerIdForEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger13debuggerIdForEi
	.type	_ZN12v8_inspector10V8Debugger13debuggerIdForEi, @function
_ZN12v8_inspector10V8Debugger13debuggerIdForEi:
.LFB7915:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	616(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	%r12d, %rax
	divq	%rsi
	movq	608(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L2618
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movl	8(%rcx), %edi
	jmp	.L2620
	.p2align 4,,10
	.p2align 3
.L2637:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2618
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L2618
.L2620:
	cmpl	%r12d, %edi
	jne	.L2637
	movq	16(%rcx), %rax
	movq	24(%rcx), %rdx
.L2626:
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2638
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2618:
	.cfi_restore_state
	movq	16(%rbx), %rdi
	call	_ZN2v85debug18GetNextRandomInt64EPNS_7IsolateE@PLT
	movq	16(%rbx), %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v85debug18GetNextRandomInt64EPNS_7IsolateE@PLT
	movq	%rax, %xmm0
	orq	-200(%rbp), %rax
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -192(%rbp)
	je	.L2639
.L2621:
	leaq	-176(%rbp), %rsi
	leaq	608(%rbx), %rdi
	movl	%r12d, -176(%rbp)
	movups	%xmm0, -168(%rbp)
	leaq	-80(%rbp), %r13
	leaq	-128(%rbp), %r12
	call	_ZNSt10_HashtableIiSt4pairIKiS0_IllEESaIS3_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJS0_IiS2_EEEES0_INS5_14_Node_iteratorIS3_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	leaq	-144(%rbp), %rdi
	leaq	-192(%rbp), %rsi
	call	_ZN12v8_inspector18debuggerIdToStringERKSt4pairIllE@PLT
	movq	-144(%rbp), %rax
	movq	%r13, -96(%rbp)
	cmpq	%r12, %rax
	je	.L2640
	movq	%rax, -96(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -80(%rbp)
.L2623:
	movq	-136(%rbp), %rax
	movdqa	-192(%rbp), %xmm1
	leaq	-96(%rbp), %rsi
	leaq	664(%rbx), %rdi
	movq	%r12, -144(%rbp)
	movq	%rax, -88(%rbp)
	xorl	%eax, %eax
	movw	%ax, -128(%rbp)
	movq	-112(%rbp), %rax
	movq	$0, -136(%rbp)
	movq	%rax, -64(%rbp)
	movups	%xmm1, -56(%rbp)
	call	_ZNSt10_HashtableIN12v8_inspector8String16ESt4pairIKS1_S2_IllEESaIS5_ENSt8__detail10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb1ELb0ELb1EEEE10_M_emplaceIJS2_IS1_S4_EEEES2_INS7_14_Node_iteratorIS5_Lb0ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2624
	call	_ZdlPv@PLT
.L2624:
	movq	-144(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L2625
	call	_ZdlPv@PLT
.L2625:
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %rdx
	jmp	.L2626
	.p2align 4,,10
	.p2align 3
.L2640:
	movdqa	-128(%rbp), %xmm2
	movaps	%xmm2, -80(%rbp)
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2639:
	movq	$1, -192(%rbp)
	movl	$1, %eax
	movq	%rax, %xmm0
	movhps	-200(%rbp), %xmm0
	jmp	.L2621
.L2638:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7915:
	.size	_ZN12v8_inspector10V8Debugger13debuggerIdForEi, .-_ZN12v8_inspector10V8Debugger13debuggerIdForEi
	.section	.text._ZN12v8_inspector10V8Debugger13stackTraceForEiRKNS_14V8StackTraceIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger13stackTraceForEiRKNS_14V8StackTraceIdE
	.type	_ZN12v8_inspector10V8Debugger13stackTraceForEiRKNS_14V8StackTraceIdE, @function
_ZN12v8_inspector10V8Debugger13stackTraceForEiRKNS_14V8StackTraceIdE:
.LFB7861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	movl	%edx, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	_ZN12v8_inspector10V8Debugger13debuggerIdForEi
	cmpq	%rax, 8(%rbx)
	je	.L2665
.L2643:
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%r12)
.L2641:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2665:
	.cfi_restore_state
	cmpq	%rdx, 16(%rbx)
	jne	.L2643
	movq	(%rbx), %r8
	movq	552(%r13), %rdi
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	movq	544(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L2643
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2645
	.p2align 4,,10
	.p2align 3
.L2666:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2643
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L2643
.L2645:
	cmpq	%rsi, %r8
	jne	.L2666
	movq	24(%rcx), %rax
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L2651
	leaq	8(%rax), %rdx
	movl	8(%rax), %eax
.L2648:
	testl	%eax, %eax
	je	.L2647
	leal	1(%rax), %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L2648
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L2651
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L2651
	movq	16(%rcx), %rax
.L2650:
	movq	%rax, (%r12)
	jmp	.L2641
.L2647:
	movq	$0, 8(%r12)
.L2651:
	xorl	%eax, %eax
	jmp	.L2650
	.cfi_endproc
.LFE7861:
	.size	_ZN12v8_inspector10V8Debugger13stackTraceForEiRKNS_14V8StackTraceIdE, .-_ZN12v8_inspector10V8Debugger13stackTraceForEiRKNS_14V8StackTraceIdE
	.section	.text._ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb.part.0, @function
_ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb.part.0:
.LFB15599:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L2668
	movq	16(%rbx), %rsi
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rdi
	movq	24(%rbx), %r12
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	_ZN2v811HandleScopeD1Ev@PLT
.L2668:
	cmpl	%r12d, 60(%rbx)
	jne	.L2667
	testb	%r14b, %r14b
	jne	.L2678
	movl	%r12d, %esi
	movq	%rbx, %rdi
	leaq	-80(%rbp), %r12
	call	_ZN12v8_inspector10V8Debugger13debuggerIdForEi
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, %rcx
	movq	%rax, %rdx
	call	_ZN12v8_inspector14V8StackTraceIdC1EmSt4pairIllE@PLT
	movq	-80(%rbp), %rax
	movdqu	-72(%rbp), %xmm2
	movq	%rax, 520(%rbx)
	movups	%xmm2, 528(%rbx)
.L2671:
	movl	64(%rbx), %eax
	testl	%eax, %eax
	jne	.L2672
	movq	16(%rbx), %rdi
	call	_ZN2v85debug13BreakRightNowEPNS_7IsolateE@PLT
.L2672:
	movq	%r12, %rdi
	call	_ZN12v8_inspector14V8StackTraceIdC1Ev@PLT
	movq	-80(%rbp), %rax
	movdqu	-72(%rbp), %xmm0
	movq	%rax, 520(%rbx)
	movups	%xmm0, 528(%rbx)
.L2667:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2679
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2678:
	.cfi_restore_state
	leaq	-80(%rbp), %r12
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector14V8StackTraceIdC1EmSt4pairIllE@PLT
	movq	-80(%rbp), %rax
	movdqu	-72(%rbp), %xmm1
	movq	%rax, 520(%rbx)
	movups	%xmm1, 528(%rbx)
	jmp	.L2671
.L2679:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15599:
	.size	_ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb.part.0, .-_ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb.part.0
	.section	.rodata._ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib.str1.1,"aMS",@progbits,1
.LC32:
	.string	"Promise.then"
.LC33:
	.string	"Promise.catch"
.LC34:
	.string	"Promise.finally"
.LC35:
	.string	"async function"
	.section	.text.unlikely._ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib,"ax",@progbits
	.align 2
.LCOLDB36:
	.section	.text._ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib,"ax",@progbits
.LHOTB36:
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib
	.type	_ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib, @function
_ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib:
.LFB7817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$6, %esi
	ja	.L2680
	leal	1(%rdx,%rdx), %r12d
	movl	%esi, %esi
	leaq	.L2683(%rip), %rdx
	movl	%ecx, %ebx
	movslq	(%rdx,%rsi,4), %rax
	movq	%rdi, %r13
	movslq	%r12d, %r12
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib,"a",@progbits
	.align 4
	.align 4
.L2683:
	.long	.L2689-.L2683
	.long	.L2688-.L2683
	.long	.L2687-.L2683
	.long	.L2686-.L2683
	.long	.L2685-.L2683
	.long	.L2684-.L2683
	.long	.L2682-.L2683
	.section	.text._ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib
	.p2align 4,,10
	.p2align 3
.L2695:
	leaq	-80(%rbp), %r14
	leaq	.LC35(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movl	$1, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector10V8Debugger26asyncTaskScheduledForStackERKNS_8String16EPvb
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2698
	call	_ZdlPv@PLT
.L2698:
	movq	128(%r13), %rsi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	120(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2680
	movq	(%rax), %rcx
.L2696:
	movq	8(%rcx), %rdi
	jmp	.L2700
	.p2align 4,,10
	.p2align 3
.L2740:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2680
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%r10, %rdx
	jne	.L2680
.L2700:
	cmpq	%rdi, %r12
	jne	.L2740
	movq	24(%rcx), %r13
	testq	%r13, %r13
	je	.L2680
	movl	8(%r13), %eax
	testl	%eax, %eax
	je	.L2680
	movl	8(%r13), %eax
	leaq	8(%r13), %rbx
.L2703:
	testl	%eax, %eax
	je	.L2728
	leal	1(%rax), %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L2703
	movq	16(%rcx), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector15AsyncStackTrace18setSuspendedTaskIdEPv@PLT
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r12
	testq	%r12, %r12
	je	.L2704
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
.L2705:
	cmpl	$1, %eax
	jne	.L2680
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r12, %r12
	je	.L2706
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L2707:
	cmpl	$1, %eax
	jne	.L2680
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L2680:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2741
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2684:
	.cfi_restore_state
	movq	128(%rdi), %rsi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	120(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L2695
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	movq	%rcx, %rdi
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2742:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2695
	movq	8(%rdi), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L2695
.L2697:
	cmpq	%r8, %r12
	jne	.L2742
	jmp	.L2696
	.p2align 4,,10
	.p2align 3
.L2682:
	movq	%r12, %rsi
	call	_ZN12v8_inspector10V8Debugger25asyncTaskCanceledForStackEPv
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2689:
	leaq	-80(%rbp), %r14
	leaq	.LC32(%rip), %rsi
.L2738:
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector10V8Debugger26asyncTaskScheduledForStackERKNS_8String16EPvb
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2694
	call	_ZdlPv@PLT
.L2694:
	testb	%bl, %bl
	jne	.L2680
	cmpb	$0, 512(%r13)
	je	.L2680
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb.part.0
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2688:
	leaq	-80(%rbp), %r14
	leaq	.LC33(%rip), %rsi
	jmp	.L2738
	.p2align 4,,10
	.p2align 3
.L2687:
	leaq	-80(%rbp), %r14
	leaq	.LC34(%rip), %rsi
	jmp	.L2738
	.p2align 4,,10
	.p2align 3
.L2686:
	movq	%r12, %rsi
	call	_ZN12v8_inspector10V8Debugger24asyncTaskStartedForStackEPv
	cmpb	$0, 504(%r13)
	jne	.L2680
	cmpq	$0, 472(%r13)
	jne	.L2680
	cmpq	456(%r13), %r12
	jne	.L2680
	movq	16(%r13), %rdi
	call	_ZN2v85debug26SetBreakOnNextFunctionCallEPNS_7IsolateE@PLT
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2685:
	movq	%r12, %rsi
	call	_ZN12v8_inspector10V8Debugger25asyncTaskFinishedForStackEPv
	cmpq	$0, 472(%r13)
	jne	.L2680
	cmpq	456(%r13), %r12
	jne	.L2680
	cmpb	$0, 504(%r13)
	movq	$0, 456(%r13)
	jne	.L2680
	movq	16(%r13), %rdi
	call	_ZN2v85debug28ClearBreakOnNextFunctionCallEPNS_7IsolateE@PLT
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2704:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L2705
.L2706:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L2707
.L2741:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib
	.cfi_startproc
	.type	_ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib.cold, @function
_ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib.cold:
.LFSB7817:
.L2728:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	call	abort@PLT
	.cfi_endproc
.LFE7817:
	.section	.text._ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib
	.size	_ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib, .-_ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib
	.section	.text.unlikely._ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib
	.size	_ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib.cold, .-_ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib.cold
.LCOLDE36:
	.section	.text._ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib
.LHOTE36:
	.set	.LTHUNK12,_ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib
	.section	.text._ZThn8_N12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib,"ax",@progbits
	.p2align 4
	.globl	_ZThn8_N12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib
	.type	_ZThn8_N12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib, @function
_ZThn8_N12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib:
.LFB15619:
	.cfi_startproc
	endbr64
	subq	$8, %rdi
	jmp	.LTHUNK12
	.cfi_endproc
.LFE15619:
	.size	_ZThn8_N12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib, .-_ZThn8_N12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib
	.section	.text._ZN12v8_inspector10V8Debugger18asyncTaskScheduledERKNS_10StringViewEPvb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger18asyncTaskScheduledERKNS_10StringViewEPvb
	.type	_ZN12v8_inspector10V8Debugger18asyncTaskScheduledERKNS_10StringViewEPvb, @function
_ZN12v8_inspector10V8Debugger18asyncTaskScheduledERKNS_10StringViewEPvb:
.LFB7867:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r14
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%ecx, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movq	%r12, %rdi
	movzbl	%bl, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector10V8Debugger26asyncTaskScheduledForStackERKNS_8String16EPvb
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2744
	call	_ZdlPv@PLT
.L2744:
	cmpb	$0, 512(%r12)
	je	.L2743
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb.part.0
.L2743:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2748
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2748:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7867:
	.size	_ZN12v8_inspector10V8Debugger18asyncTaskScheduledERKNS_10StringViewEPvb, .-_ZN12v8_inspector10V8Debugger18asyncTaskScheduledERKNS_10StringViewEPvb
	.section	.text._ZN12v8_inspector10V8Debugger22storeCurrentStackTraceERKNS_10StringViewE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger22storeCurrentStackTraceERKNS_10StringViewE
	.type	_ZN12v8_inspector10V8Debugger22storeCurrentStackTraceERKNS_10StringViewE, @function
_ZN12v8_inspector10V8Debugger22storeCurrentStackTraceERKNS_10StringViewE:
.LFB7862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movl	236(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L2750
	call	_ZN12v8_inspector14V8StackTraceIdC1Ev@PLT
.L2749:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2783
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2750:
	.cfi_restore_state
	movq	%rsi, %r15
	leaq	-160(%rbp), %r14
	movq	16(%rsi), %rsi
	movq	%rdx, %r13
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r15), %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	jne	.L2752
.L2754:
	movq	%r12, %rdi
	call	_ZN12v8_inspector14V8StackTraceIdC1Ev@PLT
.L2753:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L2749
	.p2align 4,,10
	.p2align 3
.L2752:
	movq	16(%r15), %rsi
	leaq	-128(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	24(%r15), %r8
	movq	16(%r15), %rdi
	movq	%r8, -184(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-184(%rbp), %r8
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%rbx, %rdi
	movl	%eax, -184(%rbp)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	-184(%rbp), %eax
	testl	%eax, %eax
	je	.L2754
	leaq	-96(%rbp), %rcx
	movl	_ZN12v8_inspector16V8StackTraceImpl25maxCallStackSizeToCaptureE(%rip), %r8d
	movq	%r13, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -192(%rbp)
	movl	%r8d, -196(%rbp)
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	movl	-196(%rbp), %r8d
	movq	%r15, %rsi
	movq	-192(%rbp), %rcx
	movl	-184(%rbp), %edx
	leaq	-176(%rbp), %rdi
	call	_ZN12v8_inspector15AsyncStackTrace7captureEPNS_10V8DebuggerEiRKNS_8String16Ei@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2755
	call	_ZdlPv@PLT
.L2755:
	movq	-176(%rbp), %rax
	testq	%rax, %rax
	je	.L2784
	movq	%rax, -128(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2758
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2759
	lock addl	$1, 8(%rax)
.L2758:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector15AsyncStackTrace5storeEPNS_10V8DebuggerESt10shared_ptrIS0_E@PLT
	movq	-120(%rbp), %rbx
	movq	%rax, %r13
	testq	%rbx, %rbx
	je	.L2761
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L2762
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rbx)
.L2763:
	cmpl	$1, %edx
	je	.L2785
.L2761:
	movl	$32, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movdqa	-176(%rbp), %xmm1
	leaq	320(%r15), %rsi
	movq	%rax, %rdi
	movaps	%xmm0, -176(%rbp)
	movups	%xmm1, 16(%rax)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 336(%r15)
	movq	%r15, %rdi
	addl	$1, 312(%r15)
	call	_ZN12v8_inspector10V8Debugger29collectOldAsyncStacksIfNeededEv
	cmpb	$0, 512(%r15)
	je	.L2767
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb.part.0
.L2767:
	movl	-184(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN12v8_inspector10V8Debugger13debuggerIdForEi
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, %rcx
	movq	%rax, %rdx
	call	_ZN12v8_inspector14V8StackTraceIdC1EmSt4pairIllE@PLT
.L2757:
	movq	-168(%rbp), %r13
	testq	%r13, %r13
	je	.L2753
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L2770
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r13)
.L2771:
	cmpl	$1, %edx
	jne	.L2753
	movq	0(%r13), %rdx
	movq	%rax, -184(%rbp)
	movq	%r13, %rdi
	call	*16(%rdx)
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L2773
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L2774:
	cmpl	$1, %eax
	jne	.L2753
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L2753
	.p2align 4,,10
	.p2align 3
.L2784:
	movq	%r12, %rdi
	call	_ZN12v8_inspector14V8StackTraceIdC1Ev@PLT
	jmp	.L2757
	.p2align 4,,10
	.p2align 3
.L2770:
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L2771
	.p2align 4,,10
	.p2align 3
.L2759:
	addl	$1, 8(%rax)
	jmp	.L2758
	.p2align 4,,10
	.p2align 3
.L2762:
	movl	8(%rbx), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rbx)
	jmp	.L2763
	.p2align 4,,10
	.p2align 3
.L2785:
	movq	(%rbx), %rdx
	movq	%rax, -192(%rbp)
	movq	%rbx, %rdi
	call	*16(%rdx)
	movq	-192(%rbp), %rax
	testq	%rax, %rax
	je	.L2765
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rbx)
.L2766:
	cmpl	$1, %eax
	jne	.L2761
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L2773:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L2774
	.p2align 4,,10
	.p2align 3
.L2765:
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L2766
.L2783:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7862:
	.size	_ZN12v8_inspector10V8Debugger22storeCurrentStackTraceERKNS_10StringViewE, .-_ZN12v8_inspector10V8Debugger22storeCurrentStackTraceERKNS_10StringViewE
	.section	.text._ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb
	.type	_ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb, @function
_ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb:
.LFB7875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 512(%rdi)
	jne	.L2798
.L2786:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2799
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2798:
	.cfi_restore_state
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	movq	%rsi, %r12
	movl	%edx, %r13d
	xorl	%r14d, %r14d
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L2789
	movq	16(%rbx), %rsi
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rdi
	movq	24(%rbx), %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r15, %rdi
	movl	%eax, %r14d
	call	_ZN2v811HandleScopeD1Ev@PLT
.L2789:
	cmpl	%r14d, 60(%rbx)
	jne	.L2786
	testb	%r13b, %r13b
	jne	.L2800
	movl	%r14d, %esi
	movq	%rbx, %rdi
	leaq	-80(%rbp), %r13
	call	_ZN12v8_inspector10V8Debugger13debuggerIdForEi
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rdx, %rcx
	movq	%rax, %rdx
	call	_ZN12v8_inspector14V8StackTraceIdC1EmSt4pairIllE@PLT
	movq	-80(%rbp), %rax
	movdqu	-72(%rbp), %xmm2
	movq	%rax, 520(%rbx)
	movups	%xmm2, 528(%rbx)
.L2792:
	movl	64(%rbx), %eax
	testl	%eax, %eax
	jne	.L2793
	movq	16(%rbx), %rdi
	call	_ZN2v85debug13BreakRightNowEPNS_7IsolateE@PLT
.L2793:
	movq	%r13, %rdi
	call	_ZN12v8_inspector14V8StackTraceIdC1Ev@PLT
	movq	-80(%rbp), %rax
	movdqu	-72(%rbp), %xmm0
	movq	%rax, 520(%rbx)
	movups	%xmm0, 528(%rbx)
	jmp	.L2786
	.p2align 4,,10
	.p2align 3
.L2800:
	leaq	-80(%rbp), %r13
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector14V8StackTraceIdC1EmSt4pairIllE@PLT
	movq	-80(%rbp), %rax
	movdqu	-72(%rbp), %xmm1
	movq	%rax, 520(%rbx)
	movups	%xmm1, 528(%rbx)
	jmp	.L2792
.L2799:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7875:
	.size	_ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb, .-_ZN12v8_inspector10V8Debugger29asyncTaskCandidateForSteppingEPvb
	.section	.data.rel.ro.local._ZTVN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateE,"aw"
	.align 8
	.type	_ZTVN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateE, @object
	.size	_ZTVN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateE, 40
_ZTVN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD1Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicateD0Ev
	.quad	_ZN12v8_inspector12_GLOBAL__N_123MatchPrototypePredicate6FilterEN2v85LocalINS2_6ObjectEEE
	.weak	_ZTVN12v8_inspector10V8DebuggerE
	.section	.data.rel.ro.local._ZTVN12v8_inspector10V8DebuggerE,"awG",@progbits,_ZTVN12v8_inspector10V8DebuggerE,comdat
	.align 8
	.type	_ZTVN12v8_inspector10V8DebuggerE, @object
	.size	_ZTVN12v8_inspector10V8DebuggerE, 112
_ZTVN12v8_inspector10V8DebuggerE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector10V8DebuggerD1Ev
	.quad	_ZN12v8_inspector10V8DebuggerD0Ev
	.quad	_ZN12v8_inspector10V8Debugger14ScriptCompiledEN2v85LocalINS1_5debug6ScriptEEEbb
	.quad	_ZN12v8_inspector10V8Debugger21BreakProgramRequestedEN2v85LocalINS1_7ContextEEERKSt6vectorIiSaIiEE
	.quad	_ZN12v8_inspector10V8Debugger15ExceptionThrownEN2v85LocalINS1_7ContextEEENS2_INS1_5ValueEEES6_bNS1_5debug13ExceptionTypeE
	.quad	_ZN12v8_inspector10V8Debugger20IsFunctionBlackboxedEN2v85LocalINS1_5debug6ScriptEEERKNS3_8LocationES8_
	.quad	_ZN12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib
	.quad	-8
	.quad	0
	.quad	_ZThn8_N12v8_inspector10V8DebuggerD1Ev
	.quad	_ZThn8_N12v8_inspector10V8DebuggerD0Ev
	.quad	_ZThn8_N12v8_inspector10V8Debugger18AsyncEventOccurredEN2v85debug20DebugAsyncActionTypeEib
	.weak	_ZTVSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN12v8_inspector10StackFrameELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
