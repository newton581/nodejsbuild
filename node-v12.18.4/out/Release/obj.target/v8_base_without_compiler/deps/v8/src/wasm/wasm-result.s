	.file	"wasm-result.cc"
	.text
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_115VPrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcP13__va_list_tag,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_115VPrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcP13__va_list_tag, @function
_ZN2v88internal4wasm12_GLOBAL__N_115VPrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcP13__va_list_tag:
.LFB17818:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rdx, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	movl	%ebx, -92(%rbp)
	addq	%rbx, %rax
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
.L3:
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6resizeEmc@PLT
	movdqu	(%r14), %xmm0
	movq	%r12, %rcx
	movq	%r15, %rdx
	movups	%xmm0, (%r12)
	movq	16(%r14), %rax
	movq	%rax, 16(%r12)
	movq	0(%r13), %rdi
	movl	-88(%rbp), %esi
	subl	-92(%rbp), %esi
	addq	%rbx, %rdi
	movslq	%esi, %rsi
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	testl	%eax, %eax
	js	.L7
	cltq
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	(%rax,%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6resizeEmc@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L8
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movq	-88(%rbp), %rdi
	addq	$1, %rdi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo64Em@PLT
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	jmp	.L3
.L8:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17818:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_115VPrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcP13__va_list_tag, .-_ZN2v88internal4wasm12_GLOBAL__N_115VPrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcP13__va_list_tag
	.section	.rodata._ZN2v88internal4wasm12_GLOBAL__N_114PrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcz.constprop.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: "
	.section	.text._ZN2v88internal4wasm12_GLOBAL__N_114PrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcz.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal4wasm12_GLOBAL__N_114PrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcz.constprop.0, @function
_ZN2v88internal4wasm12_GLOBAL__N_114PrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcz.constprop.0:
.LFB21534:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L10
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L10:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	xorl	%esi, %esi
	leaq	-208(%rbp), %rcx
	movq	%rax, -200(%rbp)
	leaq	.LC0(%rip), %rdx
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$24, -208(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm12_GLOBAL__N_115VPrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcP13__va_list_tag
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L13
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21534:
	.size	_ZN2v88internal4wasm12_GLOBAL__N_114PrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcz.constprop.0, .-_ZN2v88internal4wasm12_GLOBAL__N_114PrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcz.constprop.0
	.section	.text._ZN2v88internal4wasm9WasmError11FormatErrorB5cxx11EPKcP13__va_list_tag,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm9WasmError11FormatErrorB5cxx11EPKcP13__va_list_tag
	.type	_ZN2v88internal4wasm9WasmError11FormatErrorB5cxx11EPKcP13__va_list_tag, @function
_ZN2v88internal4wasm9WasmError11FormatErrorB5cxx11EPKcP13__va_list_tag:
.LFB17820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movb	$0, 16(%rdi)
	call	_ZN2v88internal4wasm12_GLOBAL__N_115VPrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcP13__va_list_tag
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17820:
	.size	_ZN2v88internal4wasm9WasmError11FormatErrorB5cxx11EPKcP13__va_list_tag, .-_ZN2v88internal4wasm9WasmError11FormatErrorB5cxx11EPKcP13__va_list_tag
	.section	.text._ZN2v88internal4wasm12ErrorThrower6FormatENS2_9ErrorTypeEPKcP13__va_list_tag,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12ErrorThrower6FormatENS2_9ErrorTypeEPKcP13__va_list_tag
	.type	_ZN2v88internal4wasm12ErrorThrower6FormatENS2_9ErrorTypeEPKcP13__va_list_tag, @function
_ZN2v88internal4wasm12ErrorThrower6FormatENS2_9ErrorTypeEPKcP13__va_list_tag:
.LFB17821:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jne	.L22
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L18
	leaq	.LC0(%rip), %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12_GLOBAL__N_114PrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcz.constprop.0
	movq	32(%rbx), %rsi
.L18:
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_115VPrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcP13__va_list_tag
	movl	%r12d, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE17821:
	.size	_ZN2v88internal4wasm12ErrorThrower6FormatENS2_9ErrorTypeEPKcP13__va_list_tag, .-_ZN2v88internal4wasm12ErrorThrower6FormatENS2_9ErrorTypeEPKcP13__va_list_tag
	.section	.text._ZN2v88internal4wasm12ErrorThrower9TypeErrorEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12ErrorThrower9TypeErrorEPKcz
	.type	_ZN2v88internal4wasm12ErrorThrower9TypeErrorEPKcz, @function
_ZN2v88internal4wasm12ErrorThrower9TypeErrorEPKcz:
.LFB17822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rdx, -192(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L26
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L26:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movl	$16, -240(%rbp)
	movq	%rax, -232(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -224(%rbp)
	movl	16(%rbx), %eax
	movl	$48, -236(%rbp)
	testl	%eax, %eax
	jne	.L25
	movq	8(%rbx), %rcx
	leaq	24(%rbx), %r13
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.L28
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12_GLOBAL__N_114PrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcz.constprop.0
	movq	32(%rbx), %rsi
.L28:
	leaq	-240(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_115VPrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcP13__va_list_tag
	movl	$1, 16(%rbx)
.L25:
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L33
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L33:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17822:
	.size	_ZN2v88internal4wasm12ErrorThrower9TypeErrorEPKcz, .-_ZN2v88internal4wasm12ErrorThrower9TypeErrorEPKcz
	.section	.text._ZN2v88internal4wasm12ErrorThrower10RangeErrorEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12ErrorThrower10RangeErrorEPKcz
	.type	_ZN2v88internal4wasm12ErrorThrower10RangeErrorEPKcz, @function
_ZN2v88internal4wasm12ErrorThrower10RangeErrorEPKcz:
.LFB17823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rdx, -192(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L35
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L35:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movl	$16, -240(%rbp)
	movq	%rax, -232(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -224(%rbp)
	movl	16(%rbx), %eax
	movl	$48, -236(%rbp)
	testl	%eax, %eax
	jne	.L34
	movq	8(%rbx), %rcx
	leaq	24(%rbx), %r13
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.L37
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12_GLOBAL__N_114PrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcz.constprop.0
	movq	32(%rbx), %rsi
.L37:
	leaq	-240(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_115VPrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcP13__va_list_tag
	movl	$2, 16(%rbx)
.L34:
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L42
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L42:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17823:
	.size	_ZN2v88internal4wasm12ErrorThrower10RangeErrorEPKcz, .-_ZN2v88internal4wasm12ErrorThrower10RangeErrorEPKcz
	.section	.text._ZN2v88internal4wasm12ErrorThrower12CompileErrorEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12ErrorThrower12CompileErrorEPKcz
	.type	_ZN2v88internal4wasm12ErrorThrower12CompileErrorEPKcz, @function
_ZN2v88internal4wasm12ErrorThrower12CompileErrorEPKcz:
.LFB17824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rdx, -192(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L44
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L44:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movl	$16, -240(%rbp)
	movq	%rax, -232(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -224(%rbp)
	movl	16(%rbx), %eax
	movl	$48, -236(%rbp)
	testl	%eax, %eax
	jne	.L43
	movq	8(%rbx), %rcx
	leaq	24(%rbx), %r13
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.L46
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12_GLOBAL__N_114PrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcz.constprop.0
	movq	32(%rbx), %rsi
.L46:
	leaq	-240(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_115VPrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcP13__va_list_tag
	movl	$3, 16(%rbx)
.L43:
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L51:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17824:
	.size	_ZN2v88internal4wasm12ErrorThrower12CompileErrorEPKcz, .-_ZN2v88internal4wasm12ErrorThrower12CompileErrorEPKcz
	.section	.text._ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz
	.type	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz, @function
_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz:
.LFB17825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rdx, -192(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L53
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L53:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movl	$16, -240(%rbp)
	movq	%rax, -232(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -224(%rbp)
	movl	16(%rbx), %eax
	movl	$48, -236(%rbp)
	testl	%eax, %eax
	jne	.L52
	movq	8(%rbx), %rcx
	leaq	24(%rbx), %r13
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.L55
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12_GLOBAL__N_114PrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcz.constprop.0
	movq	32(%rbx), %rsi
.L55:
	leaq	-240(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_115VPrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcP13__va_list_tag
	movl	$4, 16(%rbx)
.L52:
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L60
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L60:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17825:
	.size	_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz, .-_ZN2v88internal4wasm12ErrorThrower9LinkErrorEPKcz
	.section	.text._ZN2v88internal4wasm12ErrorThrower12RuntimeErrorEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12ErrorThrower12RuntimeErrorEPKcz
	.type	_ZN2v88internal4wasm12ErrorThrower12RuntimeErrorEPKcz, @function
_ZN2v88internal4wasm12ErrorThrower12RuntimeErrorEPKcz:
.LFB17826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rdx, -192(%rbp)
	movq	%rcx, -184(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L62
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L62:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	movl	$16, -240(%rbp)
	movq	%rax, -232(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -224(%rbp)
	movl	16(%rbx), %eax
	movl	$48, -236(%rbp)
	testl	%eax, %eax
	jne	.L61
	movq	8(%rbx), %rcx
	leaq	24(%rbx), %r13
	xorl	%esi, %esi
	testq	%rcx, %rcx
	je	.L64
	leaq	.LC0(%rip), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm12_GLOBAL__N_114PrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcz.constprop.0
	movq	32(%rbx), %rsi
.L64:
	leaq	-240(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm12_GLOBAL__N_115VPrintFToStringEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEmPKcP13__va_list_tag
	movl	$5, 16(%rbx)
.L61:
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17826:
	.size	_ZN2v88internal4wasm12ErrorThrower12RuntimeErrorEPKcz, .-_ZN2v88internal4wasm12ErrorThrower12RuntimeErrorEPKcz
	.section	.rodata._ZN2v88internal4wasm12ErrorThrower5ReifyEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"unreachable code"
.LC2:
	.string	"(location_) != nullptr"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal4wasm12ErrorThrower5ReifyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12ErrorThrower5ReifyEv
	.type	_ZN2v88internal4wasm12ErrorThrower5ReifyEv, @function
_ZN2v88internal4wasm12ErrorThrower5ReifyEv:
.LFB17827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$5, 16(%rdi)
	ja	.L71
	movl	16(%rdi), %eax
	leaq	.L73(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm12ErrorThrower5ReifyEv,"a",@progbits
	.align 4
	.align 4
.L73:
	.long	.L78-.L73
	.long	.L77-.L73
	.long	.L76-.L73
	.long	.L75-.L73
	.long	.L74-.L73
	.long	.L72-.L73
	.section	.text._ZN2v88internal4wasm12ErrorThrower5ReifyEv
	.p2align 4,,10
	.p2align 3
.L72:
	movq	(%rdi), %r13
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	1799(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L97
.L91:
	movq	41088(%r13), %r12
	cmpq	41096(%r13), %r12
	je	.L103
.L93:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r12)
.L71:
	movq	24(%rbx), %rdx
	movq	32(%rbx), %rax
	leaq	-64(%rbp), %rsi
	movq	(%rbx), %rdi
	movq	%rdx, -64(%rbp)
	xorl	%edx, %edx
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal7Factory17NewStringFromUtf8ERKNS0_6VectorIKcEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L104
	movq	24(%rbx), %rax
	movl	$0, 16(%rbx)
	movq	%r12, %rsi
	movq	$0, 32(%rbx)
	movb	$0, (%rax)
	movq	(%rbx), %rdi
	call	_ZN2v88internal7Factory8NewErrorENS0_6HandleINS0_10JSFunctionEEENS2_INS0_6StringEEE@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L105
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	(%rdi), %r13
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	1791(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L91
.L97:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L76:
	movq	(%rdi), %r13
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	1719(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L97
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L77:
	movq	(%rdi), %r13
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	1767(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L97
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L75:
	movq	(%rdi), %r13
	movq	12464(%r13), %rax
	movq	39(%rax), %rax
	movq	1783(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L97
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L104:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L93
.L78:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L105:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17827:
	.size	_ZN2v88internal4wasm12ErrorThrower5ReifyEv, .-_ZN2v88internal4wasm12ErrorThrower5ReifyEv
	.section	.text._ZN2v88internal4wasm12ErrorThrower5ResetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12ErrorThrower5ResetEv
	.type	_ZN2v88internal4wasm12ErrorThrower5ResetEv, @function
_ZN2v88internal4wasm12ErrorThrower5ResetEv:
.LFB17828:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	movl	$0, 16(%rdi)
	movq	$0, 32(%rdi)
	movb	$0, (%rax)
	ret
	.cfi_endproc
.LFE17828:
	.size	_ZN2v88internal4wasm12ErrorThrower5ResetEv, .-_ZN2v88internal4wasm12ErrorThrower5ResetEv
	.section	.text._ZN2v88internal4wasm12ErrorThrowerC2EOS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12ErrorThrowerC2EOS2_
	.type	_ZN2v88internal4wasm12ErrorThrowerC2EOS2_, @function
_ZN2v88internal4wasm12ErrorThrowerC2EOS2_:
.LFB17830:
	.cfi_startproc
	endbr64
	movl	16(%rsi), %eax
	movdqu	(%rsi), %xmm0
	movl	%eax, 16(%rdi)
	leaq	40(%rdi), %rax
	movq	%rax, 24(%rdi)
	leaq	40(%rsi), %rax
	movups	%xmm0, (%rdi)
	movq	24(%rsi), %rdx
	cmpq	%rax, %rdx
	je	.L110
	movq	%rdx, 24(%rdi)
	movq	40(%rsi), %rdx
	movq	%rdx, 40(%rdi)
.L109:
	movq	32(%rsi), %rdx
	movq	%rax, 24(%rsi)
	movq	$0, 32(%rsi)
	movq	%rdx, 32(%rdi)
	movb	$0, 40(%rsi)
	movl	$0, 16(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	movdqu	40(%rsi), %xmm1
	movups	%xmm1, 40(%rdi)
	jmp	.L109
	.cfi_endproc
.LFE17830:
	.size	_ZN2v88internal4wasm12ErrorThrowerC2EOS2_, .-_ZN2v88internal4wasm12ErrorThrowerC2EOS2_
	.globl	_ZN2v88internal4wasm12ErrorThrowerC1EOS2_
	.set	_ZN2v88internal4wasm12ErrorThrowerC1EOS2_,_ZN2v88internal4wasm12ErrorThrowerC2EOS2_
	.section	.text._ZN2v88internal4wasm12ErrorThrowerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm12ErrorThrowerD2Ev
	.type	_ZN2v88internal4wasm12ErrorThrowerD2Ev, @function
_ZN2v88internal4wasm12ErrorThrowerD2Ev:
.LFB17833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	16(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L112
	movq	(%rdi), %r12
	movq	12480(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L115
.L112:
	movq	24(%rbx), %rdi
	addq	$40, %rbx
	cmpq	%rbx, %rdi
	je	.L111
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	call	_ZN2v88internal4wasm12ErrorThrower5ReifyEv
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	jmp	.L112
	.cfi_endproc
.LFE17833:
	.size	_ZN2v88internal4wasm12ErrorThrowerD2Ev, .-_ZN2v88internal4wasm12ErrorThrowerD2Ev
	.globl	_ZN2v88internal4wasm12ErrorThrowerD1Ev
	.set	_ZN2v88internal4wasm12ErrorThrowerD1Ev,_ZN2v88internal4wasm12ErrorThrowerD2Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm9WasmError11FormatErrorB5cxx11EPKcP13__va_list_tag,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm9WasmError11FormatErrorB5cxx11EPKcP13__va_list_tag, @function
_GLOBAL__sub_I__ZN2v88internal4wasm9WasmError11FormatErrorB5cxx11EPKcP13__va_list_tag:
.LFB21506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21506:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm9WasmError11FormatErrorB5cxx11EPKcP13__va_list_tag, .-_GLOBAL__sub_I__ZN2v88internal4wasm9WasmError11FormatErrorB5cxx11EPKcP13__va_list_tag
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm9WasmError11FormatErrorB5cxx11EPKcP13__va_list_tag
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
