	.file	"debug-type-profile.cc"
	.text
	.section	.text._ZN2v88internal11TypeProfile10SelectModeEPNS0_7IsolateENS_5debug15TypeProfileModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11TypeProfile10SelectModeEPNS0_7IsolateENS_5debug15TypeProfileModeE
	.type	_ZN2v88internal11TypeProfile10SelectModeEPNS0_7IsolateENS_5debug15TypeProfileModeE, @function
_ZN2v88internal11TypeProfile10SelectModeEPNS0_7IsolateENS_5debug15TypeProfileModeE:
.LFB17834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	41836(%rdi), %esi
	je	.L2
	call	_ZN2v88internal7Isolate42CollectSourcePositionsForAllBytecodeArraysEv@PLT
.L2:
	movq	41088(%r12), %rax
	movl	41104(%r12), %edx
	movq	41096(%r12), %r14
	movq	%rax, -104(%rbp)
	leal	1(%rdx), %eax
	movl	%eax, 41104(%r12)
	testl	%ebx, %ebx
	jne	.L3
	movq	4720(%r12), %rax
	cmpq	%rax, 88(%r12)
	je	.L4
	movl	11(%rax), %ecx
	testl	%ecx, %ecx
	je	.L6
	xorl	%r15d, %r15d
	leaq	-80(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L9:
	movq	15(%rax), %rcx
	sarq	$32, %rcx
	cmpl	%r15d, %ecx
	jle	.L6
	movq	23(%rax,%r15,8), %rax
	movq	%r13, %rdi
	movq	%rax, -96(%rbp)
	movq	7(%rax), %rax
	movq	23(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal16FeedbackMetadata18HasTypeProfileSlotEv@PLT
	testb	%al, %al
	jne	.L28
.L7:
	movq	4720(%r12), %rax
	addq	$1, %r15
	movl	11(%rax), %edx
	testl	%edx, %edx
	jne	.L9
.L6:
	movl	41832(%r12), %eax
	testl	%eax, %eax
	je	.L10
.L26:
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
.L11:
	movl	%ebx, 41836(%r12)
	movq	-104(%rbp), %rbx
	movl	%eax, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	%rdx, %r14
	je	.L1
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movl	$0, 41836(%r12)
	movl	%edx, 41104(%r12)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	-96(%rbp), %rdi
	call	_ZNK2v88internal14FeedbackVector18GetTypeProfileSlotEv@PLT
	movq	-96(%rbp), %rcx
	movq	$0, -80(%rbp)
	movl	%eax, -64(%rbp)
	movl	%eax, %esi
	xorl	%eax, %eax
	movq	%rcx, -88(%rbp)
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	jne	.L30
.L8:
	movq	%r13, %rdi
	movl	%eax, -60(%rbp)
	call	_ZN2v88internal13FeedbackNexus16ResetTypeProfileEv@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	-88(%rbp), %rdi
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate33MaybeInitializeVectorListFromHeapEv@PLT
	movl	41104(%r12), %eax
	movq	41096(%r12), %rdx
	subl	$1, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L10:
	movq	88(%r12), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate35SetFeedbackVectorsForProfilingToolsENS0_6ObjectE@PLT
	jmp	.L26
.L29:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17834:
	.size	_ZN2v88internal11TypeProfile10SelectModeEPNS0_7IsolateENS_5debug15TypeProfileModeE, .-_ZN2v88internal11TypeProfile10SelectModeEPNS0_7IsolateENS_5debug15TypeProfileModeE
	.section	.rodata._ZNSt6vectorIN2v88internal16TypeProfileEntryESaIS2_EE17_M_realloc_insertIJRiS_INS1_6HandleINS1_6StringEEESaIS9_EEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal16TypeProfileEntryESaIS2_EE17_M_realloc_insertIJRiS_INS1_6HandleINS1_6StringEEESaIS9_EEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal16TypeProfileEntryESaIS2_EE17_M_realloc_insertIJRiS_INS1_6HandleINS1_6StringEEESaIS9_EEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal16TypeProfileEntryESaIS2_EE17_M_realloc_insertIJRiS_INS1_6HandleINS1_6StringEEESaIS9_EEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal16TypeProfileEntryESaIS2_EE17_M_realloc_insertIJRiS_INS1_6HandleINS1_6StringEEESaIS9_EEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal16TypeProfileEntryESaIS2_EE17_M_realloc_insertIJRiS_INS1_6HandleINS1_6StringEEESaIS9_EEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB20825:
	.cfi_startproc
	endbr64
	movabsq	$288230376151711743, %r8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r15
	movq	%r13, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%r8, %rax
	je	.L50
	movq	%rsi, %rbx
	movq	%rdi, %r12
	subq	%r15, %rsi
	testq	%rax, %rax
	je	.L41
	movabsq	$9223372036854775776, %rdi
	leaq	(%rax,%rax), %r9
	cmpq	%r9, %rax
	jbe	.L51
.L33:
	movq	%rcx, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rsi
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rcx
	leaq	(%rax,%rdi), %rax
	leaq	32(%r14), %r8
.L40:
	movl	(%rdx), %edi
	movdqu	(%rcx), %xmm0
	addq	%r14, %rsi
	pxor	%xmm1, %xmm1
	movq	16(%rcx), %rdx
	movups	%xmm1, (%rcx)
	movq	$0, 16(%rcx)
	movl	%edi, (%rsi)
	movq	%rdx, 24(%rsi)
	movups	%xmm0, 8(%rsi)
	cmpq	%r15, %rbx
	je	.L35
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L36:
	movl	(%rdx), %esi
	addq	$32, %rdx
	addq	$32, %rcx
	movl	%esi, -32(%rcx)
	movdqu	-24(%rdx), %xmm2
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L36
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%r14,%rdx), %r8
.L35:
	cmpq	%r13, %rbx
	je	.L37
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L38:
	movl	(%rdx), %esi
	movdqu	8(%rdx), %xmm3
	addq	$32, %rdx
	addq	$32, %rcx
	movl	%esi, -32(%rcx)
	movq	-8(%rdx), %rsi
	movups	%xmm3, -24(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%r13, %rdx
	jne	.L38
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L37:
	testq	%r15, %r15
	je	.L39
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L39:
	movq	%r14, %xmm0
	movq	%r8, %xmm4
	movq	%rax, 16(%r12)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	testq	%r9, %r9
	jne	.L34
	movl	$32, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$32, %edi
	jmp	.L33
.L34:
	cmpq	%r8, %r9
	movq	%r8, %rax
	cmovbe	%r9, %rax
	salq	$5, %rax
	movq	%rax, %rdi
	jmp	.L33
.L50:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE20825:
	.size	_ZNSt6vectorIN2v88internal16TypeProfileEntryESaIS2_EE17_M_realloc_insertIJRiS_INS1_6HandleINS1_6StringEEESaIS9_EEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal16TypeProfileEntryESaIS2_EE17_M_realloc_insertIJRiS_INS1_6HandleINS1_6StringEEESaIS9_EEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal17TypeProfileScriptESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal17TypeProfileScriptESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal17TypeProfileScriptESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal17TypeProfileScriptESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal17TypeProfileScriptESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB20834:
	.cfi_startproc
	endbr64
	movabsq	$288230376151711743, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rax
	movq	(%rdi), %r14
	movq	%rax, -72(%rbp)
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rcx, %rax
	je	.L95
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movq	%rsi, %r15
	subq	%r14, %rbx
	testq	%rax, %rax
	je	.L75
	leaq	(%rax,%rax), %rsi
	movq	%rsi, -64(%rbp)
	cmpq	%rsi, %rax
	jbe	.L96
	movabsq	$9223372036854775776, %rax
	movq	%rax, -64(%rbp)
.L54:
	movq	-64(%rbp), %rdi
	movq	%rdx, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
.L74:
	movq	-56(%rbp), %rax
	movq	16(%rdx), %r9
	pxor	%xmm0, %xmm0
	movq	8(%rdx), %rsi
	leaq	(%rax,%rbx), %rcx
	movq	(%rdx), %rax
	movq	%r9, %rbx
	subq	%rsi, %rbx
	movq	$0, 24(%rcx)
	movq	%rax, (%rcx)
	movq	%rbx, %rax
	sarq	$5, %rax
	movups	%xmm0, 8(%rcx)
	je	.L97
	movabsq	$288230376151711743, %rsi
	cmpq	%rsi, %rax
	ja	.L62
	movq	%rbx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%rcx, -80(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rcx
	movq	%rax, %r12
	movq	16(%rdx), %r9
	movq	8(%rdx), %rsi
.L57:
	movq	%r12, %xmm0
	leaq	(%r12,%rbx), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 24(%rcx)
	movups	%xmm0, 8(%rcx)
	cmpq	%r9, %rsi
	je	.L59
	movq	%rcx, -96(%rbp)
	movq	%r12, %rbx
	movq	%rsi, %r12
	movq	%r9, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L68:
	movl	(%r12), %eax
	pxor	%xmm1, %xmm1
	movl	%eax, (%rbx)
	movq	16(%r12), %rdx
	subq	8(%r12), %rdx
	movq	$0, 24(%rbx)
	movq	%rdx, %rax
	movups	%xmm1, 8(%rbx)
	sarq	$3, %rax
	je	.L98
	movabsq	$1152921504606846975, %rsi
	cmpq	%rsi, %rax
	ja	.L62
	movq	%rdx, %rdi
	movq	%rdx, -80(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rdx
.L61:
	movq	%rax, %xmm0
	addq	%rax, %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	movq	16(%r12), %rdi
	movq	8(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L63
	subq	$8, %rdi
	leaq	15(%rdx), %r8
	subq	%rdx, %rdi
	subq	%rax, %r8
	movq	%rdi, %r9
	shrq	$3, %r9
	cmpq	$30, %r8
	jbe	.L77
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %r9
	je	.L77
	addq	$1, %r9
	xorl	%r8d, %r8d
	movq	%r9, %r10
	shrq	%r10
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L65:
	movdqu	(%rdx,%r8), %xmm2
	movups	%xmm2, (%rax,%r8)
	addq	$16, %r8
	cmpq	%r10, %r8
	jne	.L65
	movq	%r9, %r10
	andq	$-2, %r10
	leaq	0(,%r10,8), %r8
	addq	%r8, %rdx
	addq	%rax, %r8
	cmpq	%r10, %r9
	je	.L67
	movq	(%rdx), %rdx
	movq	%rdx, (%r8)
.L67:
	leaq	8(%rax,%rdi), %rax
.L63:
	movq	%rax, 16(%rbx)
	addq	$32, %r12
	addq	$32, %rbx
	cmpq	%r12, -88(%rbp)
	jne	.L68
	movq	-96(%rbp), %rcx
	movq	%rbx, %r12
.L59:
	movq	%r12, 16(%rcx)
	cmpq	%r14, %r15
	je	.L78
	movq	-56(%rbp), %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L70:
	movq	(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -32(%rdx)
	movdqu	-24(%rax), %xmm3
	movups	%xmm3, -24(%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r15
	jne	.L70
	movq	%r15, %rbx
	subq	%r14, %rbx
	addq	-56(%rbp), %rbx
.L69:
	addq	$32, %rbx
	cmpq	-72(%rbp), %r15
	je	.L71
	movq	%rbx, %rdx
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L72:
	movq	(%rax), %rcx
	movdqu	8(%rax), %xmm4
	addq	$32, %rdx
	addq	$32, %rax
	movq	%rcx, -32(%rdx)
	movq	-8(%rax), %rcx
	movups	%xmm4, -24(%rdx)
	movq	%rcx, -8(%rdx)
	cmpq	-72(%rbp), %rax
	jne	.L72
	subq	%r15, %rax
	addq	%rax, %rbx
.L71:
	testq	%r14, %r14
	je	.L73
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L73:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm5
	movq	%rax, %xmm0
	addq	-64(%rbp), %rax
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L55
	movq	$0, -56(%rbp)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L77:
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L64:
	movq	(%rdx,%r8,8), %rcx
	movq	%rcx, (%rax,%r8,8)
	movq	%r8, %rcx
	addq	$1, %r8
	cmpq	%r9, %rcx
	jne	.L64
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%eax, %eax
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L75:
	movq	$32, -64(%rbp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L97:
	xorl	%r12d, %r12d
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L78:
	movq	-56(%rbp), %rbx
	jmp	.L69
.L62:
	call	_ZSt17__throw_bad_allocv@PLT
.L95:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L55:
	movq	-64(%rbp), %rax
	cmpq	%rcx, %rax
	cmova	%rcx, %rax
	salq	$5, %rax
	movq	%rax, -64(%rbp)
	jmp	.L54
	.cfi_endproc
.LFE20834:
	.size	_ZNSt6vectorIN2v88internal17TypeProfileScriptESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal17TypeProfileScriptESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZN2v88internal11TypeProfile7CollectEPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11TypeProfile7CollectEPNS0_7IsolateE
	.type	_ZN2v88internal11TypeProfile7CollectEPNS0_7IsolateE, @function
_ZN2v88internal11TypeProfile7CollectEPNS0_7IsolateE:
.LFB17809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$264, %rsp
	movq	%rdi, -272(%rbp)
	movl	$24, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	movq	%rax, (%r15)
	leaq	-208(%rbp), %r15
	movq	$0, 16(%rax)
	movq	%r15, %rdi
	movups	%xmm0, (%rax)
	call	_ZN2v88internal6Script8IteratorC1EPNS0_7IsolateE@PLT
	movq	%r15, %rdi
	call	_ZN2v88internal6Script8Iterator4NextEv@PLT
	leaq	-232(%rbp), %rsi
	movq	%rsi, -248(%rbp)
	leaq	-236(%rbp), %rsi
	movq	%rax, -232(%rbp)
	movq	%rsi, -296(%rbp)
	testq	%rax, %rax
	je	.L99
	movq	%r15, -256(%rbp)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L149:
	movq	-256(%rbp), %rdi
	call	_ZN2v88internal6Script8Iterator4NextEv@PLT
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	je	.L99
.L100:
	movq	-248(%rbp), %rdi
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L149
	movq	41112(%rbx), %rdi
	movq	-232(%rbp), %rsi
	testq	%rdi, %rdi
	je	.L103
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L104:
	movq	4720(%rbx), %rdx
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	11(%rdx), %esi
	testl	%esi, %esi
	je	.L149
	leaq	-224(%rbp), %rax
	xorl	%r14d, %r14d
	leaq	-128(%rbp), %r12
	movq	%rax, -280(%rbp)
	.p2align 4,,10
	.p2align 3
.L126:
	movq	15(%rdx), %rax
	sarq	$32, %rax
	cmpl	%r14d, %eax
	jle	.L107
	movq	23(%rdx,%r14,8), %rax
	movq	%rax, -224(%rbp)
	movq	7(%rax), %rax
	movq	%rax, -216(%rbp)
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L179
.L108:
	cmpq	%rax, -232(%rbp)
	je	.L180
.L111:
	movl	11(%rdx), %eax
	addq	$1, %r14
	testl	%eax, %eax
	jne	.L126
.L107:
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %r13
	cmpq	%r13, %rax
	je	.L128
	movq	-272(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -264(%rbp)
	movq	8(%rax), %r13
	cmpq	16(%rax), %r13
	je	.L129
	movq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, 24(%r13)
	movq	%rax, 0(%r13)
	movq	-80(%rbp), %r12
	subq	-88(%rbp), %r12
	movups	%xmm0, 8(%r13)
	movq	%r12, %rax
	sarq	$5, %rax
	je	.L181
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L136
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%rax, %r14
.L131:
	movq	%r14, %xmm0
	addq	%r14, %r12
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 24(%r13)
	movups	%xmm0, 8(%r13)
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %r8
	cmpq	%r8, %rax
	je	.L133
	movq	%r13, -280(%rbp)
	movq	%rax, %r12
	movq	%r8, %r13
	movabsq	$2305843009213693948, %r15
	movq	%rbx, -288(%rbp)
	movq	%r14, %rbx
	.p2align 4,,10
	.p2align 3
.L143:
	movl	0(%r13), %eax
	pxor	%xmm3, %xmm3
	movl	%eax, (%rbx)
	movq	16(%r13), %r14
	subq	8(%r13), %r14
	movq	$0, 24(%rbx)
	movq	%r14, %rax
	movups	%xmm3, 8(%rbx)
	sarq	$3, %rax
	je	.L182
	movabsq	$1152921504606846975, %rsi
	cmpq	%rsi, %rax
	ja	.L136
	movq	%r14, %rdi
	call	_Znwm@PLT
.L135:
	movq	%rax, %xmm0
	leaq	(%rax,%r14), %rdx
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, 24(%rbx)
	movups	%xmm0, 8(%rbx)
	movq	16(%r13), %rdi
	movq	8(%r13), %rcx
	cmpq	%rcx, %rdi
	je	.L137
	subq	$8, %rdi
	leaq	15(%rcx), %rdx
	subq	%rcx, %rdi
	subq	%rax, %rdx
	movq	%rdi, %r9
	shrq	$3, %r9
	cmpq	$30, %rdx
	jbe	.L152
	testq	%r15, %r9
	je	.L152
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %r10
	shrq	%r10
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L139:
	movdqu	(%rcx,%rdx), %xmm2
	movups	%xmm2, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r10
	jne	.L139
	movq	%r9, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %r10
	addq	%r10, %rcx
	addq	%rax, %r10
	cmpq	%r9, %rdx
	je	.L142
	movq	(%rcx), %rdx
	movq	%rdx, (%r10)
.L142:
	leaq	8(%rax,%rdi), %rax
.L137:
	addq	$32, %r13
	movq	%rax, 16(%rbx)
	addq	$32, %rbx
	cmpq	%r13, %r12
	jne	.L143
	movq	%rbx, %r14
	movq	-280(%rbp), %r13
	movq	-288(%rbp), %rbx
.L133:
	movq	-264(%rbp), %rax
	movq	%r14, 16(%r13)
	addq	$32, 8(%rax)
.L144:
	movq	-80(%rbp), %r12
	movq	-88(%rbp), %r13
	cmpq	%r13, %r12
	je	.L128
	.p2align 4,,10
	.p2align 3
.L148:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L145
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %r12
	jne	.L148
.L146:
	movq	-88(%rbp), %r13
.L128:
	testq	%r13, %r13
	je	.L149
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L145:
	addq	$32, %r13
	cmpq	%r13, %r12
	jne	.L148
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L180:
	movq	-216(%rbp), %rax
	movq	23(%rax), %rax
	movq	-1(%rax), %rcx
	cmpw	$75, 11(%rcx)
	jne	.L111
	movl	7(%rax), %ecx
	testl	%ecx, %ecx
	je	.L111
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal16FeedbackMetadata18HasTypeProfileSlotEv@PLT
	testb	%al, %al
	jne	.L174
.L178:
	movq	4720(%rbx), %rdx
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L179:
	movq	-1(%rax), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L108
	movq	23(%rax), %rax
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L182:
	xorl	%eax, %eax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L152:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L138:
	movq	(%rcx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%r9, %rsi
	jne	.L138
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L103:
	movq	41088(%rbx), %rax
	cmpq	%rax, 41096(%rbx)
	je	.L183
.L105:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L99:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L184
	movq	-272(%rbp), %rax
	addq	$264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	movq	-280(%rbp), %rdi
	call	_ZNK2v88internal14FeedbackVector18GetTypeProfileSlotEv@PLT
	movq	-224(%rbp), %rdx
	movq	$0, -192(%rbp)
	movl	%eax, -176(%rbp)
	movl	%eax, %esi
	xorl	%eax, %eax
	movq	%rdx, -128(%rbp)
	movq	%rdx, -184(%rbp)
	testq	%rdx, %rdx
	jne	.L185
.L113:
	leaq	-216(%rbp), %rdi
	movl	%eax, -172(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo9DebugNameEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L114
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L115:
	leaq	-192(%rbp), %r13
	leaq	-160(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK2v88internal13FeedbackNexus18GetSourcePositionsEv@PLT
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %r15
	movq	%rax, -264(%rbp)
	cmpq	%r15, %rax
	je	.L123
	.p2align 4,,10
	.p2align 3
.L124:
	movl	(%r15), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	%edx, -236(%rbp)
	call	_ZNK2v88internal13FeedbackNexus26GetTypesForSourcePositionsEj@PLT
	movq	-80(%rbp), %rsi
	cmpq	-72(%rbp), %rsi
	je	.L119
	movdqa	-128(%rbp), %xmm0
	movl	-236(%rbp), %edx
	pxor	%xmm1, %xmm1
	movq	-112(%rbp), %rax
	movaps	%xmm1, -128(%rbp)
	movq	$0, -112(%rbp)
	movl	%edx, (%rsi)
	movups	%xmm0, 8(%rsi)
	addq	$32, -80(%rbp)
	movq	%rax, 24(%rsi)
.L120:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L121
	call	_ZdlPv@PLT
	addq	$4, %r15
	cmpq	%r15, -264(%rbp)
	jne	.L124
.L123:
	movq	%r13, %rdi
	call	_ZN2v88internal13FeedbackNexus16ResetTypeProfileEv@PLT
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L178
	call	_ZdlPv@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%rax, %rdi
	leaq	-96(%rbp), %rdx
	movq	%r13, %rsi
	call	_ZNSt6vectorIN2v88internal17TypeProfileScriptESaIS2_EE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L114:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L186
.L116:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L121:
	addq	$4, %r15
	cmpq	%r15, -264(%rbp)
	jne	.L124
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L119:
	movq	-296(%rbp), %rdx
	leaq	-88(%rbp), %rdi
	movq	%r12, %rcx
	call	_ZNSt6vectorIN2v88internal16TypeProfileEntryESaIS2_EE17_M_realloc_insertIJRiS_INS1_6HandleINS1_6StringEEESaIS9_EEEEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L181:
	xorl	%r14d, %r14d
	jmp	.L131
.L185:
	movq	%r12, %rdi
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	jmp	.L113
.L183:
	movq	%rbx, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	jmp	.L105
.L186:
	movq	%rbx, %rdi
	movq	%rsi, -264(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	jmp	.L116
.L136:
	call	_ZSt17__throw_bad_allocv@PLT
.L184:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17809:
	.size	_ZN2v88internal11TypeProfile7CollectEPNS0_7IsolateE, .-_ZN2v88internal11TypeProfile7CollectEPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11TypeProfile7CollectEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11TypeProfile7CollectEPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal11TypeProfile7CollectEPNS0_7IsolateE:
.LFB21924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21924:
	.size	_GLOBAL__sub_I__ZN2v88internal11TypeProfile7CollectEPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal11TypeProfile7CollectEPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11TypeProfile7CollectEPNS0_7IsolateE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
