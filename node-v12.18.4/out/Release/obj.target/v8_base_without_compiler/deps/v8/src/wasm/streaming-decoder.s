	.file	"streaming-decoder.cc"
	.text
	.section	.text._ZNK2v88internal4wasm16StreamingDecoder13SectionBuffer7GetCodeENS1_12WireBytesRefE,"axG",@progbits,_ZNK2v88internal4wasm16StreamingDecoder13SectionBuffer7GetCodeENS1_12WireBytesRefE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal4wasm16StreamingDecoder13SectionBuffer7GetCodeENS1_12WireBytesRefE
	.type	_ZNK2v88internal4wasm16StreamingDecoder13SectionBuffer7GetCodeENS1_12WireBytesRefE, @function
_ZNK2v88internal4wasm16StreamingDecoder13SectionBuffer7GetCodeENS1_12WireBytesRefE:
.LFB5459:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	shrq	$32, %rsi
	subl	8(%rdi), %edx
	movl	%edx, %eax
	addl	%esi, %edx
	subq	%rax, %rdx
	addq	16(%rdi), %rax
	movslq	%edx, %rdx
	ret
	.cfi_endproc
.LFE5459:
	.size	_ZNK2v88internal4wasm16StreamingDecoder13SectionBuffer7GetCodeENS1_12WireBytesRefE, .-_ZNK2v88internal4wasm16StreamingDecoder13SectionBuffer7GetCodeENS1_12WireBytesRefE
	.section	.text._ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv,"axG",@progbits,_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv
	.type	_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv, @function
_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv:
.LFB5467:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5467:
	.size	_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv, .-_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv
	.section	.text._ZN2v88internal4wasm7Decoder12onFirstErrorEv,"axG",@progbits,_ZN2v88internal4wasm7Decoder12onFirstErrorEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.type	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, @function
_ZN2v88internal4wasm7Decoder12onFirstErrorEv:
.LFB17912:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE17912:
	.size	_ZN2v88internal4wasm7Decoder12onFirstErrorEv, .-_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.section	.text._ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv
	.type	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv, @function
_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv:
.LFB18046:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	movl	$5, %edx
	ret
	.cfi_endproc
.LFE18046:
	.size	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv, .-_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv
	.section	.text._ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader6bufferEv,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader6bufferEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader6bufferEv
	.type	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader6bufferEv, @function
_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader6bufferEv:
.LFB18047:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	movl	$8, %edx
	ret
	.cfi_endproc
.LFE18047:
	.size	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader6bufferEv, .-_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader6bufferEv
	.section	.text._ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID6bufferEv,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID6bufferEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID6bufferEv
	.type	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID6bufferEv, @function
_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID6bufferEv:
.LFB18051:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	movl	$1, %edx
	ret
	.cfi_endproc
.LFE18051:
	.size	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID6bufferEv, .-_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID6bufferEv
	.section	.text._ZNK2v88internal4wasm16StreamingDecoder15DecodeSectionID20is_finishing_allowedEv,"axG",@progbits,_ZNK2v88internal4wasm16StreamingDecoder15DecodeSectionID20is_finishing_allowedEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal4wasm16StreamingDecoder15DecodeSectionID20is_finishing_allowedEv
	.type	_ZNK2v88internal4wasm16StreamingDecoder15DecodeSectionID20is_finishing_allowedEv, @function
_ZNK2v88internal4wasm16StreamingDecoder15DecodeSectionID20is_finishing_allowedEv:
.LFB18052:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE18052:
	.size	_ZNK2v88internal4wasm16StreamingDecoder15DecodeSectionID20is_finishing_allowedEv, .-_ZNK2v88internal4wasm16StreamingDecoder15DecodeSectionID20is_finishing_allowedEv
	.section	.text._ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload6bufferEv,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload6bufferEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload6bufferEv
	.type	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload6bufferEv, @function
_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload6bufferEv:
.LFB18063:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rcx
	movq	32(%rcx), %rax
	movq	24(%rcx), %rdx
	subq	%rax, %rdx
	addq	16(%rcx), %rax
	ret
	.cfi_endproc
.LFE18063:
	.size	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload6bufferEv, .-_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload6bufferEv
	.section	.text._ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody6bufferEv,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody6bufferEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody6bufferEv
	.type	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody6bufferEv, @function
_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody6bufferEv:
.LFB18073:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	addq	16(%rcx), %rax
	ret
	.cfi_endproc
.LFE18073:
	.size	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody6bufferEv, .-_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody6bufferEv
	.section	.text._ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD2Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD2Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD2Ev, @function
_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD2Ev:
.LFB21408:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21408:
	.size	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD2Ev, .-_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD2Ev
	.weak	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD1Ev
	.set	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD1Ev,_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD2Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD2Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD2Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD2Ev, @function
_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD2Ev:
.LFB21432:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21432:
	.size	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD2Ev, .-_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD2Ev
	.weak	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD1Ev
	.set	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD1Ev,_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD2Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD2Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD2Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD2Ev, @function
_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD2Ev:
.LFB21456:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21456:
	.size	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD2Ev, .-_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD2Ev
	.weak	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD1Ev
	.set	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD1Ev,_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD2Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD2Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD2Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD2Ev, @function
_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD2Ev:
.LFB21480:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21480:
	.size	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD2Ev, .-_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD2Ev
	.weak	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD1Ev
	.set	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD1Ev,_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD2Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD2Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD2Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD2Ev, @function
_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD2Ev:
.LFB21508:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21508:
	.size	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD2Ev, .-_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD2Ev
	.weak	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD1Ev
	.set	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD1Ev,_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD2Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD2Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD2Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD2Ev, @function
_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD2Ev:
.LFB21532:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21532:
	.size	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD2Ev, .-_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD2Ev
	.weak	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD1Ev
	.set	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD1Ev,_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD2Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD2Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD2Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD2Ev, @function
_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD2Ev:
.LFB23146:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23146:
	.size	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD2Ev, .-_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD2Ev
	.weak	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD1Ev
	.set	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD1Ev,_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB23183:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE23183:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB23186:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	leaq	16(%rdi), %r8
	movq	%r8, %rdi
	movq	(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE23186:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN2v88internal4wasm7DecoderD2Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD2Ev
	.type	_ZN2v88internal4wasm7DecoderD2Ev, @function
_ZN2v88internal4wasm7DecoderD2Ev:
.LFB23150:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %r8
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	addq	$64, %rdi
	movq	%rax, -64(%rdi)
	cmpq	%rdi, %r8
	je	.L20
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	ret
	.cfi_endproc
.LFE23150:
	.size	_ZN2v88internal4wasm7DecoderD2Ev, .-_ZN2v88internal4wasm7DecoderD2Ev
	.weak	_ZN2v88internal4wasm7DecoderD1Ev
	.set	_ZN2v88internal4wasm7DecoderD1Ev,_ZN2v88internal4wasm7DecoderD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB23187:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE23187:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD0Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD0Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD0Ev, @function
_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD0Ev:
.LFB21534:
	.cfi_startproc
	endbr64
	movl	$56, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21534:
	.size	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD0Ev, .-_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD0Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD0Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD0Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD0Ev, @function
_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD0Ev:
.LFB21482:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21482:
	.size	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD0Ev, .-_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD0Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD0Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD0Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD0Ev, @function
_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD0Ev:
.LFB21410:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21410:
	.size	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD0Ev, .-_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD0Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD0Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD0Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD0Ev, @function
_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD0Ev:
.LFB23148:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23148:
	.size	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD0Ev, .-_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD0Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD0Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD0Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD0Ev, @function
_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD0Ev:
.LFB21510:
	.cfi_startproc
	endbr64
	movl	$80, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21510:
	.size	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD0Ev, .-_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD0Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD0Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD0Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD0Ev, @function
_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD0Ev:
.LFB21458:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21458:
	.size	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD0Ev, .-_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD0Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD0Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD0Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD0Ev, @function
_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD0Ev:
.LFB21434:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21434:
	.size	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD0Ev, .-_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD0Ev
	.section	.text._ZN2v88internal4wasm7DecoderD0Ev,"axG",@progbits,_ZN2v88internal4wasm7DecoderD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7DecoderD0Ev
	.type	_ZN2v88internal4wasm7DecoderD0Ev, @function
_ZN2v88internal4wasm7DecoderD0Ev:
.LFB23152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L31
	call	_ZdlPv@PLT
.L31:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23152:
	.size	_ZN2v88internal4wasm7DecoderD0Ev, .-_ZN2v88internal4wasm7DecoderD0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB23185:
	.cfi_startproc
	endbr64
	movl	$56, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23185:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder13DecodingState9ReadBytesEPS2_NS0_6VectorIKhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder13DecodingState9ReadBytesEPS2_NS0_6VectorIKhEE
	.type	_ZN2v88internal4wasm16StreamingDecoder13DecodingState9ReadBytesEPS2_NS0_6VectorIKhEE, @function
_ZN2v88internal4wasm16StreamingDecoder13DecodingState9ReadBytesEPS2_NS0_6VectorIKhEE:
.LFB17996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	8(%rdi), %r13
	call	*32(%rax)
	movq	%r14, %rsi
	subq	%r13, %rdx
	leaq	0(%r13,%rax), %rdi
	cmpq	%r12, %rdx
	cmovb	%rdx, %r12
	movq	%r12, %rdx
	call	memcpy@PLT
	addq	%r12, 8(%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17996:
	.size	_ZN2v88internal4wasm16StreamingDecoder13DecodingState9ReadBytesEPS2_NS0_6VectorIKhEE, .-_ZN2v88internal4wasm16StreamingDecoder13DecodingState9ReadBytesEPS2_NS0_6VectorIKhEE
	.section	.text._ZN2v88internal4wasm16StreamingDecoder13SectionBufferD2Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD2Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD2Ev, @function
_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD2Ev:
.LFB23162:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder13SectionBufferE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L36
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	ret
	.cfi_endproc
.LFE23162:
	.size	_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD2Ev, .-_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD2Ev
	.weak	_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD1Ev
	.set	_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD1Ev,_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD2Ev
	.section	.text._ZN2v88internal4wasm16StreamingDecoder13SectionBufferD0Ev,"axG",@progbits,_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD0Ev
	.type	_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD0Ev, @function
_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD0Ev:
.LFB23164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder13SectionBufferE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L39
	call	_ZdaPv@PLT
.L39:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE23164:
	.size	_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD0Ev, .-_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB23188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L44
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L44:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23188:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.rodata._ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID4NextEPS2_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"section length"
	.section	.text._ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID4NextEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID4NextEPS2_
	.type	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID4NextEPS2_, @function
_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID4NextEPS2_:
.LFB18079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$64, %edi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movzbl	16(%rsi), %r13d
	movl	20(%rsi), %ebx
	call	_Znwm@PLT
	leaq	.LC0(%rip), %rdx
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	$1073741824, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	%rcx, (%rax)
	movb	%r13b, 56(%rax)
	movl	%ebx, 60(%rax)
	movups	%xmm0, 40(%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18079:
	.size	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID4NextEPS2_, .-_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID4NextEPS2_
	.section	.text._ZNSt14_Function_base13_Base_managerIN2v88internal4wasm12_GLOBAL__N_123TopTierCompiledCallbackEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIN2v88internal4wasm12_GLOBAL__N_123TopTierCompiledCallbackEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIN2v88internal4wasm12_GLOBAL__N_123TopTierCompiledCallbackEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation:
.LFB21370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpl	$2, %edx
	je	.L51
	cmpl	$3, %edx
	je	.L52
	cmpl	$1, %edx
	je	.L76
.L53:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	(%rsi), %r13
	movl	$48, %edi
	call	_Znwm@PLT
	movq	%rax, %rbx
	movq	0(%r13), %rax
	movq	%rax, (%rbx)
	movq	8(%r13), %rax
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.L54
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L55
	lock addl	$1, 12(%rax)
.L54:
	movq	$0, 32(%rbx)
	movq	32(%r13), %rax
	testq	%rax, %rax
	je	.L56
	leaq	16(%r13), %rsi
	leaq	16(%rbx), %rdi
	movl	$2, %edx
	call	*%rax
	movdqu	32(%r13), %xmm0
	movups	%xmm0, 32(%rbx)
.L56:
	movq	%rbx, (%r12)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L53
	movq	32(%r12), %rax
	testq	%rax, %rax
	je	.L57
	leaq	16(%r12), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L57:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L59
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L60
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L61:
	cmpl	$1, %eax
	jne	.L59
	movq	(%rdi), %rax
	call	*24(%rax)
.L59:
	movq	%r12, %rdi
	movl	$48, %esi
	call	_ZdlPvm@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	addl	$1, 12(%rax)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L60:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L61
	.cfi_endproc
.LFE21370:
	.size	_ZNSt14_Function_base13_Base_managerIN2v88internal4wasm12_GLOBAL__N_123TopTierCompiledCallbackEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIN2v88internal4wasm12_GLOBAL__N_123TopTierCompiledCallbackEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation
	.section	.text._ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader4NextEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader4NextEPS2_
	.type	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader4NextEPS2_, @function
_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader4NextEPS2_:
.LFB18076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdx), %r13
	testq	%r13, %r13
	je	.L80
	movq	0(%r13), %rax
	movq	8(%rdx), %rdi
	movq	%rdx, %rbx
	movq	16(%rax), %r14
	movq	(%rdi), %rax
	call	*32(%rax)
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movslq	%edx, %rdx
	call	*%r14
	testb	%al, %al
	je	.L89
	cmpq	$0, (%rbx)
	je	.L80
.L90:
	movl	44(%rbx), %ebx
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder15DecodeSectionIDE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movb	$0, 16(%rax)
	movl	%ebx, 20(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	$0, (%rbx)
	testq	%rdi, %rdi
	je	.L80
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpq	$0, (%rbx)
	jne	.L90
.L80:
	popq	%rbx
	movq	%r12, %rax
	movq	$0, (%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18076:
	.size	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader4NextEPS2_, .-_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader4NextEPS2_
	.section	.text._ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload4NextEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload4NextEPS2_
	.type	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload4NextEPS2_, @function
_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload4NextEPS2_:
.LFB18087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L94
	movq	16(%rsi), %rax
	movq	%rdx, %rbx
	movq	(%rdi), %r9
	movq	32(%rax), %r8
	movq	16(%rax), %rdx
	movq	24(%rax), %rcx
	leaq	(%rdx,%r8), %r10
	movsbl	(%rdx), %esi
	subq	%r8, %rcx
	movq	%r10, %rdx
	addl	8(%rax), %r8d
	movslq	%ecx, %rcx
	call	*24(%r9)
	testb	%al, %al
	je	.L103
	cmpq	$0, (%rbx)
	je	.L94
.L104:
	movl	44(%rbx), %ebx
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder15DecodeSectionIDE(%rip), %rdx
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movb	$0, 16(%rax)
	movl	%ebx, 20(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	$0, (%rbx)
	testq	%rdi, %rdi
	je	.L94
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpq	$0, (%rbx)
	jne	.L104
.L94:
	movq	%r12, %rax
	popq	%rbx
	movq	$0, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18087:
	.size	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload4NextEPS2_, .-_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload4NextEPS2_
	.section	.text._ZNSt17_Function_handlerIFvN2v88internal4wasm16CompilationEventEENS2_12_GLOBAL__N_123TopTierCompiledCallbackEE9_M_invokeERKSt9_Any_dataOS3_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvN2v88internal4wasm16CompilationEventEENS2_12_GLOBAL__N_123TopTierCompiledCallbackEE9_M_invokeERKSt9_Any_dataOS3_, @function
_ZNSt17_Function_handlerIFvN2v88internal4wasm16CompilationEventEENS2_12_GLOBAL__N_123TopTierCompiledCallbackEE9_M_invokeERKSt9_Any_dataOS3_:
.LFB21368:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$1, (%rsi)
	je	.L135
.L105:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L136
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	movq	(%rdi), %rdx
	movq	8(%rdx), %rax
	movq	%rax, -40(%rbp)
	testq	%rax, %rax
	je	.L105
	leaq	8(%rax), %rcx
	movl	8(%rax), %eax
.L109:
	testl	%eax, %eax
	je	.L105
	leal	1(%rax), %esi
	lock cmpxchgl	%esi, (%rcx)
	jne	.L109
	movq	-40(%rbp), %rax
	testq	%rax, %rax
	je	.L105
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L112
	movq	$0, -48(%rbp)
.L113:
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L105
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L116
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L117:
	cmpl	$1, %eax
	jne	.L105
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L119
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L120:
	cmpl	$1, %eax
	jne	.L105
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%rdx), %rax
	movq	%rax, -48(%rbp)
	testq	%rax, %rax
	je	.L113
	cmpq	$0, 32(%rdx)
	je	.L137
	leaq	-48(%rbp), %rsi
	leaq	16(%rdx), %rdi
	call	*40(%rdx)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L116:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L119:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L120
.L136:
	call	__stack_chk_fail@PLT
.L137:
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE21368:
	.size	_ZNSt17_Function_handlerIFvN2v88internal4wasm16CompilationEventEENS2_12_GLOBAL__N_123TopTierCompiledCallbackEE9_M_invokeERKSt9_Any_dataOS3_, .-_ZNSt17_Function_handlerIFvN2v88internal4wasm16CompilationEventEENS2_12_GLOBAL__N_123TopTierCompiledCallbackEE9_M_invokeERKSt9_Any_dataOS3_
	.section	.rodata._ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody4NextEPS2_.str1.1,"aMS",@progbits,1
.LC1:
	.string	"body size"
	.section	.text._ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody4NextEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody4NextEPS2_
	.type	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody4NextEPS2_, @function
_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody4NextEPS2_:
.LFB18094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	leaq	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody6bufferEv(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$152, %rsp
	movl	48(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L139
	movq	16(%rsi), %rax
	movq	32(%rbx), %rdx
	movq	24(%rsi), %rsi
	addq	16(%rax), %rsi
.L140:
	movq	(%r12), %rdi
	movslq	%edx, %rdx
	testq	%rdi, %rdi
	je	.L145
	movq	(%rdi), %rax
	call	*40(%rax)
	testb	%al, %al
	jne	.L169
	movq	(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L145
	movq	(%rdi), %rax
	call	*8(%rax)
.L169:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L145
	movq	40(%rbx), %r15
	movq	32(%rbx), %r14
	addq	24(%rbx), %r14
	movq	16(%rbx), %rbx
	testq	%r15, %r15
	jne	.L170
	cmpq	24(%rbx), %r14
	je	.L149
	xorl	%edx, %edx
	leaq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rbx
	movq	$36, -168(%rbp)
	leaq	-168(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-168(%rbp), %rdx
	movdqa	.LC2(%rip), %xmm0
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC3(%rip), %xmm0
	movq	-160(%rbp), %rdx
	movl	$1684370293, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-168(%rbp), %rax
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	movl	44(%r12), %eax
	movq	-160(%rbp), %rdx
	subl	$1, %eax
	cmpq	%rbx, %rdx
	je	.L171
	movq	-144(%rbp), %rcx
	leaq	-72(%rbp), %r15
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	movq	%rcx, -112(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movl	%eax, -96(%rbp)
	movq	%r15, -88(%rbp)
	cmpq	%r14, %rdx
	je	.L151
	movq	%rdx, -88(%rbp)
	movq	%rcx, -72(%rbp)
.L153:
	movq	(%r12), %rdi
	movq	%rsi, -80(%rbp)
	movq	%r14, -128(%rbp)
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
	testq	%rdi, %rdi
	je	.L154
	movq	(%rdi), %rax
	leaq	-96(%rbp), %rsi
	call	*64(%rax)
	movq	(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L154
	movq	(%rdi), %rax
	call	*8(%rax)
.L154:
	movq	-88(%rbp), %rdi
	movq	$0, 0(%r13)
	cmpq	%r15, %rdi
	je	.L155
	call	_ZdlPv@PLT
.L155:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L156
	call	_ZdlPv@PLT
.L156:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L138
	call	_ZdlPv@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L170:
	movl	$80, %edi
	subq	$1, %r15
	call	_Znwm@PLT
	leaq	.LC1(%rip), %rcx
	pxor	%xmm0, %xmm0
	movq	%rcx, 32(%rax)
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	$7654321, 24(%rax)
	movq	%rcx, (%rax)
	movq	%rbx, 56(%rax)
	movq	%r14, 64(%rax)
	movq	%r15, 72(%rax)
	movups	%xmm0, 40(%rax)
	movq	%rax, 0(%r13)
.L138:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	$0, 0(%r13)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L139:
	movl	%ecx, -180(%rbp)
	movq	%rsi, %rdi
	call	*%rax
	movl	-180(%rbp), %ecx
	movq	%rax, %rsi
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L149:
	movl	44(%r12), %ebx
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder15DecodeSectionIDE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movb	$0, 16(%rax)
	movl	%ebx, 20(%rax)
	movq	%rax, 0(%r13)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	-72(%rbp), %r15
	movdqa	-144(%rbp), %xmm1
	movl	%eax, -96(%rbp)
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movb	$0, -144(%rbp)
	movq	$0, -152(%rbp)
	movq	%r15, -88(%rbp)
	movaps	%xmm1, -112(%rbp)
.L151:
	movdqa	-112(%rbp), %xmm2
	movups	%xmm2, -72(%rbp)
	jmp	.L153
.L172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18094:
	.size	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody4NextEPS2_, .-_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody4NextEPS2_
	.section	.rodata._ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt324NextEPS2_.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"function size > maximum function size: "
	.section	.rodata._ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt324NextEPS2_.str1.1,"aMS",@progbits,1
.LC5:
	.string	" < "
	.section	.text._ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt324NextEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt324NextEPS2_
	.type	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt324NextEPS2_, @function
_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt324NextEPS2_:
.LFB18075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$536, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, (%rdx)
	je	.L197
	movq	24(%rsi), %rax
	movq	%rsi, %r14
	movq	%rdx, %rbx
	cmpq	%rax, 40(%rsi)
	ja	.L198
	movq	(%rsi), %rax
	call	*48(%rax)
.L173:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L199
	addq	$536, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	movq	.LC6(%rip), %xmm1
	leaq	-320(%rbp), %r13
	leaq	-432(%rbp), %r12
	movq	%r13, %rdi
	movhps	.LC7(%rip), %xmm1
	movaps	%xmm1, -560(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movq	%rax, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-560(%rbp), %xmm1
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -568(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -560(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$39, %edx
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	40(%r14), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$3, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	24(%r14), %rsi
	movq	%r12, %rdi
	leaq	-528(%rbp), %r12
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-384(%rbp), %rax
	movq	%r12, -544(%rbp)
	leaq	-544(%rbp), %rdi
	movq	$0, -536(%rbp)
	movb	$0, -528(%rbp)
	testq	%rax, %rax
	je	.L177
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L178
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L179:
	movl	44(%rbx), %eax
	movq	-544(%rbp), %rdx
	subl	$1, %eax
	cmpq	%r12, %rdx
	je	.L200
	movq	-528(%rbp), %rcx
	movl	%eax, -480(%rbp)
	leaq	-496(%rbp), %r14
	leaq	-456(%rbp), %rax
	movq	-536(%rbp), %rsi
	movq	%r12, -544(%rbp)
	movq	%rcx, -496(%rbp)
	movq	$0, -536(%rbp)
	movb	$0, -528(%rbp)
	movq	%rax, -472(%rbp)
	cmpq	%r14, %rdx
	je	.L181
	movq	%rdx, -472(%rbp)
	movq	%rcx, -456(%rbp)
.L183:
	movq	(%rbx), %rdi
	movq	%rsi, -464(%rbp)
	movq	%r14, -512(%rbp)
	movq	$0, -504(%rbp)
	movb	$0, -496(%rbp)
	testq	%rdi, %rdi
	je	.L184
	movq	(%rdi), %rdx
	movq	%rax, -576(%rbp)
	leaq	-480(%rbp), %rsi
	call	*64(%rdx)
	movq	(%rbx), %rdi
	movq	-576(%rbp), %rax
	movq	$0, (%rbx)
	testq	%rdi, %rdi
	je	.L184
	movq	(%rdi), %rdx
	call	*8(%rdx)
	movq	-576(%rbp), %rax
.L184:
	movq	-472(%rbp), %rdi
	movq	$0, (%r15)
	cmpq	%rax, %rdi
	je	.L185
	call	_ZdlPv@PLT
.L185:
	movq	-512(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L186
	call	_ZdlPv@PLT
.L186:
	movq	-544(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L187
	call	_ZdlPv@PLT
.L187:
	movq	.LC6(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC8(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-560(%rbp), %rdi
	je	.L188
	call	_ZdlPv@PLT
.L188:
	movq	-568(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L197:
	movq	$0, (%rdi)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L178:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L200:
	movl	%eax, -480(%rbp)
	leaq	-456(%rbp), %rax
	movdqa	-528(%rbp), %xmm2
	leaq	-496(%rbp), %r14
	movq	-536(%rbp), %rsi
	movb	$0, -528(%rbp)
	movq	$0, -536(%rbp)
	movq	%rax, -472(%rbp)
	movaps	%xmm2, -496(%rbp)
.L181:
	movdqa	-496(%rbp), %xmm3
	movups	%xmm3, -456(%rbp)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L177:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L179
.L199:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18075:
	.size	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt324NextEPS2_, .-_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt324NextEPS2_
	.section	.text._ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctions13NextWithValueEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctions13NextWithValueEPS2_
	.type	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctions13NextWithValueEPS2_, @function
_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctions13NextWithValueEPS2_:
.LFB18088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	48(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	56(%rsi), %rax
	movq	32(%rax), %rcx
	movq	24(%rax), %r14
	subq	%rcx, %r14
	cmpq	%r14, %r8
	ja	.L281
	movq	16(%rax), %r15
	movq	(%rsi), %rax
	leaq	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv(%rip), %rdx
	movq	%rsi, %rbx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L212
	leaq	16(%rsi), %rsi
.L213:
	leaq	(%r15,%rcx), %rdi
	movq	%r8, %rdx
	call	memcpy@PLT
	movq	40(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L214
	cmpq	%r14, 48(%rbx)
	je	.L215
	xorl	%edx, %edx
	leaq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rbx
	movq	$36, -176(%rbp)
	leaq	-176(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-176(%rbp), %rdx
	movdqa	.LC2(%rip), %xmm0
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC3(%rip), %xmm0
	movl	$1684370293, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-176(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	movl	44(%r12), %eax
	movq	-160(%rbp), %rdx
	subl	$1, %eax
	cmpq	%rbx, %rdx
	je	.L282
	movq	-144(%rbp), %rcx
	leaq	-72(%rbp), %r15
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	movq	%rcx, -112(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movl	%eax, -96(%rbp)
	movq	%r15, -88(%rbp)
	cmpq	%r14, %rdx
	je	.L217
.L218:
	movq	%rdx, -88(%rbp)
	movq	%rcx, -72(%rbp)
.L219:
	movq	%rsi, -80(%rbp)
	movq	%r14, -128(%rbp)
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L220
	movq	(%rdi), %rax
	leaq	-96(%rbp), %rsi
	call	*64(%rax)
	movq	(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L220
	movq	(%rdi), %rax
	call	*8(%rax)
.L220:
	movq	-88(%rbp), %rdi
	movq	$0, 0(%r13)
	cmpq	%r15, %rdi
	je	.L221
	call	_ZdlPv@PLT
.L221:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L222
	call	_ZdlPv@PLT
.L222:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L283
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movq	24(%r12), %rdx
	movq	-8(%rdx), %r14
	movq	-16(%rdx), %rax
	testq	%r14, %r14
	je	.L224
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rcx
	leaq	8(%r14), %rdx
	testq	%rcx, %rcx
	je	.L225
	lock addl	$1, (%rdx)
.L226:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L248
	movl	40(%rbx), %esi
.L245:
	movq	(%rdi), %rdx
	movq	%r14, %xmm1
	movq	%rax, %xmm0
	movl	44(%r12), %eax
	punpcklqdq	%xmm1, %xmm0
	leaq	-176(%rbp), %rcx
	movq	32(%rdx), %r8
	movaps	%xmm0, -176(%rbp)
	leal	-1(%rax), %edx
	call	*%r8
	movq	-168(%rbp), %r14
	movl	%eax, %r15d
	testq	%r14, %r14
	je	.L228
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rcx
	testq	%rcx, %rcx
	je	.L229
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L230:
	cmpl	$1, %eax
	je	.L284
.L228:
	movq	(%r12), %rdi
	testb	%r15b, %r15b
	jne	.L238
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L243
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	(%r12), %rdi
.L238:
	testq	%rdi, %rdi
	je	.L243
	movq	56(%rbx), %r14
	movl	$80, %edi
	movq	32(%r14), %r12
	addq	48(%rbx), %r12
	movq	40(%rbx), %rbx
	call	_Znwm@PLT
	leaq	.LC1(%rip), %rcx
	pxor	%xmm0, %xmm0
	movq	%rcx, 32(%rax)
	leaq	-1(%rbx), %rdx
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	$7654321, 24(%rax)
	movq	%rcx, (%rax)
	movq	%r14, 56(%rax)
	movq	%r12, 64(%rax)
	movq	%rdx, 72(%rax)
	movups	%xmm0, 40(%rax)
	movq	%rax, 0(%r13)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L281:
	xorl	%edx, %edx
	leaq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rbx
	movq	$27, -176(%rbp)
	leaq	-176(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-176(%rbp), %rdx
	movdqa	.LC9(%rip), %xmm0
	movabsq	$7954883203367397748, %rcx
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movl	$29799, %edx
	movq	%rcx, 16(%rax)
	movw	%dx, 24(%rax)
	movb	$104, 26(%rax)
	movups	%xmm0, (%rax)
	movq	-176(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	movl	44(%r12), %eax
	movq	-160(%rbp), %rdx
	subl	$1, %eax
	cmpq	%rbx, %rdx
	je	.L285
	movq	-144(%rbp), %rcx
	leaq	-72(%rbp), %r15
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	movq	%rcx, -112(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movl	%eax, -96(%rbp)
	movq	%r15, -88(%rbp)
	cmpq	%r14, %rdx
	jne	.L218
	.p2align 4,,10
	.p2align 3
.L204:
	movdqa	-112(%rbp), %xmm3
	movups	%xmm3, -72(%rbp)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L224:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L245
	.p2align 4,,10
	.p2align 3
.L243:
	movq	$0, 0(%r13)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L215:
	movl	44(%r12), %ebx
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder15DecodeSectionIDE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movb	$0, 16(%rax)
	movl	%ebx, 20(%rax)
	movq	%rax, 0(%r13)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%rcx, -192(%rbp)
	movq	%rsi, %rdi
	movq	%r8, -184(%rbp)
	call	*%rax
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L248:
	testq	%rcx, %rcx
	je	.L286
	movl	$-1, %eax
	lock xaddl	%eax, (%rdx)
.L236:
	cmpl	$1, %eax
	je	.L237
.L270:
	movq	(%r12), %rdi
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L285:
	movdqa	-144(%rbp), %xmm2
	leaq	-72(%rbp), %r15
	movl	%eax, -96(%rbp)
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movb	$0, -144(%rbp)
	movq	$0, -152(%rbp)
	movq	%r15, -88(%rbp)
	movaps	%xmm2, -112(%rbp)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L282:
	leaq	-72(%rbp), %r15
	movdqa	-144(%rbp), %xmm4
	movl	%eax, -96(%rbp)
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movb	$0, -144(%rbp)
	movq	$0, -152(%rbp)
	movq	%r15, -88(%rbp)
	movaps	%xmm4, -112(%rbp)
.L217:
	movdqa	-112(%rbp), %xmm5
	movups	%xmm5, -72(%rbp)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L225:
	addl	$1, 8(%r14)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L286:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L229:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L237:
	movq	(%r14), %rax
	movq	%rcx, -184(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-184(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L239
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L240:
	cmpl	$1, %eax
	jne	.L270
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L284:
	movq	(%r14), %rax
	movq	%rcx, -184(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	movq	-184(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L232
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L233:
	cmpl	$1, %eax
	jne	.L228
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L239:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L232:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L233
.L283:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18088:
	.size	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctions13NextWithValueEPS2_, .-_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctions13NextWithValueEPS2_
	.section	.text._ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLength13NextWithValueEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLength13NextWithValueEPS2_
	.type	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLength13NextWithValueEPS2_, @function
_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLength13NextWithValueEPS2_:
.LFB18091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	56(%rsi), %rdx
	movq	64(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	48(%rsi), %r15
	movq	24(%rdx), %rax
	subq	%rcx, %rax
	cmpq	%rax, %r15
	ja	.L353
	movq	(%rsi), %rax
	movq	16(%rdx), %r14
	leaq	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv(%rip), %rdx
	movq	%rsi, %rbx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L298
	leaq	16(%rsi), %rsi
.L299:
	movq	%r15, %rdx
	leaq	(%r14,%rcx), %rdi
	call	memcpy@PLT
	movq	40(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L354
	movq	56(%rbx), %r14
	movq	48(%rbx), %rax
	addq	64(%rbx), %rax
	leaq	(%rdx,%rax), %rcx
	cmpq	24(%r14), %rcx
	jbe	.L309
	xorl	%edx, %edx
	leaq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rbx
	movq	$29, -168(%rbp)
	leaq	-168(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-168(%rbp), %rdx
	movdqa	.LC12(%rip), %xmm0
	movabsq	$2336927755366786419, %rcx
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rcx, 16(%rax)
	movl	$1702132066, 24(%rax)
	movb	$115, 28(%rax)
	movups	%xmm0, (%rax)
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	movl	44(%r12), %eax
	movq	-160(%rbp), %rdx
	subl	$1, %eax
	cmpq	%rbx, %rdx
	je	.L355
	movq	-144(%rbp), %rcx
	leaq	-72(%rbp), %r15
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	movq	%rcx, -112(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movl	%eax, -96(%rbp)
	movq	%r15, -88(%rbp)
	cmpq	%r14, %rdx
	jne	.L312
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L354:
	leaq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rbx
	movq	$27, -168(%rbp)
	leaq	-168(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-168(%rbp), %rdx
	movdqa	.LC11(%rip), %xmm0
	movabsq	$2335244394374196256, %rcx
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movl	$12328, %edx
	movq	%rcx, 16(%rax)
	movw	%dx, 24(%rax)
	movb	$41, 26(%rax)
	movups	%xmm0, (%rax)
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	movl	44(%r12), %eax
	movq	-160(%rbp), %rdx
	subl	$1, %eax
	cmpq	%rbx, %rdx
	je	.L356
	movq	-144(%rbp), %rcx
	leaq	-72(%rbp), %r15
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	movq	%rcx, -112(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movl	%eax, -96(%rbp)
	movq	%r15, -88(%rbp)
	cmpq	%r14, %rdx
	jne	.L312
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L353:
	xorl	%edx, %edx
	leaq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rbx
	movq	$26, -168(%rbp)
	leaq	-168(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-168(%rbp), %rdx
	movabsq	$7286945621875123045, %rcx
	movdqa	.LC10(%rip), %xmm0
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rcx, 16(%rax)
	movl	$25710, %ecx
	movw	%cx, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	movl	44(%r12), %eax
	movq	-160(%rbp), %rdx
	subl	$1, %eax
	cmpq	%rbx, %rdx
	je	.L357
	movq	-144(%rbp), %rcx
	leaq	-72(%rbp), %r15
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	movq	%rcx, -112(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movl	%eax, -96(%rbp)
	movq	%r15, -88(%rbp)
	cmpq	%r14, %rdx
	je	.L290
.L312:
	movq	%rdx, -88(%rbp)
	movq	%rcx, -72(%rbp)
.L313:
	movq	%rsi, -80(%rbp)
	movq	%r14, -128(%rbp)
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L314
	movq	(%rdi), %rax
	leaq	-96(%rbp), %rsi
	call	*64(%rax)
	movq	(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L314
	movq	(%rdi), %rax
	call	*8(%rax)
.L314:
	movq	-88(%rbp), %rdi
	movq	$0, 0(%r13)
	cmpq	%r15, %rdi
	je	.L315
	call	_ZdlPv@PLT
.L315:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L316
	call	_ZdlPv@PLT
.L316:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L287
	call	_ZdlPv@PLT
.L287:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L358
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movq	%rax, %xmm0
	movq	%rdx, %xmm1
	movl	$56, %edi
	movl	44(%r12), %r12d
	punpcklqdq	%xmm1, %xmm0
	movq	72(%rbx), %rbx
	movaps	%xmm0, -192(%rbp)
	call	_Znwm@PLT
	movdqa	-192(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movq	%r14, 16(%rax)
	movq	%rbx, 40(%rax)
	movl	%r12d, 48(%rax)
	movq	%rax, 0(%r13)
	movups	%xmm0, 24(%rax)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L355:
	leaq	-72(%rbp), %r15
	movdqa	-144(%rbp), %xmm4
	movl	%eax, -96(%rbp)
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movb	$0, -144(%rbp)
	movq	$0, -152(%rbp)
	movq	%r15, -88(%rbp)
	movaps	%xmm4, -112(%rbp)
.L311:
	movdqa	-112(%rbp), %xmm5
	movups	%xmm5, -72(%rbp)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L356:
	leaq	-72(%rbp), %r15
	movdqa	-144(%rbp), %xmm6
	movl	%eax, -96(%rbp)
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movb	$0, -144(%rbp)
	movq	$0, -152(%rbp)
	movq	%r15, -88(%rbp)
	movaps	%xmm6, -112(%rbp)
.L302:
	movdqa	-112(%rbp), %xmm7
	movups	%xmm7, -72(%rbp)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L298:
	movq	%rcx, -192(%rbp)
	movq	%rsi, %rdi
	call	*%rax
	movq	-192(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L357:
	leaq	-72(%rbp), %r15
	movdqa	-144(%rbp), %xmm2
	movl	%eax, -96(%rbp)
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movb	$0, -144(%rbp)
	movq	$0, -152(%rbp)
	movq	%r15, -88(%rbp)
	movaps	%xmm2, -112(%rbp)
.L290:
	movdqa	-112(%rbp), %xmm3
	movups	%xmm3, -72(%rbp)
	jmp	.L313
.L358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18091:
	.size	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLength13NextWithValueEPS2_, .-_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLength13NextWithValueEPS2_
	.section	.rodata._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.str1.1,"aMS",@progbits,1
.LC13:
	.string	"0 < len"
.LC14:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag,"axG",@progbits,_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	.type	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag, @function
_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag:
.LFB17932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$392, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 56(%rdi)
	je	.L386
.L359:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	movq	$256, -328(%rbp)
	movq	%rdi, %r12
	movl	%esi, %ebx
	leaq	-320(%rbp), %rdi
	movl	$256, %esi
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	testl	%eax, %eax
	jle	.L388
	movq	-336(%rbp), %r14
	leaq	-400(%rbp), %r13
	movslq	%eax, %r15
	leaq	-416(%rbp), %rdi
	movq	%r13, -416(%rbp)
	testq	%r14, %r14
	je	.L389
	movq	%r15, -424(%rbp)
	cmpl	$15, %eax
	jg	.L390
	movq	%r13, %rdi
	cmpl	$1, %eax
	jne	.L364
	movzbl	(%r14), %eax
	movq	%r13, %rdx
	movb	%al, -400(%rbp)
	movl	$1, %eax
.L365:
	movq	%rax, -408(%rbp)
	leaq	-360(%rbp), %r14
	movb	$0, (%rdx,%rax)
	movq	-416(%rbp), %rax
	movl	%ebx, -384(%rbp)
	movq	%r14, -376(%rbp)
	cmpq	%r13, %rax
	je	.L391
	movq	-400(%rbp), %rcx
	movq	-408(%rbp), %rdx
	movq	%rax, -376(%rbp)
	movq	%r13, -416(%rbp)
	movq	48(%r12), %rdi
	movq	%rcx, -360(%rbp)
	movq	%rdx, -368(%rbp)
	movq	$0, -408(%rbp)
	movb	$0, -400(%rbp)
	movl	%ebx, 40(%r12)
	cmpq	%r14, %rax
	je	.L367
	leaq	64(%r12), %rsi
	cmpq	%rsi, %rdi
	je	.L392
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	64(%r12), %rsi
	movq	%rax, 48(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 56(%r12)
	testq	%rdi, %rdi
	je	.L373
	movq	%rdi, -376(%rbp)
	movq	%rsi, -360(%rbp)
.L371:
	movq	$0, -368(%rbp)
	movb	$0, (%rdi)
	movq	-376(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L374
	call	_ZdlPv@PLT
.L374:
	movq	-416(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L375
	call	_ZdlPv@PLT
.L375:
	movq	(%r12), %rax
	leaq	_ZN2v88internal4wasm7Decoder12onFirstErrorEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L359
	movq	%r12, %rdi
	call	*%rax
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	-424(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -416(%rbp)
	movq	%rax, %rdi
	movq	-424(%rbp), %rax
	movq	%rax, -400(%rbp)
.L364:
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-424(%rbp), %rax
	movq	-416(%rbp), %rdx
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L391:
	movq	-408(%rbp), %rdx
	movq	48(%r12), %rdi
	movl	%ebx, 40(%r12)
	movdqa	-400(%rbp), %xmm2
	movq	$0, -408(%rbp)
	movq	%rdx, -368(%rbp)
	movb	$0, -400(%rbp)
	movups	%xmm2, -360(%rbp)
.L367:
	testq	%rdx, %rdx
	je	.L369
	cmpq	$1, %rdx
	je	.L393
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-368(%rbp), %rdx
	movq	48(%r12), %rdi
.L369:
	movq	%rdx, 56(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-376(%rbp), %rdi
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L392:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, 48(%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 56(%r12)
.L373:
	movq	%r14, -376(%rbp)
	leaq	-360(%rbp), %r14
	movq	%r14, %rdi
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L393:
	movzbl	-360(%rbp), %eax
	movb	%al, (%rdi)
	movq	-368(%rbp), %rdx
	movq	48(%r12), %rdi
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L388:
	leaq	.LC13(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L387:
	call	__stack_chk_fail@PLT
.L389:
	leaq	.LC15(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE17932:
	.size	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag, .-_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEjPKcz,"axG",@progbits,_ZN2v88internal4wasm7Decoder6errorfEjPKcz,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.type	_ZN2v88internal4wasm7Decoder6errorfEjPKcz, @function
_ZN2v88internal4wasm7Decoder6errorfEjPKcz:
.LFB17910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L395
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L395:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	leaq	-208(%rbp), %rcx
	movl	$24, -208(%rbp)
	movq	%rax, -200(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L398
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L398:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17910:
	.size	_ZN2v88internal4wasm7Decoder6errorfEjPKcz, .-_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.section	.rodata._ZN2v88internal4wasm7Decoder5errorEPKhPKc.str1.1,"aMS",@progbits,1
.LC16:
	.string	"%s"
	.section	.text._ZN2v88internal4wasm7Decoder5errorEPKhPKc,"axG",@progbits,_ZN2v88internal4wasm7Decoder5errorEPKhPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.type	_ZN2v88internal4wasm7Decoder5errorEPKhPKc, @function
_ZN2v88internal4wasm7Decoder5errorEPKhPKc:
.LFB17908:
	.cfi_startproc
	endbr64
	movq	%rdx, %rcx
	subq	8(%rdi), %rsi
	leaq	.LC16(%rip), %rdx
	xorl	%eax, %eax
	addl	32(%rdi), %esi
	jmp	_ZN2v88internal4wasm7Decoder6errorfEjPKcz
	.cfi_endproc
.LFE17908:
	.size	_ZN2v88internal4wasm7Decoder5errorEPKhPKc, .-_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	.section	.text._ZN2v88internal4wasm7Decoder6errorfEPKhPKcz,"axG",@progbits,_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	.type	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz, @function
_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz:
.LFB17911:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$208, %rsp
	movq	%rcx, -152(%rbp)
	movq	%r8, -144(%rbp)
	movq	%r9, -136(%rbp)
	testb	%al, %al
	je	.L401
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	movaps	%xmm4, -64(%rbp)
	movaps	%xmm5, -48(%rbp)
	movaps	%xmm6, -32(%rbp)
	movaps	%xmm7, -16(%rbp)
.L401:
	movq	%fs:40, %rax
	movq	%rax, -184(%rbp)
	xorl	%eax, %eax
	leaq	16(%rbp), %rax
	subq	8(%rdi), %rsi
	leaq	-208(%rbp), %rcx
	movq	%rax, -200(%rbp)
	addl	32(%rdi), %esi
	leaq	-176(%rbp), %rax
	movq	%rax, -192(%rbp)
	movl	$24, -208(%rbp)
	movl	$48, -204(%rbp)
	call	_ZN2v88internal4wasm7Decoder7verrorfEjPKcP13__va_list_tag
	movq	-184(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L404
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L404:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17911:
	.size	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz, .-_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	.section	.rodata._ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt329ReadBytesEPS2_NS0_6VectorIKhEE.str1.1,"aMS",@progbits,1
.LC17:
	.string	"expected %s"
.LC18:
	.string	"extra bits in varint"
	.section	.text._ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt329ReadBytesEPS2_NS0_6VectorIKhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt329ReadBytesEPS2_NS0_6VectorIKhEE
	.type	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt329ReadBytesEPS2_NS0_6VectorIKhEE, @function
_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt329ReadBytesEPS2_NS0_6VectorIKhEE:
.LFB18074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%rcx, -152(%rbp)
	leaq	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv(%rip), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L406
	leaq	16(%rdi), %r13
	movl	$5, %r12d
.L407:
	movq	8(%rbx), %rdi
	movq	-152(%rbp), %rax
	subq	%rdi, %r12
	movq	%rax, %r15
	cmpq	%rax, %r12
	cmovbe	%r12, %r15
	addq	%r13, %rdi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	8(%rbx), %rax
	movl	44(%r14), %edx
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r10
	leaq	-80(%rbp), %r11
	movq	%r10, -144(%rbp)
	movq	32(%rbx), %r9
	leaq	-144(%rbp), %rdi
	subl	%eax, %edx
	addq	%r15, %rax
	movq	%r13, -136(%rbp)
	cltq
	movq	%r13, -128(%rbp)
	addq	%r13, %rax
	movl	%edx, -112(%rbp)
	movq	%rax, -120(%rbp)
	movl	$0, -104(%rbp)
	movq	%r11, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	cmpq	%rax, %r13
	jb	.L440
	movq	%r11, -160(%rbp)
	movq	%r9, %rcx
	leaq	.LC17(%rip), %rdx
	movq	%r13, %rsi
.L439:
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-128(%rbp), %rax
	movq	-88(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	-160(%rbp), %r11
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r10
	subq	%r13, %rax
.L420:
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	testq	%rdx, %rdx
	je	.L421
	cmpq	-152(%rbp), %r12
	jbe	.L441
.L422:
	addq	%r15, 8(%rbx)
.L424:
	movq	-96(%rbp), %rdi
	movq	%r10, -144(%rbp)
	cmpq	%r11, %rdi
	je	.L405
	call	_ZdlPv@PLT
.L405:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L442
	addq	$152, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L440:
	.cfi_restore_state
	movzbl	0(%r13), %ecx
	leaq	1(%r13), %rsi
	movl	%ecx, %edx
	movl	%ecx, %r8d
	andl	$127, %edx
	andl	$127, %r8d
	testb	%cl, %cl
	js	.L443
	movzbl	%dl, %edx
	movq	%rsi, -128(%rbp)
	movl	$1, %eax
	movq	%rdx, %xmm0
	movhps	.LC19(%rip), %xmm0
	movups	%xmm0, 40(%rbx)
.L421:
	subq	8(%rbx), %rax
	leaq	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv(%rip), %rcx
	movl	$5, %edx
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L444
.L425:
	movq	%rdx, 8(%rbx)
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L443:
	cmpq	%rsi, %rax
	jbe	.L410
	movzbl	1(%r13), %edx
	leaq	2(%r13), %rsi
	movl	%edx, %ecx
	sall	$7, %ecx
	andl	$16256, %ecx
	orl	%r8d, %ecx
	testb	%dl, %dl
	js	.L445
	movl	$2, %eax
	movd	%ecx, %xmm0
	movq	%rsi, -128(%rbp)
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 40(%rbx)
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%rdx, -160(%rbp)
	call	*%rax
	movq	-160(%rbp), %rsi
	movq	%rax, %r13
	movq	%rdx, %r12
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L445:
	cmpq	%rsi, %rax
	ja	.L446
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%rsi, -128(%rbp)
	movq	%r9, %rcx
	leaq	.LC17(%rip), %rdx
	movq	%r11, -160(%rbp)
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L444:
	movq	%r11, -152(%rbp)
	movq	%rbx, %rdi
	call	*%rax
	movq	-152(%rbp), %r11
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r10
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L441:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L422
	movq	(%rdi), %rax
	movq	%r11, -152(%rbp)
	leaq	-104(%rbp), %rsi
	call	*64(%rax)
	movq	(%r14), %rdi
	movq	-152(%rbp), %r11
	movq	$0, (%r14)
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r10
	testq	%rdi, %rdi
	je	.L422
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-152(%rbp), %r11
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r10
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L446:
	movzbl	2(%r13), %r8d
	leaq	3(%r13), %rsi
	movl	%r8d, %edx
	sall	$14, %edx
	andl	$2080768, %edx
	orl	%edx, %ecx
	testb	%r8b, %r8b
	js	.L447
	movl	$3, %eax
	movd	%ecx, %xmm0
	movq	%rsi, -128(%rbp)
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 40(%rbx)
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L447:
	cmpq	%rsi, %rax
	jbe	.L410
	movzbl	3(%r13), %esi
	movl	%esi, %edx
	sall	$21, %edx
	andl	$266338304, %edx
	orl	%edx, %ecx
	leaq	4(%r13), %rdx
	movq	%rdx, -160(%rbp)
	testb	%sil, %sil
	js	.L448
	movl	$4, %eax
	movd	%ecx, %xmm0
	movq	%rdx, -128(%rbp)
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 40(%rbx)
	jmp	.L421
.L448:
	cmpq	%rdx, %rax
	jbe	.L416
	movzbl	4(%r13), %r8d
	leaq	5(%r13), %rax
	movq	%rax, -128(%rbp)
	testb	%r8b, %r8b
	js	.L417
	movl	%r8d, %eax
	sall	$28, %eax
	orl	%eax, %ecx
.L418:
	andl	$240, %r8d
	jne	.L419
	movq	-128(%rbp), %rax
	movq	-88(%rbp), %rdx
	movd	%ecx, %xmm0
	subq	%r13, %rax
	jmp	.L420
.L416:
	movq	%rdx, -128(%rbp)
	xorl	%r8d, %r8d
.L417:
	movq	-160(%rbp), %rsi
	movq	%r9, %rcx
	leaq	.LC17(%rip), %rdx
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	movb	%r8b, -169(%rbp)
	movq	%rdi, -168(%rbp)
	call	_ZN2v88internal4wasm7Decoder6errorfEPKhPKcz
	movq	-184(%rbp), %r11
	movq	-168(%rbp), %rdi
	xorl	%ecx, %ecx
	movzbl	-169(%rbp), %r8d
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r10
	jmp	.L418
.L419:
	movq	-160(%rbp), %rsi
	leaq	.LC18(%rip), %rdx
	movq	%r11, -168(%rbp)
	call	_ZN2v88internal4wasm7Decoder5errorEPKhPKc
	movq	-128(%rbp), %rax
	movq	-88(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	-168(%rbp), %r11
	leaq	16+_ZTVN2v88internal4wasm7DecoderE(%rip), %r10
	subq	%r13, %rax
	jmp	.L420
.L442:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18074:
	.size	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt329ReadBytesEPS2_NS0_6VectorIKhEE, .-_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt329ReadBytesEPS2_NS0_6VectorIKhEE
	.section	.text._ZN2v88internal4wasm16StreamingDecoder5AbortEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder5AbortEv
	.type	_ZN2v88internal4wasm16StreamingDecoder5AbortEv, @function
_ZN2v88internal4wasm16StreamingDecoder5AbortEv:
.LFB18001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L449
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	(%rbx), %rdi
	movq	$0, (%rbx)
	testq	%rdi, %rdi
	je	.L449
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18001:
	.size	_ZN2v88internal4wasm16StreamingDecoder5AbortEv, .-_ZN2v88internal4wasm16StreamingDecoder5AbortEv
	.section	.text._ZN2v88internal4wasm16StreamingDecoder25SetModuleCompiledCallbackESt8functionIFvRKSt10shared_ptrINS1_12NativeModuleEEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder25SetModuleCompiledCallbackESt8functionIFvRKSt10shared_ptrINS1_12NativeModuleEEEE
	.type	_ZN2v88internal4wasm16StreamingDecoder25SetModuleCompiledCallbackESt8functionIFvRKSt10shared_ptrINS1_12NativeModuleEEEE, @function
_ZN2v88internal4wasm16StreamingDecoder25SetModuleCompiledCallbackESt8functionIFvRKSt10shared_ptrINS1_12NativeModuleEEEE:
.LFB18002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	-40(%rbp), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	movq	$0, -48(%rbp)
	testq	%rax, %rax
	je	.L459
	movq	%rsi, %r12
	movl	$2, %edx
	leaq	-64(%rbp), %rdi
	call	*%rax
	movq	24(%r12), %rdx
	movq	16(%r12), %rax
.L459:
	movdqa	-64(%rbp), %xmm0
	movdqu	56(%rbx), %xmm1
	movq	%rdx, %xmm2
	movq	72(%rbx), %rcx
	movq	80(%rbx), %rsi
	movups	%xmm0, 56(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, -48(%rbp)
	movq	%rsi, -40(%rbp)
	movaps	%xmm1, -64(%rbp)
	movups	%xmm0, 72(%rbx)
	testq	%rcx, %rcx
	je	.L457
	leaq	-64(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rcx
.L457:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L466
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L466:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18002:
	.size	_ZN2v88internal4wasm16StreamingDecoder25SetModuleCompiledCallbackESt8functionIFvRKSt10shared_ptrINS1_12NativeModuleEEEE, .-_ZN2v88internal4wasm16StreamingDecoder25SetModuleCompiledCallbackESt8functionIFvRKSt10shared_ptrINS1_12NativeModuleEEEE
	.section	.text._ZN2v88internal4wasm16StreamingDecoder22SetCompiledModuleBytesENS0_6VectorIKhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder22SetCompiledModuleBytesENS0_6VectorIKhEE
	.type	_ZN2v88internal4wasm16StreamingDecoder22SetCompiledModuleBytesENS0_6VectorIKhEE, @function
_ZN2v88internal4wasm16StreamingDecoder22SetCompiledModuleBytesENS0_6VectorIKhEE:
.LFB18003:
	.cfi_startproc
	endbr64
	movq	%rsi, 112(%rdi)
	movl	$1, %eax
	movq	%rdx, 120(%rdi)
	ret
	.cfi_endproc
.LFE18003:
	.size	_ZN2v88internal4wasm16StreamingDecoder22SetCompiledModuleBytesENS0_6VectorIKhEE, .-_ZN2v88internal4wasm16StreamingDecoder22SetCompiledModuleBytesENS0_6VectorIKhEE
	.section	.text._ZN2v88internal4wasm16StreamingDecoder25NotifyNativeModuleCreatedERKSt10shared_ptrINS1_12NativeModuleEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder25NotifyNativeModuleCreatedERKSt10shared_ptrINS1_12NativeModuleEE
	.type	_ZN2v88internal4wasm16StreamingDecoder25NotifyNativeModuleCreatedERKSt10shared_ptrINS1_12NativeModuleEE, @function
_ZN2v88internal4wasm16StreamingDecoder25NotifyNativeModuleCreatedERKSt10shared_ptrINS1_12NativeModuleEE:
.LFB18019:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	72(%rdi), %rax
	testq	%rax, %rax
	je	.L468
	movq	(%rsi), %rdx
	movq	8(%rsi), %rcx
	movq	%rdi, %rbx
	movq	520(%rdx), %r13
	testq	%rcx, %rcx
	je	.L471
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L472
	lock addl	$1, 12(%rcx)
	movq	72(%rdi), %rax
.L471:
	movq	-136(%rbp), %rdi
	movq	80(%rbx), %rsi
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movdqa	-160(%rbp), %xmm2
	punpcklqdq	%xmm3, %xmm0
	movdqu	56(%rbx), %xmm1
	movq	$0, 72(%rbx)
	movq	%rdi, 80(%rbx)
	movdqa	-80(%rbp), %xmm4
	movq	%rsi, %xmm5
	movl	$48, %edi
	movq	-56(%rbp), %rdx
	movaps	%xmm0, -96(%rbp)
	movq	%rax, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm2, 56(%rbx)
	movq	$0, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	$0, -112(%rbp)
	movaps	%xmm4, -160(%rbp)
	movaps	%xmm1, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_Znwm@PLT
	movdqa	-96(%rbp), %xmm6
	movq	%rax, %r12
	movq	-88(%rbp), %rax
	movups	%xmm6, (%r12)
	testq	%rax, %rax
	je	.L473
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L474
	lock addl	$1, 12(%rax)
.L473:
	movq	$0, 32(%r12)
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L475
	leaq	-80(%rbp), %rsi
	leaq	16(%r12), %rdi
	movl	$2, %edx
	call	*%rax
	movdqa	-64(%rbp), %xmm1
	movups	%xmm1, 32(%r12)
.L475:
	leaq	_ZNSt17_Function_handlerIFvN2v88internal4wasm16CompilationEventEENS2_12_GLOBAL__N_123TopTierCompiledCallbackEE9_M_invokeERKSt9_Any_dataOS3_(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIN2v88internal4wasm12_GLOBAL__N_123TopTierCompiledCallbackEE10_M_managerERSt9_Any_dataRKS7_St18_Manager_operation(%rip), %rcx
	movq	%r12, -128(%rbp)
	movq	%r13, %rdi
	movq	%rax, %xmm7
	movq	%rcx, %xmm0
	leaq	-128(%rbp), %r12
	punpcklqdq	%xmm7, %xmm0
	movq	%r12, %rsi
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88internal4wasm16CompilationState11AddCallbackESt8functionIFvNS1_16CompilationEventEEE@PLT
	movq	-112(%rbp), %rax
	testq	%rax, %rax
	je	.L476
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L476:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L477
	leaq	-80(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L477:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L479
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L480
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L481:
	cmpl	$1, %eax
	jne	.L479
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L479:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L483
	leaq	-160(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L483:
	movq	72(%rbx), %rax
	testq	%rax, %rax
	je	.L468
	leaq	56(%rbx), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
	pxor	%xmm0, %xmm0
	movups	%xmm0, 72(%rbx)
.L468:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L508
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	addl	$1, 12(%rcx)
	movq	72(%rdi), %rax
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L474:
	addl	$1, 12(%rax)
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L480:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L481
.L508:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18019:
	.size	_ZN2v88internal4wasm16StreamingDecoder25NotifyNativeModuleCreatedERKSt10shared_ptrINS1_12NativeModuleEE, .-_ZN2v88internal4wasm16StreamingDecoder25NotifyNativeModuleCreatedERKSt10shared_ptrINS1_12NativeModuleEE
	.section	.text._ZN2v88internal4wasm16StreamingDecoderC2ESt10unique_ptrINS1_18StreamingProcessorESt14default_deleteIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoderC2ESt10unique_ptrINS1_18StreamingProcessorESt14default_deleteIS4_EE
	.type	_ZN2v88internal4wasm16StreamingDecoderC2ESt10unique_ptrINS1_18StreamingProcessorESt14default_deleteIS4_EE, @function
_ZN2v88internal4wasm16StreamingDecoderC2ESt10unique_ptrINS1_18StreamingProcessorESt14default_deleteIS4_EE:
.LFB18118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	movq	%rax, (%rdi)
	movl	$24, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderE(%rip), %rdx
	movq	$0, 16(%rbx)
	movq	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	%rdx, (%rax)
	movq	%rax, 8(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	movb	$0, 40(%rbx)
	movl	$0, 44(%rbx)
	movq	$0, 48(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 120(%rbx)
	movups	%xmm0, 88(%rbx)
	movups	%xmm0, 104(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18118:
	.size	_ZN2v88internal4wasm16StreamingDecoderC2ESt10unique_ptrINS1_18StreamingProcessorESt14default_deleteIS4_EE, .-_ZN2v88internal4wasm16StreamingDecoderC2ESt10unique_ptrINS1_18StreamingProcessorESt14default_deleteIS4_EE
	.globl	_ZN2v88internal4wasm16StreamingDecoderC1ESt10unique_ptrINS1_18StreamingProcessorESt14default_deleteIS4_EE
	.set	_ZN2v88internal4wasm16StreamingDecoderC1ESt10unique_ptrINS1_18StreamingProcessorESt14default_deleteIS4_EE,_ZN2v88internal4wasm16StreamingDecoderC2ESt10unique_ptrINS1_18StreamingProcessorESt14default_deleteIS4_EE
	.section	.rodata._ZNSt6vectorISt10shared_ptrIN2v88internal4wasm16StreamingDecoder13SectionBufferEESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_.str1.1,"aMS",@progbits,1
.LC20:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorISt10shared_ptrIN2v88internal4wasm16StreamingDecoder13SectionBufferEESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorISt10shared_ptrIN2v88internal4wasm16StreamingDecoder13SectionBufferEESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10shared_ptrIN2v88internal4wasm16StreamingDecoder13SectionBufferEESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.type	_ZNSt6vectorISt10shared_ptrIN2v88internal4wasm16StreamingDecoder13SectionBufferEESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, @function
_ZNSt6vectorISt10shared_ptrIN2v88internal4wasm16StreamingDecoder13SectionBufferEESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_:
.LFB21588:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r14
	movq	(%rdi), %r12
	movq	%r14, %rax
	subq	%r12, %rax
	sarq	$4, %rax
	cmpq	%rcx, %rax
	je	.L541
	movq	%rsi, %rbx
	movq	%rdi, %r13
	subq	%r12, %rsi
	testq	%rax, %rax
	je	.L531
	movabsq	$9223372036854775792, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L542
.L513:
	movq	%r15, %rdi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdx
	addq	%rax, %r15
	movq	%rax, -56(%rbp)
	addq	$16, %rax
	movq	%r15, -64(%rbp)
.L530:
	movdqu	(%rdx), %xmm4
	movq	-56(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rdx)
	movups	%xmm4, (%rdi,%rsi)
	cmpq	%r12, %rbx
	je	.L515
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rdi, %rdx
	movq	%r12, %r15
	jne	.L516
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L520:
	movq	(%rdi), %rcx
	movq	%rdx, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	*16(%rcx)
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %rdx
	movl	12(%rdi), %ecx
	leal	-1(%rcx), %esi
	cmpl	$1, %ecx
	movl	%esi, 12(%rdi)
	jne	.L519
	movq	(%rdi), %rcx
	movq	%rdx, -72(%rbp)
	call	*24(%rcx)
	movq	-72(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L519:
	addq	$16, %r15
	addq	$16, %rdx
	cmpq	%r15, %rbx
	je	.L521
.L522:
	movq	(%r15), %rcx
	movq	$0, 8(%rdx)
	pxor	%xmm1, %xmm1
	movq	%rcx, (%rdx)
	movq	8(%r15), %rcx
	movq	%rcx, 8(%rdx)
	movups	%xmm1, (%r15)
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L519
	movl	8(%rdi), %ecx
	leal	-1(%rcx), %esi
	movl	%esi, 8(%rdi)
	cmpl	$1, %ecx
	je	.L520
	addq	$16, %r15
	addq	$16, %rdx
	cmpq	%r15, %rbx
	jne	.L522
	.p2align 4,,10
	.p2align 3
.L521:
	movq	%rbx, %rax
	subq	%r12, %rax
	addq	-56(%rbp), %rax
	addq	$16, %rax
.L515:
	cmpq	%r14, %rbx
	je	.L527
	subq	%rbx, %r14
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	leaq	-16(%r14), %rdi
	movq	%rdi, %rsi
	shrq	$4, %rsi
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L528:
	movdqu	(%rbx,%rdx), %xmm3
	addq	$1, %rcx
	movups	%xmm3, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rcx
	jb	.L528
	leaq	16(%rax,%rdi), %rax
.L527:
	testq	%r12, %r12
	je	.L529
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rax
.L529:
	movq	-56(%rbp), %xmm0
	movq	%rax, %xmm5
	movq	-64(%rbp), %rax
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	movq	(%rdi), %rcx
	movq	%rdi, -72(%rbp)
	movq	%rdx, -80(%rbp)
	call	*16(%rcx)
	movq	-72(%rbp), %rdi
	movl	$-1, %ecx
	lock xaddl	%ecx, 12(%rdi)
	movq	-80(%rbp), %rdx
	cmpl	$1, %ecx
	jne	.L524
	movq	(%rdi), %rcx
	movq	%rdx, -72(%rbp)
	call	*24(%rcx)
	movq	-72(%rbp), %rdx
	.p2align 4,,10
	.p2align 3
.L524:
	addq	$16, %r15
	addq	$16, %rdx
	cmpq	%r15, %rbx
	je	.L521
.L516:
	movq	(%r15), %rcx
	movq	$0, 8(%rdx)
	pxor	%xmm2, %xmm2
	movq	%rcx, (%rdx)
	movq	8(%r15), %rcx
	movq	%rcx, 8(%rdx)
	movups	%xmm2, (%r15)
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L524
	lock subl	$1, 8(%rdi)
	jne	.L524
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L542:
	testq	%rdi, %rdi
	jne	.L514
	movq	$0, -64(%rbp)
	movl	$16, %eax
	movq	$0, -56(%rbp)
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L531:
	movl	$16, %r15d
	jmp	.L513
.L541:
	leaq	.LC20(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L514:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$4, %r15
	jmp	.L513
	.cfi_endproc
.LFE21588:
	.size	_ZNSt6vectorISt10shared_ptrIN2v88internal4wasm16StreamingDecoder13SectionBufferEESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_, .-_ZNSt6vectorISt10shared_ptrIN2v88internal4wasm16StreamingDecoder13SectionBufferEESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	.section	.text._ZN2v88internal4wasm16StreamingDecoder15CreateNewBufferEjhmNS0_6VectorIKhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder15CreateNewBufferEjhmNS0_6VectorIKhEE
	.type	_ZN2v88internal4wasm16StreamingDecoder15CreateNewBufferEjhmNS0_6VectorIKhEE, @function
_ZN2v88internal4wasm16StreamingDecoder15CreateNewBufferEjhmNS0_6VectorIKhEE:
.LFB18120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$56, %edi
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	%esi, -104(%rbp)
	movl	%edx, -92(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	call	_Znwm@PLT
	movl	-104(%rbp), %esi
	leal	1(%r15), %edx
	movq	%rax, %rbx
	movslq	%edx, %rdx
	movabsq	$4294967297, %rax
	movq	%rax, 8(%rbx)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	16(%rbx), %rax
	movq	%rax, -88(%rbp)
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder13SectionBufferE(%rip), %rax
	movq	%rax, 16(%rbx)
	movl	%esi, 24(%rbx)
	addq	%rdx, %r13
	jne	.L545
	movq	$0, 32(%rbx)
	movl	$1, %edi
	xorl	%eax, %eax
	movq	$0, 40(%rbx)
.L546:
	movzbl	-92(%rbp), %ecx
	movq	%rdx, 48(%rbx)
	movq	%r14, %rsi
	movslq	%r15d, %rdx
	movb	%cl, (%rax)
	call	memcpy@PLT
	movq	-88(%rbp), %xmm0
	movq	%rbx, %xmm1
	movq	24(%r12), %rsi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	cmpq	32(%r12), %rsi
	je	.L547
	movq	-80(%rbp), %rax
	movq	$0, 8(%rsi)
	pxor	%xmm0, %xmm0
	movq	%rax, (%rsi)
	movq	-72(%rbp), %rax
	movq	%rax, 8(%rsi)
	addq	$16, 24(%r12)
	movaps	%xmm0, -80(%rbp)
.L548:
	movq	-72(%rbp), %r13
	testq	%r13, %r13
	je	.L550
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L551
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L558
	.p2align 4,,10
	.p2align 3
.L550:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	24(%r12), %rax
	movq	-16(%rax), %rax
	jne	.L559
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L551:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L550
.L558:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L554
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L555:
	cmpl	$1, %eax
	jne	.L550
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L545:
	movq	%r13, %rdi
	movq	%rdx, -104(%rbp)
	call	_Znam@PLT
	movq	%r13, 40(%rbx)
	movq	-104(%rbp), %rdx
	movq	%rax, 32(%rbx)
	leaq	1(%rax), %rdi
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L547:
	leaq	-80(%rbp), %rdx
	leaq	16(%r12), %rdi
	call	_ZNSt6vectorISt10shared_ptrIN2v88internal4wasm16StreamingDecoder13SectionBufferEESaIS6_EE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S8_EEDpOT_
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L554:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L555
.L559:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18120:
	.size	_ZN2v88internal4wasm16StreamingDecoder15CreateNewBufferEjhmNS0_6VectorIKhEE, .-_ZN2v88internal4wasm16StreamingDecoder15CreateNewBufferEjhmNS0_6VectorIKhEE
	.section	.rodata._ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLength13NextWithValueEPS2_.str1.1,"aMS",@progbits,1
.LC21:
	.string	"functions count"
	.section	.text._ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLength13NextWithValueEPS2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLength13NextWithValueEPS2_
	.type	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLength13NextWithValueEPS2_, @function
_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLength13NextWithValueEPS2_:
.LFB18082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	leaq	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv(%rip), %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L561
	leaq	16(%rsi), %r8
.L562:
	movq	40(%rbx), %rcx
	movzbl	56(%rbx), %edx
	movq	%r12, %rdi
	movl	60(%rbx), %esi
	movslq	48(%rbx), %r9
	call	_ZN2v88internal4wasm16StreamingDecoder15CreateNewBufferEjhmNS0_6VectorIKhEE
	cmpq	$0, 40(%rbx)
	movq	%rax, %r14
	jne	.L563
	cmpb	$10, 56(%rbx)
	je	.L617
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L576
	movq	32(%rax), %r8
	movq	24(%r14), %rcx
	movq	16(%rax), %rax
	movq	(%rdi), %r9
	subq	%r8, %rcx
	leaq	(%rax,%r8), %rdx
	movsbl	(%rax), %esi
	addl	8(%r14), %r8d
	movslq	%ecx, %rcx
	call	*24(%r9)
	testb	%al, %al
	jne	.L575
	movq	(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L576
	movq	(%rdi), %rax
	call	*8(%rax)
.L575:
	cmpq	$0, (%r12)
	je	.L576
	movl	44(%r12), %ebx
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder15DecodeSectionIDE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movb	$0, 16(%rax)
	movl	%ebx, 20(%rax)
	movq	%rax, 0(%r13)
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L563:
	cmpb	$10, 56(%rbx)
	jne	.L577
	cmpb	$0, 40(%r12)
	je	.L578
	xorl	%edx, %edx
	leaq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rbx
	movq	$33, -168(%rbp)
	leaq	-168(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-168(%rbp), %rdx
	movdqa	.LC22(%rip), %xmm0
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC23(%rip), %xmm0
	movb	$101, 32(%rax)
	movups	%xmm0, 16(%rax)
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	movl	44(%r12), %eax
	movq	-160(%rbp), %rdx
	subl	$1, %eax
	cmpq	%rbx, %rdx
	je	.L618
	movq	-144(%rbp), %rcx
	leaq	-72(%rbp), %r15
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	movq	%rcx, -112(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movl	%eax, -96(%rbp)
	movq	%r15, -88(%rbp)
	cmpq	%r14, %rdx
	jne	.L581
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L577:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	%rcx, (%rax)
	movq	%r14, 16(%rax)
	movq	%rax, 0(%r13)
.L560:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L619
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L617:
	.cfi_restore_state
	xorl	%edx, %edx
	leaq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rbx
	movq	$31, -168(%rbp)
	leaq	-168(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-168(%rbp), %rdx
	movdqa	.LC22(%rip), %xmm0
	movabsq	$7311138144931573614, %rcx
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movl	$8293, %edx
	movq	%rcx, 16(%rax)
	movl	$2053731104, 24(%rax)
	movw	%dx, 28(%rax)
	movb	$48, 30(%rax)
	movups	%xmm0, (%rax)
	movq	-168(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	movl	44(%r12), %eax
	movq	-160(%rbp), %rdx
	subl	$1, %eax
	cmpq	%rbx, %rdx
	je	.L620
	movq	-144(%rbp), %rcx
	leaq	-72(%rbp), %r15
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	movq	%rcx, -112(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movl	%eax, -96(%rbp)
	movq	%r15, -88(%rbp)
	cmpq	%r14, %rdx
	je	.L566
.L581:
	movq	%rdx, -88(%rbp)
	movq	%rcx, -72(%rbp)
.L582:
	movq	(%r12), %rdi
	movq	%rsi, -80(%rbp)
	movq	%r14, -128(%rbp)
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
	testq	%rdi, %rdi
	je	.L583
	movq	(%rdi), %rax
	leaq	-96(%rbp), %rsi
	call	*64(%rax)
	movq	(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L583
	movq	(%rdi), %rax
	call	*8(%rax)
.L583:
	movq	-88(%rbp), %rdi
	movq	$0, 0(%r13)
	cmpq	%r15, %rdi
	je	.L584
	call	_ZdlPv@PLT
.L584:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L585
	call	_ZdlPv@PLT
.L585:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L560
	call	_ZdlPv@PLT
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L578:
	movb	$1, 40(%r12)
	movl	$64, %edi
	call	_Znwm@PLT
	leaq	.LC21(%rip), %rcx
	pxor	%xmm0, %xmm0
	movq	%rcx, 32(%rax)
	leaq	16+_ZTVN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsE(%rip), %rcx
	movq	$0, 8(%rax)
	movq	$1000000, 24(%rax)
	movq	%rcx, (%rax)
	movq	%r14, 56(%rax)
	movups	%xmm0, 40(%rax)
	movq	%rax, 0(%r13)
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L576:
	movq	$0, 0(%r13)
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L561:
	movq	%rsi, %rdi
	call	*%rax
	movq	%rax, %r8
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L620:
	leaq	-72(%rbp), %r15
	movdqa	-144(%rbp), %xmm1
	movl	%eax, -96(%rbp)
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movb	$0, -144(%rbp)
	movq	$0, -152(%rbp)
	movq	%r15, -88(%rbp)
	movaps	%xmm1, -112(%rbp)
.L566:
	movdqa	-112(%rbp), %xmm2
	movups	%xmm2, -72(%rbp)
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L618:
	leaq	-72(%rbp), %r15
	movdqa	-144(%rbp), %xmm3
	movl	%eax, -96(%rbp)
	leaq	-112(%rbp), %r14
	movq	-152(%rbp), %rsi
	movb	$0, -144(%rbp)
	movq	$0, -152(%rbp)
	movq	%r15, -88(%rbp)
	movaps	%xmm3, -112(%rbp)
.L580:
	movdqa	-112(%rbp), %xmm4
	movups	%xmm4, -72(%rbp)
	jmp	.L582
.L619:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18082:
	.size	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLength13NextWithValueEPS2_, .-_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLength13NextWithValueEPS2_
	.section	.rodata._ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag.str1.1,"aMS",@progbits,1
.LC24:
	.string	"vector::_M_range_insert"
	.section	.text._ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.type	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag, @function
_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag:
.LFB22174:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	je	.L658
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	subq	%rdx, %r14
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rdi
	movq	16(%rbx), %rax
	subq	%rdi, %rax
	cmpq	%r14, %rax
	jb	.L624
	movq	%rdi, %r15
	subq	%rsi, %r15
	cmpq	%r15, %r14
	jnb	.L625
	movq	%rdi, %r15
	movq	%r14, %rdx
	subq	%r14, %r15
	movq	%r15, %rsi
	call	memmove@PLT
	addq	%r14, 8(%rbx)
	movq	%rax, %rdi
	subq	%r13, %r15
	jne	.L662
.L626:
	movq	%r14, %rdx
.L661:
	addq	$40, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L624:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rdx
	movq	(%rbx), %r8
	movq	%rdx, %rax
	subq	%r8, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %r14
	ja	.L663
	cmpq	%rdi, %r14
	movq	%rdi, %rax
	cmovnb	%r14, %rax
	addq	%rax, %rdi
	movq	%rdi, %r15
	jc	.L641
	testq	%rdi, %rdi
	js	.L641
	jne	.L632
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L638:
	movq	%r13, %rdx
	subq	%r8, %rdx
	leaq	(%r14,%rdx), %r9
	leaq	(%rcx,%rdx), %r10
	addq	%rcx, %r9
	testq	%rdx, %rdx
	jne	.L664
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	subq	%r13, %rdx
	leaq	(%r9,%rdx), %r12
	jne	.L634
.L636:
	testq	%r8, %r8
	jne	.L635
.L637:
	movq	%rcx, %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
.L621:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	.cfi_restore_state
	leaq	(%rdx,%r15), %rsi
	subq	%rsi, %rcx
	jne	.L665
.L627:
	subq	%r15, %r14
	addq	%r14, %rdi
	movq	%rdi, 8(%rbx)
	testq	%r15, %r15
	je	.L621
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	addq	%r15, 8(%rbx)
	movq	%r15, %rdx
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L658:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdx, %r15
.L632:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r8
	movq	%rax, %rcx
	addq	%rax, %r15
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L662:
	subq	%r15, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L665:
	movq	%rcx, %rdx
	call	memmove@PLT
	movq	8(%rbx), %rdi
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L664:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%r10, -80(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r10
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	subq	%r13, %rdx
	leaq	(%r9,%rdx), %r12
	jne	.L634
.L635:
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L634:
	movq	%r13, %rsi
	movq	%r9, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	jmp	.L636
.L663:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22174:
	.size	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag, .-_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	.section	.text._ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE
	.type	_ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE, @function
_ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE:
.LFB17994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	120(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	jne	.L667
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L668
	leaq	-64(%rbp), %rax
	movq	%rax, -72(%rbp)
	testq	%rdx, %rdx
	jne	.L669
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L672:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L668
.L688:
	cmpq	%rbx, %r12
	jbe	.L670
.L669:
	movq	8(%r15), %rdi
	movq	%r12, %rcx
	leaq	(%r14,%rbx), %rdx
	movq	%r15, %rsi
	subq	%rbx, %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%r15), %rdi
	addl	%eax, 44(%r15)
	addq	%rax, %rbx
	movq	(%rdi), %rax
	movq	8(%rdi), %r13
	call	*32(%rax)
	cmpq	%rdx, %r13
	jne	.L672
	movq	8(%r15), %rsi
	movq	-72(%rbp), %rdi
	movq	%r15, %rdx
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-64(%rbp), %rax
	movq	8(%r15), %rdi
	movq	$0, -64(%rbp)
	movq	%rax, 8(%r15)
	testq	%rdi, %rdi
	je	.L672
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L672
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L688
.L668:
	addq	%r12, 48(%r15)
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L670:
	addq	%r12, 48(%r15)
	movq	(%rdi), %rax
	call	*48(%rax)
.L666:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L689
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L667:
	.cfi_restore_state
	movq	96(%rdi), %rsi
	leaq	(%r14,%rdx), %rcx
	leaq	88(%rdi), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIhSaIhEE15_M_range_insertIPKhEEvN9__gnu_cxx17__normal_iteratorIPhS1_EET_S9_St20forward_iterator_tag
	jmp	.L666
.L689:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17994:
	.size	_ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE, .-_ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE
	.section	.text.unlikely._ZN2v88internal4wasm16StreamingDecoder6FinishEv,"ax",@progbits
	.align 2
.LCOLDB26:
	.section	.text._ZN2v88internal4wasm16StreamingDecoder6FinishEv,"ax",@progbits
.LHOTB26:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm16StreamingDecoder6FinishEv
	.type	_ZN2v88internal4wasm16StreamingDecoder6FinishEv, @function
_ZN2v88internal4wasm16StreamingDecoder6FinishEv:
.LFB17999:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L690
	cmpq	$0, 120(%r12)
	je	.L692
	movq	88(%r12), %r14
	movq	96(%r12), %r13
	movq	(%rdi), %rax
	movq	112(%r12), %rsi
	subq	%r14, %r13
	movq	120(%r12), %rdx
	movq	%r14, %rcx
	movslq	%r13d, %r13
	movq	%r13, %r8
	call	*80(%rax)
	testb	%al, %al
	jne	.L690
	movq	$0, 112(%r12)
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	$0, 120(%r12)
	call	_ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE
.L692:
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testb	%al, %al
	je	.L722
	movq	48(%r12), %r14
	testq	%r14, %r14
	je	.L703
	movq	%r14, %rdi
	call	_Znam@PLT
	movq	24(%r12), %r13
	movq	16(%r12), %rbx
	movq	%rax, %r15
	movabsq	$6131245312, %rax
	movq	%rax, (%r15)
	leaq	8(%r15), %rcx
	cmpq	%rbx, %r13
	je	.L708
	.p2align 4,,10
	.p2align 3
.L707:
	movq	(%rbx), %rax
	movq	%rcx, %rdi
	addq	$16, %rbx
	movq	16(%rax), %rsi
	movq	24(%rax), %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
	movq	-16(%rbx), %rax
	addq	24(%rax), %rcx
	cmpq	%rbx, %r13
	jne	.L707
.L708:
	movq	(%r12), %rdi
	leaq	-176(%rbp), %rsi
	movq	(%rdi), %rax
	movq	56(%rax), %rax
	movq	%r15, -176(%rbp)
	movq	%r14, -168(%rbp)
	call	*%rax
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L690
	call	_ZdaPv@PLT
.L690:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L723
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L722:
	.cfi_restore_state
	xorl	%edx, %edx
	leaq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rbx
	movq	$24, -176(%rbp)
	leaq	-176(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-176(%rbp), %rdx
	movdqa	.LC25(%rip), %xmm0
	movabsq	$7881692365129457766, %rcx
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	movups	%xmm0, (%rax)
	movq	-160(%rbp), %rdx
	movq	%rcx, 16(%rax)
	movq	-176(%rbp), %rax
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	movl	44(%r12), %eax
	movq	-160(%rbp), %rdx
	subl	$1, %eax
	cmpq	%rbx, %rdx
	je	.L724
	movq	-144(%rbp), %rcx
	leaq	-72(%rbp), %r14
	leaq	-112(%rbp), %r13
	movq	-152(%rbp), %rsi
	movq	%rbx, -160(%rbp)
	movq	%rcx, -112(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movl	%eax, -96(%rbp)
	movq	%r14, -88(%rbp)
	cmpq	%r13, %rdx
	je	.L696
	movq	%rdx, -88(%rbp)
	movq	%rcx, -72(%rbp)
.L698:
	movq	(%r12), %rdi
	movq	%rsi, -80(%rbp)
	movq	%r13, -128(%rbp)
	movq	$0, -120(%rbp)
	movb	$0, -112(%rbp)
	testq	%rdi, %rdi
	je	.L699
	movq	(%rdi), %rax
	leaq	-96(%rbp), %rsi
	call	*64(%rax)
	movq	(%r12), %rdi
	movq	$0, (%r12)
	testq	%rdi, %rdi
	je	.L699
	movq	(%rdi), %rax
	call	*8(%rax)
.L699:
	movq	-88(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L700
	call	_ZdlPv@PLT
.L700:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L701
	call	_ZdlPv@PLT
.L701:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L690
	call	_ZdlPv@PLT
	jmp	.L690
.L724:
	leaq	-72(%rbp), %r14
	movdqa	-144(%rbp), %xmm1
	movl	%eax, -96(%rbp)
	leaq	-112(%rbp), %r13
	movq	-152(%rbp), %rsi
	movb	$0, -144(%rbp)
	movq	$0, -152(%rbp)
	movq	%r14, -88(%rbp)
	movaps	%xmm1, -112(%rbp)
.L696:
	movdqa	-112(%rbp), %xmm2
	movups	%xmm2, -72(%rbp)
	jmp	.L698
.L723:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal4wasm16StreamingDecoder6FinishEv
	.cfi_startproc
	.type	_ZN2v88internal4wasm16StreamingDecoder6FinishEv.cold, @function
_ZN2v88internal4wasm16StreamingDecoder6FinishEv.cold:
.LFSB17999:
.L703:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	$0, 0
	ud2
	.cfi_endproc
.LFE17999:
	.section	.text._ZN2v88internal4wasm16StreamingDecoder6FinishEv
	.size	_ZN2v88internal4wasm16StreamingDecoder6FinishEv, .-_ZN2v88internal4wasm16StreamingDecoder6FinishEv
	.section	.text.unlikely._ZN2v88internal4wasm16StreamingDecoder6FinishEv
	.size	_ZN2v88internal4wasm16StreamingDecoder6FinishEv.cold, .-_ZN2v88internal4wasm16StreamingDecoder6FinishEv.cold
.LCOLDE26:
	.section	.text._ZN2v88internal4wasm16StreamingDecoder6FinishEv
.LHOTE26:
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE, @function
_GLOBAL__sub_I__ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE:
.LFB23193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23193:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE, .-_GLOBAL__sub_I__ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm16StreamingDecoder15OnBytesReceivedENS0_6VectorIKhEE
	.weak	_ZTVN2v88internal4wasm16StreamingDecoder13SectionBufferE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm16StreamingDecoder13SectionBufferE,"awG",@progbits,_ZTVN2v88internal4wasm16StreamingDecoder13SectionBufferE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm16StreamingDecoder13SectionBufferE, @object
	.size	_ZTVN2v88internal4wasm16StreamingDecoder13SectionBufferE, 40
_ZTVN2v88internal4wasm16StreamingDecoder13SectionBufferE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD1Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder13SectionBufferD0Ev
	.quad	_ZNK2v88internal4wasm16StreamingDecoder13SectionBuffer7GetCodeENS1_12WireBytesRefE
	.weak	_ZTVN2v88internal4wasm7DecoderE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm7DecoderE,"awG",@progbits,_ZTVN2v88internal4wasm7DecoderE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm7DecoderE, @object
	.size	_ZTVN2v88internal4wasm7DecoderE, 40
_ZTVN2v88internal4wasm7DecoderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm7DecoderD1Ev
	.quad	_ZN2v88internal4wasm7DecoderD0Ev
	.quad	_ZN2v88internal4wasm7Decoder12onFirstErrorEv
	.weak	_ZTVN2v88internal4wasm16StreamingDecoder13DecodingStateE
	.section	.data.rel.ro._ZTVN2v88internal4wasm16StreamingDecoder13DecodingStateE,"awG",@progbits,_ZTVN2v88internal4wasm16StreamingDecoder13DecodingStateE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm16StreamingDecoder13DecodingStateE, @object
	.size	_ZTVN2v88internal4wasm16StreamingDecoder13DecodingStateE, 64
_ZTVN2v88internal4wasm16StreamingDecoder13DecodingStateE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm16StreamingDecoder13DecodingState9ReadBytesEPS2_NS0_6VectorIKhEE
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv
	.weak	_ZTVN2v88internal4wasm16StreamingDecoder14DecodeVarInt32E
	.section	.data.rel.ro._ZTVN2v88internal4wasm16StreamingDecoder14DecodeVarInt32E,"awG",@progbits,_ZTVN2v88internal4wasm16StreamingDecoder14DecodeVarInt32E,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm16StreamingDecoder14DecodeVarInt32E, @object
	.size	_ZTVN2v88internal4wasm16StreamingDecoder14DecodeVarInt32E, 72
_ZTVN2v88internal4wasm16StreamingDecoder14DecodeVarInt32E:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt329ReadBytesEPS2_NS0_6VectorIKhEE
	.quad	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt324NextEPS2_
	.quad	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv
	.quad	_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv
	.quad	__cxa_pure_virtual
	.weak	_ZTVN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderE,"awG",@progbits,_ZTVN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderE, @object
	.size	_ZTVN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderE, 64
_ZTVN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD1Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeaderD0Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder13DecodingState9ReadBytesEPS2_NS0_6VectorIKhEE
	.quad	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader4NextEPS2_
	.quad	_ZN2v88internal4wasm16StreamingDecoder18DecodeModuleHeader6bufferEv
	.quad	_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv
	.weak	_ZTVN2v88internal4wasm16StreamingDecoder15DecodeSectionIDE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm16StreamingDecoder15DecodeSectionIDE,"awG",@progbits,_ZTVN2v88internal4wasm16StreamingDecoder15DecodeSectionIDE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm16StreamingDecoder15DecodeSectionIDE, @object
	.size	_ZTVN2v88internal4wasm16StreamingDecoder15DecodeSectionIDE, 64
_ZTVN2v88internal4wasm16StreamingDecoder15DecodeSectionIDE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD1Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionIDD0Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder13DecodingState9ReadBytesEPS2_NS0_6VectorIKhEE
	.quad	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID4NextEPS2_
	.quad	_ZN2v88internal4wasm16StreamingDecoder15DecodeSectionID6bufferEv
	.quad	_ZNK2v88internal4wasm16StreamingDecoder15DecodeSectionID20is_finishing_allowedEv
	.weak	_ZTVN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthE,"awG",@progbits,_ZTVN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthE, @object
	.size	_ZTVN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthE, 72
_ZTVN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD1Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLengthD0Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt329ReadBytesEPS2_NS0_6VectorIKhEE
	.quad	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt324NextEPS2_
	.quad	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv
	.quad	_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv
	.quad	_ZN2v88internal4wasm16StreamingDecoder19DecodeSectionLength13NextWithValueEPS2_
	.weak	_ZTVN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadE,"awG",@progbits,_ZTVN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadE, @object
	.size	_ZTVN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadE, 64
_ZTVN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD1Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayloadD0Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder13DecodingState9ReadBytesEPS2_NS0_6VectorIKhEE
	.quad	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload4NextEPS2_
	.quad	_ZN2v88internal4wasm16StreamingDecoder20DecodeSectionPayload6bufferEv
	.quad	_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv
	.weak	_ZTVN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsE,"awG",@progbits,_ZTVN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsE, @object
	.size	_ZTVN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsE, 72
_ZTVN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD1Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctionsD0Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt329ReadBytesEPS2_NS0_6VectorIKhEE
	.quad	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt324NextEPS2_
	.quad	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv
	.quad	_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv
	.quad	_ZN2v88internal4wasm16StreamingDecoder23DecodeNumberOfFunctions13NextWithValueEPS2_
	.weak	_ZTVN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthE,"awG",@progbits,_ZTVN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthE, @object
	.size	_ZTVN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthE, 72
_ZTVN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD1Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLengthD0Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt329ReadBytesEPS2_NS0_6VectorIKhEE
	.quad	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt324NextEPS2_
	.quad	_ZN2v88internal4wasm16StreamingDecoder14DecodeVarInt326bufferEv
	.quad	_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv
	.quad	_ZN2v88internal4wasm16StreamingDecoder20DecodeFunctionLength13NextWithValueEPS2_
	.weak	_ZTVN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyE
	.section	.data.rel.ro.local._ZTVN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyE,"awG",@progbits,_ZTVN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyE,comdat
	.align 8
	.type	_ZTVN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyE, @object
	.size	_ZTVN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyE, 64
_ZTVN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD1Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBodyD0Ev
	.quad	_ZN2v88internal4wasm16StreamingDecoder13DecodingState9ReadBytesEPS2_NS0_6VectorIKhEE
	.quad	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody4NextEPS2_
	.quad	_ZN2v88internal4wasm16StreamingDecoder18DecodeFunctionBody6bufferEv
	.quad	_ZNK2v88internal4wasm16StreamingDecoder13DecodingState20is_finishing_allowedEv
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN2v88internal4wasm16StreamingDecoder13SectionBufferESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	2336361471110573934
	.quad	7162257365379870563
	.align 16
.LC3:
	.quad	8392847272012900724
	.quad	2334397761977348965
	.section	.data.rel.ro,"aw"
	.align 8
.LC6:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC7:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC8:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst16
	.align 16
.LC9:
	.quad	2334106421097295465
	.quad	7162257365379870563
	.align 16
.LC10:
	.quad	8314049671528015218
	.quad	8295741995656355956
	.align 16
.LC11:
	.quad	2334106421097295465
	.quad	7957695015192261990
	.align 16
.LC12:
	.quad	8462103605467705198
	.quad	2334382411428685927
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC19:
	.quad	1
	.section	.rodata.cst16
	.align 16
.LC22:
	.quad	7162257365379870563
	.quad	7953747407855905140
	.align 16
.LC23:
	.quad	8097789310871170848
	.quad	7164786242341660016
	.align 16
.LC25:
	.quad	8386658464824651381
	.quad	8007510562770543717
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
