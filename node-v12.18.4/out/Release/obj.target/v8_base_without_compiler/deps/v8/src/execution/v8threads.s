	.file	"v8threads.cc"
	.text
	.section	.text._ZN2v86Locker8IsLockedEPNS_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v86Locker8IsLockedEPNS_7IsolateE
	.type	_ZN2v86Locker8IsLockedEPNS_7IsolateE, @function
_ZN2v86Locker8IsLockedEPNS_7IsolateE:
.LFB17937:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	41168(%rdi), %rbx
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	movl	40(%rbx), %edx
	cmpl	%eax, %edx
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17937:
	.size	_ZN2v86Locker8IsLockedEPNS_7IsolateE, .-_ZN2v86Locker8IsLockedEPNS_7IsolateE
	.section	.text._ZN2v86Locker8IsActiveEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v86Locker8IsActiveEv
	.type	_ZN2v86Locker8IsActiveEv, @function
_ZN2v86Locker8IsActiveEv:
.LFB17938:
	.cfi_startproc
	endbr64
	movl	_ZN2v812_GLOBAL__N_123g_locker_was_ever_used_E(%rip), %eax
	testl	%eax, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE17938:
	.size	_ZN2v86Locker8IsActiveEv, .-_ZN2v86Locker8IsActiveEv
	.section	.rodata._ZN2v88Unlocker10InitializeEPNS_7IsolateE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"NewArray"
	.section	.text._ZN2v88Unlocker10InitializeEPNS_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88Unlocker10InitializeEPNS_7IsolateE
	.type	_ZN2v88Unlocker10InitializeEPNS_7IsolateE, @function
_ZN2v88Unlocker10InitializeEPNS_7IsolateE:
.LFB17942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, (%rdi)
	movq	41168(%rsi), %r12
	movq	56(%r12), %rax
	movq	16(%rax), %rbx
	cmpq	%rbx, %rax
	je	.L9
.L6:
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	movq	%rax, 24(%rdx)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	72(%r12), %rdi
	call	_ZN2v88internal7Isolate40FindOrAllocatePerThreadDataForThisThreadEv@PLT
	movq	%rbx, 24(%rax)
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	movq	%rbx, 48(%r12)
	movl	%eax, 44(%r12)
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	movl	%eax, (%rbx)
	movq	0(%r13), %rax
	movq	41168(%rax), %rdi
	movl	$-1, 40(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movl	$40, %edi
	call	_Znwm@PLT
	movl	$-1, (%rax)
	movq	%rax, %r14
	movq	%rax, %rbx
	movq	$0, 8(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 24(%rax)
	movq	%r12, 32(%rax)
	call	_ZN2v88internal22HandleScopeImplementer21ArchiveSpacePerThreadEv@PLT
	movl	%eax, -52(%rbp)
	call	_ZN2v88internal5Debug21ArchiveSpacePerThreadEv@PLT
	movl	%eax, %r15d
	call	_ZN2v88internal12Bootstrapper21ArchiveSpacePerThreadEv@PLT
	movl	-52(%rbp), %edx
	leal	272(%rdx,%r15), %r15d
	addl	%eax, %r15d
	call	_ZN2v88internal11Relocatable21ArchiveSpacePerThreadEv@PLT
	leaq	_ZSt7nothrow(%rip), %rsi
	addl	%eax, %r15d
	movslq	%r15d, %r15
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L10
.L7:
	movq	%rax, 8(%r14)
	jmp	.L6
.L10:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r15, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L7
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE17942:
	.size	_ZN2v88Unlocker10InitializeEPNS_7IsolateE, .-_ZN2v88Unlocker10InitializeEPNS_7IsolateE
	.section	.text._ZN2v88internal13ThreadManager10InitThreadERKNS0_15ExecutionAccessE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager10InitThreadERKNS0_15ExecutionAccessE
	.type	_ZN2v88internal13ThreadManager10InitThreadERKNS0_15ExecutionAccessE, @function
_ZN2v88internal13ThreadManager10InitThreadERKNS0_15ExecutionAccessE:
.LFB17946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	72(%rdi), %rdi
	call	_ZN2v88internal7Isolate21InitializeThreadLocalEv@PLT
	movq	72(%rbx), %rax
	movq	%r12, %rsi
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard10InitThreadERKNS0_15ExecutionAccessE@PLT
	movq	72(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	41472(%rax), %rdi
	jmp	_ZN2v88internal5Debug10ThreadInitEv@PLT
	.cfi_endproc
.LFE17946:
	.size	_ZN2v88internal13ThreadManager10InitThreadERKNS0_15ExecutionAccessE, .-_ZN2v88internal13ThreadManager10InitThreadERKNS0_15ExecutionAccessE
	.section	.text._ZN2v88internal13ThreadManager4LockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager4LockEv
	.type	_ZN2v88internal13ThreadManager4LockEv, @function
_ZN2v88internal13ThreadManager4LockEv:
.LFB17948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v84base5Mutex4LockEv@PLT
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	movl	%eax, 40(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17948:
	.size	_ZN2v88internal13ThreadManager4LockEv, .-_ZN2v88internal13ThreadManager4LockEv
	.section	.text._ZN2v88internal13ThreadManager6UnlockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager6UnlockEv
	.type	_ZN2v88internal13ThreadManager6UnlockEv, @function
_ZN2v88internal13ThreadManager6UnlockEv:
.LFB17949:
	.cfi_startproc
	endbr64
	movl	$-1, 40(%rdi)
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.cfi_endproc
.LFE17949:
	.size	_ZN2v88internal13ThreadManager6UnlockEv, .-_ZN2v88internal13ThreadManager6UnlockEv
	.section	.text._ZN2v88internal11ThreadStateC2EPNS0_13ThreadManagerE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ThreadStateC2EPNS0_13ThreadManagerE
	.type	_ZN2v88internal11ThreadStateC2EPNS0_13ThreadManagerE, @function
_ZN2v88internal11ThreadStateC2EPNS0_13ThreadManagerE:
.LFB17952:
	.cfi_startproc
	endbr64
	movl	$-1, (%rdi)
	movq	$0, 8(%rdi)
	movq	%rdi, 16(%rdi)
	movq	%rdi, 24(%rdi)
	movq	%rsi, 32(%rdi)
	ret
	.cfi_endproc
.LFE17952:
	.size	_ZN2v88internal11ThreadStateC2EPNS0_13ThreadManagerE, .-_ZN2v88internal11ThreadStateC2EPNS0_13ThreadManagerE
	.globl	_ZN2v88internal11ThreadStateC1EPNS0_13ThreadManagerE
	.set	_ZN2v88internal11ThreadStateC1EPNS0_13ThreadManagerE,_ZN2v88internal11ThreadStateC2EPNS0_13ThreadManagerE
	.section	.text._ZN2v88internal11ThreadStateD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ThreadStateD2Ev
	.type	_ZN2v88internal11ThreadStateD2Ev, @function
_ZN2v88internal11ThreadStateD2Ev:
.LFB17955:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L17
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	ret
	.cfi_endproc
.LFE17955:
	.size	_ZN2v88internal11ThreadStateD2Ev, .-_ZN2v88internal11ThreadStateD2Ev
	.globl	_ZN2v88internal11ThreadStateD1Ev
	.set	_ZN2v88internal11ThreadStateD1Ev,_ZN2v88internal11ThreadStateD2Ev
	.section	.text._ZN2v88internal11ThreadState13AllocateSpaceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ThreadState13AllocateSpaceEv
	.type	_ZN2v88internal11ThreadState13AllocateSpaceEv, @function
_ZN2v88internal11ThreadState13AllocateSpaceEv:
.LFB17957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal22HandleScopeImplementer21ArchiveSpacePerThreadEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal5Debug21ArchiveSpacePerThreadEv@PLT
	movl	%eax, %r13d
	call	_ZN2v88internal12Bootstrapper21ArchiveSpacePerThreadEv@PLT
	leal	272(%r14,%r13), %r12d
	addl	%eax, %r12d
	call	_ZN2v88internal11Relocatable21ArchiveSpacePerThreadEv@PLT
	leaq	_ZSt7nothrow(%rip), %rsi
	addl	%eax, %r12d
	movslq	%r12d, %r12
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L22
.L20:
	movq	%rax, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r12, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L20
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE17957:
	.size	_ZN2v88internal11ThreadState13AllocateSpaceEv, .-_ZN2v88internal11ThreadState13AllocateSpaceEv
	.section	.text._ZN2v88internal11ThreadState6UnlinkEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ThreadState6UnlinkEv
	.type	_ZN2v88internal11ThreadState6UnlinkEv, @function
_ZN2v88internal11ThreadState6UnlinkEv:
.LFB17958:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	24(%rdi), %rax
	movq	%rax, 24(%rdx)
	movq	16(%rdi), %rdx
	movq	%rdx, 16(%rax)
	ret
	.cfi_endproc
.LFE17958:
	.size	_ZN2v88internal11ThreadState6UnlinkEv, .-_ZN2v88internal11ThreadState6UnlinkEv
	.section	.text._ZN2v88internal11ThreadState8LinkIntoENS1_4ListE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ThreadState8LinkIntoENS1_4ListE
	.type	_ZN2v88internal11ThreadState8LinkIntoENS1_4ListE, @function
_ZN2v88internal11ThreadState8LinkIntoENS1_4ListE:
.LFB17959:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	testl	%esi, %esi
	movq	64(%rdx), %rax
	cmove	56(%rdx), %rax
	movq	16(%rax), %rdx
	movq	%rax, 24(%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rdi, 16(%rax)
	movq	16(%rdi), %rax
	movq	%rdi, 24(%rax)
	ret
	.cfi_endproc
.LFE17959:
	.size	_ZN2v88internal11ThreadState8LinkIntoENS1_4ListE, .-_ZN2v88internal11ThreadState8LinkIntoENS1_4ListE
	.section	.text._ZN2v88internal13ThreadManager18GetFreeThreadStateEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager18GetFreeThreadStateEv
	.type	_ZN2v88internal13ThreadManager18GetFreeThreadStateEv, @function
_ZN2v88internal13ThreadManager18GetFreeThreadStateEv:
.LFB17960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	56(%rdi), %rax
	movq	16(%rax), %r12
	cmpq	%r12, %rax
	je	.L32
.L28:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	.cfi_restore_state
	movq	%rdi, %rbx
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%rbx, 32(%rax)
	movq	%rax, %r13
	movq	%rax, %r12
	movl	$-1, (%rax)
	movq	$0, 8(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 24(%rax)
	call	_ZN2v88internal22HandleScopeImplementer21ArchiveSpacePerThreadEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal5Debug21ArchiveSpacePerThreadEv@PLT
	movl	%eax, %ebx
	call	_ZN2v88internal12Bootstrapper21ArchiveSpacePerThreadEv@PLT
	leal	272(%r14,%rbx), %r14d
	addl	%eax, %r14d
	call	_ZN2v88internal11Relocatable21ArchiveSpacePerThreadEv@PLT
	leaq	_ZSt7nothrow(%rip), %rsi
	addl	%eax, %r14d
	movslq	%r14d, %r14
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L33
.L30:
	movq	%rax, 8(%r13)
	jmp	.L28
.L33:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L30
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE17960:
	.size	_ZN2v88internal13ThreadManager18GetFreeThreadStateEv, .-_ZN2v88internal13ThreadManager18GetFreeThreadStateEv
	.section	.text._ZN2v88internal13ThreadManager21FirstThreadStateInUseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager21FirstThreadStateInUseEv
	.type	_ZN2v88internal13ThreadManager21FirstThreadStateInUseEv, @function
_ZN2v88internal13ThreadManager21FirstThreadStateInUseEv:
.LFB17961:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdx
	movq	16(%rdx), %rax
	movq	32(%rdx), %rdx
	cmpq	64(%rdx), %rax
	movl	$0, %edx
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE17961:
	.size	_ZN2v88internal13ThreadManager21FirstThreadStateInUseEv, .-_ZN2v88internal13ThreadManager21FirstThreadStateInUseEv
	.section	.text._ZN2v88internal11ThreadState4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11ThreadState4NextEv
	.type	_ZN2v88internal11ThreadState4NextEv, @function
_ZN2v88internal11ThreadState4NextEv:
.LFB17962:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	movq	16(%rdi), %rax
	cmpq	64(%rdx), %rax
	movl	$0, %edx
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE17962:
	.size	_ZN2v88internal11ThreadState4NextEv, .-_ZN2v88internal11ThreadState4NextEv
	.section	.text._ZN2v88internal13ThreadManagerC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManagerC2EPNS0_7IsolateE
	.type	_ZN2v88internal13ThreadManagerC2EPNS0_7IsolateE, @function
_ZN2v88internal13ThreadManagerC2EPNS0_7IsolateE:
.LFB17964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v84base5MutexC1Ev@PLT
	movq	%r12, 72(%rbx)
	pxor	%xmm0, %xmm0
	movl	$40, %edi
	movq	$-1, 40(%rbx)
	movq	$0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	call	_Znwm@PLT
	movl	$40, %edi
	movq	%rbx, 32(%rax)
	movq	%rax, 56(%rbx)
	movl	$-1, (%rax)
	movq	$0, 8(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 24(%rax)
	call	_Znwm@PLT
	movl	$-1, (%rax)
	movq	$0, 8(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 24(%rax)
	movq	%rbx, 32(%rax)
	movq	%rax, 64(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17964:
	.size	_ZN2v88internal13ThreadManagerC2EPNS0_7IsolateE, .-_ZN2v88internal13ThreadManagerC2EPNS0_7IsolateE
	.globl	_ZN2v88internal13ThreadManagerC1EPNS0_7IsolateE
	.set	_ZN2v88internal13ThreadManagerC1EPNS0_7IsolateE,_ZN2v88internal13ThreadManagerC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal13ThreadManagerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManagerD2Ev
	.type	_ZN2v88internal13ThreadManagerD2Ev, @function
_ZN2v88internal13ThreadManagerD2Ev:
.LFB17967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	56(%rdi), %r12
	movq	16(%r12), %r13
	cmpq	%r13, %r12
	jne	.L44
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rbx, %r13
.L44:
	movq	8(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L42
	call	_ZdaPv@PLT
.L42:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	cmpq	%rbx, %r12
	jne	.L58
.L41:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L45
	call	_ZdaPv@PLT
.L45:
	movq	%r12, %rdi
	movl	$40, %esi
	call	_ZdlPvm@PLT
	movq	64(%r14), %r12
	movq	16(%r12), %r13
	cmpq	%r13, %r12
	jne	.L49
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%rbx, %r13
.L49:
	movq	8(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L47
	call	_ZdaPv@PLT
.L47:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	cmpq	%rbx, %r12
	jne	.L59
.L46:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L50
	call	_ZdaPv@PLT
.L50:
	movq	%r12, %rdi
	movl	$40, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5MutexD1Ev@PLT
	.cfi_endproc
.LFE17967:
	.size	_ZN2v88internal13ThreadManagerD2Ev, .-_ZN2v88internal13ThreadManagerD2Ev
	.globl	_ZN2v88internal13ThreadManagerD1Ev
	.set	_ZN2v88internal13ThreadManagerD1Ev,_ZN2v88internal13ThreadManagerD2Ev
	.section	.text._ZN2v88internal13ThreadManager21DeleteThreadStateListEPNS0_11ThreadStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager21DeleteThreadStateListEPNS0_11ThreadStateE
	.type	_ZN2v88internal13ThreadManager21DeleteThreadStateListEPNS0_11ThreadStateE, @function
_ZN2v88internal13ThreadManager21DeleteThreadStateListEPNS0_11ThreadStateE:
.LFB17969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rsi), %r12
	cmpq	%r12, %rsi
	jne	.L64
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%rbx, %r12
.L64:
	movq	8(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L62
	call	_ZdaPv@PLT
.L62:
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	cmpq	%rbx, %r13
	jne	.L70
.L61:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L65
	call	_ZdaPv@PLT
.L65:
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$40, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE17969:
	.size	_ZN2v88internal13ThreadManager21DeleteThreadStateListEPNS0_11ThreadStateE, .-_ZN2v88internal13ThreadManager21DeleteThreadStateListEPNS0_11ThreadStateE
	.section	.text._ZN2v88internal13ThreadManager13ArchiveThreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager13ArchiveThreadEv
	.type	_ZN2v88internal13ThreadManager13ArchiveThreadEv, @function
_ZN2v88internal13ThreadManager13ArchiveThreadEv:
.LFB17970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	56(%rdi), %rax
	movq	16(%rax), %rbx
	cmpq	%rbx, %rax
	je	.L75
.L72:
	movq	16(%rbx), %rdx
	movq	24(%rbx), %rax
	movq	%rax, 24(%rdx)
	movq	16(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	72(%r12), %rdi
	call	_ZN2v88internal7Isolate40FindOrAllocatePerThreadDataForThisThreadEv@PLT
	movq	%rbx, 24(%rax)
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	movq	%rbx, 48(%r12)
	movl	%eax, 44(%r12)
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	movl	%eax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movl	$40, %edi
	call	_Znwm@PLT
	movl	$-1, (%rax)
	movq	%rax, %r13
	movq	%rax, %rbx
	movq	$0, 8(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 24(%rax)
	movq	%r12, 32(%rax)
	call	_ZN2v88internal22HandleScopeImplementer21ArchiveSpacePerThreadEv@PLT
	movl	%eax, %r15d
	call	_ZN2v88internal5Debug21ArchiveSpacePerThreadEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal12Bootstrapper21ArchiveSpacePerThreadEv@PLT
	leal	272(%r15,%r14), %r14d
	addl	%eax, %r14d
	call	_ZN2v88internal11Relocatable21ArchiveSpacePerThreadEv@PLT
	leaq	_ZSt7nothrow(%rip), %rsi
	addl	%eax, %r14d
	movslq	%r14d, %r14
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L76
.L73:
	movq	%rax, 8(%r13)
	jmp	.L72
.L76:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movq	%r14, %rdi
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	jne	.L73
	leaq	.LC0(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE17970:
	.size	_ZN2v88internal13ThreadManager13ArchiveThreadEv, .-_ZN2v88internal13ThreadManager13ArchiveThreadEv
	.section	.text._ZN2v86LockerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v86LockerD2Ev
	.type	_ZN2v86LockerD2Ev, @function
_ZN2v86LockerD2Ev:
.LFB17940:
	.cfi_startproc
	endbr64
	cmpb	$0, (%rdi)
	je	.L77
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rax
	movq	%rdi, %rbx
	cmpb	$0, 1(%rdi)
	movq	41168(%rax), %r12
	jne	.L84
	movq	%r12, %rdi
	call	_ZN2v88internal13ThreadManager13ArchiveThreadEv
.L80:
	movq	8(%rbx), %rax
	movq	41168(%rax), %rdi
	movl	$-1, 40(%rdi)
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v84base5Mutex6UnlockEv@PLT
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	72(%r12), %rax
	movq	41120(%rax), %rdi
	call	_ZN2v88internal22HandleScopeImplementer19FreeThreadResourcesEv@PLT
	movq	72(%r12), %rax
	leaq	12448(%rax), %rdi
	call	_ZN2v88internal14ThreadLocalTop4FreeEv@PLT
	movq	72(%r12), %rax
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard19FreeThreadResourcesEv@PLT
	movq	72(%r12), %rax
	movq	41208(%rax), %rdi
	call	_ZN2v88internal11RegExpStack11ThreadLocal4FreeEv@PLT
	movq	72(%r12), %rax
	movq	40936(%rax), %rdi
	call	_ZN2v88internal12Bootstrapper19FreeThreadResourcesEv@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE17940:
	.size	_ZN2v86LockerD2Ev, .-_ZN2v86LockerD2Ev
	.globl	_ZN2v86LockerD1Ev
	.set	_ZN2v86LockerD1Ev,_ZN2v86LockerD2Ev
	.section	.text._ZN2v88internal13ThreadManager20EagerlyArchiveThreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager20EagerlyArchiveThreadEv
	.type	_ZN2v88internal13ThreadManager20EagerlyArchiveThreadEv, @function
_ZN2v88internal13ThreadManager20EagerlyArchiveThreadEv:
.LFB17971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	48(%rdi), %rax
	movq	32(%rax), %rdx
	movq	64(%rdx), %rdx
	movq	16(%rdx), %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%rax)
	movq	%rax, 16(%rdx)
	movq	16(%rax), %rdx
	movq	%rax, 24(%rdx)
	movq	8(%rax), %rsi
	movq	72(%rdi), %rax
	movq	41120(%rax), %rdi
	call	_ZN2v88internal22HandleScopeImplementer13ArchiveThreadEPc@PLT
	movq	72(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Isolate13ArchiveThreadEPc@PLT
	movq	72(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11Relocatable12ArchiveStateEPNS0_7IsolateEPc@PLT
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	41472(%rax), %rdi
	call	_ZN2v88internal5Debug12ArchiveDebugEPc@PLT
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard17ArchiveStackGuardEPc@PLT
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	41208(%rax), %rdi
	call	_ZN2v88internal11RegExpStack12ArchiveStackEPc@PLT
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	40936(%rax), %rdi
	call	_ZN2v88internal12Bootstrapper12ArchiveStateEPc@PLT
	movl	$-1, 44(%rbx)
	movq	$0, 48(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17971:
	.size	_ZN2v88internal13ThreadManager20EagerlyArchiveThreadEv, .-_ZN2v88internal13ThreadManager20EagerlyArchiveThreadEv
	.section	.text._ZN2v88internal13ThreadManager13RestoreThreadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager13RestoreThreadEv
	.type	_ZN2v88internal13ThreadManager13RestoreThreadEv, @function
_ZN2v88internal13ThreadManager13RestoreThreadEv:
.LFB17947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	cmpl	44(%rbx), %eax
	je	.L97
	movq	72(%rbx), %rdi
	movq	%rdi, -48(%rbp)
	addq	$40976, %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	cmpl	$-1, 44(%rbx)
	jne	.L98
.L90:
	movq	72(%rbx), %rdi
	call	_ZN2v88internal7Isolate30FindPerThreadDataForThisThreadEv@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L99
	movq	24(%rax), %r12
	movq	72(%rbx), %rdi
	testq	%r12, %r12
	je	.L92
	movq	8(%r12), %rsi
	movq	41120(%rdi), %rdi
	call	_ZN2v88internal22HandleScopeImplementer13RestoreThreadEPc@PLT
	movq	72(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal7Isolate13RestoreThreadEPc@PLT
	movq	72(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11Relocatable12RestoreStateEPNS0_7IsolateEPc@PLT
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	41472(%rax), %rdi
	call	_ZN2v88internal5Debug12RestoreDebugEPc@PLT
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard17RestoreStackGuardEPc@PLT
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	41208(%rax), %rdi
	call	_ZN2v88internal11RegExpStack12RestoreStackEPc@PLT
	movq	%rax, %rsi
	movq	72(%rbx), %rax
	movq	40936(%rax), %rdi
	call	_ZN2v88internal12Bootstrapper12RestoreStateEPc@PLT
	movq	$0, 24(%r13)
	movq	16(%r12), %rdx
	movq	24(%r12), %rax
	movl	$-1, (%r12)
	movq	%rax, 24(%rdx)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rax)
	movq	32(%r12), %rax
	movq	56(%rax), %rax
	movq	16(%rax), %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r12)
	movq	%r12, 16(%rax)
	movq	16(%r12), %rax
	movq	%r12, 24(%rax)
	movl	$1, %r12d
.L94:
	movq	-48(%rbp), %rax
	leaq	40976(%rax), %rdi
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
.L87:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L100
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal13ThreadManager20EagerlyArchiveThreadEv
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L99:
	movq	72(%rbx), %rdi
.L92:
	call	_ZN2v88internal7Isolate21InitializeThreadLocalEv@PLT
	movq	72(%rbx), %rax
	leaq	-48(%rbp), %rsi
	xorl	%r12d, %r12d
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard10InitThreadERKNS0_15ExecutionAccessE@PLT
	movq	72(%rbx), %rax
	movq	41472(%rax), %rdi
	call	_ZN2v88internal5Debug10ThreadInitEv@PLT
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$-1, 44(%rbx)
	movq	72(%rbx), %rdi
	movl	$1, %r12d
	call	_ZN2v88internal7Isolate30FindPerThreadDataForThisThreadEv@PLT
	movq	%rax, %r8
	movq	48(%rbx), %rax
	movl	$-1, (%rax)
	movq	48(%rbx), %rax
	movq	32(%rax), %rdx
	movq	56(%rdx), %rdx
	movq	16(%rdx), %xmm0
	movq	%rdx, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%rax)
	movq	%rax, 16(%rdx)
	movq	16(%rax), %rdx
	movq	%rax, 24(%rdx)
	movq	$0, 48(%rbx)
	movq	$0, 24(%r8)
	jmp	.L87
.L100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE17947:
	.size	_ZN2v88internal13ThreadManager13RestoreThreadEv, .-_ZN2v88internal13ThreadManager13RestoreThreadEv
	.section	.text._ZN2v88UnlockerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88UnlockerD2Ev
	.type	_ZN2v88UnlockerD2Ev, @function
_ZN2v88UnlockerD2Ev:
.LFB17944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	41168(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	movl	%eax, 40(%r12)
	movq	(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	41168(%rax), %rdi
	jmp	_ZN2v88internal13ThreadManager13RestoreThreadEv
	.cfi_endproc
.LFE17944:
	.size	_ZN2v88UnlockerD2Ev, .-_ZN2v88UnlockerD2Ev
	.globl	_ZN2v88UnlockerD1Ev
	.set	_ZN2v88UnlockerD1Ev,_ZN2v88UnlockerD2Ev
	.section	.text._ZN2v86Locker10InitializeEPNS_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v86Locker10InitializeEPNS_7IsolateE
	.type	_ZN2v86Locker10InitializeEPNS_7IsolateE, @function
_ZN2v86Locker10InitializeEPNS_7IsolateE:
.LFB17936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$256, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movw	%ax, (%rdi)
	movq	%rsi, 8(%rdi)
	movl	$1, _ZN2v812_GLOBAL__N_123g_locker_was_ever_used_E(%rip)
	movq	8(%rdi), %rax
	movq	41168(%rax), %r12
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	movl	%eax, %r8d
	movl	40(%r12), %eax
	cmpl	%eax, %r8d
	jne	.L111
.L103:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	41168(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v84base5Mutex4LockEv@PLT
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	movl	%eax, 40(%r12)
	movb	$1, (%rbx)
	movq	8(%rbx), %rax
	movq	41168(%rax), %rdi
	call	_ZN2v88internal13ThreadManager13RestoreThreadEv
	testb	%al, %al
	je	.L103
	movb	$0, 1(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17936:
	.size	_ZN2v86Locker10InitializeEPNS_7IsolateE, .-_ZN2v86Locker10InitializeEPNS_7IsolateE
	.section	.text._ZN2v88internal13ThreadManager19FreeThreadResourcesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager19FreeThreadResourcesEv
	.type	_ZN2v88internal13ThreadManager19FreeThreadResourcesEv, @function
_ZN2v88internal13ThreadManager19FreeThreadResourcesEv:
.LFB17972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	72(%rdi), %rax
	movq	41120(%rax), %rdi
	call	_ZN2v88internal22HandleScopeImplementer19FreeThreadResourcesEv@PLT
	movq	72(%rbx), %rax
	leaq	12448(%rax), %rdi
	call	_ZN2v88internal14ThreadLocalTop4FreeEv@PLT
	movq	72(%rbx), %rax
	leaq	37512(%rax), %rdi
	call	_ZN2v88internal10StackGuard19FreeThreadResourcesEv@PLT
	movq	72(%rbx), %rax
	movq	41208(%rax), %rdi
	call	_ZN2v88internal11RegExpStack11ThreadLocal4FreeEv@PLT
	movq	72(%rbx), %rax
	movq	40936(%rax), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12Bootstrapper19FreeThreadResourcesEv@PLT
	.cfi_endproc
.LFE17972:
	.size	_ZN2v88internal13ThreadManager19FreeThreadResourcesEv, .-_ZN2v88internal13ThreadManager19FreeThreadResourcesEv
	.section	.text._ZN2v88internal13ThreadManager10IsArchivedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager10IsArchivedEv
	.type	_ZN2v88internal13ThreadManager10IsArchivedEv, @function
_ZN2v88internal13ThreadManager10IsArchivedEv:
.LFB17973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	72(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal7Isolate30FindPerThreadDataForThisThreadEv@PLT
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L114
	cmpq	$0, 24(%rax)
	setne	%r8b
.L114:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17973:
	.size	_ZN2v88internal13ThreadManager10IsArchivedEv, .-_ZN2v88internal13ThreadManager10IsArchivedEv
	.section	.text._ZN2v88internal13ThreadManager7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager7IterateEPNS0_11RootVisitorE
	.type	_ZN2v88internal13ThreadManager7IterateEPNS0_11RootVisitorE, @function
_ZN2v88internal13ThreadManager7IterateEPNS0_11RootVisitorE:
.LFB17974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	64(%rdi), %rax
	movq	16(%rax), %rbx
	movq	32(%rax), %rax
	cmpq	64(%rax), %rbx
	jne	.L122
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%rax, %rbx
.L122:
	testq	%rbx, %rbx
	je	.L120
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal22HandleScopeImplementer7IterateEPNS0_11RootVisitorEPc@PLT
	movq	72(%r13), %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal7Isolate7IterateEPNS0_11RootVisitorEPc@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal11Relocatable7IterateEPNS0_11RootVisitorEPc@PLT
	movq	32(%rbx), %rdx
	movq	16(%rbx), %rax
	cmpq	64(%rdx), %rax
	jne	.L123
.L120:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17974:
	.size	_ZN2v88internal13ThreadManager7IterateEPNS0_11RootVisitorE, .-_ZN2v88internal13ThreadManager7IterateEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal13ThreadManager22IterateArchivedThreadsEPNS0_13ThreadVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager22IterateArchivedThreadsEPNS0_13ThreadVisitorE
	.type	_ZN2v88internal13ThreadManager22IterateArchivedThreadsEPNS0_13ThreadVisitorE, @function
_ZN2v88internal13ThreadManager22IterateArchivedThreadsEPNS0_13ThreadVisitorE:
.LFB17975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	64(%rdi), %rax
	movq	16(%rax), %rbx
	movq	32(%rax), %rax
	cmpq	64(%rax), %rbx
	jne	.L131
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%rax, %rbx
.L131:
	testq	%rbx, %rbx
	je	.L129
	movq	8(%rbx), %r12
	call	_ZN2v88internal22HandleScopeImplementer21ArchiveSpacePerThreadEv@PLT
	movq	72(%r13), %rdi
	movq	%r14, %rsi
	movslq	%eax, %rdx
	addq	%r12, %rdx
	call	_ZN2v88internal7Isolate13IterateThreadEPNS0_13ThreadVisitorEPc@PLT
	movq	32(%rbx), %rdx
	movq	16(%rbx), %rax
	cmpq	64(%rdx), %rax
	jne	.L132
.L129:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17975:
	.size	_ZN2v88internal13ThreadManager22IterateArchivedThreadsEPNS0_13ThreadVisitorE, .-_ZN2v88internal13ThreadManager22IterateArchivedThreadsEPNS0_13ThreadVisitorE
	.section	.text._ZN2v88internal13ThreadManager9CurrentIdEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13ThreadManager9CurrentIdEv
	.type	_ZN2v88internal13ThreadManager9CurrentIdEv, @function
_ZN2v88internal13ThreadManager9CurrentIdEv:
.LFB17976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal8ThreadId18GetCurrentThreadIdEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17976:
	.size	_ZN2v88internal13ThreadManager9CurrentIdEv, .-_ZN2v88internal13ThreadManager9CurrentIdEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v86Locker10InitializeEPNS_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v86Locker10InitializeEPNS_7IsolateE, @function
_GLOBAL__sub_I__ZN2v86Locker10InitializeEPNS_7IsolateE:
.LFB21721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21721:
	.size	_GLOBAL__sub_I__ZN2v86Locker10InitializeEPNS_7IsolateE, .-_GLOBAL__sub_I__ZN2v86Locker10InitializeEPNS_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v86Locker10InitializeEPNS_7IsolateE
	.section	.bss._ZN2v812_GLOBAL__N_123g_locker_was_ever_used_E,"aw",@nobits
	.align 4
	.type	_ZN2v812_GLOBAL__N_123g_locker_was_ever_used_E, @object
	.size	_ZN2v812_GLOBAL__N_123g_locker_was_ever_used_E, 4
_ZN2v812_GLOBAL__N_123g_locker_was_ever_used_E:
	.zero	4
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
