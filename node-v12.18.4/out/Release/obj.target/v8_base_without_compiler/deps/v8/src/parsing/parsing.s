	.file	"parsing.cc"
	.text
	.section	.text._ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE
	.type	_ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE, @function
_ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE:
.LFB21096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1208, %rsp
	movl	%edx, -1236(%rbp)
	movl	12616(%rsi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$2, 12616(%rsi)
	movq	80(%rdi), %rax
	movq	41112(%rbx), %rdi
	movq	(%rax), %rax
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L3:
	movq	40960(%rbx), %rdx
	movl	11(%rsi), %r13d
	cmpb	$0, 6464(%rdx)
	je	.L5
	movq	6456(%rdx), %rax
.L6:
	testq	%rax, %rax
	je	.L7
	addl	%r13d, (%rax)
.L7:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%r12, %rdi
	leaq	-1224(%rbp), %rsi
	movq	%rax, -1224(%rbp)
	call	_ZN2v88internal9ParseInfo20set_character_streamESt10unique_ptrINS0_20Utf16CharacterStreamESt14default_deleteIS3_EE@PLT
	movq	-1224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L8
	movq	(%rdi), %rax
	call	*8(%rax)
.L8:
	leaq	-1216(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6ParserC1EPNS0_9ParseInfoE@PLT
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Parser12ParseProgramEPNS0_7IsolateEPNS0_9ParseInfoE@PLT
	movq	%rax, 168(%r12)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L9
	movq	%rax, %rdi
	call	_ZNK2v88internal15FunctionLiteral13language_modeEv@PLT
	testb	%al, %al
	movl	8(%r12), %eax
	jne	.L87
	andl	$-9, %eax
.L11:
	movl	%eax, 8(%r12)
	testb	$4, %al
	je	.L12
	movl	%eax, %edx
	andb	$-65, %dh
	cmpb	$0, -907(%rbp)
	jne	.L88
.L14:
	movl	%edx, 8(%r12)
.L12:
	movl	-1236(%rbp), %edx
	testl	%edx, %edx
	jne	.L32
.L31:
	movq	80(%r12), %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Parser16UpdateStatisticsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE@PLT
.L32:
	movq	-472(%rbp), %r12
	testq	%r13, %r13
	setne	%r13b
	testq	%r12, %r12
	je	.L15
	movq	296(%r12), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	176(%r12), %rdi
	testq	%rdi, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L15:
	movq	$0, -472(%rbp)
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L19
	call	_ZdlPv@PLT
.L19:
	leaq	-536(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L20
	call	_ZdaPv@PLT
.L20:
	movq	-608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZdaPv@PLT
.L21:
	leaq	-696(%rbp), %r12
	leaq	-936(%rbp), %r15
.L26:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L22
	call	_ZdaPv@PLT
.L22:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L23
	call	_ZdaPv@PLT
	subq	$80, %r12
	cmpq	%r12, %r15
	jne	.L26
.L24:
	movq	-992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	-1016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L28
	call	_ZdlPv@PLT
.L28:
	movq	-1152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L29
	call	_ZdlPv@PLT
.L29:
	movq	-1176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	_ZdlPv@PLT
.L30:
	movl	%r14d, 12616(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L89
	addq	$1208, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	orl	$8, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L23:
	subq	$80, %r12
	cmpq	%r12, %r15
	jne	.L26
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L5:
	movb	$1, 6464(%rdx)
	leaq	6440(%rdx), %rdi
	movq	%rdx, -1248(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	-1248(%rbp), %rdx
	movq	%rax, 6456(%rdx)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L9:
	movl	-1236(%rbp), %eax
	testl	%eax, %eax
	jne	.L32
	movq	112(%r12), %rcx
	movq	80(%r12), %rdx
	leaq	176(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L2:
	movq	41088(%rbx), %r15
	cmpq	%r15, 41096(%rbx)
	je	.L90
.L4:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L88:
	orb	$64, %ah
	movl	%eax, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%rbx, %rdi
	movq	%rsi, -1248(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1248(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L4
.L89:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21096:
	.size	_ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE, .-_ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE
	.section	.text._ZN2v88internal7parsing13ParseFunctionEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal7parsing13ParseFunctionEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE
	.type	_ZN2v88internal7parsing13ParseFunctionEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE, @function
_ZN2v88internal7parsing13ParseFunctionEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE:
.LFB21098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$1208, %rsp
	movl	%ecx, -1244(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	movq	41112(%rdx), %rdi
	movq	(%rax), %rax
	movq	7(%rax), %rsi
	testq	%rdi, %rdi
	je	.L92
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L93:
	movq	40960(%rbx), %r15
	movl	11(%rsi), %edx
	cmpb	$0, 6464(%r15)
	je	.L95
	movq	6456(%r15), %rax
.L96:
	testq	%rax, %rax
	je	.L97
	addl	%edx, (%rax)
.L97:
	leaq	-1224(%rbp), %r8
	movq	0(%r13), %rax
	movq	%r8, %rdi
	movq	%r8, -1240(%rbp)
	movq	%rax, -1224(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo11EndPositionEv@PLT
	leaq	-1232(%rbp), %rdi
	movl	%eax, %r15d
	movq	0(%r13), %rax
	movq	%rax, -1232(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	movl	%r15d, %ecx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal13ScannerStream3ForEPNS0_7IsolateENS0_6HandleINS0_6StringEEEii@PLT
	movq	-1240(%rbp), %r8
	movq	%r12, %rdi
	movq	%rax, -1224(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal9ParseInfo20set_character_streamESt10unique_ptrINS0_20Utf16CharacterStreamESt14default_deleteIS3_EE@PLT
	movq	-1224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L98
	movq	(%rdi), %rax
	call	*8(%rax)
.L98:
	movl	12616(%rbx), %r15d
	leaq	-1216(%rbp), %r14
	movq	%r12, %rsi
	movl	$2, 12616(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88internal6ParserC1EPNS0_9ParseInfoE@PLT
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Parser13ParseFunctionEPNS0_7IsolateEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	movq	%rax, 168(%r12)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L99
	movq	112(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal15AstValueFactory11InternalizeEPNS0_7IsolateE@PLT
	movl	8(%r12), %eax
	testb	$4, %al
	je	.L100
	movl	%eax, %edx
	andb	$-65, %dh
	cmpb	$0, -907(%rbp)
	je	.L102
	orb	$64, %ah
	movl	%eax, %edx
.L102:
	movl	%edx, 8(%r12)
.L100:
	movl	-1244(%rbp), %edx
	testl	%edx, %edx
	jne	.L120
.L119:
	movq	80(%r12), %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Parser16UpdateStatisticsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEE@PLT
.L120:
	movq	-472(%rbp), %r12
	testq	%r13, %r13
	setne	%r13b
	testq	%r12, %r12
	je	.L103
	movq	296(%r12), %rdi
	testq	%rdi, %rdi
	je	.L104
	call	_ZdlPv@PLT
.L104:
	movq	176(%r12), %rdi
	testq	%rdi, %rdi
	je	.L105
	call	_ZdlPv@PLT
.L105:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L106
	call	_ZdlPv@PLT
.L106:
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L103:
	movq	$0, -472(%rbp)
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_ZdlPv@PLT
.L107:
	leaq	-536(%rbp), %rdi
	call	_ZN2v88internal4ZoneD1Ev@PLT
	movq	-584(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L108
	call	_ZdaPv@PLT
.L108:
	movq	-608(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L109
	call	_ZdaPv@PLT
.L109:
	leaq	-696(%rbp), %r12
	leaq	-936(%rbp), %r14
.L114:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L110
	call	_ZdaPv@PLT
.L110:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L111
	call	_ZdaPv@PLT
	subq	$80, %r12
	cmpq	%r12, %r14
	jne	.L114
.L112:
	movq	-992(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L115
	call	_ZdlPv@PLT
.L115:
	movq	-1016(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L116
	call	_ZdlPv@PLT
.L116:
	movq	-1152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L117
	call	_ZdlPv@PLT
.L117:
	movq	-1176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L118
	call	_ZdlPv@PLT
.L118:
	movl	%r15d, 12616(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$1208, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	subq	$80, %r12
	cmpq	%r12, %r14
	jne	.L114
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L95:
	movb	$1, 6464(%r15)
	leaq	6440(%r15), %rdi
	movl	%edx, -1240(%rbp)
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movl	-1240(%rbp), %edx
	movq	%rax, 6456(%r15)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L92:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L175
.L94:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L99:
	movl	-1244(%rbp), %eax
	testl	%eax, %eax
	jne	.L120
	movq	112(%r12), %rcx
	movq	80(%r12), %rdx
	leaq	176(%r12), %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal30PendingCompilationErrorHandler12ReportErrorsEPNS0_7IsolateENS0_6HandleINS0_6ScriptEEEPNS0_15AstValueFactoryE@PLT
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%rdx, %rdi
	movq	%rsi, -1240(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-1240(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L94
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21098:
	.size	_ZN2v88internal7parsing13ParseFunctionEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE, .-_ZN2v88internal7parsing13ParseFunctionEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE
	.section	.text._ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE
	.type	_ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE, @function
_ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE:
.LFB21099:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	%rdx, %rsi
	testb	$1, 8(%rdi)
	je	.L177
	movl	%ecx, %edx
	jmp	_ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%r8, %rsi
	jmp	_ZN2v88internal7parsing13ParseFunctionEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE
	.cfi_endproc
.LFE21099:
	.size	_ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE, .-_ZN2v88internal7parsing8ParseAnyEPNS0_9ParseInfoENS0_6HandleINS0_18SharedFunctionInfoEEEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE, @function
_GLOBAL__sub_I__ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE:
.LFB27251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE27251:
	.size	_GLOBAL__sub_I__ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE, .-_GLOBAL__sub_I__ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal7parsing12ParseProgramEPNS0_9ParseInfoEPNS0_7IsolateENS1_29ReportErrorsAndStatisticsModeE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
