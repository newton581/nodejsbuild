	.file	"combined-heap.cc"
	.text
	.section	.text._ZN2v88internal26CombinedHeapObjectIteratorC2EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26CombinedHeapObjectIteratorC2EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE
	.type	_ZN2v88internal26CombinedHeapObjectIteratorC2EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE, @function
_ZN2v88internal26CombinedHeapObjectIteratorC2EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE:
.LFB19474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN2v88internal18HeapObjectIteratorC1EPNS0_4HeapENS1_20HeapObjectsFilteringE@PLT
	movq	3200(%r12), %rsi
	leaq	40(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal26ReadOnlyHeapObjectIteratorC1EPNS0_12ReadOnlyHeapE@PLT
	.cfi_endproc
.LFE19474:
	.size	_ZN2v88internal26CombinedHeapObjectIteratorC2EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE, .-_ZN2v88internal26CombinedHeapObjectIteratorC2EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE
	.globl	_ZN2v88internal26CombinedHeapObjectIteratorC1EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE
	.set	_ZN2v88internal26CombinedHeapObjectIteratorC1EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE,_ZN2v88internal26CombinedHeapObjectIteratorC2EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE
	.section	.text._ZN2v88internal26CombinedHeapObjectIterator4NextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26CombinedHeapObjectIterator4NextEv
	.type	_ZN2v88internal26CombinedHeapObjectIterator4NextEv, @function
_ZN2v88internal26CombinedHeapObjectIterator4NextEv:
.LFB19476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$40, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal26ReadOnlyHeapObjectIterator4NextEv@PLT
	testq	%rax, %rax
	je	.L9
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal18HeapObjectIterator4NextEv@PLT
	.cfi_endproc
.LFE19476:
	.size	_ZN2v88internal26CombinedHeapObjectIterator4NextEv, .-_ZN2v88internal26CombinedHeapObjectIterator4NextEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal26CombinedHeapObjectIteratorC2EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal26CombinedHeapObjectIteratorC2EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE, @function
_GLOBAL__sub_I__ZN2v88internal26CombinedHeapObjectIteratorC2EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE:
.LFB24339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24339:
	.size	_GLOBAL__sub_I__ZN2v88internal26CombinedHeapObjectIteratorC2EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE, .-_GLOBAL__sub_I__ZN2v88internal26CombinedHeapObjectIteratorC2EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal26CombinedHeapObjectIteratorC2EPNS0_4HeapENS0_18HeapObjectIterator20HeapObjectsFilteringE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
