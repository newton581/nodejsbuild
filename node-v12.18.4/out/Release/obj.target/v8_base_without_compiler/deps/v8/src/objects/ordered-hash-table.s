	.file	"ordered-hash-table.cc"
	.text
	.section	.rodata._ZN2v88internal14OrderedHashSet18ConvertToKeysArrayEPNS0_7IsolateENS0_6HandleIS1_EENS0_17GetKeysConversionE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"key.IsName()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal14OrderedHashSet18ConvertToKeysArrayEPNS0_7IsolateENS0_6HandleIS1_EENS0_17GetKeysConversionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14OrderedHashSet18ConvertToKeysArrayEPNS0_7IsolateENS0_6HandleIS1_EENS0_17GetKeysConversionE
	.type	_ZN2v88internal14OrderedHashSet18ConvertToKeysArrayEPNS0_7IsolateENS0_6HandleIS1_EENS0_17GetKeysConversionE, @function
_ZN2v88internal14OrderedHashSet18ConvertToKeysArrayEPNS0_7IsolateENS0_6HandleIS1_EENS0_17GetKeysConversionE:
.LFB19695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%rdi, -88(%rbp)
	movq	(%rsi), %rdi
	movq	15(%rdi), %rax
	sarq	$32, %rax
	movq	%rax, -96(%rbp)
	movl	%eax, -52(%rbp)
	movq	31(%rdi), %r15
	movq	152(%rcx), %rdx
	sarq	$32, %r15
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	jne	.L48
.L2:
	movq	-88(%rbp), %rax
	movq	37672(%rax), %rdx
	movq	%rdx, %rax
	shrq	$9, %rax
	cmpq	$8389119, %rdx
	movl	$16384, %edx
	cmova	%rdx, %rax
	movl	$512, %edx
	cmpq	$512, %rax
	cmovb	%rdx, %rax
	addl	%eax, %eax
	cmpq	$0, -96(%rbp)
	movl	%eax, -56(%rbp)
	jle	.L26
	leal	40(,%r15,8), %r8d
	xorl	%r12d, %r12d
	movq	%r14, %r15
	movslq	%r8d, %r13
	movl	%r12d, %eax
	movq	%r13, %r12
	movl	%eax, %r13d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L50:
	sarq	$32, %rdx
	js	.L22
	xorl	%r11d, %r11d
	cmpl	%r13d, -56(%rbp)
	movl	%edx, %r14d
	setg	%r11b
.L9:
	movq	%rdx, %rsi
	movq	-88(%rbp), %rdi
	movl	%r11d, %edx
	salq	$32, %rsi
	call	_ZN2v88internal7Factory14NumberToStringENS0_3SmiEb@PLT
	movq	%rax, %r10
	movq	(%r10), %rdx
	movl	11(%rdx), %esi
	cmpl	$10, %esi
	jle	.L44
.L46:
	movq	(%r15), %rdi
.L6:
	leal	16(,%r13,8), %eax
	cltq
	leaq	-1(%rax,%rdi), %r14
	movq	%rdx, (%r14)
	testb	$1, %dl
	je	.L28
.L30:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rsi
	movq	%rax, -64(%rbp)
	testl	$262144, %esi
	je	.L24
	movq	%r14, %rsi
	movq	%rdx, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rax
	movq	-80(%rbp), %rdx
	movq	-72(%rbp), %rdi
	movq	8(%rax), %rsi
.L24:
	andl	$24, %esi
	je	.L28
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L28
	movq	%r14, %rsi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	addl	$1, %r13d
	addq	$16, %r12
	cmpl	-52(%rbp), %r13d
	je	.L49
.L27:
	movq	(%r15), %rdi
	movq	-1(%r12,%rdi), %rdx
	testl	%ebx, %ebx
	jne	.L6
	testb	$1, %dl
	je	.L50
	movq	-1(%rdx), %rax
	leaq	-1(%rdx), %r10
	cmpw	$65, 11(%rax)
	je	.L51
.L11:
	movq	(%r10), %rax
	cmpw	$64, 11(%rax)
	ja	.L22
	leal	16(,%r13,8), %eax
	cltq
	leaq	-1(%rax,%rdi), %r14
	movq	%rdx, (%r14)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r15, %r14
.L26:
	movl	-96(%rbp), %edx
	movq	-88(%rbp), %rdi
	addq	$56, %rsp
	movq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10FixedArray13ShrinkOrEmptyEPNS0_7IsolateENS0_6HandleIS1_EEi@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movsd	7(%rdx), %xmm0
	movsd	.LC2(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movq	%xmm2, %rax
	movq	%xmm2, %rsi
	shrq	$32, %rax
	cmpq	$1127219200, %rax
	jne	.L11
	movl	%esi, %eax
	pxor	%xmm1, %xmm1
	movd	%xmm2, %r14d
	cvtsi2sdq	%rax, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L11
	jne	.L11
	cmpl	$-1, %esi
	je	.L11
	xorl	%r10d, %r10d
	cmpl	%r13d, -56(%rbp)
	movl	%esi, %edx
	setg	%r10b
	movl	%r10d, %r11d
	testl	%esi, %esi
	jns	.L9
	movq	-88(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r10d, -64(%rbp)
	call	_ZN2v88internal7Factory17NewNumberFromUintEjNS0_14AllocationTypeE@PLT
	movl	-64(%rbp), %r10d
	movq	-88(%rbp), %rdi
	movq	%rax, %rsi
	movl	%r10d, %edx
	call	_ZN2v88internal7Factory14NumberToStringENS0_6HandleINS0_6ObjectEEEb@PLT
	movq	%rax, %r10
	movq	(%r10), %rdx
	movl	11(%rdx), %esi
	cmpl	$10, %esi
	jg	.L46
.L44:
	cmpl	$3, 7(%rdx)
	jne	.L46
	movl	%r14d, %edi
	movq	%r10, -64(%rbp)
	call	_ZN2v88internal12StringHasher18MakeArrayIndexHashEji@PLT
	movq	-64(%rbp), %r10
	movq	(%r10), %rdx
	movl	%eax, 7(%rdx)
	movq	(%r10), %rdx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L48:
	testb	$1, %dl
	je	.L2
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L2
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2
	.cfi_endproc
.LFE19695:
	.size	_ZN2v88internal14OrderedHashSet18ConvertToKeysArrayEPNS0_7IsolateENS0_6HandleIS1_EENS0_17GetKeysConversionE, .-_ZN2v88internal14OrderedHashSet18ConvertToKeysArrayEPNS0_7IsolateENS0_6HandleIS1_EENS0_17GetKeysConversionE
	.section	.text._ZN2v88internal14OrderedHashSet8GetEmptyENS0_13ReadOnlyRootsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14OrderedHashSet8GetEmptyENS0_13ReadOnlyRootsE
	.type	_ZN2v88internal14OrderedHashSet8GetEmptyENS0_13ReadOnlyRootsE, @function
_ZN2v88internal14OrderedHashSet8GetEmptyENS0_13ReadOnlyRootsE:
.LFB19696:
	.cfi_startproc
	endbr64
	movq	976(%rdi), %rax
	ret
	.cfi_endproc
.LFE19696:
	.size	_ZN2v88internal14OrderedHashSet8GetEmptyENS0_13ReadOnlyRootsE, .-_ZN2v88internal14OrderedHashSet8GetEmptyENS0_13ReadOnlyRootsE
	.section	.text._ZN2v88internal14OrderedHashMap8GetEmptyENS0_13ReadOnlyRootsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14OrderedHashMap8GetEmptyENS0_13ReadOnlyRootsE
	.type	_ZN2v88internal14OrderedHashMap8GetEmptyENS0_13ReadOnlyRootsE, @function
_ZN2v88internal14OrderedHashMap8GetEmptyENS0_13ReadOnlyRootsE:
.LFB19697:
	.cfi_startproc
	endbr64
	movq	968(%rdi), %rax
	ret
	.cfi_endproc
.LFE19697:
	.size	_ZN2v88internal14OrderedHashMap8GetEmptyENS0_13ReadOnlyRootsE, .-_ZN2v88internal14OrderedHashMap8GetEmptyENS0_13ReadOnlyRootsE
	.section	.text._ZN2v88internal14OrderedHashMap7GetHashEPNS0_7IsolateEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14OrderedHashMap7GetHashEPNS0_7IsolateEm
	.type	_ZN2v88internal14OrderedHashMap7GetHashEPNS0_7IsolateEm, @function
_ZN2v88internal14OrderedHashMap7GetHashEPNS0_7IsolateEm:
.LFB19703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	$1, %sil
	jne	.L55
	sarq	$32, %rsi
	movl	%esi, %eax
	sall	$15, %eax
	subl	%esi, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
	salq	$32, %rax
.L79:
	cmpq	%rax, 88(%rbx)
	je	.L94
.L54:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L95
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	cmpw	$65, 11(%rax)
	je	.L57
	movq	-1(%rsi), %rax
	cmpw	$64, 11(%rax)
	ja	.L96
	movl	7(%rsi), %eax
	testb	$1, %al
	je	.L93
	leaq	-32(%rbp), %rdi
	movq	%rsi, -32(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
.L92:
	salq	$32, %rax
	cmpq	%rax, 88(%rbx)
	jne	.L54
.L94:
	movabsq	$-4294967296, %rax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L96:
	movq	-1(%rsi), %rax
	cmpw	$67, 11(%rax)
	je	.L97
	movq	-1(%rsi), %rax
	cmpw	$66, 11(%rax)
	je	.L98
	movq	-1(%rsi), %rax
	cmpw	$160, 11(%rax)
	je	.L99
	leaq	-32(%rbp), %rdi
	movq	%rsi, -32(%rbp)
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L57:
	movabsq	$9223372032559808512, %rax
	movq	7(%rsi), %rdx
	movq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L79
	comisd	.LC3(%rip), %xmm0
	jb	.L62
	movsd	.LC4(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L62
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L62
	jne	.L62
	movl	%ecx, %eax
	sall	$15, %eax
	subl	%ecx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	andl	$1073741823, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L93:
	shrl	$2, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L98:
	movl	7(%rsi), %edx
	xorl	%eax, %eax
	andl	$2147483646, %edx
	je	.L79
	movq	15(%rsi), %rdx
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	andl	$1073741823, %eax
	salq	$32, %rax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L97:
	movq	15(%rsi), %rdx
	movl	7(%rdx), %eax
	testb	$1, %al
	je	.L93
	leaq	-32(%rbp), %rdi
	movq	%rdx, -32(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L99:
	leaq	-32(%rbp), %rdi
	movq	%rsi, -32(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	andl	$2147483647, %eax
	jmp	.L92
.L95:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19703:
	.size	_ZN2v88internal14OrderedHashMap7GetHashEPNS0_7IsolateEm, .-_ZN2v88internal14OrderedHashMap7GetHashEPNS0_7IsolateEm
	.section	.text._ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE9FindEntryEPNS0_7IsolateENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	.type	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE9FindEntryEPNS0_7IsolateENS0_6ObjectE, @function
_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE9FindEntryEPNS0_7IsolateENS0_6ObjectE:
.LFB19709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L101
	shrl	$2, %eax
	movl	%eax, %edx
.L102:
	movq	(%r12), %rcx
	movq	39(%rcx), %rax
	sarq	$32, %rax
	subl	$1, %eax
	andl	%edx, %eax
	leal	48(,%rax,8), %eax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L114:
	movq	39(%rcx), %rax
	leal	0(,%r8,4), %edx
	sarq	$32, %rax
	leal	6(%rdx,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rcx,%rax), %rax
	cmpq	%rax, %rbx
	je	.L100
	movq	39(%rcx), %rax
	sarq	$32, %rax
	leal	9(%rdx,%rax), %eax
	sall	$3, %eax
.L113:
	cltq
	movq	-1(%rcx,%rax), %rax
	sarq	$32, %rax
	movl	%eax, %r8d
	cmpq	$-1, %rax
	jne	.L114
	movl	$-1, %r8d
.L100:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	leaq	-32(%rbp), %rdi
	movq	%rdx, -32(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	%eax, %edx
	jmp	.L102
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19709:
	.size	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE9FindEntryEPNS0_7IsolateENS0_6ObjectE, .-_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	.section	.text._ZN2v88internal21OrderedNameDictionary8SetEntryEPNS0_7IsolateEiNS0_6ObjectES4_NS0_15PropertyDetailsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21OrderedNameDictionary8SetEntryEPNS0_7IsolateEiNS0_6ObjectES4_NS0_15PropertyDetailsE
	.type	_ZN2v88internal21OrderedNameDictionary8SetEntryEPNS0_7IsolateEiNS0_6ObjectES4_NS0_15PropertyDetailsE, @function
_ZN2v88internal21OrderedNameDictionary8SetEntryEPNS0_7IsolateEiNS0_6ObjectES4_NS0_15PropertyDetailsE:
.LFB19715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	39(%rdx), %rax
	sarq	$32, %rax
	leal	6(%rax,%r10,4), %ebx
	sall	$3, %ebx
	movslq	%ebx, %rcx
	movq	%r15, -1(%rcx,%rdx)
	movq	(%rdi), %rdi
	leaq	-1(%rdi), %rax
	testb	$1, %r15b
	je	.L124
	movq	%r15, %r8
	movq	%rcx, -64(%rbp)
	leaq	(%rcx,%rax), %rsi
	andq	$-262144, %r8
	movq	8(%r8), %rdx
	movq	%r8, -56(%rbp)
	testl	$262144, %edx
	jne	.L138
	andl	$24, %edx
	je	.L124
.L143:
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L139
	.p2align 4,,10
	.p2align 3
.L124:
	leal	8(%rbx), %r15d
	movslq	%r15d, %r15
	movq	%r14, (%r15,%rax)
	movq	0(%r13), %rdi
	leaq	-1(%rdi), %rax
	testb	$1, %r14b
	je	.L123
	movq	%r14, %rcx
	leaq	(%r15,%rax), %rsi
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -56(%rbp)
	testl	$262144, %edx
	jne	.L140
	andl	$24, %edx
	je	.L123
.L142:
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L141
.L123:
	addl	%r12d, %r12d
	addl	$16, %ebx
	sarl	%r12d
	movslq	%ebx, %rbx
	salq	$32, %r12
	movq	%r12, (%rbx,%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	%r14, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	0(%r13), %rdi
	movq	8(%rcx), %rdx
	leaq	-1(%rdi), %rax
	leaq	(%r15,%rax), %rsi
	andl	$24, %edx
	jne	.L142
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r15, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r8
	movq	0(%r13), %rdi
	movq	-64(%rbp), %rcx
	movq	8(%r8), %rdx
	leaq	-1(%rdi), %rax
	leaq	(%rcx,%rax), %rsi
	andl	$24, %edx
	jne	.L143
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rax
	subq	$1, %rax
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rax
	subq	$1, %rax
	jmp	.L123
	.cfi_endproc
.LFE19715:
	.size	_ZN2v88internal21OrderedNameDictionary8SetEntryEPNS0_7IsolateEiNS0_6ObjectES4_NS0_15PropertyDetailsE, .-_ZN2v88internal21OrderedNameDictionary8SetEntryEPNS0_7IsolateEiNS0_6ObjectES4_NS0_15PropertyDetailsE
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE9FindEntryEPNS0_7IsolateENS0_6ObjectE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE9FindEntryEPNS0_7IsolateENS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE9FindEntryEPNS0_7IsolateENS0_6ObjectE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE9FindEntryEPNS0_7IsolateENS0_6ObjectE:
.LFB21986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$1, %edx
	jne	.L145
	movq	%rbx, %rdx
	sarq	$32, %rdx
	movl	%edx, %eax
	sall	$15, %eax
	subl	%edx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	movq	(%rdi), %rdx
	imull	$2057, %eax, %eax
	movq	31(%rdx), %rcx
	movl	%eax, %esi
	sarq	$32, %rcx
	shrl	$16, %esi
	subl	$1, %ecx
	xorl	%esi, %eax
	andl	%ecx, %eax
	leal	40(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %r14
	shrq	$32, %r14
.L174:
	cmpl	$-1, %r14d
	je	.L172
	movq	(%r12), %rdx
	leaq	-64(%rbp), %r13
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L195:
	movq	(%r12), %rdx
	movq	31(%rdx), %rax
	sarq	$32, %rax
	leal	6(%r15,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	movl	%eax, %r14d
	cmpq	$-1, %rax
	je	.L172
.L146:
	movq	31(%rdx), %rax
	leal	(%r14,%r14), %r15d
	movq	%rbx, %rsi
	movq	%r13, %rdi
	sarq	$32, %rax
	leal	5(%r15,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object13SameValueZeroES1_@PLT
	testb	%al, %al
	je	.L195
.L144:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L202:
	.cfi_restore_state
	subl	$1, 41104(%r15)
	movq	%rsi, 41088(%r15)
	cmpq	41096(%r15), %rcx
	je	.L172
	movq	%rcx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	movl	$-1, %r14d
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%rsi, %r15
	movq	41088(%rsi), %rsi
	movq	41096(%r15), %rcx
	addl	$1, 41104(%r15)
	movq	-1(%rbx), %rax
	cmpw	$65, 11(%rax)
	je	.L148
	movq	-1(%rbx), %rax
	cmpw	$64, 11(%rax)
	ja	.L197
	movl	7(%rbx), %edx
	testb	$1, %dl
	jne	.L162
	shrl	$2, %edx
.L194:
	salq	$32, %rdx
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L148:
	movq	7(%rbx), %rdx
	movq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L198
	comisd	.LC3(%rip), %xmm0
	jb	.L153
	movsd	.LC4(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L153
	cvttsd2sil	%xmm0, %edi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L153
	jne	.L153
	movl	%edi, %eax
	sall	$15, %eax
	subl	%edi, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%eax, %edx
	andl	$1073741823, %edx
	jmp	.L194
.L197:
	movq	-1(%rbx), %rax
	cmpw	$67, 11(%rax)
	je	.L199
	movq	-1(%rbx), %rax
	cmpw	$66, 11(%rax)
	je	.L200
	movq	-1(%rbx), %rax
	cmpw	$160, 11(%rax)
	je	.L201
	leaq	-64(%rbp), %r13
	movq	%rcx, -80(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rcx
	movq	%rax, %rdx
.L171:
	cmpq	88(%r15), %rdx
	je	.L202
	movq	(%r12), %rdi
	sarq	$32, %rdx
	movq	31(%rdi), %rax
	sarq	$32, %rax
	subl	$1, %eax
	andl	%edx, %eax
	leal	40(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdi), %r14
	subl	$1, 41104(%r15)
	movq	%rsi, 41088(%r15)
	shrq	$32, %r14
	cmpq	41096(%r15), %rcx
	je	.L174
	movq	%rcx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L174
.L162:
	leaq	-64(%rbp), %r13
	movq	%rcx, -80(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movl	%eax, %edx
	jmp	.L194
.L153:
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rax, %rdx
	andl	$1073741823, %edx
	jmp	.L194
.L199:
	movq	15(%rbx), %rdx
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L166
	shrl	$2, %eax
.L167:
	movq	%rax, %rdx
	salq	$32, %rdx
	jmp	.L171
.L200:
	movl	7(%rbx), %eax
	xorl	%edx, %edx
	testl	$2147483646, %eax
	je	.L171
	movq	15(%rbx), %rdx
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rax, %rdx
	andl	$1073741823, %edx
	jmp	.L194
.L166:
	leaq	-64(%rbp), %r13
	movq	%rcx, -80(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rsi
	jmp	.L167
.L201:
	leaq	-64(%rbp), %r13
	movq	%rcx, -80(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rcx
	movl	%eax, %edx
	andl	$2147483647, %edx
	salq	$32, %rdx
	jmp	.L171
.L198:
	movabsq	$9223372032559808512, %rdx
	jmp	.L171
.L196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21986:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE9FindEntryEPNS0_7IsolateENS0_6ObjectE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE:
.LFB21984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	leaq	-8(%rbp), %rdi
	movq	%rsi, -8(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	leave
	.cfi_def_cfa 7, 8
	cmpl	$-1, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE21984:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE:
.LFB21985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-56(%rbp), %rdi
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rsi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	xorl	%r8d, %r8d
	cmpl	$-1, %eax
	jne	.L221
.L205:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	-56(%rbp), %rdx
	movq	15(%rdx), %r12
	movq	23(%rdx), %r13
	movq	31(%rdx), %rcx
	movq	96(%rbx), %r14
	sarq	$32, %r12
	sarq	$32, %r13
	sarq	$32, %rcx
	leal	5(%rcx,%rax,2), %ebx
	sall	$3, %ebx
	movslq	%ebx, %rbx
	movq	%r14, -1(%rbx,%rdx)
	movq	-56(%rbp), %rdi
	testb	$1, %r14b
	je	.L210
	movq	%r14, %r15
	leaq	-1(%rbx,%rdi), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L222
	testb	$24, %al
	je	.L210
.L223:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L210
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L210:
	subl	$1, %r12d
	addl	$1, %r13d
	movl	$1, %r8d
	salq	$32, %r12
	salq	$32, %r13
	movq	%r12, 15(%rdi)
	movq	-56(%rbp), %rax
	movq	%r13, 23(%rax)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%r14, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rdi
	movq	8(%r15), %rax
	leaq	-1(%rbx,%rdi), %rsi
	testb	$24, %al
	jne	.L223
	jmp	.L210
	.cfi_endproc
.LFE21985:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE9FindEntryEPNS0_7IsolateENS0_6ObjectE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE9FindEntryEPNS0_7IsolateENS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE9FindEntryEPNS0_7IsolateENS0_6ObjectE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE9FindEntryEPNS0_7IsolateENS0_6ObjectE:
.LFB21991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	andl	$1, %edx
	jne	.L225
	movq	%rbx, %rdx
	sarq	$32, %rdx
	movl	%edx, %eax
	sall	$15, %eax
	subl	%edx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	movq	(%rdi), %rdx
	imull	$2057, %eax, %eax
	movq	31(%rdx), %rcx
	movl	%eax, %esi
	sarq	$32, %rcx
	shrl	$16, %esi
	subl	$1, %ecx
	xorl	%esi, %eax
	andl	%ecx, %eax
	leal	40(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %r14
	shrq	$32, %r14
.L254:
	cmpl	$-1, %r14d
	je	.L252
	movq	(%r12), %rdx
	leaq	-64(%rbp), %r13
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L275:
	movq	(%r12), %rdx
	movq	31(%rdx), %rax
	sarq	$32, %rax
	leal	7(%r15,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	movl	%eax, %r14d
	cmpq	$-1, %rax
	je	.L252
.L226:
	movq	31(%rdx), %rax
	leal	(%r14,%r14,2), %r15d
	movq	%rbx, %rsi
	movq	%r13, %rdi
	sarq	$32, %rax
	leal	5(%r15,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object13SameValueZeroES1_@PLT
	testb	%al, %al
	je	.L275
.L224:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	addq	$40, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L282:
	.cfi_restore_state
	subl	$1, 41104(%r15)
	movq	%rsi, 41088(%r15)
	cmpq	41096(%r15), %rcx
	je	.L252
	movq	%rcx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L252:
	movl	$-1, %r14d
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L225:
	movq	%rsi, %r15
	movq	41088(%rsi), %rsi
	movq	41096(%r15), %rcx
	addl	$1, 41104(%r15)
	movq	-1(%rbx), %rax
	cmpw	$65, 11(%rax)
	je	.L228
	movq	-1(%rbx), %rax
	cmpw	$64, 11(%rax)
	ja	.L277
	movl	7(%rbx), %edx
	testb	$1, %dl
	jne	.L242
	shrl	$2, %edx
.L274:
	salq	$32, %rdx
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L228:
	movq	7(%rbx), %rdx
	movq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L278
	comisd	.LC3(%rip), %xmm0
	jb	.L233
	movsd	.LC4(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L233
	cvttsd2sil	%xmm0, %edi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%edi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L233
	jne	.L233
	movl	%edi, %eax
	sall	$15, %eax
	subl	%edi, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%eax, %edx
	andl	$1073741823, %edx
	jmp	.L274
.L277:
	movq	-1(%rbx), %rax
	cmpw	$67, 11(%rax)
	je	.L279
	movq	-1(%rbx), %rax
	cmpw	$66, 11(%rax)
	je	.L280
	movq	-1(%rbx), %rax
	cmpw	$160, 11(%rax)
	je	.L281
	leaq	-64(%rbp), %r13
	movq	%rcx, -80(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rcx
	movq	%rax, %rdx
.L251:
	cmpq	88(%r15), %rdx
	je	.L282
	movq	(%r12), %rdi
	sarq	$32, %rdx
	movq	31(%rdi), %rax
	sarq	$32, %rax
	subl	$1, %eax
	andl	%edx, %eax
	leal	40(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdi), %r14
	subl	$1, 41104(%r15)
	movq	%rsi, 41088(%r15)
	shrq	$32, %r14
	cmpq	41096(%r15), %rcx
	je	.L254
	movq	%rcx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
	jmp	.L254
.L242:
	leaq	-64(%rbp), %r13
	movq	%rcx, -80(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rsi
	movl	%eax, %edx
	jmp	.L274
.L233:
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rax, %rdx
	andl	$1073741823, %edx
	jmp	.L274
.L279:
	movq	15(%rbx), %rdx
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L246
	shrl	$2, %eax
.L247:
	movq	%rax, %rdx
	salq	$32, %rdx
	jmp	.L251
.L280:
	movl	7(%rbx), %eax
	xorl	%edx, %edx
	testl	$2147483646, %eax
	je	.L251
	movq	15(%rbx), %rdx
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rax, %rdx
	andl	$1073741823, %edx
	jmp	.L274
.L246:
	leaq	-64(%rbp), %r13
	movq	%rcx, -80(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rsi
	jmp	.L247
.L281:
	leaq	-64(%rbp), %r13
	movq	%rcx, -80(%rbp)
	movq	%r13, %rdi
	movq	%rsi, -72(%rbp)
	movq	%rbx, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rcx
	movl	%eax, %edx
	andl	$2147483647, %edx
	salq	$32, %rdx
	jmp	.L251
.L278:
	movabsq	$9223372032559808512, %rdx
	jmp	.L251
.L276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21991:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE9FindEntryEPNS0_7IsolateENS0_6ObjectE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE:
.LFB21989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	leaq	-8(%rbp), %rdi
	movq	%rsi, -8(%rbp)
	movq	%r8, %rsi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	leave
	.cfi_def_cfa 7, 8
	cmpl	$-1, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE21989:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE:
.LFB21990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-56(%rbp), %rdi
	subq	$40, %rsp
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rsi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	xorl	%r8d, %r8d
	cmpl	$-1, %eax
	je	.L285
	movq	-56(%rbp), %rdi
	leal	(%rax,%rax,2), %eax
	movq	15(%rdi), %r13
	movq	23(%rdi), %r12
	movq	31(%rdi), %rdx
	movq	96(%rbx), %r14
	sarq	$32, %r13
	sarq	$32, %r12
	sarq	$32, %rdx
	movq	%r14, %r15
	movq	%r14, %r8
	leal	3(%rax,%rdx), %eax
	notq	%r15
	andq	$-262144, %r8
	leal	16(,%rax,8), %ebx
	cltq
	andl	$1, %r15d
	movslq	%ebx, %rbx
	leaq	32(,%rax,8), %rcx
.L290:
	movq	%r14, -1(%rdi,%rbx)
	movq	-56(%rbp), %rdi
	testb	%r15b, %r15b
	jne	.L291
	movq	8(%r8), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testl	$262144, %eax
	jne	.L303
.L288:
	testb	$24, %al
	je	.L291
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L304
	.p2align 4,,10
	.p2align 3
.L291:
	addq	$8, %rbx
	cmpq	%rbx, %rcx
	jne	.L290
	subl	$1, %r13d
	addl	$1, %r12d
	movl	$1, %r8d
	salq	$32, %r13
	salq	$32, %r12
	movq	%r13, 15(%rdi)
	movq	-56(%rbp), %rax
	movq	%r12, 23(%rax)
.L285:
	addq	$40, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rcx
	movq	8(%r8), %rax
	leaq	-1(%rdi,%rbx), %rsi
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%r14, %rdx
	movq	%r8, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rdi
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rcx
	jmp	.L291
	.cfi_endproc
.LFE21990:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE:
.LFB19720:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal7Factory22NewSmallOrderedHashSetEiNS0_14AllocationTypeE@PLT
	.cfi_endproc
.LFE19720:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE:
.LFB19721:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal7Factory22NewSmallOrderedHashMapEiNS0_14AllocationTypeE@PLT
	.cfi_endproc
.LFE19721:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE:
.LFB19722:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal7Factory29NewSmallOrderedNameDictionaryEiNS0_14AllocationTypeE@PLT
	.cfi_endproc
.LFE19722:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, .-_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE9FindEntryEPNS0_7IsolateENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE9FindEntryEPNS0_7IsolateENS0_6ObjectE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE9FindEntryEPNS0_7IsolateENS0_6ObjectE:
.LFB19736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L309
	shrl	$2, %eax
	movl	%eax, %edx
.L310:
	movq	(%r12), %r8
	movzbl	17(%r8), %ecx
	leaq	-1(%r8), %rsi
	leal	(%rcx,%rcx,2), %edi
	leal	-1(%rcx), %eax
	sall	$4, %edi
	andl	%edx, %eax
	addl	$24, %edi
	addl	%edi, %eax
	cltq
	movzbl	-1(%r8,%rax), %eax
	cmpl	$255, %eax
	je	.L308
	addl	%ecx, %edi
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L319:
	addl	%edi, %eax
	cltq
	movzbl	(%rax,%rsi), %eax
	cmpl	$255, %eax
	je	.L308
.L312:
	leal	3(%rax,%rax,2), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	cmpq	(%rcx,%rsi), %rbx
	jne	.L319
.L308:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L320
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	leaq	-32(%rbp), %rdi
	movq	%rdx, -32(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movl	%eax, %edx
	jmp	.L310
.L320:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19736:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE9FindEntryEPNS0_7IsolateENS0_6ObjectE, .-_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE10InitializeEPNS0_7IsolateEi,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE10InitializeEPNS0_7IsolateEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE10InitializeEPNS0_7IsolateEi
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE10InitializeEPNS0_7IsolateEi, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE10InitializeEPNS0_7IsolateEi:
.LFB22068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	shrl	$31, %edx
	addl	%ebx, %edx
	sarl	%edx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movb	%dl, 9(%rax)
	movq	(%rdi), %rax
	addl	%ebx, %edx
	movslq	%edx, %rdx
	movb	$0, 7(%rax)
	movq	(%rdi), %rax
	movb	$0, 8(%rax)
	movq	(%rdi), %rax
	movl	$0, 10(%rax)
	movb	$0, 14(%rax)
	movl	%ebx, %eax
	movq	(%rdi), %rsi
	sall	$4, %eax
	cltq
	leaq	15(%rsi,%rax), %rdi
	movl	$255, %esi
	call	memset@PLT
	movq	(%r15), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L322
	leal	(%rbx,%rbx), %ecx
	movq	96(%r14), %rax
	leaq	15(%rdx), %rdi
	movslq	%ecx, %rcx
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
.L321:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	testl	%ebx, %ebx
	jle	.L321
	leal	-1(%rbx), %eax
	movl	$32, %r13d
	addq	$3, %rax
	salq	$4, %rax
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L330:
	leaq	-16(%r13), %rbx
.L329:
	movq	96(%r14), %r12
	movq	%r12, -1(%rdx,%rbx)
	testb	$1, %r12b
	je	.L331
	movq	%r12, %r8
	movq	(%r15), %rdi
	andq	$-262144, %r8
	movq	8(%r8), %rax
	movq	%r8, -56(%rbp)
	leaq	-1(%rdi,%rbx), %rsi
	testl	$262144, %eax
	je	.L326
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r8
	movq	(%r15), %rdi
	movq	8(%r8), %rax
	leaq	-1(%rdi,%rbx), %rsi
.L326:
	testb	$24, %al
	je	.L331
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L331
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L331:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L328
	movq	(%r15), %rdx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L328:
	addq	$16, %r13
	cmpq	%r13, -64(%rbp)
	je	.L321
	movq	(%r15), %rdx
	jmp	.L330
	.cfi_endproc
.LFE22068:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE10InitializeEPNS0_7IsolateEi, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE10InitializeEPNS0_7IsolateEi
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE10InitializeEPNS0_7IsolateEi,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE10InitializeEPNS0_7IsolateEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE10InitializeEPNS0_7IsolateEi
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE10InitializeEPNS0_7IsolateEi, @function
_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE10InitializeEPNS0_7IsolateEi:
.LFB22073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	sarl	%eax
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$255, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	leal	(%rbx,%rbx,2), %r12d
	subq	$24, %rsp
	movq	(%rdi), %rdx
	movb	%al, 17(%rdx)
	movq	(%rdi), %rdx
	addl	%ebx, %eax
	movb	$0, 15(%rdx)
	movq	(%rdi), %rdx
	movb	$0, 16(%rdx)
	movq	(%rdi), %rdx
	movl	$0, 18(%rdx)
	movb	$0, 22(%rdx)
	movslq	%eax, %rdx
	movq	(%rdi), %rcx
	leal	0(,%r12,8), %eax
	cltq
	leaq	23(%rcx,%rax), %rdi
	call	memset@PLT
	movq	(%r15), %rdx
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L340
	movq	96(%r13), %rax
	movslq	%r12d, %rcx
	leaq	23(%rdx), %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
.L339:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	testl	%ebx, %ebx
	jle	.L339
	leal	-1(%rbx), %eax
	movl	$48, %r14d
	leaq	(%rax,%rax,2), %rax
	leaq	72(,%rax,8), %rax
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	-24(%r14), %rbx
.L347:
	movq	96(%r13), %r12
	movq	%r12, -1(%rdx,%rbx)
	testb	$1, %r12b
	je	.L349
	movq	%r12, %rcx
	movq	(%r15), %rdi
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -56(%rbp)
	leaq	-1(%rdi,%rbx), %rsi
	testl	$262144, %edx
	je	.L344
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	(%r15), %rdi
	movq	8(%rcx), %rdx
	leaq	-1(%rdi,%rbx), %rsi
.L344:
	andl	$24, %edx
	je	.L349
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L349
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L349:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L346
	movq	(%r15), %rdx
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L346:
	addq	$24, %r14
	cmpq	%r14, -64(%rbp)
	je	.L339
	movq	(%r15), %rdx
	jmp	.L348
	.cfi_endproc
.LFE22073:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE10InitializeEPNS0_7IsolateEi, .-_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE10InitializeEPNS0_7IsolateEi
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler9FindEntryEPNS0_7IsolateENS0_10HeapObjectENS0_4NameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler9FindEntryEPNS0_7IsolateENS0_10HeapObjectENS0_4NameE
	.type	_ZN2v88internal28OrderedNameDictionaryHandler9FindEntryEPNS0_7IsolateENS0_10HeapObjectENS0_4NameE, @function
_ZN2v88internal28OrderedNameDictionaryHandler9FindEntryEPNS0_7IsolateENS0_10HeapObjectENS0_4NameE:
.LFB19762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %r8
	movq	-1(%rsi), %rax
	cmpw	$163, 11(%rax)
	movq	%rsi, -16(%rbp)
	movq	%rdi, %rsi
	movq	%r8, %rdi
	je	.L363
	call	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE9FindEntryEPNS0_7IsolateENS0_6ObjectE
.L357:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L364
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	.cfi_restore_state
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	movl	$-1, %edx
	cmpl	$255, %eax
	cmove	%edx, %eax
	jmp	.L357
.L364:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19762:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler9FindEntryEPNS0_7IsolateENS0_10HeapObjectENS0_4NameE, .-_ZN2v88internal28OrderedNameDictionaryHandler9FindEntryEPNS0_7IsolateENS0_10HeapObjectENS0_4NameE
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler7ValueAtENS0_10HeapObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler7ValueAtENS0_10HeapObjectEi
	.type	_ZN2v88internal28OrderedNameDictionaryHandler7ValueAtENS0_10HeapObjectEi, @function
_ZN2v88internal28OrderedNameDictionaryHandler7ValueAtENS0_10HeapObjectEi:
.LFB19763:
	.cfi_startproc
	endbr64
	movq	-1(%rdi), %rax
	cmpw	$163, 11(%rax)
	je	.L368
	movq	39(%rdi), %rax
	sarq	$32, %rax
	leal	7(%rax,%rsi,4), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdi,%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	leal	3(%rsi,%rsi,2), %eax
	leal	8(,%rax,8), %eax
	cltq
	movq	-1(%rdi,%rax), %rax
	ret
	.cfi_endproc
.LFE19763:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler7ValueAtENS0_10HeapObjectEi, .-_ZN2v88internal28OrderedNameDictionaryHandler7ValueAtENS0_10HeapObjectEi
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler9DetailsAtENS0_10HeapObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler9DetailsAtENS0_10HeapObjectEi
	.type	_ZN2v88internal28OrderedNameDictionaryHandler9DetailsAtENS0_10HeapObjectEi, @function
_ZN2v88internal28OrderedNameDictionaryHandler9DetailsAtENS0_10HeapObjectEi:
.LFB19765:
	.cfi_startproc
	endbr64
	movq	-1(%rdi), %rax
	cmpw	$163, 11(%rax)
	je	.L372
	movq	39(%rdi), %rax
	sarq	$32, %rax
	leal	8(%rax,%rsi,4), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdi,%rax), %rax
	shrq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	leal	3(%rsi,%rsi,2), %eax
	leal	16(,%rax,8), %eax
	cltq
	movl	3(%rdi,%rax), %eax
	ret
	.cfi_endproc
.LFE19765:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler9DetailsAtENS0_10HeapObjectEi, .-_ZN2v88internal28OrderedNameDictionaryHandler9DetailsAtENS0_10HeapObjectEi
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler4HashENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler4HashENS0_10HeapObjectE
	.type	_ZN2v88internal28OrderedNameDictionaryHandler4HashENS0_10HeapObjectE, @function
_ZN2v88internal28OrderedNameDictionaryHandler4HashENS0_10HeapObjectE:
.LFB19767:
	.cfi_startproc
	endbr64
	movq	-1(%rdi), %rax
	cmpw	$163, 11(%rax)
	jne	.L374
	movl	7(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	movq	15(%rdi), %rax
	shrq	$32, %rax
	ret
	.cfi_endproc
.LFE19767:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler4HashENS0_10HeapObjectE, .-_ZN2v88internal28OrderedNameDictionaryHandler4HashENS0_10HeapObjectE
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler7SetHashENS0_10HeapObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler7SetHashENS0_10HeapObjectEi
	.type	_ZN2v88internal28OrderedNameDictionaryHandler7SetHashENS0_10HeapObjectEi, @function
_ZN2v88internal28OrderedNameDictionaryHandler7SetHashENS0_10HeapObjectEi:
.LFB19768:
	.cfi_startproc
	endbr64
	movq	-1(%rdi), %rax
	cmpw	$163, 11(%rax)
	je	.L379
	salq	$32, %rsi
	movq	%rsi, 15(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	movl	%esi, 7(%rdi)
	ret
	.cfi_endproc
.LFE19768:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler7SetHashENS0_10HeapObjectEi, .-_ZN2v88internal28OrderedNameDictionaryHandler7SetHashENS0_10HeapObjectEi
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler5KeyAtENS0_10HeapObjectEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler5KeyAtENS0_10HeapObjectEi
	.type	_ZN2v88internal28OrderedNameDictionaryHandler5KeyAtENS0_10HeapObjectEi, @function
_ZN2v88internal28OrderedNameDictionaryHandler5KeyAtENS0_10HeapObjectEi:
.LFB19769:
	.cfi_startproc
	endbr64
	movq	-1(%rdi), %rax
	cmpw	$163, 11(%rax)
	je	.L383
	movq	39(%rdi), %rax
	sarq	$32, %rax
	leal	6(%rax,%rsi,4), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdi,%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L383:
	leal	3(%rsi,%rsi,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdi,%rax), %rax
	ret
	.cfi_endproc
.LFE19769:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler5KeyAtENS0_10HeapObjectEi, .-_ZN2v88internal28OrderedNameDictionaryHandler5KeyAtENS0_10HeapObjectEi
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler16NumberOfElementsENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler16NumberOfElementsENS0_10HeapObjectE
	.type	_ZN2v88internal28OrderedNameDictionaryHandler16NumberOfElementsENS0_10HeapObjectE, @function
_ZN2v88internal28OrderedNameDictionaryHandler16NumberOfElementsENS0_10HeapObjectE:
.LFB19770:
	.cfi_startproc
	endbr64
	movq	-1(%rdi), %rax
	cmpw	$163, 11(%rax)
	jne	.L385
	movzbl	15(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	movq	23(%rdi), %rax
	shrq	$32, %rax
	ret
	.cfi_endproc
.LFE19770:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler16NumberOfElementsENS0_10HeapObjectE, .-_ZN2v88internal28OrderedNameDictionaryHandler16NumberOfElementsENS0_10HeapObjectE
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler8CapacityENS0_10HeapObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler8CapacityENS0_10HeapObjectE
	.type	_ZN2v88internal28OrderedNameDictionaryHandler8CapacityENS0_10HeapObjectE, @function
_ZN2v88internal28OrderedNameDictionaryHandler8CapacityENS0_10HeapObjectE:
.LFB19771:
	.cfi_startproc
	endbr64
	movq	-1(%rdi), %rax
	cmpw	$163, 11(%rax)
	jne	.L388
	movzbl	17(%rdi), %eax
	addl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L388:
	movq	39(%rdi), %rax
	sarq	$32, %rax
	addl	%eax, %eax
	ret
	.cfi_endproc
.LFE19771:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler8CapacityENS0_10HeapObjectE, .-_ZN2v88internal28OrderedNameDictionaryHandler8CapacityENS0_10HeapObjectE
	.section	.text._ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE8MoveNextEv,"axG",@progbits,_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE8MoveNextEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE8MoveNextEv
	.type	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE8MoveNextEv, @function
_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE8MoveNextEv:
.LFB22136:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	35(%rdx), %rax
	addl	$1, %eax
	salq	$32, %rax
	movq	%rax, 31(%rdx)
	ret
	.cfi_endproc
.LFE22136:
	.size	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE8MoveNextEv, .-_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE8MoveNextEv
	.section	.text._ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10CurrentKeyEv,"axG",@progbits,_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10CurrentKeyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10CurrentKeyEv
	.type	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10CurrentKeyEv, @function
_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10CurrentKeyEv:
.LFB22137:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	23(%rax), %rdx
	movslq	35(%rax), %rcx
	movq	31(%rdx), %rax
	sarq	$32, %rax
	leal	5(%rax,%rcx,2), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	ret
	.cfi_endproc
.LFE22137:
	.size	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10CurrentKeyEv, .-_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10CurrentKeyEv
	.section	.text._ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10TransitionEv,"axG",@progbits,_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10TransitionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10TransitionEv
	.type	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10TransitionEv, @function
_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10TransitionEv:
.LFB22138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r8
	movq	23(%r8), %r12
	movq	15(%r12), %rax
	testb	$1, %al
	je	.L392
	movl	35(%r8), %ebx
	movq	%rdi, %r13
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%rsi, %r12
.L394:
	movq	15(%r12), %rax
	testb	$1, %al
	je	.L395
	movq	15(%r12), %rsi
	testl	%ebx, %ebx
	jle	.L396
	movq	23(%r12), %rax
	sarq	$32, %rax
	cmpq	$-1, %rax
	jne	.L412
	xorl	%ebx, %ebx
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%r12, 23(%r8)
	movq	0(%r13), %rdi
	testb	$1, %r12b
	je	.L401
	movq	%r12, %r14
	leaq	23(%rdi), %rsi
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L399
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	movq	8(%r14), %rax
	leaq	23(%rdi), %rsi
.L399:
	testb	$24, %al
	je	.L401
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L401
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	.p2align 4,,10
	.p2align 3
.L401:
	salq	$32, %rbx
	movq	%rbx, 31(%rdi)
.L392:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore_state
	testq	%rax, %rax
	jle	.L396
	movl	%ebx, %edi
	leaq	39(%r12), %rdx
	movl	%ebx, %ecx
	subl	%eax, %edi
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L413:
	subl	$1, %ecx
	addq	$8, %rdx
	cmpl	%edi, %ecx
	je	.L404
.L397:
	movq	(%rdx), %rax
	sarq	$32, %rax
	cmpl	%eax, %ebx
	jg	.L413
.L404:
	movl	%ecx, %ebx
	jmp	.L396
	.cfi_endproc
.LFE22138:
	.size	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10TransitionEv, .-_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10TransitionEv
	.section	.text._ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE7HasMoreEv,"axG",@progbits,_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE7HasMoreEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE7HasMoreEv
	.type	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE7HasMoreEv, @function
_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE7HasMoreEv:
.LFB22135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	call	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE10TransitionEv
	movq	(%rbx), %rdi
	subq	$37592, %r12
	movq	23(%rdi), %rsi
	movslq	35(%rdi), %r8
	movq	15(%rsi), %rcx
	movq	23(%rsi), %rax
	sarq	$32, %rcx
	sarq	$32, %rax
	addl	%eax, %ecx
	cmpl	%ecx, %r8d
	jge	.L424
	movl	%r8d, %edx
	leaq	31(%rsi), %r8
	subq	$1, %rsi
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L416:
	addl	$1, %edx
	cmpl	%ecx, %edx
	je	.L415
.L419:
	movq	(%r8), %rax
	sarq	$32, %rax
	leal	5(%rax,%rdx,2), %eax
	sall	$3, %eax
	cltq
	movq	(%rax,%rsi), %rax
	cmpq	%rax, 96(%r12)
	je	.L416
	movq	%rdx, %rax
	salq	$32, %rax
	movq	%rax, 31(%rdi)
	movq	(%rbx), %rax
	cmpl	%edx, %ecx
	jle	.L418
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L424:
	.cfi_restore_state
	movl	%r8d, %ecx
	.p2align 4,,10
	.p2align 3
.L415:
	salq	$32, %rcx
	movq	%rcx, 31(%rdi)
	movq	(%rbx), %rax
.L418:
	movq	1032(%r12), %r12
	movq	%r12, 23(%rax)
	testb	$1, %r12b
	je	.L423
	movq	%r12, %r13
	movq	(%rbx), %rdi
	andq	$-262144, %r13
	movq	8(%r13), %rax
	leaq	23(%rdi), %rsi
	testl	$262144, %eax
	jne	.L434
.L421:
	testb	$24, %al
	je	.L423
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L435
.L423:
	xorl	%eax, %eax
.L436:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r13), %rax
	leaq	23(%rdi), %rsi
	jmp	.L421
.L435:
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	xorl	%eax, %eax
	jmp	.L436
	.cfi_endproc
.LFE22135:
	.size	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE7HasMoreEv, .-_ZN2v88internal24OrderedHashTableIteratorINS0_13JSSetIteratorENS0_14OrderedHashSetEE7HasMoreEv
	.section	.text._ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE8MoveNextEv,"axG",@progbits,_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE8MoveNextEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE8MoveNextEv
	.type	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE8MoveNextEv, @function
_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE8MoveNextEv:
.LFB22140:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	35(%rdx), %rax
	addl	$1, %eax
	salq	$32, %rax
	movq	%rax, 31(%rdx)
	ret
	.cfi_endproc
.LFE22140:
	.size	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE8MoveNextEv, .-_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE8MoveNextEv
	.section	.text._ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10CurrentKeyEv,"axG",@progbits,_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10CurrentKeyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10CurrentKeyEv
	.type	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10CurrentKeyEv, @function
_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10CurrentKeyEv:
.LFB22141:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	23(%rax), %rdx
	movslq	35(%rax), %rcx
	movq	31(%rdx), %rax
	leal	(%rcx,%rcx,2), %ecx
	sarq	$32, %rax
	leal	5(%rcx,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	ret
	.cfi_endproc
.LFE22141:
	.size	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10CurrentKeyEv, .-_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10CurrentKeyEv
	.section	.text._ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10TransitionEv,"axG",@progbits,_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10TransitionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10TransitionEv
	.type	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10TransitionEv, @function
_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10TransitionEv:
.LFB22142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r8
	movq	23(%r8), %r12
	movq	15(%r12), %rax
	testb	$1, %al
	je	.L439
	movl	35(%r8), %ebx
	movq	%rdi, %r13
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%rsi, %r12
.L441:
	movq	15(%r12), %rax
	testb	$1, %al
	je	.L442
	movq	15(%r12), %rsi
	testl	%ebx, %ebx
	jle	.L443
	movq	23(%r12), %rax
	sarq	$32, %rax
	cmpq	$-1, %rax
	jne	.L459
	xorl	%ebx, %ebx
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L442:
	movq	%r12, 23(%r8)
	movq	0(%r13), %rdi
	testb	$1, %r12b
	je	.L448
	movq	%r12, %r14
	leaq	23(%rdi), %rsi
	andq	$-262144, %r14
	movq	8(%r14), %rax
	testl	$262144, %eax
	je	.L446
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	movq	8(%r14), %rax
	leaq	23(%rdi), %rsi
.L446:
	testb	$24, %al
	je	.L448
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L448
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	.p2align 4,,10
	.p2align 3
.L448:
	salq	$32, %rbx
	movq	%rbx, 31(%rdi)
.L439:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	testq	%rax, %rax
	jle	.L443
	movl	%ebx, %edi
	leaq	39(%r12), %rdx
	movl	%ebx, %ecx
	subl	%eax, %edi
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L460:
	subl	$1, %ecx
	addq	$8, %rdx
	cmpl	%edi, %ecx
	je	.L451
.L444:
	movq	(%rdx), %rax
	sarq	$32, %rax
	cmpl	%eax, %ebx
	jg	.L460
.L451:
	movl	%ecx, %ebx
	jmp	.L443
	.cfi_endproc
.LFE22142:
	.size	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10TransitionEv, .-_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10TransitionEv
	.section	.text._ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE7HasMoreEv,"axG",@progbits,_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE7HasMoreEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE7HasMoreEv
	.type	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE7HasMoreEv, @function
_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE7HasMoreEv:
.LFB22139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r12
	call	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE10TransitionEv
	movq	(%rbx), %r9
	subq	$37592, %r12
	movq	23(%r9), %rdi
	movslq	35(%r9), %rcx
	movq	15(%rdi), %rsi
	movq	23(%rdi), %rax
	sarq	$32, %rsi
	sarq	$32, %rax
	addl	%eax, %esi
	cmpl	%esi, %ecx
	jge	.L471
	leaq	31(%rdi), %r8
	movl	%ecx, %edx
	subq	$1, %rdi
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L463:
	addl	$1, %edx
	cmpl	%esi, %edx
	je	.L462
.L466:
	movq	(%r8), %rax
	leal	(%rdx,%rdx,2), %ecx
	sarq	$32, %rax
	leal	5(%rcx,%rax), %eax
	sall	$3, %eax
	cltq
	movq	(%rax,%rdi), %rax
	cmpq	%rax, 96(%r12)
	je	.L463
	movq	%rdx, %rax
	salq	$32, %rax
	movq	%rax, 31(%r9)
	movq	(%rbx), %rax
	cmpl	%edx, %esi
	jle	.L465
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L471:
	.cfi_restore_state
	movl	%ecx, %esi
	.p2align 4,,10
	.p2align 3
.L462:
	salq	$32, %rsi
	movq	%rsi, 31(%r9)
	movq	(%rbx), %rax
.L465:
	movq	1024(%r12), %r12
	movq	%r12, 23(%rax)
	testb	$1, %r12b
	je	.L470
	movq	%r12, %r13
	movq	(%rbx), %rdi
	andq	$-262144, %r13
	movq	8(%r13), %rax
	leaq	23(%rdi), %rsi
	testl	$262144, %eax
	jne	.L481
.L468:
	testb	$24, %al
	je	.L470
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L482
.L470:
	xorl	%eax, %eax
.L483:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	movq	%r12, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	(%rbx), %rdi
	movq	8(%r13), %rax
	leaq	23(%rdi), %rsi
	jmp	.L468
.L482:
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	xorl	%eax, %eax
	jmp	.L483
	.cfi_endproc
.LFE22139:
	.size	_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE7HasMoreEv, .-_ZN2v88internal24OrderedHashTableIteratorINS0_13JSMapIteratorENS0_14OrderedHashMapEE7HasMoreEv
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE:
.LFB21883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	3(%rsi,%rsi,2), %eax
	sall	$3, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leal	(%rdx,%rax,8), %ebx
	movq	(%rdi), %rax
	movslq	%ebx, %rbx
	movq	%rcx, -1(%rbx,%rax)
	testb	$1, %cl
	je	.L484
	movq	%rcx, %r14
	movq	%rdi, %r13
	movq	(%rdi), %rdi
	movq	%rcx, %r12
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testl	$262144, %eax
	jne	.L496
	testb	$24, %al
	je	.L484
.L498:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L497
.L484:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L496:
	.cfi_restore_state
	movq	%rcx, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testb	$24, %al
	jne	.L498
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L497:
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE21883:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE, .-_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	.section	.text._ZN2v88internal26SmallOrderedNameDictionary8SetEntryEPNS0_7IsolateEiNS0_6ObjectES4_NS0_15PropertyDetailsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SmallOrderedNameDictionary8SetEntryEPNS0_7IsolateEiNS0_6ObjectES4_NS0_15PropertyDetailsE
	.type	_ZN2v88internal26SmallOrderedNameDictionary8SetEntryEPNS0_7IsolateEiNS0_6ObjectES4_NS0_15PropertyDetailsE, @function
_ZN2v88internal26SmallOrderedNameDictionary8SetEntryEPNS0_7IsolateEiNS0_6ObjectES4_NS0_15PropertyDetailsE:
.LFB19741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	movq	%r8, %rcx
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	movl	$1, %edx
	pushq	%rbx
	movl	%r12d, %esi
	leal	3(%r12,%r12,2), %r14d
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	sall	$3, %r14d
	movslq	%r14d, %r14
	subq	$24, %rsp
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	movq	0(%r13), %rax
	movq	%r15, -1(%r14,%rax)
	testb	$1, %r15b
	je	.L503
	movq	%r15, %rcx
	movq	0(%r13), %rdi
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -56(%rbp)
	leaq	-1(%r14,%rdi), %rsi
	testl	$262144, %eax
	jne	.L511
	testb	$24, %al
	je	.L503
.L513:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L512
.L503:
	leal	(%rbx,%rbx), %ecx
	addq	$24, %rsp
	movl	%r12d, %esi
	movq	%r13, %rdi
	sarl	%ecx
	popq	%rbx
	movl	$2, %edx
	popq	%r12
	salq	$32, %rcx
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	.p2align 4,,10
	.p2align 3
.L511:
	.cfi_restore_state
	movq	%r15, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	0(%r13), %rdi
	movq	8(%rcx), %rax
	leaq	-1(%r14,%rdi), %rsi
	testb	$24, %al
	jne	.L513
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L512:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L503
	.cfi_endproc
.LFE19741:
	.size	_ZN2v88internal26SmallOrderedNameDictionary8SetEntryEPNS0_7IsolateEiNS0_6ObjectES4_NS0_15PropertyDetailsE, .-_ZN2v88internal26SmallOrderedNameDictionary8SetEntryEPNS0_7IsolateEiNS0_6ObjectES4_NS0_15PropertyDetailsE
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler10ValueAtPutENS0_10HeapObjectEiNS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler10ValueAtPutENS0_10HeapObjectEiNS0_6ObjectE
	.type	_ZN2v88internal28OrderedNameDictionaryHandler10ValueAtPutENS0_10HeapObjectEiNS0_6ObjectE, @function
_ZN2v88internal28OrderedNameDictionaryHandler10ValueAtPutENS0_10HeapObjectEiNS0_6ObjectE:
.LFB19764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdi), %rax
	cmpw	$163, 11(%rax)
	je	.L529
	movq	39(%rdi), %rax
	leaq	-1(%rdi), %rdx
	sarq	$32, %rax
	leal	7(%rax,%rsi,4), %r12d
	sall	$3, %r12d
	movslq	%r12d, %r12
	addq	%rdx, %r12
	movq	%r14, (%r12)
	testb	$1, %r14b
	je	.L514
	movq	%r14, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L530
	testb	$24, %al
	je	.L514
.L533:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L531
	.p2align 4,,10
	.p2align 3
.L514:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L532
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L530:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L533
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%rdi, -48(%rbp)
	movq	%rdx, %rcx
	leaq	-48(%rbp), %rdi
	movl	$1, %edx
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L531:
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L514
.L532:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19764:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler10ValueAtPutENS0_10HeapObjectEiNS0_6ObjectE, .-_ZN2v88internal28OrderedNameDictionaryHandler10ValueAtPutENS0_10HeapObjectEiNS0_6ObjectE
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler12DetailsAtPutENS0_10HeapObjectEiNS0_15PropertyDetailsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler12DetailsAtPutENS0_10HeapObjectEiNS0_15PropertyDetailsE
	.type	_ZN2v88internal28OrderedNameDictionaryHandler12DetailsAtPutENS0_10HeapObjectEiNS0_15PropertyDetailsE, @function
_ZN2v88internal28OrderedNameDictionaryHandler12DetailsAtPutENS0_10HeapObjectEiNS0_15PropertyDetailsE:
.LFB19766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leal	(%rdx,%rdx), %ecx
	sarl	%ecx
	salq	$32, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdi), %rax
	cmpw	$163, 11(%rax)
	je	.L539
	movq	39(%rdi), %rax
	sarq	$32, %rax
	leal	8(%rax,%rsi,4), %eax
	sall	$3, %eax
	cltq
	movq	%rcx, -1(%rdi,%rax)
.L534:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L540
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	.cfi_restore_state
	movq	%rdi, -16(%rbp)
	movl	$2, %edx
	leaq	-16(%rbp), %rdi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	jmp	.L534
.L540:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19766:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler12DetailsAtPutENS0_10HeapObjectEiNS0_15PropertyDetailsE, .-_ZN2v88internal28OrderedNameDictionaryHandler12DetailsAtPutENS0_10HeapObjectEiNS0_15PropertyDetailsE
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler8SetEntryEPNS0_7IsolateENS0_10HeapObjectEiNS0_6ObjectES5_NS0_15PropertyDetailsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler8SetEntryEPNS0_7IsolateENS0_10HeapObjectEiNS0_6ObjectES5_NS0_15PropertyDetailsE
	.type	_ZN2v88internal28OrderedNameDictionaryHandler8SetEntryEPNS0_7IsolateENS0_10HeapObjectEiNS0_6ObjectES5_NS0_15PropertyDetailsE, @function
_ZN2v88internal28OrderedNameDictionaryHandler8SetEntryEPNS0_7IsolateENS0_10HeapObjectEiNS0_6ObjectES5_NS0_15PropertyDetailsE:
.LFB19761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addl	%r9d, %r9d
	sarl	%r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	salq	$32, %r13
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rsi), %rdx
	cmpw	$163, 11(%rdx)
	je	.L566
	movq	39(%rsi), %rdx
	leaq	-1(%rsi), %rax
	sarq	$32, %rdx
	leal	6(%rdx,%r12,4), %r12d
	sall	$3, %r12d
	movslq	%r12d, %rsi
	addq	%rax, %rsi
	movq	%rcx, (%rsi)
	testb	$1, %cl
	je	.L551
	andq	$-262144, %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -72(%rbp)
	testl	$262144, %edx
	jne	.L567
	andl	$24, %edx
	je	.L551
.L573:
	movq	%rbx, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L568
	.p2align 4,,10
	.p2align 3
.L551:
	leal	8(%r12), %r15d
	movslq	%r15d, %r15
	addq	%rax, %r15
	movq	%r14, (%r15)
	testb	$1, %r14b
	je	.L550
	movq	%r14, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -72(%rbp)
	testl	$262144, %eax
	jne	.L569
	testb	$24, %al
	je	.L550
.L572:
	movq	%rbx, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L570
	.p2align 4,,10
	.p2align 3
.L550:
	addl	$16, %r12d
	movslq	%r12d, %r12
	movq	%r13, -1(%rbx,%r12)
.L541:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L571
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L569:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	8(%rcx), %rax
	testb	$24, %al
	jne	.L572
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L567:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rax, -88(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rsi
	movq	8(%rcx), %rdx
	andl	$24, %edx
	jne	.L573
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L566:
	leaq	-64(%rbp), %rbx
	movq	%r8, %rcx
	movq	%rsi, -64(%rbp)
	movl	$1, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	movq	%r15, %rcx
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	movq	%r13, %rcx
	movl	$2, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L568:
	movq	%r15, %rdx
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rax
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L570:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L550
.L571:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19761:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler8SetEntryEPNS0_7IsolateENS0_10HeapObjectEiNS0_6ObjectES5_NS0_15PropertyDetailsE, .-_ZN2v88internal28OrderedNameDictionaryHandler8SetEntryEPNS0_7IsolateENS0_10HeapObjectEiNS0_6ObjectES5_NS0_15PropertyDetailsE
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE:
.LFB21967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$4, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	cmovge	%esi, %edi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej@PLT
	cmpl	$26843544, %eax
	jle	.L575
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	movl	%eax, %ebx
	movl	%eax, %r14d
	movl	%r13d, %ecx
	movl	$63, %esi
	shrl	$31, %ebx
	movq	%r12, %rdi
	addl	%eax, %ebx
	sarl	%ebx
	leal	3(%rbx,%rax,2), %edx
	call	_ZN2v88internal7Factory20NewFixedArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EENS0_9RootIndexEiNS0_14AllocationTypeE@PLT
	cmpl	$1, %r14d
	jle	.L577
	movabsq	$-4294967296, %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L578:
	movq	(%rax), %rcx
	movq	%rsi, 39(%rcx,%rdx,8)
	addq	$1, %rdx
	cmpl	%edx, %ebx
	jg	.L578
.L577:
	movq	(%rax), %rdx
	salq	$32, %rbx
	movq	%rbx, 31(%rdx)
	movq	(%rax), %rdx
	movq	$0, 15(%rdx)
	movq	(%rax), %rdx
	movq	$0, 23(%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21967:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal14OrderedHashSet8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14OrderedHashSet8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.type	_ZN2v88internal14OrderedHashSet8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, @function
_ZN2v88internal14OrderedHashSet8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE:
.LFB19717:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.cfi_endproc
.LFE19717:
	.size	_ZN2v88internal14OrderedHashSet8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, .-_ZN2v88internal14OrderedHashSet8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE8AllocateEPNS0_7IsolateEi,"axG",@progbits,_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE8AllocateEPNS0_7IsolateEi,comdat
	.p2align 4
	.weak	_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE8AllocateEPNS0_7IsolateEi
	.type	_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE8AllocateEPNS0_7IsolateEi, @function
_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE8AllocateEPNS0_7IsolateEi:
.LFB22078:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$253, %esi
	jg	.L583
	call	_ZN2v88internal7Factory22NewSmallOrderedHashSetEiNS0_14AllocationTypeE@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L583:
	.cfi_restore_state
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22078:
	.size	_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE8AllocateEPNS0_7IsolateEi, .-_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE8AllocateEPNS0_7IsolateEi
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi:
.LFB21931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	%edx, %esi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r15), %rax
	movq	%rdi, -72(%rbp)
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	sete	%dl
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	movq	-72(%rbp), %rcx
	testq	%rax, %rax
	je	.L664
	movq	(%r15), %rdx
	movq	%rax, %r13
	movq	15(%rdx), %rax
	sarq	$32, %rax
	movq	%rax, -88(%rbp)
	movq	%rax, %rbx
	movq	23(%rdx), %rax
	movq	0(%r13), %rdi
	sarq	$32, %rax
	movq	31(%rdi), %rsi
	addl	%ebx, %eax
	sarq	$32, %rsi
	testl	%eax, %eax
	jle	.L625
	leal	-1(%rax), %r10d
	leal	-1(%rsi), %eax
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	movl	%eax, -72(%rbp)
	leal	32(,%rsi,8), %eax
	movq	%r10, %r11
	xorl	%r8d, %r8d
	cltq
	movq	%r13, %r10
	movq	%r15, %r9
	movq	%rax, -160(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -96(%rbp)
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L666:
	leal	40(,%r14,8), %eax
	movq	%rbx, %rsi
	addl	$1, %r14d
	cltq
	salq	$32, %rsi
	movq	%rsi, -1(%rax,%rdx)
	leaq	1(%rbx), %rax
	cmpq	%r11, %rbx
	je	.L665
.L661:
	movq	(%r9), %rdx
	movq	%rax, %rbx
.L626:
	movq	31(%rdx), %rax
	leal	(%rbx,%rbx), %r12d
	sarq	$32, %rax
	leal	5(%r12,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	cmpq	%rax, 96(%rcx)
	je	.L666
	testb	$1, %al
	jne	.L594
	sarq	$32, %rax
	movq	%rax, %rdx
	sall	$15, %eax
	subl	%edx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	-72(%rbp), %eax
	leal	40(,%rax,8), %edx
	movslq	%edx, %rdx
.L618:
	movq	(%r10), %rax
	movq	%r8, %rsi
	salq	$32, %rsi
	movq	-1(%rdx,%rax), %r13
	movq	%rsi, -1(%rdx,%rax)
	movq	(%r10), %rdi
	movq	31(%rdi), %rax
	movq	(%r9), %rsi
	movq	31(%rsi), %rdx
	sarq	$32, %rax
	sarq	$32, %rdx
	leal	5(%r12,%rdx), %edx
	leal	5(%rax,%r8,2), %r12d
	sall	$3, %edx
	sall	$3, %r12d
	movslq	%edx, %rdx
	movslq	%r12d, %rax
	movq	-1(%rdx,%rsi), %r15
	leaq	-1(%rdi,%rax), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L631
	movq	%r15, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -80(%rbp)
	testl	$262144, %edx
	je	.L620
	movq	%r15, %rdx
	movq	%r9, -152(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%r11, -136(%rbp)
	movq	%r10, -128(%rbp)
	movl	%r8d, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rax
	movq	-128(%rbp), %r10
	movq	-152(%rbp), %r9
	movq	-144(%rbp), %rcx
	movq	8(%rax), %rdx
	movq	-136(%rbp), %r11
	movl	-120(%rbp), %r8d
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
.L620:
	andl	$24, %edx
	je	.L631
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L667
	.p2align 4,,10
	.p2align 3
.L631:
	movq	(%r10), %r15
	addl	$8, %r12d
	movslq	%r12d, %r12
	leaq	-1(%r15,%r12), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L630
	movq	%r13, %r12
	andq	$-262144, %r12
	movq	8(%r12), %rax
	testl	$262144, %eax
	je	.L623
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%r9, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	movl	%r8d, -104(%rbp)
	movq	%rsi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r12), %rax
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %r11
	movq	-112(%rbp), %r10
	movl	-104(%rbp), %r8d
	movq	-80(%rbp), %rsi
.L623:
	testb	$24, %al
	je	.L630
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L668
.L630:
	addl	$1, %r8d
.L673:
	leaq	1(%rbx), %rax
	cmpq	%r11, %rbx
	jne	.L661
.L665:
	movq	(%r10), %rdi
	movq	%r10, %r13
	movq	%r9, %r15
.L625:
	movq	-88(%rbp), %rax
	salq	$32, %rax
	movq	%rax, 15(%rdi)
	movq	(%r15), %r14
	movq	0(%r13), %r12
	movq	%r12, 15(%r14)
	leaq	15(%r14), %r15
	testb	$1, %r12b
	je	.L629
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L669
	testb	$24, %al
	je	.L629
.L674:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L670
	.p2align 4,,10
	.p2align 3
.L629:
	movq	%r13, %rax
.L588:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L671
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L594:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L596
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	ja	.L672
	movl	7(%rax), %edx
	testb	$1, %dl
	jne	.L609
	shrl	$2, %edx
.L610:
	andl	-72(%rbp), %edx
	leal	40(,%rdx,8), %edx
	movslq	%edx, %rdx
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L668:
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movl	-80(%rbp), %r8d
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %r10
	addl	$1, %r8d
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L667:
	movq	%r15, %rdx
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %r10
	movl	-80(%rbp), %r8d
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L669:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L674
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L596:
	movq	7(%rax), %rdx
	movq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L675
	comisd	.LC3(%rip), %xmm0
	jb	.L601
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jb	.L601
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L601
	jne	.L601
	movl	%esi, %eax
	sall	$15, %eax
	subl	%esi, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L672:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L676
	movq	-1(%rax), %rdx
	cmpw	$66, 11(%rdx)
	je	.L677
	movq	-1(%rax), %rdx
	cmpw	$160, 11(%rdx)
	je	.L678
	movq	-96(%rbp), %rdi
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	movl	%r8d, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	movl	-80(%rbp), %r8d
	movq	-104(%rbp), %r10
	sarq	$32, %rax
	andl	-72(%rbp), %eax
	movq	-112(%rbp), %r11
	leal	40(,%rax,8), %edx
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r9
	movslq	%edx, %rdx
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L670:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L609:
	movq	-96(%rbp), %rdi
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	movl	%r8d, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %r10
	movl	%eax, %edx
	movl	-80(%rbp), %r8d
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L601:
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	andl	$1073741823, %eax
.L663:
	andl	-72(%rbp), %eax
	leal	40(,%rax,8), %edx
	movslq	%edx, %rdx
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L676:
	movq	15(%rax), %rdx
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L613
	shrl	$2, %eax
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L664:
	xorl	%eax, %eax
	jmp	.L588
.L677:
	movl	7(%rax), %esi
	movl	$40, %edx
	andl	$2147483646, %esi
	je	.L618
	movq	15(%rax), %rdx
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	jmp	.L663
.L613:
	movq	-96(%rbp), %rdi
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	movl	%r8d, -80(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %r10
	movl	-80(%rbp), %r8d
	jmp	.L663
.L678:
	movq	-96(%rbp), %rdi
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	movl	%r8d, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	andl	-72(%rbp), %eax
	movq	-128(%rbp), %r9
	leal	40(,%rax,8), %edx
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %r10
	movl	-80(%rbp), %r8d
	movslq	%edx, %rdx
	jmp	.L618
.L675:
	movq	-160(%rbp), %rdx
	jmp	.L618
.L671:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21931:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.section	.text._ZN2v88internal14OrderedHashSet6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14OrderedHashSet6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal14OrderedHashSet6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal14OrderedHashSet6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB19699:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.cfi_endproc
.LFE19699:
	.size	_ZN2v88internal14OrderedHashSet6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal14OrderedHashSet6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE:
.LFB21921:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rcx
	movq	%rsi, %rax
	movq	15(%rcx), %rdx
	movq	23(%rcx), %rsi
	movq	31(%rcx), %rcx
	sarq	$32, %rsi
	sarq	$32, %rdx
	sarq	$32, %rcx
	addl	%esi, %edx
	leal	(%rcx,%rcx), %r8d
	cmpl	%r8d, %edx
	jl	.L684
	movl	%r8d, %edx
	sall	$2, %ecx
	sarl	%edx
	cmpl	%esi, %edx
	movq	%rax, %rsi
	cmovg	%ecx, %r8d
	movl	%r8d, %edx
	jmp	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.p2align 4,,10
	.p2align 3
.L684:
	ret
	.cfi_endproc
.LFE21921:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, @function
_ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE:
.LFB19690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdi, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-64(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object15GetOrCreateHashEPNS0_7IsolateE@PLT
	movq	(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -72(%rbp)
	movq	%rax, %rdi
	movq	31(%rdx), %rax
	sarq	$32, %rax
	subl	$1, %eax
	andl	%edi, %eax
	leal	40(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	movl	%eax, %ecx
	cmpq	$-1, %rax
	jne	.L686
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L689:
	movq	(%r12), %rdx
	movq	31(%rdx), %rax
	sarq	$32, %rax
	leal	6(%r13,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	movl	%eax, %ecx
	cmpq	$-1, %rax
	je	.L691
.L686:
	movq	31(%rdx), %rax
	leal	(%rcx,%rcx), %r13d
	movq	%rbx, %rdi
	sarq	$32, %rax
	leal	5(%r13,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	movq	(%r15), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object13SameValueZeroES1_@PLT
	testb	%al, %al
	je	.L689
.L710:
	movq	%r12, %rax
.L690:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L711
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L691:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L712
	movq	(%rax), %rdi
	movq	31(%rdi), %rcx
	movq	-72(%rbp), %rbx
	movq	31(%rdi), %rax
	movl	%ebx, %r13d
	sarq	$32, %rcx
	sarq	$32, %rax
	subl	$1, %ecx
	subl	$1, %eax
	andl	%ebx, %ecx
	andl	%eax, %r13d
	leal	40(,%r13,8), %eax
	cltq
	movq	-1(%rdi,%rax), %rax
	movq	15(%rdi), %rbx
	movq	23(%rdi), %r13
	movq	31(%rdi), %rdx
	sarq	$32, %rbx
	movq	(%r15), %r15
	sarq	$32, %rax
	sarq	$32, %r13
	sarq	$32, %rdx
	addl	%ebx, %r13d
	leal	5(%rdx,%r13,2), %r14d
	sall	$3, %r14d
	movslq	%r14d, %rdx
	leaq	-1(%rdi,%rdx), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L695
	movq	%r15, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rdx
	movq	%r8, -72(%rbp)
	testl	$262144, %edx
	jne	.L713
	andl	$24, %edx
	je	.L695
.L714:
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L695
	movq	%r15, %rdx
	movq	%rax, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rax
	movl	-72(%rbp), %ecx
	.p2align 4,,10
	.p2align 3
.L695:
	movq	(%r12), %rdx
	addl	$8, %r14d
	salq	$32, %rax
	addl	$1, %ebx
	salq	$32, %r13
	movslq	%r14d, %r14
	salq	$32, %rbx
	movq	%rax, -1(%r14,%rdx)
	leal	40(,%rcx,8), %eax
	cltq
	movq	(%r12), %rdx
	movq	%r13, -1(%rax,%rdx)
	movq	(%r12), %rax
	movq	%rbx, 15(%rax)
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L713:
	movq	%r15, %rdx
	movq	%rax, -104(%rbp)
	movl	%ecx, -92(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	movq	-104(%rbp), %rax
	movl	-92(%rbp), %ecx
	movq	-88(%rbp), %rsi
	movq	8(%r8), %rdx
	movq	-80(%rbp), %rdi
	andl	$24, %edx
	jne	.L714
	jmp	.L695
.L712:
	xorl	%eax, %eax
	jmp	.L690
.L711:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19690:
	.size	_ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, .-_ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE:
.LFB21982:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	15(%rdx), %rax
	movq	31(%rdx), %rdx
	sarq	$32, %rdx
	sarq	$32, %rax
	leal	(%rdx,%rdx), %ecx
	sarl	$2, %ecx
	cmpl	%eax, %ecx
	jg	.L716
	movq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L716:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	testq	%rax, %rax
	je	.L722
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L722:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21982:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal21OrderedHashSetHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashSetEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21OrderedHashSetHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashSetEEE
	.type	_ZN2v88internal21OrderedHashSetHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashSetEEE, @function
_ZN2v88internal21OrderedHashSetHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashSetEEE:
.LFB19756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$512, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	testq	%rax, %rax
	je	.L731
	movq	%rax, %r14
	movq	(%r12), %rax
	movzbl	7(%rax), %edx
	movzbl	8(%rax), %ecx
	addl	%ecx, %edx
	je	.L726
	subl	$1, %edx
	movl	$16, %r15d
	leaq	24(,%rdx,8), %r13
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L740:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L728:
	cmpq	%rsi, 96(%rbx)
	je	.L730
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L731
.L730:
	addq	$8, %r15
	cmpq	%r15, %r13
	je	.L726
	movq	(%r12), %rax
.L732:
	movq	41112(%rbx), %rdi
	movq	-1(%rax,%r15), %rsi
	testq	%rdi, %rdi
	jne	.L740
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L741
.L729:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L731:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L741:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L726:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19756:
	.size	_ZN2v88internal21OrderedHashSetHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashSetEEE, .-_ZN2v88internal21OrderedHashSetHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashSetEEE
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE:
.LFB21983:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movl	$4, %esi
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	sete	%dl
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	testq	%rax, %rax
	je	.L755
	movq	(%rbx), %r14
	movq	(%rax), %r13
	movq	%rax, %r12
	movq	%r13, 15(%r14)
	leaq	15(%r14), %rsi
	testb	$1, %r13b
	je	.L747
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L756
	testb	$24, %al
	je	.L747
.L758:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L757
.L747:
	movabsq	$-4294967296, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 23(%rax)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L756:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L758
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L757:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L755:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21983:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE:
.LFB21972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$4, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	cmovge	%esi, %edi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej@PLT
	cmpl	$19173960, %eax
	jle	.L760
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	.cfi_restore_state
	movl	%eax, %edx
	movl	%eax, %ebx
	movl	%r13d, %ecx
	movl	$62, %esi
	shrl	$31, %edx
	movq	%r12, %rdi
	addl	%eax, %edx
	leal	(%rax,%rax,2), %eax
	sarl	%edx
	movl	%edx, %r14d
	leal	3(%rdx,%rax), %edx
	call	_ZN2v88internal7Factory20NewFixedArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EENS0_9RootIndexEiNS0_14AllocationTypeE@PLT
	cmpl	$1, %ebx
	jle	.L762
	movabsq	$-4294967296, %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L763:
	movq	(%rax), %rcx
	movq	%rsi, 39(%rcx,%rdx,8)
	addq	$1, %rdx
	cmpl	%edx, %r14d
	jg	.L763
.L762:
	movq	(%rax), %rcx
	movq	%r14, %rdx
	salq	$32, %rdx
	movq	%rdx, 31(%rcx)
	movq	(%rax), %rdx
	movq	$0, 15(%rdx)
	movq	(%rax), %rdx
	movq	$0, 23(%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21972:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal14OrderedHashMap8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14OrderedHashMap8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.type	_ZN2v88internal14OrderedHashMap8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, @function
_ZN2v88internal14OrderedHashMap8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE:
.LFB19718:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.cfi_endproc
.LFE19718:
	.size	_ZN2v88internal14OrderedHashMap8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, .-_ZN2v88internal14OrderedHashMap8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE8AllocateEPNS0_7IsolateEi,"axG",@progbits,_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE8AllocateEPNS0_7IsolateEi,comdat
	.p2align 4
	.weak	_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE8AllocateEPNS0_7IsolateEi
	.type	_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE8AllocateEPNS0_7IsolateEi, @function
_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE8AllocateEPNS0_7IsolateEi:
.LFB22079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$253, %esi
	jg	.L768
	call	_ZN2v88internal7Factory22NewSmallOrderedHashMapEiNS0_14AllocationTypeE@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L768:
	.cfi_restore_state
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22079:
	.size	_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE8AllocateEPNS0_7IsolateEi, .-_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE8AllocateEPNS0_7IsolateEi
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi:
.LFB21932:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	(%rax), %rax
	movl	%edx, %esi
	movq	%rdi, -88(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, -80(%rbp)
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	sete	%dl
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	testq	%rax, %rax
	je	.L855
	movq	%rax, %rcx
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rdx
	movq	%rdx, %rbx
	sarq	$32, %rbx
	movq	%rbx, -168(%rbp)
	movq	23(%rax), %rdx
	movq	(%rcx), %rdi
	sarq	$32, %rdx
	movq	31(%rdi), %rsi
	addl	%ebx, %edx
	sarq	$32, %rsi
	testl	%edx, %edx
	jle	.L811
	leal	-1(%rdx), %edi
	leal	32(,%rsi,8), %edx
	xorl	%r12d, %r12d
	xorl	%r8d, %r8d
	movq	%rdi, -96(%rbp)
	leal	-1(%rsi), %edi
	movslq	%edx, %rsi
	movq	%rsi, -176(%rbp)
	leaq	-64(%rbp), %rsi
	movl	%edi, -104(%rbp)
	movl	$0, -100(%rbp)
	movq	%rsi, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L812:
	movq	31(%rax), %rdx
	leal	(%r12,%r12,2), %ebx
	sarq	$32, %rdx
	leal	5(%rbx,%rdx), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	-1(%rax,%rdx), %rdx
	movq	-88(%rbp), %rsi
	cmpq	%rdx, 96(%rsi)
	je	.L856
	testb	$1, %dl
	jne	.L779
	sarq	$32, %rdx
	movl	%edx, %eax
	sall	$15, %eax
	subl	%edx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	-104(%rbp), %eax
	leal	40(,%rax,8), %eax
	cltq
.L803:
	movq	(%rcx), %rdx
	movq	%r8, %rsi
	salq	$32, %rsi
	movq	-1(%rax,%rdx), %r9
	movq	%rsi, -1(%rax,%rdx)
	leal	(%r8,%r8,2), %edx
	movq	(%rcx), %r15
	movq	31(%r15), %rax
	movq	%r15, %rdi
	sarq	$32, %rax
	leal	3(%rdx,%rax), %r10d
	movq	-72(%rbp), %rax
	movslq	%r10d, %r14
	movq	(%rax), %rax
	movq	31(%rax), %rdx
	movq	%r12, -80(%rbp)
	sarq	$32, %rdx
	leal	3(%rbx,%rdx), %edx
	leal	16(,%rdx,8), %ebx
	movslq	%edx, %rdx
	subq	%rdx, %r14
	leaq	32(,%rdx,8), %r11
	movslq	%ebx, %rbx
	salq	$3, %r14
	movq	%r11, %r12
	movq	%r14, %r11
.L807:
	movq	-1(%rbx,%rax), %r13
	leaq	(%rbx,%r11), %rax
	leaq	-1(%rax,%rdi), %r14
	movq	%r13, (%r14)
	testb	$1, %r13b
	je	.L817
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	je	.L805
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r11, -152(%rbp)
	movl	%r10d, -144(%rbp)
	movq	%r9, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movl	%r8d, -120(%rbp)
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-128(%rbp), %rcx
	movq	-152(%rbp), %r11
	movl	-144(%rbp), %r10d
	movq	-136(%rbp), %r9
	movl	-120(%rbp), %r8d
	movq	-112(%rbp), %rdi
.L805:
	testb	$24, %al
	je	.L817
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L817
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -128(%rbp)
	movq	%r11, -144(%rbp)
	movl	%r10d, -136(%rbp)
	movq	%rcx, -120(%rbp)
	movl	%r8d, -112(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-144(%rbp), %r11
	movl	-136(%rbp), %r10d
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %rcx
	movl	-112(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L817:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	je	.L848
	movq	-72(%rbp), %rax
	movq	(%rcx), %rdi
	movq	(%rax), %rax
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L848:
	movq	(%rcx), %r13
	leal	32(,%r10,8), %eax
	movq	-80(%rbp), %r12
	cltq
	leaq	-1(%r13,%rax), %r14
	movq	%r9, (%r14)
	testb	$1, %r9b
	je	.L816
	movq	%r9, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L809
	movq	%r9, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rcx, -120(%rbp)
	movl	%r8d, -112(%rbp)
	movq	%r9, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-120(%rbp), %rcx
	movl	-112(%rbp), %r8d
	movq	-80(%rbp), %r9
.L809:
	testb	$24, %al
	je	.L816
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L816
	movq	%r9, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rcx, -112(%rbp)
	movl	%r8d, -80(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %rcx
	movl	-80(%rbp), %r8d
	.p2align 4,,10
	.p2align 3
.L816:
	addl	$1, %r8d
	leaq	1(%r12), %rax
	cmpq	%r12, -96(%rbp)
	je	.L857
.L849:
	movq	%rax, %r12
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L856:
	movl	-100(%rbp), %edi
	movq	%r12, %rsi
	salq	$32, %rsi
	leal	40(,%rdi,8), %edx
	movslq	%edx, %rdx
	movq	%rsi, -1(%rdx,%rax)
	leal	1(%rdi), %eax
	movl	%eax, -100(%rbp)
	leaq	1(%r12), %rax
	cmpq	%r12, -96(%rbp)
	jne	.L849
.L857:
	movq	(%rcx), %rdi
.L811:
	movq	-168(%rbp), %rax
	salq	$32, %rax
	movq	%rax, 15(%rdi)
	movq	-72(%rbp), %rax
	movq	(%rcx), %r12
	movq	(%rax), %r13
	movq	%r12, 15(%r13)
	leaq	15(%r13), %r14
	testb	$1, %r12b
	je	.L815
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L858
	testb	$24, %al
	je	.L815
.L864:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L859
	.p2align 4,,10
	.p2align 3
.L815:
	movq	%rcx, %rax
.L773:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L860
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L779:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L781
	movq	-1(%rdx), %rax
	cmpw	$64, 11(%rax)
	jbe	.L854
	movq	-1(%rdx), %rax
	cmpw	$67, 11(%rax)
	je	.L861
	movq	-1(%rdx), %rax
	cmpw	$66, 11(%rax)
	je	.L862
	movq	-1(%rdx), %rax
	cmpw	$160, 11(%rax)
	je	.L863
	movq	-160(%rbp), %rdi
	movq	%rcx, -112(%rbp)
	movl	%r8d, -80(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	movl	-80(%rbp), %r8d
	movq	-112(%rbp), %rcx
	sarq	$32, %rax
	andl	-104(%rbp), %eax
	leal	40(,%rax,8), %eax
	cltq
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L861:
	movq	15(%rdx), %rdx
.L854:
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L798
	shrl	$2, %eax
.L851:
	andl	-104(%rbp), %eax
	leal	40(,%rax,8), %eax
	cltq
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L798:
	movq	-160(%rbp), %rdi
	movq	%rcx, -112(%rbp)
	movl	%r8d, -80(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-112(%rbp), %rcx
	movl	-80(%rbp), %r8d
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L858:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-72(%rbp), %rcx
	testb	$24, %al
	jne	.L864
	jmp	.L815
	.p2align 4,,10
	.p2align 3
.L781:
	movq	7(%rdx), %rdx
	movq	-176(%rbp), %rax
	movq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L803
	comisd	.LC3(%rip), %xmm0
	jb	.L786
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jb	.L786
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L786
	jne	.L786
	movl	%esi, %eax
	sall	$15, %eax
	subl	%esi, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L786:
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	andl	$1073741823, %eax
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L859:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	jmp	.L815
.L862:
	movl	7(%rdx), %esi
	movl	$40, %eax
	andl	$2147483646, %esi
	je	.L803
	movq	15(%rdx), %rdx
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L855:
	xorl	%eax, %eax
	jmp	.L773
.L863:
	movq	-160(%rbp), %rdi
	movq	%rcx, -112(%rbp)
	movl	%r8d, -80(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	andl	-104(%rbp), %eax
	movq	-112(%rbp), %rcx
	leal	40(,%rax,8), %eax
	movl	-80(%rbp), %r8d
	cltq
	jmp	.L803
.L860:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21932:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.section	.text._ZN2v88internal14OrderedHashMap6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14OrderedHashMap6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal14OrderedHashMap6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal14OrderedHashMap6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB19700:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.cfi_endproc
.LFE19700:
	.size	_ZN2v88internal14OrderedHashMap6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal14OrderedHashMap6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE:
.LFB21948:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rcx
	movq	%rsi, %rax
	movq	15(%rcx), %rdx
	movq	23(%rcx), %rsi
	movq	31(%rcx), %rcx
	sarq	$32, %rsi
	sarq	$32, %rdx
	sarq	$32, %rcx
	addl	%esi, %edx
	leal	(%rcx,%rcx), %r8d
	cmpl	%r8d, %edx
	jl	.L870
	movl	%r8d, %edx
	sall	$2, %ecx
	sarl	%edx
	cmpl	%esi, %edx
	movq	%rax, %rsi
	cmovg	%ecx, %r8d
	movl	%r8d, %edx
	jmp	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.p2align 4,,10
	.p2align 3
.L870:
	ret
	.cfi_endproc
.LFE21948:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal14OrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14OrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_
	.type	_ZN2v88internal14OrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_, @function
_ZN2v88internal14OrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_:
.LFB19704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdi, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-64(%rbp), %rbx
	subq	$72, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rbx, %rdi
	movq	%rcx, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object15GetOrCreateHashEPNS0_7IsolateE@PLT
	movq	(%r12), %rdx
	sarq	$32, %rax
	movq	%rax, -88(%rbp)
	movq	%rax, %rsi
	movq	31(%rdx), %rax
	sarq	$32, %rax
	subl	$1, %eax
	andl	%esi, %eax
	leal	40(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	movq	(%r14), %r13
	sarq	$32, %rax
	movl	%eax, %ecx
	cmpq	$-1, %rax
	jne	.L872
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L875:
	movq	(%r12), %rdx
	movq	31(%rdx), %rax
	sarq	$32, %rax
	leal	7(%r15,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	sarq	$32, %rax
	movl	%eax, %ecx
	cmpq	$-1, %rax
	je	.L877
.L872:
	movq	31(%rdx), %rax
	leal	(%rcx,%rcx,2), %r15d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	sarq	$32, %rax
	leal	5(%r15,%rax), %eax
	sall	$3, %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object13SameValueZeroES1_@PLT
	testb	%al, %al
	je	.L875
.L906:
	movq	%r12, %rax
.L876:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L907
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L877:
	.cfi_restore_state
	movq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L908
	movq	(%rax), %rdi
	movq	31(%rdi), %rbx
	movq	-88(%rbp), %rsi
	movq	31(%rdi), %rax
	sarq	$32, %rbx
	sarq	$32, %rax
	subl	$1, %ebx
	subl	$1, %eax
	andl	%esi, %ebx
	andl	%esi, %eax
	leal	40(,%rax,8), %eax
	cltq
	movq	-1(%rdi,%rax), %r13
	movq	15(%rdi), %rax
	movq	23(%rdi), %r15
	movq	31(%rdi), %rcx
	sarq	$32, %rax
	movq	(%r14), %rdx
	sarq	$32, %r13
	sarq	$32, %r15
	sarq	$32, %rcx
	addl	%eax, %r15d
	leal	(%r15,%r15,2), %esi
	leal	5(%rsi,%rcx), %r14d
	sall	$3, %r14d
	movslq	%r14d, %rcx
	leaq	-1(%rdi,%rcx), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L885
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %r8
	movq	%rcx, -72(%rbp)
	testl	$262144, %r8d
	jne	.L909
	andl	$24, %r8d
	je	.L885
.L912:
	movq	%rdi, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	jne	.L885
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L885:
	movq	-80(%rbp), %rsi
	movq	(%r12), %rdi
	leal	8(%r14), %ecx
	movslq	%ecx, %rcx
	movq	(%rsi), %rdx
	leaq	-1(%rdi,%rcx), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L884
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rcx
	movq	%r8, -72(%rbp)
	testl	$262144, %ecx
	jne	.L910
	andl	$24, %ecx
	je	.L884
.L911:
	movq	%rdi, %rcx
	andq	$-262144, %rcx
	testb	$24, 8(%rcx)
	jne	.L884
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rax
	.p2align 4,,10
	.p2align 3
.L884:
	movq	(%r12), %rdx
	addl	$16, %r14d
	salq	$32, %r13
	addl	$1, %eax
	salq	$32, %r15
	movslq	%r14d, %r14
	salq	$32, %rax
	movq	%r13, -1(%r14,%rdx)
	leal	40(,%rbx,8), %edx
	movslq	%edx, %rdx
	movq	(%r12), %rcx
	movq	%r15, -1(%rdx,%rcx)
	movq	(%r12), %rdx
	movq	%rax, 15(%rdx)
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L910:
	movq	%rax, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	8(%r8), %rcx
	movq	-80(%rbp), %rdi
	andl	$24, %ecx
	jne	.L911
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L909:
	movq	%rax, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	8(%rcx), %r8
	movq	-88(%rbp), %rdi
	andl	$24, %r8d
	jne	.L912
	jmp	.L885
.L908:
	xorl	%eax, %eax
	jmp	.L876
.L907:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19704:
	.size	_ZN2v88internal14OrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_, .-_ZN2v88internal14OrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE:
.LFB21987:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	15(%rdx), %rax
	movq	31(%rdx), %rdx
	sarq	$32, %rdx
	sarq	$32, %rax
	leal	(%rdx,%rdx), %ecx
	sarl	$2, %ecx
	cmpl	%eax, %ecx
	jg	.L914
	movq	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L914:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	testq	%rax, %rax
	je	.L920
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L920:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21987:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal21OrderedHashMapHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashMapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21OrderedHashMapHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashMapEEE
	.type	_ZN2v88internal21OrderedHashMapHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashMapEEE, @function
_ZN2v88internal21OrderedHashMapHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashMapEEE:
.LFB19755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$512, %esi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	testq	%rax, %rax
	je	.L939
	movq	%rax, %r12
	movq	(%r14), %rax
	movzbl	7(%rax), %edx
	movzbl	8(%rax), %ecx
	addl	%ecx, %edx
	je	.L924
	leal	-1(%rdx), %r15d
	addq	$2, %r15
	salq	$4, %r15
	movq	%r15, -56(%rbp)
	movl	$16, %r15d
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L940:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L927:
	cmpq	%rsi, 96(%rbx)
	je	.L930
	movq	(%r14), %rax
	movq	41112(%rbx), %rdi
	movq	7(%r15,%rax), %rsi
	testq	%rdi, %rdi
	je	.L931
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L932:
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	_ZN2v88internal14OrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L939
.L930:
	addq	$16, %r15
	cmpq	%r15, -56(%rbp)
	je	.L924
	movq	(%r14), %rax
.L925:
	movq	41112(%rbx), %rdi
	movq	-1(%rax,%r15), %rsi
	testq	%rdi, %rdi
	jne	.L940
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L941
.L928:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L931:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L942
.L933:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L939:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L941:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L942:
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L924:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19755:
	.size	_ZN2v88internal21OrderedHashMapHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashMapEEE, .-_ZN2v88internal21OrderedHashMapHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashMapEEE
	.section	.text._ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE
	.type	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE, @function
_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE:
.LFB21988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movl	$4, %esi
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	sete	%dl
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	testq	%rax, %rax
	je	.L956
	movq	(%rbx), %r14
	movq	(%rax), %r13
	movq	%rax, %r12
	movq	%r13, 15(%r14)
	leaq	15(%r14), %rsi
	testb	$1, %r13b
	je	.L948
	movq	%r13, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L957
	testb	$24, %al
	je	.L948
.L959:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L958
.L948:
	movabsq	$-4294967296, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 23(%rax)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L957:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L959
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L958:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L956:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21988:
	.size	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE, .-_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE5ClearEPNS0_7IsolateENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.type	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, @function
_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE:
.LFB21977:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	$4, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	cmovge	%esi, %edi
	call	_ZN2v84base4bits21RoundUpToPowerOfTwo32Ej@PLT
	cmpl	$14913080, %eax
	jle	.L961
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L961:
	.cfi_restore_state
	movl	%eax, %ebx
	movl	%eax, %r14d
	movl	%r13d, %ecx
	movl	$64, %esi
	shrl	$31, %ebx
	movq	%r12, %rdi
	addl	%eax, %ebx
	sarl	%ebx
	leal	4(%rbx,%rax,4), %edx
	call	_ZN2v88internal7Factory20NewFixedArrayWithMapINS0_10FixedArrayEEENS0_6HandleIT_EENS0_9RootIndexEiNS0_14AllocationTypeE@PLT
	cmpl	$1, %r14d
	jle	.L963
	movabsq	$-4294967296, %rsi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L964:
	movq	(%rax), %rcx
	movq	%rsi, 47(%rcx,%rdx,8)
	addq	$1, %rdx
	cmpl	%edx, %ebx
	jg	.L964
.L963:
	movq	(%rax), %rdx
	salq	$32, %rbx
	movq	%rbx, 39(%rdx)
	movq	(%rax), %rdx
	movq	$0, 23(%rdx)
	movq	(%rax), %rdx
	movq	$0, 31(%rdx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21977:
	.size	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, .-_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal21OrderedNameDictionary8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21OrderedNameDictionary8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.type	_ZN2v88internal21OrderedNameDictionary8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, @function
_ZN2v88internal21OrderedNameDictionary8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE:
.LFB19719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	testq	%rax, %rax
	je	.L968
	movq	(%rax), %rdx
	movq	$0, 15(%rdx)
.L968:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19719:
	.size	_ZN2v88internal21OrderedNameDictionary8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE, .-_ZN2v88internal21OrderedNameDictionary8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	.section	.text._ZN2v88internal23OrderedHashTableHandlerINS0_26SmallOrderedNameDictionaryENS0_21OrderedNameDictionaryEE8AllocateEPNS0_7IsolateEi,"axG",@progbits,_ZN2v88internal23OrderedHashTableHandlerINS0_26SmallOrderedNameDictionaryENS0_21OrderedNameDictionaryEE8AllocateEPNS0_7IsolateEi,comdat
	.p2align 4
	.weak	_ZN2v88internal23OrderedHashTableHandlerINS0_26SmallOrderedNameDictionaryENS0_21OrderedNameDictionaryEE8AllocateEPNS0_7IsolateEi
	.type	_ZN2v88internal23OrderedHashTableHandlerINS0_26SmallOrderedNameDictionaryENS0_21OrderedNameDictionaryEE8AllocateEPNS0_7IsolateEi, @function
_ZN2v88internal23OrderedHashTableHandlerINS0_26SmallOrderedNameDictionaryENS0_21OrderedNameDictionaryEE8AllocateEPNS0_7IsolateEi:
.LFB22080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$253, %esi
	jg	.L974
	call	_ZN2v88internal7Factory29NewSmallOrderedNameDictionaryEiNS0_14AllocationTypeE@PLT
.L975:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L974:
	.cfi_restore_state
	call	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	testq	%rax, %rax
	je	.L975
	movq	(%rax), %rdx
	movq	$0, 15(%rdx)
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22080:
	.size	_ZN2v88internal23OrderedHashTableHandlerINS0_26SmallOrderedNameDictionaryENS0_21OrderedNameDictionaryEE8AllocateEPNS0_7IsolateEi, .-_ZN2v88internal23OrderedHashTableHandlerINS0_26SmallOrderedNameDictionaryENS0_21OrderedNameDictionaryEE8AllocateEPNS0_7IsolateEi
	.section	.text._ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.type	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi, @function
_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi:
.LFB21933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r9
	movl	%edx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -112(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r9), %rax
	movq	%r9, -72(%rbp)
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	sete	%dl
	call	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	testq	%rax, %rax
	je	.L982
	movq	%rax, %r15
	movq	(%rax), %rax
	movq	$0, 15(%rax)
	movq	-72(%rbp), %r9
	movq	(%r9), %rax
	movq	23(%rax), %rdx
	movq	%rdx, %rcx
	sarq	$32, %rcx
	movq	%rcx, -168(%rbp)
	movq	31(%rax), %rdx
	movq	(%r15), %rdi
	sarq	$32, %rdx
	movq	39(%rdi), %rsi
	addl	%ecx, %edx
	sarq	$32, %rsi
	testl	%edx, %edx
	jle	.L1020
	leal	-1(%rdx), %ecx
	leal	40(,%rsi,8), %edx
	movl	$0, -80(%rbp)
	xorl	%r8d, %r8d
	movq	%rcx, -144(%rbp)
	leal	-1(%rsi), %ecx
	movl	%ecx, -148(%rbp)
	movslq	%edx, %rcx
	movq	%rcx, -176(%rbp)
	leaq	-64(%rbp), %rcx
	movl	$0, -76(%rbp)
	movq	%rcx, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	39(%rax), %rdx
	leal	0(,%r8,4), %ebx
	sarq	$32, %rdx
	leal	6(%rbx,%rdx), %edx
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	-1(%rax,%rdx), %rdx
	movq	-112(%rbp), %rcx
	cmpq	%rdx, 96(%rcx)
	je	.L1066
	testb	$1, %dl
	jne	.L988
	sarq	$32, %rdx
	movl	%edx, %eax
	sall	$15, %eax
	subl	%edx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	-148(%rbp), %eax
	leal	48(,%rax,8), %eax
	cltq
.L1012:
	movq	(%r15), %rdx
	movq	-1(%rax,%rdx), %r10
	movl	-76(%rbp), %ecx
	movq	%rcx, %rsi
	salq	$32, %rsi
	movq	%rsi, -1(%rax,%rdx)
	movq	(%r15), %r13
	movq	39(%r13), %rax
	movq	%r13, %rdi
	sarq	$32, %rax
	leal	4(%rax,%rcx,4), %esi
	movq	(%r9), %rax
	movl	%esi, -88(%rbp)
	movslq	%esi, %r14
	movq	39(%rax), %rdx
	movq	%r10, -96(%rbp)
	movq	%r8, -104(%rbp)
	sarq	$32, %rdx
	leal	4(%rbx,%rdx), %edx
	leal	16(,%rdx,8), %ebx
	movslq	%edx, %rdx
	subq	%rdx, %r14
	leaq	40(,%rdx,8), %r11
	movslq	%ebx, %rbx
	salq	$3, %r14
	movq	%r14, %rcx
	movq	%r15, %r14
	movq	%r11, %r15
.L1016:
	movq	-1(%rbx,%rax), %r12
	leaq	(%rbx,%rcx), %rax
	leaq	-1(%rax,%rdi), %r13
	movq	%r12, 0(%r13)
	testb	$1, %r12b
	je	.L1026
	movq	%r12, %rax
	andq	$-262144, %rax
	movq	8(%rax), %rdx
	movq	%rax, -72(%rbp)
	testl	$262144, %edx
	je	.L1014
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r9, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rax
	movq	-136(%rbp), %r9
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %rdi
	movq	8(%rax), %rdx
.L1014:
	andl	$24, %edx
	je	.L1026
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1026
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r9, -120(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-120(%rbp), %r9
	movq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L1026:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1058
	movq	(%r9), %rax
	movq	(%r14), %rdi
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1058:
	movl	-88(%rbp), %eax
	movq	(%r14), %r12
	movq	%r14, %r15
	movq	-96(%rbp), %r10
	movq	-104(%rbp), %r8
	leal	40(,%rax,8), %eax
	cltq
	leaq	-1(%r12,%rax), %r13
	movq	%r10, 0(%r13)
	testb	$1, %r10b
	je	.L1025
	movq	%r10, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L1018
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -96(%rbp)
	movq	%r8, -88(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r8
	movq	-72(%rbp), %r10
.L1018:
	testb	$24, %al
	je	.L1025
	movq	%r12, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L1025
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %r9
	movq	-72(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L1025:
	addl	$1, -76(%rbp)
	leaq	1(%r8), %rax
	cmpq	%r8, -144(%rbp)
	je	.L1067
.L1059:
	movq	%rax, %r8
	movq	(%r9), %rax
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1066:
	movl	-80(%rbp), %edi
	movq	%r8, %rsi
	salq	$32, %rsi
	leal	48(,%rdi,8), %edx
	movslq	%edx, %rdx
	movq	%rsi, -1(%rdx,%rax)
	leal	1(%rdi), %eax
	movl	%eax, -80(%rbp)
	leaq	1(%r8), %rax
	cmpq	%r8, -144(%rbp)
	jne	.L1059
.L1067:
	movq	(%r15), %rdi
.L1020:
	movq	-168(%rbp), %rax
	salq	$32, %rax
	movq	%rax, 23(%rdi)
	movq	(%r9), %r13
	movq	(%r15), %r12
	movq	%r12, 23(%r13)
	leaq	23(%r13), %r14
	testb	$1, %r12b
	je	.L1024
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L1068
	testb	$24, %al
	je	.L1024
.L1074:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1069
	.p2align 4,,10
	.p2align 3
.L1024:
	movq	%r15, %rax
.L1027:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1070
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L988:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L990
	movq	-1(%rdx), %rax
	cmpw	$64, 11(%rax)
	jbe	.L1065
	movq	-1(%rdx), %rax
	cmpw	$67, 11(%rax)
	je	.L1071
	movq	-1(%rdx), %rax
	cmpw	$66, 11(%rax)
	je	.L1072
	movq	-1(%rdx), %rax
	cmpw	$160, 11(%rax)
	je	.L1073
	movq	-160(%rbp), %rdi
	movq	%r9, -88(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %r9
	sarq	$32, %rax
	andl	-148(%rbp), %eax
	leal	48(,%rax,8), %eax
	cltq
	jmp	.L1012
.L1071:
	movq	15(%rdx), %rdx
.L1065:
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L1007
	shrl	$2, %eax
.L1062:
	andl	-148(%rbp), %eax
	leal	48(,%rax,8), %eax
	cltq
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	-160(%rbp), %rdi
	movq	%r9, -88(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-88(%rbp), %r9
	movq	-72(%rbp), %r8
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L1074
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L990:
	movq	7(%rdx), %rdx
	movq	-176(%rbp), %rax
	movq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L1012
	comisd	.LC3(%rip), %xmm0
	jb	.L995
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jb	.L995
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L995
	jne	.L995
	movl	%esi, %eax
	sall	$15, %eax
	subl	%esi, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L995:
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	andl	$1073741823, %eax
	jmp	.L1062
.L1069:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L1024
.L1072:
	movl	7(%rdx), %esi
	movl	$48, %eax
	andl	$2147483646, %esi
	je	.L1012
	movq	15(%rdx), %rdx
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L982:
	xorl	%eax, %eax
	jmp	.L1027
.L1073:
	movq	-160(%rbp), %rdi
	movq	%r9, -88(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	andl	-148(%rbp), %eax
	movq	-88(%rbp), %r9
	leal	48(,%rax,8), %eax
	movq	-72(%rbp), %r8
	cltq
	jmp	.L1012
.L1070:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21933:
	.size	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi, .-_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.section	.text._ZN2v88internal21OrderedNameDictionary6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21OrderedNameDictionary6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal21OrderedNameDictionary6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal21OrderedNameDictionary6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB19701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	testq	%rax, %rax
	je	.L1076
	movq	(%rbx), %rdx
	movq	(%rax), %rcx
	movq	15(%rdx), %rdx
	sarq	$32, %rdx
	salq	$32, %rdx
	movq	%rdx, 15(%rcx)
.L1076:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19701:
	.size	_ZN2v88internal21OrderedNameDictionary6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal21OrderedNameDictionary6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE
	.type	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE, @function
_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE:
.LFB21957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rcx
	movq	23(%rcx), %rdx
	movq	31(%rcx), %rax
	movq	39(%rcx), %rcx
	sarq	$32, %rax
	sarq	$32, %rdx
	sarq	$32, %rcx
	addl	%eax, %edx
	leal	(%rcx,%rcx), %r8d
	cmpl	%r8d, %edx
	jge	.L1082
	movq	%rsi, %rax
.L1083:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1082:
	.cfi_restore_state
	movl	%r8d, %edx
	sall	$2, %ecx
	sarl	%edx
	cmpl	%eax, %edx
	cmovg	%ecx, %r8d
	movl	%r8d, %edx
	call	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	testq	%rax, %rax
	je	.L1083
	movq	(%rbx), %rdx
	movq	(%rax), %rcx
	movq	15(%rdx), %rdx
	sarq	$32, %rdx
	salq	$32, %rdx
	movq	%rdx, 15(%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21957:
	.size	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE, .-_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal21OrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21OrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE
	.type	_ZN2v88internal21OrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE, @function
_ZN2v88internal21OrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE:
.LFB19710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE14EnsureGrowableEPNS0_7IsolateENS0_6HandleIS2_EE
	testq	%rax, %rax
	je	.L1117
	movq	0(%r13), %rdx
	movq	%rax, %rbx
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L1093
	shrl	$2, %eax
	movl	%eax, %ecx
.L1094:
	movq	(%rbx), %rdi
	movq	39(%rdi), %r13
	movq	39(%rdi), %rax
	sarq	$32, %rax
	sarq	$32, %r13
	subl	$1, %eax
	subl	$1, %r13d
	andl	%ecx, %eax
	andl	%ecx, %r13d
	leal	48(,%rax,8), %eax
	cltq
	movq	-1(%rdi,%rax), %r14
	movq	23(%rdi), %rcx
	movq	31(%rdi), %r9
	movq	39(%rdi), %rax
	sarq	$32, %rcx
	sarq	$32, %r14
	sarq	$32, %r9
	sarq	$32, %rax
	addl	%ecx, %r9d
	leal	6(%rax,%r9,4), %eax
	sall	$3, %eax
	movslq	%eax, %rsi
	leaq	-1(%rdi,%rsi), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L1102
	movq	%rdx, %r8
	andq	$-262144, %r8
	movq	8(%r8), %r10
	movq	%r8, -72(%rbp)
	testl	$262144, %r10d
	je	.L1096
	movq	%rcx, -120(%rbp)
	movl	%eax, -112(%rbp)
	movl	%r9d, -100(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	movq	-120(%rbp), %rcx
	movl	-112(%rbp), %eax
	movl	-100(%rbp), %r9d
	movq	8(%r8), %r10
	movq	-96(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
.L1096:
	andl	$24, %r10d
	je	.L1102
	movq	%rdi, %r8
	andq	$-262144, %r8
	testb	$24, 8(%r8)
	je	.L1118
	.p2align 4,,10
	.p2align 3
.L1102:
	movq	(%rbx), %rdi
	leal	8(%rax), %edx
	movq	(%r15), %r15
	movslq	%edx, %rdx
	leaq	-1(%rdi,%rdx), %rsi
	movq	%r15, (%rsi)
	testb	$1, %r15b
	je	.L1101
	movq	%r15, %r8
	andq	$-262144, %r8
	movq	8(%r8), %rdx
	movq	%r8, -72(%rbp)
	testl	$262144, %edx
	je	.L1099
	movq	%r15, %rdx
	movq	%rcx, -112(%rbp)
	movl	%eax, -100(%rbp)
	movl	%r9d, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r8
	movq	-112(%rbp), %rcx
	movl	-100(%rbp), %eax
	movl	-96(%rbp), %r9d
	movq	8(%r8), %rdx
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
.L1099:
	andl	$24, %edx
	je	.L1101
	movq	%rdi, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	je	.L1119
	.p2align 4,,10
	.p2align 3
.L1101:
	addl	%r12d, %r12d
	movq	(%rbx), %rsi
	leal	16(%rax), %edx
	addl	$24, %eax
	sarl	%r12d
	movslq	%edx, %rdx
	cltq
	salq	$32, %r14
	salq	$32, %r12
	salq	$32, %r9
	addl	$1, %ecx
	movq	%r12, -1(%rdx,%rsi)
	salq	$32, %rcx
	movq	(%rbx), %rdx
	movq	%r14, -1(%rax,%rdx)
	leal	48(,%r13,8), %eax
	cltq
	movq	(%rbx), %rdx
	movq	%r9, -1(%rax,%rdx)
	movq	(%rbx), %rax
	movq	%rcx, 23(%rax)
	movq	%rbx, %rax
.L1092:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1120
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1093:
	.cfi_restore_state
	leaq	-64(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	0(%r13), %rdx
	movl	%eax, %ecx
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1119:
	movq	%r15, %rdx
	movq	%rcx, -88(%rbp)
	movl	%eax, -80(%rbp)
	movl	%r9d, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movl	-80(%rbp), %eax
	movl	-72(%rbp), %r9d
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1118:
	movq	%rcx, -88(%rbp)
	movl	%eax, -80(%rbp)
	movl	%r9d, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movl	-80(%rbp), %eax
	movl	-72(%rbp), %r9d
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1117:
	xorl	%eax, %eax
	jmp	.L1092
.L1120:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19710:
	.size	_ZN2v88internal21OrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE, .-_ZN2v88internal21OrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_26SmallOrderedNameDictionaryEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_26SmallOrderedNameDictionaryEEE
	.type	_ZN2v88internal28OrderedNameDictionaryHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_26SmallOrderedNameDictionaryEEE, @function
_ZN2v88internal28OrderedNameDictionaryHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_26SmallOrderedNameDictionaryEEE:
.LFB19757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$512, %esi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE8AllocateEPNS0_7IsolateEiNS0_14AllocationTypeE
	testq	%rax, %rax
	je	.L1122
	movq	%rax, %r14
	movq	(%rax), %rax
	movq	$0, 15(%rax)
	movq	(%r15), %rax
	movzbl	15(%rax), %edx
	movzbl	16(%rax), %ecx
	addl	%ecx, %edx
	je	.L1123
	subl	$1, %edx
	movl	$24, %r12d
	leaq	(%rdx,%rdx,2), %rdx
	leaq	48(,%rdx,8), %rdx
	movq	%rdx, -56(%rbp)
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1140:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r13
.L1126:
	cmpq	%rsi, 96(%rbx)
	je	.L1129
	movq	(%r15), %rax
	movq	41112(%rbx), %rdi
	movq	7(%r12,%rax), %rsi
	testq	%rdi, %rdi
	je	.L1130
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1131:
	movq	(%r15), %rax
	movq	%r14, %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movl	19(%r12,%rax), %r8d
	call	_ZN2v88internal21OrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1122
.L1129:
	addq	$24, %r12
	cmpq	%r12, -56(%rbp)
	je	.L1123
	movq	(%r15), %rax
.L1124:
	movq	41112(%rbx), %rdi
	movq	-1(%rax,%r12), %rsi
	testq	%rdi, %rdi
	jne	.L1140
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L1141
.L1127:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, 0(%r13)
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1130:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L1142
.L1132:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1122:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1141:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1123:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1142:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L1132
	.cfi_endproc
.LFE19757:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_26SmallOrderedNameDictionaryEEE, .-_ZN2v88internal28OrderedNameDictionaryHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_26SmallOrderedNameDictionaryEEE
	.section	.text._ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE,"axG",@progbits,_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	.type	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE, @function
_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE:
.LFB21966:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdx
	movq	23(%rdx), %rax
	movq	39(%rdx), %rdx
	sarq	$32, %rdx
	sarq	$32, %rax
	leal	(%rdx,%rdx), %ecx
	sarl	$2, %ecx
	cmpl	%eax, %ecx
	jg	.L1144
	addq	$8, %rsp
	movq	%rsi, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1144:
	.cfi_restore_state
	call	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	testq	%rax, %rax
	je	.L1146
	movq	(%rbx), %rdx
	movq	(%rax), %rcx
	movq	15(%rdx), %rdx
	sarq	$32, %rdx
	salq	$32, %rdx
	movq	%rdx, 15(%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1146:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21966:
	.size	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE, .-_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal21OrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21OrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal21OrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal21OrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB19716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rsi), %rax
	movq	96(%rdi), %rdx
	movq	39(%rax), %rcx
	sarq	$32, %rcx
	leal	6(%rcx,%r8,4), %ebx
	leaq	-1(%rax), %rcx
	sall	$3, %ebx
	movslq	%ebx, %r15
	leal	8(%rbx), %r14d
	addq	%rcx, %r15
	movslq	%r14d, %r14
	movq	%rdx, (%r15)
	addq	%rcx, %r14
	testb	$1, %dl
	je	.L1150
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rsi
	testl	$262144, %esi
	jne	.L1169
	movq	%rax, %r8
	andq	$-262144, %r8
	andl	$24, %esi
	je	.L1152
.L1174:
	testb	$24, 8(%r8)
	je	.L1170
.L1152:
	movq	%rdx, (%r14)
	movq	%rcx, -56(%rbp)
	movq	8(%rcx), %rsi
	testl	$262144, %esi
	jne	.L1171
	andl	$24, %esi
	je	.L1155
.L1173:
	testb	$24, 8(%r8)
	je	.L1172
.L1155:
	addl	$16, %ebx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movabsq	$824633720832, %rdx
	movslq	%ebx, %rbx
	movq	%rdx, -1(%rax,%rbx)
	movq	(%r12), %rdx
	movq	23(%rdx), %rax
	sarq	$32, %rax
	subl	$1, %eax
	salq	$32, %rax
	movq	%rax, 23(%rdx)
	movq	(%r12), %rdx
	movq	31(%rdx), %rax
	sarq	$32, %rax
	addl	$1, %eax
	salq	$32, %rax
	movq	%rax, 31(%rdx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	.p2align 4,,10
	.p2align 3
.L1171:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rax
	movq	8(%rcx), %rsi
	andl	$24, %esi
	jne	.L1173
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1169:
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rcx, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %rcx
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rdx
	movq	8(%rcx), %rsi
	movq	%rax, %r8
	andq	$-262144, %r8
	andl	$24, %esi
	jne	.L1174
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1150:
	movq	%rdx, (%r14)
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rax
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1172:
	movq	%rax, %rdi
	movq	%r14, %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rax
	jmp	.L1155
	.cfi_endproc
.LFE19716:
	.size	_ZN2v88internal21OrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal21OrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE12SetDataEntryEiiNS0_6ObjectE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE12SetDataEntryEiiNS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE12SetDataEntryEiiNS0_6ObjectE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE12SetDataEntryEiiNS0_6ObjectE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE12SetDataEntryEiiNS0_6ObjectE:
.LFB22008:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sall	$3, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leal	16(%rdx,%rsi,8), %ebx
	movq	(%rdi), %rax
	movslq	%ebx, %rbx
	movq	%rcx, -1(%rbx,%rax)
	testb	$1, %cl
	je	.L1175
	movq	%rcx, %r14
	movq	%rdi, %r13
	movq	(%rdi), %rdi
	movq	%rcx, %r12
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testl	$262144, %eax
	jne	.L1187
	testb	$24, %al
	je	.L1175
.L1189:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1188
.L1175:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1187:
	.cfi_restore_state
	movq	%rcx, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testb	$24, %al
	jne	.L1189
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1188:
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE22008:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE12SetDataEntryEiiNS0_6ObjectE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE12SetDataEntryEiiNS0_6ObjectE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE10InitializeEPNS0_7IsolateEi,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE10InitializeEPNS0_7IsolateEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE10InitializeEPNS0_7IsolateEi
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE10InitializeEPNS0_7IsolateEi, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE10InitializeEPNS0_7IsolateEi:
.LFB22062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movl	$255, %esi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	shrl	$31, %edx
	movq	(%rdi), %rax
	addl	%ebx, %edx
	sarl	%edx
	movb	%dl, 9(%rax)
	movq	(%rdi), %rax
	addl	%ebx, %edx
	movslq	%edx, %rdx
	movb	$0, 7(%rax)
	movq	(%rdi), %rax
	movb	$0, 8(%rax)
	movq	(%rdi), %rax
	movl	$0, 10(%rax)
	movb	$0, 14(%rax)
	leal	0(,%rbx,8), %eax
	movq	(%rdi), %rcx
	cltq
	leaq	15(%rcx,%rax), %rdi
	call	memset@PLT
	movq	0(%r13), %rdi
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1191
	movq	96(%r12), %rax
	movslq	%ebx, %rcx
	addq	$15, %rdi
#APP
# 185 "../deps/v8/src/utils/memcopy.h" 1
	cld;rep ; stosq
# 0 "" 2
#NO_APP
.L1190:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1191:
	.cfi_restore_state
	testl	%ebx, %ebx
	jle	.L1190
	xorl	%r14d, %r14d
.L1194:
	movq	96(%r12), %rcx
	movl	%r14d, %esi
	xorl	%edx, %edx
	movq	%r13, %rdi
	addl	$1, %r14d
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE12SetDataEntryEiiNS0_6ObjectE
	cmpl	%r14d, %ebx
	jne	.L1194
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22062:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE10InitializeEPNS0_7IsolateEi, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE10InitializeEPNS0_7IsolateEi
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi,comdat
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi:
.LFB22058:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	%edx, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r14), %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	sete	%dl
	call	_ZN2v88internal7Factory22NewSmallOrderedHashSetEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r15
	movq	(%r14), %rax
	movzbl	7(%rax), %ecx
	movzbl	8(%rax), %edx
	movb	%cl, -81(%rbp)
	addl	%ecx, %edx
	je	.L1198
	subl	$1, %edx
	leaq	-64(%rbp), %rsi
	movl	$16, %ebx
	xorl	%r13d, %r13d
	movq	%rsi, -80(%rbp)
	leaq	24(,%rdx,8), %r8
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1200:
	addq	$8, %rbx
	cmpq	%r8, %rbx
	je	.L1198
.L1246:
	movq	(%r14), %rax
.L1226:
	movq	-1(%rbx,%rax), %rdx
	cmpq	%rdx, 96(%r12)
	je	.L1200
	testb	$1, %dl
	jne	.L1201
	sarq	$32, %rdx
	movl	%edx, %eax
	sall	$15, %eax
	subl	%edx, %eax
.L1241:
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
.L1225:
	movq	(%r15), %rcx
	movl	%r13d, %esi
	movq	%r8, -72(%rbp)
	addq	$8, %rbx
	movzbl	9(%rcx), %edi
	leal	-1(%rdi), %edx
	addl	$1, %edi
	andl	%edx, %eax
	sall	$4, %edi
	addl	%edi, %eax
	cltq
	leaq	-1(%rcx,%rax), %rax
	movzbl	(%rax), %ecx
	movb	%r13b, (%rax)
	movq	(%r15), %rdx
	movzbl	9(%rdx), %edi
	leal	1(%rdi), %eax
	sall	$4, %eax
	addl	%edi, %eax
	movq	-80(%rbp), %rdi
	addl	%r13d, %eax
	addl	$1, %r13d
	cltq
	movb	%cl, -1(%rdx,%rax)
	movq	(%r14), %rax
	xorl	%edx, %edx
	movq	-9(%rbx,%rax), %rcx
	movq	(%r15), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE12SetDataEntryEiiNS0_6ObjectE
	movq	-72(%rbp), %r8
	cmpq	%r8, %rbx
	jne	.L1246
.L1198:
	movq	(%r15), %rax
	movzbl	-81(%rbp), %esi
	movb	%sil, 7(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1247
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1201:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L1203
	movq	-1(%rdx), %rax
	cmpw	$64, 11(%rax)
	ja	.L1248
.L1245:
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L1220
	shrl	$2, %eax
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	-1(%rdx), %rax
	cmpw	$67, 11(%rax)
	je	.L1249
	movq	-1(%rdx), %rax
	cmpw	$66, 11(%rax)
	je	.L1250
	movq	-1(%rdx), %rax
	cmpw	$160, 11(%rax)
	je	.L1251
	movq	-80(%rbp), %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	movq	-72(%rbp), %r8
	shrq	$32, %rax
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1203:
	movq	7(%rdx), %rdx
	movl	$2147483647, %eax
	movq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L1225
	comisd	.LC3(%rip), %xmm0
	jb	.L1243
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jb	.L1243
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1243
	jne	.L1243
	movl	%ecx, %eax
	sall	$15, %eax
	subl	%ecx, %eax
	jmp	.L1241
.L1250:
	movl	7(%rdx), %eax
	shrl	%eax
	andl	$1073741823, %eax
	je	.L1225
	movq	15(%rdx), %rdx
	.p2align 4,,10
	.p2align 3
.L1243:
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	andl	$1073741823, %eax
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1220:
	movq	-80(%rbp), %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-72(%rbp), %r8
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1249:
	movq	15(%rdx), %rdx
	jmp	.L1245
.L1251:
	movq	-80(%rbp), %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	movq	-72(%rbp), %r8
	andl	$2147483647, %eax
	jmp	.L1225
.L1247:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22058:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.section	.text._ZN2v88internal19SmallOrderedHashSet6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SmallOrderedHashSet6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal19SmallOrderedHashSet6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal19SmallOrderedHashSet6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB19746:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.cfi_endproc
.LFE19746:
	.size	_ZN2v88internal19SmallOrderedHashSet6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal19SmallOrderedHashSet6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE:
.LFB22061:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	%rsi, %rax
	movzbl	9(%rdx), %r8d
	movzbl	7(%rdx), %edx
	movl	%r8d, %ecx
	shrl	%ecx
	cmpl	%edx, %ecx
	jg	.L1257
	ret
	.p2align 4,,10
	.p2align 3
.L1257:
	movl	%r8d, %edx
	jmp	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.cfi_endproc
.LFE22061:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE:
.LFB21999:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rcx
	movzbl	9(%rcx), %eax
	movzbl	8(%rcx), %ecx
	leal	(%rax,%rax), %edx
	cmpl	%ecx, %eax
	jle	.L1259
	leal	0(,%rax,4), %edx
	cmpl	$64, %eax
	je	.L1261
	cmpl	$254, %edx
	jle	.L1259
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1261:
	movl	$254, %edx
.L1259:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE21999:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE12SetDataEntryEiiNS0_6ObjectE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE12SetDataEntryEiiNS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE12SetDataEntryEiiNS0_6ObjectE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE12SetDataEntryEiiNS0_6ObjectE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE12SetDataEntryEiiNS0_6ObjectE:
.LFB22030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addl	$1, %esi
	sall	$4, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leal	(%rsi,%rdx,8), %ebx
	movq	(%rdi), %rax
	movslq	%ebx, %rbx
	movq	%rcx, -1(%rbx,%rax)
	testb	$1, %cl
	je	.L1265
	movq	%rcx, %r14
	movq	%rdi, %r13
	movq	(%rdi), %rdi
	movq	%rcx, %r12
	andq	$-262144, %r14
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testl	$262144, %eax
	jne	.L1277
	testb	$24, %al
	je	.L1265
.L1279:
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1278
.L1265:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1277:
	.cfi_restore_state
	movq	%rcx, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	0(%r13), %rdi
	movq	8(%r14), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testb	$24, %al
	jne	.L1279
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1278:
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE22030:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE12SetDataEntryEiiNS0_6ObjectE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE12SetDataEntryEiiNS0_6ObjectE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi,comdat
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi:
.LFB22059:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	%edx, %esi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	sete	%dl
	call	_ZN2v88internal7Factory22NewSmallOrderedHashMapEiNS0_14AllocationTypeE@PLT
	movq	%rax, %r14
	movq	(%r12), %rax
	movzbl	7(%rax), %ecx
	movzbl	8(%rax), %edx
	movb	%cl, -81(%rbp)
	addl	%ecx, %edx
	je	.L1281
	leal	-1(%rdx), %ebx
	movl	$16, %r15d
	leaq	-64(%rbp), %r13
	addq	$2, %rbx
	salq	$4, %rbx
	movq	%rbx, -80(%rbp)
	xorl	%ebx, %ebx
	jmp	.L1282
	.p2align 4,,10
	.p2align 3
.L1284:
	addq	$16, %r15
	cmpq	-80(%rbp), %r15
	je	.L1281
.L1329:
	movq	(%r12), %rax
.L1282:
	movq	-1(%r15,%rax), %rdx
	movq	-72(%rbp), %rax
	cmpq	%rdx, 96(%rax)
	je	.L1284
	testb	$1, %dl
	jne	.L1285
	sarq	$32, %rdx
	movl	%edx, %eax
	sall	$15, %eax
	subl	%edx, %eax
.L1324:
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
.L1309:
	movq	(%r14), %rcx
	movl	%ebx, %esi
	addq	$16, %r15
	movzbl	9(%rcx), %edx
	leal	-1(%rdx), %edi
	sall	$5, %edx
	andl	%edi, %eax
	leal	16(%rax,%rdx), %eax
	cltq
	leaq	-1(%rcx,%rax), %rax
	movzbl	(%rax), %edx
	movb	%bl, (%rax)
	movq	(%r14), %rcx
	movzbl	9(%rcx), %eax
	movl	%eax, %edi
	sall	$5, %edi
	leal	16(%rax,%rdi), %eax
	movq	%r13, %rdi
	addl	%ebx, %eax
	cltq
	movb	%dl, -1(%rcx,%rax)
	movq	(%r12), %rax
	xorl	%edx, %edx
	movq	-17(%r15,%rax), %rcx
	movq	(%r14), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE12SetDataEntryEiiNS0_6ObjectE
	movq	(%r12), %rax
	movl	%ebx, %esi
	movl	$1, %edx
	movq	%r13, %rdi
	addl	$1, %ebx
	movq	-9(%r15,%rax), %rcx
	movq	(%r14), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE12SetDataEntryEiiNS0_6ObjectE
	cmpq	-80(%rbp), %r15
	jne	.L1329
.L1281:
	movq	(%r14), %rax
	movzbl	-81(%rbp), %esi
	movb	%sil, 7(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1330
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1285:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L1287
	movq	-1(%rdx), %rax
	cmpw	$64, 11(%rax)
	ja	.L1331
.L1328:
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L1304
	shrl	$2, %eax
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1331:
	movq	-1(%rdx), %rax
	cmpw	$67, 11(%rax)
	je	.L1332
	movq	-1(%rdx), %rax
	cmpw	$66, 11(%rax)
	je	.L1333
	movq	-1(%rdx), %rax
	cmpw	$160, 11(%rax)
	je	.L1334
	movq	%r13, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	shrq	$32, %rax
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	7(%rdx), %rdx
	movl	$2147483647, %eax
	movq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L1309
	comisd	.LC3(%rip), %xmm0
	jb	.L1326
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jb	.L1326
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1326
	jne	.L1326
	movl	%ecx, %eax
	sall	$15, %eax
	subl	%ecx, %eax
	jmp	.L1324
.L1333:
	movl	7(%rdx), %eax
	shrl	%eax
	andl	$1073741823, %eax
	je	.L1309
	movq	15(%rdx), %rdx
	.p2align 4,,10
	.p2align 3
.L1326:
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	andl	$1073741823, %eax
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1304:
	movq	%r13, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1332:
	movq	15(%rdx), %rdx
	jmp	.L1328
.L1334:
	movq	%r13, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	andl	$2147483647, %eax
	jmp	.L1309
.L1330:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22059:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.section	.text._ZN2v88internal19SmallOrderedHashMap6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SmallOrderedHashMap6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal19SmallOrderedHashMap6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal19SmallOrderedHashMap6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB19747:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.cfi_endproc
.LFE19747:
	.size	_ZN2v88internal19SmallOrderedHashMap6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal19SmallOrderedHashMap6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE:
.LFB22067:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	%rsi, %rax
	movzbl	9(%rdx), %r8d
	movzbl	7(%rdx), %edx
	movl	%r8d, %ecx
	shrl	%ecx
	cmpl	%edx, %ecx
	jg	.L1340
	ret
	.p2align 4,,10
	.p2align 3
.L1340:
	movl	%r8d, %edx
	jmp	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.cfi_endproc
.LFE22067:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE:
.LFB22021:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rcx
	movzbl	9(%rcx), %eax
	movzbl	8(%rcx), %ecx
	leal	(%rax,%rax), %edx
	cmpl	%ecx, %eax
	jle	.L1342
	leal	0(,%rax,4), %edx
	cmpl	$64, %eax
	je	.L1344
	cmpl	$254, %edx
	jle	.L1342
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1344:
	movl	$254, %edx
.L1342:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22021:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi,comdat
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi, @function
_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi:
.LFB22060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movl	%edx, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -80(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rcx), %rax
	movq	%rcx, -72(%rbp)
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	sete	%dl
	call	_ZN2v88internal7Factory29NewSmallOrderedNameDictionaryEiNS0_14AllocationTypeE@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rcx), %rax
	movzbl	15(%rax), %esi
	movzbl	16(%rax), %edx
	movb	%sil, -93(%rbp)
	addl	%esi, %edx
	je	.L1349
	subl	$1, %edx
	xorl	%r14d, %r14d
	leaq	-64(%rbp), %rsi
	movl	$24, %r12d
	leaq	(%rdx,%rdx,2), %rdx
	movq	%rsi, -128(%rbp)
	leaq	48(,%rdx,8), %rdi
	movq	%rdi, -88(%rbp)
	movl	%r14d, %edi
	movq	%r15, %r14
	movl	%edi, %r15d
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1410:
	addq	$24, %r12
	cmpq	%r12, -88(%rbp)
	je	.L1403
.L1411:
	movq	(%rcx), %rax
.L1350:
	movq	-1(%rax,%r12), %rdx
	movq	-80(%rbp), %rax
	cmpq	%rdx, 96(%rax)
	je	.L1410
	testb	$1, %dl
	jne	.L1353
	sarq	$32, %rdx
	movl	%edx, %eax
	sall	$15, %eax
	subl	%edx, %eax
.L1405:
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
.L1377:
	movq	(%r14), %rsi
	movq	%r12, %rbx
	leaq	24(%r12), %r12
	movq	%r12, %r9
	movzbl	17(%rsi), %edi
	leal	-1(%rdi), %edx
	andl	%edx, %eax
	leal	(%rdi,%rdi,2), %edx
	sall	$4, %edx
	leal	24(%rax,%rdx), %eax
	cltq
	leaq	-1(%rsi,%rax), %rax
	movzbl	(%rax), %esi
	movb	%r15b, (%rax)
	movq	(%r14), %rdx
	movzbl	17(%rdx), %edi
	leal	(%rdi,%rdi,2), %eax
	sall	$4, %eax
	leal	24(%rdi,%rax), %eax
	addl	%r15d, %eax
	addl	$1, %r15d
	cltq
	movl	%r15d, -92(%rbp)
	movb	%sil, -1(%rdx,%rax)
	leal	(%r15,%r15,2), %eax
	movq	%r14, %r15
	sall	$3, %eax
	cltq
	subq	%rbx, %rax
.L1381:
	movq	(%rcx), %rdx
	movq	(%r15), %r12
	movq	-1(%rbx,%rdx), %r13
	leaq	(%rax,%rbx), %rdx
	leaq	-1(%r12,%rdx), %r14
	movq	%r13, (%r14)
	testb	$1, %r13b
	je	.L1382
	movq	%r13, %r10
	andq	$-262144, %r10
	movq	8(%r10), %rdx
	movq	%r10, -72(%rbp)
	testl	$262144, %edx
	je	.L1379
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r9, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-72(%rbp), %r10
	movq	-120(%rbp), %r9
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rcx
	movq	8(%r10), %rdx
.L1379:
	andl	$24, %edx
	je	.L1382
	movq	%r12, %rdx
	andq	$-262144, %rdx
	testb	$24, 8(%rdx)
	jne	.L1382
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r9, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rcx, -72(%rbp)
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %rax
	movq	-72(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L1382:
	addq	$8, %rbx
	cmpq	%rbx, %r9
	jne	.L1381
	movq	%r15, %r14
	movq	%r9, %r12
	movl	-92(%rbp), %r15d
	cmpq	%r12, -88(%rbp)
	jne	.L1411
.L1403:
	movq	%r14, %r15
.L1349:
	movq	(%r15), %rax
	movzbl	-93(%rbp), %edi
	movb	%dil, 15(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1412
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1353:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$65, 11(%rax)
	je	.L1355
	movq	-1(%rdx), %rax
	cmpw	$64, 11(%rax)
	ja	.L1413
.L1409:
	movl	7(%rdx), %eax
	testb	$1, %al
	jne	.L1372
	shrl	$2, %eax
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1413:
	movq	-1(%rdx), %rax
	cmpw	$67, 11(%rax)
	je	.L1414
	movq	-1(%rdx), %rax
	cmpw	$66, 11(%rax)
	je	.L1415
	movq	-1(%rdx), %rax
	cmpw	$160, 11(%rax)
	je	.L1416
	movq	-128(%rbp), %rdi
	movq	%rcx, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	movq	-72(%rbp), %rcx
	shrq	$32, %rax
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1355:
	movq	7(%rdx), %rdx
	movl	$2147483647, %eax
	movq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L1377
	comisd	.LC3(%rip), %xmm0
	jb	.L1407
	movsd	.LC4(%rip), %xmm2
	comisd	%xmm0, %xmm2
	jb	.L1407
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1407
	jne	.L1407
	movl	%esi, %eax
	sall	$15, %eax
	subl	%esi, %eax
	jmp	.L1405
.L1415:
	movl	7(%rdx), %eax
	shrl	%eax
	andl	$1073741823, %eax
	je	.L1377
	movq	15(%rdx), %rdx
	.p2align 4,,10
	.p2align 3
.L1407:
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	andl	$1073741823, %eax
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1372:
	movq	-128(%rbp), %rdi
	movq	%rcx, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	-72(%rbp), %rcx
	jmp	.L1377
.L1414:
	movq	15(%rdx), %rdx
	jmp	.L1409
.L1416:
	movq	-128(%rbp), %rdi
	movq	%rcx, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	movq	-72(%rbp), %rcx
	andl	$2147483647, %eax
	jmp	.L1377
.L1412:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22060:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi, .-_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	.section	.text._ZN2v88internal26SmallOrderedNameDictionary6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SmallOrderedNameDictionary6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal26SmallOrderedNameDictionary6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal26SmallOrderedNameDictionary6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB19748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	movq	(%rbx), %rdx
	movl	7(%rdx), %ecx
	movq	(%rax), %rdx
	movl	%ecx, 7(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19748:
	.size	_ZN2v88internal26SmallOrderedNameDictionary6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal26SmallOrderedNameDictionary6RehashEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE,comdat
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE:
.LFB22057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rax
	movzbl	17(%rax), %edx
	movzbl	15(%rax), %eax
	movl	%edx, %ecx
	shrl	%ecx
	cmpl	%eax, %ecx
	jg	.L1420
	addq	$8, %rsp
	movq	%rsi, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1420:
	.cfi_restore_state
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	movq	(%rbx), %rdx
	movl	7(%rdx), %ecx
	movq	(%rax), %rdx
	movl	%ecx, 7(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22057:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE, .-_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	.section	.text._ZN2v88internal26SmallOrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SmallOrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal26SmallOrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal26SmallOrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB19744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	movl	$1, %edx
	subq	$24, %rsp
	movq	96(%rdi), %r15
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	%ebx, %esi
	movq	%r15, %rcx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	xorl	%edx, %edx
	movq	%r15, %rcx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	leal	3(%rbx,%rbx,2), %eax
	movq	-64(%rbp), %rdx
	movq	%r12, %rsi
	leal	16(,%rax,8), %eax
	movq	%r13, %rdi
	movabsq	$824633720832, %rcx
	cltq
	movq	%rcx, -1(%rax,%rdx)
	movq	(%r12), %rax
	subb	$1, 15(%rax)
	movq	(%r12), %rax
	addb	$1, 16(%rax)
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1426
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1426:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19744:
	.size	_ZN2v88internal26SmallOrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal26SmallOrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler11DeleteEntryEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler11DeleteEntryEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEEi
	.type	_ZN2v88internal28OrderedNameDictionaryHandler11DeleteEntryEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEEi, @function
_ZN2v88internal28OrderedNameDictionaryHandler11DeleteEntryEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEEi:
.LFB19773:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	-1(%rax), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpw	$163, 11(%rax)
	je	.L1431
	call	_ZN2v88internal21OrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1431:
	.cfi_restore_state
	call	_ZN2v88internal26SmallOrderedNameDictionary11DeleteEntryEPNS0_7IsolateENS0_6HandleIS1_EEi
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19773:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler11DeleteEntryEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEEi, .-_ZN2v88internal28OrderedNameDictionaryHandler11DeleteEntryEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEEi
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler6ShrinkEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler6ShrinkEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal28OrderedNameDictionaryHandler6ShrinkEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal28OrderedNameDictionaryHandler6ShrinkEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE:
.LFB19772:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	-1(%rax), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpw	$163, 11(%rax)
	je	.L1436
	call	_ZN2v88internal16OrderedHashTableINS0_21OrderedNameDictionaryELi3EE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1436:
	.cfi_restore_state
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6ShrinkEPNS0_7IsolateENS0_6HandleIS2_EE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19772:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler6ShrinkEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal28OrderedNameDictionaryHandler6ShrinkEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal26SmallOrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal26SmallOrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE
	.type	_ZN2v88internal26SmallOrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE, @function
_ZN2v88internal26SmallOrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE:
.LFB19737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r8d, -68(%rbp)
	movq	(%rsi), %rdx
	movzbl	15(%rdx), %r12d
	movzbl	16(%rdx), %r9d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	17(%rdx), %eax
	leal	(%r12,%r9), %r10d
	leal	(%rax,%rax), %r8d
	cmpl	%r8d, %r10d
	jl	.L1445
	movl	%r8d, %edx
	movq	%rsi, %r15
	sarl	%edx
	cmpl	%edx, %r9d
	jge	.L1439
	leal	0(,%rax,4), %r8d
	cmpl	$64, %eax
	je	.L1444
	xorl	%eax, %eax
	cmpl	$254, %r8d
	jg	.L1440
.L1439:
	movl	%r8d, %edx
	movq	%r15, %rsi
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE6RehashEPNS0_7IsolateENS0_6HandleIS2_EEi
	movq	-80(%rbp), %rcx
	movq	%rax, %rbx
	movq	(%r15), %rax
	movl	7(%rax), %edx
	movq	(%rbx), %rax
	movl	%edx, 7(%rax)
	movq	(%rbx), %rdx
	movq	(%r14), %rdi
	movzbl	15(%rdx), %r12d
	movl	7(%rdi), %eax
	movl	%r12d, %r13d
	testb	$1, %al
	jne	.L1441
.L1448:
	shrl	$2, %eax
	leaq	-64(%rbp), %r15
.L1442:
	movzbl	17(%rdx), %r8d
	movq	(%rcx), %rcx
	movq	%r15, %rdi
	leal	-1(%r8), %r9d
	leal	(%r8,%r8,2), %r8d
	andl	%r9d, %eax
	sall	$4, %r8d
	leal	24(%rax,%r8), %r8d
	movl	%eax, -84(%rbp)
	movslq	%r8d, %r8
	movzbl	-1(%rdx,%r8), %r9d
	movzbl	16(%rdx), %r8d
	movq	%rdx, -64(%rbp)
	movl	$1, %edx
	addl	%r8d, %r12d
	movb	%r9b, -69(%rbp)
	movl	%r12d, %esi
	movb	%r8b, -80(%rbp)
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	movq	(%rbx), %rdx
	movq	(%r14), %rcx
	movq	%r15, %rdi
	movl	%r12d, %esi
	movq	%rdx, -64(%rbp)
	xorl	%edx, %edx
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	movl	-68(%rbp), %ecx
	movq	(%rbx), %rdx
	movl	%r12d, %esi
	movq	%r15, %rdi
	addl	%ecx, %ecx
	movq	%rdx, -64(%rbp)
	movl	$2, %edx
	sarl	%ecx
	salq	$32, %rcx
	call	_ZN2v88internal21SmallOrderedHashTableINS0_26SmallOrderedNameDictionaryEE12SetDataEntryEiiNS0_6ObjectE
	movq	(%rbx), %rcx
	movl	-84(%rbp), %eax
	movzbl	-80(%rbp), %r10d
	movzbl	-69(%rbp), %r9d
	movzbl	17(%rcx), %edx
	leal	(%r10,%r13), %r15d
	addl	$1, %r13d
	leal	(%rdx,%rdx,2), %edx
	sall	$4, %edx
	leal	24(%rax,%rdx), %eax
	cltq
	movb	%r15b, -1(%rcx,%rax)
	movq	(%rbx), %rcx
	movzbl	17(%rcx), %edx
	leal	(%rdx,%rdx,2), %eax
	sall	$4, %eax
	leal	24(%rdx,%rax), %eax
	leal	(%rax,%r12), %esi
	movslq	%esi, %rsi
	movb	%r9b, -1(%rcx,%rsi)
	movq	(%rbx), %rax
	movb	%r13b, 15(%rax)
	movq	%rbx, %rax
.L1440:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1447
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1445:
	.cfi_restore_state
	movq	(%r14), %rdi
	movq	%rsi, %rbx
	movl	%r12d, %r13d
	movl	7(%rdi), %eax
	testb	$1, %al
	je	.L1448
.L1441:
	leaq	-64(%rbp), %r15
	movq	%rdi, -64(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -80(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	(%rbx), %rdx
	movq	-80(%rbp), %rcx
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1444:
	movl	$254, %r8d
	jmp	.L1439
.L1447:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19737:
	.size	_ZN2v88internal26SmallOrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE, .-_ZN2v88internal26SmallOrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE
	.section	.text._ZN2v88internal28OrderedNameDictionaryHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28OrderedNameDictionaryHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE
	.type	_ZN2v88internal28OrderedNameDictionaryHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE, @function
_ZN2v88internal28OrderedNameDictionaryHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE:
.LFB19760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	cmpw	$163, 11(%rax)
	je	.L1456
.L1450:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal21OrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE
.L1454:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1456:
	.cfi_restore_state
	movl	%r8d, -36(%rbp)
	movq	%rsi, %r12
	call	_ZN2v88internal26SmallOrderedNameDictionary3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE
	testq	%rax, %rax
	jne	.L1454
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal28OrderedNameDictionaryHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_26SmallOrderedNameDictionaryEEE
	movl	-36(%rbp), %r8d
	movq	%rax, %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.L1450
	jmp	.L1454
	.cfi_endproc
.LFE19760:
	.size	_ZN2v88internal28OrderedNameDictionaryHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE, .-_ZN2v88internal28OrderedNameDictionaryHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_15PropertyDetailsE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE9FindEntryEPNS0_7IsolateENS0_6ObjectE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE9FindEntryEPNS0_7IsolateENS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE9FindEntryEPNS0_7IsolateENS0_6ObjectE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE9FindEntryEPNS0_7IsolateENS0_6ObjectE:
.LFB23401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	andl	$1, %edx
	jne	.L1458
	movq	%r13, %rdx
	sarq	$32, %rdx
	movl	%edx, %eax
	sall	$15, %eax
	subl	%edx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
	salq	$32, %rax
.L1481:
	cmpq	88(%r12), %rax
	je	.L1484
	movq	(%rbx), %rcx
	sarq	$32, %rax
	movzbl	9(%rcx), %esi
	leal	-1(%rsi), %edx
	addl	$1, %esi
	andl	%edx, %eax
	sall	$4, %esi
	addl	%esi, %eax
	cltq
	movzbl	-1(%rcx,%rax), %r12d
	cmpl	$255, %r12d
	je	.L1484
	leaq	-48(%rbp), %r14
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1506:
	movq	(%rbx), %rcx
	movzbl	9(%rcx), %edx
	leal	1(%rdx), %eax
	sall	$4, %eax
	addl	%edx, %eax
	addl	%r12d, %eax
	cltq
	movzbl	-1(%rcx,%rax), %r12d
	cmpl	$255, %r12d
	je	.L1484
.L1486:
	leal	16(,%r12,8), %eax
	movq	%r13, %rsi
	movq	%r14, %rdi
	cltq
	movq	-1(%rcx,%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Object13SameValueZeroES1_@PLT
	testb	%al, %al
	je	.L1506
.L1457:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1507
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1484:
	.cfi_restore_state
	movl	$255, %r12d
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1458:
	movq	-1(%r13), %rax
	cmpw	$65, 11(%rax)
	je	.L1460
	movq	-1(%r13), %rax
	cmpw	$64, 11(%rax)
	ja	.L1508
	movl	7(%r13), %eax
	testb	$1, %al
	je	.L1505
	leaq	-48(%rbp), %r14
	movq	%r13, -48(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
.L1504:
	salq	$32, %rax
	jmp	.L1481
	.p2align 4,,10
	.p2align 3
.L1508:
	movq	-1(%r13), %rax
	cmpw	$67, 11(%rax)
	je	.L1509
	movq	-1(%r13), %rax
	cmpw	$66, 11(%rax)
	je	.L1510
	movq	-1(%r13), %rax
	cmpw	$160, 11(%rax)
	je	.L1511
	leaq	-48(%rbp), %r14
	movq	%r13, -48(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	jmp	.L1481
	.p2align 4,,10
	.p2align 3
.L1460:
	movabsq	$9223372032559808512, %rax
	movq	7(%r13), %rdx
	movq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L1481
	comisd	.LC3(%rip), %xmm0
	jb	.L1464
	movsd	.LC4(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1464
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1464
	jne	.L1464
	movl	%ecx, %eax
	sall	$15, %eax
	subl	%ecx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
	jmp	.L1504
	.p2align 4,,10
	.p2align 3
.L1464:
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	andl	$1073741823, %eax
	jmp	.L1504
.L1505:
	shrl	$2, %eax
	jmp	.L1504
.L1510:
	movl	7(%r13), %edx
	xorl	%eax, %eax
	andl	$2147483646, %edx
	je	.L1481
	movq	15(%r13), %rdx
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	andl	$1073741823, %eax
	jmp	.L1504
.L1509:
	movq	15(%r13), %rdx
	movl	7(%rdx), %eax
	testb	$1, %al
	je	.L1505
	leaq	-48(%rbp), %r14
	movq	%rdx, -48(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	jmp	.L1504
.L1511:
	leaq	-48(%rbp), %r14
	movq	%r13, -48(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	andl	$2147483647, %eax
	salq	$32, %rax
	jmp	.L1481
.L1507:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23401:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE9FindEntryEPNS0_7IsolateENS0_6ObjectE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB22013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdx), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$255, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE22013:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal19SmallOrderedHashSet6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SmallOrderedHashSet6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal19SmallOrderedHashSet6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal19SmallOrderedHashSet6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB19729:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.cfi_endproc
.LFE19729:
	.size	_ZN2v88internal19SmallOrderedHashSet6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal19SmallOrderedHashSet6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal19SmallOrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SmallOrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal19SmallOrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, @function
_ZN2v88internal19SmallOrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE:
.LFB19724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rdi, %rsi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1516
	movq	%r12, %rax
.L1517:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1522
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1516:
	.cfi_restore_state
	movq	(%r12), %rax
	movzbl	7(%rax), %edx
	movzbl	8(%rax), %ecx
	movzbl	9(%rax), %eax
	addl	%ecx, %edx
	addl	%eax, %eax
	cmpl	%eax, %edx
	jl	.L1518
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L1517
.L1518:
	movq	(%rbx), %rax
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object15GetOrCreateHashEPNS0_7IsolateE@PLT
	movq	0(%r13), %rdx
	movq	%r14, %rdi
	sarq	$32, %rax
	movzbl	9(%rdx), %ecx
	movzbl	7(%rdx), %esi
	leal	-1(%rcx), %r15d
	addl	$1, %ecx
	movl	%esi, %r12d
	andl	%eax, %r15d
	sall	$4, %ecx
	movzbl	8(%rdx), %eax
	addl	%r15d, %ecx
	movslq	%ecx, %rcx
	addl	%eax, %esi
	movb	%al, -69(%rbp)
	movzbl	-1(%rdx,%rcx), %r8d
	movq	(%rbx), %rcx
	movq	%rdx, -64(%rbp)
	xorl	%edx, %edx
	movl	%esi, -68(%rbp)
	movb	%r8b, -70(%rbp)
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE12SetDataEntryEiiNS0_6ObjectE
	movq	0(%r13), %rdx
	movzbl	-69(%rbp), %r9d
	movl	-68(%rbp), %esi
	movzbl	-70(%rbp), %r8d
	movzbl	9(%rdx), %eax
	addl	%r12d, %r9d
	addl	$1, %r12d
	addl	$1, %eax
	sall	$4, %eax
	addl	%eax, %r15d
	movslq	%r15d, %r15
	movb	%r9b, -1(%rdx,%r15)
	movq	0(%r13), %rdx
	movzbl	9(%rdx), %ecx
	leal	1(%rcx), %eax
	sall	$4, %eax
	addl	%ecx, %eax
	addl	%esi, %eax
	cltq
	movb	%r8b, -1(%rdx,%rax)
	movq	0(%r13), %rax
	movb	%r12b, 7(%rax)
	movq	%r13, %rax
	jmp	.L1517
.L1522:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19724:
	.size	_ZN2v88internal19SmallOrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, .-_ZN2v88internal19SmallOrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal21OrderedHashSetHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21OrderedHashSetHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEE
	.type	_ZN2v88internal21OrderedHashSetHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEE, @function
_ZN2v88internal21OrderedHashSetHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEE:
.LFB19759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	cmpw	$162, 11(%rax)
	je	.L1530
.L1524:
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
.L1528:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1530:
	.cfi_restore_state
	movq	%rsi, %r12
	call	_ZN2v88internal19SmallOrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	testq	%rax, %rax
	jne	.L1528
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal21OrderedHashSetHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashSetEEE
	movq	%rax, %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.L1524
	jmp	.L1528
	.cfi_endproc
.LFE19759:
	.size	_ZN2v88internal21OrderedHashSetHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEE, .-_ZN2v88internal21OrderedHashSetHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEE
	.section	.text._ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE,"axG",@progbits,_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE,comdat
	.p2align 4
	.weak	_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE
	.type	_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE, @function
_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE:
.LFB22081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-1(%rsi), %rax
	cmpw	$162, 11(%rax)
	jne	.L1532
	leaq	-16(%rbp), %r8
	movq	%rsi, -16(%rbp)
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
.L1531:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1536
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1532:
	.cfi_restore_state
	movq	(%rdx), %rdx
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashSetELi1EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE
	jmp	.L1531
.L1536:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22081:
	.size	_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE, .-_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashSetENS0_14OrderedHashSetEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE,comdat
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE:
.LFB22012:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-40(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%rsi, -40(%rbp)
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	movl	%eax, %esi
	xorl	%eax, %eax
	cmpl	$255, %esi
	jne	.L1543
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1543:
	.cfi_restore_state
	movq	-40(%rbp), %rax
	movq	96(%rbx), %rcx
	movq	%r12, %rdi
	xorl	%edx, %edx
	movzbl	7(%rax), %r14d
	movzbl	8(%rax), %r13d
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE12SetDataEntryEiiNS0_6ObjectE
	movq	-40(%rbp), %rax
	subl	$1, %r14d
	addl	$1, %r13d
	movb	%r14b, 7(%rax)
	movq	-40(%rbp), %rax
	movb	%r13b, 8(%rax)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22012:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE
	.section	.text._ZN2v88internal19SmallOrderedHashSet6DeleteEPNS0_7IsolateES1_NS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SmallOrderedHashSet6DeleteEPNS0_7IsolateES1_NS0_6ObjectE
	.type	_ZN2v88internal19SmallOrderedHashSet6DeleteEPNS0_7IsolateES1_NS0_6ObjectE, @function
_ZN2v88internal19SmallOrderedHashSet6DeleteEPNS0_7IsolateES1_NS0_6ObjectE:
.LFB19728:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashSetEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE
	.cfi_endproc
.LFE19728:
	.size	_ZN2v88internal19SmallOrderedHashSet6DeleteEPNS0_7IsolateES1_NS0_6ObjectE, .-_ZN2v88internal19SmallOrderedHashSet6DeleteEPNS0_7IsolateES1_NS0_6ObjectE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE9FindEntryEPNS0_7IsolateENS0_6ObjectE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE9FindEntryEPNS0_7IsolateENS0_6ObjectE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE9FindEntryEPNS0_7IsolateENS0_6ObjectE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE9FindEntryEPNS0_7IsolateENS0_6ObjectE:
.LFB23418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	andl	$1, %edx
	jne	.L1546
	movq	%r13, %rdx
	sarq	$32, %rdx
	movl	%edx, %eax
	sall	$15, %eax
	subl	%edx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
	salq	$32, %rax
.L1569:
	cmpq	88(%r12), %rax
	je	.L1572
	movq	(%rbx), %rcx
	sarq	$32, %rax
	movzbl	9(%rcx), %edx
	leal	-1(%rdx), %esi
	sall	$5, %edx
	andl	%esi, %eax
	leal	16(%rax,%rdx), %eax
	cltq
	movzbl	-1(%rcx,%rax), %r12d
	cmpl	$255, %r12d
	je	.L1572
	leaq	-48(%rbp), %r14
	jmp	.L1574
	.p2align 4,,10
	.p2align 3
.L1594:
	movq	(%rbx), %rcx
	movzbl	9(%rcx), %eax
	movl	%eax, %edx
	sall	$5, %edx
	leal	16(%rax,%rdx), %eax
	addl	%r12d, %eax
	cltq
	movzbl	-1(%rcx,%rax), %r12d
	cmpl	$255, %r12d
	je	.L1572
.L1574:
	leal	1(%r12), %eax
	movq	%r13, %rsi
	movq	%r14, %rdi
	sall	$4, %eax
	cltq
	movq	-1(%rcx,%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Object13SameValueZeroES1_@PLT
	testb	%al, %al
	je	.L1594
.L1545:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1595
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1572:
	.cfi_restore_state
	movl	$255, %r12d
	jmp	.L1545
	.p2align 4,,10
	.p2align 3
.L1546:
	movq	-1(%r13), %rax
	cmpw	$65, 11(%rax)
	je	.L1548
	movq	-1(%r13), %rax
	cmpw	$64, 11(%rax)
	ja	.L1596
	movl	7(%r13), %eax
	testb	$1, %al
	je	.L1593
	leaq	-48(%rbp), %r14
	movq	%r13, -48(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
.L1592:
	salq	$32, %rax
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1596:
	movq	-1(%r13), %rax
	cmpw	$67, 11(%rax)
	je	.L1597
	movq	-1(%r13), %rax
	cmpw	$66, 11(%rax)
	je	.L1598
	movq	-1(%r13), %rax
	cmpw	$160, 11(%rax)
	je	.L1599
	leaq	-48(%rbp), %r14
	movq	%r13, -48(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1548:
	movabsq	$9223372032559808512, %rax
	movq	7(%r13), %rdx
	movq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L1569
	comisd	.LC3(%rip), %xmm0
	jb	.L1552
	movsd	.LC4(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1552
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1552
	jne	.L1552
	movl	%ecx, %eax
	sall	$15, %eax
	subl	%ecx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1073741823, %eax
	jmp	.L1592
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	andl	$1073741823, %eax
	jmp	.L1592
.L1593:
	shrl	$2, %eax
	jmp	.L1592
.L1598:
	movl	7(%r13), %edx
	xorl	%eax, %eax
	andl	$2147483646, %edx
	je	.L1569
	movq	15(%r13), %rdx
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rdx, %rax
	andl	$1073741823, %eax
	jmp	.L1592
.L1597:
	movq	15(%r13), %rdx
	movl	7(%rdx), %eax
	testb	$1, %al
	je	.L1593
	leaq	-48(%rbp), %r14
	movq	%rdx, -48(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	jmp	.L1592
.L1599:
	leaq	-48(%rbp), %r14
	movq	%r13, -48(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	andl	$2147483647, %eax
	salq	$32, %rax
	jmp	.L1569
.L1595:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23418:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE9FindEntryEPNS0_7IsolateENS0_6ObjectE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB22035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdx), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$255, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE22035:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal19SmallOrderedHashMap6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SmallOrderedHashMap6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal19SmallOrderedHashMap6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal19SmallOrderedHashMap6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE:
.LFB19735:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.cfi_endproc
.LFE19735:
	.size	_ZN2v88internal19SmallOrderedHashMap6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal19SmallOrderedHashMap6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal19SmallOrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SmallOrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_
	.type	_ZN2v88internal19SmallOrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_, @function
_ZN2v88internal19SmallOrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_:
.LFB19730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%rcx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rdi, %rsi
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L1604
	movq	%r12, %rax
.L1605:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1610
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1604:
	.cfi_restore_state
	movq	(%r12), %rax
	movzbl	7(%rax), %edx
	movzbl	8(%rax), %ecx
	movzbl	9(%rax), %eax
	addl	%ecx, %edx
	addl	%eax, %eax
	cmpl	%eax, %edx
	jl	.L1606
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE4GrowEPNS0_7IsolateENS0_6HandleIS2_EE
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L1605
.L1606:
	movq	(%rbx), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object15GetOrCreateHashEPNS0_7IsolateE@PLT
	movq	0(%r13), %rdx
	movq	%r15, %rdi
	sarq	$32, %rax
	movzbl	9(%rdx), %ecx
	movzbl	7(%rdx), %r14d
	leal	-1(%rcx), %r11d
	sall	$5, %ecx
	movl	%r14d, %r12d
	andl	%eax, %r11d
	leal	16(%r11,%rcx), %eax
	movl	%r11d, -80(%rbp)
	cltq
	movzbl	-1(%rdx,%rax), %r9d
	movzbl	8(%rdx), %eax
	movq	%rdx, -64(%rbp)
	movl	$1, %edx
	addl	%eax, %r14d
	movb	%al, -73(%rbp)
	movq	-72(%rbp), %rax
	movl	%r14d, %esi
	movb	%r9b, -74(%rbp)
	movq	(%rax), %rcx
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE12SetDataEntryEiiNS0_6ObjectE
	movq	0(%r13), %rax
	movq	(%rbx), %rcx
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE12SetDataEntryEiiNS0_6ObjectE
	movq	0(%r13), %rdx
	movl	-80(%rbp), %r11d
	movzbl	-73(%rbp), %r10d
	movzbl	-74(%rbp), %r9d
	movzbl	9(%rdx), %eax
	addl	%r12d, %r10d
	addl	$1, %r12d
	sall	$5, %eax
	leal	16(%r11,%rax), %eax
	cltq
	movb	%r10b, -1(%rdx,%rax)
	movq	0(%r13), %rdx
	movzbl	9(%rdx), %eax
	movl	%eax, %ecx
	sall	$5, %ecx
	leal	16(%rax,%rcx), %eax
	addl	%r14d, %eax
	cltq
	movb	%r9b, -1(%rdx,%rax)
	movq	0(%r13), %rax
	movb	%r12b, 7(%rax)
	movq	%r13, %rax
	jmp	.L1605
.L1610:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19730:
	.size	_ZN2v88internal19SmallOrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_, .-_ZN2v88internal19SmallOrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_
	.section	.text._ZN2v88internal21OrderedHashMapHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEES8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21OrderedHashMapHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEES8_
	.type	_ZN2v88internal21OrderedHashMapHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEES8_, @function
_ZN2v88internal21OrderedHashMapHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEES8_:
.LFB19758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	cmpw	$161, 11(%rax)
	je	.L1618
.L1612:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal14OrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_
.L1616:
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1618:
	.cfi_restore_state
	movq	%rsi, %r12
	call	_ZN2v88internal19SmallOrderedHashMap3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEES7_
	testq	%rax, %rax
	jne	.L1616
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal21OrderedHashMapHandler20AdjustRepresentationEPNS0_7IsolateENS0_6HandleINS0_19SmallOrderedHashMapEEE
	movq	%rax, %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.L1612
	jmp	.L1616
	.cfi_endproc
.LFE19758:
	.size	_ZN2v88internal21OrderedHashMapHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEES8_, .-_ZN2v88internal21OrderedHashMapHandler3AddEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS4_INS0_6ObjectEEES8_
	.section	.text._ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE,"axG",@progbits,_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE,comdat
	.p2align 4
	.weak	_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE
	.type	_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE, @function
_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE:
.LFB22082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-1(%rsi), %rax
	cmpw	$161, 11(%rax)
	jne	.L1620
	leaq	-16(%rbp), %r8
	movq	%rsi, -16(%rbp)
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE
.L1619:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1624
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1620:
	.cfi_restore_state
	movq	(%rdx), %rdx
	call	_ZN2v88internal16OrderedHashTableINS0_14OrderedHashMapELi2EE6HasKeyEPNS0_7IsolateES2_NS0_6ObjectE
	jmp	.L1619
.L1624:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22082:
	.size	_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE, .-_ZN2v88internal23OrderedHashTableHandlerINS0_19SmallOrderedHashMapENS0_14OrderedHashMapEE6HasKeyEPNS0_7IsolateENS0_6HandleINS0_10HeapObjectEEENS7_INS0_6ObjectEEE
	.section	.text._ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE,"axG",@progbits,_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE,comdat
	.p2align 4
	.weak	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE
	.type	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE, @function
_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE:
.LFB22034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-56(%rbp), %rdi
	subq	$24, %rsp
	movq	%rsi, -56(%rbp)
	movq	%rbx, %rsi
	call	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE9FindEntryEPNS0_7IsolateENS0_6ObjectE
	xorl	%r8d, %r8d
	cmpl	$255, %eax
	je	.L1625
	movq	-56(%rbp), %rdi
	movq	96(%rbx), %r14
	addl	$1, %eax
	sall	$4, %eax
	movzbl	7(%rdi), %ecx
	movq	%r14, %r15
	movslq	%eax, %rbx
	movq	%r14, %r12
	notq	%r15
	leaq	16(%rbx), %r13
	andq	$-262144, %r12
	movb	%cl, -58(%rbp)
	movzbl	8(%rdi), %ecx
	andl	$1, %r15d
	movb	%cl, -57(%rbp)
.L1630:
	movq	%r14, -1(%rdi,%rbx)
	movq	-56(%rbp), %rdi
	testb	%r15b, %r15b
	jne	.L1631
	movq	8(%r12), %rax
	leaq	-1(%rdi,%rbx), %rsi
	testl	$262144, %eax
	je	.L1628
	movq	%r14, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rdi
	movq	8(%r12), %rax
	leaq	-1(%rdi,%rbx), %rsi
.L1628:
	testb	$24, %al
	je	.L1631
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L1643
	.p2align 4,,10
	.p2align 3
.L1631:
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L1630
	movzbl	-58(%rbp), %r13d
	movzbl	-57(%rbp), %r12d
	movl	$1, %r8d
	subl	$1, %r13d
	addl	$1, %r12d
	movb	%r13b, 7(%rdi)
	movq	-56(%rbp), %rax
	movb	%r12b, 8(%rax)
.L1625:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1643:
	.cfi_restore_state
	movq	%r14, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %rdi
	jmp	.L1631
	.cfi_endproc
.LFE22034:
	.size	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE, .-_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE
	.section	.text._ZN2v88internal19SmallOrderedHashMap6DeleteEPNS0_7IsolateES1_NS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal19SmallOrderedHashMap6DeleteEPNS0_7IsolateES1_NS0_6ObjectE
	.type	_ZN2v88internal19SmallOrderedHashMap6DeleteEPNS0_7IsolateES1_NS0_6ObjectE, @function
_ZN2v88internal19SmallOrderedHashMap6DeleteEPNS0_7IsolateES1_NS0_6ObjectE:
.LFB19734:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal21SmallOrderedHashTableINS0_19SmallOrderedHashMapEE6DeleteEPNS0_7IsolateES2_NS0_6ObjectE
	.cfi_endproc
.LFE19734:
	.size	_ZN2v88internal19SmallOrderedHashMap6DeleteEPNS0_7IsolateES1_NS0_6ObjectE, .-_ZN2v88internal19SmallOrderedHashMap6DeleteEPNS0_7IsolateES1_NS0_6ObjectE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, @function
_GLOBAL__sub_I__ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE:
.LFB25071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25071:
	.size	_GLOBAL__sub_I__ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE, .-_GLOBAL__sub_I__ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal14OrderedHashSet3AddEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	1127219200
	.align 8
.LC3:
	.long	0
	.long	-1042284544
	.align 8
.LC4:
	.long	4290772992
	.long	1105199103
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
