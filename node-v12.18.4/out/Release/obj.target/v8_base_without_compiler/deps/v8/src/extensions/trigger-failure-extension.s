	.file	"trigger-failure-extension.cc"
	.text
	.section	.text._ZN2v89ExtensionD2Ev,"axG",@progbits,_ZN2v89ExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v89ExtensionD2Ev
	.type	_ZN2v89ExtensionD2Ev, @function
_ZN2v89ExtensionD2Ev:
.LFB2485:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1:
	ret
	.cfi_endproc
.LFE2485:
	.size	_ZN2v89ExtensionD2Ev, .-_ZN2v89ExtensionD2Ev
	.weak	_ZN2v89ExtensionD1Ev
	.set	_ZN2v89ExtensionD1Ev,_ZN2v89ExtensionD2Ev
	.section	.text._ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,"axG",@progbits,_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.type	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, @function
_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE:
.LFB2488:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2488:
	.size	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, .-_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.text._ZN2v88internal23TriggerFailureExtension18TriggerAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23TriggerFailureExtension18TriggerAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal23TriggerFailureExtension18TriggerAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal23TriggerFailureExtension18TriggerAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE:
.LFB3940:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3940:
	.size	_ZN2v88internal23TriggerFailureExtension18TriggerAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE, .-_ZN2v88internal23TriggerFailureExtension18TriggerAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.section	.text._ZN2v88internal23TriggerFailureExtensionD2Ev,"axG",@progbits,_ZN2v88internal23TriggerFailureExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23TriggerFailureExtensionD2Ev
	.type	_ZN2v88internal23TriggerFailureExtensionD2Ev, @function
_ZN2v88internal23TriggerFailureExtensionD2Ev:
.LFB4625:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L6:
	ret
	.cfi_endproc
.LFE4625:
	.size	_ZN2v88internal23TriggerFailureExtensionD2Ev, .-_ZN2v88internal23TriggerFailureExtensionD2Ev
	.weak	_ZN2v88internal23TriggerFailureExtensionD1Ev
	.set	_ZN2v88internal23TriggerFailureExtensionD1Ev,_ZN2v88internal23TriggerFailureExtensionD2Ev
	.section	.rodata._ZN2v88internal23TriggerFailureExtension17TriggerCheckFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"false"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal23TriggerFailureExtension17TriggerCheckFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23TriggerFailureExtension17TriggerCheckFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal23TriggerFailureExtension17TriggerCheckFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal23TriggerFailureExtension17TriggerCheckFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE:
.LFB3939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3939:
	.size	_ZN2v88internal23TriggerFailureExtension17TriggerCheckFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE, .-_ZN2v88internal23TriggerFailureExtension17TriggerCheckFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.section	.rodata._ZN2v88internal23TriggerFailureExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"triggerCheckFalse"
.LC3:
	.string	"triggerAssertFalse"
.LC4:
	.string	"triggerSlowAssertFalse"
	.section	.rodata._ZN2v88internal23TriggerFailureExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"0 == strcmp(*v8::String::Utf8Value(isolate, str), \"triggerSlowAssertFalse\")"
	.section	.text._ZN2v88internal23TriggerFailureExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23TriggerFailureExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.type	_ZN2v88internal23TriggerFailureExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, @function
_ZN2v88internal23TriggerFailureExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE:
.LFB3938:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r14
	movq	%rdx, %r13
	pushq	%r12
	movq	%r14, %rdi
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-64(%rbp), %rsi
	movl	$18, %ecx
	leaq	.LC2(%rip), %rdi
	repz cmpsb
	movq	%r14, %rdi
	seta	%bl
	sbbb	$0, %bl
	movsbl	%bl, %ebx
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	testl	%ebx, %ebx
	jne	.L11
	subq	$8, %rsp
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	xorl	%edx, %edx
	leaq	_ZN2v88internal23TriggerFailureExtension17TriggerCheckFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	popq	%r8
	popq	%r9
.L12:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L17
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r13, %rdx
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-64(%rbp), %rsi
	movl	$19, %ecx
	leaq	.LC3(%rip), %rdi
	repz cmpsb
	movq	%r14, %rdi
	seta	%bl
	sbbb	$0, %bl
	movsbl	%bl, %ebx
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	testl	%ebx, %ebx
	je	.L18
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r13, %rdx
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-64(%rbp), %rsi
	movl	$23, %ecx
	leaq	.LC4(%rip), %rdi
	repz cmpsb
	movq	%r14, %rdi
	seta	%bl
	sbbb	$0, %bl
	movsbl	%bl, %ebx
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	testl	%ebx, %ebx
	jne	.L19
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r9d
	pushq	$0
	xorl	%r8d, %r8d
	leaq	_ZN2v88internal23TriggerFailureExtension22TriggerSlowAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	popq	%rdx
	popq	%rcx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L18:
	subq	$8, %rsp
	leaq	_ZN2v88internal23TriggerFailureExtension18TriggerAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE(%rip), %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	pushq	$0
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	popq	%rsi
	popq	%rdi
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE3938:
	.size	_ZN2v88internal23TriggerFailureExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE, .-_ZN2v88internal23TriggerFailureExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.section	.text._ZN2v88internal23TriggerFailureExtensionD0Ev,"axG",@progbits,_ZN2v88internal23TriggerFailureExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal23TriggerFailureExtensionD0Ev
	.type	_ZN2v88internal23TriggerFailureExtensionD0Ev, @function
_ZN2v88internal23TriggerFailureExtensionD0Ev:
.LFB4627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L21
	movq	(%rdi), %rax
	call	*8(%rax)
.L21:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4627:
	.size	_ZN2v88internal23TriggerFailureExtensionD0Ev, .-_ZN2v88internal23TriggerFailureExtensionD0Ev
	.section	.text._ZN2v89ExtensionD0Ev,"axG",@progbits,_ZN2v89ExtensionD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v89ExtensionD0Ev
	.type	_ZN2v89ExtensionD0Ev, @function
_ZN2v89ExtensionD0Ev:
.LFB2487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v89ExtensionE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L27
	movq	(%rdi), %rax
	call	*8(%rax)
.L27:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE2487:
	.size	_ZN2v89ExtensionD0Ev, .-_ZN2v89ExtensionD0Ev
	.section	.text._ZN2v88internal23TriggerFailureExtension22TriggerSlowAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23TriggerFailureExtension22TriggerSlowAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.type	_ZN2v88internal23TriggerFailureExtension22TriggerSlowAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE, @function
_ZN2v88internal23TriggerFailureExtension22TriggerSlowAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE:
.LFB4638:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4638:
	.size	_ZN2v88internal23TriggerFailureExtension22TriggerSlowAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE, .-_ZN2v88internal23TriggerFailureExtension22TriggerSlowAssertFalseERKNS_20FunctionCallbackInfoINS_5ValueEEE
	.weak	_ZTVN2v89ExtensionE
	.section	.data.rel.ro.local._ZTVN2v89ExtensionE,"awG",@progbits,_ZTVN2v89ExtensionE,comdat
	.align 8
	.type	_ZTVN2v89ExtensionE, @object
	.size	_ZTVN2v89ExtensionE, 40
_ZTVN2v89ExtensionE:
	.quad	0
	.quad	0
	.quad	_ZN2v89ExtensionD1Ev
	.quad	_ZN2v89ExtensionD0Ev
	.quad	_ZN2v89Extension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.weak	_ZTVN2v88internal23TriggerFailureExtensionE
	.section	.data.rel.ro.local._ZTVN2v88internal23TriggerFailureExtensionE,"awG",@progbits,_ZTVN2v88internal23TriggerFailureExtensionE,comdat
	.align 8
	.type	_ZTVN2v88internal23TriggerFailureExtensionE, @object
	.size	_ZTVN2v88internal23TriggerFailureExtensionE, 40
_ZTVN2v88internal23TriggerFailureExtensionE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal23TriggerFailureExtensionD1Ev
	.quad	_ZN2v88internal23TriggerFailureExtensionD0Ev
	.quad	_ZN2v88internal23TriggerFailureExtension25GetNativeFunctionTemplateEPNS_7IsolateENS_5LocalINS_6StringEEE
	.globl	_ZN2v88internal23TriggerFailureExtension7kSourceE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"native function triggerCheckFalse();native function triggerAssertFalse();native function triggerSlowAssertFalse();"
	.section	.data.rel.ro.local._ZN2v88internal23TriggerFailureExtension7kSourceE,"aw"
	.align 8
	.type	_ZN2v88internal23TriggerFailureExtension7kSourceE, @object
	.size	_ZN2v88internal23TriggerFailureExtension7kSourceE, 8
_ZN2v88internal23TriggerFailureExtension7kSourceE:
	.quad	.LC6
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
