	.file	"asm-parser.cc"
	.text
	.section	.text._ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv,"axG",@progbits,_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv
	.type	_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv, @function
_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv:
.LFB5530:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE5530:
	.size	_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv, .-_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE.part.0.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Unexpected token"
.LC1:
	.string	"Expected ArrayBuffer view"
	.section	.text._ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE.part.0, @function
_ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE.part.0:
.LFB23214:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$46, 16(%rbx)
	je	.L4
.L21:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L3:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r14d
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	leal	9980(%r14), %eax
	cmpl	$7, %eax
	ja	.L6
	leaq	.L8(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE.part.0,"a",@progbits
	.align 4
	.align 4
.L8:
	.long	.L15-.L8
	.long	.L14-.L8
	.long	.L13-.L8
	.long	.L12-.L8
	.long	.L11-.L8
	.long	.L10-.L8
	.long	.L9-.L8
	.long	.L7-.L8
	.section	.text._ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE.part.0
	.p2align 4,,10
	.p2align 3
.L9:
	movabsq	$12884901888, %rax
	movq	$4194307, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	btsq	$35, 328(%rbx)
	.p2align 4,,10
	.p2align 3
.L16:
	cmpl	$40, 16(%rbx)
	jne	.L21
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	564(%rbx), %eax
	cmpl	%eax, 16(%rbx)
	jne	.L21
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$41, 16(%rbx)
	jne	.L21
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movabsq	$12884901888, %rax
	movq	$8388611, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	btsq	$36, 328(%rbx)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L15:
	movabsq	$12884901888, %rax
	movq	$131075, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	orq	$536870912, 328(%rbx)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L14:
	movabsq	$12884901888, %rax
	movq	$65539, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	orq	$1073741824, 328(%rbx)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L13:
	movabsq	$12884901888, %rax
	movq	$524291, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	btsq	$31, 328(%rbx)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L12:
	movabsq	$12884901888, %rax
	movq	$262147, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	btsq	$32, 328(%rbx)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L11:
	movabsq	$12884901888, %rax
	movq	$2097155, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	btsq	$33, 328(%rbx)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L10:
	movabsq	$12884901888, %rax
	movq	$1048579, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	btsq	$34, 328(%rbx)
	jmp	.L16
.L6:
	leaq	.LC1(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L3
	.cfi_endproc
.LFE23214:
	.size	_ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE.part.0, .-_ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE.part.0
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv.part.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Expected ;"
.LC3:
	.string	"Illegal continue"
	.section	.text._ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv.part.0, @function
_ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv.part.0:
.LFB23221:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r13d
	leal	9999(%r13), %eax
	cmpl	$10254, %eax
	jbe	.L39
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	600(%rbx), %rax
	movq	592(%rbx), %rcx
	cmpq	%rcx, %rax
	je	.L25
	xorl	%edx, %edx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L26:
	subq	$8, %rax
	addl	$1, %edx
	cmpq	%rax, %rcx
	je	.L25
.L28:
	cmpl	$1, -8(%rax)
	jne	.L26
	cmpl	-4(%rax), %r13d
	jne	.L26
.L27:
	movq	304(%rbx), %rdi
	movl	$12, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithI32VENS1_10WasmOpcodeEi@PLT
	movl	16(%rbx), %eax
	cmpl	$59, %eax
	je	.L40
	cmpl	$125, %eax
	je	.L22
	cmpb	$0, 292(%rbx)
	je	.L41
.L22:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	leaq	.LC3(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	600(%rbx), %rax
	movq	592(%rbx), %rcx
	xorl	%edx, %edx
	cmpq	%rax, %rcx
	jne	.L24
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L42:
	subq	$8, %rax
	addl	$1, %edx
	cmpq	%rcx, %rax
	je	.L25
.L24:
	cmpl	$1, -8(%rax)
	jne	.L42
	jmp	.L27
.L41:
	leaq	.LC2(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L40:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	.cfi_endproc
.LFE23221:
	.size	_ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv.part.0, .-_ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv.part.0
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser14BreakStatementEv.part.0.str1.1,"aMS",@progbits,1
.LC4:
	.string	"Illegal break"
	.section	.text._ZN2v88internal4wasm11AsmJsParser14BreakStatementEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm11AsmJsParser14BreakStatementEv.part.0, @function
_ZN2v88internal4wasm11AsmJsParser14BreakStatementEv.part.0:
.LFB23222:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	8(%rdi), %r13
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r14), %ebx
	leal	9999(%rbx), %eax
	cmpl	$10254, %eax
	jbe	.L66
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	600(%r14), %rax
	movq	592(%r14), %rcx
	cmpq	%rcx, %rax
	je	.L46
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L51:
	movl	-8(%rax), %edx
	testl	%edx, %edx
	je	.L47
	cmpl	$2, %edx
	jne	.L48
.L47:
	cmpl	-4(%rax), %ebx
	je	.L50
.L48:
	subq	$8, %rax
	addl	$1, %r12d
	cmpq	%rax, %rcx
	jne	.L51
.L46:
	leaq	.LC4(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
.L43:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movq	600(%r14), %rax
	movq	592(%r14), %rcx
	xorl	%r12d, %r12d
	cmpq	%rax, %rcx
	je	.L46
.L45:
	movl	-8(%rax), %edx
	testl	%edx, %edx
	je	.L50
	cmpl	$2, %edx
	jne	.L52
	movl	-4(%rax), %edx
	testl	%edx, %edx
	jne	.L52
	.p2align 4,,10
	.p2align 3
.L50:
	movq	304(%r14), %rdi
	movl	$12, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	304(%r14), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder8EmitI32VEi@PLT
	movl	16(%r14), %eax
	cmpl	$59, %eax
	je	.L67
	cmpl	$125, %eax
	je	.L43
	cmpb	$0, 292(%r14)
	jne	.L43
	leaq	.LC2(%rip), %rax
	popq	%rbx
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	popq	%r12
	popq	%r13
	movl	%eax, 552(%r14)
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	subq	$8, %rax
	addl	$1, %r12d
	cmpq	%rcx, %rax
	jne	.L45
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L67:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	.cfi_endproc
.LFE23222:
	.size	_ZN2v88internal4wasm11AsmJsParser14BreakStatementEv.part.0, .-_ZN2v88internal4wasm11AsmJsParser14BreakStatementEv.part.0
	.section	.text._ZN2v88internal4Zone3NewEm,"axG",@progbits,_ZN2v88internal4Zone3NewEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4Zone3NewEm
	.type	_ZN2v88internal4Zone3NewEm, @function
_ZN2v88internal4Zone3NewEm:
.LFB5439:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	movq	24(%rdi), %rax
	addq	$7, %rsi
	andq	$-8, %rsi
	subq	%r8, %rax
	cmpq	%rsi, %rax
	jb	.L70
	addq	%r8, %rsi
	movq	%r8, %rax
	movq	%rsi, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	jmp	_ZN2v88internal4Zone9NewExpandEm@PLT
	.cfi_endproc
.LFE5439:
	.size	_ZN2v88internal4Zone3NewEm, .-_ZN2v88internal4Zone3NewEm
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser16ConvertSignatureEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"unreachable code"
	.section	.text._ZN2v88internal4wasm11AsmJsParser16ConvertSignatureEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser16ConvertSignatureEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
	.type	_ZN2v88internal4wasm11AsmJsParser16ConvertSignatureEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE, @function
_ZN2v88internal4wasm11AsmJsParser16ConvertSignatureEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE:
.LFB18183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	movq	%rsi, -72(%rbp)
	sarq	$3, %rax
	movl	$17, %esi
	movq	%rax, %r14
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	movq	(%r12), %rcx
	xorl	$1, %eax
	movzbl	%al, %esi
	movq	16(%rcx), %r12
	movzbl	%al, %r13d
	movq	24(%rcx), %rax
	addl	%r14d, %esi
	movq	%rcx, -64(%rbp)
	movslq	%esi, %rsi
	movq	%rax, -56(%rbp)
	subq	%r12, %rax
	addq	$7, %rsi
	andq	$-8, %rsi
	cmpq	%rax, %rsi
	ja	.L93
	addq	%r12, %rsi
	movq	%rsi, 16(%rcx)
.L73:
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rdx
	je	.L74
	movq	%rdx, %r15
	xorl	%ebx, %ebx
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	(%rbx,%r12), %rax
	movb	$4, (%rax,%r13)
.L76:
	addq	$1, %rbx
	addq	$8, %r15
	cmpq	%r15, -56(%rbp)
	je	.L74
.L79:
	movq	(%r15), %r14
	movl	$237, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L94
	movl	$57357, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L77
	leaq	(%rbx,%r12), %rax
	addq	$8, %r15
	addq	$1, %rbx
	movb	$3, (%rax,%r13)
	cmpq	%r15, -56(%rbp)
	jne	.L79
.L74:
	movq	-72(%rbp), %rbx
	movl	$17, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L95
.L80:
	movq	-64(%rbp), %rdx
	movq	24(%rdx), %rcx
	movq	16(%rdx), %rax
	movq	%rcx, %rdx
	movq	%rcx, -56(%rbp)
	subq	%rax, %rdx
	cmpq	$23, %rdx
	jbe	.L96
	movq	-64(%rbp), %rcx
	leaq	24(%rax), %rdx
	movq	%rdx, 16(%rcx)
.L84:
	movq	%r13, %xmm0
	movq	%r12, 16(%rax)
	movhps	-80(%rbp), %xmm0
	movups	%xmm0, (%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	movl	$769, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L78
	leaq	(%rbx,%r12), %rax
	movb	$1, (%rax,%r13)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$237, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L81
	movb	$4, (%r12)
	jmp	.L80
.L81:
	movq	-72(%rbp), %rdi
	movl	$57357, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L82
	movb	$3, (%r12)
	jmp	.L80
.L96:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L84
.L93:
	movq	%rcx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L73
.L82:
	movq	-72(%rbp), %rdi
	movl	$1825, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L78
	movb	$1, (%r12)
	jmp	.L80
.L78:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18183:
	.size	_ZN2v88internal4wasm11AsmJsParser16ConvertSignatureEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE, .-_ZN2v88internal4wasm11AsmJsParser16ConvertSignatureEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
	.section	.text._ZN2v88internal4wasm11AsmJsParser8VarIndexEPNS2_7VarInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser8VarIndexEPNS2_7VarInfoE
	.type	_ZN2v88internal4wasm11AsmJsParser8VarIndexEPNS2_7VarInfoE, @function
_ZN2v88internal4wasm11AsmJsParser8VarIndexEPNS2_7VarInfoE:
.LFB18193:
	.cfi_startproc
	endbr64
	movl	28(%rsi), %eax
	addl	752(%rdi), %eax
	ret
	.cfi_endproc
.LFE18193:
	.size	_ZN2v88internal4wasm11AsmJsParser8VarIndexEPNS2_7VarInfoE, .-_ZN2v88internal4wasm11AsmJsParser8VarIndexEPNS2_7VarInfoE
	.section	.text._ZN2v88internal4wasm11AsmJsParser15AddGlobalImportENS0_6VectorIKcEEPNS1_7AsmTypeENS1_9ValueTypeEbPNS2_7VarInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser15AddGlobalImportENS0_6VectorIKcEEPNS1_7AsmTypeENS1_9ValueTypeEbPNS2_7VarInfoE
	.type	_ZN2v88internal4wasm11AsmJsParser15AddGlobalImportENS0_6VectorIKcEEPNS1_7AsmTypeENS1_9ValueTypeEbPNS2_7VarInfoE, @function
_ZN2v88internal4wasm11AsmJsParser15AddGlobalImportENS0_6VectorIKcEEPNS1_7AsmTypeENS1_9ValueTypeEbPNS2_7VarInfoE:
.LFB18194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movzbl	%r8b, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movl	$1, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	%r9d, -88(%rbp)
	movq	16(%rbp), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rcx, (%r14)
	leaq	-80(%rbp), %rcx
	movl	$2, 32(%r14)
	movq	296(%rdi), %rdi
	movl	$0, -80(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movl	-88(%rbp), %r9d
	movl	%eax, 28(%r14)
	movb	%r9b, 36(%r14)
	movq	728(%rbx), %r8
	leaq	736(%rbx), %r9
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	$47, %rax
	jbe	.L103
	leaq	48(%rdi), %rax
	movq	%rax, 16(%r8)
.L100:
	movq	%r13, 16(%rdi)
	movq	%r9, %rsi
	movq	%r12, 24(%rdi)
	movb	%r15b, 32(%rdi)
	movq	%r14, 40(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 752(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	%r8, %rdi
	movl	$48, %esi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r9
	movq	%rax, %rdi
	jmp	.L100
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18194:
	.size	_ZN2v88internal4wasm11AsmJsParser15AddGlobalImportENS0_6VectorIKcEEPNS1_7AsmTypeENS1_9ValueTypeEbPNS2_7VarInfoE, .-_ZN2v88internal4wasm11AsmJsParser15AddGlobalImportENS0_6VectorIKcEEPNS1_7AsmTypeENS1_9ValueTypeEbPNS2_7VarInfoE
	.section	.text._ZN2v88internal4wasm11AsmJsParser13DeclareGlobalEPNS2_7VarInfoEbPNS1_7AsmTypeENS1_9ValueTypeERKNS1_12WasmInitExprE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser13DeclareGlobalEPNS2_7VarInfoEbPNS1_7AsmTypeENS1_9ValueTypeERKNS1_12WasmInitExprE
	.type	_ZN2v88internal4wasm11AsmJsParser13DeclareGlobalEPNS2_7VarInfoEbPNS1_7AsmTypeENS1_9ValueTypeERKNS1_12WasmInitExprE, @function
_ZN2v88internal4wasm11AsmJsParser13DeclareGlobalEPNS2_7VarInfoEbPNS1_7AsmTypeENS1_9ValueTypeERKNS1_12WasmInitExprE:
.LFB18195:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r10
	movq	%r9, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%edx, %r12d
	movl	$1, %edx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movl	$2, 32(%rsi)
	movq	%r10, (%rsi)
	movq	296(%rdi), %rdi
	movzbl	%r8b, %esi
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	%r12b, 36(%rbx)
	movl	%eax, 28(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18195:
	.size	_ZN2v88internal4wasm11AsmJsParser13DeclareGlobalEPNS2_7VarInfoEbPNS1_7AsmTypeENS1_9ValueTypeERKNS1_12WasmInitExprE, .-_ZN2v88internal4wasm11AsmJsParser13DeclareGlobalEPNS2_7VarInfoEbPNS1_7AsmTypeENS1_9ValueTypeERKNS1_12WasmInitExprE
	.section	.text._ZN2v88internal4wasm11AsmJsParser17DeclareStdlibFuncEPNS2_7VarInfoENS2_7VarKindEPNS1_7AsmTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser17DeclareStdlibFuncEPNS2_7VarInfoENS2_7VarKindEPNS1_7AsmTypeE
	.type	_ZN2v88internal4wasm11AsmJsParser17DeclareStdlibFuncEPNS2_7VarInfoENS2_7VarKindEPNS1_7AsmTypeE, @function
_ZN2v88internal4wasm11AsmJsParser17DeclareStdlibFuncEPNS2_7VarInfoENS2_7VarKindEPNS1_7AsmTypeE:
.LFB18196:
	.cfi_startproc
	endbr64
	movl	%edx, 32(%rsi)
	movq	%rcx, (%rsi)
	movl	$0, 28(%rsi)
	movb	$0, 36(%rsi)
	ret
	.cfi_endproc
.LFE18196:
	.size	_ZN2v88internal4wasm11AsmJsParser17DeclareStdlibFuncEPNS2_7VarInfoENS2_7VarKindEPNS1_7AsmTypeE, .-_ZN2v88internal4wasm11AsmJsParser17DeclareStdlibFuncEPNS2_7VarInfoENS2_7VarKindEPNS1_7AsmTypeE
	.section	.text._ZN2v88internal4wasm11AsmJsParser12TempVariableEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser12TempVariableEi
	.type	_ZN2v88internal4wasm11AsmJsParser12TempVariableEi, @function
_ZN2v88internal4wasm11AsmJsParser12TempVariableEi:
.LFB18197:
	.cfi_startproc
	endbr64
	cmpl	%esi, 532(%rdi)
	jg	.L109
	leal	1(%rsi), %eax
	movl	%eax, 532(%rdi)
.L109:
	movl	528(%rdi), %eax
	addl	%esi, %eax
	ret
	.cfi_endproc
.LFE18197:
	.size	_ZN2v88internal4wasm11AsmJsParser12TempVariableEi, .-_ZN2v88internal4wasm11AsmJsParser12TempVariableEi
	.section	.text._ZN2v88internal4wasm11AsmJsParser27CopyCurrentIdentifierStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser27CopyCurrentIdentifierStringEv
	.type	_ZN2v88internal4wasm11AsmJsParser27CopyCurrentIdentifierStringEv, @function
_ZN2v88internal4wasm11AsmJsParser27CopyCurrentIdentifierStringEv:
.LFB18198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	64(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	72(%rbx), %rax
	movq	16(%rdi), %r12
	leaq	7(%rax), %rsi
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%r12, %rax
	cmpq	%rax, %rsi
	ja	.L114
	addq	%r12, %rsi
	movq	%rsi, 16(%rdi)
.L112:
	movq	72(%rbx), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4copyEPcmm@PLT
	movslq	72(%rbx), %rdx
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L112
	.cfi_endproc
.LFE18198:
	.size	_ZN2v88internal4wasm11AsmJsParser27CopyCurrentIdentifierStringEv, .-_ZN2v88internal4wasm11AsmJsParser27CopyCurrentIdentifierStringEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser13SkipSemicolonEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser13SkipSemicolonEv
	.type	_ZN2v88internal4wasm11AsmJsParser13SkipSemicolonEv, @function
_ZN2v88internal4wasm11AsmJsParser13SkipSemicolonEv:
.LFB18199:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	cmpl	$59, %eax
	je	.L116
	cmpl	$125, %eax
	je	.L119
	cmpb	$0, 292(%rdi)
	je	.L120
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	addq	$8, %rdi
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	.p2align 4,,10
	.p2align 3
.L120:
	leaq	.LC2(%rip), %rax
	movb	$1, 540(%rdi)
	movq	%rax, 544(%rdi)
	movq	32(%rdi), %rax
	movl	%eax, 552(%rdi)
	ret
	.cfi_endproc
.LFE18199:
	.size	_ZN2v88internal4wasm11AsmJsParser13SkipSemicolonEv, .-_ZN2v88internal4wasm11AsmJsParser13SkipSemicolonEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser3EndEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser3EndEv
	.type	_ZN2v88internal4wasm11AsmJsParser3EndEv, @function
_ZN2v88internal4wasm11AsmJsParser3EndEv:
.LFB18202:
	.cfi_startproc
	endbr64
	subq	$8, 600(%rdi)
	movq	304(%rdi), %rdi
	movl	$11, %esi
	jmp	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	.cfi_endproc
.LFE18202:
	.size	_ZN2v88internal4wasm11AsmJsParser3EndEv, .-_ZN2v88internal4wasm11AsmJsParser3EndEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser7BareEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser7BareEndEv
	.type	_ZN2v88internal4wasm11AsmJsParser7BareEndEv, @function
_ZN2v88internal4wasm11AsmJsParser7BareEndEv:
.LFB18204:
	.cfi_startproc
	endbr64
	subq	$8, 600(%rdi)
	ret
	.cfi_endproc
.LFE18204:
	.size	_ZN2v88internal4wasm11AsmJsParser7BareEndEv, .-_ZN2v88internal4wasm11AsmJsParser7BareEndEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser22FindContinueLabelDepthEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser22FindContinueLabelDepthEi
	.type	_ZN2v88internal4wasm11AsmJsParser22FindContinueLabelDepthEi, @function
_ZN2v88internal4wasm11AsmJsParser22FindContinueLabelDepthEi:
.LFB18205:
	.cfi_startproc
	endbr64
	movq	600(%rdi), %rax
	movq	592(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L128
	xorl	%r8d, %r8d
	testl	%esi, %esi
	jne	.L127
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L126:
	subq	$8, %rax
	addl	$1, %r8d
	cmpq	%rdx, %rax
	je	.L128
.L127:
	cmpl	$1, -8(%rax)
	jne	.L126
	cmpl	%esi, -4(%rax)
	jne	.L126
.L123:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	subq	$8, %rax
	addl	$1, %r8d
	cmpq	%rdx, %rax
	je	.L128
.L125:
	cmpl	$1, -8(%rax)
	jne	.L132
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$-1, %r8d
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE18205:
	.size	_ZN2v88internal4wasm11AsmJsParser22FindContinueLabelDepthEi, .-_ZN2v88internal4wasm11AsmJsParser22FindContinueLabelDepthEi
	.section	.text._ZN2v88internal4wasm11AsmJsParser19FindBreakLabelDepthEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser19FindBreakLabelDepthEi
	.type	_ZN2v88internal4wasm11AsmJsParser19FindBreakLabelDepthEi, @function
_ZN2v88internal4wasm11AsmJsParser19FindBreakLabelDepthEi:
.LFB18206:
	.cfi_startproc
	endbr64
	movq	600(%rdi), %rax
	movq	592(%rdi), %rcx
	cmpq	%rcx, %rax
	je	.L141
	xorl	%r8d, %r8d
	testl	%esi, %esi
	je	.L135
	.p2align 4,,10
	.p2align 3
.L139:
	movl	-8(%rax), %edx
	testl	%edx, %edx
	je	.L136
	cmpl	$2, %edx
	jne	.L137
.L136:
	cmpl	-4(%rax), %esi
	je	.L133
.L137:
	subq	$8, %rax
	addl	$1, %r8d
	cmpq	%rax, %rcx
	jne	.L139
.L141:
	movl	$-1, %r8d
.L133:
	movl	%r8d, %eax
	ret
.L148:
	movl	-4(%rax), %edx
	testl	%edx, %edx
	je	.L133
.L140:
	subq	$8, %rax
	addl	$1, %r8d
	cmpq	%rax, %rcx
	je	.L141
.L135:
	movl	-8(%rax), %edx
	testl	%edx, %edx
	je	.L133
	cmpl	$2, %edx
	je	.L148
	jmp	.L140
	.cfi_endproc
.LFE18206:
	.size	_ZN2v88internal4wasm11AsmJsParser19FindBreakLabelDepthEi, .-_ZN2v88internal4wasm11AsmJsParser19FindBreakLabelDepthEi
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser24ValidateModuleParametersEv.str1.1,"aMS",@progbits,1
.LC6:
	.string	"Expected stdlib parameter"
.LC7:
	.string	"Expected foreign parameter"
.LC8:
	.string	"Expected heap parameter"
	.section	.text._ZN2v88internal4wasm11AsmJsParser24ValidateModuleParametersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser24ValidateModuleParametersEv
	.type	_ZN2v88internal4wasm11AsmJsParser24ValidateModuleParametersEv, @function
_ZN2v88internal4wasm11AsmJsParser24ValidateModuleParametersEv:
.LFB18208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$40, 16(%rdi)
	je	.L150
.L169:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L149:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	leaq	8(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r12d
	movl	$0, 556(%rbx)
	movq	$0, 560(%rbx)
	cmpl	$41, %r12d
	je	.L158
	cmpl	$255, %r12d
	jg	.L153
	leaq	.LC6(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %eax
	movl	%r12d, 556(%rbx)
	cmpl	$41, %eax
	je	.L158
	cmpl	$44, %eax
	jne	.L169
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r12d
	cmpl	$255, %r12d
	jg	.L155
	leaq	.LC7(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L149
.L157:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$41, 16(%rbx)
	movl	%r12d, 564(%rbx)
	jne	.L169
.L158:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %eax
	movl	%r12d, 560(%rbx)
	cmpl	$41, %eax
	je	.L158
	cmpl	$44, %eax
	jne	.L169
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r12d
	cmpl	$255, %r12d
	jg	.L157
	leaq	.LC8(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L149
	.cfi_endproc
.LFE18208:
	.size	_ZN2v88internal4wasm11AsmJsParser24ValidateModuleParametersEv, .-_ZN2v88internal4wasm11AsmJsParser24ValidateModuleParametersEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarImportEPNS2_7VarInfoEb.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"Expected |0 type annotation for foreign integer import"
	.section	.text._ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarImportEPNS2_7VarInfoEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarImportEPNS2_7VarInfoEb
	.type	_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarImportEPNS2_7VarInfoEb, @function
_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarImportEPNS2_7VarInfoEb:
.LFB18212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	16(%rdi), %eax
	cmpl	$43, %eax
	je	.L171
	cmpl	%eax, 560(%rdi)
	je	.L203
.L202:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L170:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	leaq	8(%rdi), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$46, 16(%rbx)
	jne	.L202
	movq	%r15, %rdi
	leaq	64(%rbx), %r14
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	(%rbx), %rdi
	movq	72(%rbx), %rax
	movq	16(%rdi), %r8
	leaq	7(%rax), %rsi
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rsi
	ja	.L205
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L184:
	movq	72(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -88(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4copyEPcmm@PLT
	movq	%r15, %rdi
	movslq	72(%rbx), %r14
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$124, 16(%rbx)
	movq	-88(%rbp), %r8
	je	.L206
	movl	$6, 32(%r12)
	movq	(%rbx), %r13
	movq	16(%r13), %rbx
	movq	24(%r13), %rax
	subq	%rbx, %rax
	cmpq	$79, %rax
	jbe	.L207
	leaq	80(%rbx), %rax
	movq	%rax, 16(%r13)
.L192:
	movq	%r14, 8(%rbx)
	leaq	72(%rbx), %r14
	leaq	56(%rbx), %rdi
	movl	$100, %esi
	movq	%r13, 16(%rbx)
	movq	%r8, (%rbx)
	movq	%r14, 24(%rbx)
	movq	$1, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movl	$0x3f800000, 56(%rbx)
	movq	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy11_M_next_bktEm@PLT
	movq	%rax, %r13
	cmpq	32(%rbx), %rax
	jbe	.L193
	cmpq	$1, %rax
	je	.L208
	movq	16(%rbx), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, %rsi
	movq	16(%rdi), %r14
	movq	24(%rdi), %rax
	subq	%r14, %rax
	cmpq	%rax, %rdx
	ja	.L209
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L197:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	memset@PLT
.L195:
	movq	%r14, 24(%rbx)
	movq	%r13, 32(%rbx)
.L193:
	movq	%rbx, 16(%r12)
	movb	$0, 36(%r12)
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	8(%rdi), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %eax
	cmpl	%eax, 560(%rbx)
	jne	.L202
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$46, 16(%rbx)
	jne	.L202
	movq	%r14, %rdi
	leaq	64(%rbx), %r15
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	(%rbx), %rdi
	movq	72(%rbx), %rax
	movq	16(%rdi), %r8
	leaq	7(%rax), %rsi
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%r8, %rax
	cmpq	%rax, %rsi
	ja	.L210
	addq	%r8, %rsi
	movq	%rsi, 16(%rdi)
.L178:
	movq	72(%rbx), %rdx
	movq	%r8, %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r8, -88(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4copyEPcmm@PLT
	movslq	72(%rbx), %r15
	leaq	-80(%rbp), %rcx
	movl	$2, 32(%r12)
	movq	$237, (%r12)
	movq	296(%rbx), %rdi
	movl	$1, %edx
	movl	$4, %esi
	movl	$0, -80(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	%r13b, 36(%r12)
	movq	-88(%rbp), %r8
	leaq	736(%rbx), %r13
	movl	%eax, 28(%r12)
	movq	728(%rbx), %r9
	movq	16(%r9), %rdi
	movq	24(%r9), %rax
	subq	%rdi, %rax
	cmpq	$47, %rax
	jbe	.L211
	leaq	48(%rdi), %rax
	movq	%rax, 16(%r9)
.L180:
	movq	%r8, 16(%rdi)
	movq	%r13, %rsi
	movq	%r15, 24(%rdi)
	movq	%r12, 40(%rdi)
	movb	$4, 32(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 752(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L206:
	movq	%r15, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$-3, 16(%rbx)
	movq	-88(%rbp), %r8
	jne	.L187
	movl	288(%rbx), %eax
	testl	%eax, %eax
	je	.L212
.L187:
	leaq	.LC9(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L170
.L205:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r8
	jmp	.L184
.L207:
	movl	$80, %esi
	movq	%r13, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rbx
	jmp	.L192
.L208:
	movq	$0, 72(%rbx)
	jmp	.L195
.L209:
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L197
.L212:
	movq	%r15, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	leaq	-80(%rbp), %rcx
	movl	$1, %edx
	movl	$2, 32(%r12)
	movq	$769, (%r12)
	movq	296(%rbx), %rdi
	movl	$1, %esi
	movl	$0, -80(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	%r13b, 36(%r12)
	movq	-88(%rbp), %r8
	leaq	736(%rbx), %r13
	movl	%eax, 28(%r12)
	movq	728(%rbx), %r9
	movq	16(%r9), %rdi
	movq	24(%r9), %rax
	subq	%rdi, %rax
	cmpq	$47, %rax
	jbe	.L213
	leaq	48(%rdi), %rax
	movq	%rax, 16(%r9)
.L191:
	movq	%r8, 16(%rdi)
	movq	%r13, %rsi
	movq	%r14, 24(%rdi)
	movb	$1, 32(%rdi)
	movq	%r12, 40(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 752(%rbx)
	jmp	.L170
.L211:
	movq	%r9, %rdi
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rdi
	jmp	.L180
.L210:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r8
	jmp	.L178
.L204:
	call	__stack_chk_fail@PLT
.L213:
	movq	%r9, %rdi
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rdi
	jmp	.L191
	.cfi_endproc
.LFE18212:
	.size	_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarImportEPNS2_7VarInfoEb, .-_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarImportEPNS2_7VarInfoEb
	.section	.text._ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE
	.type	_ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE, @function
_ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE:
.LFB18213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	16(%rdi), %eax
	cmpl	%eax, 556(%rdi)
	je	.L215
.L233:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L214:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	leaq	8(%rdi), %r13
	movq	%rsi, %r12
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$46, 16(%rbx)
	jne	.L233
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r14d
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	leal	9980(%r14), %eax
	cmpl	$7, %eax
	ja	.L218
	leaq	.L220(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE,"a",@progbits
	.align 4
	.align 4
.L220:
	.long	.L227-.L220
	.long	.L226-.L220
	.long	.L225-.L220
	.long	.L224-.L220
	.long	.L223-.L220
	.long	.L222-.L220
	.long	.L221-.L220
	.long	.L219-.L220
	.section	.text._ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE
	.p2align 4,,10
	.p2align 3
.L221:
	movabsq	$12884901888, %rax
	movq	$4194307, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	btsq	$35, 328(%rbx)
	.p2align 4,,10
	.p2align 3
.L228:
	cmpl	$40, 16(%rbx)
	jne	.L233
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	564(%rbx), %eax
	cmpl	%eax, 16(%rbx)
	jne	.L233
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$41, 16(%rbx)
	jne	.L233
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movabsq	$12884901888, %rax
	movq	$1048579, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	btsq	$34, 328(%rbx)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L223:
	movabsq	$12884901888, %rax
	movq	$2097155, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	btsq	$33, 328(%rbx)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L224:
	movabsq	$12884901888, %rax
	movq	$262147, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	btsq	$32, 328(%rbx)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L225:
	movabsq	$12884901888, %rax
	movq	$524291, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	btsq	$31, 328(%rbx)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L226:
	movabsq	$12884901888, %rax
	movq	$65539, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	orq	$1073741824, 328(%rbx)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L227:
	movabsq	$12884901888, %rax
	movq	$131075, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	orq	$536870912, 328(%rbx)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L219:
	movabsq	$12884901888, %rax
	movq	$8388611, (%r12)
	movq	%rax, 28(%r12)
	movb	$0, 36(%r12)
	btsq	$36, 328(%rbx)
	jmp	.L228
.L218:
	leaq	.LC1(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L214
	.cfi_endproc
.LFE18213:
	.size	_ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE, .-_ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarStdlibEPNS2_7VarInfoE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"Invalid member of stdlib.Math"
.LC22:
	.string	"Invalid member of stdlib"
	.section	.text._ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarStdlibEPNS2_7VarInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarStdlibEPNS2_7VarInfoE
	.type	_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarStdlibEPNS2_7VarInfoE, @function
_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarStdlibEPNS2_7VarInfoE:
.LFB18214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	16(%rdi), %eax
	cmpl	$-9962, %eax
	je	.L235
	cmpl	$-9964, %eax
	je	.L273
	cmpl	$-9963, %eax
	je	.L274
	leaq	.LC22(%rip), %rax
	movb	$1, 540(%rdi)
	movq	%rax, 544(%rdi)
	movq	32(%rdi), %rax
	movl	%eax, 552(%rdi)
.L234:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L275
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	leaq	8(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$46, 16(%rbx)
	je	.L238
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L238:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r14d
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	leal	9999(%r14), %eax
	cmpl	$34, %eax
	ja	.L240
	leaq	.L242(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarStdlibEPNS2_7VarInfoE,"a",@progbits
	.align 4
	.align 4
.L242:
	.long	.L268-.L242
	.long	.L267-.L242
	.long	.L266-.L242
	.long	.L265-.L242
	.long	.L264-.L242
	.long	.L263-.L242
	.long	.L262-.L242
	.long	.L261-.L242
	.long	.L260-.L242
	.long	.L259-.L242
	.long	.L258-.L242
	.long	.L257-.L242
	.long	.L256-.L242
	.long	.L255-.L242
	.long	.L254-.L242
	.long	.L253-.L242
	.long	.L252-.L242
	.long	.L251-.L242
	.long	.L250-.L242
	.long	.L240-.L242
	.long	.L240-.L242
	.long	.L240-.L242
	.long	.L240-.L242
	.long	.L240-.L242
	.long	.L240-.L242
	.long	.L240-.L242
	.long	.L240-.L242
	.long	.L249-.L242
	.long	.L248-.L242
	.long	.L247-.L242
	.long	.L246-.L242
	.long	.L245-.L242
	.long	.L244-.L242
	.long	.L243-.L242
	.long	.L241-.L242
	.section	.text._ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarStdlibEPNS2_7VarInfoE
	.p2align 4,,10
	.p2align 3
.L273:
	leaq	8(%rdi), %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	.LC20(%rip), %rax
	leaq	-64(%rbp), %rcx
	movl	$2, 32(%r12)
	movq	$237, (%r12)
	movq	296(%rbx), %rdi
	movl	$1, %edx
	movl	$4, %esi
	movl	$5, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	$0, 36(%r12)
	movl	%eax, 28(%r12)
	orq	$1, 328(%rbx)
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L274:
	leaq	8(%rdi), %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	.LC21(%rip), %rax
	leaq	-64(%rbp), %rcx
	movl	$2, 32(%r12)
	movq	$237, (%r12)
	movq	296(%rbx), %rdi
	movl	$1, %edx
	movl	$4, %esi
	movl	$5, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	$0, 36(%r12)
	movl	%eax, 28(%r12)
	orq	$2, 328(%rbx)
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L240:
	leaq	.LC19(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L234
.L241:
	movq	.LC18(%rip), %rax
	movq	$237, (%r12)
	leaq	-64(%rbp), %rcx
	movl	$1, %edx
	movl	$2, 32(%r12)
	movq	296(%rbx), %rdi
	movl	$4, %esi
	movl	$5, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	$0, 36(%r12)
	movl	%eax, 28(%r12)
	orq	$268435456, 328(%rbx)
	jmp	.L234
.L243:
	movq	.LC17(%rip), %rax
	movq	$237, (%r12)
	leaq	-64(%rbp), %rcx
	movl	$1, %edx
	movl	$2, 32(%r12)
	movq	296(%rbx), %rdi
	movl	$4, %esi
	movl	$5, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	$0, 36(%r12)
	movl	%eax, 28(%r12)
	orq	$134217728, 328(%rbx)
	jmp	.L234
.L244:
	movq	.LC16(%rip), %rax
	movq	$237, (%r12)
	leaq	-64(%rbp), %rcx
	movl	$1, %edx
	movl	$2, 32(%r12)
	movq	296(%rbx), %rdi
	movl	$4, %esi
	movl	$5, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	$0, 36(%r12)
	movl	%eax, 28(%r12)
	orq	$67108864, 328(%rbx)
	jmp	.L234
.L245:
	movq	.LC15(%rip), %rax
	movq	$237, (%r12)
	leaq	-64(%rbp), %rcx
	movl	$1, %edx
	movl	$2, 32(%r12)
	movq	296(%rbx), %rdi
	movl	$4, %esi
	movl	$5, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	$0, 36(%r12)
	movl	%eax, 28(%r12)
	orq	$33554432, 328(%rbx)
	jmp	.L234
.L246:
	movq	.LC14(%rip), %rax
	movq	$237, (%r12)
	leaq	-64(%rbp), %rcx
	movl	$1, %edx
	movl	$2, 32(%r12)
	movq	296(%rbx), %rdi
	movl	$4, %esi
	movl	$5, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	$0, 36(%r12)
	movl	%eax, 28(%r12)
	orq	$16777216, 328(%rbx)
	jmp	.L234
.L247:
	movq	.LC13(%rip), %rax
	movq	$237, (%r12)
	leaq	-64(%rbp), %rcx
	movl	$1, %edx
	movl	$2, 32(%r12)
	movq	296(%rbx), %rdi
	movl	$4, %esi
	movl	$5, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	$0, 36(%r12)
	movl	%eax, 28(%r12)
	orq	$8388608, 328(%rbx)
	jmp	.L234
.L248:
	movq	.LC12(%rip), %rax
	movq	$237, (%r12)
	leaq	-64(%rbp), %rcx
	movl	$1, %edx
	movl	$2, 32(%r12)
	movq	296(%rbx), %rdi
	movl	$4, %esi
	movl	$5, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	$0, 36(%r12)
	movl	%eax, 28(%r12)
	orq	$4194304, 328(%rbx)
	jmp	.L234
.L249:
	movq	.LC11(%rip), %rax
	movq	$237, (%r12)
	leaq	-64(%rbp), %rcx
	movl	$1, %edx
	movl	$2, 32(%r12)
	movq	296(%rbx), %rdi
	movl	$4, %esi
	movl	$5, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	$0, 36(%r12)
	movl	%eax, 28(%r12)
	orq	$2097152, 328(%rbx)
	jmp	.L234
.L250:
	movq	664(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$107374182400, %rax
	movq	%rax, 28(%r12)
	orq	$1048576, 328(%rbx)
	jmp	.L234
.L251:
	movq	664(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$103079215104, %rax
	movq	%rax, 28(%r12)
	orq	$524288, 328(%rbx)
	jmp	.L234
.L252:
	movq	664(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$98784247808, %rax
	movq	%rax, 28(%r12)
	orq	$262144, 328(%rbx)
	jmp	.L234
.L253:
	movq	632(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$94489280512, %rax
	movq	%rax, 28(%r12)
	orq	$131072, 328(%rbx)
	jmp	.L234
.L254:
	movq	640(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$90194313216, %rax
	movq	%rax, 28(%r12)
	orq	$65536, 328(%rbx)
	jmp	.L234
.L255:
	movq	624(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$85899345920, %rax
	movq	%rax, 28(%r12)
	orq	$32768, 328(%rbx)
	jmp	.L234
.L256:
	movq	624(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$81604378624, %rax
	movq	%rax, 28(%r12)
	orq	$16384, 328(%rbx)
	jmp	.L234
.L257:
	movq	616(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$77309411328, %rax
	movq	%rax, 28(%r12)
	orq	$8192, 328(%rbx)
	jmp	.L234
.L258:
	movq	616(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$73014444032, %rax
	movq	%rax, 28(%r12)
	orq	$4096, 328(%rbx)
	jmp	.L234
.L259:
	movq	616(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$68719476736, %rax
	movq	%rax, 28(%r12)
	orq	$2048, 328(%rbx)
	jmp	.L234
.L260:
	movq	616(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$64424509440, %rax
	movq	%rax, 28(%r12)
	orq	$1024, 328(%rbx)
	jmp	.L234
.L261:
	movq	616(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$60129542144, %rax
	movq	%rax, 28(%r12)
	orq	$512, 328(%rbx)
	jmp	.L234
.L262:
	movq	616(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$55834574848, %rax
	movq	%rax, 28(%r12)
	orq	$256, 328(%rbx)
	jmp	.L234
.L263:
	movq	616(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$51539607552, %rax
	movq	%rax, 28(%r12)
	orq	$128, 328(%rbx)
	jmp	.L234
.L264:
	movq	616(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$47244640256, %rax
	movq	%rax, 28(%r12)
	orq	$64, 328(%rbx)
	jmp	.L234
.L265:
	movq	672(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$42949672960, %rax
	movq	%rax, 28(%r12)
	orq	$32, 328(%rbx)
	jmp	.L234
.L266:
	movq	656(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$38654705664, %rax
	movq	%rax, 28(%r12)
	orq	$16, 328(%rbx)
	jmp	.L234
.L267:
	movq	648(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$34359738368, %rax
	movq	%rax, 28(%r12)
	orq	$8, 328(%rbx)
	jmp	.L234
.L268:
	movq	648(%rbx), %rax
	movb	$0, 36(%r12)
	movq	%rax, (%r12)
	movabsq	$30064771072, %rax
	movq	%rax, 28(%r12)
	orq	$4, 328(%rbx)
	jmp	.L234
.L275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18214:
	.size	_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarStdlibEPNS2_7VarInfoE, .-_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarStdlibEPNS2_7VarInfoE
	.section	.text._ZN2v88internal4wasm11AsmJsParser14EmptyStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser14EmptyStatementEv
	.type	_ZN2v88internal4wasm11AsmJsParser14EmptyStatementEv, @function
_ZN2v88internal4wasm11AsmJsParser14EmptyStatementEv:
.LFB18223:
	.cfi_startproc
	endbr64
	cmpl	$59, 16(%rdi)
	je	.L277
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rdi)
	movq	%rax, 544(%rdi)
	movq	32(%rdi), %rax
	movl	%eax, 552(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	addq	$8, %rdi
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	.cfi_endproc
.LFE18223:
	.size	_ZN2v88internal4wasm11AsmJsParser14EmptyStatementEv, .-_ZN2v88internal4wasm11AsmJsParser14EmptyStatementEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser14BreakStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser14BreakStatementEv
	.type	_ZN2v88internal4wasm11AsmJsParser14BreakStatementEv, @function
_ZN2v88internal4wasm11AsmJsParser14BreakStatementEv:
.LFB18230:
	.cfi_startproc
	endbr64
	cmpl	$-9960, 16(%rdi)
	je	.L281
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rdi)
	movq	%rax, 544(%rdi)
	movq	32(%rdi), %rax
	movl	%eax, 552(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	jmp	_ZN2v88internal4wasm11AsmJsParser14BreakStatementEv.part.0
	.cfi_endproc
.LFE18230:
	.size	_ZN2v88internal4wasm11AsmJsParser14BreakStatementEv, .-_ZN2v88internal4wasm11AsmJsParser14BreakStatementEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv
	.type	_ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv, @function
_ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv:
.LFB18231:
	.cfi_startproc
	endbr64
	cmpl	$-9957, 16(%rdi)
	je	.L285
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rdi)
	movq	%rax, 544(%rdi)
	movq	32(%rdi), %rax
	movl	%eax, 552(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	jmp	_ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv.part.0
	.cfi_endproc
.LFE18231:
	.size	_ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv, .-_ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser14NumericLiteralEv.str1.1,"aMS",@progbits,1
.LC23:
	.string	"Expected numeric literal."
	.section	.text._ZN2v88internal4wasm11AsmJsParser14NumericLiteralEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser14NumericLiteralEv
	.type	_ZN2v88internal4wasm11AsmJsParser14NumericLiteralEv, @function
_ZN2v88internal4wasm11AsmJsParser14NumericLiteralEv:
.LFB18238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	16(%rdi), %eax
	movq	$0, 680(%rdi)
	cmpl	$-4, %eax
	je	.L289
	cmpl	$-3, %eax
	je	.L296
	leaq	.LC23(%rip), %rax
	movb	$1, 540(%rdi)
	movq	%rax, 544(%rdi)
	movq	32(%rdi), %rax
	movl	%eax, 552(%rdi)
	xorl	%eax, %eax
.L288:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L289:
	.cfi_restore_state
	movsd	280(%rdi), %xmm0
	leaq	8(%rdi), %rdi
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	304(%rbx), %rdi
	movsd	-24(%rbp), %xmm0
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF64ConstEd@PLT
	addq	$16, %rsp
	movl	$237, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	movl	288(%rdi), %r12d
	leaq	8(%rdi), %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	304(%rbx), %rdi
	movl	%r12d, %esi
	testl	%r12d, %r12d
	js	.L293
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movl	$7969, %eax
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L293:
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movl	$2817, %eax
	jmp	.L288
	.cfi_endproc
.LFE18238:
	.size	_ZN2v88internal4wasm11AsmJsParser14NumericLiteralEv, .-_ZN2v88internal4wasm11AsmJsParser14NumericLiteralEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser24ScanToClosingParenthesisEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser24ScanToClosingParenthesisEv
	.type	_ZN2v88internal4wasm11AsmJsParser24ScanToClosingParenthesisEv, @function
_ZN2v88internal4wasm11AsmJsParser24ScanToClosingParenthesisEv:
.LFB18279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	xorl	%ebx, %ebx
	subq	$8, %rsp
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L298:
	cmpl	$41, %eax
	je	.L310
	cmpl	$-1, %eax
	je	.L297
.L299:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
.L302:
	movl	16(%r12), %eax
	cmpl	$40, %eax
	jne	.L298
	addl	$1, %ebx
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L310:
	subl	$1, %ebx
	cmpl	$-1, %ebx
	jne	.L299
.L297:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18279:
	.size	_ZN2v88internal4wasm11AsmJsParser24ScanToClosingParenthesisEv, .-_ZN2v88internal4wasm11AsmJsParser24ScanToClosingParenthesisEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser11GatherCasesEPNS0_10ZoneVectorIiEE.str1.1,"aMS",@progbits,1
.LC24:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal4wasm11AsmJsParser11GatherCasesEPNS0_10ZoneVectorIiEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser11GatherCasesEPNS0_10ZoneVectorIiEE
	.type	_ZN2v88internal4wasm11AsmJsParser11GatherCasesEPNS0_10ZoneVectorIiEE, @function
_ZN2v88internal4wasm11AsmJsParser11GatherCasesEPNS0_10ZoneVectorIiEE:
.LFB18280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$56, %rsp
	movq	32(%rdi), %r15
	movq	%r15, -56(%rbp)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L312:
	cmpl	$125, %eax
	je	.L371
	cmpl	$1, %ebx
	je	.L324
.L316:
	addl	$2, %eax
	cmpl	$1, %eax
	jbe	.L365
.L313:
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
.L336:
	movl	16(%r12), %eax
	cmpl	$123, %eax
	jne	.L312
	addl	$1, %ebx
	jmp	.L313
.L373:
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$-3, 16(%r12)
	jne	.L365
	movl	288(%r12), %r15d
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r15d, %edx
	negl	%edx
	cmpl	$-2147483648, %r15d
	cmove	%r15d, %edx
	movq	16(%r13), %r15
	cmpq	24(%r13), %r15
	je	.L321
.L375:
	movl	%edx, (%r15)
	addq	$4, 16(%r13)
.L370:
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r12), %eax
	cmpl	$123, %eax
	je	.L372
	cmpl	$125, %eax
	je	.L365
	.p2align 4,,10
	.p2align 3
.L324:
	cmpl	$-9959, %eax
	jne	.L316
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r12), %eax
	cmpl	$45, %eax
	je	.L373
	cmpl	$-3, %eax
	je	.L374
.L365:
	movq	-56(%rbp), %r15
	addq	$56, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	movq	%r15, %rsi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner4SeekEm@PLT
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	subl	$1, %ebx
	testl	%ebx, %ebx
	jg	.L313
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L372:
	movl	$2, %ebx
	jmp	.L313
.L374:
	movl	288(%r12), %r15d
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r15d, %edx
	movq	16(%r13), %r15
	cmpq	24(%r13), %r15
	jne	.L375
.L321:
	movq	8(%r13), %rcx
	movq	%r15, %r8
	subq	%rcx, %r8
	movq	%r8, %rax
	sarq	$2, %rax
	cmpq	$536870911, %rax
	je	.L376
	testq	%rax, %rax
	je	.L340
	leaq	(%rax,%rax), %rsi
	movl	$2147483648, %r10d
	movl	$2147483644, %r9d
	cmpq	%rsi, %rax
	jbe	.L377
.L326:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rsi
	movq	24(%rdi), %rax
	subq	%rsi, %rax
	cmpq	%r10, %rax
	jb	.L378
	leaq	(%rsi,%r10), %rax
	movq	%rax, 16(%rdi)
.L329:
	leaq	(%rsi,%r9), %rdi
	leaq	4(%rsi), %rax
.L327:
	movl	%edx, (%rsi,%r8)
	cmpq	%rcx, %r15
	je	.L330
	leaq	-4(%r15), %r8
	leaq	15(%rsi), %rax
	subq	%rcx, %r8
	subq	%rcx, %rax
	movq	%r8, %rdx
	shrq	$2, %rdx
	cmpq	$30, %rax
	jbe	.L343
	movabsq	$4611686018427387900, %rax
	testq	%rax, %rdx
	je	.L343
	leaq	1(%rdx), %r9
	xorl	%eax, %eax
	movq	%r9, %rdx
	shrq	$2, %rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L332:
	movdqu	(%rcx,%rax), %xmm1
	movups	%xmm1, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L332
	movq	%r9, %rdx
	andq	$-4, %rdx
	leaq	0(,%rdx,4), %rax
	addq	%rax, %rcx
	addq	%rsi, %rax
	cmpq	%r9, %rdx
	je	.L334
	movl	(%rcx), %edx
	movl	%edx, (%rax)
	leaq	4(%rcx), %rdx
	cmpq	%rdx, %r15
	je	.L334
	movl	4(%rcx), %edx
	movl	%edx, 4(%rax)
	leaq	8(%rcx), %rdx
	cmpq	%rdx, %r15
	je	.L334
	movl	8(%rcx), %edx
	movl	%edx, 8(%rax)
.L334:
	leaq	8(%rsi,%r8), %rax
.L330:
	movq	%rsi, %xmm0
	movq	%rax, %xmm2
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r13)
	jmp	.L370
.L377:
	testq	%rsi, %rsi
	jne	.L379
	movl	$4, %eax
	xorl	%edi, %edi
	xorl	%esi, %esi
	jmp	.L327
.L340:
	movl	$8, %r10d
	movl	$4, %r9d
	jmp	.L326
.L343:
	xorl	%eax, %eax
.L331:
	movl	(%rcx,%rax,4), %r9d
	movl	%r9d, (%rsi,%rax,4)
	movq	%rax, %r9
	addq	$1, %rax
	cmpq	%rdx, %r9
	jne	.L331
	jmp	.L334
.L378:
	movq	%r10, %rsi
	movl	%edx, -84(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movl	-84(%rbp), %edx
	movq	%rax, %rsi
	jmp	.L329
.L376:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L379:
	cmpq	$536870911, %rsi
	movl	$536870911, %r9d
	cmova	%r9, %rsi
	leaq	0(,%rsi,4), %r9
	leaq	7(%r9), %rax
	andq	$-8, %rax
	movq	%rax, %r10
	jmp	.L326
	.cfi_endproc
.LFE18280:
	.size	_ZN2v88internal4wasm11AsmJsParser11GatherCasesEPNS0_10ZoneVectorIiEE, .-_ZN2v88internal4wasm11AsmJsParser11GatherCasesEPNS0_10ZoneVectorIiEE
	.section	.text._ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB20438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %rbx
	movq	%r14, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L418
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %r12
	movq	%rdx, %r15
	subq	%rbx, %r8
	testq	%rax, %rax
	je	.L396
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L419
	movl	$2147483640, %esi
	movl	$2147483640, %edx
.L382:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rcx
	subq	%rax, %rcx
	cmpq	%rsi, %rcx
	jb	.L420
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L385:
	leaq	(%rax,%rdx), %rdi
	leaq	8(%rax), %rcx
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L419:
	testq	%rdx, %rdx
	jne	.L421
	movl	$8, %ecx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L383:
	movq	(%r15), %rdx
	movq	%rdx, (%rax,%r8)
	cmpq	%rbx, %r12
	je	.L386
	leaq	-8(%r12), %r8
	leaq	15(%rax), %rdx
	subq	%rbx, %r8
	subq	%rbx, %rdx
	movq	%r8, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L399
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rcx
	je	.L399
	leaq	1(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L388:
	movdqu	(%rbx,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L388
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %rbx
	addq	%rax, %rcx
	cmpq	%rdx, %rsi
	je	.L390
	movq	(%rbx), %rdx
	movq	%rdx, (%rcx)
.L390:
	leaq	16(%rax,%r8), %rcx
.L386:
	cmpq	%r14, %r12
	je	.L391
	subq	%r12, %r14
	leaq	15(%r12), %rdx
	leaq	-8(%r14), %r9
	subq	%rcx, %rdx
	movq	%r9, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L400
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L400
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L393:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L393
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	leaq	(%rcx,%rsi), %r10
	addq	%rsi, %r12
	cmpq	%rdx, %r8
	je	.L395
	movq	(%r12), %rdx
	movq	%rdx, (%r10)
.L395:
	leaq	8(%rcx,%r9), %rcx
.L391:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %edx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L400:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L392:
	movq	(%r12,%rdx,8), %r8
	movq	%r8, (%rcx,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%rsi, %r8
	jne	.L392
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L399:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L387:
	movq	(%rbx,%rdx,8), %rsi
	movq	%rsi, (%rax,%rdx,8)
	movq	%rdx, %rsi
	addq	$1, %rdx
	cmpq	%rsi, %rcx
	jne	.L387
	jmp	.L390
.L420:
	movq	%r8, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %r8
	jmp	.L385
.L418:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L421:
	cmpq	$268435455, %rdx
	movl	$268435455, %eax
	cmova	%rax, %rdx
	salq	$3, %rdx
	movq	%rdx, %rsi
	jmp	.L382
	.cfi_endproc
.LFE20438:
	.size	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text.unlikely._ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv,"ax",@progbits
	.align 2
.LCOLDB25:
	.section	.text._ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv,"ax",@progbits
.LHOTB25:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv
	.type	_ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv, @function
_ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv:
.LFB18182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%r12), %rdx
	movq	16(%r12), %rax
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L480
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%r12)
.L424:
	leaq	16+_ZTVN2v88internal4wasm15AsmFunctionTypeE(%rip), %r14
	movq	$237, 8(%rax)
	movq	%r14, (%rax)
	movq	%r12, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	%rax, 616(%rbx)
	movq	$77, -64(%rbp)
	testb	$1, %al
	jne	.L425
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	je	.L426
	movq	$77, (%rsi)
	addq	$8, 32(%rax)
.L427:
	movq	(%rbx), %r12
	movq	16(%r12), %rax
	movq	24(%r12), %rdx
	subq	%rax, %rdx
	cmpq	$47, %rdx
	jbe	.L481
	leaq	48(%rax), %rdx
	movq	%rdx, 16(%r12)
.L429:
	movq	%r14, (%rax)
	movq	$237, 8(%rax)
	movq	%r12, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	%rax, 624(%rbx)
	movq	$77, -64(%rbp)
	testb	$1, %al
	jne	.L430
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	je	.L431
	movq	$77, (%rsi)
	addq	$8, 32(%rax)
.L432:
	movq	624(%rbx), %rdi
	testb	$1, %dil
	jne	.L433
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	$77, -64(%rbp)
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	je	.L434
	movq	$77, (%rsi)
	addq	$8, 32(%rax)
.L435:
	movq	(%rbx), %r13
	movq	16(%r13), %r12
	movq	24(%r13), %rax
	subq	%r12, %rax
	cmpq	$47, %rax
	jbe	.L482
	leaq	48(%r12), %rax
	movq	%rax, 16(%r13)
.L437:
	movq	%r14, (%r12)
	movq	$8197, 8(%r12)
	movq	%r13, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movq	$0, 40(%r12)
	movq	$24589, -64(%rbp)
	testb	$1, %r12b
	jne	.L438
	leaq	-64(%rbp), %r15
	leaq	16(%r12), %rdi
	xorl	%esi, %esi
	movq	%r15, %rdx
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	(%rbx), %rdx
	movq	16(%rdx), %r13
	movq	24(%rdx), %rax
	subq	%r13, %rax
	cmpq	$47, %rax
	jbe	.L483
	leaq	48(%r13), %rax
	movq	%rax, 16(%rdx)
.L440:
	movq	%r14, 0(%r13)
	movq	$2817, 8(%r13)
	movq	%rdx, 16(%r13)
	movq	$0, 24(%r13)
	movq	$0, 32(%r13)
	movq	$0, 40(%r13)
	movq	$1825, -64(%rbp)
	testb	$1, %r13b
	jne	.L441
	movq	%r15, %rdx
	leaq	16(%r13), %rdi
	xorl	%esi, %esi
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	(%rbx), %rdx
	movq	16(%rdx), %rax
	movq	24(%rdx), %rcx
	subq	%rax, %rcx
	cmpq	$47, %rcx
	jbe	.L484
	leaq	48(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L443:
	movq	%r14, (%rax)
	movq	$1825, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	%rax, 632(%rbx)
	movq	$769, -64(%rbp)
	testb	$1, %al
	jne	.L444
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	je	.L445
	movq	$769, (%rsi)
	addq	$8, 32(%rax)
.L446:
	movq	(%rbx), %rdx
	movq	16(%rdx), %rax
	movq	24(%rdx), %rcx
	subq	%rax, %rcx
	cmpq	$47, %rcx
	jbe	.L485
	leaq	48(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L448:
	movq	%r14, (%rax)
	movq	$1825, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	%rax, 640(%rbx)
	movq	$769, -64(%rbp)
	testb	$1, %al
	jne	.L449
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	je	.L450
	movq	$769, (%rsi)
	addq	$8, 32(%rax)
.L451:
	movq	640(%rbx), %rdi
	testb	$1, %dil
	jne	.L452
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	$769, -64(%rbp)
	movq	32(%rax), %rsi
	cmpq	40(%rax), %rsi
	je	.L453
	movq	$769, (%rsi)
	addq	$8, 32(%rax)
.L454:
	movq	(%rbx), %rdi
	movl	$237, %edx
	movl	$237, %esi
	call	_ZN2v88internal4wasm7AsmType10MinMaxTypeEPNS0_4ZoneEPS2_S5_@PLT
	movq	(%rbx), %rdi
	movl	$57357, %edx
	movl	$57357, %esi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal4wasm7AsmType10MinMaxTypeEPNS0_4ZoneEPS2_S5_@PLT
	movq	(%rbx), %rdi
	movl	$1825, %edx
	movl	$1825, %esi
	movq	%rax, %r15
	call	_ZN2v88internal4wasm7AsmType10MinMaxTypeEPNS0_4ZoneEPS2_S5_@PLT
	movq	(%rbx), %rdx
	movq	%rax, %r8
	movq	24(%rdx), %rcx
	movq	16(%rdx), %rax
	subq	%rax, %rcx
	cmpq	$39, %rcx
	jbe	.L486
	leaq	40(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L456:
	leaq	16+_ZTVN2v88internal4wasm25AsmOverloadedFunctionTypeE(%rip), %r14
	testb	$1, %al
	movq	%rdx, 8(%rax)
	movq	%r8, %rsi
	movq	%r14, (%rax)
	movl	$0, %edi
	movq	$0, 16(%rax)
	cmove	%rax, %rdi
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	%rax, 648(%rbx)
	call	_ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE@PLT
	movq	648(%rbx), %r8
	xorl	%edi, %edi
	testb	$1, %r8b
	jne	.L458
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*24(%rax)
	movq	%rax, %rdi
.L458:
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE@PLT
	movq	648(%rbx), %r8
	xorl	%edi, %edi
	testb	$1, %r8b
	jne	.L459
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*24(%rax)
	movq	%rax, %rdi
.L459:
	movq	-72(%rbp), %rsi
	call	_ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE@PLT
	movq	(%rbx), %r15
	movq	16(%r15), %rax
	movq	24(%r15), %rdx
	subq	%rax, %rdx
	cmpq	$39, %rdx
	jbe	.L487
	leaq	40(%rax), %rdx
	movq	%rdx, 16(%r15)
.L461:
	testb	$1, %al
	movq	%r14, (%rax)
	movl	$0, %edi
	movq	%r13, %rsi
	movq	%r15, 8(%rax)
	cmove	%rax, %rdi
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	%rax, 656(%rbx)
	call	_ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE@PLT
	movq	656(%rbx), %r8
	xorl	%edi, %edi
	testb	$1, %r8b
	jne	.L463
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*24(%rax)
	movq	%rax, %rdi
.L463:
	movq	616(%rbx), %rsi
	call	_ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE@PLT
	movq	656(%rbx), %r8
	xorl	%edi, %edi
	testb	$1, %r8b
	jne	.L464
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*24(%rax)
	movq	%rax, %rdi
.L464:
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE@PLT
	movq	(%rbx), %r13
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	subq	%rax, %rdx
	cmpq	$39, %rdx
	jbe	.L488
	leaq	40(%rax), %rdx
	movq	%rdx, 16(%r13)
.L466:
	movq	%r14, (%rax)
	testb	$1, %al
	movl	$0, %edi
	movq	%r13, 8(%rax)
	cmove	%rax, %rdi
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	616(%rbx), %rsi
	movq	%rax, 664(%rbx)
	call	_ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE@PLT
	movq	664(%rbx), %r8
	xorl	%edi, %edi
	testb	$1, %r8b
	jne	.L468
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*24(%rax)
	movq	%rax, %rdi
.L468:
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm25AsmOverloadedFunctionType11AddOverloadEPNS1_7AsmTypeE@PLT
	movq	(%rbx), %rdi
	call	_ZN2v88internal4wasm7AsmType10FroundTypeEPNS0_4ZoneE@PLT
	movq	%rax, 672(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L489
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore_state
	leaq	16(%rax), %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L426:
	leaq	-64(%rbp), %rdx
	leaq	16(%rax), %rdi
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L431:
	leaq	-64(%rbp), %rdx
	leaq	16(%rax), %rdi
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L434:
	leaq	-64(%rbp), %rdx
	leaq	16(%rax), %rdi
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L445:
	leaq	16(%rax), %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L450:
	leaq	16(%rax), %rdi
	movq	%r15, %rdx
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L485:
	movq	%rdx, %rdi
	movl	$48, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L480:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L481:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L482:
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r12
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L483:
	movq	%rdx, %rdi
	movl	$48, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L484:
	movq	%rdx, %rdi
	movl	$48, %esi
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-72(%rbp), %rdx
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L486:
	movq	%rdx, %rdi
	movl	$40, %esi
	movq	%r8, -88(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %r8
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L487:
	movl	$40, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L488:
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	jmp	.L466
.L489:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv
	.cfi_startproc
	.type	_ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv.cold, @function
_ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv.cold:
.LFSB18182:
.L452:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	$769, -64(%rbp)
	movq	32, %rax
	ud2
.L425:
	movq	32, %rax
	ud2
.L449:
	movq	32, %rax
	ud2
.L433:
	movq	$77, -64(%rbp)
	movq	32, %rax
	ud2
.L430:
	movq	32, %rax
	ud2
.L444:
	movq	32, %rax
	ud2
.L438:
	movq	32, %rax
	ud2
.L441:
	movq	32, %rax
	ud2
	.cfi_endproc
.LFE18182:
	.section	.text._ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv
	.size	_ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv, .-_ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv
	.section	.text.unlikely._ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv
	.size	_ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv.cold, .-_ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv.cold
.LCOLDE25:
	.section	.text._ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv
.LHOTE25:
	.section	.text._ZN2v88internal4wasm11AsmJsParserC2EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParserC2EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE
	.type	_ZN2v88internal4wasm11AsmJsParserC2EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE, @function
_ZN2v88internal4wasm11AsmJsParserC2EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE:
.LFB18180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$8, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	%rcx, %rsi
	movq	%rbx, -8(%rdi)
	call	_ZN2v88internal12AsmJsScannerC1EPNS0_20Utf16CharacterStreamE@PLT
	movq	16(%rbx), %r13
	movq	24(%rbx), %rax
	subq	%r13, %rax
	cmpq	$383, %rax
	jbe	.L494
	leaq	384(%r13), %rax
	movq	%rax, 16(%rbx)
.L492:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm17WasmModuleBuilderC1EPNS0_4ZoneE@PLT
	movq	(%r12), %rax
	movq	%r13, %rdi
	xorl	%esi, %esi
	movdqa	.LC26(%rip), %xmm0
	movq	%r13, 296(%r12)
	movq	%rax, 400(%r12)
	movq	%rax, 432(%r12)
	movq	%rax, 464(%r12)
	movq	%rax, 496(%r12)
	leaq	736(%r12), %rax
	movq	%r14, 320(%r12)
	movq	%rbx, 336(%r12)
	movq	%rbx, 368(%r12)
	movups	%xmm0, 552(%r12)
	movq	$0, 312(%r12)
	movq	$0, 328(%r12)
	movq	$0, 344(%r12)
	movq	$0, 352(%r12)
	movq	$0, 360(%r12)
	movq	$0, 376(%r12)
	movq	$0, 384(%r12)
	movq	$0, 392(%r12)
	movq	$0, 408(%r12)
	movq	$0, 416(%r12)
	movq	$0, 424(%r12)
	movq	$0, 440(%r12)
	movq	$0, 448(%r12)
	movq	$0, 456(%r12)
	movq	$0, 472(%r12)
	movq	$0, 480(%r12)
	movq	$0, 488(%r12)
	movq	$0, 504(%r12)
	movq	$0, 512(%r12)
	movq	$0, 520(%r12)
	movb	$0, 540(%r12)
	movb	$0, 568(%r12)
	movq	%rbx, 584(%r12)
	movq	%rbx, 728(%r12)
	movq	$0, 576(%r12)
	movq	$0, 592(%r12)
	movq	$0, 600(%r12)
	movq	$0, 608(%r12)
	movq	$0, 680(%r12)
	movq	$0, 696(%r12)
	movl	$0, 724(%r12)
	movq	%rax, 744(%r12)
	movq	%rax, 736(%r12)
	movq	$0, 752(%r12)
	call	_ZN2v88internal4wasm17WasmModuleBuilder16SetMinMemorySizeEj@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11AsmJsParser21InitializeStdlibTypesEv
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	movl	$384, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L492
	.cfi_endproc
.LFE18180:
	.size	_ZN2v88internal4wasm11AsmJsParserC2EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE, .-_ZN2v88internal4wasm11AsmJsParserC2EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE
	.globl	_ZN2v88internal4wasm11AsmJsParserC1EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE
	.set	_ZN2v88internal4wasm11AsmJsParserC1EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE,_ZN2v88internal4wasm11AsmJsParserC2EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE
	.section	.rodata._ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC28:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.type	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, @function
_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm:
.LFB21434:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L516
	movabsq	$-3689348814741910323, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$53687091, %esi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rcx
	movq	24(%r13), %rax
	movq	%rcx, %rbx
	subq	8(%rdi), %rbx
	subq	%rcx, %rax
	movq	%rsi, %rdi
	movq	%rbx, %r14
	sarq	$3, %rax
	sarq	$3, %r14
	imulq	%rdx, %rax
	imulq	%rdx, %r14
	subq	%r14, %rdi
	cmpq	%r12, %rax
	jb	.L497
	movdqa	.LC27(%rip), %xmm0
	movq	%rcx, %rax
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L498:
	movq	$0, 32(%rax)
	addq	$40, %rax
	movups	%xmm0, -40(%rax)
	movq	$0, -24(%rax)
	movl	$0, -16(%rax)
	movl	$0, -12(%rax)
	movb	$1, -4(%rax)
	subq	$1, %rdx
	jne	.L498
	leaq	(%r12,%r12,4), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 16(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%r12, %rdi
	jb	.L519
	cmpq	%r14, %r12
	movq	%r14, %rax
	movq	0(%r13), %r8
	cmovnb	%r12, %rax
	movq	16(%r8), %rdi
	addq	%r14, %rax
	cmpq	$53687091, %rax
	cmova	%rsi, %rax
	leaq	(%rax,%rax,4), %r15
	movq	24(%r8), %rax
	salq	$3, %r15
	subq	%rdi, %rax
	movq	%r15, %rsi
	cmpq	%r15, %rax
	jb	.L520
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L502:
	movdqa	.LC27(%rip), %xmm0
	leaq	(%rdi,%rbx), %rax
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L503:
	movq	$0, 32(%rax)
	addq	$40, %rax
	movups	%xmm0, -40(%rax)
	movq	$0, -24(%rax)
	movl	$0, -16(%rax)
	movl	$0, -12(%rax)
	movb	$1, -4(%rax)
	subq	$1, %rdx
	jne	.L503
	movq	8(%r13), %rax
	movq	16(%r13), %rsi
	movq	%rdi, %rdx
	subq	%rax, %rdx
	cmpq	%rax, %rsi
	je	.L505
	.p2align 4,,10
	.p2align 3
.L504:
	movdqu	(%rax), %xmm1
	addq	$40, %rax
	movups	%xmm1, -40(%rax,%rdx)
	movdqu	-24(%rax), %xmm2
	movups	%xmm2, -24(%rax,%rdx)
	movq	-8(%rax), %rcx
	movq	%rcx, -8(%rax,%rdx)
	cmpq	%rax, %rsi
	jne	.L504
.L505:
	addq	%r14, %r12
	addq	%rdi, %r15
	movq	%rdi, 8(%r13)
	leaq	(%r12,%r12,4), %rax
	movq	%r15, 24(%r13)
	leaq	(%rdi,%rax,8), %rax
	movq	%rax, 16(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L520:
	.cfi_restore_state
	movq	%r8, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rdi
	jmp	.L502
.L519:
	leaq	.LC28(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE21434:
	.size	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm, .-_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	.section	.text._ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	.type	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi, @function
_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi:
.LFB18192:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	cmpl	$255, %esi
	jle	.L522
	movq	344(%rdi), %rdx
	movq	352(%rdi), %rax
	movabsq	$-3689348814741910323, %rcx
	subl	$256, %esi
	movslq	%esi, %rbx
	subq	%rdx, %rax
	leaq	1(%rbx), %rsi
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rax, %rsi
	ja	.L523
.L528:
	leaq	(%rbx,%rbx,4), %rax
	popq	%rbx
	popq	%r12
	leaq	(%rdx,%rax,8), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	cmpl	$-9999, %esi
	jge	.L525
	movq	376(%rdi), %rdx
	movq	384(%rdi), %rax
	movl	$-10000, %ebx
	movabsq	$-3689348814741910323, %rcx
	subl	%esi, %ebx
	subq	%rdx, %rax
	movslq	%ebx, %rbx
	sarq	$3, %rax
	leaq	1(%rbx), %rsi
	imulq	%rcx, %rax
	cmpq	%rsi, %rax
	jnb	.L528
	subq	%rax, %rsi
	leaq	368(%rdi), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	376(%r12), %rdx
	leaq	(%rbx,%rbx,4), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	(%rdx,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L523:
	.cfi_restore_state
	subq	%rax, %rsi
	leaq	336(%rdi), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	344(%r12), %rdx
	leaq	(%rbx,%rbx,4), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	(%rdx,%rax,8), %rax
	ret
.L525:
	.cfi_restore_state
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18192:
	.size	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi, .-_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser14ValidateExportEv.str1.1,"aMS",@progbits,1
.LC29:
	.string	"Illegal export name"
.LC30:
	.string	"Expected function name"
.LC31:
	.string	"Expected function"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser14ValidateExportEv.str1.8,"aMS",@progbits,1
	.align 8
.LC32:
	.string	"Single function export must be a function name"
	.align 8
.LC33:
	.string	"Single function export must be a function"
	.section	.text._ZN2v88internal4wasm11AsmJsParser14ValidateExportEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser14ValidateExportEv
	.type	_ZN2v88internal4wasm11AsmJsParser14ValidateExportEv, @function
_ZN2v88internal4wasm11AsmJsParser14ValidateExportEv:
.LFB18215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$-9948, 16(%rdi)
	je	.L531
.L555:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L530:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	.cfi_restore_state
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r13d
	cmpl	$123, %r13d
	je	.L533
	cmpl	$255, %r13d
	jg	.L556
	leaq	.LC32(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L556:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	cmpl	$4, 32(%rax)
	je	.L547
	leaq	.LC33(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L533:
	movq	%r12, %rdi
	leaq	64(%rbx), %r13
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
.L545:
	movq	(%rbx), %rdi
	movq	72(%rbx), %rax
	movq	16(%rdi), %r14
	leaq	7(%rax), %rsi
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%r14, %rax
	cmpq	%rax, %rsi
	ja	.L557
	addq	%r14, %rsi
	movq	%rsi, 16(%rdi)
.L537:
	movq	72(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4copyEPcmm@PLT
	movl	16(%rbx), %eax
	movslq	72(%rbx), %r15
	addl	$9999, %eax
	cmpl	$10254, %eax
	jbe	.L538
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$58, 16(%rbx)
	jne	.L555
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %esi
	cmpl	$255, %esi
	jle	.L558
	movq	%r12, %rdi
	movl	%esi, -52(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	-52(%rbp), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	cmpl	$4, 32(%rax)
	jne	.L559
	movq	8(%rax), %rax
	movq	296(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdx
	movl	60(%rax), %r8d
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddExportENS0_6VectorIKcEENS1_20ImportExportKindCodeEj@PLT
	movl	16(%rbx), %eax
	cmpl	$44, %eax
	je	.L560
	cmpl	$125, %eax
	jne	.L555
.L546:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	.p2align 4,,10
	.p2align 3
.L547:
	.cfi_restore_state
	movq	_ZN2v88internal5AsmJs19kSingleFunctionNameE(%rip), %r12
	movq	296(%rbx), %r13
	movq	8(%rax), %rbx
	movq	%r12, %rdi
	call	strlen@PLT
	movl	60(%rbx), %r8d
	addq	$24, %rsp
	movq	%r12, %rsi
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	xorl	%ecx, %ecx
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm17WasmModuleBuilder9AddExportENS0_6VectorIKcEENS1_20ImportExportKindCodeEj@PLT
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$125, 16(%rbx)
	je	.L546
	jmp	.L545
.L557:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L537
.L559:
	leaq	.LC31(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L530
.L558:
	leaq	.LC30(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L530
.L538:
	leaq	.LC29(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L530
	.cfi_endproc
.LFE18215:
	.size	_ZN2v88internal4wasm11AsmJsParser14ValidateExportEv, .-_ZN2v88internal4wasm11AsmJsParser14ValidateExportEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser21ValidateFunctionTableEv.str1.1,"aMS",@progbits,1
.LC34:
	.string	"Expected table name"
.LC35:
	.string	"Function table redefined"
.LC36:
	.string	"Function table name collides"
.LC37:
	.string	"Exceeded function table size"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser21ValidateFunctionTableEv.str1.8,"aMS",@progbits,1
	.align 8
.LC38:
	.string	"Function table definition doesn't match use"
	.align 8
.LC39:
	.string	"Function table size does not match uses"
	.section	.text._ZN2v88internal4wasm11AsmJsParser21ValidateFunctionTableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser21ValidateFunctionTableEv
	.type	_ZN2v88internal4wasm11AsmJsParser21ValidateFunctionTableEv, @function
_ZN2v88internal4wasm11AsmJsParser21ValidateFunctionTableEv:
.LFB18216:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpl	$-9946, 16(%rdi)
	je	.L562
.L592:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L561:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r13d
	cmpl	$255, %r13d
	jg	.L564
	leaq	.LC34(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movq	%rax, %r13
	movl	32(%rax), %eax
	cmpl	$5, %eax
	je	.L593
	testl	%eax, %eax
	jne	.L594
.L567:
	cmpl	$61, 16(%rbx)
	jne	.L592
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$91, 16(%rbx)
	jne	.L592
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r14d
.L576:
	cmpl	$255, %r14d
	jle	.L595
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	cmpl	$4, 32(%rax)
	movq	%rax, %r14
	jne	.L596
	cmpl	$5, 32(%r13)
	je	.L597
.L572:
	movl	16(%rbx), %eax
	leaq	1(%r15), %rdx
	cmpl	$44, %eax
	je	.L598
	cmpl	$93, %eax
	jne	.L592
.L577:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$5, 32(%r13)
	jne	.L578
	movl	24(%r13), %eax
	cmpq	%rax, %r15
	je	.L578
	leaq	.LC39(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L593:
	cmpb	$0, 37(%r13)
	jne	.L599
	movb	$1, 37(%r13)
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L599:
	leaq	.LC35(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L594:
	leaq	.LC36(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L561
.L597:
	movl	24(%r13), %eax
	addq	$1, %rax
	cmpq	%r15, %rax
	jbe	.L600
	movq	0(%r13), %rsi
	movq	(%r14), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L601
	movl	28(%r13), %esi
	movl	28(%r14), %edx
	movq	296(%rbx), %rdi
	addl	%r15d, %esi
	call	_ZN2v88internal4wasm17WasmModuleBuilder19SetIndirectFunctionEjj@PLT
	jmp	.L572
.L578:
	movl	16(%rbx), %eax
	cmpl	$59, %eax
	je	.L579
	cmpl	$125, %eax
	je	.L561
	cmpb	$0, 292(%rbx)
	jne	.L561
	leaq	.LC2(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L561
.L598:
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r14d
	cmpl	$93, %r14d
	je	.L577
	movq	-56(%rbp), %rdx
	movq	%rdx, %r15
	jmp	.L576
.L579:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
.L596:
	.cfi_restore_state
	leaq	.LC31(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L561
.L595:
	leaq	.LC30(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L561
.L601:
	leaq	.LC38(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L561
.L600:
	leaq	.LC37(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L561
	.cfi_endproc
.LFE18216:
	.size	_ZN2v88internal4wasm11AsmJsParser21ValidateFunctionTableEv, .-_ZN2v88internal4wasm11AsmJsParser21ValidateFunctionTableEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser10IdentifierEv.str1.1,"aMS",@progbits,1
.LC40:
	.string	"Undefined local variable"
.LC41:
	.string	"Undefined global variable"
	.section	.text._ZN2v88internal4wasm11AsmJsParser10IdentifierEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser10IdentifierEv
	.type	_ZN2v88internal4wasm11AsmJsParser10IdentifierEv, @function
_ZN2v88internal4wasm11AsmJsParser10IdentifierEv:
.LFB18239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	16(%rdi), %r12d
	movq	%rdi, %rbx
	movq	$0, 680(%rdi)
	cmpl	$-9999, %r12d
	jl	.L611
	cmpl	$255, %r12d
	jle	.L606
	leaq	8(%rdi), %rdi
	subl	$256, %r12d
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	344(%rbx), %rdx
	movq	352(%rbx), %rax
	movslq	%r12d, %r12
	movabsq	$-3689348814741910323, %rcx
	leaq	1(%r12), %rsi
	subq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L607
	leaq	(%r12,%r12,4), %rax
	leaq	(%rdx,%rax,8), %r12
	cmpl	$2, 32(%r12)
	je	.L608
.L612:
	leaq	.LC41(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L608:
	.cfi_restore_state
	movq	304(%rbx), %rdi
	movl	28(%r12), %edx
	movl	$35, %esi
	addl	752(%rbx), %edx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithI32VENS1_10WasmOpcodeEi@PLT
	movq	(%r12), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L611:
	.cfi_restore_state
	leaq	8(%rdi), %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	cmpl	$1, 32(%rax)
	movq	%rax, %r12
	je	.L604
	leaq	.LC40(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	subq	%rax, %rsi
	leaq	336(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	344(%rbx), %rdx
	leaq	(%r12,%r12,4), %rax
	leaq	(%rdx,%rax,8), %r12
	cmpl	$2, 32(%r12)
	jne	.L612
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L604:
	movl	28(%rax), %esi
	movq	304(%rbx), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj@PLT
	movq	(%r12), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L606:
	.cfi_restore_state
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18239:
	.size	_ZN2v88internal4wasm11AsmJsParser10IdentifierEv, .-_ZN2v88internal4wasm11AsmJsParser10IdentifierEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser27ValidateModuleVarFromGlobalEPNS2_7VarInfoEb.str1.8,"aMS",@progbits,1
	.align 8
.LC46:
	.string	"Can only use immutable variables in global definition"
	.align 8
.LC47:
	.string	"Can only define immutable variables with other immutables"
	.align 8
.LC48:
	.string	"Expected int, float, double, or fround for global definition"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser27ValidateModuleVarFromGlobalEPNS2_7VarInfoEb.str1.1,"aMS",@progbits,1
.LC54:
	.string	"Expected numeric literal"
	.section	.text._ZN2v88internal4wasm11AsmJsParser27ValidateModuleVarFromGlobalEPNS2_7VarInfoEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser27ValidateModuleVarFromGlobalEPNS2_7VarInfoEb
	.type	_ZN2v88internal4wasm11AsmJsParser27ValidateModuleVarFromGlobalEPNS2_7VarInfoEb, @function
_ZN2v88internal4wasm11AsmJsParser27ValidateModuleVarFromGlobalEPNS2_7VarInfoEb:
.LFB18211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	8(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	16(%rdi), %r12d
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$255, %r12d
	jle	.L614
	movq	344(%rbx), %rdx
	movq	352(%rbx), %rax
	movabsq	$-3689348814741910323, %rcx
	subl	$256, %r12d
	movslq	%r12d, %r12
	subq	%rdx, %rax
	leaq	1(%r12), %rsi
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L615
.L644:
	leaq	(%r12,%r12,4), %rax
	leaq	(%rdx,%rax,8), %r12
.L616:
	movq	672(%rbx), %rsi
	movq	(%r12), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L656
	cmpl	$40, 16(%rbx)
	je	.L624
.L655:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L613:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L657
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	cmpl	$-9999, %r12d
	jge	.L617
	movq	376(%rbx), %rcx
	movq	384(%rbx), %rax
	movabsq	$-3689348814741910323, %rdx
	subq	%rcx, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movl	$-10000, %edx
	subl	%r12d, %edx
	movslq	%edx, %r12
	leaq	1(%r12), %rsi
	cmpq	%rsi, %rax
	jnb	.L645
	subq	%rax, %rsi
	leaq	368(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	376(%rbx), %rcx
.L645:
	leaq	(%r12,%r12,4), %rax
	leaq	(%rcx,%rax,8), %r12
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L656:
	cmpb	$0, 36(%r12)
	jne	.L658
	testb	%r14b, %r14b
	je	.L622
	leaq	.LC47(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L615:
	subq	%rax, %rsi
	leaq	336(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	344(%rbx), %rdx
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L622:
	movq	(%r12), %rdi
	movl	$769, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L623
	movq	(%r12), %rdi
	movl	$57357, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L659
.L623:
	movl	$2, 32(%r13)
	movq	(%r12), %rax
	movq	%rax, 0(%r13)
	movl	28(%r12), %eax
	movb	$0, 36(%r13)
	movl	%eax, 28(%r13)
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L658:
	leaq	.LC46(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L624:
	movq	%r15, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %eax
	cmpl	$45, %eax
	je	.L625
	cmpl	$-4, %eax
	je	.L660
	cmpl	$-3, %eax
	je	.L661
.L640:
	leaq	.LC54(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L659:
	movq	(%r12), %rdi
	movl	$237, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L623
	leaq	.LC48(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L613
.L617:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L625:
	movq	%r15, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %eax
	cmpl	$-4, %eax
	je	.L662
	cmpl	$-3, %eax
	jne	.L640
	movl	288(%rbx), %r12d
	movq	%r15, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
	xorpd	.LC49(%rip), %xmm0
.L638:
	movl	$4, -80(%rbp)
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, -72(%rbp)
.L654:
	leaq	-80(%rbp), %rcx
	movl	$1, %edx
	movl	$3, %esi
	movl	$2, 32(%r13)
	movq	$57357, 0(%r13)
	movq	296(%rbx), %rdi
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	%r14b, 36(%r13)
	movl	%eax, 28(%r13)
	cmpl	$41, 16(%rbx)
	jne	.L655
	movq	%r15, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L660:
	movsd	280(%rbx), %xmm0
	movq	%r15, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movsd	-88(%rbp), %xmm0
.L627:
	comisd	.LC50(%rip), %xmm0
	jbe	.L651
	movsd	.LC51(%rip), %xmm2
	movss	.LC42(%rip), %xmm1
	comisd	%xmm0, %xmm2
	jnb	.L631
	movss	.LC43(%rip), %xmm1
.L631:
	movl	$4, -80(%rbp)
	movss	%xmm1, -72(%rbp)
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L651:
	movsd	.LC52(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L652
	comisd	.LC53(%rip), %xmm0
	movss	.LC44(%rip), %xmm1
	jnb	.L631
	movss	.LC45(%rip), %xmm1
	jmp	.L631
.L662:
	movsd	280(%rbx), %xmm0
	movq	%r15, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movsd	-88(%rbp), %xmm0
	xorpd	.LC49(%rip), %xmm0
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L661:
	movl	288(%rbx), %r12d
	movq	%r15, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r12, %xmm0
	jmp	.L638
.L652:
	pxor	%xmm1, %xmm1
	cvtsd2ss	%xmm0, %xmm1
	jmp	.L631
.L657:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18211:
	.size	_ZN2v88internal4wasm11AsmJsParser27ValidateModuleVarFromGlobalEPNS2_7VarInfoEb, .-_ZN2v88internal4wasm11AsmJsParser27ValidateModuleVarFromGlobalEPNS2_7VarInfoEb
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser18ValidateModuleVarsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC55:
	.string	"Stack overflow while parsing asm.js module."
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser18ValidateModuleVarsEv.str1.1,"aMS",@progbits,1
.LC56:
	.string	"Expected identifier"
.LC57:
	.string	"Redefinition of variable"
.LC58:
	.string	"Numeric literal out of range"
.LC59:
	.string	"Bad variable declaration"
	.section	.text._ZN2v88internal4wasm11AsmJsParser18ValidateModuleVarsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser18ValidateModuleVarsEv
	.type	_ZN2v88internal4wasm11AsmJsParser18ValidateModuleVarsEv, @function
_ZN2v88internal4wasm11AsmJsParser18ValidateModuleVarsEv:
.LFB18209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	8(%rdi), %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	16(%rdi), %eax
	.p2align 4,,10
	.p2align 3
.L664:
	cmpl	$-9946, %eax
	je	.L665
	cmpl	$-9958, %eax
	jne	.L663
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
.L668:
	cmpb	$1, %r13b
	sbbq	%rbx, %rbx
	andl	$1056, %ebx
	addq	$769, %rbx
.L704:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r15), %rax
	jb	.L724
	movl	16(%r15), %esi
	cmpl	$255, %esi
	jg	.L671
	leaq	.LC56(%rip), %rax
	movb	$1, 540(%r15)
	movq	%rax, 544(%r15)
	movq	32(%r15), %rax
	movl	%eax, 552(%r15)
.L663:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L725
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%esi, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	-88(%rbp), %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movq	%rax, %r9
	movl	32(%rax), %eax
	testl	%eax, %eax
	jne	.L726
	cmpl	$61, 16(%r15)
	je	.L673
.L694:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%r15)
	movq	%rax, 544(%r15)
	movq	32(%r15), %rax
	movl	%eax, 552(%r15)
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L665:
	movq	%r12, %rdi
	movl	$1, %r14d
	movl	$1, %r13d
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L726:
	leaq	.LC57(%rip), %rax
	movb	$1, 540(%r15)
	movq	%rax, 544(%r15)
	movq	32(%r15), %rax
	movl	%eax, 552(%r15)
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L673:
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r15), %eax
	movq	-88(%rbp), %r9
	cmpl	$-4, %eax
	je	.L674
	cmpl	$-3, %eax
	je	.L727
	cmpl	$45, %eax
	je	.L728
	cmpl	$-9949, %eax
	je	.L729
	cmpl	556(%r15), %eax
	je	.L730
	cmpl	%eax, 560(%r15)
	je	.L696
	cmpl	$43, %eax
	je	.L696
	cmpl	$255, %eax
	jle	.L719
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r15), %rax
	movq	-88(%rbp), %r9
	jnb	.L702
	.p2align 4,,10
	.p2align 3
.L724:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%r15)
	movq	%rax, 544(%r15)
	movq	32(%r15), %rax
	movl	%eax, 552(%r15)
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L728:
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r15), %eax
	movq	-88(%rbp), %r9
	cmpl	$-4, %eax
	je	.L731
	cmpl	$-3, %eax
	je	.L732
	leaq	.LC54(%rip), %rax
	movb	$1, 540(%r15)
	movq	%rax, 544(%r15)
	movq	32(%r15), %rax
	movl	%eax, 552(%r15)
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L696:
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r15), %rax
	movq	-88(%rbp), %r9
	jb	.L724
	movl	%r14d, %edx
	movq	%r9, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarImportEPNS2_7VarInfoEb
	.p2align 4,,10
	.p2align 3
.L677:
	cmpb	$0, 540(%r15)
	jne	.L663
	movl	16(%r15), %eax
	cmpl	$44, %eax
	je	.L733
	cmpl	$59, %eax
	je	.L705
	cmpl	$125, %eax
	je	.L663
	cmpb	$0, 292(%r15)
	jne	.L664
	movq	32(%r15), %rdx
	leaq	.LC2(%rip), %rcx
	movb	$1, 540(%r15)
	movq	%rcx, 544(%r15)
	movl	%edx, 552(%r15)
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L733:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L705:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r15), %eax
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L674:
	movsd	280(%r15), %xmm0
	movq	%r12, %rdi
	movq	%r9, -96(%rbp)
	movsd	%xmm0, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	$5, -80(%rbp)
	movsd	-88(%rbp), %xmm0
.L721:
	movq	-96(%rbp), %r9
	movsd	%xmm0, -72(%rbp)
	leaq	-80(%rbp), %rcx
	movl	$1, %edx
	movl	$4, %esi
	movl	$2, 32(%r9)
	movq	$237, (%r9)
	movq	296(%r15), %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movq	-88(%rbp), %r9
	movl	%eax, 28(%r9)
	movb	%r13b, 36(%r9)
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L727:
	movl	288(%r15), %eax
	movq	%r12, %rdi
	movq	%r9, -96(%rbp)
	movl	%eax, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %r9
	testl	%eax, %eax
	js	.L687
	movl	$2, -80(%rbp)
.L722:
	movl	%eax, -72(%rbp)
	leaq	-80(%rbp), %rcx
	movl	$1, %edx
	movl	$1, %esi
	movl	$2, 32(%r9)
	movq	%rbx, (%r9)
	movq	296(%r15), %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movq	-88(%rbp), %r9
	movl	%eax, 28(%r9)
	movb	%r13b, 36(%r9)
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L729:
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r15), %rax
	movq	-88(%rbp), %r9
	jb	.L724
	movl	556(%r15), %eax
	cmpl	%eax, 16(%r15)
	jne	.L694
	movq	%r9, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE.part.0
	jmp	.L677
.L719:
	leaq	.LC59(%rip), %rax
	movb	$1, 540(%r15)
	movq	%rax, 544(%r15)
	movq	32(%r15), %rax
	movl	%eax, 552(%r15)
	jmp	.L663
.L730:
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$46, 16(%r15)
	movq	-88(%rbp), %r9
	jne	.L694
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r15), %rax
	movq	-88(%rbp), %r9
	jb	.L724
	movq	%r9, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarStdlibEPNS2_7VarInfoE
	jmp	.L677
.L702:
	movl	%r14d, %edx
	movq	%r9, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser27ValidateModuleVarFromGlobalEPNS2_7VarInfoEb
	jmp	.L677
.L687:
	leaq	.LC58(%rip), %rax
	movb	$1, 540(%r15)
	movq	%rax, 544(%r15)
	movq	32(%r15), %rax
	movl	%eax, 552(%r15)
	jmp	.L663
.L731:
	movsd	280(%r15), %xmm0
	movq	%r12, %rdi
	movq	%r9, -96(%rbp)
	movsd	%xmm0, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movsd	-88(%rbp), %xmm0
	movl	$5, -80(%rbp)
	xorpd	.LC49(%rip), %xmm0
	jmp	.L721
.L732:
	movl	288(%r15), %eax
	movq	%r12, %rdi
	movq	%r9, -96(%rbp)
	movl	%eax, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %r9
	testl	%eax, %eax
	js	.L687
	movl	$2, -80(%rbp)
	negl	%eax
	jmp	.L722
.L725:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18209:
	.size	_ZN2v88internal4wasm11AsmJsParser18ValidateModuleVarsEv, .-_ZN2v88internal4wasm11AsmJsParser18ValidateModuleVarsEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser8PeekCallEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser8PeekCallEv
	.type	_ZN2v88internal4wasm11AsmJsParser8PeekCallEv, @function
_ZN2v88internal4wasm11AsmJsParser8PeekCallEv:
.LFB18276:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %eax
	cmpl	$255, %eax
	jg	.L735
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L735:
	movabsq	$-3689348814741910323, %rsi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$256, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movslq	%eax, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	344(%rdi), %rcx
	movq	352(%rdi), %rdx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	imulq	%rsi, %rdx
	leaq	1(%r12), %rsi
	cmpq	%rsi, %rdx
	jb	.L737
.L745:
	leaq	(%r12,%r12,4), %rax
	cmpl	$4, 32(%rcx,%rax,8)
	je	.L739
	movl	16(%rbx), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	cmpl	$5, 32(%rax)
	jle	.L750
.L739:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L750:
	.cfi_restore_state
	movl	16(%rbx), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movl	32(%rax), %eax
	testl	%eax, %eax
	je	.L743
	movl	16(%rbx), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	cmpl	$5, 32(%rax)
	je	.L743
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L737:
	.cfi_restore_state
	subq	%rdx, %rsi
	leaq	336(%rdi), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	344(%rbx), %rcx
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L743:
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %edx
	movq	%r12, %rdi
	cmpl	$40, %edx
	sete	%al
	cmpl	$91, %edx
	sete	%dl
	orb	%dl, %al
	movb	%al, -17(%rbp)
	call	_ZN2v88internal12AsmJsScanner6RewindEv@PLT
	movzbl	-17(%rbp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18276:
	.size	_ZN2v88internal4wasm11AsmJsParser8PeekCallEv, .-_ZN2v88internal4wasm11AsmJsParser8PeekCallEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser17ValidateModuleVarEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser17ValidateModuleVarEb
	.type	_ZN2v88internal4wasm11AsmJsParser17ValidateModuleVarEb, @function
_ZN2v88internal4wasm11AsmJsParser17ValidateModuleVarEb:
.LFB18210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	16(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$255, %ebx
	jg	.L752
	leaq	.LC56(%rip), %rax
	movb	$1, 540(%rdi)
	movq	%rax, 544(%rdi)
	movq	32(%rdi), %rax
	movl	%eax, 552(%rdi)
.L751:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L807
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L752:
	.cfi_restore_state
	leaq	8(%rdi), %r14
	movl	%esi, %r13d
	subl	$256, %ebx
	movq	%r14, %rdi
	movslq	%ebx, %rbx
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	344(%r12), %rdx
	leaq	1(%rbx), %rsi
	movq	352(%r12), %rax
	movabsq	$-3689348814741910323, %rcx
	subq	%rdx, %rax
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L754
.L787:
	leaq	(%rbx,%rbx,4), %rax
	leaq	(%rdx,%rax,8), %r15
	movl	32(%r15), %eax
	testl	%eax, %eax
	jne	.L808
	cmpl	$61, 16(%r12)
	je	.L756
.L778:
	movb	$1, 540(%r12)
	leaq	.LC0(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L808:
	movb	$1, 540(%r12)
	leaq	.LC57(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L754:
	subq	%rax, %rsi
	leaq	336(%r12), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	344(%r12), %rdx
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L756:
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r12), %eax
	cmpl	$-4, %eax
	je	.L757
	cmpl	$-3, %eax
	je	.L809
	cmpl	$45, %eax
	je	.L810
	cmpl	$-9949, %eax
	je	.L811
	cmpl	%eax, 556(%r12)
	je	.L812
	cmpl	%eax, 560(%r12)
	je	.L780
	cmpl	$43, %eax
	je	.L780
	cmpl	$255, %eax
	jle	.L801
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L805
	movzbl	%r13b, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser27ValidateModuleVarFromGlobalEPNS2_7VarInfoEb
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L811:
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L805
	movl	556(%r12), %eax
	cmpl	%eax, 16(%r12)
	jne	.L778
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser26ValidateModuleVarNewStdlibEPNS2_7VarInfoE.part.0
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L757:
	movsd	280(%r12), %xmm0
	movq	%r14, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	$5, -80(%rbp)
	movsd	-88(%rbp), %xmm0
.L804:
	movsd	%xmm0, -72(%rbp)
	leaq	-80(%rbp), %rcx
	movl	$1, %edx
	movl	$4, %esi
	movl	$2, 32(%r15)
	movq	$237, (%r15)
	movq	296(%r12), %rdi
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	%r13b, 36(%r15)
	movl	%eax, 28(%r15)
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L805:
	movb	$1, 540(%r12)
	leaq	.LC55(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L809:
	movl	288(%r12), %ebx
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	testl	%ebx, %ebx
	js	.L770
	movl	$2, -80(%rbp)
.L806:
	cmpb	$1, %r13b
	movl	%ebx, -72(%rbp)
	leaq	-80(%rbp), %rcx
	movl	$1, %edx
	sbbq	%rax, %rax
	movl	$2, 32(%r15)
	movl	$1, %esi
	andl	$1056, %eax
	addq	$769, %rax
	movq	%rax, (%r15)
	movq	296(%r12), %rdi
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddGlobalENS1_9ValueTypeEbRKNS1_12WasmInitExprE@PLT
	movb	%r13b, 36(%r15)
	movl	%eax, 28(%r15)
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L780:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L805
	movzbl	%r13b, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarImportEPNS2_7VarInfoEb
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L810:
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r12), %eax
	cmpl	$-4, %eax
	je	.L813
	cmpl	$-3, %eax
	je	.L814
	movb	$1, 540(%r12)
	leaq	.LC54(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	jmp	.L751
.L801:
	movb	$1, 540(%r12)
	leaq	.LC59(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	jmp	.L751
.L812:
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$46, 16(%r12)
	jne	.L778
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L805
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser23ValidateModuleVarStdlibEPNS2_7VarInfoE
	jmp	.L751
.L770:
	movb	$1, 540(%r12)
	leaq	.LC58(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	jmp	.L751
.L813:
	movsd	280(%r12), %xmm0
	movq	%r14, %rdi
	movsd	%xmm0, -88(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movsd	-88(%rbp), %xmm0
	movl	$5, -80(%rbp)
	xorpd	.LC49(%rip), %xmm0
	jmp	.L804
.L814:
	movl	288(%r12), %ebx
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	testl	%ebx, %ebx
	js	.L770
	movl	$2, -80(%rbp)
	negl	%ebx
	jmp	.L806
.L807:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18210:
	.size	_ZN2v88internal4wasm11AsmJsParser17ValidateModuleVarEb, .-_ZN2v88internal4wasm11AsmJsParser17ValidateModuleVarEb
	.section	.text._ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_:
.LFB21444:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %r12
	movq	%r14, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L853
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rdx
	testq	%rax, %rax
	je	.L831
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L854
	movl	$2147483640, %esi
	movl	$2147483640, %ecx
.L817:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L855
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L820:
	leaq	(%rax,%rcx), %rsi
	leaq	8(%rax), %rcx
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L854:
	testq	%rcx, %rcx
	jne	.L856
	movl	$8, %ecx
	xorl	%esi, %esi
	xorl	%eax, %eax
.L818:
	movq	(%r15), %rdi
	movq	%rdi, (%rax,%rdx)
	cmpq	%r12, %rbx
	je	.L821
	leaq	-8(%rbx), %rdi
	leaq	15(%rax), %rcx
	movq	%r12, %rdx
	subq	%r12, %rdi
	subq	%r12, %rcx
	movq	%rdi, %r8
	shrq	$3, %r8
	cmpq	$30, %rcx
	jbe	.L834
	movabsq	$2305843009213693950, %rcx
	testq	%rcx, %r8
	je	.L834
	addq	$1, %r8
	xorl	%edx, %edx
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L823:
	movdqu	(%r12,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L823
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r12
	addq	%rax, %rcx
	cmpq	%rdx, %r8
	je	.L825
	movq	(%r12), %rdx
	movq	%rdx, (%rcx)
.L825:
	leaq	16(%rax,%rdi), %rcx
.L821:
	cmpq	%r14, %rbx
	je	.L826
	movq	%r14, %rdi
	movq	%rbx, %rdx
	subq	%rbx, %rdi
	leaq	-8(%rdi), %r8
	leaq	15(%rbx), %rdi
	movq	%r8, %r9
	subq	%rcx, %rdi
	shrq	$3, %r9
	cmpq	$30, %rdi
	jbe	.L835
	movabsq	$2305843009213693950, %rdi
	testq	%rdi, %r9
	je	.L835
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L828:
	movdqu	(%rbx,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L828
	movq	%r9, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %r10
	leaq	(%rcx,%r10), %rdi
	addq	%r10, %rbx
	cmpq	%rdx, %r9
	je	.L830
	movq	(%rbx), %rdx
	movq	%rdx, (%rdi)
.L830:
	leaq	8(%rcx,%r8), %rcx
.L826:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rsi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L831:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %ecx
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L835:
	movq	%rcx, %rdi
	.p2align 4,,10
	.p2align 3
.L827:
	movl	(%rdx), %r10d
	movl	4(%rdx), %r9d
	addq	$8, %rdx
	addq	$8, %rdi
	movl	%r10d, -8(%rdi)
	movl	%r9d, -4(%rdi)
	cmpq	%rdx, %r14
	jne	.L827
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L834:
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L822:
	movl	(%rdx), %r9d
	movl	4(%rdx), %r8d
	addq	$8, %rdx
	addq	$8, %rcx
	movl	%r9d, -8(%rcx)
	movl	%r8d, -4(%rcx)
	cmpq	%rdx, %rbx
	jne	.L822
	jmp	.L825
.L855:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L820
.L853:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L856:
	cmpq	$268435455, %rcx
	movl	$268435455, %eax
	cmova	%rax, %rcx
	salq	$3, %rcx
	movq	%rcx, %rsi
	jmp	.L817
	.cfi_endproc
.LFE21444:
	.size	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_, .-_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	.section	.text._ZN2v88internal4wasm11AsmJsParser9BareBeginENS2_9BlockKindEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser9BareBeginENS2_9BlockKindEi
	.type	_ZN2v88internal4wasm11AsmJsParser9BareBeginENS2_9BlockKindEi, @function
_ZN2v88internal4wasm11AsmJsParser9BareBeginENS2_9BlockKindEi:
.LFB18203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	600(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	%esi, -16(%rbp)
	movl	%edx, -12(%rbp)
	cmpq	608(%rdi), %r8
	je	.L858
	movl	%esi, (%r8)
	movl	%edx, 4(%r8)
	addq	$8, 600(%rdi)
.L857:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L862
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L858:
	.cfi_restore_state
	leaq	-16(%rbp), %rdx
	addq	$584, %rdi
	movq	%r8, %rsi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L857
.L862:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18203:
	.size	_ZN2v88internal4wasm11AsmJsParser9BareBeginENS2_9BlockKindEi, .-_ZN2v88internal4wasm11AsmJsParser9BareBeginENS2_9BlockKindEi
	.section	.text._ZN2v88internal4wasm11AsmJsParser5BeginEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser5BeginEi
	.type	_ZN2v88internal4wasm11AsmJsParser5BeginEi, @function
_ZN2v88internal4wasm11AsmJsParser5BeginEi:
.LFB18200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	600(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -32(%rbp)
	movl	%esi, -28(%rbp)
	cmpq	608(%rdi), %r8
	je	.L864
	movl	$0, (%r8)
	movl	%esi, 4(%r8)
	addq	$8, 600(%rdi)
.L865:
	movq	304(%rbx), %rdi
	movl	$64, %edx
	movl	$2, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L868
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L864:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	584(%rdi), %rdi
	movq	%r8, %rsi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L865
.L868:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18200:
	.size	_ZN2v88internal4wasm11AsmJsParser5BeginEi, .-_ZN2v88internal4wasm11AsmJsParser5BeginEi
	.section	.text._ZN2v88internal4wasm11AsmJsParser4LoopEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser4LoopEi
	.type	_ZN2v88internal4wasm11AsmJsParser4LoopEi, @function
_ZN2v88internal4wasm11AsmJsParser4LoopEi:
.LFB18201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	600(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$1, -32(%rbp)
	movl	%esi, -28(%rbp)
	cmpq	608(%rdi), %r8
	je	.L870
	movl	$1, (%r8)
	movl	%esi, 4(%r8)
	addq	$8, 600(%rdi)
.L871:
	movq	32(%rbx), %rsi
	movq	304(%rbx), %rdi
	movq	%rsi, %rdx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder16AddAsmWasmOffsetEmm@PLT
	movl	$64, %edx
	movl	$3, %esi
	movq	304(%rbx), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L874
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L870:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	leaq	584(%rdi), %rdi
	movq	%r8, %rsi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L871
.L874:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18201:
	.size	_ZN2v88internal4wasm11AsmJsParser4LoopEi, .-_ZN2v88internal4wasm11AsmJsParser4LoopEi
	.section	.text._ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.type	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, @function
_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_:
.LFB21501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r13
	cmpq	24(%rdi), %r13
	je	.L876
	movq	(%rsi), %rax
	movq	%rax, 0(%r13)
	addq	$8, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L876:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r13, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L901
	testq	%rax, %rax
	je	.L888
	leaq	(%rax,%rax), %rdx
	cmpq	%rdx, %rax
	jbe	.L902
	movl	$2147483640, %esi
	movl	$2147483640, %r15d
.L879:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L903
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L882:
	leaq	(%rax,%r15), %rdi
	leaq	8(%rax), %rdx
.L880:
	movq	(%r12), %rsi
	movq	%rsi, (%rax,%rcx)
	cmpq	%r14, %r13
	je	.L883
	leaq	-8(%r13), %rdx
	movq	%rdx, %rcx
	leaq	15(%rax), %rdx
	subq	%r14, %rcx
	subq	%r14, %rdx
	movq	%rcx, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rdx
	jbe	.L891
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rsi
	je	.L891
	leaq	1(%rsi), %r8
	xorl	%edx, %edx
	movq	%r8, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L885:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rsi, %rdx
	jne	.L885
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rsi
	addq	%rsi, %r14
	addq	%rax, %rsi
	cmpq	%rdx, %r8
	je	.L887
	movq	(%r14), %rdx
	movq	%rdx, (%rsi)
.L887:
	leaq	16(%rax,%rcx), %rdx
.L883:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rdi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L902:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L904
	movl	$8, %edx
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L888:
	movl	$8, %esi
	movl	$8, %r15d
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L891:
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L884:
	movq	(%r14,%rdx,8), %r8
	movq	%r8, (%rax,%rdx,8)
	movq	%rdx, %r8
	addq	$1, %rdx
	cmpq	%r8, %rsi
	jne	.L884
	jmp	.L887
.L903:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L882
.L901:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L904:
	movl	$268435455, %r15d
	cmpq	$268435455, %rdx
	cmova	%r15, %rdx
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rsi
	jmp	.L879
	.cfi_endproc
.LFE21501:
	.size	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_, .-_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	.section	.text._ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_:
.LFB21503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %r12
	cmpq	24(%rdi), %r12
	je	.L906
	movzbl	(%rsi), %eax
	movb	%al, (%r12)
	addq	$1, 16(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L906:
	.cfi_restore_state
	movq	8(%rdi), %r14
	movq	%r12, %r15
	subq	%r14, %r15
	cmpq	$2147483647, %r15
	je	.L929
	testq	%r15, %r15
	je	.L918
	leaq	(%r15,%r15), %rax
	movl	$2147483648, %esi
	movl	$2147483647, %ecx
	cmpq	%rax, %r15
	jbe	.L930
.L909:
	movq	(%rbx), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %rsi
	ja	.L931
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L911:
	leaq	(%rax,%rcx), %rsi
	movzbl	0(%r13), %ecx
	leaq	1(%rax), %rdx
	movb	%cl, (%rax,%r15)
	cmpq	%r14, %r12
	je	.L912
	leaq	15(%rax), %rdx
	subq	%r14, %rdx
	cmpq	$30, %rdx
	jbe	.L913
	leaq	-1(%r12), %rdx
	subq	%r14, %rdx
	cmpq	$14, %rdx
	jbe	.L913
	movq	%r12, %r8
	xorl	%edx, %edx
	subq	%r14, %r8
	movq	%r8, %rcx
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L914:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L914
	movq	%r8, %rdi
	andq	$-16, %rdi
	leaq	(%r14,%rdi), %rdx
	leaq	(%rax,%rdi), %rcx
	cmpq	%rdi, %r8
	je	.L917
	movzbl	(%rdx), %edi
	movb	%dil, (%rcx)
	leaq	1(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	1(%rdx), %edi
	movb	%dil, 1(%rcx)
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	2(%rdx), %edi
	movb	%dil, 2(%rcx)
	leaq	3(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	3(%rdx), %edi
	movb	%dil, 3(%rcx)
	leaq	4(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	4(%rdx), %edi
	movb	%dil, 4(%rcx)
	leaq	5(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	5(%rdx), %edi
	movb	%dil, 5(%rcx)
	leaq	6(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	6(%rdx), %edi
	movb	%dil, 6(%rcx)
	leaq	7(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	7(%rdx), %edi
	movb	%dil, 7(%rcx)
	leaq	8(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	8(%rdx), %edi
	movb	%dil, 8(%rcx)
	leaq	9(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	9(%rdx), %edi
	movb	%dil, 9(%rcx)
	leaq	10(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	10(%rdx), %edi
	movb	%dil, 10(%rcx)
	leaq	11(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	11(%rdx), %edi
	movb	%dil, 11(%rcx)
	leaq	12(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	12(%rdx), %edi
	movb	%dil, 12(%rcx)
	leaq	13(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	13(%rdx), %edi
	movb	%dil, 13(%rcx)
	leaq	14(%rdx), %rdi
	cmpq	%rdi, %r12
	je	.L917
	movzbl	14(%rdx), %edx
	movb	%dl, 14(%rcx)
	.p2align 4,,10
	.p2align 3
.L917:
	subq	%r14, %r12
	leaq	1(%rax,%r12), %rdx
.L912:
	movq	%rax, %xmm0
	movq	%rdx, %xmm2
	movq	%rsi, 24(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L918:
	.cfi_restore_state
	movl	$8, %esi
	movl	$1, %ecx
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L913:
	movq	%r12, %rdi
	xorl	%edx, %edx
	subq	%r14, %rdi
	.p2align 4,,10
	.p2align 3
.L916:
	movzbl	(%r14,%rdx), %ecx
	movb	%cl, (%rax,%rdx)
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L916
	jmp	.L917
.L931:
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	jmp	.L911
.L929:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L930:
	cmpq	%rsi, %rax
	cmovb	%rax, %rcx
	leaq	7(%rcx), %rsi
	andq	$-8, %rsi
	jmp	.L909
	.cfi_endproc
.LFE21503:
	.size	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser22ValidateFunctionLocalsEmPNS0_10ZoneVectorINS1_9ValueTypeEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC60:
	.string	"Expected local variable identifier"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser22ValidateFunctionLocalsEmPNS0_10ZoneVectorINS1_9ValueTypeEEE.str1.1,"aMS",@progbits,1
.LC61:
	.string	"Duplicate local variable name"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser22ValidateFunctionLocalsEmPNS0_10ZoneVectorINS1_9ValueTypeEEE.str1.8
	.align 8
.LC62:
	.string	"Expected variable initial value"
	.align 8
.LC63:
	.string	"Initializing from global requires const variable"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser22ValidateFunctionLocalsEmPNS0_10ZoneVectorINS1_9ValueTypeEEE.str1.1
.LC64:
	.string	"Bad local variable definition"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser22ValidateFunctionLocalsEmPNS0_10ZoneVectorINS1_9ValueTypeEEE.str1.8
	.align 8
.LC65:
	.string	"expected fround or const global"
	.section	.text._ZN2v88internal4wasm11AsmJsParser22ValidateFunctionLocalsEmPNS0_10ZoneVectorINS1_9ValueTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser22ValidateFunctionLocalsEmPNS0_10ZoneVectorINS1_9ValueTypeEEE
	.type	_ZN2v88internal4wasm11AsmJsParser22ValidateFunctionLocalsEmPNS0_10ZoneVectorINS1_9ValueTypeEEE, @function
_ZN2v88internal4wasm11AsmJsParser22ValidateFunctionLocalsEmPNS0_10ZoneVectorINS1_9ValueTypeEEE:
.LFB18219:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-9946, 16(%rdi)
	jne	.L932
	movq	%rdi, %r14
	movq	%rdx, %r13
	leaq	8(%rdi), %r12
.L934:
	movb	$1, 96(%r14)
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r14), %r15d
	movb	$0, 96(%r14)
	cmpl	$-9999, %r15d
	jge	.L997
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movl	32(%rax), %edx
	movq	%rax, %rbx
	testl	%edx, %edx
	jne	.L998
	cmpl	$61, 16(%r14)
	jne	.L996
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r14), %r15d
	cmpl	$45, %r15d
	je	.L939
	cmpl	$255, %r15d
	jg	.L999
	cmpl	$-4, %r15d
	je	.L1000
	cmpl	$-3, %r15d
	je	.L1001
.L972:
	leaq	.LC62(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
.L932:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1002
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L999:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	cmpl	$2, 32(%rax)
	movq	%rax, %r15
	jne	.L947
	cmpb	$0, 36(%rax)
	jne	.L1003
	movl	$1, 32(%rbx)
	movq	(%rax), %rax
	movl	$769, %esi
	movq	%rax, (%rbx)
	movq	16(%r13), %rax
	subq	8(%r13), %rax
	addl	-72(%rbp), %eax
	movl	%eax, 28(%rbx)
	movq	(%r15), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1004
	movq	(%r15), %rdi
	movl	$57357, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L951
	leaq	-57(%rbp), %rsi
	movq	%r13, %rdi
	movb	$3, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
.L950:
	movq	304(%r14), %rdi
	movl	28(%r15), %edx
	movl	$35, %esi
	addl	752(%r14), %edx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithI32VENS1_10WasmOpcodeEi@PLT
	movl	28(%rbx), %esi
	movq	304(%r14), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj@PLT
.L943:
	movl	16(%r14), %eax
	cmpl	$44, %eax
	je	.L934
	cmpl	$59, %eax
	je	.L975
	cmpl	$125, %eax
	je	.L932
	cmpb	$0, 292(%r14)
	jne	.L977
	movq	32(%r14), %rdx
	leaq	.LC2(%rip), %rcx
	movb	$1, 540(%r14)
	movq	%rcx, 544(%r14)
	movl	%edx, 552(%r14)
.L977:
	cmpl	$-9946, %eax
	je	.L934
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L939:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r14), %eax
	cmpl	$-4, %eax
	je	.L1005
	cmpl	$-3, %eax
	jne	.L972
	movl	288(%r14), %r15d
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	testl	%r15d, %r15d
	js	.L967
	movl	$1, 32(%rbx)
	leaq	-57(%rbp), %rsi
	movq	%r13, %rdi
	movq	$769, (%rbx)
	movq	16(%r13), %rax
	subq	8(%r13), %rax
	addl	-72(%rbp), %eax
	movl	%eax, 28(%rbx)
	movb	$1, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movq	304(%r14), %rdi
	movl	%r15d, %esi
	negl	%esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movl	28(%rbx), %esi
	movq	304(%r14), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj@PLT
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L947:
	movq	672(%r14), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	movl	%eax, %r15d
	testb	%al, %al
	je	.L953
	cmpl	$40, 16(%r14)
	jne	.L996
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r14), %eax
	cmpl	$45, %eax
	je	.L1006
	xorl	%r15d, %r15d
.L955:
	cmpl	$-4, %eax
	je	.L1007
	cmpl	$-3, %eax
	jne	.L972
	movl	288(%r14), %eax
	movq	%r12, %rdi
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	js	.L967
	movl	$1, 32(%rbx)
	leaq	-57(%rbp), %rsi
	movq	%r13, %rdi
	movq	$57357, (%rbx)
	movq	16(%r13), %rax
	subq	8(%r13), %rax
	addl	-72(%rbp), %eax
	movl	%eax, 28(%rbx)
	movb	$3, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movl	-80(%rbp), %ecx
	pxor	%xmm0, %xmm0
	movq	304(%r14), %rdi
	movl	%ecx, %eax
	negl	%eax
	testb	%r15b, %r15b
	cmove	%ecx, %eax
	cvtsi2ssl	%eax, %xmm0
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF32ConstEf@PLT
	movl	28(%rbx), %esi
	movq	304(%r14), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj@PLT
.L966:
	cmpl	$41, 16(%r14)
	jne	.L996
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L1004:
	leaq	-57(%rbp), %rsi
	movq	%r13, %rdi
	movb	$1, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1000:
	movsd	280(%r14), %xmm0
	movq	%r12, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	$1, 32(%rbx)
	leaq	-57(%rbp), %rsi
	movq	%r13, %rdi
	movq	$237, (%rbx)
	movq	16(%r13), %rax
	subq	8(%r13), %rax
	addl	-72(%rbp), %eax
	movl	%eax, 28(%rbx)
	movb	$4, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movq	304(%r14), %rdi
	movsd	-80(%rbp), %xmm0
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF64ConstEd@PLT
	movl	28(%rbx), %esi
	movq	304(%r14), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj@PLT
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	%r12, %rdi
	movl	288(%r14), %r15d
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	$1, 32(%rbx)
	leaq	-57(%rbp), %rsi
	movq	%r13, %rdi
	movq	$769, (%rbx)
	movq	16(%r13), %rax
	subq	8(%r13), %rax
	addl	-72(%rbp), %eax
	movl	%eax, 28(%rbx)
	movb	$1, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movq	304(%r14), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movl	28(%rbx), %esi
	movq	304(%r14), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj@PLT
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L1005:
	movsd	280(%r14), %xmm0
	movq	%r12, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	$1, 32(%rbx)
	leaq	-57(%rbp), %rsi
	movq	%r13, %rdi
	movq	$237, (%rbx)
	movq	16(%r13), %rax
	subq	8(%r13), %rax
	addl	-72(%rbp), %eax
	movl	%eax, 28(%rbx)
	movb	$4, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	movsd	-80(%rbp), %xmm0
	movq	304(%r14), %rdi
	xorpd	.LC49(%rip), %xmm0
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF64ConstEd@PLT
	movl	28(%rbx), %esi
	movq	304(%r14), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj@PLT
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L951:
	movq	(%r15), %rdi
	movl	$237, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L952
	leaq	-57(%rbp), %rsi
	movq	%r13, %rdi
	movb	$4, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	jmp	.L950
.L953:
	leaq	.LC65(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r14), %eax
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L1007:
	movsd	280(%r14), %xmm0
	movq	%r12, %rdi
	movsd	%xmm0, -80(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	$1, 32(%rbx)
	leaq	-57(%rbp), %rsi
	movq	%r13, %rdi
	movq	$57357, (%rbx)
	movq	16(%r13), %rax
	subq	8(%r13), %rax
	addl	-72(%rbp), %eax
	movl	%eax, 28(%rbx)
	movb	$3, -57(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm9ValueTypeENS1_13ZoneAllocatorIS3_EEE12emplace_backIJS3_EEEvDpOT_
	testb	%r15b, %r15b
	movsd	-80(%rbp), %xmm0
	je	.L958
	xorpd	.LC49(%rip), %xmm0
.L958:
	comisd	.LC50(%rip), %xmm0
	jbe	.L990
	movsd	.LC51(%rip), %xmm2
	movss	.LC42(%rip), %xmm1
	comisd	%xmm0, %xmm2
	jnb	.L963
	movss	.LC43(%rip), %xmm1
.L963:
	movq	304(%r14), %rdi
	movaps	%xmm1, %xmm0
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF32ConstEf@PLT
	movl	28(%rbx), %esi
	movq	304(%r14), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj@PLT
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L996:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L997:
	leaq	.LC60(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L998:
	leaq	.LC61(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L990:
	movsd	.LC52(%rip), %xmm3
	comisd	%xmm0, %xmm3
	jbe	.L991
	comisd	.LC53(%rip), %xmm0
	movss	.LC44(%rip), %xmm1
	jnb	.L963
	movss	.LC45(%rip), %xmm1
	jmp	.L963
.L991:
	pxor	%xmm1, %xmm1
	cvtsd2ss	%xmm0, %xmm1
	jmp	.L963
.L1003:
	leaq	.LC63(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L932
.L975:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%r14), %eax
	jmp	.L977
.L967:
	leaq	.LC58(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L932
.L952:
	leaq	.LC64(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L932
.L1002:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18219:
	.size	_ZN2v88internal4wasm11AsmJsParser22ValidateFunctionLocalsEmPNS0_10ZoneVectorINS1_9ValueTypeEEE, .-_ZN2v88internal4wasm11AsmJsParser22ValidateFunctionLocalsEmPNS0_10ZoneVectorINS1_9ValueTypeEEE
	.section	.text._ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm,"axG",@progbits,_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm
	.type	_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm, @function
_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm:
.LFB22218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movq	%r8, %rcx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1032
.L1009:
	movq	%r14, 40(%r12)
	movq	8(%rbx), %rax
	leaq	0(,%r15,8), %rcx
	movq	(%rax,%r15,8), %rax
	testq	%rax, %rax
	je	.L1018
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	(%rax,%r15,8), %rax
	movq	%r12, (%rax)
.L1019:
	addq	$1, 32(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1032:
	.cfi_restore_state
	movq	%rdx, %r13
	cmpq	$1, %rdx
	je	.L1033
	movq	(%rbx), %r8
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rsi
	movq	16(%r8), %rdi
	movq	24(%r8), %rax
	subq	%rdi, %rax
	cmpq	%rax, %rdx
	ja	.L1034
	addq	%rdi, %rsi
	movq	%rsi, 16(%r8)
.L1013:
	xorl	%esi, %esi
	call	memset@PLT
	movq	%rax, %rdi
.L1011:
	movq	24(%rbx), %rsi
	movq	$0, 24(%rbx)
	testq	%rsi, %rsi
	je	.L1014
	xorl	%r9d, %r9d
	leaq	24(%rbx), %r10
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	(%r8), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1017:
	testq	%rsi, %rsi
	je	.L1014
.L1015:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	40(%rcx), %rax
	divq	%r13
	leaq	(%rdi,%rdx,8), %rax
	movq	(%rax), %r8
	testq	%r8, %r8
	jne	.L1016
	movq	24(%rbx), %r8
	movq	%r8, (%rcx)
	movq	%rcx, 24(%rbx)
	movq	%r10, (%rax)
	cmpq	$0, (%rcx)
	je	.L1021
	movq	%rcx, (%rdi,%r9,8)
	movq	%rdx, %r9
	testq	%rsi, %rsi
	jne	.L1015
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	%r13, 16(%rbx)
	divq	%r13
	movq	%rdi, 8(%rbx)
	movq	%rdx, %r15
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	24(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 24(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L1020
	movq	40(%rax), %rax
	xorl	%edx, %edx
	divq	16(%rbx)
	movq	8(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
.L1020:
	movq	8(%rbx), %rax
	leaq	24(%rbx), %rdx
	movq	%rdx, (%rax,%rcx)
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	%rdx, %r9
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1033:
	movq	$0, 56(%rbx)
	leaq	56(%rbx), %rdi
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	%r8, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rdx
	movq	%rax, %rdi
	jmp	.L1013
	.cfi_endproc
.LFE22218:
	.size	_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm, .-_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm
	.section	.text._ZNSt6vectorIN2v88internal10ZoneVectorIPNS1_4wasm7AsmTypeEEENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal10ZoneVectorIPNS1_4wasm7AsmTypeEEENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal10ZoneVectorIPNS1_4wasm7AsmTypeEEENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal10ZoneVectorIPNS1_4wasm7AsmTypeEEENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_, @function
_ZNSt6vectorIN2v88internal10ZoneVectorIPNS1_4wasm7AsmTypeEEENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_:
.LFB22458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r15
	movq	%r13, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	$67108863, %rax
	je	.L1051
	movq	%rdx, %r14
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L1045
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1052
	movl	$2147483616, %esi
	movl	$2147483616, %ecx
.L1037:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L1053
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1040:
	leaq	(%rax,%rcx), %rdi
	leaq	32(%rax), %r8
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1052:
	testq	%rcx, %rcx
	jne	.L1054
	movl	$32, %r8d
	xorl	%edi, %edi
	xorl	%eax, %eax
.L1038:
	movq	(%r14), %rcx
	addq	%rax, %rdx
	pxor	%xmm0, %xmm0
	movq	%rcx, (%rdx)
	movdqu	8(%r14), %xmm3
	movq	24(%r14), %rcx
	movups	%xmm0, 8(%r14)
	movq	$0, 24(%r14)
	movq	%rcx, 24(%rdx)
	movups	%xmm3, 8(%rdx)
	cmpq	%r15, %rbx
	je	.L1041
	movq	%r15, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -32(%rcx)
	movdqu	-24(%rdx), %xmm1
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	movq	$0, -8(%rdx)
	movups	%xmm0, -24(%rdx)
	cmpq	%rdx, %rbx
	jne	.L1042
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%rax,%rdx), %r8
.L1041:
	cmpq	%r13, %rbx
	je	.L1043
	movq	%rbx, %rdx
	movq	%r8, %rcx
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -32(%rcx)
	movdqu	-24(%rdx), %xmm2
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	movq	$0, -8(%rdx)
	movups	%xmm0, -24(%rdx)
	cmpq	%rdx, %r13
	jne	.L1044
	subq	%rbx, %r13
	addq	%r13, %r8
.L1043:
	movq	%rax, %xmm0
	movq	%r8, %xmm4
	movq	%rdi, 24(%r12)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1045:
	.cfi_restore_state
	movl	$32, %esi
	movl	$32, %ecx
	jmp	.L1037
.L1053:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L1040
.L1054:
	cmpq	$67108863, %rcx
	movl	$67108863, %eax
	cmova	%rax, %rcx
	salq	$5, %rcx
	movq	%rcx, %rsi
	jmp	.L1037
.L1051:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22458:
	.size	_ZNSt6vectorIN2v88internal10ZoneVectorIPNS1_4wasm7AsmTypeEEENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_, .-_ZNSt6vectorIN2v88internal10ZoneVectorIPNS1_4wasm7AsmTypeEEENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal10ZoneVectorINS1_4wasm9ValueTypeEEENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal10ZoneVectorINS1_4wasm9ValueTypeEEENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal10ZoneVectorINS1_4wasm9ValueTypeEEENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal10ZoneVectorINS1_4wasm9ValueTypeEEENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, @function
_ZNSt6vectorIN2v88internal10ZoneVectorINS1_4wasm9ValueTypeEEENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_:
.LFB22477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r15
	movq	%r13, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	$67108863, %rax
	je	.L1071
	movq	%rdx, %r14
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L1065
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1072
	movl	$2147483616, %esi
	movl	$2147483616, %ecx
.L1057:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L1073
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1060:
	leaq	(%rax,%rcx), %rdi
	leaq	32(%rax), %r8
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1072:
	testq	%rcx, %rcx
	jne	.L1074
	movl	$32, %r8d
	xorl	%edi, %edi
	xorl	%eax, %eax
.L1058:
	movq	(%r14), %rcx
	addq	%rax, %rdx
	pxor	%xmm0, %xmm0
	movq	%rcx, (%rdx)
	movdqu	8(%r14), %xmm3
	movq	24(%r14), %rcx
	movups	%xmm0, 8(%r14)
	movq	$0, 24(%r14)
	movq	%rcx, 24(%rdx)
	movups	%xmm3, 8(%rdx)
	cmpq	%r15, %rbx
	je	.L1061
	movq	%r15, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -32(%rcx)
	movdqu	-24(%rdx), %xmm1
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	movq	$0, -8(%rdx)
	movups	%xmm0, -24(%rdx)
	cmpq	%rdx, %rbx
	jne	.L1062
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%rax,%rdx), %r8
.L1061:
	cmpq	%r13, %rbx
	je	.L1063
	movq	%rbx, %rdx
	movq	%r8, %rcx
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1064:
	movq	(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -32(%rcx)
	movdqu	-24(%rdx), %xmm2
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	movq	$0, -8(%rdx)
	movups	%xmm0, -24(%rdx)
	cmpq	%rdx, %r13
	jne	.L1064
	subq	%rbx, %r13
	addq	%r13, %r8
.L1063:
	movq	%rax, %xmm0
	movq	%r8, %xmm4
	movq	%rdi, 24(%r12)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1065:
	.cfi_restore_state
	movl	$32, %esi
	movl	$32, %ecx
	jmp	.L1057
.L1073:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L1060
.L1074:
	cmpq	$67108863, %rcx
	movl	$67108863, %eax
	cmova	%rax, %rcx
	salq	$5, %rcx
	movq	%rcx, %rsi
	jmp	.L1057
.L1071:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22477:
	.size	_ZNSt6vectorIN2v88internal10ZoneVectorINS1_4wasm9ValueTypeEEENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_, .-_ZNSt6vectorIN2v88internal10ZoneVectorINS1_4wasm9ValueTypeEEENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal10ZoneVectorIiEENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal10ZoneVectorIiEENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal10ZoneVectorIiEENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal10ZoneVectorIiEENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal10ZoneVectorIiEENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_:
.LFB22494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r13
	movq	8(%rdi), %r15
	movq	%r13, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	$67108863, %rax
	je	.L1091
	movq	%rdx, %r14
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %rbx
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L1085
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1092
	movl	$2147483616, %esi
	movl	$2147483616, %ecx
.L1077:
	movq	(%r12), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L1093
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1080:
	leaq	(%rax,%rcx), %rdi
	leaq	32(%rax), %r8
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1092:
	testq	%rcx, %rcx
	jne	.L1094
	movl	$32, %r8d
	xorl	%edi, %edi
	xorl	%eax, %eax
.L1078:
	movq	(%r14), %rcx
	addq	%rax, %rdx
	pxor	%xmm0, %xmm0
	movq	%rcx, (%rdx)
	movdqu	8(%r14), %xmm3
	movq	24(%r14), %rcx
	movups	%xmm0, 8(%r14)
	movq	$0, 24(%r14)
	movq	%rcx, 24(%rdx)
	movups	%xmm3, 8(%rdx)
	cmpq	%r15, %rbx
	je	.L1081
	movq	%r15, %rdx
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -32(%rcx)
	movdqu	-24(%rdx), %xmm1
	movups	%xmm1, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	movq	$0, -8(%rdx)
	movups	%xmm0, -24(%rdx)
	cmpq	%rdx, %rbx
	jne	.L1082
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%rax,%rdx), %r8
.L1081:
	cmpq	%r13, %rbx
	je	.L1083
	movq	%rbx, %rdx
	movq	%r8, %rcx
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1084:
	movq	(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -32(%rcx)
	movdqu	-24(%rdx), %xmm2
	movups	%xmm2, -24(%rcx)
	movq	-8(%rdx), %rsi
	movq	%rsi, -8(%rcx)
	movq	$0, -8(%rdx)
	movups	%xmm0, -24(%rdx)
	cmpq	%rdx, %r13
	jne	.L1084
	subq	%rbx, %r13
	addq	%r13, %r8
.L1083:
	movq	%rax, %xmm0
	movq	%r8, %xmm4
	movq	%rdi, 24(%r12)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1085:
	.cfi_restore_state
	movl	$32, %esi
	movl	$32, %ecx
	jmp	.L1077
.L1093:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L1080
.L1094:
	cmpq	$67108863, %rcx
	movl	$67108863, %eax
	cmova	%rax, %rcx
	salq	$5, %rcx
	movq	%rcx, %rsi
	jmp	.L1077
.L1091:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE22494:
	.size	_ZNSt6vectorIN2v88internal10ZoneVectorIiEENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal10ZoneVectorIiEENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser22ValidateFunctionParamsEPNS0_10ZoneVectorIPNS1_7AsmTypeEEE.str1.1,"aMS",@progbits,1
.LC66:
	.string	"Expected parameter name"
.LC67:
	.string	"Duplicate parameter name"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser22ValidateFunctionParamsEPNS0_10ZoneVectorIPNS1_7AsmTypeEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC68:
	.string	"Bad integer parameter annotation."
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser22ValidateFunctionParamsEPNS0_10ZoneVectorIPNS1_7AsmTypeEEE.str1.1
.LC69:
	.string	"Expected fround"
	.section	.text._ZN2v88internal4wasm11AsmJsParser22ValidateFunctionParamsEPNS0_10ZoneVectorIPNS1_7AsmTypeEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser22ValidateFunctionParamsEPNS0_10ZoneVectorIPNS1_7AsmTypeEEE
	.type	_ZN2v88internal4wasm11AsmJsParser22ValidateFunctionParamsEPNS0_10ZoneVectorIPNS1_7AsmTypeEEE, @function
_ZN2v88internal4wasm11AsmJsParser22ValidateFunctionParamsEPNS0_10ZoneVectorIPNS1_7AsmTypeEEE:
.LFB18218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$40, 16(%rdi)
	movb	$1, 96(%rdi)
	je	.L1096
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rdi)
	movq	%rax, 544(%rdi)
	movq	32(%rdi), %rax
	movl	%eax, 552(%rdi)
.L1095:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1184
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1096:
	.cfi_restore_state
	leaq	8(%rdi), %rbx
	movq	%rsi, %r12
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	464(%r13), %rax
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	%rax, -96(%rbp)
	leaq	464(%r13), %rax
	movq	%rax, -64(%rbp)
	movq	480(%r13), %rax
	movq	$0, -72(%rbp)
	cmpq	472(%r13), %rax
	je	.L1120
	movdqu	-24(%rax), %xmm0
	movq	$0, -24(%rax)
	movq	-80(%rbp), %rsi
	movq	-8(%rax), %rcx
	movq	%rsi, -16(%rax)
	movq	-72(%rbp), %rsi
	movq	%rsi, -8(%rax)
	movq	%xmm0, %rax
	subq	$32, 480(%r13)
	movq	%rcx, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	cmpq	-80(%rbp), %rax
	je	.L1120
	movq	%xmm0, -80(%rbp)
.L1120:
	cmpb	$0, 540(%r13)
	movl	16(%r13), %r14d
	jne	.L1101
	cmpl	$41, %r14d
	je	.L1102
	cmpl	$-9999, %r14d
	jge	.L1185
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	-80(%rbp), %r15
	cmpq	-72(%rbp), %r15
	je	.L1107
	movl	%r14d, (%r15)
	movl	16(%r13), %eax
	addq	$4, -80(%rbp)
	cmpl	$41, %eax
	jne	.L1186
.L1102:
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$123, 16(%r13)
	movb	$0, 96(%r13)
	je	.L1121
.L1181:
	movb	$1, 540(%r13)
.L1182:
	leaq	.LC0(%rip), %rax
	movq	%rax, 544(%r13)
	movq	32(%r13), %rax
	movl	%eax, 552(%r13)
.L1106:
	movq	-64(%rbp), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L1148
	movq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-72(%rbp), %rax
	movq	%rax, 24(%rsi)
	movaps	%xmm0, -80(%rbp)
	addq	$32, 16(%rdi)
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	-88(%rbp), %r8
	movq	%r15, %r10
	subq	%r8, %r10
	movq	%r10, %rax
	sarq	$2, %rax
	cmpq	$536870911, %rax
	je	.L1187
	testq	%rax, %rax
	je	.L1150
	leaq	(%rax,%rax), %rdi
	movl	$2147483648, %esi
	movl	$2147483644, %r9d
	cmpq	%rdi, %rax
	jbe	.L1188
.L1110:
	movq	-96(%rbp), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r11
	subq	%rax, %r11
	cmpq	%rsi, %r11
	jb	.L1189
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L1113:
	addq	%rax, %r9
	leaq	4(%rax), %rsi
.L1111:
	movl	%r14d, (%rax,%r10)
	cmpq	%r8, %r15
	je	.L1114
	leaq	-4(%r15), %r10
	leaq	15(%rax), %rcx
	subq	%r8, %r10
	subq	%r8, %rcx
	movq	%r10, %rsi
	shrq	$2, %rsi
	cmpq	$30, %rcx
	jbe	.L1153
	movabsq	$4611686018427387900, %rdx
	testq	%rdx, %rsi
	je	.L1153
	addq	$1, %rsi
	xorl	%ecx, %ecx
	movq	%rsi, %rdi
	shrq	$2, %rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L1116:
	movdqu	(%r8,%rcx), %xmm1
	movups	%xmm1, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rdi
	jne	.L1116
	movq	%rsi, %rdi
	andq	$-4, %rdi
	leaq	0(,%rdi,4), %rcx
	addq	%rcx, %r8
	addq	%rax, %rcx
	cmpq	%rsi, %rdi
	je	.L1118
	movl	(%r8), %esi
	movl	%esi, (%rcx)
	leaq	4(%r8), %rsi
	cmpq	%rsi, %r15
	je	.L1118
	movl	4(%r8), %esi
	movl	%esi, 4(%rcx)
	leaq	8(%r8), %rsi
	cmpq	%rsi, %r15
	je	.L1118
	movl	8(%r8), %esi
	movl	%esi, 8(%rcx)
.L1118:
	leaq	8(%rax,%r10), %rsi
.L1114:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movl	16(%r13), %eax
	movq	%r9, -72(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -88(%rbp)
	cmpl	$41, %eax
	je	.L1102
	.p2align 4,,10
	.p2align 3
.L1186:
	cmpl	$44, %eax
	jne	.L1181
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1188:
	testq	%rdi, %rdi
	jne	.L1190
	movl	$4, %esi
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1150:
	movl	$8, %esi
	movl	$4, %r9d
	jmp	.L1110
.L1101:
	cmpl	$41, %r14d
	jne	.L1182
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1153:
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L1115:
	movl	(%r8,%rcx,4), %edi
	movl	%edi, (%rax,%rcx,4)
	movq	%rcx, %rdi
	addq	$1, %rcx
	cmpq	%rsi, %rdi
	jne	.L1115
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1185:
	leaq	.LC66(%rip), %rax
	movb	$1, 540(%r13)
	movq	%rax, 544(%r13)
	movq	32(%r13), %rax
	movl	%eax, 552(%r13)
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1148:
	leaq	-96(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal10ZoneVectorIiEENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L1095
.L1121:
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %r15
	movq	%rax, -136(%rbp)
	cmpq	%r15, %rax
	je	.L1106
	movq	%r15, -120(%rbp)
.L1146:
	movq	-120(%rbp), %rax
	movl	(%rax), %r14d
	cmpl	16(%r13), %r14d
	jne	.L1181
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$61, 16(%r13)
	jne	.L1181
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movl	32(%rax), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	jne	.L1191
	movl	16(%r13), %esi
	cmpl	%r14d, %esi
	je	.L1192
	cmpl	$43, %esi
	je	.L1193
	cmpl	$255, %esi
	jle	.L1137
	movq	%rbx, %rdi
	movl	%esi, -128(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	-128(%rbp), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movq	672(%r13), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1137
	cmpl	$40, 16(%r13)
	jne	.L1181
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	16(%r13), %r14d
	jne	.L1181
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$41, 16(%r13)
	jne	.L1181
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	$1, 32(%r15)
	leaq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	$57357, (%r15)
	movq	16(%r12), %rax
	subq	8(%r12), %rax
	sarq	$3, %rax
	movl	%eax, 28(%r15)
	movq	$57357, -104(%rbp)
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
.L1134:
	movl	16(%r13), %eax
	cmpl	$59, %eax
	je	.L1143
	cmpl	$125, %eax
	je	.L1145
	cmpb	$0, 292(%r13)
	jne	.L1145
	leaq	.LC2(%rip), %rax
	movb	$1, 540(%r13)
	movq	%rax, 544(%r13)
	movq	32(%r13), %rax
	movl	%eax, 552(%r13)
	.p2align 4,,10
	.p2align 3
.L1145:
	addq	$4, -120(%rbp)
	movq	-120(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L1146
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1189:
	movq	%r10, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%r9, -120(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %r8
	movq	-136(%rbp), %r10
	jmp	.L1113
.L1192:
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$124, 16(%r13)
	jne	.L1181
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$-3, 16(%r13)
	jne	.L1133
	movl	288(%r13), %eax
	testl	%eax, %eax
	je	.L1194
.L1133:
	leaq	.LC68(%rip), %rax
	movb	$1, 540(%r13)
	movq	%rax, 544(%r13)
	movq	32(%r13), %rax
	movl	%eax, 552(%r13)
	jmp	.L1106
.L1193:
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	16(%r13), %r14d
	jne	.L1181
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	$1, 32(%r15)
	leaq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	$237, (%r15)
	movq	16(%r12), %rax
	subq	8(%r12), %rax
	sarq	$3, %rax
	movl	%eax, 28(%r15)
	movq	$237, -104(%rbp)
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	jmp	.L1134
.L1143:
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	jmp	.L1145
.L1194:
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	$1, 32(%r15)
	leaq	-104(%rbp), %rsi
	movq	%r12, %rdi
	movq	$769, (%r15)
	movq	16(%r12), %rax
	subq	8(%r12), %rax
	sarq	$3, %rax
	movl	%eax, 28(%r15)
	movq	$769, -104(%rbp)
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	jmp	.L1134
.L1137:
	leaq	.LC69(%rip), %rax
	movb	$1, 540(%r13)
	movq	%rax, 544(%r13)
	movq	32(%r13), %rax
	movl	%eax, 552(%r13)
	jmp	.L1106
.L1191:
	leaq	.LC67(%rip), %rax
	movb	$1, 540(%r13)
	movq	%rax, 544(%r13)
	movq	32(%r13), %rax
	movl	%eax, 552(%r13)
	jmp	.L1106
.L1187:
	leaq	.LC24(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1184:
	call	__stack_chk_fail@PLT
.L1190:
	cmpq	$536870911, %rdi
	movl	$536870911, %r9d
	cmovbe	%rdi, %r9
	salq	$2, %r9
	leaq	7(%r9), %rsi
	andq	$-8, %rsi
	jmp	.L1110
	.cfi_endproc
.LFE18218:
	.size	_ZN2v88internal4wasm11AsmJsParser22ValidateFunctionParamsEPNS0_10ZoneVectorIPNS1_7AsmTypeEEE, .-_ZN2v88internal4wasm11AsmJsParser22ValidateFunctionParamsEPNS0_10ZoneVectorIPNS1_7AsmTypeEEE
	.section	.text._ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m,"axG",@progbits,_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m
	.type	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m, @function
_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m:
.LFB22508:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	(%rax,%rsi,8), %r10
	testq	%r10, %r10
	je	.L1206
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	(%r10), %rax
	movq	40(%rax), %r8
	cmpq	%rcx, %r8
	je	.L1209
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	(%rax), %r9
	testq	%r9, %r9
	je	.L1200
	movq	40(%r9), %r8
	movq	%rax, %r10
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	16(%rdi)
	cmpq	%rdx, %rsi
	jne	.L1200
	movq	%r9, %rax
	cmpq	%rcx, %r8
	jne	.L1197
.L1209:
	leaq	8(%rax), %rdx
	cmpq	%rdx, %r11
	je	.L1195
	movq	8(%r11), %r9
	cmpq	16(%rax), %r9
	jne	.L1197
	movq	(%r11), %rbx
	cmpq	8(%rax), %rbx
	jne	.L1197
	movq	16(%r11), %rdx
	addq	%rbx, %r9
	movq	24(%rax), %r8
	addq	%rdx, %r9
	cmpq	%r9, %rdx
	je	.L1195
	.p2align 4,,10
	.p2align 3
.L1198:
	movzbl	(%r8), %ebx
	cmpb	%bl, (%rdx)
	jne	.L1197
	addq	$1, %rdx
	addq	$1, %r8
	cmpq	%rdx, %r9
	jne	.L1198
.L1195:
	movq	%r10, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1200:
	.cfi_restore_state
	xorl	%r10d, %r10d
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	%r10, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1206:
	.cfi_restore 3
	.cfi_restore 6
	movq	%r10, %rax
	ret
	.cfi_endproc
.LFE22508:
	.size	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m, .-_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m
	.section	.text._ZNSt8__detail9_Map_baseIN2v88internal9SignatureINS2_4wasm9ValueTypeEEESt4pairIKS6_jENS2_13ZoneAllocatorIS9_EENS_10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,"axG",@progbits,_ZNSt8__detail9_Map_baseIN2v88internal9SignatureINS2_4wasm9ValueTypeEEESt4pairIKS6_jENS2_13ZoneAllocatorIS9_EENS_10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIN2v88internal9SignatureINS2_4wasm9ValueTypeEEESt4pairIKS6_jENS2_13ZoneAllocatorIS9_EENS_10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.type	_ZNSt8__detail9_Map_baseIN2v88internal9SignatureINS2_4wasm9ValueTypeEEESt4pairIKS6_jENS2_13ZoneAllocatorIS9_EENS_10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, @function
_ZNSt8__detail9_Map_baseIN2v88internal9SignatureINS2_4wasm9ValueTypeEEESt4pairIKS6_jENS2_13ZoneAllocatorIS9_EENS_10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_:
.LFB21510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rdi
	movq	(%rsi), %rsi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	16(%rbx), %r13
	movq	8(%rbx), %r15
	movq	%rax, %r12
	addq	%r13, %r15
	addq	(%rbx), %r15
	cmpq	%r15, %r13
	je	.L1211
	.p2align 4,,10
	.p2align 3
.L1212:
	movq	%r12, %rdi
	addq	$1, %r13
	call	_ZN2v84base10hash_valueEm@PLT
	movzbl	-1(%r13), %esi
	xorl	%edi, %edi
	movq	%rax, %r12
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %r12
	cmpq	%r13, %r15
	jne	.L1212
.L1211:
	xorl	%edx, %edx
	movq	%r12, %rax
	movq	%r12, %rcx
	movq	%r14, %rdi
	divq	16(%r14)
	movq	%rdx, %r13
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m
	testq	%rax, %rax
	je	.L1213
	movq	(%rax), %rdx
	leaq	32(%rdx), %rax
	testq	%rdx, %rdx
	je	.L1213
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1213:
	.cfi_restore_state
	movq	(%r14), %rdi
	movq	16(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	cmpq	$47, %rax
	jbe	.L1223
	leaq	48(%rcx), %rax
	movq	%rax, 16(%rdi)
.L1216:
	movq	$0, (%rcx)
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	16(%rbx), %rax
	movdqu	(%rbx), %xmm0
	movl	$0, 32(%rcx)
	movl	$1, %r8d
	movq	%rax, 24(%rcx)
	movups	%xmm0, 8(%rcx)
	call	_ZNSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS8_Lb1EEEm
	addq	$8, %rsp
	popq	%rbx
	addq	$32, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1223:
	.cfi_restore_state
	movl	$48, %esi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %rcx
	jmp	.L1216
	.cfi_endproc
.LFE21510:
	.size	_ZNSt8__detail9_Map_baseIN2v88internal9SignatureINS2_4wasm9ValueTypeEEESt4pairIKS6_jENS2_13ZoneAllocatorIS9_EENS_10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, .-_ZNSt8__detail9_Map_baseIN2v88internal9SignatureINS2_4wasm9ValueTypeEEESt4pairIKS6_jENS2_13ZoneAllocatorIS9_EENS_10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv.str1.1,"aMS",@progbits,1
.LC70:
	.string	"Expected intish index"
.LC71:
	.string	"Expected mask literal"
.LC72:
	.string	"Expected power of 2 mask"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv.str1.8,"aMS",@progbits,1
	.align 8
.LC73:
	.string	"Exceeded maximum function table size"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv.str1.1
.LC74:
	.string	"Expected call table"
.LC75:
	.string	"Mask size mismatch"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv.str1.8
	.align 8
.LC76:
	.string	"Expected function as call target"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv.str1.1
.LC77:
	.string	"Bad function argument type"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv.str1.8
	.align 8
.LC78:
	.string	"Imported function args must be type extern"
	.align 8
.LC79:
	.string	"Imported function can't be called as float"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv.str1.1
.LC80:
	.string	"Expected callable function"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv.str1.8
	.align 8
.LC81:
	.string	"Function use doesn't match definition"
	.section	.text.unlikely._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv,"ax",@progbits
	.align 2
.LCOLDB82:
	.section	.text._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv,"ax",@progbits
.LHOTB82:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser12ValidateCallEv
	.type	_ZN2v88internal4wasm11AsmJsParser12ValidateCallEv, @function
_ZN2v88internal4wasm11AsmJsParser12ValidateCallEv:
.LFB18254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	680(%rdi), %r13
	movl	16(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	$0, 680(%rdi)
	movq	%rax, -176(%rbp)
	movq	688(%rdi), %rax
	movq	%rax, -200(%rbp)
	movq	704(%rdi), %rax
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$91, 16(%rbx)
	je	.L1225
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movq	%rax, %r15
	movl	32(%rax), %eax
	movl	%eax, -168(%rbp)
	testl	%eax, %eax
	je	.L1404
	cmpl	$4, %eax
	setne	%dl
	cmpl	$5, %eax
	setle	%al
	andb	%al, %dl
	movb	%dl, -177(%rbp)
	jne	.L1405
	movq	-176(%rbp), %rax
	xorl	%r15d, %r15d
	movl	$1, -216(%rbp)
	movl	$0, -168(%rbp)
	movq	%rax, -208(%rbp)
.L1242:
	movq	432(%rbx), %rcx
	leaq	432(%rbx), %rdx
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	448(%rbx), %rax
	movq	%rcx, -144(%rbp)
	movq	$0, -120(%rbp)
	movq	%rdx, -112(%rbp)
	cmpq	440(%rbx), %rax
	je	.L1243
	movdqu	-24(%rax), %xmm0
	movq	$0, -24(%rax)
	movq	-128(%rbp), %rsi
	movq	-8(%rax), %rcx
	movq	%rsi, -16(%rax)
	movq	-120(%rbp), %rsi
	movq	%rsi, -8(%rax)
	movq	%xmm0, %rax
	subq	$32, 448(%rbx)
	movq	%rcx, -120(%rbp)
	movups	%xmm0, -136(%rbp)
	cmpq	-128(%rbp), %rax
	je	.L1401
	movq	%xmm0, -128(%rbp)
.L1401:
	movq	432(%rbx), %rsi
	movq	448(%rbx), %rax
	movq	$0, -88(%rbp)
	movq	440(%rbx), %rcx
	movq	$0, -80(%rbp)
	movq	%rsi, -96(%rbp)
	movq	$0, -72(%rbp)
	movq	%rdx, -64(%rbp)
	cmpq	%rcx, %rax
	je	.L1247
	movdqu	-24(%rax), %xmm0
	movq	$0, -24(%rax)
	movq	-80(%rbp), %rcx
	movq	-8(%rax), %rdx
	movq	%rcx, -16(%rax)
	movq	-72(%rbp), %rcx
	movq	%rcx, -8(%rax)
	movq	%xmm0, %rax
	subq	$32, 448(%rbx)
	movq	%rdx, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	cmpq	-80(%rbp), %rax
	je	.L1247
	movq	%xmm0, -80(%rbp)
.L1247:
	cmpl	$40, 16(%rbx)
	je	.L1249
.L1254:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r13d, %r13d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L1250:
	movq	-64(%rbp), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L1347
.L1421:
	movq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-72(%rbp), %rax
	movq	%rax, 24(%rsi)
	movaps	%xmm0, -80(%rbp)
	addq	$32, 16(%rdi)
	movq	-112(%rbp), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L1349
.L1422:
	movq	-144(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rsi)
	movq	-136(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-128(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-120(%rbp), %rax
	movq	%rax, 24(%rsi)
	movaps	%xmm0, -128(%rbp)
	addq	$32, 16(%rdi)
.L1350:
	cmpb	$0, -177(%rbp)
	je	.L1224
	subl	$1, 536(%r15)
.L1224:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1406
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1404:
	.cfi_restore_state
	movl	$4, 32(%r15)
	movq	296(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal4wasm17WasmModuleBuilder11AddFunctionEPNS0_9SignatureINS1_9ValueTypeEEE@PLT
	movb	$0, -177(%rbp)
	movq	%rax, 8(%r15)
	movl	60(%rax), %eax
	movl	$1, -216(%rbp)
	movl	%eax, 28(%r15)
	movq	-176(%rbp), %rax
	movb	$0, 36(%r15)
	xorl	%r15d, %r15d
	movq	%rax, -208(%rbp)
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1249:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpb	$0, 540(%rbx)
	movl	16(%rbx), %eax
	jne	.L1265
	leaq	-160(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1409:
	leaq	-152(%rbp), %rsi
	leaq	-144(%rbp), %rdi
	movq	$769, -152(%rbp)
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
.L1260:
	movl	16(%rbx), %eax
	cmpl	$41, %eax
	je	.L1253
.L1410:
	cmpl	$44, %eax
	jne	.L1407
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpb	$0, 540(%rbx)
	movl	16(%rbx), %eax
	jne	.L1265
.L1266:
	cmpl	$41, %eax
	je	.L1253
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1408
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, -160(%rbp)
	jne	.L1402
	movq	-80(%rbp), %rsi
	cmpq	-72(%rbp), %rsi
	je	.L1257
	movq	%rax, (%rsi)
	addq	$8, -80(%rbp)
.L1258:
	movq	-160(%rbp), %rdi
	movl	$769, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1409
	movq	-160(%rbp), %rdi
	movl	$57357, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1261
	leaq	-152(%rbp), %rsi
	leaq	-144(%rbp), %rdi
	movq	$57357, -152(%rbp)
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	movl	16(%rbx), %eax
	cmpl	$41, %eax
	jne	.L1410
.L1253:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movq	-176(%rbp), %rcx
	movq	%rax, -224(%rbp)
	cmpq	%rcx, -192(%rbp)
	je	.L1411
.L1267:
	testq	%r13, %r13
	je	.L1412
.L1269:
	movq	(%rbx), %r12
	movq	16(%r12), %r14
	movq	24(%r12), %rax
	subq	%r14, %rax
	cmpq	$47, %rax
	jbe	.L1413
	leaq	48(%r14), %rax
	movq	%rax, 16(%r12)
.L1272:
	leaq	16+_ZTVN2v88internal4wasm15AsmFunctionTypeE(%rip), %rax
	movq	%r13, 8(%r14)
	movq	%rax, (%r14)
	movq	%r12, 16(%r14)
	movq	-136(%rbp), %r12
	movq	-128(%rbp), %r8
	movq	$0, 24(%r14)
	movq	$0, 32(%r14)
	movq	$0, 40(%r14)
	cmpq	%r8, %r12
	je	.L1280
	testb	$1, %r14b
	jne	.L1276
	leaq	_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	(%r14), %rax
	movq	(%r12), %rdx
	movq	16(%rax), %rsi
	movq	%r14, %rax
	cmpq	%rcx, %rsi
	jne	.L1414
.L1277:
	movq	%rdx, -152(%rbp)
	movq	32(%rax), %rsi
	cmpq	%rsi, 40(%rax)
	je	.L1415
	addq	$8, %r12
	movq	%rdx, (%rsi)
	addq	$8, 32(%rax)
	cmpq	%r12, %r8
	jne	.L1281
.L1280:
	leaq	-144(%rbp), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser16ConvertSignatureEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
	movq	296(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal4wasm17WasmModuleBuilder12AddSignatureEPNS0_9SignatureINS1_9ValueTypeEEE@PLT
	movl	%eax, %r8d
	movq	-224(%rbp), %rax
	cmpl	$6, 32(%rax)
	je	.L1416
	movq	(%rax), %rdi
	jle	.L1292
	call	_ZN2v88internal4wasm7AsmType14AsCallableTypeEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1417
	movq	(%rax), %rax
	leaq	-96(%rbp), %r14
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	call	*8(%rax)
	testb	%al, %al
	jne	.L1294
	movq	(%r12), %rax
	movq	%r14, %rdx
	movl	$57357, %esi
	movq	%r12, %rdi
	call	*8(%rax)
	testb	%al, %al
	jne	.L1355
	movq	(%r12), %rax
	movq	%r14, %rdx
	movl	$8197, %esi
	movq	%r12, %rdi
	call	*8(%rax)
	testb	%al, %al
	jne	.L1356
	movq	(%r12), %rax
	movq	%r14, %rdx
	movl	$237, %esi
	movq	%r12, %rdi
	call	*8(%rax)
	testb	%al, %al
	jne	.L1357
	movq	(%r12), %rax
	movq	%r14, %rdx
	movl	$1825, %esi
	movq	%r12, %rdi
	call	*8(%rax)
	testb	%al, %al
	jne	.L1358
	movq	(%r12), %rax
	movq	%r14, %rdx
	movl	$2817, %esi
	movq	%r12, %rdi
	movl	$2817, %r13d
	call	*8(%rax)
	testb	%al, %al
	je	.L1344
.L1294:
	movq	-224(%rbp), %rax
	movl	32(%rax), %eax
	movl	%eax, -168(%rbp)
	subl	$7, %eax
	cmpl	$18, %eax
	ja	.L1295
	leaq	.L1297(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv,"a",@progbits
	.align 4
	.align 4
.L1297:
	.long	.L1313-.L1297
	.long	.L1313-.L1297
	.long	.L1312-.L1297
	.long	.L1295-.L1297
	.long	.L1311-.L1297
	.long	.L1310-.L1297
	.long	.L1309-.L1297
	.long	.L1308-.L1297
	.long	.L1307-.L1297
	.long	.L1306-.L1297
	.long	.L1305-.L1297
	.long	.L1304-.L1297
	.long	.L1303-.L1297
	.long	.L1302-.L1297
	.long	.L1301-.L1297
	.long	.L1300-.L1297
	.long	.L1299-.L1297
	.long	.L1298-.L1297
	.long	.L1296-.L1297
	.section	.text._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv
	.p2align 4,,10
	.p2align 3
.L1261:
	movq	-160(%rbp), %rdi
	movl	$237, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1262
	leaq	-152(%rbp), %rsi
	leaq	-144(%rbp), %rdi
	movq	$237, -152(%rbp)
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE12emplace_backIJS4_EEEvDpOT_
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1257:
	movq	-224(%rbp), %rdx
	leaq	-96(%rbp), %rdi
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L1258
	.p2align 4,,10
	.p2align 3
.L1225:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1418
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser18EqualityExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %rdi
	jne	.L1352
	movl	$257, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	movb	%al, -177(%rbp)
	testb	%al, %al
	je	.L1419
	cmpl	$38, 16(%rbx)
	jne	.L1403
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$-3, 16(%rbx)
	je	.L1232
	leaq	.LC71(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r13d, %r13d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1405:
	leaq	.LC76(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r13d, %r13d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1418:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r13d, %r13d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1224
.L1416:
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %r14
	cmpq	%r14, %rax
	je	.L1287
	movq	%r12, -168(%rbp)
	movq	%r14, %r12
	movq	%rax, %r14
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1285:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L1420
.L1286:
	movq	(%r12), %rdi
	movl	$33, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1285
	leaq	.LC78(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	-64(%rbp), %rdi
	xorl	%r13d, %r13d
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	jne	.L1421
	.p2align 4,,10
	.p2align 3
.L1347:
	leaq	-96(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal10ZoneVectorIPNS1_4wasm7AsmTypeEEENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_
	movq	-112(%rbp), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	jne	.L1422
	.p2align 4,,10
	.p2align 3
.L1349:
	leaq	-144(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal10ZoneVectorIPNS1_4wasm7AsmTypeEEENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1243:
	movq	%rcx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rdx, -64(%rbp)
	jmp	.L1247
	.p2align 4,,10
	.p2align 3
.L1265:
	cmpl	$41, %eax
	jne	.L1254
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1403:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r13d, %r13d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1224
.L1313:
	movq	-88(%rbp), %rax
	movl	$237, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1318
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	cmpq	$15, %rax
	jbe	.L1250
	movl	$1, %r12d
.L1321:
	movq	-224(%rbp), %rax
	movq	304(%rbx), %rdi
	cmpl	$7, 32(%rax)
	je	.L1423
	movl	$165, %esi
	addq	$1, %r12
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$3, %rax
	cmpq	%rax, %r12
	jb	.L1321
	jmp	.L1250
.L1296:
	movq	-88(%rbp), %rax
	movl	$77, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1317
	movq	304(%rbx), %rdi
	movl	$159, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1298:
	movq	-88(%rbp), %rax
	movl	$77, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1316
	movq	304(%rbx), %rdi
	movl	$156, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1305:
	movq	304(%rbx), %rdi
	movl	$203, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1301:
	movq	304(%rbx), %rdi
	movl	$108, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1302:
	movq	304(%rbx), %rdi
	movl	$206, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1303:
	movq	304(%rbx), %rdi
	movl	$205, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1304:
	movq	304(%rbx), %rdi
	movl	$204, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1299:
	movq	-88(%rbp), %rax
	movl	$77, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1314
	movq	304(%rbx), %rdi
	movl	$155, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1300:
	movq	304(%rbx), %rdi
	movl	$103, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1309:
	movq	304(%rbx), %rdi
	movl	$199, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1310:
	movq	304(%rbx), %rdi
	movl	$198, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1311:
	movq	304(%rbx), %rdi
	movl	$197, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1312:
	movq	-88(%rbp), %rax
	movl	$1825, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1335
	movl	536(%rbx), %r14d
	movq	304(%rbx), %rdi
	leal	1(%r14), %r12d
	movl	%r12d, 536(%rbx)
	cmpl	%r14d, 532(%rbx)
	jg	.L1336
	movl	%r12d, 532(%rbx)
.L1336:
	movl	528(%rbx), %esi
	addl	%r14d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitTeeLocalEj@PLT
	movq	304(%rbx), %rdi
	cmpl	%r14d, 532(%rbx)
	jg	.L1337
	movl	%r12d, 532(%rbx)
.L1337:
	movl	528(%rbx), %esi
	addl	%r14d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj@PLT
	movq	304(%rbx), %rdi
	movl	$31, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movq	304(%rbx), %rdi
	movl	$117, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	304(%rbx), %rdi
	cmpl	%r14d, 532(%rbx)
	jg	.L1338
	movl	%r12d, 532(%rbx)
.L1338:
	movl	528(%rbx), %esi
	addl	%r14d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitTeeLocalEj@PLT
	movq	304(%rbx), %rdi
	movl	$115, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	304(%rbx), %rdi
	cmpl	%r14d, 532(%rbx)
	jg	.L1339
	movl	%r12d, 532(%rbx)
.L1339:
	movl	528(%rbx), %esi
	addl	%r14d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj@PLT
	movq	304(%rbx), %rdi
	movl	$107, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	subl	$1, 536(%rbx)
	jmp	.L1250
.L1307:
	movq	304(%rbx), %rdi
	movl	$201, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1308:
	movq	304(%rbx), %rdi
	movl	$200, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1306:
	movq	304(%rbx), %rdi
	movl	$202, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1295:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1408:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1402
.L1415:
	leaq	-152(%rbp), %rdx
	leaq	16(%rax), %rdi
	movq	%r8, -176(%rbp)
	addq	$8, %r12
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-176(%rbp), %r8
	leaq	_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv(%rip), %rcx
	cmpq	%r12, %r8
	jne	.L1281
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1352:
	xorl	%r13d, %r13d
	jmp	.L1224
.L1232:
	movl	288(%rbx), %r15d
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r15d, %r9d
	addl	$1, %r9d
	je	.L1233
	testl	%r15d, %r9d
	jne	.L1233
	movq	304(%rbx), %rdi
	movl	%r15d, %esi
	movl	%r9d, -168(%rbp)
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movq	304(%rbx), %rdi
	movl	$113, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	cmpl	$93, 16(%rbx)
	movl	-168(%rbp), %r9d
	jne	.L1403
	movq	%r12, %rdi
	movl	%r9d, -168(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movl	-168(%rbp), %r9d
	movq	%rax, %rdx
	movl	32(%rax), %eax
	testl	%eax, %eax
	je	.L1424
	cmpl	$5, %eax
	jne	.L1425
	cmpl	%r15d, 24(%rdx)
	jne	.L1426
	movl	28(%rdx), %esi
.L1238:
	movq	304(%rbx), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movq	304(%rbx), %rdi
	movl	$106, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movl	536(%rbx), %eax
	movq	304(%rbx), %rdi
	leal	1(%rax), %ecx
	movl	%eax, -168(%rbp)
	movl	%ecx, -216(%rbp)
	movl	%ecx, 536(%rbx)
	cmpl	532(%rbx), %eax
	jl	.L1241
	movl	%ecx, 532(%rbx)
.L1241:
	movl	-168(%rbp), %esi
	addl	528(%rbx), %esi
	movq	%rbx, %r15
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj@PLT
	movq	32(%rbx), %rax
	movq	%rax, -208(%rbp)
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1407:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1402
	.p2align 4,,10
	.p2align 3
.L1233:
	leaq	.LC72(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r13d, %r13d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	%r8, -192(%rbp)
	movq	%r14, %rdi
	movq	%rdx, -176(%rbp)
	call	*%rsi
	movq	-192(%rbp), %r8
	movq	-176(%rbp), %rdx
	leaq	_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv(%rip), %rcx
	jmp	.L1277
.L1419:
	leaq	.LC70(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r13d, %r13d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1224
.L1262:
	leaq	.LC77(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1402
.L1411:
	cmpl	$124, 16(%rbx)
	jne	.L1267
	cmpl	$6, 32(%rax)
	jg	.L1267
	testq	%r13, %r13
	je	.L1270
	movl	$57357, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1269
.L1270:
	movq	32(%rbx), %rax
	movl	$1825, %r13d
	movq	$1825, 696(%rbx)
	movq	%rax, -200(%rbp)
	jmp	.L1269
.L1292:
	movl	$2147483649, %esi
	movl	%r8d, -176(%rbp)
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	movl	-176(%rbp), %r8d
	testb	%al, %al
	je	.L1341
	movq	-224(%rbp), %rax
	movq	%r14, (%rax)
.L1342:
	movq	-224(%rbp), %rax
	movq	304(%rbx), %rdi
	cmpl	$5, 32(%rax)
	jne	.L1345
	movl	-168(%rbp), %eax
	cmpl	532(%r15), %eax
	jl	.L1346
	movl	-216(%rbp), %eax
	movl	%eax, 532(%r15)
.L1346:
	movl	-168(%rbp), %esi
	addl	528(%r15), %esi
	movl	%r8d, -176(%rbp)
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj@PLT
	movq	-200(%rbp), %rdx
	movq	304(%rbx), %rdi
	movq	-208(%rbp), %rsi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder16AddAsmWasmOffsetEmm@PLT
	movq	304(%rbx), %rdi
	movl	$17, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movl	-176(%rbp), %r8d
	movq	304(%rbx), %rdi
	movl	%r8d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder8EmitU32VEj@PLT
	movq	304(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder8EmitU32VEj@PLT
	jmp	.L1250
.L1412:
	movq	-208(%rbp), %rax
	movl	$17, %r13d
	movq	%rax, -200(%rbp)
	jmp	.L1269
.L1420:
	movq	-168(%rbp), %r12
.L1287:
	movl	$57357, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1427
	leaq	.LC79(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r13d, %r13d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1250
.L1345:
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %rsi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder16AddAsmWasmOffsetEmm@PLT
	movq	304(%rbx), %rdi
	movl	$16, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	-224(%rbp), %rax
	movq	304(%rbx), %rdi
	movl	28(%rax), %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder19EmitDirectCallIndexEj@PLT
	jmp	.L1250
.L1341:
	movq	-224(%rbp), %rax
	movl	%r8d, -176(%rbp)
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType14AsCallableTypeEv@PLT
	movl	-176(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L1344
	movq	(%rax), %rax
	movl	%r8d, -176(%rbp)
	leaq	-96(%rbp), %rdx
	movq	%r13, %rsi
	call	*8(%rax)
	movl	-176(%rbp), %r8d
	testb	%al, %al
	jne	.L1342
.L1344:
	leaq	.LC81(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r13d, %r13d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1250
.L1413:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r14
	jmp	.L1272
.L1427:
	movq	-224(%rbp), %rax
	movq	8(%r12), %rdi
	movq	(%r12), %rsi
	movq	16(%rax), %rax
	movq	%rax, -168(%rbp)
	addq	$16, %rax
	movq	%rax, -176(%rbp)
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	8(%r12), %r14
	movq	(%r12), %rcx
	movq	%rax, %rdi
	movq	16(%r12), %rax
	addq	%rax, %r14
	addq	%r14, %rcx
	cmpq	%rcx, %rax
	je	.L1288
	movq	%r12, -192(%rbp)
	movq	%rax, %r14
	movq	%rbx, -216(%rbp)
	movq	%rcx, %rbx
.L1289:
	call	_ZN2v84base10hash_valueEm@PLT
	movzbl	(%r14), %esi
	xorl	%edi, %edi
	addq	$1, %r14
	movq	%rax, %r12
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v84base12hash_combineEmm@PLT
	movq	%rax, %rdi
	cmpq	%r14, %rbx
	jne	.L1289
	movq	-192(%rbp), %r12
	movq	-216(%rbp), %rbx
.L1288:
	movq	-168(%rbp), %rcx
	movq	%rdi, %rax
	xorl	%edx, %edx
	divq	32(%rcx)
	movq	%rdi, %rcx
	movq	-176(%rbp), %rdi
	movq	%rdx, %rsi
	movq	%r12, %rdx
	call	_ZNKSt10_HashtableIN2v88internal9SignatureINS1_4wasm9ValueTypeEEESt4pairIKS5_jENS1_13ZoneAllocatorIS8_EENSt8__detail10_Select1stESt8equal_toIS5_ENS0_4base4hashIS5_EENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb1ELb0ELb1EEEE19_M_find_before_nodeEmRS7_m
	testq	%rax, %rax
	je	.L1290
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L1290
	movl	32(%rax), %r14d
.L1291:
	movq	304(%rbx), %rdi
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %rsi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder16AddAsmWasmOffsetEmm@PLT
	movq	304(%rbx), %rdi
	movl	%r14d, %edx
	movl	$16, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU32VENS1_10WasmOpcodeEj@PLT
	jmp	.L1250
.L1355:
	movl	$57357, %r13d
	jmp	.L1294
.L1417:
	leaq	.LC80(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r13d, %r13d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1250
.L1424:
	movq	296(%rbx), %rdi
	movl	%r9d, %esi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal4wasm17WasmModuleBuilder25AllocateIndirectFunctionsEj@PLT
	movq	-168(%rbp), %rdx
	cmpl	$-1, %eax
	movl	%eax, %esi
	je	.L1428
	movl	$5, 32(%rdx)
	movl	%r15d, 24(%rdx)
	movl	%eax, 28(%rdx)
	movb	$0, 36(%rdx)
	jmp	.L1238
.L1290:
	movq	-224(%rbp), %rax
	movq	296(%rbx), %rdi
	movq	%r12, %rcx
	movq	16(%rax), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddImportENS0_6VectorIKcEEPNS0_9SignatureINS1_9ValueTypeEEE@PLT
	movq	%r12, %rsi
	movl	%eax, %r14d
	movq	-224(%rbp), %rax
	movq	16(%rax), %rcx
	leaq	16(%rcx), %rdi
	movq	%rcx, -168(%rbp)
	call	_ZNSt8__detail9_Map_baseIN2v88internal9SignatureINS2_4wasm9ValueTypeEEESt4pairIKS6_jENS2_13ZoneAllocatorIS9_EENS_10_Select1stESt8equal_toIS6_ENS1_4base4hashIS6_EENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movl	%r14d, (%rax)
	movq	-224(%rbp), %rax
	movb	$1, 37(%rax)
	jmp	.L1291
.L1425:
	leaq	.LC74(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r13d, %r13d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1224
.L1423:
	movl	$164, %esi
	addq	$1, %r12
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$3, %rax
	cmpq	%r12, %rax
	ja	.L1321
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1356:
	movl	$8197, %r13d
	jmp	.L1294
.L1426:
	leaq	.LC75(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r13d, %r13d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1224
.L1357:
	movl	$237, %r13d
	jmp	.L1294
.L1406:
	call	__stack_chk_fail@PLT
.L1358:
	movl	$1825, %r13d
	jmp	.L1294
.L1318:
	movq	-88(%rbp), %rax
	movl	$57357, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1322
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	cmpq	$15, %rax
	jbe	.L1250
	movl	$1, %r12d
	jmp	.L1325
.L1323:
	movl	$151, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
.L1324:
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	addq	$1, %r12
	sarq	$3, %rax
	cmpq	%rax, %r12
	jnb	.L1250
.L1325:
	movq	-224(%rbp), %rax
	movq	304(%rbx), %rdi
	cmpl	$7, 32(%rax)
	jne	.L1323
	movl	$150, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1324
.L1317:
	movq	-88(%rbp), %rax
	movl	$24589, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1315
	movq	304(%rbx), %rdi
	movl	$145, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1314:
	movq	-88(%rbp), %rax
	movl	$24589, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1315
	movq	304(%rbx), %rdi
	movl	$141, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1428:
	leaq	.LC73(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r13d, %r13d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1224
.L1335:
	movq	-88(%rbp), %rax
	movl	$77, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1340
	movq	304(%rbx), %rdi
	movl	$153, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1316:
	movq	-88(%rbp), %rax
	movl	$24589, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1315
	movq	304(%rbx), %rdi
	movl	$142, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
.L1322:
	movq	-88(%rbp), %rax
	movl	$1825, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1315
	movl	536(%rbx), %r14d
	movq	$1, -168(%rbp)
	leal	2(%r14), %eax
	leal	1(%r14), %r12d
	movl	%eax, -176(%rbp)
	movl	%eax, 536(%rbx)
	jmp	.L1334
.L1330:
	movl	$76, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
.L1331:
	movq	304(%rbx), %rdi
	movl	$127, %edx
	movl	$4, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	movq	304(%rbx), %rdi
	cmpl	532(%rbx), %r14d
	jl	.L1332
	movl	%r12d, 532(%rbx)
.L1332:
	movl	528(%rbx), %esi
	addl	%r14d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj@PLT
	movq	304(%rbx), %rdi
	movl	$5, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	304(%rbx), %rdi
	cmpl	532(%rbx), %r12d
	jl	.L1333
	movl	-176(%rbp), %eax
	movl	%eax, 532(%rbx)
.L1333:
	movl	528(%rbx), %esi
	addl	%r12d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj@PLT
	movq	304(%rbx), %rdi
	movl	$11, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	addq	$1, -168(%rbp)
.L1334:
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	sarq	$3, %rax
	cmpq	%rax, -168(%rbp)
	jnb	.L1326
	movq	304(%rbx), %rdi
	cmpl	532(%rbx), %r14d
	jl	.L1327
	movl	%r12d, 532(%rbx)
.L1327:
	movl	528(%rbx), %esi
	addl	%r14d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj@PLT
	movq	304(%rbx), %rdi
	cmpl	532(%rbx), %r12d
	jl	.L1328
	movl	-176(%rbp), %eax
	movl	%eax, 532(%rbx)
.L1328:
	movl	528(%rbx), %esi
	addl	%r12d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitTeeLocalEj@PLT
	movq	304(%rbx), %rdi
	cmpl	532(%rbx), %r14d
	jl	.L1329
	movl	%r12d, 532(%rbx)
.L1329:
	movl	528(%rbx), %esi
	addl	%r14d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj@PLT
	movq	-224(%rbp), %rax
	movq	304(%rbx), %rdi
	cmpl	$7, 32(%rax)
	jne	.L1330
	movl	$78, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1331
.L1326:
	subl	$2, 536(%rbx)
	jmp	.L1250
.L1315:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1340:
	movq	-88(%rbp), %rax
	movl	$24589, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1315
	movq	304(%rbx), %rdi
	movl	$139, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1250
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv
	.cfi_startproc
	.type	_ZN2v88internal4wasm11AsmJsParser12ValidateCallEv.cold, @function
_ZN2v88internal4wasm11AsmJsParser12ValidateCallEv.cold:
.LFSB18254:
.L1276:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%r12), %rax
	movq	%rax, -152(%rbp)
	movq	32, %rax
	ud2
	.cfi_endproc
.LFE18254:
	.section	.text._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv
	.size	_ZN2v88internal4wasm11AsmJsParser12ValidateCallEv, .-_ZN2v88internal4wasm11AsmJsParser12ValidateCallEv
	.section	.text.unlikely._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv
	.size	_ZN2v88internal4wasm11AsmJsParser12ValidateCallEv.cold, .-_ZN2v88internal4wasm11AsmJsParser12ValidateCallEv.cold
.LCOLDE82:
	.section	.text._ZN2v88internal4wasm11AsmJsParser12ValidateCallEv
.LHOTE82:
	.section	.text._ZN2v88internal4wasm11AsmJsParser14CallExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser14CallExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser14CallExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser14CallExpressionEv:
.LFB18240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movl	16(%rdi), %esi
	cmpl	$255, %esi
	jg	.L1462
.L1430:
	cmpl	$40, %esi
	je	.L1463
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser8PeekCallEv
	testb	%al, %al
	jne	.L1464
	movl	16(%r12), %eax
	addl	$9999, %eax
	cmpl	$10254, %eax
	jbe	.L1465
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L1445
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10IdentifierEv
	cmpb	$0, 540(%r12)
	je	.L1429
.L1437:
	xorl	%eax, %eax
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1463:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jnb	.L1438
.L1445:
	movb	$1, 540(%r12)
	leaq	.LC55(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	xorl	%eax, %eax
.L1429:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1462:
	.cfi_restore_state
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movq	672(%r12), %rsi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1466
	movl	16(%r12), %esi
	cmpl	$255, %esi
	jle	.L1430
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movl	$3, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1433
	movl	16(%r12), %esi
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1438:
	cmpl	$40, 16(%r12)
	movq	$0, 680(%r12)
	jne	.L1457
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L1467
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%r12)
	jne	.L1437
	cmpl	$41, 16(%r12)
	jne	.L1457
	movq	%r13, %rdi
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	-24(%rbp), %rax
	cmpb	$0, 540(%r12)
	jne	.L1437
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1464:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L1445
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser12ValidateCallEv
	cmpb	$0, 540(%r12)
	jne	.L1437
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1433:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L1445
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser16MemberExpressionEv
	cmpb	$0, 540(%r12)
	je	.L1429
	jmp	.L1437
	.p2align 4,,10
	.p2align 3
.L1457:
	movb	$1, 540(%r12)
	leaq	.LC0(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	xorl	%eax, %eax
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1466:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser21ValidateFloatCoercionEv
	addq	$16, %rsp
	movl	$57357, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1465:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L1445
	movl	16(%r12), %eax
	movq	$0, 680(%r12)
	cmpl	$-4, %eax
	je	.L1448
	cmpl	$-3, %eax
	je	.L1468
	movb	$1, 540(%r12)
	leaq	.LC23(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	xorl	%eax, %eax
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1448:
	movsd	280(%r12), %xmm0
	leaq	8(%r12), %rdi
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	304(%r12), %rdi
	movsd	-24(%rbp), %xmm0
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitF64ConstEd@PLT
	movl	$237, %eax
.L1451:
	cmpb	$0, 540(%r12)
	je	.L1429
	jmp	.L1437
	.p2align 4,,10
	.p2align 3
.L1467:
	movb	$1, 540(%r12)
	leaq	.LC55(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	xorl	%eax, %eax
	jmp	.L1429
.L1468:
	movl	288(%r12), %r13d
	leaq	8(%r12), %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	304(%r12), %rdi
	movl	%r13d, %esi
	testl	%r13d, %r13d
	js	.L1452
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movl	$7969, %eax
	jmp	.L1451
.L1452:
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movl	$2817, %eax
	jmp	.L1451
	.cfi_endproc
.LFE18240:
	.size	_ZN2v88internal4wasm11AsmJsParser14CallExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser14CallExpressionEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC83:
	.string	"Integer numeric literal out of range."
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv.str1.1,"aMS",@progbits,1
.LC84:
	.string	"expected int/double?/float?"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv.str1.8
	.align 8
.LC85:
	.string	"expected signed/unsigned/double?/float?"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv.str1.1
.LC86:
	.string	"expected int"
.LC87:
	.string	"expected double or float?"
.LC88:
	.string	"operator ~ expects intish"
	.section	.text._ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv:
.LFB18243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %eax
	cmpl	$45, %eax
	je	.L1470
	cmpl	$43, %eax
	je	.L1522
	cmpl	$33, %eax
	je	.L1523
	cmpl	$126, %eax
	je	.L1524
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1498
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser14CallExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r12
	jne	.L1480
.L1469:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1498:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1470:
	.cfi_restore_state
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$-3, 16(%rbx)
	je	.L1525
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1498
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r12
	jne	.L1480
	movl	$769, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1481
	movl	536(%rbx), %r12d
	movq	304(%rbx), %rdi
	leal	1(%r12), %r13d
	movl	%r13d, 536(%rbx)
	cmpl	%r12d, 532(%rbx)
	jg	.L1482
	movl	%r13d, 532(%rbx)
.L1482:
	movl	528(%rbx), %esi
	addl	%r12d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj@PLT
	movq	304(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movq	304(%rbx), %rdi
	cmpl	%r12d, 532(%rbx)
	jg	.L1483
	movl	%r13d, 532(%rbx)
.L1483:
	movl	528(%rbx), %esi
	addl	%r12d, %esi
	movl	$257, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj@PLT
	movq	304(%rbx), %rdi
	movl	$107, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	subl	$1, 536(%rbx)
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1524:
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$126, 16(%rbx)
	je	.L1526
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1498
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %rdi
	jne	.L1480
	movl	$257, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1527
	movq	304(%rbx), %rdi
	movl	$-1, %esi
	movl	$1825, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movq	304(%rbx), %rdi
	movl	$115, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1480:
	addq	$8, %rsp
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1522:
	.cfi_restore_state
	movq	32(%rdi), %rax
	leaq	8(%rdi), %rdi
	movq	$237, 672(%rdi)
	movq	%rax, 680(%rdi)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1498
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r13
	jne	.L1480
	movl	$1825, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1528
	movl	$2817, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1488
	movq	304(%rbx), %rdi
	movl	$184, %esi
	movl	$237, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1523:
	leaq	8(%rdi), %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1498
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r12
	jne	.L1480
	movl	$769, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1529
	movq	304(%rbx), %rdi
	movl	$69, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1525:
	movl	288(%rbx), %r13d
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$-2147483648, %r13d
	ja	.L1530
	movq	304(%rbx), %rdi
	movl	%r13d, %esi
	cmpl	$-2147483648, %r13d
	je	.L1478
	negl	%esi
.L1478:
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movl	$1825, %r12d
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1528:
	movq	304(%rbx), %rdi
	movl	$183, %esi
	movl	$237, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1529:
	leaq	.LC86(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1481:
	movl	$77, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1484
	movq	304(%rbx), %rdi
	movl	$154, %esi
	movl	$237, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1526:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1498
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r12
	jne	.L1480
	movl	$237, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1504
	movq	304(%rbx), %rdi
	movl	$229, %esi
	movl	$1825, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1488:
	movl	$77, %esi
	movq	%r13, %rdi
	movl	$237, %r12d
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1469
	movl	$24589, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1489
	movq	304(%rbx), %rdi
	movl	$187, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1484:
	movl	$24589, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1485
	movq	304(%rbx), %rdi
	movl	$140, %esi
	movl	$8197, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1469
	.p2align 4,,10
	.p2align 3
.L1527:
	leaq	.LC88(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1469
.L1530:
	leaq	.LC83(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1469
.L1504:
	movl	$24589, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1505
	movq	304(%rbx), %rdi
	movl	$227, %esi
	movl	$1825, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1469
.L1485:
	leaq	.LC84(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1469
.L1489:
	leaq	.LC85(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1469
.L1505:
	leaq	.LC87(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1469
	.cfi_endproc
.LFE18243:
	.size	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser24MultiplicativeExpressionEv.str1.1,"aMS",@progbits,1
.LC89:
	.string	"Expected int"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser24MultiplicativeExpressionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC90:
	.string	"Constant multiple out of range"
	.align 8
.LC91:
	.string	"Integer multiply of expects int"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser24MultiplicativeExpressionEv.str1.1
.LC92:
	.string	"expected doubles or floats"
	.section	.text._ZN2v88internal4wasm11AsmJsParser24MultiplicativeExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser24MultiplicativeExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser24MultiplicativeExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser24MultiplicativeExpressionEv:
.LFB18244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	16(%rdi), %eax
	movq	%rdi, %rbx
	cmpl	$-3, %eax
	jne	.L1532
	movl	288(%rdi), %r12d
	cmpl	$1048575, %r12d
	jbe	.L1625
.L1624:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1615
.L1607:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r12
	jne	.L1540
	movl	16(%rbx), %eax
.L1547:
	leaq	8(%rbx), %r13
	cmpl	$42, %eax
	je	.L1626
.L1551:
	cmpl	$47, %eax
	je	.L1627
	cmpl	$37, %eax
	je	.L1628
.L1531:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1532:
	.cfi_restore_state
	cmpl	$45, %eax
	jne	.L1624
	leaq	8(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$-3, 16(%rbx)
	jne	.L1544
	movl	288(%rbx), %r12d
	cmpl	$1048575, %r12d
	jbe	.L1629
.L1544:
	movq	%r13, %rdi
.L1622:
	call	_ZN2v88internal12AsmJsScanner6RewindEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jnb	.L1607
.L1615:
	leaq	.LC55(%rip), %rax
	xorl	%r12d, %r12d
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1625:
	.cfi_restore_state
	leaq	8(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$42, 16(%rbx)
	movq	%r13, %rdi
	jne	.L1622
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1615
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %rdi
	jne	.L1540
	movl	$769, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1616
	movq	304(%rbx), %rdi
	movl	%r12d, %esi
.L1617:
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
.L1618:
	movq	304(%rbx), %rdi
	movl	$108, %esi
	movl	$257, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1531
	.p2align 4,,10
	.p2align 3
.L1628:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1615
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r14
	jne	.L1540
	movl	$77, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1591
	movl	$77, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1630
.L1591:
	movl	$1825, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1589
	movl	$1825, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1631
.L1589:
	movl	$2817, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1593
	movl	$2817, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1593
	movq	304(%rbx), %rdi
	movl	$214, %esi
	movl	$257, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	.p2align 4,,10
	.p2align 3
.L1571:
	movl	16(%rbx), %eax
	cmpl	$42, %eax
	jne	.L1551
.L1626:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %eax
	cmpl	$45, %eax
	je	.L1632
	cmpl	$-3, %eax
	je	.L1633
.L1559:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1615
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r14
	jne	.L1540
	movl	$77, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1570
	movl	$77, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1634
.L1570:
	movl	$24589, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1593
	movl	$24589, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1593
	movq	304(%rbx), %rdi
	movl	$148, %esi
	movl	$8197, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1627:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1615
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r14
	jne	.L1540
	movl	$77, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1579
	movl	$77, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1635
.L1579:
	movl	$24589, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1577
	movl	$24589, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1636
.L1577:
	movl	$1825, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1581
	movl	$1825, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1637
.L1581:
	movl	$2817, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1593
	movl	$2817, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1593
	movq	304(%rbx), %rdi
	movl	$212, %esi
	movl	$257, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1540:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1636:
	.cfi_restore_state
	movq	304(%rbx), %rdi
	movl	$149, %esi
	movl	$8197, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1630:
	movq	304(%rbx), %rdi
	movl	$207, %esi
	movl	$237, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	304(%rbx), %rdi
	movl	$211, %esi
	movl	$257, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1631:
	movq	304(%rbx), %rdi
	movl	$213, %esi
	movl	$257, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1632:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$-3, 16(%rbx)
	je	.L1638
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner6RewindEv@PLT
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1633:
	movl	288(%rbx), %r14d
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$1048575, %r14d
	ja	.L1620
	movl	$769, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1619
	movq	304(%rbx), %rdi
	movl	%r14d, %esi
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r12d, %esi
	movq	304(%rbx), %rdi
	movl	$1825, %r12d
	negl	%esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movl	16(%rbx), %eax
	cmpl	$42, %eax
	jne	.L1547
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1615
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15UnaryExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %rdi
	jne	.L1540
	movl	$769, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1618
.L1616:
	leaq	.LC89(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1531
	.p2align 4,,10
	.p2align 3
.L1635:
	movq	304(%rbx), %rdi
	movl	$163, %esi
	movl	$237, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1634:
	movq	304(%rbx), %rdi
	movl	$162, %esi
	movl	$237, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1620:
	leaq	.LC90(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1531
	.p2align 4,,10
	.p2align 3
.L1593:
	leaq	.LC92(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1531
.L1619:
	leaq	.LC91(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1531
.L1638:
	movl	288(%rbx), %r14d
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$1048575, %r14d
	ja	.L1620
	movl	$769, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1619
	movl	%r14d, %esi
	movq	304(%rbx), %rdi
	negl	%esi
	jmp	.L1617
	.cfi_endproc
.LFE18244:
	.size	_ZN2v88internal4wasm11AsmJsParser24MultiplicativeExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser24MultiplicativeExpressionEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser18AdditiveExpressionEv.str1.1,"aMS",@progbits,1
.LC93:
	.string	"illegal types for +"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser18AdditiveExpressionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC94:
	.string	"more than 2^20 additive values"
	.section	.text._ZN2v88internal4wasm11AsmJsParser18AdditiveExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser18AdditiveExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser18AdditiveExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser18AdditiveExpressionEv:
.LFB18245:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1681
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser24MultiplicativeExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r12
	jne	.L1642
	xorl	%r13d, %r13d
	leaq	8(%rbx), %r14
.L1643:
	movl	16(%rbx), %eax
	cmpl	$43, %eax
	je	.L1644
	cmpl	$45, %eax
	je	.L1683
.L1639:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1681:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1681
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser24MultiplicativeExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r15
	jne	.L1642
	movl	$237, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1651
	movl	$237, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1684
.L1651:
	movl	$24589, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1649
	movl	$24589, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1685
.L1649:
	movl	$769, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1653
	movl	$769, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1686
.L1653:
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1670
	movl	$257, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1670
	addl	$1, %r13d
	cmpl	$1048576, %r13d
	jg	.L1682
	movq	304(%rbx), %rdi
	movl	$106, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1683:
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1681
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser24MultiplicativeExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r15
	jne	.L1642
	movl	$237, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1665
	movl	$237, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1687
.L1665:
	movl	$24589, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1663
	movl	$24589, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1688
.L1663:
	movl	$769, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1666
.L1667:
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1670
	movl	$257, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1670
	addl	$1, %r13d
	cmpl	$1048576, %r13d
	jg	.L1682
	movq	304(%rbx), %rdi
	movl	$107, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1642:
	xorl	%r12d, %r12d
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1666:
	movl	$769, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1667
	movq	304(%rbx), %rdi
	movl	$107, %esi
	movl	$2, %r13d
	movl	$257, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1685:
	movq	304(%rbx), %rdi
	movl	$146, %esi
	movl	$8197, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1687:
	movq	304(%rbx), %rdi
	movl	$161, %esi
	movl	$237, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	304(%rbx), %rdi
	movl	$106, %esi
	movl	$2, %r13d
	movl	$257, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1688:
	movq	304(%rbx), %rdi
	movl	$147, %esi
	movl	$8197, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1684:
	movq	304(%rbx), %rdi
	movl	$160, %esi
	movl	$237, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1643
.L1670:
	leaq	.LC93(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1639
.L1682:
	leaq	.LC94(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1639
	.cfi_endproc
.LFE18245:
	.size	_ZN2v88internal4wasm11AsmJsParser18AdditiveExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser18AdditiveExpressionEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser15ShiftExpressionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC95:
	.string	"Expected intish for operator >>."
	.align 8
.LC96:
	.string	"Expected intish for operator \"<<\"."
	.align 8
.LC97:
	.string	"Expected intish for operator \">>>\"."
	.section	.text._ZN2v88internal4wasm11AsmJsParser15ShiftExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser15ShiftExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser15ShiftExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser15ShiftExpressionEv:
.LFB18246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L1735
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser18AdditiveExpressionEv
	cmpb	$0, 540(%r14)
	movq	%rax, %r15
	jne	.L1734
	movq	$-1, 712(%r14)
	leaq	8(%r14), %rbx
.L1693:
	movl	16(%r14), %eax
	cmpl	$-9939, %eax
	je	.L1694
	cmpl	$-9938, %eax
	je	.L1695
	cmpl	$-9940, %eax
	je	.L1736
.L1689:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1735:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%r14)
	xorl	%r15d, %r15d
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1695:
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	$-1, 712(%r14)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L1735
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser18AdditiveExpressionEv
	cmpb	$0, 540(%r14)
	movq	%rax, %r12
	jne	.L1734
	movl	$257, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1706
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1737
.L1706:
	leaq	.LC97(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
.L1734:
	xorl	%r15d, %r15d
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1694:
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	$257, %esi
	movq	%r15, %rdi
	movq	$-1, 712(%r14)
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L1738
.L1697:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L1735
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser18AdditiveExpressionEv
	cmpb	$0, 540(%r14)
	movq	%rax, %r13
	jne	.L1734
	testb	%r12b, %r12b
	jne	.L1739
.L1700:
	movl	$257, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1701
	movl	$257, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1701
	movq	304(%r14), %rdi
	movl	$117, %esi
	movl	$1825, %r15d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1736:
	movq	%rbx, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	$-1, 712(%r14)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L1735
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser18AdditiveExpressionEv
	cmpb	$0, 540(%r14)
	movq	%rax, %r12
	jne	.L1734
	movl	$257, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1704
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1704
	movq	304(%r14), %rdi
	movl	$116, %esi
	movl	$1825, %r15d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1737:
	movq	304(%r14), %rdi
	movl	$118, %esi
	movl	$2817, %r15d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1693
	.p2align 4,,10
	.p2align 3
.L1738:
	cmpl	$-3, 16(%r14)
	je	.L1740
	xorl	%r12d, %r12d
	jmp	.L1697
	.p2align 4,,10
	.p2align 3
.L1739:
	movq	-56(%rbp), %rax
	cmpq	32(%r14), %rax
	jne	.L1700
	movq	-64(%rbp), %rax
	movq	%rax, 712(%r14)
	movl	-68(%rbp), %eax
	movl	%eax, 720(%r14)
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L1701:
	leaq	.LC95(%rip), %rax
	movb	$1, 540(%r14)
	xorl	%r15d, %r15d
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1704:
	leaq	.LC96(%rip), %rax
	movb	$1, 540(%r14)
	xorl	%r15d, %r15d
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1740:
	movl	288(%r14), %eax
	movq	%rbx, %rdi
	movl	%eax, -68(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	32(%r14), %rax
	movq	%rbx, %rdi
	movq	%rax, -56(%rbp)
	movq	304(%r14), %rax
	movq	80(%rax), %rdx
	subq	72(%rax), %rdx
	movq	%rdx, -64(%rbp)
	call	_ZN2v88internal12AsmJsScanner6RewindEv@PLT
	jmp	.L1697
	.cfi_endproc
.LFE18246:
	.size	_ZN2v88internal4wasm11AsmJsParser15ShiftExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser15ShiftExpressionEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser20RelationalExpressionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC98:
	.string	"Expected signed, unsigned, double, or float for operator \"<\"."
	.align 8
.LC99:
	.string	"Expected signed, unsigned, double, or float for operator \"<=\"."
	.align 8
.LC100:
	.string	"Expected signed, unsigned, double, or float for operator \">\"."
	.align 8
.LC101:
	.string	"Expected signed, unsigned, double, or float for operator \">=\"."
	.section	.text._ZN2v88internal4wasm11AsmJsParser20RelationalExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser20RelationalExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser20RelationalExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser20RelationalExpressionEv:
.LFB18247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1817
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15ShiftExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r12
	jne	.L1744
	leaq	8(%rbx), %r13
.L1745:
	movl	16(%rbx), %eax
	cmpl	$60, %eax
	je	.L1746
	jg	.L1747
	cmpl	$-9944, %eax
	je	.L1748
	cmpl	$-9943, %eax
	jne	.L1741
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1817
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15ShiftExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r14
	jne	.L1744
	movl	$1825, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1787
.L1791:
	movl	$2817, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1789
	movl	$2817, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1818
.L1789:
	movl	$237, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1793
	movl	$237, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1819
.L1793:
	movl	$57357, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1796
	movl	$57357, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1796
	movq	304(%rbx), %rdi
	movl	$96, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1747:
	cmpl	$62, %eax
	jne	.L1741
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1817
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15ShiftExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r14
	jne	.L1744
	movl	$1825, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1775
.L1779:
	movl	$2817, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1777
	movl	$2817, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1820
.L1777:
	movl	$237, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1781
	movl	$237, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1821
.L1781:
	movl	$57357, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1784
	movl	$57357, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1784
	movq	304(%rbx), %rdi
	movl	$94, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1744:
	xorl	%r12d, %r12d
.L1741:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1817:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	xorl	%r12d, %r12d
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1748:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1817
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15ShiftExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r14
	jne	.L1744
	movl	$1825, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1763
.L1767:
	movl	$2817, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1765
	movl	$2817, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1822
.L1765:
	movl	$237, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1769
	movl	$237, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1823
.L1769:
	movl	$57357, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1772
	movl	$57357, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1772
	movq	304(%rbx), %rdi
	movl	$95, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1746:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1817
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15ShiftExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r14
	jne	.L1744
	movl	$1825, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1824
.L1752:
	movl	$2817, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1758
	movl	$2817, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1825
.L1758:
	movl	$237, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1756
	movl	$237, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1826
.L1756:
	movl	$57357, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1760
	movl	$57357, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1760
	movq	304(%rbx), %rdi
	movl	$93, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	.p2align 4,,10
	.p2align 3
.L1753:
	movl	$769, %r12d
	jmp	.L1745
	.p2align 4,,10
	.p2align 3
.L1775:
	movl	$1825, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1779
	movq	304(%rbx), %rdi
	movl	$74, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1824:
	movl	$1825, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1752
	movq	304(%rbx), %rdi
	movl	$72, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1763:
	movl	$1825, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1767
	movq	304(%rbx), %rdi
	movl	$76, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1787:
	movl	$1825, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1791
	movq	304(%rbx), %rdi
	movl	$78, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1772:
	leaq	.LC99(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1796:
	leaq	.LC101(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1760:
	leaq	.LC98(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1784:
	leaq	.LC100(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1818:
	movq	304(%rbx), %rdi
	movl	$79, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1820:
	movq	304(%rbx), %rdi
	movl	$75, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1822:
	movq	304(%rbx), %rdi
	movl	$77, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1825:
	movq	304(%rbx), %rdi
	movl	$73, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1823:
	movq	304(%rbx), %rdi
	movl	$101, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1826:
	movq	304(%rbx), %rdi
	movl	$99, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1819:
	movq	304(%rbx), %rdi
	movl	$102, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1821:
	movq	304(%rbx), %rdi
	movl	$100, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1753
	.cfi_endproc
.LFE18247:
	.size	_ZN2v88internal4wasm11AsmJsParser20RelationalExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser20RelationalExpressionEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser18EqualityExpressionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC102:
	.string	"Expected signed, unsigned, double, or float for operator \"==\"."
	.align 8
.LC103:
	.string	"Expected signed, unsigned, double, or float for operator \"!=\"."
	.section	.text._ZN2v88internal4wasm11AsmJsParser18EqualityExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser18EqualityExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser18EqualityExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser18EqualityExpressionEv:
.LFB18248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1870
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20RelationalExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r12
	jne	.L1830
	leaq	8(%rbx), %r14
.L1831:
	movl	16(%rbx), %eax
	cmpl	$-9942, %eax
	je	.L1832
	cmpl	$-9941, %eax
	jne	.L1827
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1870
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20RelationalExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r13
	jne	.L1830
	movl	$1825, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1850
	movl	$1825, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1853
.L1850:
	movl	$2817, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1848
	movl	$2817, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1853
.L1848:
	movl	$237, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1851
.L1852:
	movl	$57357, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1855
	movl	$57357, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1855
	movq	304(%rbx), %rdi
	movl	$92, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1836
	.p2align 4,,10
	.p2align 3
.L1830:
	xorl	%r12d, %r12d
.L1827:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1832:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1870
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20RelationalExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r13
	jne	.L1830
	movl	$1825, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1835
	movl	$1825, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1840
.L1835:
	movl	$2817, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1841
	movl	$2817, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1840
.L1841:
	movl	$237, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L1838
.L1839:
	movl	$57357, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1843
	movl	$57357, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1843
	movq	304(%rbx), %rdi
	movl	$91, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
.L1836:
	movl	$769, %r12d
	jmp	.L1831
	.p2align 4,,10
	.p2align 3
.L1870:
	leaq	.LC55(%rip), %rax
	xorl	%r12d, %r12d
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1851:
	.cfi_restore_state
	movl	$237, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1852
	movq	304(%rbx), %rdi
	movl	$98, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1836
	.p2align 4,,10
	.p2align 3
.L1838:
	movl	$237, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1839
	movq	304(%rbx), %rdi
	movl	$97, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1836
	.p2align 4,,10
	.p2align 3
.L1855:
	leaq	.LC103(%rip), %rax
	xorl	%r12d, %r12d
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1843:
	.cfi_restore_state
	leaq	.LC102(%rip), %rax
	xorl	%r12d, %r12d
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1853:
	.cfi_restore_state
	movq	304(%rbx), %rdi
	movl	$71, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1836
	.p2align 4,,10
	.p2align 3
.L1840:
	movq	304(%rbx), %rdi
	movl	$70, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L1836
	.cfi_endproc
.LFE18248:
	.size	_ZN2v88internal4wasm11AsmJsParser18EqualityExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser18EqualityExpressionEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser20BitwiseANDExpressionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC104:
	.string	"Expected intish for operator &."
	.section	.text._ZN2v88internal4wasm11AsmJsParser20BitwiseANDExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser20BitwiseANDExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser20BitwiseANDExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser20BitwiseANDExpressionEv:
.LFB18249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1885
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser18EqualityExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r12
	jne	.L1886
	cmpl	$38, 16(%rbx)
	je	.L1887
.L1871:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1885:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L1886:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1887:
	.cfi_restore_state
	leaq	8(%rbx), %r14
.L1879:
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1885
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser18EqualityExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r13
	jne	.L1886
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1878
	movl	$257, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1878
	movq	304(%rbx), %rdi
	movl	$113, %esi
	movl	$1825, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	cmpl	$38, 16(%rbx)
	je	.L1879
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1878:
	.cfi_restore_state
	leaq	.LC104(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1871
	.cfi_endproc
.LFE18249:
	.size	_ZN2v88internal4wasm11AsmJsParser20BitwiseANDExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser20BitwiseANDExpressionEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser20BitwiseXORExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser20BitwiseXORExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser20BitwiseXORExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser20BitwiseXORExpressionEv:
.LFB18250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1914
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1913
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser18EqualityExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r12
	jne	.L1892
	movl	16(%rbx), %eax
	cmpl	$38, %eax
	je	.L1916
.L1894:
	cmpl	$94, %eax
	je	.L1917
.L1888:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1913:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L1892:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1914:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	xorl	%r12d, %r12d
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1917:
	.cfi_restore_state
	leaq	8(%rbx), %r14
.L1902:
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1914
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20BitwiseANDExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r13
	jne	.L1892
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1901
	movl	$257, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1901
	movq	304(%rbx), %rdi
	movl	$115, %esi
	movl	$1825, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	cmpl	$94, 16(%rbx)
	je	.L1902
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1916:
	.cfi_restore_state
	leaq	8(%rbx), %r13
.L1898:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L1913
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser18EqualityExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r14
	jne	.L1892
	movl	$257, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1897
	movl	$257, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1897
	movq	304(%rbx), %rdi
	movl	$113, %esi
	movl	$1825, %r12d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movl	16(%rbx), %eax
	cmpl	$38, %eax
	je	.L1898
	cmpb	$0, 540(%rbx)
	je	.L1894
	jmp	.L1892
	.p2align 4,,10
	.p2align 3
.L1901:
	leaq	.LC104(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1888
.L1897:
	leaq	.LC104(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L1892
	.cfi_endproc
.LFE18250:
	.size	_ZN2v88internal4wasm11AsmJsParser20BitwiseXORExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser20BitwiseXORExpressionEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser19BitwiseORExpressionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC105:
	.string	"Expected |0 type annotation for call"
	.align 8
.LC106:
	.string	"Expected intish for operator |."
	.section	.text._ZN2v88internal4wasm11AsmJsParser19BitwiseORExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser19BitwiseORExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser19BitwiseORExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser19BitwiseORExpressionEv:
.LFB18251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rdi), %rax
	movq	%rax, 704(%rdi)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L1946
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20BitwiseXORExpressionEv
	cmpb	$0, 540(%r14)
	movq	%rax, %r15
	jne	.L1921
	cmpl	$124, 16(%r14)
	leaq	8(%r14), %r13
	je	.L1922
.L1918:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1946:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%r14)
	xorl	%r15d, %r15d
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L1918
	.p2align 4,,10
	.p2align 3
.L1934:
	xorl	%ebx, %ebx
.L1923:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L1946
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20BitwiseXORExpressionEv
	cmpb	$0, 540(%r14)
	movq	%rax, %r9
	jne	.L1921
	testb	%bl, %bl
	je	.L1925
	movq	-64(%rbp), %rax
	cmpq	32(%r14), %rax
	je	.L1926
.L1925:
	testb	%r12b, %r12b
	jne	.L1947
	movl	$257, %esi
	movq	%r15, %rdi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	movq	-56(%rbp), %r9
	testb	%al, %al
	je	.L1931
	movl	$257, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1931
	movq	304(%r14), %rdi
	movl	$114, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
.L1929:
	cmpl	$124, 16(%r14)
	movl	$1825, %r15d
	jne	.L1918
	.p2align 4,,10
	.p2align 3
.L1922:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	696(%r14), %rdi
	movl	$1825, %esi
	call	_ZN2v88internal4wasm7AsmType9IsExactlyEPS2_S3_@PLT
	movl	$257, %esi
	movq	%r15, %rdi
	movq	$0, 696(%r14)
	movl	%eax, %r12d
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	movl	%eax, %ebx
	testb	%al, %al
	je	.L1923
	cmpl	$-3, 16(%r14)
	jne	.L1934
	movl	288(%r14), %eax
	testl	%eax, %eax
	jne	.L1934
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	32(%r14), %rax
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	movq	304(%r14), %rax
	movq	80(%rax), %rdx
	subq	72(%rax), %rdx
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal12AsmJsScanner6RewindEv@PLT
	jmp	.L1923
	.p2align 4,,10
	.p2align 3
.L1921:
	xorl	%r15d, %r15d
	jmp	.L1918
.L1926:
	movq	304(%r14), %rdi
	movq	-72(%rbp), %rsi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder15DeleteCodeAfterEm@PLT
	jmp	.L1929
.L1931:
	leaq	.LC106(%rip), %rax
	movb	$1, 540(%r14)
	xorl	%r15d, %r15d
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L1918
.L1947:
	leaq	.LC105(%rip), %rax
	movb	$1, 540(%r14)
	xorl	%r15d, %r15d
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L1918
	.cfi_endproc
.LFE18251:
	.size	_ZN2v88internal4wasm11AsmJsParser19BitwiseORExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser19BitwiseORExpressionEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser21ConditionalExpressionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC107:
	.string	"Expected int in condition of ternary operator."
	.align 8
.LC108:
	.string	"Type mismatch in ternary operator."
	.section	.text._ZN2v88internal4wasm11AsmJsParser21ConditionalExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser21ConditionalExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser21ConditionalExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser21ConditionalExpressionEv:
.LFB18252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L1996
	movq	32(%r14), %rax
	movq	%rax, 704(%r14)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L1994
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20BitwiseXORExpressionEv
	cmpb	$0, 540(%r14)
	movq	%rax, %r15
	jne	.L1952
	movl	16(%r14), %eax
	leaq	8(%r14), %r13
	cmpl	$124, %eax
	je	.L1955
	cmpl	$63, %eax
	je	.L1997
.L1948:
	addq	$40, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1994:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
.L1952:
	xorl	%r15d, %r15d
	jmp	.L1948
	.p2align 4,,10
	.p2align 3
.L1996:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%r14)
	xorl	%r15d, %r15d
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L1948
	.p2align 4,,10
	.p2align 3
.L1955:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	696(%r14), %rdi
	movl	$1825, %esi
	call	_ZN2v88internal4wasm7AsmType9IsExactlyEPS2_S3_@PLT
	movl	$257, %esi
	movq	%r15, %rdi
	movq	$0, 696(%r14)
	movl	%eax, %r12d
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	movl	%eax, %ebx
	testb	%al, %al
	je	.L1957
	cmpl	$-3, 16(%r14)
	jne	.L1979
	movl	288(%r14), %eax
	testl	%eax, %eax
	jne	.L1979
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	32(%r14), %rax
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	movq	304(%r14), %rax
	movq	80(%rax), %rdx
	subq	72(%rax), %rdx
	movq	%rdx, -72(%rbp)
	call	_ZN2v88internal12AsmJsScanner6RewindEv@PLT
	.p2align 4,,10
	.p2align 3
.L1957:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L1994
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20BitwiseXORExpressionEv
	cmpb	$0, 540(%r14)
	movq	%rax, %r9
	jne	.L1952
	testb	%bl, %bl
	jne	.L1998
.L1959:
	testb	%r12b, %r12b
	jne	.L1999
	movl	$257, %esi
	movq	%r15, %rdi
	movq	%r9, -56(%rbp)
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	movq	-56(%rbp), %r9
	testb	%al, %al
	je	.L1965
	movl	$257, %esi
	movq	%r9, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1965
	movq	304(%r14), %rdi
	movl	$114, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
.L1963:
	movl	16(%r14), %eax
	movl	$1825, %r15d
	cmpl	$124, %eax
	je	.L1955
	cmpb	$0, 540(%r14)
	jne	.L1952
	cmpl	$63, %eax
	jne	.L1948
.L1997:
	leaq	8(%r14), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	$769, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2000
	movq	304(%r14), %rdi
	movl	$127, %edx
	movl	$4, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	movq	304(%r14), %rax
	movq	72(%rax), %rbx
	movq	80(%rax), %r13
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L1996
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv
	cmpb	$0, 540(%r14)
	movq	%rax, %r15
	jne	.L1952
	movq	304(%r14), %rdi
	movl	$5, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	cmpl	$58, 16(%r14)
	jne	.L2001
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L1996
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv
	cmpb	$0, 540(%r14)
	movq	%rax, %r12
	jne	.L1952
	movq	304(%r14), %rdi
	movl	$11, %esi
	subq	%rbx, %r13
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movl	$769, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1974
	movl	$769, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1974
	movq	304(%r14), %rax
	movl	$769, %r15d
	movq	72(%rax), %rax
	movb	$127, -1(%rax,%r13)
	jmp	.L1948
	.p2align 4,,10
	.p2align 3
.L1998:
	movq	-64(%rbp), %rax
	cmpq	%rax, 32(%r14)
	jne	.L1959
	movq	304(%r14), %rdi
	movq	-72(%rbp), %rsi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder15DeleteCodeAfterEm@PLT
	jmp	.L1963
	.p2align 4,,10
	.p2align 3
.L1979:
	xorl	%ebx, %ebx
	jmp	.L1957
	.p2align 4,,10
	.p2align 3
.L2000:
	leaq	.LC107(%rip), %rax
	movb	$1, 540(%r14)
	xorl	%r15d, %r15d
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L1948
	.p2align 4,,10
	.p2align 3
.L2001:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%r14)
	xorl	%r15d, %r15d
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L1948
.L1974:
	movl	$237, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1972
	movl	$237, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1972
	movq	304(%r14), %rax
	movl	$237, %r15d
	movq	72(%rax), %rax
	movb	$124, -1(%rax,%r13)
	jmp	.L1948
.L1972:
	movl	$57357, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1976
	movl	$57357, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L1976
	movq	304(%r14), %rax
	movl	$57357, %r15d
	movq	72(%rax), %rax
	movb	$125, -1(%rax,%r13)
	jmp	.L1948
.L1965:
	leaq	.LC106(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L1952
.L1976:
	leaq	.LC108(%rip), %rax
	movb	$1, 540(%r14)
	xorl	%r15d, %r15d
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L1948
.L1999:
	leaq	.LC105(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L1952
	.cfi_endproc
.LFE18252:
	.size	_ZN2v88internal4wasm11AsmJsParser21ConditionalExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser21ConditionalExpressionEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv.str1.1,"aMS",@progbits,1
.LC109:
	.string	"Invalid assignment target"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC110:
	.string	"Illegal type stored to heap view"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv.str1.1
.LC111:
	.string	"Undeclared assignment target"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv.str1.8
	.align 8
.LC112:
	.string	"Expected mutable variable in assignment"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv.str1.1
.LC113:
	.string	"Type mismatch in assignment"
	.section	.text._ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv:
.LFB18242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movl	16(%rdi), %esi
	movq	%rdi, %rbx
	cmpl	$255, %esi
	jg	.L2062
.L2003:
	leal	9999(%rsi), %eax
	cmpl	$10254, %eax
	jbe	.L2061
	movq	%rbx, %rdi
	leaq	8(%rbx), %r13
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movq	%r13, %rdi
	movq	(%rax), %r12
	movq	%rax, %r14
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$61, 16(%rbx)
	movq	%r13, %rdi
	je	.L2063
	call	_ZN2v88internal12AsmJsScanner6RewindEv@PLT
.L2061:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2028
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser21ConditionalExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r12
	jne	.L2056
.L2002:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2028:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L2056:
	xorl	%r12d, %r12d
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2062:
	.cfi_restore_state
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movl	$3, %esi
	movq	(%rax), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2004
	movl	16(%rbx), %esi
	jmp	.L2003
	.p2align 4,,10
	.p2align 3
.L2063:
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	32(%r14), %eax
	testl	%eax, %eax
	je	.L2064
	cmpb	$0, 36(%r14)
	je	.L2065
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2028
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %rdi
	jne	.L2056
	movq	%r12, %rsi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2066
	movl	32(%r14), %eax
	cmpl	$1, %eax
	je	.L2067
	cmpl	$2, %eax
	jne	.L2039
	movq	304(%rbx), %rdi
	movl	28(%r14), %edx
	movl	$36, %esi
	addl	752(%rbx), %edx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU32VENS1_10WasmOpcodeEj@PLT
	movq	304(%rbx), %rdi
	movl	28(%r14), %edx
	movl	$35, %esi
	addl	752(%rbx), %edx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithU32VENS1_10WasmOpcodeEj@PLT
	jmp	.L2002
	.p2align 4,,10
	.p2align 3
.L2004:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2028
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser21ConditionalExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r12
	jne	.L2056
	cmpl	$61, 16(%rbx)
	jne	.L2002
	cmpb	$0, 568(%rbx)
	je	.L2068
	movb	$0, 568(%rbx)
	leaq	8(%rbx), %rdi
	movq	576(%rbx), %r14
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2028
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r13
	jne	.L2056
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2069
	movl	$4194307, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2070
.L2016:
	movl	$8388611, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2071
.L2019:
	movl	$131075, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2057
	movl	$65539, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2057
	movl	$524291, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2058
	movl	$262147, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2058
	movl	$2097155, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2059
	movl	$1048579, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2059
	movl	$4194307, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2072
	movl	$8388611, %esi
	movq	%r14, %rdi
	movq	%r13, %r12
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2002
	movq	304(%rbx), %rdi
	movl	$226, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2002
	.p2align 4,,10
	.p2align 3
.L2065:
	leaq	.LC112(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2002
	.p2align 4,,10
	.p2align 3
.L2064:
	leaq	.LC111(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2002
.L2066:
	leaq	.LC113(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2002
.L2068:
	leaq	.LC109(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2002
.L2067:
	movl	28(%r14), %esi
	movq	304(%rbx), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitTeeLocalEj@PLT
	jmp	.L2002
.L2057:
	movq	304(%rbx), %rdi
	movl	$222, %esi
	movq	%r13, %r12
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2002
.L2069:
	leaq	.LC110(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2002
.L2071:
	movl	$24589, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2019
	movq	304(%rbx), %rdi
	movl	$187, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2019
.L2070:
	movl	$77, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2016
	movq	304(%rbx), %rdi
	movl	$182, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2016
.L2058:
	movq	304(%rbx), %rdi
	movl	$223, %esi
	movq	%r13, %r12
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2002
.L2059:
	movq	304(%rbx), %rdi
	movl	$224, %esi
	movq	%r13, %r12
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2002
.L2072:
	movq	304(%rbx), %rdi
	movl	$225, %esi
	movq	%r13, %r12
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2002
.L2039:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18242:
	.size	_ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE.str1.1,"aMS",@progbits,1
.LC114:
	.string	"Expected actual type"
.LC115:
	.string	"Unexpected type"
	.section	.text._ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	.type	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE, @function
_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE:
.LFB18237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
.L2081:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2087
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser20AssignmentExpressionEv
	cmpb	$0, 540(%rbx)
	movq	%rax, %r12
	jne	.L2082
	cmpl	$44, 16(%rbx)
	jne	.L2076
	movl	$2147483649, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2088
	movl	$17, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2089
	cmpl	$44, 16(%rbx)
	je	.L2080
.L2079:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L2073:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2089:
	.cfi_restore_state
	movq	304(%rbx), %rdi
	movl	$26, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	cmpl	$44, 16(%rbx)
	jne	.L2079
.L2080:
	movq	%r14, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2087:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2082:
	xorl	%r12d, %r12d
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2076:
	testq	%r13, %r13
	je	.L2073
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2073
	leaq	.LC115(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2088:
	leaq	.LC114(%rip), %rax
	movb	$1, 540(%rbx)
	xorl	%r12d, %r12d
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2073
	.cfi_endproc
.LFE18237:
	.size	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE, .-_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv.part.0.str1.1,"aMS",@progbits,1
.LC116:
	.string	"Invalid return type"
.LC117:
	.string	"Invalid void return type"
	.section	.text._ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv.part.0, @function
_ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv.part.0:
.LFB23476:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %eax
	cmpl	$59, %eax
	je	.L2091
	cmpl	$125, %eax
	jne	.L2114
.L2091:
	movq	312(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2115
	movl	$17, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2116
.L2099:
	movq	304(%rbx), %rdi
	movl	$15, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movl	16(%rbx), %eax
	cmpl	$59, %eax
	je	.L2102
	cmpl	$125, %eax
	je	.L2090
	cmpb	$0, 292(%rbx)
	jne	.L2090
	leaq	.LC2(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L2090:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2114:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jnb	.L2117
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2115:
	.cfi_restore_state
	movq	$17, 312(%rbx)
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2117:
	movq	312(%rbx), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%rbx)
	movq	%rax, %r13
	jne	.L2090
	movl	$237, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2098
	movq	$237, 312(%rbx)
	jmp	.L2099
	.p2align 4,,10
	.p2align 3
.L2102:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	.p2align 4,,10
	.p2align 3
.L2116:
	.cfi_restore_state
	leaq	.LC117(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2098:
	.cfi_restore_state
	movl	$57357, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2100
	movq	$57357, 312(%rbx)
	jmp	.L2099
.L2100:
	movl	$1825, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2101
	movq	$1825, 312(%rbx)
	jmp	.L2099
.L2101:
	leaq	.LC116(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2090
	.cfi_endproc
.LFE23476:
	.size	_ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv.part.0, .-_ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv.part.0
	.section	.text._ZN2v88internal4wasm11AsmJsParser18ValidateExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser18ValidateExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser18ValidateExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser18ValidateExpressionEv:
.LFB18236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2122
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%rbx)
	movl	$0, %edx
	cmovne	%rdx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2122:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18236:
	.size	_ZN2v88internal4wasm11AsmJsParser18ValidateExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser18ValidateExpressionEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser23ParenthesizedExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser23ParenthesizedExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser23ParenthesizedExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser23ParenthesizedExpressionEv:
.LFB18253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	cmpl	$40, 16(%rdi)
	movq	$0, 680(%rdi)
	jne	.L2130
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2131
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%rbx)
	jne	.L2128
	cmpl	$41, 16(%rbx)
	jne	.L2130
	movq	%r12, %rdi
	movq	%rax, -24(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	-24(%rbp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2130:
	.cfi_restore_state
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	xorl	%eax, %eax
.L2123:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2131:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2128:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L2123
	.cfi_endproc
.LFE18253:
	.size	_ZN2v88internal4wasm11AsmJsParser23ParenthesizedExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser23ParenthesizedExpressionEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser19ExpressionStatementEv.str1.1,"aMS",@progbits,1
.LC118:
	.string	"Double label unsupported"
	.section	.text._ZN2v88internal4wasm11AsmJsParser19ExpressionStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser19ExpressionStatementEv
	.type	_ZN2v88internal4wasm11AsmJsParser19ExpressionStatementEv, @function
_ZN2v88internal4wasm11AsmJsParser19ExpressionStatementEv:
.LFB18222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	16(%rdi), %eax
	movq	%rdi, %r12
	addl	$9999, %eax
	cmpl	$10254, %eax
	jbe	.L2133
	leaq	8(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$58, 16(%r12)
	movq	%r13, %rdi
	je	.L2154
	call	_ZN2v88internal12AsmJsScanner6RewindEv@PLT
.L2133:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jnb	.L2140
.L2153:
	movb	$1, 540(%r12)
	leaq	.LC55(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
.L2132:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2140:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L2153
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%r12)
	movq	%rax, %rdi
	jne	.L2132
	movl	$17, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2155
.L2146:
	movl	16(%r12), %eax
	cmpl	$59, %eax
	je	.L2144
	cmpl	$125, %eax
	je	.L2132
	cmpb	$0, 292(%r12)
	jne	.L2132
	movb	$1, 540(%r12)
	leaq	.LC2(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	jmp	.L2132
	.p2align 4,,10
	.p2align 3
.L2154:
	call	_ZN2v88internal12AsmJsScanner6RewindEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L2153
	movl	724(%r12), %eax
	testl	%eax, %eax
	je	.L2137
	movb	$1, 540(%r12)
	leaq	.LC118(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	jmp	.L2132
	.p2align 4,,10
	.p2align 3
.L2155:
	movq	304(%r12), %rdi
	movl	$26, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2146
	.p2align 4,,10
	.p2align 3
.L2137:
	movl	16(%r12), %eax
	movq	%r13, %rdi
	movl	%eax, 724(%r12)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$58, 16(%r12)
	je	.L2138
	movb	$1, 540(%r12)
	leaq	.LC0(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	jmp	.L2132
	.p2align 4,,10
	.p2align 3
.L2144:
	leaq	8(%r12), %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	.p2align 4,,10
	.p2align 3
.L2138:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L2153
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv
	.cfi_endproc
.LFE18222:
	.size	_ZN2v88internal4wasm11AsmJsParser19ExpressionStatementEv, .-_ZN2v88internal4wasm11AsmJsParser19ExpressionStatementEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser15ValidateDefaultEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser15ValidateDefaultEv
	.type	_ZN2v88internal4wasm11AsmJsParser15ValidateDefaultEv, @function
_ZN2v88internal4wasm11AsmJsParser15ValidateDefaultEv:
.LFB18235:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpl	$-9956, 16(%rdi)
	movq	%rdi, %rbx
	je	.L2157
.L2189:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L2156:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2157:
	.cfi_restore_state
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$58, 16(%rbx)
	jne	.L2189
.L2159:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
.L2187:
	cmpb	$0, 540(%rbx)
	jne	.L2156
	cmpl	$125, 16(%rbx)
	je	.L2156
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2188
	movq	$0, 680(%rbx)
	movl	16(%rbx), %eax
	cmpl	$123, %eax
	je	.L2190
	cmpl	$59, %eax
	je	.L2191
	cmpl	$-9950, %eax
	je	.L2192
	cmpl	$-9948, %eax
	je	.L2193
	cmpl	$-9945, %eax
	je	.L2194
	cmpl	$-9955, %eax
	je	.L2195
	cmpl	$-9952, %eax
	je	.L2196
	cmpl	$-9960, %eax
	je	.L2197
	cmpl	$-9957, %eax
	je	.L2198
	cmpl	$-9947, %eax
	je	.L2199
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jnb	.L2184
.L2188:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2190:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2188
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser5BlockEv
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2193:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2188
	cmpl	$-9948, 16(%rbx)
	jne	.L2189
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv.part.0
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2191:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2188
	cmpl	$59, 16(%rbx)
	jne	.L2189
	jmp	.L2159
	.p2align 4,,10
	.p2align 3
.L2192:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2188
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser11IfStatementEv
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2196:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser12ForStatementEv.part.0
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2194:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser14WhileStatementEv
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2195:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser11DoStatementEv
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2184:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser19ExpressionStatementEv
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2199:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2188
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15SwitchStatementEv
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2197:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2188
	cmpl	$-9960, 16(%rbx)
	jne	.L2189
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser14BreakStatementEv.part.0
	jmp	.L2187
	.p2align 4,,10
	.p2align 3
.L2198:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2188
	cmpl	$-9957, 16(%rbx)
	jne	.L2189
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv.part.0
	jmp	.L2187
	.cfi_endproc
.LFE18235:
	.size	_ZN2v88internal4wasm11AsmJsParser15ValidateDefaultEv, .-_ZN2v88internal4wasm11AsmJsParser15ValidateDefaultEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser15SwitchStatementEv.str1.8,"aMS",@progbits,1
	.align 8
.LC119:
	.string	"Expected signed for switch value"
	.section	.text._ZN2v88internal4wasm11AsmJsParser15SwitchStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser15SwitchStatementEv
	.type	_ZN2v88internal4wasm11AsmJsParser15SwitchStatementEv, @function
_ZN2v88internal4wasm11AsmJsParser15SwitchStatementEv:
.LFB18233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-9947, 16(%rdi)
	je	.L2201
.L2246:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
.L2200:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2247
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2201:
	.cfi_restore_state
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$40, 16(%r14)
	jne	.L2246
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jnb	.L2204
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L2200
	.p2align 4,,10
	.p2align 3
.L2204:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%r14)
	movq	%rax, %rdi
	jne	.L2200
	movl	$1825, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2248
	cmpl	$41, 16(%r14)
	jne	.L2246
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	532(%r14), %eax
	testl	%eax, %eax
	jg	.L2207
	movl	$1, 532(%r14)
.L2207:
	movl	528(%r14), %ebx
	movq	304(%r14), %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitSetLocalEj@PLT
	movl	724(%r14), %eax
	leaq	584(%r14), %rcx
	movl	$0, -96(%rbp)
	movq	%rcx, -120(%rbp)
	movq	600(%r14), %rsi
	movl	%eax, -92(%rbp)
	cmpq	608(%r14), %rsi
	je	.L2208
	movl	%eax, 4(%rsi)
	leaq	-96(%rbp), %rax
	movl	$0, (%rsi)
	addq	$8, 600(%r14)
	movq	%rax, -136(%rbp)
.L2209:
	movq	304(%r14), %rdi
	movl	$64, %edx
	movl	$2, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	movq	496(%r14), %rax
	movl	$0, 724(%r14)
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	leaq	496(%r14), %rax
	movq	%rax, -64(%rbp)
	movq	512(%r14), %rax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	cmpq	504(%r14), %rax
	je	.L2211
	movdqu	-24(%rax), %xmm0
	movq	$0, -24(%rax)
	movq	-80(%rbp), %rsi
	movq	-8(%rax), %rdx
	movq	%rsi, -16(%rax)
	movq	-72(%rbp), %rsi
	movq	%rsi, -8(%rax)
	movq	%xmm0, %rax
	subq	$32, 512(%r14)
	movq	%rdx, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	cmpq	-80(%rbp), %rax
	je	.L2211
	movq	%xmm0, -80(%rbp)
.L2211:
	movq	-136(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser11GatherCasesEPNS0_10ZoneVectorIiEE
	cmpl	$123, 16(%r14)
	je	.L2213
.L2226:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
.L2214:
	movq	-64(%rbp), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L2233
.L2244:
	movq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-72(%rbp), %rax
	movq	%rax, 24(%rsi)
	movaps	%xmm0, -80(%rbp)
	addq	$32, 16(%rdi)
	jmp	.L2200
.L2248:
	leaq	.LC119(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L2200
.L2213:
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	-80(%rbp), %r13
	subq	-88(%rbp), %r13
	leaq	-104(%rbp), %rax
	sarq	$2, %r13
	movq	%rax, -128(%rbp)
	cmpq	$-1, %r13
	jne	.L2215
	jmp	.L2219
	.p2align 4,,10
	.p2align 3
.L2249:
	movq	$3, (%rsi)
	addq	$8, 600(%r14)
.L2242:
	movq	304(%r14), %rdi
	movl	$64, %edx
	movl	$2, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	leaq	1(%r15), %rdx
	cmpq	%r15, %r13
	je	.L2219
	movq	%rdx, %r15
.L2215:
	movq	$3, -104(%rbp)
	movq	600(%r14), %rsi
	cmpq	608(%r14), %rsi
	jne	.L2249
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2242
.L2219:
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, -120(%rbp)
	movq	%rax, %rcx
	movq	-80(%rbp), %rax
	cmpq	%rcx, %rax
	je	.L2217
	leaq	-4(%rax), %r13
	subq	-120(%rbp), %r13
	shrq	$2, %r13
	movq	%r13, -128(%rbp)
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L2221:
	movq	-120(%rbp), %rax
	movq	304(%r14), %rdi
	movl	%ebx, %esi
	movl	(%rax,%r13,4), %r15d
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitGetLocalEj@PLT
	movq	304(%r14), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movq	304(%r14), %rdi
	movl	$70, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	304(%r14), %rdi
	movl	%r13d, %edx
	movl	$13, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithI32VENS1_10WasmOpcodeEi@PLT
	movq	%r13, %rdx
	addq	$1, %r13
	cmpq	-128(%rbp), %rdx
	jne	.L2221
	movl	-128(%rbp), %edx
	addl	$1, %edx
.L2217:
	movq	304(%r14), %rdi
	movl	$12, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithI32VENS1_10WasmOpcodeEi@PLT
	cmpb	$0, 540(%r14)
	movq	304(%r14), %rdi
	je	.L2229
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2250:
	movl	$11, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	subq	$8, 600(%r14)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L2243
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser12ValidateCaseEv
	cmpb	$0, 540(%r14)
	jne	.L2214
	movq	304(%r14), %rdi
.L2229:
	cmpl	$-9959, 16(%r14)
	je	.L2250
.L2222:
	movl	$11, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	subq	$8, 600(%r14)
	cmpl	$-9956, 16(%r14)
	je	.L2225
.L2231:
	cmpl	$125, 16(%r14)
	jne	.L2226
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	subq	$8, 600(%r14)
	movl	$11, %esi
	movq	304(%r14), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	-64(%rbp), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	jne	.L2244
.L2233:
	movq	-136(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal10ZoneVectorIiEENS1_13ZoneAllocatorIS3_EEE17_M_realloc_insertIJS3_EEEvN9__gnu_cxx17__normal_iteratorIPS3_S6_EEDpOT_
	jmp	.L2200
.L2208:
	leaq	-96(%rbp), %rax
	leaq	584(%r14), %rdi
	movq	%rax, %rdx
	movq	%rax, -136(%rbp)
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2209
.L2243:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L2214
.L2247:
	call	__stack_chk_fail@PLT
.L2225:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L2243
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15ValidateDefaultEv
	cmpb	$0, 540(%r14)
	je	.L2231
	jmp	.L2214
	.cfi_endproc
.LFE18233:
	.size	_ZN2v88internal4wasm11AsmJsParser15SwitchStatementEv, .-_ZN2v88internal4wasm11AsmJsParser15SwitchStatementEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser5BlockEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser5BlockEv
	.type	_ZN2v88internal4wasm11AsmJsParser5BlockEv, @function
_ZN2v88internal4wasm11AsmJsParser5BlockEv:
.LFB18221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	724(%rdi), %r12d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%r12d, %r12d
	jne	.L2302
	movl	$0, 724(%rbx)
	cmpl	$123, 16(%rbx)
	je	.L2255
.L2261:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L2251:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2303
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2302:
	.cfi_restore_state
	movl	$2, -48(%rbp)
	movq	600(%rdi), %rsi
	movl	%r12d, -44(%rbp)
	cmpq	608(%rdi), %rsi
	je	.L2253
	movl	$2, (%rsi)
	movl	%r12d, 4(%rsi)
	addq	$8, 600(%rdi)
.L2254:
	movq	304(%rbx), %rdi
	movl	$64, %edx
	movl	$2, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	cmpl	$123, 16(%rbx)
	movl	$0, 724(%rbx)
	jne	.L2261
.L2255:
	leaq	8(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpb	$0, 540(%rbx)
	movl	16(%rbx), %eax
	jne	.L2257
	.p2align 4,,10
	.p2align 3
.L2286:
	cmpl	$125, %eax
	je	.L2258
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2301
	movq	$0, 680(%rbx)
	movl	16(%rbx), %eax
	cmpl	$123, %eax
	je	.L2304
	cmpl	$59, %eax
	je	.L2305
	cmpl	$-9950, %eax
	je	.L2306
	cmpl	$-9948, %eax
	je	.L2307
	cmpl	$-9945, %eax
	je	.L2308
	cmpl	$-9955, %eax
	je	.L2309
	cmpl	$-9952, %eax
	je	.L2310
	cmpl	$-9960, %eax
	je	.L2311
	cmpl	$-9957, %eax
	je	.L2312
	cmpl	$-9947, %eax
	je	.L2313
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jnb	.L2285
	.p2align 4,,10
	.p2align 3
.L2301:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2304:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2301
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser5BlockEv
.L2265:
	cmpb	$0, 540(%rbx)
	jne	.L2251
	movl	16(%rbx), %eax
	jmp	.L2286
	.p2align 4,,10
	.p2align 3
.L2307:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2301
	cmpl	$-9948, 16(%rbx)
	jne	.L2261
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv.part.0
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2305:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2301
	cmpl	$59, 16(%rbx)
	jne	.L2261
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2306:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2301
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser11IfStatementEv
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2310:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser12ForStatementEv.part.0
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2285:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser19ExpressionStatementEv
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2308:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser14WhileStatementEv
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2309:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser11DoStatementEv
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2257:
	cmpl	$125, %eax
	jne	.L2261
	.p2align 4,,10
	.p2align 3
.L2258:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	testl	%r12d, %r12d
	je	.L2251
	subq	$8, 600(%rbx)
	movq	304(%rbx), %rdi
	movl	$11, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2313:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2301
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15SwitchStatementEv
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2311:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2301
	cmpl	$-9960, 16(%rbx)
	jne	.L2261
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser14BreakStatementEv.part.0
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2312:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2301
	cmpl	$-9957, 16(%rbx)
	jne	.L2261
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv.part.0
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2253:
	leaq	-48(%rbp), %rdx
	leaq	584(%rdi), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2254
.L2303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18221:
	.size	_ZN2v88internal4wasm11AsmJsParser5BlockEv, .-_ZN2v88internal4wasm11AsmJsParser5BlockEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv
	.type	_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv, @function
_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv:
.LFB18220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	16(%rdi), %eax
	movq	$0, 680(%rdi)
	cmpl	$123, %eax
	je	.L2341
	cmpl	$59, %eax
	je	.L2342
	cmpl	$-9950, %eax
	je	.L2343
	cmpl	$-9948, %eax
	je	.L2344
	cmpl	$-9945, %eax
	je	.L2345
	cmpl	$-9955, %eax
	je	.L2346
	cmpl	$-9952, %eax
	je	.L2347
	cmpl	$-9960, %eax
	je	.L2348
	cmpl	$-9957, %eax
	je	.L2349
	cmpl	$-9947, %eax
	je	.L2350
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jnb	.L2337
.L2339:
	movb	$1, 540(%r12)
	leaq	.LC55(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2341:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L2339
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11AsmJsParser5BlockEv
	.p2align 4,,10
	.p2align 3
.L2344:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L2339
	cmpl	$-9948, 16(%r12)
	je	.L2325
.L2340:
	movb	$1, 540(%r12)
	leaq	.LC0(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2347:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11AsmJsParser12ForStatementEv.part.0
	.p2align 4,,10
	.p2align 3
.L2342:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L2339
	cmpl	$59, 16(%r12)
	jne	.L2340
	addq	$8, %rsp
	leaq	8(%r12), %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	.p2align 4,,10
	.p2align 3
.L2343:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L2339
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11AsmJsParser11IfStatementEv
	.p2align 4,,10
	.p2align 3
.L2345:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11AsmJsParser14WhileStatementEv
	.p2align 4,,10
	.p2align 3
.L2346:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11AsmJsParser11DoStatementEv
	.p2align 4,,10
	.p2align 3
.L2337:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11AsmJsParser19ExpressionStatementEv
	.p2align 4,,10
	.p2align 3
.L2325:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv.part.0
	.p2align 4,,10
	.p2align 3
.L2350:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L2339
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11AsmJsParser15SwitchStatementEv
	.p2align 4,,10
	.p2align 3
.L2348:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L2339
	cmpl	$-9960, 16(%r12)
	jne	.L2340
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11AsmJsParser14BreakStatementEv.part.0
	.p2align 4,,10
	.p2align 3
.L2349:
	.cfi_restore_state
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L2339
	cmpl	$-9957, 16(%r12)
	jne	.L2340
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11AsmJsParser17ContinueStatementEv.part.0
	.cfi_endproc
.LFE18220:
	.size	_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv, .-_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv.str1.8,"aMS",@progbits,1
	.align 8
.LC120:
	.string	"Function name collides with variable"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv.str1.1,"aMS",@progbits,1
.LC121:
	.string	"Function redefined"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv.str1.8
	.align 8
.LC122:
	.string	"Number of parameters exceeds internal limit"
	.align 8
.LC123:
	.string	"Expected return at end of non-void function"
	.align 8
.LC124:
	.string	"Number of local variables exceeds internal limit"
	.align 8
.LC125:
	.string	"Size of function body exceeds internal limit"
	.align 8
.LC126:
	.string	"Function definition doesn't match use"
	.section	.text.unlikely._ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv,"ax",@progbits
	.align 2
.LCOLDB127:
	.section	.text._ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv,"ax",@progbits
.LHOTB127:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv
	.type	_ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv, @function
_ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv:
.LFB18217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-9951, 16(%rdi)
	je	.L2352
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rdi)
	movq	%rax, 544(%rdi)
	movq	32(%rdi), %rax
	movl	%eax, 552(%rdi)
.L2353:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2422
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2352:
	.cfi_restore_state
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$255, 16(%r14)
	jg	.L2354
	leaq	.LC30(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2354:
	movq	(%r14), %rdi
	movq	72(%r14), %rax
	leaq	64(%r14), %r13
	leaq	7(%rax), %rsi
	movq	16(%rdi), %r15
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%r15, %rax
	cmpq	%rax, %rsi
	ja	.L2423
	addq	%r15, %rsi
	movq	%rsi, 16(%rdi)
.L2356:
	movq	72(%r14), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4copyEPcmm@PLT
	movslq	72(%r14), %rax
	movl	16(%r14), %r13d
	movq	%r12, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movq	%rax, %rbx
	movl	32(%rax), %eax
	testl	%eax, %eax
	je	.L2424
	cmpl	$4, %eax
	je	.L2359
	leaq	.LC120(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2359:
	cmpb	$0, 37(%rbx)
	jne	.L2425
	movq	8(%rbx), %rdi
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2424:
	movl	$4, 32(%rbx)
	movq	296(%r14), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal4wasm17WasmModuleBuilder11AddFunctionEPNS0_9SignatureINS1_9ValueTypeEEE@PLT
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movl	60(%rax), %eax
	movb	$0, 36(%rbx)
	movl	%eax, 28(%rbx)
.L2358:
	movb	$1, 37(%rbx)
	movq	-168(%rbp), %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder7SetNameENS0_6VectorIKcEE@PLT
	movq	8(%rbx), %rdi
	movq	32(%r14), %rsi
	movq	$0, 312(%r14)
	movq	%rdi, 304(%r14)
	call	_ZN2v88internal4wasm19WasmFunctionBuilder27SetAsmFunctionStartPositionEm@PLT
	movq	432(%r14), %rax
	movq	$0, -136(%rbp)
	movq	$0, -128(%rbp)
	movq	%rax, -144(%rbp)
	leaq	432(%r14), %rax
	movq	%rax, -112(%rbp)
	movq	448(%r14), %rax
	movq	$0, -120(%rbp)
	cmpq	440(%r14), %rax
	je	.L2362
	movdqu	-24(%rax), %xmm0
	movq	$0, -24(%rax)
	movq	-128(%rbp), %rcx
	movq	-8(%rax), %rdx
	movq	%rcx, -16(%rax)
	movq	-120(%rbp), %rcx
	movq	%rcx, -8(%rax)
	movq	%xmm0, %rax
	subq	$32, 448(%r14)
	movq	%rdx, -120(%rbp)
	movups	%xmm0, -136(%rbp)
	cmpq	-128(%rbp), %rax
	je	.L2362
	movq	%xmm0, -128(%rbp)
.L2362:
	leaq	-144(%rbp), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZN2v88internal4wasm11AsmJsParser22ValidateFunctionParamsEPNS0_10ZoneVectorIPNS1_7AsmTypeEEE
	movq	-128(%rbp), %rsi
	subq	-136(%rbp), %rsi
	sarq	$3, %rsi
	cmpq	$999, %rsi
	jbe	.L2364
	leaq	.LC122(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
.L2365:
	movq	-112(%rbp), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L2402
	movq	-144(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rsi)
	movq	-136(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-128(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-120(%rbp), %rax
	movq	%rax, 24(%rsi)
	movaps	%xmm0, -128(%rbp)
	addq	$32, 16(%rdi)
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2364:
	movq	400(%r14), %rax
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	%rax, -96(%rbp)
	leaq	400(%r14), %rax
	movq	%rax, -64(%rbp)
	movq	416(%r14), %rax
	movq	$0, -72(%rbp)
	cmpq	408(%r14), %rax
	je	.L2366
	movdqu	-24(%rax), %xmm0
	movq	$0, -24(%rax)
	movq	-80(%rbp), %rcx
	movq	-8(%rax), %rdx
	movq	%rcx, -16(%rax)
	movq	-72(%rbp), %rcx
	movq	%rcx, -8(%rax)
	movq	%xmm0, %rax
	subq	$32, 416(%r14)
	movq	%rdx, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	cmpq	-80(%rbp), %rax
	je	.L2417
	movq	%xmm0, -80(%rbp)
.L2417:
	movq	-128(%rbp), %rsi
	subq	-136(%rbp), %rsi
	sarq	$3, %rsi
.L2366:
	leaq	-96(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal4wasm11AsmJsParser22ValidateFunctionLocalsEmPNS0_10ZoneVectorINS1_9ValueTypeEEE
	movq	-128(%rbp), %rax
	subq	-136(%rbp), %rax
	movq	$0, 532(%r14)
	sarq	$3, %rax
	movzbl	540(%r14), %ebx
	movq	%rax, %rdx
	movq	-80(%rbp), %rax
	subq	-88(%rbp), %rax
	addl	%edx, %eax
	movl	%eax, 528(%r14)
	movl	16(%r14), %eax
	testb	%bl, %bl
	je	.L2375
	jmp	.L2368
	.p2align 4,,10
	.p2align 3
.L2427:
	cmpl	$-9948, %eax
	sete	%bl
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r14), %rax
	jb	.L2426
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv
	cmpb	$0, 540(%r14)
	jne	.L2374
	movl	16(%r14), %eax
.L2375:
	cmpl	$125, %eax
	jne	.L2427
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	testb	%bl, %bl
	jne	.L2376
.L2406:
	movq	312(%r14), %rdi
	testq	%rdi, %rdi
	je	.L2428
	movl	$17, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2379
.L2376:
	movq	312(%r14), %rsi
.L2378:
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser16ConvertSignatureEPNS1_7AsmTypeERKNS0_10ZoneVectorIS4_EE
	movq	304(%r14), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12SetSignatureEPNS0_9SignatureINS1_9ValueTypeEEE@PLT
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %rbx
	movq	%rax, -168(%rbp)
	cmpq	%rbx, %rax
	je	.L2384
	.p2align 4,,10
	.p2align 3
.L2383:
	movzbl	(%rbx), %esi
	movq	304(%r14), %rdi
	addq	$1, %rbx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder8AddLocalENS1_9ValueTypeE@PLT
	cmpq	%rbx, -168(%rbp)
	jne	.L2383
.L2384:
	movslq	532(%r14), %rax
	testl	%eax, %eax
	jle	.L2381
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L2382:
	movq	304(%r14), %rdi
	movl	$1, %esi
	addl	$1, %ebx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder8AddLocalENS1_9ValueTypeE@PLT
	movslq	532(%r14), %rax
	cmpl	%ebx, %eax
	jg	.L2382
.L2381:
	movq	-80(%rbp), %rdx
	subq	-88(%rbp), %rdx
	addq	%rdx, %rax
	cmpq	$50000, %rax
	jbe	.L2385
	leaq	.LC124(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	.p2align 4,,10
	.p2align 3
.L2374:
	movq	-64(%rbp), %rdi
	movq	16(%rdi), %rsi
	cmpq	24(%rdi), %rsi
	je	.L2403
	movq	-96(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, (%rsi)
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rsi)
	movq	-72(%rbp), %rax
	movq	%rax, 24(%rsi)
	movaps	%xmm0, -80(%rbp)
	addq	$32, 16(%rdi)
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2425:
	leaq	.LC121(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2423:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r15
	jmp	.L2356
	.p2align 4,,10
	.p2align 3
.L2402:
	movq	%r15, %rdx
	call	_ZNSt6vectorIN2v88internal10ZoneVectorIPNS1_4wasm7AsmTypeEEENS1_13ZoneAllocatorIS6_EEE17_M_realloc_insertIJS6_EEEvN9__gnu_cxx17__normal_iteratorIPS6_S9_EEDpOT_
	jmp	.L2353
.L2368:
	cmpl	$125, %eax
	je	.L2429
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L2374
	.p2align 4,,10
	.p2align 3
.L2426:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L2374
.L2403:
	movq	-176(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal10ZoneVectorINS1_4wasm9ValueTypeEEENS1_13ZoneAllocatorIS5_EEE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S8_EEDpOT_
	jmp	.L2365
.L2385:
	movq	304(%r14), %rdi
	movl	$11, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	304(%r14), %rdx
	movq	80(%rdx), %rax
	subq	72(%rdx), %rax
	cmpq	$7654321, %rax
	jbe	.L2386
	leaq	.LC125(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L2374
.L2428:
	movq	$17, 312(%r14)
	movl	$17, %esi
	jmp	.L2378
.L2386:
	movq	(%r14), %rbx
	movq	312(%r14), %rdx
	movq	16(%rbx), %r9
	movq	24(%rbx), %rax
	subq	%r9, %rax
	cmpq	$47, %rax
	jbe	.L2430
	leaq	48(%r9), %rax
	movq	%rax, 16(%rbx)
.L2388:
	leaq	16+_ZTVN2v88internal4wasm15AsmFunctionTypeE(%rip), %rax
	movq	%rdx, 8(%r9)
	movq	%rax, (%r9)
	movq	%rbx, 16(%r9)
	movq	-136(%rbp), %rbx
	movq	-128(%rbp), %rcx
	movq	$0, 24(%r9)
	movq	$0, 32(%r9)
	movq	$0, 40(%r9)
	cmpq	%rbx, %rcx
	je	.L2396
	testb	$1, %r9b
	jne	.L2392
	leaq	_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv(%rip), %r10
	.p2align 4,,10
	.p2align 3
.L2397:
	movq	(%r9), %rax
	movq	(%rbx), %rdx
	movq	16(%rax), %rsi
	movq	%r9, %rax
	cmpq	%r10, %rsi
	jne	.L2431
.L2393:
	movq	%rdx, -152(%rbp)
	movq	32(%rax), %rsi
	cmpq	%rsi, 40(%rax)
	je	.L2432
	addq	$8, %rbx
	movq	%rdx, (%rsi)
	addq	$8, 32(%rax)
	cmpq	%rbx, %rcx
	jne	.L2397
.L2396:
	movl	%r13d, %esi
	movq	%r14, %rdi
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movl	$2147483649, %esi
	movq	(%rax), %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	movq	-168(%rbp), %r9
	testb	%al, %al
	je	.L2433
	movq	%r9, (%rbx)
.L2398:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner11ResetLocalsEv@PLT
	movq	376(%r14), %rax
	cmpq	384(%r14), %rax
	je	.L2374
	movq	%rax, 384(%r14)
	jmp	.L2374
.L2432:
	leaq	-152(%rbp), %rdx
	leaq	16(%rax), %rdi
	addq	$8, %rbx
	movq	%r9, -184(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZNSt6vectorIPN2v88internal4wasm7AsmTypeENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	movq	-168(%rbp), %rcx
	movq	-184(%rbp), %r9
	leaq	_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv(%rip), %r10
	cmpq	%rbx, %rcx
	jne	.L2397
	jmp	.L2396
.L2431:
	movq	%rdx, -192(%rbp)
	movq	%r9, %rdi
	movq	%rcx, -184(%rbp)
	movq	%r9, -168(%rbp)
	call	*%rsi
	movq	-192(%rbp), %rdx
	movq	-184(%rbp), %rcx
	leaq	_ZN2v88internal4wasm15AsmFunctionType14AsFunctionTypeEv(%rip), %r10
	movq	-168(%rbp), %r9
	jmp	.L2393
.L2379:
	leaq	.LC123(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L2374
.L2433:
	movq	(%rbx), %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2398
	leaq	.LC126(%rip), %rax
	movb	$1, 540(%r14)
	movq	%rax, 544(%r14)
	movq	32(%r14), %rax
	movl	%eax, 552(%r14)
	jmp	.L2374
.L2422:
	call	__stack_chk_fail@PLT
.L2430:
	movl	$48, %esi
	movq	%rbx, %rdi
	movq	%rdx, -168(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-168(%rbp), %rdx
	movq	%rax, %r9
	jmp	.L2388
.L2429:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	jmp	.L2406
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv
	.cfi_startproc
	.type	_ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv.cold, @function
_ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv.cold:
.LFSB18217:
.L2392:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	(%rbx), %rax
	movq	%rax, -152(%rbp)
	movq	32, %rax
	ud2
	.cfi_endproc
.LFE18217:
	.section	.text._ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv
	.size	_ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv, .-_ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv
	.section	.text.unlikely._ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv
	.size	_ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv.cold, .-_ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv.cold
.LCOLDE127:
	.section	.text._ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv
.LHOTE127:
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser14ValidateModuleEv.str1.1,"aMS",@progbits,1
.LC128:
	.string	"Undefined function"
.LC129:
	.string	"Undefined function table"
	.section	.text._ZN2v88internal4wasm11AsmJsParser14ValidateModuleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser14ValidateModuleEv
	.type	_ZN2v88internal4wasm11AsmJsParser14ValidateModuleEv, @function
_ZN2v88internal4wasm11AsmJsParser14ValidateModuleEv:
.LFB18207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2532
	cmpl	$40, 16(%rbx)
	je	.L2437
.L2531:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L2434:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2437:
	.cfi_restore_state
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r13d
	movl	$0, 556(%rbx)
	movq	$0, 560(%rbx)
	cmpl	$41, %r13d
	je	.L2445
	cmpl	$255, %r13d
	jg	.L2440
	leaq	.LC6(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
	.p2align 4,,10
	.p2align 3
.L2440:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %eax
	movl	%r13d, 556(%rbx)
	cmpl	$41, %eax
	jne	.L2536
.L2445:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpb	$0, 540(%rbx)
	jne	.L2434
	cmpl	$123, 16(%rbx)
	jne	.L2531
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$-9937, 16(%rbx)
	jne	.L2531
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2532
	movl	16(%rbx), %eax
	cmpl	$59, %eax
	je	.L2449
	cmpl	$125, %eax
	je	.L2451
	cmpb	$0, 292(%rbx)
	je	.L2533
.L2451:
	cmpb	$0, 540(%rbx)
	jne	.L2434
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2532
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser18ValidateModuleVarsEv
	cmpb	$0, 540(%rbx)
	je	.L2455
	jmp	.L2434
	.p2align 4,,10
	.p2align 3
.L2537:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2532
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser16ValidateFunctionEv
	cmpb	$0, 540(%rbx)
	jne	.L2434
.L2455:
	movl	16(%rbx), %eax
	cmpl	$-9951, %eax
	je	.L2537
	.p2align 4,,10
	.p2align 3
.L2453:
	cmpl	$-9946, %eax
	jne	.L2456
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2532
	cmpl	$-9946, 16(%rbx)
	jne	.L2531
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r13d
	cmpl	$255, %r13d
	jg	.L2459
	leaq	.LC34(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
	.p2align 4,,10
	.p2align 3
.L2532:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2536:
	.cfi_restore_state
	cmpl	$44, %eax
	jne	.L2531
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r13d
	cmpl	$255, %r13d
	jg	.L2442
	leaq	.LC7(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2442:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %eax
	movl	%r13d, 560(%rbx)
	cmpl	$41, %eax
	je	.L2445
	cmpl	$44, %eax
	jne	.L2531
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r13d
	cmpl	$255, %r13d
	jg	.L2444
	leaq	.LC8(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
	.p2align 4,,10
	.p2align 3
.L2444:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$41, 16(%rbx)
	movl	%r13d, 564(%rbx)
	jne	.L2531
	jmp	.L2445
	.p2align 4,,10
	.p2align 3
.L2449:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	jmp	.L2451
.L2533:
	leaq	.LC2(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2459:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	movq	%rax, %r13
	movl	32(%rax), %eax
	cmpl	$5, %eax
	je	.L2538
	testl	%eax, %eax
	jne	.L2539
.L2462:
	cmpl	$61, 16(%rbx)
	jne	.L2531
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$91, 16(%rbx)
	jne	.L2531
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
.L2471:
	movl	16(%rbx), %r15d
	cmpl	$255, %r15d
	jle	.L2534
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	cmpl	$4, 32(%rax)
	movq	%rax, %r15
	jne	.L2535
	cmpl	$5, 32(%r13)
	je	.L2540
.L2467:
	movl	16(%rbx), %eax
	leaq	1(%r14), %r15
	cmpl	$44, %eax
	je	.L2541
	cmpl	$93, %eax
	jne	.L2531
.L2472:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$5, 32(%r13)
	jne	.L2473
	movl	24(%r13), %eax
	cmpq	%rax, %r14
	je	.L2473
	leaq	.LC39(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2538:
	cmpb	$0, 37(%r13)
	jne	.L2542
	movb	$1, 37(%r13)
	jmp	.L2462
.L2456:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2532
	cmpl	$-9948, 16(%rbx)
	jne	.L2531
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %r13d
	cmpl	$123, %r13d
	je	.L2479
	cmpl	$255, %r13d
	jg	.L2543
	leaq	.LC32(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2539:
	leaq	.LC36(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2542:
	leaq	.LC35(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2534:
	leaq	.LC30(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2473:
	movl	16(%rbx), %eax
	cmpl	$59, %eax
	je	.L2474
	cmpl	$125, %eax
	je	.L2476
	cmpb	$0, 292(%rbx)
	je	.L2533
.L2476:
	cmpb	$0, 540(%rbx)
	jne	.L2434
	movl	16(%rbx), %eax
	jmp	.L2453
.L2474:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	jmp	.L2476
.L2541:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$93, 16(%rbx)
	je	.L2472
	movq	%r15, %r14
	jmp	.L2471
.L2540:
	movl	24(%r13), %eax
	addq	$1, %rax
	cmpq	%r14, %rax
	jbe	.L2544
	movq	0(%r13), %rsi
	movq	(%r15), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2545
	movl	28(%r13), %esi
	movl	28(%r15), %edx
	movq	296(%rbx), %rdi
	addl	%r14d, %esi
	call	_ZN2v88internal4wasm17WasmModuleBuilder19SetIndirectFunctionEjj@PLT
	jmp	.L2467
.L2535:
	leaq	.LC31(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2545:
	leaq	.LC38(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2544:
	leaq	.LC37(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2543:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	cmpl	$4, 32(%rax)
	je	.L2493
	leaq	.LC33(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2479:
	movq	%r12, %rdi
	leaq	64(%rbx), %r14
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
.L2490:
	movq	(%rbx), %rdi
	movq	72(%rbx), %rax
	movq	16(%rdi), %r13
	leaq	7(%rax), %rsi
	movq	24(%rdi), %rax
	andq	$-8, %rsi
	subq	%r13, %rax
	cmpq	%rax, %rsi
	ja	.L2546
	addq	%r13, %rsi
	movq	%rsi, 16(%rdi)
.L2483:
	movq	72(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4copyEPcmm@PLT
	movl	16(%rbx), %eax
	movslq	72(%rbx), %r15
	addl	$9999, %eax
	cmpl	$10254, %eax
	jbe	.L2484
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$58, 16(%rbx)
	jne	.L2531
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %esi
	cmpl	$255, %esi
	jle	.L2534
	movq	%r12, %rdi
	movl	%esi, -52(%rbp)
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	-52(%rbp), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10GetVarInfoEi
	cmpl	$4, 32(%rax)
	jne	.L2535
	movq	8(%rax), %rax
	movq	296(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r15, %rdx
	movl	60(%rax), %r8d
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddExportENS0_6VectorIKcEENS1_20ImportExportKindCodeEj@PLT
	movl	16(%rbx), %eax
	cmpl	$44, %eax
	je	.L2547
	cmpl	$125, %eax
	jne	.L2531
.L2491:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
.L2492:
	cmpb	$0, 540(%rbx)
	jne	.L2434
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2532
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser13SkipSemicolonEv
	cmpb	$0, 540(%rbx)
	jne	.L2434
	cmpl	$125, 16(%rbx)
	jne	.L2531
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	352(%rbx), %r13
	movq	344(%rbx), %r12
	jmp	.L2500
.L2497:
	cmpl	$5, %eax
	jne	.L2499
	cmpb	$0, 37(%r12)
	je	.L2548
.L2498:
	addq	$40, %r12
.L2500:
	cmpq	%r12, %r13
	je	.L2496
	movl	32(%r12), %eax
	cmpl	$4, %eax
	jne	.L2497
	cmpb	$0, 37(%r12)
	jne	.L2498
	leaq	.LC128(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2499:
	cmpl	$6, %eax
	jne	.L2498
	cmpb	$0, 37(%r12)
	jne	.L2498
	movq	(%rbx), %r15
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal4Zone3NewEm
	movq	%r15, %rdi
	movl	$24, %esi
	movq	%rax, %r14
	call	_ZN2v88internal4Zone3NewEm
	pxor	%xmm1, %xmm1
	movups	%xmm1, (%rax)
	movq	%rax, %rcx
	movq	%r14, 16(%rax)
	movq	16(%r12), %rax
	movq	296(%rbx), %rdi
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddImportENS0_6VectorIKcEEPNS0_9SignatureINS1_9ValueTypeEEE@PLT
	jmp	.L2498
.L2547:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$125, 16(%rbx)
	je	.L2491
	jmp	.L2490
.L2484:
	leaq	.LC29(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2496:
	movq	296(%rbx), %rdi
	xorl	%esi, %esi
	leaq	736(%rbx), %r14
	call	_ZN2v88internal4wasm17WasmModuleBuilder11AddFunctionEPNS0_9SignatureINS1_9ValueTypeEEE@PLT
	movq	296(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v88internal4wasm17WasmModuleBuilder17MarkStartFunctionEPNS1_19WasmFunctionBuilderE@PLT
	movq	736(%rbx), %r13
	jmp	.L2502
.L2549:
	movzbl	32(%r13), %ecx
	movq	16(%r13), %rsi
	xorl	%r8d, %r8d
	movq	24(%r13), %rdx
	movq	296(%rbx), %rdi
	call	_ZN2v88internal4wasm17WasmModuleBuilder15AddGlobalImportENS0_6VectorIKcEENS1_9ValueTypeEb@PLT
	movl	$35, %esi
	movq	%r12, %rdi
	movl	%eax, %edx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithI32VENS1_10WasmOpcodeEi@PLT
	movq	40(%r13), %rax
	movl	$36, %esi
	movq	%r12, %rdi
	movl	28(%rax), %edx
	addl	752(%rbx), %edx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitWithI32VENS1_10WasmOpcodeEi@PLT
	movq	0(%r13), %r13
.L2502:
	cmpq	%r14, %r13
	jne	.L2549
	movq	%r12, %rdi
	movl	$11, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	(%rbx), %r13
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4Zone3NewEm
	movq	%r13, %rdi
	movl	$24, %esi
	movq	%rax, %rbx
	call	_ZN2v88internal4Zone3NewEm
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rbx, 16(%rax)
	movq	%rax, %rsi
	movups	%xmm0, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm19WasmFunctionBuilder12SetSignatureEPNS0_9SignatureINS1_9ValueTypeEEE@PLT
.L2548:
	.cfi_restore_state
	leaq	.LC129(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2434
.L2546:
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	%rax, %r13
	jmp	.L2483
.L2493:
	movq	_ZN2v88internal5AsmJs19kSingleFunctionNameE(%rip), %rsi
	movq	8(%rax), %r8
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	296(%rbx), %r9
	movq	%rsi, %rdi
	movl	60(%r8), %r8d
	repnz scasb
	movq	%r9, %rdi
	movq	%rcx, %rax
	xorl	%ecx, %ecx
	notq	%rax
	leaq	-1(%rax), %rdx
	call	_ZN2v88internal4wasm17WasmModuleBuilder9AddExportENS0_6VectorIKcEENS1_20ImportExportKindCodeEj@PLT
	jmp	.L2492
	.cfi_endproc
.LFE18207:
	.size	_ZN2v88internal4wasm11AsmJsParser14ValidateModuleEv, .-_ZN2v88internal4wasm11AsmJsParser14ValidateModuleEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser3RunEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser3RunEv
	.type	_ZN2v88internal4wasm11AsmJsParser3RunEv, @function
_ZN2v88internal4wasm11AsmJsParser3RunEv:
.LFB18184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v88internal4wasm11AsmJsParser14ValidateModuleEv
	movzbl	540(%rbx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	xorl	$1, %eax
	ret
	.cfi_endproc
.LFE18184:
	.size	_ZN2v88internal4wasm11AsmJsParser3RunEv, .-_ZN2v88internal4wasm11AsmJsParser3RunEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser11IfStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser11IfStatementEv
	.type	_ZN2v88internal4wasm11AsmJsParser11IfStatementEv, @function
_ZN2v88internal4wasm11AsmJsParser11IfStatementEv:
.LFB18224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$-9950, 16(%rdi)
	je	.L2553
.L2569:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L2552:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2571
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2553:
	.cfi_restore_state
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$40, 16(%rbx)
	jne	.L2569
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2570
	movl	$769, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%rbx)
	jne	.L2552
	cmpl	$41, 16(%rbx)
	jne	.L2569
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	$3, -32(%rbp)
	movq	600(%rbx), %rsi
	cmpq	608(%rbx), %rsi
	je	.L2559
	movq	$3, (%rsi)
	addq	$8, 600(%rbx)
.L2560:
	movq	304(%rbx), %rdi
	movl	$64, %edx
	movl	$4, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2570
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv
	cmpb	$0, 540(%rbx)
	jne	.L2552
	cmpl	$-9954, 16(%rbx)
	je	.L2563
.L2565:
	movq	304(%rbx), %rdi
	movl	$11, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	subq	$8, 600(%rbx)
	jmp	.L2552
	.p2align 4,,10
	.p2align 3
.L2570:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2552
.L2559:
	leaq	-32(%rbp), %rdx
	leaq	584(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2560
.L2563:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	304(%rbx), %rdi
	movl	$5, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2570
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv
	cmpb	$0, 540(%rbx)
	je	.L2565
	jmp	.L2552
.L2571:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18224:
	.size	_ZN2v88internal4wasm11AsmJsParser11IfStatementEv, .-_ZN2v88internal4wasm11AsmJsParser11IfStatementEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser14WhileStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser14WhileStatementEv
	.type	_ZN2v88internal4wasm11AsmJsParser14WhileStatementEv, @function
_ZN2v88internal4wasm11AsmJsParser14WhileStatementEv:
.LFB18227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	584(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	600(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	724(%rdi), %eax
	movl	$0, -32(%rbp)
	movl	%eax, -28(%rbp)
	cmpq	608(%rdi), %rsi
	je	.L2573
	movl	$0, (%rsi)
	movl	%eax, 4(%rsi)
	addq	$8, 600(%rdi)
.L2574:
	movq	304(%rbx), %rdi
	movl	$2, %esi
	movl	$64, %edx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	movl	724(%rbx), %eax
	movl	$1, -32(%rbp)
	movq	600(%rbx), %rsi
	movl	%eax, -28(%rbp)
	cmpq	608(%rbx), %rsi
	je	.L2575
	movl	$1, (%rsi)
	movl	%eax, 4(%rsi)
	addq	$8, 600(%rbx)
.L2576:
	movq	32(%rbx), %rsi
	movq	304(%rbx), %rdi
	movq	%rsi, %rdx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder16AddAsmWasmOffsetEmm@PLT
	movl	$64, %edx
	movl	$3, %esi
	movq	304(%rbx), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	cmpl	$-9945, 16(%rbx)
	movl	$0, 724(%rbx)
	je	.L2577
.L2588:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L2572:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2590
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2577:
	.cfi_restore_state
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$40, 16(%rbx)
	jne	.L2588
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2589
	movl	$769, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%rbx)
	jne	.L2572
	cmpl	$41, 16(%rbx)
	jne	.L2588
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	304(%rbx), %rdi
	movl	$69, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movl	$1, %edx
	movl	$13, %esi
	movq	304(%rbx), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2589
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv
	cmpb	$0, 540(%rbx)
	jne	.L2572
	movq	304(%rbx), %rdi
	xorl	%edx, %edx
	movl	$12, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	subq	$8, 600(%rbx)
	movl	$11, %esi
	movq	304(%rbx), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	subq	$8, 600(%rbx)
	movl	$11, %esi
	movq	304(%rbx), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L2573:
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2574
	.p2align 4,,10
	.p2align 3
.L2575:
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2576
	.p2align 4,,10
	.p2align 3
.L2589:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2572
.L2590:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18227:
	.size	_ZN2v88internal4wasm11AsmJsParser14WhileStatementEv, .-_ZN2v88internal4wasm11AsmJsParser14WhileStatementEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser11DoStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser11DoStatementEv
	.type	_ZN2v88internal4wasm11AsmJsParser11DoStatementEv, @function
_ZN2v88internal4wasm11AsmJsParser11DoStatementEv:
.LFB18228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	584(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	600(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	724(%rdi), %eax
	movl	$0, -32(%rbp)
	movl	%eax, -28(%rbp)
	cmpq	608(%rdi), %rsi
	je	.L2592
	movl	$0, (%rsi)
	movl	%eax, 4(%rsi)
	addq	$8, 600(%rdi)
.L2593:
	movq	304(%r12), %rdi
	movl	$2, %esi
	movl	$64, %edx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	movq	$1, -32(%rbp)
	movq	600(%r12), %rsi
	cmpq	608(%r12), %rsi
	je	.L2594
	movq	$1, (%rsi)
	addq	$8, 600(%r12)
.L2595:
	movq	32(%r12), %rsi
	movq	304(%r12), %rdi
	movq	%rsi, %rdx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder16AddAsmWasmOffsetEmm@PLT
	movl	$3, %esi
	movl	$64, %edx
	movq	304(%r12), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	movl	724(%r12), %eax
	movl	$1, -32(%rbp)
	movq	600(%r12), %rsi
	movl	%eax, -28(%rbp)
	cmpq	608(%r12), %rsi
	je	.L2596
	movl	$1, (%rsi)
	movl	%eax, 4(%rsi)
	addq	$8, 600(%r12)
.L2597:
	movq	304(%r12), %rdi
	movl	$64, %edx
	movl	$2, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	cmpl	$-9955, 16(%r12)
	movl	$0, 724(%r12)
	je	.L2598
.L2611:
	movb	$1, 540(%r12)
	leaq	.LC0(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
.L2591:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2612
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2598:
	.cfi_restore_state
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jnb	.L2600
.L2610:
	movb	$1, 540(%r12)
	leaq	.LC55(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	jmp	.L2591
	.p2align 4,,10
	.p2align 3
.L2600:
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv
	cmpb	$0, 540(%r12)
	jne	.L2591
	cmpl	$-9945, 16(%r12)
	jne	.L2611
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	304(%r12), %rdi
	movl	$11, %esi
	subq	$8, 600(%r12)
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	cmpl	$40, 16(%r12)
	jne	.L2611
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jb	.L2610
	movl	$769, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%r12)
	jne	.L2591
	movq	304(%r12), %rdi
	movl	$69, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movl	$1, %edx
	movl	$13, %esi
	movq	304(%r12), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	movq	304(%r12), %rdi
	xorl	%edx, %edx
	movl	$12, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	cmpl	$41, 16(%r12)
	jne	.L2611
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	304(%r12), %rdi
	movl	$11, %esi
	subq	$8, 600(%r12)
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	304(%r12), %rdi
	movl	$11, %esi
	subq	$8, 600(%r12)
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser13SkipSemicolonEv
	jmp	.L2591
	.p2align 4,,10
	.p2align 3
.L2596:
	leaq	-32(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2597
	.p2align 4,,10
	.p2align 3
.L2592:
	leaq	-32(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2593
	.p2align 4,,10
	.p2align 3
.L2594:
	leaq	-32(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2595
.L2612:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18228:
	.size	_ZN2v88internal4wasm11AsmJsParser11DoStatementEv, .-_ZN2v88internal4wasm11AsmJsParser11DoStatementEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser12ForStatementEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal4wasm11AsmJsParser12ForStatementEv.part.0, @function
_ZN2v88internal4wasm11AsmJsParser12ForStatementEv.part.0:
.LFB23482:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	8(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$40, 16(%rbx)
	je	.L2614
.L2633:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L2613:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2645
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2614:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$59, 16(%rbx)
	jne	.L2646
.L2616:
	movq	%r12, %rdi
	leaq	584(%rbx), %r13
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	724(%rbx), %eax
	movl	$0, -48(%rbp)
	movq	600(%rbx), %rsi
	movl	%eax, -44(%rbp)
	cmpq	608(%rbx), %rsi
	je	.L2620
	movl	$0, (%rsi)
	movl	%eax, 4(%rsi)
	addq	$8, 600(%rbx)
.L2621:
	movq	304(%rbx), %rdi
	movl	$2, %esi
	movl	$64, %edx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	movq	$1, -48(%rbp)
	movq	600(%rbx), %rsi
	cmpq	608(%rbx), %rsi
	je	.L2622
	movq	$1, (%rsi)
	addq	$8, 600(%rbx)
.L2623:
	movq	32(%rbx), %rsi
	movq	304(%rbx), %rdi
	movq	%rsi, %rdx
	call	_ZN2v88internal4wasm19WasmFunctionBuilder16AddAsmWasmOffsetEmm@PLT
	movl	$3, %esi
	movl	$64, %edx
	movq	304(%rbx), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	movl	724(%rbx), %eax
	movl	$1, -48(%rbp)
	movq	600(%rbx), %rsi
	movl	%eax, -44(%rbp)
	cmpq	608(%rbx), %rsi
	je	.L2624
	movl	$1, (%rsi)
	movl	%eax, 4(%rsi)
	addq	$8, 600(%rbx)
.L2625:
	movq	304(%rbx), %rdi
	movl	$64, %edx
	movl	$2, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	cmpl	$59, 16(%rbx)
	movl	$0, 724(%rbx)
	jne	.L2647
.L2626:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	32(%rbx), %r14
	jmp	.L2634
	.p2align 4,,10
	.p2align 3
.L2629:
	cmpl	$41, %eax
	je	.L2648
	cmpl	$-1, %eax
	je	.L2633
.L2630:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
.L2634:
	movl	16(%rbx), %eax
	cmpl	$40, %eax
	jne	.L2629
	addl	$1, %r13d
	jmp	.L2630
	.p2align 4,,10
	.p2align 3
.L2646:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2644
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%rbx)
	movq	%rax, %rdi
	jne	.L2613
	movl	$17, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2649
.L2619:
	cmpl	$59, 16(%rbx)
	jne	.L2633
	jmp	.L2616
	.p2align 4,,10
	.p2align 3
.L2644:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2613
	.p2align 4,,10
	.p2align 3
.L2647:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2644
	movl	$769, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%rbx)
	jne	.L2613
	movq	304(%rbx), %rdi
	movl	$69, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movl	$2, %edx
	movl	$13, %esi
	movq	304(%rbx), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	cmpl	$59, 16(%rbx)
	jne	.L2633
	jmp	.L2626
	.p2align 4,,10
	.p2align 3
.L2648:
	subl	$1, %r13d
	cmpl	$-1, %r13d
	jne	.L2630
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2644
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv
	cmpb	$0, 540(%rbx)
	jne	.L2613
	subq	$8, 600(%rbx)
	movq	304(%rbx), %rdi
	movl	$11, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	32(%rbx), %r13
	call	_ZN2v88internal12AsmJsScanner4SeekEm@PLT
	cmpl	$41, 16(%rbx)
	je	.L2637
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2644
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%rbx)
	jne	.L2613
.L2637:
	movq	304(%rbx), %rdi
	xorl	%edx, %edx
	movl	$12, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder10EmitWithU8ENS1_10WasmOpcodeEh@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4SeekEm@PLT
	subq	$8, 600(%rbx)
	movl	$11, %esi
	movq	304(%rbx), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	subq	$8, 600(%rbx)
	movl	$11, %esi
	movq	304(%rbx), %rdi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2613
	.p2align 4,,10
	.p2align 3
.L2649:
	movq	304(%rbx), %rdi
	movl	$26, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2619
	.p2align 4,,10
	.p2align 3
.L2624:
	leaq	-48(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2625
	.p2align 4,,10
	.p2align 3
.L2622:
	leaq	-48(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2620:
	leaq	-48(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser9BlockInfoENS1_13ZoneAllocatorIS4_EEE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S7_EEDpOT_
	jmp	.L2621
.L2645:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23482:
	.size	_ZN2v88internal4wasm11AsmJsParser12ForStatementEv.part.0, .-_ZN2v88internal4wasm11AsmJsParser12ForStatementEv.part.0
	.section	.text._ZN2v88internal4wasm11AsmJsParser12ForStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser12ForStatementEv
	.type	_ZN2v88internal4wasm11AsmJsParser12ForStatementEv, @function
_ZN2v88internal4wasm11AsmJsParser12ForStatementEv:
.LFB18229:
	.cfi_startproc
	endbr64
	cmpl	$-9952, 16(%rdi)
	je	.L2651
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rdi)
	movq	%rax, 544(%rdi)
	movq	32(%rdi), %rax
	movl	%eax, 552(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L2651:
	jmp	_ZN2v88internal4wasm11AsmJsParser12ForStatementEv.part.0
	.cfi_endproc
.LFE18229:
	.size	_ZN2v88internal4wasm11AsmJsParser12ForStatementEv, .-_ZN2v88internal4wasm11AsmJsParser12ForStatementEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser18IterationStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser18IterationStatementEv
	.type	_ZN2v88internal4wasm11AsmJsParser18IterationStatementEv, @function
_ZN2v88internal4wasm11AsmJsParser18IterationStatementEv:
.LFB18226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	16(%rdi), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpl	$-9945, %eax
	je	.L2660
	cmpl	$-9955, %eax
	je	.L2661
	xorl	%r8d, %r8d
	cmpl	$-9952, %eax
	je	.L2662
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2662:
	.cfi_restore_state
	call	_ZN2v88internal4wasm11AsmJsParser12ForStatementEv.part.0
	movl	$1, %r8d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2660:
	.cfi_restore_state
	call	_ZN2v88internal4wasm11AsmJsParser14WhileStatementEv
	movl	$1, %r8d
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2661:
	.cfi_restore_state
	call	_ZN2v88internal4wasm11AsmJsParser11DoStatementEv
	movl	$1, %r8d
	popq	%rbp
	.cfi_def_cfa 7, 8
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE18226:
	.size	_ZN2v88internal4wasm11AsmJsParser18IterationStatementEv, .-_ZN2v88internal4wasm11AsmJsParser18IterationStatementEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser17LabelledStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser17LabelledStatementEv
	.type	_ZN2v88internal4wasm11AsmJsParser17LabelledStatementEv, @function
_ZN2v88internal4wasm11AsmJsParser17LabelledStatementEv:
.LFB18232:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	724(%rdi), %eax
	movq	%rdi, %r12
	testl	%eax, %eax
	je	.L2664
	leaq	.LC118(%rip), %rax
	movb	$1, 540(%rdi)
	movq	%rax, 544(%rdi)
	movq	32(%rdi), %rax
	movl	%eax, 552(%rdi)
.L2663:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2664:
	.cfi_restore_state
	movl	16(%rdi), %eax
	leaq	8(%rdi), %r13
	movl	%eax, 724(%rdi)
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$58, 16(%r12)
	je	.L2666
	movb	$1, 540(%r12)
	leaq	.LC0(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2666:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%r12), %rax
	jnb	.L2667
	movb	$1, 540(%r12)
	leaq	.LC55(%rip), %rax
	movq	%rax, 544(%r12)
	movq	32(%r12), %rax
	movl	%eax, 552(%r12)
	jmp	.L2663
	.p2align 4,,10
	.p2align 3
.L2667:
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv
	.cfi_endproc
.LFE18232:
	.size	_ZN2v88internal4wasm11AsmJsParser17LabelledStatementEv, .-_ZN2v88internal4wasm11AsmJsParser17LabelledStatementEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser12ValidateCaseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser12ValidateCaseEv
	.type	_ZN2v88internal4wasm11AsmJsParser12ValidateCaseEv, @function
_ZN2v88internal4wasm11AsmJsParser12ValidateCaseEv:
.LFB18234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$-9959, 16(%rdi)
	je	.L2670
.L2690:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L2669:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2670:
	.cfi_restore_state
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %eax
	cmpl	$45, %eax
	je	.L2691
	cmpl	$-3, %eax
	je	.L2692
.L2681:
	leaq	.LC54(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2692:
	.cfi_restore_state
	movl	288(%rbx), %r13d
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	testl	%r13d, %r13d
	js	.L2678
.L2679:
	cmpl	$58, 16(%rbx)
	jne	.L2690
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpb	$0, 540(%rbx)
	je	.L2675
	jmp	.L2669
	.p2align 4,,10
	.p2align 3
.L2677:
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser17ValidateStatementEv
	cmpb	$0, 540(%rbx)
	jne	.L2669
.L2675:
	movl	16(%rbx), %eax
	cmpl	$125, %eax
	setne	%cl
	cmpl	$-9959, %eax
	setne	%dl
	testb	%dl, %cl
	je	.L2669
	cmpl	$-9956, %eax
	je	.L2669
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jnb	.L2677
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2669
	.p2align 4,,10
	.p2align 3
.L2691:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$-3, 16(%rbx)
	jne	.L2681
	movl	288(%rbx), %r13d
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$-2147483648, %r13d
	jbe	.L2679
.L2678:
	leaq	.LC58(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2669
	.cfi_endproc
.LFE18234:
	.size	_ZN2v88internal4wasm11AsmJsParser12ValidateCaseEv, .-_ZN2v88internal4wasm11AsmJsParser12ValidateCaseEv
	.section	.text._ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv
	.type	_ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv, @function
_ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv:
.LFB18225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpl	$-9948, 16(%rdi)
	je	.L2694
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rdi)
	movq	%rax, 544(%rdi)
	movq	32(%rdi), %rax
	movl	%eax, 552(%rdi)
.L2693:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2694:
	.cfi_restore_state
	leaq	8(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	16(%rbx), %eax
	cmpl	$59, %eax
	je	.L2696
	cmpl	$125, %eax
	jne	.L2718
.L2696:
	movq	312(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2719
	movl	$17, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2720
.L2703:
	movq	304(%rbx), %rdi
	movl	$15, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movl	16(%rbx), %eax
	cmpl	$59, %eax
	je	.L2706
	cmpl	$125, %eax
	je	.L2693
	cmpb	$0, 292(%rbx)
	jne	.L2693
	leaq	.LC2(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2693
	.p2align 4,,10
	.p2align 3
.L2718:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jnb	.L2721
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2693
	.p2align 4,,10
	.p2align 3
.L2719:
	movq	$17, 312(%rbx)
	jmp	.L2703
	.p2align 4,,10
	.p2align 3
.L2721:
	movq	312(%rbx), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%rbx)
	movq	%rax, %r13
	jne	.L2693
	movl	$237, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2702
	movq	$237, 312(%rbx)
	jmp	.L2703
	.p2align 4,,10
	.p2align 3
.L2706:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	.p2align 4,,10
	.p2align 3
.L2720:
	.cfi_restore_state
	leaq	.LC117(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2693
.L2702:
	movl	$57357, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2704
	movq	$57357, 312(%rbx)
	jmp	.L2703
.L2704:
	movl	$1825, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2705
	movq	$1825, 312(%rbx)
	jmp	.L2703
.L2705:
	leaq	.LC116(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2693
	.cfi_endproc
.LFE18225:
	.size	_ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv, .-_ZN2v88internal4wasm11AsmJsParser15ReturnStatementEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser21ValidateFloatCoercionEv.str1.1,"aMS",@progbits,1
.LC130:
	.string	"Illegal conversion to float"
	.section	.text._ZN2v88internal4wasm11AsmJsParser21ValidateFloatCoercionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser21ValidateFloatCoercionEv
	.type	_ZN2v88internal4wasm11AsmJsParser21ValidateFloatCoercionEv, @function
_ZN2v88internal4wasm11AsmJsParser21ValidateFloatCoercionEv:
.LFB18278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %eax
	cmpl	$255, %eax
	jg	.L2723
.L2726:
	leaq	.LC69(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L2722:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2723:
	.cfi_restore_state
	movq	344(%rdi), %rcx
	movq	352(%rdi), %rdx
	subl	$256, %eax
	movabsq	$-3689348814741910323, %rsi
	movslq	%eax, %r12
	subq	%rcx, %rdx
	sarq	$3, %rdx
	imulq	%rsi, %rdx
	leaq	1(%r12), %rsi
	cmpq	%rsi, %rdx
	jb	.L2725
.L2737:
	leaq	(%r12,%r12,4), %rax
	movq	672(%rbx), %rsi
	movq	(%rcx,%rax,8), %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2726
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$40, 16(%rbx)
	je	.L2727
.L2745:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2725:
	.cfi_restore_state
	subq	%rdx, %rsi
	leaq	336(%rdi), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	344(%rbx), %rcx
	jmp	.L2737
	.p2align 4,,10
	.p2align 3
.L2727:
	movq	%r12, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	32(%rbx), %rax
	movq	$57357, 680(%rbx)
	movq	%rax, 688(%rbx)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jnb	.L2728
.L2746:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2722
	.p2align 4,,10
	.p2align 3
.L2728:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2746
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%rbx)
	movq	%rax, %r13
	jne	.L2722
	movl	$8197, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2747
.L2733:
	cmpl	$41, 16(%rbx)
	jne	.L2745
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal12AsmJsScanner4NextEv@PLT
.L2747:
	.cfi_restore_state
	movl	$77, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2732
	movq	304(%rbx), %rdi
	movl	$182, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2733
.L2732:
	movl	$1825, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2734
	movq	304(%rbx), %rdi
	movl	$178, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2733
.L2734:
	movl	$2817, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2735
	movq	304(%rbx), %rdi
	movl	$179, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	jmp	.L2733
.L2735:
	leaq	.LC130(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2722
	.cfi_endproc
.LFE18278:
	.size	_ZN2v88internal4wasm11AsmJsParser21ValidateFloatCoercionEv, .-_ZN2v88internal4wasm11AsmJsParser21ValidateFloatCoercionEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser18ValidateHeapAccessEv.str1.1,"aMS",@progbits,1
.LC131:
	.string	"Heap access out of range"
.LC132:
	.string	"Expected shift of word size"
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser18ValidateHeapAccessEv.str1.8,"aMS",@progbits,1
	.align 8
.LC133:
	.string	"Expected valid heap access shift"
	.align 8
.LC134:
	.string	"Expected heap access shift to match heap view"
	.section	.text._ZN2v88internal4wasm11AsmJsParser18ValidateHeapAccessEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser18ValidateHeapAccessEv
	.type	_ZN2v88internal4wasm11AsmJsParser18ValidateHeapAccessEv, @function
_ZN2v88internal4wasm11AsmJsParser18ValidateHeapAccessEv:
.LFB18277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	8(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	16(%rdi), %r12d
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$255, %r12d
	jle	.L2749
	movq	344(%rbx), %rdx
	movq	352(%rbx), %rax
	movabsq	$-3689348814741910323, %rcx
	subl	$256, %r12d
	movslq	%r12d, %r12
	subq	%rdx, %rax
	leaq	1(%r12), %rsi
	sarq	$3, %rax
	imulq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L2750
.L2771:
	leaq	(%r12,%r12,4), %rax
	leaq	(%rdx,%rax,8), %r14
.L2751:
	movq	(%r14), %rdi
	call	_ZN2v88internal4wasm7AsmType18ElementSizeInBytesEv@PLT
	cmpl	$91, 16(%rbx)
	movl	%eax, %r12d
	je	.L2754
.L2775:
	leaq	.LC0(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L2748:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2749:
	.cfi_restore_state
	cmpl	$-9999, %r12d
	jge	.L2752
	movq	376(%rbx), %rcx
	movq	384(%rbx), %rax
	movabsq	$-3689348814741910323, %rdx
	subq	%rcx, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movl	$-10000, %edx
	subl	%r12d, %edx
	movslq	%edx, %r12
	leaq	1(%r12), %rsi
	cmpq	%rsi, %rax
	jnb	.L2772
	subq	%rax, %rsi
	leaq	368(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	376(%rbx), %rcx
.L2772:
	leaq	(%r12,%r12,4), %rax
	leaq	(%rcx,%rax,8), %r14
	jmp	.L2751
	.p2align 4,,10
	.p2align 3
.L2750:
	subq	%rax, %rsi
	leaq	336(%rbx), %rdi
	call	_ZNSt6vectorIN2v88internal4wasm11AsmJsParser7VarInfoENS1_13ZoneAllocatorIS4_EEE17_M_default_appendEm
	movq	344(%rbx), %rdx
	jmp	.L2771
	.p2align 4,,10
	.p2align 3
.L2754:
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	cmpl	$-3, 16(%rbx)
	je	.L2776
.L2756:
	movq	(%r14), %rdi
	movl	$131075, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2777
.L2760:
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jnb	.L2763
.L2761:
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2777:
	.cfi_restore_state
	movq	(%r14), %rdi
	movl	$65539, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2760
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2761
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser15ShiftExpressionEv
	cmpb	$0, 540(%rbx)
	jne	.L2748
	movq	712(%rbx), %rsi
	cmpq	$-1, %rsi
	je	.L2778
	movl	720(%rbx), %ecx
	cmpl	$3, %ecx
	ja	.L2779
	movl	$1, %edx
	sall	%cl, %edx
	cmpl	%r12d, %edx
	je	.L2768
	leaq	.LC134(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2748
	.p2align 4,,10
	.p2align 3
.L2763:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser10ExpressionEPNS1_7AsmTypeE
	cmpb	$0, 540(%rbx)
	jne	.L2748
.L2764:
	movl	$257, %esi
	movq	%rax, %rdi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2780
	cmpl	$93, 16(%rbx)
	jne	.L2775
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movq	(%r14), %rax
	movq	%rax, 576(%rbx)
	jmp	.L2748
	.p2align 4,,10
	.p2align 3
.L2776:
	movl	288(%rbx), %r15d
	movq	%r13, %rdi
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	testl	%r15d, %r15d
	js	.L2757
	movl	%r15d, %edx
	movslq	%r12d, %rax
	imulq	%rax, %rdx
	cmpq	$2147483647, %rdx
	ja	.L2757
	cmpl	$93, 16(%rbx)
	movq	%r13, %rdi
	je	.L2781
	call	_ZN2v88internal12AsmJsScanner6RewindEv@PLT
	jmp	.L2756
	.p2align 4,,10
	.p2align 3
.L2757:
	leaq	.LC131(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2748
.L2752:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2780:
	leaq	.LC70(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2748
.L2781:
	call	_ZN2v88internal12AsmJsScanner4NextEv@PLT
	movl	%r12d, %esi
	movq	304(%rbx), %rdi
	imull	%r15d, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movq	(%r14), %rax
	movq	%rax, 576(%rbx)
	jmp	.L2748
.L2779:
	leaq	.LC133(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2748
.L2778:
	leaq	.LC132(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	jmp	.L2748
.L2768:
	movq	304(%rbx), %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal4wasm19WasmFunctionBuilder15DeleteCodeAfterEm@PLT
	movq	304(%rbx), %rdi
	movl	%r12d, %esi
	negl	%esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder12EmitI32ConstEi@PLT
	movq	304(%rbx), %rdi
	movl	$113, %esi
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	-56(%rbp), %rax
	jmp	.L2764
	.cfi_endproc
.LFE18277:
	.size	_ZN2v88internal4wasm11AsmJsParser18ValidateHeapAccessEv, .-_ZN2v88internal4wasm11AsmJsParser18ValidateHeapAccessEv
	.section	.rodata._ZN2v88internal4wasm11AsmJsParser16MemberExpressionEv.str1.1,"aMS",@progbits,1
.LC135:
	.string	"Expected valid heap load"
	.section	.text._ZN2v88internal4wasm11AsmJsParser16MemberExpressionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4wasm11AsmJsParser16MemberExpressionEv
	.type	_ZN2v88internal4wasm11AsmJsParser16MemberExpressionEv, @function
_ZN2v88internal4wasm11AsmJsParser16MemberExpressionEv:
.LFB18241:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	$0, 680(%rdi)
	call	_ZN2v88internal23GetCurrentStackPositionEv@PLT
	cmpq	320(%rbx), %rax
	jb	.L2798
	movq	%rbx, %rdi
	call	_ZN2v88internal4wasm11AsmJsParser18ValidateHeapAccessEv
	cmpb	$0, 540(%rbx)
	jne	.L2782
	cmpl	$61, 16(%rbx)
	movq	576(%rbx), %rdi
	je	.L2799
	movl	$131075, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2800
	movq	576(%rbx), %rdi
	movl	$65539, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2787
	movq	304(%rbx), %rdi
	movl	$216, %esi
.L2796:
	call	_ZN2v88internal4wasm19WasmFunctionBuilder4EmitENS1_10WasmOpcodeE@PLT
	movq	576(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm7AsmType8LoadTypeEv@PLT
.L2793:
	.cfi_restore_state
	leaq	.LC135(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
.L2782:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2798:
	.cfi_restore_state
	leaq	.LC55(%rip), %rax
	movb	$1, 540(%rbx)
	movq	%rax, 544(%rbx)
	movq	32(%rbx), %rax
	movl	%eax, 552(%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2799:
	.cfi_restore_state
	movb	$1, 568(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal4wasm7AsmType9StoreTypeEv@PLT
	.p2align 4,,10
	.p2align 3
.L2800:
	.cfi_restore_state
	movq	304(%rbx), %rdi
	movl	$215, %esi
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2787:
	movq	576(%rbx), %rdi
	movl	$524291, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2801
	movq	576(%rbx), %rdi
	movl	$262147, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2789
	movq	304(%rbx), %rdi
	movl	$218, %esi
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2801:
	movq	304(%rbx), %rdi
	movl	$217, %esi
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2789:
	movq	576(%rbx), %rdi
	movl	$2097155, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2797
	movq	576(%rbx), %rdi
	movl	$1048579, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	jne	.L2797
	movq	576(%rbx), %rdi
	movl	$4194307, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2792
	movq	304(%rbx), %rdi
	movl	$220, %esi
	jmp	.L2796
	.p2align 4,,10
	.p2align 3
.L2797:
	movq	304(%rbx), %rdi
	movl	$219, %esi
	jmp	.L2796
.L2792:
	movq	576(%rbx), %rdi
	movl	$8388611, %esi
	call	_ZN2v88internal4wasm7AsmType3IsAEPS2_@PLT
	testb	%al, %al
	je	.L2793
	movq	304(%rbx), %rdi
	movl	$221, %esi
	jmp	.L2796
	.cfi_endproc
.LFE18241:
	.size	_ZN2v88internal4wasm11AsmJsParser16MemberExpressionEv, .-_ZN2v88internal4wasm11AsmJsParser16MemberExpressionEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4wasm11AsmJsParserC2EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4wasm11AsmJsParserC2EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE, @function
_GLOBAL__sub_I__ZN2v88internal4wasm11AsmJsParserC2EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE:
.LFB23177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE23177:
	.size	_GLOBAL__sub_I__ZN2v88internal4wasm11AsmJsParserC2EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE, .-_GLOBAL__sub_I__ZN2v88internal4wasm11AsmJsParserC2EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4wasm11AsmJsParserC2EPNS0_4ZoneEmPNS0_20Utf16CharacterStreamE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC11:
	.long	2333366121
	.long	1074118410
	.align 8
.LC12:
	.long	3149223190
	.long	1073900465
	.align 8
.LC13:
	.long	4277811695
	.long	1072049730
	.align 8
.LC14:
	.long	1697350398
	.long	1073157447
	.align 8
.LC15:
	.long	354870542
	.long	1071369083
	.align 8
.LC16:
	.long	1413754136
	.long	1074340347
	.align 8
.LC17:
	.long	1719614413
	.long	1072079006
	.align 8
.LC18:
	.long	1719614413
	.long	1073127582
	.align 8
.LC20:
	.long	0
	.long	2146435072
	.align 8
.LC21:
	.long	0
	.long	2146959360
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC26:
	.long	-1
	.long	0
	.long	0
	.long	0
	.align 16
.LC27:
	.quad	2147483649
	.quad	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC42:
	.long	2139095039
	.align 4
.LC43:
	.long	2139095040
	.align 4
.LC44:
	.long	4286578687
	.align 4
.LC45:
	.long	4286578688
	.section	.rodata.cst16
	.align 16
.LC49:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC50:
	.long	3758096384
	.long	1206910975
	.align 8
.LC51:
	.long	4026531839
	.long	1206910975
	.align 8
.LC52:
	.long	3758096384
	.long	-940572673
	.align 8
.LC53:
	.long	4026531839
	.long	-940572673
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
