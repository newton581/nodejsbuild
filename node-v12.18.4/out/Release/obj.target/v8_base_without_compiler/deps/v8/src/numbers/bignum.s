	.file	"bignum.cc"
	.text
	.section	.text._ZN2v88internal6Bignum13SubtractTimesERKS1_i.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal6Bignum13SubtractTimesERKS1_i.part.0, @function
_ZN2v88internal6Bignum13SubtractTimesERKS1_i.part.0:
.LFB5818:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edx, %r11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	528(%rsi), %eax
	movl	532(%rsi), %edx
	subl	532(%rdi), %edx
	movl	%edx, %r8d
	testl	%eax, %eax
	jle	.L2
	movslq	%edx, %r9
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	salq	$2, %r9
	.p2align 4,,10
	.p2align 3
.L4:
	movq	512(%rsi), %rdx
	movq	512(%rdi), %r10
	movl	(%rdx,%rcx,4), %edx
	addq	%r9, %r10
	addq	$1, %rcx
	addq	$4, %r9
	movl	(%r10), %ebx
	imulq	%r11, %rdx
	addq	%rax, %rdx
	movl	%edx, %eax
	shrq	$28, %rdx
	andl	$268435455, %eax
	subl	%eax, %ebx
	movl	%ebx, %eax
	andl	$268435455, %ebx
	shrl	$31, %eax
	movl	%ebx, (%r10)
	addl	%edx, %eax
	movl	528(%rsi), %edx
	cmpl	%ecx, %edx
	jg	.L4
	movslq	528(%rdi), %rsi
	addl	%r8d, %edx
	cmpl	%esi, %edx
	jge	.L5
	testl	%eax, %eax
	je	.L1
	movslq	%edx, %rcx
	salq	$2, %rcx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$4, %rcx
	testl	%eax, %eax
	je	.L1
.L8:
	movq	512(%rdi), %rsi
	addl	$1, %edx
	addq	%rcx, %rsi
	movl	(%rsi), %ebx
	subl	%eax, %ebx
	movl	%ebx, %r8d
	movl	%ebx, %eax
	andl	$268435455, %r8d
	shrl	$31, %eax
	movl	%r8d, (%rsi)
	movslq	528(%rdi), %rsi
	cmpl	%edx, %esi
	jg	.L20
.L5:
	testl	%esi, %esi
	jle	.L10
	leal	-1(%rsi), %r8d
	leaq	-2(%rsi), %rdx
	movq	512(%rdi), %rcx
	movslq	%r8d, %rax
	movl	%r8d, %r8d
	subq	%r8, %rdx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L24:
	movl	%eax, 528(%rdi)
	subq	$1, %rax
	cmpq	%rax, %rdx
	je	.L10
.L12:
	movl	(%rcx,%rax,4), %r8d
	movl	%eax, %esi
	testl	%r8d, %r8d
	je	.L24
.L1:
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	testl	%esi, %esi
	jne	.L1
	movl	$0, 532(%rdi)
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movslq	528(%rdi), %rsi
	addl	%edx, %eax
	cmpl	%esi, %eax
	jge	.L5
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5818:
	.size	_ZN2v88internal6Bignum13SubtractTimesERKS1_i.part.0, .-_ZN2v88internal6Bignum13SubtractTimesERKS1_i.part.0
	.section	.text._ZN2v88internal6BignumC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6BignumC2Ev
	.type	_ZN2v88internal6BignumC2Ev, @function
_ZN2v88internal6BignumC2Ev:
.LFB5006:
	.cfi_startproc
	endbr64
	movq	%rdi, 512(%rdi)
	movl	$4, %eax
	movq	$128, 520(%rdi)
	movq	$0, 528(%rdi)
	movl	$0, (%rdi)
	.p2align 4,,10
	.p2align 3
.L26:
	movq	512(%rdi), %rdx
	movl	$0, (%rdx,%rax)
	addq	$4, %rax
	cmpq	$512, %rax
	jne	.L26
	ret
	.cfi_endproc
.LFE5006:
	.size	_ZN2v88internal6BignumC2Ev, .-_ZN2v88internal6BignumC2Ev
	.globl	_ZN2v88internal6BignumC1Ev
	.set	_ZN2v88internal6BignumC1Ev,_ZN2v88internal6BignumC2Ev
	.section	.text._ZN2v88internal6Bignum12AssignUInt16Et,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum12AssignUInt16Et
	.type	_ZN2v88internal6Bignum12AssignUInt16Et, @function
_ZN2v88internal6Bignum12AssignUInt16Et:
.LFB5009:
	.cfi_startproc
	endbr64
	movl	528(%rdi), %eax
	testl	%eax, %eax
	jle	.L29
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L30:
	movq	512(%rdi), %rdx
	movl	$0, (%rdx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, 528(%rdi)
	jg	.L30
.L29:
	movq	$0, 528(%rdi)
	testw	%si, %si
	je	.L28
	movq	512(%rdi), %rax
	movzwl	%si, %esi
	movl	%esi, (%rax)
	movl	$1, 528(%rdi)
.L28:
	ret
	.cfi_endproc
.LFE5009:
	.size	_ZN2v88internal6Bignum12AssignUInt16Et, .-_ZN2v88internal6Bignum12AssignUInt16Et
	.section	.text._ZN2v88internal6Bignum12AssignUInt64Em,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum12AssignUInt64Em
	.type	_ZN2v88internal6Bignum12AssignUInt64Em, @function
_ZN2v88internal6Bignum12AssignUInt64Em:
.LFB5010:
	.cfi_startproc
	endbr64
	movl	528(%rdi), %r8d
	testl	%r8d, %r8d
	jle	.L34
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L35:
	movq	512(%rdi), %rdx
	movl	$0, (%rdx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, 528(%rdi)
	jg	.L35
.L34:
	movq	$0, 528(%rdi)
	testq	%rsi, %rsi
	je	.L33
	movq	512(%rdi), %rax
	movl	%esi, %edx
	andl	$268435455, %edx
	movl	%edx, (%rax)
	movq	%rsi, %rax
	movq	512(%rdi), %rdx
	shrq	$56, %rsi
	shrq	$28, %rax
	andl	$268435455, %eax
	movl	%eax, 4(%rdx)
	movq	512(%rdi), %rax
	movl	%esi, 8(%rax)
	movq	512(%rdi), %rax
	movl	$3, 528(%rdi)
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L33
	movl	$2, 528(%rdi)
	movl	4(%rax), %edx
	testl	%edx, %edx
	jne	.L33
	movl	$1, 528(%rdi)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L33
	movq	$0, 528(%rdi)
.L33:
	ret
	.cfi_endproc
.LFE5010:
	.size	_ZN2v88internal6Bignum12AssignUInt64Em, .-_ZN2v88internal6Bignum12AssignUInt64Em
	.section	.text._ZN2v88internal6Bignum12AssignBignumERKS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum12AssignBignumERKS1_
	.type	_ZN2v88internal6Bignum12AssignBignumERKS1_, @function
_ZN2v88internal6Bignum12AssignBignumERKS1_:
.LFB5011:
	.cfi_startproc
	endbr64
	movl	532(%rsi), %eax
	xorl	%edx, %edx
	movl	%eax, 532(%rdi)
	movl	528(%rsi), %eax
	testl	%eax, %eax
	jle	.L44
	.p2align 4,,10
	.p2align 3
.L41:
	movq	512(%rsi), %rax
	movl	(%rax,%rdx,4), %ecx
	movq	512(%rdi), %rax
	movl	%ecx, (%rax,%rdx,4)
	movl	528(%rsi), %eax
	addq	$1, %rdx
	cmpl	%edx, %eax
	jg	.L41
.L44:
	movslq	%eax, %rdx
	salq	$2, %rdx
	cmpl	%eax, 528(%rdi)
	jle	.L42
	.p2align 4,,10
	.p2align 3
.L45:
	movq	512(%rdi), %rcx
	addl	$1, %eax
	movl	$0, (%rcx,%rdx)
	addq	$4, %rdx
	cmpl	%eax, 528(%rdi)
	jg	.L45
	movl	528(%rsi), %eax
.L42:
	movl	%eax, 528(%rdi)
	ret
	.cfi_endproc
.LFE5011:
	.size	_ZN2v88internal6Bignum12AssignBignumERKS1_, .-_ZN2v88internal6Bignum12AssignBignumERKS1_
	.section	.rodata._ZN2v88internal6Bignum15AssignHexStringENS0_6VectorIKcEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internal6Bignum15AssignHexStringENS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum15AssignHexStringENS0_6VectorIKcEE
	.type	_ZN2v88internal6Bignum15AssignHexStringENS0_6VectorIKcEE, @function
_ZN2v88internal6Bignum15AssignHexStringENS0_6VectorIKcEE:
.LFB5015:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	528(%rdi), %r8d
	testl	%r8d, %r8d
	jle	.L54
	.p2align 4,,10
	.p2align 3
.L51:
	movq	512(%rdi), %rcx
	movl	$0, (%rcx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, 528(%rdi)
	jg	.L51
.L54:
	movq	$0, 528(%rdi)
	movslq	%edx, %r9
	movl	%edx, %eax
	imulq	$-1840700269, %r9, %r9
	sarl	$31, %eax
	shrq	$32, %r9
	addl	%edx, %r9d
	sarl	$2, %r9d
	subl	%eax, %r9d
	cmpl	$895, %edx
	jg	.L52
	leal	-1(%rdx), %r8d
	cmpl	$6, %edx
	jle	.L55
	movslq	%r8d, %rax
	leal	-8(%rdx), %r11d
	xorl	%r10d, %r10d
	addq	%rsi, %rax
	subl	%eax, %r11d
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L99:
	leal	-97(%rcx), %r8d
	cmpb	$5, %r8b
	jbe	.L57
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L52
	leal	-55(%rcx), %r8d
.L58:
	movsbl	-1(%rax), %ecx
	leal	-48(%rcx), %ebx
	movl	%ecx, %edx
	cmpb	$9, %bl
	jbe	.L59
.L100:
	leal	-97(%rcx), %ebx
	cmpb	$5, %bl
	jbe	.L60
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L52
	subl	$55, %ecx
.L61:
	sall	$4, %ecx
	addl	%r8d, %ecx
	movsbl	-2(%rax), %r8d
	leal	-48(%r8), %ebx
	movl	%r8d, %edx
	cmpb	$9, %bl
	jbe	.L62
	leal	-97(%r8), %ebx
	cmpb	$5, %bl
	jbe	.L63
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L52
	subl	$55, %r8d
.L64:
	sall	$8, %r8d
	addl	%r8d, %ecx
	movsbl	-3(%rax), %r8d
	leal	-48(%r8), %ebx
	movl	%r8d, %edx
	cmpb	$9, %bl
	jbe	.L65
	leal	-97(%r8), %ebx
	cmpb	$5, %bl
	jbe	.L66
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L52
	subl	$55, %r8d
.L67:
	sall	$12, %r8d
	addl	%ecx, %r8d
	movsbl	-4(%rax), %ecx
	leal	-48(%rcx), %ebx
	movl	%ecx, %edx
	cmpb	$9, %bl
	jbe	.L68
	leal	-97(%rcx), %ebx
	cmpb	$5, %bl
	jbe	.L69
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L52
	subl	$55, %ecx
.L70:
	movsbl	-5(%rax), %edx
	sall	$16, %ecx
	addl	%ecx, %r8d
	leal	-48(%rdx), %ebx
	movl	%edx, %ecx
	cmpb	$9, %bl
	jbe	.L71
	leal	-97(%rdx), %ebx
	cmpb	$5, %bl
	jbe	.L72
	subl	$65, %ecx
	cmpb	$5, %cl
	ja	.L52
	subl	$55, %edx
.L73:
	sall	$20, %edx
	leal	(%rdx,%r8), %ecx
	movsbl	-6(%rax), %edx
	leal	(%r11,%rax), %r8d
	leal	-48(%rdx), %r12d
	movl	%edx, %ebx
	cmpb	$9, %r12b
	jbe	.L74
	leal	-97(%rdx), %r12d
	cmpb	$5, %r12b
	jbe	.L75
	subl	$65, %ebx
	cmpb	$5, %bl
	ja	.L52
	subl	$55, %edx
.L76:
	movq	512(%rdi), %rbx
	sall	$24, %edx
	subq	$7, %rax
	addl	%ecx, %edx
	movl	%edx, (%rbx,%r10,4)
	addq	$1, %r10
	cmpl	%r10d, %r9d
	jle	.L55
.L77:
	movsbl	(%rax), %ecx
	leal	-48(%rcx), %r8d
	movl	%ecx, %edx
	cmpb	$9, %r8b
	ja	.L99
	leal	-48(%rcx), %r8d
	movsbl	-1(%rax), %ecx
	leal	-48(%rcx), %ebx
	movl	%ecx, %edx
	cmpb	$9, %bl
	ja	.L100
.L59:
	subl	$48, %ecx
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L71:
	subl	$48, %edx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L68:
	subl	$48, %ecx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L65:
	subl	$48, %r8d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L62:
	subl	$48, %r8d
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L74:
	subl	$48, %edx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L57:
	leal	-87(%rcx), %r8d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L72:
	subl	$87, %edx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L75:
	subl	$87, %edx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L69:
	subl	$87, %ecx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L66:
	subl	$87, %r8d
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L63:
	subl	$87, %r8d
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L60:
	subl	$87, %ecx
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L55:
	movl	%r9d, 528(%rdi)
	testl	%r8d, %r8d
	js	.L78
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L79:
	leal	-97(%r10), %r11d
	cmpb	$5, %r11b
	jbe	.L101
	subl	$65, %eax
	cmpb	$5, %al
	ja	.L52
	leal	-55(%r10), %eax
.L80:
	addq	$1, %rcx
	addl	%edx, %eax
	cmpl	%ecx, %r8d
	jl	.L102
.L82:
	movsbl	(%rsi,%rcx), %r10d
	sall	$4, %eax
	movl	%eax, %edx
	leal	-48(%r10), %r11d
	movl	%r10d, %eax
	cmpb	$9, %r11b
	ja	.L79
	leal	-48(%r10), %eax
	addq	$1, %rcx
	addl	%edx, %eax
	cmpl	%ecx, %r8d
	jge	.L82
.L102:
	testl	%eax, %eax
	je	.L78
	movq	512(%rdi), %rdx
	movslq	%r9d, %r9
	movl	%eax, (%rdx,%r9,4)
	movl	528(%rdi), %eax
	leal	1(%rax), %r9d
	movl	%r9d, 528(%rdi)
.L78:
	testl	%r9d, %r9d
	jle	.L83
	leal	-1(%r9), %esi
	movslq	%r9d, %rdx
	movq	512(%rdi), %rcx
	movslq	%esi, %rax
	subq	$2, %rdx
	movl	%esi, %esi
	subq	%rsi, %rdx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L103:
	movl	%eax, 528(%rdi)
	subq	$1, %rax
	cmpq	%rax, %rdx
	je	.L83
.L86:
	movl	(%rcx,%rax,4), %esi
	movl	%eax, %r9d
	testl	%esi, %esi
	je	.L103
.L50:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore_state
	leal	-87(%r10), %eax
	jmp	.L80
.L83:
	testl	%r9d, %r9d
	jne	.L50
	movl	$0, 532(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L52:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5015:
	.size	_ZN2v88internal6Bignum15AssignHexStringENS0_6VectorIKcEE, .-_ZN2v88internal6Bignum15AssignHexStringENS0_6VectorIKcEE
	.section	.text._ZN2v88internal6Bignum9AddBignumERKS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum9AddBignumERKS1_
	.type	_ZN2v88internal6Bignum9AddBignumERKS1_, @function
_ZN2v88internal6Bignum9AddBignumERKS1_:
.LFB5017:
	.cfi_startproc
	endbr64
	movl	532(%rdi), %r8d
	movl	532(%rsi), %edx
	movl	528(%rdi), %ecx
	cmpl	%edx, %r8d
	jle	.L105
	subl	%edx, %r8d
	leal	(%r8,%rcx), %eax
	cmpl	$128, %eax
	jg	.L111
	movl	%ecx, %r9d
	subl	$1, %r9d
	js	.L109
	movslq	%r9d, %rax
	movslq	%ecx, %rdx
	movl	%r9d, %r9d
	subq	%r9, %rdx
	movslq	%r8d, %r9
	salq	$2, %rax
	leaq	-8(,%rdx,4), %r10
	salq	$2, %r9
	.p2align 4,,10
	.p2align 3
.L110:
	movq	512(%rdi), %rdx
	movl	(%rdx,%rax), %ecx
	addq	%r9, %rdx
	movl	%ecx, (%rdx,%rax)
	subq	$4, %rax
	cmpq	%rax, %r10
	jne	.L110
.L109:
	leal	-1(%r8), %eax
	leaq	4(,%rax,4), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L108:
	movq	512(%rdi), %rdx
	movl	$0, (%rdx,%rax)
	addq	$4, %rax
	cmpq	%rax, %rcx
	jne	.L108
	movl	528(%rdi), %ecx
	movl	532(%rdi), %eax
	addl	%r8d, %ecx
	subl	%r8d, %eax
	movl	%ecx, 528(%rdi)
	movl	%eax, %r8d
	movl	%eax, 532(%rdi)
	movl	532(%rsi), %edx
.L105:
	movl	528(%rsi), %r9d
	leal	(%rcx,%r8), %r10d
	leal	(%r9,%rdx), %eax
	cmpl	%r10d, %eax
	cmovl	%r10d, %eax
	addl	$1, %eax
	subl	%r8d, %eax
	cmpl	$128, %eax
	jg	.L111
	subl	%r8d, %edx
	testl	%r9d, %r9d
	jle	.L112
	movslq	%edx, %r8
	xorl	%ecx, %ecx
	leal	1(%rdx), %r11d
	xorl	%eax, %eax
	salq	$2, %r8
	.p2align 4,,10
	.p2align 3
.L117:
	movq	512(%rdi), %r9
	movq	512(%rsi), %r10
	addq	%r8, %r9
	addq	$4, %r8
	movl	(%r9), %edx
	addl	(%r10,%rcx,4), %edx
	addl	%edx, %eax
	movl	%eax, %edx
	shrl	$28, %eax
	andl	$268435455, %edx
	movl	%edx, (%r9)
	leal	(%r11,%rcx), %edx
	addq	$1, %rcx
	cmpl	%ecx, 528(%rsi)
	jg	.L117
	movslq	%edx, %rcx
	salq	$2, %rcx
	testl	%eax, %eax
	je	.L114
	.p2align 4,,10
	.p2align 3
.L118:
	movq	512(%rdi), %rsi
	addl	$1, %edx
	addq	%rcx, %rsi
	addq	$4, %rcx
	addl	(%rsi), %eax
	movl	%eax, %r8d
	andl	$268435455, %r8d
	shrl	$28, %eax
	movl	%r8d, (%rsi)
	jne	.L118
.L114:
	movl	528(%rdi), %ecx
.L112:
	cmpl	%ecx, %edx
	cmovl	%ecx, %edx
	movl	%edx, 528(%rdi)
	ret
.L111:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5017:
	.size	_ZN2v88internal6Bignum9AddBignumERKS1_, .-_ZN2v88internal6Bignum9AddBignumERKS1_
	.section	.text._ZN2v88internal6Bignum9AddUInt64Em.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal6Bignum9AddUInt64Em.part.0, @function
_ZN2v88internal6Bignum9AddUInt64Em.part.0:
.LFB5822:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$544, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-544(%rbp), %r8
	movq	$128, -24(%rbp)
	movl	$4, %eax
	movq	%r8, -32(%rbp)
	movq	$0, -16(%rbp)
	movl	$0, -544(%rbp)
	.p2align 4,,10
	.p2align 3
.L127:
	movq	-32(%rbp), %rdx
	movl	$0, (%rdx,%rax)
	addq	$4, %rax
	cmpq	$512, %rax
	jne	.L127
	movl	-16(%rbp), %r9d
	xorl	%eax, %eax
	testl	%r9d, %r9d
	jle	.L131
	.p2align 4,,10
	.p2align 3
.L128:
	movq	-32(%rbp), %rdx
	movl	$0, (%rdx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, -16(%rbp)
	jg	.L128
.L131:
	movq	$0, -16(%rbp)
	testq	%rsi, %rsi
	je	.L132
	movq	-32(%rbp), %rax
	movl	%esi, %edx
	andl	$268435455, %edx
	movl	%edx, (%rax)
	movq	%rsi, %rax
	movq	-32(%rbp), %rdx
	shrq	$56, %rsi
	shrq	$28, %rax
	andl	$268435455, %eax
	movl	%eax, 4(%rdx)
	movq	-32(%rbp), %rax
	movl	%esi, 8(%rax)
	movq	-32(%rbp), %rax
	movl	$3, -16(%rbp)
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L132
	movl	$2, -16(%rbp)
	movl	4(%rax), %edx
	testl	%edx, %edx
	jne	.L132
	movl	$1, -16(%rbp)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L132
	movq	$0, -16(%rbp)
.L132:
	movq	%r8, %rsi
	call	_ZN2v88internal6Bignum9AddBignumERKS1_
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L142
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L142:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5822:
	.size	_ZN2v88internal6Bignum9AddUInt64Em.part.0, .-_ZN2v88internal6Bignum9AddUInt64Em.part.0
	.section	.text._ZN2v88internal6Bignum9AddUInt64Em,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum9AddUInt64Em
	.type	_ZN2v88internal6Bignum9AddUInt64Em, @function
_ZN2v88internal6Bignum9AddUInt64Em:
.LFB5016:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L143
	jmp	_ZN2v88internal6Bignum9AddUInt64Em.part.0
	.p2align 4,,10
	.p2align 3
.L143:
	ret
	.cfi_endproc
.LFE5016:
	.size	_ZN2v88internal6Bignum9AddUInt64Em, .-_ZN2v88internal6Bignum9AddUInt64Em
	.section	.text._ZN2v88internal6Bignum14SubtractBignumERKS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum14SubtractBignumERKS1_
	.type	_ZN2v88internal6Bignum14SubtractBignumERKS1_, @function
_ZN2v88internal6Bignum14SubtractBignumERKS1_:
.LFB5018:
	.cfi_startproc
	endbr64
	movl	532(%rdi), %eax
	movl	532(%rsi), %edx
	cmpl	%edx, %eax
	jle	.L146
	subl	%edx, %eax
	movslq	528(%rdi), %rdx
	movl	%eax, %r10d
	leal	(%rax,%rdx), %eax
	cmpl	$128, %eax
	jg	.L177
	movl	%edx, %ecx
	subl	$1, %ecx
	js	.L150
	movslq	%ecx, %rax
	movl	%ecx, %ecx
	movslq	%r10d, %r8
	salq	$2, %rax
	subq	%rcx, %rdx
	salq	$2, %r8
	leaq	-8(,%rdx,4), %r9
	.p2align 4,,10
	.p2align 3
.L151:
	movq	512(%rdi), %rdx
	movl	(%rdx,%rax), %ecx
	addq	%r8, %rdx
	movl	%ecx, (%rdx,%rax)
	subq	$4, %rax
	cmpq	%rax, %r9
	jne	.L151
.L150:
	leal	-1(%r10), %eax
	leaq	4(,%rax,4), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L149:
	movq	512(%rdi), %rdx
	movl	$0, (%rdx,%rax)
	addq	$4, %rax
	cmpq	%rax, %rcx
	jne	.L149
	movl	532(%rdi), %eax
	addl	%r10d, 528(%rdi)
	subl	%r10d, %eax
	movl	%eax, 532(%rdi)
	movl	532(%rsi), %edx
.L146:
	movl	528(%rsi), %r9d
	subl	%eax, %edx
	testl	%r9d, %r9d
	jle	.L152
	movslq	%edx, %r11
	xorl	%eax, %eax
	xorl	%edx, %edx
	movq	%r11, %r10
	leaq	0(,%r11,4), %rcx
	negq	%r10
	salq	$2, %r10
	.p2align 4,,10
	.p2align 3
.L156:
	movq	512(%rdi), %r8
	addl	$1, %edx
	addq	%rcx, %r8
	movl	(%r8), %r9d
	subl	%eax, %r9d
	movl	%r9d, %eax
	movq	512(%rsi), %r9
	addq	%r10, %r9
	subl	(%r9,%rcx), %eax
	addq	$4, %rcx
	movl	%eax, %r9d
	shrl	$31, %eax
	andl	$268435455, %r9d
	movl	%r9d, (%r8)
	cmpl	%edx, 528(%rsi)
	jg	.L156
	movslq	%edx, %rdx
	addq	%r11, %rdx
	salq	$2, %rdx
	testl	%eax, %eax
	je	.L152
	.p2align 4,,10
	.p2align 3
.L159:
	movq	512(%rdi), %rcx
	addq	%rdx, %rcx
	addq	$4, %rdx
	movl	(%rcx), %eax
	subl	$1, %eax
	movl	%eax, %esi
	andl	$268435455, %esi
	movl	%esi, (%rcx)
	testl	%eax, %eax
	js	.L159
.L152:
	movslq	528(%rdi), %rdx
	testl	%edx, %edx
	jle	.L158
	leal	-1(%rdx), %r8d
	leaq	-2(%rdx), %rcx
	movq	512(%rdi), %rsi
	movslq	%r8d, %rax
	movl	%r8d, %r8d
	subq	%r8, %rcx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L178:
	movl	%eax, 528(%rdi)
	subq	$1, %rax
	cmpq	%rax, %rcx
	je	.L158
.L162:
	movl	(%rsi,%rax,4), %r8d
	movl	%eax, %edx
	testl	%r8d, %r8d
	je	.L178
.L145:
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	testl	%edx, %edx
	jne	.L145
	movl	$0, 532(%rdi)
	ret
.L177:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5018:
	.size	_ZN2v88internal6Bignum14SubtractBignumERKS1_, .-_ZN2v88internal6Bignum14SubtractBignumERKS1_
	.section	.text._ZN2v88internal6Bignum9ShiftLeftEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum9ShiftLeftEi
	.type	_ZN2v88internal6Bignum9ShiftLeftEi, @function
_ZN2v88internal6Bignum9ShiftLeftEi:
.LFB5019:
	.cfi_startproc
	endbr64
	movl	528(%rdi), %edx
	testl	%edx, %edx
	je	.L179
	movslq	%esi, %rax
	movl	%esi, %ecx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%esi, %eax
	sarl	$4, %eax
	subl	%ecx, %eax
	addl	%eax, 532(%rdi)
	imull	$28, %eax, %eax
	subl	%eax, %esi
	cmpl	$127, %edx
	jg	.L192
	testl	%edx, %edx
	jle	.L179
	movl	$28, %r10d
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	subl	%esi, %r10d
	.p2align 4,,10
	.p2align 3
.L184:
	movq	512(%rdi), %rax
	movl	%r8d, %r11d
	movl	%r10d, %ecx
	leaq	(%rax,%rdx,4), %r9
	addq	$1, %rdx
	movl	(%r9), %eax
	movl	%eax, %r8d
	shrl	%cl, %r8d
	movl	%esi, %ecx
	sall	%cl, %eax
	addl	%r11d, %eax
	andl	$268435455, %eax
	movl	%eax, (%r9)
	movslq	528(%rdi), %rax
	cmpl	%edx, %eax
	jg	.L184
	testl	%r8d, %r8d
	je	.L179
	movq	512(%rdi), %rdx
	movl	%r8d, (%rdx,%rax,4)
	addl	$1, 528(%rdi)
.L179:
	ret
.L192:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5019:
	.size	_ZN2v88internal6Bignum9ShiftLeftEi, .-_ZN2v88internal6Bignum9ShiftLeftEi
	.section	.text._ZN2v88internal6Bignum16MultiplyByUInt32Ej,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum16MultiplyByUInt32Ej
	.type	_ZN2v88internal6Bignum16MultiplyByUInt32Ej, @function
_ZN2v88internal6Bignum16MultiplyByUInt32Ej:
.LFB5020:
	.cfi_startproc
	endbr64
	cmpl	$1, %esi
	je	.L193
	movl	528(%rdi), %edx
	testl	%esi, %esi
	je	.L217
	testl	%edx, %edx
	jle	.L218
	movl	%esi, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L203:
	movq	512(%rdi), %rax
	leaq	(%rax,%rcx,4), %rsi
	addq	$1, %rcx
	movl	(%rsi), %eax
	imulq	%r8, %rax
	addq	%rax, %rdx
	movl	%edx, %eax
	shrq	$28, %rdx
	andl	$268435455, %eax
	movl	%eax, (%rsi)
	movl	528(%rdi), %eax
	cmpl	%ecx, %eax
	jg	.L203
	testq	%rdx, %rdx
	je	.L219
	.p2align 4,,10
	.p2align 3
.L201:
	cmpl	$127, %eax
	jg	.L220
	movl	%edx, %esi
	movq	512(%rdi), %rcx
	cltq
	andl	$268435455, %esi
	movl	%esi, (%rcx,%rax,4)
	movl	528(%rdi), %eax
	addl	$1, %eax
	shrq	$28, %rdx
	movl	%eax, 528(%rdi)
	jne	.L201
.L193:
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L198
	.p2align 4,,10
	.p2align 3
.L197:
	movq	512(%rdi), %rdx
	movl	$0, (%rdx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, 528(%rdi)
	jg	.L197
.L198:
	movq	$0, 528(%rdi)
	ret
.L218:
	ret
.L219:
	ret
.L220:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5020:
	.size	_ZN2v88internal6Bignum16MultiplyByUInt32Ej, .-_ZN2v88internal6Bignum16MultiplyByUInt32Ej
	.section	.text._ZN2v88internal6Bignum16MultiplyByUInt64Em,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum16MultiplyByUInt64Em
	.type	_ZN2v88internal6Bignum16MultiplyByUInt64Em, @function
_ZN2v88internal6Bignum16MultiplyByUInt64Em:
.LFB5021:
	.cfi_startproc
	endbr64
	cmpq	$1, %rsi
	je	.L221
	movl	528(%rdi), %eax
	testq	%rsi, %rsi
	je	.L242
	movl	%esi, %r9d
	shrq	$32, %rsi
	testl	%eax, %eax
	jle	.L221
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L228:
	movq	512(%rdi), %rax
	movq	%r9, %rcx
	movq	%rdx, %r11
	shrq	$28, %rdx
	andl	$268435455, %r11d
	leaq	(%rax,%r8,4), %r10
	addq	$1, %r8
	movl	(%r10), %eax
	imulq	%rax, %rcx
	imulq	%rsi, %rax
	addq	%r11, %rcx
	movl	%ecx, %r11d
	salq	$4, %rax
	addq	%rdx, %rax
	andl	$268435455, %r11d
	shrq	$28, %rcx
	movl	%r11d, (%r10)
	leaq	(%rcx,%rax), %rdx
	movl	528(%rdi), %eax
	cmpl	%r8d, %eax
	jg	.L228
	testq	%rdx, %rdx
	je	.L243
	.p2align 4,,10
	.p2align 3
.L229:
	cmpl	$127, %eax
	jg	.L244
	movl	%edx, %esi
	movq	512(%rdi), %rcx
	cltq
	andl	$268435455, %esi
	movl	%esi, (%rcx,%rax,4)
	movl	528(%rdi), %eax
	addl	$1, %eax
	shrq	$28, %rdx
	movl	%eax, 528(%rdi)
	jne	.L229
.L221:
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	testl	%eax, %eax
	jle	.L226
	.p2align 4,,10
	.p2align 3
.L225:
	movq	512(%rdi), %rax
	movl	$0, (%rax,%rsi,4)
	addq	$1, %rsi
	cmpl	%esi, 528(%rdi)
	jg	.L225
.L226:
	movq	$0, 528(%rdi)
	ret
.L243:
	ret
.L244:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5021:
	.size	_ZN2v88internal6Bignum16MultiplyByUInt64Em, .-_ZN2v88internal6Bignum16MultiplyByUInt64Em
	.section	.text._ZN2v88internal6Bignum20MultiplyByPowerOfTenEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum20MultiplyByPowerOfTenEi
	.type	_ZN2v88internal6Bignum20MultiplyByPowerOfTenEi, @function
_ZN2v88internal6Bignum20MultiplyByPowerOfTenEi:
.LFB5022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movdqa	.LC1(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movdqa	.LC2(%rip), %xmm0
	movaps	%xmm0, -64(%rbp)
	movdqa	.LC3(%rip), %xmm0
	movaps	%xmm0, -48(%rbp)
	testl	%esi, %esi
	je	.L245
	movl	528(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L245
	movl	%esi, %r12d
	movl	%esi, %edi
	cmpl	$26, %esi
	jle	.L247
	movl	$4195354525, %r9d
	.p2align 4,,10
	.p2align 3
.L255:
	testl	%eax, %eax
	jle	.L248
	xorl	%r8d, %r8d
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L249:
	movq	512(%rbx), %rax
	movq	%r10, %rsi
	andl	$268435455, %esi
	leaq	(%rax,%r8,4), %rcx
	addq	$1, %r8
	movl	(%rcx), %eax
	movq	%rax, %rdx
	imulq	$1734723475, %rax, %rax
	imulq	%r9, %rdx
	salq	$4, %rax
	addq	%rdx, %rsi
	movl	%esi, %edx
	shrq	$28, %rsi
	andl	$268435455, %edx
	movl	%edx, (%rcx)
	movq	%rax, %rdx
	movq	%r10, %rax
	shrq	$28, %rax
	addq	%rdx, %rax
	leaq	(%rsi,%rax), %r10
	movl	528(%rbx), %eax
	cmpl	%r8d, %eax
	jg	.L249
	testq	%r10, %r10
	je	.L248
	.p2align 4,,10
	.p2align 3
.L252:
	cmpl	$127, %eax
	jg	.L260
	movl	%r10d, %ecx
	movq	512(%rbx), %rdx
	cltq
	andl	$268435455, %ecx
	movl	%ecx, (%rdx,%rax,4)
	movl	528(%rbx), %eax
	addl	$1, %eax
	shrq	$28, %r10
	movl	%eax, 528(%rbx)
	jne	.L252
.L248:
	subl	$27, %edi
	cmpl	$26, %edi
	jg	.L255
.L247:
	cmpl	$12, %edi
	jle	.L250
.L251:
	testl	%eax, %eax
	jle	.L256
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L257:
	movq	512(%rbx), %rax
	leaq	(%rax,%rcx,4), %rsi
	addq	$1, %rcx
	movl	(%rsi), %eax
	imulq	$1220703125, %rax, %rax
	addq	%rax, %rdx
	movl	%edx, %eax
	shrq	$28, %rdx
	andl	$268435455, %eax
	movl	%eax, (%rsi)
	movl	528(%rbx), %eax
	cmpl	%ecx, %eax
	jg	.L257
	testq	%rdx, %rdx
	je	.L256
	.p2align 4,,10
	.p2align 3
.L258:
	cmpl	$127, %eax
	jg	.L260
	movl	%edx, %esi
	movq	512(%rbx), %rcx
	cltq
	andl	$268435455, %esi
	movl	%esi, (%rcx,%rax,4)
	movl	528(%rbx), %eax
	addl	$1, %eax
	shrq	$28, %rdx
	movl	%eax, 528(%rbx)
	jne	.L258
.L256:
	subl	$13, %edi
	cmpl	$13, %edi
	je	.L251
.L250:
	testl	%edi, %edi
	jle	.L261
	subl	$1, %edi
	movslq	%edi, %rdi
	movl	-80(%rbp,%rdi,4), %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej
	movl	528(%rbx), %eax
.L261:
	testl	%eax, %eax
	je	.L245
	movslq	%r12d, %rdx
	movl	%r12d, %ecx
	imulq	$-1840700269, %rdx, %rdx
	sarl	$31, %ecx
	shrq	$32, %rdx
	addl	%r12d, %edx
	sarl	$4, %edx
	subl	%ecx, %edx
	addl	%edx, 532(%rbx)
	imull	$28, %edx, %edx
	subl	%edx, %r12d
	cmpl	$127, %eax
	jg	.L260
	testl	%eax, %eax
	jle	.L245
	movl	$28, %esi
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	subl	%r12d, %esi
	.p2align 4,,10
	.p2align 3
.L262:
	movq	512(%rbx), %rax
	movl	%r8d, %r9d
	movl	%esi, %ecx
	leaq	(%rax,%rdx,4), %rdi
	addq	$1, %rdx
	movl	(%rdi), %eax
	movl	%eax, %r8d
	shrl	%cl, %r8d
	movl	%r12d, %ecx
	sall	%cl, %eax
	addl	%r9d, %eax
	andl	$268435455, %eax
	movl	%eax, (%rdi)
	movslq	528(%rbx), %rax
	cmpl	%edx, %eax
	jg	.L262
	testl	%r8d, %r8d
	je	.L245
	movq	512(%rbx), %rdx
	movl	%r8d, (%rdx,%rax,4)
	addl	$1, 528(%rbx)
	.p2align 4,,10
	.p2align 3
.L245:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L290
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L260:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L290:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5022:
	.size	_ZN2v88internal6Bignum20MultiplyByPowerOfTenEi, .-_ZN2v88internal6Bignum20MultiplyByPowerOfTenEi
	.section	.text._ZN2v88internal6Bignum19AssignDecimalStringENS0_6VectorIKcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum19AssignDecimalStringENS0_6VectorIKcEE
	.type	_ZN2v88internal6Bignum19AssignDecimalStringENS0_6VectorIKcEE, @function
_ZN2v88internal6Bignum19AssignDecimalStringENS0_6VectorIKcEE:
.LFB5013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	528(%rdi), %r8d
	movq	%rsi, -56(%rbp)
	testl	%r8d, %r8d
	jle	.L295
	.p2align 4,,10
	.p2align 3
.L292:
	movq	512(%r14), %rcx
	movl	$0, (%rcx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, 528(%r14)
	jg	.L292
.L295:
	movq	$0, 528(%r14)
	movl	%edx, %r12d
	cmpl	$18, %edx
	jle	.L318
	movq	-56(%rbp), %rax
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	leaq	19(%rax), %r15
	.p2align 4,,10
	.p2align 3
.L294:
	addl	$19, %ebx
	leaq	-19(%r15), %rdx
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L297:
	movsbl	(%rdx), %eax
	leaq	0(%r13,%r13,4), %rcx
	addq	$1, %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %r13
	cmpq	%rdx, %r15
	jne	.L297
	subl	$19, %r12d
	testl	%esi, %esi
	je	.L298
	jle	.L299
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L300:
	movq	512(%r14), %rax
	leaq	(%rax,%rsi,4), %rdi
	addq	$1, %rsi
	movl	(%rdi), %edx
	imulq	$1220703125, %rdx, %rdx
	leaq	(%rdx,%rcx), %rax
	movl	%eax, %edx
	shrq	$28, %rax
	andl	$268435455, %edx
	movq	%rax, %rcx
	movl	%edx, (%rdi)
	movl	528(%r14), %eax
	cmpl	%esi, %eax
	jg	.L300
	testq	%rcx, %rcx
	je	.L299
	.p2align 4,,10
	.p2align 3
.L302:
	cmpl	$127, %eax
	jg	.L301
	movl	%ecx, %esi
	movq	512(%r14), %rdx
	cltq
	andl	$268435455, %esi
	movl	%esi, (%rdx,%rax,4)
	movl	528(%r14), %eax
	addl	$1, %eax
	shrq	$28, %rcx
	movl	%eax, 528(%r14)
	jne	.L302
.L299:
	movl	$15625, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej
	movl	528(%r14), %eax
	testl	%eax, %eax
	je	.L298
	cmpl	$127, %eax
	jg	.L301
	testl	%eax, %eax
	jle	.L298
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L306:
	movq	512(%r14), %rax
	movl	%esi, %edi
	leaq	(%rax,%rdx,4), %rcx
	addq	$1, %rdx
	movl	(%rcx), %eax
	movl	%eax, %esi
	sall	$19, %eax
	addl	%edi, %eax
	shrl	$9, %esi
	andl	$268435455, %eax
	movl	%eax, (%rcx)
	movslq	528(%r14), %rax
	cmpl	%edx, %eax
	jg	.L306
	testl	%esi, %esi
	je	.L298
	movq	512(%r14), %rdx
	movl	%esi, (%rdx,%rax,4)
	addl	$1, 528(%r14)
	.p2align 4,,10
	.p2align 3
.L298:
	testq	%r13, %r13
	je	.L307
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Bignum9AddUInt64Em.part.0
.L307:
	addq	$19, %r15
	cmpl	$18, %r12d
	jle	.L343
	movl	528(%r14), %esi
	jmp	.L294
.L343:
	leal	(%r12,%rbx), %eax
.L293:
	cmpl	%eax, %ebx
	jge	.L308
	movq	-56(%rbp), %rdi
	movslq	%ebx, %rcx
	subl	$1, %eax
	xorl	%r13d, %r13d
	subl	%ebx, %eax
	leaq	1(%rcx,%rdi), %rsi
	leaq	(%rdi,%rcx), %rdx
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L310:
	movsbl	(%rdx), %eax
	leaq	0(%r13,%r13,4), %rcx
	addq	$1, %rdx
	subl	$48, %eax
	cltq
	leaq	(%rax,%rcx,2), %r13
	cmpq	%rdx, %rsi
	jne	.L310
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Bignum20MultiplyByPowerOfTenEi
	testq	%r13, %r13
	je	.L311
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Bignum9AddUInt64Em.part.0
.L311:
	movslq	528(%r14), %rdx
	testl	%edx, %edx
	jle	.L312
	leal	-1(%rdx), %edi
	leaq	-2(%rdx), %rcx
	movq	512(%r14), %rsi
	movslq	%edi, %rax
	movl	%edi, %edi
	subq	%rdi, %rcx
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L344:
	movl	%eax, 528(%r14)
	subq	$1, %rax
	cmpq	%rcx, %rax
	je	.L312
.L315:
	movl	(%rsi,%rax,4), %edi
	movl	%eax, %edx
	testl	%edi, %edi
	je	.L344
.L291:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L312:
	.cfi_restore_state
	testl	%edx, %edx
	jne	.L291
	movl	$0, 532(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L318:
	.cfi_restore_state
	movl	%edx, %eax
	xorl	%ebx, %ebx
	jmp	.L293
.L308:
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal6Bignum20MultiplyByPowerOfTenEi
	jmp	.L311
.L301:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5013:
	.size	_ZN2v88internal6Bignum19AssignDecimalStringENS0_6VectorIKcEE, .-_ZN2v88internal6Bignum19AssignDecimalStringENS0_6VectorIKcEE
	.section	.rodata._ZN2v88internal6Bignum6SquareEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"unimplemented code"
	.section	.text._ZN2v88internal6Bignum6SquareEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum6SquareEv
	.type	_ZN2v88internal6Bignum6SquareEv, @function
_ZN2v88internal6Bignum6SquareEv:
.LFB5023:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	528(%rdi), %r11d
	leal	(%r11,%r11), %r13d
	cmpl	$128, %r13d
	jg	.L382
	cmpl	$255, %r11d
	jg	.L383
	testl	%r11d, %r11d
	jle	.L384
	movslq	%r11d, %r12
	xorl	%eax, %eax
	salq	$2, %r12
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L352:
	movq	512(%rdi), %rcx
	movl	(%rcx,%rax,4), %esi
	addq	$1, %rax
	movl	%esi, (%rcx,%rdx)
	movl	528(%rdi), %r10d
	addq	$4, %rdx
	cmpl	%eax, %r10d
	jg	.L352
	testl	%r10d, %r10d
	jle	.L369
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	leaq	-12(%r12), %r14
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L350:
	movq	512(%rdi), %r9
	leal	1(%r8), %r10d
	cmpl	$2, %r8d
	jle	.L370
	leaq	(%r9,%r12), %rax
	leaq	(%r14,%rbx), %rcx
	pxor	%xmm3, %xmm3
	movl	%r10d, %esi
	shrl	$2, %esi
	addq	%r9, %rcx
	salq	$4, %rsi
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L356:
	movdqu	(%rcx), %xmm5
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	subq	$16, %rcx
	pshufd	$27, %xmm5, %xmm1
	movdqa	%xmm0, %xmm2
	movdqa	%xmm1, %xmm4
	punpckhdq	%xmm0, %xmm2
	punpckldq	%xmm0, %xmm0
	punpckhdq	%xmm1, %xmm4
	punpckldq	%xmm1, %xmm1
	pmuludq	%xmm4, %xmm2
	pmuludq	%xmm1, %xmm0
	paddq	%xmm2, %xmm0
	paddq	%xmm0, %xmm3
	cmpq	%rsi, %rax
	jne	.L356
	movdqa	%xmm3, %xmm0
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm3
	movq	%xmm3, %rax
	addq	%rax, %rdx
	movl	%r10d, %eax
	andl	$-4, %eax
	subl	%eax, %r8d
	cmpl	%r10d, %eax
	je	.L357
.L355:
	leal	(%r11,%rax), %ecx
	leal	(%r11,%r8), %esi
	movslq	%ecx, %rcx
	movslq	%esi, %rsi
	movl	(%r9,%rsi,4), %esi
	movl	(%r9,%rcx,4), %ecx
	imulq	%rsi, %rcx
	leal	-1(%r8), %esi
	addq	%rcx, %rdx
	leal	1(%rax), %ecx
	testl	%r8d, %r8d
	je	.L357
	addl	%r11d, %esi
	addl	%r11d, %ecx
	addl	$2, %eax
	movslq	%esi, %rsi
	movslq	%ecx, %rcx
	movl	(%r9,%rcx,4), %ecx
	movl	(%r9,%rsi,4), %esi
	imulq	%rcx, %rsi
	leal	-2(%r8), %ecx
	addq	%rsi, %rdx
	cmpl	$1, %r8d
	je	.L357
	addl	%r11d, %eax
	addl	%r11d, %ecx
	cltq
	movslq	%ecx, %rcx
	movl	(%r9,%rax,4), %eax
	movl	(%r9,%rcx,4), %ecx
	imulq	%rcx, %rax
	addq	%rax, %rdx
.L357:
	movl	%edx, %eax
	movl	%r10d, %r8d
	shrq	$28, %rdx
	andl	$268435455, %eax
	movl	%eax, (%r9,%rbx)
	movl	528(%rdi), %r10d
	addq	$4, %rbx
	cmpl	%r8d, %r10d
	jg	.L350
.L348:
	cmpl	%r10d, %r13d
	jle	.L353
	movslq	%r10d, %r15
	movslq	%r11d, %rax
	movl	%r13d, -52(%rbp)
	movl	%r10d, %r12d
	salq	$2, %r15
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L354:
	leal	-1(%r12), %r9d
	movl	%r10d, %esi
	movq	512(%rdi), %rbx
	subl	%r9d, %esi
	cmpl	%r12d, %esi
	jge	.L358
	movl	%r12d, %r14d
	subl	%esi, %r14d
	leal	-1(%r14), %eax
	cmpl	$2, %eax
	jbe	.L359
	movslq	%esi, %rax
	movl	%r14d, %r8d
	pxor	%xmm3, %xmm3
	movslq	%r9d, %rcx
	addq	%r13, %rax
	shrl	$2, %r8d
	addq	%r13, %rcx
	leaq	(%rbx,%rax,4), %rax
	salq	$4, %r8
	leaq	-12(%rbx,%rcx,4), %rcx
	addq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L361:
	movdqu	(%rcx), %xmm6
	movdqu	(%rax), %xmm0
	addq	$16, %rax
	subq	$16, %rcx
	pshufd	$27, %xmm6, %xmm1
	movdqa	%xmm0, %xmm2
	movdqa	%xmm1, %xmm4
	punpckhdq	%xmm0, %xmm2
	punpckldq	%xmm0, %xmm0
	punpckhdq	%xmm1, %xmm4
	punpckldq	%xmm1, %xmm1
	pmuludq	%xmm4, %xmm2
	pmuludq	%xmm1, %xmm0
	paddq	%xmm2, %xmm0
	paddq	%xmm0, %xmm3
	cmpq	%rax, %r8
	jne	.L361
	movdqa	%xmm3, %xmm0
	psrldq	$8, %xmm0
	paddq	%xmm0, %xmm3
	movq	%xmm3, %rax
	addq	%rax, %rdx
	movl	%r14d, %eax
	andl	$-4, %eax
	subl	%eax, %r9d
	addl	%eax, %esi
	cmpl	%r14d, %eax
	je	.L358
.L359:
	leal	(%r11,%r9), %eax
	leal	(%r11,%rsi), %ecx
	cltq
	movslq	%ecx, %rcx
	movl	(%rbx,%rcx,4), %ecx
	movl	(%rbx,%rax,4), %eax
	imulq	%rcx, %rax
	leal	-1(%r9), %ecx
	addq	%rax, %rdx
	leal	1(%rsi), %eax
	cmpl	%r12d, %eax
	jge	.L358
	addl	%r11d, %ecx
	addl	%r11d, %eax
	addl	$2, %esi
	subl	$2, %r9d
	movslq	%ecx, %rcx
	cltq
	movl	(%rbx,%rcx,4), %ecx
	movl	(%rbx,%rax,4), %eax
	imulq	%rax, %rcx
	addq	%rcx, %rdx
	cmpl	%r12d, %esi
	jge	.L358
	addl	%r11d, %esi
	addl	%r11d, %r9d
	movslq	%esi, %rsi
	movslq	%r9d, %r9
	movl	(%rbx,%rsi,4), %eax
	movl	(%rbx,%r9,4), %ecx
	imulq	%rcx, %rax
	addq	%rax, %rdx
.L358:
	movl	%edx, %eax
	addl	$1, %r10d
	shrq	$28, %rdx
	andl	$268435455, %eax
	movl	%eax, (%rbx,%r15)
	addq	$4, %r15
	cmpl	%r10d, -52(%rbp)
	je	.L380
	movl	528(%rdi), %r12d
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L380:
	movl	-52(%rbp), %r13d
.L353:
	movl	%r13d, 528(%rdi)
	sall	532(%rdi)
	testl	%r13d, %r13d
	jle	.L363
	leal	-1(%r13), %esi
	movslq	%r13d, %rdx
	movq	512(%rdi), %rcx
	movslq	%esi, %rax
	subq	$2, %rdx
	movl	%esi, %esi
	subq	%rsi, %rdx
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L385:
	movl	%eax, 528(%rdi)
	subq	$1, %rax
	cmpq	%rax, %rdx
	je	.L363
.L366:
	movl	(%rcx,%rax,4), %esi
	movl	%eax, %r13d
	testl	%esi, %esi
	je	.L385
.L345:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L355
.L363:
	testl	%r13d, %r13d
	jne	.L345
	movl	$0, 532(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L384:
	.cfi_restore_state
	movl	%r11d, %r10d
	xorl	%edx, %edx
	jmp	.L348
.L382:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L369:
	xorl	%edx, %edx
	jmp	.L348
.L383:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5023:
	.size	_ZN2v88internal6Bignum6SquareEv, .-_ZN2v88internal6Bignum6SquareEv
	.section	.text._ZN2v88internal6Bignum17AssignPowerUInt16Eti,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum17AssignPowerUInt16Eti
	.type	_ZN2v88internal6Bignum17AssignPowerUInt16Eti, @function
_ZN2v88internal6Bignum17AssignPowerUInt16Eti:
.LFB5024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movl	528(%rdi), %edx
	testl	%r14d, %r14d
	je	.L450
	movzwl	%si, %r15d
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L394
	.p2align 4,,10
	.p2align 3
.L391:
	movq	512(%r12), %rdx
	movl	$0, (%rdx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, 528(%r12)
	jg	.L391
.L394:
	movzwl	%si, %eax
	xorl	%r13d, %r13d
	andl	$1, %esi
	movq	$0, 528(%r12)
	jne	.L392
	.p2align 4,,10
	.p2align 3
.L393:
	sarl	%eax
	addl	$1, %r13d
	movzwl	%ax, %r15d
	testb	$1, %al
	je	.L393
.L392:
	xorl	%edx, %edx
	testl	%eax, %eax
	je	.L395
	.p2align 4,,10
	.p2align 3
.L396:
	addl	$1, %edx
	sarl	%eax
	jne	.L396
	movl	%r14d, %eax
	imull	%edx, %eax
	cmpl	$3555, %eax
	jg	.L409
.L395:
	movzwl	%r15w, %edi
	testl	%r14d, %r14d
	jle	.L415
	movl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L398:
	addl	%ebx, %ebx
	cmpl	%ebx, %r14d
	jge	.L398
	sarl	$2, %ebx
	je	.L416
	movl	$64, %ecx
	movq	$-1, %rax
	xorl	%esi, %esi
	subl	%edx, %ecx
	movl	$4294967295, %edx
	salq	%cl, %rax
	movq	%rax, %rcx
	movq	%rdi, %rax
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L451:
	imulq	%rdi, %rax
.L399:
	sarl	%ebx
	cmpq	%rdx, %rax
	ja	.L418
.L452:
	testl	%ebx, %ebx
	je	.L418
.L400:
	imulq	%rax, %rax
	testl	%ebx, %r14d
	je	.L399
	testq	%rax, %rcx
	je	.L451
	movl	$1, %esi
	sarl	%ebx
	cmpq	%rdx, %rax
	jbe	.L452
	.p2align 4,,10
	.p2align 3
.L418:
	testq	%rax, %rax
	je	.L402
.L397:
	movq	512(%r12), %rdx
	movl	%eax, %ecx
	andl	$268435455, %ecx
	movl	%ecx, (%rdx)
	movq	%rax, %rdx
	movq	512(%r12), %rcx
	shrq	$56, %rax
	shrq	$28, %rdx
	andl	$268435455, %edx
	movl	%edx, 4(%rcx)
	movq	512(%r12), %rdx
	movl	%eax, 8(%rdx)
	movq	512(%r12), %rax
	movl	$3, 528(%r12)
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L402
	movl	$2, 528(%r12)
	movl	4(%rax), %edx
	testl	%edx, %edx
	jne	.L402
	movl	$1, 528(%r12)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L402
	movq	$0, 528(%r12)
	.p2align 4,,10
	.p2align 3
.L402:
	testb	%sil, %sil
	jne	.L453
.L403:
	testl	%ebx, %ebx
	jne	.L404
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L407:
	sarl	%ebx
	je	.L408
.L404:
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum6SquareEv
	testl	%ebx, %r14d
	je	.L407
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej
	sarl	%ebx
	jne	.L404
.L408:
	movl	528(%r12), %edx
	testl	%edx, %edx
	je	.L386
	imull	%r14d, %r13d
	movslq	%r13d, %rax
	movl	%r13d, %ecx
	imulq	$-1840700269, %rax, %rax
	sarl	$31, %ecx
	shrq	$32, %rax
	addl	%r13d, %eax
	sarl	$4, %eax
	subl	%ecx, %eax
	addl	%eax, 532(%r12)
	imull	$28, %eax, %eax
	subl	%eax, %r13d
	cmpl	$127, %edx
	jg	.L409
	testl	%edx, %edx
	jle	.L386
	movl	$28, %r9d
	xorl	%edx, %edx
	xorl	%edi, %edi
	subl	%r13d, %r9d
	.p2align 4,,10
	.p2align 3
.L410:
	movq	512(%r12), %rax
	movl	%edi, %r8d
	movl	%r9d, %ecx
	leaq	(%rax,%rdx,4), %rsi
	addq	$1, %rdx
	movl	(%rsi), %eax
	movl	%eax, %edi
	shrl	%cl, %edi
	movl	%r13d, %ecx
	sall	%cl, %eax
	addl	%r8d, %eax
	andl	$268435455, %eax
	movl	%eax, (%rsi)
	movslq	528(%r12), %rax
	cmpl	%edx, %eax
	jg	.L410
	testl	%edi, %edi
	je	.L386
	movq	512(%r12), %rdx
	movl	%edi, (%rdx,%rax,4)
	addl	$1, 528(%r12)
.L386:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore_state
	movzwl	%r15w, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum16MultiplyByUInt32Ej
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L450:
	xorl	%eax, %eax
	testl	%edx, %edx
	jle	.L390
	.p2align 4,,10
	.p2align 3
.L388:
	movq	512(%r12), %rdx
	movl	$0, (%rdx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, 528(%r12)
	jg	.L388
.L390:
	movq	512(%r12), %rax
	movl	$0, 532(%r12)
	movl	$1, (%rax)
	movl	$1, 528(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	movq	%rdi, %rax
	xorl	%esi, %esi
	xorl	%ebx, %ebx
	jmp	.L397
.L409:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L416:
	movq	%rdi, %rax
	xorl	%esi, %esi
	jmp	.L397
	.cfi_endproc
.LFE5024:
	.size	_ZN2v88internal6Bignum17AssignPowerUInt16Eti, .-_ZN2v88internal6Bignum17AssignPowerUInt16Eti
	.section	.text._ZNK2v88internal6Bignum11ToHexStringEPci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6Bignum11ToHexStringEPci
	.type	_ZNK2v88internal6Bignum11ToHexStringEPci, @function
_ZNK2v88internal6Bignum11ToHexStringEPci:
.LFB5027:
	.cfi_startproc
	endbr64
	movl	528(%rdi), %eax
	testl	%eax, %eax
	jne	.L455
	cmpl	$1, %edx
	jle	.L454
	movl	$48, %edi
	movl	$1, %eax
	movw	%di, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	movl	532(%rdi), %ecx
	movq	512(%rdi), %r9
	addl	%eax, %ecx
	subl	$1, %eax
	subl	$1, %ecx
	cltq
	leal	0(,%rcx,8), %r8d
	movl	(%r9,%rax,4), %eax
	subl	%ecx, %r8d
	movl	%r8d, %ecx
	testl	%eax, %eax
	je	.L457
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L458:
	addl	$1, %ecx
	shrl	$4, %eax
	jne	.L458
	addl	%r8d, %ecx
.L457:
	xorl	%eax, %eax
	cmpl	%ecx, %edx
	jg	.L501
.L454:
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	leal	-1(%rcx), %r8d
	movslq	%ecx, %rcx
	xorl	%edx, %edx
	movb	$0, (%rsi,%rcx)
	movl	532(%rdi), %ecx
	movslq	%r8d, %rax
	addq	%rsi, %rax
	testl	%ecx, %ecx
	jle	.L462
	.p2align 4,,10
	.p2align 3
.L463:
	movb	$48, (%rax)
	subl	$7, %r8d
	addl	$1, %edx
	subq	$7, %rax
	movb	$48, 6(%rax)
	movb	$48, 5(%rax)
	movb	$48, 4(%rax)
	movb	$48, 3(%rax)
	movb	$48, 2(%rax)
	movb	$48, 1(%rax)
	cmpl	%edx, 532(%rdi)
	jg	.L463
.L462:
	movl	528(%rdi), %eax
	movslq	%r8d, %rdx
	xorl	%r9d, %r9d
	addq	%rsi, %rdx
	subl	$1, %eax
	testl	%eax, %eax
	jg	.L478
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L502:
	addl	$55, %eax
.L500:
	movb	%al, -6(%rdx)
	movl	528(%rdi), %eax
	addq	$1, %r9
	subq	$7, %rdx
	subl	$1, %eax
	cmpl	%r9d, %eax
	jle	.L460
.L478:
	movq	512(%rdi), %rax
	movl	(%rax,%r9,4), %eax
	movl	%eax, %r10d
	andl	$15, %r10d
	cmpl	$10, %r10d
	leal	55(%r10), %r11d
	leal	48(%r10), %ecx
	cmovge	%r11d, %ecx
	movb	%cl, (%rdx)
	movl	%eax, %ecx
	shrl	$4, %ecx
	andl	$15, %ecx
	leal	55(%rcx), %r11d
	cmpl	$10, %ecx
	leal	48(%rcx), %r10d
	movl	%r11d, %ecx
	cmovl	%r10d, %ecx
	movb	%cl, -1(%rdx)
	movl	%eax, %ecx
	shrl	$8, %ecx
	andl	$15, %ecx
	leal	55(%rcx), %r11d
	cmpl	$10, %ecx
	leal	48(%rcx), %r10d
	movl	%r11d, %ecx
	cmovl	%r10d, %ecx
	movb	%cl, -2(%rdx)
	movl	%eax, %ecx
	shrl	$12, %ecx
	andl	$15, %ecx
	leal	55(%rcx), %r11d
	cmpl	$10, %ecx
	leal	48(%rcx), %r10d
	movl	%r11d, %ecx
	cmovl	%r10d, %ecx
	movb	%cl, -3(%rdx)
	movl	%eax, %ecx
	shrl	$16, %ecx
	andl	$15, %ecx
	leal	55(%rcx), %r11d
	cmpl	$10, %ecx
	leal	48(%rcx), %r10d
	movl	%r11d, %ecx
	cmovl	%r10d, %ecx
	movb	%cl, -4(%rdx)
	movl	%eax, %ecx
	shrl	$20, %ecx
	andl	$15, %ecx
	leal	55(%rcx), %r11d
	cmpl	$10, %ecx
	leal	48(%rcx), %r10d
	movl	%r11d, %ecx
	cmovl	%r10d, %ecx
	shrl	$24, %eax
	subl	$7, %r8d
	andl	$15, %eax
	movb	%cl, -5(%rdx)
	cmpl	$9, %eax
	jg	.L502
	addl	$48, %eax
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L460:
	movq	512(%rdi), %rdx
	cltq
	movslq	%r8d, %r8
	addq	%rsi, %r8
	movl	(%rdx,%rax,4), %edx
	testl	%edx, %edx
	je	.L481
	.p2align 4,,10
	.p2align 3
.L483:
	movl	%edx, %eax
	andl	$15, %eax
	cmpl	$9, %eax
	jle	.L503
	addl	$55, %eax
	subq	$1, %r8
	movb	%al, 1(%r8)
	shrl	$4, %edx
	jne	.L483
.L481:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	addl	$48, %eax
	subq	$1, %r8
	movb	%al, 1(%r8)
	shrl	$4, %edx
	jne	.L483
	jmp	.L481
	.cfi_endproc
.LFE5027:
	.size	_ZNK2v88internal6Bignum11ToHexStringEPci, .-_ZNK2v88internal6Bignum11ToHexStringEPci
	.section	.text._ZNK2v88internal6Bignum7BigitAtEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6Bignum7BigitAtEi
	.type	_ZNK2v88internal6Bignum7BigitAtEi, @function
_ZNK2v88internal6Bignum7BigitAtEi:
.LFB5028:
	.cfi_startproc
	endbr64
	movl	532(%rdi), %eax
	movl	528(%rdi), %edx
	addl	%eax, %edx
	cmpl	%edx, %esi
	jge	.L506
	cmpl	%eax, %esi
	jl	.L506
	subl	%eax, %esi
	movq	512(%rdi), %rax
	movslq	%esi, %rsi
	movl	(%rax,%rsi,4), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L506:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5028:
	.size	_ZNK2v88internal6Bignum7BigitAtEi, .-_ZNK2v88internal6Bignum7BigitAtEi
	.section	.text._ZN2v88internal6Bignum7CompareERKS1_S3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum7CompareERKS1_S3_
	.type	_ZN2v88internal6Bignum7CompareERKS1_S3_, @function
_ZN2v88internal6Bignum7CompareERKS1_S3_:
.LFB5029:
	.cfi_startproc
	endbr64
	movl	532(%rdi), %r9d
	movl	528(%rdi), %r10d
	movl	$-1, %eax
	movl	532(%rsi), %ecx
	movl	528(%rsi), %r11d
	addl	%r9d, %r10d
	addl	%ecx, %r11d
	cmpl	%r10d, %r11d
	jg	.L522
	movl	$1, %eax
	jl	.L522
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	%ecx, %r9d
	leal	-1(%r10), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	%ecx, %ebx
	cmovle	%r9d, %ebx
	cmpl	%ebx, %eax
	jge	.L511
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L526:
	cmpl	%eax, %r9d
	jg	.L509
	movl	%eax, %r8d
	movq	512(%rdi), %r12
	subl	%r9d, %r8d
	movslq	%r8d, %r8
	movl	(%r12,%r8,4), %r12d
	xorl	%r8d, %r8d
	testb	%dl, %dl
	je	.L525
	cmpl	%r8d, %r12d
	ja	.L518
.L512:
	subl	$1, %eax
	cmpl	%ebx, %eax
	jl	.L515
.L511:
	cmpl	%eax, %r11d
	setle	%dl
	cmpl	%eax, %ecx
	setg	%r8b
	orl	%r8d, %edx
	cmpl	%eax, %r10d
	jg	.L526
.L509:
	testb	%dl, %dl
	jne	.L512
	movl	%eax, %edx
	movq	512(%rsi), %r8
	subl	%ecx, %edx
	movslq	%edx, %rdx
	movl	(%r8,%rdx,4), %edx
	testl	%edx, %edx
	je	.L512
.L519:
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	%eax, %edx
	movq	512(%rsi), %r8
	subl	%ecx, %edx
	movslq	%edx, %rdx
	movl	(%r8,%rdx,4), %r8d
	cmpl	%r12d, %r8d
	ja	.L519
	cmpl	%r8d, %r12d
	jbe	.L512
.L518:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5029:
	.size	_ZN2v88internal6Bignum7CompareERKS1_S3_, .-_ZN2v88internal6Bignum7CompareERKS1_S3_
	.section	.text._ZN2v88internal6Bignum21DivideModuloIntBignumERKS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum21DivideModuloIntBignumERKS1_
	.type	_ZN2v88internal6Bignum21DivideModuloIntBignumERKS1_, @function
_ZN2v88internal6Bignum21DivideModuloIntBignumERKS1_:
.LFB5025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	532(%rdi), %ecx
	movl	528(%rsi), %esi
	movslq	528(%rdi), %rdx
	movl	532(%r12), %edi
	leal	(%rdx,%rcx), %r9d
	leal	(%rsi,%rdi), %eax
	cmpl	%r9d, %eax
	jle	.L562
.L527:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	cmpl	%edi, %ecx
	jle	.L529
	subl	%edi, %ecx
	leal	(%rdx,%rcx), %eax
	cmpl	$128, %eax
	jg	.L563
	movl	%edx, %esi
	subl	$1, %esi
	js	.L533
	movslq	%esi, %rax
	movl	%esi, %esi
	movslq	%ecx, %rdi
	salq	$2, %rax
	subq	%rsi, %rdx
	salq	$2, %rdi
	leaq	-8(,%rdx,4), %r8
	.p2align 4,,10
	.p2align 3
.L534:
	movq	512(%rbx), %rdx
	movl	(%rdx,%rax), %esi
	addq	%rdi, %rdx
	movl	%esi, (%rdx,%rax)
	subq	$4, %rax
	cmpq	%rax, %r8
	jne	.L534
.L533:
	leal	-1(%rcx), %eax
	leaq	4(,%rax,4), %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L532:
	movq	512(%rbx), %rdx
	movl	$0, (%rdx,%rax)
	addq	$4, %rax
	cmpq	%rax, %rsi
	jne	.L532
	movl	532(%rbx), %eax
	movl	528(%rbx), %r9d
	movl	%eax, %edi
	leal	(%rcx,%r9), %edx
	addl	%eax, %r9d
	subl	%ecx, %edi
	movl	%edx, 528(%rbx)
	movl	%edi, 532(%rbx)
	movl	528(%r12), %esi
	movl	%edi, %ecx
	movl	532(%r12), %eax
	addl	%esi, %eax
.L529:
	movq	512(%rbx), %r8
	xorl	%r14d, %r14d
	leal	-1(%rdx), %edi
	cmpl	%eax, %r9d
	jg	.L535
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L537:
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Bignum13SubtractTimesERKS1_i.part.0
.L561:
	movl	528(%rbx), %edx
	movl	528(%r12), %esi
	movl	532(%r12), %eax
	movl	532(%rbx), %ecx
	movq	512(%rbx), %r8
	leal	-1(%rdx), %edi
	addl	%esi, %eax
.L538:
	leal	(%rdx,%rcx), %r9d
	cmpl	%r9d, %eax
	jge	.L536
.L535:
	movslq	%edi, %r9
	movl	(%r8,%r9,4), %r13d
	addl	%r13d, %r14d
	cmpl	$2, %r13d
	jg	.L537
	testl	%r13d, %r13d
	jle	.L538
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Bignum14SubtractBignumERKS1_
	cmpl	$1, %r13d
	je	.L561
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Bignum14SubtractBignumERKS1_
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L536:
	movq	512(%r12), %rdx
	movslq	%edi, %rdi
	leal	-1(%rsi), %eax
	leaq	(%r8,%rdi,4), %rdi
	cltq
	movl	(%rdi), %r15d
	movl	(%rdx,%rax,4), %ecx
	cmpl	$1, %esi
	je	.L564
	leal	1(%rcx), %esi
	movl	%r15d, %eax
	xorl	%edx, %edx
	divl	%esi
	movl	%eax, %r13d
	addl	%eax, %r14d
	cmpl	$2, %eax
	jle	.L565
	movl	%eax, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal6Bignum13SubtractTimesERKS1_i.part.0
	movl	-52(%rbp), %ecx
.L545:
	addl	$1, %r13d
	imull	%ecx, %r13d
	cmpl	%r15d, %r13d
	jbe	.L548
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L566:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	addl	$1, %r14d
	call	_ZN2v88internal6Bignum14SubtractBignumERKS1_
.L548:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Bignum7CompareERKS1_S3_
	testl	%eax, %eax
	jle	.L566
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L564:
	movl	%r15d, %eax
	xorl	%edx, %edx
	divl	%ecx
	movl	%edx, (%rdi)
	movslq	528(%rbx), %rdx
	addl	%eax, %r14d
	testl	%edx, %edx
	jle	.L541
	leal	-1(%rdx), %edi
	leaq	-2(%rdx), %rcx
	movq	512(%rbx), %rsi
	movslq	%edi, %rax
	movl	%edi, %edi
	subq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L542:
	movl	(%rsi,%rax,4), %edi
	movl	%eax, %edx
	testl	%edi, %edi
	jne	.L527
	movl	%eax, 528(%rbx)
	subq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L542
.L541:
	testl	%edx, %edx
	jne	.L527
	movl	$0, 532(%rbx)
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L565:
	testl	%eax, %eax
	jle	.L545
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%ecx, -52(%rbp)
	call	_ZN2v88internal6Bignum14SubtractBignumERKS1_
	cmpl	$1, %r13d
	movl	-52(%rbp), %ecx
	je	.L545
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Bignum14SubtractBignumERKS1_
	movl	-52(%rbp), %ecx
	jmp	.L545
.L563:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5025:
	.size	_ZN2v88internal6Bignum21DivideModuloIntBignumERKS1_, .-_ZN2v88internal6Bignum21DivideModuloIntBignumERKS1_
	.section	.text._ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_
	.type	_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_, @function
_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_:
.LFB5031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	528(%rdi), %r9d
	movl	532(%rdi), %eax
	movl	528(%rsi), %r8d
	movl	532(%rsi), %ecx
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L576:
	movl	%eax, %r9d
	movq	%rdi, %r10
	movl	%ecx, %eax
	movq	%rsi, %rdi
	movl	%r9d, %ecx
	movq	%r10, %rsi
	movl	%ebx, %r9d
.L568:
	leal	(%rcx,%r8), %r11d
	leal	(%r9,%rax), %r10d
	movl	%r8d, %ebx
	movl	%r9d, %r8d
	cmpl	%r11d, %r10d
	jl	.L576
	movl	532(%rdx), %r12d
	leal	1(%r10), %r9d
	movl	528(%rdx), %r14d
	movl	$-1, %r8d
	addl	%r12d, %r14d
	cmpl	%r14d, %r9d
	jl	.L567
	movl	$1, %r8d
	cmpl	%r10d, %r14d
	jl	.L567
	jle	.L584
	movl	$-1, %r8d
	cmpl	%eax, %r11d
	jle	.L567
.L584:
	cmpl	%eax, %ecx
	movl	%eax, %r15d
	leal	-1(%r14), %r8d
	cmovle	%ecx, %r15d
	cmpl	%r12d, %r15d
	cmovg	%r12d, %r15d
	cmpl	%r15d, %r8d
	jl	.L580
	movq	%rdx, -48(%rbp)
	xorl	%r9d, %r9d
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L591:
	cmpl	%r8d, %r10d
	jle	.L581
	movl	%r8d, %ebx
	movq	512(%rdi), %r13
	subl	%eax, %ebx
	movslq	%ebx, %rbx
	movl	0(%r13,%rbx,4), %ebx
.L572:
	cmpl	%r8d, %ecx
	jg	.L573
	cmpl	%r8d, %r11d
	jle	.L573
	movl	%r8d, %r13d
	movq	512(%rsi), %rdx
	subl	%ecx, %r13d
	movslq	%r13d, %r13
	addl	(%rdx,%r13,4), %ebx
.L573:
	cmpl	%r8d, %r12d
	jg	.L574
	cmpl	%r8d, %r14d
	jle	.L574
	movq	-48(%rbp), %rdx
	movl	%r8d, %r13d
	subl	%r12d, %r13d
	movq	512(%rdx), %rdx
	movslq	%r13d, %r13
	addl	(%rdx,%r13,4), %r9d
.L574:
	cmpl	%r9d, %ebx
	ja	.L582
	subl	%ebx, %r9d
	cmpl	$1, %r9d
	ja	.L583
	subl	$1, %r8d
	sall	$28, %r9d
	cmpl	%r15d, %r8d
	jl	.L590
.L575:
	cmpl	%r8d, %eax
	jle	.L591
.L581:
	xorl	%ebx, %ebx
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L583:
	movl	$-1, %r8d
.L567:
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	.cfi_restore_state
	movl	$1, %r8d
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L590:
	.cfi_restore_state
	xorl	%r8d, %r8d
	testl	%r9d, %r9d
	setne	%r8b
	negl	%r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L580:
	.cfi_restore_state
	xorl	%r8d, %r8d
	jmp	.L567
	.cfi_endproc
.LFE5031:
	.size	_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_, .-_ZN2v88internal6Bignum11PlusCompareERKS1_S3_S3_
	.section	.text._ZN2v88internal6Bignum5ClampEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum5ClampEv
	.type	_ZN2v88internal6Bignum5ClampEv, @function
_ZN2v88internal6Bignum5ClampEv:
.LFB5032:
	.cfi_startproc
	endbr64
	movslq	528(%rdi), %rdx
	testl	%edx, %edx
	jle	.L593
	leal	-1(%rdx), %r8d
	leaq	-2(%rdx), %rcx
	movq	512(%rdi), %rsi
	movslq	%r8d, %rax
	movl	%r8d, %r8d
	subq	%r8, %rcx
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L599:
	movl	%eax, 528(%rdi)
	subq	$1, %rax
	cmpq	%rcx, %rax
	je	.L593
.L596:
	movl	(%rsi,%rax,4), %r8d
	movl	%eax, %edx
	testl	%r8d, %r8d
	je	.L599
.L592:
	ret
	.p2align 4,,10
	.p2align 3
.L593:
	testl	%edx, %edx
	jne	.L592
	movl	$0, 532(%rdi)
	ret
	.cfi_endproc
.LFE5032:
	.size	_ZN2v88internal6Bignum5ClampEv, .-_ZN2v88internal6Bignum5ClampEv
	.section	.text._ZNK2v88internal6Bignum9IsClampedEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal6Bignum9IsClampedEv
	.type	_ZNK2v88internal6Bignum9IsClampedEv, @function
_ZNK2v88internal6Bignum9IsClampedEv:
.LFB5033:
	.cfi_startproc
	endbr64
	movl	528(%rdi), %eax
	movl	$1, %r8d
	testl	%eax, %eax
	je	.L600
	subl	$1, %eax
	movq	512(%rdi), %rdx
	cltq
	movl	(%rdx,%rax,4), %eax
	testl	%eax, %eax
	setne	%r8b
.L600:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE5033:
	.size	_ZNK2v88internal6Bignum9IsClampedEv, .-_ZNK2v88internal6Bignum9IsClampedEv
	.section	.text._ZN2v88internal6Bignum4ZeroEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum4ZeroEv
	.type	_ZN2v88internal6Bignum4ZeroEv, @function
_ZN2v88internal6Bignum4ZeroEv:
.LFB5034:
	.cfi_startproc
	endbr64
	movl	528(%rdi), %eax
	testl	%eax, %eax
	jle	.L605
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L606:
	movq	512(%rdi), %rdx
	movl	$0, (%rdx,%rax,4)
	addq	$1, %rax
	cmpl	%eax, 528(%rdi)
	jg	.L606
.L605:
	movq	$0, 528(%rdi)
	ret
	.cfi_endproc
.LFE5034:
	.size	_ZN2v88internal6Bignum4ZeroEv, .-_ZN2v88internal6Bignum4ZeroEv
	.section	.text._ZN2v88internal6Bignum5AlignERKS1_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum5AlignERKS1_
	.type	_ZN2v88internal6Bignum5AlignERKS1_, @function
_ZN2v88internal6Bignum5AlignERKS1_:
.LFB5035:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movl	532(%rdi), %esi
	movl	532(%r8), %eax
	cmpl	%eax, %esi
	jle	.L608
	movslq	528(%rdi), %rdx
	subl	%eax, %esi
	leal	(%rdx,%rsi), %eax
	cmpl	$128, %eax
	jg	.L619
	movl	%edx, %ecx
	subl	$1, %ecx
	js	.L611
	movslq	%ecx, %rax
	movl	%ecx, %ecx
	movslq	%esi, %r8
	salq	$2, %rax
	subq	%rcx, %rdx
	salq	$2, %r8
	leaq	-8(,%rdx,4), %r9
	.p2align 4,,10
	.p2align 3
.L612:
	movq	512(%rdi), %rdx
	movl	(%rdx,%rax), %ecx
	addq	%r8, %rdx
	movl	%ecx, (%rdx,%rax)
	subq	$4, %rax
	cmpq	%rax, %r9
	jne	.L612
.L611:
	leal	-1(%rsi), %eax
	leaq	4(,%rax,4), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L613:
	movq	512(%rdi), %rdx
	movl	$0, (%rdx,%rax)
	addq	$4, %rax
	cmpq	%rax, %rcx
	jne	.L613
	addl	%esi, 528(%rdi)
	subl	%esi, 532(%rdi)
.L608:
	ret
.L619:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5035:
	.size	_ZN2v88internal6Bignum5AlignERKS1_, .-_ZN2v88internal6Bignum5AlignERKS1_
	.section	.text._ZN2v88internal6Bignum15BigitsShiftLeftEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum15BigitsShiftLeftEi
	.type	_ZN2v88internal6Bignum15BigitsShiftLeftEi, @function
_ZN2v88internal6Bignum15BigitsShiftLeftEi:
.LFB5036:
	.cfi_startproc
	endbr64
	movl	528(%rdi), %eax
	testl	%eax, %eax
	jle	.L620
	movl	$28, %r11d
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	subl	%esi, %r11d
	.p2align 4,,10
	.p2align 3
.L623:
	movq	512(%rdi), %rax
	movl	%r9d, %r10d
	movl	%r11d, %ecx
	leaq	(%rax,%rdx,4), %r8
	addq	$1, %rdx
	movl	(%r8), %eax
	movl	%eax, %r9d
	shrl	%cl, %r9d
	movl	%esi, %ecx
	sall	%cl, %eax
	addl	%r10d, %eax
	andl	$268435455, %eax
	movl	%eax, (%r8)
	movslq	528(%rdi), %rax
	cmpl	%edx, %eax
	jg	.L623
	testl	%r9d, %r9d
	je	.L620
	movq	512(%rdi), %rdx
	movl	%r9d, (%rdx,%rax,4)
	addl	$1, 528(%rdi)
.L620:
	ret
	.cfi_endproc
.LFE5036:
	.size	_ZN2v88internal6Bignum15BigitsShiftLeftEi, .-_ZN2v88internal6Bignum15BigitsShiftLeftEi
	.section	.text._ZN2v88internal6Bignum13SubtractTimesERKS1_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6Bignum13SubtractTimesERKS1_i
	.type	_ZN2v88internal6Bignum13SubtractTimesERKS1_i, @function
_ZN2v88internal6Bignum13SubtractTimesERKS1_i:
.LFB5037:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	jle	.L641
	jmp	_ZN2v88internal6Bignum13SubtractTimesERKS1_i.part.0
	.p2align 4,,10
	.p2align 3
.L641:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	testl	%edx, %edx
	movl	%edx, -4(%rbp)
	jle	.L629
	movq	%rsi, -24(%rbp)
	movq	%rdi, -16(%rbp)
	call	_ZN2v88internal6Bignum14SubtractBignumERKS1_
	movl	-4(%rbp), %edx
	cmpl	$1, %edx
	je	.L629
	movq	-24(%rbp), %rsi
	movq	-16(%rbp), %rdi
	leave
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal6Bignum14SubtractBignumERKS1_
	.p2align 4,,10
	.p2align 3
.L629:
	.cfi_restore_state
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5037:
	.size	_ZN2v88internal6Bignum13SubtractTimesERKS1_i, .-_ZN2v88internal6Bignum13SubtractTimesERKS1_i
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal6BignumC2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal6BignumC2Ev, @function
_GLOBAL__sub_I__ZN2v88internal6BignumC2Ev:
.LFB5811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5811:
	.size	_GLOBAL__sub_I__ZN2v88internal6BignumC2Ev, .-_GLOBAL__sub_I__ZN2v88internal6BignumC2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal6BignumC2Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	5
	.long	25
	.long	125
	.long	625
	.align 16
.LC2:
	.long	3125
	.long	15625
	.long	78125
	.long	390625
	.align 16
.LC3:
	.long	1953125
	.long	9765625
	.long	48828125
	.long	244140625
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
