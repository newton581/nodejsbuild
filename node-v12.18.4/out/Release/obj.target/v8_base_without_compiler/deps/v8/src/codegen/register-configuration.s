	.file	"register-configuration.cc"
	.text
	.section	.text._ZN2v88internal21RegisterConfigurationD2Ev,"axG",@progbits,_ZN2v88internal21RegisterConfigurationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21RegisterConfigurationD2Ev
	.type	_ZN2v88internal21RegisterConfigurationD2Ev, @function
_ZN2v88internal21RegisterConfigurationD2Ev:
.LFB5213:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5213:
	.size	_ZN2v88internal21RegisterConfigurationD2Ev, .-_ZN2v88internal21RegisterConfigurationD2Ev
	.weak	_ZN2v88internal21RegisterConfigurationD1Ev
	.set	_ZN2v88internal21RegisterConfigurationD1Ev,_ZN2v88internal21RegisterConfigurationD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD2Ev, @function
_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD2Ev:
.LFB6161:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6161:
	.size	_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD2Ev, .-_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD1Ev,_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD2Ev, @function
_ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD2Ev:
.LFB6165:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6165:
	.size	_ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD2Ev, .-_ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD1Ev,_ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD0Ev, @function
_ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD0Ev:
.LFB6167:
	.cfi_startproc
	endbr64
	movl	$336, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6167:
	.size	_ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD0Ev, .-_ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD0Ev
	.section	.text._ZN2v88internal21RegisterConfigurationD0Ev,"axG",@progbits,_ZN2v88internal21RegisterConfigurationD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21RegisterConfigurationD0Ev
	.type	_ZN2v88internal21RegisterConfigurationD0Ev, @function
_ZN2v88internal21RegisterConfigurationD0Ev:
.LFB5215:
	.cfi_startproc
	endbr64
	movl	$336, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5215:
	.size	_ZN2v88internal21RegisterConfigurationD0Ev, .-_ZN2v88internal21RegisterConfigurationD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD0Ev, @function
_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD0Ev:
.LFB6163:
	.cfi_startproc
	endbr64
	movl	$336, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6163:
	.size	_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD0Ev, .-_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD0Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD2Ev, @function
_ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD2Ev:
.LFB6157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	344(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L9
	call	_ZdaPv@PLT
.L9:
	movq	336(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L8
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6157:
	.size	_ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD2Ev, .-_ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD2Ev
	.set	_ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD1Ev,_ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD2Ev
	.section	.text._ZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEv,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEv, @function
_ZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEv:
.LFB5218:
	.cfi_startproc
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip), %eax
	testb	%al, %al
	je	.L26
	leaq	_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZGVZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	jne	.L27
	leaq	_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	leaq	_ZN2v88internal12_GLOBAL__N_1L24kAllocatableGeneralCodesE(%rip), %rax
	movdqa	_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %xmm2
	movdqa	16+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %xmm1
	leaq	_ZGVZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip), %rdi
	movq	%rax, 56+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	leaq	_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %rax
	movdqa	32+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %xmm0
	movq	%rax, 192+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movl	56+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %eax
	movq	48+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %rdx
	movups	%xmm2, 200+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movl	%eax, 256+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movl	%eax, 120+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movabsq	$68719476752, %rax
	movq	%rax, 8+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movq	%rax, 16+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movabsq	$64424509452, %rax
	movq	%rax, 24+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	addq	$3, %rax
	movq	%rax, 32+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movabsq	$140733193444303, %rax
	movq	%rax, 40+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	subq	$23504, %rax
	movq	%rax, 48+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationE(%rip), %rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movl	$0, 328+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movq	%rdx, 248+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movq	%rdx, 112+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movups	%xmm1, 216+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movups	%xmm0, 232+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movaps	%xmm2, 64+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movaps	%xmm1, 80+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movaps	%xmm0, 96+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	call	__cxa_guard_release@PLT
	leaq	_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5218:
	.size	_ZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEv, .-_ZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEv
	.section	.text._ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD0Ev, @function
_ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD0Ev:
.LFB6159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	344(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L29
	call	_ZdaPv@PLT
.L29:
	movq	336(%r12), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	_ZdaPv@PLT
.L30:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$352, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6159:
	.size	_ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD0Ev, .-_ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD0Ev
	.section	.text._ZN2v88internal21RegisterConfiguration7DefaultEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21RegisterConfiguration7DefaultEv
	.type	_ZN2v88internal21RegisterConfiguration7DefaultEv, @function
_ZN2v88internal21RegisterConfiguration7DefaultEv:
.LFB5234:
	.cfi_startproc
	endbr64
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip), %eax
	testb	%al, %al
	je	.L49
	leaq	_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZGVZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	jne	.L50
	leaq	_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore_state
	leaq	_ZN2v88internal12_GLOBAL__N_1L24kAllocatableGeneralCodesE(%rip), %rax
	movdqa	_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %xmm2
	movdqa	16+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %xmm1
	leaq	_ZGVZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip), %rdi
	movq	%rax, 56+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	leaq	_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %rax
	movdqa	32+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %xmm0
	movq	%rax, 192+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movl	56+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %eax
	movq	48+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %rdx
	movups	%xmm2, 200+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movl	%eax, 256+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movl	%eax, 120+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movabsq	$68719476752, %rax
	movq	%rax, 8+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movq	%rax, 16+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movabsq	$64424509452, %rax
	movq	%rax, 24+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	addq	$3, %rax
	movq	%rax, 32+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movabsq	$140733193444303, %rax
	movq	%rax, 40+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	subq	$23504, %rax
	movq	%rax, 48+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationE(%rip), %rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movl	$0, 328+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movq	%rdx, 248+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movq	%rdx, 112+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movups	%xmm1, 216+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movups	%xmm0, 232+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movaps	%xmm2, 64+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movaps	%xmm1, 80+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	movaps	%xmm0, 96+_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip)
	call	__cxa_guard_release@PLT
	leaq	_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5234:
	.size	_ZN2v88internal21RegisterConfiguration7DefaultEv, .-_ZN2v88internal21RegisterConfiguration7DefaultEv
	.section	.text._ZN2v88internal21RegisterConfiguration9PoisoningEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21RegisterConfiguration9PoisoningEv
	.type	_ZN2v88internal21RegisterConfiguration9PoisoningEv, @function
_ZN2v88internal21RegisterConfiguration9PoisoningEv:
.LFB5235:
	.cfi_startproc
	endbr64
	movzbl	_ZGVZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip), %eax
	testb	%al, %al
	je	.L62
	leaq	_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZGVZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	jne	.L63
	leaq	_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movabsq	$12884901888, %rax
	movl	$1, %edx
	movabsq	$4294967298, %rcx
	movabsq	$30064771078, %r8
	movq	%rax, _ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfiguration26allocatable_general_codes_E(%rip)
	movl	%edx, %r9d
	movl	%edx, %r10d
	leaq	_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfiguration26allocatable_general_codes_E(%rip), %rax
	movq	%rax, 56+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	leaq	_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %rax
	movabsq	$38654705672, %rdi
	movabsq	$60129542155, %rsi
	movq	%rax, 192+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	movl	%edx, %eax
	movdqa	_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %xmm2
	sall	%cl, %eax
	movq	%rcx, 8+_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfiguration26allocatable_general_codes_E(%rip)
	leal	(%rdx,%rdx), %ecx
	movdqa	16+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %xmm1
	orl	$9, %eax
	movq	%rdi, 24+_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfiguration26allocatable_general_codes_E(%rip)
	movdqa	32+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %xmm0
	orl	%ecx, %eax
	movl	%r8d, %ecx
	movq	%r8, 16+_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfiguration26allocatable_general_codes_E(%rip)
	sall	%cl, %r9d
	movl	%edx, %ecx
	movq	%rsi, 32+_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfiguration26allocatable_general_codes_E(%rip)
	orl	%r9d, %eax
	sall	$7, %ecx
	movl	$15, 40+_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfiguration26allocatable_general_codes_E(%rip)
	orl	%ecx, %eax
	movl	%edi, %ecx
	movl	%edx, %edi
	movl	$0, 328+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	sall	%cl, %r10d
	movl	%edx, %ecx
	sall	$14, %edx
	movl	$32767, 44+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	orl	%r10d, %eax
	sall	$9, %ecx
	movups	%xmm2, 200+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	orl	%ecx, %eax
	movl	%esi, %ecx
	movups	%xmm1, 216+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	sall	%cl, %edi
	movups	%xmm0, 232+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	orl	%edi, %eax
	leaq	_ZGVZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip), %rdi
	movaps	%xmm2, 64+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	orl	%edx, %eax
	movq	48+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %rdx
	movaps	%xmm1, 80+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	orb	$-128, %ah
	movaps	%xmm0, 96+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	movl	%eax, 40+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	movabsq	$68719476752, %rax
	movq	%rax, 8+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	movq	%rax, 16+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	movabsq	$64424509451, %rax
	movq	%rax, 24+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	addq	$4, %rax
	movq	%rax, 32+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	movl	56+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %eax
	movq	%rdx, 248+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	movl	%eax, 256+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	movl	%eax, 120+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	movabsq	$140733193420799, %rax
	movq	%rax, 48+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationE(%rip), %rax
	movq	%rax, _ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	movq	%rdx, 112+_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip)
	call	__cxa_guard_release@PLT
	leaq	_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5235:
	.size	_ZN2v88internal21RegisterConfiguration9PoisoningEv, .-_ZN2v88internal21RegisterConfiguration9PoisoningEv
	.globl	__popcountdi2
	.section	.text._ZN2v88internal21RegisterConfiguration24RestrictGeneralRegistersEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21RegisterConfiguration24RestrictGeneralRegistersEj
	.type	_ZN2v88internal21RegisterConfiguration24RestrictGeneralRegistersEj, @function
_ZN2v88internal21RegisterConfiguration24RestrictGeneralRegistersEj:
.LFB5236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edi, -52(%rbp)
	call	__popcountdi2@PLT
	movslq	%eax, %r12
	leaq	0(,%r12,4), %rdi
	movq	%r12, %r13
	call	_Znam@PLT
	leaq	0(,%r12,8), %rdi
	xorl	%r12d, %r12d
	movq	%rax, %rbx
	call	_Znam@PLT
	movq	%rax, -64(%rbp)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L81:
	call	_ZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEv
	movq	56(%rax), %rax
	movl	(%rax,%r14,4), %eax
	cmpl	$-1, %eax
	je	.L66
	movl	-52(%rbp), %esi
	btl	%eax, %esi
	jnc	.L66
	movslq	%r12d, %rcx
	movq	-64(%rbp), %rdi
	addl	$1, %r12d
	movl	%eax, (%rbx,%rcx,4)
	movq	(%r15,%r14,8), %rax
	movq	%rax, (%rdi,%rcx,8)
.L66:
	addq	$1, %r14
.L67:
	call	_ZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEv
	cmpl	%r14d, 24(%rax)
	jg	.L81
	movl	$352, %edi
	call	_Znwm@PLT
	movl	-52(%rbp), %edx
	leaq	_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %rsi
	movl	$16, 8(%rax)
	movl	$16, 16(%rax)
	movl	%r13d, 24(%rax)
	movl	$15, 32(%rax)
	movl	$0, 40(%rax)
	movq	%rbx, 56(%rax)
	movq	%rsi, 192(%rax)
	movl	$0, 328(%rax)
	testl	%edx, %edx
	je	.L68
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L69:
	movl	(%rbx,%rdx,4), %ecx
	movl	%edi, %r8d
	addq	$1, %rdx
	sall	%cl, %r8d
	orl	%r8d, %esi
	cmpl	%edx, %r13d
	jg	.L69
	movl	%esi, 40(%rax)
.L68:
	movdqa	32+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %xmm0
	movq	48+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %rsi
	movl	$16, 20(%rax)
	movdqa	_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %xmm2
	movdqa	16+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %xmm1
	movl	$16, 12(%rax)
	movq	%rsi, 248(%rax)
	movl	56+_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE(%rip), %ecx
	movq	%rsi, 112(%rax)
	movabsq	$140733193420799, %rsi
	movups	%xmm0, 232(%rax)
	movups	%xmm0, 96(%rax)
	movq	%rbx, %xmm0
	movq	%rsi, 48(%rax)
	movhps	-64(%rbp), %xmm0
	leaq	16+_ZTVN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationE(%rip), %rsi
	movl	$15, 36(%rax)
	movl	$15, 28(%rax)
	movl	%ecx, 256(%rax)
	movl	%ecx, 120(%rax)
	movl	$32767, 44(%rax)
	movq	%rsi, (%rax)
	movups	%xmm2, 200(%rax)
	movups	%xmm1, 216(%rax)
	movups	%xmm2, 64(%rax)
	movups	%xmm1, 80(%rax)
	movups	%xmm0, 336(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5236:
	.size	_ZN2v88internal21RegisterConfiguration24RestrictGeneralRegistersEj, .-_ZN2v88internal21RegisterConfiguration24RestrictGeneralRegistersEj
	.section	.text._ZN2v88internal21RegisterConfigurationC2EiiiiPKiS3_NS1_12AliasingKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21RegisterConfigurationC2EiiiiPKiS3_NS1_12AliasingKindE
	.type	_ZN2v88internal21RegisterConfigurationC2EiiiiPKiS3_NS1_12AliasingKindE, @function
_ZN2v88internal21RegisterConfigurationC2EiiiiPKiS3_NS1_12AliasingKindE:
.LFB5238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal21RegisterConfigurationE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rbp), %r13
	movl	24(%rbp), %edi
	movq	%rax, (%rbx)
	movl	%esi, 8(%rbx)
	movl	%edx, 16(%rbx)
	movl	$0, 20(%rbx)
	movl	%ecx, 24(%rbx)
	movl	$0, 28(%rbx)
	movl	%r8d, 32(%rbx)
	movl	$0, 36(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	movq	%r9, 56(%rbx)
	movq	%r13, 192(%rbx)
	movl	%edi, 328(%rbx)
	testl	%ecx, %ecx
	jle	.L87
	leal	-1(%rcx), %eax
	movl	$1, %esi
	leaq	4(%r9,%rax,4), %r10
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L86:
	movl	(%r9), %ecx
	movl	%esi, %r14d
	addq	$4, %r9
	sall	%cl, %r14d
	orl	%r14d, %eax
	cmpq	%r10, %r9
	jne	.L86
	movl	%eax, 40(%rbx)
.L87:
	testl	%r8d, %r8d
	jle	.L108
	leal	-1(%r8), %r14d
	leaq	4(%r13), %rsi
	movq	%r13, %r9
	movq	%r13, %rax
	leaq	(%rsi,%r14,4), %r10
	xorl	%r12d, %r12d
	movl	$1, %r11d
	.p2align 4,,10
	.p2align 3
.L89:
	movl	(%rax), %ecx
	movl	%r11d, %r15d
	addq	$4, %rax
	sall	%cl, %r15d
	orl	%r15d, %r12d
	cmpq	%rax, %r10
	jne	.L89
	movl	%r12d, 48(%rbx)
	cmpl	$1, %edi
	je	.L109
	movl	%edx, 20(%rbx)
	leaq	4(,%r14,4), %r14
	leaq	200(%rbx), %rdi
	movq	%r13, %rsi
	movl	%edx, 12(%rbx)
	movq	%r14, %rdx
	movl	%r8d, 36(%rbx)
	movl	%r8d, 28(%rbx)
	call	memcpy@PLT
	leaq	64(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
.L99:
	movl	%r12d, 52(%rbx)
	movl	%r12d, 44(%rbx)
.L82:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	leal	(%rdx,%rdx), %eax
	movl	$32, %ecx
	movl	$3, %r11d
	cmpl	$32, %eax
	cmovg	%ecx, %eax
	movl	%eax, 12(%rbx)
	.p2align 4,,10
	.p2align 3
.L92:
	movl	(%r9), %eax
	leal	(%rax,%rax), %ecx
	cmpl	$31, %ecx
	jg	.L91
	movslq	28(%rbx), %rdi
	movq	%rdi, %rax
	leaq	(%rbx,%rdi,4), %rdi
	addl	$2, %eax
	movl	%ecx, 64(%rdi)
	movl	%eax, 28(%rbx)
	leal	1(%rcx), %eax
	movl	%eax, 68(%rdi)
	movl	%r11d, %eax
	sall	%cl, %eax
	orl	%eax, 44(%rbx)
.L91:
	addq	$4, %r9
	cmpq	%r9, %r10
	jne	.L92
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%eax, %edx
	movl	0(%r13), %eax
	sarl	%edx
	movl	%eax, %ecx
	movl	%edx, 20(%rbx)
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	cmpl	$1, %r8d
	je	.L82
	leal	-2(%r8), %eax
	movl	$1, %r8d
	leaq	8(%r13,%rax,4), %rdi
	.p2align 4,,10
	.p2align 3
.L96:
	movl	(%rsi), %eax
	movl	%ecx, %edx
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	cmpl	%edx, %ecx
	jne	.L95
	movslq	36(%rbx), %rax
	leal	1(%rax), %edx
	movl	%edx, 36(%rbx)
	movl	%ecx, 200(%rbx,%rax,4)
	movl	%r8d, %eax
	sall	%cl, %eax
	orl	%eax, 52(%rbx)
.L95:
	addq	$4, %rsi
	cmpq	%rdi, %rsi
	jne	.L96
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	cmpl	$1, %edi
	je	.L110
	movl	%edx, 20(%rbx)
	xorl	%r12d, %r12d
	movl	%edx, 12(%rbx)
	movl	%r8d, 36(%rbx)
	movl	%r8d, 28(%rbx)
	jmp	.L99
.L110:
	leal	(%rdx,%rdx), %eax
	movl	$32, %ecx
	cmpl	$32, %eax
	cmovg	%ecx, %eax
	movl	%eax, 12(%rbx)
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%eax, %edx
	sarl	%edx
	movl	%edx, 20(%rbx)
	jmp	.L82
	.cfi_endproc
.LFE5238:
	.size	_ZN2v88internal21RegisterConfigurationC2EiiiiPKiS3_NS1_12AliasingKindE, .-_ZN2v88internal21RegisterConfigurationC2EiiiiPKiS3_NS1_12AliasingKindE
	.globl	_ZN2v88internal21RegisterConfigurationC1EiiiiPKiS3_NS1_12AliasingKindE
	.set	_ZN2v88internal21RegisterConfigurationC1EiiiiPKiS3_NS1_12AliasingKindE,_ZN2v88internal21RegisterConfigurationC2EiiiiPKiS3_NS1_12AliasingKindE
	.section	.text._ZNK2v88internal21RegisterConfiguration10GetAliasesENS0_21MachineRepresentationEiS2_Pi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21RegisterConfiguration10GetAliasesENS0_21MachineRepresentationEiS2_Pi
	.type	_ZNK2v88internal21RegisterConfiguration10GetAliasesENS0_21MachineRepresentationEiS2_Pi, @function
_ZNK2v88internal21RegisterConfiguration10GetAliasesENS0_21MachineRepresentationEiS2_Pi:
.LFB5240:
	.cfi_startproc
	endbr64
	cmpb	%cl, %sil
	je	.L116
	movzbl	%sil, %esi
	movzbl	%cl, %ecx
	cmpl	%ecx, %esi
	jle	.L114
	subl	%ecx, %esi
	xorl	%eax, %eax
	movl	%esi, %ecx
	sall	%cl, %edx
	cmpl	$31, %edx
	jle	.L117
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	subl	%esi, %ecx
	sarl	%cl, %edx
.L116:
	movl	%edx, (%r8)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$1, %eax
	movl	%edx, (%r8)
	sall	%cl, %eax
	ret
	.cfi_endproc
.LFE5240:
	.size	_ZNK2v88internal21RegisterConfiguration10GetAliasesENS0_21MachineRepresentationEiS2_Pi, .-_ZNK2v88internal21RegisterConfiguration10GetAliasesENS0_21MachineRepresentationEiS2_Pi
	.section	.text._ZNK2v88internal21RegisterConfiguration10AreAliasesENS0_21MachineRepresentationEiS2_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21RegisterConfiguration10AreAliasesENS0_21MachineRepresentationEiS2_i
	.type	_ZNK2v88internal21RegisterConfiguration10AreAliasesENS0_21MachineRepresentationEiS2_i, @function
_ZNK2v88internal21RegisterConfiguration10AreAliasesENS0_21MachineRepresentationEiS2_i:
.LFB5241:
	.cfi_startproc
	endbr64
	cmpb	%cl, %sil
	je	.L122
	movzbl	%sil, %esi
	movzbl	%cl, %ecx
	cmpl	%ecx, %esi
	jg	.L123
	subl	%esi, %ecx
	sarl	%cl, %edx
.L122:
	cmpl	%r8d, %edx
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	subl	%ecx, %esi
	movl	%esi, %ecx
	sarl	%cl, %r8d
	cmpl	%edx, %r8d
	sete	%al
	ret
	.cfi_endproc
.LFE5241:
	.size	_ZNK2v88internal21RegisterConfiguration10AreAliasesENS0_21MachineRepresentationEiS2_i, .-_ZNK2v88internal21RegisterConfiguration10AreAliasesENS0_21MachineRepresentationEiS2_i
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal21RegisterConfiguration7DefaultEv,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal21RegisterConfiguration7DefaultEv, @function
_GLOBAL__sub_I__ZN2v88internal21RegisterConfiguration7DefaultEv:
.LFB6177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE6177:
	.size	_GLOBAL__sub_I__ZN2v88internal21RegisterConfiguration7DefaultEv, .-_GLOBAL__sub_I__ZN2v88internal21RegisterConfiguration7DefaultEv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal21RegisterConfiguration7DefaultEv
	.weak	_ZTVN2v88internal21RegisterConfigurationE
	.section	.data.rel.ro.local._ZTVN2v88internal21RegisterConfigurationE,"awG",@progbits,_ZTVN2v88internal21RegisterConfigurationE,comdat
	.align 8
	.type	_ZTVN2v88internal21RegisterConfigurationE, @object
	.size	_ZTVN2v88internal21RegisterConfigurationE, 32
_ZTVN2v88internal21RegisterConfigurationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal21RegisterConfigurationD1Ev
	.quad	_ZN2v88internal21RegisterConfigurationD0Ev
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationE, 32
_ZTVN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_132ArchDefaultRegisterConfigurationD0Ev
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationE, 32
_ZTVN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfigurationD0Ev
	.section	.data.rel.ro.local._ZTVN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationE,"aw"
	.align 8
	.type	_ZTVN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationE, @object
	.size	_ZTVN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationE, 32
_ZTVN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD1Ev
	.quad	_ZN2v88internal12_GLOBAL__N_131RestrictedRegisterConfigurationD0Ev
	.section	.bss._ZGVZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object, @object
	.size	_ZGVZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object, 8
_ZGVZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object,"aw",@nobits
	.align 32
	.type	_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object, @object
	.size	_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object, 336
_ZZN2v88internal12_GLOBAL__N_140GetDefaultPoisoningRegisterConfigurationEvE6object:
	.zero	336
	.section	.bss._ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfiguration26allocatable_general_codes_E,"aw",@nobits
	.align 32
	.type	_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfiguration26allocatable_general_codes_E, @object
	.size	_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfiguration26allocatable_general_codes_E, 44
_ZN2v88internal12_GLOBAL__N_141ArchDefaultPoisoningRegisterConfiguration26allocatable_general_codes_E:
	.zero	44
	.section	.bss._ZGVZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object,"aw",@nobits
	.align 8
	.type	_ZGVZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object, @object
	.size	_ZGVZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object, 8
_ZGVZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object:
	.zero	8
	.section	.bss._ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object,"aw",@nobits
	.align 32
	.type	_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object, @object
	.size	_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object, 336
_ZZN2v88internal12_GLOBAL__N_131GetDefaultRegisterConfigurationEvE6object:
	.zero	336
	.section	.rodata._ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE,"a"
	.align 32
	.type	_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE, @object
	.size	_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE, 60
_ZN2v88internal12_GLOBAL__N_1L23kAllocatableDoubleCodesE:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	14
	.section	.rodata._ZN2v88internal12_GLOBAL__N_1L24kAllocatableGeneralCodesE,"a"
	.align 32
	.type	_ZN2v88internal12_GLOBAL__N_1L24kAllocatableGeneralCodesE, @object
	.size	_ZN2v88internal12_GLOBAL__N_1L24kAllocatableGeneralCodesE, 48
_ZN2v88internal12_GLOBAL__N_1L24kAllocatableGeneralCodesE:
	.long	0
	.long	3
	.long	2
	.long	1
	.long	6
	.long	7
	.long	8
	.long	9
	.long	11
	.long	12
	.long	14
	.long	15
	.weak	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rax"
.LC1:
	.string	"rcx"
.LC2:
	.string	"rdx"
.LC3:
	.string	"rbx"
.LC4:
	.string	"rsp"
.LC5:
	.string	"rbp"
.LC6:
	.string	"rsi"
.LC7:
	.string	"rdi"
.LC8:
	.string	"r8"
.LC9:
	.string	"r9"
.LC10:
	.string	"r10"
.LC11:
	.string	"r11"
.LC12:
	.string	"r12"
.LC13:
	.string	"r13"
.LC14:
	.string	"r14"
.LC15:
	.string	"r15"
	.section	.data.rel.ro.local._ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names,"awG",@progbits,_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names,comdat
	.align 32
	.type	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names, @gnu_unique_object
	.size	_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names, 128
_ZZN2v88internal12RegisterNameENS0_8RegisterEE5Names:
	.quad	.LC0
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
