	.file	"zone.cc"
	.text
	.section	.text._ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE,"axG",@progbits,_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE
	.type	_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE, @function
_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE:
.LFB3740:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3740:
	.size	_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE, .-_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE
	.section	.text._ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE,"axG",@progbits,_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE
	.type	_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE, @function
_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE:
.LFB3741:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE3741:
	.size	_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE, .-_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE
	.section	.text._ZN2v88internal4ZoneC2EPNS0_19AccountingAllocatorEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4ZoneC2EPNS0_19AccountingAllocatorEPKc
	.type	_ZN2v88internal4ZoneC2EPNS0_19AccountingAllocatorEPKc, @function
_ZN2v88internal4ZoneC2EPNS0_19AccountingAllocatorEPKc:
.LFB5111:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rax
	movq	%rdi, %r8
	pxor	%xmm0, %xmm0
	movq	%rsi, %rdi
	movq	%rdx, 48(%r8)
	leaq	_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE(%rip), %rdx
	movq	%rsi, 32(%r8)
	movq	$0, 40(%r8)
	movb	$0, 56(%r8)
	movups	%xmm0, (%r8)
	movups	%xmm0, 16(%r8)
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L6
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%r8, %rsi
	jmp	*%rax
	.cfi_endproc
.LFE5111:
	.size	_ZN2v88internal4ZoneC2EPNS0_19AccountingAllocatorEPKc, .-_ZN2v88internal4ZoneC2EPNS0_19AccountingAllocatorEPKc
	.globl	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc
	.set	_ZN2v88internal4ZoneC1EPNS0_19AccountingAllocatorEPKc,_ZN2v88internal4ZoneC2EPNS0_19AccountingAllocatorEPKc
	.section	.text._ZN2v88internal4ZoneD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4ZoneD2Ev
	.type	_ZN2v88internal4ZoneD2Ev, @function
_ZN2v88internal4ZoneD2Ev:
.LFB5114:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L16
.L8:
	movq	40(%r12), %rbx
	testq	%rbx, %rbx
	je	.L7
	.p2align 4,,10
	.p2align 3
.L10:
	movq	32(%r12), %rdi
	movq	%rbx, %rsi
	movq	8(%rbx), %rbx
	movq	16(%rsi), %rax
	subq	%rax, 8(%r12)
	movq	(%rdi), %rax
	call	*24(%rax)
	testq	%rbx, %rbx
	jne	.L10
.L7:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	movq	%r12, %rsi
	call	*%rax
	jmp	.L8
	.cfi_endproc
.LFE5114:
	.size	_ZN2v88internal4ZoneD2Ev, .-_ZN2v88internal4ZoneD2Ev
	.globl	_ZN2v88internal4ZoneD1Ev
	.set	_ZN2v88internal4ZoneD1Ev,_ZN2v88internal4ZoneD2Ev
	.section	.rodata._ZN2v88internal4Zone7AsanNewEm.str1.1,"aMS",@progbits,1
.LC0:
	.string	"!sealed_"
.LC1:
	.string	"Check failed: %s."
.LC2:
	.string	"Zone"
	.section	.text._ZN2v88internal4Zone7AsanNewEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Zone7AsanNewEm
	.type	_ZN2v88internal4Zone7AsanNewEm, @function
_ZN2v88internal4Zone7AsanNewEm:
.LFB5116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movzbl	56(%rdi), %ecx
	testb	%cl, %cl
	jne	.L36
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	leaq	7(%rsi), %rbx
	movq	%rdi, %r12
	andq	$-8, %rbx
	subq	%rax, %rdx
	cmpq	%rbx, %rdx
	jb	.L37
	addq	%rax, %rbx
	movq	%rbx, 16(%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movq	40(%rdi), %rdx
	movq	%rbx, %rsi
	testq	%rdx, %rdx
	je	.L20
	addq	(%rdi), %rax
	subq	$24, %rax
	subq	%rdx, %rax
	movq	%rax, (%rdi)
	movq	16(%rdx), %rsi
	addq	%rsi, %rsi
	addq	%rbx, %rsi
	setc	%cl
.L20:
	addq	$32, %rsi
	cmpq	$31, %rsi
	jbe	.L27
	testb	%cl, %cl
	jne	.L27
	cmpq	$8191, %rsi
	jbe	.L30
	cmpq	$32767, %rsi
	jbe	.L26
	leaq	32(%rbx), %rsi
	movl	$32768, %eax
	cmpq	$32768, %rsi
	cmovb	%rax, %rsi
.L26:
	cmpq	$2147483647, %rsi
	ja	.L27
.L25:
	movq	32(%r12), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L27
	movq	16(%rax), %rax
	addq	%rax, 8(%r12)
	movq	%r12, (%rdx)
	movq	40(%r12), %rax
	movq	%rax, 8(%rdx)
	leaq	31(%rdx), %rax
	andq	$-8, %rax
	movq	%rdx, 40(%r12)
	addq	%rax, %rbx
	movq	%rbx, 16(%r12)
	addq	16(%rdx), %rdx
	movq	%rdx, 24(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	$8192, %esi
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.cfi_endproc
.LFE5116:
	.size	_ZN2v88internal4Zone7AsanNewEm, .-_ZN2v88internal4Zone7AsanNewEm
	.section	.text._ZN2v88internal4Zone13ReleaseMemoryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Zone13ReleaseMemoryEv
	.type	_ZN2v88internal4Zone13ReleaseMemoryEv, @function
_ZN2v88internal4Zone13ReleaseMemoryEv:
.LFB5117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN2v88internal19AccountingAllocator15ZoneDestructionEPKNS0_4ZoneE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L48
.L39:
	movq	40(%r12), %rsi
	testq	%rsi, %rsi
	jne	.L42
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rbx, %rsi
.L42:
	movq	16(%rsi), %rax
	movq	8(%rsi), %rbx
	subq	%rax, 8(%r12)
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	32(%r12), %rdi
	testq	%rbx, %rbx
	jne	.L41
.L40:
	pxor	%xmm0, %xmm0
	movq	$0, (%r12)
	leaq	_ZN2v88internal19AccountingAllocator12ZoneCreationEPKNS0_4ZoneE(%rip), %rdx
	movq	$0, 40(%r12)
	movups	%xmm0, 16(%r12)
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L49
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	%r12, %rsi
	call	*%rax
	movq	32(%r12), %rdi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L49:
	popq	%rbx
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE5117:
	.size	_ZN2v88internal4Zone13ReleaseMemoryEv, .-_ZN2v88internal4Zone13ReleaseMemoryEv
	.section	.text._ZN2v88internal4Zone9DeleteAllEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Zone9DeleteAllEv
	.type	_ZN2v88internal4Zone9DeleteAllEv, @function
_ZN2v88internal4Zone9DeleteAllEv:
.LFB5118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	40(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L51
	.p2align 4,,10
	.p2align 3
.L52:
	movq	32(%r12), %rdi
	movq	%rbx, %rsi
	movq	8(%rbx), %rbx
	movq	16(%rsi), %rax
	subq	%rax, 8(%r12)
	movq	(%rdi), %rax
	call	*24(%rax)
	testq	%rbx, %rbx
	jne	.L52
.L51:
	pxor	%xmm0, %xmm0
	popq	%rbx
	movq	$0, (%r12)
	movq	$0, 40(%r12)
	movups	%xmm0, 16(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5118:
	.size	_ZN2v88internal4Zone9DeleteAllEv, .-_ZN2v88internal4Zone9DeleteAllEv
	.section	.text._ZN2v88internal4Zone9NewExpandEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal4Zone9NewExpandEm
	.type	_ZN2v88internal4Zone9NewExpandEm, @function
_ZN2v88internal4Zone9NewExpandEm:
.LFB5120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	40(%rdi), %rdx
	movq	%rdi, %rbx
	testq	%rdx, %rdx
	je	.L67
	movq	(%rdi), %rax
	addq	16(%rdi), %rax
	subq	$24, %rax
	subq	%rdx, %rax
	movq	%rax, (%rdi)
	movq	16(%rdx), %rsi
	addq	%rsi, %rsi
	addq	%r12, %rsi
	setc	%al
.L59:
	addq	$32, %rsi
	cmpq	$31, %rsi
	jbe	.L66
	testb	%al, %al
	jne	.L66
	cmpq	$8191, %rsi
	jbe	.L68
	cmpq	$32767, %rsi
	jbe	.L65
	leaq	32(%r12), %rsi
	movl	$32768, %eax
	cmpq	$32768, %rsi
	cmovb	%rax, %rsi
.L65:
	cmpq	$2147483647, %rsi
	ja	.L66
.L64:
	movq	32(%rbx), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	testq	%rax, %rax
	je	.L66
	leaq	31(%rax), %r8
	movq	16(%rax), %rdx
	addq	%rdx, 8(%rbx)
	andq	$-8, %r8
	movq	%rbx, (%rax)
	movq	40(%rbx), %rdx
	addq	%r8, %r12
	movq	%rdx, 8(%rax)
	movq	%r12, 16(%rbx)
	movq	%rax, 40(%rbx)
	addq	16(%rax), %rax
	movq	%rax, 24(%rbx)
	popq	%rbx
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	$8192, %esi
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L67:
	xorl	%eax, %eax
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal2V823FatalProcessOutOfMemoryEPNS0_7IsolateEPKcb@PLT
	.cfi_endproc
.LFE5120:
	.size	_ZN2v88internal4Zone9NewExpandEm, .-_ZN2v88internal4Zone9NewExpandEm
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal4ZoneC2EPNS0_19AccountingAllocatorEPKc,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal4ZoneC2EPNS0_19AccountingAllocatorEPKc, @function
_GLOBAL__sub_I__ZN2v88internal4ZoneC2EPNS0_19AccountingAllocatorEPKc:
.LFB5890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5890:
	.size	_GLOBAL__sub_I__ZN2v88internal4ZoneC2EPNS0_19AccountingAllocatorEPKc, .-_GLOBAL__sub_I__ZN2v88internal4ZoneC2EPNS0_19AccountingAllocatorEPKc
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal4ZoneC2EPNS0_19AccountingAllocatorEPKc
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
