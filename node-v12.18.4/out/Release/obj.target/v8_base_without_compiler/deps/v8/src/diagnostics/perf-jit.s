	.file	"perf-jit.cc"
	.text
	.section	.text._ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv,"axG",@progbits,_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.type	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv, @function
_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv:
.LFB7101:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7101:
	.size	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv, .-_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.section	.text._ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm,"axG",@progbits,_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm
	.type	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm, @function
_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm:
.LFB7159:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7159:
	.size	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm, .-_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm,"axG",@progbits,_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm
	.type	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm, @function
_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm:
.LFB7160:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7160:
	.size	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm, .-_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm,"axG",@progbits,_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm
	.type	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm, @function
_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm:
.LFB7161:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7161:
	.size	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm, .-_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm
	.section	.text._ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm,"axG",@progbits,_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm
	.type	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm, @function
_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm:
.LFB7162:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7162:
	.size	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm, .-_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm
	.section	.text._ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm,"axG",@progbits,_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm
	.type	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm, @function
_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm:
.LFB7163:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7163:
	.size	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm, .-_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm
	.section	.text._ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv,"axG",@progbits,_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv
	.type	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv, @function
_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv:
.LFB7164:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7164:
	.size	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv, .-_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv
	.section	.text._ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi,"axG",@progbits,_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.type	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, @function
_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi:
.LFB7165:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7165:
	.size	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi, .-_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.section	.text._ZN2v88internal13PerfJitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,"axG",@progbits,_ZN2v88internal13PerfJitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13PerfJitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal13PerfJitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, @function
_ZN2v88internal13PerfJitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE:
.LFB7176:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7176:
	.size	_ZN2v88internal13PerfJitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE, .-_ZN2v88internal13PerfJitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB22121:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE22121:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB22122:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L13
	cmpl	$3, %edx
	je	.L14
	cmpl	$1, %edx
	je	.L18
.L14:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22122:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZN2v84base16LazyInstanceImplINS0_14RecursiveMutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS0_14RecursiveMutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS0_14RecursiveMutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS0_14RecursiveMutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS0_14RecursiveMutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv:
.LFB21744:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base14RecursiveMutexC1Ev@PLT
	.cfi_endproc
.LFE21744:
	.size	_ZN2v84base16LazyInstanceImplINS0_14RecursiveMutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS0_14RecursiveMutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD1Ev
	.type	_ZN2v88internal8OFStreamD1Ev, @function
_ZN2v88internal8OFStreamD1Ev:
.LFB18133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE18133:
	.size	_ZN2v88internal8OFStreamD1Ev, .-_ZN2v88internal8OFStreamD1Ev
	.section	.rodata._ZN2v88internal13PerfJitLogger13CodeMoveEventENS0_12AbstractCodeES2_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"from.IsBytecodeArray()"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal13PerfJitLogger13CodeMoveEventENS0_12AbstractCodeES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger13CodeMoveEventENS0_12AbstractCodeES2_
	.type	_ZN2v88internal13PerfJitLogger13CodeMoveEventENS0_12AbstractCodeES2_, @function
_ZN2v88internal13PerfJitLogger13CodeMoveEventENS0_12AbstractCodeES2_:
.LFB18226:
	.cfi_startproc
	endbr64
	movq	-1(%rsi), %rax
	cmpw	$72, 11(%rax)
	jne	.L27
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18226:
	.size	_ZN2v88internal13PerfJitLogger13CodeMoveEventENS0_12AbstractCodeES2_, .-_ZN2v88internal13PerfJitLogger13CodeMoveEventENS0_12AbstractCodeES2_
	.section	.text._ZN2v88internal8OFStreamD0Ev,"axG",@progbits,_ZN2v88internal8OFStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD0Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD0Ev:
.LFB22298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22298:
	.size	_ZTv0_n24_N2v88internal8OFStreamD0Ev, .-_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal8OFStreamD0Ev
	.type	_ZN2v88internal8OFStreamD0Ev, @function
_ZN2v88internal8OFStreamD0Ev:
.LFB18134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18134:
	.size	_ZN2v88internal8OFStreamD0Ev, .-_ZN2v88internal8OFStreamD0Ev
	.section	.text._ZN2v88internal8OFStreamD1Ev,"axG",@progbits,_ZN2v88internal8OFStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.type	_ZTv0_n24_N2v88internal8OFStreamD1Ev, @function
_ZTv0_n24_N2v88internal8OFStreamD1Ev:
.LFB22299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE22299:
	.size	_ZTv0_n24_N2v88internal8OFStreamD1Ev, .-_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.section	.text._ZN2v88internal13PerfJitLoggerD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLoggerD2Ev
	.type	_ZN2v88internal13PerfJitLoggerD2Ev, @function
_ZN2v88internal13PerfJitLoggerD2Ev:
.LFB18185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal13PerfJitLoggerE(%rip), %rax
	movq	%rax, (%rdi)
	movzbl	_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L49
.L35:
	leaq	8+_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	subq	$1, _ZN2v88internal13PerfJitLogger16reference_count_E(%rip)
	jne	.L37
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rdi
	testq	%rdi, %rdi
	je	.L37
	call	fclose@PLT
	movq	$0, _ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip)
.L37:
	leaq	8+_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rdi
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal15CodeEventLoggerD2Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L50
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_14RecursiveMutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rax
	leaq	-64(%rbp), %r13
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L35
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L35
.L50:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18185:
	.size	_ZN2v88internal13PerfJitLoggerD2Ev, .-_ZN2v88internal13PerfJitLoggerD2Ev
	.globl	_ZN2v88internal13PerfJitLoggerD1Ev
	.set	_ZN2v88internal13PerfJitLoggerD1Ev,_ZN2v88internal13PerfJitLoggerD2Ev
	.section	.text._ZN2v88internal13PerfJitLoggerD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLoggerD0Ev
	.type	_ZN2v88internal13PerfJitLoggerD0Ev, @function
_ZN2v88internal13PerfJitLoggerD0Ev:
.LFB18187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal13PerfJitLoggerD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE18187:
	.size	_ZN2v88internal13PerfJitLoggerD0Ev, .-_ZN2v88internal13PerfJitLoggerD0Ev
	.section	.rodata._ZN2v88internal13PerfJitLogger15OpenJitDumpFileEv.str1.1,"aMS",@progbits,1
.LC2:
	.string	"NewArray"
.LC3:
	.string	"size != -1"
.LC4:
	.string	"w+"
	.section	.text._ZN2v88internal13PerfJitLogger15OpenJitDumpFileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger15OpenJitDumpFileEv
	.type	_ZN2v88internal13PerfJitLogger15OpenJitDumpFileEv, @function
_ZN2v88internal13PerfJitLogger15OpenJitDumpFileEv:
.LFB18176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$30, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	$0, _ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip)
	call	_ZnamRKSt9nothrow_t@PLT
	testq	%rax, %rax
	je	.L74
	movq	%rax, %r12
.L54:
	movq	$30, -24(%rbp)
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movq	-24(%rbp), %rsi
	leaq	_ZN2v88internal13PerfJitLogger21kFilenameFormatStringE(%rip), %rdx
	movq	%r12, %rdi
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	cmpl	$-1, %eax
	je	.L75
	movl	$438, %edx
	movl	$578, %esi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	open@PLT
	movl	%eax, %r13d
	cmpl	$-1, %eax
	je	.L58
	movl	$30, %edi
	call	sysconf@PLT
	movq	%rax, %rsi
	cmpq	$-1, %rax
	je	.L59
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	%r13d, %r8d
	movl	$2, %ecx
	movl	$5, %edx
	call	mmap@PLT
	cmpq	$-1, %rax
	je	.L59
	movq	%rax, _ZN2v88internal13PerfJitLogger15marker_address_E(%rip)
	testq	%rax, %rax
	je	.L58
	movl	%r13d, %edi
	leaq	.LC4(%rip), %rsi
	call	fdopen@PLT
	movq	%rax, _ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L58
	movl	$2097152, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	setvbuf@PLT
.L58:
	addq	$16, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	$0, _ZN2v88internal13PerfJitLogger15marker_address_E(%rip)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L74:
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*24(%rax)
	leaq	_ZSt7nothrow(%rip), %rsi
	movl	$30, %edi
	call	_ZnamRKSt9nothrow_t@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L54
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal23FatalProcessOutOfMemoryEPNS0_7IsolateEPKc@PLT
	.cfi_endproc
.LFE18176:
	.size	_ZN2v88internal13PerfJitLogger15OpenJitDumpFileEv, .-_ZN2v88internal13PerfJitLogger15OpenJitDumpFileEv
	.section	.text._ZN2v88internal13PerfJitLogger16CloseJitDumpFileEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger16CloseJitDumpFileEv
	.type	_ZN2v88internal13PerfJitLogger16CloseJitDumpFileEv, @function
_ZN2v88internal13PerfJitLogger16CloseJitDumpFileEv:
.LFB18178:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rdi
	testq	%rdi, %rdi
	je	.L82
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	fclose@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	$0, _ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE18178:
	.size	_ZN2v88internal13PerfJitLogger16CloseJitDumpFileEv, .-_ZN2v88internal13PerfJitLogger16CloseJitDumpFileEv
	.section	.text._ZN2v88internal13PerfJitLogger14OpenMarkerFileEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger14OpenMarkerFileEi
	.type	_ZN2v88internal13PerfJitLogger14OpenMarkerFileEi, @function
_ZN2v88internal13PerfJitLogger14OpenMarkerFileEi:
.LFB18179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$30, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%esi, %ebx
	subq	$8, %rsp
	call	sysconf@PLT
	cmpq	$-1, %rax
	je	.L88
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movq	%rax, %rsi
	movl	%ebx, %r8d
	movl	$2, %ecx
	movl	$5, %edx
	call	mmap@PLT
	cmpq	$-1, %rax
	je	.L88
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18179:
	.size	_ZN2v88internal13PerfJitLogger14OpenMarkerFileEi, .-_ZN2v88internal13PerfJitLogger14OpenMarkerFileEi
	.section	.text._ZN2v88internal13PerfJitLogger15CloseMarkerFileEPv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger15CloseMarkerFileEPv
	.type	_ZN2v88internal13PerfJitLogger15CloseMarkerFileEPv, @function
_ZN2v88internal13PerfJitLogger15CloseMarkerFileEPv:
.LFB18180:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L101
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$30, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	call	sysconf@PLT
	movq	%rax, %rsi
	cmpq	$-1, %rax
	je	.L93
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	munmap@PLT
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE18180:
	.size	_ZN2v88internal13PerfJitLogger15CloseMarkerFileEPv, .-_ZN2v88internal13PerfJitLogger15CloseMarkerFileEPv
	.section	.text._ZN2v88internal13PerfJitLoggerC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLoggerC2EPNS0_7IsolateE
	.type	_ZN2v88internal13PerfJitLoggerC2EPNS0_7IsolateE, @function
_ZN2v88internal13PerfJitLoggerC2EPNS0_7IsolateE:
.LFB18182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal15CodeEventLoggerC2EPNS0_7IsolateE@PLT
	leaq	16+_ZTVN2v88internal13PerfJitLoggerE(%rip), %rax
	movq	%rax, (%r12)
	movzbl	_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L120
.L105:
	leaq	8+_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	movq	_ZN2v88internal13PerfJitLogger16reference_count_E(%rip), %rax
	addq	$1, %rax
	movq	%rax, _ZN2v88internal13PerfJitLogger16reference_count_E(%rip)
	cmpq	$1, %rax
	je	.L121
.L107:
	leaq	8+_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rdi
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_14RecursiveMutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rax
	leaq	-64(%rbp), %r13
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r13, %rsi
	leaq	_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L105
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%r12, %rdi
	call	_ZN2v88internal13PerfJitLogger15OpenJitDumpFileEv
	cmpq	$0, _ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip)
	je	.L107
	movdqa	.LC5(%rip), %xmm0
	movl	$-559038737, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movl	%eax, -92(%rbp)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*128(%rax)
	mulsd	.LC6(%rip), %xmm0
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L109
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -88(%rbp)
.L110:
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	leaq	-112(%rbp), %rdi
	movl	$40, %edx
	movl	$1, %esi
	movq	$0, -80(%rbp)
	call	fwrite@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L109:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -88(%rbp)
	btcq	$63, -88(%rbp)
	jmp	.L110
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18182:
	.size	_ZN2v88internal13PerfJitLoggerC2EPNS0_7IsolateE, .-_ZN2v88internal13PerfJitLoggerC2EPNS0_7IsolateE
	.globl	_ZN2v88internal13PerfJitLoggerC1EPNS0_7IsolateE
	.set	_ZN2v88internal13PerfJitLoggerC1EPNS0_7IsolateE,_ZN2v88internal13PerfJitLoggerC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal13PerfJitLogger12GetTimestampEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger12GetTimestampEv
	.type	_ZN2v88internal13PerfJitLogger12GetTimestampEv, @function
_ZN2v88internal13PerfJitLogger12GetTimestampEv:
.LFB18188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-32(%rbp), %rsi
	call	clock_gettime@PLT
	imulq	$1000000000, -32(%rbp), %rax
	addq	-24(%rbp), %rax
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L126
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L126:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18188:
	.size	_ZN2v88internal13PerfJitLogger12GetTimestampEv, .-_ZN2v88internal13PerfJitLogger12GetTimestampEv
	.section	.text._ZN2v88internal13PerfJitLogger21WriteJitCodeLoadEntryEPKhjPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger21WriteJitCodeLoadEntryEPKhjPKci
	.type	_ZN2v88internal13PerfJitLogger21WriteJitCodeLoadEntryEPKhjPKci, @function
_ZN2v88internal13PerfJitLogger21WriteJitCodeLoadEntryEPKhjPKci:
.LFB18191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	leaq	-112(%rbp), %rsi
	pushq	%r12
	.cfi_offset 12, -40
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leal	57(%rdx,%r8), %eax
	movl	$0, -96(%rbp)
	movl	%eax, -92(%rbp)
	call	clock_gettime@PLT
	imulq	$1000000000, -112(%rbp), %rax
	addq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movl	%eax, -80(%rbp)
	call	_ZN2v84base2OS18GetCurrentThreadIdEv@PLT
	movq	%r13, %xmm0
	leaq	-96(%rbp), %rdi
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	movl	%eax, -76(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movl	$56, %edx
	movq	_ZN2v88internal13PerfJitLogger11code_index_E(%rip), %rax
	movups	%xmm0, -72(%rbp)
	movd	%ebx, %xmm0
	movl	$1, %esi
	movhps	_ZN2v88internal13PerfJitLogger11code_index_E(%rip), %xmm0
	addq	$1, %rax
	movups	%xmm0, -56(%rbp)
	movq	%rax, _ZN2v88internal13PerfJitLogger11code_index_E(%rip)
	call	fwrite@PLT
	movslq	%r12d, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	call	fwrite@PLT
	movl	$1, %edx
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	movl	$1, %esi
	leaq	_ZZN2v88internal13PerfJitLogger21WriteJitCodeLoadEntryEPKhjPKciE17string_terminator(%rip), %rdi
	call	fwrite@PLT
	movslq	%ebx, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	call	fwrite@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L130
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L130:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18191:
	.size	_ZN2v88internal13PerfJitLogger21WriteJitCodeLoadEntryEPKhjPKci, .-_ZN2v88internal13PerfJitLogger21WriteJitCodeLoadEntryEPKhjPKci
	.section	.text._ZN2v88internal13PerfJitLogger17LogWriteDebugInfoENS0_4CodeENS0_18SharedFunctionInfoE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger17LogWriteDebugInfoENS0_4CodeENS0_18SharedFunctionInfoE
	.type	_ZN2v88internal13PerfJitLogger17LogWriteDebugInfoENS0_4CodeENS0_18SharedFunctionInfoE, @function
_ZN2v88internal13PerfJitLogger17LogWriteDebugInfoENS0_4CodeENS0_18SharedFunctionInfoE:
.LFB18197:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -264(%rbp)
	movq	%rdx, -272(%rbp)
	movq	23(%rsi), %rsi
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testb	$1, %sil
	jne	.L132
.L134:
	movq	7(%rsi), %rsi
.L133:
	leaq	-128(%rbp), %r12
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE@PLT
	cmpl	$-1, -104(%rbp)
	je	.L131
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L136:
	movq	%r12, %rdi
	addl	$1, %ebx
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -104(%rbp)
	jne	.L136
	testl	%ebx, %ebx
	je	.L131
	leaq	-272(%rbp), %rdi
	call	_ZNK2v88internal18SharedFunctionInfo13HasSourceCodeEv@PLT
	testb	%al, %al
	je	.L131
	movq	-272(%rbp), %rax
	movq	%rax, %rdx
	movq	31(%rax), %rsi
	andq	$-262144, %rdx
	movq	24(%rdx), %r13
	subq	$37592, %r13
	testb	$1, %sil
	jne	.L202
.L140:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L141
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L142:
	movq	%r12, %rsi
	movl	$1, %edi
	movl	$2, -192(%rbp)
	call	clock_gettime@PLT
	movq	-264(%rbp), %rsi
	imulq	$1000000000, -128(%rbp), %rax
	addq	-120(%rbp), %rax
	movq	%rax, -184(%rbp)
	movl	43(%rsi), %ecx
	leaq	63(%rsi), %rax
	testl	%ecx, %ecx
	js	.L203
.L145:
	movq	41112(%r13), %rdi
	movq	%rax, -176(%rbp)
	movl	%ebx, %eax
	sall	$4, %ebx
	movq	%rax, -168(%rbp)
	addl	$32, %ebx
	testq	%rdi, %rdi
	je	.L146
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L147:
	movq	41112(%r13), %rdi
	movq	-272(%rbp), %rsi
	testq	%rdi, %rdi
	je	.L149
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L150:
	movq	-264(%rbp), %rax
	movq	23(%rax), %rsi
	testb	$1, %sil
	jne	.L152
.L154:
	movq	7(%rsi), %rsi
.L153:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE@PLT
	cmpl	$-1, -104(%rbp)
	je	.L155
	leaq	-160(%rbp), %rax
	movq	%rax, -280(%rbp)
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L205:
	leaq	-224(%rbp), %r13
	leaq	-232(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZNK2v88internal14SourcePosition13InliningStackENS0_6HandleINS0_4CodeEEE@PLT
	movq	-224(%rbp), %rdi
	movdqu	(%rdi), %xmm0
	movaps	%xmm0, -160(%rbp)
	movdqu	16(%rdi), %xmm1
	movaps	%xmm1, -144(%rbp)
	call	_ZdlPv@PLT
.L157:
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L160
	movq	(%rax), %rax
	leaq	-224(%rbp), %r13
	movq	%r13, %rdi
	movq	%rax, -224(%rbp)
	call	_ZN2v88internal6Script18GetNameOrSourceURLEv@PLT
	testb	$1, %al
	jne	.L204
.L160:
	movl	$10, %eax
	movq	%r12, %rdi
	addl	%eax, %ebx
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -104(%rbp)
	je	.L155
.L164:
	movq	-88(%rbp), %rsi
	movq	%rsi, -232(%rbp)
	movq	(%r14), %rax
	testb	$64, 43(%rax)
	jne	.L205
	movq	-280(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal18SourcePositionInfoC1ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L178:
	movslq	-284(%rbp), %rdx
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	leaq	-64(%rbp), %rdi
	movl	$1, %esi
	movq	$0, -64(%rbp)
	call	fwrite@PLT
.L131:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	cmpw	$70, 11(%rax)
	jne	.L134
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L141:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L207
.L143:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L142
.L202:
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L140
	movq	23(%rsi), %rsi
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L204:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L160
	movq	%rax, -232(%rbp)
	movq	-1(%rax), %rdx
	testb	$8, 11(%rdx)
	je	.L161
	movslq	11(%rax), %rax
.L162:
	addl	$1, %eax
	movq	%r12, %rdi
	addl	%eax, %ebx
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -104(%rbp)
	jne	.L164
	.p2align 4,,10
	.p2align 3
.L155:
	leal	7(%rbx), %eax
	movl	$32, %edx
	movl	$1, %esi
	andl	$-8, %eax
	leaq	-192(%rbp), %rdi
	movl	%eax, %ecx
	movl	%eax, -188(%rbp)
	subl	%ebx, %ecx
	movl	%ecx, -284(%rbp)
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	call	fwrite@PLT
	movq	-264(%rbp), %rax
	movl	43(%rax), %edx
	leaq	63(%rax), %rbx
	testl	%edx, %edx
	js	.L208
.L166:
	movq	23(%rax), %rsi
	testb	$1, %sil
	jne	.L167
.L169:
	movq	7(%rsi), %rsi
.L168:
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-224(%rbp), %r13
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_9ByteArrayENS1_15IterationFilterE@PLT
	cmpl	$-1, -104(%rbp)
	jne	.L170
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%r13, %rdi
	leaq	-232(%rbp), %rsi
	movq	%r14, %rdx
	call	_ZNK2v88internal14SourcePosition13InliningStackENS0_6HandleINS0_4CodeEEE@PLT
	movq	-224(%rbp), %rdi
	movdqu	(%rdi), %xmm2
	movaps	%xmm2, -160(%rbp)
	movdqu	16(%rdi), %xmm3
	movaps	%xmm3, -144(%rbp)
	call	_ZdlPv@PLT
.L173:
	movslq	-96(%rbp), %rax
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	movl	$16, %edx
	movq	%r13, %rdi
	movl	$1, %esi
	leaq	64(%rbx,%rax), %rax
	movq	%rax, -224(%rbp)
	movl	-136(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -216(%rbp)
	movl	-132(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -212(%rbp)
	call	fwrite@PLT
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	je	.L174
	movq	(%rax), %rax
	leaq	-232(%rbp), %rdi
	movq	%rdi, -280(%rbp)
	movq	%rax, -232(%rbp)
	call	_ZN2v88internal6Script18GetNameOrSourceURLEv@PLT
	movq	-280(%rbp), %rdi
	testb	$1, %al
	jne	.L209
.L174:
	movl	$10, %edx
	leaq	_ZN2v88internal12_GLOBAL__N_1L24kUnknownScriptNameStringE(%rip), %rdi
.L176:
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	movl	$1, %esi
	call	fwrite@PLT
.L177:
	movq	%r12, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -104(%rbp)
	je	.L178
.L170:
	movq	-88(%rbp), %rsi
	movq	%rsi, -232(%rbp)
	movq	(%r14), %rax
	testb	$64, 43(%rax)
	jne	.L210
	leaq	-160(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN2v88internal18SourcePositionInfoC1ENS0_14SourcePositionENS0_6HandleINS0_18SharedFunctionInfoEEE@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L209:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L175
	movq	-1(%rax), %rdx
	testb	$7, 11(%rdx)
	je	.L211
.L175:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L174
	leaq	-240(%rbp), %rsi
	movl	$1, %ecx
	leaq	-244(%rbp), %r8
	movl	$1, %edx
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movl	-244(%rbp), %eax
	movl	$1, %esi
	movq	-232(%rbp), %rdi
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	leal	1(%rax), %edx
	movq	%rdi, -280(%rbp)
	movslq	%edx, %rdx
	call	fwrite@PLT
	movq	-280(%rbp), %rdi
	call	_ZdaPv@PLT
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L149:
	movq	41088(%r13), %r15
	cmpq	%r15, 41096(%r13)
	je	.L212
.L151:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L146:
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L213
.L148:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L147
.L152:
	movq	-1(%rsi), %rax
	cmpw	$70, 11(%rax)
	jne	.L154
	jmp	.L153
.L167:
	movq	-1(%rsi), %rax
	cmpw	$70, 11(%rax)
	jne	.L169
	jmp	.L168
.L203:
	leaq	-264(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-264(%rbp), %rsi
	jmp	.L145
.L208:
	leaq	-264(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rbx
	movq	-264(%rbp), %rax
	jmp	.L166
.L211:
	movq	-1(%rax), %rdx
	testb	$8, 11(%rdx)
	je	.L175
	movl	11(%rax), %edx
	leaq	15(%rax), %rdi
	addl	$1, %edx
	movslq	%edx, %rdx
	jmp	.L176
.L161:
	movq	%r13, %rdi
	movl	$1, %ecx
	movl	$1, %edx
	leaq	-232(%rbp), %rsi
	leaq	-240(%rbp), %r8
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L163
	call	_ZdaPv@PLT
.L163:
	movslq	-240(%rbp), %rax
	jmp	.L162
.L213:
	movq	%r13, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L148
.L212:
	movq	%r13, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L151
.L207:
	movq	%r13, %rdi
	movq	%rsi, -280(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-280(%rbp), %rsi
	jmp	.L143
.L206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18197:
	.size	_ZN2v88internal13PerfJitLogger17LogWriteDebugInfoENS0_4CodeENS0_18SharedFunctionInfoE, .-_ZN2v88internal13PerfJitLogger17LogWriteDebugInfoENS0_4CodeENS0_18SharedFunctionInfoE
	.section	.text._ZN2v88internal13PerfJitLogger17LogWriteDebugInfoEPKNS0_4wasm8WasmCodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger17LogWriteDebugInfoEPKNS0_4wasm8WasmCodeE
	.type	_ZN2v88internal13PerfJitLogger17LogWriteDebugInfoEPKNS0_4wasm8WasmCodeE, @function
_ZN2v88internal13PerfJitLogger17LogWriteDebugInfoEPKNS0_4wasm8WasmCodeE:
.LFB18224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 3, -56
	movq	48(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v88internal4wasm12NativeModule16GetWasmSourceMapEv@PLT
	testq	%rax, %rax
	je	.L214
	cmpb	$0, 96(%rax)
	movq	%rax, %rbx
	jne	.L243
.L214:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L244
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	movq	48(%r12), %rax
	movq	%rbx, %rdi
	movq	208(%rax), %rdx
	movl	56(%r12), %eax
	salq	$5, %rax
	addq	136(%rdx), %rax
	movl	16(%rax), %r14d
	movl	%r14d, %edx
	movq	%r14, %rsi
	addl	20(%rax), %edx
	movq	%r14, %r13
	call	_ZNK2v88internal4wasm19WasmModuleSourceMap9HasSourceEmm@PLT
	testb	%al, %al
	je	.L214
	movq	40(%r12), %rdx
	movq	32(%r12), %rsi
	leaq	-160(%rbp), %r15
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE@PLT
	cmpl	$-1, -136(%rbp)
	je	.L214
	movl	$0, -224(%rbp)
	leaq	-96(%rbp), %rax
	movl	$0, -228(%rbp)
	movq	%rax, -240(%rbp)
	.p2align 4,,10
	.p2align 3
.L219:
	movq	-120(%rbp), %rax
	movq	%r14, %rsi
	movq	%rbx, %rdi
	shrq	%rax
	andl	$1073741823, %eax
	leal	-1(%r13,%rax), %edx
	movq	%rdx, -216(%rbp)
	call	_ZNK2v88internal4wasm19WasmModuleSourceMap13HasValidEntryEmm@PLT
	testb	%al, %al
	je	.L217
	movq	-240(%rbp), %rdi
	movq	-216(%rbp), %rdx
	movq	%rbx, %rsi
	addl	$1, -228(%rbp)
	call	_ZNK2v88internal4wasm19WasmModuleSourceMap11GetFilenameB5cxx11Em@PLT
	movl	-224(%rbp), %ecx
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rdi
	leal	1(%rcx,%rax), %eax
	movl	%eax, -224(%rbp)
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L217
	call	_ZdlPv@PLT
.L217:
	movq	%r15, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -136(%rbp)
	jne	.L219
	movl	-228(%rbp), %eax
	testl	%eax, %eax
	je	.L214
	movq	%r15, %rsi
	movl	$1, %edi
	movl	$2, -192(%rbp)
	call	clock_gettime@PLT
	movl	-228(%rbp), %ecx
	movl	$1, %esi
	imulq	$1000000000, -160(%rbp), %rax
	addq	-152(%rbp), %rax
	leaq	-192(%rbp), %rdi
	movq	%rax, -184(%rbp)
	movq	(%r12), %rax
	movq	%rcx, -168(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rcx, %rax
	movl	-224(%rbp), %ecx
	sall	$4, %eax
	leal	32(%rcx,%rax), %edx
	leal	7(%rdx), %eax
	andl	$-8, %eax
	movl	%eax, %ecx
	movl	%eax, -188(%rbp)
	subl	%edx, %ecx
	movl	$32, %edx
	movl	%ecx, -228(%rbp)
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	call	fwrite@PLT
	movq	(%r12), %rax
	movq	40(%r12), %rdx
	xorl	%ecx, %ecx
	movq	32(%r12), %rsi
	movq	%r15, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE@PLT
	cmpl	$-1, -136(%rbp)
	je	.L222
	leaq	-208(%rbp), %rax
	movq	%rax, -224(%rbp)
	.p2align 4,,10
	.p2align 3
.L224:
	movq	-120(%rbp), %rax
	movq	%r14, %rsi
	movq	%rbx, %rdi
	shrq	%rax
	andl	$1073741823, %eax
	leal	-1(%r13,%rax), %r12d
	movq	%r12, %rdx
	call	_ZNK2v88internal4wasm19WasmModuleSourceMap13HasValidEntryEmm@PLT
	testb	%al, %al
	je	.L225
	movq	-216(%rbp), %rcx
	movslq	-128(%rbp), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	64(%rcx,%rax), %rax
	movq	%rax, -208(%rbp)
	call	_ZNK2v88internal4wasm19WasmModuleSourceMap13GetSourceLineEm@PLT
	movl	$16, %edx
	movl	$1, %esi
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	movq	-224(%rbp), %rdi
	addl	$1, %eax
	movl	$1, -196(%rbp)
	movl	%eax, -200(%rbp)
	call	fwrite@PLT
	leaq	-96(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZNK2v88internal4wasm19WasmModuleSourceMap11GetFilenameB5cxx11Em@PLT
	movl	-88(%rbp), %eax
	movq	-96(%rbp), %rdi
	movl	$1, %esi
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	leal	1(%rax), %edx
	movslq	%edx, %rdx
	call	fwrite@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L225
	call	_ZdlPv@PLT
.L225:
	movq	%r15, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -136(%rbp)
	jne	.L224
.L222:
	movslq	-228(%rbp), %rdx
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	leaq	-64(%rbp), %rdi
	movl	$1, %esi
	movq	$0, -64(%rbp)
	call	fwrite@PLT
	jmp	.L214
.L244:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18224:
	.size	_ZN2v88internal13PerfJitLogger17LogWriteDebugInfoEPKNS0_4wasm8WasmCodeE, .-_ZN2v88internal13PerfJitLogger17LogWriteDebugInfoEPKNS0_4wasm8WasmCodeE
	.section	.text._ZN2v88internal13PerfJitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.type	_ZN2v88internal13PerfJitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci, @function
_ZN2v88internal13PerfJitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci:
.LFB18190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L260
.L246:
	leaq	8+_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	cmpq	$0, _ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip)
	je	.L259
	cmpb	$0, _ZN2v88internal28FLAG_perf_prof_annotate_wasmE(%rip)
	jne	.L261
.L250:
	movl	8(%rbx), %edx
	movq	(%rbx), %rsi
	movl	%r14d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal13PerfJitLogger21WriteJitCodeLoadEntryEPKhjPKci
.L259:
	leaq	8+_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rdi
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L262
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_14RecursiveMutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rax
	leaq	-96(%rbp), %r15
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r15, %rsi
	leaq	_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L246
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13PerfJitLogger17LogWriteDebugInfoEPKNS0_4wasm8WasmCodeE
	jmp	.L250
.L262:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18190:
	.size	_ZN2v88internal13PerfJitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci, .-_ZN2v88internal13PerfJitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.section	.text._ZN2v88internal13PerfJitLogger21LogWriteUnwindingInfoENS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger21LogWriteUnwindingInfoENS0_4CodeE
	.type	_ZN2v88internal13PerfJitLogger21LogWriteUnwindingInfoENS0_4CodeE, @function
_ZN2v88internal13PerfJitLogger21LogWriteUnwindingInfoENS0_4CodeE:
.LFB18225:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	-464(%rbp), %rsi
	subq	$440, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$4, -448(%rbp)
	call	clock_gettime@PLT
	imulq	$1000000000, -464(%rbp), %rax
	addq	-456(%rbp), %rax
	movq	$20, -424(%rbp)
	movq	%rax, -440(%rbp)
	testb	$1, 43(%rbx)
	je	.L268
	movl	39(%rbx), %eax
	addl	$71, %eax
	andl	$-8, %eax
	cltq
	movslq	-1(%rbx,%rax), %rdx
	leal	47(%rdx), %eax
	leal	40(%rdx), %ecx
	andl	$-8, %eax
	movl	%eax, %r12d
	subl	%ecx, %r12d
	movq	%rdx, %rcx
	movslq	%r12d, %r12
.L264:
	movq	%rcx, -416(%rbp)
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	movl	$1, %esi
	leaq	-448(%rbp), %rdi
	movq	%rdx, -432(%rbp)
	movl	$40, %edx
	movl	%eax, -444(%rbp)
	call	fwrite@PLT
	testb	$1, 43(%rbx)
	je	.L265
	movl	39(%rbx), %eax
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	movl	$1, %esi
	leal	71(%rax), %edi
	andl	$-8, %edi
	movslq	%edi, %rdi
	addq	%rdi, %rbx
	movslq	-1(%rbx), %rdx
	leaq	7(%rbx), %rdi
	call	fwrite@PLT
.L266:
	leaq	-49(%rbp), %rdi
	movq	%r12, %rdx
	movl	$1, %esi
	movb	$0, -41(%rbp)
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	movq	$0, -49(%rbp)
	call	fwrite@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L270
	addq	$440, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movl	$4, %r12d
	movl	$64, %eax
	xorl	%ecx, %ecx
	movl	$20, %edx
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L265:
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rsi
	leaq	-400(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal8OFStreamC1EP8_IO_FILE@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal13EhFrameWriter17WriteEmptyEhFrameERSo@PLT
	leaq	64+_ZTVN2v88internal8OFStreamE(%rip), %rax
	leaq	24+_ZTVN2v88internal8OFStreamE(%rip), %rcx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal8OFStreamE0_So(%rip), %rax
	leaq	-320(%rbp), %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L266
.L270:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18225:
	.size	_ZN2v88internal13PerfJitLogger21LogWriteUnwindingInfoENS0_4CodeE, .-_ZN2v88internal13PerfJitLogger21LogWriteUnwindingInfoENS0_4CodeE
	.section	.text._ZN2v88internal13PerfJitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.type	_ZN2v88internal13PerfJitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci, @function
_ZN2v88internal13PerfJitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci:
.LFB18189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$88, %rsp
	movq	%rdx, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal35FLAG_perf_basic_prof_only_functionsE(%rip)
	je	.L275
	movq	-1(%rsi), %rax
	cmpw	$69, 11(%rax)
	je	.L298
.L275:
	movzbl	_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %eax
	cmpb	$2, %al
	jne	.L299
.L274:
	leaq	8+_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rdi
	call	_ZN2v84base14RecursiveMutex4LockEv@PLT
	cmpq	$0, _ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip)
	je	.L280
	movq	-1(%r14), %rax
	cmpw	$69, 11(%rax)
	je	.L300
.L280:
	leaq	8+_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rdi
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
.L271:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L301
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS0_14RecursiveMutexENS0_32StaticallyAllocatedInstanceTraitIS2_EENS0_21DefaultConstructTraitIS2_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS2_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %r8
	leaq	8+_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r8, %rsi
	leaq	_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rdi
	movq	%rax, %xmm1
	movq	%r8, -128(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L274
	movq	-128(%rbp), %r8
	movl	$3, %edx
	movq	%r8, %rsi
	movq	%r8, %rdi
	call	*%rax
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L298:
	movl	43(%rsi), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$12, %eax
	je	.L275
	movq	-1(%rsi), %rdx
	cmpw	$69, 11(%rdx)
	jne	.L271
	testl	%eax, %eax
	jne	.L271
	movzbl	_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %eax
	cmpb	$2, %al
	je	.L274
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L300:
	cmpb	$0, _ZN2v88internal14FLAG_perf_profE(%rip)
	movl	43(%r14), %eax
	movq	%r14, -104(%rbp)
	je	.L281
	cmpq	$0, -120(%rbp)
	je	.L281
	movl	%eax, %edx
	shrl	%edx
	andl	$31, %edx
	subl	$7, %edx
	cmpl	$1, %edx
	ja	.L302
	.p2align 4,,10
	.p2align 3
.L281:
	addq	$63, %r15
	leaq	-104(%rbp), %r14
	testl	%eax, %eax
	js	.L303
.L283:
	movq	%r14, %rdi
	call	_ZNK2v88internal4Code25ExecutableInstructionSizeEv@PLT
	cmpb	$0, _ZN2v88internal29FLAG_perf_prof_unwinding_infoE(%rip)
	movl	%eax, %r14d
	jne	.L304
.L284:
	movq	%r12, %rdi
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movl	%r14d, %edx
	movq	%r15, %rsi
	call	_ZN2v88internal13PerfJitLogger21WriteJitCodeLoadEntryEPKhjPKci
	leaq	8+_ZN2v88internal13PerfJitLogger11file_mutex_E(%rip), %rdi
	call	_ZN2v84base14RecursiveMutex6UnlockEv@PLT
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L304:
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13PerfJitLogger21LogWriteUnwindingInfoENS0_4CodeE
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L303:
	movq	%r14, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %r15
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L302:
	movq	-120(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal13PerfJitLogger17LogWriteDebugInfoENS0_4CodeENS0_18SharedFunctionInfoE
	movq	-104(%rbp), %r15
	movl	43(%r15), %eax
	jmp	.L281
.L301:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18189:
	.size	_ZN2v88internal13PerfJitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci, .-_ZN2v88internal13PerfJitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.section	.text._ZN2v88internal13PerfJitLogger13LogWriteBytesEPKci,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger13LogWriteBytesEPKci
	.type	_ZN2v88internal13PerfJitLogger13LogWriteBytesEPKci, @function
_ZN2v88internal13PerfJitLogger13LogWriteBytesEPKci:
.LFB18227:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	movq	%rsi, %rdi
	movslq	%edx, %rdx
	movl	$1, %esi
	jmp	fwrite@PLT
	.cfi_endproc
.LFE18227:
	.size	_ZN2v88internal13PerfJitLogger13LogWriteBytesEPKci, .-_ZN2v88internal13PerfJitLogger13LogWriteBytesEPKci
	.section	.text._ZN2v88internal13PerfJitLogger14LogWriteHeaderEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13PerfJitLogger14LogWriteHeaderEv
	.type	_ZN2v88internal13PerfJitLogger14LogWriteHeaderEv, @function
_ZN2v88internal13PerfJitLogger14LogWriteHeaderEv:
.LFB18228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movdqa	.LC5(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$-559038737, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base2OS19GetCurrentProcessIdEv@PLT
	movl	%eax, -28(%rbp)
	call	_ZN2v88internal2V818GetCurrentPlatformEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	call	*128(%rax)
	mulsd	.LC6(%rip), %xmm0
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L307
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -24(%rbp)
.L308:
	movq	_ZN2v88internal13PerfJitLogger19perf_output_handle_E(%rip), %rcx
	leaq	-48(%rbp), %rdi
	movl	$40, %edx
	movl	$1, %esi
	movq	$0, -16(%rbp)
	call	fwrite@PLT
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L311
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	movq	%rax, -24(%rbp)
	btcq	$63, -24(%rbp)
	jmp	.L308
.L311:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18228:
	.size	_ZN2v88internal13PerfJitLogger14LogWriteHeaderEv, .-_ZN2v88internal13PerfJitLogger14LogWriteHeaderEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal13PerfJitLogger21kFilenameFormatStringE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal13PerfJitLogger21kFilenameFormatStringE, @function
_GLOBAL__sub_I__ZN2v88internal13PerfJitLogger21kFilenameFormatStringE:
.LFB22231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22231:
	.size	_GLOBAL__sub_I__ZN2v88internal13PerfJitLogger21kFilenameFormatStringE, .-_GLOBAL__sub_I__ZN2v88internal13PerfJitLogger21kFilenameFormatStringE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal13PerfJitLogger21kFilenameFormatStringE
	.hidden	_ZTCN2v88internal8OFStreamE0_So
	.weak	_ZTCN2v88internal8OFStreamE0_So
	.section	.rodata._ZTCN2v88internal8OFStreamE0_So,"aG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal8OFStreamE0_So, @object
	.size	_ZTCN2v88internal8OFStreamE0_So, 80
_ZTCN2v88internal8OFStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal8OFStreamE, @object
	.size	_ZTTN2v88internal8OFStreamE, 32
_ZTTN2v88internal8OFStreamE:
	.quad	_ZTVN2v88internal8OFStreamE+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+24
	.quad	_ZTCN2v88internal8OFStreamE0_So+64
	.quad	_ZTVN2v88internal8OFStreamE+64
	.weak	_ZTVN2v88internal8OFStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal8OFStreamE,"awG",@progbits,_ZTVN2v88internal8OFStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal8OFStreamE, @object
	.size	_ZTVN2v88internal8OFStreamE, 80
_ZTVN2v88internal8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal8OFStreamD1Ev
	.quad	_ZN2v88internal8OFStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal8OFStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal8OFStreamD0Ev
	.weak	_ZTVN2v88internal13PerfJitLoggerE
	.section	.data.rel.ro._ZTVN2v88internal13PerfJitLoggerE,"awG",@progbits,_ZTVN2v88internal13PerfJitLoggerE,comdat
	.align 8
	.type	_ZTVN2v88internal13PerfJitLoggerE, @object
	.size	_ZTVN2v88internal13PerfJitLoggerE, 176
_ZTVN2v88internal13PerfJitLoggerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal13PerfJitLoggerD1Ev
	.quad	_ZN2v88internal13PerfJitLoggerD0Ev
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeEPKc
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_4NameE
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameE
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsENS0_12AbstractCodeENS0_18SharedFunctionInfoENS0_4NameEii
	.quad	_ZN2v88internal15CodeEventLogger15CodeCreateEventENS0_17CodeEventListener16LogEventsAndTagsEPKNS0_4wasm8WasmCodeENS0_6VectorIKcEE
	.quad	_ZN2v88internal15CodeEventLogger13CallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger19GetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger19SetterCallbackEventENS0_4NameEm
	.quad	_ZN2v88internal15CodeEventLogger21RegExpCodeCreateEventENS0_12AbstractCodeENS0_6StringE
	.quad	_ZN2v88internal13PerfJitLogger13CodeMoveEventENS0_12AbstractCodeES2_
	.quad	_ZN2v88internal15CodeEventLogger27SharedFunctionInfoMoveEventEmm
	.quad	_ZN2v88internal15CodeEventLogger22NativeContextMoveEventEmm
	.quad	_ZN2v88internal15CodeEventLogger17CodeMovingGCEventEv
	.quad	_ZN2v88internal13PerfJitLogger19CodeDisableOptEventENS0_12AbstractCodeENS0_18SharedFunctionInfoE
	.quad	_ZN2v88internal15CodeEventLogger14CodeDeoptEventENS0_4CodeENS0_14DeoptimizeKindEmi
	.quad	_ZN2v88internal17CodeEventListener27is_listening_to_code_eventsEv
	.quad	_ZN2v88internal13PerfJitLogger17LogRecordedBufferENS0_12AbstractCodeENS0_18SharedFunctionInfoEPKci
	.quad	_ZN2v88internal13PerfJitLogger17LogRecordedBufferEPKNS0_4wasm8WasmCodeEPKci
	.section	.rodata._ZN2v88internal12_GLOBAL__N_1L24kUnknownScriptNameStringE,"a"
	.align 8
	.type	_ZN2v88internal12_GLOBAL__N_1L24kUnknownScriptNameStringE, @object
	.size	_ZN2v88internal12_GLOBAL__N_1L24kUnknownScriptNameStringE, 10
_ZN2v88internal12_GLOBAL__N_1L24kUnknownScriptNameStringE:
	.string	"<unknown>"
	.section	.rodata._ZZN2v88internal13PerfJitLogger21WriteJitCodeLoadEntryEPKhjPKciE17string_terminator,"a"
	.type	_ZZN2v88internal13PerfJitLogger21WriteJitCodeLoadEntryEPKhjPKciE17string_terminator, @object
	.size	_ZZN2v88internal13PerfJitLogger21WriteJitCodeLoadEntryEPKhjPKciE17string_terminator, 2
_ZZN2v88internal13PerfJitLogger21WriteJitCodeLoadEntryEPKhjPKciE17string_terminator:
	.zero	2
	.globl	_ZN2v88internal13PerfJitLogger19perf_output_handle_E
	.section	.bss._ZN2v88internal13PerfJitLogger19perf_output_handle_E,"aw",@nobits
	.align 8
	.type	_ZN2v88internal13PerfJitLogger19perf_output_handle_E, @object
	.size	_ZN2v88internal13PerfJitLogger19perf_output_handle_E, 8
_ZN2v88internal13PerfJitLogger19perf_output_handle_E:
	.zero	8
	.globl	_ZN2v88internal13PerfJitLogger11code_index_E
	.section	.bss._ZN2v88internal13PerfJitLogger11code_index_E,"aw",@nobits
	.align 8
	.type	_ZN2v88internal13PerfJitLogger11code_index_E, @object
	.size	_ZN2v88internal13PerfJitLogger11code_index_E, 8
_ZN2v88internal13PerfJitLogger11code_index_E:
	.zero	8
	.globl	_ZN2v88internal13PerfJitLogger15marker_address_E
	.section	.bss._ZN2v88internal13PerfJitLogger15marker_address_E,"aw",@nobits
	.align 8
	.type	_ZN2v88internal13PerfJitLogger15marker_address_E, @object
	.size	_ZN2v88internal13PerfJitLogger15marker_address_E, 8
_ZN2v88internal13PerfJitLogger15marker_address_E:
	.zero	8
	.globl	_ZN2v88internal13PerfJitLogger16reference_count_E
	.section	.bss._ZN2v88internal13PerfJitLogger16reference_count_E,"aw",@nobits
	.align 8
	.type	_ZN2v88internal13PerfJitLogger16reference_count_E, @object
	.size	_ZN2v88internal13PerfJitLogger16reference_count_E, 8
_ZN2v88internal13PerfJitLogger16reference_count_E:
	.zero	8
	.globl	_ZN2v88internal13PerfJitLogger11file_mutex_E
	.section	.bss._ZN2v88internal13PerfJitLogger11file_mutex_E,"aw",@nobits
	.align 32
	.type	_ZN2v88internal13PerfJitLogger11file_mutex_E, @object
	.size	_ZN2v88internal13PerfJitLogger11file_mutex_E, 48
_ZN2v88internal13PerfJitLogger11file_mutex_E:
	.zero	48
	.globl	_ZN2v88internal13PerfJitLogger22kFilenameBufferPaddingE
	.section	.rodata._ZN2v88internal13PerfJitLogger22kFilenameBufferPaddingE,"a"
	.align 4
	.type	_ZN2v88internal13PerfJitLogger22kFilenameBufferPaddingE, @object
	.size	_ZN2v88internal13PerfJitLogger22kFilenameBufferPaddingE, 4
_ZN2v88internal13PerfJitLogger22kFilenameBufferPaddingE:
	.long	16
	.globl	_ZN2v88internal13PerfJitLogger21kFilenameFormatStringE
	.section	.rodata._ZN2v88internal13PerfJitLogger21kFilenameFormatStringE,"a"
	.align 8
	.type	_ZN2v88internal13PerfJitLogger21kFilenameFormatStringE, @object
	.size	_ZN2v88internal13PerfJitLogger21kFilenameFormatStringE, 14
_ZN2v88internal13PerfJitLogger21kFilenameFormatStringE:
	.string	"./jit-%d.dump"
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.long	1248416836
	.long	1
	.long	40
	.long	62
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC6:
	.long	0
	.long	1083129856
	.align 8
.LC7:
	.long	0
	.long	1138753536
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
