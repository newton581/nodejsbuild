	.file	"runtime-bigint.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Runtime_Runtime_BigIntEqualToNumber"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"args[0].IsBigInt()"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE:
.LFB18353:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L46
.L16:
	movq	_ZZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic57(%rip), %rbx
	testq	%rbx, %rbx
	je	.L47
.L18:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L48
.L20:
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L24
.L25:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L47:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L49
.L19:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic57(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L24:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L25
	leaq	-8(%r12), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt13EqualToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	%r13, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	leaq	-144(%rbp), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L50
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L52
.L21:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L46:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$230, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L52:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L21
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18353:
	.size	_ZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"V8.Runtime_Runtime_BigIntEqualToString"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"args[1].IsString()"
	.section	.text._ZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE:
.LFB18356:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L87
.L54:
	movq	_ZZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic66(%rip), %rbx
	testq	%rbx, %rbx
	je	.L88
.L56:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L89
.L58:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L62
.L63:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L90
.L57:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic66(%rip)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L62:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L63
	movq	-8(%r13), %rax
	leaq	-8(%r13), %rdx
	testb	$1, %al
	jne	.L91
.L64:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L92
.L59:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L60
	movq	(%rdi), %rax
	call	*8(%rax)
.L60:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L61
	movq	(%rdi), %rax
	call	*8(%rax)
.L61:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L91:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L64
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt13EqualToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE@PLT
	movq	%r12, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L66
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L66:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L93
.L53:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L94
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	.cfi_restore_state
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$231, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L92:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L57
.L94:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18356:
	.size	_ZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL28Stats_Runtime_BigIntToNumberEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"V8.Runtime_Runtime_BigIntToNumber"
	.section	.text._ZN2v88internalL28Stats_Runtime_BigIntToNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_BigIntToNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internalL28Stats_Runtime_BigIntToNumberEiPmPNS0_7IsolateE:
.LFB18362:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L127
.L96:
	movq	_ZZN2v88internalL28Stats_Runtime_BigIntToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic82(%rip), %rbx
	testq	%rbx, %rbx
	je	.L128
.L98:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L129
.L100:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L104
.L105:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L128:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L130
.L99:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_BigIntToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic82(%rip)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L105
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L106
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L106:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L131
.L95:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L133
.L101:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L102
	movq	(%rdi), %rax
	call	*8(%rax)
.L102:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L103
	movq	(%rdi), %rax
	call	*8(%rax)
.L103:
	leaq	.LC6(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L127:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$233, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L133:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC6(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L101
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18362:
	.size	_ZN2v88internalL28Stats_Runtime_BigIntToNumberEiPmPNS0_7IsolateE, .-_ZN2v88internalL28Stats_Runtime_BigIntToNumberEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"V8.Runtime_Runtime_BigIntEqualToBigInt"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC8:
	.string	"args[1].IsBigInt()"
	.section	.text._ZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE, @function
_ZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE:
.LFB18350:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L167
.L135:
	movq	_ZZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic48(%rip), %rbx
	testq	%rbx, %rbx
	je	.L168
.L137:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L169
.L139:
	movq	0(%r13), %rdi
	testb	$1, %dil
	jne	.L143
.L144:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L168:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L170
.L138:
	movq	%rbx, _ZZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic48(%rip)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L143:
	movq	-1(%rdi), %rax
	cmpw	$66, 11(%rax)
	jne	.L144
	movq	-8(%r13), %rsi
	testb	$1, %sil
	jne	.L171
.L145:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L169:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L172
.L140:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rax
	call	*8(%rax)
.L141:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L142
	movq	(%rdi), %rax
	call	*8(%rax)
.L142:
	leaq	.LC7(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L171:
	movq	-1(%rsi), %rax
	cmpw	$66, 11(%rax)
	jne	.L145
	call	_ZN2v88internal6BigInt13EqualToBigIntES1_S1_@PLT
	movq	%r12, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	leaq	-144(%rbp), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L173
.L134:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$229, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L173:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L172:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC7(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L170:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L138
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18350:
	.size	_ZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE, .-_ZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Stats_Runtime_BigIntToBooleanEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"V8.Runtime_Runtime_BigIntToBoolean"
	.section	.text._ZN2v88internalL29Stats_Runtime_BigIntToBooleanEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_BigIntToBooleanEiPmPNS0_7IsolateE, @function
_ZN2v88internalL29Stats_Runtime_BigIntToBooleanEiPmPNS0_7IsolateE:
.LFB18359:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L206
.L176:
	movq	_ZZN2v88internalL29Stats_Runtime_BigIntToBooleanEiPmPNS0_7IsolateEE27trace_event_unique_atomic75(%rip), %rbx
	testq	%rbx, %rbx
	je	.L207
.L178:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L208
.L180:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L184
.L185:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L207:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L209
.L179:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_BigIntToBooleanEiPmPNS0_7IsolateEE27trace_event_unique_atomic75(%rip)
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-1(%rax), %rdx
	cmpw	$66, 11(%rdx)
	jne	.L185
	movl	7(%rax), %eax
	xorl	%esi, %esi
	movq	%r12, %rdi
	testl	$2147483646, %eax
	setne	%sil
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	leaq	-144(%rbp), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L210
.L175:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L211
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L212
.L181:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L182
	movq	(%rdi), %rax
	call	*8(%rax)
.L182:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L183
	movq	(%rdi), %rax
	call	*8(%rax)
.L183:
	leaq	.LC9(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L206:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$232, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L210:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L209:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L212:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L181
.L211:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18359:
	.size	_ZN2v88internalL29Stats_Runtime_BigIntToBooleanEiPmPNS0_7IsolateE, .-_ZN2v88internalL29Stats_Runtime_BigIntToBooleanEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"V8.Runtime_Runtime_BigIntCompareToBigInt"
	.section	.rodata._ZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC11:
	.string	"args[0].IsSmi()"
.LC12:
	.string	"args[2].IsBigInt()"
	.section	.text._ZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE, @function
_ZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE:
.LFB18341:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L245
.L214:
	movq	_ZZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic14(%rip), %r12
	testq	%r12, %r12
	je	.L246
.L216:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L247
	testb	$1, (%rbx)
	jne	.L248
.L222:
	movq	-8(%rbx), %rax
	leaq	-8(%rbx), %rdi
	testb	$1, %al
	jne	.L249
.L223:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L246:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L250
.L217:
	movq	%r12, _ZZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic14(%rip)
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L247:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L251
.L219:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L220
	movq	(%rdi), %rax
	call	*8(%rax)
.L220:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L221
	movq	(%rdi), %rax
	call	*8(%rax)
.L221:
	leaq	.LC10(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	testb	$1, (%rbx)
	je	.L222
.L248:
	leaq	.LC11(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L249:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L223
	movq	-16(%rbx), %rax
	leaq	-16(%rbx), %rsi
	testb	$1, %al
	jne	.L252
.L224:
	leaq	.LC12(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L252:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L224
	call	_ZN2v88internal6BigInt15CompareToBigIntENS0_6HandleIS1_EES3_@PLT
	movq	(%rbx), %rdi
	movl	%eax, %esi
	sarq	$32, %rdi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	movq	%r13, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	leaq	-144(%rbp), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L253
.L213:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L254
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$226, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L253:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L251:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L250:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L217
.L254:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18341:
	.size	_ZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE, .-_ZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Stats_Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8.Runtime_Runtime_BigIntCompareToNumber"
	.section	.text._ZN2v88internalL35Stats_Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internalL35Stats_Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateE:
.LFB18344:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L284
.L256:
	movq	_ZZN2v88internalL35Stats_Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic25(%rip), %r12
	testq	%r12, %r12
	je	.L285
.L258:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L286
	testb	$1, (%rbx)
	jne	.L287
.L264:
	movq	-8(%rbx), %rax
	leaq	-8(%rbx), %rdi
	testb	$1, %al
	jne	.L288
.L265:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L285:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L289
.L259:
	movq	%r12, _ZZN2v88internalL35Stats_Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic25(%rip)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L286:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L290
.L261:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L262
	movq	(%rdi), %rax
	call	*8(%rax)
.L262:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L263
	movq	(%rdi), %rax
	call	*8(%rax)
.L263:
	leaq	.LC13(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	testb	$1, (%rbx)
	je	.L264
.L287:
	leaq	.LC11(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L288:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L265
	leaq	-16(%rbx), %rsi
	call	_ZN2v88internal6BigInt15CompareToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	(%rbx), %rdi
	movl	%eax, %esi
	sarq	$32, %rdi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	movq	%r13, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	leaq	-144(%rbp), %rdi
	movq	(%rax), %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L291
.L255:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L292
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$227, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L291:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L290:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L289:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L259
.L292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18344:
	.size	_ZN2v88internalL35Stats_Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateE, .-_ZN2v88internalL35Stats_Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"V8.Runtime_Runtime_BigIntCompareToString"
	.section	.rodata._ZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"args[2].IsString()"
	.section	.text._ZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE:
.LFB18347:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L329
.L294:
	movq	_ZZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic36(%rip), %r13
	testq	%r13, %r13
	je	.L330
.L296:
	movq	$0, -160(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L331
.L298:
	movq	41088(%r12), %r14
	movq	41096(%r12), %r13
	addl	$1, 41104(%r12)
	testb	$1, (%rbx)
	jne	.L332
	movq	-8(%rbx), %rax
	leaq	-8(%rbx), %rsi
	testb	$1, %al
	jne	.L333
.L303:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L330:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L334
.L297:
	movq	%r13, _ZZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic36(%rip)
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L331:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L335
.L299:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L300
	movq	(%rdi), %rax
	call	*8(%rax)
.L300:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L301
	movq	(%rdi), %rax
	call	*8(%rax)
.L301:
	leaq	.LC14(%rip), %rax
	movq	%r13, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L333:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L303
	movq	-16(%rbx), %rax
	leaq	-16(%rbx), %rdx
	testb	$1, %al
	jne	.L336
.L304:
	leaq	.LC15(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L336:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L304
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt15CompareToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE@PLT
	movq	(%rbx), %rdi
	movl	%eax, %esi
	sarq	$32, %rdi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	movq	%r12, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r15
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r13
	je	.L307
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L307:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L337
.L293:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	leaq	.LC11(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L329:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$228, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L337:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L335:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L334:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L297
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18347:
	.size	_ZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE, .-_ZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL22Stats_Runtime_ToBigIntEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC16:
	.string	"V8.Runtime_Runtime_ToBigInt"
	.section	.text._ZN2v88internalL22Stats_Runtime_ToBigIntEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL22Stats_Runtime_ToBigIntEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL22Stats_Runtime_ToBigIntEiPmPNS0_7IsolateE.isra.0:
.LFB22134:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L372
.L340:
	movq	_ZZN2v88internalL22Stats_Runtime_ToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic89(%rip), %rbx
	testq	%rbx, %rbx
	je	.L373
.L342:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L374
.L344:
	addl	$1, 41104(%r12)
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L375
	movq	(%rax), %r13
.L349:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L352
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L352:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L376
.L339:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L377
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L378
.L345:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L346
	movq	(%rdi), %rax
	call	*8(%rax)
.L346:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L347
	movq	(%rdi), %rax
	call	*8(%rax)
.L347:
	leaq	.LC16(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L373:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L379
.L343:
	movq	%rbx, _ZZN2v88internalL22Stats_Runtime_ToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic89(%rip)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L375:
	movq	312(%r12), %r13
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L376:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L372:
	movq	40960(%rsi), %rax
	movl	$235, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L379:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L378:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC16(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L345
.L377:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22134:
	.size	_ZN2v88internalL22Stats_Runtime_ToBigIntEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL22Stats_Runtime_ToBigIntEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Runtime_Runtime_BigIntBinaryOp"
	.section	.rodata._ZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC18:
	.string	"args[2].IsSmi()"
.LC19:
	.string	"unreachable code"
	.section	.text._ZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE.isra.0:
.LFB22135:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L430
.L381:
	movq	_ZZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateEE27trace_event_unique_atomic96(%rip), %rbx
	testq	%rbx, %rbx
	je	.L431
.L383:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L432
.L385:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	addl	$1, 41104(%r12)
	movq	-16(%r13), %rdx
	movq	0(%r13), %rax
	testb	$1, %dl
	jne	.L433
	testb	$1, %al
	jne	.L434
.L390:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$21, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L405:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L410
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L410:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L435
.L380:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L436
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L437
.L386:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L387
	movq	(%rdi), %rax
	call	*8(%rax)
.L387:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L388
	movq	(%rdi), %rax
	call	*8(%rax)
.L388:
	leaq	.LC17(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L431:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L438
.L384:
	movq	%rbx, _ZZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateEE27trace_event_unique_atomic96(%rip)
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L434:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L390
	movq	-8(%r13), %rax
	leaq	-8(%r13), %r8
	testb	$1, %al
	je	.L390
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L390
	sarq	$32, %rdx
	cmpl	$11, %edx
	ja	.L391
	movl	%edx, %edx
	leaq	.L393(%rip), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movslq	(%rcx,%rdx,4), %rax
	movq	%r8, %rdx
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE.isra.0,"a",@progbits
	.align 4
	.align 4
.L393:
	.long	.L404-.L393
	.long	.L403-.L393
	.long	.L402-.L393
	.long	.L401-.L393
	.long	.L400-.L393
	.long	.L399-.L393
	.long	.L398-.L393
	.long	.L397-.L393
	.long	.L396-.L393
	.long	.L395-.L393
	.long	.L394-.L393
	.long	.L392-.L393
	.section	.text._ZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L433:
	leaq	.LC18(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L430:
	movq	40960(%rsi), %rax
	movl	$225, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L435:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L438:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L437:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L386
.L394:
	call	_ZN2v88internal6BigInt16SignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	.p2align 4,,10
	.p2align 3
.L406:
	testq	%rax, %rax
	je	.L439
	movq	(%rax), %r13
	jmp	.L405
.L395:
	call	_ZN2v88internal6BigInt9LeftShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L406
.L396:
	call	_ZN2v88internal6BigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L406
.L397:
	call	_ZN2v88internal6BigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L406
.L398:
	call	_ZN2v88internal6BigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L406
.L399:
	call	_ZN2v88internal6BigInt12ExponentiateEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L406
.L400:
	call	_ZN2v88internal6BigInt9RemainderEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L406
.L401:
	call	_ZN2v88internal6BigInt6DivideEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L406
.L402:
	call	_ZN2v88internal6BigInt8MultiplyEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L406
.L403:
	call	_ZN2v88internal6BigInt8SubtractEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L406
.L404:
	call	_ZN2v88internal6BigInt3AddEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L406
.L392:
	call	_ZN2v88internal6BigInt18UnsignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L406
.L439:
	movq	312(%r12), %r13
	jmp	.L405
.L436:
	call	__stack_chk_fail@PLT
.L391:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE22135:
	.size	_ZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"V8.Runtime_Runtime_BigIntUnaryOp"
	.section	.rodata._ZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC21:
	.string	"args[1].IsSmi()"
	.section	.text._ZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE.isra.0:
.LFB22136:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L484
.L441:
	movq	_ZZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateEE28trace_event_unique_atomic154(%rip), %rbx
	testq	%rbx, %rbx
	je	.L485
.L443:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L486
.L445:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L487
.L449:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L485:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L488
.L444:
	movq	%rbx, _ZZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateEE28trace_event_unique_atomic154(%rip)
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L487:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L449
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L489
	sarq	$32, %rax
	cmpq	$14, %rax
	je	.L452
	cmpl	$14, %eax
	jg	.L453
	cmpl	$12, %eax
	jne	.L490
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt10BitwiseNotEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
.L458:
	testq	%rax, %rax
	je	.L491
.L459:
	movq	(%rax), %r13
.L460:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L463
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L463:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L492
.L440:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L493
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	cmpl	$13, %eax
	jne	.L456
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt10UnaryMinusEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	jne	.L459
.L491:
	movq	312(%r12), %r13
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L453:
	cmpl	$15, %eax
	jne	.L456
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt9DecrementEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L486:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L494
.L446:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L447
	movq	(%rdi), %rax
	call	*8(%rax)
.L447:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L448
	movq	(%rdi), %rax
	call	*8(%rax)
.L448:
	leaq	.LC20(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L452:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt9IncrementEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L484:
	movq	40960(%rsi), %rax
	movl	$234, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L489:
	leaq	.LC21(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L492:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L488:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L494:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC20(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L446
.L456:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L493:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22136:
	.size	_ZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal29Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE:
.LFB18342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %r12
	testl	%eax, %eax
	jne	.L505
	testb	$1, (%rsi)
	jne	.L506
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdi
	testb	$1, %al
	jne	.L507
.L498:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L507:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L498
	movq	-16(%r12), %rax
	leaq	-16(%rsi), %rsi
	testb	$1, %al
	jne	.L508
.L499:
	leaq	.LC12(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L508:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L499
	call	_ZN2v88internal6BigInt15CompareToBigIntENS0_6HandleIS1_EES3_@PLT
	movq	(%r12), %rdi
	movl	%eax, %esi
	sarq	$32, %rdi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	movq	%r13, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	popq	%r12
	popq	%r13
	movq	(%rax), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L505:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L506:
	.cfi_restore_state
	leaq	.LC11(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18342:
	.size	_ZN2v88internal29Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateE:
.LFB18345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %r12
	testl	%eax, %eax
	jne	.L516
	testb	$1, (%rsi)
	jne	.L517
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdi
	testb	$1, %al
	jne	.L518
.L512:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L518:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L512
	leaq	-16(%rsi), %rsi
	call	_ZN2v88internal6BigInt15CompareToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	(%r12), %rdi
	movl	%eax, %esi
	sarq	$32, %rdi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	movq	%r13, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	popq	%r12
	popq	%r13
	movq	(%rax), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L516:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL35Stats_Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	leaq	.LC11(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18345:
	.size	_ZN2v88internal29Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE:
.LFB18348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L529
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, (%rsi)
	jne	.L530
	movq	-8(%r13), %rax
	leaq	-8(%rsi), %rsi
	testb	$1, %al
	jne	.L531
.L522:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L531:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L522
	movq	-16(%r13), %rax
	leaq	-16(%r13), %rdx
	testb	$1, %al
	jne	.L532
.L523:
	leaq	.LC15(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L532:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L523
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt15CompareToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE@PLT
	movq	0(%r13), %rdi
	movl	%eax, %esi
	sarq	$32, %rdi
	call	_ZN2v88internal22ComparisonResultToBoolENS0_9OperationENS0_16ComparisonResultE@PLT
	movq	%r12, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r13
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L519
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L519:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L530:
	.cfi_restore_state
	leaq	.LC11(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18348:
	.size	_ZN2v88internal29Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_BigIntCompareToStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE:
.LFB18351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L540
	movq	(%rsi), %rdi
	testb	$1, %dil
	jne	.L535
.L536:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L535:
	movq	-1(%rdi), %rax
	cmpw	$66, 11(%rax)
	jne	.L536
	movq	-8(%rsi), %rsi
	testb	$1, %sil
	jne	.L541
.L537:
	leaq	.LC8(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L541:
	movq	-1(%rsi), %rax
	cmpw	$66, 11(%rax)
	jne	.L537
	call	_ZN2v88internal6BigInt13EqualToBigIntES1_S1_@PLT
	movq	%r12, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L540:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18351:
	.size	_ZN2v88internal27Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE:
.LFB18354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edi, %r8d
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	subq	$8, %rsp
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L547
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L544
.L545:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L544:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L545
	leaq	-8(%rsi), %rsi
	call	_ZN2v88internal6BigInt13EqualToNumberENS0_6HandleIS1_EENS2_INS0_6ObjectEEE@PLT
	movq	%r12, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	.cfi_restore_state
	addq	$8, %rsp
	movl	%r8d, %edi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18354:
	.size	_ZN2v88internal27Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE:
.LFB18357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L556
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L550
.L551:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L550:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L551
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %rdx
	testb	$1, %al
	jne	.L557
.L552:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L557:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L552
	movq	%r12, %rdi
	call	_ZN2v88internal6BigInt13EqualToStringEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6StringEEE@PLT
	movq	%r12, %rdi
	movzbl	%al, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L548
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L548:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L556:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18357:
	.size	_ZN2v88internal27Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_BigIntEqualToStringEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal23Runtime_BigIntToBooleanEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_BigIntToBooleanEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_BigIntToBooleanEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_BigIntToBooleanEiPmPNS0_7IsolateE:
.LFB18360:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movl	%edi, %r8d
	movq	%rdx, %rdi
	testl	%eax, %eax
	jne	.L564
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testb	$1, %al
	jne	.L560
.L561:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L560:
	movq	-1(%rax), %rdx
	cmpw	$66, 11(%rdx)
	jne	.L561
	movl	7(%rax), %eax
	xorl	%esi, %esi
	testl	$2147483646, %eax
	setne	%sil
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore 6
	movl	%r8d, %edi
	jmp	_ZN2v88internalL29Stats_Runtime_BigIntToBooleanEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18360:
	.size	_ZN2v88internal23Runtime_BigIntToBooleanEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_BigIntToBooleanEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_BigIntToNumberEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_BigIntToNumberEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_BigIntToNumberEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_BigIntToNumberEiPmPNS0_7IsolateE:
.LFB18363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L571
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L567
.L568:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L567:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L568
	movq	%rdx, %rdi
	call	_ZN2v88internal6BigInt8ToNumberEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rax), %r14
	movq	%r13, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L565
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L565:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L571:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_BigIntToNumberEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18363:
	.size	_ZN2v88internal22Runtime_BigIntToNumberEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_BigIntToNumberEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal16Runtime_ToBigIntEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal16Runtime_ToBigIntEiPmPNS0_7IsolateE
	.type	_ZN2v88internal16Runtime_ToBigIntEiPmPNS0_7IsolateE, @function
_ZN2v88internal16Runtime_ToBigIntEiPmPNS0_7IsolateE:
.LFB18366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L579
	addl	$1, 41104(%rdx)
	movq	%rdx, %rdi
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	call	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L580
	movq	(%rax), %r14
.L575:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L572
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L572:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L579:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL22Stats_Runtime_ToBigIntEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18366:
	.size	_ZN2v88internal16Runtime_ToBigIntEiPmPNS0_7IsolateE, .-_ZN2v88internal16Runtime_ToBigIntEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal22Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal22Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE
	.type	_ZN2v88internal22Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE, @function
_ZN2v88internal22Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE:
.LFB18369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L605
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	movq	-16(%rsi), %rdx
	movq	(%rsi), %rax
	testb	$1, %dl
	jne	.L606
	testb	$1, %al
	jne	.L607
.L584:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$21, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L599:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L581
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L581:
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L607:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L584
	movq	-8(%rsi), %rax
	leaq	-8(%rsi), %r8
	testb	$1, %al
	je	.L584
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L584
	sarq	$32, %rdx
	cmpl	$11, %edx
	ja	.L585
	movl	%edx, %edx
	leaq	.L587(%rip), %rcx
	movq	%r12, %rdi
	movslq	(%rcx,%rdx,4), %rax
	movq	%r8, %rdx
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal22Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE,"a",@progbits
	.align 4
	.align 4
.L587:
	.long	.L598-.L587
	.long	.L597-.L587
	.long	.L596-.L587
	.long	.L595-.L587
	.long	.L594-.L587
	.long	.L593-.L587
	.long	.L592-.L587
	.long	.L591-.L587
	.long	.L590-.L587
	.long	.L589-.L587
	.long	.L588-.L587
	.long	.L586-.L587
	.section	.text._ZN2v88internal22Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE
	.p2align 4,,10
	.p2align 3
.L605:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L606:
	.cfi_restore_state
	leaq	.LC18(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L588:
	call	_ZN2v88internal6BigInt16SignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	.p2align 4,,10
	.p2align 3
.L600:
	testq	%rax, %rax
	je	.L608
	movq	(%rax), %r13
	jmp	.L599
.L589:
	call	_ZN2v88internal6BigInt9LeftShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L600
.L591:
	call	_ZN2v88internal6BigInt9BitwiseOrEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L600
.L592:
	call	_ZN2v88internal6BigInt10BitwiseAndEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L600
.L590:
	call	_ZN2v88internal6BigInt10BitwiseXorEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L600
.L593:
	call	_ZN2v88internal6BigInt12ExponentiateEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L600
.L594:
	call	_ZN2v88internal6BigInt9RemainderEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L600
.L595:
	call	_ZN2v88internal6BigInt6DivideEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L600
.L596:
	call	_ZN2v88internal6BigInt8MultiplyEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L600
.L597:
	call	_ZN2v88internal6BigInt8SubtractEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L600
.L598:
	call	_ZN2v88internal6BigInt3AddEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L600
.L586:
	call	_ZN2v88internal6BigInt18UnsignedRightShiftEPNS0_7IsolateENS0_6HandleIS1_EES5_@PLT
	jmp	.L600
.L608:
	movq	312(%r12), %r13
	jmp	.L599
.L585:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18369:
	.size	_ZN2v88internal22Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE, .-_ZN2v88internal22Runtime_BigIntBinaryOpEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE:
.LFB18375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L627
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	testb	$1, %al
	jne	.L628
.L611:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L628:
	movq	-1(%rax), %rax
	cmpw	$66, 11(%rax)
	jne	.L611
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L629
	sarq	$32, %rax
	cmpq	$14, %rax
	je	.L614
	cmpl	$14, %eax
	jg	.L615
	cmpl	$12, %eax
	jne	.L630
	movq	%rdx, %rdi
	call	_ZN2v88internal6BigInt10BitwiseNotEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
.L620:
	testq	%rax, %rax
	je	.L631
.L621:
	movq	(%rax), %r14
.L622:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L609
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L609:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L630:
	.cfi_restore_state
	cmpl	$13, %eax
	jne	.L618
	movq	%rdx, %rdi
	call	_ZN2v88internal6BigInt10UnaryMinusEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	jne	.L621
.L631:
	movq	312(%r12), %r14
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L615:
	cmpl	$15, %eax
	jne	.L618
	movq	%rdx, %rdi
	call	_ZN2v88internal6BigInt9DecrementEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L614:
	movq	%rdx, %rdi
	call	_ZN2v88internal6BigInt9IncrementEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L627:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%r8, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE.isra.0
	.p2align 4,,10
	.p2align 3
.L629:
	.cfi_restore_state
	leaq	.LC21(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L618:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18375:
	.size	_ZN2v88internal21Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE, .-_ZN2v88internal21Runtime_BigIntUnaryOpEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal29Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal29Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal29Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE:
.LFB22118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22118:
	.size	_GLOBAL__sub_I__ZN2v88internal29Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal29Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal29Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateEE28trace_event_unique_atomic154,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateEE28trace_event_unique_atomic154, @object
	.size	_ZZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateEE28trace_event_unique_atomic154, 8
_ZZN2v88internalL27Stats_Runtime_BigIntUnaryOpEiPmPNS0_7IsolateEE28trace_event_unique_atomic154:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateEE27trace_event_unique_atomic96,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateEE27trace_event_unique_atomic96, @object
	.size	_ZZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateEE27trace_event_unique_atomic96, 8
_ZZN2v88internalL28Stats_Runtime_BigIntBinaryOpEiPmPNS0_7IsolateEE27trace_event_unique_atomic96:
	.zero	8
	.section	.bss._ZZN2v88internalL22Stats_Runtime_ToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic89,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL22Stats_Runtime_ToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic89, @object
	.size	_ZZN2v88internalL22Stats_Runtime_ToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic89, 8
_ZZN2v88internalL22Stats_Runtime_ToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic89:
	.zero	8
	.section	.bss._ZZN2v88internalL28Stats_Runtime_BigIntToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic82,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL28Stats_Runtime_BigIntToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic82, @object
	.size	_ZZN2v88internalL28Stats_Runtime_BigIntToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic82, 8
_ZZN2v88internalL28Stats_Runtime_BigIntToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic82:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_BigIntToBooleanEiPmPNS0_7IsolateEE27trace_event_unique_atomic75,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_BigIntToBooleanEiPmPNS0_7IsolateEE27trace_event_unique_atomic75, @object
	.size	_ZZN2v88internalL29Stats_Runtime_BigIntToBooleanEiPmPNS0_7IsolateEE27trace_event_unique_atomic75, 8
_ZZN2v88internalL29Stats_Runtime_BigIntToBooleanEiPmPNS0_7IsolateEE27trace_event_unique_atomic75:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic66,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic66, @object
	.size	_ZZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic66, 8
_ZZN2v88internalL33Stats_Runtime_BigIntEqualToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic66:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic57,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic57, @object
	.size	_ZZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic57, 8
_ZZN2v88internalL33Stats_Runtime_BigIntEqualToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic57:
	.zero	8
	.section	.bss._ZZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic48,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic48, @object
	.size	_ZZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic48, 8
_ZZN2v88internalL33Stats_Runtime_BigIntEqualToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic48:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic36,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic36, @object
	.size	_ZZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic36, 8
_ZZN2v88internalL35Stats_Runtime_BigIntCompareToStringEiPmPNS0_7IsolateEE27trace_event_unique_atomic36:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic25,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic25, @object
	.size	_ZZN2v88internalL35Stats_Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic25, 8
_ZZN2v88internalL35Stats_Runtime_BigIntCompareToNumberEiPmPNS0_7IsolateEE27trace_event_unique_atomic25:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic14,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic14, @object
	.size	_ZZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic14, 8
_ZZN2v88internalL35Stats_Runtime_BigIntCompareToBigIntEiPmPNS0_7IsolateEE27trace_event_unique_atomic14:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
