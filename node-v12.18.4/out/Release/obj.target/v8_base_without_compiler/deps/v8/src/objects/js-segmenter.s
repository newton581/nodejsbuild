	.file	"js-segmenter.cc"
	.text
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB22567:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE22567:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB22568:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L4
	cmpl	$3, %edx
	je	.L5
	cmpl	$1, %edx
	je	.L9
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22568:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB22683:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22683:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB22686:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L11
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE22686:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB22688:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE22688:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB22687:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE22687:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB22685:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22685:
	.size	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv,"axG",@progbits,_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv,comdat
	.p2align 4
	.weak	_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv
	.type	_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv, @function
_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv:
.LFB22093:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L16
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L19
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L20
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L21:
	cmpl	$1, %eax
	je	.L28
.L19:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L23
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L24:
	cmpl	$1, %eax
	jne	.L19
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L23:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L24
	.cfi_endproc
.LFE22093:
	.size	_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv, .-_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv
	.section	.rodata._ZN2v88internal11JSSegmenter14GetGranularityEPKc.str1.1,"aMS",@progbits,1
.LC0:
	.string	"grapheme"
.LC1:
	.string	"word"
.LC2:
	.string	"sentence"
.LC3:
	.string	"unreachable code"
	.section	.text._ZN2v88internal11JSSegmenter14GetGranularityEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11JSSegmenter14GetGranularityEPKc
	.type	_ZN2v88internal11JSSegmenter14GetGranularityEPKc, @function
_ZN2v88internal11JSSegmenter14GetGranularityEPKc:
.LFB18163:
	.cfi_startproc
	endbr64
	movq	%rdi, %rdx
	movl	$9, %ecx
	leaq	.LC0(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	testl	%eax, %eax
	je	.L29
	movl	$5, %ecx
	leaq	.LC1(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L31
	movl	$9, %ecx
	leaq	.LC2(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L39
	movl	$2, %eax
.L29:
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$1, %eax
	ret
.L39:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18163:
	.size	_ZN2v88internal11JSSegmenter14GetGranularityEPKc, .-_ZN2v88internal11JSSegmenter14GetGranularityEPKc
	.section	.text._ZNK2v88internal11JSSegmenter19GranularityAsStringEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11JSSegmenter19GranularityAsStringEv
	.type	_ZNK2v88internal11JSSegmenter19GranularityAsStringEv, @function
_ZNK2v88internal11JSSegmenter19GranularityAsStringEv:
.LFB18245:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movslq	43(%rdx), %rax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L41
	cmpl	$2, %eax
	je	.L42
	testl	%eax, %eax
	je	.L47
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$34000, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$36144, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	andq	$-262144, %rdx
	movq	24(%rdx), %rax
	subq	$34336, %rax
	ret
	.cfi_endproc
.LFE18245:
	.size	_ZNK2v88internal11JSSegmenter19GranularityAsStringEv, .-_ZNK2v88internal11JSSegmenter19GranularityAsStringEv
	.section	.text._ZN2v88internal11JSSegmenter15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11JSSegmenter15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE
	.type	_ZN2v88internal11JSSegmenter15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE, @function
_ZN2v88internal11JSSegmenter15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE:
.LFB18244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rax
	movq	879(%rax), %r13
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L49
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L50:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	23(%rax), %rsi
	testq	%rdi, %rdi
	je	.L52
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L53:
	xorl	%r8d, %r8d
	leaq	1592(%r12), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	(%rbx), %rax
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZNK2v88internal11JSSegmenter19GranularityAsStringEv
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	leaq	1440(%r12), %rdx
	call	_ZN2v88internal8JSObject11AddPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEENS4_INS0_6ObjectEEENS0_18PropertyAttributesE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L58
.L51:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L52:
	movq	41088(%r12), %rcx
	cmpq	41096(%r12), %rcx
	je	.L59
.L54:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rcx)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L54
.L57:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18244:
	.size	_ZN2v88internal11JSSegmenter15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE, .-_ZN2v88internal11JSSegmenter15ResolvedOptionsEPNS0_7IsolateENS0_6HandleIS1_EE
	.section	.text._ZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11Ev
	.type	_ZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11Ev, @function
_ZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11Ev:
.LFB18246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movzbl	_ZZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %eax
	cmpb	$2, %al
	jne	.L71
.L61:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	addq	$56, %rsp
	leaq	16+_ZZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	leaq	-64(%rbp), %r12
	movq	%rax, -56(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L61
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L61
.L72:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18246:
	.size	_ZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11Ev, .-_ZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB21351:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L88
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L77:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L75
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L73
.L76:
	movq	%rbx, %r12
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L76
.L73:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE21351:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv:
.LFB22119:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	16(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE(%rip), %rax
	movl	$0, 16(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 24(%rdi)
	movq	%r13, 32(%rdi)
	movq	%r13, 40(%rdi)
	movq	$0, 48(%rdi)
	leaq	-116(%rbp), %rdi
	movl	$0, -116(%rbp)
	call	_ZN6icu_6713BreakIterator19getAvailableLocalesERi@PLT
	movl	-116(%rbp), %edx
	leaq	-112(%rbp), %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	call	_ZN2v88internal4Intl14BuildLocaleSetB5cxx11EPKN6icu_676LocaleEiPKcS7_@PLT
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L92
	leaq	8(%rbx), %r15
.L95:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r14
	cmpq	%rax, %rdi
	je	.L93
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L92
.L94:
	movq	%r14, %r12
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L94
.L92:
	movq	-96(%rbp), %rax
	movq	$0, 24(%rbx)
	movq	%r13, 32(%rbx)
	movq	%r13, 40(%rbx)
	movq	$0, 48(%rbx)
	testq	%rax, %rax
	je	.L91
	movl	-104(%rbp), %edx
	movq	%rax, 24(%rbx)
	movl	%edx, 16(%rbx)
	movq	-88(%rbp), %rdx
	movq	%rdx, 32(%rbx)
	movq	-80(%rbp), %rdx
	movq	%rdx, 40(%rbx)
	movq	%r13, 8(%rax)
	movq	-72(%rbp), %rax
	movq	%rax, 48(%rbx)
.L91:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L111:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22119:
	.size	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv
	.section	.text._ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev,"axG",@progbits,_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev
	.type	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev, @function
_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev:
.LFB22690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L112
	leaq	8(%rdi), %r13
.L116:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L114
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L112
.L115:
	movq	%rbx, %r12
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L114:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L115
.L112:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE22690:
	.size	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev, .-_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev
	.weak	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED1Ev
	.set	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED1Ev,_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED2Ev
	.section	.text._ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev,"axG",@progbits,_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev
	.type	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev, @function
_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev:
.LFB22692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	24(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L128
	leaq	8(%rdi), %r14
.L131:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L129
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L128
.L130:
	movq	%rbx, %r12
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L130
.L128:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$56, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22692:
	.size	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev, .-_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB21353:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L158
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L147:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L144
	call	_ZdlPv@PLT
.L144:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L145
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L142
.L146:
	movq	%rbx, %r12
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L146
.L142:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE21353:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E:
.LFB22342:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rsi)
	movq	$0, (%rdi)
	je	.L164
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	$0, (%rbx)
	movabsq	$4294967297, %rcx
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE22342:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11JSSegmenter14GetGranularityEPKc,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11JSSegmenter14GetGranularityEPKc, @function
_GLOBAL__sub_I__ZN2v88internal11JSSegmenter14GetGranularityEPKc:
.LFB22709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22709:
	.size	_GLOBAL__sub_I__ZN2v88internal11JSSegmenter14GetGranularityEPKc, .-_GLOBAL__sub_I__ZN2v88internal11JSSegmenter14GetGranularityEPKc
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11JSSegmenter14GetGranularityEPKc
	.section	.rodata._ZN2v88internal11JSSegmenter3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZN2v88internal11JSSegmenter3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.1,"aMS",@progbits,1
.LC5:
	.string	"Intl.Segmenter"
.LC6:
	.string	"(location_) != nullptr"
.LC7:
	.string	"Check failed: %s."
.LC8:
	.string	"granularity"
.LC9:
	.string	"U_SUCCESS(status)"
	.section	.rodata._ZN2v88internal11JSSegmenter3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_.str1.8
	.align 8
.LC10:
	.string	"(icu_break_iterator.get()) != nullptr"
	.section	.text._ZN2v88internal11JSSegmenter3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11JSSegmenter3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_
	.type	_ZN2v88internal11JSSegmenter3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_, @function
_ZN2v88internal11JSSegmenter3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_:
.LFB18164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-688(%rbp), %rdi
	pushq	%rbx
	subq	$744, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -776(%rbp)
	movq	%r12, %rsi
	movq	%rcx, -752(%rbp)
	xorl	%ecx, %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4Intl22CanonicalizeLocaleListB5cxx11EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEb@PLT
	cmpb	$0, -688(%rbp)
	je	.L171
	movq	-672(%rbp), %rbx
	pxor	%xmm0, %xmm0
	movq	-680(%rbp), %r9
	movq	$0, -704(%rbp)
	movaps	%xmm0, -720(%rbp)
	movq	%rbx, %r13
	subq	%r9, %r13
	movq	%r13, %rax
	sarq	$5, %rax
	je	.L334
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L335
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r9
	movq	%rax, %r14
.L173:
	movq	%r14, %xmm0
	addq	%r14, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, -704(%rbp)
	movaps	%xmm0, -720(%rbp)
	cmpq	%rbx, %r9
	je	.L175
	movq	%r9, %r15
	leaq	-656(%rbp), %r13
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L177:
	cmpq	$1, %r8
	jne	.L179
	movzbl	(%r10), %eax
	movb	%al, 16(%r14)
.L180:
	addq	$32, %r15
	movq	%r8, 8(%r14)
	addq	$32, %r14
	movb	$0, (%rdi,%r8)
	cmpq	%r15, %rbx
	je	.L175
.L181:
	leaq	16(%r14), %rdi
	movq	%rdi, (%r14)
	movq	(%r15), %r10
	movq	8(%r15), %r8
	movq	%r10, %rax
	addq	%r8, %rax
	je	.L176
	testq	%r10, %r10
	je	.L336
.L176:
	movq	%r8, -656(%rbp)
	cmpq	$15, %r8
	jbe	.L177
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r8, -768(%rbp)
	movq	%r10, -744(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-744(%rbp), %r10
	movq	-768(%rbp), %r8
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	-656(%rbp), %rax
	movq	%rax, 16(%r14)
.L178:
	movq	%r8, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-656(%rbp), %r8
	movq	(%r14), %rdi
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L185:
	xorl	%r14d, %r14d
.L188:
	movq	-712(%rbp), %rbx
	movq	-720(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L246
	.p2align 4,,10
	.p2align 3
.L250:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L247
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L250
.L248:
	movq	-720(%rbp), %r12
.L246:
	testq	%r12, %r12
	je	.L171
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L171:
	movq	-672(%rbp), %rbx
	movq	-680(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L252
	.p2align 4,,10
	.p2align 3
.L256:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L253
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L256
.L254:
	movq	-680(%rbp), %r12
.L252:
	testq	%r12, %r12
	je	.L257
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L257:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L337
	addq	$744, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L256
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L179:
	testq	%r8, %r8
	je	.L180
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L175:
	movq	-752(%rbp), %r13
	movq	%r14, -712(%rbp)
	movq	0(%r13), %rax
	cmpq	88(%r12), %rax
	je	.L338
	testb	$1, %al
	jne	.L184
.L187:
	movq	-752(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object12ToObjectImplEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L185
.L183:
	leaq	.LC5(%rip), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal4Intl16GetLocaleMatcherEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKc@PLT
	testb	%al, %al
	je	.L185
	sarq	$32, %rax
	movl	$0, -648(%rbp)
	leaq	-368(%rbp), %r10
	movq	$0, -640(%rbp)
	movq	%rax, %rbx
	leaq	-648(%rbp), %rax
	movq	%rax, -632(%rbp)
	movq	%rax, -624(%rbp)
	movq	$0, -616(%rbp)
	movzbl	_ZZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %eax
	cmpb	$2, %al
	jne	.L339
.L189:
	leaq	-656(%rbp), %r15
	movl	%ebx, %r8d
	movq	%r12, %rsi
	movq	%r10, %rdi
	leaq	-720(%rbp), %rcx
	movq	%r15, %r9
	leaq	16+_ZZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdx
	call	_ZN2v88internal4Intl13ResolveLocaleEPNS0_7IsolateERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISA_ESaISA_EERKSt6vectorISA_SD_ENS1_13MatcherOptionESG_@PLT
	movq	-640(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L195
.L191:
	movq	24(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%rbx), %rdi
	leaq	48(%rbx), %rdx
	movq	16(%rbx), %r14
	cmpq	%rdx, %rdi
	je	.L194
	call	_ZdlPv@PLT
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	je	.L195
.L196:
	movq	%r14, %rbx
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	testq	%r14, %r14
	jne	.L196
.L195:
	movq	-368(%rbp), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rbx, -656(%rbp)
	movq	%rax, -648(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, -744(%rbp)
	testq	%rax, %rax
	je	.L340
	movabsq	$4294967296, %rax
	movl	$12, %edi
	movl	$2, -596(%rbp)
	movq	%rax, -604(%rbp)
	call	_Znwm@PLT
	leaq	.LC0(%rip), %rcx
	movl	$24, %edi
	movq	%rax, %r14
	movq	-604(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rax, (%r14)
	movl	-596(%rbp), %eax
	movl	%eax, 8(%r14)
	leaq	.LC1(%rip), %rax
	movq	%rax, %xmm1
	leaq	.LC2(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, -576(%rbp)
	movaps	%xmm0, -592(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movdqa	-592(%rbp), %xmm2
	movq	%rax, %rbx
	movaps	%xmm0, -656(%rbp)
	movups	%xmm2, (%rax)
	movq	-576(%rbp), %rax
	movq	$0, -728(%rbp)
	movq	%rax, 16(%rbx)
	movq	$0, -640(%rbp)
	call	_Znwm@PLT
	movq	16(%rbx), %rcx
	movdqu	(%rbx), %xmm3
	movq	%r13, %rsi
	leaq	24(%rax), %rdx
	movq	%rax, -656(%rbp)
	movq	%r12, %rdi
	leaq	.LC5(%rip), %r8
	movq	%rcx, 16(%rax)
	movq	%r15, %rcx
	movups	%xmm3, (%rax)
	leaq	-728(%rbp), %rax
	movq	%rdx, -640(%rbp)
	movq	%rax, %r9
	movq	%rdx, -648(%rbp)
	leaq	.LC8(%rip), %rdx
	movq	%rax, -768(%rbp)
	call	_ZN2v88internal4Intl15GetStringOptionEPNS0_7IsolateENS0_6HandleINS0_10JSReceiverEEEPKcSt6vectorIS8_SaIS8_EES8_PSt10unique_ptrIA_cSt14default_deleteISD_EE@PLT
	movq	-656(%rbp), %rdi
	movl	%eax, %r13d
	testq	%rdi, %rdi
	je	.L197
	movb	%al, -752(%rbp)
	call	_ZdlPv@PLT
	movzbl	-752(%rbp), %eax
.L197:
	movq	-728(%rbp), %rdi
	testb	%al, %al
	je	.L198
	shrw	$8, %r13w
	jne	.L341
	testq	%rdi, %rdi
	je	.L333
	call	_ZdaPv@PLT
.L333:
	movq	%rbx, %rdi
	leaq	-336(%rbp), %r13
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	leaq	-592(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -784(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	movl	$0, -732(%rbp)
.L205:
	movq	-784(%rbp), %rdi
	leaq	-732(%rbp), %rsi
	call	_ZN6icu_6713BreakIterator23createCharacterInstanceERKNS_6LocaleER10UErrorCode@PLT
	movl	$0, -752(%rbp)
	movq	%rax, %r14
.L210:
	movl	-732(%rbp), %eax
	testl	%eax, %eax
	jg	.L342
	testq	%r14, %r14
	je	.L209
	movq	-768(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r14, -728(%rbp)
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN6icu_6713BreakIteratorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	movq	-656(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L213
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L214
	lock addl	$1, 8(%rbx)
	movq	-656(%rbp), %r15
	testq	%r15, %r15
	je	.L213
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
.L217:
	cmpl	$1, %eax
	je	.L343
.L213:
	movq	32(%r12), %rax
	subq	48(%r12), %rax
	cmpq	$33554432, %rax
	jg	.L344
.L220:
	movq	%rbx, %xmm5
	movq	%r14, %xmm0
	movl	$16, %edi
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -768(%rbp)
	call	_Znwm@PLT
	movdqa	-768(%rbp), %xmm0
	movq	%rax, %r15
	movups	%xmm0, (%rax)
	testq	%rbx, %rbx
	je	.L221
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L222
	lock addl	$1, 8(%rbx)
.L221:
	movl	$48, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %r8
	movups	%xmm0, 8(%rax)
	movq	%r15, 24(%rax)
	movq	%r8, %rsi
	movq	$0, (%rax)
	leaq	_ZN2v88internal7ManagedIN6icu_6713BreakIteratorEE10DestructorEPv(%rip), %rax
	movq	%rax, 32(%r8)
	movq	$0, 40(%r8)
	movq	%r8, -768(%rbp)
	call	_ZN2v88internal7Factory10NewForeignEmNS0_14AllocationTypeE@PLT
	movq	41152(%r12), %rdi
	movq	(%rax), %rsi
	movq	%rax, %r15
	call	_ZN2v88internal13GlobalHandles6CreateENS0_6ObjectE@PLT
	movq	-768(%rbp), %r8
	movq	_ZN2v88internal22ManagedObjectFinalizerERKNS_16WeakCallbackInfoIvEE@GOTPCREL(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	%rax, 40(%r8)
	movq	%r8, %rsi
	call	_ZN2v88internal13GlobalHandles8MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	-768(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal7Isolate28RegisterManagedPtrDestructorEPNS0_20ManagedPtrDestructorE@PLT
	testq	%rbx, %rbx
	je	.L224
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L225
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rbx)
.L226:
	cmpl	$1, %eax
	je	.L345
.L224:
	movq	-728(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L230
	movq	(%rdi), %rax
	call	*8(%rax)
.L230:
	movq	-776(%rbp), %rsi
	movq	(%rsi), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L231
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory22NewSlowJSObjectFromMapENS0_6HandleINS0_3MapEEEiNS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r14
.L232:
	movq	(%r14), %rax
	movq	$0, 39(%rax)
	movq	-744(%rbp), %rax
	movq	(%r14), %rdi
	movq	(%rax), %r12
	leaq	23(%rdi), %rsi
	movq	%r12, 23(%rdi)
	testb	$1, %r12b
	je	.L259
	movq	%r12, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -744(%rbp)
	testl	$262144, %eax
	je	.L234
	movq	%r12, %rdx
	movq	%rsi, -776(%rbp)
	movq	%rdi, -768(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-744(%rbp), %rcx
	movq	-776(%rbp), %rsi
	movq	-768(%rbp), %rdi
	movq	8(%rcx), %rax
.L234:
	testb	$24, %al
	je	.L259
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L259
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L259:
	movq	(%r14), %rdx
	movslq	43(%rdx), %rax
	andl	$-4, %eax
	orl	-752(%rbp), %eax
	salq	$32, %rax
	movq	%rax, 39(%rdx)
	movq	(%r14), %rdi
	movq	(%r15), %r12
	movq	%r12, 31(%rdi)
	leaq	31(%rdi), %rsi
	testb	$1, %r12b
	je	.L258
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	je	.L237
	movq	%r12, %rdx
	movq	%rsi, -768(%rbp)
	movq	%rdi, -744(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-768(%rbp), %rsi
	movq	-744(%rbp), %rdi
.L237:
	testb	$24, %al
	je	.L258
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L258
	movq	%r12, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L258:
	movq	-784(%rbp), %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L247:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L250
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L334:
	xorl	%r14d, %r14d
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L339:
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS3_17SkipResourceCheckEEENS0_32StaticallyAllocatedInstanceTraitIS8_EENS0_21DefaultConstructTraitIS8_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS8_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%r10, %rsi
	movq	%r10, -744(%rbp)
	movq	%rax, -368(%rbp)
	leaq	8+_ZZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rax
	movq	%rcx, %xmm0
	leaq	_ZZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11EvE17available_locales(%rip), %rdi
	movq	%rax, -360(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -352(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-352(%rbp), %rax
	movq	-744(%rbp), %r10
	testq	%rax, %rax
	je	.L189
	movq	%r10, %rsi
	movq	%r10, %rdi
	movl	$3, %edx
	call	*%rax
	movq	-744(%rbp), %r10
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L338:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	%rax, %r13
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L187
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L198:
	testq	%rdi, %rdi
	jne	.L206
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L260:
	xorl	%r14d, %r14d
	leaq	-336(%rbp), %r13
.L207:
	movq	-96(%rbp), %r12
	leaq	-112(%rbp), %r15
	testq	%r12, %r12
	je	.L244
.L239:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L242
	call	_ZdlPv@PLT
.L242:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L243
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L244
.L245:
	movq	%rbx, %r12
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L245
.L244:
	movq	%r13, %rdi
	call	_ZN6icu_676LocaleD1Ev@PLT
	movq	-368(%rbp), %rdi
	leaq	-352(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L188
	call	_ZdlPv@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L341:
	movq	(%rbx), %rsi
	movq	%rdi, -752(%rbp)
	call	strcmp@PLT
	movq	-752(%rbp), %rdi
	testl	%eax, %eax
	je	.L262
	movq	8(%rbx), %rsi
	call	strcmp@PLT
	movq	-752(%rbp), %rdi
	testl	%eax, %eax
	je	.L263
	movq	16(%rbx), %rsi
	call	strcmp@PLT
	movq	-752(%rbp), %rdi
	testl	%eax, %eax
	je	.L346
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L340:
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L231:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory18NewJSObjectFromMapENS0_6HandleINS0_3MapEEENS0_14AllocationTypeENS2_INS0_14AllocationSiteEEE@PLT
	movq	%rax, %r14
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L206:
	call	_ZdaPv@PLT
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L344:
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L222:
	addl	$1, 8(%rbx)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L225:
	movl	8(%rbx), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%rbx)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L214:
	addl	$1, 8(%rbx)
	movl	8(%rbx), %eax
	movq	%rbx, %r15
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%rbx)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L345:
	movq	(%rbx), %rax
	movq	%rdx, -768(%rbp)
	movq	%rbx, %rdi
	call	*16(%rax)
	movq	-768(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L228
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rbx)
.L229:
	cmpl	$1, %eax
	jne	.L224
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L343:
	movq	(%r15), %rax
	movq	%rdx, -768(%rbp)
	movq	%r15, %rdi
	call	*16(%rax)
	movq	-768(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L218
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L219:
	cmpl	$1, %eax
	jne	.L213
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L213
.L342:
	leaq	.LC9(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L348:
	testl	%eax, %eax
	je	.L205
.L209:
	leaq	.LC10(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L228:
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L229
.L218:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L219
.L262:
	xorl	%eax, %eax
.L200:
	movl	(%r14,%rax,4), %eax
	leaq	-336(%rbp), %r13
	movl	%eax, -752(%rbp)
	call	_ZdaPv@PLT
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	leaq	-592(%rbp), %rcx
	movq	%r13, %rsi
	movq	%rcx, %rdi
	movq	%rcx, -784(%rbp)
	call	_ZN6icu_676LocaleC1ERKS0_@PLT
	cmpl	$1, -752(%rbp)
	movl	$0, -732(%rbp)
	jne	.L347
	movq	-784(%rbp), %rdi
	leaq	-732(%rbp), %rsi
	call	_ZN6icu_6713BreakIterator18createWordInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r14
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L347:
	movl	-752(%rbp), %eax
	cmpl	$2, %eax
	jne	.L348
	movq	-784(%rbp), %rdi
	leaq	-732(%rbp), %rsi
	call	_ZN6icu_6713BreakIterator22createSentenceInstanceERKNS_6LocaleER10UErrorCode@PLT
	movq	%rax, %r14
	jmp	.L210
.L263:
	movl	$1, %eax
	jmp	.L200
.L346:
	movl	$2, %eax
	jmp	.L200
.L336:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L337:
	call	__stack_chk_fail@PLT
.L335:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE18164:
	.size	_ZN2v88internal11JSSegmenter3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_, .-_ZN2v88internal11JSSegmenter3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_6ObjectEEES8_
	.weak	_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE
	.section	.data.rel.ro.local._ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE,"awG",@progbits,_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE,comdat
	.align 8
	.type	_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE, @object
	.size	_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE, 32
_ZTVN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEEE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED1Ev
	.quad	_ZN2v88internal4Intl16AvailableLocalesIN6icu_6713BreakIteratorENS1_17SkipResourceCheckEED0Ev
	.weak	_ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt19_Sp_counted_deleterIPN6icu_6713BreakIteratorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.bss._ZZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11EvE17available_locales,"aw",@nobits
	.align 32
	.type	_ZZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11EvE17available_locales, @object
	.size	_ZZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11EvE17available_locales, 64
_ZZN2v88internal11JSSegmenter19GetAvailableLocalesB5cxx11EvE17available_locales:
	.zero	64
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
