	.file	"v8-regex.cc"
	.text
	.section	.rodata._ZN12v8_inspector7V8RegexC2EPNS_15V8InspectorImplERKNS_8String16Ebb.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Internal error"
	.section	.text._ZN12v8_inspector7V8RegexC2EPNS_15V8InspectorImplERKNS_8String16Ebb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector7V8RegexC2EPNS_15V8InspectorImplERKNS_8String16Ebb
	.type	_ZN12v8_inspector7V8RegexC2EPNS_15V8InspectorImplERKNS_8String16Ebb, @function
_ZN12v8_inspector7V8RegexC2EPNS_15V8InspectorImplERKNS_8String16Ebb:
.LFB7240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r15
	leaq	-144(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%rdx, -200(%rbp)
	movl	%ecx, -188(%rbp)
	movl	%r8d, -184(%rbp)
	xorl	%r8d, %r8d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	32(%rdi), %rax
	movw	%r8w, 32(%rdi)
	movq	%rax, 16(%rdi)
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 48(%rdi)
	movq	8(%rsi), %r13
	movq	%r15, %rdi
	movq	%rax, -208(%rbp)
	movq	%r13, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	(%rbx), %rdi
	call	_ZN12v8_inspector15V8InspectorImpl12regexContextEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v87Context5EnterEv@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movl	-188(%rbp), %ecx
	movl	-184(%rbp), %r8d
	movq	%r13, %rdi
	movq	-200(%rbp), %r10
	cmpb	$1, %cl
	sbbl	%eax, %eax
	movq	%r10, %rsi
	andl	$2, %eax
	addl	$4, %eax
	cmpb	$1, %cl
	sbbl	%edx, %edx
	andl	$2, %edx
	testb	%r8b, %r8b
	cmovne	%eax, %edx
	movl	%edx, -184(%rbp)
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movl	-184(%rbp), %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v86RegExp3NewENS_5LocalINS_7ContextEEENS1_INS_6StringEEENS0_5FlagsE@PLT
	testq	%rax, %rax
	je	.L62
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L21
	movq	%rsi, -184(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%rbx)
	movq	-184(%rbp), %rsi
.L21:
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 8(%rbx)
.L6:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L63
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L64
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch7MessageEv@PLT
	movq	%rax, %rdi
	call	_ZNK2v87Message3GetEv@PLT
	leaq	-96(%rbp), %rdi
	movq	%r13, %rsi
	leaq	-80(%rbp), %r13
	movq	%rax, %rdx
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-96(%rbp), %rdx
	movq	16(%rbx), %rdi
	movq	-88(%rbp), %rax
	cmpq	%r13, %rdx
	je	.L60
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -208(%rbp)
	je	.L65
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	32(%rbx), %rsi
	movq	%rdx, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	testq	%rdi, %rdi
	jne	.L53
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%r13, -96(%rbp)
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	-96(%rbp), %rdi
	leaq	.LC0(%rip), %rsi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %r13
	movq	16(%rbx), %rdi
	movq	-88(%rbp), %rax
	cmpq	%r13, %rdx
	je	.L60
	movq	-80(%rbp), %rcx
	cmpq	%rdi, -208(%rbp)
	je	.L66
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	32(%rbx), %rsi
	movq	%rdx, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
	testq	%rdi, %rdi
	je	.L19
.L53:
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L60:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L15
	cmpq	$1, %rax
	je	.L67
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L15
	movq	%r13, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	16(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%ecx, %ecx
	movq	%rax, 24(%rbx)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
.L17:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 48(%rbx)
	cmpq	%r13, %rdi
	je	.L6
	call	_ZdlPv@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	%rdx, 16(%rbx)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 24(%rbx)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdx, 16(%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 24(%rbx)
	jmp	.L19
.L67:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	16(%rbx), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L15
.L63:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7240:
	.size	_ZN12v8_inspector7V8RegexC2EPNS_15V8InspectorImplERKNS_8String16Ebb, .-_ZN12v8_inspector7V8RegexC2EPNS_15V8InspectorImplERKNS_8String16Ebb
	.globl	_ZN12v8_inspector7V8RegexC1EPNS_15V8InspectorImplERKNS_8String16Ebb
	.set	_ZN12v8_inspector7V8RegexC1EPNS_15V8InspectorImplERKNS_8String16Ebb,_ZN12v8_inspector7V8RegexC2EPNS_15V8InspectorImplERKNS_8String16Ebb
	.section	.rodata._ZNK12v8_inspector7V8Regex5matchERKNS_8String16EiPi.str1.1,"aMS",@progbits,1
.LC1:
	.string	"exec"
.LC2:
	.string	"basic_string::substr"
	.section	.rodata._ZNK12v8_inspector7V8Regex5matchERKNS_8String16EiPi.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata._ZNK12v8_inspector7V8Regex5matchERKNS_8String16EiPi.str1.1
.LC5:
	.string	"index"
	.section	.text._ZNK12v8_inspector7V8Regex5matchERKNS_8String16EiPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK12v8_inspector7V8Regex5matchERKNS_8String16EiPi
	.type	_ZNK12v8_inspector7V8Regex5matchERKNS_8String16EiPi, @function
_ZNK12v8_inspector7V8Regex5matchERKNS_8String16EiPi:
.LFB7242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$264, %rsp
	movl	%edx, -260(%rbp)
	movq	%rcx, -296(%rbp)
	movq	%fs:40, %rdx
	movq	%rdx, -56(%rbp)
	xorl	%edx, %edx
	testq	%rcx, %rcx
	je	.L69
	movl	$0, (%rcx)
.L69:
	cmpq	$0, 8(%r14)
	je	.L96
	movq	8(%rbx), %rax
	subq	$1, %rax
	cmpq	$2147483646, %rax
	ja	.L96
	movq	(%r14), %rax
	leaq	-208(%rbp), %r15
	movq	8(%rax), %r13
	leaq	-240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -256(%rbp)
	movq	%r13, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	(%r14), %rdi
	call	_ZN12v8_inspector15V8InspectorImpl12regexContextEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v87Context5EnterEv@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	leaq	-176(%rbp), %rax
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	8(%r14), %r14
	testq	%r14, %r14
	je	.L71
	movq	(%r14), %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r14
.L71:
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, -272(%rbp)
	testq	%rax, %rax
	je	.L72
	movslq	-260(%rbp), %rdx
	movq	8(%rbx), %rcx
	cmpq	%rcx, %rdx
	ja	.L119
	leaq	-112(%rbp), %rax
	subq	%rdx, %rcx
	movq	%rax, -280(%rbp)
	movq	%rax, -128(%rbp)
	movq	(%rbx), %rax
	leaq	(%rax,%rdx,2), %rsi
	movl	$4294967295, %eax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	movq	%rsi, %rax
	leaq	(%rcx,%rcx), %rbx
	addq	%rbx, %rax
	je	.L74
	testq	%rsi, %rsi
	je	.L120
.L74:
	movq	%rbx, %rcx
	sarq	%rcx
	cmpq	$14, %rbx
	ja	.L121
	cmpq	$2, %rbx
	jne	.L76
	movzwl	(%rsi), %eax
	movw	%ax, -112(%rbp)
	movq	-128(%rbp), %rax
.L77:
	xorl	%edx, %edx
	movq	%rcx, -120(%rbp)
	leaq	-96(%rbp), %r8
	movw	%dx, (%rax,%rbx)
	leaq	-128(%rbp), %rbx
	movq	%r8, %rdi
	movq	%rbx, %rsi
	movq	%r8, -288(%rbp)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-128(%rbp), %rdi
	cmpq	-280(%rbp), %rdi
	movq	-288(%rbp), %r8
	je	.L78
	movq	%r8, -280(%rbp)
	call	_ZdlPv@PLT
	movq	-280(%rbp), %r8
.L78:
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -128(%rbp)
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	-272(%rbp), %rdi
	movq	%rbx, %r8
	movq	%r14, %rdx
	movq	%r12, %rsi
	movl	$1, %ecx
	movl	$-1, %r14d
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L85
	movq	%rax, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L72
	movq	%r13, %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L85
	cmpq	$0, -296(%rbp)
	je	.L86
	movq	%rbx, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L72
	call	_ZNK2v86String6LengthEv@PLT
	movq	-296(%rbp), %rbx
	movl	%eax, (%rbx)
.L86:
	movq	%r13, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	movl	-260(%rbp), %r14d
	addl	%eax, %r14d
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$-1, %r14d
.L85:
	movq	-248(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-256(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L68:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	addq	$264, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	leaq	2(%rbx), %rdi
	movq	%rcx, -304(%rbp)
	movq	%rsi, -288(%rbp)
	call	_Znwm@PLT
	movq	-304(%rbp), %rcx
	movq	-288(%rbp), %rsi
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	%rcx, -112(%rbp)
.L75:
	movq	%rbx, %rdx
	movq	%rcx, -288(%rbp)
	call	memmove@PLT
	movq	-128(%rbp), %rax
	movq	-288(%rbp), %rcx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L76:
	movq	-280(%rbp), %rax
	testq	%rbx, %rbx
	je	.L77
	movq	%rax, %rdi
	jmp	.L75
.L120:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$-1, %r14d
	jmp	.L68
.L122:
	call	__stack_chk_fail@PLT
.L119:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE7242:
	.size	_ZNK12v8_inspector7V8Regex5matchERKNS_8String16EiPi, .-_ZNK12v8_inspector7V8Regex5matchERKNS_8String16EiPi
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
