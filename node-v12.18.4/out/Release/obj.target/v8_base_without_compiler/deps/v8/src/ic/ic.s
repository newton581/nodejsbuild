	.file	"ic.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB1723:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE1723:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4861:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4861:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4862:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4862:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4864:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4864:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB29699:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE29699:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB29700:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L8
	cmpl	$3, %edx
	je	.L9
	cmpl	$1, %edx
	je	.L13
.L9:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE29700:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZN2v88internal12_GLOBAL__N_119IsOutOfBoundsAccessENS0_6HandleINS0_6ObjectEEEj,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119IsOutOfBoundsAccessENS0_6HandleINS0_6ObjectEEEj, @function
_ZN2v88internal12_GLOBAL__N_119IsOutOfBoundsAccessENS0_6HandleINS0_6ObjectEEEj:
.LFB24294:
	.cfi_startproc
	movq	(%rdi), %rax
	testb	$1, %al
	jne	.L34
.L28:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movq	-1(%rax), %rdx
	cmpw	$1061, 11(%rdx)
	je	.L17
	movq	-1(%rax), %rdx
	cmpw	$1086, 11(%rdx)
	je	.L35
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L36
	movq	15(%rax), %rax
	movslq	11(%rax), %rax
.L23:
	movl	%esi, %esi
	cmpq	%rax, %rsi
	setnb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L28
	movslq	11(%rax), %rax
	movl	%esi, %esi
	cmpq	%rax, %rsi
	setnb	%al
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movq	47(%rax), %rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L17:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L19
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L20:
	movsd	.LC0(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L21
	cvttsd2siq	%xmm0, %rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L19:
	movsd	7(%rax), %xmm0
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	btcq	$63, %rax
	jmp	.L23
	.cfi_endproc
.LFE24294:
	.size	_ZN2v88internal12_GLOBAL__N_119IsOutOfBoundsAccessENS0_6HandleINS0_6ObjectEEEj, .-_ZN2v88internal12_GLOBAL__N_119IsOutOfBoundsAccessENS0_6HandleINS0_6ObjectEEEj
	.section	.text._ZN2v88internal2ICD2Ev,"axG",@progbits,_ZN2v88internal2ICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal2ICD2Ev
	.type	_ZN2v88internal2ICD2Ev, @function
_ZN2v88internal2ICD2Ev:
.LFB9109:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L37
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	ret
	.cfi_endproc
.LFE9109:
	.size	_ZN2v88internal2ICD2Ev, .-_ZN2v88internal2ICD2Ev
	.weak	_ZN2v88internal2ICD1Ev
	.set	_ZN2v88internal2ICD1Ev,_ZN2v88internal2ICD2Ev
	.section	.text._ZN2v88internal6LoadICD2Ev,"axG",@progbits,_ZN2v88internal6LoadICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6LoadICD2Ev
	.type	_ZN2v88internal6LoadICD2Ev, @function
_ZN2v88internal6LoadICD2Ev:
.LFB9119:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L39
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	ret
	.cfi_endproc
.LFE9119:
	.size	_ZN2v88internal6LoadICD2Ev, .-_ZN2v88internal6LoadICD2Ev
	.weak	_ZN2v88internal6LoadICD1Ev
	.set	_ZN2v88internal6LoadICD1Ev,_ZN2v88internal6LoadICD2Ev
	.section	.text._ZN2v88internal12LoadGlobalICD2Ev,"axG",@progbits,_ZN2v88internal12LoadGlobalICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12LoadGlobalICD2Ev
	.type	_ZN2v88internal12LoadGlobalICD2Ev, @function
_ZN2v88internal12LoadGlobalICD2Ev:
.LFB30680:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L41
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	ret
	.cfi_endproc
.LFE30680:
	.size	_ZN2v88internal12LoadGlobalICD2Ev, .-_ZN2v88internal12LoadGlobalICD2Ev
	.weak	_ZN2v88internal12LoadGlobalICD1Ev
	.set	_ZN2v88internal12LoadGlobalICD1Ev,_ZN2v88internal12LoadGlobalICD2Ev
	.section	.text._ZN2v88internal11KeyedLoadICD2Ev,"axG",@progbits,_ZN2v88internal11KeyedLoadICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11KeyedLoadICD2Ev
	.type	_ZN2v88internal11KeyedLoadICD2Ev, @function
_ZN2v88internal11KeyedLoadICD2Ev:
.LFB30676:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L43
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	ret
	.cfi_endproc
.LFE30676:
	.size	_ZN2v88internal11KeyedLoadICD2Ev, .-_ZN2v88internal11KeyedLoadICD2Ev
	.weak	_ZN2v88internal11KeyedLoadICD1Ev
	.set	_ZN2v88internal11KeyedLoadICD1Ev,_ZN2v88internal11KeyedLoadICD2Ev
	.section	.text._ZN2v88internal7StoreICD2Ev,"axG",@progbits,_ZN2v88internal7StoreICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7StoreICD2Ev
	.type	_ZN2v88internal7StoreICD2Ev, @function
_ZN2v88internal7StoreICD2Ev:
.LFB9134:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L45
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	ret
	.cfi_endproc
.LFE9134:
	.size	_ZN2v88internal7StoreICD2Ev, .-_ZN2v88internal7StoreICD2Ev
	.weak	_ZN2v88internal7StoreICD1Ev
	.set	_ZN2v88internal7StoreICD1Ev,_ZN2v88internal7StoreICD2Ev
	.section	.text._ZN2v88internal13StoreGlobalICD2Ev,"axG",@progbits,_ZN2v88internal13StoreGlobalICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13StoreGlobalICD2Ev
	.type	_ZN2v88internal13StoreGlobalICD2Ev, @function
_ZN2v88internal13StoreGlobalICD2Ev:
.LFB30672:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L47
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L47:
	ret
	.cfi_endproc
.LFE30672:
	.size	_ZN2v88internal13StoreGlobalICD2Ev, .-_ZN2v88internal13StoreGlobalICD2Ev
	.weak	_ZN2v88internal13StoreGlobalICD1Ev
	.set	_ZN2v88internal13StoreGlobalICD1Ev,_ZN2v88internal13StoreGlobalICD2Ev
	.section	.text._ZN2v88internal12KeyedStoreICD2Ev,"axG",@progbits,_ZN2v88internal12KeyedStoreICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12KeyedStoreICD2Ev
	.type	_ZN2v88internal12KeyedStoreICD2Ev, @function
_ZN2v88internal12KeyedStoreICD2Ev:
.LFB9147:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L49
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	ret
	.cfi_endproc
.LFE9147:
	.size	_ZN2v88internal12KeyedStoreICD2Ev, .-_ZN2v88internal12KeyedStoreICD2Ev
	.weak	_ZN2v88internal12KeyedStoreICD1Ev
	.set	_ZN2v88internal12KeyedStoreICD1Ev,_ZN2v88internal12KeyedStoreICD2Ev
	.section	.text._ZN2v88internal21StoreInArrayLiteralICD2Ev,"axG",@progbits,_ZN2v88internal21StoreInArrayLiteralICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21StoreInArrayLiteralICD2Ev
	.type	_ZN2v88internal21StoreInArrayLiteralICD2Ev, @function
_ZN2v88internal21StoreInArrayLiteralICD2Ev:
.LFB30668:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L51
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	ret
	.cfi_endproc
.LFE30668:
	.size	_ZN2v88internal21StoreInArrayLiteralICD2Ev, .-_ZN2v88internal21StoreInArrayLiteralICD2Ev
	.weak	_ZN2v88internal21StoreInArrayLiteralICD1Ev
	.set	_ZN2v88internal21StoreInArrayLiteralICD1Ev,_ZN2v88internal21StoreInArrayLiteralICD2Ev
	.section	.text._ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv:
.LFB29053:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal7ICStatsC1Ev@PLT
	.cfi_endproc
.LFE29053:
	.size	_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.section	.text._ZN2v88internal2ICD0Ev,"axG",@progbits,_ZN2v88internal2ICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal2ICD0Ev
	.type	_ZN2v88internal2ICD0Ev, @function
_ZN2v88internal2ICD0Ev:
.LFB9111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9111:
	.size	_ZN2v88internal2ICD0Ev, .-_ZN2v88internal2ICD0Ev
	.section	.text._ZN2v88internal6LoadICD0Ev,"axG",@progbits,_ZN2v88internal6LoadICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal6LoadICD0Ev
	.type	_ZN2v88internal6LoadICD0Ev, @function
_ZN2v88internal6LoadICD0Ev:
.LFB9121:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9121:
	.size	_ZN2v88internal6LoadICD0Ev, .-_ZN2v88internal6LoadICD0Ev
	.section	.text._ZN2v88internal12LoadGlobalICD0Ev,"axG",@progbits,_ZN2v88internal12LoadGlobalICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12LoadGlobalICD0Ev
	.type	_ZN2v88internal12LoadGlobalICD0Ev, @function
_ZN2v88internal12LoadGlobalICD0Ev:
.LFB30682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE30682:
	.size	_ZN2v88internal12LoadGlobalICD0Ev, .-_ZN2v88internal12LoadGlobalICD0Ev
	.section	.text._ZN2v88internal11KeyedLoadICD0Ev,"axG",@progbits,_ZN2v88internal11KeyedLoadICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11KeyedLoadICD0Ev
	.type	_ZN2v88internal11KeyedLoadICD0Ev, @function
_ZN2v88internal11KeyedLoadICD0Ev:
.LFB30678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE30678:
	.size	_ZN2v88internal11KeyedLoadICD0Ev, .-_ZN2v88internal11KeyedLoadICD0Ev
	.section	.text._ZN2v88internal7StoreICD0Ev,"axG",@progbits,_ZN2v88internal7StoreICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7StoreICD0Ev
	.type	_ZN2v88internal7StoreICD0Ev, @function
_ZN2v88internal7StoreICD0Ev:
.LFB9136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9136:
	.size	_ZN2v88internal7StoreICD0Ev, .-_ZN2v88internal7StoreICD0Ev
	.section	.text._ZN2v88internal13StoreGlobalICD0Ev,"axG",@progbits,_ZN2v88internal13StoreGlobalICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13StoreGlobalICD0Ev
	.type	_ZN2v88internal13StoreGlobalICD0Ev, @function
_ZN2v88internal13StoreGlobalICD0Ev:
.LFB30674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE30674:
	.size	_ZN2v88internal13StoreGlobalICD0Ev, .-_ZN2v88internal13StoreGlobalICD0Ev
	.section	.text._ZN2v88internal12KeyedStoreICD0Ev,"axG",@progbits,_ZN2v88internal12KeyedStoreICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12KeyedStoreICD0Ev
	.type	_ZN2v88internal12KeyedStoreICD0Ev, @function
_ZN2v88internal12KeyedStoreICD0Ev:
.LFB9149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZdlPv@PLT
.L91:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9149:
	.size	_ZN2v88internal12KeyedStoreICD0Ev, .-_ZN2v88internal12KeyedStoreICD0Ev
	.section	.text._ZN2v88internal21StoreInArrayLiteralICD0Ev,"axG",@progbits,_ZN2v88internal21StoreInArrayLiteralICD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21StoreInArrayLiteralICD0Ev
	.type	_ZN2v88internal21StoreInArrayLiteralICD0Ev, @function
_ZN2v88internal21StoreInArrayLiteralICD0Ev:
.LFB30670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L97
	call	_ZdlPv@PLT
.L97:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE30670:
	.size	_ZN2v88internal21StoreInArrayLiteralICD0Ev, .-_ZN2v88internal21StoreInArrayLiteralICD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD1Ev
	.type	_ZN2v88internal12StdoutStreamD1Ev, @function
_ZN2v88internal12StdoutStreamD1Ev:
.LFB29849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE29849:
	.size	_ZN2v88internal12StdoutStreamD1Ev, .-_ZN2v88internal12StdoutStreamD1Ev
	.section	.text._ZNK2v88internal12LoadGlobalIC9slow_stubEv,"axG",@progbits,_ZNK2v88internal12LoadGlobalIC9slow_stubEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal12LoadGlobalIC9slow_stubEv
	.type	_ZNK2v88internal12LoadGlobalIC9slow_stubEv, @function
_ZNK2v88internal12LoadGlobalIC9slow_stubEv:
.LFB9124:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movl	$120, %esi
	addq	$41184, %rdi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.cfi_endproc
.LFE9124:
	.size	_ZNK2v88internal12LoadGlobalIC9slow_stubEv, .-_ZNK2v88internal12LoadGlobalIC9slow_stubEv
	.section	.text._ZNK2v88internal13StoreGlobalIC9slow_stubEv,"axG",@progbits,_ZNK2v88internal13StoreGlobalIC9slow_stubEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal13StoreGlobalIC9slow_stubEv
	.type	_ZNK2v88internal13StoreGlobalIC9slow_stubEv, @function
_ZNK2v88internal13StoreGlobalIC9slow_stubEv:
.LFB9139:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movl	$126, %esi
	addq	$41184, %rdi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.cfi_endproc
.LFE9139:
	.size	_ZNK2v88internal13StoreGlobalIC9slow_stubEv, .-_ZNK2v88internal13StoreGlobalIC9slow_stubEv
	.section	.text._ZNK2v88internal21StoreInArrayLiteralIC9slow_stubEv,"axG",@progbits,_ZNK2v88internal21StoreInArrayLiteralIC9slow_stubEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal21StoreInArrayLiteralIC9slow_stubEv
	.type	_ZNK2v88internal21StoreInArrayLiteralIC9slow_stubEv, @function
_ZNK2v88internal21StoreInArrayLiteralIC9slow_stubEv:
.LFB9152:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movl	$128, %esi
	addq	$41184, %rdi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.cfi_endproc
.LFE9152:
	.size	_ZNK2v88internal21StoreInArrayLiteralIC9slow_stubEv, .-_ZNK2v88internal21StoreInArrayLiteralIC9slow_stubEv
	.section	.text._ZNK2v88internal12KeyedStoreIC9slow_stubEv,"axG",@progbits,_ZNK2v88internal12KeyedStoreIC9slow_stubEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal12KeyedStoreIC9slow_stubEv
	.type	_ZNK2v88internal12KeyedStoreIC9slow_stubEv, @function
_ZNK2v88internal12KeyedStoreIC9slow_stubEv:
.LFB9144:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movl	$119, %esi
	addq	$41184, %rdi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.cfi_endproc
.LFE9144:
	.size	_ZNK2v88internal12KeyedStoreIC9slow_stubEv, .-_ZNK2v88internal12KeyedStoreIC9slow_stubEv
	.section	.text._ZNK2v88internal7StoreIC9slow_stubEv,"axG",@progbits,_ZNK2v88internal7StoreIC9slow_stubEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal7StoreIC9slow_stubEv
	.type	_ZNK2v88internal7StoreIC9slow_stubEv, @function
_ZNK2v88internal7StoreIC9slow_stubEv:
.LFB9131:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movl	$119, %esi
	addq	$41184, %rdi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.cfi_endproc
.LFE9131:
	.size	_ZNK2v88internal7StoreIC9slow_stubEv, .-_ZNK2v88internal7StoreIC9slow_stubEv
	.section	.text._ZNK2v88internal6LoadIC9slow_stubEv,"axG",@progbits,_ZNK2v88internal6LoadIC9slow_stubEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal6LoadIC9slow_stubEv
	.type	_ZNK2v88internal6LoadIC9slow_stubEv, @function
_ZNK2v88internal6LoadIC9slow_stubEv:
.LFB9116:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	cmpl	$9, 28(%rdi)
	leaq	41184(%rax), %r8
	je	.L111
	movl	$122, %esi
	movq	%r8, %rdi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$155, %esi
	movq	%r8, %rdi
	jmp	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	.cfi_endproc
.LFE9116:
	.size	_ZNK2v88internal6LoadIC9slow_stubEv, .-_ZNK2v88internal6LoadIC9slow_stubEv
	.section	.text._ZN2v88internal12_GLOBAL__N_134AllowConvertHoleElementToUndefinedEPNS0_7IsolateENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_134AllowConvertHoleElementToUndefinedEPNS0_7IsolateENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal12_GLOBAL__N_134AllowConvertHoleElementToUndefinedEPNS0_7IsolateENS0_6HandleINS0_3MapEEE:
.LFB24287:
	.cfi_startproc
	movq	(%rsi), %rax
	cmpw	$1086, 11(%rax)
	je	.L126
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	call	_ZN2v88internal7Isolate27IsNoElementsProtectorIntactEv@PLT
	testb	%al, %al
	je	.L112
	movq	(%rbx), %rdx
	movzwl	11(%rdx), %eax
	cmpw	$63, %ax
	jbe	.L115
	cmpw	$1024, %ax
	jbe	.L119
	movq	41112(%r12), %rdi
	movq	23(%rdx), %rsi
	testq	%rdi, %rdi
	je	.L116
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L117:
	movl	$56, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	testb	%al, %al
	jne	.L115
	movq	(%rbx), %rsi
	addq	$16, %rsp
	movq	%r12, %rdi
	movl	$63, %edx
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal7Isolate14IsInAnyContextENS0_6ObjectEj@PLT
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	xorl	%eax, %eax
.L112:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L127
.L118:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L117
.L127:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L118
	.cfi_endproc
.LFE24287:
	.size	_ZN2v88internal12_GLOBAL__N_134AllowConvertHoleElementToUndefinedEPNS0_7IsolateENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal12_GLOBAL__N_134AllowConvertHoleElementToUndefinedEPNS0_7IsolateENS0_6HandleINS0_3MapEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_112GetStoreModeENS0_6HandleINS0_8JSObjectEEEj,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_112GetStoreModeENS0_6HandleINS0_8JSObjectEEEj, @function
_ZN2v88internal12_GLOBAL__N_112GetStoreModeENS0_6HandleINS0_8JSObjectEEEj:
.LFB24313:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal12_GLOBAL__N_119IsOutOfBoundsAccessENS0_6HandleINS0_6ObjectEEEj
	movq	(%rdi), %rdx
	movq	-1(%rdx), %rcx
	cmpw	$1061, 11(%rcx)
	jne	.L129
	testb	%al, %al
	jne	.L146
	movq	-1(%rdx), %rax
	leaq	-32(%rbp), %rdi
.L137:
	movq	15(%rdx), %rax
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal14FixedArrayBase10IsCowArrayEv@PLT
	cmpb	$1, %al
	sbbl	%eax, %eax
	notl	%eax
	andl	$3, %eax
.L128:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L147
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	-1(%rdx), %rcx
	movzbl	14(%rcx), %ecx
	shrl	$3, %ecx
	subl	$17, %ecx
	cmpb	$10, %cl
	ja	.L145
	testb	%al, %al
	jne	.L133
.L145:
	leaq	-32(%rbp), %rdi
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L146:
	leaq	-32(%rbp), %rdi
	movl	%r8d, %esi
	movq	%rdx, -32(%rbp)
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal8JSObject26WouldConvertToSlowElementsEj@PLT
	movq	-40(%rbp), %rdi
	testb	%al, %al
	je	.L148
	movq	(%rbx), %rdx
	movq	-1(%rdx), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$17, %eax
	cmpb	$10, %al
	ja	.L137
.L133:
	movl	$2, %eax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L148:
	movl	$1, %eax
	jmp	.L128
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24313:
	.size	_ZN2v88internal12_GLOBAL__N_112GetStoreModeENS0_6HandleINS0_8JSObjectEEEj, .-_ZN2v88internal12_GLOBAL__N_112GetStoreModeENS0_6HandleINS0_8JSObjectEEEj
	.section	.text._ZNK2v88internal10JSReceiver17HasFastPropertiesEPNS0_7IsolateE.isra.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal10JSReceiver17HasFastPropertiesEPNS0_7IsolateE.isra.0, @function
_ZNK2v88internal10JSReceiver17HasFastPropertiesEPNS0_7IsolateE.isra.0:
.LFB30900:
	.cfi_startproc
	movq	(%rdi), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	shrl	$21, %eax
	xorl	$1, %eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE30900:
	.size	_ZNK2v88internal10JSReceiver17HasFastPropertiesEPNS0_7IsolateE.isra.0, .-_ZNK2v88internal10JSReceiver17HasFastPropertiesEPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal12StdoutStreamD0Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD0Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD0Ev:
.LFB31075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	addq	-24(%rax), %rdi
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	movq	%rax, 80(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE31075:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12StdoutStreamD0Ev
	.type	_ZN2v88internal12StdoutStreamD0Ev, @function
_ZN2v88internal12StdoutStreamD0Ev:
.LFB29850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	64(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, 16(%rdi)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -64(%rdi)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, (%r12)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$344, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29850:
	.size	_ZN2v88internal12StdoutStreamD0Ev, .-_ZN2v88internal12StdoutStreamD0Ev
	.section	.text._ZN2v88internal12StdoutStreamD1Ev,"axG",@progbits,_ZN2v88internal12StdoutStreamD1Ev,comdat
	.p2align 4
	.weak	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.type	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, @function
_ZTv0_n24_N2v88internal12StdoutStreamD1Ev:
.LFB31076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rbx
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	addq	%rdi, %rbx
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	64(%rbx), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8ios_baseD2Ev@PLT
	.cfi_endproc
.LFE31076:
	.size	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev, .-_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.section	.text._ZN2v88internalL13TryConvertKeyENS0_6HandleINS0_6ObjectEEEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL13TryConvertKeyENS0_6HandleINS0_6ObjectEEEPNS0_7IsolateE, @function
_ZN2v88internalL13TryConvertKeyENS0_6HandleINS0_6ObjectEEEPNS0_7IsolateE:
.LFB24284:
	.cfi_startproc
	movq	(%rdi), %rdx
	movq	%rdi, %rax
	testb	$1, %dl
	jne	.L180
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$24, %rsp
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	je	.L159
	movq	-1(%rdx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L162
	movq	-1(%rdx), %rdx
	movzwl	11(%rdx), %edx
	andl	$65504, %edx
	jne	.L181
.L162:
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movsd	7(%rdx), %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L182
	movsd	.LC1(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L162
	comisd	.LC2(%rip), %xmm0
	jb	.L162
	cvttsd2sil	%xmm0, %esi
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%esi, %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L162
	jne	.L162
	movq	41112(%r12), %rdi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L166
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L181:
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L182:
	leaq	2880(%rsi), %rax
	jmp	.L162
.L166:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L183
.L167:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L162
.L183:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L167
	.cfi_endproc
.LFE24284:
	.size	_ZN2v88internalL13TryConvertKeyENS0_6HandleINS0_6ObjectEEEPNS0_7IsolateE, .-_ZN2v88internalL13TryConvertKeyENS0_6HandleINS0_6ObjectEEEPNS0_7IsolateE
	.section	.text._ZN2v88internalL18CanFastCloneObjectENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL18CanFastCloneObjectENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internalL18CanFastCloneObjectENS0_6HandleINS0_3MapEEE:
.LFB24356:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	-36704(%rdx), %rax
	jne	.L185
.L187:
	movl	$1, %eax
.L184:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L198
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	leaq	-37592(%rdx), %rcx
	cmpq	872(%rcx), %rax
	je	.L187
	cmpw	$1024, 11(%rax)
	jbe	.L190
	cmpb	$31, 14(%rax)
	ja	.L190
	movq	%rdi, %rbx
	leaq	-32(%rbp), %rdi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal3Map23OnlyHasSimplePropertiesEv@PLT
	testb	%al, %al
	je	.L190
	movq	(%rbx), %r9
	xorl	%edi, %edi
	movq	39(%r9), %rbx
	leaq	31(%rbx), %rcx
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L191:
	addq	$24, %rcx
.L192:
	movl	15(%r9), %edx
	shrl	$10, %edx
	andl	$1023, %edx
	cmpl	%edx, %edi
	jge	.L184
	movq	(%rcx), %rdx
	addl	$1, %edi
	movq	-8(%rcx), %r8
	movq	%rdx, %rsi
	shrq	$35, %rdx
	sarq	$32, %rsi
	andl	$2, %edx
	andl	$1, %esi
	orl	%esi, %edx
	jne	.L190
	movq	-1(%r8), %rdx
	cmpw	$64, 11(%rdx)
	jne	.L191
	testb	$16, 11(%r8)
	je	.L191
.L190:
	xorl	%eax, %eax
	jmp	.L184
.L198:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24356:
	.size	_ZN2v88internalL18CanFastCloneObjectENS0_6HandleINS0_3MapEEE, .-_ZN2v88internalL18CanFastCloneObjectENS0_6HandleINS0_3MapEEE
	.section	.text._ZN2v88internalL19CloneObjectSlowPathEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL19CloneObjectSlowPathEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEi, @function
_ZN2v88internalL19CloneObjectSlowPathEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEi:
.LFB24358:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	andl	$16, %edx
	je	.L200
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory24NewJSObjectWithNullProtoENS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L208
.L212:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver23SetOrCopyDataPropertiesEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_6ObjectEEEPKNS0_12ScopedVectorIS7_EEb@PLT
	testb	%al, %al
	movl	$0, %eax
	cmovne	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L202
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L203:
	movq	879(%rsi), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L205
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L206:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory11NewJSObjectENS0_6HandleINS0_10JSFunctionEEENS0_14AllocationTypeE@PLT
	movq	%rax, %rbx
	movq	0(%r13), %rax
	testb	$1, %al
	je	.L212
.L208:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37488(%rdx)
	jne	.L215
.L211:
	addq	$16, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L216
.L204:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L205:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L217
.L207:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r14, (%rsi)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L215:
	cmpq	%rax, -37504(%rdx)
	jne	.L212
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L216:
	movq	%r12, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L204
	.cfi_endproc
.LFE24358:
	.size	_ZN2v88internalL19CloneObjectSlowPathEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEi, .-_ZN2v88internalL19CloneObjectSlowPathEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEi
	.section	.rodata._ZN2v88internalL18FastCloneObjectMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEEi.str1.1,"aMS",@progbits,1
.LC3:
	.string	"ObjectWithNullProto"
.LC4:
	.string	"InitializeClonedDescriptors"
	.section	.rodata._ZN2v88internalL18FastCloneObjectMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEEi.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"static_cast<unsigned>(id) < 256"
	.section	.rodata._ZN2v88internalL18FastCloneObjectMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEEi.str1.1
.LC6:
	.string	"Check failed: %s."
	.section	.rodata._ZN2v88internalL18FastCloneObjectMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEEi.str1.8
	.align 8
.LC7:
	.string	"static_cast<unsigned>(value) <= 255"
	.section	.text._ZN2v88internalL18FastCloneObjectMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEEi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL18FastCloneObjectMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEEi, @function
_ZN2v88internalL18FastCloneObjectMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEEi:
.LFB24357:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdi), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L219
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L220:
	movq	879(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L222
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L223:
	movq	41112(%r12), %rdi
	movq	55(%rsi), %rsi
	testq	%rdi, %rdi
	je	.L225
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
	movq	(%r14), %rax
	cmpw	$1024, 11(%rax)
	ja	.L273
.L228:
	andl	$16, %r15d
	jne	.L274
	movl	15(%rax), %eax
	movq	%rbx, %r13
	testl	$1047552, %eax
	jne	.L237
.L248:
	movq	%rbx, %rax
.L236:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L275
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L276
.L221:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L225:
	movq	41088(%r12), %rbx
	cmpq	%rbx, 41096(%r12)
	je	.L277
.L227:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	movq	(%r14), %rax
	cmpw	$1024, 11(%rax)
	jbe	.L228
.L273:
	movzbl	7(%rax), %edx
	movzbl	8(%rax), %esi
	movq	(%rbx), %rdi
	subl	%esi, %edx
	movzbl	7(%rdi), %ecx
	movzbl	8(%rdi), %edi
	subl	%edi, %ecx
	cmpl	%edx, %ecx
	je	.L228
	movzbl	7(%rax), %ecx
	movzbl	8(%rax), %edx
	xorl	%r8d, %r8d
	subl	%edx, %ecx
	movzbl	9(%rax), %edx
	leal	24(,%rcx,8), %r9d
	cmpl	$2, %edx
	jg	.L278
.L229:
	movl	%r9d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map14CopyInitialMapEPNS0_7IsolateENS0_6HandleIS1_EEiii@PLT
	andl	$16, %r15d
	movq	%rax, %r13
	je	.L231
	cmpq	%rax, %rbx
	je	.L232
	testq	%rax, %rax
	je	.L234
	movq	(%rbx), %rax
	cmpq	%rax, 0(%r13)
	jne	.L234
.L232:
	movq	%r13, %rsi
	leaq	.LC3(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
.L234:
	leaq	104(%r12), %rdx
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map12SetPrototypeEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_10HeapObjectEEEb@PLT
.L231:
	movq	(%r14), %rax
	movl	15(%rax), %edx
	andl	$1047552, %edx
	je	.L279
	cmpq	%r13, %rbx
	je	.L237
	testq	%r13, %r13
	je	.L238
	testq	%rbx, %rbx
	je	.L238
	movq	(%rbx), %rcx
	cmpq	%rcx, 0(%r13)
	jne	.L238
.L237:
	movq	%r13, %rsi
	leaq	.LC4(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal3Map4CopyEPNS0_7IsolateENS0_6HandleIS1_EEPKc@PLT
	movq	%rax, %r13
	movq	(%r14), %rax
.L238:
	movq	41112(%r12), %rdi
	movq	39(%rax), %r15
	testq	%rdi, %rdi
	je	.L239
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L240:
	movq	(%r14), %rax
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	15(%rax), %r15d
	shrl	$10, %r15d
	andl	$1023, %r15d
	movl	%r15d, %edx
	call	_ZN2v88internal15DescriptorArray22CopyForFastObjectCloneEPNS0_7IsolateENS0_6HandleIS1_EEii@PLT
	movl	%r15d, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rax, %rbx
	call	_ZN2v88internal16LayoutDescriptor3NewEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_15DescriptorArrayEEEi@PLT
	movq	0(%r13), %rdx
	leaq	-64(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rdx, -64(%rbp)
	movq	(%rbx), %rdx
	movq	(%rax), %r15
	movswl	9(%rdx), %ecx
	call	_ZN2v88internal3Map22SetInstanceDescriptorsEPNS0_7IsolateENS0_15DescriptorArrayEi@PLT
	movq	-64(%rbp), %rax
	movq	%r15, 47(%rax)
	testb	$1, %r15b
	je	.L247
	movq	%r15, %rbx
	movq	-64(%rbp), %rdi
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	leaq	47(%rdi), %rsi
	testl	$262144, %eax
	jne	.L280
.L243:
	testb	$24, %al
	je	.L247
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L281
	.p2align 4,,10
	.p2align 3
.L247:
	movq	-64(%rbp), %rdi
	call	_ZN2v88internal3Map12GetVisitorIdES1_@PLT
	cmpl	$255, %eax
	ja	.L282
	movq	-64(%rbp), %rdx
	movb	%al, 10(%rdx)
	movq	(%r14), %rdx
	movq	0(%r13), %rcx
	movzbl	9(%rdx), %eax
	cmpl	$2, %eax
	jg	.L283
.L246:
	movb	%al, 9(%rcx)
	movq	0(%r13), %rcx
	movq	(%r14), %rax
	movl	15(%rax), %eax
	movl	15(%rcx), %edx
	andl	$268435456, %eax
	andl	$-268435457, %edx
	orl	%edx, %eax
	movl	%eax, 15(%rcx)
	movq	%r13, %rax
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L222:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L284
.L224:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L239:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L285
.L241:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r15, (%rsi)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%r15, %rdx
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-64(%rbp), %rdi
	movq	8(%rbx), %rax
	leaq	47(%rdi), %rsi
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%r15, %rdx
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L283:
	movzbl	7(%rcx), %esi
	movzbl	7(%rdx), %edi
	movzbl	%sil, %edx
	movzbl	%dil, %esi
	subl	%esi, %edx
	addl	%edx, %eax
	cmpl	$255, %eax
	jbe	.L246
	leaq	.LC7(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%rbx, %r13
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L284:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L276:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L278:
	movzbl	7(%rax), %r8d
	subl	%edx, %r8d
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L282:
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L241
.L275:
	call	__stack_chk_fail@PLT
.L279:
	movq	%r13, %rbx
	jmp	.L248
	.cfi_endproc
.LFE24357:
	.size	_ZN2v88internalL18FastCloneObjectMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEEi, .-_ZN2v88internalL18FastCloneObjectMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEEi
	.section	.text._ZN2v88internal17PrototypeIterator7AdvanceEv,"axG",@progbits,_ZN2v88internal17PrototypeIterator7AdvanceEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17PrototypeIterator7AdvanceEv
	.type	_ZN2v88internal17PrototypeIterator7AdvanceEv, @function
_ZN2v88internal17PrototypeIterator7AdvanceEv:
.LFB12435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L304
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L291
	subq	$1, %rax
.L293:
	movq	(%rax), %rdx
	movq	(%rbx), %r12
	movl	$1, %eax
	movq	23(%rdx), %rsi
	cmpq	104(%r12), %rsi
	je	.L294
	xorl	%eax, %eax
	cmpl	$1, 24(%rbx)
	je	.L305
.L294:
	cmpq	$0, 16(%rbx)
	movb	%al, 28(%rbx)
	je	.L306
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L296
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L303:
	movq	%rax, 16(%rbx)
.L286:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L307
.L298:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L304:
	movq	8(%rdi), %rdx
	leaq	-1(%rdx), %rax
	testb	$1, %dl
	je	.L293
	movq	-1(%rdx), %rdx
	cmpw	$1024, 11(%rdx)
	jne	.L293
	movq	(%rdi), %rax
	movb	$1, 28(%rdi)
	movq	104(%rax), %rax
	movq	%rax, 8(%rdi)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%rsi, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	cmpw	$1026, 11(%rdx)
	setne	%al
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L291:
	movq	(%rdi), %rax
	movb	$1, 28(%rdi)
	addq	$104, %rax
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L298
	.cfi_endproc
.LFE12435:
	.size	_ZN2v88internal17PrototypeIterator7AdvanceEv, .-_ZN2v88internal17PrototypeIterator7AdvanceEv
	.section	.text._ZN2v88internal20FunctionTemplateInfo12BreakAtEntryEv,"axG",@progbits,_ZN2v88internal20FunctionTemplateInfo12BreakAtEntryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20FunctionTemplateInfo12BreakAtEntryEv
	.type	_ZN2v88internal20FunctionTemplateInfo12BreakAtEntryEv, @function
_ZN2v88internal20FunctionTemplateInfo12BreakAtEntryEv:
.LFB15782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	79(%rax), %rax
	testb	$1, %al
	jne	.L309
.L311:
	xorl	%eax, %eax
.L308:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L314
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$160, 11(%rdx)
	jne	.L311
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo12BreakAtEntryEv@PLT
	jmp	.L308
.L314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15782:
	.size	_ZN2v88internal20FunctionTemplateInfo12BreakAtEntryEv, .-_ZN2v88internal20FunctionTemplateInfo12BreakAtEntryEv
	.section	.text._ZN2v88internal14LookupIterator21IsCacheableTransitionEv,"axG",@progbits,_ZN2v88internal14LookupIterator21IsCacheableTransitionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14LookupIterator21IsCacheableTransitionEv
	.type	_ZN2v88internal14LookupIterator21IsCacheableTransitionEv, @function
_ZN2v88internal14LookupIterator21IsCacheableTransitionEv:
.LFB17134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %rax
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L338
.L316:
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L330
	movq	48(%rbx), %rax
	movq	(%rax), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, %r12
	testb	$1, %al
	jne	.L339
.L329:
	movq	(%r12), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L330
.L331:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	movq	40(%rbx), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %cl
	jne	.L340
.L320:
	movq	88(%rax), %rdx
	subq	$1, %rdx
.L332:
	movq	(%rdx), %rax
	cmpw	$68, 11(%rax)
	sete	%al
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	leaq	-1(%rcx), %rdx
	movq	-1(%rcx), %rcx
	cmpq	%rcx, 136(%rax)
	jne	.L320
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L338:
	movq	-1(%rax), %rdx
	cmpw	$159, 11(%rdx)
	jne	.L316
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L339:
	movq	-1(%rax), %rcx
	cmpw	$1026, 11(%rcx)
	jne	.L329
	movq	-1(%rax), %rax
	movq	23(%rax), %rsi
	movq	-1(%rsi), %rax
	leaq	-1(%rsi), %r12
	cmpw	$1025, 11(%rax)
	je	.L341
	movq	%rdx, %r12
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L341:
	movq	24(%rbx), %r13
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L326
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r12
	subq	$1, %r12
	jmp	.L329
.L326:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L342
.L328:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L329
.L342:
	movq	%r13, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L328
	.cfi_endproc
.LFE17134:
	.size	_ZN2v88internal14LookupIterator21IsCacheableTransitionEv, .-_ZN2v88internal14LookupIterator21IsCacheableTransitionEv
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB18365:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L349
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L352
.L343:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L349:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L343
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE18365:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC9:
	.string	"V8.Runtime_Runtime_StoreInArrayLiteralIC_Slow"
	.align 8
.LC10:
	.string	"JSObject::DefineOwnPropertyIgnoreAttributes( &it, value, NONE, Just(ShouldThrow::kThrowOnError)) .FromJust()"
	.section	.text._ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateE.isra.0:
.LFB30859:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L388
.L354:
	movq	_ZZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2485(%rip), %r13
	testq	%r13, %r13
	je	.L389
.L356:
	movq	$0, -240(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L390
.L358:
	addl	$1, 41104(%r12)
	leaq	-16(%rbx), %rcx
	leaq	-8(%rbx), %rdx
	movq	%r12, %rsi
	leaq	-160(%rbp), %r13
	leaq	-241(%rbp), %r8
	movl	$1, %r9d
	movb	$0, -241(%rbp)
	movq	%r13, %rdi
	movq	41088(%r12), %r15
	movq	41096(%r12), %r14
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rbx, %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE@PLT
	testb	%al, %al
	je	.L391
	shrw	$8, %ax
	je	.L392
.L366:
	movq	(%rbx), %r13
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L365
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L365:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L393
.L353:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L394
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L390:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L395
.L359:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L360
	movq	(%rdi), %rax
	call	*8(%rax)
.L360:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L361
	movq	(%rdi), %rax
	call	*8(%rax)
.L361:
	leaq	.LC9(%rip), %rax
	movq	%r13, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r14, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L389:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L396
.L357:
	movq	%r13, _ZZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2485(%rip)
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L388:
	movq	40960(%rsi), %rax
	movl	$332, %edx
	leaq	-200(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L391:
	movl	%eax, -260(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-260(%rbp), %eax
	shrw	$8, %ax
	jne	.L366
	.p2align 4,,10
	.p2align 3
.L392:
	leaq	.LC10(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L393:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L396:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L395:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L359
.L394:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30859:
	.size	_ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"V8.Runtime_Runtime_KeyedStoreIC_Slow"
	.section	.text._ZN2v88internalL31Stats_Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateE.isra.0:
.LFB30867:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L430
.L398:
	movq	_ZZN2v88internalL31Stats_Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2473(%rip), %rbx
	testq	%rbx, %rbx
	je	.L431
.L400:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L432
.L402:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	-8(%r13), %rsi
	movq	%r13, %rcx
	addl	$1, 41104(%r12)
	leaq	-16(%r13), %rdx
	movq	%r12, %rdi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r14
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L433
	movq	(%rax), %r13
.L407:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L410
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L410:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L434
.L397:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L435
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L436
.L403:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L404
	movq	(%rdi), %rax
	call	*8(%rax)
.L404:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L405
	movq	(%rdi), %rax
	call	*8(%rax)
.L405:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L431:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L437
.L401:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2473(%rip)
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L433:
	movq	312(%r12), %r13
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L434:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L430:
	movq	40960(%rsi), %rax
	movl	$321, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L437:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L436:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L403
.L435:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30867:
	.size	_ZN2v88internalL31Stats_Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"V8.Runtime_Runtime_CloneObjectIC_Miss"
	.section	.text._ZN2v88internalL32Stats_Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateE.isra.0:
.LFB30877:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L487
.L439:
	movq	_ZZN2v88internalL32Stats_Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2623(%rip), %rbx
	testq	%rbx, %rbx
	je	.L488
.L441:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L489
.L443:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rax
	movslq	-4(%r13), %r15
	testb	$1, %al
	jne	.L490
.L448:
	movq	-24(%r13), %rax
	movq	-16(%r13), %rsi
	leaq	-24(%r13), %rdi
	movq	-1(%rax), %rcx
	cmpw	$155, 11(%rcx)
	je	.L491
.L451:
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internalL19CloneObjectSlowPathEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEi
	testq	%rax, %rax
	je	.L492
	movq	(%rax), %r13
.L460:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L464
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L464:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L493
.L438:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L494
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L448
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	je	.L448
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L489:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L495
.L444:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L445
	movq	(%rdi), %rax
	call	*8(%rax)
.L445:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L446
	movq	(%rdi), %rax
	call	*8(%rax)
.L446:
	leaq	.LC12(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L488:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L496
.L442:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2623(%rip)
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L491:
	movq	%rdi, -192(%rbp)
	sarq	$32, %rsi
	leaq	-200(%rbp), %rdi
	movq	$0, -184(%rbp)
	movl	%esi, -176(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movl	%eax, -172(%rbp)
	testb	$1, 0(%r13)
	je	.L451
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	cmpl	$6, %eax
	je	.L451
	movq	0(%r13), %rax
	movq	-1(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L456
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L457:
	movq	%rsi, %rdi
	movq	%rsi, -224(%rbp)
	call	_ZN2v88internalL18CanFastCloneObjectENS0_6HandleINS0_3MapEEE
	testb	%al, %al
	je	.L459
	movq	-224(%rbp), %rsi
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internalL18FastCloneObjectMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEEi
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
	movq	%rax, %r13
	movq	%rax, %rdx
	call	_ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_@PLT
	movq	0(%r13), %r13
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L492:
	movq	312(%r12), %r13
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L487:
	movq	40960(%rsi), %rax
	movl	$334, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L493:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L496:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L495:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC12(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L444
.L459:
	movq	-216(%rbp), %rdi
	call	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicEv@PLT
	jmp	.L451
.L456:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L497
.L458:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L457
.L497:
	movq	%r12, %rdi
	movq	%r8, -224(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L458
.L494:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30877:
	.size	_ZN2v88internalL32Stats_Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8.Runtime_Runtime_StoreGlobalIC_Slow"
	.section	.rodata._ZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC14:
	.string	"args[4].IsString()"
	.section	.text._ZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE.isra.0:
.LFB30905:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L563
.L499:
	movq	_ZZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2358(%rip), %rbx
	testq	%rbx, %rbx
	je	.L564
.L501:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L565
.L503:
	movq	41088(%r15), %rax
	addl	$1, 41104(%r15)
	movq	41096(%r15), %rbx
	movq	%rax, -192(%rbp)
	leaq	-32(%r12), %rax
	movq	%rax, -184(%rbp)
	movq	-32(%r12), %rax
	testb	$1, %al
	jne	.L566
.L507:
	leaq	.LC14(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L564:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L567
.L502:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2358(%rip)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L566:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L507
	movq	12464(%r15), %rax
	leaq	-176(%rbp), %r14
	movq	%r14, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L568
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L510:
	movq	12464(%r15), %rax
	movq	39(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L512
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L513:
	movq	1135(%rsi), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L515
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L516:
	movq	-32(%r12), %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%r8, -200(%rbp)
	call	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE@PLT
	testb	%al, %al
	je	.L518
	movl	-176(%rbp), %eax
	movq	-200(%rbp), %r8
	leal	24(,%rax,8), %eax
	movq	(%r8), %rdx
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L519
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpb	$1, -168(%rbp)
	movq	%rax, %r14
	je	.L569
.L522:
	movl	-172(%rbp), %eax
	movq	(%r14), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L524
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	cmpq	%rsi, 96(%r15)
	jne	.L527
.L579:
	movq	-184(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movl	$177, %esi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L562:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r12
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L518:
	movq	-184(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L570
	movq	(%rax), %r12
.L523:
	movq	-192(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %rbx
	je	.L534
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L534:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L571
.L498:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L572
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	movq	41088(%r15), %r8
	cmpq	41096(%r15), %r8
	je	.L573
.L517:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r8)
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L512:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L574
.L514:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L568:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L575
.L511:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L565:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L576
.L504:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L505
	movq	(%rdi), %rax
	call	*8(%rax)
.L505:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L506
	movq	(%rdi), %rax
	call	*8(%rax)
.L506:
	leaq	.LC13(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L519:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L577
.L521:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	cmpb	$1, -168(%rbp)
	jne	.L522
.L569:
	movq	-184(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movq	%r15, %rdi
	movl	$37, %esi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L524:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L578
.L526:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	cmpq	%rsi, 96(%r15)
	je	.L579
.L527:
	movl	-172(%rbp), %eax
	movq	(%r14), %r14
	movq	(%r12), %r13
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%r14,%rax), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L535
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -184(%rbp)
	testl	$262144, %eax
	je	.L529
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-184(%rbp), %rcx
	movq	-200(%rbp), %rsi
	movq	8(%rcx), %rax
.L529:
	testb	$24, %al
	je	.L535
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L580
	.p2align 4,,10
	.p2align 3
.L535:
	movq	(%r12), %r12
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L563:
	movq	40960(%rsi), %rax
	movl	$330, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L571:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L570:
	movq	312(%r15), %r12
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L575:
	movq	%r15, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L574:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L573:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L576:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L567:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L577:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L580:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L578:
	movq	%r15, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	jmp	.L526
.L572:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30905:
	.size	_ZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"V8.Runtime_Runtime_LoadGlobalIC_Slow"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"args[0].IsString()"
	.section	.text._ZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE, @function
_ZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE:
.LFB24323:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L637
.L582:
	movq	_ZZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2232(%rip), %rbx
	testq	%rbx, %rbx
	je	.L638
.L584:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L639
.L586:
	movq	41096(%r12), %rax
	movq	41088(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	%rax, -200(%rbp)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L640
.L590:
	leaq	.LC16(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L638:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L641
.L585:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2232(%rip)
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L640:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L590
	movq	12464(%r12), %rax
	movq	39(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L642
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L593:
	movq	1135(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L595
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L596:
	movq	0(%r13), %rdx
	leaq	-172(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE@PLT
	testb	%al, %al
	je	.L598
	movl	-172(%rbp), %eax
	movq	(%r14), %rdx
	leal	24(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L599
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L600:
	movl	-168(%rbp), %eax
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rsi,%rax), %r14
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L602
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r14
.L603:
	cmpq	%r14, 96(%r12)
	jne	.L606
.L636:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$177, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r14
.L606:
	subl	$1, 41104(%r12)
	movq	-200(%rbp), %rax
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %rax
	je	.L615
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L615:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L643
.L581:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L644
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L598:
	.cfi_restore_state
	movq	(%r15), %rax
	leaq	-184(%rbp), %r14
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r8
	testq	%rdi, %rdi
	je	.L607
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L608:
	movq	%r13, %rdx
	leaq	-185(%rbp), %rcx
	movq	%r12, %rdi
	movb	$0, -185(%rbp)
	call	_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L645
	cmpb	$0, -185(%rbp)
	jne	.L611
	movq	%rax, -208(%rbp)
	movslq	-4(%r13), %rsi
	movq	%r14, %rdi
	movq	-16(%r13), %rax
	movq	%rax, -184(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	-208(%rbp), %rdx
	cmpl	$6, %eax
	je	.L636
.L611:
	movq	(%rdx), %r14
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L595:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L646
.L597:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L642:
	movq	%rbx, %r15
	cmpq	41096(%r12), %rbx
	je	.L647
.L594:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L639:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L648
.L587:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L588
	movq	(%rdi), %rax
	call	*8(%rax)
.L588:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L589
	movq	(%rdi), %rax
	call	*8(%rax)
.L589:
	leaq	.LC15(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L607:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L649
.L609:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L602:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L650
.L604:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r14, (%rax)
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L599:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L651
.L601:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L637:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$324, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L643:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L645:
	movq	312(%r12), %r14
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L647:
	movq	%r12, %rdi
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L646:
	movq	%r12, %rdi
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L648:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC15(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L641:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L651:
	movq	%r12, %rdi
	movq	%rsi, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %rsi
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L649:
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-208(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L650:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L604
.L644:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24323:
	.size	_ZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE, .-_ZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL47Stats_Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Runtime_Runtime_ElementsTransitionAndStoreIC_Miss"
	.section	.text._ZN2v88internalL47Stats_Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL47Stats_Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateE, @function
_ZN2v88internalL47Stats_Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateE:
.LFB24353:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	movq	$0, -176(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L691
.L653:
	movq	_ZZN2v88internalL47Stats_Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2496(%rip), %rbx
	testq	%rbx, %rbx
	je	.L692
.L655:
	movq	$0, -240(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L693
.L657:
	movq	41088(%r12), %rax
	leaq	-160(%rbp), %r14
	leaq	-8(%r13), %r15
	addl	$1, 41104(%r12)
	movslq	-28(%r13), %rsi
	movq	%r14, %rdi
	movq	41096(%r12), %rbx
	movq	%rax, -264(%rbp)
	leaq	-16(%r13), %rax
	movq	%rax, -272(%rbp)
	movq	-40(%r13), %rax
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	0(%r13), %rcx
	testb	$1, %cl
	jne	.L694
.L662:
	cmpl	$14, %eax
	jne	.L664
	leaq	-241(%rbp), %r8
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$1, %r9d
	movb	$0, -241(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r14, %rdi
	movl	$1, %ecx
	leaq	-16(%r13), %rsi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE@PLT
	testb	%al, %al
	je	.L695
.L665:
	shrw	$8, %ax
	je	.L696
	movq	-16(%r13), %r13
.L667:
	subl	$1, 41104(%r12)
	movq	-264(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L671
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L671:
	leaq	-240(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L697
.L652:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L698
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L664:
	.cfi_restore_state
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	-16(%r13), %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L699
	movq	(%rax), %r13
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L692:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L700
.L656:
	movq	%rbx, _ZZN2v88internalL47Stats_Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2496(%rip)
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L694:
	movq	-1(%rcx), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L662
	movq	-24(%r13), %rcx
	movq	%r13, %rdi
	movl	%eax, -272(%rbp)
	movzbl	14(%rcx), %esi
	shrl	$3, %esi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movl	-272(%rbp), %eax
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L693:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L701
.L658:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L659
	movq	(%rdi), %rax
	call	*8(%rax)
.L659:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L660
	movq	(%rdi), %rax
	call	*8(%rax)
.L660:
	leaq	.LC17(%rip), %rax
	movq	%rbx, -232(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%r14, -216(%rbp)
	movq	%rax, -240(%rbp)
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L699:
	movq	312(%r12), %r13
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L696:
	leaq	.LC10(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L695:
	movl	%eax, -272(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-272(%rbp), %eax
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L691:
	movq	40960(%rdx), %rax
	leaq	-200(%rbp), %rsi
	movl	$317, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L697:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L700:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L701:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L658
.L698:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24353:
	.size	_ZN2v88internalL47Stats_Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateE, .-_ZN2v88internalL47Stats_Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"V8.Runtime_Runtime_StoreCallbackProperty"
	.section	.rodata._ZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC19:
	.string	"V8.ExternalCallback"
.LC20:
	.string	"accessor-setter"
	.section	.text._ZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE.isra.0:
.LFB30945:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$344, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -256(%rbp)
	movq	$0, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L791
.L703:
	movq	_ZZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateEE29trace_event_unique_atomic2653(%rip), %rbx
	testq	%rbx, %rbx
	je	.L792
.L705:
	movq	$0, -288(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L793
.L707:
	movq	41088(%r12), %rax
	leaq	-16(%r13), %rbx
	leaq	-24(%r13), %r14
	addl	$1, 41104(%r12)
	movq	%rax, -352(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -344(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L794
	movq	-16(%r13), %rax
	movq	0(%r13), %rcx
	xorl	%r9d, %r9d
	movq	%r12, %rsi
	movq	-8(%r13), %r8
	leaq	-144(%rbp), %rdi
	movq	55(%rax), %rdx
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r15
	movq	$0, -176(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L795
.L714:
	movq	-16(%r13), %rax
	movq	31(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L745
	movq	7(%rax), %r8
	movq	%r8, -360(%rbp)
.L715:
	cmpl	$32, 41828(%r15)
	jne	.L716
	movq	41112(%r15), %rdi
	movq	41472(%r15), %r9
	movq	-72(%rbp), %rsi
	testq	%rdi, %rdi
	je	.L717
	movq	%r9, -376(%rbp)
	movq	%r8, -368(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-368(%rbp), %r8
	movq	-376(%rbp), %r9
	movq	%rax, %rdx
.L718:
	movl	$2, %ecx
	movq	%rbx, %rsi
	movq	%r9, %rdi
	movq	%r8, -368(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-368(%rbp), %r8
	testb	%al, %al
	je	.L720
.L716:
	movq	12608(%r15), %rax
	movl	12616(%r15), %ebx
	movq	%r15, -320(%rbp)
	movl	$6, 12616(%r15)
	movq	%rax, -304(%rbp)
	leaq	-320(%rbp), %rax
	movq	%r8, -312(%rbp)
	movq	%rax, 12608(%r15)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L796
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L797
.L724:
	movq	41016(%r15), %rdi
	leaq	-120(%rbp), %rax
	movq	%rax, -328(%rbp)
	movq	%rdi, -368(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-368(%rbp), %rdi
	testb	%al, %al
	jne	.L798
.L728:
	movq	-360(%rbp), %rax
	leaq	-328(%rbp), %rdx
	movq	%r14, %rdi
	leaq	-32(%r13), %rsi
	call	*%rax
	movq	-320(%rbp), %rax
	movq	-304(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L799
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L800
.L732:
	movl	%ebx, 12616(%r15)
.L720:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L801
.L736:
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L802
	movq	-32(%r13), %r13
.L738:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
.L713:
	subl	$1, 41104(%r12)
	movq	-352(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-344(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L741
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L741:
	leaq	-288(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L803
.L702:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L804
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L802:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L800:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L805
.L733:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L734
	movq	(%rdi), %rax
	call	*8(%rax)
.L734:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L732
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L799:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L806
.L731:
	movq	%r14, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	movzbl	(%r14), %eax
	testb	$5, %al
	je	.L732
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L797:
	pxor	%xmm0, %xmm0
	movq	%rdx, -368(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-368(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L807
.L725:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L726
	movq	(%rdi), %rax
	call	*8(%rax)
.L726:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L724
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L798:
	movq	-24(%r13), %rcx
	movq	-112(%rbp), %rdx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L796:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L808
.L723:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	movzbl	(%rdx), %eax
	testb	$5, %al
	je	.L724
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L793:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L809
.L708:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L709
	movq	(%rdi), %rax
	call	*8(%rax)
.L709:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L710
	movq	(%rdi), %rax
	call	*8(%rax)
.L710:
	leaq	.LC18(%rip), %rax
	movq	%rbx, -280(%rbp)
	movq	%rax, -272(%rbp)
	leaq	-280(%rbp), %rax
	movq	%r14, -264(%rbp)
	movq	%rax, -288(%rbp)
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L792:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L810
.L706:
	movq	%rbx, _ZZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateEE29trace_event_unique_atomic2653(%rip)
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L717:
	movq	41088(%r15), %rdx
	cmpq	41096(%r15), %rdx
	je	.L811
.L719:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%rdx)
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L794:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	-32(%r13), %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L812
	movq	(%rax), %r13
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L803:
	leaq	-248(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L791:
	movq	40960(%rsi), %rax
	movl	$327, %edx
	leaq	-248(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -256(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L801:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L795:
	movq	40960(%r15), %rax
	leaq	-200(%rbp), %rsi
	movl	$110, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L745:
	movq	$0, -360(%rbp)
	xorl	%r8d, %r8d
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L810:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L809:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$88, %esi
	leaq	-144(%rbp), %rdx
	pushq	$0
	leaq	.LC18(%rip), %rcx
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L808:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L805:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	leaq	-160(%rbp), %rax
	pushq	$0
	movl	$69, %esi
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L806:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L807:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L811:
	movq	%r15, %rdi
	movq	%rsi, -384(%rbp)
	movq	%r9, -376(%rbp)
	movq	%r8, -368(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-384(%rbp), %rsi
	movq	-376(%rbp), %r9
	movq	-368(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L719
	.p2align 4,,10
	.p2align 3
.L812:
	movq	312(%r12), %r13
	jmp	.L713
.L804:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30945:
	.size	_ZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC21:
	.string	"V8.Runtime_Runtime_LoadPropertyWithInterceptor"
	.section	.rodata._ZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC22:
	.string	"interceptor-named-getter"
	.section	.text._ZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0:
.LFB30946:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$376, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -288(%rbp)
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L932
.L814:
	movq	_ZZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2680(%rip), %rbx
	testq	%rbx, %rbx
	je	.L933
.L816:
	movq	$0, -320(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L934
.L818:
	movq	41088(%r12), %rax
	movq	%r13, -408(%rbp)
	leaq	-8(%r13), %r14
	leaq	-16(%r13), %rbx
	addl	$1, 41104(%r12)
	movq	%rax, -376(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -384(%rbp)
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L822
.L825:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertReceiverEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L935
.L826:
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
.L931:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L936
.L831:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L832
	movq	31(%rdx), %rsi
.L832:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L833
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r11
.L834:
	movq	63(%rsi), %rdx
	movq	(%r14), %rcx
	movq	%r12, %rsi
	leaq	-144(%rbp), %rdi
	movq	(%rbx), %r8
	movq	%r11, -392(%rbp)
	movabsq	$4294967297, %r9
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r15
	movq	$0, -208(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	-392(%rbp), %r11
	testl	%eax, %eax
	jne	.L937
.L836:
	movq	41016(%r15), %r15
	movq	%r11, -392(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-392(%rbp), %r11
	testb	%al, %al
	jne	.L938
.L837:
	movq	(%r11), %rax
	movq	7(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L876
	movq	7(%rax), %r8
	movq	%r8, -392(%rbp)
.L838:
	movq	-104(%rbp), %r15
	cmpl	$32, 41828(%r15)
	je	.L839
.L842:
	movl	12616(%r15), %eax
	movl	$6, 12616(%r15)
	movq	%r15, -352(%rbp)
	movq	%r8, -344(%rbp)
	movl	%eax, -400(%rbp)
	movq	12608(%r15), %rax
	movq	%rax, -336(%rbp)
	leaq	-352(%rbp), %rax
	movq	%rax, 12608(%r15)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L840
.L841:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L939
.L845:
	leaq	-120(%rbp), %rax
	leaq	-360(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -360(%rbp)
	movq	-392(%rbp), %rax
	call	*%rax
	movq	96(%r15), %rax
	xorl	%r10d, %r10d
	cmpq	%rax, -88(%rbp)
	je	.L849
	leaq	-88(%rbp), %r10
.L849:
	movq	-352(%rbp), %rax
	movq	-336(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	testq	%rdx, %rdx
	je	.L940
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L941
.L853:
	movl	-400(%rbp), %eax
	movl	%eax, 12616(%r15)
.L843:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L942
.L857:
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L943
	testq	%r10, %r10
	je	.L860
	movq	(%r10), %r13
.L859:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
.L827:
	subl	$1, 41104(%r12)
	movq	-376(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-384(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L870
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L870:
	leaq	-320(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L944
.L813:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L945
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L833:
	.cfi_restore_state
	movq	41088(%r12), %r11
	cmpq	41096(%r12), %r11
	je	.L946
.L835:
	leaq	8(%r11), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r11)
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L934:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L947
.L819:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L820
	movq	(%rdi), %rax
	call	*8(%rax)
.L820:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L821
	movq	(%rdi), %rax
	call	*8(%rax)
.L821:
	leaq	.LC21(%rip), %rax
	movq	%rbx, -312(%rbp)
	movq	%rax, -304(%rbp)
	leaq	-312(%rbp), %rax
	movq	%r14, -296(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L933:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L948
.L817:
	movq	%rbx, _ZZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2680(%rip)
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L822:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L825
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L936:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L931
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L831
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L839:
	movq	41472(%r15), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r11, %rsi
	movq	%r8, -400(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	xorl	%r10d, %r10d
	movq	-400(%rbp), %r8
	testb	%al, %al
	je	.L843
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L938:
	movq	0(%r13), %rcx
	movq	-112(%rbp), %rdx
	leaq	.LC22(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	movq	-392(%rbp), %r11
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L943:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L940:
	movq	%r10, -392(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	-392(%rbp), %r10
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L949
.L852:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	movzbl	(%rdx), %eax
	testb	$5, %al
	je	.L853
.L941:
	pxor	%xmm0, %xmm0
	movq	%rdx, -416(%rbp)
	movq	%r10, -392(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-392(%rbp), %r10
	movq	-416(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L950
.L854:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L855
	movq	(%rdi), %rax
	movq	%r10, -392(%rbp)
	call	*8(%rax)
	movq	-392(%rbp), %r10
.L855:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L853
	movq	(%rdi), %rax
	movq	%r10, -392(%rbp)
	call	*8(%rax)
	movq	-392(%rbp), %r10
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L939:
	pxor	%xmm0, %xmm0
	movq	%rdx, -416(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-416(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L951
.L846:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L847
	movq	(%rdi), %rax
	call	*8(%rax)
.L847:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L845
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L840:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L952
.L844:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L860:
	movq	(%rbx), %rax
	movq	0(%r13), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$3, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L861
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L861:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	0(%r13), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L953
.L862:
	movq	%r13, -208(%rbp)
	leaq	-240(%rbp), %r13
	movq	%r13, %rdi
	movq	$0, -200(%rbp)
	movq	%r14, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L954:
	testq	%rax, %rax
	je	.L863
	movq	(%rbx), %rcx
	cmpq	%rcx, (%rax)
	je	.L864
.L863:
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
.L867:
	cmpl	$2, -236(%rbp)
	jne	.L863
	movq	-184(%rbp), %rax
	cmpq	%rax, %rbx
	jne	.L954
.L864:
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L955
	cmpl	$4, -236(%rbp)
	je	.L868
	movq	(%rax), %r13
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L935:
	movq	312(%r12), %r13
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L944:
	leaq	-280(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L813
	.p2align 4,,10
	.p2align 3
.L932:
	movq	40960(%rsi), %rax
	movl	$326, %edx
	leaq	-280(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L937:
	movq	40960(%r15), %rax
	leaq	-232(%rbp), %rsi
	movl	$173, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-392(%rbp), %r11
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L942:
	leaq	-232(%rbp), %rsi
	movq	%r10, -392(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-392(%rbp), %r10
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L876:
	movq	$0, -392(%rbp)
	xorl	%r8d, %r8d
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L868:
	movq	-408(%rbp), %rax
	leaq	-352(%rbp), %rdi
	movslq	-20(%rax), %rsi
	movq	-32(%rax), %rax
	movq	%rax, -352(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	cmpl	$6, %eax
	je	.L869
	movq	88(%r12), %r13
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L946:
	movq	%r12, %rdi
	movq	%rsi, -392(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-392(%rbp), %rsi
	movq	%rax, %r11
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L947:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$88, %esi
	leaq	-144(%rbp), %rdx
	pushq	$0
	leaq	.LC21(%rip), %rcx
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L819
	.p2align 4,,10
	.p2align 3
.L948:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L950:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	movq	-392(%rbp), %r10
	addq	$64, %rsp
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L949:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	-392(%rbp), %r10
	movq	%rax, %rdx
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L951:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L952:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L869:
	movq	-208(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$177, %esi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L953:
	movq	%r13, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r13
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L955:
	movq	312(%r12), %r13
	jmp	.L859
.L945:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30946:
	.size	_ZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC23:
	.string	"V8.Runtime_Runtime_StorePropertyWithInterceptor"
	.section	.rodata._ZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC24:
	.string	"interceptor-named-set"
	.section	.text._ZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0:
.LFB30947:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$376, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -288(%rbp)
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1069
.L957:
	movq	_ZZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2729(%rip), %r13
	testq	%r13, %r13
	je	.L1070
.L959:
	movq	$0, -320(%rbp)
	movzbl	0(%r13), %eax
	leaq	-144(%rbp), %r15
	testb	$5, %al
	jne	.L1071
.L961:
	movq	41088(%r12), %rax
	movq	41096(%r12), %r14
	leaq	-24(%rbx), %r13
	addl	$1, 41104(%r12)
	movslq	-4(%rbx), %rsi
	movq	%r13, %rdx
	movq	%rax, -384(%rbp)
	leaq	-32(%rbx), %rax
	movq	%rax, -392(%rbp)
	movq	-24(%rbx), %rax
	movq	-1(%rax), %rax
	cmpw	$1026, 11(%rax)
	je	.L1072
.L965:
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
.L1068:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1073
.L973:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L974
	movq	31(%rdx), %rsi
.L974:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L975
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L976:
	movq	-24(%rbx), %rcx
	movq	63(%rsi), %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movabsq	$4294967297, %r9
	movq	%rax, -376(%rbp)
	movq	%rcx, %r8
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movq	-376(%rbp), %rax
	movq	(%rax), %rax
	movq	15(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L1014
	movq	7(%rax), %rax
	movq	%rax, -376(%rbp)
.L978:
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r15
	movq	$0, -208(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testl	%edx, %edx
	jne	.L1074
.L979:
	xorl	%r11d, %r11d
	cmpl	$32, 41828(%r15)
	jne	.L1075
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1076
.L997:
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1077
.L998:
	testq	%r11, %r11
	je	.L1000
.L1004:
	movq	(%rbx), %r13
.L999:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movq	-384(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L1007
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1007:
	leaq	-320(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1078
.L956:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1079
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1000:
	.cfi_restore_state
	movq	-24(%rbx), %rax
	movq	-32(%rbx), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$3, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	je	.L1080
.L1001:
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	-32(%rbx), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L1081
.L1002:
	movq	-392(%rbp), %rax
	leaq	-240(%rbp), %rdi
	movq	$0, -200(%rbp)
	movq	%rdi, -376(%rbp)
	movq	%rax, -208(%rbp)
	movq	%r13, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%r13, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movl	-236(%rbp), %eax
	movq	-376(%rbp), %rdi
	testl	%eax, %eax
	je	.L1082
.L1003:
	movq	%rdi, -376(%rbp)
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	-376(%rbp), %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	jne	.L1004
	movq	312(%r12), %r13
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1075:
	movq	%rax, -344(%rbp)
	movq	12608(%r15), %rax
	movl	12616(%r15), %ecx
	movq	%r15, -352(%rbp)
	movq	%rax, -336(%rbp)
	leaq	-352(%rbp), %rax
	movl	%ecx, -400(%rbp)
	movl	$6, 12616(%r15)
	movq	%rax, 12608(%r15)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1083
.L982:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L1084
.L984:
	movq	41016(%r15), %rdi
	leaq	-120(%rbp), %rax
	movq	%rax, -360(%rbp)
	movq	%rdi, -408(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-408(%rbp), %rdi
	testb	%al, %al
	jne	.L1085
.L988:
	movq	-376(%rbp), %rax
	movq	-392(%rbp), %rdi
	leaq	-360(%rbp), %rdx
	movq	%rbx, %rsi
	call	*%rax
	movq	96(%r15), %rax
	xorl	%r11d, %r11d
	cmpq	%rax, -88(%rbp)
	je	.L989
	leaq	-88(%rbp), %r11
.L989:
	movq	-352(%rbp), %rax
	movq	-336(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1086
.L991:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L1087
.L993:
	movl	-400(%rbp), %eax
	movl	%eax, 12616(%r15)
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L997
.L1076:
	leaq	-232(%rbp), %rsi
	movq	%r11, -376(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-376(%rbp), %r11
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L998
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L975:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1088
.L977:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L1068
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L973
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L1071:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1089
.L962:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L963
	movq	(%rdi), %rax
	call	*8(%rax)
.L963:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L964
	movq	(%rdi), %rax
	call	*8(%rax)
.L964:
	leaq	.LC23(%rip), %rax
	movq	%r13, -312(%rbp)
	movq	%rax, -304(%rbp)
	leaq	-312(%rbp), %rax
	movq	%r14, -296(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L1070:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1090
.L960:
	movq	%r13, _ZZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2729(%rip)
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L1072:
	movq	-16(%rbx), %rax
	leaq	-240(%rbp), %rdi
	movq	%rdi, -376(%rbp)
	movq	%rax, -240(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	-376(%rbp), %rdi
	cmpl	$1, %eax
	je	.L1021
	movq	%r13, %rdx
	cmpl	$10, %eax
	jne	.L965
.L1021:
	movq	12464(%r12), %rax
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L967
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L1080:
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1082:
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movq	-376(%rbp), %rdi
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1084:
	pxor	%xmm0, %xmm0
	movq	%rdx, -408(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-408(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L1091
.L985:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L986
	movq	(%rdi), %rax
	call	*8(%rax)
.L986:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L984
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1083:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1092
.L983:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1087:
	pxor	%xmm0, %xmm0
	movq	%rdx, -408(%rbp)
	movq	%r11, -376(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-376(%rbp), %r11
	movq	-408(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r10
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r10
	jne	.L1093
.L994:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L995
	movq	(%rdi), %rax
	movq	%r11, -376(%rbp)
	call	*8(%rax)
	movq	-376(%rbp), %r11
.L995:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L993
	movq	(%rdi), %rax
	movq	%r11, -376(%rbp)
	call	*8(%rax)
	movq	-376(%rbp), %r11
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1086:
	movq	%r11, -376(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	-376(%rbp), %r11
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1094
.L992:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1085:
	movq	-32(%rbx), %rcx
	movq	-112(%rbp), %rdx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1074:
	movq	%rax, -400(%rbp)
	movq	40960(%r15), %rax
	movl	$175, %edx
	leaq	-232(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-400(%rbp), %rax
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1069:
	movq	40960(%rsi), %rax
	movl	$333, %edx
	leaq	-280(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L1078:
	leaq	-280(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	$0, -376(%rbp)
	xorl	%eax, %eax
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L1088:
	movq	%r12, %rdi
	movq	%rsi, -376(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-376(%rbp), %rsi
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1089:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	pushq	$0
	leaq	.LC23(%rip), %rcx
	movl	$88, %esi
	pushq	%r15
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1090:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	-392(%rbp), %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, -392(%rbp)
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L967:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L1095
.L969:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L1094:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	-376(%rbp), %r11
	movq	%rax, %rdx
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1092:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1091:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L1093:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	movq	-376(%rbp), %r11
	addq	$64, %rsp
	jmp	.L994
.L1095:
	movq	%r12, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-376(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L969
.L1079:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30947:
	.size	_ZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC25:
	.string	"V8.Runtime_Runtime_LoadElementWithInterceptor"
	.section	.rodata._ZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC26:
	.string	"interceptor-indexed-getter"
	.section	.text._ZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE.isra.0:
.LFB30948:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$360, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -288(%rbp)
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1196
.L1097:
	movq	_ZZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2776(%rip), %r13
	testq	%r13, %r13
	je	.L1197
.L1099:
	movq	$0, -320(%rbp)
	movzbl	0(%r13), %eax
	leaq	-144(%rbp), %r15
	testb	$5, %al
	jne	.L1198
.L1101:
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movq	%rax, -392(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -384(%rbp)
	movslq	-4(%rbx), %rax
	movq	%rax, -376(%rbp)
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
.L1195:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1199
.L1108:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L1109
	movq	39(%rdx), %rsi
.L1109:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1110
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L1111:
	movq	(%rbx), %rcx
	movq	63(%rsi), %rdx
	movq	%r15, %rdi
	movabsq	$4294967297, %r9
	movq	%r12, %rsi
	movq	%rcx, %r8
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r15
	movq	$0, -208(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1200
.L1113:
	movq	41016(%r15), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L1201
.L1114:
	movq	(%r14), %rax
	movq	7(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L1146
	movq	7(%rax), %r8
	movq	%r8, %r15
.L1115:
	movq	-104(%rbp), %r13
	cmpl	$32, 41828(%r13)
	je	.L1116
.L1119:
	movl	12616(%r13), %eax
	movq	%r13, -352(%rbp)
	movl	$6, 12616(%r13)
	movl	%eax, %r14d
	movq	12608(%r13), %rax
	movq	%r8, -344(%rbp)
	movq	%rax, -336(%rbp)
	leaq	-352(%rbp), %rax
	movq	%rax, 12608(%r13)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1117
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L1202
.L1122:
	leaq	-120(%rbp), %rax
	movl	-376(%rbp), %edi
	leaq	-360(%rbp), %rsi
	movq	%rax, -360(%rbp)
	call	*%r15
	movq	96(%r13), %rax
	xorl	%r15d, %r15d
	cmpq	%rax, -88(%rbp)
	je	.L1126
	leaq	-88(%rbp), %r15
.L1126:
	movq	-352(%rbp), %rax
	movq	-336(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1203
.L1128:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L1204
.L1130:
	movl	%r14d, 12616(%r13)
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1205
.L1134:
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L1206
.L1135:
	testq	%r15, %r15
	je	.L1207
.L1137:
	movq	(%r15), %r13
.L1136:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movq	-392(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	movq	-384(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L1141
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1141:
	leaq	-320(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1208
.L1096:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1209
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1207:
	.cfi_restore_state
	leaq	-240(%rbp), %r13
	movq	%r12, -216(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	%r13, %rdi
	movl	-376(%rbp), %eax
	movl	$3, -240(%rbp)
	movl	%eax, -168(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%rbx, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movl	$-1, -164(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L1137
	movq	312(%r12), %r13
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L1210
.L1112:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L1195
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1108
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	41472(%r13), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r8, -400(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-400(%rbp), %r8
	testb	%al, %al
	jne	.L1119
	movq	-240(%rbp), %rdi
	xorl	%r15d, %r15d
	testq	%rdi, %rdi
	je	.L1134
	.p2align 4,,10
	.p2align 3
.L1205:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	je	.L1135
	.p2align 4,,10
	.p2align 3
.L1206:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1197:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1211
.L1100:
	movq	%r13, _ZZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2776(%rip)
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1198:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1212
.L1102:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1103
	movq	(%rdi), %rax
	call	*8(%rax)
.L1103:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1104
	movq	(%rdi), %rax
	call	*8(%rax)
.L1104:
	leaq	.LC25(%rip), %rax
	movq	%r13, -312(%rbp)
	movq	%rax, -304(%rbp)
	leaq	-312(%rbp), %rax
	movq	%r14, -296(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1201:
	movl	-376(%rbp), %ecx
	movq	-112(%rbp), %rdx
	leaq	.LC26(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1117:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1213
.L1121:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	movzbl	(%rdx), %eax
	testb	$5, %al
	je	.L1122
.L1202:
	pxor	%xmm0, %xmm0
	movq	%rdx, -400(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-400(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r10
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r10
	jne	.L1214
.L1123:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1124
	movq	(%rdi), %rax
	call	*8(%rax)
.L1124:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1122
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1204:
	pxor	%xmm0, %xmm0
	movq	%rdx, -400(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-400(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r10
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r10
	jne	.L1215
.L1131:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1132
	movq	(%rdi), %rax
	call	*8(%rax)
.L1132:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1130
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1203:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1216
.L1129:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1208:
	leaq	-280(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	40960(%rsi), %rax
	movl	$322, %edx
	leaq	-280(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	40960(%r15), %rax
	leaq	-232(%rbp), %rsi
	movl	$173, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1146:
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1210:
	movq	%r12, %rdi
	movq	%rsi, -400(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-400(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1212:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	pushq	$0
	leaq	.LC25(%rip), %rcx
	movl	$88, %esi
	pushq	%r15
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1211:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1214:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1216:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1215:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1213:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1121
.L1209:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30948:
	.size	_ZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC27:
	.string	"V8.Runtime_Runtime_HasElementWithInterceptor"
	.section	.rodata._ZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC28:
	.string	"interceptor-indexed-query"
.LC29:
	.string	"result->ToInt32(&value)"
	.section	.text._ZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE.isra.0:
.LFB30949:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$376, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -288(%rbp)
	movq	$0, -256(%rbp)
	movaps	%xmm0, -272(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1389
.L1218:
	movq	_ZZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2822(%rip), %r13
	testq	%r13, %r13
	je	.L1390
.L1220:
	movq	$0, -320(%rbp)
	movzbl	0(%r13), %eax
	leaq	-144(%rbp), %r15
	testb	$5, %al
	jne	.L1391
.L1222:
	movq	41088(%r12), %rax
	addl	$1, 41104(%r12)
	movq	%rax, -392(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -384(%rbp)
	movslq	-4(%rbx), %rax
	movq	%rax, -376(%rbp)
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
.L1384:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L1392
.L1229:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L1230
	movq	39(%rdx), %rsi
.L1230:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1231
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L1232:
	movq	(%rbx), %rcx
	movq	63(%rsi), %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movabsq	$4294967297, %r9
	movq	%rcx, %r8
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movq	(%r14), %rax
	movq	88(%r12), %rdx
	cmpq	23(%rax), %rdx
	je	.L1234
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r13
	movq	$0, -208(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1393
.L1235:
	movq	(%r14), %rax
	movq	23(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L1292
	movq	7(%rax), %r8
	movq	%r8, %r15
.L1236:
	cmpl	$32, 41828(%r13)
	je	.L1394
.L1237:
	movl	12616(%r13), %eax
	leaq	-352(%rbp), %r14
	movl	$6, 12616(%r13)
	movq	%r13, -352(%rbp)
	movq	%r8, -344(%rbp)
	movl	%eax, -400(%rbp)
	movq	12608(%r13), %rax
	movq	%rax, -336(%rbp)
	movq	%r14, 12608(%r13)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1395
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L1396
.L1243:
	leaq	-120(%rbp), %rax
	movq	%rax, -360(%rbp)
	movq	41016(%r13), %rdi
	movq	%rdi, -408(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-408(%rbp), %rdi
	testb	%al, %al
	jne	.L1397
.L1247:
	movl	-376(%rbp), %edi
	leaq	-360(%rbp), %rsi
	call	*%r15
	movq	96(%r13), %rax
	xorl	%r15d, %r15d
	cmpq	%rax, -88(%rbp)
	je	.L1248
	leaq	-88(%rbp), %r15
.L1248:
	movq	-352(%rbp), %rax
	movq	-336(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1398
.L1250:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L1399
.L1252:
	movl	-400(%rbp), %eax
	movl	%eax, 12616(%r13)
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1400
.L1256:
	testq	%r15, %r15
	je	.L1288
	movq	(%r15), %rax
	leaq	-240(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L1401
	cmpl	$64, -352(%rbp)
	je	.L1283
.L1386:
	movq	112(%r12), %r13
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1234:
	cmpq	7(%rax), %rdx
	jne	.L1402
.L1288:
	leaq	-240(%rbp), %r13
	movq	%r12, -216(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	%r13, %rdi
	movl	-376(%rbp), %eax
	movl	$3, -240(%rbp)
	movl	%eax, -168(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	%rbx, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movl	$-1, -164(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	je	.L1403
	shrw	$8, %ax
	jne	.L1386
.L1283:
	movq	120(%r12), %r13
.L1260:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movq	-392(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	movq	-384(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L1286
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1286:
	leaq	-320(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-288(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1404
.L1217:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1405
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1231:
	.cfi_restore_state
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L1406
.L1233:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1392:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L1384
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1229
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1391:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -144(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1407
.L1223:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1224
	movq	(%rdi), %rax
	call	*8(%rax)
.L1224:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1225
	movq	(%rdi), %rax
	call	*8(%rax)
.L1225:
	leaq	.LC27(%rip), %rax
	movq	%r13, -312(%rbp)
	movq	%rax, -304(%rbp)
	leaq	-312(%rbp), %rax
	movq	%r14, -296(%rbp)
	movq	%rax, -320(%rbp)
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1390:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1408
.L1221:
	movq	%r13, _ZZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2822(%rip)
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1403:
	movq	312(%r12), %r13
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	41472(%r13), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r8, -400(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-400(%rbp), %r8
	testb	%al, %al
	jne	.L1237
.L1388:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1288
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1402:
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r15
	movq	$0, -208(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L1409
.L1261:
	movq	41016(%r15), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L1410
.L1262:
	movq	(%r14), %rax
	movq	7(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L1296
	movq	7(%rax), %r8
	movq	%r8, %r13
.L1263:
	movq	-104(%rbp), %r15
	cmpl	$32, 41828(%r15)
	je	.L1411
.L1264:
	movl	12616(%r15), %eax
	movl	$6, 12616(%r15)
	movq	%r15, -352(%rbp)
	movq	%r8, -344(%rbp)
	movl	%eax, %r14d
	movq	12608(%r15), %rax
	movq	%rax, -336(%rbp)
	leaq	-352(%rbp), %rax
	movq	%rax, 12608(%r15)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1412
.L1267:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L1413
.L1269:
	leaq	-120(%rbp), %rax
	movl	-376(%rbp), %edi
	leaq	-360(%rbp), %rsi
	movq	%rax, -360(%rbp)
	call	*%r13
	movq	96(%r15), %rax
	xorl	%r13d, %r13d
	cmpq	%rax, -88(%rbp)
	je	.L1273
	leaq	-88(%rbp), %r13
.L1273:
	movq	-352(%rbp), %rax
	movq	-336(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1414
.L1275:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L1415
.L1277:
	movl	%r14d, 12616(%r15)
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1416
.L1281:
	testq	%r13, %r13
	jne	.L1386
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1395:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1417
.L1242:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	movzbl	(%rdx), %eax
	testb	$5, %al
	je	.L1243
.L1396:
	pxor	%xmm0, %xmm0
	movq	%rdx, -408(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-408(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r10
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r10
	jne	.L1418
.L1244:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1245
	movq	(%rdi), %rax
	call	*8(%rax)
.L1245:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1243
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1397:
	movl	-376(%rbp), %ecx
	movq	-112(%rbp), %rdx
	leaq	.LC28(%rip), %rsi
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	jmp	.L1247
	.p2align 4,,10
	.p2align 3
.L1399:
	pxor	%xmm0, %xmm0
	movq	%rdx, -408(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-408(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r10
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r10
	jne	.L1419
.L1253:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1254
	movq	(%rdi), %rax
	call	*8(%rax)
.L1254:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1252
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1398:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1420
.L1251:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	40960(%rsi), %rax
	movl	$336, %edx
	leaq	-280(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1218
	.p2align 4,,10
	.p2align 3
.L1404:
	leaq	-280(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1406:
	movq	%r12, %rdi
	movq	%rsi, -400(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-400(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1408:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1407:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	pushq	$0
	leaq	.LC27(%rip), %rcx
	movl	$88, %esi
	pushq	%r15
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1411:
	movq	41472(%r15), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r8, -400(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-400(%rbp), %r8
	testb	%al, %al
	jne	.L1264
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1410:
	movl	-376(%rbp), %ecx
	movq	-112(%rbp), %rdx
	leaq	.LC26(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	jmp	.L1262
	.p2align 4,,10
	.p2align 3
.L1393:
	movq	40960(%r13), %rax
	leaq	-232(%rbp), %rsi
	movl	$159, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1292:
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1415:
	pxor	%xmm0, %xmm0
	movq	%rdx, -400(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-400(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r10
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r10
	jne	.L1421
.L1278:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1279
	movq	(%rdi), %rax
	call	*8(%rax)
.L1279:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1277
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1414:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1422
.L1276:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1413:
	pxor	%xmm0, %xmm0
	movq	%rdx, -400(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-400(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r10
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r10
	jne	.L1423
.L1270:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1271
	movq	(%rdi), %rax
	call	*8(%rax)
.L1271:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1269
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1412:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1424
.L1268:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1400:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1420:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1419:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1418:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1417:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1401:
	leaq	.LC29(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1409:
	movq	40960(%r15), %rax
	leaq	-232(%rbp), %rsi
	movl	$173, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1296:
	xorl	%r8d, %r8d
	xorl	%r13d, %r13d
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1416:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L1281
.L1423:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L1270
.L1421:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L1278
.L1422:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1276
.L1424:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1268
.L1405:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30949:
	.size	_ZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internal2IC23TransitionMarkFromStateENS0_16InlineCacheStateE.str1.1,"aMS",@progbits,1
.LC30:
	.string	"unreachable code"
	.section	.text._ZN2v88internal2IC23TransitionMarkFromStateENS0_16InlineCacheStateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC23TransitionMarkFromStateENS0_16InlineCacheStateE
	.type	_ZN2v88internal2IC23TransitionMarkFromStateENS0_16InlineCacheStateE, @function
_ZN2v88internal2IC23TransitionMarkFromStateENS0_16InlineCacheStateE:
.LFB24225:
	.cfi_startproc
	endbr64
	cmpl	$7, %esi
	ja	.L1426
	movl	%esi, %esi
	leaq	CSWTCH.1572(%rip), %rax
	movzbl	(%rax,%rsi), %eax
	ret
.L1426:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24225:
	.size	_ZN2v88internal2IC23TransitionMarkFromStateENS0_16InlineCacheStateE, .-_ZN2v88internal2IC23TransitionMarkFromStateENS0_16InlineCacheStateE
	.section	.text._ZN2v88internal2ICC2EPNS0_7IsolateENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotENS0_16FeedbackSlotKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2ICC2EPNS0_7IsolateENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotENS0_16FeedbackSlotKindE
	.type	_ZN2v88internal2ICC2EPNS0_7IsolateENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotENS0_16FeedbackSlotKindE, @function
_ZN2v88internal2ICC2EPNS0_7IsolateENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotENS0_16FeedbackSlotKindE:
.LFB24246:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movb	$0, 16(%rdi)
	movl	%r8d, 28(%rdi)
	movb	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	%rdx, 80(%rdi)
	movq	$0, 88(%rdi)
	movl	%ecx, 96(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	testq	%rdx, %rdx
	je	.L1431
	movq	(%rdx), %rax
	leaq	-32(%rbp), %rdi
	movl	%ecx, %esi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	80(%rbx), %rdi
	movl	%eax, 100(%rbx)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
.L1432:
	movl	%eax, 24(%rbx)
	movl	%eax, 20(%rbx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1436
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1431:
	.cfi_restore_state
	movl	$0, 100(%rdi)
	xorl	%eax, %eax
	jmp	.L1432
.L1436:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24246:
	.size	_ZN2v88internal2ICC2EPNS0_7IsolateENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotENS0_16FeedbackSlotKindE, .-_ZN2v88internal2ICC2EPNS0_7IsolateENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotENS0_16FeedbackSlotKindE
	.globl	_ZN2v88internal2ICC1EPNS0_7IsolateENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotENS0_16FeedbackSlotKindE
	.set	_ZN2v88internal2ICC1EPNS0_7IsolateENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotENS0_16FeedbackSlotKindE,_ZN2v88internal2ICC2EPNS0_7IsolateENS0_6HandleINS0_14FeedbackVectorEEENS0_12FeedbackSlotENS0_16FeedbackSlotKindE
	.section	.text._ZN2v88internal2IC22ShouldRecomputeHandlerENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC22ShouldRecomputeHandlerENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal2IC22ShouldRecomputeHandlerENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal2IC22ShouldRecomputeHandlerENS0_6HandleINS0_6StringEEE:
.LFB24249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	28(%rdi), %ecx
	cmpl	$14, %ecx
	ja	.L1440
	movl	$25352, %r12d
	shrq	%cl, %r12
	notq	%r12
	andl	$1, %r12d
	jne	.L1440
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L1459
.L1444:
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1459:
	.cfi_restore_state
	movq	%rsi, -24(%rbp)
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	ja	.L1444
	leaq	80(%rdi), %rdi
	call	_ZNK2v88internal13FeedbackNexus7GetNameEv@PLT
	movq	-24(%rbp), %rsi
	cmpq	%rax, (%rsi)
	jne	.L1444
	movl	28(%rbx), %ecx
	.p2align 4,,10
	.p2align 3
.L1440:
	cmpl	$10, %ecx
	jbe	.L1460
.L1445:
	leaq	80(%rbx), %r12
	movq	32(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal13FeedbackNexus17FindHandlerForMapENS0_6HandleINS0_3MapEEE@PLT
	testq	%rdx, %rdx
	je	.L1461
.L1446:
	addq	$16, %rsp
	movl	$1, %r12d
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1460:
	.cfi_restore_state
	movl	$1218, %eax
	btq	%rcx, %rax
	jc	.L1446
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1461:
	movq	32(%rbx), %rax
	movq	(%rax), %rax
	cmpw	$1024, 11(%rax)
	ja	.L1447
.L1450:
	xorl	%r12d, %r12d
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1447:
	cmpb	$0, 64(%rbx)
	jne	.L1449
	movb	$1, 64(%rbx)
	leaq	40(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal13FeedbackNexus11ExtractMapsEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EE@PLT
.L1449:
	movq	40(%rbx), %rax
	cmpq	%rax, 48(%rbx)
	je	.L1450
	movq	(%rax), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1450
	movq	8(%rbx), %r12
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1451
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L1452:
	movl	15(%rsi), %edx
	movl	$1, %r12d
	andl	$16777216, %edx
	jne	.L1444
	movq	32(%rbx), %rdx
	movq	(%rax), %rax
	movq	(%rdx), %rdx
	movzbl	14(%rax), %edi
	movzbl	14(%rdx), %esi
	addq	$16, %rsp
	shrl	$3, %edi
	popq	%rbx
	popq	%r12
	shrl	$3, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
.L1451:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L1462
.L1453:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L1452
.L1462:
	movq	%r12, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1453
	.cfi_endproc
.LFE24249:
	.size	_ZN2v88internal2IC22ShouldRecomputeHandlerENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal2IC22ShouldRecomputeHandlerENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal2IC23RecomputeHandlerForNameENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC23RecomputeHandlerForNameENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal2IC23RecomputeHandlerForNameENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal2IC23RecomputeHandlerForNameENS0_6HandleINS0_6ObjectEEE:
.LFB24250:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %ecx
	movl	$1, %eax
	cmpl	$14, %ecx
	ja	.L1470
	movl	$25352, %eax
	shrq	%cl, %rax
	notq	%rax
	andl	$1, %eax
	jne	.L1470
	movq	(%rsi), %rdx
	testb	$1, %dl
	jne	.L1473
.L1470:
	ret
	.p2align 4,,10
	.p2align 3
.L1473:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%rsi, -8(%rbp)
	movq	-1(%rdx), %rdx
	cmpw	$64, 11(%rdx)
	ja	.L1463
	addq	$80, %rdi
	call	_ZNK2v88internal13FeedbackNexus7GetNameEv@PLT
	movq	-8(%rbp), %rsi
	cmpq	%rax, (%rsi)
	sete	%al
.L1463:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24250:
	.size	_ZN2v88internal2IC23RecomputeHandlerForNameENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal2IC23RecomputeHandlerForNameENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	.type	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_, @function
_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_:
.LFB24251:
	.cfi_startproc
	endbr64
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jne	.L1492
	ret
	.p2align 4,,10
	.p2align 3
.L1492:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rax
	movq	8(%rdi), %r13
	testb	$1, %al
	jne	.L1477
	addq	$256, %r13
	movq	%r13, 32(%rdi)
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L1493
.L1474:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1477:
	.cfi_restore_state
	movq	-1(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1479
	movq	%rdx, -40(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-40(%rbp), %rdx
.L1480:
	movq	%rax, 32(%rbx)
	movq	(%rdx), %rax
	testb	$1, %al
	je	.L1474
.L1493:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1474
	movl	24(%rbx), %eax
	subl	$3, %eax
	andl	$-3, %eax
	jne	.L1474
	movq	8(%rbx), %rcx
	movq	(%r12), %rax
	cmpq	%rax, 104(%rcx)
	je	.L1474
	cmpq	%rax, 88(%rcx)
	je	.L1474
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal2IC22ShouldRecomputeHandlerENS0_6HandleINS0_6StringEEE
	testb	%al, %al
	je	.L1474
	movl	24(%rbx), %eax
	movl	$4, 24(%rbx)
	movl	%eax, 20(%rbx)
	jmp	.L1474
	.p2align 4,,10
	.p2align 3
.L1479:
	movq	41088(%r13), %rax
	cmpq	41096(%r13), %rax
	je	.L1494
.L1481:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r13)
	movq	%rsi, (%rax)
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	%r13, %rdi
	movq	%rdx, -48(%rbp)
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-48(%rbp), %rdx
	movq	-40(%rbp), %rsi
	jmp	.L1481
	.cfi_endproc
.LFE24251:
	.size	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_, .-_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	.section	.text._ZN2v88internal2IC9TypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC9TypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_
	.type	_ZN2v88internal2IC9TypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_, @function
_ZN2v88internal2IC9TypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_:
.LFB24252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	xorl	%r8d, %r8d
	movq	%rcx, %rdx
	movq	%r9, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r12
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	8(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1496
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1496:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24252:
	.size	_ZN2v88internal2IC9TypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_, .-_ZN2v88internal2IC9TypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_
	.section	.text._ZN2v88internal2IC14ReferenceErrorENS0_6HandleINS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC14ReferenceErrorENS0_6HandleINS0_4NameEEE
	.type	_ZN2v88internal2IC14ReferenceErrorENS0_6HandleINS0_4NameEEE, @function
_ZN2v88internal2IC14ReferenceErrorENS0_6HandleINS0_4NameEEE:
.LFB24253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$177, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r12
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	8(%rdi), %r13
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%r14, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %rbx
	je	.L1499
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L1499:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24253:
	.size	_ZN2v88internal2IC14ReferenceErrorENS0_6HandleINS0_4NameEEE, .-_ZN2v88internal2IC14ReferenceErrorENS0_6HandleINS0_4NameEEE
	.section	.rodata._ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc.str1.1,"aMS",@progbits,1
.LC31:
	.string	"[resetting ticks for "
.LC32:
	.string	" from "
.LC33:
	.string	" due to IC change: "
.LC34:
	.string	"]"
	.section	.text._ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	.type	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc, @function
_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc:
.LFB24255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$376, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal22FLAG_trace_opt_verboseE(%rip)
	je	.L1502
	movl	39(%rsi), %edx
	testl	%edx, %edx
	jne	.L1511
.L1502:
	movl	$0, 39(%rbx)
	movq	40944(%r15), %rax
	movb	$1, 8(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1512
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1511:
	.cfi_restore_state
	leaq	-320(%rbp), %r14
	movq	%rcx, %r12
	leaq	-400(%rbp), %r13
	movq	%r14, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	stdout(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	leaq	8+_ZTTN2v88internal12StdoutStreamE(%rip), %rsi
	movups	%xmm0, -88(%rbp)
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm0, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	$0, -104(%rbp)
	call	_ZN2v88internal8OFStreamC2EP8_IO_FILE@PLT
	leaq	24+_ZTVN2v88internal12StdoutStreamE(%rip), %rax
	movq	%r13, %rdi
	movl	$21, %edx
	movq	%rax, -400(%rbp)
	leaq	.LC31(%rip), %rsi
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	7(%rbx), %rax
	movq	%r13, %rsi
	leaq	-408(%rbp), %rdi
	movq	%rax, -408(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintERSo@PLT
	movl	$6, %edx
	movq	%r13, %rdi
	leaq	.LC32(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	39(%rbx), %esi
	movq	%r13, %rdi
	call	_ZNSolsEi@PLT
	movl	$19, %edx
	leaq	.LC33(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	testq	%r12, %r12
	je	.L1513
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1504:
	movl	$1, %edx
	leaq	.LC34(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %r12
	testq	%r12, %r12
	je	.L1514
	cmpb	$0, 56(%r12)
	je	.L1506
	movsbl	67(%r12), %esi
.L1507:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	64+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rax
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE(%rip), %rdx
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rdx, %xmm0
	leaq	-336(%rbp), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -400(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	leaq	24+_ZTCN2v88internal12StdoutStreamE0_So(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -400(%rbp)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1506:
	movq	%r12, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%r12), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1507
	movq	%r12, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1513:
	movq	0(%r13), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L1504
.L1512:
	call	__stack_chk_fail@PLT
.L1514:
	call	_ZSt16__throw_bad_castv@PLT
	.cfi_endproc
.LFE24255:
	.size	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc, .-_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	.section	.text._ZN2v88internal2IC17OnFeedbackChangedEPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC17OnFeedbackChangedEPKc
	.type	_ZN2v88internal2IC17OnFeedbackChangedEPKc, @function
_ZN2v88internal2IC17OnFeedbackChangedEPKc:
.LFB24254:
	.cfi_startproc
	endbr64
	movq	80(%rdi), %rax
	movb	$1, 16(%rdi)
	movq	%rsi, %rcx
	testq	%rax, %rax
	je	.L1518
	movl	96(%rdi), %edx
	movq	(%rax), %rsi
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	88(%rdi), %rsi
	movl	96(%rdi), %edx
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	.cfi_endproc
.LFE24254:
	.size	_ZN2v88internal2IC17OnFeedbackChangedEPKc, .-_ZN2v88internal2IC17OnFeedbackChangedEPKc
	.section	.rodata._ZN2v88internal2IC20ConfigureVectorStateENS0_16InlineCacheStateENS0_6HandleINS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC35:
	.string	"Megamorphic"
	.section	.text._ZN2v88internal2IC20ConfigureVectorStateENS0_16InlineCacheStateENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC20ConfigureVectorStateENS0_16InlineCacheStateENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal2IC20ConfigureVectorStateENS0_16InlineCacheStateENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal2IC20ConfigureVectorStateENS0_16InlineCacheStateENS0_6HandleINS0_6ObjectEEE:
.LFB24257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdx), %rax
	movq	%rdi, %rbx
	addq	$80, %rdi
	testb	$1, %al
	jne	.L1526
.L1520:
	call	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicENS0_11IcCheckTypeE@PLT
	movb	$1, 16(%rbx)
	movl	%eax, %r12d
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.L1527
	movq	(%rax), %rsi
.L1522:
	movl	96(%rbx), %edx
	movq	8(%rbx), %rdi
	leaq	.LC35(%rip), %rcx
	call	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1526:
	.cfi_restore_state
	movq	-1(%rax), %rax
	xorl	%esi, %esi
	cmpw	$64, 11(%rax)
	setbe	%sil
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L1527:
	movq	88(%rbx), %rsi
	jmp	.L1522
	.cfi_endproc
.LFE24257:
	.size	_ZN2v88internal2IC20ConfigureVectorStateENS0_16InlineCacheStateENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal2IC20ConfigureVectorStateENS0_16InlineCacheStateENS0_6HandleINS0_6ObjectEEE
	.section	.rodata._ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_3MapEEE.str1.1,"aMS",@progbits,1
.LC36:
	.string	"Premonomorphic"
	.section	.text._ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_3MapEEE
	.type	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_3MapEEE:
.LFB24258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$80, %rdi
	subq	$8, %rsp
	call	_ZN2v88internal13FeedbackNexus23ConfigurePremonomorphicENS0_6HandleINS0_3MapEEE@PLT
	movq	80(%rbx), %rax
	movb	$1, 16(%rbx)
	testq	%rax, %rax
	je	.L1532
	movq	(%rax), %rsi
.L1530:
	movl	96(%rbx), %edx
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	leaq	.LC36(%rip), %rcx
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	.p2align 4,,10
	.p2align 3
.L1532:
	.cfi_restore_state
	movq	88(%rbx), %rsi
	jmp	.L1530
	.cfi_endproc
.LFE24258:
	.size	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_3MapEEE
	.section	.rodata._ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS2_INS0_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC37:
	.string	"LoadGlobal"
.LC38:
	.string	"Monomorphic"
	.section	.text._ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS2_INS0_6ObjectEEE:
.LFB24259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	80(%rdi), %rdi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	-52(%rdi), %eax
	movl	$1, -48(%rbp)
	movq	%rcx, -40(%rbp)
	cmpl	$10, %eax
	ja	.L1548
	movl	$1218, %ecx
	btq	%rax, %rcx
	jnc	.L1536
	leaq	-48(%rbp), %rsi
	call	_ZN2v88internal13FeedbackNexus20ConfigureHandlerModeERKNS0_17MaybeObjectHandleE@PLT
.L1537:
	movl	28(%rbx), %eax
	leaq	.LC37(%rip), %rcx
	movb	$1, 16(%rbx)
	subl	$6, %eax
	cmpl	$2, %eax
	leaq	.LC38(%rip), %rax
	cmovnb	%rax, %rcx
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.L1549
	movq	(%rax), %rsi
.L1541:
	movl	96(%rbx), %edx
	movq	8(%rbx), %rdi
	call	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1550
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1548:
	.cfi_restore_state
	cmpl	$14, %eax
	jbe	.L1536
.L1535:
	xorl	%esi, %esi
	jmp	.L1538
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	88(%rbx), %rsi
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1536:
	movl	$25352, %ecx
	btq	%rax, %rcx
	jnc	.L1535
.L1538:
	leaq	-48(%rbp), %rcx
	call	_ZN2v88internal13FeedbackNexus20ConfigureMonomorphicENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE@PLT
	jmp	.L1537
.L1550:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24259:
	.size	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE
	.type	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE, @function
_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE:
.LFB24260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rcx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	80(%rdi), %rdi
	subq	$8, %rsp
	movl	-52(%rdi), %eax
	cmpl	$10, %eax
	ja	.L1565
	movl	$1218, %ecx
	btq	%rax, %rcx
	jnc	.L1554
	call	_ZN2v88internal13FeedbackNexus20ConfigureHandlerModeERKNS0_17MaybeObjectHandleE@PLT
.L1555:
	movl	28(%rbx), %eax
	leaq	.LC37(%rip), %rcx
	movb	$1, 16(%rbx)
	subl	$6, %eax
	cmpl	$2, %eax
	leaq	.LC38(%rip), %rax
	cmovnb	%rax, %rcx
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.L1566
	movq	(%rax), %rsi
.L1559:
	movl	96(%rbx), %edx
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	.p2align 4,,10
	.p2align 3
.L1565:
	.cfi_restore_state
	cmpl	$14, %eax
	jbe	.L1554
.L1553:
	xorl	%r8d, %r8d
	jmp	.L1556
	.p2align 4,,10
	.p2align 3
.L1566:
	movq	88(%rbx), %rsi
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1554:
	movl	$25352, %ecx
	btq	%rax, %rcx
	jnc	.L1553
.L1556:
	movq	%rsi, %rcx
	movq	%r8, %rsi
	call	_ZN2v88internal13FeedbackNexus20ConfigureMonomorphicENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE@PLT
	jmp	.L1555
	.cfi_endproc
.LFE24260:
	.size	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE, .-_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE
	.section	.rodata._ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE.str1.1,"aMS",@progbits,1
.LC39:
	.string	"Polymorphic"
	.section	.text._ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE
	.type	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE, @function
_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE:
.LFB24261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	28(%rdi), %eax
	cmpl	$14, %eax
	ja	.L1568
	movl	$25352, %edi
	btq	%rax, %rdi
	jnc	.L1568
.L1569:
	leaq	80(%rbx), %rdi
	call	_ZN2v88internal13FeedbackNexus20ConfigurePolymorphicENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE@PLT
	movq	80(%rbx), %rax
	movb	$1, 16(%rbx)
	testq	%rax, %rax
	je	.L1576
	movq	(%rax), %rsi
.L1571:
	movl	96(%rbx), %edx
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	leaq	.LC39(%rip), %rcx
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	.p2align 4,,10
	.p2align 3
.L1576:
	.cfi_restore_state
	movq	88(%rbx), %rsi
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1568:
	xorl	%esi, %esi
	jmp	.L1569
	.cfi_endproc
.LFE24261:
	.size	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE, .-_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE
	.section	.text._ZN2v88internal2IC19UpdateMonomorphicICERKNS0_17MaybeObjectHandleENS0_6HandleINS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC19UpdateMonomorphicICERKNS0_17MaybeObjectHandleENS0_6HandleINS0_4NameEEE
	.type	_ZN2v88internal2IC19UpdateMonomorphicICERKNS0_17MaybeObjectHandleENS0_6HandleINS0_4NameEEE, @function
_ZN2v88internal2IC19UpdateMonomorphicICERKNS0_17MaybeObjectHandleENS0_6HandleINS0_4NameEEE:
.LFB24275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	80(%rdi), %rdi
	subq	$8, %rsp
	movl	-52(%rdi), %eax
	movq	-48(%rdi), %r9
	cmpl	$10, %eax
	ja	.L1591
	movl	$1218, %edx
	btq	%rax, %rdx
	jnc	.L1580
	call	_ZN2v88internal13FeedbackNexus20ConfigureHandlerModeERKNS0_17MaybeObjectHandleE@PLT
.L1581:
	movl	28(%rbx), %eax
	leaq	.LC37(%rip), %rcx
	movb	$1, 16(%rbx)
	subl	$6, %eax
	cmpl	$2, %eax
	leaq	.LC38(%rip), %rax
	cmovnb	%rax, %rcx
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.L1592
	movq	(%rax), %rsi
.L1585:
	movl	96(%rbx), %edx
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	.p2align 4,,10
	.p2align 3
.L1591:
	.cfi_restore_state
	cmpl	$14, %eax
	jbe	.L1580
.L1579:
	xorl	%r8d, %r8d
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	88(%rbx), %rsi
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1580:
	movl	$25352, %edx
	btq	%rax, %rdx
	jnc	.L1579
.L1582:
	movq	%rsi, %rcx
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal13FeedbackNexus20ConfigureMonomorphicENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE@PLT
	jmp	.L1581
	.cfi_endproc
.LFE24275:
	.size	_ZN2v88internal2IC19UpdateMonomorphicICERKNS0_17MaybeObjectHandleENS0_6HandleINS0_4NameEEE, .-_ZN2v88internal2IC19UpdateMonomorphicICERKNS0_17MaybeObjectHandleENS0_6HandleINS0_4NameEEE
	.section	.rodata._ZN2v88internal2IC24CopyICToMegamorphicCacheENS0_6HandleINS0_4NameEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC40:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.rodata._ZN2v88internal2IC24CopyICToMegamorphicCacheENS0_6HandleINS0_4NameEEE.str1.1,"aMS",@progbits,1
.LC41:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal2IC24CopyICToMegamorphicCacheENS0_6HandleINS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC24CopyICToMegamorphicCacheENS0_6HandleINS0_4NameEEE
	.type	_ZN2v88internal2IC24CopyICToMegamorphicCacheENS0_6HandleINS0_4NameEEE, @function
_ZN2v88internal2IC24CopyICToMegamorphicCacheENS0_6HandleINS0_4NameEEE:
.LFB24276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	addq	$80, %rdi
	pushq	%r12
	leaq	-64(%rbp), %rdx
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	leaq	-96(%rbp), %rsi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	$0, -48(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	_ZNK2v88internal13FeedbackNexus22ExtractMapsAndHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EE@PLT
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rcx
	movq	-64(%rbp), %r9
	cmpq	%rcx, %r8
	je	.L1604
	xorl	%r12d, %r12d
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1623:
	testq	%rax, %rax
	je	.L1602
	movq	(%rax), %rcx
	orq	$2, %rcx
.L1601:
	movq	(%r8,%r12,8), %rax
	movq	(%rbx), %rsi
	movq	%r10, %rdi
	movq	(%rax), %rdx
	call	_ZN2v88internal9StubCache3SetENS0_4NameENS0_3MapENS0_11MaybeObjectE@PLT
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rcx
	movq	-64(%rbp), %r9
.L1596:
	movq	%rcx, %rax
	addq	$1, %r12
	subq	%r8, %rax
	sarq	$3, %rax
	cmpq	%rax, %r12
	jnb	.L1604
.L1603:
	movq	-56(%rbp), %rdx
	subq	%r9, %rdx
	sarq	$4, %rdx
	cmpq	%r12, %rdx
	jbe	.L1622
	movl	28(%r13), %eax
	cmpl	$9, %eax
	je	.L1596
	subl	$5, %eax
	movq	8(%r13), %rdx
	cmpl	$3, %eax
	jbe	.L1597
	movq	41032(%rdx), %r10
.L1598:
	movq	%r12, %rdi
	salq	$4, %rdi
	addq	%r9, %rdi
	movl	(%rdi), %edx
	movq	8(%rdi), %rax
	testl	%edx, %edx
	je	.L1623
	testq	%rax, %rax
	je	.L1602
	movq	(%rax), %rcx
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	41024(%rdx), %r10
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1602:
	leaq	.LC41(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1604:
	testq	%r9, %r9
	je	.L1605
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	movq	-96(%rbp), %r8
.L1605:
	testq	%r8, %r8
	je	.L1593
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L1593:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1624
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1622:
	.cfi_restore_state
	movq	%r12, %rsi
	leaq	.LC40(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1624:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24276:
	.size	_ZN2v88internal2IC24CopyICToMegamorphicCacheENS0_6HandleINS0_4NameEEE, .-_ZN2v88internal2IC24CopyICToMegamorphicCacheENS0_6HandleINS0_4NameEEE
	.section	.rodata._ZN2v88internal2IC31IsTransitionOfMonomorphicTargetENS0_3MapES2_.str1.1,"aMS",@progbits,1
.LC42:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZN2v88internal2IC31IsTransitionOfMonomorphicTargetENS0_3MapES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC31IsTransitionOfMonomorphicTargetENS0_3MapES2_
	.type	_ZN2v88internal2IC31IsTransitionOfMonomorphicTargetENS0_3MapES2_, @function
_ZN2v88internal2IC31IsTransitionOfMonomorphicTargetENS0_3MapES2_:
.LFB24277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	je	.L1651
	movq	%rdx, %rbx
	testq	%rdx, %rdx
	jne	.L1627
.L1631:
	xorl	%eax, %eax
.L1625:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1675
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1627:
	.cfi_restore_state
	movl	15(%rsi), %edx
	movq	%rdi, %r12
	movq	%rsi, %rax
	andl	$1048576, %edx
	jne	.L1628
.L1632:
	movzbl	14(%rbx), %esi
	movzbl	14(%rax), %edi
	shrl	$3, %esi
	shrl	$3, %edi
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	testb	%al, %al
	jne	.L1629
	xorl	%r12d, %r12d
.L1630:
	cmpq	%r12, %rbx
	sete	%al
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1651:
	movl	$1, %eax
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	8(%r12), %r14
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1633
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L1634:
	movq	-72(%rbp), %rdx
	cmpq	-64(%rbp), %rdx
	je	.L1636
	movq	%r13, (%rdx)
	addq	$8, -72(%rbp)
.L1637:
	movq	8(%r12), %rsi
	leaq	-88(%rbp), %rdi
	leaq	-80(%rbp), %rdx
	call	_ZN2v88internal3Map31FindElementsKindTransitionedMapEPNS0_7IsolateERKSt6vectorINS0_6HandleIS1_EESaIS6_EE@PLT
	movq	-80(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1630
	call	_ZdlPv@PLT
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1628:
	movl	15(%rsi), %edx
	andl	$4194304, %edx
	je	.L1631
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1633:
	movq	41088(%r14), %r13
	cmpq	41096(%r14), %r13
	je	.L1676
.L1635:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r14)
	movq	%rbx, 0(%r13)
	jmp	.L1634
	.p2align 4,,10
	.p2align 3
.L1636:
	movq	-80(%rbp), %r8
	movq	%rdx, %rsi
	movabsq	$1152921504606846975, %r15
	subq	%r8, %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%r15, %rax
	je	.L1677
	testq	%rax, %rax
	je	.L1678
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	ja	.L1679
	testq	%rcx, %rcx
	jne	.L1680
	movq	$8, -96(%rbp)
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.L1641:
	movq	%r13, (%r14,%rsi)
	cmpq	%r8, %rdx
	je	.L1642
	leaq	-8(%rdx), %rsi
	leaq	15(%r14), %rax
	subq	%r8, %rsi
	subq	%r8, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L1654
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L1654
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L1644:
	movdqu	(%r8,%rdx), %xmm1
	movups	%xmm1, (%r14,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rax
	jne	.L1644
	movq	%rdi, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rdx
	leaq	(%r8,%rdx), %rax
	addq	%r14, %rdx
	cmpq	%rdi, %r9
	je	.L1646
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L1646:
	leaq	16(%r14,%rsi), %rax
	movq	%rax, -96(%rbp)
.L1642:
	testq	%r8, %r8
	je	.L1647
	movq	%r8, %rdi
	call	_ZdlPv@PLT
.L1647:
	movq	%r14, %xmm0
	movq	%r15, -64(%rbp)
	movhps	-96(%rbp), %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L1637
.L1676:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L1635
.L1678:
	movl	$8, %r15d
.L1639:
	movq	%r15, %rdi
	movq	%rsi, -120(%rbp)
	movq	%r8, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %r8
	movq	%rax, %r14
	addq	%rax, %r15
	leaq	8(%rax), %rax
	movq	-120(%rbp), %rsi
	movq	%rax, -96(%rbp)
	jmp	.L1641
.L1679:
	movabsq	$9223372036854775800, %r15
	jmp	.L1639
.L1654:
	movq	%r14, %rdi
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L1643:
	movq	(%rax), %r9
	addq	$8, %rax
	addq	$8, %rdi
	movq	%r9, -8(%rdi)
	cmpq	%rax, %rdx
	jne	.L1643
	jmp	.L1646
.L1680:
	cmpq	%r15, %rcx
	cmova	%r15, %rcx
	leaq	0(,%rcx,8), %r15
	jmp	.L1639
.L1675:
	call	__stack_chk_fail@PLT
.L1677:
	leaq	.LC42(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24277:
	.size	_ZN2v88internal2IC31IsTransitionOfMonomorphicTargetENS0_3MapES2_, .-_ZN2v88internal2IC31IsTransitionOfMonomorphicTargetENS0_3MapES2_
	.section	.text._ZN2v88internal2IC10stub_cacheEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC10stub_cacheEv
	.type	_ZN2v88internal2IC10stub_cacheEv, @function
_ZN2v88internal2IC10stub_cacheEv:
.LFB24281:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %eax
	movq	8(%rdi), %rdx
	subl	$5, %eax
	cmpl	$3, %eax
	jbe	.L1682
	movq	41032(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1682:
	movq	41024(%rdx), %rax
	ret
	.cfi_endproc
.LFE24281:
	.size	_ZN2v88internal2IC10stub_cacheEv, .-_ZN2v88internal2IC10stub_cacheEv
	.section	.text._ZN2v88internal2IC22UpdateMegamorphicCacheENS0_6HandleINS0_3MapEEENS2_INS0_4NameEEERKNS0_17MaybeObjectHandleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC22UpdateMegamorphicCacheENS0_6HandleINS0_3MapEEENS2_INS0_4NameEEERKNS0_17MaybeObjectHandleE
	.type	_ZN2v88internal2IC22UpdateMegamorphicCacheENS0_6HandleINS0_3MapEEENS2_INS0_4NameEEERKNS0_17MaybeObjectHandleE, @function
_ZN2v88internal2IC22UpdateMegamorphicCacheENS0_6HandleINS0_3MapEEENS2_INS0_4NameEEERKNS0_17MaybeObjectHandleE:
.LFB24282:
	.cfi_startproc
	endbr64
	movl	28(%rdi), %eax
	cmpl	$9, %eax
	je	.L1684
	subl	$5, %eax
	movq	8(%rdi), %rdi
	cmpl	$3, %eax
	jbe	.L1686
	movq	8(%rcx), %rax
	movl	(%rcx), %ecx
	movq	41032(%rdi), %rdi
	testl	%ecx, %ecx
	jne	.L1688
.L1697:
	testq	%rax, %rax
	je	.L1691
	movq	(%rsi), %r8
	movq	(%rax), %rcx
	movq	(%rdx), %rsi
	orq	$2, %rcx
	movq	%r8, %rdx
	jmp	_ZN2v88internal9StubCache3SetENS0_4NameENS0_3MapENS0_11MaybeObjectE@PLT
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	8(%rcx), %rax
	movl	(%rcx), %ecx
	movq	41024(%rdi), %rdi
	testl	%ecx, %ecx
	je	.L1697
.L1688:
	testq	%rax, %rax
	je	.L1691
	movq	(%rsi), %r8
	movq	(%rax), %rcx
	movq	(%rdx), %rsi
	movq	%r8, %rdx
	jmp	_ZN2v88internal9StubCache3SetENS0_4NameENS0_3MapENS0_11MaybeObjectE@PLT
	.p2align 4,,10
	.p2align 3
.L1684:
	ret
	.p2align 4,,10
	.p2align 3
.L1691:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC41(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24282:
	.size	_ZN2v88internal2IC22UpdateMegamorphicCacheENS0_6HandleINS0_3MapEEENS2_INS0_4NameEEERKNS0_17MaybeObjectHandleE, .-_ZN2v88internal2IC22UpdateMegamorphicCacheENS0_6HandleINS0_3MapEEENS2_INS0_4NameEEERKNS0_17MaybeObjectHandleE
	.section	.text._ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE
	.type	_ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE, @function
_ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE:
.LFB24283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	48(%rsi), %rbx
	movq	8(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$9, 28(%rdi)
	je	.L1699
	movq	(%rbx), %rax
	testb	$1, %al
	jne	.L2054
.L1699:
	movl	4(%r13), %eax
	movq	32(%r12), %r15
	movq	56(%r13), %r14
	cmpl	$3, %eax
	je	.L1721
	cmpq	%r14, %rbx
	je	.L1722
	testq	%rbx, %rbx
	je	.L1723
	testq	%r14, %r14
	je	.L1724
	movq	(%r14), %rdx
	cmpq	(%rbx), %rdx
	sete	%r9b
	cmpl	$7, %eax
	ja	.L1725
	leaq	.L1727(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE,"a",@progbits
	.align 4
	.align 4
.L1727:
	.long	.L1726-.L1727
	.long	.L1731-.L1727
	.long	.L1730-.L1727
	.long	.L1725-.L1727
	.long	.L1726-.L1727
	.long	.L1729-.L1727
	.long	.L1728-.L1727
	.long	.L1726-.L1727
	.section	.text._ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE
	.p2align 4,,10
	.p2align 3
.L1723:
	cmpl	$7, %eax
	ja	.L1725
	leaq	.L1732(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE
	.align 4
	.align 4
.L1732:
	.long	.L1726-.L1732
	.long	.L1731-.L1732
	.long	.L1924-.L1732
	.long	.L1725-.L1732
	.long	.L1726-.L1732
	.long	.L1926-.L1732
	.long	.L1914-.L1732
	.long	.L1726-.L1732
	.section	.text._ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE
.L1926:
	xorl	%r9d, %r9d
.L1729:
	movq	32(%r13), %rdx
	movq	%r8, %rdi
	leaq	-112(%rbp), %rcx
	movq	%r15, %rsi
	movb	%r9b, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	$0, -112(%rbp)
	call	_ZN2v88internal9Accessors23IsJSObjectFieldAccessorEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_4NameEEEPNS0_10FieldIndexE@PLT
	movq	-128(%rbp), %r8
	movzbl	-136(%rbp), %r9d
	testb	%al, %al
	je	.L1749
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2055
.L1750:
	movq	-112(%rbp), %rax
	xorl	%esi, %esi
	movq	8(%r12), %rbx
	movq	%rax, %rdx
	movq	41112(%rbx), %rdi
	shrq	$15, %rdx
	andl	$3, %edx
	cmpl	$1, %edx
	movq	%rax, %rdx
	sete	%sil
	shrq	$14, %rdx
	sall	$7, %esi
	andl	$1, %edx
	sall	$5, %eax
	sall	$6, %edx
	andl	$524032, %eax
	orl	%edx, %esi
	orl	%eax, %esi
	orl	$4, %esi
	salq	$32, %rsi
	testq	%rdi, %rdi
	jne	.L2042
.L1787:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2056
.L1789:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1707
.L1914:
	movq	(%r14), %rdx
	xorl	%r9d, %r9d
.L1728:
	movq	-1(%rdx), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L1854
	movq	-1(%rdx), %rax
	cmpw	$1025, 11(%rax)
	je	.L2057
	movq	41112(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1860
	movabsq	$8589934592, %rsi
	movb	%r9b, -128(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-128(%rbp), %r9d
	movq	%rax, %rcx
.L1861:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2058
.L1863:
	testb	%r9b, %r9b
	jne	.L2041
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2059
.L1865:
	testb	$4, 16(%r13)
	je	.L1886
	movq	%r13, %rdi
	movq	%rcx, -128(%rbp)
	call	_ZNK2v88internal14LookupIterator12GetDataValueEv@PLT
	movq	-128(%rbp), %rcx
	movq	(%rax), %rsi
	testb	$1, %sil
	jne	.L2060
.L1878:
	movq	8(%r12), %rbx
	movq	41112(%rbx), %rdi
.L1884:
	movl	$1, %r13d
	testq	%rdi, %rdi
	je	.L1887
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L2040:
	movq	8(%r12), %rdx
	movq	%r9, %rbx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1894
	movabsq	$21474836480, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L1895:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2061
.L1897:
	xorl	%eax, %eax
	xorl	%edx, %edx
	movl	%r13d, %r8d
	movq	%rbx, %r9
	movabsq	$-4294967296, %rdi
	movq	%rax, %rsi
	andq	%rdi, %rsi
	movq	8(%r12), %rdi
	pushq	%rdx
	movq	%r14, %rdx
	orq	$1, %rsi
	pushq	%rsi
	movq	%r15, %rsi
	call	_ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_@PLT
	popq	%r8
	popq	%r9
	jmp	.L1707
.L1924:
	xorl	%r9d, %r9d
.L1730:
	movq	41112(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1733
	movabsq	$42949672960, %rsi
	movb	%r9b, -128(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-128(%rbp), %r9d
	movq	%rax, %rcx
.L1734:
	movq	(%r14), %rax
	movq	-1(%rax), %rax
.L2038:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L2062
.L1739:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	cmpq	%rax, %rdx
	je	.L1740
	movq	31(%rdx), %rax
.L1740:
	testb	$4, 75(%rax)
	je	.L1741
	movq	8(%r12), %r8
	movl	$1, -96(%rbp)
	leaq	104(%r8), %rax
	movq	%rax, -88(%rbp)
	testb	%r9b, %r9b
	jne	.L2063
.L1742:
	movl	$0, -96(%rbp)
	movq	%r14, -88(%rbp)
.L1743:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2064
.L1744:
	leaq	-96(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internal11LoadHandler13LoadFullChainEPNS0_7IsolateENS0_6HandleINS0_3MapEEERKNS0_17MaybeObjectHandleENS4_INS0_3SmiEEE@PLT
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1722:
	cmpl	$7, %eax
	ja	.L1725
	leaq	.L1910(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE
	.align 4
	.align 4
.L1910:
	.long	.L1726-.L1910
	.long	.L1731-.L1910
	.long	.L1921-.L1910
	.long	.L1725-.L1910
	.long	.L1726-.L1910
	.long	.L1923-.L1910
	.long	.L1922-.L1910
	.long	.L1726-.L1910
	.section	.text._ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE
.L1923:
	movl	$1, %r9d
	jmp	.L1729
.L1922:
	movq	(%r14), %rdx
	movl	$1, %r9d
	jmp	.L1728
.L1921:
	movl	$1, %r9d
	jmp	.L1730
	.p2align 4,,10
	.p2align 3
.L2054:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L1701
.L2035:
	xorl	%edx, %edx
	testb	%dl, %dl
	jne	.L1699
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	je	.L1708
.L2036:
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	testb	%dl, %dl
	jne	.L1699
	movq	-1(%rax), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1699
	movq	32(%r13), %rdx
	movq	(%rdx), %rcx
	cmpq	%rcx, 3104(%r8)
	jne	.L1699
	movq	-1(%rax), %rdx
	movzbl	13(%rdx), %edx
	testb	%dl, %dl
	jns	.L1718
	movq	-1(%rax), %rdx
	testb	$64, 13(%rdx)
	je	.L1718
.L1717:
	movq	-1(%rax), %rax
	movzbl	13(%rax), %eax
	testb	$1, %al
	jne	.L1699
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2065
.L1720:
	leaq	41184(%r8), %rdi
	movl	$121, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	jmp	.L1707
.L1731:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2066
.L1898:
	movq	41112(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1899
	movabsq	$51539607552, %rsi
.L2042:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	.p2align 4,,10
	.p2align 3
.L1707:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2067
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1725:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1701:
	movq	32(%rsi), %rdx
	movq	(%rdx), %rcx
	cmpq	%rcx, 2768(%r8)
	jne	.L2035
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2068
.L1706:
	leaq	41184(%r8), %rdi
	movl	$123, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1854:
	movq	%r13, %rdi
	movb	%r9b, -128(%rbp)
	call	_ZNK2v88internal14LookupIterator13GetFieldIndexEv@PLT
	xorl	%esi, %esi
	movq	8(%r12), %rbx
	movzbl	-128(%rbp), %r9d
	movq	%rax, %rdx
	shrq	$15, %rdx
	movq	41112(%rbx), %rdi
	andl	$3, %edx
	cmpl	$1, %edx
	movq	%rax, %rdx
	sete	%sil
	shrq	$14, %rdx
	sall	$7, %esi
	andl	$1, %edx
	sall	$5, %eax
	sall	$6, %edx
	andl	$524032, %eax
	orl	%edx, %esi
	orl	%eax, %esi
	orl	$4, %esi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L1866
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-128(%rbp), %r9d
	movq	%rax, %rcx
.L1867:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2069
.L1869:
	testb	%r9b, %r9b
	je	.L1870
.L2041:
	movq	%rcx, %rax
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1749:
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$1027, 11(%rdx)
	jne	.L1755
	movq	8(%r12), %rbx
	movq	23(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	7(%rax), %r15
	testq	%rdi, %rdi
	je	.L1756
	movq	%r15, %rsi
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-128(%rbp), %r8
	movq	(%rax), %r15
.L1757:
	movq	32(%r13), %rcx
	movq	(%rcx), %rax
	movq	%rcx, %rsi
	testb	$1, %al
	jne	.L1759
	sarq	$32, %rax
	movq	%rax, %rdx
	sall	$15, %eax
	subl	%edx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %edx
	movl	%edx, %eax
	shrl	$4, %eax
	xorl	%eax, %edx
	imull	$2057, %edx, %edx
	movl	%edx, %eax
	shrl	$16, %eax
	xorl	%eax, %edx
	andl	$1073741823, %edx
	salq	$32, %rdx
.L1783:
	movslq	35(%r15), %r13
	sarq	$32, %rdx
	movl	$1, %r14d
	movl	%edx, %ebx
	leal	-1(%r13), %eax
	movl	%eax, -148(%rbp)
	andl	%eax, %ebx
	movq	88(%r8), %rax
	movq	%rax, -136(%rbp)
	leaq	-1(%r15), %rax
	leaq	-96(%rbp), %r15
	movq	%rax, -144(%rbp)
	jmp	.L1786
	.p2align 4,,10
	.p2align 3
.L2071:
	movq	(%rcx), %rax
	movq	%r15, %rdi
	movq	%rcx, -128(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6Object9SameValueES1_@PLT
	movq	-128(%rbp), %rcx
	testb	%al, %al
	jne	.L2070
	leal	(%rbx,%r14), %edx
	movl	-148(%rbp), %ebx
	addl	$1, %r14d
	andl	%edx, %ebx
.L1786:
	leal	(%rbx,%rbx), %r13d
	movq	-144(%rbp), %rdi
	leal	40(,%r13,8), %eax
	cltq
	movq	(%rax,%rdi), %rsi
	cmpq	%rsi, -136(%rbp)
	jne	.L2071
	movabsq	$605590388736, %rsi
.L1784:
	movq	8(%r12), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L2042
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1733:
	movq	41088(%r8), %rcx
	cmpq	41096(%r8), %rcx
	je	.L2072
.L1735:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r8)
	movabsq	$42949672960, %rax
	movq	%rax, (%rcx)
	jmp	.L1734
	.p2align 4,,10
	.p2align 3
.L1721:
	cmpq	%r14, %rbx
	je	.L1919
	testq	%r14, %r14
	je	.L1920
	testq	%rbx, %rbx
	je	.L1920
	movq	(%r14), %rax
	cmpq	%rax, (%rbx)
	sete	%bl
.L1902:
	movq	41112(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1903
	movabsq	$47244640256, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1904:
	testb	%bl, %bl
	jne	.L1707
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	movq	8(%r12), %rdi
	movl	$1, %r8d
	movq	%rcx, %rdx
	pushq	%rbx
	movq	%rax, %rcx
	xorl	%r9d, %r9d
	movabsq	$-4294967296, %rsi
	andq	%rsi, %rdx
	movq	%r15, %rsi
	orq	$1, %rdx
	pushq	%rdx
	movq	%r14, %rdx
	call	_ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_@PLT
	popq	%rdx
	popq	%rcx
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1741:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testb	%r9b, %r9b
	je	.L1746
	testl	%eax, %eax
	je	.L2041
	movq	8(%r12), %rax
	movl	$1119, %esi
	movq	%rcx, -128(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	-128(%rbp), %rcx
	jmp	.L2041
	.p2align 4,,10
	.p2align 3
.L1724:
	cmpl	$7, %eax
	ja	.L1725
	leaq	.L1911(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE
	.align 4
	.align 4
.L1911:
	.long	.L1726-.L1911
	.long	.L1731-.L1911
	.long	.L1924-.L1911
	.long	.L1725-.L1911
	.long	.L1726-.L1911
	.long	.L1926-.L1911
	.long	.L1925-.L1911
	.long	.L1726-.L1911
	.section	.text._ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE
	.p2align 4,,10
	.p2align 3
.L1899:
	movq	41088(%r8), %rax
	cmpq	41096(%r8), %rax
	je	.L2073
.L1901:
	movabsq	$51539607552, %rcx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r8)
	movq	%rcx, (%rax)
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1903:
	movq	41088(%r8), %rax
	cmpq	41096(%r8), %rax
	je	.L2074
.L1905:
	movabsq	$47244640256, %rcx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r8)
	movq	%rcx, (%rax)
	jmp	.L1904
	.p2align 4,,10
	.p2align 3
.L2062:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rsi
	cmpw	$68, 11(%rdx)
	je	.L2038
	movq	(%rsi), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L1739
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L1739
.L1726:
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1746:
	testl	%eax, %eax
	jne	.L2075
.L1886:
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	8(%r12), %r10
	movl	$1, %r8d
	movq	%rsi, %rax
	pushq	%rdi
	movq	%r15, %rsi
	xorl	%r9d, %r9d
	movabsq	$-4294967296, %rdx
	movq	%r10, %rdi
	andq	%rdx, %rax
	movq	%r14, %rdx
	orq	$1, %rax
	pushq	%rax
	call	_ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_@PLT
	popq	%rsi
	popq	%rdi
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L2063:
	movl	28(%r12), %eax
	subl	$6, %eax
	cmpl	$1, %eax
	ja	.L1743
	jmp	.L1742
	.p2align 4,,10
	.p2align 3
.L1755:
	movq	%r13, %rdi
	movb	%r9b, -128(%rbp)
	call	_ZNK2v88internal14LookupIterator12GetAccessorsEv@PLT
	movzbl	-128(%rbp), %r9d
	movq	%rax, %rdx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L2076
.L1792:
	movq	39(%rax), %rax
	cmpq	%rax, _ZN2v88internal3Smi5kZeroE(%rip)
	je	.L1845
	cmpq	$0, 7(%rax)
	jne	.L2077
.L1845:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2078
.L1844:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1866:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L2079
.L1868:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L1867
	.p2align 4,,10
	.p2align 3
.L1860:
	movq	41088(%r8), %rcx
	cmpq	41096(%r8), %rcx
	je	.L2080
.L1862:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r8)
	movabsq	$8589934592, %rax
	movq	%rax, (%rcx)
	jmp	.L1861
	.p2align 4,,10
	.p2align 3
.L1920:
	xorl	%ebx, %ebx
	jmp	.L1902
	.p2align 4,,10
	.p2align 3
.L1925:
	movq	0, %rdx
	xorl	%r9d, %r9d
	jmp	.L1728
.L2056:
	movq	%rbx, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	jmp	.L1789
	.p2align 4,,10
	.p2align 3
.L1708:
	movq	23(%rax), %rdx
	testb	$1, %dl
	je	.L2036
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2036
	movq	32(%r13), %rdx
	movq	2768(%r8), %rcx
	cmpq	%rcx, (%rdx)
	jne	.L2036
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2081
.L1715:
	leaq	41184(%r8), %rdi
	movl	$124, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1919:
	movl	$1, %ebx
	jmp	.L1902
	.p2align 4,,10
	.p2align 3
.L2057:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2082
.L1856:
	movq	41112(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1857
	movabsq	$12884901888, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L1858:
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	movq	%r13, %rdi
	movq	%r10, -136(%rbp)
	movq	%rcx, %rax
	movq	%rbx, -120(%rbp)
	movabsq	$-4294967296, %rdx
	andq	%rdx, %rax
	orq	$1, %rax
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal14LookupIterator15GetPropertyCellEv@PLT
	movq	-128(%rbp), %rcx
	movq	-120(%rbp), %rbx
	xorl	%r8d, %r8d
	movq	8(%r12), %rdi
	movq	%rax, %r9
	movq	%r14, %rdx
	movq	%r15, %rsi
	pushq	%rbx
	movq	-136(%rbp), %r10
	pushq	%rcx
	movq	%r10, %rcx
	call	_ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_@PLT
	popq	%r10
	popq	%r11
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L1870:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L1865
	movq	8(%r12), %rax
	movl	$1115, %esi
	movq	%rcx, -128(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	-128(%rbp), %rcx
	jmp	.L1865
	.p2align 4,,10
	.p2align 3
.L2070:
	leal	4(%r13), %esi
	sall	$6, %esi
	orl	$13, %esi
	salq	$32, %rsi
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1756:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2083
.L1758:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r15, (%rax)
	jmp	.L1757
	.p2align 4,,10
	.p2align 3
.L2066:
	movq	40960(%r8), %rdi
	movl	$1118, %esi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	8(%r12), %r8
	jmp	.L1898
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	je	.L1761
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	jbe	.L2049
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L2084
	movq	-1(%rax), %rdx
	cmpw	$66, 11(%rdx)
	je	.L2085
	movq	-1(%rax), %rdx
	cmpw	$160, 11(%rdx)
	je	.L2086
	leaq	-96(%rbp), %rdi
	movq	%r8, -128(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal10JSReceiver15GetIdentityHashEv@PLT
	movq	32(%r13), %rcx
	movq	-128(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L1783
	.p2align 4,,10
	.p2align 3
.L2076:
	movq	-1(%rax), %rcx
	cmpw	$79, 11(%rcx)
	jne	.L1792
	movq	%r13, %rdi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88internal14LookupIterator23TryLookupCachedPropertyEv@PLT
	movzbl	-128(%rbp), %r9d
	movq	-136(%rbp), %rdx
	testb	%al, %al
	jne	.L2087
	movq	8(%r12), %rcx
	movq	(%rdx), %rax
	movq	41112(%rcx), %rdi
	movq	7(%rax), %rbx
	testq	%rdi, %rdi
	je	.L1796
	movq	%rbx, %rsi
	movb	%r9b, -128(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-128(%rbp), %r9d
	movq	(%rax), %rbx
	movq	%rax, %rdx
.L1797:
	testb	$1, %bl
	je	.L1845
	movq	-1(%rbx), %rax
	cmpw	$1105, 11(%rax)
	movq	(%rdx), %rax
	je	.L1803
	testb	$1, %al
	je	.L1845
	movq	-1(%rax), %rcx
	cmpw	$88, 11(%rcx)
	jne	.L1845
.L1803:
	leaq	-96(%rbp), %rbx
	testb	$1, %al
	jne	.L2088
.L1801:
	movq	8(%r12), %rsi
	movq	%rbx, %rdi
	movb	%r9b, -128(%rbp)
	call	_ZN2v88internal16CallOptimizationC1EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	cmpb	$0, -88(%rbp)
	je	.L2089
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal16CallOptimization23IsCompatibleReceiverMapENS0_6HandleINS0_3MapEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	je	.L1845
	movq	(%r14), %rax
	leaq	-104(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal10JSReceiver17HasFastPropertiesEPNS0_7IsolateE.isra.0
	testb	%al, %al
	je	.L1845
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNK2v88internal16CallOptimization26LookupHolderOfExpectedTypeENS0_6HandleINS0_3MapEEEPNS1_12HolderLookupE@PLT
	xorl	%eax, %eax
	movq	8(%r12), %rdx
	cmpl	$1, -104(%rbp)
	setne	%al
	addl	$8, %eax
	movq	41112(%rdx), %rdi
	movq	%rax, %rsi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L1815
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L1816:
	movq	8(%r12), %rdx
	movq	(%r14), %rax
	movq	%rbx, %rdi
	movq	%rdx, -128(%rbp)
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal16CallOptimization18GetAccessorContextENS0_3MapE@PLT
	movq	-128(%rbp), %rdx
	movq	%rax, %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2090
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L1822:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2091
.L1824:
	xorl	%eax, %eax
	movl	$4294967295, %ecx
	movq	8(%r12), %rdi
	pushq	%rbx
	movq	%rax, %rsi
	salq	$32, %rcx
	movq	-72(%rbp), %r9
	xorl	%r8d, %r8d
	andq	%rcx, %rsi
	pushq	%rsi
.L2044:
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_@PLT
	popq	%rbx
	popq	%r12
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L2074:
	movq	%r8, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %r8
	jmp	.L1905
	.p2align 4,,10
	.p2align 3
.L2072:
	movq	%r8, %rdi
	movb	%r9b, -136(%rbp)
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movzbl	-136(%rbp), %r9d
	movq	-128(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L1735
	.p2align 4,,10
	.p2align 3
.L2073:
	movq	%r8, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %r8
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L1857:
	movq	41088(%r8), %r10
	cmpq	41096(%r8), %r10
	je	.L2092
.L1859:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r8)
	movabsq	$12884901888, %rax
	movq	%rax, (%r10)
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L1718:
	movq	23(%rax), %rdx
	movl	47(%rdx), %edx
	andl	$31, %edx
	subl	$12, %edx
	cmpb	$3, %dl
	jbe	.L1717
	jmp	.L1699
.L2077:
	movq	%rdx, %rdi
	movq	%r15, %rsi
	movb	%r9b, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal12AccessorInfo23IsCompatibleReceiverMapENS0_6HandleIS1_EENS2_INS0_3MapEEE@PLT
	movq	-128(%rbp), %rdx
	movzbl	-136(%rbp), %r9d
	testb	%al, %al
	je	.L1845
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	jne	.L1845
	movq	(%rdx), %rax
	testb	$8, 19(%rax)
	je	.L1846
	movq	(%rbx), %rax
	testb	$1, %al
	je	.L1845
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L1845
.L1846:
	movq	%r13, %rdi
	movb	%r9b, -128(%rbp)
	call	_ZNK2v88internal14LookupIterator16GetAccessorIndexEv@PLT
	movq	8(%r12), %rdx
	movzbl	-128(%rbp), %r9d
	sall	$6, %eax
	movq	41112(%rdx), %rdi
	movl	%eax, %ebx
	orl	$7, %ebx
	salq	$32, %rbx
	testq	%rdi, %rdi
	je	.L1847
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-128(%rbp), %r9d
	movq	%rax, %r13
.L1848:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2093
.L1851:
	testb	%r9b, %r9b
	je	.L1852
.L2043:
	movq	%r13, %rax
	jmp	.L1707
	.p2align 4,,10
	.p2align 3
.L2064:
	movq	40960(%r8), %rdi
	movl	$1124, %esi
	movq	%rcx, -128(%rbp)
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	8(%r12), %r8
	movq	-128(%rbp), %rcx
	jmp	.L1744
.L2055:
	movq	8(%r12), %rax
	movl	$1114, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1750
.L2069:
	movq	8(%r12), %rax
	movl	$1114, %esi
	movq	%rcx, -136(%rbp)
	movb	%r9b, -128(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	-136(%rbp), %rcx
	movzbl	-128(%rbp), %r9d
	jmp	.L1869
.L2079:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	movb	%r9b, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movzbl	-128(%rbp), %r9d
	movq	%rax, %rcx
	jmp	.L1868
.L2084:
	movq	15(%rax), %rax
.L2049:
	movl	7(%rax), %edx
	testb	$1, %dl
	jne	.L1778
	shrl	$2, %edx
.L1782:
	salq	$32, %rdx
	movq	%rsi, %rcx
	jmp	.L1783
.L2058:
	movq	8(%r12), %rax
	movl	$1125, %esi
	movq	%rcx, -136(%rbp)
	movb	%r9b, -128(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	-136(%rbp), %rcx
	movzbl	-128(%rbp), %r9d
	jmp	.L1863
.L1761:
	movq	7(%rax), %rdx
	movq	%rdx, %xmm0
	ucomisd	%xmm0, %xmm0
	jp	.L2094
	comisd	.LC2(%rip), %xmm0
	jb	.L1766
	movsd	.LC1(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1766
	cvttsd2sil	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1766
	jne	.L1766
	movl	%ecx, %eax
	sall	$15, %eax
	subl	%ecx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%eax, %edx
	andl	$1073741823, %edx
	jmp	.L1782
.L2080:
	movq	%r8, %rdi
	movb	%r9b, -136(%rbp)
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movzbl	-136(%rbp), %r9d
	movq	-128(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L1862
.L2075:
	movq	8(%r12), %rax
	movl	$1120, %esi
	movq	%rcx, -128(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	-128(%rbp), %rcx
	jmp	.L1886
.L1894:
	movq	41088(%rdx), %rcx
	cmpq	41096(%rdx), %rcx
	je	.L2095
.L1896:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rdx)
	movabsq	$21474836480, %rax
	movq	%rax, (%rcx)
	jmp	.L1895
.L2078:
	movq	8(%r12), %rax
	movl	$1129, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1844
.L1766:
	movq	%rdx, %rax
	salq	$18, %rax
	subq	%rdx, %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,4), %rax
.L2045:
	movq	%rax, %rdx
	shrq	$11, %rdx
	xorq	%rdx, %rax
	movq	%rax, %rdx
	salq	$6, %rdx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$22, %rdx
	xorq	%rax, %rdx
	andl	$1073741823, %edx
	jmp	.L1782
.L2083:
	movq	%rbx, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %r8
	jmp	.L1758
.L2060:
	movq	-1(%rsi), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1875
	movq	-1(%rsi), %rdx
	movzwl	11(%rdx), %edx
	andl	$7, %edx
	cmpw	$5, %dx
	jne	.L1875
	movq	8(%r12), %rbx
	movq	15(%rsi), %r13
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2030
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-128(%rbp), %rcx
.L1875:
	movq	(%rax), %rsi
	testb	$1, %sil
	je	.L1878
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L2039
	movq	-1(%rsi), %rax
	cmpw	$63, 11(%rax)
	ja	.L1886
	movq	-1(%rsi), %rax
	movzwl	11(%rax), %eax
	testl	$65504, %eax
	jne	.L1886
.L2039:
	movq	8(%r12), %rbx
	xorl	%eax, %eax
	movq	41112(%rbx), %rdi
	testb	%al, %al
	jne	.L1884
	testq	%rdi, %rdi
	je	.L1891
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r9
.L1892:
	xorl	%r13d, %r13d
	jmp	.L2040
.L1778:
	leaq	-96(%rbp), %rdi
	movq	%r8, -128(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal6String17ComputeAndSetHashEv@PLT
	movq	32(%r13), %rsi
	movq	-128(%rbp), %r8
	movl	%eax, %edx
	jmp	.L1782
.L2082:
	movq	40960(%r8), %rdi
	movl	$1116, %esi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	8(%r12), %r8
	jmp	.L1856
.L2068:
	movq	40960(%r8), %rdi
	movl	$1130, %esi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	8(%r12), %r8
	jmp	.L1706
.L2092:
	movq	%r8, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %r8
	movq	%rax, %r10
	jmp	.L1859
.L1887:
	movq	41088(%rbx), %r9
	cmpq	41096(%rbx), %r9
	je	.L2096
.L1889:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r9)
	jmp	.L2040
.L1891:
	movq	41088(%rbx), %r9
	cmpq	41096(%rbx), %r9
	je	.L2097
.L1893:
	leaq	8(%r9), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r9)
	jmp	.L1892
.L2059:
	movq	8(%r12), %rax
	movl	$1126, %esi
	movq	%rcx, -128(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	-128(%rbp), %rcx
	jmp	.L1865
.L2087:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE
	jmp	.L1707
.L1796:
	movq	41088(%rcx), %rdx
	cmpq	%rdx, 41096(%rcx)
	je	.L2098
.L1798:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rbx, (%rdx)
	jmp	.L1797
.L2085:
	movl	7(%rax), %ecx
	xorl	%edx, %edx
	andl	$2147483646, %ecx
	je	.L1782
	imulq	$262143, 15(%rax), %rax
	subq	$1, %rax
	movq	%rax, %rdx
	shrq	$31, %rdx
	xorq	%rdx, %rax
	imulq	$21, %rax, %rax
	jmp	.L2045
.L2061:
	movq	8(%r12), %rax
	movl	$1113, %esi
	movq	%rcx, -128(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	-128(%rbp), %rcx
	jmp	.L1897
.L2086:
	leaq	-96(%rbp), %rdi
	movq	%r8, -128(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal18SharedFunctionInfo4HashEv@PLT
	movq	32(%r13), %rcx
	movq	-128(%rbp), %r8
	movl	%eax, %edx
	andl	$2147483647, %edx
	salq	$32, %rdx
	jmp	.L1783
.L2095:
	movq	%rdx, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rdx
	movq	%rax, %rcx
	jmp	.L1896
.L2065:
	movq	40960(%r8), %rdi
	movl	$1106, %esi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	8(%r12), %r8
	jmp	.L1720
.L2094:
	movl	$2147483647, %edx
	salq	$32, %rdx
	jmp	.L1783
.L2030:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2099
.L1880:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	jmp	.L1875
.L2067:
	call	__stack_chk_fail@PLT
.L2096:
	movq	%rbx, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L1889
.L2097:
	movq	%rbx, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L1893
.L2089:
	movq	(%r14), %rax
	leaq	-104(%rbp), %rdi
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal10JSReceiver17HasFastPropertiesEPNS0_7IsolateE.isra.0
	movzbl	-128(%rbp), %r9d
	testb	%al, %al
	je	.L1825
	movq	%r13, %rdi
	call	_ZNK2v88internal14LookupIterator16GetAccessorIndexEv@PLT
	movq	8(%r12), %rdx
	movzbl	-128(%rbp), %r9d
	sall	$6, %eax
	movq	41112(%rdx), %rdi
	movl	%eax, %ebx
	orl	$6, %ebx
	salq	$32, %rbx
	testq	%rdi, %rdi
	je	.L1826
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-128(%rbp), %r9d
	movq	%rax, %r13
.L1827:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2100
.L1829:
	testb	%r9b, %r9b
	jne	.L2043
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2101
.L1853:
	xorl	%eax, %eax
	movl	$4294967295, %esi
	xorl	%edx, %edx
	movq	8(%r12), %rdi
	movq	%rax, %rcx
	salq	$32, %rsi
	pushq	%rdx
	movl	$1, %r8d
	andq	%rsi, %rcx
	xorl	%r9d, %r9d
	orq	$1, %rcx
	pushq	%rcx
	jmp	.L2044
.L1847:
	movq	41088(%rdx), %r13
	cmpq	41096(%rdx), %r13
	je	.L2102
.L1849:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rdx)
	movq	%rbx, 0(%r13)
	jmp	.L1848
.L2098:
	movq	%rcx, %rdi
	movb	%r9b, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movzbl	-136(%rbp), %r9d
	movq	-128(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L1798
.L1852:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L1853
	movq	8(%r12), %rax
	movl	$1122, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1853
.L2088:
	movq	-1(%rax), %rcx
	cmpw	$88, 11(%rcx)
	je	.L1805
.L1807:
	leaq	-96(%rbp), %rbx
	testb	$1, %al
	je	.L1801
	movq	-1(%rax), %rcx
	leaq	-96(%rbp), %rbx
	cmpw	$1105, 11(%rcx)
	jne	.L1801
	movq	%rdx, -136(%rbp)
	movq	%rbx, %rdi
	movb	%r9b, -128(%rbp)
	movq	23(%rax), %rax
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo12BreakAtEntryEv@PLT
	movzbl	-128(%rbp), %r9d
	movq	-136(%rbp), %rdx
	testb	%al, %al
	jne	.L1845
	jmp	.L1801
.L2099:
	movq	%rbx, %rdi
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rcx
	jmp	.L1880
.L1805:
	leaq	-104(%rbp), %rdi
	movq	%rdx, -136(%rbp)
	movb	%r9b, -128(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal20FunctionTemplateInfo12BreakAtEntryEv
	testb	%al, %al
	jne	.L1845
	movq	-136(%rbp), %rdx
	movzbl	-128(%rbp), %r9d
	movq	(%rdx), %rax
	jmp	.L1807
.L2093:
	movq	8(%r12), %rax
	movl	$1121, %esi
	movb	%r9b, -128(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movzbl	-128(%rbp), %r9d
	jmp	.L1851
.L2081:
	movq	40960(%r8), %rdi
	movl	$1131, %esi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	8(%r12), %r8
	jmp	.L1715
.L2102:
	movq	%rdx, %rdi
	movb	%r9b, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movzbl	-136(%rbp), %r9d
	movq	-128(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L1849
.L1826:
	movq	41088(%rdx), %r13
	cmpq	41096(%rdx), %r13
	je	.L2103
.L1828:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rdx)
	movq	%rbx, 0(%r13)
	jmp	.L1827
.L1825:
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	cmpw	$1025, 11(%rax)
	je	.L2104
	movq	8(%r12), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1837
	movl	$1, %esi
	movb	%r9b, -128(%rbp)
	salq	$33, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-128(%rbp), %r9d
	movq	%rax, %r13
.L1838:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2105
.L1840:
	testb	%r9b, %r9b
	jne	.L2043
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L1853
	movq	8(%r12), %rax
	movl	$1126, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1853
.L2090:
	movq	41088(%rdx), %rbx
	cmpq	%rbx, 41096(%rdx)
	je	.L2106
.L1823:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rbx)
	jmp	.L1822
.L1815:
	movq	41088(%rdx), %r13
	cmpq	41096(%rdx), %r13
	je	.L2107
.L1817:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, 0(%r13)
	jmp	.L1816
.L2101:
	movq	8(%r12), %rax
	movl	$1109, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1853
.L1837:
	movq	41088(%rbx), %r13
	cmpq	41096(%rbx), %r13
	je	.L2108
.L1839:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%rbx)
	movl	$1, %eax
	salq	$33, %rax
	movq	%rax, 0(%r13)
	jmp	.L1838
.L2104:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2109
.L1833:
	movq	8(%r12), %rdx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1834
	movl	$3, %esi
	salq	$32, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L1835:
	xorl	%r8d, %r8d
	movl	$4294967295, %edx
	xorl	%r9d, %r9d
	movq	%r13, %rdi
	movq	%r8, %rax
	salq	$32, %rdx
	movq	%r9, -120(%rbp)
	andq	%rdx, %rax
	orq	$1, %rax
	movq	%rax, -128(%rbp)
	call	_ZNK2v88internal14LookupIterator15GetPropertyCellEv@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %r9
	movq	%r14, %rdx
	movq	8(%r12), %rdi
	movq	%rbx, %rcx
	movq	%r15, %rsi
	pushq	%r9
	movq	%rax, %r9
	pushq	%r8
	xorl	%r8d, %r8d
	call	_ZN2v88internal11LoadHandler17LoadFromPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_@PLT
	popq	%r13
	popq	%r14
	jmp	.L1707
.L2108:
	movq	%rbx, %rdi
	movb	%r9b, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movzbl	-128(%rbp), %r9d
	movq	%rax, %r13
	jmp	.L1839
.L1834:
	movq	41088(%rdx), %rbx
	cmpq	41096(%rdx), %rbx
	je	.L2110
.L1836:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%rdx)
	movl	$3, %eax
	salq	$32, %rax
	movq	%rax, (%rbx)
	jmp	.L1835
.L2109:
	movq	8(%r12), %rax
	movl	$1117, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1833
.L2110:
	movq	%rdx, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L1836
.L2105:
	movq	8(%r12), %rax
	movl	$1125, %esi
	movb	%r9b, -128(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movzbl	-128(%rbp), %r9d
	jmp	.L1840
.L2106:
	movq	%rdx, %rdi
	movq	%rdx, -128(%rbp)
	movq	%rax, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L1823
.L2091:
	movq	8(%r12), %rax
	movl	$1110, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L1824
.L2103:
	movq	%rdx, %rdi
	movb	%r9b, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movzbl	-136(%rbp), %r9d
	movq	-128(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L1828
.L2100:
	movq	8(%r12), %rax
	movl	$1108, %esi
	movb	%r9b, -128(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movzbl	-128(%rbp), %r9d
	jmp	.L1829
.L2107:
	movq	%rdx, %rdi
	movq	%rsi, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %rdx
	movq	%rax, %r13
	jmp	.L1817
	.cfi_endproc
.LFE24283:
	.size	_ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE, .-_ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE
	.section	.text._ZN2v88internal11KeyedLoadIC27CanChangeToAllowOutOfBoundsENS0_6HandleINS0_3MapEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11KeyedLoadIC27CanChangeToAllowOutOfBoundsENS0_6HandleINS0_3MapEEE
	.type	_ZN2v88internal11KeyedLoadIC27CanChangeToAllowOutOfBoundsENS0_6HandleINS0_3MapEEE, @function
_ZN2v88internal11KeyedLoadIC27CanChangeToAllowOutOfBoundsENS0_6HandleINS0_3MapEEE:
.LFB24285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$80, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v88internal13FeedbackNexus17FindHandlerForMapENS0_6HandleINS0_3MapEEE@PLT
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	je	.L2112
	movq	(%rdx), %rdi
	movq	%rdi, %rdx
	orq	$2, %rdx
	testl	%eax, %eax
	cmove	%rdx, %rdi
	call	_ZN2v88internal11LoadHandler22GetKeyedAccessLoadModeENS0_11MaybeObjectE@PLT
	testl	%eax, %eax
	sete	%r8b
.L2112:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24285:
	.size	_ZN2v88internal11KeyedLoadIC27CanChangeToAllowOutOfBoundsENS0_6HandleINS0_3MapEEE, .-_ZN2v88internal11KeyedLoadIC27CanChangeToAllowOutOfBoundsENS0_6HandleINS0_3MapEEE
	.section	.text._ZN2v88internal11KeyedLoadIC18LoadElementHandlerENS0_6HandleINS0_3MapEEENS0_19KeyedAccessLoadModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11KeyedLoadIC18LoadElementHandlerENS0_6HandleINS0_3MapEEENS0_19KeyedAccessLoadModeE
	.type	_ZN2v88internal11KeyedLoadIC18LoadElementHandlerENS0_6HandleINS0_3MapEEENS0_19KeyedAccessLoadModeE, @function
_ZN2v88internal11KeyedLoadIC18LoadElementHandlerENS0_6HandleINS0_3MapEEENS0_19KeyedAccessLoadModeE:
.LFB24288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rax
	testb	$8, 13(%rax)
	jne	.L2196
	movq	8(%rdi), %r12
.L2119:
	movzwl	11(%rax), %ecx
	cmpw	$63, %cx
	jbe	.L2207
.L2197:
	cmpw	$1023, %cx
	jbe	.L2208
	cmpw	$1024, %cx
	je	.L2209
	movzbl	14(%rax), %r13d
	shrl	$3, %r13d
	leal	-13(%r13), %eax
	cmpb	$1, %al
	jbe	.L2210
	cmpw	$1061, %cx
	sete	%r15b
	cmpb	$12, %r13b
	je	.L2211
	movl	%r13d, %eax
	xorl	%r14d, %r14d
	andl	$-3, %eax
	cmpb	$1, %al
	je	.L2212
.L2166:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2213
.L2167:
	movl	%r13d, %esi
	movzbl	%r15b, %r13d
	movq	41112(%r12), %rdi
	sall	$7, %r13d
	sall	$9, %esi
	orl	%r13d, %esi
	xorl	%r13d, %r13d
	cmpl	$1, %edx
	sete	%r13b
	sall	$6, %r13d
	orl	%r13d, %esi
	orl	%r14d, %esi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L2168
.L2203:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2198:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2208:
	.cfi_restore_state
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2214
.L2150:
	cmpl	$9, 28(%rbx)
	leaq	41184(%r12), %rdi
	je	.L2215
	movl	$117, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2196:
	.cfi_restore_state
	movq	31(%rax), %rcx
	movq	%rax, %rdi
	testb	$1, %cl
	jne	.L2122
.L2123:
	movq	71(%rcx), %r8
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	movq	-37504(%rcx), %rcx
	cmpq	%rcx, %r8
	je	.L2124
	movq	39(%r8), %rcx
.L2124:
	movq	8(%rbx), %r12
	movq	88(%r12), %r8
	cmpq	7(%rcx), %r8
	jne	.L2125
	cmpl	$9, 28(%rbx)
	jne	.L2119
	movq	31(%rdi), %rcx
	testb	$1, %cl
	jne	.L2128
.L2129:
	movq	71(%rcx), %rdi
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	movq	-37504(%rcx), %rcx
	cmpq	%rcx, %rdi
	je	.L2130
	movq	39(%rdi), %rcx
.L2130:
	cmpq	%r8, 23(%rcx)
	je	.L2119
	.p2align 4,,10
	.p2align 3
.L2125:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L2216
.L2134:
	movq	71(%rax), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	cmpq	%rax, %rcx
	je	.L2135
	movq	39(%rcx), %rax
.L2135:
	testb	$4, 75(%rax)
	je	.L2136
	movq	(%rsi), %rax
	movzwl	11(%rax), %ecx
	cmpw	$63, %cx
	ja	.L2197
	.p2align 4,,10
	.p2align 3
.L2207:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2217
.L2144:
	cmpl	$9, 28(%rbx)
	je	.L2218
	xorl	%esi, %esi
	cmpl	$1, %edx
	movq	41112(%r12), %rdi
	sete	%sil
	sall	$6, %esi
	orl	$1, %esi
	salq	$32, %rsi
	testq	%rdi, %rdi
	jne	.L2203
.L2168:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2219
.L2170:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2215:
	.cfi_restore_state
	movl	$155, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2216:
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdi
	cmpw	$68, 11(%rcx)
	je	.L2125
	movq	(%rdi), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L2134
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	8(%rbx), %r12
	jmp	.L2134
	.p2align 4,,10
	.p2align 3
.L2211:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2220
.L2162:
	xorl	%esi, %esi
	cmpl	$1, %edx
	movzbl	%r15b, %r13d
	movq	41112(%r12), %rdi
	sete	%sil
	sall	$7, %r13d
	sall	$6, %esi
	orl	%r13d, %esi
	orl	$6144, %esi
	salq	$32, %rsi
	testq	%rdi, %rdi
	jne	.L2203
	jmp	.L2168
	.p2align 4,,10
	.p2align 3
.L2221:
	movq	31(%rcx), %rcx
	testb	$1, %cl
	je	.L2200
	.p2align 4,,10
	.p2align 3
.L2122:
	movq	-1(%rcx), %rax
	leaq	-1(%rcx), %r8
	cmpw	$68, 11(%rax)
	je	.L2221
	movq	(%r8), %rax
	cmpw	$1105, 11(%rax)
	jne	.L2200
	movq	23(%rcx), %rax
	movq	7(%rax), %rcx
	movq	(%rsi), %rdi
.L2200:
	movq	%rdi, %rax
	jmp	.L2123
	.p2align 4,,10
	.p2align 3
.L2218:
	leaq	41184(%r12), %rdi
	movl	$155, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2210:
	.cfi_restore_state
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2222
.L2158:
	cmpl	$9, 28(%rbx)
	leaq	41184(%r12), %rdi
	je	.L2223
	movl	$129, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2209:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2154
	movabsq	$47244640256, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2212:
	movq	%r12, %rdi
	movl	%edx, -56(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_134AllowConvertHoleElementToUndefinedEPNS0_7IsolateENS0_6HandleINS0_3MapEEE
	movq	8(%rbx), %r12
	movl	-56(%rbp), %edx
	movzbl	%al, %esi
	sall	$8, %esi
	movl	%esi, %r14d
	jmp	.L2166
	.p2align 4,,10
	.p2align 3
.L2136:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2224
.L2138:
	cmpl	$9, 28(%rbx)
	leaq	41184(%r12), %rdi
	je	.L2225
	movl	$130, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2154:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2226
.L2156:
	movabsq	$47244640256, %rbx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rbx, (%rax)
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2219:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L2170
	.p2align 4,,10
	.p2align 3
.L2214:
	movq	40960(%r12), %rdi
	movl	$1098, %esi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	8(%rbx), %r12
	jmp	.L2150
	.p2align 4,,10
	.p2align 3
.L2225:
	movl	$154, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	jmp	.L2198
.L2220:
	movq	40960(%r12), %rdi
	movl	$1095, %esi
	movl	%edx, -56(%rbp)
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	8(%rbx), %r12
	movl	-56(%rbp), %edx
	jmp	.L2162
.L2227:
	movq	31(%rcx), %rcx
	testb	$1, %cl
	je	.L2201
	.p2align 4,,10
	.p2align 3
.L2128:
	movq	-1(%rcx), %rax
	leaq	-1(%rcx), %r8
	cmpw	$68, 11(%rax)
	je	.L2227
	movq	(%r8), %rax
	cmpw	$1105, 11(%rax)
	je	.L2172
.L2201:
	movq	88(%r12), %r8
	movq	%rdi, %rax
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2217:
	movq	40960(%r12), %rdi
	movl	$1097, %esi
	movl	%edx, -56(%rbp)
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	8(%rbx), %r12
	movl	-56(%rbp), %edx
	jmp	.L2144
	.p2align 4,,10
	.p2align 3
.L2223:
	movl	$153, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2213:
	movq	40960(%r12), %rdi
	movl	$1095, %esi
	movl	%edx, -56(%rbp)
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	8(%rbx), %r12
	movl	-56(%rbp), %edx
	jmp	.L2167
.L2222:
	movq	40960(%r12), %rdi
	movl	$1094, %esi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	8(%rbx), %r12
	jmp	.L2158
.L2226:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L2156
.L2224:
	movq	40960(%r12), %rdi
	movl	$1096, %esi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	8(%rbx), %r12
	jmp	.L2138
.L2172:
	movq	23(%rcx), %rax
	movq	7(%rax), %rcx
	movq	8(%rbx), %r12
	movq	(%rsi), %rax
	movq	88(%r12), %r8
	jmp	.L2129
	.cfi_endproc
.LFE24288:
	.size	_ZN2v88internal11KeyedLoadIC18LoadElementHandlerENS0_6HandleINS0_3MapEEENS0_19KeyedAccessLoadModeE, .-_ZN2v88internal11KeyedLoadIC18LoadElementHandlerENS0_6HandleINS0_3MapEEENS0_19KeyedAccessLoadModeE
	.section	.text._ZN2v88internal11KeyedLoadIC11RuntimeLoadENS0_6HandleINS0_6ObjectEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11KeyedLoadIC11RuntimeLoadENS0_6HandleINS0_6ObjectEEES4_
	.type	_ZN2v88internal11KeyedLoadIC11RuntimeLoadENS0_6HandleINS0_6ObjectEEES4_, @function
_ZN2v88internal11KeyedLoadIC11RuntimeLoadENS0_6HandleINS0_6ObjectEEES4_:
.LFB24296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpl	$8, 28(%rdi)
	movq	8(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	je	.L2235
	movq	%r8, %rdi
	call	_ZN2v88internal7Runtime11HasPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2235:
	.cfi_restore_state
	xorl	%ecx, %ecx
	movq	%r8, %rdi
	call	_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24296:
	.size	_ZN2v88internal11KeyedLoadIC11RuntimeLoadENS0_6HandleINS0_6ObjectEEES4_, .-_ZN2v88internal11KeyedLoadIC11RuntimeLoadENS0_6HandleINS0_6ObjectEEES4_
	.section	.text._ZN2v88internal7StoreIC14LookupForWriteEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7StoreIC14LookupForWriteEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE
	.type	_ZN2v88internal7StoreIC14LookupForWriteEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE, @function
_ZN2v88internal7StoreIC14LookupForWriteEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE:
.LFB24298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	48(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L2338
.L2281:
	xorl	%r13d, %r13d
.L2236:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2339
	addq	$88, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2338:
	.cfi_restore_state
	movq	%rsi, %r12
	movq	-1(%rax), %rsi
	cmpw	$1024, 11(%rsi)
	je	.L2313
	movq	-1(%rax), %rax
	xorl	%r13d, %r13d
	cmpw	$1024, 11(%rax)
	jbe	.L2281
	movl	4(%r12), %eax
	cmpl	$7, %eax
	je	.L2246
	cmpl	$4, %eax
	je	.L2246
	movq	%rdi, %rbx
	leaq	.L2249(%rip), %r15
.L2282:
	cmpl	$7, %eax
	ja	.L2247
	movslq	(%r15,%rax,4), %rax
	addq	%r15, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7StoreIC14LookupForWriteEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE,"a",@progbits
	.align 4
	.align 4
.L2249:
	.long	.L2254-.L2249
	.long	.L2236-.L2249
	.long	.L2252-.L2249
	.long	.L2313-.L2249
	.long	.L2247-.L2249
	.long	.L2251-.L2249
	.long	.L2250-.L2249
	.long	.L2248-.L2249
	.section	.text._ZN2v88internal7StoreIC14LookupForWriteEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE
.L2271:
	movq	8(%rbx), %r12
	movq	-1(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2273
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2274:
	movq	%rax, 32(%rbx)
	.p2align 4,,10
	.p2align 3
.L2313:
	movl	$1, %r13d
	jmp	.L2236
	.p2align 4,,10
	.p2align 3
.L2252:
	movq	56(%r12), %r8
	movq	(%r8), %rax
	movq	-1(%rax), %rax
.L2336:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L2340
.L2258:
	movq	71(%rax), %rdi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdi
	je	.L2259
	movq	31(%rdi), %rsi
.L2259:
	movq	%r12, %rdi
	movl	%ecx, -124(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZNK2v88internal14LookupIterator33HolderIsReceiverOrHiddenPrototypeEv@PLT
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %rsi
	testb	%al, %al
	movq	-120(%rbp), %rdx
	movl	-124(%rbp), %ecx
	jne	.L2341
	movq	8(%rbx), %rax
	movq	88(%rax), %rax
	cmpq	7(%rsi), %rax
	jne	.L2236
	cmpq	23(%rsi), %rax
	jne	.L2236
.L2247:
	movq	%r12, %rdi
	movl	%ecx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	4(%r12), %eax
	movq	-104(%rbp), %rdx
	movl	-112(%rbp), %ecx
	cmpl	$4, %eax
	jne	.L2282
	.p2align 4,,10
	.p2align 3
.L2246:
	movq	48(%r12), %rsi
	movq	(%rsi), %rax
	leaq	-1(%rax), %rdi
	testb	$1, %al
	jne	.L2342
.L2289:
	movq	(%rdi), %rax
	movl	15(%rax), %eax
	testl	$134217728, %eax
	jne	.L2291
	cmpl	$-1, 72(%r12)
	jne	.L2236
	movq	32(%r12), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdi
	cmpw	$64, 11(%rdi)
	jne	.L2236
	testb	$1, 11(%rax)
	je	.L2236
	.p2align 4,,10
	.p2align 3
.L2291:
	movl	%ecx, %r8d
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	call	_ZN2v88internal14LookupIterator31PrepareTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesENS0_11StoreOriginE@PLT
	movq	40(%r12), %rax
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L2343
.L2294:
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L2308
	movq	48(%r12), %rax
	movq	(%rax), %rax
	leaq	-1(%rax), %rdx
	testb	$1, %al
	jne	.L2344
.L2307:
	movq	(%rdx), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	jne	.L2313
.L2308:
	movq	40(%r12), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	subq	$37592, %rax
	testb	$1, %cl
	je	.L2298
	leaq	-1(%rcx), %rdx
	movq	-1(%rcx), %rcx
	cmpq	%rcx, 136(%rax)
	je	.L2310
.L2298:
	movq	88(%rax), %rdx
	subq	$1, %rdx
.L2310:
	movq	(%rdx), %rax
	cmpw	$68, 11(%rax)
	sete	%r13b
	jmp	.L2236
	.p2align 4,,10
	.p2align 3
.L2250:
	testb	$8, 16(%r12)
	jne	.L2236
	movq	56(%r12), %rax
	cmpq	%rax, %r14
	je	.L2269
	movq	(%r14), %rsi
	testq	%rax, %rax
	je	.L2270
	cmpq	%rsi, (%rax)
	je	.L2269
.L2270:
	movq	-1(%rsi), %rax
	cmpw	$1026, 11(%rax)
	je	.L2345
	movq	%r12, %rdi
	movl	%ecx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZNK2v88internal14LookupIterator33HolderIsReceiverOrHiddenPrototypeEv@PLT
	testb	%al, %al
	jne	.L2236
	movq	(%r14), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	movq	-104(%rbp), %rdx
	movl	-112(%rbp), %ecx
	testl	$134217728, %eax
	jne	.L2279
	cmpl	$-1, 72(%r12)
	jne	.L2281
	movq	32(%r12), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L2281
	testb	$1, 11(%rax)
	je	.L2281
.L2279:
	movl	%ecx, %r8d
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator31PrepareTransitionToDataPropertyENS0_6HandleINS0_10JSReceiverEEENS2_INS0_6ObjectEEENS0_18PropertyAttributesENS0_11StoreOriginE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator21IsCacheableTransitionEv
	movl	%eax, %r13d
	jmp	.L2236
	.p2align 4,,10
	.p2align 3
.L2251:
	movl	16(%r12), %r13d
	shrl	$3, %r13d
	andl	$1, %r13d
	xorl	$1, %r13d
	jmp	.L2236
	.p2align 4,,10
	.p2align 3
.L2254:
	movq	56(%r12), %rax
	movq	(%rax), %rsi
	movq	-1(%rsi), %rax
	cmpw	$1026, 11(%rax)
	je	.L2346
	movq	-1(%rsi), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L2267:
	testb	%al, %al
	je	.L2247
	jmp	.L2236
.L2248:
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2342:
	movq	-1(%rax), %r8
	cmpw	$1026, 11(%r8)
	jne	.L2289
	movq	-1(%rax), %rax
	movq	23(%rax), %r14
	movq	-1(%r14), %rax
	cmpw	$1025, 11(%rax)
	jne	.L2289
	movq	24(%r12), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2286
	movq	%r14, %rsi
	movl	%ecx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %rdx
	movl	-112(%rbp), %ecx
	movq	%rax, %rsi
.L2287:
	movq	(%rsi), %rax
	leaq	-1(%rax), %rdi
	jmp	.L2289
	.p2align 4,,10
	.p2align 3
.L2340:
	movq	-1(%rax), %rsi
	leaq	-1(%rax), %rdi
	cmpw	$68, 11(%rsi)
	je	.L2336
	movq	(%rdi), %rsi
	cmpw	$1105, 11(%rsi)
	jne	.L2258
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L2258
.L2346:
	movq	%rsi, %r8
	leaq	-96(%rbp), %rdi
	movl	%ecx, -124(%rbp)
	andq	$-262144, %r8
	movq	%rdx, -120(%rbp)
	movq	24(%r8), %rax
	movq	%rsi, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	-25128(%rax), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %r8
	movq	-120(%rbp), %rdx
	movl	-124(%rbp), %ecx
	testb	$1, %sil
	movq	24(%r8), %rdi
	jne	.L2347
.L2265:
	movq	-1(%rsi), %rsi
	movq	23(%rsi), %rsi
.L2266:
	cmpq	%rax, %rsi
	setne	%al
	jmp	.L2267
.L2269:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator22PrepareForDataPropertyENS0_6HandleINS0_6ObjectEEE@PLT
	movq	(%r14), %rax
	testb	$1, %al
	jne	.L2271
	movq	8(%rbx), %rax
	addq	$256, %rax
	movq	%rax, 32(%rbx)
	jmp	.L2313
.L2343:
	movq	-1(%rax), %rdx
	cmpw	$159, 11(%rdx)
	jne	.L2294
	jmp	.L2313
.L2345:
	movq	8(%rbx), %rax
	leaq	-96(%rbp), %rdi
	movq	%r14, -80(%rbp)
	movq	$0, -88(%rbp)
	movq	%rax, -96(%rbp)
	movl	$0, -72(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	movq	56(%r12), %rdx
	movq	-80(%rbp), %rax
	cmpq	%rdx, %rax
	je	.L2313
	testq	%rax, %rax
	je	.L2281
	testq	%rdx, %rdx
	je	.L2281
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	sete	%r13b
	jmp	.L2236
.L2344:
	movq	-1(%rax), %rcx
	cmpw	$1026, 11(%rcx)
	jne	.L2307
	movq	-1(%rax), %rax
	movq	23(%rax), %r13
	movq	-1(%r13), %rax
	cmpw	$1025, 11(%rax)
	jne	.L2307
	movq	24(%r12), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2304
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L2305:
	movq	(%rax), %rdx
	subq	$1, %rdx
	jmp	.L2307
.L2273:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2348
.L2275:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%r13, (%rax)
	jmp	.L2274
.L2347:
	movq	-1(%rsi), %r8
	cmpw	$1024, 11(%r8)
	jne	.L2265
	movq	-37488(%rdi), %rsi
	jmp	.L2266
.L2341:
	testb	$4, 75(%rsi)
	jne	.L2236
	cmpq	%r8, %r14
	je	.L2262
	movq	(%r8), %rax
	cmpq	%rax, (%r14)
	jne	.L2236
.L2262:
	movq	8(%rbx), %rax
	movq	15(%rsi), %rbx
	cmpq	%rbx, 88(%rax)
	setne	%r13b
	jmp	.L2236
.L2286:
	movq	41088(%rbx), %rsi
	cmpq	41096(%rbx), %rsi
	je	.L2349
.L2288:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%rbx)
	movq	%r14, (%rsi)
	jmp	.L2287
.L2339:
	call	__stack_chk_fail@PLT
.L2348:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L2275
.L2304:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2350
.L2306:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	jmp	.L2305
.L2349:
	movq	%rbx, %rdi
	movl	%ecx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movl	-112(%rbp), %ecx
	movq	-104(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L2288
.L2350:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L2306
	.cfi_endproc
.LFE24298:
	.size	_ZN2v88internal7StoreIC14LookupForWriteEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE, .-_ZN2v88internal7StoreIC14LookupForWriteEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE
	.section	.rodata._ZN2v88internal7StoreIC14ComputeHandlerEPNS0_14LookupIteratorE.str1.1,"aMS",@progbits,1
.LC43:
	.string	"accessor on slow map"
.LC44:
	.string	"setter == kNullAddress"
	.section	.rodata._ZN2v88internal7StoreIC14ComputeHandlerEPNS0_14LookupIteratorE.str1.8,"aMS",@progbits,1
	.align 8
.LC45:
	.string	"special data property in prototype chain"
	.section	.rodata._ZN2v88internal7StoreIC14ComputeHandlerEPNS0_14LookupIteratorE.str1.1
.LC46:
	.string	"incompatible receiver type"
.LC47:
	.string	"setter not a function"
.LC48:
	.string	"incompatible receiver"
.LC49:
	.string	"setter non-simple template"
.LC50:
	.string	"constant property"
	.section	.text._ZN2v88internal7StoreIC14ComputeHandlerEPNS0_14LookupIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7StoreIC14ComputeHandlerEPNS0_14LookupIteratorE
	.type	_ZN2v88internal7StoreIC14ComputeHandlerEPNS0_14LookupIteratorE, @function
_ZN2v88internal7StoreIC14ComputeHandlerEPNS0_14LookupIteratorE:
.LFB24302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	4(%rsi), %eax
	cmpl	$7, %eax
	ja	.L2352
	leaq	.L2354(%rip), %rdx
	movl	%eax, %r14d
	movq	%rdi, %r13
	movq	%rsi, %r12
	movslq	(%rdx,%r14,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal7StoreIC14ComputeHandlerEPNS0_14LookupIteratorE,"a",@progbits
	.align 4
	.align 4
.L2354:
	.long	.L2357-.L2354
	.long	.L2357-.L2354
	.long	.L2359-.L2354
	.long	.L2358-.L2354
	.long	.L2357-.L2354
	.long	.L2356-.L2354
	.long	.L2355-.L2354
	.long	.L2353-.L2354
	.section	.text._ZN2v88internal7StoreIC14ComputeHandlerEPNS0_14LookupIteratorE
	.p2align 4,,10
	.p2align 3
.L2359:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2540
.L2377:
	movq	8(%r13), %rdi
	movl	$131, %esi
	addq	$41184, %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%rax, %rdx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L2376:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2541
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2358:
	.cfi_restore_state
	movq	56(%rsi), %rdx
	movq	48(%rsi), %rcx
	movq	32(%rdi), %rsi
	movq	8(%rdi), %rdi
	call	_ZN2v88internal12StoreHandler10StoreProxyEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_7JSProxyEEENS4_INS0_10JSReceiverEEE@PLT
	movq	%rax, %rdx
.L2535:
	movl	$1, %eax
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2356:
	movq	48(%rsi), %rax
	movq	56(%rsi), %r15
	movq	%rax, -120(%rbp)
	movq	(%r15), %rax
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$2097152, %eax
	je	.L2378
	leaq	.LC43(%rip), %rax
	movq	%rax, 72(%rdi)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2536
.L2458:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movq	%rax, %rdx
	movl	$1, %eax
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2355:
	movq	56(%rsi), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$2097152, %edx
	jne	.L2542
	testb	$2, 16(%rsi)
	jne	.L2451
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2543
.L2452:
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator23GetFieldDescriptorIndexEv@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZNK2v88internal14LookupIterator13GetFieldIndexEv@PLT
	movl	16(%r12), %edx
	movl	$1, %ecx
	testb	$4, %dl
	jne	.L2544
.L2453:
	shrl	$6, %edx
	movq	8(%r13), %r12
	andl	$7, %edx
	leal	-1(%rdx), %esi
	cmpb	$3, %sil
	ja	.L2357
	movq	%rax, %rsi
	sall	$16, %eax
	subl	$1, %edx
	movq	41112(%r12), %rdi
	shrq	$14, %rsi
	andl	$1073217536, %eax
	sall	$17, %edx
	andl	$1, %esi
	sall	$6, %ebx
	sall	$16, %esi
	orl	%eax, %esi
	orl	%edx, %esi
	orl	%ebx, %esi
	orl	%ecx, %esi
	salq	$32, %rsi
	testq	%rdi, %rdi
	je	.L2545
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
	jmp	.L2535
	.p2align 4,,10
	.p2align 3
.L2353:
	movq	48(%rsi), %r14
	movq	(%r14), %rax
	leaq	-1(%rax), %rbx
	testb	$1, %al
	jne	.L2546
.L2367:
	movq	(%rbx), %rax
	cmpw	$1025, 11(%rax)
	je	.L2547
	movq	40(%r12), %rsi
	movq	8(%r13), %rdi
	call	_ZN2v88internal12StoreHandler15StoreTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEE@PLT
	jmp	.L2376
.L2352:
	movl	$1, %eax
	xorl	%edx, %edx
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2378:
	movq	%rsi, %rdi
	call	_ZNK2v88internal14LookupIterator12GetAccessorsEv@PLT
	movq	%rax, %rbx
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L2548
.L2538:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L2458
.L2536:
	movq	8(%r13), %rax
	movl	$1138, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2458
	.p2align 4,,10
	.p2align 3
.L2547:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2549
.L2369:
	movq	32(%r13), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$1025, 11(%rax)
	je	.L2550
	movq	8(%r13), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2372
	movabsq	$30064771072, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L2373:
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	movq	8(%r13), %rdi
	movq	32(%r13), %rsi
	movq	%rcx, %rax
	pushq	%rbx
	xorl	%r8d, %r8d
	movq	%r10, %rcx
	movabsq	$-4294967296, %rdx
	andq	%rdx, %rax
	movq	%r14, %rdx
	orq	$1, %rax
	pushq	%rax
	movq	40(%r12), %r9
	call	_ZN2v88internal12StoreHandler21StoreThroughPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %rdx
	movl	$1, %eax
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2451:
	leaq	.LC50(%rip), %rax
	movq	%rax, 72(%rdi)
	jmp	.L2538
	.p2align 4,,10
	.p2align 3
.L2546:
	movq	-1(%rax), %rdx
	cmpw	$1026, 11(%rdx)
	jne	.L2367
	movq	-1(%rax), %rdx
	movq	23(%rdx), %r15
	movq	-1(%r15), %rdx
	leaq	-1(%r15), %rbx
	cmpw	$1025, 11(%rdx)
	je	.L2551
	leaq	-1(%rax), %rbx
	jmp	.L2367
	.p2align 4,,10
	.p2align 3
.L2542:
	movq	-1(%rax), %rax
	cmpw	$1025, 11(%rax)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	je	.L2552
	testl	%eax, %eax
	jne	.L2553
.L2447:
	movq	8(%r13), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2448
	movabsq	$34359738368, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
	jmp	.L2535
.L2357:
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2545:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L2554
.L2457:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L2535
	.p2align 4,,10
	.p2align 3
.L2550:
	movq	40(%r12), %rdi
	call	_ZN2v88internal12StoreHandler11StoreGlobalENS0_6HandleINS0_12PropertyCellEEE@PLT
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2544:
	xorl	%esi, %esi
	cmpl	$12, 100(%r13)
	setne	%sil
	leal	1(%rsi), %ecx
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L2552:
	testl	%eax, %eax
	jne	.L2555
.L2446:
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator15GetPropertyCellEv@PLT
	movq	%rax, %rdi
	call	_ZN2v88internal12StoreHandler11StoreGlobalENS0_6HandleINS0_12PropertyCellEEE@PLT
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2548:
	movq	-1(%rax), %rdx
	cmpw	$78, 11(%rdx)
	je	.L2381
	movq	-1(%rax), %rdx
	cmpw	$79, 11(%rdx)
	jne	.L2538
	movq	8(%r13), %rbx
	movq	15(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2405
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rdx
.L2406:
	testb	$1, %sil
	jne	.L2556
.L2408:
	leaq	.LC47(%rip), %rax
	movq	%rax, 72(%r13)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L2458
	jmp	.L2536
	.p2align 4,,10
	.p2align 3
.L2372:
	movq	41088(%rbx), %r10
	cmpq	41096(%rbx), %r10
	je	.L2557
.L2374:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%rbx)
	movabsq	$30064771072, %rax
	movq	%rax, (%r10)
	jmp	.L2373
	.p2align 4,,10
	.p2align 3
.L2448:
	movq	41088(%rbx), %rdx
	cmpq	41096(%rbx), %rdx
	je	.L2558
.L2450:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movabsq	$34359738368, %rax
	movq	%rax, (%rdx)
	jmp	.L2535
	.p2align 4,,10
	.p2align 3
.L2540:
	movq	8(%rdi), %rax
	movl	$1145, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2377
.L2381:
	movq	31(%rax), %rdx
	cmpq	%rdx, _ZN2v88internal3Smi5kZeroE(%rip)
	je	.L2384
	cmpq	$0, 7(%rdx)
	je	.L2384
	testb	$4, 19(%rax)
	je	.L2392
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator33HolderIsReceiverOrHiddenPrototypeEv@PLT
	testb	%al, %al
	je	.L2559
.L2392:
	movq	32(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal12AccessorInfo23IsCompatibleReceiverMapENS0_6HandleIS1_EENS2_INS0_3MapEEE@PLT
	testb	%al, %al
	jne	.L2390
	leaq	.LC46(%rip), %rax
	movq	%rax, 72(%r13)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L2458
	jmp	.L2536
	.p2align 4,,10
	.p2align 3
.L2551:
	movq	24(%rsi), %rdx
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2364
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
	movq	(%rax), %rax
	leaq	-1(%rax), %rbx
	jmp	.L2367
.L2384:
	leaq	.LC44(%rip), %rax
	movq	%rax, 72(%r13)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L2458
	jmp	.L2536
	.p2align 4,,10
	.p2align 3
.L2549:
	movq	8(%r13), %rax
	movl	$1144, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2369
.L2543:
	movq	8(%rdi), %rax
	movl	$1142, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2452
.L2554:
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L2457
.L2405:
	movq	41088(%rbx), %rdx
	cmpq	%rdx, 41096(%rbx)
	je	.L2560
.L2407:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rdx)
	jmp	.L2406
.L2553:
	movq	8(%rdi), %rax
	movl	$1148, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2447
.L2364:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L2561
.L2366:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%r15, (%r14)
	jmp	.L2367
.L2557:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r10
	jmp	.L2374
.L2558:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdx
	jmp	.L2450
.L2556:
	movq	-1(%rsi), %rax
	cmpw	$1105, 11(%rax)
	movq	(%rdx), %rax
	je	.L2412
	testb	$1, %al
	je	.L2408
	movq	-1(%rax), %rsi
	cmpw	$88, 11(%rsi)
	jne	.L2408
.L2412:
	leaq	-96(%rbp), %rbx
	testb	$1, %al
	jne	.L2562
.L2410:
	movq	8(%r13), %rsi
	movq	%rbx, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZN2v88internal16CallOptimizationC1EPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	cmpb	$0, -88(%rbp)
	movq	-128(%rbp), %rdx
	jne	.L2563
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L2564
.L2433:
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator16GetAccessorIndexEv@PLT
	movq	8(%r13), %rcx
	sall	$6, %eax
	movl	%eax, %ebx
	movq	41112(%rcx), %rdi
	orl	$3, %ebx
	salq	$32, %rbx
	testq	%rdi, %rdi
	je	.L2565
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L2438:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2566
.L2440:
	movq	-120(%rbp), %rax
	cmpq	%rax, %r15
	je	.L2441
	testq	%rax, %rax
	je	.L2442
	movq	(%rax), %rax
	cmpq	%rax, (%r15)
	je	.L2441
.L2442:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2567
.L2443:
	xorl	%eax, %eax
	xorl	%edx, %edx
	movq	8(%r13), %rdi
	movl	$1, %r8d
	movabsq	$-4294967296, %rsi
	movq	%rax, %rcx
	xorl	%r9d, %r9d
	andq	%rsi, %rcx
	movq	32(%r13), %rsi
	pushq	%rdx
	orq	$1, %rcx
	pushq	%rcx
	movq	%r12, %rcx
.L2534:
	movq	%r15, %rdx
	call	_ZN2v88internal12StoreHandler21StoreThroughPrototypeEPNS0_7IsolateENS0_6HandleINS0_3MapEEENS4_INS0_10JSReceiverEEENS4_INS0_3SmiEEENS0_17MaybeObjectHandleESB_@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %rdx
	movl	$1, %eax
	jmp	.L2376
.L2555:
	movq	8(%rdi), %rax
	movl	$1143, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2446
.L2390:
	movq	%r12, %rdi
	call	_ZNK2v88internal14LookupIterator16GetAccessorIndexEv@PLT
	movq	8(%r13), %r14
	sall	$6, %eax
	movl	%eax, %ebx
	movq	41112(%r14), %rdi
	orl	$4, %ebx
	salq	$32, %rbx
	testq	%rdi, %rdi
	je	.L2395
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r12
.L2396:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2568
.L2398:
	movq	-120(%rbp), %rax
	cmpq	%rax, %r15
	je	.L2441
	testq	%rax, %rax
	je	.L2400
	movq	(%rax), %rax
	cmpq	%rax, (%r15)
	je	.L2441
.L2400:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L2443
	movq	8(%r13), %rax
	movl	$1147, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2443
	.p2align 4,,10
	.p2align 3
.L2441:
	movl	$1, %eax
	movq	%r12, %rdx
	jmp	.L2376
.L2395:
	movq	41088(%r14), %r12
	cmpq	41096(%r14), %r12
	je	.L2569
.L2397:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%r14)
	movq	%rbx, (%r12)
	jmp	.L2396
.L2560:
	movq	%rbx, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L2407
.L2559:
	leaq	.LC45(%rip), %rax
	movq	%rax, 72(%r13)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L2458
	jmp	.L2536
.L2562:
	movq	-1(%rax), %rsi
	cmpw	$88, 11(%rsi)
	je	.L2415
.L2417:
	leaq	-96(%rbp), %rbx
	testb	$1, %al
	je	.L2410
	movq	-1(%rax), %rsi
	leaq	-96(%rbp), %rbx
	cmpw	$1105, 11(%rsi)
	jne	.L2410
	movq	%rdx, -128(%rbp)
	movq	23(%rax), %rax
	movq	%rbx, %rdi
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo12BreakAtEntryEv@PLT
	movq	-128(%rbp), %rdx
	testb	%al, %al
	jne	.L2538
	jmp	.L2410
.L2563:
	movq	-120(%rbp), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZNK2v88internal16CallOptimization20IsCompatibleReceiverENS0_6HandleINS0_6ObjectEEENS2_INS0_8JSObjectEEE@PLT
	testb	%al, %al
	je	.L2423
	movq	32(%r13), %rsi
	leaq	-104(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNK2v88internal16CallOptimization26LookupHolderOfExpectedTypeENS0_6HandleINS0_3MapEEEPNS1_12HolderLookupE@PLT
	cmpl	$1, -104(%rbp)
	movq	8(%r13), %rdx
	movl	$6, %eax
	cmovne	%rax, %r14
	movq	41112(%rdx), %rdi
	salq	$32, %r14
	movq	%r14, %r12
	testq	%rdi, %rdi
	je	.L2425
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L2426:
	movq	(%r15), %rax
	movq	8(%r13), %r12
	movq	%rbx, %rdi
	movq	-1(%rax), %rsi
	call	_ZNK2v88internal16CallOptimization18GetAccessorContextENS0_3MapE@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2428
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rbx
.L2429:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2570
.L2431:
	xorl	%eax, %eax
	movl	$4294967295, %ecx
	movq	8(%r13), %rdi
	movq	-72(%rbp), %r9
	movq	%rax, %rsi
	salq	$32, %rcx
	xorl	%r8d, %r8d
	andq	%rcx, %rsi
	movq	%r14, %rcx
	movq	%rsi, %rax
	movq	32(%r13), %rsi
	pushq	%rbx
	pushq	%rax
	jmp	.L2534
.L2561:
	movq	%rdx, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2366
.L2541:
	call	__stack_chk_fail@PLT
.L2565:
	movq	41088(%rcx), %r12
	cmpq	41096(%rcx), %r12
	je	.L2571
.L2439:
	leaq	8(%r12), %rax
	movq	%rax, 41088(%rcx)
	movq	%rbx, (%r12)
	jmp	.L2438
.L2564:
	movq	-1(%rax), %rax
	cmpw	$88, 11(%rax)
	jne	.L2433
	leaq	.LC49(%rip), %rax
	movq	%rax, 72(%r13)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L2458
	jmp	.L2536
.L2423:
	leaq	.LC48(%rip), %rax
	movq	%rax, 72(%r13)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L2458
	jmp	.L2536
.L2415:
	leaq	-104(%rbp), %rdi
	movq	%rdx, -128(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal20FunctionTemplateInfo12BreakAtEntryEv
	testb	%al, %al
	jne	.L2538
	movq	-128(%rbp), %rdx
	movq	(%rdx), %rax
	jmp	.L2417
.L2568:
	movq	8(%r13), %rax
	movl	$1146, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2398
.L2566:
	movq	8(%r13), %rax
	movl	$1139, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2440
.L2571:
	movq	%rcx, %rdi
	movq	%rcx, -128(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-128(%rbp), %rcx
	movq	%rax, %r12
	jmp	.L2439
.L2425:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L2572
.L2427:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%r12, (%r14)
	jmp	.L2426
.L2569:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L2397
.L2428:
	movq	41088(%r12), %rbx
	cmpq	%rbx, 41096(%r12)
	je	.L2573
.L2430:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L2429
.L2570:
	movq	8(%r13), %rax
	movl	$1141, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2431
.L2573:
	movq	%r12, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L2430
.L2567:
	movq	8(%r13), %rax
	movl	$1140, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2443
.L2572:
	movq	%rdx, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L2427
	.cfi_endproc
.LFE24302:
	.size	_ZN2v88internal7StoreIC14ComputeHandlerEPNS0_14LookupIteratorE, .-_ZN2v88internal7StoreIC14ComputeHandlerEPNS0_14LookupIteratorE
	.section	.text._ZN2v88internal12KeyedStoreIC19StoreElementHandlerENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12KeyedStoreIC19StoreElementHandlerENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeE
	.type	_ZN2v88internal12KeyedStoreIC19StoreElementHandlerENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeE, @function
_ZN2v88internal12KeyedStoreIC19StoreElementHandlerENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeE:
.LFB24304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpw	$1024, 11(%rax)
	je	.L2628
	movzbl	14(%rax), %eax
	movq	%rsi, %r12
	shrl	$3, %eax
	leal	-13(%rax), %ecx
	cmpb	$1, %cl
	jbe	.L2629
	cmpb	$9, %al
	jbe	.L2583
	subl	$17, %eax
	cmpb	$10, %al
	ja	.L2630
.L2583:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2631
.L2586:
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal11CodeFactory18StoreFastElementICEPNS0_7IsolateENS0_20KeyedAccessStoreModeE@PLT
	movq	(%r12), %rax
	movq	-80(%rbp), %r13
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$17, %eax
	cmpb	$10, %al
	ja	.L2582
.L2627:
	movq	%r13, %rax
.L2579:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2632
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2630:
	.cfi_restore_state
	cmpl	$14, 28(%rdi)
	je	.L2633
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2634
.L2588:
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal11CodeFactory17KeyedStoreIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE@PLT
	movq	-80(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L2582:
	cmpl	$14, 28(%rbx)
	je	.L2627
.L2589:
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal3Map37GetOrCreatePrototypeChainValidityCellENS0_6HandleIS1_EEPNS0_7IsolateE@PLT
	movq	%rax, %r12
	testb	$1, (%rax)
	je	.L2627
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v88internal7Factory15NewStoreHandlerEi@PLT
	movq	(%r12), %r12
	movq	(%rax), %r14
	movq	%rax, %rbx
	movq	%r12, 15(%r14)
	leaq	15(%r14), %rsi
	testb	$1, %r12b
	je	.L2598
	movq	%r12, %r15
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L2635
.L2592:
	testb	$24, %al
	je	.L2598
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2598
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2598:
	movq	(%rbx), %r14
	movq	0(%r13), %r12
	movq	%r12, 7(%r14)
	leaq	7(%r14), %r15
	testb	$1, %r12b
	je	.L2597
	movq	%r12, %r13
	andq	$-262144, %r13
	movq	8(%r13), %rax
	testl	$262144, %eax
	jne	.L2636
.L2595:
	testb	$24, %al
	je	.L2597
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L2597
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L2597:
	movq	%rbx, %rax
	jmp	.L2579
	.p2align 4,,10
	.p2align 3
.L2629:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2637
.L2581:
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal11CodeFactory28KeyedStoreIC_SloppyArgumentsEPNS0_7IsolateENS0_20KeyedAccessStoreModeE@PLT
	cmpl	$14, 28(%rbx)
	movq	-80(%rbp), %r13
	je	.L2627
	jmp	.L2589
	.p2align 4,,10
	.p2align 3
.L2628:
	movq	8(%rdi), %rbx
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2576
	movabsq	$38654705664, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L2579
	.p2align 4,,10
	.p2align 3
.L2576:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L2638
.L2578:
	movabsq	$38654705664, %rcx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rcx, (%rax)
	jmp	.L2579
	.p2align 4,,10
	.p2align 3
.L2631:
	movq	8(%rbx), %rax
	movl	$1103, %esi
	movl	%edx, -88(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movl	-88(%rbp), %edx
	jmp	.L2586
	.p2align 4,,10
	.p2align 3
.L2635:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-88(%rbp), %rsi
	jmp	.L2592
	.p2align 4,,10
	.p2align 3
.L2636:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r13), %rax
	jmp	.L2595
	.p2align 4,,10
	.p2align 3
.L2633:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2639
.L2587:
	movq	8(%rbx), %rsi
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal11CodeFactory26StoreInArrayLiteralIC_SlowEPNS0_7IsolateENS0_20KeyedAccessStoreModeE@PLT
	movq	-80(%rbp), %r13
	jmp	.L2582
	.p2align 4,,10
	.p2align 3
.L2637:
	movq	8(%rdi), %rax
	movl	$1100, %esi
	movl	%edx, -88(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movl	-88(%rbp), %edx
	jmp	.L2581
	.p2align 4,,10
	.p2align 3
.L2638:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L2578
.L2634:
	movq	8(%rdi), %rax
	movl	$1102, %esi
	movl	%edx, -88(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movl	-88(%rbp), %edx
	jmp	.L2588
.L2639:
	movq	8(%rdi), %rax
	movl	$1150, %esi
	movl	%edx, -88(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movl	-88(%rbp), %edx
	jmp	.L2587
.L2632:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24304:
	.size	_ZN2v88internal12KeyedStoreIC19StoreElementHandlerENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeE, .-_ZN2v88internal12KeyedStoreIC19StoreElementHandlerENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeE
	.section	.text._ZN2v88internal25Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE:
.LFB24324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2672
	movq	41096(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %r14
	movq	%rax, -104(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L2673
.L2643:
	leaq	.LC16(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2673:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2643
	movq	12464(%rdx), %rax
	movq	39(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2674
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L2646:
	movq	1135(%rsi), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2648
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L2649:
	movq	0(%r13), %rdx
	leaq	-68(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE@PLT
	testb	%al, %al
	je	.L2651
	movl	-68(%rbp), %eax
	movq	(%rbx), %rdx
	leal	24(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2652
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2653:
	movl	-64(%rbp), %eax
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rsi,%rax), %rbx
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2655
	movq	%rbx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rbx
.L2656:
	cmpq	%rbx, 96(%r12)
	jne	.L2659
.L2671:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$177, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %rbx
.L2659:
	subl	$1, 41104(%r12)
	movq	-104(%rbp), %rax
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rax
	je	.L2640
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2640:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2675
	addq	$72, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2651:
	.cfi_restore_state
	movq	(%r15), %rax
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r8
	testq	%rdi, %rdi
	je	.L2660
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2661:
	movq	%r13, %rdx
	leaq	-81(%rbp), %rcx
	movq	%r12, %rdi
	movb	$0, -81(%rbp)
	call	_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2676
	cmpb	$0, -81(%rbp)
	jne	.L2664
	movq	%rax, -112(%rbp)
	movslq	-4(%r13), %rsi
	movq	%rbx, %rdi
	movq	-16(%r13), %rax
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	-112(%rbp), %rdx
	cmpl	$6, %eax
	je	.L2671
.L2664:
	movq	(%rdx), %rbx
	jmp	.L2659
	.p2align 4,,10
	.p2align 3
.L2648:
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L2677
.L2650:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	jmp	.L2649
	.p2align 4,,10
	.p2align 3
.L2674:
	movq	%r14, %r15
	cmpq	41096(%rdx), %r14
	je	.L2678
.L2647:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L2646
	.p2align 4,,10
	.p2align 3
.L2660:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2679
.L2662:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2661
	.p2align 4,,10
	.p2align 3
.L2655:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2680
.L2657:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rbx, (%rax)
	jmp	.L2656
	.p2align 4,,10
	.p2align 3
.L2652:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L2681
.L2654:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	jmp	.L2653
	.p2align 4,,10
	.p2align 3
.L2672:
	call	_ZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE
	movq	%rax, %rbx
	jmp	.L2640
	.p2align 4,,10
	.p2align 3
.L2676:
	movq	312(%r12), %rbx
	jmp	.L2659
	.p2align 4,,10
	.p2align 3
.L2678:
	movq	%rdx, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L2647
	.p2align 4,,10
	.p2align 3
.L2677:
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L2650
	.p2align 4,,10
	.p2align 3
.L2679:
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2662
	.p2align 4,,10
	.p2align 3
.L2681:
	movq	%r12, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	jmp	.L2654
	.p2align 4,,10
	.p2align 3
.L2680:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L2657
.L2675:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24324:
	.size	_ZN2v88internal25Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE:
.LFB24339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2723
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %rbx
	movq	%rax, -96(%rbp)
	leaq	-32(%rsi), %rax
	movq	%rax, -88(%rbp)
	movq	-32(%rsi), %rax
	testb	$1, %al
	jne	.L2724
.L2685:
	leaq	.LC14(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2724:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2685
	movq	12464(%rdx), %rax
	leaq	-72(%rbp), %r14
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2725
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L2688:
	movq	12464(%r15), %rax
	movq	39(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2690
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L2691:
	movq	1135(%rsi), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2693
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L2694:
	movq	-32(%r12), %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE@PLT
	testb	%al, %al
	je	.L2696
	movl	-72(%rbp), %eax
	movq	-104(%rbp), %r8
	leal	24(,%rax,8), %eax
	movq	(%r8), %rdx
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2697
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpb	$1, -64(%rbp)
	movq	%rax, %r14
	je	.L2726
.L2700:
	movl	-68(%rbp), %eax
	movq	(%r14), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2702
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	cmpq	%rsi, 96(%r15)
	jne	.L2705
.L2733:
	movq	-88(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$177, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
.L2722:
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r12
	jmp	.L2701
	.p2align 4,,10
	.p2align 3
.L2696:
	movq	-88(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L2727
	movq	(%rax), %r12
.L2701:
	movq	-96(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	cmpq	41096(%r15), %rbx
	je	.L2682
	movq	%rbx, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2682:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2728
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2693:
	.cfi_restore_state
	movq	41088(%r15), %r8
	cmpq	41096(%r15), %r8
	je	.L2729
.L2695:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r8)
	jmp	.L2694
	.p2align 4,,10
	.p2align 3
.L2690:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L2730
.L2692:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L2691
	.p2align 4,,10
	.p2align 3
.L2725:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L2731
.L2689:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L2688
	.p2align 4,,10
	.p2align 3
.L2702:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L2732
.L2704:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	cmpq	%rsi, 96(%r15)
	je	.L2733
.L2705:
	movl	-68(%rbp), %eax
	movq	(%r14), %r14
	movq	(%r12), %r13
	leal	16(,%rax,8), %eax
	cltq
	leaq	-1(%r14,%rax), %rsi
	movq	%r13, (%rsi)
	testb	$1, %r13b
	je	.L2712
	movq	%r13, %rcx
	andq	$-262144, %rcx
	movq	8(%rcx), %rax
	movq	%rcx, -88(%rbp)
	testl	$262144, %eax
	jne	.L2734
.L2707:
	testb	$24, %al
	je	.L2712
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L2735
	.p2align 4,,10
	.p2align 3
.L2712:
	movq	(%r12), %r12
	jmp	.L2701
	.p2align 4,,10
	.p2align 3
.L2697:
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L2736
.L2699:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	cmpb	$1, -64(%rbp)
	jne	.L2700
.L2726:
	movq	-88(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	movl	$37, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	jmp	.L2722
	.p2align 4,,10
	.p2align 3
.L2723:
	movq	%r12, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r12
	jmp	.L2682
	.p2align 4,,10
	.p2align 3
.L2727:
	movq	312(%r15), %r12
	jmp	.L2701
	.p2align 4,,10
	.p2align 3
.L2731:
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2689
	.p2align 4,,10
	.p2align 3
.L2730:
	movq	%r15, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	jmp	.L2692
	.p2align 4,,10
	.p2align 3
.L2729:
	movq	%r15, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L2695
	.p2align 4,,10
	.p2align 3
.L2734:
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-88(%rbp), %rcx
	movq	-104(%rbp), %rsi
	movq	8(%rcx), %rax
	jmp	.L2707
	.p2align 4,,10
	.p2align 3
.L2735:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L2712
	.p2align 4,,10
	.p2align 3
.L2736:
	movq	%r15, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L2699
	.p2align 4,,10
	.p2align 3
.L2732:
	movq	%r15, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	jmp	.L2704
.L2728:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24339:
	.size	_ZN2v88internal26Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateE:
.LFB24348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2744
	addl	$1, 41104(%rdx)
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	leaq	-8(%rsi), %rsi
	leaq	-16(%rcx), %rdx
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L2745
	movq	(%rax), %r14
.L2740:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L2737
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2737:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2745:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L2740
	.p2align 4,,10
	.p2align 3
.L2744:
	popq	%rbx
	movq	%rdx, %rsi
	popq	%r12
	movq	%rcx, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL31Stats_Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE24348:
	.size	_ZN2v88internal25Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateE:
.LFB24351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2757
	addl	$1, 41104(%rdx)
	leaq	-144(%rbp), %rbx
	leaq	-16(%rsi), %rcx
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %r14
	leaq	-145(%rbp), %r8
	leaq	-8(%rsi), %rdx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	$1, %r9d
	movb	$0, -145(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r13, %rsi
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE@PLT
	testb	%al, %al
	je	.L2758
.L2749:
	shrw	$8, %ax
	je	.L2759
	movq	0(%r13), %r13
	movq	%r15, 41088(%r12)
	subl	$1, 41104(%r12)
	cmpq	41096(%r12), %r14
	je	.L2746
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2746:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2760
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2757:
	.cfi_restore_state
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L2746
	.p2align 4,,10
	.p2align 3
.L2758:
	movl	%eax, -164(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-164(%rbp), %eax
	jmp	.L2749
	.p2align 4,,10
	.p2align 3
.L2759:
	leaq	.LC10(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2760:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24351:
	.size	_ZN2v88internal34Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal41Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal41Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateE
	.type	_ZN2v88internal41Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateE, @function
_ZN2v88internal41Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateE:
.LFB24354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2776
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	leaq	-8(%rsi), %r15
	leaq	-144(%rbp), %r14
	movq	%r14, %rdi
	movq	41096(%rdx), %rbx
	movq	%rax, -168(%rbp)
	leaq	-16(%rsi), %rax
	movslq	-28(%rsi), %rsi
	movq	%rax, -176(%rbp)
	movq	-40(%r13), %rax
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	0(%r13), %rcx
	testb	$1, %cl
	jne	.L2777
.L2765:
	cmpl	$14, %eax
	je	.L2778
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	-16(%r13), %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L2779
	movq	(%rax), %r13
.L2770:
	subl	$1, 41104(%r12)
	movq	-168(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2761
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2761:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2780
	addq	$136, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2778:
	.cfi_restore_state
	leaq	-145(%rbp), %r8
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	$1, %r9d
	movb	$0, -145(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r14, %rdi
	movl	$1, %ecx
	leaq	-16(%r13), %rsi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE@PLT
	testb	%al, %al
	je	.L2781
.L2768:
	shrw	$8, %ax
	je	.L2782
	movq	-16(%r13), %r13
	jmp	.L2770
	.p2align 4,,10
	.p2align 3
.L2777:
	movq	-1(%rcx), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L2765
	movq	-24(%r13), %rcx
	movq	%r13, %rdi
	movl	%eax, -176(%rbp)
	movzbl	14(%rcx), %esi
	shrl	$3, %esi
	call	_ZN2v88internal8JSObject22TransitionElementsKindENS0_6HandleIS1_EENS0_12ElementsKindE@PLT
	movl	-176(%rbp), %eax
	jmp	.L2765
	.p2align 4,,10
	.p2align 3
.L2776:
	call	_ZN2v88internalL47Stats_Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L2779:
	movq	312(%r12), %r13
	jmp	.L2770
	.p2align 4,,10
	.p2align 3
.L2782:
	leaq	.LC10(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2781:
	movl	%eax, -176(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-176(%rbp), %eax
	jmp	.L2768
.L2780:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24354:
	.size	_ZN2v88internal41Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateE, .-_ZN2v88internal41Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateE:
.LFB24363:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2808
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movslq	-4(%rsi), %r15
	testb	$1, %al
	jne	.L2809
.L2787:
	movq	-24(%r13), %rax
	movq	-16(%r13), %rsi
	leaq	-24(%r13), %rdi
	movq	-1(%rax), %rcx
	cmpw	$155, 11(%rcx)
	je	.L2810
.L2790:
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internalL19CloneObjectSlowPathEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEi
	testq	%rax, %rax
	je	.L2811
	movq	(%rax), %r13
.L2799:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L2783
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2783:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2812
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2809:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L2787
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	je	.L2787
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	jmp	.L2790
	.p2align 4,,10
	.p2align 3
.L2810:
	movq	%rdi, -80(%rbp)
	sarq	$32, %rsi
	leaq	-88(%rbp), %rdi
	movq	$0, -72(%rbp)
	movl	%esi, -64(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movl	%eax, -60(%rbp)
	testb	$1, 0(%r13)
	je	.L2790
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	cmpl	$6, %eax
	je	.L2790
	movq	0(%r13), %rax
	movq	-1(%rax), %r8
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2795
	movq	%r8, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L2796:
	movq	%rsi, %rdi
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internalL18CanFastCloneObjectENS0_6HandleINS0_3MapEEE
	testb	%al, %al
	je	.L2798
	movq	-112(%rbp), %rsi
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internalL18FastCloneObjectMapEPNS0_7IsolateENS0_6HandleINS0_3MapEEEi
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	%rax, %r13
	movq	%rax, %rdx
	call	_ZN2v88internal13FeedbackNexus20ConfigureCloneObjectENS0_6HandleINS0_3MapEEES4_@PLT
	movq	0(%r13), %r13
	jmp	.L2799
	.p2align 4,,10
	.p2align 3
.L2811:
	movq	312(%r12), %r13
	jmp	.L2799
	.p2align 4,,10
	.p2align 3
.L2808:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL32Stats_Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L2783
.L2798:
	movq	-104(%rbp), %rdi
	call	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicEv@PLT
	jmp	.L2790
.L2795:
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L2813
.L2797:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r8, (%rsi)
	jmp	.L2796
.L2813:
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L2797
.L2812:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24363:
	.size	_ZN2v88internal26Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal29Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal29Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal29Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE, @function
_ZN2v88internal29Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE:
.LFB24366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2879
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	leaq	-16(%rsi), %r15
	leaq	-24(%rsi), %r13
	movq	%rax, -272(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -264(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2880
	movq	-16(%rsi), %rax
	movq	(%rsi), %rcx
	xorl	%r9d, %r9d
	leaq	-144(%rbp), %rdi
	movq	-8(%rsi), %r8
	movq	%r12, %rsi
	movq	55(%rax), %rdx
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r14
	movq	$0, -176(%rbp)
	movaps	%xmm0, -208(%rbp)
	movaps	%xmm0, -192(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L2881
.L2820:
	movq	-16(%rbx), %rax
	movq	31(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L2848
	movq	7(%rax), %r8
	movq	%r8, -280(%rbp)
.L2821:
	cmpl	$32, 41828(%r14)
	jne	.L2822
	movq	41112(%r14), %rdi
	movq	41472(%r14), %r9
	movq	-72(%rbp), %rsi
	testq	%rdi, %rdi
	je	.L2823
	movq	%r9, -296(%rbp)
	movq	%r8, -288(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-288(%rbp), %r8
	movq	-296(%rbp), %r9
	movq	%rax, %rdx
.L2824:
	movl	$2, %ecx
	movq	%r15, %rsi
	movq	%r9, %rdi
	movq	%r8, -288(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-288(%rbp), %r8
	testb	%al, %al
	je	.L2826
.L2822:
	movq	12608(%r14), %rax
	movl	12616(%r14), %r15d
	movq	%r14, -240(%rbp)
	movl	$6, 12616(%r14)
	movq	%rax, -224(%rbp)
	leaq	-240(%rbp), %rax
	movq	%r8, -232(%rbp)
	movq	%rax, 12608(%r14)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L2882
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L2883
.L2830:
	movq	41016(%r14), %rdi
	leaq	-120(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	%rdi, -288(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-288(%rbp), %rdi
	testb	%al, %al
	jne	.L2884
.L2834:
	movq	-280(%rbp), %rax
	leaq	-248(%rbp), %rdx
	movq	%r13, %rdi
	leaq	-32(%rbx), %rsi
	call	*%rax
	movq	-240(%rbp), %rax
	movq	-224(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L2885
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L2886
.L2838:
	movl	%r15d, 12616(%r14)
.L2826:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2887
.L2842:
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L2888
	movq	-32(%rbx), %r13
.L2844:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
.L2819:
	subl	$1, 41104(%r12)
	movq	-272(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-264(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L2814
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2814:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2889
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2888:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
	jmp	.L2844
	.p2align 4,,10
	.p2align 3
.L2886:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r10
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r10
	jne	.L2890
.L2839:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2840
	movq	(%rdi), %rax
	call	*8(%rax)
.L2840:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2838
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2838
	.p2align 4,,10
	.p2align 3
.L2882:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2891
.L2829:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	movzbl	(%rdx), %eax
	testb	$5, %al
	je	.L2830
.L2883:
	pxor	%xmm0, %xmm0
	movq	%rdx, -288(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	-288(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2892
.L2831:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2832
	movq	(%rdi), %rax
	call	*8(%rax)
.L2832:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2830
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2830
	.p2align 4,,10
	.p2align 3
.L2884:
	movq	-24(%rbx), %rcx
	movq	-112(%rbp), %rdx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	jmp	.L2834
	.p2align 4,,10
	.p2align 3
.L2885:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2893
.L2837:
	movq	%r13, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	movzbl	0(%r13), %eax
	testb	$5, %al
	je	.L2838
	jmp	.L2886
	.p2align 4,,10
	.p2align 3
.L2823:
	movq	41088(%r14), %rdx
	cmpq	41096(%r14), %rdx
	je	.L2894
.L2825:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rdx)
	jmp	.L2824
	.p2align 4,,10
	.p2align 3
.L2879:
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L2814
	.p2align 4,,10
	.p2align 3
.L2880:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	-32(%rsi), %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testq	%rax, %rax
	je	.L2895
	movq	(%rax), %r13
	jmp	.L2819
	.p2align 4,,10
	.p2align 3
.L2881:
	movq	40960(%r14), %rax
	leaq	-200(%rbp), %rsi
	movl	$110, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -208(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L2820
	.p2align 4,,10
	.p2align 3
.L2887:
	leaq	-200(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L2842
	.p2align 4,,10
	.p2align 3
.L2848:
	movq	$0, -280(%rbp)
	xorl	%r8d, %r8d
	jmp	.L2821
	.p2align 4,,10
	.p2align 3
.L2890:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rdx
	leaq	-160(%rbp), %rax
	pushq	$0
	movl	$69, %esi
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L2839
	.p2align 4,,10
	.p2align 3
.L2893:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L2837
	.p2align 4,,10
	.p2align 3
.L2892:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-160(%rbp), %rcx
	pushq	$0
	pushq	%rcx
	leaq	.LC19(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L2831
	.p2align 4,,10
	.p2align 3
.L2891:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L2829
	.p2align 4,,10
	.p2align 3
.L2894:
	movq	%r14, %rdi
	movq	%rsi, -304(%rbp)
	movq	%r9, -296(%rbp)
	movq	%r8, -288(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-304(%rbp), %rsi
	movq	-296(%rbp), %r9
	movq	-288(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L2825
	.p2align 4,,10
	.p2align 3
.L2895:
	movq	312(%r12), %r13
	jmp	.L2819
.L2889:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24366:
	.size	_ZN2v88internal29Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE, .-_ZN2v88internal29Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE:
.LFB24369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movl	%eax, -316(%rbp)
	testl	%eax, %eax
	jne	.L2991
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	leaq	-8(%rsi), %r14
	leaq	-16(%rsi), %rbx
	movq	%rsi, -336(%rbp)
	movq	%rax, -296(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -304(%rbp)
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L2899
.L2902:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object15ConvertReceiverEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2992
.L2903:
	movq	(%rbx), %rax
	movq	-1(%rax), %rax
.L2990:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L2993
.L2908:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L2909
	movq	31(%rdx), %rsi
.L2909:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2910
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r11
.L2911:
	movq	63(%rsi), %rdx
	movq	(%r14), %rcx
	movq	%r12, %rsi
	leaq	-144(%rbp), %rdi
	movq	(%rbx), %r8
	movq	%r11, -312(%rbp)
	movabsq	$4294967297, %r9
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r15
	movq	$0, -208(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	-312(%rbp), %r11
	testl	%eax, %eax
	jne	.L2994
.L2913:
	movq	41016(%r15), %r15
	movq	%r11, -312(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-312(%rbp), %r11
	testb	%al, %al
	jne	.L2995
.L2914:
	movq	(%r11), %rax
	movq	7(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L2950
	movq	7(%rax), %r8
	movq	%r8, -312(%rbp)
.L2915:
	movq	-104(%rbp), %r15
	cmpl	$32, 41828(%r15)
	je	.L2916
.L2919:
	movl	12616(%r15), %eax
	movl	$6, 12616(%r15)
	movq	%r15, -272(%rbp)
	movq	%r8, -264(%rbp)
	movl	%eax, -328(%rbp)
	movq	12608(%r15), %rax
	movq	%rax, -256(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, 12608(%r15)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L2917
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L2996
.L2922:
	leaq	-120(%rbp), %rax
	leaq	-280(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, -280(%rbp)
	movq	-312(%rbp), %rax
	call	*%rax
	movq	96(%r15), %rax
	xorl	%r10d, %r10d
	cmpq	%rax, -88(%rbp)
	je	.L2926
	leaq	-88(%rbp), %r10
.L2926:
	movq	-272(%rbp), %rax
	movq	-256(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	testq	%rdx, %rdx
	je	.L2997
.L2928:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L2998
.L2930:
	movl	-328(%rbp), %eax
	movl	%eax, 12616(%r15)
.L2920:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L2999
.L2934:
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L3000
	testq	%r10, %r10
	je	.L2937
	movq	(%r10), %r13
.L2936:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
.L2904:
	subl	$1, 41104(%r12)
	movq	-296(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-304(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L2896
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L2896:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3001
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2910:
	.cfi_restore_state
	movq	41088(%r12), %r11
	cmpq	41096(%r12), %r11
	je	.L3002
.L2912:
	leaq	8(%r11), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r11)
	jmp	.L2911
	.p2align 4,,10
	.p2align 3
.L2899:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L2902
	jmp	.L2903
	.p2align 4,,10
	.p2align 3
.L2993:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L2990
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L2908
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L2908
	.p2align 4,,10
	.p2align 3
.L2916:
	movq	41472(%r15), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r11, %rsi
	movq	%r8, -328(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	xorl	%r10d, %r10d
	movq	-328(%rbp), %r8
	testb	%al, %al
	je	.L2920
	jmp	.L2919
	.p2align 4,,10
	.p2align 3
.L2995:
	movq	0(%r13), %rcx
	movq	-112(%rbp), %rdx
	leaq	.LC22(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	movq	-312(%rbp), %r11
	jmp	.L2914
	.p2align 4,,10
	.p2align 3
.L3000:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
	jmp	.L2936
	.p2align 4,,10
	.p2align 3
.L2917:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3003
.L2921:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	movzbl	(%rdx), %eax
	testb	$5, %al
	je	.L2922
.L2996:
	pxor	%xmm0, %xmm0
	movq	%rdx, -344(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-344(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L3004
.L2923:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2924
	movq	(%rdi), %rax
	call	*8(%rax)
.L2924:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2922
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L2922
	.p2align 4,,10
	.p2align 3
.L2998:
	pxor	%xmm0, %xmm0
	movq	%rdx, -344(%rbp)
	movq	%r10, -312(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-312(%rbp), %r10
	movq	-344(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L3005
.L2931:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2932
	movq	(%rdi), %rax
	movq	%r10, -312(%rbp)
	call	*8(%rax)
	movq	-312(%rbp), %r10
.L2932:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2930
	movq	(%rdi), %rax
	movq	%r10, -312(%rbp)
	call	*8(%rax)
	movq	-312(%rbp), %r10
	jmp	.L2930
	.p2align 4,,10
	.p2align 3
.L2997:
	movq	%r10, -312(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	-312(%rbp), %r10
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3006
.L2929:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	jmp	.L2928
	.p2align 4,,10
	.p2align 3
.L2937:
	movq	(%rbx), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	0(%r13), %rax
	movq	-1(%rax), %rdx
	subq	$37592, %rdi
	cmpw	$64, 11(%rdx)
	je	.L3007
	movl	$3, -316(%rbp)
.L2938:
	movl	-316(%rbp), %eax
	movq	%rdi, -216(%rbp)
	movl	%eax, -240(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -228(%rbp)
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L3008
.L2939:
	movq	%r13, -208(%rbp)
	leaq	-240(%rbp), %r13
	movq	%r13, %rdi
	movq	$0, -200(%rbp)
	movq	%r14, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	%rbx, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	jmp	.L2944
	.p2align 4,,10
	.p2align 3
.L3009:
	testq	%rax, %rax
	je	.L2940
	movq	(%rbx), %rcx
	cmpq	%rcx, (%rax)
	je	.L2941
.L2940:
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
.L2944:
	cmpl	$2, -236(%rbp)
	jne	.L2940
	movq	-184(%rbp), %rax
	cmpq	%rax, %rbx
	jne	.L3009
.L2941:
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	testq	%rax, %rax
	je	.L3010
	cmpl	$4, -236(%rbp)
	je	.L2945
	movq	(%rax), %r13
	jmp	.L2936
	.p2align 4,,10
	.p2align 3
.L2992:
	movq	312(%r12), %r13
	jmp	.L2904
	.p2align 4,,10
	.p2align 3
.L2991:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L2896
	.p2align 4,,10
	.p2align 3
.L2994:
	movq	40960(%r15), %rax
	leaq	-232(%rbp), %rsi
	movl	$173, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-312(%rbp), %r11
	jmp	.L2913
	.p2align 4,,10
	.p2align 3
.L2999:
	leaq	-232(%rbp), %rsi
	movq	%r10, -312(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-312(%rbp), %r10
	jmp	.L2934
	.p2align 4,,10
	.p2align 3
.L2950:
	movq	$0, -312(%rbp)
	xorl	%r8d, %r8d
	jmp	.L2915
	.p2align 4,,10
	.p2align 3
.L2945:
	movq	-336(%rbp), %rax
	leaq	-272(%rbp), %rdi
	movslq	-20(%rax), %rsi
	movq	-32(%rax), %rax
	movq	%rax, -272(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	cmpl	$6, %eax
	je	.L2946
	movq	88(%r12), %r13
	jmp	.L2936
	.p2align 4,,10
	.p2align 3
.L3002:
	movq	%r12, %rdi
	movq	%rsi, -312(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-312(%rbp), %rsi
	movq	%rax, %r11
	jmp	.L2912
	.p2align 4,,10
	.p2align 3
.L3007:
	testb	$1, 11(%rax)
	movl	$3, %eax
	cmovne	-316(%rbp), %eax
	movl	%eax, -316(%rbp)
	jmp	.L2938
	.p2align 4,,10
	.p2align 3
.L3005:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	movq	-312(%rbp), %r10
	addq	$64, %rsp
	jmp	.L2931
	.p2align 4,,10
	.p2align 3
.L3006:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	-312(%rbp), %r10
	movq	%rax, %rdx
	jmp	.L2929
	.p2align 4,,10
	.p2align 3
.L3004:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L2923
	.p2align 4,,10
	.p2align 3
.L3003:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L2921
	.p2align 4,,10
	.p2align 3
.L2946:
	movq	-208(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$177, %esi
	call	_ZN2v88internal7Factory17NewReferenceErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
	jmp	.L2936
	.p2align 4,,10
	.p2align 3
.L3008:
	movq	%r13, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, %r13
	jmp	.L2939
	.p2align 4,,10
	.p2align 3
.L3010:
	movq	312(%r12), %r13
	jmp	.L2936
.L3001:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24369:
	.size	_ZN2v88internal35Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE, .-_ZN2v88internal35Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal36Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE:
.LFB24372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %ebx
	testl	%ebx, %ebx
	jne	.L3100
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	leaq	-24(%rsi), %r14
	movq	%rax, -304(%rbp)
	movq	41096(%rdx), %rax
	movq	%r14, %rdx
	movq	%rax, -296(%rbp)
	leaq	-32(%rsi), %rax
	movslq	-4(%rsi), %rsi
	movq	%rax, -312(%rbp)
	movq	-24(%r13), %rax
	movq	-1(%rax), %rax
	cmpw	$1026, 11(%rax)
	je	.L3101
.L3014:
	movq	(%rdx), %rax
	movq	-1(%rax), %rax
.L3099:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L3102
.L3022:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L3023
	movq	31(%rdx), %rsi
.L3023:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3024
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L3025:
	movq	-24(%r13), %rcx
	movq	63(%rsi), %rdx
	leaq	-144(%rbp), %rdi
	movabsq	$4294967297, %r9
	movq	%r12, %rsi
	movq	%rcx, %r8
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movq	(%r15), %rax
	movq	15(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L3060
	movq	7(%rax), %rax
	movq	%rax, -320(%rbp)
.L3027:
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r15
	movq	$0, -208(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testl	%edx, %edx
	jne	.L3103
.L3028:
	xorl	%r11d, %r11d
	cmpl	$32, 41828(%r15)
	je	.L3029
	movq	%rax, -264(%rbp)
	movq	12608(%r15), %rax
	movl	12616(%r15), %ecx
	movq	%r15, -272(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-272(%rbp), %rax
	movl	%ecx, -328(%rbp)
	movl	$6, 12616(%r15)
	movq	%rax, 12608(%r15)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L3104
.L3031:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L3105
.L3033:
	movq	41016(%r15), %rdi
	leaq	-120(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	%rdi, -336(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-336(%rbp), %rdi
	testb	%al, %al
	jne	.L3106
.L3037:
	movq	-320(%rbp), %rax
	movq	-312(%rbp), %rdi
	leaq	-280(%rbp), %rdx
	movq	%r13, %rsi
	call	*%rax
	movq	96(%r15), %rax
	xorl	%r11d, %r11d
	cmpq	%rax, -88(%rbp)
	je	.L3038
	leaq	-88(%rbp), %r11
.L3038:
	movq	-272(%rbp), %rax
	movq	-256(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	testq	%rdx, %rdx
	je	.L3107
.L3040:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L3108
.L3042:
	movl	-328(%rbp), %eax
	movl	%eax, 12616(%r15)
.L3029:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3109
.L3046:
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L3110
	testq	%r11, %r11
	je	.L3049
.L3053:
	movq	0(%r13), %r13
.L3048:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movq	-304(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	movq	-296(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L3011
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3011:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3111
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3024:
	.cfi_restore_state
	movq	41088(%r12), %r15
	cmpq	41096(%r12), %r15
	je	.L3112
.L3026:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r15)
	jmp	.L3025
	.p2align 4,,10
	.p2align 3
.L3102:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L3099
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L3022
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L3022
	.p2align 4,,10
	.p2align 3
.L3110:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
	jmp	.L3048
	.p2align 4,,10
	.p2align 3
.L3101:
	movq	-16(%r13), %rax
	leaq	-240(%rbp), %r15
	movq	%r15, %rdi
	movq	%rax, -240(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	cmpl	$1, %eax
	je	.L3067
	movq	%r14, %rdx
	cmpl	$10, %eax
	jne	.L3014
.L3067:
	movq	12464(%r12), %rax
	movq	%r15, %rdi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3016
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
	jmp	.L3014
	.p2align 4,,10
	.p2align 3
.L3049:
	movq	-24(%r13), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-32(%r13), %rax
	movq	-1(%rax), %rdx
	subq	$37592, %rdi
	cmpw	$64, 11(%rdx)
	je	.L3113
	movl	$3, %ebx
.L3050:
	movabsq	$824633720832, %rax
	movl	%ebx, -240(%rbp)
	movq	%rax, -228(%rbp)
	movq	-32(%r13), %rax
	movq	%rdi, -216(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L3114
.L3051:
	movq	-312(%rbp), %rax
	leaq	-240(%rbp), %r15
	movq	$0, -200(%rbp)
	movq	%r15, %rdi
	movq	%r14, -192(%rbp)
	movq	%rax, -208(%rbp)
	movq	$0, -184(%rbp)
	movq	%r14, -176(%rbp)
	movq	$-1, -168(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movl	-236(%rbp), %eax
	testl	%eax, %eax
	je	.L3115
.L3052:
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	jne	.L3053
	movq	312(%r12), %r13
	jmp	.L3048
	.p2align 4,,10
	.p2align 3
.L3108:
	pxor	%xmm0, %xmm0
	movq	%rdx, -336(%rbp)
	movq	%r11, -320(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-320(%rbp), %r11
	movq	-336(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r10
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r10
	jne	.L3116
.L3043:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3044
	movq	(%rdi), %rax
	movq	%r11, -320(%rbp)
	call	*8(%rax)
	movq	-320(%rbp), %r11
.L3044:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3042
	movq	(%rdi), %rax
	movq	%r11, -320(%rbp)
	call	*8(%rax)
	movq	-320(%rbp), %r11
	jmp	.L3042
	.p2align 4,,10
	.p2align 3
.L3107:
	movq	%r11, -320(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	-320(%rbp), %r11
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3117
.L3041:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	jmp	.L3040
	.p2align 4,,10
	.p2align 3
.L3106:
	movq	-32(%r13), %rcx
	movq	-112(%rbp), %rdx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v88internal6Logger22ApiNamedPropertyAccessEPKcNS0_8JSObjectENS0_6ObjectE@PLT
	jmp	.L3037
	.p2align 4,,10
	.p2align 3
.L3105:
	pxor	%xmm0, %xmm0
	movq	%rdx, -336(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-336(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L3118
.L3034:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3035
	movq	(%rdi), %rax
	call	*8(%rax)
.L3035:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3033
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3033
	.p2align 4,,10
	.p2align 3
.L3104:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3119
.L3032:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	jmp	.L3031
	.p2align 4,,10
	.p2align 3
.L3100:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L3011
	.p2align 4,,10
	.p2align 3
.L3103:
	movq	%rax, -328(%rbp)
	movq	40960(%r15), %rax
	movl	$175, %edx
	leaq	-232(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	movq	-328(%rbp), %rax
	jmp	.L3028
	.p2align 4,,10
	.p2align 3
.L3109:
	leaq	-232(%rbp), %rsi
	movq	%r11, -320(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	movq	-320(%rbp), %r11
	jmp	.L3046
	.p2align 4,,10
	.p2align 3
.L3060:
	movq	$0, -320(%rbp)
	xorl	%eax, %eax
	jmp	.L3027
	.p2align 4,,10
	.p2align 3
.L3112:
	movq	%r12, %rdi
	movq	%rsi, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3113:
	testb	$1, 11(%rax)
	movl	$3, %eax
	cmove	%eax, %ebx
	jmp	.L3050
	.p2align 4,,10
	.p2align 3
.L3115:
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	jmp	.L3052
	.p2align 4,,10
	.p2align 3
.L3016:
	movq	41088(%r12), %rdx
	cmpq	41096(%r12), %rdx
	je	.L3120
.L3018:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rdx)
	jmp	.L3014
	.p2align 4,,10
	.p2align 3
.L3114:
	movq	-312(%rbp), %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	%rax, -312(%rbp)
	jmp	.L3051
	.p2align 4,,10
	.p2align 3
.L3119:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L3032
	.p2align 4,,10
	.p2align 3
.L3118:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L3034
	.p2align 4,,10
	.p2align 3
.L3117:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	-320(%rbp), %r11
	movq	%rax, %rdx
	jmp	.L3041
	.p2align 4,,10
	.p2align 3
.L3116:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	movq	-320(%rbp), %r11
	addq	$64, %rsp
	jmp	.L3043
.L3120:
	movq	%r12, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L3018
.L3111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24372:
	.size	_ZN2v88internal36Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE, .-_ZN2v88internal36Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE:
.LFB24375:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3197
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -312(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -304(%rbp)
	movslq	-4(%rsi), %rax
	movq	%rax, -296(%rbp)
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
.L3196:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L3198
.L3127:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L3128
	movq	39(%rdx), %rsi
.L3128:
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3129
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L3130:
	movq	0(%r13), %rcx
	movq	63(%rsi), %rdx
	leaq	-144(%rbp), %rdi
	movabsq	$4294967297, %r9
	movq	%r12, %rsi
	movq	%rcx, %r8
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r15
	movq	$0, -208(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3199
.L3132:
	movq	41016(%r15), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L3200
.L3133:
	movq	(%r14), %rax
	movq	7(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L3162
	movq	7(%rax), %r8
	movq	%r8, %r15
.L3134:
	movq	-104(%rbp), %rbx
	cmpl	$32, 41828(%rbx)
	je	.L3135
.L3138:
	movl	12616(%rbx), %eax
	movq	%rbx, -272(%rbp)
	movl	$6, 12616(%rbx)
	movl	%eax, %r14d
	movq	12608(%rbx), %rax
	movq	%r8, -264(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, 12608(%rbx)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L3136
.L3137:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L3201
.L3141:
	leaq	-120(%rbp), %rax
	movl	-296(%rbp), %edi
	leaq	-280(%rbp), %rsi
	movq	%rax, -280(%rbp)
	call	*%r15
	movq	96(%rbx), %rax
	xorl	%r15d, %r15d
	cmpq	%rax, -88(%rbp)
	je	.L3145
	leaq	-88(%rbp), %r15
.L3145:
	movq	-272(%rbp), %rax
	movq	-256(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	testq	%rdx, %rdx
	je	.L3202
.L3147:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L3203
.L3149:
	movl	%r14d, 12616(%rbx)
.L3139:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3204
.L3153:
	movq	12552(%r12), %rax
	cmpq	%rax, 96(%r12)
	jne	.L3205
	testq	%r15, %r15
	je	.L3206
.L3156:
	movq	(%r15), %r13
.L3155:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movq	-312(%rbp), %rax
	subl	$1, 41104(%r12)
	movq	%rax, 41088(%r12)
	movq	-304(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L3121
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3121:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3207
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3129:
	.cfi_restore_state
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L3208
.L3131:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	jmp	.L3130
	.p2align 4,,10
	.p2align 3
.L3198:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L3196
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L3127
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L3135:
	movq	41472(%rbx), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r8, -320(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-320(%rbp), %r8
	testb	%al, %al
	jne	.L3138
	xorl	%r15d, %r15d
	jmp	.L3139
	.p2align 4,,10
	.p2align 3
.L3200:
	movl	-296(%rbp), %ecx
	movq	-112(%rbp), %rdx
	leaq	.LC26(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	jmp	.L3133
	.p2align 4,,10
	.p2align 3
.L3205:
	movq	%r12, %rdi
	call	_ZN2v88internal7Isolate25PromoteScheduledExceptionEv@PLT
	movq	%rax, %r13
	jmp	.L3155
	.p2align 4,,10
	.p2align 3
.L3203:
	pxor	%xmm0, %xmm0
	movq	%rdx, -320(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-320(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L3209
.L3150:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3151
	movq	(%rdi), %rax
	call	*8(%rax)
.L3151:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3149
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3149
	.p2align 4,,10
	.p2align 3
.L3202:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3210
.L3148:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	jmp	.L3147
	.p2align 4,,10
	.p2align 3
.L3201:
	pxor	%xmm0, %xmm0
	movq	%rdx, -320(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-320(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L3211
.L3142:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3143
	movq	(%rdi), %rax
	call	*8(%rax)
.L3143:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3141
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3141
	.p2align 4,,10
	.p2align 3
.L3136:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3212
.L3140:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	jmp	.L3137
	.p2align 4,,10
	.p2align 3
.L3206:
	movabsq	$824633720832, %rax
	movq	%r13, -192(%rbp)
	movq	%rax, -228(%rbp)
	movl	-296(%rbp), %eax
	movq	%r13, -176(%rbp)
	leaq	-240(%rbp), %r13
	movq	%r13, %rdi
	movl	%eax, -168(%rbp)
	movl	$3, -240(%rbp)
	movq	%r12, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -184(%rbp)
	movl	$-1, -164(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r13, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	jne	.L3156
	movq	312(%r12), %r13
	jmp	.L3155
	.p2align 4,,10
	.p2align 3
.L3197:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L3121
	.p2align 4,,10
	.p2align 3
.L3204:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3153
	.p2align 4,,10
	.p2align 3
.L3199:
	movq	40960(%r15), %rax
	leaq	-232(%rbp), %rsi
	movl	$173, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3132
	.p2align 4,,10
	.p2align 3
.L3162:
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	jmp	.L3134
	.p2align 4,,10
	.p2align 3
.L3208:
	movq	%r12, %rdi
	movq	%rsi, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L3209:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L3150
	.p2align 4,,10
	.p2align 3
.L3212:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L3140
	.p2align 4,,10
	.p2align 3
.L3210:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L3148
	.p2align 4,,10
	.p2align 3
.L3211:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L3142
.L3207:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24375:
	.size	_ZN2v88internal34Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE:
.LFB24381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3361
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -312(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -304(%rbp)
	movslq	-4(%rsi), %rax
	movq	%rax, -296(%rbp)
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
.L3356:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L3362
.L3219:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L3220
	movq	39(%rdx), %rsi
.L3220:
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3221
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r14
.L3222:
	movq	(%r12), %rcx
	movq	63(%rsi), %rdx
	leaq	-144(%rbp), %rdi
	movabsq	$4294967297, %r9
	movq	%r13, %rsi
	movq	%rcx, %r8
	call	_ZN2v88internal25PropertyCallbackArgumentsC1EPNS0_7IsolateENS0_6ObjectES4_NS0_8JSObjectENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movq	(%r14), %rax
	movq	88(%r13), %rdx
	cmpq	23(%rax), %rdx
	je	.L3224
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %rbx
	movq	$0, -208(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3363
.L3225:
	movq	(%r14), %rax
	movq	23(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L3279
	movq	7(%rax), %r8
	movq	%r8, %r15
.L3226:
	cmpl	$32, 41828(%rbx)
	je	.L3364
.L3227:
	movl	12616(%rbx), %eax
	leaq	-272(%rbp), %r14
	movl	$6, 12616(%rbx)
	movq	%rbx, -272(%rbp)
	movq	%r8, -264(%rbp)
	movl	%eax, -320(%rbp)
	movq	12608(%rbx), %rax
	movq	%rax, -256(%rbp)
	movq	%r14, 12608(%rbx)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	testq	%rdx, %rdx
	je	.L3365
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L3366
.L3233:
	leaq	-120(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	41016(%rbx), %rdi
	movq	%rdi, -328(%rbp)
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	movq	-328(%rbp), %rdi
	testb	%al, %al
	jne	.L3367
.L3237:
	movl	-296(%rbp), %edi
	leaq	-280(%rbp), %rsi
	call	*%r15
	movq	96(%rbx), %rax
	xorl	%r15d, %r15d
	cmpq	%rax, -88(%rbp)
	je	.L3238
	leaq	-88(%rbp), %r15
.L3238:
	movq	-272(%rbp), %rax
	movq	-256(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	testq	%rdx, %rdx
	je	.L3368
.L3240:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L3369
.L3242:
	movl	-320(%rbp), %eax
	movl	%eax, 12616(%rbx)
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3370
.L3246:
	testq	%r15, %r15
	je	.L3277
	movq	(%r15), %rax
	leaq	-240(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, -240(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L3371
	cmpl	$64, -272(%rbp)
	je	.L3273
.L3358:
	movq	112(%r13), %r12
.L3250:
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rdx, 41704(%rax)
	movq	-312(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-304(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L3213
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3213:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3372
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3221:
	.cfi_restore_state
	movq	41088(%r13), %r14
	cmpq	41096(%r13), %r14
	je	.L3373
.L3223:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r14)
	jmp	.L3222
	.p2align 4,,10
	.p2align 3
.L3364:
	movq	41472(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r8, -320(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-320(%rbp), %r8
	testb	%al, %al
	jne	.L3227
.L3360:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3277
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	.p2align 4,,10
	.p2align 3
.L3277:
	movabsq	$824633720832, %rax
	movq	%r12, -192(%rbp)
	movq	%rax, -228(%rbp)
	movl	-296(%rbp), %eax
	movq	%r12, -176(%rbp)
	leaq	-240(%rbp), %r12
	movq	%r12, %rdi
	movl	%eax, -168(%rbp)
	movl	$3, -240(%rbp)
	movq	%r13, -216(%rbp)
	movq	$0, -208(%rbp)
	movq	$0, -200(%rbp)
	movq	$0, -184(%rbp)
	movl	$-1, -164(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	je	.L3374
	shrw	$8, %ax
	jne	.L3358
.L3273:
	movq	120(%r13), %r12
	jmp	.L3250
	.p2align 4,,10
	.p2align 3
.L3224:
	cmpq	7(%rax), %rdx
	je	.L3277
	pxor	%xmm0, %xmm0
	movq	-104(%rbp), %r15
	movq	$0, -208(%rbp)
	movaps	%xmm0, -240(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3375
.L3251:
	movq	41016(%r15), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	jne	.L3376
.L3252:
	movq	(%r14), %rax
	movq	7(%rax), %rax
	cmpq	_ZN2v88internal3Smi5kZeroE(%rip), %rax
	je	.L3283
	movq	7(%rax), %r8
	movq	%r8, %rbx
.L3253:
	movq	-104(%rbp), %r15
	cmpl	$32, 41828(%r15)
	je	.L3377
.L3254:
	movl	12616(%r15), %eax
	movl	$6, 12616(%r15)
	movq	%r15, -272(%rbp)
	movq	%r8, -264(%rbp)
	movl	%eax, -320(%rbp)
	movq	12608(%r15), %rax
	movq	%rax, -256(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, 12608(%r15)
	movq	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip), %rdx
	movq	%rdx, %r14
	testq	%rdx, %rdx
	je	.L3378
.L3257:
	movzbl	(%r14), %eax
	testb	$5, %al
	jne	.L3379
.L3259:
	leaq	-120(%rbp), %rax
	movl	-296(%rbp), %edi
	leaq	-280(%rbp), %rsi
	xorl	%r14d, %r14d
	movq	%rax, -280(%rbp)
	call	*%rbx
	movq	96(%r15), %rax
	cmpq	%rax, -88(%rbp)
	je	.L3263
	leaq	-88(%rbp), %r14
.L3263:
	movq	-272(%rbp), %rax
	movq	-256(%rbp), %rdx
	movq	%rdx, 12608(%rax)
	movq	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip), %rdx
	testq	%rdx, %rdx
	je	.L3380
.L3265:
	movzbl	(%rdx), %eax
	testb	$5, %al
	jne	.L3381
.L3267:
	movl	-320(%rbp), %eax
	movl	%eax, 12616(%r15)
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3382
.L3271:
	testq	%r14, %r14
	jne	.L3358
	jmp	.L3277
	.p2align 4,,10
	.p2align 3
.L3362:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L3356
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L3219
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L3219
	.p2align 4,,10
	.p2align 3
.L3374:
	movq	312(%r13), %r12
	jmp	.L3250
	.p2align 4,,10
	.p2align 3
.L3365:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3383
.L3232:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	movzbl	(%rdx), %eax
	testb	$5, %al
	je	.L3233
.L3366:
	pxor	%xmm0, %xmm0
	movq	%rdx, -328(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	-328(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3384
.L3234:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3235
	movq	(%rdi), %rax
	call	*8(%rax)
.L3235:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3233
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3233
	.p2align 4,,10
	.p2align 3
.L3367:
	movl	-296(%rbp), %ecx
	movq	-112(%rbp), %rdx
	leaq	.LC28(%rip), %rsi
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	jmp	.L3237
	.p2align 4,,10
	.p2align 3
.L3369:
	pxor	%xmm0, %xmm0
	movq	%rdx, -328(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	-328(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3385
.L3243:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3244
	movq	(%rdi), %rax
	call	*8(%rax)
.L3244:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3242
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3242
	.p2align 4,,10
	.p2align 3
.L3368:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3386
.L3241:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	jmp	.L3240
	.p2align 4,,10
	.p2align 3
.L3361:
	movq	%r12, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r12
	jmp	.L3213
	.p2align 4,,10
	.p2align 3
.L3373:
	movq	%r13, %rdi
	movq	%rsi, -320(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-320(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3223
	.p2align 4,,10
	.p2align 3
.L3377:
	movq	41472(%r15), %rdi
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r14, %rsi
	movq	%r8, -320(%rbp)
	call	_ZN2v88internal5Debug33PerformSideEffectCheckForCallbackENS0_6HandleINS0_6ObjectEEES4_NS1_12AccessorKindE@PLT
	movq	-320(%rbp), %r8
	testb	%al, %al
	jne	.L3254
	jmp	.L3360
	.p2align 4,,10
	.p2align 3
.L3376:
	movl	-296(%rbp), %ecx
	movq	-112(%rbp), %rdx
	leaq	.LC26(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6Logger24ApiIndexedPropertyAccessEPKcNS0_8JSObjectEj@PLT
	jmp	.L3252
	.p2align 4,,10
	.p2align 3
.L3363:
	movq	40960(%rbx), %rax
	leaq	-232(%rbp), %rsi
	movl	$159, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3225
	.p2align 4,,10
	.p2align 3
.L3279:
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	jmp	.L3226
	.p2align 4,,10
	.p2align 3
.L3381:
	pxor	%xmm0, %xmm0
	movq	%rdx, -328(%rbp)
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-328(%rbp), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L3387
.L3268:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3269
	movq	(%rdi), %rax
	call	*8(%rax)
.L3269:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3267
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3267
	.p2align 4,,10
	.p2align 3
.L3380:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3388
.L3266:
	movq	%rdx, _ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68(%rip)
	jmp	.L3265
	.p2align 4,,10
	.p2align 3
.L3379:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L3389
.L3260:
	movq	-152(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3261
	movq	(%rdi), %rax
	call	*8(%rax)
.L3261:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3259
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3259
	.p2align 4,,10
	.p2align 3
.L3378:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3390
.L3258:
	movq	%r14, _ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62(%rip)
	jmp	.L3257
	.p2align 4,,10
	.p2align 3
.L3370:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3246
	.p2align 4,,10
	.p2align 3
.L3386:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L3241
	.p2align 4,,10
	.p2align 3
.L3385:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-160(%rbp), %rcx
	pushq	$0
	pushq	%rcx
	leaq	.LC19(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L3243
	.p2align 4,,10
	.p2align 3
.L3384:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-160(%rbp), %rcx
	pushq	$0
	pushq	%rcx
	leaq	.LC19(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L3234
	.p2align 4,,10
	.p2align 3
.L3383:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L3232
	.p2align 4,,10
	.p2align 3
.L3371:
	leaq	.LC29(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3375:
	movq	40960(%r15), %rax
	leaq	-232(%rbp), %rsi
	movl	$173, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3251
	.p2align 4,,10
	.p2align 3
.L3283:
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
	jmp	.L3253
	.p2align 4,,10
	.p2align 3
.L3382:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3271
.L3388:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L3266
.L3390:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L3258
.L3389:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	leaq	-160(%rbp), %rax
	pushq	$0
	movl	$66, %esi
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L3260
.L3387:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-160(%rbp), %rax
	pushq	$0
	leaq	.LC19(%rip), %rcx
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L3268
.L3372:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24381:
	.size	_ZN2v88internal33Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE, .-_ZN2v88internal33Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateE
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,"axG",@progbits,_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,comdat
	.p2align 4
	.weak	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z:
.LFB24458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$232, %rsp
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L3392
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L3392:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rcx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rcx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L3394
.L3408:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L3408
.L3394:
	andl	$4095, %ecx
	subq	%rcx, %rsp
	testq	%rcx, %rcx
	jne	.L3409
.L3395:
	leaq	16(%rbp), %rax
	leaq	15(%rsp), %r14
	movl	$32, -240(%rbp)
	movq	%rax, -232(%rbp)
	andq	$-16, %r14
	leaq	-208(%rbp), %rax
	leaq	-240(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	movl	$48, -236(%rbp)
	call	*%r10
	leaq	16(%r12), %rdi
	movslq	%eax, %r13
	movq	%rdi, (%r12)
	movq	%r13, -248(%rbp)
	cmpq	$15, %r13
	ja	.L3410
	cmpq	$1, %r13
	jne	.L3398
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L3399:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3411
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3398:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L3399
	jmp	.L3397
	.p2align 4,,10
	.p2align 3
.L3410:
	movq	%r12, %rdi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L3397:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L3399
	.p2align 4,,10
	.p2align 3
.L3409:
	orq	$0, -8(%rsp,%rcx)
	jmp	.L3395
.L3411:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24458:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.section	.rodata._ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_.str1.1,"aMS",@progbits,1
.LC51:
	.string	""
.LC52:
	.string	".IGNORE_OOB"
.LC53:
	.string	".STORE+COW"
.LC54:
	.string	".COW"
.LC55:
	.string	"Keyed"
.LC56:
	.string	"basic_string::append"
.LC57:
	.string	"("
.LC58:
	.string	"->"
.LC59:
	.string	")"
.LC60:
	.string	"%d"
	.section	.text._ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_
	.type	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_, @function
_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_:
.LFB24229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1560, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags8ic_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3532
.L3412:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3533
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3532:
	.cfi_restore_state
	movq	32(%rdi), %rax
	movq	%rdi, %r12
	movq	%rsi, %r15
	movl	%ecx, %r13d
	movq	$0, -1568(%rbp)
	movl	%r8d, %ebx
	testq	%rax, %rax
	je	.L3414
	movq	(%rax), %rax
	movq	%rax, -1568(%rbp)
.L3414:
	movl	24(%r12), %ecx
	movl	28(%r12), %eax
	testl	%ecx, %ecx
	je	.L3474
	cmpl	$8, %eax
	je	.L3534
	leal	-13(%rax), %ecx
	cmpl	$1, %ecx
	jbe	.L3482
	leaq	.LC51(%rip), %rcx
	movq	%rcx, -1576(%rbp)
	cmpl	$3, %eax
	jne	.L3418
.L3482:
	leaq	80(%r12), %rdi
	movq	%rdx, -1560(%rbp)
	call	_ZNK2v88internal13FeedbackNexus23GetKeyedAccessStoreModeEv@PLT
	movq	-1560(%rbp), %rdx
	cmpl	$2, %eax
	je	.L3420
	ja	.L3421
	testl	%eax, %eax
	je	.L3535
	leaq	.LC53(%rip), %rsi
	movl	28(%r12), %eax
	movq	%rsi, -1576(%rbp)
	jmp	.L3415
	.p2align 4,,10
	.p2align 3
.L3474:
	leaq	.LC51(%rip), %rcx
	movq	%rcx, -1576(%rbp)
.L3415:
	movb	$1, -1560(%rbp)
	cmpl	$8, %eax
	je	.L3425
.L3418:
	movb	$0, -1560(%rbp)
	cmpl	$14, %eax
	ja	.L3425
	movl	$25096, %ecx
	btq	%rax, %rcx
	jnc	.L3425
	cmpl	$14, %eax
	setne	-1560(%rbp)
.L3425:
	movq	%rdx, -1584(%rbp)
	movl	_ZN2v88internal12TracingFlags8ic_statsE(%rip), %eax
	testb	$2, %al
	je	.L3536
	movq	8(%r12), %rsi
	leaq	-1536(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rax
	movq	%rax, -1584(%rbp)
	testq	%rax, %rax
	je	.L3428
	movq	%r12, %rdi
	call	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv@PLT
	movq	-120(%rbp), %rax
	movq	%rax, -1584(%rbp)
.L3428:
	movq	-1584(%rbp), %rdi
	movq	(%rdi), %rax
	call	*152(%rax)
	movq	%rax, %r12
	movzbl	_ZN2v88internal7ICStats9instance_E(%rip), %eax
	cmpb	$2, %al
	je	.L3429
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal7ICStats9instance_E(%rip), %rax
	leaq	-96(%rbp), %r14
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r14, %rsi
	leaq	_ZN2v88internal7ICStats9instance_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L3429
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L3429:
	leaq	8+_ZN2v88internal7ICStats9instance_E(%rip), %rdi
	call	_ZN2v88internal7ICStats5BeginEv@PLT
	movzbl	_ZN2v88internal7ICStats9instance_E(%rip), %eax
	cmpb	$2, %al
	je	.L3431
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal7ICStats9instance_E(%rip), %rax
	leaq	-96(%rbp), %r14
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r14, %rsi
	leaq	_ZN2v88internal7ICStats9instance_E(%rip), %rdi
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L3431
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
.L3431:
	movslq	152+_ZN2v88internal7ICStats9instance_E(%rip), %rax
	leaq	.LC55(%rip), %rcx
	leaq	(%rax,%rax,8), %r11
	movzbl	-1560(%rbp), %eax
	salq	$4, %r11
	addq	16+_ZN2v88internal7ICStats9instance_E(%rip), %r11
	movq	%rax, %r8
	movq	8(%r11), %rdx
	movq	%r11, %rdi
	movq	%r11, %r14
	salq	$63, %r8
	sarq	$63, %r8
	testb	%al, %al
	leaq	.LC51(%rip), %rax
	cmove	%rax, %rcx
	andl	$5, %r8d
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r14), %rax
	cmpq	%rax, %rdx
	ja	.L3458
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	47(%r12), %rax
	cmpl	$67, 59(%rax)
	je	.L3437
	movabsq	$287762808832, %rdx
	movq	23(%r12), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3437
	testb	$1, %al
	jne	.L3537
.L3438:
	movq	47(%r12), %rax
	movl	59(%rax), %eax
	leal	-64(%rax), %edx
	cmpl	$1, %edx
	jbe	.L3440
	cmpl	$57, %eax
	jne	.L3538
.L3440:
	movq	-1584(%rbp), %rax
	movq	32(%rax), %rdi
	call	_ZN2v88internal16InterpretedFrame17GetBytecodeOffsetEm@PLT
	movl	%eax, %edx
.L3442:
	movq	47(%r12), %rax
	cmpl	$67, 59(%rax)
	je	.L3447
	movabsq	$287762808832, %rcx
	movq	23(%r12), %rax
	movq	7(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3447
	testb	$1, %al
	jne	.L3539
.L3448:
	movq	47(%r12), %rax
	movl	59(%rax), %eax
	leal	-64(%rax), %ecx
	cmpl	$1, %ecx
	jbe	.L3450
	cmpl	$57, %eax
	jne	.L3540
.L3450:
	movq	23(%r12), %rax
	movq	31(%rax), %rcx
	testb	$1, %cl
	jne	.L3541
.L3452:
	movq	7(%rax), %rcx
	testb	$1, %cl
	jne	.L3542
.L3454:
	movq	7(%rax), %rax
	movq	7(%rax), %rsi
.L3455:
	movq	%r12, %rdi
	leaq	64(%r14), %r12
	call	_ZN2v88internal15JavaScriptFrame34CollectFunctionAndOffsetForICStatsENS0_10JSFunctionENS0_12AbstractCodeEi@PLT
	movl	$17, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movq	72(%r14), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, %r8d
	leaq	.LC57(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	cmpl	$7, %r13d
	ja	.L3424
	movq	72(%r14), %rsi
	movq	64(%r14), %rdx
	movl	%r13d, %r9d
	leaq	80(%r14), %r15
	leaq	CSWTCH.1572(%rip), %r10
	movzbl	(%r10,%r9), %r9d
	leaq	1(%rsi), %rax
	cmpq	%r15, %rdx
	je	.L3480
	movq	80(%r14), %rcx
.L3456:
	cmpq	%rcx, %rax
	ja	.L3543
.L3457:
	movb	%r9b, (%rdx,%rsi)
	movq	%rax, 72(%r14)
	movq	64(%r14), %rax
	movb	$0, 1(%rax,%rsi)
	movabsq	$4611686018427387903, %rax
	subq	72(%r14), %rax
	cmpq	$1, %rax
	jbe	.L3458
	movl	$2, %edx
	leaq	.LC58(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	cmpl	$7, %ebx
	ja	.L3424
	leaq	CSWTCH.1572(%rip), %r10
	movq	64(%r14), %rax
	movzbl	(%r10,%rbx), %r9d
	movq	72(%r14), %rbx
	leaq	1(%rbx), %r13
	cmpq	%rax, %r15
	je	.L3481
	movq	80(%r14), %rdx
.L3459:
	cmpq	%rdx, %r13
	ja	.L3544
.L3460:
	movb	%r9b, (%rax,%rbx)
	movq	64(%r14), %rax
	movq	%r13, 72(%r14)
	movq	-1576(%rbp), %r15
	movb	$0, 1(%rax,%rbx)
	movabsq	$4611686018427387903, %rbx
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movq	%rbx, %rax
	subq	72(%r14), %rax
	cmpq	%rax, %rdx
	ja	.L3458
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	cmpq	%rbx, 72(%r14)
	je	.L3458
	movl	$1, %edx
	leaq	.LC59(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-1568(%rbp), %rax
	movq	%rax, 96(%r14)
	testq	%rax, %rax
	jne	.L3545
.L3461:
	movzbl	_ZN2v88internal7ICStats9instance_E(%rip), %eax
	cmpb	$2, %al
	je	.L3469
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rbx
	movq	%rax, -96(%rbp)
	movq	%rbx, %xmm0
	leaq	8+_ZN2v88internal7ICStats9instance_E(%rip), %rax
	leaq	-96(%rbp), %r12
	movq	%rax, -88(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r12, %rsi
	leaq	_ZN2v88internal7ICStats9instance_E(%rip), %rdi
	movq	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L3469
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L3469:
	leaq	8+_ZN2v88internal7ICStats9instance_E(%rip), %rdi
	call	_ZN2v88internal7ICStats3EndEv@PLT
	jmp	.L3412
	.p2align 4,,10
	.p2align 3
.L3536:
	movq	8(%r12), %rax
	movq	41016(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L3412
	movq	72(%r12), %rsi
	cmpl	$7, %ebx
	ja	.L3424
	leaq	CSWTCH.1572(%rip), %rax
	movsbl	(%rax,%rbx), %ecx
	cmpl	$7, %r13d
	ja	.L3424
	subq	$8, %rsp
	movq	-1584(%rbp), %rdx
	movl	%r13d, %r9d
	movq	%r14, %rdi
	movzbl	-1560(%rbp), %r13d
	movsbl	(%rax,%r9), %r9d
	pushq	%rsi
	movq	%r15, %rsi
	pushq	-1576(%rbp)
	pushq	%rcx
	movq	-1568(%rbp), %rcx
	movq	(%rdx), %r8
	movl	%r13d, %edx
	call	_ZN2v88internal6Logger7ICEventEPKcbNS0_3MapENS0_6ObjectEccS3_S3_@PLT
	addq	$32, %rsp
	jmp	.L3412
	.p2align 4,,10
	.p2align 3
.L3534:
	leaq	80(%r12), %rdi
	movq	%rdx, -1560(%rbp)
	call	_ZNK2v88internal13FeedbackNexus22GetKeyedAccessLoadModeEv@PLT
	movq	-1560(%rbp), %rdx
	cmpl	$1, %eax
	movl	28(%r12), %eax
	je	.L3531
.L3530:
	leaq	.LC51(%rip), %rsi
	movq	%rsi, -1576(%rbp)
	jmp	.L3415
.L3538:
	movq	47(%r12), %rax
	testb	$62, 43(%rax)
	jne	.L3437
	movq	47(%r12), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	jne	.L3440
	.p2align 4,,10
	.p2align 3
.L3437:
	movq	-1584(%rbp), %rax
	movq	40(%rax), %rax
	movq	(%rax), %r15
	movq	47(%r12), %rax
	movq	%rax, -1544(%rbp)
	leaq	63(%rax), %rcx
	movl	43(%rax), %eax
	testl	%eax, %eax
	js	.L3546
.L3444:
	movl	%r15d, %edx
	subl	%ecx, %edx
	jmp	.L3442
.L3540:
	movq	47(%r12), %rax
	testb	$62, 43(%rax)
	jne	.L3447
	movq	47(%r12), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	jne	.L3450
	.p2align 4,,10
	.p2align 3
.L3447:
	movq	47(%r12), %rsi
	jmp	.L3455
.L3421:
	cmpl	$3, %eax
	jne	.L3424
	leaq	.LC54(%rip), %rcx
	movl	28(%r12), %eax
	movq	%rcx, -1576(%rbp)
	jmp	.L3415
	.p2align 4,,10
	.p2align 3
.L3545:
	movq	%rax, %rbx
	movl	15(%rax), %eax
	leaq	-96(%rbp), %rdi
	movl	$16, %edx
	leaq	.LC60(%rip), %rcx
	shrl	$21, %eax
	andl	$1, %eax
	movb	%al, 104(%r14)
	movl	15(%rbx), %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	shrl	$10, %eax
	andl	$1023, %eax
	movl	%eax, 108(%r14)
	movzwl	11(%rbx), %r8d
	xorl	%eax, %eax
	leaq	-80(%rbp), %rbx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	movq	-96(%rbp), %rdx
	movq	112(%r14), %rdi
	cmpq	%rbx, %rdx
	je	.L3547
	leaq	128(%r14), %rsi
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rax
	cmpq	%rsi, %rdi
	je	.L3548
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	128(%r14), %rsi
	movq	%rdx, 112(%r14)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 120(%r14)
	testq	%rdi, %rdi
	je	.L3467
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L3465:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L3461
	call	_ZdlPv@PLT
	jmp	.L3461
	.p2align 4,,10
	.p2align 3
.L3544:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movb	%r9b, -1560(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	64(%r14), %rax
	movzbl	-1560(%rbp), %r9d
	jmp	.L3460
	.p2align 4,,10
	.p2align 3
.L3543:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rax, -1592(%rbp)
	movb	%r9b, -1584(%rbp)
	movq	%rsi, -1560(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	64(%r14), %rdx
	movq	-1592(%rbp), %rax
	movzbl	-1584(%rbp), %r9d
	movq	-1560(%rbp), %rsi
	jmp	.L3457
.L3481:
	movl	$15, %edx
	jmp	.L3459
.L3480:
	movl	$15, %ecx
	jmp	.L3456
.L3541:
	movq	-1(%rcx), %rsi
	cmpw	$86, 11(%rsi)
	jne	.L3452
	movq	39(%rcx), %rsi
	testb	$1, %sil
	je	.L3452
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L3452
	movq	31(%rcx), %rsi
	jmp	.L3455
.L3420:
	movl	28(%r12), %eax
.L3531:
	leaq	.LC52(%rip), %rcx
	movq	%rcx, -1576(%rbp)
	jmp	.L3415
.L3546:
	leaq	-1544(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rcx
	jmp	.L3444
	.p2align 4,,10
	.p2align 3
.L3542:
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L3454
	movq	7(%rax), %rsi
	jmp	.L3455
	.p2align 4,,10
	.p2align 3
.L3535:
	movl	28(%r12), %eax
	jmp	.L3530
.L3537:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L3437
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L3438
	jmp	.L3437
.L3539:
	movq	-1(%rax), %rcx
	cmpw	$165, 11(%rcx)
	je	.L3447
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L3448
	jmp	.L3447
.L3547:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L3463
	cmpq	$1, %rdx
	je	.L3549
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	112(%r14), %rdi
.L3463:
	movq	%rdx, 120(%r14)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L3465
.L3548:
	movq	%rax, %xmm0
	movq	%rcx, %xmm5
	movq	%rdx, 112(%r14)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 120(%r14)
.L3467:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L3465
.L3549:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	112(%r14), %rdi
	jmp	.L3463
.L3533:
	call	__stack_chk_fail@PLT
.L3424:
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3458:
	leaq	.LC56(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE24229:
	.size	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_, .-_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_
	.section	.text._ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE:
.LFB24228:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags8ic_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3558
	ret
	.p2align 4,,10
	.p2align 3
.L3558:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	24(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L3559
.L3552:
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_
	.p2align 4,,10
	.p2align 3
.L3559:
	.cfi_restore_state
	leaq	80(%rdi), %rdi
	movq	%rdx, -32(%rbp)
	movq	%rsi, -24(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movl	24(%r12), %ecx
	movq	-32(%rbp), %rdx
	movq	-24(%rbp), %rsi
	movl	%eax, %r8d
	jmp	.L3552
	.cfi_endproc
.LFE24228:
	.size	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE
	.section	.rodata._ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE7reserveEm.str1.1,"aMS",@progbits,1
.LC61:
	.string	"vector::reserve"
	.section	.text._ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE7reserveEm,"axG",@progbits,_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE7reserveEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE7reserveEm
	.type	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE7reserveEm, @function
_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE7reserveEm:
.LFB26879:
	.cfi_startproc
	endbr64
	movabsq	$576460752303423487, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	%rax, %rsi
	ja	.L3573
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	16(%rbx), %rax
	subq	%rdi, %rax
	sarq	$4, %rax
	cmpq	%rax, %rsi
	ja	.L3574
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3574:
	.cfi_restore_state
	movq	8(%rbx), %r8
	movq	%rsi, %r14
	xorl	%r13d, %r13d
	salq	$4, %r14
	movq	%r8, %r12
	subq	%rdi, %r12
	testq	%rsi, %rsi
	je	.L3563
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	8(%rbx), %r8
	movq	(%rbx), %rdi
	movq	%rax, %r13
.L3563:
	movq	%r13, %rcx
	movq	%rdi, %rdx
	cmpq	%rdi, %r8
	je	.L3567
	.p2align 4,,10
	.p2align 3
.L3564:
	movl	(%rdx), %eax
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movl	%eax, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %r8
	jne	.L3564
.L3567:
	testq	%rdi, %rdi
	je	.L3566
	call	_ZdlPv@PLT
.L3566:
	movq	%r13, (%rbx)
	addq	%r13, %r12
	addq	%r14, %r13
	movq	%r12, 8(%rbx)
	movq	%r13, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3573:
	.cfi_restore_state
	leaq	.LC61(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE26879:
	.size	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE7reserveEm, .-_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE7reserveEm
	.section	.text._ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, @function
_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_:
.LFB27220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movabsq	$1152921504606846975, %rsi
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	(%rdi), %r12
	movq	%rcx, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L3602
	movq	%r13, %r9
	movq	%rdi, %r15
	subq	%r12, %r9
	testq	%rax, %rax
	je	.L3587
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L3603
.L3577:
	movq	%r14, %rdi
	movq	%rdx, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r9
	movq	%rax, %rbx
	leaq	(%rax,%r14), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	8(%rbx), %r14
.L3586:
	movq	(%rdx), %rax
	movq	%rax, (%rbx,%r9)
	cmpq	%r12, %r13
	je	.L3579
	leaq	-8(%r13), %rsi
	leaq	15(%rbx), %rax
	subq	%r12, %rsi
	subq	%r12, %rax
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rax
	jbe	.L3589
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdi
	je	.L3589
	addq	$1, %rdi
	xorl	%eax, %eax
	movq	%rdi, %rdx
	shrq	%rdx
	salq	$4, %rdx
	.p2align 4,,10
	.p2align 3
.L3581:
	movdqu	(%r12,%rax), %xmm1
	movups	%xmm1, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L3581
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%r12,%rdx), %rax
	addq	%rbx, %rdx
	cmpq	%r8, %rdi
	je	.L3583
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L3583:
	leaq	16(%rbx,%rsi), %r14
.L3579:
	cmpq	%rcx, %r13
	je	.L3584
	subq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%rcx, %rdx
	movq	%rcx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rcx
	addq	%rcx, %r14
.L3584:
	testq	%r12, %r12
	je	.L3585
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L3585:
	movq	-56(%rbp), %rax
	movq	%rbx, %xmm0
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3603:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L3578
	movq	$0, -56(%rbp)
	movl	$8, %r14d
	xorl	%ebx, %ebx
	jmp	.L3586
	.p2align 4,,10
	.p2align 3
.L3587:
	movl	$8, %r14d
	jmp	.L3577
	.p2align 4,,10
	.p2align 3
.L3589:
	movq	%rbx, %rdx
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L3580:
	movq	(%rax), %rdi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rdi, -8(%rdx)
	cmpq	%rax, %r13
	jne	.L3580
	jmp	.L3583
.L3578:
	cmpq	%rsi, %rdi
	cmovbe	%rdi, %rsi
	movq	%rsi, %r14
	salq	$3, %r14
	jmp	.L3577
.L3602:
	leaq	.LC42(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE27220:
	.size	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_, .-_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	.section	.text._ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB28309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$576460752303423487, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L3623
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L3614
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L3624
.L3606:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	16(%r14), %r8
.L3613:
	movq	8(%rdx), %rsi
	movl	(%rdx), %edx
	addq	%r14, %rcx
	movl	%edx, (%rcx)
	movq	%rsi, 8(%rcx)
	cmpq	%r15, %rbx
	je	.L3608
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L3609:
	movl	(%rdx), %edi
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movl	%edi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L3609
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	16(%r14,%rdx), %r8
.L3608:
	cmpq	%r12, %rbx
	je	.L3610
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L3611:
	movq	8(%rdx), %rsi
	movl	(%rdx), %edi
	addq	$16, %rdx
	addq	$16, %rcx
	movl	%edi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L3611
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L3610:
	testq	%r15, %r15
	je	.L3612
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L3612:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3624:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L3607
	movl	$16, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L3613
	.p2align 4,,10
	.p2align 3
.L3614:
	movl	$16, %esi
	jmp	.L3606
.L3607:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$4, %rax
	movq	%rax, %rsi
	jmp	.L3606
.L3623:
	leaq	.LC42(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE28309:
	.size	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZN2v88internal2IC19UpdatePolymorphicICENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC19UpdatePolymorphicICENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	.type	_ZN2v88internal2IC19UpdatePolymorphicICENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE, @function
_ZN2v88internal2IC19UpdatePolymorphicICENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE:
.LFB24265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -160(%rbp)
	movl	28(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$14, %ecx
	ja	.L3688
	movl	$25352, %r12d
	leaq	80(%rdi), %rax
	shrq	%cl, %r12
	movq	%rax, -136(%rbp)
	notq	%r12
	andl	$1, %r12d
	jne	.L3627
	cmpl	$4, 24(%rdi)
	jne	.L3689
.L3627:
	movq	32(%r15), %rax
	movq	-136(%rbp), %rdi
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rdx
	movq	%rdx, -184(%rbp)
	movq	%rax, -120(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, %rsi
	movq	$0, -96(%rbp)
	movq	$0, -64(%rbp)
	movq	%rax, -176(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZNK2v88internal13FeedbackNexus22ExtractMapsAndHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EE@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rax
	movq	%rdx, %r11
	subq	%rax, %r11
	sarq	$3, %r11
	testl	%r11d, %r11d
	jle	.L3629
	leal	-1(%r11), %r10d
	xorl	%ecx, %ecx
	movq	%r15, %r9
	movq	%r14, %r8
	movq	%r11, -168(%rbp)
	xorl	%r12d, %r12d
	movq	%r10, %r15
	movl	%ecx, %r14d
	movl	$-1, %r13d
	jmp	.L3630
	.p2align 4,,10
	.p2align 3
.L3691:
	addl	$1, %r14d
.L3634:
	leaq	1(%r12), %rsi
	cmpq	%r15, %r12
	je	.L3640
.L3693:
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rdx
	movq	%rsi, %r12
.L3630:
	subq	%rax, %rdx
	movl	%r12d, %ebx
	sarq	$3, %rdx
	cmpq	%r12, %rdx
	jbe	.L3690
	movq	(%rax,%r12,8), %rax
	movq	(%rax), %rsi
	movl	15(%rsi), %edx
	andl	$16777216, %edx
	jne	.L3691
	movq	-120(%rbp), %rdx
	cmpq	%rax, %rdx
	je	.L3635
	testq	%rdx, %rdx
	je	.L3636
	cmpq	(%rdx), %rsi
	jne	.L3636
.L3635:
	movq	-80(%rbp), %rdi
	movq	%r12, %rax
	movl	%ebx, %r13d
	salq	$4, %rax
	addq	%rdi, %rax
	movl	(%rax), %ecx
	cmpl	%ecx, (%r8)
	jne	.L3634
	movq	8(%rax), %rax
	movq	8(%r8), %rdx
	testq	%rax, %rax
	je	.L3692
	testq	%rdx, %rdx
	je	.L3634
	cmpq	%rax, %rdx
	je	.L3638
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	jne	.L3634
.L3638:
	cmpl	$4, 24(%r9)
	jne	.L3667
	movl	%ebx, %r13d
	leaq	1(%r12), %rsi
	cmpq	%r15, %r12
	jne	.L3693
	.p2align 4,,10
	.p2align 3
.L3640:
	movq	-168(%rbp), %r11
	movl	%r14d, %ecx
	xorl	%eax, %eax
	movq	%r9, %r15
	movq	%r8, %r14
	subl	%ecx, %r11d
	cmpl	$-1, %r13d
	setne	%al
	subl	%eax, %r11d
	cmpl	_ZN2v88internal30FLAG_max_polymorphic_map_countE(%rip), %r11d
	jl	.L3661
.L3687:
	movq	-80(%rbp), %rdi
	xorl	%r12d, %r12d
.L3639:
	testq	%rdi, %rdi
	je	.L3660
	call	_ZdlPv@PLT
.L3660:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3625
	call	_ZdlPv@PLT
.L3625:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3694
	addq	$152, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3636:
	.cfi_restore_state
	cmpl	$-1, %r13d
	jne	.L3634
	movq	(%rdx), %rdx
	movq	%r9, %rdi
	movq	%r8, -152(%rbp)
	movq	%r9, -144(%rbp)
	call	_ZN2v88internal2IC31IsTransitionOfMonomorphicTargetENS0_3MapES2_
	movq	-152(%rbp), %r8
	movq	-144(%rbp), %r9
	testb	%al, %al
	cmovne	%ebx, %r13d
	jmp	.L3634
	.p2align 4,,10
	.p2align 3
.L3688:
	leaq	80(%rdi), %rax
	movq	%rax, -136(%rbp)
	jmp	.L3627
	.p2align 4,,10
	.p2align 3
.L3689:
	movq	-136(%rbp), %rdi
	movq	(%rsi), %rbx
	call	_ZNK2v88internal13FeedbackNexus7GetNameEv@PLT
	cmpq	%rax, %rbx
	jne	.L3625
	jmp	.L3627
	.p2align 4,,10
	.p2align 3
.L3661:
	testl	%r11d, %r11d
	je	.L3644
	movl	28(%r15), %ecx
	cmpl	$14, %ecx
	jbe	.L3662
.L3646:
	cmpl	$-1, %r13d
	je	.L3649
	movslq	%r13d, %rsi
	movdqu	(%r14), %xmm1
	movq	%rsi, %rax
	salq	$4, %rax
	addq	-80(%rbp), %rax
	movups	%xmm1, (%rax)
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rdx
	subq	%rax, %rdx
	sarq	$3, %rdx
	cmpq	%rdx, %rsi
	jnb	.L3695
	leaq	(%rax,%rsi,8), %rcx
	movq	-120(%rbp), %rdx
	movq	(%rcx), %rax
	cmpq	%rax, %rdx
	je	.L3651
	testq	%rax, %rax
	je	.L3652
	testq	%rdx, %rdx
	je	.L3652
	movq	(%rax), %rax
	cmpq	%rax, (%rdx)
	je	.L3651
.L3652:
	movq	-120(%rbp), %rax
	movq	%rax, (%rcx)
.L3651:
	movl	28(%r15), %eax
	movq	-160(%rbp), %rsi
	cmpl	$14, %eax
	ja	.L3656
	movl	$25352, %edx
	btq	%rax, %rdx
	jnc	.L3656
.L3657:
	movq	-184(%rbp), %rcx
	movq	-176(%rbp), %rdx
	movq	-136(%rbp), %rdi
	call	_ZN2v88internal13FeedbackNexus20ConfigurePolymorphicENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE@PLT
	movq	80(%r15), %rax
	movb	$1, 16(%r15)
	testq	%rax, %rax
	je	.L3696
	movq	(%rax), %rsi
.L3659:
	movq	8(%r15), %rdi
	movl	96(%r15), %edx
	leaq	.LC39(%rip), %rcx
	movl	$1, %r12d
	call	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	movq	-80(%rbp), %rdi
	jmp	.L3639
	.p2align 4,,10
	.p2align 3
.L3692:
	testq	%rdx, %rdx
	jne	.L3634
	jmp	.L3638
.L3642:
	movl	28(%r15), %ecx
	cmpl	$14, %ecx
	ja	.L3649
	orl	$-1, %r13d
.L3662:
	movl	$25352, %r12d
	shrq	%cl, %r12
	notq	%r12
	andl	$1, %r12d
	jne	.L3646
	movq	-160(%rbp), %rax
	movq	-136(%rbp), %rdi
	movq	(%rax), %rbx
	call	_ZNK2v88internal13FeedbackNexus7GetNameEv@PLT
	cmpq	%rax, %rbx
	je	.L3646
	movq	-80(%rbp), %rdi
	jmp	.L3639
.L3629:
	cmpl	%r11d, _ZN2v88internal30FLAG_max_polymorphic_map_countE(%rip)
	jle	.L3687
	testl	%r11d, %r11d
	jne	.L3642
	movl	24(%r15), %eax
	subl	$3, %eax
	andl	$-3, %eax
	jne	.L3687
.L3644:
	movq	32(%r15), %rdx
	movq	%r15, %rdi
	movq	%r14, %rcx
	movl	$1, %r12d
	movq	-160(%rbp), %rsi
	call	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE
	movq	-80(%rbp), %rdi
	jmp	.L3639
.L3656:
	xorl	%esi, %esi
	jmp	.L3657
.L3696:
	movq	88(%r15), %rsi
	jmp	.L3659
.L3649:
	movq	-104(%rbp), %rsi
	cmpq	-96(%rbp), %rsi
	je	.L3653
	movq	-120(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -104(%rbp)
.L3654:
	movq	-72(%rbp), %rsi
	cmpq	-64(%rbp), %rsi
	je	.L3655
	movl	(%r14), %edx
	movq	8(%r14), %rax
	movl	%edx, (%rsi)
	movq	%rax, 8(%rsi)
	addq	$16, -72(%rbp)
	jmp	.L3651
.L3667:
	xorl	%r12d, %r12d
	jmp	.L3639
.L3690:
	movq	%r12, %rsi
	leaq	.LC40(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L3655:
	movq	-184(%rbp), %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L3651
.L3653:
	movq	-176(%rbp), %rdi
	leaq	-120(%rbp), %rdx
	call	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L3654
.L3694:
	call	__stack_chk_fail@PLT
.L3695:
	leaq	.LC40(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24265:
	.size	_ZN2v88internal2IC19UpdatePolymorphicICENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE, .-_ZN2v88internal2IC19UpdatePolymorphicICENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	.section	.text._ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	.type	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE, @function
_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE:
.LFB24279:
	.cfi_startproc
	endbr64
	cmpl	$7, 24(%rdi)
	ja	.L3726
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	leaq	.L3700(%rip), %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movl	24(%rdi), %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE,"a",@progbits
	.align 4
	.align 4
.L3700:
	.long	.L3699-.L3700
	.long	.L3729-.L3700
	.long	.L3729-.L3700
	.long	.L3703-.L3700
	.long	.L3703-.L3700
	.long	.L3702-.L3700
	.long	.L3705-.L3700
	.long	.L3699-.L3700
	.section	.text._ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	.p2align 4,,10
	.p2align 3
.L3703:
	movl	28(%rdi), %eax
	cmpl	$10, %eax
	ja	.L3702
	movl	$1218, %edx
	btq	%rax, %rdx
	jnc	.L3702
.L3729:
	movq	32(%r12), %rdx
	addq	$8, %rsp
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE
	.p2align 4,,10
	.p2align 3
.L3702:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC19UpdatePolymorphicICENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	testb	%al, %al
	jne	.L3697
	movl	28(%r12), %eax
	cmpl	$14, %eax
	ja	.L3706
	movl	$25352, %edx
	btq	%rax, %rdx
	jnc	.L3706
	cmpl	$4, 24(%r12)
	je	.L3706
.L3707:
	movq	(%r14), %rax
	leaq	80(%r12), %rdi
	xorl	%esi, %esi
	testb	$1, %al
	jne	.L3730
.L3708:
	call	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicENS0_11IcCheckTypeE@PLT
	movq	80(%r12), %rax
	movb	$1, 16(%r12)
	testq	%rax, %rax
	je	.L3731
	movq	(%rax), %rsi
.L3710:
	movl	96(%r12), %edx
	movq	8(%r12), %rdi
	leaq	.LC35(%rip), %rcx
	call	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
.L3705:
	movl	28(%r12), %eax
	cmpl	$9, %eax
	je	.L3711
	subl	$5, %eax
	movq	8(%r12), %rdx
	cmpl	$3, %eax
	jbe	.L3712
	movq	41032(%rdx), %rdi
	movl	0(%r13), %edx
	movq	8(%r13), %rax
	testl	%edx, %edx
	jne	.L3714
.L3732:
	testq	%rax, %rax
	je	.L3717
	movq	(%rax), %rcx
	orq	$2, %rcx
.L3716:
	movq	32(%r12), %rax
	movq	(%r14), %rsi
	movq	(%rax), %rdx
	call	_ZN2v88internal9StubCache3SetENS0_4NameENS0_3MapENS0_11MaybeObjectE@PLT
.L3711:
	movb	$1, 16(%r12)
.L3697:
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3712:
	.cfi_restore_state
	movq	41024(%rdx), %rdi
	movl	0(%r13), %edx
	movq	8(%r13), %rax
	testl	%edx, %edx
	je	.L3732
.L3714:
	testq	%rax, %rax
	je	.L3717
	movq	(%rax), %rcx
	jmp	.L3716
	.p2align 4,,10
	.p2align 3
.L3706:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC24CopyICToMegamorphicCacheENS0_6HandleINS0_4NameEEE
	jmp	.L3707
	.p2align 4,,10
	.p2align 3
.L3699:
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3730:
	movq	-1(%rax), %rax
	xorl	%esi, %esi
	cmpw	$64, 11(%rax)
	setbe	%sil
	jmp	.L3708
	.p2align 4,,10
	.p2align 3
.L3731:
	movq	88(%r12), %rsi
	jmp	.L3710
	.p2align 4,,10
	.p2align 3
.L3717:
	leaq	.LC41(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L3726:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE24279:
	.size	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE, .-_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	.section	.text._ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE:
.LFB24278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -24(%rbp)
	leaq	-32(%rbp), %rdx
	movl	$1, -32(%rbp)
	call	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3736
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3736:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24278:
	.size	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE
	.section	.rodata._ZN2v88internal6LoadIC12UpdateCachesEPNS0_14LookupIteratorE.str1.1,"aMS",@progbits,1
.LC62:
	.string	"LoadGlobalIC"
.LC63:
	.string	"LoadIC"
	.section	.text._ZN2v88internal6LoadIC12UpdateCachesEPNS0_14LookupIteratorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6LoadIC12UpdateCachesEPNS0_14LookupIteratorE
	.type	_ZN2v88internal6LoadIC12UpdateCachesEPNS0_14LookupIteratorE, @function
_ZN2v88internal6LoadIC12UpdateCachesEPNS0_14LookupIteratorE:
.LFB24280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	4(%rsi), %eax
	testl	%eax, %eax
	jne	.L3738
	movq	(%rdi), %rax
	call	*16(%rax)
	leaq	-64(%rbp), %rdx
.L3739:
	movq	32(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	movl	$1, -64(%rbp)
	call	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	movq	32(%rbx), %r13
	movl	_ZN2v88internal12TracingFlags8ic_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3760
.L3737:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3761
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3738:
	.cfi_restore_state
	cmpl	$4, %eax
	je	.L3762
	movl	28(%rdi), %eax
	subl	$6, %eax
	cmpl	$1, %eax
	jbe	.L3763
.L3745:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6LoadIC14ComputeHandlerEPNS0_14LookupIteratorE
	leaq	-64(%rbp), %rdx
	jmp	.L3739
	.p2align 4,,10
	.p2align 3
.L3763:
	movq	%rsi, %rdi
	call	_ZN2v88internal14LookupIterator23TryLookupCachedPropertyEv@PLT
	cmpl	$6, 4(%rbx)
	jne	.L3745
	movq	48(%rbx), %rdx
	movq	56(%rbx), %rax
	cmpq	%rax, %rdx
	je	.L3746
	testq	%rax, %rax
	je	.L3745
	testq	%rdx, %rdx
	je	.L3745
	movq	(%rdx), %rcx
	cmpq	%rcx, (%rax)
	jne	.L3745
.L3746:
	movq	%rbx, %rdi
	call	_ZNK2v88internal14LookupIterator15GetPropertyCellEv@PLT
	leaq	80(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal13FeedbackNexus25ConfigurePropertyCellModeENS0_6HandleINS0_12PropertyCellEEE@PLT
	movq	32(%rbx), %rdx
	leaq	.LC62(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE
	jmp	.L3737
	.p2align 4,,10
	.p2align 3
.L3762:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3764
.L3741:
	movq	8(%r12), %r13
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3742
	movabsq	$51539607552, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L3743:
	movq	8(%r12), %rdi
	movq	32(%r12), %rsi
	leaq	-64(%rbp), %rdx
	movl	$1, -64(%rbp)
	movq	%rdx, -72(%rbp)
	leaq	104(%rdi), %rax
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal11LoadHandler13LoadFullChainEPNS0_7IsolateENS0_6HandleINS0_3MapEEERKNS0_17MaybeObjectHandleENS4_INS0_3SmiEEE@PLT
	movq	-72(%rbp), %rdx
	jmp	.L3739
	.p2align 4,,10
	.p2align 3
.L3760:
	movl	24(%r12), %r8d
	xorl	%ecx, %ecx
	testl	%r8d, %r8d
	jne	.L3765
.L3749:
	movq	%r13, %rdx
	leaq	.LC63(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_
	jmp	.L3737
	.p2align 4,,10
	.p2align 3
.L3742:
	movq	41088(%r13), %rcx
	cmpq	41096(%r13), %rcx
	je	.L3766
.L3744:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movabsq	$51539607552, %rax
	movq	%rax, (%rcx)
	jmp	.L3743
	.p2align 4,,10
	.p2align 3
.L3765:
	leaq	80(%r12), %rdi
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movl	24(%r12), %ecx
	movl	%eax, %r8d
	jmp	.L3749
	.p2align 4,,10
	.p2align 3
.L3764:
	movq	8(%rdi), %rax
	movl	$1123, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3741
.L3766:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L3744
.L3761:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24280:
	.size	_ZN2v88internal6LoadIC12UpdateCachesEPNS0_14LookupIteratorE, .-_ZN2v88internal6LoadIC12UpdateCachesEPNS0_14LookupIteratorE
	.section	.text._ZN2v88internal6LoadIC4LoadENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal6LoadIC4LoadENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEE
	.type	_ZN2v88internal6LoadIC4LoadENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEE, @function
_ZN2v88internal6LoadIC4LoadENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEE:
.LFB24262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movzbl	_ZN2v88internal11FLAG_use_icE(%rip), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jne	.L3768
	xorl	%ebx, %ebx
.L3768:
	cmpl	$9, 28(%r12)
	movq	0(%r13), %rax
	je	.L3881
	movq	8(%r12), %rdi
	movq	%rdi, %rdx
	cmpq	%rax, 104(%rdi)
	jne	.L3882
.L3773:
	testb	%bl, %bl
	jne	.L3771
.L3772:
	movq	8(%r12), %r15
	movq	(%r14), %rax
	cmpq	%rax, 3856(%r15)
	je	.L3883
	cmpl	$9, 28(%r12)
	je	.L3884
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10ErrorUtils28ThrowLoadFromNullOrUndefinedEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEENS0_11MaybeHandleIS5_EE@PLT
	xorl	%eax, %eax
.L3784:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L3885
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3771:
	.cfi_restore_state
	cmpl	$2, 24(%r12)
	je	.L3772
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testl	%edx, %edx
	jne	.L3886
.L3775:
	movq	8(%r12), %rbx
	testb	$1, %al
	jne	.L3776
	addq	$256, %rbx
	movq	%rbx, 32(%r12)
.L3777:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	leaq	-144(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	movl	$1, -144(%rbp)
	call	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	movl	_ZN2v88internal12TracingFlags8ic_statsE(%rip), %eax
	testl	%eax, %eax
	je	.L3772
	movl	24(%r12), %r8d
	xorl	%ecx, %ecx
	testl	%r8d, %r8d
	jne	.L3887
.L3782:
	movq	%r14, %rdx
	leaq	.LC63(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_
	jmp	.L3772
	.p2align 4,,10
	.p2align 3
.L3882:
	cmpq	%rax, 88(%rdi)
	je	.L3773
	movq	%rax, %rcx
	notq	%rcx
	jmp	.L3774
	.p2align 4,,10
	.p2align 3
.L3884:
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	$65, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC9TypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_
	jmp	.L3784
	.p2align 4,,10
	.p2align 3
.L3881:
	movq	%rax, %rcx
	notq	%rcx
	testb	$1, %al
	je	.L3773
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L3773
	movq	8(%r12), %rdi
	movq	%rdi, %rdx
.L3774:
	andl	$1, %ecx
	je	.L3888
.L3787:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal8JSObject18MakePrototypesFastENS0_6HandleINS0_6ObjectEEENS0_12WhereToStartEPNS0_7IsolateE@PLT
	movq	0(%r13), %rax
	movq	8(%r12), %r15
	testb	$1, %al
	jne	.L3790
	leaq	256(%r15), %rax
	movq	%rax, 32(%r12)
.L3791:
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L3795
.L3797:
	movl	$-1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	%rax, %rcx
.L3796:
	movq	(%r14), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L3798
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L3798:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	(%r14), %rax
	movq	%r15, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r14, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L3889
.L3799:
	leaq	-144(%rbp), %r15
	movq	%rax, -112(%rbp)
	movq	%r15, %rdi
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movl	28(%r12), %eax
	movl	%eax, -152(%rbp)
	movl	-140(%rbp), %eax
	cmpl	$4, %eax
	je	.L3801
	leaq	.L3804(%rip), %rdx
.L3800:
	cmpl	$7, %eax
	ja	.L3802
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal6LoadIC4LoadENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEE,"a",@progbits
	.align 4
	.align 4
.L3804:
	.long	.L3806-.L3804
	.long	.L3801-.L3804
	.long	.L3805-.L3804
	.long	.L3801-.L3804
	.long	.L3802-.L3804
	.long	.L3801-.L3804
	.long	.L3801-.L3804
	.long	.L3803-.L3804
	.section	.text._ZN2v88internal6LoadIC4LoadENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEE
	.p2align 4,,10
	.p2align 3
.L3805:
	movq	-88(%rbp), %rsi
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
.L3876:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L3890
.L3810:
	movq	71(%rax), %rcx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	cmpq	%rax, %rcx
	je	.L3811
	movq	31(%rcx), %rax
.L3811:
	movq	-120(%rbp), %rcx
	movq	88(%rcx), %rdi
	cmpq	%rdi, 7(%rax)
	jne	.L3801
	cmpl	$9, -152(%rbp)
	je	.L3891
.L3802:
	movq	%r15, %rdi
	call	_ZN2v88internal14LookupIterator4NextEv@PLT
	movl	-140(%rbp), %eax
	leaq	.L3804(%rip), %rdx
	cmpl	$4, %eax
	jne	.L3800
	.p2align 4,,10
	.p2align 3
.L3801:
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$64, 11(%rdx)
	je	.L3892
.L3823:
	cmpl	$4, -140(%rbp)
	je	.L3893
.L3834:
	testb	%bl, %bl
	je	.L3879
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6LoadIC12UpdateCachesEPNS0_14LookupIteratorE
.L3879:
	movl	28(%r12), %eax
.L3832:
	cmpl	$9, %eax
	je	.L3894
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L3880
	cmpl	$4, -140(%rbp)
	je	.L3842
.L3843:
	movq	%rbx, %rax
	jmp	.L3784
	.p2align 4,,10
	.p2align 3
.L3806:
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
	cmpw	$1026, 11(%rax)
	jne	.L3801
	movq	%r15, %rdi
	call	_ZNK2v88internal14LookupIterator9HasAccessEv@PLT
	testb	%al, %al
	jne	.L3802
	jmp	.L3801
	.p2align 4,,10
	.p2align 3
.L3883:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal10ErrorUtils16NewIteratorErrorEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
	jmp	.L3784
	.p2align 4,,10
	.p2align 3
.L3892:
	movl	11(%rax), %edx
	testb	$1, %dl
	je	.L3823
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	je	.L3895
.L3824:
	movq	0(%r13), %rax
	testb	$1, %al
	je	.L3823
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jne	.L3823
	cmpl	$4, -140(%rbp)
	movl	28(%r12), %eax
	jne	.L3832
	cmpl	$6, %eax
	jne	.L3832
	.p2align 4,,10
	.p2align 3
.L3833:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC14ReferenceErrorENS0_6HandleINS0_4NameEEE
	jmp	.L3784
	.p2align 4,,10
	.p2align 3
.L3790:
	movq	-1(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3792
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3793:
	movq	%rax, 32(%r12)
	movq	8(%r12), %r15
	jmp	.L3791
	.p2align 4,,10
	.p2align 3
.L3888:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L3874
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	je	.L3874
	movq	%r13, %rsi
	xorl	%ebx, %ebx
	call	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	8(%r12), %rdx
	jmp	.L3787
	.p2align 4,,10
	.p2align 3
.L3874:
	movq	%rdi, %rdx
	jmp	.L3787
	.p2align 4,,10
	.p2align 3
.L3795:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L3797
	movq	%r13, %rcx
	jmp	.L3796
	.p2align 4,,10
	.p2align 3
.L3890:
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdi
	cmpw	$68, 11(%rcx)
	je	.L3876
	movq	(%rdi), %rcx
	cmpw	$1105, 11(%rcx)
	jne	.L3810
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L3810
	.p2align 4,,10
	.p2align 3
.L3893:
	cmpl	$6, 28(%r12)
	jne	.L3834
	jmp	.L3833
	.p2align 4,,10
	.p2align 3
.L3894:
	movq	%r15, %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	jne	.L3837
.L3880:
	xorl	%eax, %eax
	jmp	.L3784
	.p2align 4,,10
	.p2align 3
.L3891:
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
.L3878:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L3896
.L3817:
	movq	71(%rax), %rsi
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %rax
	cmpq	%rax, %rsi
	je	.L3818
	movq	31(%rsi), %rax
.L3818:
	movq	88(%rcx), %rcx
	cmpq	%rcx, 23(%rax)
	je	.L3802
	jmp	.L3801
	.p2align 4,,10
	.p2align 3
.L3776:
	movq	-1(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3778
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L3779:
	movq	%rax, 32(%r12)
	jmp	.L3777
.L3803:
	leaq	.LC30(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3792:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L3897
.L3794:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L3793
	.p2align 4,,10
	.p2align 3
.L3837:
	movq	8(%r12), %rdx
	leaq	112(%rdx), %rcx
	addq	$120, %rdx
	shrw	$8, %ax
	movq	%rcx, %rax
	cmove	%rdx, %rax
	jmp	.L3784
	.p2align 4,,10
	.p2align 3
.L3778:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L3898
.L3780:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L3779
	.p2align 4,,10
	.p2align 3
.L3895:
	andl	$16, %edx
	je	.L3824
	cmpl	$4, -140(%rbp)
	jne	.L3824
	movq	8(%r12), %rbx
	movq	15(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3828
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L3829:
	movq	%r13, %rdx
	movl	$259, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC9TypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_
	jmp	.L3784
	.p2align 4,,10
	.p2align 3
.L3896:
	movq	-1(%rax), %rsi
	leaq	-1(%rax), %rdi
	cmpw	$68, 11(%rsi)
	je	.L3878
	movq	(%rdi), %rsi
	cmpw	$1105, 11(%rsi)
	jne	.L3817
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	-120(%rbp), %rcx
	jmp	.L3817
	.p2align 4,,10
	.p2align 3
.L3889:
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rcx, -152(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-152(%rbp), %rcx
	jmp	.L3799
	.p2align 4,,10
	.p2align 3
.L3842:
	cmpl	$6, 28(%r12)
	je	.L3833
	movq	8(%r12), %rax
	movq	41016(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger10is_loggingEv@PLT
	testb	%al, %al
	je	.L3843
	movq	0(%r13), %rdx
	movq	(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Logger16SuspectReadEventENS0_4NameENS0_6ObjectE@PLT
	jmp	.L3843
	.p2align 4,,10
	.p2align 3
.L3886:
	movq	8(%r12), %rax
	movl	$1127, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	0(%r13), %rax
	jmp	.L3775
.L3887:
	leaq	80(%r12), %rdi
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movl	24(%r12), %ecx
	movl	%eax, %r8d
	jmp	.L3782
.L3897:
	movq	%r15, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L3794
.L3828:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L3899
.L3830:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L3829
.L3898:
	movq	%rbx, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L3780
.L3899:
	movq	%rbx, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L3830
.L3885:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24262:
	.size	_ZN2v88internal6LoadIC4LoadENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEE, .-_ZN2v88internal6LoadIC4LoadENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEE
	.section	.text._ZN2v88internal12LoadGlobalIC4LoadENS0_6HandleINS0_4NameEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12LoadGlobalIC4LoadENS0_6HandleINS0_4NameEEE
	.type	_ZN2v88internal12LoadGlobalIC4LoadENS0_6HandleINS0_4NameEEE, @function
_ZN2v88internal12LoadGlobalIC4LoadENS0_6HandleINS0_4NameEEE:
.LFB24263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%rbx), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3901
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L3902:
	movq	0(%r13), %rax
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L3904
	movq	(%r14), %rax
	movq	8(%r12), %rdx
	movq	23(%rax), %rax
	movq	1135(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3905
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L3906:
	movq	0(%r13), %rdx
	movq	8(%r12), %rdi
	leaq	-92(%rbp), %rcx
	call	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE@PLT
	testb	%al, %al
	jne	.L3935
.L3904:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6LoadIC4LoadENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEE
.L3923:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3936
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3905:
	.cfi_restore_state
	movq	41088(%rdx), %rbx
	cmpq	%rbx, 41096(%rdx)
	je	.L3937
.L3907:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%rbx)
	jmp	.L3906
	.p2align 4,,10
	.p2align 3
.L3901:
	movq	41088(%rbx), %r14
	cmpq	41096(%rbx), %r14
	je	.L3938
.L3903:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r14)
	jmp	.L3902
	.p2align 4,,10
	.p2align 3
.L3935:
	movl	-92(%rbp), %eax
	movq	8(%r12), %r14
	movq	(%rbx), %rdx
	leal	24(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3909
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
.L3910:
	movl	-88(%rbp), %eax
	movq	8(%r12), %r14
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rsi,%rax), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3912
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %rbx
.L3913:
	movq	8(%r12), %rax
	cmpq	%rsi, 96(%rax)
	je	.L3939
	movl	24(%r12), %eax
	testl	%eax, %eax
	je	.L3917
	cmpb	$0, _ZN2v88internal11FLAG_use_icE(%rip)
	jne	.L3940
.L3917:
	movq	%rbx, %rax
	jmp	.L3923
	.p2align 4,,10
	.p2align 3
.L3938:
	movq	%rbx, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L3903
	.p2align 4,,10
	.p2align 3
.L3912:
	movq	41088(%r14), %rbx
	cmpq	41096(%r14), %rbx
	je	.L3941
.L3914:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%rbx)
	jmp	.L3913
	.p2align 4,,10
	.p2align 3
.L3909:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L3942
.L3911:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L3910
	.p2align 4,,10
	.p2align 3
.L3937:
	movq	%rdx, %rdi
	movq	%rsi, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%rax, %rbx
	jmp	.L3907
	.p2align 4,,10
	.p2align 3
.L3940:
	xorl	%ecx, %ecx
	movl	-88(%rbp), %edx
	cmpb	$1, -84(%rbp)
	leaq	80(%r12), %rdi
	movl	-92(%rbp), %esi
	sete	%cl
	call	_ZN2v88internal13FeedbackNexus23ConfigureLexicalVarModeEiib@PLT
	testb	%al, %al
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	je	.L3918
	testl	%eax, %eax
	jne	.L3943
.L3919:
	movq	%r13, %rdx
	leaq	.LC62(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE
	jmp	.L3917
	.p2align 4,,10
	.p2align 3
.L3939:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC14ReferenceErrorENS0_6HandleINS0_4NameEEE
	jmp	.L3923
	.p2align 4,,10
	.p2align 3
.L3942:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	jmp	.L3911
	.p2align 4,,10
	.p2align 3
.L3941:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-104(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3918:
	testl	%eax, %eax
	jne	.L3944
.L3920:
	movq	(%r12), %rax
	leaq	_ZNK2v88internal12LoadGlobalIC9slow_stubEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3921
	movq	8(%r12), %rax
	movl	$120, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
.L3922:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$1, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	jmp	.L3919
.L3921:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3922
.L3944:
	movq	8(%r12), %rax
	movl	$1105, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3920
.L3943:
	movq	8(%r12), %rax
	movl	$1104, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3919
.L3936:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24263:
	.size	_ZN2v88internal12LoadGlobalIC4LoadENS0_6HandleINS0_4NameEEE, .-_ZN2v88internal12LoadGlobalIC4LoadENS0_6HandleINS0_4NameEEE
	.section	.rodata._ZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC64:
	.string	"V8.Runtime_Runtime_LoadGlobalIC_Miss"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC65:
	.string	"args[3].IsNumber()"
	.section	.rodata._ZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC66:
	.string	"args[3].ToInt32(&typeof_value)"
	.section	.text._ZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE.isra.0:
.LFB30910:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -240(%rbp)
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L3994
.L3946:
	movq	_ZZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2203(%rip), %rbx
	testq	%rbx, %rbx
	je	.L3995
.L3948:
	movq	$0, -272(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L3996
.L3950:
	movq	41088(%r14), %rax
	addl	$1, 41104(%r14)
	leaq	-192(%rbp), %r13
	movq	%r13, %rdi
	movq	%rax, -312(%rbp)
	movq	41096(%r14), %rax
	movq	%rax, -296(%rbp)
	movq	12464(%r14), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r14), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L3954
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L3955:
	movq	-24(%r12), %rax
	leaq	-16(%r12), %rdx
	testb	$1, %al
	jne	.L3997
.L3957:
	leaq	-284(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rdx, -304(%rbp)
	movl	$0, -284(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	movq	-304(%rbp), %rdx
	testb	%al, %al
	je	.L3998
	movq	-16(%r12), %rax
	movslq	-4(%r12), %rsi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37504(%rcx), %rax
	movl	-284(%rbp), %ecx
	movl	$0, %eax
	movq	%rbx, -192(%rbp)
	cmove	%rax, %rdx
	xorl	%eax, %eax
	movq	%r14, -184(%rbp)
	testl	%ecx, %ecx
	movb	$0, -176(%rbp)
	sete	%al
	movb	$0, -128(%rbp)
	addl	$6, %eax
	movq	$0, -120(%rbp)
	movl	%eax, -164(%rbp)
	movq	%rdx, -112(%rbp)
	movq	$0, -104(%rbp)
	movl	%esi, -96(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	testq	%rdx, %rdx
	je	.L3962
	movq	(%rdx), %rax
	leaq	-280(%rbp), %rdi
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-112(%rbp), %rdi
	movl	%eax, -92(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
.L3969:
	movl	%eax, -168(%rbp)
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r12, %rdx
	movl	%eax, -172(%rbp)
	leaq	16+_ZTVN2v88internal12LoadGlobalICE(%rip), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12LoadGlobalIC4LoadENS0_6HandleINS0_4NameEEE
	testq	%rax, %rax
	je	.L3999
	movq	(%rax), %r12
.L3964:
	movq	-152(%rbp), %rdi
	movq	%rbx, -192(%rbp)
	testq	%rdi, %rdi
	je	.L3965
	call	_ZdlPv@PLT
.L3965:
	movq	-312(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	movq	-296(%rbp), %rax
	cmpq	41096(%r14), %rax
	je	.L3968
	movq	%rax, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L3968:
	leaq	-272(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4000
.L3945:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4001
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3962:
	.cfi_restore_state
	movl	$0, -92(%rbp)
	xorl	%eax, %eax
	jmp	.L3969
	.p2align 4,,10
	.p2align 3
.L3997:
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	je	.L3957
	leaq	.LC65(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L3954:
	movq	41088(%r14), %r15
	cmpq	41096(%r14), %r15
	je	.L4002
.L3956:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r14)
	movq	%rsi, (%r15)
	jmp	.L3955
	.p2align 4,,10
	.p2align 3
.L3996:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4003
.L3951:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3952
	movq	(%rdi), %rax
	call	*8(%rax)
.L3952:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3953
	movq	(%rdi), %rax
	call	*8(%rax)
.L3953:
	leaq	.LC64(%rip), %rax
	movq	%rbx, -264(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-264(%rbp), %rax
	movq	%r13, -248(%rbp)
	movq	%rax, -272(%rbp)
	jmp	.L3950
	.p2align 4,,10
	.p2align 3
.L3995:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4004
.L3949:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2203(%rip)
	jmp	.L3948
	.p2align 4,,10
	.p2align 3
.L3999:
	movq	312(%r14), %r12
	jmp	.L3964
	.p2align 4,,10
	.p2align 3
.L3994:
	movq	40960(%rsi), %rax
	movl	$323, %edx
	leaq	-232(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L3946
	.p2align 4,,10
	.p2align 3
.L3998:
	leaq	.LC66(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4002:
	movq	%r14, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-304(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L3956
	.p2align 4,,10
	.p2align 3
.L4000:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L3945
	.p2align 4,,10
	.p2align 3
.L4004:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L3949
	.p2align 4,,10
	.p2align 3
.L4003:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC64(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L3951
.L4001:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30910:
	.size	_ZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal25Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE:
.LFB24321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %ebx
	testl	%ebx, %ebx
	jne	.L4030
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	leaq	-160(%rbp), %r12
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -184(%rbp)
	movq	12464(%rdx), %rax
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L4008
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L4009:
	movq	-24(%r14), %rax
	leaq	-16(%r14), %rdx
	testb	$1, %al
	je	.L4011
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L4031
.L4011:
	leaq	-172(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rdx, -192(%rbp)
	movl	$0, -172(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	movq	-192(%rbp), %rdx
	testb	%al, %al
	je	.L4032
	movq	-16(%r14), %rax
	movslq	-4(%r14), %rsi
	pxor	%xmm0, %xmm0
	movq	%rax, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37504(%rcx), %rax
	movl	-172(%rbp), %ecx
	movl	$0, %eax
	movq	%r13, -152(%rbp)
	cmove	%rax, %rdx
	xorl	%eax, %eax
	movb	$0, -144(%rbp)
	testl	%ecx, %ecx
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rcx
	movb	$0, -96(%rbp)
	sete	%al
	movq	%rcx, -160(%rbp)
	addl	$6, %eax
	movq	$0, -88(%rbp)
	movl	%eax, -132(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	%esi, -64(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	testq	%rdx, %rdx
	je	.L4016
	movq	(%rdx), %rax
	leaq	-168(%rbp), %rdi
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, -60(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movl	%eax, %ebx
.L4022:
	leaq	16+_ZTVN2v88internal12LoadGlobalICE(%rip), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r14, %rdx
	movl	%ebx, -136(%rbp)
	movl	%ebx, -140(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12LoadGlobalIC4LoadENS0_6HandleINS0_4NameEEE
	testq	%rax, %rax
	je	.L4033
	movq	(%rax), %r12
.L4018:
	movq	-120(%rbp), %rdi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rax, -160(%rbp)
	testq	%rdi, %rdi
	je	.L4019
	call	_ZdlPv@PLT
.L4019:
	movq	-200(%rbp), %rax
	subl	$1, 41104(%r13)
	movq	%rax, 41088(%r13)
	movq	-184(%rbp), %rax
	cmpq	41096(%r13), %rax
	je	.L4005
	movq	%rax, 41096(%r13)
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4005:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4034
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4008:
	.cfi_restore_state
	movq	41088(%r13), %r15
	cmpq	41096(%r13), %r15
	je	.L4035
.L4010:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%r15)
	jmp	.L4009
	.p2align 4,,10
	.p2align 3
.L4016:
	movl	$0, -60(%rbp)
	jmp	.L4022
	.p2align 4,,10
	.p2align 3
.L4033:
	movq	312(%r13), %r12
	jmp	.L4018
	.p2align 4,,10
	.p2align 3
.L4030:
	movq	%rdx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r12
	jmp	.L4005
	.p2align 4,,10
	.p2align 3
.L4031:
	leaq	.LC65(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4032:
	leaq	.LC66(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4035:
	movq	%r13, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L4010
.L4034:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24321:
	.size	_ZN2v88internal25Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internal7StoreIC12UpdateCachesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE.str1.1,"aMS",@progbits,1
.LC67:
	.string	"StoreGlobalIC"
.LC68:
	.string	"LookupForWrite said 'false'"
.LC69:
	.string	"StoreIC"
	.section	.text._ZN2v88internal7StoreIC12UpdateCachesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7StoreIC12UpdateCachesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE
	.type	_ZN2v88internal7StoreIC12UpdateCachesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE, @function
_ZN2v88internal7StoreIC12UpdateCachesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE:
.LFB24301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$1, -64(%rbp)
	movq	$0, -56(%rbp)
	call	_ZN2v88internal7StoreIC14LookupForWriteEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE
	testb	%al, %al
	je	.L4037
	movl	28(%r12), %eax
	cmpl	$1, %eax
	je	.L4059
	cmpl	$10, %eax
	jne	.L4038
.L4059:
	cmpl	$6, 4(%rbx)
	je	.L4077
.L4038:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7StoreIC14ComputeHandlerEPNS0_14LookupIteratorE
	movl	%eax, -64(%rbp)
	movq	%rdx, -56(%rbp)
.L4042:
	movq	32(%rbx), %rsi
	leaq	-64(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	movq	32(%rbx), %r13
	movl	_ZN2v88internal12TracingFlags8ic_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4078
.L4036:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4079
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4037:
	.cfi_restore_state
	cmpl	$1, 24(%r12)
	je	.L4080
.L4043:
	leaq	.LC68(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, 72(%r12)
	movq	(%r12), %rax
	call	*16(%rax)
	movl	$1, -64(%rbp)
	movq	%rax, -56(%rbp)
	jmp	.L4042
	.p2align 4,,10
	.p2align 3
.L4080:
	movl	28(%r12), %eax
	cmpl	$1, %eax
	je	.L4060
	cmpl	$10, %eax
	jne	.L4043
.L4060:
	cmpl	$2, 4(%rbx)
	jne	.L4043
	movq	56(%rbx), %rax
	movq	(%rax), %rax
	movq	-1(%rax), %rax
.L4076:
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L4081
.L4048:
	movq	71(%rax), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	-37504(%rax), %r13
	cmpq	%r13, %rdx
	je	.L4049
	movq	31(%rdx), %r13
.L4049:
	movq	%rbx, %rdi
	call	_ZNK2v88internal14LookupIterator33HolderIsReceiverOrHiddenPrototypeEv@PLT
	testb	%al, %al
	jne	.L4043
	movq	8(%r12), %rax
	movq	7(%r13), %rdi
	cmpq	%rdi, 88(%rax)
	je	.L4043
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testl	%edx, %edx
	jne	.L4082
.L4050:
	movq	32(%r12), %rsi
	leaq	80(%r12), %rdi
	call	_ZN2v88internal13FeedbackNexus23ConfigurePremonomorphicENS0_6HandleINS0_3MapEEE@PLT
	movq	80(%r12), %rax
	movb	$1, 16(%r12)
	testq	%rax, %rax
	je	.L4083
	movq	(%rax), %rsi
.L4052:
	movl	96(%r12), %edx
	movq	8(%r12), %rdi
	leaq	.LC36(%rip), %rcx
	call	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	movq	32(%rbx), %rdx
	leaq	.LC67(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE
	jmp	.L4036
	.p2align 4,,10
	.p2align 3
.L4077:
	movq	48(%rbx), %rdx
	movq	56(%rbx), %rax
	cmpq	%rax, %rdx
	je	.L4040
	testq	%rax, %rax
	je	.L4038
	testq	%rdx, %rdx
	je	.L4038
	movq	(%rdx), %rcx
	cmpq	%rcx, (%rax)
	jne	.L4038
.L4040:
	movq	%rbx, %rdi
	call	_ZNK2v88internal14LookupIterator15GetPropertyCellEv@PLT
	leaq	80(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal13FeedbackNexus25ConfigurePropertyCellModeENS0_6HandleINS0_12PropertyCellEEE@PLT
	movq	32(%rbx), %rdx
	leaq	.LC67(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE
	jmp	.L4036
	.p2align 4,,10
	.p2align 3
.L4078:
	movl	24(%r12), %r8d
	xorl	%ecx, %ecx
	testl	%r8d, %r8d
	jne	.L4084
.L4054:
	movq	%r13, %rdx
	leaq	.LC69(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_
	jmp	.L4036
	.p2align 4,,10
	.p2align 3
.L4084:
	leaq	80(%r12), %rdi
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movl	24(%r12), %ecx
	movl	%eax, %r8d
	jmp	.L4054
.L4081:
	movq	-1(%rax), %rdx
	leaq	-1(%rax), %rcx
	cmpw	$68, 11(%rdx)
	je	.L4076
	movq	(%rcx), %rdx
	cmpw	$1105, 11(%rdx)
	jne	.L4048
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	jmp	.L4048
.L4083:
	movq	88(%r12), %rsi
	jmp	.L4052
.L4079:
	call	__stack_chk_fail@PLT
.L4082:
	movq	40960(%rax), %rdi
	movl	$1134, %esi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4050
	.cfi_endproc
.LFE24301:
	.size	_ZN2v88internal7StoreIC12UpdateCachesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE, .-_ZN2v88internal7StoreIC12UpdateCachesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE
	.section	.text._ZN2v88internal7StoreIC5StoreENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEES4_NS0_11StoreOriginE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7StoreIC5StoreENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEES4_NS0_11StoreOriginE
	.type	_ZN2v88internal7StoreIC5StoreENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEES4_NS0_11StoreOriginE, @function
_ZN2v88internal7StoreIC5StoreENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEES4_NS0_11StoreOriginE:
.LFB24300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$120, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L4157
.L4087:
	movl	24(%r12), %edx
	movq	104(%rdi), %rcx
	testl	%edx, %edx
	je	.L4092
	movzbl	_ZN2v88internal11FLAG_use_icE(%rip), %r8d
	testb	%r8b, %r8b
	je	.L4092
	cmpq	%rcx, %rax
	jne	.L4158
.L4096:
	cmpl	$2, %edx
	je	.L4095
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testl	%edx, %edx
	jne	.L4159
.L4107:
	movq	8(%r12), %rbx
	testb	$1, %al
	jne	.L4108
	addq	$256, %rbx
	movq	%rbx, 32(%r12)
.L4109:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	leaq	-144(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -136(%rbp)
	movl	$1, -144(%rbp)
	call	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	movl	_ZN2v88internal12TracingFlags8ic_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4160
	.p2align 4,,10
	.p2align 3
.L4095:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	$86, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC9TypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_
.L4091:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L4161
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4092:
	.cfi_restore_state
	cmpq	%rcx, %rax
	je	.L4095
	cmpq	88(%rdi), %rax
	je	.L4095
	xorl	%r8d, %r8d
.L4094:
	movq	%rdi, %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	movb	%r8b, -152(%rbp)
	call	_ZN2v88internal8JSObject18MakePrototypesFastENS0_6HandleINS0_6ObjectEEENS0_12WhereToStartEPNS0_7IsolateE@PLT
	movq	(%r14), %rax
	movq	8(%r12), %rdi
	movzbl	-152(%rbp), %r8d
	testb	$1, %al
	jne	.L4097
.L4099:
	movl	$-1, %edx
	movq	%r14, %rsi
	movb	%r8b, -160(%rbp)
	movq	%rdi, -152(%rbp)
	call	_ZN2v88internal14LookupIterator23GetRootForNonJSReceiverEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEj@PLT
	movq	-152(%rbp), %rdi
	movzbl	-160(%rbp), %r8d
	movq	%rax, %rcx
.L4098:
	movq	(%r15), %rdx
	movl	$3, %eax
	movq	-1(%rdx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L4100
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L4100:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	(%r15), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r15, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L4162
.L4101:
	leaq	-144(%rbp), %r9
	movb	%r8b, -160(%rbp)
	movq	%r9, %rdi
	movq	%rax, -112(%rbp)
	movq	%r9, -152(%rbp)
	movq	$0, -104(%rbp)
	movq	%r14, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	(%r15), %rax
	movq	-1(%rax), %rdx
	movq	-152(%rbp), %r9
	movzbl	-160(%rbp), %r8d
	cmpw	$64, 11(%rdx)
	je	.L4163
.L4104:
	testb	%r8b, %r8b
	jne	.L4164
.L4122:
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r9, %rdi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_14LookupIteratorENS0_6HandleIS1_EENS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	testb	%al, %al
	movl	$0, %eax
	cmovne	%r13, %rax
	jmp	.L4091
	.p2align 4,,10
	.p2align 3
.L4157:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	jbe	.L4087
	movq	-1(%rax), %rdx
	movl	15(%rdx), %edx
	andl	$16777216, %edx
	je	.L4087
	call	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	8(%r12), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal6Object11SetPropertyEPNS0_7IsolateENS0_6HandleIS1_EENS4_INS0_4NameEEES5_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	jmp	.L4091
	.p2align 4,,10
	.p2align 3
.L4160:
	movl	24(%r12), %r8d
	xorl	%ecx, %ecx
	testl	%r8d, %r8d
	jne	.L4165
.L4114:
	movq	%r15, %rdx
	leaq	.LC69(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_
	jmp	.L4095
	.p2align 4,,10
	.p2align 3
.L4108:
	movq	-1(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4110
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L4111:
	movq	%rax, 32(%r12)
	jmp	.L4109
	.p2align 4,,10
	.p2align 3
.L4163:
	movl	11(%rax), %edx
	testb	$1, %dl
	je	.L4104
	movq	-1(%rax), %rcx
	cmpw	$64, 11(%rcx)
	je	.L4166
.L4106:
	movq	(%r14), %rax
	testb	$1, %al
	je	.L4104
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jne	.L4104
	jmp	.L4122
	.p2align 4,,10
	.p2align 3
.L4097:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4099
	movq	%r14, %rcx
	jmp	.L4098
	.p2align 4,,10
	.p2align 3
.L4164:
	movq	%r9, %rsi
	movl	%ebx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%r9, -152(%rbp)
	call	_ZN2v88internal7StoreIC12UpdateCachesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_11StoreOriginE
	movq	-152(%rbp), %r9
	jmp	.L4122
	.p2align 4,,10
	.p2align 3
.L4162:
	movq	%r15, %rsi
	movq	%rcx, -160(%rbp)
	movb	%r8b, -152(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-160(%rbp), %rcx
	movzbl	-152(%rbp), %r8d
	jmp	.L4101
	.p2align 4,,10
	.p2align 3
.L4110:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L4167
.L4112:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L4111
	.p2align 4,,10
	.p2align 3
.L4159:
	movq	40960(%rdi), %rdi
	movl	$1136, %esi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	(%r14), %rax
	jmp	.L4107
	.p2align 4,,10
	.p2align 3
.L4166:
	andl	$16, %edx
	je	.L4106
	cmpl	$4, -140(%rbp)
	jne	.L4106
	movq	8(%r12), %rbx
	movq	15(%rax), %r13
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4118
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L4119:
	movq	%r14, %rdx
	movl	$260, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC9TypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_
	jmp	.L4091
.L4165:
	leaq	80(%r12), %rdi
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movl	24(%r12), %ecx
	movl	%eax, %r8d
	jmp	.L4114
.L4167:
	movq	%rbx, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	jmp	.L4112
.L4118:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L4168
.L4120:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%r13, (%rcx)
	jmp	.L4119
.L4168:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L4120
.L4161:
	call	__stack_chk_fail@PLT
.L4158:
	cmpq	88(%rdi), %rax
	je	.L4096
	jmp	.L4094
	.cfi_endproc
.LFE24300:
	.size	_ZN2v88internal7StoreIC5StoreENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEES4_NS0_11StoreOriginE, .-_ZN2v88internal7StoreIC5StoreENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEES4_NS0_11StoreOriginE
	.section	.rodata._ZN2v88internalL26Stats_Runtime_StoreIC_MissEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC70:
	.string	"V8.Runtime_Runtime_StoreIC_Miss"
	.section	.text._ZN2v88internalL26Stats_Runtime_StoreIC_MissEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL26Stats_Runtime_StoreIC_MissEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL26Stats_Runtime_StoreIC_MissEiPmPNS0_7IsolateE.isra.0:
.LFB30912:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -240(%rbp)
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4210
.L4170:
	movq	_ZZN2v88internalL26Stats_Runtime_StoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2296(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4211
.L4172:
	movq	$0, -272(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L4212
.L4174:
	movq	41088(%r12), %rax
	leaq	-16(%r13), %rcx
	leaq	-24(%r13), %r14
	addl	$1, 41104(%r12)
	leaq	-32(%r13), %r15
	movslq	-4(%r13), %r8
	movq	%rax, -312(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -296(%rbp)
	movq	-16(%r13), %rax
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-37504(%rsi), %rax
	jne	.L4213
	xorl	%ecx, %ecx
	movl	$11, %eax
	leaq	-192(%rbp), %r9
.L4178:
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movq	%r12, -184(%rbp)
	movq	%rbx, -192(%rbp)
	movb	$0, -176(%rbp)
	movl	%eax, -164(%rbp)
	movb	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	$0, -104(%rbp)
	movl	%r8d, -96(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	testq	%rcx, %rcx
	je	.L4179
	movq	(%rcx), %rax
	leaq	-280(%rbp), %rdi
	movl	%r8d, %esi
	movq	%r9, -304(%rbp)
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-112(%rbp), %rdi
	movl	%eax, -92(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	-304(%rbp), %r9
.L4186:
	movq	%r9, %rdi
	movl	%eax, -168(%rbp)
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	%eax, -172(%rbp)
	leaq	16+_ZTVN2v88internal7StoreICE(%rip), %rax
	movq	%r9, -304(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	-304(%rbp), %r9
	movl	$1, %r8d
	movq	%r9, %rdi
	call	_ZN2v88internal7StoreIC5StoreENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEES4_NS0_11StoreOriginE
	testq	%rax, %rax
	je	.L4214
	movq	(%rax), %r13
.L4181:
	movq	-152(%rbp), %rdi
	movq	%rbx, -192(%rbp)
	testq	%rdi, %rdi
	je	.L4182
	call	_ZdlPv@PLT
.L4182:
	subl	$1, 41104(%r12)
	movq	-312(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-296(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L4185
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4185:
	leaq	-272(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4215
.L4169:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4216
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4179:
	.cfi_restore_state
	movl	$0, -92(%rbp)
	xorl	%eax, %eax
	jmp	.L4186
	.p2align 4,,10
	.p2align 3
.L4213:
	leaq	-192(%rbp), %r9
	movl	%r8d, %esi
	movq	%rcx, -328(%rbp)
	movq	%r9, %rdi
	movq	%r8, -320(%rbp)
	movq	%r9, -304(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	-304(%rbp), %r9
	movq	-320(%rbp), %r8
	movq	-328(%rbp), %rcx
	jmp	.L4178
	.p2align 4,,10
	.p2align 3
.L4212:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4217
.L4175:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4176
	movq	(%rdi), %rax
	call	*8(%rax)
.L4176:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4177
	movq	(%rdi), %rax
	call	*8(%rax)
.L4177:
	leaq	.LC70(%rip), %rax
	movq	%rbx, -264(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-264(%rbp), %rax
	movq	%r14, -248(%rbp)
	movq	%rax, -272(%rbp)
	jmp	.L4174
	.p2align 4,,10
	.p2align 3
.L4211:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4218
.L4173:
	movq	%rbx, _ZZN2v88internalL26Stats_Runtime_StoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2296(%rip)
	jmp	.L4172
	.p2align 4,,10
	.p2align 3
.L4214:
	movq	312(%r12), %r13
	jmp	.L4181
	.p2align 4,,10
	.p2align 3
.L4215:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4169
	.p2align 4,,10
	.p2align 3
.L4210:
	movq	40960(%rsi), %rax
	movl	$331, %edx
	leaq	-232(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4170
	.p2align 4,,10
	.p2align 3
.L4218:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L4173
	.p2align 4,,10
	.p2align 3
.L4217:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC70(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L4175
.L4216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30912:
	.size	_ZN2v88internalL26Stats_Runtime_StoreIC_MissEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL26Stats_Runtime_StoreIC_MissEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal20Runtime_StoreIC_MissEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20Runtime_StoreIC_MissEiPmPNS0_7IsolateE
	.type	_ZN2v88internal20Runtime_StoreIC_MissEiPmPNS0_7IsolateE, @function
_ZN2v88internal20Runtime_StoreIC_MissEiPmPNS0_7IsolateE:
.LFB24330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %edx
	testl	%edx, %edx
	jne	.L4236
	movq	41088(%r12), %rax
	leaq	-16(%rsi), %rcx
	leaq	-24(%rsi), %r14
	addl	$1, 41104(%r12)
	leaq	-32(%rsi), %r15
	movslq	-4(%rsi), %r8
	movq	%rax, -200(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -184(%rbp)
	movq	-16(%rsi), %rax
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-37504(%rsi), %rax
	jne	.L4237
	xorl	%ecx, %ecx
	movl	$11, %eax
	leaq	-160(%rbp), %r9
.L4222:
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movq	%r12, -152(%rbp)
	movq	%rbx, -160(%rbp)
	movb	$0, -144(%rbp)
	movl	%eax, -132(%rbp)
	movb	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	%r8d, -64(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	testq	%rcx, %rcx
	je	.L4223
	movq	(%rcx), %rax
	leaq	-168(%rbp), %rdi
	movl	%r8d, %esi
	movq	%r9, -192(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, -60(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	-192(%rbp), %r9
	movl	%eax, %edx
.L4229:
	movq	%r9, %rdi
	movl	%edx, -136(%rbp)
	movq	%r14, %rsi
	leaq	16+_ZTVN2v88internal7StoreICE(%rip), %rax
	movl	%edx, -140(%rbp)
	movq	%r15, %rdx
	movq	%r9, -192(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	-192(%rbp), %r9
	movl	$1, %r8d
	movq	%r9, %rdi
	call	_ZN2v88internal7StoreIC5StoreENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEES4_NS0_11StoreOriginE
	testq	%rax, %rax
	je	.L4238
	movq	(%rax), %r13
.L4225:
	movq	-120(%rbp), %rdi
	movq	%rbx, -160(%rbp)
	testq	%rdi, %rdi
	je	.L4226
	call	_ZdlPv@PLT
.L4226:
	subl	$1, 41104(%r12)
	movq	-200(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-184(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L4219
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4219:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4239
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4223:
	.cfi_restore_state
	movl	$0, -60(%rbp)
	jmp	.L4229
	.p2align 4,,10
	.p2align 3
.L4237:
	leaq	-160(%rbp), %r9
	movl	%r8d, %esi
	movl	%edx, -220(%rbp)
	movq	%r9, %rdi
	movq	%rcx, -216(%rbp)
	movq	%r8, -208(%rbp)
	movq	%r9, -192(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	-192(%rbp), %r9
	movq	-208(%rbp), %r8
	movq	-216(%rbp), %rcx
	movl	-220(%rbp), %edx
	jmp	.L4222
	.p2align 4,,10
	.p2align 3
.L4238:
	movq	312(%r12), %r13
	jmp	.L4225
	.p2align 4,,10
	.p2align 3
.L4236:
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internalL26Stats_Runtime_StoreIC_MissEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L4219
.L4239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24330:
	.size	_ZN2v88internal20Runtime_StoreIC_MissEiPmPNS0_7IsolateE, .-_ZN2v88internal20Runtime_StoreIC_MissEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE
	.type	_ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE, @function
_ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE:
.LFB24299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$72, %rsp
	movq	8(%rdi), %rdx
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdx), %rax
	movq	%rdx, -104(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-104(%rbp), %rdx
	movq	%rax, %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L4241
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L4242:
	movq	(%r14), %rax
	movq	8(%r12), %rdx
	movq	23(%rax), %rax
	movq	1135(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L4244
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L4245:
	movq	(%r15), %rdx
	movq	8(%r12), %rdi
	leaq	-92(%rbp), %rcx
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE@PLT
	testb	%al, %al
	je	.L4247
	movl	-92(%rbp), %eax
	movq	-104(%rbp), %r8
	movq	8(%r12), %rdx
	movq	(%r8), %rcx
	leal	24(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L4248
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpb	$1, -84(%rbp)
	movq	%rax, %r8
	je	.L4281
.L4251:
	movl	-88(%rbp), %eax
	movq	8(%r12), %r14
	movq	(%r8), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4253
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %r8
	movq	(%rax), %rsi
.L4254:
	movq	8(%r12), %rax
	cmpq	%rsi, 96(%rax)
	je	.L4282
	movl	24(%r12), %eax
	movl	-88(%rbp), %edx
	testl	%eax, %eax
	je	.L4257
	cmpb	$0, _ZN2v88internal11FLAG_use_icE(%rip)
	jne	.L4283
.L4257:
	movq	(%r8), %r14
	leal	16(,%rdx,8), %eax
	movq	0(%r13), %r12
	cltq
	leaq	-1(%r14,%rax), %r15
	movq	%r12, (%r15)
	testb	$1, %r12b
	je	.L4266
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L4264
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L4264:
	testb	$24, %al
	je	.L4266
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4284
	.p2align 4,,10
	.p2align 3
.L4266:
	movq	%r13, %rax
	jmp	.L4252
	.p2align 4,,10
	.p2align 3
.L4247:
	movl	$1, %r8d
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7StoreIC5StoreENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEES4_NS0_11StoreOriginE
.L4252:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L4285
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4248:
	.cfi_restore_state
	movq	41088(%rdx), %r8
	cmpq	41096(%rdx), %r8
	je	.L4286
.L4250:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r8)
	cmpb	$1, -84(%rbp)
	jne	.L4251
.L4281:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	$37, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC9TypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_
	jmp	.L4252
	.p2align 4,,10
	.p2align 3
.L4244:
	movq	41088(%rdx), %r8
	cmpq	%r8, 41096(%rdx)
	je	.L4287
.L4246:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r8)
	jmp	.L4245
	.p2align 4,,10
	.p2align 3
.L4241:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L4288
.L4243:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L4242
	.p2align 4,,10
	.p2align 3
.L4253:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L4289
.L4255:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L4254
	.p2align 4,,10
	.p2align 3
.L4288:
	movq	%rdx, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L4243
	.p2align 4,,10
	.p2align 3
.L4287:
	movq	%rdx, %rdi
	movq	%rsi, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L4246
	.p2align 4,,10
	.p2align 3
.L4282:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC14ReferenceErrorENS0_6HandleINS0_4NameEEE
	jmp	.L4252
	.p2align 4,,10
	.p2align 3
.L4284:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4266
	.p2align 4,,10
	.p2align 3
.L4286:
	movq	%rdx, %rdi
	movq	%rsi, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L4250
	.p2align 4,,10
	.p2align 3
.L4283:
	xorl	%ecx, %ecx
	movl	-92(%rbp), %esi
	cmpb	$1, -84(%rbp)
	leaq	80(%r12), %rdi
	sete	%cl
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal13FeedbackNexus23ConfigureLexicalVarModeEiib@PLT
	movq	-104(%rbp), %r8
	testb	%al, %al
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	je	.L4258
	testl	%eax, %eax
	jne	.L4290
.L4259:
	movq	%r15, %rdx
	leaq	.LC67(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE
	movl	-88(%rbp), %edx
	movq	-104(%rbp), %r8
	jmp	.L4257
	.p2align 4,,10
	.p2align 3
.L4289:
	movq	%r14, %rdi
	movq	%rsi, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %r8
	jmp	.L4255
	.p2align 4,,10
	.p2align 3
.L4258:
	testl	%eax, %eax
	jne	.L4291
.L4260:
	movq	(%r12), %rax
	leaq	_ZNK2v88internal13StoreGlobalIC9slow_stubEv(%rip), %rdx
	movq	%r8, -104(%rbp)
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4261
	movq	8(%r12), %rax
	movl	$126, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	-104(%rbp), %r8
.L4262:
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	movl	$1, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	movq	-104(%rbp), %r8
	jmp	.L4259
.L4261:
	movq	%r12, %rdi
	call	*%rax
	movq	-104(%rbp), %r8
	jmp	.L4262
.L4291:
	movq	8(%r12), %rax
	movl	$1132, %esi
	movq	%r8, -104(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	-104(%rbp), %r8
	jmp	.L4260
.L4290:
	movq	8(%r12), %rax
	movl	$1133, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	-104(%rbp), %r8
	jmp	.L4259
.L4285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24299:
	.size	_ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE, .-_ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE
	.section	.text._ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE.constprop.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE.constprop.0, @function
_ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE.constprop.0:
.LFB31083:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$72, %rsp
	movq	8(%rdi), %rdx
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	12464(%rdx), %rax
	movq	%rdx, -104(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-104(%rbp), %rdx
	movq	%rax, %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L4293
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
.L4294:
	movq	(%r14), %rax
	movq	8(%r12), %rdx
	movq	23(%rax), %rax
	movq	1135(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L4296
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r8
.L4297:
	movq	(%r15), %rdx
	movq	8(%r12), %rdi
	leaq	-92(%rbp), %rcx
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE@PLT
	testb	%al, %al
	je	.L4299
	movl	-92(%rbp), %eax
	movq	-104(%rbp), %r8
	movq	8(%r12), %rdx
	movq	(%r8), %rcx
	leal	24(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rcx), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L4300
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpb	$1, -84(%rbp)
	movq	%rax, %r8
	je	.L4331
.L4303:
	movl	-88(%rbp), %eax
	movq	8(%r12), %r14
	movq	(%r8), %rdx
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rax,%rdx), %rsi
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4305
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-104(%rbp), %r8
	movq	(%rax), %rsi
.L4306:
	movq	8(%r12), %rax
	cmpq	%rsi, 96(%rax)
	je	.L4332
	movl	24(%r12), %eax
	movl	-88(%rbp), %edx
	testl	%eax, %eax
	je	.L4309
	cmpb	$0, _ZN2v88internal11FLAG_use_icE(%rip)
	jne	.L4333
.L4309:
	movq	(%r8), %r14
	leal	16(,%rdx,8), %eax
	movq	0(%r13), %r12
	cltq
	leaq	-1(%r14,%rax), %r15
	movq	%r12, (%r15)
	testb	$1, %r12b
	je	.L4316
	movq	%r12, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	je	.L4314
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
.L4314:
	testb	$24, %al
	je	.L4316
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L4334
	.p2align 4,,10
	.p2align 3
.L4316:
	movq	%r13, %rax
	jmp	.L4304
	.p2align 4,,10
	.p2align 3
.L4299:
	movl	$1, %r8d
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7StoreIC5StoreENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEES4_NS0_11StoreOriginE
.L4304:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L4335
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4300:
	.cfi_restore_state
	movq	41088(%rdx), %r8
	cmpq	41096(%rdx), %r8
	je	.L4336
.L4302:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r8)
	cmpb	$1, -84(%rbp)
	jne	.L4303
.L4331:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	$37, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC9TypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_
	jmp	.L4304
	.p2align 4,,10
	.p2align 3
.L4296:
	movq	41088(%rdx), %r8
	cmpq	41096(%rdx), %r8
	je	.L4337
.L4298:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r8)
	jmp	.L4297
	.p2align 4,,10
	.p2align 3
.L4293:
	movq	41088(%rdx), %r14
	cmpq	41096(%rdx), %r14
	je	.L4338
.L4295:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r14)
	jmp	.L4294
	.p2align 4,,10
	.p2align 3
.L4305:
	movq	41088(%r14), %rax
	cmpq	41096(%r14), %rax
	je	.L4339
.L4307:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r14)
	movq	%rsi, (%rax)
	jmp	.L4306
	.p2align 4,,10
	.p2align 3
.L4338:
	movq	%rdx, %rdi
	movq	%rdx, -104(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%rax, %r14
	jmp	.L4295
	.p2align 4,,10
	.p2align 3
.L4337:
	movq	%rdx, %rdi
	movq	%rsi, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L4298
	.p2align 4,,10
	.p2align 3
.L4332:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC14ReferenceErrorENS0_6HandleINS0_4NameEEE
	jmp	.L4304
	.p2align 4,,10
	.p2align 3
.L4334:
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L4316
	.p2align 4,,10
	.p2align 3
.L4336:
	movq	%rdx, %rdi
	movq	%rsi, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%rax, %r8
	jmp	.L4302
	.p2align 4,,10
	.p2align 3
.L4333:
	xorl	%ecx, %ecx
	movl	-92(%rbp), %esi
	cmpb	$1, -84(%rbp)
	leaq	80(%r12), %rdi
	sete	%cl
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal13FeedbackNexus23ConfigureLexicalVarModeEiib@PLT
	movq	-104(%rbp), %r8
	testb	%al, %al
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	je	.L4310
	testl	%eax, %eax
	jne	.L4340
.L4311:
	movq	%r15, %rdx
	leaq	.LC67(%rip), %rsi
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE
	movl	-88(%rbp), %edx
	movq	-104(%rbp), %r8
	jmp	.L4309
	.p2align 4,,10
	.p2align 3
.L4339:
	movq	%r14, %rdi
	movq	%rsi, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %r8
	jmp	.L4307
	.p2align 4,,10
	.p2align 3
.L4310:
	testl	%eax, %eax
	jne	.L4341
.L4312:
	movq	8(%r12), %rax
	movl	$126, %esi
	movq	%r8, -104(%rbp)
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	$1, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal2IC10PatchCacheENS0_6HandleINS0_4NameEEERKNS0_17MaybeObjectHandleE
	movq	-104(%rbp), %r8
	jmp	.L4311
.L4340:
	movq	8(%r12), %rax
	movl	$1133, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	-104(%rbp), %r8
	jmp	.L4311
.L4341:
	movq	8(%r12), %rax
	movl	$1132, %esi
	movq	%r8, -104(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	-104(%rbp), %r8
	jmp	.L4312
.L4335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31083:
	.size	_ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE.constprop.0, .-_ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE.constprop.0
	.section	.rodata._ZN2v88internalL32Stats_Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC71:
	.string	"V8.Runtime_Runtime_StoreGlobalIC_Miss"
	.section	.text._ZN2v88internalL32Stats_Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Stats_Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Stats_Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateE:
.LFB24332:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -240(%rbp)
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4382
.L4343:
	movq	_ZZN2v88internalL32Stats_Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2326(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4383
.L4345:
	movq	$0, -272(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L4384
.L4347:
	movq	41088(%r15), %rax
	leaq	-16(%r12), %rcx
	addl	$1, 41104(%r15)
	leaq	-192(%rbp), %r14
	movslq	-4(%r12), %rdx
	movq	%r14, %rdi
	movq	%rcx, -312(%rbp)
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movq	%rax, -320(%rbp)
	movq	41096(%r15), %rax
	leaq	-24(%r12), %r13
	movl	%edx, %esi
	movq	%rdx, -304(%rbp)
	movq	%rax, -296(%rbp)
	movq	-16(%r12), %rax
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	-304(%rbp), %rdx
	leaq	-280(%rbp), %r9
	movq	-312(%rbp), %rcx
	movl	%eax, -164(%rbp)
	movq	-16(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	%r9, %rdi
	movl	%edx, %esi
	movq	%rcx, -112(%rbp)
	movl	%edx, -96(%rbp)
	movq	%r9, -304(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, -192(%rbp)
	movq	%r15, -184(%rbp)
	movb	$0, -176(%rbp)
	movb	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-112(%rbp), %rdi
	movl	%eax, -92(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	-304(%rbp), %r9
	movl	%eax, -168(%rbp)
	movl	%eax, -172(%rbp)
	leaq	16+_ZTVN2v88internal13StoreGlobalICE(%rip), %rax
	movq	%r9, %rdi
	movq	%rax, -192(%rbp)
	movq	12464(%r15), %rax
	movq	%rax, -280(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r15), %rdi
	movq	%rax, %r9
	testq	%rdi, %rdi
	je	.L4351
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4352:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE.constprop.0
	testq	%rax, %rax
	je	.L4385
	movq	(%rax), %r12
.L4355:
	movq	-152(%rbp), %rdi
	movq	%rbx, -192(%rbp)
	testq	%rdi, %rdi
	je	.L4356
	call	_ZdlPv@PLT
.L4356:
	movq	-320(%rbp), %rax
	subl	$1, 41104(%r15)
	movq	%rax, 41088(%r15)
	movq	-296(%rbp), %rax
	cmpq	41096(%r15), %rax
	je	.L4359
	movq	%rax, 41096(%r15)
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4359:
	leaq	-272(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4386
.L4342:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4387
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4351:
	.cfi_restore_state
	movq	41088(%r15), %rsi
	cmpq	41096(%r15), %rsi
	je	.L4388
.L4353:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r15)
	movq	%r9, (%rsi)
	jmp	.L4352
	.p2align 4,,10
	.p2align 3
.L4384:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4389
.L4348:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4349
	movq	(%rdi), %rax
	call	*8(%rax)
.L4349:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4350
	movq	(%rdi), %rax
	call	*8(%rax)
.L4350:
	leaq	.LC71(%rip), %rax
	movq	%rbx, -264(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-264(%rbp), %rax
	movq	%r13, -248(%rbp)
	movq	%rax, -272(%rbp)
	jmp	.L4347
	.p2align 4,,10
	.p2align 3
.L4383:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4390
.L4346:
	movq	%rbx, _ZZN2v88internalL32Stats_Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2326(%rip)
	jmp	.L4345
	.p2align 4,,10
	.p2align 3
.L4385:
	movq	312(%r15), %r12
	jmp	.L4355
	.p2align 4,,10
	.p2align 3
.L4386:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4342
	.p2align 4,,10
	.p2align 3
.L4382:
	movq	40960(%rdx), %rax
	leaq	-232(%rbp), %rsi
	movl	$328, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4343
	.p2align 4,,10
	.p2align 3
.L4388:
	movq	%r15, %rdi
	movq	%rax, -304(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-304(%rbp), %r9
	movq	%rax, %rsi
	jmp	.L4353
	.p2align 4,,10
	.p2align 3
.L4390:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L4346
	.p2align 4,,10
	.p2align 3
.L4389:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC71(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L4348
.L4387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24332:
	.size	_ZN2v88internalL32Stats_Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Stats_Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal26Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal26Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateE
	.type	_ZN2v88internal26Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateE, @function
_ZN2v88internal26Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateE:
.LFB24333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4407
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	leaq	-16(%rsi), %rcx
	leaq	-24(%rsi), %r13
	leaq	-160(%rbp), %r15
	movq	%rcx, -200(%rbp)
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movq	%rax, -208(%rbp)
	movq	41096(%rdx), %rax
	movq	%r15, %rdi
	movslq	-4(%rsi), %rdx
	movq	%rax, -184(%rbp)
	movq	-16(%rsi), %rax
	movl	%edx, %esi
	movq	%rdx, -192(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	-192(%rbp), %rdx
	leaq	-168(%rbp), %r10
	movq	-200(%rbp), %rcx
	movl	%eax, -132(%rbp)
	movq	-16(%r14), %rax
	pxor	%xmm0, %xmm0
	movq	%r10, %rdi
	movl	%edx, %esi
	movq	%rcx, -80(%rbp)
	movl	%edx, -64(%rbp)
	movq	%r10, -192(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, -160(%rbp)
	movq	%r12, -152(%rbp)
	movb	$0, -144(%rbp)
	movb	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, -60(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	-192(%rbp), %r10
	movl	%eax, -136(%rbp)
	movl	%eax, -140(%rbp)
	leaq	16+_ZTVN2v88internal13StoreGlobalICE(%rip), %rax
	movq	%r10, %rdi
	movq	%rax, -160(%rbp)
	movq	12464(%r12), %rax
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r12), %rdi
	movq	%rax, %r10
	testq	%rdi, %rdi
	je	.L4394
	movq	%rax, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L4395:
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE.constprop.0
	testq	%rax, %rax
	je	.L4408
	movq	(%rax), %r13
.L4398:
	movq	-120(%rbp), %rdi
	movq	%rbx, -160(%rbp)
	testq	%rdi, %rdi
	je	.L4399
	call	_ZdlPv@PLT
.L4399:
	subl	$1, 41104(%r12)
	movq	-208(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-184(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L4391
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4391:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4409
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4394:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L4410
.L4396:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r10, (%rsi)
	jmp	.L4395
	.p2align 4,,10
	.p2align 3
.L4408:
	movq	312(%r12), %r13
	jmp	.L4398
	.p2align 4,,10
	.p2align 3
.L4407:
	call	_ZN2v88internalL32Stats_Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L4391
	.p2align 4,,10
	.p2align 3
.L4410:
	movq	%r12, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-192(%rbp), %r10
	movq	%rax, %rsi
	jmp	.L4396
.L4409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24333:
	.size	_ZN2v88internal26Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateE, .-_ZN2v88internal26Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Stats_Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC72:
	.string	"V8.Runtime_Runtime_StoreGlobalICNoFeedback_Miss"
	.section	.text._ZN2v88internalL42Stats_Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Stats_Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL42Stats_Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateE.isra.0:
.LFB30866:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$224, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -224(%rbp)
	movq	$0, -192(%rbp)
	movaps	%xmm0, -208(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4448
.L4412:
	movq	_ZZN2v88internalL42Stats_Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2343(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4449
.L4414:
	movq	$0, -256(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L4450
.L4416:
	movl	$4294967295, %eax
	pxor	%xmm0, %xmm0
	leaq	-8(%r14), %rsi
	movq	%r14, %rdx
	addl	$1, 41104(%r12)
	leaq	-176(%rbp), %rdi
	movq	41088(%r12), %r13
	movq	%rax, -80(%rbp)
	leaq	16+_ZTVN2v88internal13StoreGlobalICE(%rip), %rax
	movq	41096(%r12), %rbx
	movq	%r12, -168(%rbp)
	movb	$0, -160(%rbp)
	movl	$10, -148(%rbp)
	movb	$0, -112(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -156(%rbp)
	movq	%rax, -176(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movups	%xmm0, -104(%rbp)
	call	_ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE.constprop.0
	testq	%rax, %rax
	je	.L4451
	movq	(%rax), %r14
.L4421:
	movq	-136(%rbp), %rdi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rax, -176(%rbp)
	testq	%rdi, %rdi
	je	.L4422
	call	_ZdlPv@PLT
.L4422:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4425
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4425:
	leaq	-256(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4452
.L4411:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4453
	leaq	-32(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4450:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4454
.L4417:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4418
	movq	(%rdi), %rax
	call	*8(%rax)
.L4418:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4419
	movq	(%rdi), %rax
	call	*8(%rax)
.L4419:
	leaq	.LC72(%rip), %rax
	movq	%rbx, -248(%rbp)
	movq	%rax, -240(%rbp)
	leaq	-248(%rbp), %rax
	movq	%r13, -232(%rbp)
	movq	%rax, -256(%rbp)
	jmp	.L4416
	.p2align 4,,10
	.p2align 3
.L4449:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4455
.L4415:
	movq	%rbx, _ZZN2v88internalL42Stats_Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2343(%rip)
	jmp	.L4414
	.p2align 4,,10
	.p2align 3
.L4451:
	movq	312(%r12), %r14
	jmp	.L4421
	.p2align 4,,10
	.p2align 3
.L4452:
	leaq	-216(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4411
	.p2align 4,,10
	.p2align 3
.L4448:
	movq	40960(%rsi), %rax
	movl	$329, %edx
	leaq	-216(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -224(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4412
	.p2align 4,,10
	.p2align 3
.L4455:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L4415
	.p2align 4,,10
	.p2align 3
.L4454:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC72(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L4417
.L4453:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30866:
	.size	_ZN2v88internalL42Stats_Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL42Stats_Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal36Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal36Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateE
	.type	_ZN2v88internal36Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateE, @function
_ZN2v88internal36Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateE:
.LFB24336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4469
	addl	$1, 41104(%rdx)
	movl	$4294967295, %eax
	pxor	%xmm0, %xmm0
	movq	41088(%rdx), %r13
	movq	%rax, -48(%rbp)
	movq	41096(%rdx), %rbx
	leaq	-8(%rsi), %rsi
	leaq	16+_ZTVN2v88internal13StoreGlobalICE(%rip), %rax
	movq	%rdx, -136(%rbp)
	leaq	-144(%rbp), %rdi
	movq	%r8, %rdx
	movb	$0, -128(%rbp)
	movl	$10, -116(%rbp)
	movb	$0, -80(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -124(%rbp)
	movq	%rax, -144(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movups	%xmm0, -72(%rbp)
	call	_ZN2v88internal13StoreGlobalIC5StoreENS0_6HandleINS0_4NameEEENS2_INS0_6ObjectEEE.constprop.0
	testq	%rax, %rax
	je	.L4470
	movq	(%rax), %r14
.L4460:
	movq	-104(%rbp), %rdi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rax, -144(%rbp)
	testq	%rdi, %rdi
	je	.L4461
	call	_ZdlPv@PLT
.L4461:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L4456
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4456:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4471
	addq	$112, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4470:
	.cfi_restore_state
	movq	312(%r12), %r14
	jmp	.L4460
	.p2align 4,,10
	.p2align 3
.L4469:
	movq	%rdx, %rsi
	movq	%r8, %rdi
	call	_ZN2v88internalL42Stats_Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r14
	jmp	.L4456
.L4471:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24336:
	.size	_ZN2v88internal36Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateE, .-_ZN2v88internal36Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateE
	.section	.text._ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB29110:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$576460752303423487, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$4, %rax
	cmpq	%rdi, %rax
	je	.L4491
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L4482
	movabsq	$9223372036854775792, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L4492
.L4474:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	16(%r14), %r8
.L4481:
	movq	8(%rdx), %rsi
	movl	(%rdx), %edx
	addq	%r14, %rcx
	movl	%edx, (%rcx)
	movq	%rsi, 8(%rcx)
	cmpq	%r15, %rbx
	je	.L4476
	movq	%r14, %rcx
	movq	%r15, %rdx
	.p2align 4,,10
	.p2align 3
.L4477:
	movl	(%rdx), %edi
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	addq	$16, %rcx
	movl	%edi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L4477
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	16(%r14,%rdx), %r8
.L4476:
	cmpq	%r12, %rbx
	je	.L4478
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L4479:
	movq	8(%rdx), %rsi
	movl	(%rdx), %edi
	addq	$16, %rdx
	addq	$16, %rcx
	movl	%edi, -16(%rcx)
	movq	%rsi, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L4479
	subq	%rbx, %rdx
	addq	%rdx, %r8
.L4478:
	testq	%r15, %r15
	je	.L4480
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L4480:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4492:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L4475
	movl	$16, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L4481
	.p2align 4,,10
	.p2align 3
.L4482:
	movl	$16, %esi
	jmp	.L4474
.L4475:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$4, %rax
	movq	%rax, %rsi
	jmp	.L4474
.L4491:
	leaq	.LC42(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE29110:
	.size	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZN2v88internal12KeyedStoreIC31StoreElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_20KeyedAccessStoreModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12KeyedStoreIC31StoreElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_20KeyedAccessStoreModeE
	.type	_ZN2v88internal12KeyedStoreIC31StoreElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_20KeyedAccessStoreModeE, @function
_ZN2v88internal12KeyedStoreIC31StoreElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_20KeyedAccessStoreModeE:
.LFB24308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r12
	movl	%ecx, -100(%rbp)
	movq	%rsi, -96(%rbp)
	movq	(%rsi), %rcx
	movq	%r12, %rdx
	subq	%rcx, %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, %rax
	sarq	$5, %rdx
	sarq	$3, %rax
	testq	%rdx, %rdx
	jle	.L4494
	salq	$5, %rdx
	addq	%rcx, %rdx
	jmp	.L4499
	.p2align 4,,10
	.p2align 3
.L4551:
	movq	8(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L4547
	movq	16(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L4548
	movq	24(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L4549
	addq	$32, %rcx
	cmpq	%rcx, %rdx
	je	.L4550
.L4499:
	movq	(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	je	.L4551
.L4495:
	cmpq	%r12, %rcx
	je	.L4504
	leaq	8(%rcx), %rax
	cmpq	%rax, %r12
	je	.L4507
	.p2align 4,,10
	.p2align 3
.L4509:
	movq	(%rax), %rdx
	movq	(%rdx), %rsi
	movl	15(%rsi), %esi
	andl	$16777216, %esi
	jne	.L4508
	movq	%rdx, (%rcx)
	addq	$8, %rcx
.L4508:
	addq	$8, %rax
	cmpq	%rax, %r12
	jne	.L4509
	movq	-96(%rbp), %rax
	movq	8(%rax), %rax
	cmpq	%rcx, %r12
	je	.L4531
.L4529:
	movq	%rax, %rdx
	subq	%r12, %rdx
	cmpq	%rax, %r12
	je	.L4510
	movq	%rcx, %rdi
	movq	%r12, %rsi
	call	memmove@PLT
	movq	%rax, %rcx
	movq	-96(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, %rdx
	subq	%r12, %rdx
.L4510:
	leaq	(%rcx,%rdx), %rbx
	movq	%rbx, -88(%rbp)
	cmpq	%rax, %rbx
	je	.L4506
	movq	-96(%rbp), %rax
	movq	%rbx, 8(%rax)
.L4506:
	movq	-96(%rbp), %rax
	leaq	-80(%rbp), %r15
	movq	(%rax), %r12
	cmpq	-88(%rbp), %r12
	je	.L4493
	.p2align 4,,10
	.p2align 3
.L4528:
	movq	(%r12), %rbx
	movq	(%rbx), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L4515
	movq	8(%r14), %rsi
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal3Map38DictionaryElementsInPrototypeChainOnlyEPNS0_7IsolateE@PLT
	testb	%al, %al
	je	.L4552
.L4515:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4553
.L4514:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
.L4518:
	movl	$1, -80(%rbp)
	movq	8(%r13), %rsi
	movq	%rax, -72(%rbp)
	cmpq	16(%r13), %rsi
	je	.L4525
	movl	$1, (%rsi)
	addq	$8, %r12
	movq	%rax, 8(%rsi)
	addq	$16, 8(%r13)
	cmpq	%r12, -88(%rbp)
	jne	.L4528
.L4493:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4554
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4525:
	.cfi_restore_state
	movq	%r15, %rdx
	movq	%r13, %rdi
	addq	$8, %r12
	call	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	cmpq	%r12, -88(%rbp)
	jne	.L4528
	jmp	.L4493
	.p2align 4,,10
	.p2align 3
.L4552:
	movq	(%rbx), %rax
	movq	8(%r14), %rsi
	movq	%r15, %rdi
	movq	-96(%rbp), %rdx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal3Map31FindElementsKindTransitionedMapEPNS0_7IsolateERKSt6vectorINS0_6HandleIS1_EESaIS6_EE@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	jne	.L4555
.L4517:
	movl	-100(%rbp), %edx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12KeyedStoreIC19StoreElementHandlerENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeE
	jmp	.L4518
	.p2align 4,,10
	.p2align 3
.L4553:
	movq	8(%r14), %rax
	movl	$1101, %esi
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4514
	.p2align 4,,10
	.p2align 3
.L4555:
	movq	(%rbx), %rax
	movl	15(%rax), %edx
	movq	8(%r14), %rsi
	andl	$33554432, %edx
	je	.L4545
.L4519:
	movq	41112(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L4521
	movq	%r11, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4517
.L4522:
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4556
.L4524:
	movq	8(%r14), %rdi
	movl	-100(%rbp), %ecx
	movq	%rbx, %rsi
	call	_ZN2v88internal12StoreHandler22StoreElementTransitionEPNS0_7IsolateENS0_6HandleINS0_3MapEEES6_NS0_20KeyedAccessStoreModeE@PLT
	jmp	.L4518
	.p2align 4,,10
	.p2align 3
.L4550:
	movq	%r12, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
.L4494:
	cmpq	$2, %rax
	je	.L4500
	cmpq	$3, %rax
	je	.L4501
	cmpq	$1, %rax
	je	.L4502
.L4504:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -88(%rbp)
	jmp	.L4506
	.p2align 4,,10
	.p2align 3
.L4521:
	movq	41088(%rsi), %rdx
	cmpq	%rdx, 41096(%rsi)
	je	.L4557
.L4523:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rsi)
	movq	%r11, (%rdx)
	jmp	.L4522
.L4501:
	movq	(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L4495
	addq	$8, %rcx
.L4500:
	movq	(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L4495
	addq	$8, %rcx
.L4502:
	movq	(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	je	.L4504
	jmp	.L4495
	.p2align 4,,10
	.p2align 3
.L4547:
	addq	$8, %rcx
	jmp	.L4495
	.p2align 4,,10
	.p2align 3
.L4548:
	addq	$16, %rcx
	jmp	.L4495
	.p2align 4,,10
	.p2align 3
.L4549:
	addq	$24, %rcx
	jmp	.L4495
	.p2align 4,,10
	.p2align 3
.L4545:
	movl	15(%rax), %edx
	andl	$33554432, %edx
	jne	.L4519
	movq	%r11, -112(%rbp)
	movl	15(%rax), %edx
	movq	%r15, %rdi
	orl	$33554432, %edx
	movl	%edx, 15(%rax)
	movl	$1, %edx
	movq	55(%rax), %rax
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	movq	8(%r14), %rsi
	movq	-112(%rbp), %r11
	jmp	.L4519
	.p2align 4,,10
	.p2align 3
.L4556:
	movq	8(%r14), %rax
	movl	$1099, %esi
	movq	%rdx, -112(%rbp)
	movq	40960(%rax), %rdi
	addq	$23240, %rdi
	call	_ZN2v88internal16RuntimeCallStats23CorrectCurrentCounterIdENS0_20RuntimeCallCounterIdE@PLT
	movq	-112(%rbp), %rdx
	jmp	.L4524
.L4507:
	movq	-96(%rbp), %rax
	movq	8(%rax), %rax
	jmp	.L4529
.L4557:
	movq	%rsi, %rdi
	movq	%r11, -120(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %r11
	movq	-112(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L4523
.L4554:
	call	__stack_chk_fail@PLT
.L4531:
	movq	%rax, -88(%rbp)
	jmp	.L4506
	.cfi_endproc
.LFE24308:
	.size	_ZN2v88internal12KeyedStoreIC31StoreElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_20KeyedAccessStoreModeE, .-_ZN2v88internal12KeyedStoreIC31StoreElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_20KeyedAccessStoreModeE
	.section	.rodata._ZN2v88internal12KeyedStoreIC18UpdateStoreElementENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeES4_.str1.1,"aMS",@progbits,1
.LC73:
	.string	"JSPrimitiveWrapper"
.LC74:
	.string	"same map added twice"
.LC75:
	.string	"store mode mismatch"
	.section	.rodata._ZN2v88internal12KeyedStoreIC18UpdateStoreElementENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeES4_.str1.8,"aMS",@progbits,1
	.align 8
.LC76:
	.string	"unsupported combination of external and normal arrays"
	.section	.text._ZN2v88internal12KeyedStoreIC18UpdateStoreElementENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12KeyedStoreIC18UpdateStoreElementENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeES4_
	.type	_ZN2v88internal12KeyedStoreIC18UpdateStoreElementENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeES4_, @function
_ZN2v88internal12KeyedStoreIC18UpdateStoreElementENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeES4_:
.LFB24303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -136(%rbp)
	xorl	%esi, %esi
	movl	%edx, -140(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 64(%rdi)
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	jne	.L4559
	movb	$1, 64(%rdi)
	leaq	40(%rdi), %rsi
	leaq	80(%rdi), %rdi
	call	_ZNK2v88internal13FeedbackNexus11ExtractMapsEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EE@PLT
	movq	-104(%rbp), %rsi
.L4559:
	movq	40(%r14), %rbx
	movq	48(%r14), %r15
	cmpq	%r15, %rbx
	je	.L4560
	leaq	-80(%rbp), %r13
	jmp	.L4563
	.p2align 4,,10
	.p2align 3
.L4662:
	movq	%rax, (%rsi)
	movq	-104(%rbp), %rax
	addq	$8, %rbx
	leaq	8(%rax), %rsi
	movq	%rsi, -104(%rbp)
	cmpq	%rbx, %r15
	je	.L4560
.L4563:
	movq	(%rbx), %rax
	movq	%rax, -80(%rbp)
	cmpq	%rsi, -96(%rbp)
	jne	.L4662
	leaq	-112(%rbp), %rdi
	movq	%r13, %rdx
	addq	$8, %rbx
	call	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-104(%rbp), %rsi
	cmpq	%rbx, %r15
	jne	.L4563
	.p2align 4,,10
	.p2align 3
.L4560:
	movq	-112(%rbp), %rdi
	movq	%rdi, %rax
	cmpq	%rsi, %rdi
	je	.L4663
	.p2align 4,,10
	.p2align 3
.L4568:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L4567
	movq	(%rdx), %rdx
	cmpw	$1041, 11(%rdx)
	je	.L4664
.L4567:
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L4568
	leaq	80(%r14), %rbx
	movq	%rbx, %rdi
	call	_ZNK2v88internal13FeedbackNexus23GetKeyedAccessStoreModeEv@PLT
	movq	-104(%rbp), %rsi
	movl	%eax, %r15d
	movq	-112(%rbp), %rax
	cmpq	%rsi, %rax
	je	.L4665
	cmpl	$3, 24(%r14)
	je	.L4666
	movq	-136(%rbp), %rcx
	movq	%rcx, -80(%rbp)
.L4574:
	movq	-80(%rbp), %rcx
	testq	%rcx, %rcx
	jne	.L4581
	jmp	.L4575
	.p2align 4,,10
	.p2align 3
.L4667:
	movq	(%rdx), %rdi
	cmpq	%rdi, (%rcx)
	je	.L4580
.L4579:
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L4575
.L4581:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L4579
	cmpq	%rdx, %rcx
	jne	.L4667
.L4580:
	movq	-136(%rbp), %rax
	movq	(%r12), %rdx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal2IC31IsTransitionOfMonomorphicTargetENS0_3MapES2_
	movl	%eax, %r9d
	xorl	%eax, %eax
	testb	%r9b, %r9b
	je	.L4606
.L4605:
	movq	-112(%rbp), %rdi
	movq	-104(%rbp), %r9
	movq	%r12, -80(%rbp)
	cmpq	%r9, %rdi
	je	.L4584
	testq	%r12, %r12
	je	.L4584
	movq	%rdi, %rdx
	jmp	.L4587
	.p2align 4,,10
	.p2align 3
.L4668:
	movq	(%rcx), %rcx
	cmpq	%rcx, (%r12)
	je	.L4586
.L4585:
	addq	$8, %rdx
	cmpq	%rdx, %r9
	je	.L4584
.L4587:
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L4585
	cmpq	%rcx, %r12
	jne	.L4668
.L4586:
	testb	%al, %al
	je	.L4606
.L4590:
	movq	%r9, %rsi
	subq	%rdi, %rsi
	sarq	$3, %rsi
	cmpl	%esi, _ZN2v88internal30FLAG_max_polymorphic_map_countE(%rip)
	jl	.L4566
	movl	-140(%rbp), %eax
	testl	%r15d, %r15d
	je	.L4591
	testl	%eax, %eax
	je	.L4592
	cmpl	%r15d, %eax
	jne	.L4669
.L4592:
	cmpq	%rdi, %r9
	je	.L4593
	movq	%rdi, %rdx
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L4595:
	movq	(%rdx), %rax
	movq	(%rax), %rax
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	subl	$17, %eax
	cmpb	$11, %al
	adcq	$0, %rcx
	addq	$8, %rdx
	cmpq	%rdx, %r9
	jne	.L4595
	testq	%rcx, %rcx
	je	.L4593
	cmpq	%rcx, %rsi
	jne	.L4670
.L4593:
	leaq	-80(%rbp), %r12
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %r13
	movq	$0, -64(%rbp)
	movq	%r12, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE7reserveEm
	movl	%r15d, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12KeyedStoreIC31StoreElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_20KeyedAccessStoreModeE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	cmpq	%rdx, %rax
	je	.L4671
	cmpq	$8, %rcx
	je	.L4672
	movq	%r12, %rcx
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal13FeedbackNexus20ConfigurePolymorphicENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE@PLT
	movq	80(%r14), %rax
	movb	$1, 16(%r14)
	testq	%rax, %rax
	je	.L4673
	movq	(%rax), %rsi
.L4600:
	movl	96(%r14), %edx
	movq	8(%r14), %rdi
	leaq	.LC39(%rip), %rcx
	call	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
.L4597:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4601
	call	_ZdlPv@PLT
.L4601:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4661
	jmp	.L4558
	.p2align 4,,10
	.p2align 3
.L4664:
	leaq	.LC73(%rip), %rax
	movq	%rax, 72(%r14)
.L4566:
	testq	%rdi, %rdi
	je	.L4558
.L4661:
	call	_ZdlPv@PLT
.L4558:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4674
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4666:
	.cfi_restore_state
	movq	(%rax), %rcx
	movq	(%r12), %rdx
	movq	%r14, %rdi
	movq	(%rcx), %rsi
	movq	%rcx, -152(%rbp)
	call	_ZN2v88internal2IC31IsTransitionOfMonomorphicTargetENS0_3MapES2_
	movq	-152(%rbp), %rcx
	testb	%al, %al
	jne	.L4660
	movq	-136(%rbp), %rax
	cmpq	%rcx, %rax
	je	.L4572
	testq	%rax, %rax
	je	.L4577
	movq	(%rax), %rax
	cmpq	(%rcx), %rax
	jne	.L4577
	cmpq	%r12, -136(%rbp)
	je	.L4578
.L4607:
	cmpq	%rax, (%r12)
	jne	.L4577
.L4578:
	testl	%r15d, %r15d
	jne	.L4577
	movl	-140(%rbp), %eax
	testl	%eax, %eax
	je	.L4577
	movq	-136(%rbp), %rbx
	movl	-140(%rbp), %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal12KeyedStoreIC19StoreElementHandlerENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeE
	movq	%r14, %rdi
	leaq	-80(%rbp), %rcx
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movl	$1, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE
	movq	-112(%rbp), %rdi
	jmp	.L4566
	.p2align 4,,10
	.p2align 3
.L4577:
	movq	-136(%rbp), %rcx
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rsi
	movq	%rcx, -80(%rbp)
	cmpq	%rsi, %rax
	jne	.L4574
	.p2align 4,,10
	.p2align 3
.L4575:
	cmpq	-96(%rbp), %rsi
	je	.L4582
	movq	-80(%rbp), %rax
	movq	%rax, (%rsi)
	addq	$8, -104(%rbp)
.L4583:
	movq	-136(%rbp), %rax
	movq	(%r12), %rdx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal2IC31IsTransitionOfMonomorphicTargetENS0_3MapES2_
	testb	%al, %al
	jne	.L4605
	movq	-112(%rbp), %rdi
	movq	-104(%rbp), %r9
	jmp	.L4590
	.p2align 4,,10
	.p2align 3
.L4606:
	leaq	.LC74(%rip), %rax
	movq	-112(%rbp), %rdi
	movq	%rax, 72(%r14)
	jmp	.L4566
.L4663:
	movq	-136(%rbp), %rbx
	movq	(%r12), %rdx
	movq	%r14, %rdi
	movq	(%rbx), %rsi
	call	_ZN2v88internal2IC31IsTransitionOfMonomorphicTargetENS0_3MapES2_
	testb	%al, %al
	cmove	%rbx, %r12
.L4660:
	movl	-140(%rbp), %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal12KeyedStoreIC19StoreElementHandlerENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeE
	movq	%r14, %rdi
	leaq	-80(%rbp), %rcx
	movq	%r12, %rdx
	xorl	%esi, %esi
	movl	$1, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE
	movq	-112(%rbp), %rdi
	jmp	.L4566
	.p2align 4,,10
	.p2align 3
.L4591:
	testl	%eax, %eax
	je	.L4593
	movl	%eax, %r15d
	jmp	.L4592
.L4670:
	leaq	.LC76(%rip), %rax
	movq	%rax, 72(%r14)
	jmp	.L4566
.L4669:
	leaq	.LC75(%rip), %rax
	movq	%rax, 72(%r14)
	jmp	.L4566
.L4584:
	cmpq	-96(%rbp), %r9
	je	.L4588
	movq	-80(%rbp), %rax
	movq	%rax, (%r9)
	movq	-104(%rbp), %rax
	leaq	8(%rax), %r9
	movq	%r9, -104(%rbp)
.L4589:
	movq	-112(%rbp), %rdi
	jmp	.L4590
.L4672:
	movq	-80(%rbp), %rcx
	movq	(%rax), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE
	jmp	.L4597
.L4671:
	movq	-136(%rbp), %rbx
	movl	%r15d, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal12KeyedStoreIC19StoreElementHandlerENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeE
	leaq	-128(%rbp), %rcx
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	$1, -128(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE
	jmp	.L4597
.L4572:
	cmpq	%r12, %rax
	je	.L4578
	movq	(%rax), %rax
	jmp	.L4607
.L4673:
	movq	88(%r14), %rsi
	jmp	.L4600
.L4582:
	leaq	-80(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	jmp	.L4583
.L4588:
	movq	%r9, %rsi
	leaq	-80(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-104(%rbp), %r9
	jmp	.L4589
.L4674:
	call	__stack_chk_fail@PLT
.L4665:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC40(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24303:
	.size	_ZN2v88internal12KeyedStoreIC18UpdateStoreElementENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeES4_, .-_ZN2v88internal12KeyedStoreIC18UpdateStoreElementENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeES4_
	.section	.rodata._ZN2v88internal12KeyedStoreIC5StoreENS0_6HandleINS0_6ObjectEEES4_S4_.str1.8,"aMS",@progbits,1
	.align 8
.LC77:
	.string	"unhandled internalized string key"
	.section	.rodata._ZN2v88internal12KeyedStoreIC5StoreENS0_6HandleINS0_6ObjectEEES4_S4_.str1.1,"aMS",@progbits,1
.LC78:
	.string	"map in array prototype"
.LC79:
	.string	"arguments receiver"
.LC80:
	.string	"array has read only length"
	.section	.rodata._ZN2v88internal12KeyedStoreIC5StoreENS0_6HandleINS0_6ObjectEEES4_S4_.str1.8
	.align 8
.LC81:
	.string	"typed array in the prototype chain of an Array"
	.section	.rodata._ZN2v88internal12KeyedStoreIC5StoreENS0_6HandleINS0_6ObjectEEES4_S4_.str1.1
.LC82:
	.string	"receiver with prototype map"
.LC83:
	.string	"dictionary or proxy prototype"
.LC84:
	.string	"non-smi-like key"
.LC85:
	.string	"non-JSObject receiver"
	.section	.text._ZN2v88internal12KeyedStoreIC5StoreENS0_6HandleINS0_6ObjectEEES4_S4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12KeyedStoreIC5StoreENS0_6HandleINS0_6ObjectEEES4_S4_
	.type	_ZN2v88internal12KeyedStoreIC5StoreENS0_6HandleINS0_6ObjectEEES4_S4_, @function
_ZN2v88internal12KeyedStoreIC5StoreENS0_6HandleINS0_6ObjectEEES4_S4_:
.LFB24314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L4832
.L4677:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL13TryConvertKeyENS0_6HandleINS0_6ObjectEEEPNS0_7IsolateE
	movq	%rax, %r13
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L4682
.L4686:
	movq	8(%r12), %rdx
	movl	$1, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal8JSObject18MakePrototypesFastENS0_6HandleINS0_6ObjectEEENS0_12WhereToStartEPNS0_7IsolateE@PLT
	movl	24(%r12), %eax
	testl	%eax, %eax
	je	.L4711
	cmpb	$0, _ZN2v88internal11FLAG_use_icE(%rip)
	je	.L4711
	movq	(%r14), %rdx
	testb	$1, %dl
	jne	.L4833
.L4712:
	movq	8(%r12), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L4769
.L4725:
	leaq	.LC85(%rip), %rax
	movq	%rax, 72(%r12)
	.p2align 4,,10
	.p2align 3
.L4727:
	movl	24(%r12), %eax
	testl	%eax, %eax
	je	.L4758
	cmpb	$0, 16(%r12)
	jne	.L4758
	cmpl	$6, %eax
	je	.L4834
.L4760:
	movq	0(%r13), %rax
	leaq	80(%r12), %rdi
	xorl	%esi, %esi
	testb	$1, %al
	jne	.L4835
.L4763:
	call	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicENS0_11IcCheckTypeE@PLT
	movq	80(%r12), %rax
	movb	$1, 16(%r12)
	testq	%rax, %rax
	je	.L4836
	movq	(%rax), %rsi
.L4766:
	movl	96(%r12), %edx
	movq	8(%r12), %rdi
	leaq	.LC35(%rip), %rcx
	call	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
.L4758:
	movl	_ZN2v88internal12TracingFlags8ic_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4837
.L4767:
	movq	%rbx, %rax
.L4681:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L4838
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4833:
	.cfi_restore_state
	movq	-1(%rdx), %rax
	cmpw	$1041, 11(%rax)
	je	.L4701
.L4703:
	testb	$1, %dl
	je	.L4712
	movq	-1(%rdx), %rax
	cmpw	$1026, 11(%rax)
	je	.L4839
	movq	-1(%rdx), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L4707:
	testb	%al, %al
	jne	.L4711
	movq	(%r14), %rax
	testb	$1, %al
	je	.L4712
	movq	-1(%rax), %rdx
	cmpw	$1026, 11(%rdx)
	je	.L4711
	movq	-1(%rax), %rax
	movq	8(%r12), %rsi
	movq	%rax, -96(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v88internal3Map26IsMapInArrayPrototypeChainEPNS0_7IsolateE@PLT
	testb	%al, %al
	je	.L4713
	leaq	.LC78(%rip), %rax
	movq	%rax, 72(%r12)
	.p2align 4,,10
	.p2align 3
.L4711:
	movq	8(%r12), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L4727
.L4769:
	xorl	%eax, %eax
	jmp	.L4681
	.p2align 4,,10
	.p2align 3
.L4832:
	movq	-1(%rax), %rsi
	cmpw	$1024, 11(%rsi)
	jbe	.L4677
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	je	.L4677
	movq	%r14, %rsi
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	8(%r12), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	jmp	.L4681
	.p2align 4,,10
	.p2align 3
.L4682:
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$65504, %edx
	jne	.L4690
	movq	%rax, -96(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L4688
	testb	$2, %al
	jne	.L4689
.L4688:
	leaq	-100(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L4689
	movq	0(%r13), %rax
.L4690:
	testb	$1, %al
	je	.L4686
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jne	.L4686
.L4689:
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal7StoreIC5StoreENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEES4_NS0_11StoreOriginE
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L4769
	movl	24(%r12), %eax
	testl	%eax, %eax
	je	.L4767
	cmpb	$0, 16(%r12)
	jne	.L4767
	cmpl	$6, %eax
	je	.L4840
.L4695:
	movq	%r13, %rdx
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC20ConfigureVectorStateENS0_16InlineCacheStateENS0_6HandleINS0_6ObjectEEE
	testb	%al, %al
	je	.L4767
	leaq	.LC77(%rip), %rax
	movq	%r13, %rdx
	leaq	.LC69(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, 72(%r12)
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE
	jmp	.L4767
	.p2align 4,,10
	.p2align 3
.L4835:
	movq	-1(%rax), %rax
	xorl	%esi, %esi
	cmpw	$64, 11(%rax)
	setbe	%sil
	jmp	.L4763
	.p2align 4,,10
	.p2align 3
.L4713:
	movq	(%r14), %rax
	movq	%rax, %r10
	notq	%r10
	andl	$1, %r10d
	jne	.L4712
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L4712
	movq	8(%r12), %rbx
	movq	-1(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4715
	movb	%r10b, -124(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-124(%rbp), %r10d
	movq	%rax, -120(%rbp)
.L4716:
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %r11d
	movq	-1(%rax), %rdx
	movq	0(%r13), %rsi
	movzwl	11(%rdx), %edi
	movq	%rsi, %rdx
	notq	%rdx
	andl	$1, %edx
	je	.L4831
	sarq	$32, %rsi
	js	.L4719
.L4723:
	cmpw	$1058, %r11w
	je	.L4722
	cmpw	$1024, %di
	je	.L4722
	movq	%r14, %rdi
	movb	%dl, -136(%rbp)
	movl	%r11d, -124(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_112GetStoreModeENS0_6HandleINS0_8JSObjectEEEj
	movzbl	-136(%rbp), %edx
	movl	-124(%rbp), %r11d
	movl	%eax, -128(%rbp)
	movl	%edx, %r10d
	.p2align 4,,10
	.p2align 3
.L4724:
	movq	8(%r12), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movl	%r11d, -136(%rbp)
	movb	%r10b, -124(%rbp)
	call	_ZN2v88internal7Runtime17SetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_NS0_11StoreOriginENS_5MaybeINS0_11ShouldThrowEEE@PLT
	movzbl	-124(%rbp), %r10d
	movl	-136(%rbp), %r11d
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L4769
	cmpq	$0, -120(%rbp)
	je	.L4725
	cmpw	$1058, %r11w
	jne	.L4726
	leaq	.LC79(%rip), %rax
	movq	%rax, 72(%r12)
	jmp	.L4727
	.p2align 4,,10
	.p2align 3
.L4726:
	movq	(%r14), %rdx
	testb	$1, %dl
	jne	.L4841
.L4771:
	testb	%r10b, %r10b
	jne	.L4842
	leaq	.LC84(%rip), %rax
	movq	%rax, 72(%r12)
	jmp	.L4727
	.p2align 4,,10
	.p2align 3
.L4837:
	movl	24(%r12), %r8d
	xorl	%ecx, %ecx
	testl	%r8d, %r8d
	jne	.L4843
.L4768:
	movq	%r13, %rdx
	leaq	.LC69(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_
	jmp	.L4767
	.p2align 4,,10
	.p2align 3
.L4836:
	movq	88(%r12), %rsi
	jmp	.L4766
.L4722:
	movl	%edx, %r10d
	.p2align 4,,10
	.p2align 3
.L4831:
	movl	$0, -128(%rbp)
	jmp	.L4724
	.p2align 4,,10
	.p2align 3
.L4834:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L4844
	movq	(%rax), %rdx
.L4762:
	movl	96(%r12), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	shrq	$32, %rax
	je	.L4758
	jmp	.L4760
	.p2align 4,,10
	.p2align 3
.L4841:
	movq	-1(%rdx), %rax
	cmpw	$1061, 11(%rax)
	jne	.L4776
	cmpl	$1, -128(%rbp)
	je	.L4729
.L4776:
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
.L4731:
	testb	%al, %al
	jne	.L4771
	movq	-1(%rdx), %rax
	cmpw	$1061, 11(%rax)
	jne	.L4771
	movq	%rdx, %rax
	movq	-144(%rbp), %rdi
	movb	%r10b, -124(%rbp)
	andq	$-262144, %rax
	movq	24(%rax), %rax
	movq	%rdx, -88(%rbp)
	movq	$0, -80(%rbp)
	subq	$37592, %rax
	movl	$0, -72(%rbp)
	movq	%rax, -96(%rbp)
	movb	$0, -68(%rbp)
	movl	$0, -64(%rbp)
	call	_ZN2v88internal17PrototypeIterator7AdvanceEv
	cmpb	$0, -68(%rbp)
	movzbl	-124(%rbp), %r10d
	jne	.L4771
	.p2align 4,,10
	.p2align 3
.L4736:
	movq	-88(%rbp), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L4737
	movq	-1(%rax), %rdx
	cmpw	$1086, 11(%rdx)
	je	.L4737
	movq	-80(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L4845
	movq	(%rdx), %rax
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L4771
	leaq	-1(%rax), %rdx
.L4743:
	movq	(%rdx), %rax
	movq	-96(%rbp), %r15
	movq	23(%rax), %rsi
	cmpq	104(%r15), %rsi
	je	.L4846
	cmpl	$1, -72(%rbp)
	je	.L4746
	cmpq	$0, -80(%rbp)
	movb	$0, -68(%rbp)
	je	.L4847
.L4745:
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L4749
	movb	%r10b, -124(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movzbl	-124(%rbp), %r10d
.L4750:
	movq	%rax, -80(%rbp)
	movzbl	-68(%rbp), %eax
.L4748:
	testb	%al, %al
	je	.L4736
	jmp	.L4771
	.p2align 4,,10
	.p2align 3
.L4842:
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movl	15(%rax), %edx
	andl	$1048576, %edx
	jne	.L4848
.L4753:
	movq	8(%r12), %rsi
	movq	-144(%rbp), %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal3Map38DictionaryElementsInPrototypeChainOnlyEPNS0_7IsolateE@PLT
	testb	%al, %al
	je	.L4849
	leaq	.LC83(%rip), %rax
	movq	%rax, 72(%r12)
	jmp	.L4727
	.p2align 4,,10
	.p2align 3
.L4845:
	leaq	-1(%rax), %rdx
	testb	$1, %al
	je	.L4743
	movq	-1(%rax), %rax
	cmpw	$1024, 11(%rax)
	jne	.L4743
	jmp	.L4771
	.p2align 4,,10
	.p2align 3
.L4846:
	cmpq	$0, -80(%rbp)
	movb	$1, -68(%rbp)
	jne	.L4745
	jmp	.L4771
	.p2align 4,,10
	.p2align 3
.L4715:
	movq	41088(%rbx), %rax
	movq	%rax, -120(%rbp)
	cmpq	41096(%rbx), %rax
	je	.L4850
.L4717:
	movq	-120(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L4716
	.p2align 4,,10
	.p2align 3
.L4749:
	movq	41088(%r15), %rax
	cmpq	41096(%r15), %rax
	je	.L4851
.L4751:
	leaq	8(%rax), %rcx
	movq	%rcx, 41088(%r15)
	movq	%rsi, (%rax)
	jmp	.L4750
	.p2align 4,,10
	.p2align 3
.L4839:
	movq	%rdx, %rbx
	leaq	-96(%rbp), %rdi
	movq	%rdx, -120(%rbp)
	andq	$-262144, %rbx
	movq	24(%rbx), %rax
	movq	-25128(%rax), %rax
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	-120(%rbp), %rdx
	movq	24(%rbx), %rdi
	movq	-1(%rdx), %rsi
	cmpw	$1024, 11(%rsi)
	je	.L4852
	movq	-1(%rdx), %rdx
	movq	23(%rdx), %rdx
.L4706:
	cmpq	%rax, %rdx
	setne	%al
	jmp	.L4707
	.p2align 4,,10
	.p2align 3
.L4843:
	leaq	80(%r12), %rdi
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movl	24(%r12), %ecx
	movl	%eax, %r8d
	jmp	.L4768
	.p2align 4,,10
	.p2align 3
.L4746:
	cmpw	$1026, 11(%rax)
	setne	%al
	cmpq	$0, -80(%rbp)
	movb	%al, -68(%rbp)
	jne	.L4745
	movq	%rsi, -88(%rbp)
	jmp	.L4748
.L4701:
	movq	23(%rdx), %rax
	testb	$1, %al
	je	.L4703
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L4711
	jmp	.L4703
.L4844:
	movq	88(%r12), %rdx
	jmp	.L4762
.L4719:
	testb	$1, %al
	je	.L4831
	movq	-1(%rax), %rax
	cmpw	$1086, 11(%rax)
	jne	.L4831
	jmp	.L4723
.L4848:
	movl	15(%rax), %edx
	andl	$4194304, %edx
	jne	.L4753
	leaq	.LC82(%rip), %rax
	movq	%rax, 72(%r12)
	jmp	.L4727
.L4847:
	movq	%rsi, -88(%rbp)
	jmp	.L4736
.L4840:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L4853
	movq	(%rax), %rdx
.L4697:
	movl	96(%r12), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	shrq	$32, %rax
	je	.L4767
	jmp	.L4695
.L4737:
	leaq	.LC81(%rip), %rax
	movq	%rax, 72(%r12)
	jmp	.L4727
.L4850:
	movq	%rbx, %rdi
	movq	%rsi, -136(%rbp)
	movb	%r10b, -124(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movzbl	-124(%rbp), %r10d
	movq	%rax, -120(%rbp)
	jmp	.L4717
.L4851:
	movq	%r15, %rdi
	movq	%rsi, -136(%rbp)
	movb	%r10b, -124(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movzbl	-124(%rbp), %r10d
	jmp	.L4751
.L4849:
	movq	8(%r12), %r15
	movq	(%r14), %rax
	movq	-1(%rax), %r14
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L4755
	movq	%r14, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L4756:
	movl	-128(%rbp), %edx
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12KeyedStoreIC18UpdateStoreElementENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeES4_
	jmp	.L4727
.L4853:
	movq	88(%r12), %rdx
	jmp	.L4697
.L4729:
	movq	%r14, %rdi
	movb	%r10b, -124(%rbp)
	call	_ZN2v88internal7JSArray17HasReadOnlyLengthENS0_6HandleIS1_EE@PLT
	testb	%al, %al
	jne	.L4732
	movq	(%r14), %rdx
	movzbl	-124(%rbp), %r10d
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jmp	.L4731
.L4755:
	movq	41088(%r15), %rcx
	cmpq	41096(%r15), %rcx
	je	.L4854
.L4757:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r15)
	movq	%r14, (%rcx)
	jmp	.L4756
.L4732:
	leaq	.LC80(%rip), %rax
	movq	%rax, 72(%r12)
	jmp	.L4727
.L4852:
	movq	-37488(%rdi), %rdx
	jmp	.L4706
.L4838:
	call	__stack_chk_fail@PLT
.L4854:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rcx
	jmp	.L4757
	.cfi_endproc
.LFE24314:
	.size	_ZN2v88internal12KeyedStoreIC5StoreENS0_6HandleINS0_6ObjectEEES4_S4_, .-_ZN2v88internal12KeyedStoreIC5StoreENS0_6HandleINS0_6ObjectEEES4_S4_
	.section	.rodata._ZN2v88internal21StoreInArrayLiteralIC5StoreENS0_6HandleINS0_7JSArrayEEENS2_INS0_6ObjectEEES6_.str1.1,"aMS",@progbits,1
.LC86:
	.string	"StoreInArrayLiteralIC"
.LC87:
	.string	"index out of Smi range"
	.section	.text._ZN2v88internal21StoreInArrayLiteralIC5StoreENS0_6HandleINS0_7JSArrayEEENS2_INS0_6ObjectEEES6_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal21StoreInArrayLiteralIC5StoreENS0_6HandleINS0_7JSArrayEEENS2_INS0_6ObjectEEES6_
	.type	_ZN2v88internal21StoreInArrayLiteralIC5StoreENS0_6HandleINS0_7JSArrayEEENS2_INS0_6ObjectEEES6_, @function
_ZN2v88internal21StoreInArrayLiteralIC5StoreENS0_6HandleINS0_7JSArrayEEENS2_INS0_6ObjectEEES6_:
.LFB24316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$232, %rsp
	movq	8(%rdi), %r10
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal11FLAG_use_icE(%rip)
	je	.L4856
	movl	24(%rdi), %eax
	testl	%eax, %eax
	jne	.L4916
.L4856:
	leaq	-224(%rbp), %r15
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r10, %rsi
	leaq	-226(%rbp), %r8
	movq	%r15, %rdi
	movl	$1, %r9d
	movb	$0, -226(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rbx, %rsi
	movl	$1, %ecx
	movq	%r15, %rdi
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE@PLT
	testb	%al, %al
	je	.L4917
.L4861:
	shrw	$8, %ax
	je	.L4871
.L4877:
	movl	_ZN2v88internal12TracingFlags8ic_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4918
.L4855:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4919
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4916:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%r10, %rdx
	testb	$1, %al
	jne	.L4920
.L4858:
	movl	$0, -252(%rbp)
	movq	(%r14), %rsi
	testb	$1, %sil
	je	.L4921
.L4866:
	movq	-1(%rax), %rsi
	movq	41112(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L4867
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L4868:
	movq	8(%r12), %rsi
	leaq	-144(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	leaq	-225(%rbp), %r8
	movl	$1, %r9d
	movq	%rdi, -248(%rbp)
	movb	$0, -225(%rbp)
	call	_ZN2v88internal14LookupIterator17PropertyOrElementEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_PbNS1_13ConfigurationE@PLT
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rbx, %rsi
	movq	-248(%rbp), %rdi
	movl	$1, %ecx
	call	_ZN2v88internal8JSObject33DefineOwnPropertyIgnoreAttributesEPNS0_14LookupIteratorENS0_6HandleINS0_6ObjectEEENS0_18PropertyAttributesENS_5MaybeINS0_11ShouldThrowEEENS1_20AccessorInfoHandlingE@PLT
	testb	%al, %al
	je	.L4922
.L4870:
	shrw	$8, %ax
	je	.L4871
	testb	$1, (%r14)
	je	.L4923
	leaq	.LC87(%rip), %rax
	movq	%rax, 72(%r12)
.L4876:
	movl	24(%r12), %eax
	testl	%eax, %eax
	je	.L4877
	cmpb	$0, 16(%r12)
	jne	.L4877
	cmpl	$6, %eax
	je	.L4924
.L4879:
	movq	(%r14), %rax
	leaq	80(%r12), %rdi
	xorl	%esi, %esi
	testb	$1, %al
	jne	.L4925
.L4882:
	call	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicENS0_11IcCheckTypeE@PLT
	movq	80(%r12), %rax
	movb	$1, 16(%r12)
	testq	%rax, %rax
	je	.L4926
	movq	(%rax), %rsi
.L4885:
	movl	96(%r12), %edx
	movq	8(%r12), %rdi
	leaq	.LC35(%rip), %rcx
	call	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	jmp	.L4877
	.p2align 4,,10
	.p2align 3
.L4867:
	movq	41088(%rdx), %r15
	cmpq	%r15, 41096(%rdx)
	je	.L4927
.L4869:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rdx)
	movq	%rsi, (%r15)
	jmp	.L4868
	.p2align 4,,10
	.p2align 3
.L4921:
	sarq	$32, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_112GetStoreModeENS0_6HandleINS0_8JSObjectEEEj
	movq	8(%r12), %rdx
	movl	%eax, -252(%rbp)
	movq	0(%r13), %rax
	jmp	.L4866
	.p2align 4,,10
	.p2align 3
.L4920:
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L4858
	movq	-1(%rax), %rcx
	movl	15(%rcx), %ecx
	andl	$16777216, %ecx
	je	.L4858
	movq	%r10, %rdi
	call	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	8(%r12), %r10
	jmp	.L4856
	.p2align 4,,10
	.p2align 3
.L4871:
	leaq	.LC10(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L4918:
	movl	24(%r12), %r8d
	xorl	%ecx, %ecx
	testl	%r8d, %r8d
	jne	.L4928
.L4887:
	movq	%r14, %rdx
	leaq	.LC86(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_
	jmp	.L4855
	.p2align 4,,10
	.p2align 3
.L4923:
	movq	8(%r12), %rbx
	movq	0(%r13), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L4873
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rcx
.L4874:
	movl	-252(%rbp), %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12KeyedStoreIC18UpdateStoreElementENS0_6HandleINS0_3MapEEENS0_20KeyedAccessStoreModeES4_
	jmp	.L4876
	.p2align 4,,10
	.p2align 3
.L4917:
	movl	%eax, -248(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-248(%rbp), %eax
	jmp	.L4861
	.p2align 4,,10
	.p2align 3
.L4873:
	movq	41088(%rbx), %rcx
	cmpq	41096(%rbx), %rcx
	je	.L4929
.L4875:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%rcx)
	jmp	.L4874
	.p2align 4,,10
	.p2align 3
.L4928:
	leaq	80(%r12), %rdi
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movl	24(%r12), %ecx
	movl	%eax, %r8d
	jmp	.L4887
	.p2align 4,,10
	.p2align 3
.L4922:
	movl	%eax, -248(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-248(%rbp), %eax
	jmp	.L4870
	.p2align 4,,10
	.p2align 3
.L4925:
	movq	-1(%rax), %rax
	xorl	%esi, %esi
	cmpw	$64, 11(%rax)
	setbe	%sil
	jmp	.L4882
	.p2align 4,,10
	.p2align 3
.L4926:
	movq	88(%r12), %rsi
	jmp	.L4885
	.p2align 4,,10
	.p2align 3
.L4927:
	movq	%rdx, %rdi
	movq	%rsi, -264(%rbp)
	movq	%rdx, -248(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-264(%rbp), %rsi
	movq	-248(%rbp), %rdx
	movq	%rax, %r15
	jmp	.L4869
	.p2align 4,,10
	.p2align 3
.L4924:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L4930
	movq	(%rax), %rdx
.L4881:
	movl	96(%r12), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	shrq	$32, %rax
	je	.L4877
	jmp	.L4879
.L4929:
	movq	%rbx, %rdi
	movq	%rsi, -248(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-248(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L4875
.L4930:
	movq	88(%r12), %rdx
	jmp	.L4881
.L4919:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24316:
	.size	_ZN2v88internal21StoreInArrayLiteralIC5StoreENS0_6HandleINS0_7JSArrayEEENS2_INS0_6ObjectEEES6_, .-_ZN2v88internal21StoreInArrayLiteralIC5StoreENS0_6HandleINS0_7JSArrayEEENS2_INS0_6ObjectEEES6_
	.section	.rodata._ZN2v88internalL31Stats_Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC88:
	.string	"V8.Runtime_Runtime_KeyedStoreIC_Miss"
	.section	.text._ZN2v88internalL31Stats_Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL31Stats_Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL31Stats_Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateE.isra.0:
.LFB30913:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -240(%rbp)
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L4980
.L4932:
	movq	_ZZN2v88internalL31Stats_Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2409(%rip), %rbx
	testq	%rbx, %rbx
	je	.L4981
.L4934:
	movq	$0, -272(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L4982
.L4936:
	movq	41088(%r12), %rax
	leaq	-16(%r14), %rcx
	leaq	-24(%r14), %r13
	addl	$1, 41104(%r12)
	leaq	-32(%r14), %rbx
	movslq	-4(%r14), %r9
	movq	%rax, -304(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -296(%rbp)
	movq	-16(%r14), %rax
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-37504(%rsi), %rax
	jne	.L4983
	xorl	%ecx, %ecx
	movl	$13, %eax
	leaq	-192(%rbp), %r15
.L4940:
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rdx
	movq	%r12, -184(%rbp)
	movq	%rdx, -192(%rbp)
	movb	$0, -176(%rbp)
	movl	%eax, -164(%rbp)
	movb	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%rcx, -112(%rbp)
	movq	$0, -104(%rbp)
	movl	%r9d, -96(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	testq	%rcx, %rcx
	je	.L4943
	movq	(%rcx), %rax
	leaq	-280(%rbp), %rdi
	movl	%r9d, %esi
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-112(%rbp), %rdi
	movl	%eax, -92(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
.L4952:
	movl	%eax, -168(%rbp)
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%eax, -172(%rbp)
	leaq	16+_ZTVN2v88internal12KeyedStoreICE(%rip), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12KeyedStoreIC5StoreENS0_6HandleINS0_6ObjectEEES4_S4_
	testq	%rax, %rax
	je	.L4984
	movq	(%rax), %r13
.L4945:
	movq	-152(%rbp), %rdi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rax, -192(%rbp)
	testq	%rdi, %rdi
	je	.L4947
.L4979:
	call	_ZdlPv@PLT
.L4947:
	subl	$1, 41104(%r12)
	movq	-304(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-296(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L4951
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4951:
	leaq	-272(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4985
.L4931:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4986
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4983:
	.cfi_restore_state
	leaq	-192(%rbp), %r15
	movl	%r9d, %esi
	movq	%rcx, -320(%rbp)
	movq	%r15, %rdi
	movq	%r9, -312(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	-312(%rbp), %r9
	movq	-320(%rbp), %rcx
	cmpl	$3, %eax
	je	.L4940
	cmpl	$13, %eax
	je	.L4940
	movq	-16(%r14), %rax
	pxor	%xmm0, %xmm0
	movl	%r9d, %esi
	movq	%rcx, -112(%rbp)
	leaq	16+_ZTVN2v88internal2ICE(%rip), %r8
	leaq	-280(%rbp), %rdi
	movl	%r9d, -96(%rbp)
	movq	%r8, -192(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%r12, -184(%rbp)
	movb	$0, -176(%rbp)
	movl	$14, -164(%rbp)
	movb	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-112(%rbp), %rdi
	movl	%eax, -92(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movl	%eax, -168(%rbp)
	movl	%eax, -172(%rbp)
	leaq	16+_ZTVN2v88internal21StoreInArrayLiteralICE(%rip), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	_ZN2v88internal21StoreInArrayLiteralIC5StoreENS0_6HandleINS0_7JSArrayEEENS2_INS0_6ObjectEEES6_
	movq	-152(%rbp), %rdi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %r8
	movq	(%r14), %r13
	movq	%r8, -192(%rbp)
	testq	%rdi, %rdi
	jne	.L4979
	jmp	.L4947
	.p2align 4,,10
	.p2align 3
.L4943:
	movl	$0, -92(%rbp)
	xorl	%eax, %eax
	jmp	.L4952
	.p2align 4,,10
	.p2align 3
.L4982:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4987
.L4937:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4938
	movq	(%rdi), %rax
	call	*8(%rax)
.L4938:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4939
	movq	(%rdi), %rax
	call	*8(%rax)
.L4939:
	leaq	.LC88(%rip), %rax
	movq	%rbx, -264(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-264(%rbp), %rax
	movq	%r13, -248(%rbp)
	movq	%rax, -272(%rbp)
	jmp	.L4936
	.p2align 4,,10
	.p2align 3
.L4981:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4988
.L4935:
	movq	%rbx, _ZZN2v88internalL31Stats_Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2409(%rip)
	jmp	.L4934
	.p2align 4,,10
	.p2align 3
.L4980:
	movq	40960(%rsi), %rax
	movl	$319, %edx
	leaq	-232(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L4932
	.p2align 4,,10
	.p2align 3
.L4985:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L4931
	.p2align 4,,10
	.p2align 3
.L4984:
	movq	312(%r12), %r13
	jmp	.L4945
	.p2align 4,,10
	.p2align 3
.L4987:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC88(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L4937
	.p2align 4,,10
	.p2align 3
.L4988:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L4935
.L4986:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30913:
	.size	_ZN2v88internalL31Stats_Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL31Stats_Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal25Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateE:
.LFB24342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r11d
	testl	%r11d, %r11d
	jne	.L5014
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	leaq	-16(%rsi), %r9
	leaq	-24(%rsi), %r13
	movslq	-4(%rsi), %r10
	leaq	-32(%rsi), %rbx
	movq	%rax, -192(%rbp)
	movq	41096(%rdx), %rax
	movq	%rax, -184(%rbp)
	movq	-16(%rsi), %rax
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-37504(%rsi), %rax
	jne	.L5015
	xorl	%r9d, %r9d
	movl	$13, %eax
	leaq	-160(%rbp), %r15
.L4992:
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rcx
	movq	%r12, -152(%rbp)
	movq	%rcx, -160(%rbp)
	movb	$0, -144(%rbp)
	movl	%eax, -132(%rbp)
	movb	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	%r10d, -64(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	testq	%r9, %r9
	je	.L4995
	movq	(%r9), %rax
	leaq	-168(%rbp), %rdi
	movl	%r10d, %esi
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, -60(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movl	%eax, %r11d
.L5003:
	leaq	16+_ZTVN2v88internal12KeyedStoreICE(%rip), %rax
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%r11d, -136(%rbp)
	movl	%r11d, -140(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12KeyedStoreIC5StoreENS0_6HandleINS0_6ObjectEEES4_S4_
	testq	%rax, %rax
	je	.L5016
	movq	(%rax), %r13
.L4997:
	movq	-120(%rbp), %rdi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rax
	movq	%rax, -160(%rbp)
	testq	%rdi, %rdi
	je	.L4999
.L5013:
	call	_ZdlPv@PLT
.L4999:
	subl	$1, 41104(%r12)
	movq	-192(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-184(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L4989
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L4989:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5017
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4995:
	.cfi_restore_state
	movl	$0, -60(%rbp)
	jmp	.L5003
	.p2align 4,,10
	.p2align 3
.L5015:
	leaq	-160(%rbp), %r15
	movl	%r10d, %esi
	movl	%r11d, -212(%rbp)
	movq	%r15, %rdi
	movq	%r9, -208(%rbp)
	movq	%r10, -200(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	-200(%rbp), %r10
	movq	-208(%rbp), %r9
	cmpl	$3, %eax
	movl	-212(%rbp), %r11d
	je	.L4992
	cmpl	$13, %eax
	je	.L4992
	movq	-16(%r14), %rax
	pxor	%xmm0, %xmm0
	movl	%r10d, %esi
	movq	%r9, -80(%rbp)
	leaq	16+_ZTVN2v88internal2ICE(%rip), %r8
	leaq	-168(%rbp), %rdi
	movl	%r10d, -64(%rbp)
	movq	%r8, -160(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%r12, -152(%rbp)
	movb	$0, -144(%rbp)
	movl	$14, -132(%rbp)
	movb	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, -60(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	movl	%eax, -136(%rbp)
	movl	%eax, -140(%rbp)
	leaq	16+_ZTVN2v88internal21StoreInArrayLiteralICE(%rip), %rax
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	_ZN2v88internal21StoreInArrayLiteralIC5StoreENS0_6HandleINS0_7JSArrayEEENS2_INS0_6ObjectEEES6_
	movq	-120(%rbp), %rdi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %r8
	movq	(%r14), %r13
	movq	%r8, -160(%rbp)
	testq	%rdi, %rdi
	jne	.L5013
	jmp	.L4999
	.p2align 4,,10
	.p2align 3
.L5014:
	movq	%rdx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internalL31Stats_Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L4989
	.p2align 4,,10
	.p2align 3
.L5016:
	movq	312(%r12), %r13
	jmp	.L4997
.L5017:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24342:
	.size	_ZN2v88internal25Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateE, .-_ZN2v88internal25Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC89:
	.string	"V8.Runtime_Runtime_StoreInArrayLiteralIC_Miss"
	.section	.text._ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateE.isra.0:
.LFB30914:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$264, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -240(%rbp)
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5057
.L5019:
	movq	_ZZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2451(%rip), %r13
	testq	%r13, %r13
	je	.L5058
.L5021:
	movq	$0, -272(%rbp)
	movzbl	0(%r13), %eax
	testb	$5, %al
	jne	.L5059
.L5023:
	leaq	-16(%rbx), %rax
	pxor	%xmm0, %xmm0
	addl	$1, 41104(%r12)
	movq	-16(%rbx), %rcx
	movq	41088(%r12), %r15
	leaq	16+_ZTVN2v88internal2ICE(%rip), %r13
	movq	41096(%r12), %r14
	leaq	-24(%rbx), %r8
	movq	%rcx, %rsi
	leaq	-32(%rbx), %rdx
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	movq	%r13, -192(%rbp)
	movq	%r12, -184(%rbp)
	cmpq	-37504(%rsi), %rcx
	movl	$0, %ecx
	movslq	-4(%rbx), %rsi
	movb	$0, -128(%rbp)
	cmove	%rcx, %rax
	movb	$0, -176(%rbp)
	movl	$14, -164(%rbp)
	movq	$0, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -104(%rbp)
	movl	%esi, -96(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	testq	%rax, %rax
	je	.L5028
	movq	(%rax), %rax
	leaq	-280(%rbp), %rdi
	movq	%rdx, -304(%rbp)
	movq	%r8, -296(%rbp)
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-112(%rbp), %rdi
	movl	%eax, -92(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	-304(%rbp), %rdx
	movq	-296(%rbp), %r8
.L5033:
	movl	%eax, -168(%rbp)
	leaq	-192(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r8, %rsi
	movl	%eax, -172(%rbp)
	leaq	16+_ZTVN2v88internal21StoreInArrayLiteralICE(%rip), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal21StoreInArrayLiteralIC5StoreENS0_6HandleINS0_7JSArrayEEENS2_INS0_6ObjectEEES6_
	movq	-152(%rbp), %rdi
	movq	%r13, -192(%rbp)
	movq	(%rbx), %rbx
	testq	%rdi, %rdi
	je	.L5029
	call	_ZdlPv@PLT
.L5029:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L5032
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5032:
	leaq	-272(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L5060
.L5018:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5061
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5028:
	.cfi_restore_state
	movl	$0, -92(%rbp)
	xorl	%eax, %eax
	jmp	.L5033
	.p2align 4,,10
	.p2align 3
.L5059:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5062
.L5024:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5025
	movq	(%rdi), %rax
	call	*8(%rax)
.L5025:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5026
	movq	(%rdi), %rax
	call	*8(%rax)
.L5026:
	leaq	.LC89(%rip), %rax
	movq	%r13, -264(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-264(%rbp), %rax
	movq	%r14, -248(%rbp)
	movq	%rax, -272(%rbp)
	jmp	.L5023
	.p2align 4,,10
	.p2align 3
.L5058:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5063
.L5022:
	movq	%r13, _ZZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2451(%rip)
	jmp	.L5021
	.p2align 4,,10
	.p2align 3
.L5057:
	movq	40960(%rsi), %rax
	movl	$320, %edx
	leaq	-232(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L5019
	.p2align 4,,10
	.p2align 3
.L5060:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L5018
	.p2align 4,,10
	.p2align 3
.L5063:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L5022
	.p2align 4,,10
	.p2align 3
.L5062:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC89(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L5024
.L5061:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30914:
	.size	_ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal34Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateE:
.LFB24345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5079
	addl	$1, 41104(%rdx)
	movq	-16(%rsi), %rcx
	leaq	-24(%rsi), %r8
	leaq	-32(%rsi), %r9
	movq	41088(%rdx), %r15
	movq	41096(%rdx), %r14
	leaq	-16(%rsi), %rdx
	pxor	%xmm0, %xmm0
	movq	%rcx, %rsi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	movq	%rbx, -160(%rbp)
	movq	%r12, -152(%rbp)
	cmpq	-37504(%rsi), %rcx
	movl	$0, %ecx
	movslq	-4(%r13), %rsi
	movb	$0, -96(%rbp)
	cmove	%rcx, %rdx
	movb	$0, -144(%rbp)
	movl	$14, -132(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	%esi, -64(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	testq	%rdx, %rdx
	je	.L5068
	movq	(%rdx), %rax
	leaq	-168(%rbp), %rdi
	movq	%r9, -192(%rbp)
	movq	%r8, -184(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, -60(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	-192(%rbp), %r9
	movq	-184(%rbp), %r8
.L5072:
	movl	%eax, -136(%rbp)
	movq	%r13, %rcx
	movq	%r9, %rdx
	movq	%r8, %rsi
	movl	%eax, -140(%rbp)
	leaq	-160(%rbp), %rdi
	leaq	16+_ZTVN2v88internal21StoreInArrayLiteralICE(%rip), %rax
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal21StoreInArrayLiteralIC5StoreENS0_6HandleINS0_7JSArrayEEENS2_INS0_6ObjectEEES6_
	movq	-120(%rbp), %rdi
	movq	%rbx, -160(%rbp)
	movq	0(%r13), %r13
	testq	%rdi, %rdi
	je	.L5069
	call	_ZdlPv@PLT
.L5069:
	subl	$1, 41104(%r12)
	movq	%r15, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L5064
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5064:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5080
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5068:
	.cfi_restore_state
	movl	$0, -60(%rbp)
	jmp	.L5072
	.p2align 4,,10
	.p2align 3
.L5079:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L5064
.L5080:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24345:
	.size	_ZN2v88internal34Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateE, .-_ZN2v88internal34Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal11KeyedLoadIC30LoadElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_19KeyedAccessLoadModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11KeyedLoadIC30LoadElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_19KeyedAccessLoadModeE
	.type	_ZN2v88internal11KeyedLoadIC30LoadElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_19KeyedAccessLoadModeE, @function
_ZN2v88internal11KeyedLoadIC30LoadElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_19KeyedAccessLoadModeE:
.LFB24289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	8(%rsi), %r15
	movq	(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rax
	sarq	$5, %rdx
	sarq	$3, %rax
	testq	%rdx, %rdx
	jle	.L5082
	salq	$5, %rdx
	addq	%rcx, %rdx
	jmp	.L5087
	.p2align 4,,10
	.p2align 3
.L5127:
	movq	8(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L5123
	movq	16(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L5124
	movq	24(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L5125
	addq	$32, %rcx
	cmpq	%rcx, %rdx
	je	.L5126
.L5087:
	movq	(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	je	.L5127
.L5083:
	cmpq	%r15, %rcx
	je	.L5092
	leaq	8(%rcx), %rax
	cmpq	%rax, %r15
	je	.L5095
	.p2align 4,,10
	.p2align 3
.L5097:
	movq	(%rax), %rdx
	movq	(%rdx), %rsi
	movl	15(%rsi), %esi
	andl	$16777216, %esi
	jne	.L5096
	movq	%rdx, (%rcx)
	addq	$8, %rcx
.L5096:
	addq	$8, %rax
	cmpq	%rax, %r15
	jne	.L5097
	movq	8(%r14), %rax
	cmpq	%rcx, %r15
	je	.L5109
.L5107:
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	%rax, %r15
	je	.L5098
	movq	%rcx, %rdi
	movq	%r15, %rsi
	call	memmove@PLT
	movq	%rax, %rcx
	movq	8(%r14), %rax
	movq	%rax, %rdx
	subq	%r15, %rdx
.L5098:
	leaq	(%rcx,%rdx), %rdi
	movq	%rdi, -88(%rbp)
	cmpq	%rax, %rdi
	je	.L5094
	movq	%rdi, 8(%r14)
.L5094:
	leaq	-80(%rbp), %rax
	movq	(%r14), %r15
	movq	%rax, -96(%rbp)
	cmpq	-88(%rbp), %r15
	je	.L5081
	.p2align 4,,10
	.p2align 3
.L5106:
	movq	(%r15), %r9
	movq	(%r9), %rax
	movl	15(%rax), %edx
	andl	$33554432, %edx
	je	.L5128
.L5100:
	movq	%r9, %rsi
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal11KeyedLoadIC18LoadElementHandlerENS0_6HandleINS0_3MapEEENS0_19KeyedAccessLoadModeE
	movl	$1, -80(%rbp)
	movq	8(%rbx), %rsi
	movq	%rax, -72(%rbp)
	cmpq	16(%rbx), %rsi
	je	.L5103
	movl	$1, (%rsi)
	addq	$8, %r15
	movq	%rax, 8(%rsi)
	addq	$16, 8(%rbx)
	cmpq	%r15, -88(%rbp)
	jne	.L5106
.L5081:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5129
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5128:
	.cfi_restore_state
	movq	8(%r12), %rsi
	movq	-96(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r9, -104(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal3Map31FindElementsKindTransitionedMapEPNS0_7IsolateERKSt6vectorINS0_6HandleIS1_EESaIS6_EE@PLT
	movq	-104(%rbp), %r9
	testq	%rax, %rax
	je	.L5100
	movq	(%r9), %rax
	movq	8(%r12), %rsi
	movl	15(%rax), %edx
	andl	$33554432, %edx
	jne	.L5100
	movl	15(%rax), %edx
	orl	$33554432, %edx
	movl	%edx, 15(%rax)
	movl	$1, %edx
	movq	55(%rax), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal13DependentCode28DeoptimizeDependentCodeGroupEPNS0_7IsolateENS1_15DependencyGroupE@PLT
	movq	-104(%rbp), %r9
	jmp	.L5100
	.p2align 4,,10
	.p2align 3
.L5103:
	movq	-96(%rbp), %rdx
	movq	%rbx, %rdi
	addq	$8, %r15
	call	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	cmpq	%r15, -88(%rbp)
	jne	.L5106
	jmp	.L5081
	.p2align 4,,10
	.p2align 3
.L5126:
	movq	%r15, %rax
	subq	%rcx, %rax
	sarq	$3, %rax
.L5082:
	cmpq	$2, %rax
	je	.L5088
	cmpq	$3, %rax
	je	.L5089
	cmpq	$1, %rax
	je	.L5090
.L5092:
	movq	8(%r14), %rax
	movq	%rax, -88(%rbp)
	jmp	.L5094
.L5089:
	movq	(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L5083
	addq	$8, %rcx
.L5088:
	movq	(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	jne	.L5083
	addq	$8, %rcx
.L5090:
	movq	(%rcx), %rax
	movq	(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	je	.L5092
	jmp	.L5083
	.p2align 4,,10
	.p2align 3
.L5123:
	addq	$8, %rcx
	jmp	.L5083
	.p2align 4,,10
	.p2align 3
.L5124:
	addq	$16, %rcx
	jmp	.L5083
	.p2align 4,,10
	.p2align 3
.L5125:
	addq	$24, %rcx
	jmp	.L5083
.L5095:
	movq	8(%r14), %rax
	jmp	.L5107
.L5129:
	call	__stack_chk_fail@PLT
.L5109:
	movq	%rax, -88(%rbp)
	jmp	.L5094
	.cfi_endproc
.LFE24289:
	.size	_ZN2v88internal11KeyedLoadIC30LoadElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_19KeyedAccessLoadModeE, .-_ZN2v88internal11KeyedLoadIC30LoadElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_19KeyedAccessLoadModeE
	.section	.rodata._ZN2v88internal11KeyedLoadIC17UpdateLoadElementENS0_6HandleINS0_10HeapObjectEEENS0_19KeyedAccessLoadModeE.str1.1,"aMS",@progbits,1
.LC90:
	.string	"JSProxy"
.LC91:
	.string	"max polymorph exceeded"
	.section	.text._ZN2v88internal11KeyedLoadIC17UpdateLoadElementENS0_6HandleINS0_10HeapObjectEEENS0_19KeyedAccessLoadModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11KeyedLoadIC17UpdateLoadElementENS0_6HandleINS0_10HeapObjectEEENS0_19KeyedAccessLoadModeE
	.type	_ZN2v88internal11KeyedLoadIC17UpdateLoadElementENS0_6HandleINS0_10HeapObjectEEENS0_19KeyedAccessLoadModeE, @function
_ZN2v88internal11KeyedLoadIC17UpdateLoadElementENS0_6HandleINS0_10HeapObjectEEENS0_19KeyedAccessLoadModeE:
.LFB24286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	%edx, -124(%rbp)
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	-1(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5131
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r15
.L5132:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	cmpb	$0, 64(%r14)
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	jne	.L5134
	movb	$1, 64(%r14)
	leaq	40(%r14), %rsi
	leaq	80(%r14), %rdi
	call	_ZNK2v88internal13FeedbackNexus11ExtractMapsEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EE@PLT
	movq	-104(%rbp), %rsi
.L5134:
	movq	40(%r14), %rbx
	movq	48(%r14), %rcx
	cmpq	%rcx, %rbx
	je	.L5135
	leaq	-80(%rbp), %r13
	jmp	.L5138
	.p2align 4,,10
	.p2align 3
.L5199:
	movq	%rax, (%rsi)
	movq	-104(%rbp), %rax
	addq	$8, %rbx
	leaq	8(%rax), %rsi
	movq	%rsi, -104(%rbp)
	cmpq	%rbx, %rcx
	je	.L5135
.L5138:
	movq	(%rbx), %rax
	movq	%rax, -80(%rbp)
	cmpq	%rsi, -96(%rbp)
	jne	.L5199
	leaq	-112(%rbp), %rdi
	movq	%r13, %rdx
	movq	%rcx, -120(%rbp)
	addq	$8, %rbx
	call	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-120(%rbp), %rcx
	movq	-104(%rbp), %rsi
	cmpq	%rbx, %rcx
	jne	.L5138
	.p2align 4,,10
	.p2align 3
.L5135:
	movq	-112(%rbp), %rdi
	movq	%rdi, %rcx
	movq	%rdi, %rax
	cmpq	%rsi, %rdi
	je	.L5149
	.p2align 4,,10
	.p2align 3
.L5143:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L5141
	movq	(%rdx), %rdx
	movzwl	11(%rdx), %edx
	cmpw	$1041, %dx
	je	.L5200
	cmpw	$1024, %dx
	je	.L5201
.L5141:
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L5143
	cmpl	$3, 24(%r14)
	je	.L5202
	movq	%r15, -80(%rbp)
.L5170:
	movq	%rdi, %rax
	jmp	.L5153
	.p2align 4,,10
	.p2align 3
.L5203:
	testq	%r15, %r15
	je	.L5151
	movq	(%r15), %rcx
	cmpq	%rcx, (%rdx)
	je	.L5152
.L5151:
	addq	$8, %rax
	cmpq	%rax, %rsi
	je	.L5150
.L5153:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L5151
	cmpq	%rdx, %r15
	jne	.L5203
.L5152:
	cmpl	$1, -124(%rbp)
	je	.L5204
.L5157:
	leaq	.LC74(%rip), %rax
	movq	%rax, 72(%r14)
	jmp	.L5140
	.p2align 4,,10
	.p2align 3
.L5131:
	movq	41088(%rbx), %r15
	cmpq	%r15, 41096(%rbx)
	je	.L5205
.L5133:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L5132
	.p2align 4,,10
	.p2align 3
.L5200:
	leaq	.LC73(%rip), %rax
	movq	%rax, 72(%r14)
.L5140:
	testq	%rdi, %rdi
	je	.L5130
.L5198:
	call	_ZdlPv@PLT
.L5130:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5206
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5201:
	.cfi_restore_state
	leaq	.LC90(%rip), %rax
	movq	%rax, 72(%r14)
	jmp	.L5140
	.p2align 4,,10
	.p2align 3
.L5202:
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L5145
.L5196:
	movq	-112(%rbp), %rdi
	movq	-104(%rbp), %rsi
	movq	%r15, -80(%rbp)
	cmpq	%rsi, %rdi
	jne	.L5170
.L5150:
	cmpq	%rsi, -96(%rbp)
	je	.L5154
	movq	%r15, (%rsi)
	movq	-104(%rbp), %rax
	leaq	8(%rax), %rsi
	movq	%rsi, -104(%rbp)
.L5155:
	movq	-112(%rbp), %rdi
	subq	%rdi, %rsi
	sarq	$3, %rsi
	cmpl	%esi, _ZN2v88internal30FLAG_max_polymorphic_map_countE(%rip)
	jge	.L5160
	leaq	.LC91(%rip), %rax
	movq	%rax, 72(%r14)
	jmp	.L5140
.L5145:
	movq	-1(%rax), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L5196
	movq	-1(%rax), %rax
	movzbl	14(%rax), %esi
	shrl	$3, %esi
	cmpq	-104(%rbp), %rcx
	je	.L5207
	movq	(%rcx), %rax
	movq	(%rax), %rax
	movzbl	14(%rax), %edi
	shrl	$3, %edi
	call	_ZN2v88internal35IsMoreGeneralElementsKindTransitionENS0_12ElementsKindES1_@PLT
	testb	%al, %al
	je	.L5196
	.p2align 4,,10
	.p2align 3
.L5149:
	movl	-124(%rbp), %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11KeyedLoadIC18LoadElementHandlerENS0_6HandleINS0_3MapEEENS0_19KeyedAccessLoadModeE
	movq	%r14, %rdi
	leaq	-80(%rbp), %rcx
	movq	%r15, %rdx
	xorl	%esi, %esi
	movl	$1, -80(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE
	movq	-112(%rbp), %rdi
	jmp	.L5140
	.p2align 4,,10
	.p2align 3
.L5160:
	leaq	-80(%rbp), %r12
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	leaq	-112(%rbp), %r15
	movq	%r12, %rdi
	movaps	%xmm0, -80(%rbp)
	call	_ZNSt6vectorIN2v88internal17MaybeObjectHandleESaIS2_EE7reserveEm
	movl	-124(%rbp), %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal11KeyedLoadIC30LoadElementPolymorphicHandlersEPSt6vectorINS0_6HandleINS0_3MapEEESaIS5_EEPS2_INS0_17MaybeObjectHandleESaIS9_EENS0_19KeyedAccessLoadModeE
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rax
	subq	%rdx, %rax
	cmpq	$8, %rax
	je	.L5208
	leaq	80(%r14), %rdi
	movq	%r12, %rcx
	movq	%r15, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal13FeedbackNexus20ConfigurePolymorphicENS0_6HandleINS0_4NameEEERKSt6vectorINS2_INS0_3MapEEESaIS7_EEPS5_INS0_17MaybeObjectHandleESaISC_EE@PLT
	movq	80(%r14), %rax
	movb	$1, 16(%r14)
	testq	%rax, %rax
	je	.L5209
	movq	(%rax), %rsi
.L5164:
	movl	96(%r14), %edx
	movq	8(%r14), %rdi
	leaq	.LC39(%rip), %rcx
	call	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
.L5162:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5165
	call	_ZdlPv@PLT
.L5165:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L5198
	jmp	.L5130
.L5205:
	movq	%rbx, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-120(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L5133
.L5204:
	leaq	80(%r14), %rdi
	movq	%r15, %rsi
	call	_ZNK2v88internal13FeedbackNexus17FindHandlerForMapENS0_6HandleINS0_3MapEEE@PLT
	testq	%rdx, %rdx
	je	.L5197
	movq	(%rdx), %rdi
	movq	%rdi, %rdx
	orq	$2, %rdx
	testl	%eax, %eax
	cmove	%rdx, %rdi
	call	_ZN2v88internal11LoadHandler22GetKeyedAccessLoadModeENS0_11MaybeObjectE@PLT
	movq	-104(%rbp), %rsi
	testl	%eax, %eax
	je	.L5155
.L5197:
	movq	-112(%rbp), %rdi
	jmp	.L5157
.L5209:
	movq	88(%r14), %rsi
	jmp	.L5164
.L5208:
	movq	-80(%rbp), %rcx
	movq	(%rdx), %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal2IC20ConfigureVectorStateENS0_6HandleINS0_4NameEEENS2_INS0_3MapEEERKNS0_17MaybeObjectHandleE
	jmp	.L5162
.L5154:
	leaq	-80(%rbp), %rdx
	leaq	-112(%rbp), %rdi
	call	_ZNSt6vectorIN2v88internal6HandleINS1_3MapEEESaIS4_EE17_M_realloc_insertIJRKS4_EEEvN9__gnu_cxx17__normal_iteratorIPS4_S6_EEDpOT_
	movq	-104(%rbp), %rsi
	jmp	.L5155
.L5206:
	call	__stack_chk_fail@PLT
.L5207:
	xorl	%edx, %edx
	xorl	%esi, %esi
	leaq	.LC40(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE24286:
	.size	_ZN2v88internal11KeyedLoadIC17UpdateLoadElementENS0_6HandleINS0_10HeapObjectEEENS0_19KeyedAccessLoadModeE, .-_ZN2v88internal11KeyedLoadIC17UpdateLoadElementENS0_6HandleINS0_10HeapObjectEEENS0_19KeyedAccessLoadModeE
	.section	.text._ZN2v88internal11KeyedLoadIC4LoadENS0_6HandleINS0_6ObjectEEES4_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11KeyedLoadIC4LoadENS0_6HandleINS0_6ObjectEEES4_
	.type	_ZN2v88internal11KeyedLoadIC4LoadENS0_6HandleINS0_6ObjectEEES4_, @function
_ZN2v88internal11KeyedLoadIC4LoadENS0_6HandleINS0_6ObjectEEES4_:
.LFB24297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L5340
.L5212:
	movq	%r12, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL13TryConvertKeyENS0_6HandleINS0_6ObjectEEEPNS0_7IsolateE
	movq	%rax, %r12
	movq	(%rax), %rax
	testb	$1, %al
	jne	.L5221
.L5225:
	movl	24(%rbx), %edx
	cmpb	$0, _ZN2v88internal11FLAG_use_icE(%rip)
	movl	%edx, %eax
	je	.L5243
	testl	%edx, %edx
	jne	.L5341
.L5230:
	cmpl	$8, 28(%rbx)
	movq	8(%rbx), %rdi
	jne	.L5286
	.p2align 4,,10
	.p2align 3
.L5337:
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal7Runtime17GetObjectPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_Pb@PLT
	jmp	.L5220
	.p2align 4,,10
	.p2align 3
.L5341:
	movq	(%r14), %r13
	testb	$1, %r13b
	je	.L5250
	movq	-1(%r13), %rax
	cmpw	$1026, 11(%rax)
	je	.L5342
	movq	-1(%r13), %rax
	movzbl	13(%rax), %eax
	shrb	$5, %al
	andl	$1, %eax
.L5236:
	testb	%al, %al
	jne	.L5339
	movq	(%r14), %rax
	testb	$1, %al
	je	.L5336
	movq	-1(%rax), %rdx
	cmpw	$1041, 11(%rdx)
	je	.L5339
	movq	-1(%rax), %rdx
	cmpw	$1023, 11(%rdx)
	ja	.L5247
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L5247
	xorl	%edx, %edx
.L5249:
	testb	%dl, %dl
	jne	.L5336
	movq	-1(%rax), %rax
	cmpw	$1086, 11(%rax)
	je	.L5261
	.p2align 4,,10
	.p2align 3
.L5339:
	movl	24(%rbx), %eax
.L5243:
	testl	%eax, %eax
	je	.L5230
	cmpb	$0, 16(%rbx)
	jne	.L5230
	xorl	%r13d, %r13d
.L5273:
	cmpl	$6, %eax
	je	.L5343
.L5275:
	movq	(%r12), %rax
	leaq	80(%rbx), %r15
	xorl	%esi, %esi
	testb	$1, %al
	jne	.L5344
.L5279:
	movq	%r15, %rdi
	call	_ZN2v88internal13FeedbackNexus20ConfigureMegamorphicENS0_11IcCheckTypeE@PLT
	movq	80(%rbx), %rax
	movb	$1, 16(%rbx)
	testq	%rax, %rax
	je	.L5345
	movq	(%rax), %rsi
.L5283:
	movl	96(%rbx), %edx
	movq	8(%rbx), %rdi
	leaq	.LC35(%rip), %rcx
	call	_ZN2v88internal2IC17OnFeedbackChangedEPNS0_7IsolateENS0_14FeedbackVectorENS0_12FeedbackSlotEPKc
	movl	_ZN2v88internal12TracingFlags8ic_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5346
.L5281:
	testq	%r13, %r13
	je	.L5230
.L5265:
	movq	%r13, %rax
	jmp	.L5220
	.p2align 4,,10
	.p2align 3
.L5340:
	movq	-1(%rax), %rcx
	cmpw	$1024, 11(%rcx)
	jbe	.L5212
	movq	-1(%rax), %rax
	movl	15(%rax), %eax
	testl	$16777216, %eax
	je	.L5212
	movq	%rdx, %rdi
	call	_ZN2v88internal8JSObject15MigrateInstanceEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	cmpl	$8, 28(%rbx)
	movq	8(%rbx), %rdi
	je	.L5337
.L5286:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN2v88internal7Runtime11HasPropertyEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_@PLT
.L5220:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L5347
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5221:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	movzwl	11(%rdx), %edx
	andl	$65504, %edx
	jne	.L5229
	movq	%rax, -64(%rbp)
	movl	7(%rax), %eax
	testb	$1, %al
	jne	.L5227
	testb	$2, %al
	jne	.L5228
.L5227:
	leaq	-72(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	testb	%al, %al
	je	.L5228
	movq	(%r12), %rax
.L5229:
	testb	$1, %al
	je	.L5225
	movq	-1(%rax), %rax
	cmpw	$64, 11(%rax)
	jne	.L5225
.L5228:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6LoadIC4LoadENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEE
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L5220
	movl	24(%rbx), %eax
	testl	%eax, %eax
	je	.L5265
	cmpb	$0, 16(%rbx)
	jne	.L5265
	jmp	.L5273
	.p2align 4,,10
	.p2align 3
.L5336:
	movl	24(%rbx), %edx
	.p2align 4,,10
	.p2align 3
.L5250:
	movl	%edx, %eax
	jmp	.L5243
	.p2align 4,,10
	.p2align 3
.L5344:
	movq	-1(%rax), %rax
	xorl	%esi, %esi
	cmpw	$64, 11(%rax)
	setbe	%sil
	jmp	.L5279
	.p2align 4,,10
	.p2align 3
.L5345:
	movq	88(%rbx), %rsi
	jmp	.L5283
	.p2align 4,,10
	.p2align 3
.L5343:
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.L5348
	movq	(%rax), %rdx
.L5277:
	movl	96(%rbx), %eax
	leal	56(,%rax,8), %eax
	cltq
	movq	-1(%rdx,%rax), %rax
	shrq	$32, %rax
	je	.L5281
	jmp	.L5275
	.p2align 4,,10
	.p2align 3
.L5247:
	movq	(%r12), %rdx
	testb	$1, %dl
	jne	.L5251
	sarq	$32, %rdx
	js	.L5253
	movl	%edx, -72(%rbp)
	movl	%edx, %esi
.L5260:
	movq	%r14, %rdi
	movq	8(%rbx), %r13
	call	_ZN2v88internal12_GLOBAL__N_119IsOutOfBoundsAccessENS0_6HandleINS0_6ObjectEEEj
	xorl	%edx, %edx
	testb	%al, %al
	jne	.L5349
.L5267:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal11KeyedLoadIC17UpdateLoadElementENS0_6HandleINS0_10HeapObjectEEENS0_19KeyedAccessLoadModeE
	cmpb	$0, 16(%rbx)
	je	.L5339
	movq	%r12, %rdx
	leaq	.LC63(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEE
	jmp	.L5339
	.p2align 4,,10
	.p2align 3
.L5349:
	movq	(%r14), %rax
	movq	-1(%rax), %r15
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L5268
	movq	%r15, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L5269:
	movq	%r13, %rdi
	call	_ZN2v88internal12_GLOBAL__N_134AllowConvertHoleElementToUndefinedEPNS0_7IsolateENS0_6HandleINS0_3MapEEE
	xorl	%edx, %edx
	testb	%al, %al
	setne	%dl
	jmp	.L5267
	.p2align 4,,10
	.p2align 3
.L5268:
	movq	41088(%r13), %rsi
	cmpq	41096(%r13), %rsi
	je	.L5350
.L5270:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r13)
	movq	%r15, (%rsi)
	jmp	.L5269
.L5251:
	movq	-1(%rdx), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L5253
	movsd	7(%rdx), %xmm0
	movsd	.LC92(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rdx
	shrq	$32, %rcx
	cmpq	$1127219200, %rcx
	jne	.L5253
	movl	%edx, %ecx
	pxor	%xmm1, %xmm1
	movd	%xmm2, %esi
	movd	%xmm2, -72(%rbp)
	cvtsi2sdq	%rcx, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L5253
	jne	.L5253
	cmpl	$-1, %edx
	jne	.L5260
.L5253:
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L5249
	.p2align 4,,10
	.p2align 3
.L5346:
	movl	24(%rbx), %r8d
	xorl	%ecx, %ecx
	testl	%r8d, %r8d
	jne	.L5351
.L5285:
	movq	%r12, %rdx
	leaq	.LC63(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal2IC7TraceICEPKcNS0_6HandleINS0_6ObjectEEENS0_16InlineCacheStateES7_
	jmp	.L5281
	.p2align 4,,10
	.p2align 3
.L5342:
	movq	%r13, %r15
	leaq	-64(%rbp), %rdi
	andq	$-262144, %r15
	movq	24(%r15), %rax
	movq	-25128(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	24(%r15), %rcx
	movq	-1(%r13), %rdx
	cmpw	$1024, 11(%rdx)
	je	.L5352
	movq	-1(%r13), %rdx
	movq	23(%rdx), %rdx
.L5235:
	cmpq	%rax, %rdx
	setne	%al
	jmp	.L5236
.L5261:
	movq	(%r12), %rax
	leaq	-68(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6Object7ToInt32EPi@PLT
	testb	%al, %al
	je	.L5339
	movl	-68(%rbp), %esi
	movl	%esi, -72(%rbp)
	jmp	.L5260
.L5348:
	movq	88(%rbx), %rdx
	jmp	.L5277
.L5351:
	movq	%r15, %rdi
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movl	24(%rbx), %ecx
	movl	%eax, %r8d
	jmp	.L5285
.L5350:
	movq	%r13, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L5270
.L5352:
	movq	-37488(%rcx), %rdx
	jmp	.L5235
.L5347:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24297:
	.size	_ZN2v88internal11KeyedLoadIC4LoadENS0_6HandleINS0_6ObjectEEES4_, .-_ZN2v88internal11KeyedLoadIC4LoadENS0_6HandleINS0_6ObjectEEES4_
	.section	.rodata._ZN2v88internalL25Stats_Runtime_LoadIC_MissEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC93:
	.string	"V8.Runtime_Runtime_LoadIC_Miss"
	.section	.text._ZN2v88internalL25Stats_Runtime_LoadIC_MissEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL25Stats_Runtime_LoadIC_MissEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL25Stats_Runtime_LoadIC_MissEiPmPNS0_7IsolateE.isra.0:
.LFB30909:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -240(%rbp)
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5416
.L5354:
	movq	_ZZN2v88internalL25Stats_Runtime_LoadIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2158(%rip), %rbx
	testq	%rbx, %rbx
	je	.L5417
.L5356:
	movq	$0, -272(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L5418
.L5358:
	movq	41088(%r12), %rax
	leaq	-8(%r13), %r14
	leaq	-24(%r13), %rdx
	addl	$1, 41104(%r12)
	movslq	-12(%r13), %rcx
	movq	%rax, -304(%rbp)
	movq	41096(%r12), %rax
	movq	%rax, -296(%rbp)
	movq	-24(%r13), %rax
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-37504(%rsi), %rax
	jne	.L5419
	xorl	%edx, %edx
	leaq	-192(%rbp), %r15
.L5362:
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movb	$0, -128(%rbp)
	movq	%rbx, -192(%rbp)
	movq	%r12, -184(%rbp)
	movb	$0, -176(%rbp)
	movl	$5, -164(%rbp)
	movq	$0, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	$0, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	testq	%rdx, %rdx
	je	.L5364
	movq	(%rdx), %rax
	leaq	-280(%rbp), %rdi
	movl	%ecx, %esi
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-112(%rbp), %rdi
	movl	%eax, -92(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
.L5382:
	movl	%eax, -168(%rbp)
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%eax, -172(%rbp)
	leaq	16+_ZTVN2v88internal6LoadICE(%rip), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6LoadIC4LoadENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEE
	testq	%rax, %rax
	je	.L5415
.L5376:
	movq	(%rax), %r13
.L5377:
	movq	-152(%rbp), %rdi
	movq	%rbx, -192(%rbp)
	testq	%rdi, %rdi
	je	.L5368
	call	_ZdlPv@PLT
.L5368:
	subl	$1, 41104(%r12)
	movq	-304(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-296(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L5381
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5381:
	leaq	-272(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L5420
.L5353:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5421
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5364:
	.cfi_restore_state
	movl	$0, -92(%rbp)
	xorl	%eax, %eax
	jmp	.L5382
	.p2align 4,,10
	.p2align 3
.L5419:
	leaq	-192(%rbp), %r15
	movl	%ecx, %esi
	movq	%rdx, -320(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -312(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	-312(%rbp), %rcx
	movq	-320(%rbp), %rdx
	cmpl	$5, %eax
	movl	%eax, %r11d
	je	.L5362
	leal	-6(%rax), %eax
	cmpl	$1, %eax
	ja	.L5369
	movq	12464(%r12), %rax
	movq	%r15, %rdi
	movq	%rcx, -328(%rbp)
	movl	%r11d, -320(%rbp)
	movq	%rdx, -312(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r12), %rdi
	movq	-312(%rbp), %rdx
	movl	-320(%rbp), %r11d
	movq	-328(%rbp), %rcx
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L5370
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-312(%rbp), %rdx
	movl	-320(%rbp), %r11d
	movq	-328(%rbp), %rcx
	movq	%rax, %r10
.L5371:
	movq	-24(%r13), %rax
	pxor	%xmm0, %xmm0
	movl	%ecx, %esi
	movq	%rdx, -112(%rbp)
	leaq	-280(%rbp), %rdi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movl	%ecx, -96(%rbp)
	movq	%r10, -312(%rbp)
	movl	%r11d, -164(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, -192(%rbp)
	movq	%r12, -184(%rbp)
	movb	$0, -176(%rbp)
	movb	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-112(%rbp), %rdi
	movl	%eax, -92(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	-312(%rbp), %r10
	movq	%r15, %rdi
	movq	%r14, %rdx
	movl	%eax, -168(%rbp)
	movl	%eax, -172(%rbp)
	movq	%r10, %rsi
	leaq	16+_ZTVN2v88internal12LoadGlobalICE(%rip), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12LoadGlobalIC4LoadENS0_6HandleINS0_4NameEEE
	testq	%rax, %rax
	jne	.L5376
.L5415:
	movq	312(%r12), %r13
	jmp	.L5377
	.p2align 4,,10
	.p2align 3
.L5418:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5422
.L5359:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5360
	movq	(%rdi), %rax
	call	*8(%rax)
.L5360:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5361
	movq	(%rdi), %rax
	call	*8(%rax)
.L5361:
	leaq	.LC93(%rip), %rax
	movq	%rbx, -264(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-264(%rbp), %rax
	movq	%r14, -248(%rbp)
	movq	%rax, -272(%rbp)
	jmp	.L5358
	.p2align 4,,10
	.p2align 3
.L5417:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5423
.L5357:
	movq	%rbx, _ZZN2v88internalL25Stats_Runtime_LoadIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2158(%rip)
	jmp	.L5356
	.p2align 4,,10
	.p2align 3
.L5369:
	movq	-24(%r13), %rax
	pxor	%xmm0, %xmm0
	movl	%ecx, %esi
	movq	%rdx, -112(%rbp)
	leaq	-280(%rbp), %rdi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movl	%ecx, -96(%rbp)
	movl	%r11d, -164(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, -192(%rbp)
	movq	%r12, -184(%rbp)
	movb	$0, -176(%rbp)
	movb	$0, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-112(%rbp), %rdi
	movl	%eax, -92(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%eax, -168(%rbp)
	movl	%eax, -172(%rbp)
	leaq	16+_ZTVN2v88internal11KeyedLoadICE(%rip), %rax
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11KeyedLoadIC4LoadENS0_6HandleINS0_6ObjectEEES4_
	testq	%rax, %rax
	jne	.L5376
	jmp	.L5415
	.p2align 4,,10
	.p2align 3
.L5370:
	movq	41088(%r12), %r10
	cmpq	41096(%r12), %r10
	je	.L5424
.L5372:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r10)
	jmp	.L5371
	.p2align 4,,10
	.p2align 3
.L5420:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L5353
	.p2align 4,,10
	.p2align 3
.L5416:
	movq	40960(%rsi), %rax
	movl	$325, %edx
	leaq	-232(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L5354
	.p2align 4,,10
	.p2align 3
.L5423:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L5357
	.p2align 4,,10
	.p2align 3
.L5422:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC93(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L5359
	.p2align 4,,10
	.p2align 3
.L5424:
	movq	%r12, %rdi
	movq	%rcx, -336(%rbp)
	movl	%r11d, -320(%rbp)
	movq	%rdx, -312(%rbp)
	movq	%rax, -328(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-336(%rbp), %rcx
	movq	-328(%rbp), %rsi
	movl	-320(%rbp), %r11d
	movq	-312(%rbp), %rdx
	movq	%rax, %r10
	jmp	.L5372
.L5421:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30909:
	.size	_ZN2v88internalL25Stats_Runtime_LoadIC_MissEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL25Stats_Runtime_LoadIC_MissEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal19Runtime_LoadIC_MissEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Runtime_LoadIC_MissEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Runtime_LoadIC_MissEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Runtime_LoadIC_MissEiPmPNS0_7IsolateE:
.LFB24318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %r10d
	testl	%r10d, %r10d
	jne	.L5464
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	leaq	-8(%rsi), %r14
	movslq	-12(%rsi), %rcx
	movq	%rax, -192(%rbp)
	movq	41096(%rdx), %rax
	leaq	-24(%rsi), %rdx
	movq	%rax, -184(%rbp)
	movq	-24(%rsi), %rax
	movq	%rax, %rsi
	andq	$-262144, %rsi
	movq	24(%rsi), %rsi
	cmpq	-37504(%rsi), %rax
	jne	.L5465
	xorl	%edx, %edx
	leaq	-160(%rbp), %r15
.L5428:
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movb	$0, -96(%rbp)
	movq	%rbx, -160(%rbp)
	movq	%r12, -152(%rbp)
	movb	$0, -144(%rbp)
	movl	$5, -132(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	%ecx, -64(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	testq	%rdx, %rdx
	je	.L5430
	movq	(%rdx), %rax
	leaq	-168(%rbp), %rdi
	movl	%ecx, %esi
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, -60(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movl	%eax, %r10d
.L5447:
	leaq	16+_ZTVN2v88internal6LoadICE(%rip), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%r10d, -136(%rbp)
	movl	%r10d, -140(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal6LoadIC4LoadENS0_6HandleINS0_6ObjectEEENS2_INS0_4NameEEE
	testq	%rax, %rax
	je	.L5463
.L5442:
	movq	(%rax), %r13
.L5443:
	movq	-120(%rbp), %rdi
	movq	%rbx, -160(%rbp)
	testq	%rdi, %rdi
	je	.L5434
	call	_ZdlPv@PLT
.L5434:
	subl	$1, 41104(%r12)
	movq	-192(%rbp), %rax
	movq	%rax, 41088(%r12)
	movq	-184(%rbp), %rax
	cmpq	41096(%r12), %rax
	je	.L5425
	movq	%rax, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5425:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5466
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5465:
	.cfi_restore_state
	leaq	-160(%rbp), %r15
	movl	%ecx, %esi
	movl	%r10d, -216(%rbp)
	movq	%r15, %rdi
	movq	%rdx, -208(%rbp)
	movq	%rcx, -200(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdx
	cmpl	$5, %eax
	movl	-216(%rbp), %r10d
	movl	%eax, %r11d
	je	.L5428
	leal	-6(%rax), %eax
	cmpl	$1, %eax
	ja	.L5435
	movq	12464(%r12), %rax
	movq	%r15, %rdi
	movq	%rcx, -216(%rbp)
	movq	%rdx, -208(%rbp)
	movl	%r11d, -200(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal7Context13global_objectEv@PLT
	movq	41112(%r12), %rdi
	movl	-200(%rbp), %r11d
	movq	-208(%rbp), %rdx
	movq	-216(%rbp), %rcx
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L5436
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movl	-200(%rbp), %r11d
	movq	-208(%rbp), %rdx
	movq	-216(%rbp), %rcx
	movq	%rax, %r10
.L5437:
	movq	-24(%r13), %rax
	pxor	%xmm0, %xmm0
	movl	%ecx, %esi
	movq	%rdx, -80(%rbp)
	leaq	-168(%rbp), %rdi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movl	%ecx, -64(%rbp)
	movq	%r10, -200(%rbp)
	movl	%r11d, -132(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, -160(%rbp)
	movq	%r12, -152(%rbp)
	movb	$0, -144(%rbp)
	movb	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, -60(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	-200(%rbp), %r10
	movq	%r15, %rdi
	movq	%r14, %rdx
	movl	%eax, -136(%rbp)
	movl	%eax, -140(%rbp)
	movq	%r10, %rsi
	leaq	16+_ZTVN2v88internal12LoadGlobalICE(%rip), %rax
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12LoadGlobalIC4LoadENS0_6HandleINS0_4NameEEE
	testq	%rax, %rax
	jne	.L5442
.L5463:
	movq	312(%r12), %r13
	jmp	.L5443
	.p2align 4,,10
	.p2align 3
.L5435:
	movq	-24(%r13), %rax
	pxor	%xmm0, %xmm0
	movl	%ecx, %esi
	movq	%rdx, -80(%rbp)
	leaq	-168(%rbp), %rdi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movl	%r11d, -132(%rbp)
	movl	%ecx, -64(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	%rbx, -160(%rbp)
	movq	%r12, -152(%rbp)
	movb	$0, -144(%rbp)
	movb	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, -60(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%eax, -136(%rbp)
	movl	%eax, -140(%rbp)
	leaq	16+_ZTVN2v88internal11KeyedLoadICE(%rip), %rax
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal11KeyedLoadIC4LoadENS0_6HandleINS0_6ObjectEEES4_
	testq	%rax, %rax
	jne	.L5442
	jmp	.L5463
	.p2align 4,,10
	.p2align 3
.L5430:
	movl	$0, -60(%rbp)
	jmp	.L5447
	.p2align 4,,10
	.p2align 3
.L5436:
	movq	41088(%r12), %r10
	cmpq	41096(%r12), %r10
	je	.L5467
.L5438:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r10)
	jmp	.L5437
	.p2align 4,,10
	.p2align 3
.L5464:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL25Stats_Runtime_LoadIC_MissEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L5425
	.p2align 4,,10
	.p2align 3
.L5467:
	movq	%r12, %rdi
	movq	%rcx, -224(%rbp)
	movq	%rdx, -208(%rbp)
	movl	%r11d, -200(%rbp)
	movq	%rax, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-224(%rbp), %rcx
	movq	-216(%rbp), %rsi
	movq	-208(%rbp), %rdx
	movl	-200(%rbp), %r11d
	movq	%rax, %r10
	jmp	.L5438
.L5466:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24318:
	.size	_ZN2v88internal19Runtime_LoadIC_MissEiPmPNS0_7IsolateE, .-_ZN2v88internal19Runtime_LoadIC_MissEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL30Stats_Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC94:
	.string	"V8.Runtime_Runtime_KeyedLoadIC_Miss"
	.section	.text._ZN2v88internalL30Stats_Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Stats_Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL30Stats_Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateE.isra.0:
.LFB30911:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -240(%rbp)
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5509
.L5469:
	movq	_ZZN2v88internalL30Stats_Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2276(%rip), %rbx
	testq	%rbx, %rbx
	je	.L5510
.L5471:
	movq	$0, -272(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L5511
.L5473:
	movq	41088(%r12), %rax
	pxor	%xmm0, %xmm0
	leaq	-8(%r13), %r15
	addl	$1, 41104(%r12)
	movq	-24(%r13), %rdx
	movslq	-12(%r13), %rsi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movq	%rax, -304(%rbp)
	leaq	-24(%r13), %rax
	movq	41096(%r12), %r14
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	movq	%rbx, -192(%rbp)
	movq	%r12, -184(%rbp)
	cmpq	-37504(%rcx), %rdx
	movl	$0, %edx
	movb	$0, -128(%rbp)
	cmove	%rdx, %rax
	movb	$0, -176(%rbp)
	movl	$8, -164(%rbp)
	movq	$0, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -104(%rbp)
	movl	%esi, -96(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	testq	%rax, %rax
	je	.L5478
	movq	(%rax), %rax
	leaq	-280(%rbp), %rdi
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-112(%rbp), %rdi
	movl	%eax, -92(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
.L5485:
	leaq	-192(%rbp), %rdi
	movl	%eax, -168(%rbp)
	movq	%r15, %rdx
	movq	%r13, %rsi
	movl	%eax, -172(%rbp)
	leaq	16+_ZTVN2v88internal11KeyedLoadICE(%rip), %rax
	movq	%rdi, -296(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	-296(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal11KeyedLoadIC4LoadENS0_6HandleINS0_6ObjectEEES4_
	testq	%rax, %rax
	je	.L5512
	movq	(%rax), %r13
.L5480:
	movq	-152(%rbp), %rdi
	movq	%rbx, -192(%rbp)
	testq	%rdi, %rdi
	je	.L5481
	call	_ZdlPv@PLT
.L5481:
	subl	$1, 41104(%r12)
	movq	-304(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L5484
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5484:
	leaq	-272(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L5513
.L5468:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5514
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5478:
	.cfi_restore_state
	movl	$0, -92(%rbp)
	xorl	%eax, %eax
	jmp	.L5485
	.p2align 4,,10
	.p2align 3
.L5511:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5515
.L5474:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5475
	movq	(%rdi), %rax
	call	*8(%rax)
.L5475:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5476
	movq	(%rdi), %rax
	call	*8(%rax)
.L5476:
	leaq	.LC94(%rip), %rax
	movq	%rbx, -264(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-264(%rbp), %rax
	movq	%r14, -248(%rbp)
	movq	%rax, -272(%rbp)
	jmp	.L5473
	.p2align 4,,10
	.p2align 3
.L5510:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5516
.L5472:
	movq	%rbx, _ZZN2v88internalL30Stats_Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2276(%rip)
	jmp	.L5471
	.p2align 4,,10
	.p2align 3
.L5512:
	movq	312(%r12), %r13
	jmp	.L5480
	.p2align 4,,10
	.p2align 3
.L5513:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L5468
	.p2align 4,,10
	.p2align 3
.L5509:
	movq	40960(%rsi), %rax
	movl	$318, %edx
	leaq	-232(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L5469
	.p2align 4,,10
	.p2align 3
.L5516:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L5472
	.p2align 4,,10
	.p2align 3
.L5515:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC94(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L5474
.L5514:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30911:
	.size	_ZN2v88internalL30Stats_Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL30Stats_Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal24Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal24Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateE
	.type	_ZN2v88internal24Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateE, @function
_ZN2v88internal24Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateE:
.LFB24327:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5534
	addl	$1, 41104(%rdx)
	movq	-24(%rsi), %rcx
	leaq	-8(%rsi), %r15
	pxor	%xmm0, %xmm0
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	leaq	-24(%rsi), %rdx
	movq	%rcx, %rsi
	andq	$-262144, %rsi
	movq	%rbx, -192(%rbp)
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movq	24(%rsi), %rsi
	movq	%rbx, -160(%rbp)
	movq	%r12, -152(%rbp)
	cmpq	-37504(%rsi), %rcx
	movl	$0, %ecx
	movslq	-12(%r13), %rsi
	movb	$0, -96(%rbp)
	cmove	%rcx, %rdx
	movb	$0, -144(%rbp)
	movl	$8, -132(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	%esi, -64(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	testq	%rdx, %rdx
	je	.L5521
	movq	(%rdx), %rax
	leaq	-168(%rbp), %rdi
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, -60(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
.L5527:
	leaq	-160(%rbp), %rdi
	movl	%eax, -136(%rbp)
	movq	%r15, %rdx
	movq	%r13, %rsi
	movl	%eax, -140(%rbp)
	leaq	16+_ZTVN2v88internal11KeyedLoadICE(%rip), %rax
	movq	%rdi, -184(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	-184(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal11KeyedLoadIC4LoadENS0_6HandleINS0_6ObjectEEES4_
	testq	%rax, %rax
	je	.L5535
	movq	(%rax), %r13
.L5523:
	movq	-120(%rbp), %rdi
	movq	%rbx, -160(%rbp)
	testq	%rdi, %rdi
	je	.L5524
	call	_ZdlPv@PLT
.L5524:
	subl	$1, 41104(%r12)
	movq	-192(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L5517
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5517:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5536
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5521:
	.cfi_restore_state
	movl	$0, -60(%rbp)
	jmp	.L5527
	.p2align 4,,10
	.p2align 3
.L5535:
	movq	312(%r12), %r13
	jmp	.L5523
	.p2align 4,,10
	.p2align 3
.L5534:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL30Stats_Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L5517
.L5536:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24327:
	.size	_ZN2v88internal24Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateE, .-_ZN2v88internal24Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL29Stats_Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC95:
	.string	"V8.Runtime_Runtime_KeyedHasIC_Miss"
	.section	.text._ZN2v88internalL29Stats_Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL29Stats_Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL29Stats_Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateE.isra.0:
.LFB30915:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -240(%rbp)
	movq	$0, -208(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5578
.L5538:
	movq	_ZZN2v88internalL29Stats_Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2802(%rip), %rbx
	testq	%rbx, %rbx
	je	.L5579
.L5540:
	movq	$0, -272(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L5580
.L5542:
	movq	41088(%r12), %rax
	pxor	%xmm0, %xmm0
	leaq	-8(%r13), %r15
	addl	$1, 41104(%r12)
	movq	-24(%r13), %rdx
	movslq	-12(%r13), %rsi
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movq	%rax, -304(%rbp)
	leaq	-24(%r13), %rax
	movq	41096(%r12), %r14
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	movq	%rbx, -192(%rbp)
	movq	%r12, -184(%rbp)
	cmpq	-37504(%rcx), %rdx
	movl	$0, %edx
	movb	$0, -128(%rbp)
	cmove	%rdx, %rax
	movb	$0, -176(%rbp)
	movl	$9, -164(%rbp)
	movq	$0, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	$0, -104(%rbp)
	movl	%esi, -96(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	testq	%rax, %rax
	je	.L5547
	movq	(%rax), %rax
	leaq	-280(%rbp), %rdi
	movq	%rax, -280(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-112(%rbp), %rdi
	movl	%eax, -92(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
.L5554:
	leaq	-192(%rbp), %rdi
	movl	%eax, -168(%rbp)
	movq	%r15, %rdx
	movq	%r13, %rsi
	movl	%eax, -172(%rbp)
	leaq	16+_ZTVN2v88internal11KeyedLoadICE(%rip), %rax
	movq	%rdi, -296(%rbp)
	movq	%rax, -192(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	-296(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal11KeyedLoadIC4LoadENS0_6HandleINS0_6ObjectEEES4_
	testq	%rax, %rax
	je	.L5581
	movq	(%rax), %r13
.L5549:
	movq	-152(%rbp), %rdi
	movq	%rbx, -192(%rbp)
	testq	%rdi, %rdi
	je	.L5550
	call	_ZdlPv@PLT
.L5550:
	subl	$1, 41104(%r12)
	movq	-304(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L5553
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5553:
	leaq	-272(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L5582
.L5537:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5583
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5547:
	.cfi_restore_state
	movl	$0, -92(%rbp)
	xorl	%eax, %eax
	jmp	.L5554
	.p2align 4,,10
	.p2align 3
.L5580:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5584
.L5543:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5544
	movq	(%rdi), %rax
	call	*8(%rax)
.L5544:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5545
	movq	(%rdi), %rax
	call	*8(%rax)
.L5545:
	leaq	.LC95(%rip), %rax
	movq	%rbx, -264(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-264(%rbp), %rax
	movq	%r14, -248(%rbp)
	movq	%rax, -272(%rbp)
	jmp	.L5542
	.p2align 4,,10
	.p2align 3
.L5579:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L5585
.L5541:
	movq	%rbx, _ZZN2v88internalL29Stats_Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2802(%rip)
	jmp	.L5540
	.p2align 4,,10
	.p2align 3
.L5581:
	movq	312(%r12), %r13
	jmp	.L5549
	.p2align 4,,10
	.p2align 3
.L5582:
	leaq	-232(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L5537
	.p2align 4,,10
	.p2align 3
.L5578:
	movq	40960(%rsi), %rax
	movl	$335, %edx
	leaq	-232(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -240(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L5538
	.p2align 4,,10
	.p2align 3
.L5585:
	leaq	.LC8(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L5541
	.p2align 4,,10
	.p2align 3
.L5584:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC95(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L5543
.L5583:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE30915:
	.size	_ZN2v88internalL29Stats_Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL29Stats_Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal23Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal23Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateE
	.type	_ZN2v88internal23Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateE, @function
_ZN2v88internal23Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateE:
.LFB24378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L5603
	addl	$1, 41104(%rdx)
	movq	-24(%rsi), %rcx
	leaq	-8(%rsi), %r15
	pxor	%xmm0, %xmm0
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	leaq	-24(%rsi), %rdx
	movq	%rcx, %rsi
	andq	$-262144, %rsi
	movq	%rbx, -192(%rbp)
	leaq	16+_ZTVN2v88internal2ICE(%rip), %rbx
	movq	24(%rsi), %rsi
	movq	%rbx, -160(%rbp)
	movq	%r12, -152(%rbp)
	cmpq	-37504(%rsi), %rcx
	movl	$0, %ecx
	movslq	-12(%r13), %rsi
	movb	$0, -96(%rbp)
	cmove	%rcx, %rdx
	movb	$0, -144(%rbp)
	movl	$9, -132(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	%esi, -64(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	testq	%rdx, %rdx
	je	.L5590
	movq	(%rdx), %rax
	leaq	-168(%rbp), %rdi
	movq	%rax, -168(%rbp)
	call	_ZNK2v88internal14FeedbackVector7GetKindENS0_12FeedbackSlotE@PLT
	leaq	-80(%rbp), %rdi
	movl	%eax, -60(%rbp)
	call	_ZNK2v88internal13FeedbackNexus8ic_stateEv@PLT
.L5596:
	leaq	-160(%rbp), %rdi
	movl	%eax, -136(%rbp)
	movq	%r15, %rdx
	movq	%r13, %rsi
	movl	%eax, -140(%rbp)
	leaq	16+_ZTVN2v88internal11KeyedLoadICE(%rip), %rax
	movq	%rdi, -184(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZN2v88internal2IC11UpdateStateENS0_6HandleINS0_6ObjectEEES4_
	movq	-184(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN2v88internal11KeyedLoadIC4LoadENS0_6HandleINS0_6ObjectEEES4_
	testq	%rax, %rax
	je	.L5604
	movq	(%rax), %r13
.L5592:
	movq	-120(%rbp), %rdi
	movq	%rbx, -160(%rbp)
	testq	%rdi, %rdi
	je	.L5593
	call	_ZdlPv@PLT
.L5593:
	subl	$1, 41104(%r12)
	movq	-192(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L5586
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L5586:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5605
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5590:
	.cfi_restore_state
	movl	$0, -60(%rbp)
	jmp	.L5596
	.p2align 4,,10
	.p2align 3
.L5604:
	movq	312(%r12), %r13
	jmp	.L5592
	.p2align 4,,10
	.p2align 3
.L5603:
	movq	%r13, %rdi
	movq	%rdx, %rsi
	call	_ZN2v88internalL29Stats_Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateE.isra.0
	movq	%rax, %r13
	jmp	.L5586
.L5605:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24378:
	.size	_ZN2v88internal23Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateE, .-_ZN2v88internal23Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal2IC23TransitionMarkFromStateENS0_16InlineCacheStateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal2IC23TransitionMarkFromStateENS0_16InlineCacheStateE, @function
_GLOBAL__sub_I__ZN2v88internal2IC23TransitionMarkFromStateENS0_16InlineCacheStateE:
.LFB30761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE30761:
	.size	_GLOBAL__sub_I__ZN2v88internal2IC23TransitionMarkFromStateENS0_16InlineCacheStateE, .-_GLOBAL__sub_I__ZN2v88internal2IC23TransitionMarkFromStateENS0_16InlineCacheStateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal2IC23TransitionMarkFromStateENS0_16InlineCacheStateE
	.section	.rodata.CSWTCH.1572,"a"
	.align 8
	.type	CSWTCH.1572, @object
	.size	CSWTCH.1572, 8
CSWTCH.1572:
	.byte	88
	.byte	48
	.byte	46
	.byte	49
	.byte	94
	.byte	80
	.byte	78
	.byte	71
	.weak	_ZTVN2v88internal2ICE
	.section	.data.rel.ro.local._ZTVN2v88internal2ICE,"awG",@progbits,_ZTVN2v88internal2ICE,comdat
	.align 8
	.type	_ZTVN2v88internal2ICE, @object
	.size	_ZTVN2v88internal2ICE, 32
_ZTVN2v88internal2ICE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal2ICD1Ev
	.quad	_ZN2v88internal2ICD0Ev
	.weak	_ZTVN2v88internal6LoadICE
	.section	.data.rel.ro.local._ZTVN2v88internal6LoadICE,"awG",@progbits,_ZTVN2v88internal6LoadICE,comdat
	.align 8
	.type	_ZTVN2v88internal6LoadICE, @object
	.size	_ZTVN2v88internal6LoadICE, 40
_ZTVN2v88internal6LoadICE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal6LoadICD1Ev
	.quad	_ZN2v88internal6LoadICD0Ev
	.quad	_ZNK2v88internal6LoadIC9slow_stubEv
	.weak	_ZTVN2v88internal12LoadGlobalICE
	.section	.data.rel.ro.local._ZTVN2v88internal12LoadGlobalICE,"awG",@progbits,_ZTVN2v88internal12LoadGlobalICE,comdat
	.align 8
	.type	_ZTVN2v88internal12LoadGlobalICE, @object
	.size	_ZTVN2v88internal12LoadGlobalICE, 40
_ZTVN2v88internal12LoadGlobalICE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12LoadGlobalICD1Ev
	.quad	_ZN2v88internal12LoadGlobalICD0Ev
	.quad	_ZNK2v88internal12LoadGlobalIC9slow_stubEv
	.weak	_ZTVN2v88internal11KeyedLoadICE
	.section	.data.rel.ro.local._ZTVN2v88internal11KeyedLoadICE,"awG",@progbits,_ZTVN2v88internal11KeyedLoadICE,comdat
	.align 8
	.type	_ZTVN2v88internal11KeyedLoadICE, @object
	.size	_ZTVN2v88internal11KeyedLoadICE, 40
_ZTVN2v88internal11KeyedLoadICE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11KeyedLoadICD1Ev
	.quad	_ZN2v88internal11KeyedLoadICD0Ev
	.quad	_ZNK2v88internal6LoadIC9slow_stubEv
	.weak	_ZTVN2v88internal7StoreICE
	.section	.data.rel.ro.local._ZTVN2v88internal7StoreICE,"awG",@progbits,_ZTVN2v88internal7StoreICE,comdat
	.align 8
	.type	_ZTVN2v88internal7StoreICE, @object
	.size	_ZTVN2v88internal7StoreICE, 40
_ZTVN2v88internal7StoreICE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal7StoreICD1Ev
	.quad	_ZN2v88internal7StoreICD0Ev
	.quad	_ZNK2v88internal7StoreIC9slow_stubEv
	.weak	_ZTVN2v88internal13StoreGlobalICE
	.section	.data.rel.ro.local._ZTVN2v88internal13StoreGlobalICE,"awG",@progbits,_ZTVN2v88internal13StoreGlobalICE,comdat
	.align 8
	.type	_ZTVN2v88internal13StoreGlobalICE, @object
	.size	_ZTVN2v88internal13StoreGlobalICE, 40
_ZTVN2v88internal13StoreGlobalICE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal13StoreGlobalICD1Ev
	.quad	_ZN2v88internal13StoreGlobalICD0Ev
	.quad	_ZNK2v88internal13StoreGlobalIC9slow_stubEv
	.weak	_ZTVN2v88internal12KeyedStoreICE
	.section	.data.rel.ro.local._ZTVN2v88internal12KeyedStoreICE,"awG",@progbits,_ZTVN2v88internal12KeyedStoreICE,comdat
	.align 8
	.type	_ZTVN2v88internal12KeyedStoreICE, @object
	.size	_ZTVN2v88internal12KeyedStoreICE, 40
_ZTVN2v88internal12KeyedStoreICE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12KeyedStoreICD1Ev
	.quad	_ZN2v88internal12KeyedStoreICD0Ev
	.quad	_ZNK2v88internal12KeyedStoreIC9slow_stubEv
	.weak	_ZTVN2v88internal21StoreInArrayLiteralICE
	.section	.data.rel.ro.local._ZTVN2v88internal21StoreInArrayLiteralICE,"awG",@progbits,_ZTVN2v88internal21StoreInArrayLiteralICE,comdat
	.align 8
	.type	_ZTVN2v88internal21StoreInArrayLiteralICE, @object
	.size	_ZTVN2v88internal21StoreInArrayLiteralICE, 40
_ZTVN2v88internal21StoreInArrayLiteralICE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal21StoreInArrayLiteralICD1Ev
	.quad	_ZN2v88internal21StoreInArrayLiteralICD0Ev
	.quad	_ZNK2v88internal21StoreInArrayLiteralIC9slow_stubEv
	.hidden	_ZTCN2v88internal12StdoutStreamE0_So
	.weak	_ZTCN2v88internal12StdoutStreamE0_So
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_So,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_So, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_So, 80
_ZTCN2v88internal12StdoutStreamE0_So:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.hidden	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.weak	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE
	.section	.rodata._ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE,"aG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, @object
	.size	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE, 80
_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	-80
	.quad	-80
	.quad	0
	.quad	0
	.quad	0
	.weak	_ZTTN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTTN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTTN2v88internal12StdoutStreamE, @object
	.size	_ZTTN2v88internal12StdoutStreamE, 48
_ZTTN2v88internal12StdoutStreamE:
	.quad	_ZTVN2v88internal12StdoutStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+24
	.quad	_ZTCN2v88internal12StdoutStreamE0_So+64
	.quad	_ZTCN2v88internal12StdoutStreamE0_NS0_8OFStreamE+64
	.quad	_ZTVN2v88internal12StdoutStreamE+64
	.weak	_ZTVN2v88internal12StdoutStreamE
	.section	.data.rel.ro.local._ZTVN2v88internal12StdoutStreamE,"awG",@progbits,_ZTVN2v88internal12StdoutStreamE,comdat
	.align 8
	.type	_ZTVN2v88internal12StdoutStreamE, @object
	.size	_ZTVN2v88internal12StdoutStreamE, 80
_ZTVN2v88internal12StdoutStreamE:
	.quad	80
	.quad	0
	.quad	0
	.quad	_ZN2v88internal12StdoutStreamD1Ev
	.quad	_ZN2v88internal12StdoutStreamD0Ev
	.quad	-80
	.quad	-80
	.quad	0
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD1Ev
	.quad	_ZTv0_n24_N2v88internal12StdoutStreamD0Ev
	.section	.bss._ZZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2822,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2822, @object
	.size	_ZZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2822, 8
_ZZN2v88internalL39Stats_Runtime_HasElementWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2822:
	.zero	8
	.section	.bss._ZZN2v88internalL29Stats_Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2802,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL29Stats_Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2802, @object
	.size	_ZZN2v88internalL29Stats_Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2802, 8
_ZZN2v88internalL29Stats_Runtime_KeyedHasIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2802:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2776,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2776, @object
	.size	_ZZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2776, 8
_ZZN2v88internalL40Stats_Runtime_LoadElementWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2776:
	.zero	8
	.section	.bss._ZZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2729,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2729, @object
	.size	_ZZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2729, 8
_ZZN2v88internalL42Stats_Runtime_StorePropertyWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2729:
	.zero	8
	.section	.bss._ZZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2680,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2680, @object
	.size	_ZZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2680, 8
_ZZN2v88internalL41Stats_Runtime_LoadPropertyWithInterceptorEiPmPNS0_7IsolateEE29trace_event_unique_atomic2680:
	.zero	8
	.section	.bss._ZZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateEE29trace_event_unique_atomic2653,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateEE29trace_event_unique_atomic2653, @object
	.size	_ZZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateEE29trace_event_unique_atomic2653, 8
_ZZN2v88internalL35Stats_Runtime_StoreCallbackPropertyEiPmPNS0_7IsolateEE29trace_event_unique_atomic2653:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2623,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2623, @object
	.size	_ZZN2v88internalL32Stats_Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2623, 8
_ZZN2v88internalL32Stats_Runtime_CloneObjectIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2623:
	.zero	8
	.section	.bss._ZZN2v88internalL47Stats_Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2496,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL47Stats_Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2496, @object
	.size	_ZZN2v88internalL47Stats_Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2496, 8
_ZZN2v88internalL47Stats_Runtime_ElementsTransitionAndStoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2496:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2485,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2485, @object
	.size	_ZZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2485, 8
_ZZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2485:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2473,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2473, @object
	.size	_ZZN2v88internalL31Stats_Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2473, 8
_ZZN2v88internalL31Stats_Runtime_KeyedStoreIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2473:
	.zero	8
	.section	.bss._ZZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2451,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2451, @object
	.size	_ZZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2451, 8
_ZZN2v88internalL40Stats_Runtime_StoreInArrayLiteralIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2451:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2409,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2409, @object
	.size	_ZZN2v88internalL31Stats_Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2409, 8
_ZZN2v88internalL31Stats_Runtime_KeyedStoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2409:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2358,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2358, @object
	.size	_ZZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2358, 8
_ZZN2v88internalL32Stats_Runtime_StoreGlobalIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2358:
	.zero	8
	.section	.bss._ZZN2v88internalL42Stats_Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2343,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Stats_Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2343, @object
	.size	_ZZN2v88internalL42Stats_Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2343, 8
_ZZN2v88internalL42Stats_Runtime_StoreGlobalICNoFeedback_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2343:
	.zero	8
	.section	.bss._ZZN2v88internalL32Stats_Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2326,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Stats_Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2326, @object
	.size	_ZZN2v88internalL32Stats_Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2326, 8
_ZZN2v88internalL32Stats_Runtime_StoreGlobalIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2326:
	.zero	8
	.section	.bss._ZZN2v88internalL26Stats_Runtime_StoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2296,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL26Stats_Runtime_StoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2296, @object
	.size	_ZZN2v88internalL26Stats_Runtime_StoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2296, 8
_ZZN2v88internalL26Stats_Runtime_StoreIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2296:
	.zero	8
	.section	.bss._ZZN2v88internalL30Stats_Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2276,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Stats_Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2276, @object
	.size	_ZZN2v88internalL30Stats_Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2276, 8
_ZZN2v88internalL30Stats_Runtime_KeyedLoadIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2276:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2232,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2232, @object
	.size	_ZZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2232, 8
_ZZN2v88internalL31Stats_Runtime_LoadGlobalIC_SlowEiPmPNS0_7IsolateEE29trace_event_unique_atomic2232:
	.zero	8
	.section	.bss._ZZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2203,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2203, @object
	.size	_ZZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2203, 8
_ZZN2v88internalL31Stats_Runtime_LoadGlobalIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2203:
	.zero	8
	.section	.bss._ZZN2v88internalL25Stats_Runtime_LoadIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2158,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL25Stats_Runtime_LoadIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2158, @object
	.size	_ZZN2v88internalL25Stats_Runtime_LoadIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2158, 8
_ZZN2v88internalL25Stats_Runtime_LoadIC_MissEiPmPNS0_7IsolateEE29trace_event_unique_atomic2158:
	.zero	8
	.weak	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68
	.section	.bss._ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68,"awG",@nobits,_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68,comdat
	.align 8
	.type	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68, @gnu_unique_object
	.size	_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68, 8
_ZZN2v88internal21ExternalCallbackScopeD4EvE27trace_event_unique_atomic68:
	.zero	8
	.weak	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62
	.section	.bss._ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62,"awG",@nobits,_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62,comdat
	.align 8
	.type	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62, @gnu_unique_object
	.size	_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62, 8
_ZZN2v88internal21ExternalCallbackScopeC4EPNS0_7IsolateEmE27trace_event_unique_atomic62:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1138753536
	.align 8
.LC1:
	.long	4290772992
	.long	1105199103
	.align 8
.LC2:
	.long	0
	.long	-1042284544
	.align 8
.LC92:
	.long	0
	.long	1127219200
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
