	.file	"frames.cc"
	.text
	.section	.text._ZNK2v88internal10StackFrame11is_standardEv,"axG",@progbits,_ZNK2v88internal10StackFrame11is_standardEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10StackFrame11is_standardEv
	.type	_ZNK2v88internal10StackFrame11is_standardEv, @function
_ZNK2v88internal10StackFrame11is_standardEv:
.LFB6244:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6244:
	.size	_ZNK2v88internal10StackFrame11is_standardEv, .-_ZNK2v88internal10StackFrame11is_standardEv
	.section	.text._ZNK2v88internal11NativeFrame4typeEv,"axG",@progbits,_ZNK2v88internal11NativeFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal11NativeFrame4typeEv
	.type	_ZNK2v88internal11NativeFrame4typeEv, @function
_ZNK2v88internal11NativeFrame4typeEv:
.LFB6259:
	.cfi_startproc
	endbr64
	movl	$22, %eax
	ret
	.cfi_endproc
.LFE6259:
	.size	_ZNK2v88internal11NativeFrame4typeEv, .-_ZNK2v88internal11NativeFrame4typeEv
	.section	.text._ZNK2v88internal11NativeFrame7IterateEPNS0_11RootVisitorE,"axG",@progbits,_ZNK2v88internal11NativeFrame7IterateEPNS0_11RootVisitorE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal11NativeFrame7IterateEPNS0_11RootVisitorE
	.type	_ZNK2v88internal11NativeFrame7IterateEPNS0_11RootVisitorE, @function
_ZNK2v88internal11NativeFrame7IterateEPNS0_11RootVisitorE:
.LFB6260:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6260:
	.size	_ZNK2v88internal11NativeFrame7IterateEPNS0_11RootVisitorE, .-_ZNK2v88internal11NativeFrame7IterateEPNS0_11RootVisitorE
	.section	.text._ZNK2v88internal10EntryFrame4typeEv,"axG",@progbits,_ZNK2v88internal10EntryFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10EntryFrame4typeEv
	.type	_ZNK2v88internal10EntryFrame4typeEv, @function
_ZNK2v88internal10EntryFrame4typeEv:
.LFB6261:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE6261:
	.size	_ZNK2v88internal10EntryFrame4typeEv, .-_ZNK2v88internal10EntryFrame4typeEv
	.section	.text._ZNK2v88internal10EntryFrame21GetCallerStackPointerEv,"axG",@progbits,_ZNK2v88internal10EntryFrame21GetCallerStackPointerEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal10EntryFrame21GetCallerStackPointerEv
	.type	_ZNK2v88internal10EntryFrame21GetCallerStackPointerEv, @function
_ZNK2v88internal10EntryFrame21GetCallerStackPointerEv:
.LFB6263:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6263:
	.size	_ZNK2v88internal10EntryFrame21GetCallerStackPointerEv, .-_ZNK2v88internal10EntryFrame21GetCallerStackPointerEv
	.section	.text._ZNK2v88internal19ConstructEntryFrame4typeEv,"axG",@progbits,_ZNK2v88internal19ConstructEntryFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal19ConstructEntryFrame4typeEv
	.type	_ZNK2v88internal19ConstructEntryFrame4typeEv, @function
_ZNK2v88internal19ConstructEntryFrame4typeEv:
.LFB6264:
	.cfi_startproc
	endbr64
	movl	$2, %eax
	ret
	.cfi_endproc
.LFE6264:
	.size	_ZNK2v88internal19ConstructEntryFrame4typeEv, .-_ZNK2v88internal19ConstructEntryFrame4typeEv
	.section	.text._ZNK2v88internal9ExitFrame4typeEv,"axG",@progbits,_ZNK2v88internal9ExitFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal9ExitFrame4typeEv
	.type	_ZNK2v88internal9ExitFrame4typeEv, @function
_ZNK2v88internal9ExitFrame4typeEv:
.LFB6266:
	.cfi_startproc
	endbr64
	movl	$3, %eax
	ret
	.cfi_endproc
.LFE6266:
	.size	_ZNK2v88internal9ExitFrame4typeEv, .-_ZNK2v88internal9ExitFrame4typeEv
	.section	.text._ZNK2v88internal16BuiltinExitFrame4typeEv,"axG",@progbits,_ZNK2v88internal16BuiltinExitFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal16BuiltinExitFrame4typeEv
	.type	_ZNK2v88internal16BuiltinExitFrame4typeEv, @function
_ZNK2v88internal16BuiltinExitFrame4typeEv:
.LFB6268:
	.cfi_startproc
	endbr64
	movl	$21, %eax
	ret
	.cfi_endproc
.LFE6268:
	.size	_ZNK2v88internal16BuiltinExitFrame4typeEv, .-_ZNK2v88internal16BuiltinExitFrame4typeEv
	.section	.text._ZNK2v88internal13StandardFrame11is_standardEv,"axG",@progbits,_ZNK2v88internal13StandardFrame11is_standardEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal13StandardFrame11is_standardEv
	.type	_ZNK2v88internal13StandardFrame11is_standardEv, @function
_ZNK2v88internal13StandardFrame11is_standardEv:
.LFB6308:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE6308:
	.size	_ZNK2v88internal13StandardFrame11is_standardEv, .-_ZNK2v88internal13StandardFrame11is_standardEv
	.section	.text._ZNK2v88internal15JavaScriptFrame14PrintFrameKindEPNS0_12StringStreamE,"axG",@progbits,_ZNK2v88internal15JavaScriptFrame14PrintFrameKindEPNS0_12StringStreamE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal15JavaScriptFrame14PrintFrameKindEPNS0_12StringStreamE
	.type	_ZNK2v88internal15JavaScriptFrame14PrintFrameKindEPNS0_12StringStreamE, @function
_ZNK2v88internal15JavaScriptFrame14PrintFrameKindEPNS0_12StringStreamE:
.LFB6311:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6311:
	.size	_ZNK2v88internal15JavaScriptFrame14PrintFrameKindEPNS0_12StringStreamE, .-_ZNK2v88internal15JavaScriptFrame14PrintFrameKindEPNS0_12StringStreamE
	.section	.text._ZNK2v88internal9StubFrame4typeEv,"axG",@progbits,_ZNK2v88internal9StubFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal9StubFrame4typeEv
	.type	_ZNK2v88internal9StubFrame4typeEv, @function
_ZNK2v88internal9StubFrame4typeEv:
.LFB6312:
	.cfi_startproc
	endbr64
	movl	$13, %eax
	ret
	.cfi_endproc
.LFE6312:
	.size	_ZNK2v88internal9StubFrame4typeEv, .-_ZNK2v88internal9StubFrame4typeEv
	.section	.text._ZNK2v88internal14OptimizedFrame4typeEv,"axG",@progbits,_ZNK2v88internal14OptimizedFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal14OptimizedFrame4typeEv
	.type	_ZNK2v88internal14OptimizedFrame4typeEv, @function
_ZNK2v88internal14OptimizedFrame4typeEv:
.LFB6313:
	.cfi_startproc
	endbr64
	movl	$4, %eax
	ret
	.cfi_endproc
.LFE6313:
	.size	_ZNK2v88internal14OptimizedFrame4typeEv, .-_ZNK2v88internal14OptimizedFrame4typeEv
	.section	.text._ZNK2v88internal16InterpretedFrame4typeEv,"axG",@progbits,_ZNK2v88internal16InterpretedFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal16InterpretedFrame4typeEv
	.type	_ZNK2v88internal16InterpretedFrame4typeEv, @function
_ZNK2v88internal16InterpretedFrame4typeEv:
.LFB6314:
	.cfi_startproc
	endbr64
	movl	$12, %eax
	ret
	.cfi_endproc
.LFE6314:
	.size	_ZNK2v88internal16InterpretedFrame4typeEv, .-_ZNK2v88internal16InterpretedFrame4typeEv
	.section	.text._ZNK2v88internal21ArgumentsAdaptorFrame4typeEv,"axG",@progbits,_ZNK2v88internal21ArgumentsAdaptorFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal21ArgumentsAdaptorFrame4typeEv
	.type	_ZNK2v88internal21ArgumentsAdaptorFrame4typeEv, @function
_ZNK2v88internal21ArgumentsAdaptorFrame4typeEv:
.LFB6316:
	.cfi_startproc
	endbr64
	movl	$19, %eax
	ret
	.cfi_endproc
.LFE6316:
	.size	_ZNK2v88internal21ArgumentsAdaptorFrame4typeEv, .-_ZNK2v88internal21ArgumentsAdaptorFrame4typeEv
	.section	.text._ZNK2v88internal12BuiltinFrame4typeEv,"axG",@progbits,_ZNK2v88internal12BuiltinFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal12BuiltinFrame4typeEv
	.type	_ZNK2v88internal12BuiltinFrame4typeEv, @function
_ZNK2v88internal12BuiltinFrame4typeEv:
.LFB6318:
	.cfi_startproc
	endbr64
	movl	$20, %eax
	ret
	.cfi_endproc
.LFE6318:
	.size	_ZNK2v88internal12BuiltinFrame4typeEv, .-_ZNK2v88internal12BuiltinFrame4typeEv
	.section	.text._ZNK2v88internal17WasmCompiledFrame4typeEv,"axG",@progbits,_ZNK2v88internal17WasmCompiledFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal17WasmCompiledFrame4typeEv
	.type	_ZNK2v88internal17WasmCompiledFrame4typeEv, @function
_ZNK2v88internal17WasmCompiledFrame4typeEv:
.LFB6320:
	.cfi_startproc
	endbr64
	movl	$5, %eax
	ret
	.cfi_endproc
.LFE6320:
	.size	_ZNK2v88internal17WasmCompiledFrame4typeEv, .-_ZNK2v88internal17WasmCompiledFrame4typeEv
	.section	.text._ZNK2v88internal13WasmExitFrame4typeEv,"axG",@progbits,_ZNK2v88internal13WasmExitFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal13WasmExitFrame4typeEv
	.type	_ZNK2v88internal13WasmExitFrame4typeEv, @function
_ZNK2v88internal13WasmExitFrame4typeEv:
.LFB6322:
	.cfi_startproc
	endbr64
	movl	$10, %eax
	ret
	.cfi_endproc
.LFE6322:
	.size	_ZNK2v88internal13WasmExitFrame4typeEv, .-_ZNK2v88internal13WasmExitFrame4typeEv
	.section	.text._ZNK2v88internal25WasmInterpreterEntryFrame4typeEv,"axG",@progbits,_ZNK2v88internal25WasmInterpreterEntryFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal25WasmInterpreterEntryFrame4typeEv
	.type	_ZNK2v88internal25WasmInterpreterEntryFrame4typeEv, @function
_ZNK2v88internal25WasmInterpreterEntryFrame4typeEv:
.LFB6323:
	.cfi_startproc
	endbr64
	movl	$8, %eax
	ret
	.cfi_endproc
.LFE6323:
	.size	_ZNK2v88internal25WasmInterpreterEntryFrame4typeEv, .-_ZNK2v88internal25WasmInterpreterEntryFrame4typeEv
	.section	.text._ZNK2v88internal13WasmToJsFrame4typeEv,"axG",@progbits,_ZNK2v88internal13WasmToJsFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal13WasmToJsFrame4typeEv
	.type	_ZNK2v88internal13WasmToJsFrame4typeEv, @function
_ZNK2v88internal13WasmToJsFrame4typeEv:
.LFB6325:
	.cfi_startproc
	endbr64
	movl	$6, %eax
	ret
	.cfi_endproc
.LFE6325:
	.size	_ZNK2v88internal13WasmToJsFrame4typeEv, .-_ZNK2v88internal13WasmToJsFrame4typeEv
	.section	.text._ZNK2v88internal13JsToWasmFrame4typeEv,"axG",@progbits,_ZNK2v88internal13JsToWasmFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal13JsToWasmFrame4typeEv
	.type	_ZNK2v88internal13JsToWasmFrame4typeEv, @function
_ZNK2v88internal13JsToWasmFrame4typeEv:
.LFB6326:
	.cfi_startproc
	endbr64
	movl	$7, %eax
	ret
	.cfi_endproc
.LFE6326:
	.size	_ZNK2v88internal13JsToWasmFrame4typeEv, .-_ZNK2v88internal13JsToWasmFrame4typeEv
	.section	.text._ZNK2v88internal15CWasmEntryFrame4typeEv,"axG",@progbits,_ZNK2v88internal15CWasmEntryFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal15CWasmEntryFrame4typeEv
	.type	_ZNK2v88internal15CWasmEntryFrame4typeEv, @function
_ZNK2v88internal15CWasmEntryFrame4typeEv:
.LFB6327:
	.cfi_startproc
	endbr64
	movl	$9, %eax
	ret
	.cfi_endproc
.LFE6327:
	.size	_ZNK2v88internal15CWasmEntryFrame4typeEv, .-_ZNK2v88internal15CWasmEntryFrame4typeEv
	.section	.text._ZNK2v88internal20WasmCompileLazyFrame4typeEv,"axG",@progbits,_ZNK2v88internal20WasmCompileLazyFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal20WasmCompileLazyFrame4typeEv
	.type	_ZNK2v88internal20WasmCompileLazyFrame4typeEv, @function
_ZNK2v88internal20WasmCompileLazyFrame4typeEv:
.LFB6328:
	.cfi_startproc
	endbr64
	movl	$11, %eax
	ret
	.cfi_endproc
.LFE6328:
	.size	_ZNK2v88internal20WasmCompileLazyFrame4typeEv, .-_ZNK2v88internal20WasmCompileLazyFrame4typeEv
	.section	.text._ZNK2v88internal13InternalFrame4typeEv,"axG",@progbits,_ZNK2v88internal13InternalFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal13InternalFrame4typeEv
	.type	_ZNK2v88internal13InternalFrame4typeEv, @function
_ZNK2v88internal13InternalFrame4typeEv:
.LFB6330:
	.cfi_startproc
	endbr64
	movl	$17, %eax
	ret
	.cfi_endproc
.LFE6330:
	.size	_ZNK2v88internal13InternalFrame4typeEv, .-_ZNK2v88internal13InternalFrame4typeEv
	.section	.text._ZNK2v88internal14ConstructFrame4typeEv,"axG",@progbits,_ZNK2v88internal14ConstructFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal14ConstructFrame4typeEv
	.type	_ZNK2v88internal14ConstructFrame4typeEv, @function
_ZNK2v88internal14ConstructFrame4typeEv:
.LFB6332:
	.cfi_startproc
	endbr64
	movl	$18, %eax
	ret
	.cfi_endproc
.LFE6332:
	.size	_ZNK2v88internal14ConstructFrame4typeEv, .-_ZNK2v88internal14ConstructFrame4typeEv
	.section	.text._ZNK2v88internal24BuiltinContinuationFrame4typeEv,"axG",@progbits,_ZNK2v88internal24BuiltinContinuationFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal24BuiltinContinuationFrame4typeEv
	.type	_ZNK2v88internal24BuiltinContinuationFrame4typeEv, @function
_ZNK2v88internal24BuiltinContinuationFrame4typeEv:
.LFB6334:
	.cfi_startproc
	endbr64
	movl	$14, %eax
	ret
	.cfi_endproc
.LFE6334:
	.size	_ZNK2v88internal24BuiltinContinuationFrame4typeEv, .-_ZNK2v88internal24BuiltinContinuationFrame4typeEv
	.section	.text._ZNK2v88internal34JavaScriptBuiltinContinuationFrame4typeEv,"axG",@progbits,_ZNK2v88internal34JavaScriptBuiltinContinuationFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame4typeEv
	.type	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame4typeEv, @function
_ZNK2v88internal34JavaScriptBuiltinContinuationFrame4typeEv:
.LFB6336:
	.cfi_startproc
	endbr64
	movl	$15, %eax
	ret
	.cfi_endproc
.LFE6336:
	.size	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame4typeEv, .-_ZNK2v88internal34JavaScriptBuiltinContinuationFrame4typeEv
	.section	.text._ZNK2v88internal43JavaScriptBuiltinContinuationWithCatchFrame4typeEv,"axG",@progbits,_ZNK2v88internal43JavaScriptBuiltinContinuationWithCatchFrame4typeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal43JavaScriptBuiltinContinuationWithCatchFrame4typeEv
	.type	_ZNK2v88internal43JavaScriptBuiltinContinuationWithCatchFrame4typeEv, @function
_ZNK2v88internal43JavaScriptBuiltinContinuationWithCatchFrame4typeEv:
.LFB6338:
	.cfi_startproc
	endbr64
	movl	$16, %eax
	ret
	.cfi_endproc
.LFE6338:
	.size	_ZNK2v88internal43JavaScriptBuiltinContinuationWithCatchFrame4typeEv, .-_ZNK2v88internal43JavaScriptBuiltinContinuationWithCatchFrame4typeEv
	.section	.text._ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,"axG",@progbits,_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.type	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, @function
_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE:
.LFB8113:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	8(%rcx), %r8
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE8113:
	.size	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE, .-_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE
	.section	.text._ZNK2v88internal11NativeFrame21GetCallerStackPointerEv,"axG",@progbits,_ZNK2v88internal11NativeFrame21GetCallerStackPointerEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v88internal11NativeFrame21GetCallerStackPointerEv
	.type	_ZNK2v88internal11NativeFrame21GetCallerStackPointerEv, @function
_ZNK2v88internal11NativeFrame21GetCallerStackPointerEv:
.LFB21889:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	addq	$16, %rax
	ret
	.cfi_endproc
.LFE21889:
	.size	_ZNK2v88internal11NativeFrame21GetCallerStackPointerEv, .-_ZNK2v88internal11NativeFrame21GetCallerStackPointerEv
	.section	.text._ZN2v88internal10EntryFrameD2Ev,"axG",@progbits,_ZN2v88internal10EntryFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10EntryFrameD2Ev
	.type	_ZN2v88internal10EntryFrameD2Ev, @function
_ZN2v88internal10EntryFrameD2Ev:
.LFB21895:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21895:
	.size	_ZN2v88internal10EntryFrameD2Ev, .-_ZN2v88internal10EntryFrameD2Ev
	.weak	_ZN2v88internal10EntryFrameD1Ev
	.set	_ZN2v88internal10EntryFrameD1Ev,_ZN2v88internal10EntryFrameD2Ev
	.section	.text._ZN2v88internal9ExitFrameD2Ev,"axG",@progbits,_ZN2v88internal9ExitFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9ExitFrameD2Ev
	.type	_ZN2v88internal9ExitFrameD2Ev, @function
_ZN2v88internal9ExitFrameD2Ev:
.LFB21905:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21905:
	.size	_ZN2v88internal9ExitFrameD2Ev, .-_ZN2v88internal9ExitFrameD2Ev
	.weak	_ZN2v88internal9ExitFrameD1Ev
	.set	_ZN2v88internal9ExitFrameD1Ev,_ZN2v88internal9ExitFrameD2Ev
	.section	.text._ZN2v88internal17WasmCompiledFrameD2Ev,"axG",@progbits,_ZN2v88internal17WasmCompiledFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17WasmCompiledFrameD2Ev
	.type	_ZN2v88internal17WasmCompiledFrameD2Ev, @function
_ZN2v88internal17WasmCompiledFrameD2Ev:
.LFB21960:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21960:
	.size	_ZN2v88internal17WasmCompiledFrameD2Ev, .-_ZN2v88internal17WasmCompiledFrameD2Ev
	.weak	_ZN2v88internal17WasmCompiledFrameD1Ev
	.set	_ZN2v88internal17WasmCompiledFrameD1Ev,_ZN2v88internal17WasmCompiledFrameD2Ev
	.section	.text._ZN2v88internal9StubFrameD2Ev,"axG",@progbits,_ZN2v88internal9StubFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9StubFrameD2Ev
	.type	_ZN2v88internal9StubFrameD2Ev, @function
_ZN2v88internal9StubFrameD2Ev:
.LFB21970:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21970:
	.size	_ZN2v88internal9StubFrameD2Ev, .-_ZN2v88internal9StubFrameD2Ev
	.weak	_ZN2v88internal9StubFrameD1Ev
	.set	_ZN2v88internal9StubFrameD1Ev,_ZN2v88internal9StubFrameD2Ev
	.section	.text._ZN2v88internal13InternalFrameD2Ev,"axG",@progbits,_ZN2v88internal13InternalFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13InternalFrameD2Ev
	.type	_ZN2v88internal13InternalFrameD2Ev, @function
_ZN2v88internal13InternalFrameD2Ev:
.LFB21989:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE21989:
	.size	_ZN2v88internal13InternalFrameD2Ev, .-_ZN2v88internal13InternalFrameD2Ev
	.weak	_ZN2v88internal13InternalFrameD1Ev
	.set	_ZN2v88internal13InternalFrameD1Ev,_ZN2v88internal13InternalFrameD2Ev
	.section	.text._ZN2v88internal34JavaScriptBuiltinContinuationFrameD2Ev,"axG",@progbits,_ZN2v88internal34JavaScriptBuiltinContinuationFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal34JavaScriptBuiltinContinuationFrameD2Ev
	.type	_ZN2v88internal34JavaScriptBuiltinContinuationFrameD2Ev, @function
_ZN2v88internal34JavaScriptBuiltinContinuationFrameD2Ev:
.LFB22002:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE22002:
	.size	_ZN2v88internal34JavaScriptBuiltinContinuationFrameD2Ev, .-_ZN2v88internal34JavaScriptBuiltinContinuationFrameD2Ev
	.weak	_ZN2v88internal34JavaScriptBuiltinContinuationFrameD1Ev
	.set	_ZN2v88internal34JavaScriptBuiltinContinuationFrameD1Ev,_ZN2v88internal34JavaScriptBuiltinContinuationFrameD2Ev
	.section	.text._ZNK2v88internal11NativeFrame14unchecked_codeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11NativeFrame14unchecked_codeEv
	.type	_ZNK2v88internal11NativeFrame14unchecked_codeEv, @function
_ZNK2v88internal11NativeFrame14unchecked_codeEv:
.LFB23206:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23206:
	.size	_ZNK2v88internal11NativeFrame14unchecked_codeEv, .-_ZNK2v88internal11NativeFrame14unchecked_codeEv
	.globl	_ZNK2v88internal13InternalFrame14unchecked_codeEv
	.set	_ZNK2v88internal13InternalFrame14unchecked_codeEv,_ZNK2v88internal11NativeFrame14unchecked_codeEv
	.globl	_ZNK2v88internal20WasmCompileLazyFrame14unchecked_codeEv
	.set	_ZNK2v88internal20WasmCompileLazyFrame14unchecked_codeEv,_ZNK2v88internal11NativeFrame14unchecked_codeEv
	.globl	_ZNK2v88internal25WasmInterpreterEntryFrame14unchecked_codeEv
	.set	_ZNK2v88internal25WasmInterpreterEntryFrame14unchecked_codeEv,_ZNK2v88internal11NativeFrame14unchecked_codeEv
	.globl	_ZNK2v88internal9ExitFrame14unchecked_codeEv
	.set	_ZNK2v88internal9ExitFrame14unchecked_codeEv,_ZNK2v88internal11NativeFrame14unchecked_codeEv
	.section	.text._ZNK2v88internal9ExitFrame21GetCallerStackPointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ExitFrame21GetCallerStackPointerEv
	.type	_ZNK2v88internal9ExitFrame21GetCallerStackPointerEv, @function
_ZNK2v88internal9ExitFrame21GetCallerStackPointerEv:
.LFB23216:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	addq	$16, %rax
	ret
	.cfi_endproc
.LFE23216:
	.size	_ZNK2v88internal9ExitFrame21GetCallerStackPointerEv, .-_ZNK2v88internal9ExitFrame21GetCallerStackPointerEv
	.section	.text._ZNK2v88internal13StandardFrame20GetExpressionAddressEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.type	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi, @function
_ZNK2v88internal13StandardFrame20GetExpressionAddressEi:
.LFB23231:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	sall	$3, %esi
	movslq	%esi, %rsi
	subq	%rsi, %rax
	subq	$24, %rax
	ret
	.cfi_endproc
.LFE23231:
	.size	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi, .-_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.section	.text._ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi
	.type	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi, @function
_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi:
.LFB23232:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	sall	$3, %esi
	movslq	%esi, %rsi
	subq	%rsi, %rax
	subq	$40, %rax
	ret
	.cfi_endproc
.LFE23232:
	.size	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi, .-_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi
	.section	.text._ZNK2v88internal13StandardFrame8receiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13StandardFrame8receiverEv
	.type	_ZNK2v88internal13StandardFrame8receiverEv, @function
_ZNK2v88internal13StandardFrame8receiverEv:
.LFB23234:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	88(%rax), %rax
	ret
	.cfi_endproc
.LFE23234:
	.size	_ZNK2v88internal13StandardFrame8receiverEv, .-_ZNK2v88internal13StandardFrame8receiverEv
	.globl	_ZNK2v88internal13StandardFrame7contextEv
	.set	_ZNK2v88internal13StandardFrame7contextEv,_ZNK2v88internal13StandardFrame8receiverEv
	.section	.text._ZNK2v88internal13StandardFrame22ComputeParametersCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.type	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv, @function
_ZNK2v88internal13StandardFrame22ComputeParametersCountEv:
.LFB23239:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23239:
	.size	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv, .-_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.section	.text._ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.type	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE, @function
_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE:
.LFB23240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	call	*56(%rax)
	movq	%rax, (%r12)
	movq	32(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%r12)
	movq	32(%rbx), %rax
	leaq	8(%rax), %rdi
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	testq	%rax, %rax
	je	.L47
	call	*%rax
.L45:
	movq	%rax, 16(%r12)
	movq	40(%rbx), %rax
	movq	%rax, 24(%r12)
	movq	32(%rbx), %rax
	popq	%rbx
	movq	%rax, 32(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	%rdi, %rax
	jmp	.L45
	.cfi_endproc
.LFE23240:
	.size	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE, .-_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.section	.text._ZNK2v88internal13StandardFrame13IsConstructorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13StandardFrame13IsConstructorEv
	.type	_ZNK2v88internal13StandardFrame13IsConstructorEv, @function
_ZNK2v88internal13StandardFrame13IsConstructorEv:
.LFB23241:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23241:
	.size	_ZNK2v88internal13StandardFrame13IsConstructorEv, .-_ZNK2v88internal13StandardFrame13IsConstructorEv
	.section	.text._ZNK2v88internal9StubFrame21GetCallerStackPointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9StubFrame21GetCallerStackPointerEv
	.type	_ZNK2v88internal9StubFrame21GetCallerStackPointerEv, @function
_ZNK2v88internal9StubFrame21GetCallerStackPointerEv:
.LFB23246:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	addq	$16, %rax
	ret
	.cfi_endproc
.LFE23246:
	.size	_ZNK2v88internal9StubFrame21GetCallerStackPointerEv, .-_ZNK2v88internal9StubFrame21GetCallerStackPointerEv
	.section	.text._ZNK2v88internal15JavaScriptFrame13IsConstructorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv
	.type	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv, @function
_ZNK2v88internal15JavaScriptFrame13IsConstructorEv:
.LFB23250:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	(%rax), %rdx
	movq	-8(%rdx), %rax
	cmpq	$38, %rax
	jne	.L51
	movq	(%rdx), %rax
	movq	-8(%rax), %rax
.L51:
	cmpq	$36, %rax
	sete	%al
	ret
	.cfi_endproc
.LFE23250:
	.size	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv, .-_ZNK2v88internal15JavaScriptFrame13IsConstructorEv
	.section	.text._ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv
	.type	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv, @function
_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv:
.LFB23263:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	addq	$16, %rax
	ret
	.cfi_endproc
.LFE23263:
	.size	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv, .-_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv
	.section	.text._ZNK2v88internal15JavaScriptFrame8functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame8functionEv
	.type	_ZNK2v88internal15JavaScriptFrame8functionEv, @function
_ZNK2v88internal15JavaScriptFrame8functionEv:
.LFB23267:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	ret
	.cfi_endproc
.LFE23267:
	.size	_ZNK2v88internal15JavaScriptFrame8functionEv, .-_ZNK2v88internal15JavaScriptFrame8functionEv
	.section	.text._ZNK2v88internal15JavaScriptFrame7contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame7contextEv
	.type	_ZNK2v88internal15JavaScriptFrame7contextEv, @function
_ZNK2v88internal15JavaScriptFrame7contextEv:
.LFB23270:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	-8(%rax), %rax
	ret
	.cfi_endproc
.LFE23270:
	.size	_ZNK2v88internal15JavaScriptFrame7contextEv, .-_ZNK2v88internal15JavaScriptFrame7contextEv
	.section	.text._ZN2v88internal15JavaScriptFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JavaScriptFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE
	.type	_ZN2v88internal15JavaScriptFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE, @function
_ZN2v88internal15JavaScriptFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE:
.LFB23272:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE23272:
	.size	_ZN2v88internal15JavaScriptFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE, .-_ZN2v88internal15JavaScriptFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE
	.section	.text._ZNK2v88internal34JavaScriptBuiltinContinuationFrame22ComputeParametersCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame22ComputeParametersCountEv
	.type	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame22ComputeParametersCountEv, @function
_ZNK2v88internal34JavaScriptBuiltinContinuationFrame22ComputeParametersCountEv:
.LFB23282:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movslq	-44(%rax), %rax
	ret
	.cfi_endproc
.LFE23282:
	.size	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame22ComputeParametersCountEv, .-_ZNK2v88internal34JavaScriptBuiltinContinuationFrame22ComputeParametersCountEv
	.section	.text._ZNK2v88internal34JavaScriptBuiltinContinuationFrame7contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame7contextEv
	.type	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame7contextEv, @function
_ZNK2v88internal34JavaScriptBuiltinContinuationFrame7contextEv:
.LFB23284:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	-32(%rax), %rax
	ret
	.cfi_endproc
.LFE23284:
	.size	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame7contextEv, .-_ZNK2v88internal34JavaScriptBuiltinContinuationFrame7contextEv
	.section	.text._ZNK2v88internal13InternalFrame21GetCallerStackPointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13InternalFrame21GetCallerStackPointerEv
	.type	_ZNK2v88internal13InternalFrame21GetCallerStackPointerEv, @function
_ZNK2v88internal13InternalFrame21GetCallerStackPointerEv:
.LFB23367:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	addq	$16, %rax
	ret
	.cfi_endproc
.LFE23367:
	.size	_ZNK2v88internal13InternalFrame21GetCallerStackPointerEv, .-_ZNK2v88internal13InternalFrame21GetCallerStackPointerEv
	.section	.text._ZNK2v88internal17WasmCompiledFrame21GetCallerStackPointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17WasmCompiledFrame21GetCallerStackPointerEv
	.type	_ZNK2v88internal17WasmCompiledFrame21GetCallerStackPointerEv, @function
_ZNK2v88internal17WasmCompiledFrame21GetCallerStackPointerEv:
.LFB23372:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	addq	$16, %rax
	ret
	.cfi_endproc
.LFE23372:
	.size	_ZNK2v88internal17WasmCompiledFrame21GetCallerStackPointerEv, .-_ZNK2v88internal17WasmCompiledFrame21GetCallerStackPointerEv
	.section	.text._ZNK2v88internal17WasmCompiledFrame6scriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17WasmCompiledFrame6scriptEv
	.type	_ZNK2v88internal17WasmCompiledFrame6scriptEv, @function
_ZNK2v88internal17WasmCompiledFrame6scriptEv:
.LFB23377:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	movq	135(%rax), %rax
	movq	39(%rax), %rax
	ret
	.cfi_endproc
.LFE23377:
	.size	_ZNK2v88internal17WasmCompiledFrame6scriptEv, .-_ZNK2v88internal17WasmCompiledFrame6scriptEv
	.section	.text._ZNK2v88internal25WasmInterpreterEntryFrame6scriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25WasmInterpreterEntryFrame6scriptEv
	.type	_ZNK2v88internal25WasmInterpreterEntryFrame6scriptEv, @function
_ZNK2v88internal25WasmInterpreterEntryFrame6scriptEv:
.LFB23398:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	movq	135(%rax), %rax
	movq	39(%rax), %rax
	ret
	.cfi_endproc
.LFE23398:
	.size	_ZNK2v88internal25WasmInterpreterEntryFrame6scriptEv, .-_ZNK2v88internal25WasmInterpreterEntryFrame6scriptEv
	.section	.text._ZNK2v88internal25WasmInterpreterEntryFrame7contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25WasmInterpreterEntryFrame7contextEv
	.type	_ZNK2v88internal25WasmInterpreterEntryFrame7contextEv, @function
_ZNK2v88internal25WasmInterpreterEntryFrame7contextEv:
.LFB23400:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	movq	151(%rax), %rax
	ret
	.cfi_endproc
.LFE23400:
	.size	_ZNK2v88internal25WasmInterpreterEntryFrame7contextEv, .-_ZNK2v88internal25WasmInterpreterEntryFrame7contextEv
	.section	.text._ZNK2v88internal25WasmInterpreterEntryFrame21GetCallerStackPointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25WasmInterpreterEntryFrame21GetCallerStackPointerEv
	.type	_ZNK2v88internal25WasmInterpreterEntryFrame21GetCallerStackPointerEv, @function
_ZNK2v88internal25WasmInterpreterEntryFrame21GetCallerStackPointerEv:
.LFB23401:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	addq	$16, %rax
	ret
	.cfi_endproc
.LFE23401:
	.size	_ZNK2v88internal25WasmInterpreterEntryFrame21GetCallerStackPointerEv, .-_ZNK2v88internal25WasmInterpreterEntryFrame21GetCallerStackPointerEv
	.section	.text._ZNK2v88internal20WasmCompileLazyFrame21GetCallerStackPointerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20WasmCompileLazyFrame21GetCallerStackPointerEv
	.type	_ZNK2v88internal20WasmCompileLazyFrame21GetCallerStackPointerEv, @function
_ZNK2v88internal20WasmCompileLazyFrame21GetCallerStackPointerEv:
.LFB23406:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	addq	$16, %rax
	ret
	.cfi_endproc
.LFE23406:
	.size	_ZNK2v88internal20WasmCompileLazyFrame21GetCallerStackPointerEv, .-_ZNK2v88internal20WasmCompileLazyFrame21GetCallerStackPointerEv
	.section	.text._ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data:
.LFB28285:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE28285:
	.size	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB28286:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L67
	cmpl	$3, %edx
	je	.L68
	cmpl	$1, %edx
	je	.L72
.L68:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE28286:
	.size	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZN2v88internal20WasmCompileLazyFrameD2Ev,"axG",@progbits,_ZN2v88internal20WasmCompileLazyFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20WasmCompileLazyFrameD2Ev
	.type	_ZN2v88internal20WasmCompileLazyFrameD2Ev, @function
_ZN2v88internal20WasmCompileLazyFrameD2Ev:
.LFB28464:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28464:
	.size	_ZN2v88internal20WasmCompileLazyFrameD2Ev, .-_ZN2v88internal20WasmCompileLazyFrameD2Ev
	.weak	_ZN2v88internal20WasmCompileLazyFrameD1Ev
	.set	_ZN2v88internal20WasmCompileLazyFrameD1Ev,_ZN2v88internal20WasmCompileLazyFrameD2Ev
	.section	.text._ZN2v88internal25WasmInterpreterEntryFrameD2Ev,"axG",@progbits,_ZN2v88internal25WasmInterpreterEntryFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25WasmInterpreterEntryFrameD2Ev
	.type	_ZN2v88internal25WasmInterpreterEntryFrameD2Ev, @function
_ZN2v88internal25WasmInterpreterEntryFrameD2Ev:
.LFB28468:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28468:
	.size	_ZN2v88internal25WasmInterpreterEntryFrameD2Ev, .-_ZN2v88internal25WasmInterpreterEntryFrameD2Ev
	.weak	_ZN2v88internal25WasmInterpreterEntryFrameD1Ev
	.set	_ZN2v88internal25WasmInterpreterEntryFrameD1Ev,_ZN2v88internal25WasmInterpreterEntryFrameD2Ev
	.section	.text._ZN2v88internal12BuiltinFrameD2Ev,"axG",@progbits,_ZN2v88internal12BuiltinFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12BuiltinFrameD2Ev
	.type	_ZN2v88internal12BuiltinFrameD2Ev, @function
_ZN2v88internal12BuiltinFrameD2Ev:
.LFB28472:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28472:
	.size	_ZN2v88internal12BuiltinFrameD2Ev, .-_ZN2v88internal12BuiltinFrameD2Ev
	.weak	_ZN2v88internal12BuiltinFrameD1Ev
	.set	_ZN2v88internal12BuiltinFrameD1Ev,_ZN2v88internal12BuiltinFrameD2Ev
	.section	.text._ZN2v88internal21ArgumentsAdaptorFrameD2Ev,"axG",@progbits,_ZN2v88internal21ArgumentsAdaptorFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21ArgumentsAdaptorFrameD2Ev
	.type	_ZN2v88internal21ArgumentsAdaptorFrameD2Ev, @function
_ZN2v88internal21ArgumentsAdaptorFrameD2Ev:
.LFB28476:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28476:
	.size	_ZN2v88internal21ArgumentsAdaptorFrameD2Ev, .-_ZN2v88internal21ArgumentsAdaptorFrameD2Ev
	.weak	_ZN2v88internal21ArgumentsAdaptorFrameD1Ev
	.set	_ZN2v88internal21ArgumentsAdaptorFrameD1Ev,_ZN2v88internal21ArgumentsAdaptorFrameD2Ev
	.section	.text._ZN2v88internal16InterpretedFrameD2Ev,"axG",@progbits,_ZN2v88internal16InterpretedFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16InterpretedFrameD2Ev
	.type	_ZN2v88internal16InterpretedFrameD2Ev, @function
_ZN2v88internal16InterpretedFrameD2Ev:
.LFB28480:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28480:
	.size	_ZN2v88internal16InterpretedFrameD2Ev, .-_ZN2v88internal16InterpretedFrameD2Ev
	.weak	_ZN2v88internal16InterpretedFrameD1Ev
	.set	_ZN2v88internal16InterpretedFrameD1Ev,_ZN2v88internal16InterpretedFrameD2Ev
	.section	.text._ZN2v88internal14OptimizedFrameD2Ev,"axG",@progbits,_ZN2v88internal14OptimizedFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14OptimizedFrameD2Ev
	.type	_ZN2v88internal14OptimizedFrameD2Ev, @function
_ZN2v88internal14OptimizedFrameD2Ev:
.LFB28484:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28484:
	.size	_ZN2v88internal14OptimizedFrameD2Ev, .-_ZN2v88internal14OptimizedFrameD2Ev
	.weak	_ZN2v88internal14OptimizedFrameD1Ev
	.set	_ZN2v88internal14OptimizedFrameD1Ev,_ZN2v88internal14OptimizedFrameD2Ev
	.section	.text._ZN2v88internal16BuiltinExitFrameD2Ev,"axG",@progbits,_ZN2v88internal16BuiltinExitFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16BuiltinExitFrameD2Ev
	.type	_ZN2v88internal16BuiltinExitFrameD2Ev, @function
_ZN2v88internal16BuiltinExitFrameD2Ev:
.LFB28488:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28488:
	.size	_ZN2v88internal16BuiltinExitFrameD2Ev, .-_ZN2v88internal16BuiltinExitFrameD2Ev
	.weak	_ZN2v88internal16BuiltinExitFrameD1Ev
	.set	_ZN2v88internal16BuiltinExitFrameD1Ev,_ZN2v88internal16BuiltinExitFrameD2Ev
	.section	.text._ZN2v88internal19ConstructEntryFrameD2Ev,"axG",@progbits,_ZN2v88internal19ConstructEntryFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19ConstructEntryFrameD2Ev
	.type	_ZN2v88internal19ConstructEntryFrameD2Ev, @function
_ZN2v88internal19ConstructEntryFrameD2Ev:
.LFB28492:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28492:
	.size	_ZN2v88internal19ConstructEntryFrameD2Ev, .-_ZN2v88internal19ConstructEntryFrameD2Ev
	.weak	_ZN2v88internal19ConstructEntryFrameD1Ev
	.set	_ZN2v88internal19ConstructEntryFrameD1Ev,_ZN2v88internal19ConstructEntryFrameD2Ev
	.section	.text._ZN2v88internal15CWasmEntryFrameD2Ev,"axG",@progbits,_ZN2v88internal15CWasmEntryFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CWasmEntryFrameD2Ev
	.type	_ZN2v88internal15CWasmEntryFrameD2Ev, @function
_ZN2v88internal15CWasmEntryFrameD2Ev:
.LFB28496:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28496:
	.size	_ZN2v88internal15CWasmEntryFrameD2Ev, .-_ZN2v88internal15CWasmEntryFrameD2Ev
	.weak	_ZN2v88internal15CWasmEntryFrameD1Ev
	.set	_ZN2v88internal15CWasmEntryFrameD1Ev,_ZN2v88internal15CWasmEntryFrameD2Ev
	.section	.text._ZN2v88internal11NativeFrameD2Ev,"axG",@progbits,_ZN2v88internal11NativeFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11NativeFrameD2Ev
	.type	_ZN2v88internal11NativeFrameD2Ev, @function
_ZN2v88internal11NativeFrameD2Ev:
.LFB28500:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE28500:
	.size	_ZN2v88internal11NativeFrameD2Ev, .-_ZN2v88internal11NativeFrameD2Ev
	.weak	_ZN2v88internal11NativeFrameD1Ev
	.set	_ZN2v88internal11NativeFrameD1Ev,_ZN2v88internal11NativeFrameD2Ev
	.section	.text._ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD2Ev,"axG",@progbits,_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD2Ev
	.type	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD2Ev, @function
_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD2Ev:
.LFB29292:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE29292:
	.size	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD2Ev, .-_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD2Ev
	.weak	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD1Ev
	.set	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD1Ev,_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD2Ev
	.section	.text._ZN2v88internal24BuiltinContinuationFrameD2Ev,"axG",@progbits,_ZN2v88internal24BuiltinContinuationFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24BuiltinContinuationFrameD2Ev
	.type	_ZN2v88internal24BuiltinContinuationFrameD2Ev, @function
_ZN2v88internal24BuiltinContinuationFrameD2Ev:
.LFB29296:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE29296:
	.size	_ZN2v88internal24BuiltinContinuationFrameD2Ev, .-_ZN2v88internal24BuiltinContinuationFrameD2Ev
	.weak	_ZN2v88internal24BuiltinContinuationFrameD1Ev
	.set	_ZN2v88internal24BuiltinContinuationFrameD1Ev,_ZN2v88internal24BuiltinContinuationFrameD2Ev
	.section	.text._ZN2v88internal14ConstructFrameD2Ev,"axG",@progbits,_ZN2v88internal14ConstructFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14ConstructFrameD2Ev
	.type	_ZN2v88internal14ConstructFrameD2Ev, @function
_ZN2v88internal14ConstructFrameD2Ev:
.LFB29300:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE29300:
	.size	_ZN2v88internal14ConstructFrameD2Ev, .-_ZN2v88internal14ConstructFrameD2Ev
	.weak	_ZN2v88internal14ConstructFrameD1Ev
	.set	_ZN2v88internal14ConstructFrameD1Ev,_ZN2v88internal14ConstructFrameD2Ev
	.section	.text._ZN2v88internal13JsToWasmFrameD2Ev,"axG",@progbits,_ZN2v88internal13JsToWasmFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13JsToWasmFrameD2Ev
	.type	_ZN2v88internal13JsToWasmFrameD2Ev, @function
_ZN2v88internal13JsToWasmFrameD2Ev:
.LFB29304:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE29304:
	.size	_ZN2v88internal13JsToWasmFrameD2Ev, .-_ZN2v88internal13JsToWasmFrameD2Ev
	.weak	_ZN2v88internal13JsToWasmFrameD1Ev
	.set	_ZN2v88internal13JsToWasmFrameD1Ev,_ZN2v88internal13JsToWasmFrameD2Ev
	.section	.text._ZN2v88internal13WasmToJsFrameD2Ev,"axG",@progbits,_ZN2v88internal13WasmToJsFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13WasmToJsFrameD2Ev
	.type	_ZN2v88internal13WasmToJsFrameD2Ev, @function
_ZN2v88internal13WasmToJsFrameD2Ev:
.LFB29308:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE29308:
	.size	_ZN2v88internal13WasmToJsFrameD2Ev, .-_ZN2v88internal13WasmToJsFrameD2Ev
	.weak	_ZN2v88internal13WasmToJsFrameD1Ev
	.set	_ZN2v88internal13WasmToJsFrameD1Ev,_ZN2v88internal13WasmToJsFrameD2Ev
	.section	.text._ZN2v88internal13WasmExitFrameD2Ev,"axG",@progbits,_ZN2v88internal13WasmExitFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13WasmExitFrameD2Ev
	.type	_ZN2v88internal13WasmExitFrameD2Ev, @function
_ZN2v88internal13WasmExitFrameD2Ev:
.LFB29312:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE29312:
	.size	_ZN2v88internal13WasmExitFrameD2Ev, .-_ZN2v88internal13WasmExitFrameD2Ev
	.weak	_ZN2v88internal13WasmExitFrameD1Ev
	.set	_ZN2v88internal13WasmExitFrameD1Ev,_ZN2v88internal13WasmExitFrameD2Ev
	.section	.text._ZN2v88internal11NativeFrameD0Ev,"axG",@progbits,_ZN2v88internal11NativeFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal11NativeFrameD0Ev
	.type	_ZN2v88internal11NativeFrameD0Ev, @function
_ZN2v88internal11NativeFrameD0Ev:
.LFB28502:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28502:
	.size	_ZN2v88internal11NativeFrameD0Ev, .-_ZN2v88internal11NativeFrameD0Ev
	.section	.text._ZN2v88internal9ExitFrameD0Ev,"axG",@progbits,_ZN2v88internal9ExitFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9ExitFrameD0Ev
	.type	_ZN2v88internal9ExitFrameD0Ev, @function
_ZN2v88internal9ExitFrameD0Ev:
.LFB21907:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21907:
	.size	_ZN2v88internal9ExitFrameD0Ev, .-_ZN2v88internal9ExitFrameD0Ev
	.section	.text._ZN2v88internal16BuiltinExitFrameD0Ev,"axG",@progbits,_ZN2v88internal16BuiltinExitFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16BuiltinExitFrameD0Ev
	.type	_ZN2v88internal16BuiltinExitFrameD0Ev, @function
_ZN2v88internal16BuiltinExitFrameD0Ev:
.LFB28490:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28490:
	.size	_ZN2v88internal16BuiltinExitFrameD0Ev, .-_ZN2v88internal16BuiltinExitFrameD0Ev
	.section	.text._ZN2v88internal10EntryFrameD0Ev,"axG",@progbits,_ZN2v88internal10EntryFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal10EntryFrameD0Ev
	.type	_ZN2v88internal10EntryFrameD0Ev, @function
_ZN2v88internal10EntryFrameD0Ev:
.LFB21897:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21897:
	.size	_ZN2v88internal10EntryFrameD0Ev, .-_ZN2v88internal10EntryFrameD0Ev
	.section	.text._ZN2v88internal19ConstructEntryFrameD0Ev,"axG",@progbits,_ZN2v88internal19ConstructEntryFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal19ConstructEntryFrameD0Ev
	.type	_ZN2v88internal19ConstructEntryFrameD0Ev, @function
_ZN2v88internal19ConstructEntryFrameD0Ev:
.LFB28494:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28494:
	.size	_ZN2v88internal19ConstructEntryFrameD0Ev, .-_ZN2v88internal19ConstructEntryFrameD0Ev
	.section	.text._ZN2v88internal9StubFrameD0Ev,"axG",@progbits,_ZN2v88internal9StubFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9StubFrameD0Ev
	.type	_ZN2v88internal9StubFrameD0Ev, @function
_ZN2v88internal9StubFrameD0Ev:
.LFB21972:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21972:
	.size	_ZN2v88internal9StubFrameD0Ev, .-_ZN2v88internal9StubFrameD0Ev
	.section	.text._ZN2v88internal13WasmToJsFrameD0Ev,"axG",@progbits,_ZN2v88internal13WasmToJsFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13WasmToJsFrameD0Ev
	.type	_ZN2v88internal13WasmToJsFrameD0Ev, @function
_ZN2v88internal13WasmToJsFrameD0Ev:
.LFB29310:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29310:
	.size	_ZN2v88internal13WasmToJsFrameD0Ev, .-_ZN2v88internal13WasmToJsFrameD0Ev
	.section	.text._ZN2v88internal13JsToWasmFrameD0Ev,"axG",@progbits,_ZN2v88internal13JsToWasmFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13JsToWasmFrameD0Ev
	.type	_ZN2v88internal13JsToWasmFrameD0Ev, @function
_ZN2v88internal13JsToWasmFrameD0Ev:
.LFB29306:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29306:
	.size	_ZN2v88internal13JsToWasmFrameD0Ev, .-_ZN2v88internal13JsToWasmFrameD0Ev
	.section	.text._ZN2v88internal15CWasmEntryFrameD0Ev,"axG",@progbits,_ZN2v88internal15CWasmEntryFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal15CWasmEntryFrameD0Ev
	.type	_ZN2v88internal15CWasmEntryFrameD0Ev, @function
_ZN2v88internal15CWasmEntryFrameD0Ev:
.LFB28498:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28498:
	.size	_ZN2v88internal15CWasmEntryFrameD0Ev, .-_ZN2v88internal15CWasmEntryFrameD0Ev
	.section	.text._ZN2v88internal17WasmCompiledFrameD0Ev,"axG",@progbits,_ZN2v88internal17WasmCompiledFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal17WasmCompiledFrameD0Ev
	.type	_ZN2v88internal17WasmCompiledFrameD0Ev, @function
_ZN2v88internal17WasmCompiledFrameD0Ev:
.LFB21962:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21962:
	.size	_ZN2v88internal17WasmCompiledFrameD0Ev, .-_ZN2v88internal17WasmCompiledFrameD0Ev
	.section	.text._ZN2v88internal13WasmExitFrameD0Ev,"axG",@progbits,_ZN2v88internal13WasmExitFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13WasmExitFrameD0Ev
	.type	_ZN2v88internal13WasmExitFrameD0Ev, @function
_ZN2v88internal13WasmExitFrameD0Ev:
.LFB29314:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29314:
	.size	_ZN2v88internal13WasmExitFrameD0Ev, .-_ZN2v88internal13WasmExitFrameD0Ev
	.section	.text._ZN2v88internal13InternalFrameD0Ev,"axG",@progbits,_ZN2v88internal13InternalFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13InternalFrameD0Ev
	.type	_ZN2v88internal13InternalFrameD0Ev, @function
_ZN2v88internal13InternalFrameD0Ev:
.LFB21991:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE21991:
	.size	_ZN2v88internal13InternalFrameD0Ev, .-_ZN2v88internal13InternalFrameD0Ev
	.section	.text._ZN2v88internal24BuiltinContinuationFrameD0Ev,"axG",@progbits,_ZN2v88internal24BuiltinContinuationFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal24BuiltinContinuationFrameD0Ev
	.type	_ZN2v88internal24BuiltinContinuationFrameD0Ev, @function
_ZN2v88internal24BuiltinContinuationFrameD0Ev:
.LFB29298:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29298:
	.size	_ZN2v88internal24BuiltinContinuationFrameD0Ev, .-_ZN2v88internal24BuiltinContinuationFrameD0Ev
	.section	.text._ZN2v88internal14ConstructFrameD0Ev,"axG",@progbits,_ZN2v88internal14ConstructFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14ConstructFrameD0Ev
	.type	_ZN2v88internal14ConstructFrameD0Ev, @function
_ZN2v88internal14ConstructFrameD0Ev:
.LFB29302:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29302:
	.size	_ZN2v88internal14ConstructFrameD0Ev, .-_ZN2v88internal14ConstructFrameD0Ev
	.section	.text._ZN2v88internal20WasmCompileLazyFrameD0Ev,"axG",@progbits,_ZN2v88internal20WasmCompileLazyFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal20WasmCompileLazyFrameD0Ev
	.type	_ZN2v88internal20WasmCompileLazyFrameD0Ev, @function
_ZN2v88internal20WasmCompileLazyFrameD0Ev:
.LFB28466:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28466:
	.size	_ZN2v88internal20WasmCompileLazyFrameD0Ev, .-_ZN2v88internal20WasmCompileLazyFrameD0Ev
	.section	.text._ZN2v88internal25WasmInterpreterEntryFrameD0Ev,"axG",@progbits,_ZN2v88internal25WasmInterpreterEntryFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal25WasmInterpreterEntryFrameD0Ev
	.type	_ZN2v88internal25WasmInterpreterEntryFrameD0Ev, @function
_ZN2v88internal25WasmInterpreterEntryFrameD0Ev:
.LFB28470:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28470:
	.size	_ZN2v88internal25WasmInterpreterEntryFrameD0Ev, .-_ZN2v88internal25WasmInterpreterEntryFrameD0Ev
	.section	.text._ZN2v88internal34JavaScriptBuiltinContinuationFrameD0Ev,"axG",@progbits,_ZN2v88internal34JavaScriptBuiltinContinuationFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal34JavaScriptBuiltinContinuationFrameD0Ev
	.type	_ZN2v88internal34JavaScriptBuiltinContinuationFrameD0Ev, @function
_ZN2v88internal34JavaScriptBuiltinContinuationFrameD0Ev:
.LFB22004:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE22004:
	.size	_ZN2v88internal34JavaScriptBuiltinContinuationFrameD0Ev, .-_ZN2v88internal34JavaScriptBuiltinContinuationFrameD0Ev
	.section	.text._ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD0Ev,"axG",@progbits,_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD0Ev
	.type	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD0Ev, @function
_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD0Ev:
.LFB29294:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE29294:
	.size	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD0Ev, .-_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD0Ev
	.section	.text._ZN2v88internal12BuiltinFrameD0Ev,"axG",@progbits,_ZN2v88internal12BuiltinFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal12BuiltinFrameD0Ev
	.type	_ZN2v88internal12BuiltinFrameD0Ev, @function
_ZN2v88internal12BuiltinFrameD0Ev:
.LFB28474:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28474:
	.size	_ZN2v88internal12BuiltinFrameD0Ev, .-_ZN2v88internal12BuiltinFrameD0Ev
	.section	.text._ZN2v88internal21ArgumentsAdaptorFrameD0Ev,"axG",@progbits,_ZN2v88internal21ArgumentsAdaptorFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal21ArgumentsAdaptorFrameD0Ev
	.type	_ZN2v88internal21ArgumentsAdaptorFrameD0Ev, @function
_ZN2v88internal21ArgumentsAdaptorFrameD0Ev:
.LFB28478:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28478:
	.size	_ZN2v88internal21ArgumentsAdaptorFrameD0Ev, .-_ZN2v88internal21ArgumentsAdaptorFrameD0Ev
	.section	.text._ZN2v88internal16InterpretedFrameD0Ev,"axG",@progbits,_ZN2v88internal16InterpretedFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal16InterpretedFrameD0Ev
	.type	_ZN2v88internal16InterpretedFrameD0Ev, @function
_ZN2v88internal16InterpretedFrameD0Ev:
.LFB28482:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28482:
	.size	_ZN2v88internal16InterpretedFrameD0Ev, .-_ZN2v88internal16InterpretedFrameD0Ev
	.section	.text._ZN2v88internal14OptimizedFrameD0Ev,"axG",@progbits,_ZN2v88internal14OptimizedFrameD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14OptimizedFrameD0Ev
	.type	_ZN2v88internal14OptimizedFrameD0Ev, @function
_ZN2v88internal14OptimizedFrameD0Ev:
.LFB28486:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE28486:
	.size	_ZN2v88internal14OptimizedFrameD0Ev, .-_ZN2v88internal14OptimizedFrameD0Ev
	.section	.text._ZNK2v88internal21ArgumentsAdaptorFrame14unchecked_codeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21ArgumentsAdaptorFrame14unchecked_codeEv
	.type	_ZNK2v88internal21ArgumentsAdaptorFrame14unchecked_codeEv, @function
_ZNK2v88internal21ArgumentsAdaptorFrame14unchecked_codeEv:
.LFB23364:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	movl	$3, %esi
	addq	$41184, %rdi
	jmp	_ZN2v88internal8Builtins7builtinEi@PLT
	.cfi_endproc
.LFE23364:
	.size	_ZNK2v88internal21ArgumentsAdaptorFrame14unchecked_codeEv, .-_ZNK2v88internal21ArgumentsAdaptorFrame14unchecked_codeEv
	.section	.rodata._ZNK2v88internal13StandardFrame6scriptEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZNK2v88internal13StandardFrame6scriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13StandardFrame6scriptEv
	.type	_ZNK2v88internal13StandardFrame6scriptEv, @function
_ZNK2v88internal13StandardFrame6scriptEv:
.LFB23233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23233:
	.size	_ZNK2v88internal13StandardFrame6scriptEv, .-_ZNK2v88internal13StandardFrame6scriptEv
	.section	.text._ZNK2v88internal13StandardFrame12GetParameterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13StandardFrame12GetParameterEi
	.type	_ZNK2v88internal13StandardFrame12GetParameterEi, @function
_ZNK2v88internal13StandardFrame12GetParameterEi:
.LFB23238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23238:
	.size	_ZNK2v88internal13StandardFrame12GetParameterEi, .-_ZNK2v88internal13StandardFrame12GetParameterEi
	.section	.text._ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.type	_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE, @function
_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE:
.LFB23242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23242:
	.size	_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE, .-_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.section	.text._ZNK2v88internal10EntryFrame14unchecked_codeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10EntryFrame14unchecked_codeEv
	.type	_ZNK2v88internal10EntryFrame14unchecked_codeEv, @function
_ZNK2v88internal10EntryFrame14unchecked_codeEv:
.LFB23208:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	movl	$40, %esi
	addq	$37592, %rdi
	jmp	_ZN2v88internal4Heap7builtinEi@PLT
	.cfi_endproc
.LFE23208:
	.size	_ZNK2v88internal10EntryFrame14unchecked_codeEv, .-_ZNK2v88internal10EntryFrame14unchecked_codeEv
	.section	.text._ZNK2v88internal19ConstructEntryFrame14unchecked_codeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal19ConstructEntryFrame14unchecked_codeEv
	.type	_ZNK2v88internal19ConstructEntryFrame14unchecked_codeEv, @function
_ZNK2v88internal19ConstructEntryFrame14unchecked_codeEv:
.LFB23212:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	movl	$41, %esi
	addq	$37592, %rdi
	jmp	_ZN2v88internal4Heap7builtinEi@PLT
	.cfi_endproc
.LFE23212:
	.size	_ZNK2v88internal19ConstructEntryFrame14unchecked_codeEv, .-_ZNK2v88internal19ConstructEntryFrame14unchecked_codeEv
	.section	.text._ZNK2v88internal9StubFrame14unchecked_codeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9StubFrame14unchecked_codeEv
	.type	_ZNK2v88internal9StubFrame14unchecked_codeEv, @function
_ZNK2v88internal9StubFrame14unchecked_codeEv:
.LFB23245:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	16(%rdi), %rdi
	movq	(%rax), %rsi
	jmp	_ZN2v88internal7Isolate14FindCodeObjectEm@PLT
	.cfi_endproc
.LFE23245:
	.size	_ZNK2v88internal9StubFrame14unchecked_codeEv, .-_ZNK2v88internal9StubFrame14unchecked_codeEv
	.section	.text._ZNK2v88internal17WasmCompiledFrame14unchecked_codeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17WasmCompiledFrame14unchecked_codeEv
	.type	_ZNK2v88internal17WasmCompiledFrame14unchecked_codeEv, @function
_ZNK2v88internal17WasmCompiledFrame14unchecked_codeEv:
.LFB23370:
	.cfi_startproc
	endbr64
	movq	40(%rdi), %rax
	movq	16(%rdi), %rdi
	movq	(%rax), %rsi
	jmp	_ZN2v88internal7Isolate14FindCodeObjectEm@PLT
	.cfi_endproc
.LFE23370:
	.size	_ZNK2v88internal17WasmCompiledFrame14unchecked_codeEv, .-_ZNK2v88internal17WasmCompiledFrame14unchecked_codeEv
	.section	.text._ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,"axG",@progbits,_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv,comdat
	.p2align 4
	.weak	_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.type	_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, @function
_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv:
.LFB27541:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal7ICStatsC1Ev@PLT
	.cfi_endproc
.LFE27541:
	.size	_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv, .-_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv
	.section	.text._ZNK2v88internal12BuiltinFrame22ComputeParametersCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12BuiltinFrame22ComputeParametersCountEv
	.type	_ZNK2v88internal12BuiltinFrame22ComputeParametersCountEv, @function
_ZNK2v88internal12BuiltinFrame22ComputeParametersCountEv:
.LFB23365:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movslq	-20(%rax), %rax
	ret
	.cfi_endproc
.LFE23365:
	.size	_ZNK2v88internal12BuiltinFrame22ComputeParametersCountEv, .-_ZNK2v88internal12BuiltinFrame22ComputeParametersCountEv
	.section	.text._ZNK2v88internal11NativeFrame18ComputeCallerStateEPNS0_10StackFrame5StateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11NativeFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.type	_ZNK2v88internal11NativeFrame18ComputeCallerStateEPNS0_10StackFrame5StateE, @function
_ZNK2v88internal11NativeFrame18ComputeCallerStateEPNS0_10StackFrame5StateE:
.LFB23207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal11NativeFrame21GetCallerStackPointerEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rsi, %rbx
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L125
	movq	32(%rdi), %rax
	addq	$16, %rax
.L126:
	movq	%rax, (%rbx)
	movq	32(%r12), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%rbx)
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	movq	32(%r12), %rdi
	addq	$8, %rdi
	testq	%rax, %rax
	je	.L130
	call	*%rax
.L128:
	movq	%rax, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	call	*%rax
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%rdi, %rax
	jmp	.L128
	.cfi_endproc
.LFE23207:
	.size	_ZNK2v88internal11NativeFrame18ComputeCallerStateEPNS0_10StackFrame5StateE, .-_ZNK2v88internal11NativeFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.section	.text._ZNK2v88internal9ExitFrame18ComputeCallerStateEPNS0_10StackFrame5StateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ExitFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.type	_ZNK2v88internal9ExitFrame18ComputeCallerStateEPNS0_10StackFrame5StateE, @function
_ZNK2v88internal9ExitFrame18ComputeCallerStateEPNS0_10StackFrame5StateE:
.LFB23214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal9ExitFrame21GetCallerStackPointerEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	%rsi, %rbx
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L132
	movq	32(%rdi), %rax
	addq	$16, %rax
.L133:
	movq	%rax, (%rbx)
	movq	32(%r12), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%rbx)
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	movq	32(%r12), %rdi
	addq	$8, %rdi
	testq	%rax, %rax
	je	.L137
	call	*%rax
.L135:
	movq	%rax, 16(%rbx)
	movq	$0, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_restore_state
	call	*%rax
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%rdi, %rax
	jmp	.L135
	.cfi_endproc
.LFE23214:
	.size	_ZNK2v88internal9ExitFrame18ComputeCallerStateEPNS0_10StackFrame5StateE, .-_ZNK2v88internal9ExitFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.section	.text._ZNK2v88internal15JavaScriptFrame12GetParameterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame12GetParameterEi
	.type	_ZNK2v88internal15JavaScriptFrame12GetParameterEi, @function
_ZNK2v88internal15JavaScriptFrame12GetParameterEi:
.LFB23279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movl	%esi, %ebx
	call	*120(%rax)
	leaq	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv(%rip), %rdx
	subl	%ebx, %eax
	leal	-8(,%rax,8), %ebx
	movq	(%r12), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L139
	movq	32(%r12), %rax
	movslq	%ebx, %rbx
	addq	$16, %rax
	movq	(%rbx,%rax), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	%r12, %rdi
	movslq	%ebx, %rbx
	call	*%rax
	movq	(%rbx,%rax), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23279:
	.size	_ZNK2v88internal15JavaScriptFrame12GetParameterEi, .-_ZNK2v88internal15JavaScriptFrame12GetParameterEi
	.section	.text._ZNK2v88internal16InterpretedFrame8positionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16InterpretedFrame8positionEv
	.type	_ZNK2v88internal16InterpretedFrame8positionEv, @function
_ZNK2v88internal16InterpretedFrame8positionEv:
.LFB23353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi(%rip), %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	144(%rax), %rdx
	cmpq	%rbx, %rdx
	jne	.L143
	movq	32(%rdi), %rax
	subq	$24, %rax
	movq	(%rax), %rax
	movq	%rax, -32(%rbp)
	cmpq	%rbx, %rdx
	jne	.L145
.L150:
	movq	32(%rdi), %rax
	subq	$32, %rax
.L146:
	movq	(%rax), %rsi
	leaq	-32(%rbp), %rdi
	sarq	$32, %rsi
	subl	$53, %esi
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L149
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	%rdi, -40(%rbp)
	movl	$-2, %esi
	call	*%rdx
	movq	-40(%rbp), %rdi
	movq	(%rax), %rax
	movq	(%rdi), %rdx
	movq	%rax, -32(%rbp)
	movq	144(%rdx), %rdx
	cmpq	%rbx, %rdx
	je	.L150
.L145:
	movl	$-1, %esi
	call	*%rdx
	jmp	.L146
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23353:
	.size	_ZNK2v88internal16InterpretedFrame8positionEv, .-_ZNK2v88internal16InterpretedFrame8positionEv
	.section	.text._ZN2v88internal16InterpretedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16InterpretedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE
	.type	_ZN2v88internal16InterpretedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE, @function
_ZN2v88internal16InterpretedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE:
.LFB23354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi(%rip), %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	144(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L152
	movq	32(%rdi), %rax
	subq	$24, %rax
.L153:
	movq	(%rax), %rsi
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v88internal12HandlerTableC1ENS0_13BytecodeArrayE@PLT
	movq	(%r12), %rax
	movq	144(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L154
	movq	32(%r12), %rax
	subq	$32, %rax
.L155:
	movq	(%rax), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	sarq	$32, %rsi
	subl	$53, %esi
	call	_ZN2v88internal12HandlerTable11LookupRangeEiPiPNS1_15CatchPredictionE@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L158
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	movl	$-2, %esi
	call	*%rax
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L154:
	movl	$-1, %esi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L155
.L158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23354:
	.size	_ZN2v88internal16InterpretedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE, .-_ZN2v88internal16InterpretedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE
	.section	.text._ZNK2v88internal21ArgumentsAdaptorFrame22ComputeParametersCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21ArgumentsAdaptorFrame22ComputeParametersCountEv
	.type	_ZNK2v88internal21ArgumentsAdaptorFrame22ComputeParametersCountEv, @function
_ZNK2v88internal21ArgumentsAdaptorFrame22ComputeParametersCountEv:
.LFB23363:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi(%rip), %rdx
	movq	144(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L160
	movq	32(%rdi), %rax
	subq	$24, %rax
	movq	(%rax), %rax
	sarq	$32, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*%rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rax), %rax
	sarq	$32, %rax
	ret
	.cfi_endproc
.LFE23363:
	.size	_ZNK2v88internal21ArgumentsAdaptorFrame22ComputeParametersCountEv, .-_ZNK2v88internal21ArgumentsAdaptorFrame22ComputeParametersCountEv
	.section	.text._ZNK2v88internal15JavaScriptFrame14unchecked_codeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame14unchecked_codeEv
	.type	_ZNK2v88internal15JavaScriptFrame14unchecked_codeEv, @function
_ZNK2v88internal15JavaScriptFrame14unchecked_codeEv:
.LFB23261:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %rdx
	movq	152(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L166
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	movq	47(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*%rax
	movq	47(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23261:
	.size	_ZNK2v88internal15JavaScriptFrame14unchecked_codeEv, .-_ZNK2v88internal15JavaScriptFrame14unchecked_codeEv
	.section	.text._ZNK2v88internal15JavaScriptFrame22ComputeParametersCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame22ComputeParametersCountEv
	.type	_ZNK2v88internal15JavaScriptFrame22ComputeParametersCountEv, @function
_ZNK2v88internal15JavaScriptFrame22ComputeParametersCountEv:
.LFB23280:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %rdx
	movq	152(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L172
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	movq	23(%rax), %rax
	movzwl	41(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*%rax
	movq	23(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movzwl	41(%rax), %eax
	ret
	.cfi_endproc
.LFE23280:
	.size	_ZNK2v88internal15JavaScriptFrame22ComputeParametersCountEv, .-_ZNK2v88internal15JavaScriptFrame22ComputeParametersCountEv
	.section	.text._ZNK2v88internal20WasmCompileLazyFrame7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20WasmCompileLazyFrame7IterateEPNS0_11RootVisitorE
	.type	_ZNK2v88internal20WasmCompileLazyFrame7IterateEPNS0_11RootVisitorE, @function
_ZNK2v88internal20WasmCompileLazyFrame7IterateEPNS0_11RootVisitorE:
.LFB23405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %rdx
	movq	%rdi, %rbx
	movq	(%rsi), %rax
	movq	24(%rdi), %rcx
	movl	$6, %esi
	movq	%r12, %rdi
	leaq	-152(%rdx), %r8
	xorl	%edx, %edx
	call	*16(%rax)
	movq	(%r12), %rax
	movq	32(%rbx), %r8
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdx
	movq	24(%rax), %r9
	leaq	-16(%r8), %rcx
	cmpq	%rdx, %r9
	jne	.L178
	popq	%rbx
	movq	%r12, %rdi
	movq	16(%rax), %rax
	subq	$8, %r8
	popq	%r12
	xorl	%edx, %edx
	movl	$6, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	movl	$6, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%r9
	.cfi_endproc
.LFE23405:
	.size	_ZNK2v88internal20WasmCompileLazyFrame7IterateEPNS0_11RootVisitorE, .-_ZNK2v88internal20WasmCompileLazyFrame7IterateEPNS0_11RootVisitorE
	.section	.rodata._ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE
	.type	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE, @function
_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE:
.LFB23264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	152(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L181
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	movq	23(%rax), %r14
	movq	8(%rbx), %r12
	cmpq	16(%rbx), %r12
	je	.L183
.L209:
	movq	%r14, (%r12)
	addq	$8, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	call	*%rax
	movq	23(%rax), %r14
	movq	8(%rbx), %r12
	cmpq	16(%rbx), %r12
	jne	.L209
.L183:
	movabsq	$1152921504606846975, %rsi
	movq	(%rbx), %r15
	movq	%r12, %rdx
	subq	%r15, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L210
	testq	%rax, %rax
	je	.L194
	movabsq	$9223372036854775800, %rcx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L211
.L186:
	movq	%rcx, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rax, %r13
	addq	%rax, %rcx
	leaq	8(%rax), %rax
.L187:
	movq	%r14, 0(%r13,%rdx)
	cmpq	%r15, %r12
	je	.L188
	leaq	-8(%r12), %rdi
	leaq	15(%r13), %rax
	subq	%r15, %rdi
	subq	%r15, %rax
	movq	%rdi, %rsi
	shrq	$3, %rsi
	cmpq	$30, %rax
	jbe	.L197
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rsi
	je	.L197
	addq	$1, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L190:
	movdqu	(%r15,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L190
	movq	%rsi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rdx
	leaq	(%r15,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%r8, %rsi
	je	.L192
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L192:
	leaq	16(%r13,%rdi), %rax
.L188:
	testq	%r15, %r15
	je	.L193
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rcx
.L193:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%rcx, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L212
	movl	$8, %eax
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$8, %ecx
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%r13, %rdx
	movq	%r15, %rax
	.p2align 4,,10
	.p2align 3
.L189:
	movq	(%rax), %rsi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L189
	jmp	.L192
.L210:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L212:
	cmpq	%rsi, %rdi
	movq	%rsi, %rcx
	cmovbe	%rdi, %rcx
	salq	$3, %rcx
	jmp	.L186
	.cfi_endproc
.LFE23264:
	.size	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE, .-_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE
	.section	.text._ZNK2v88internal15JavaScriptFrame6scriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame6scriptEv
	.type	_ZNK2v88internal15JavaScriptFrame6scriptEv, @function
_ZNK2v88internal15JavaScriptFrame6scriptEv:
.LFB23271:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %rdx
	movq	152(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L224
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L225
.L220:
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L220
	movq	23(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*%rax
	movq	23(%rax), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L226
.L216:
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	jne	.L216
	movq	23(%rax), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23271:
	.size	_ZNK2v88internal15JavaScriptFrame6scriptEv, .-_ZNK2v88internal15JavaScriptFrame6scriptEv
	.section	.rodata._ZNK2v88internal12BuiltinFrame14PrintFrameKindEPNS0_12StringStreamE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"builtin frame: "
	.section	.text._ZNK2v88internal12BuiltinFrame14PrintFrameKindEPNS0_12StringStreamE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12BuiltinFrame14PrintFrameKindEPNS0_12StringStreamE
	.type	_ZNK2v88internal12BuiltinFrame14PrintFrameKindEPNS0_12StringStreamE, @function
_ZNK2v88internal12BuiltinFrame14PrintFrameKindEPNS0_12StringStreamE:
.LFB23366:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$15, %edx
	leaq	.LC2(%rip), %rsi
	jmp	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	.cfi_endproc
.LFE23366:
	.size	_ZNK2v88internal12BuiltinFrame14PrintFrameKindEPNS0_12StringStreamE, .-_ZNK2v88internal12BuiltinFrame14PrintFrameKindEPNS0_12StringStreamE
	.section	.text._ZNK2v88internal15JavaScriptFrame13GetParametersEv.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZNK2v88internal15JavaScriptFrame13GetParametersEv.part.0, @function
_ZNK2v88internal15JavaScriptFrame13GetParametersEv.part.0:
.LFB29583:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	call	*120(%rax)
	movq	16(%r15), %rdi
	xorl	%edx, %edx
	movl	%eax, %esi
	movl	%eax, -56(%rbp)
	call	_ZN2v88internal7Factory13NewFixedArrayEiNS0_14AllocationTypeE@PLT
	movl	-56(%rbp), %esi
	movq	%rax, %r12
	testl	%esi, %esi
	jle	.L242
	leal	-1(%rsi), %r14d
	xorl	%ebx, %ebx
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L249:
	movq	%r15, %rdi
	call	*120(%rax)
	leaq	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv(%rip), %rcx
	subl	%ebx, %eax
	leal	-8(,%rax,8), %edx
	movq	(%r15), %rax
	movq	56(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L231
	movq	32(%r15), %rax
	addq	$16, %rax
.L232:
	movslq	%edx, %rdx
	movq	(%rdx,%rax), %rdx
.L233:
	leaq	15(%r13,%rbx,8), %rsi
	movq	%rdx, (%rsi)
	testb	$1, %dl
	je	.L239
	movq	%rdx, %r9
	andq	$-262144, %r9
	movq	8(%r9), %rax
	movq	%r9, -56(%rbp)
	testl	$262144, %eax
	je	.L235
	movq	%r13, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	-56(%rbp), %r9
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	8(%r9), %rax
.L235:
	testb	$24, %al
	je	.L239
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L239
	movq	%r13, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L239:
	leaq	1(%rbx), %rax
	cmpq	%r14, %rbx
	je	.L242
	movq	%rax, %rbx
.L238:
	movq	(%r15), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame12GetParameterEi(%rip), %rcx
	movq	(%r12), %r13
	movq	112(%rax), %rdx
	cmpq	%rcx, %rdx
	je	.L249
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	*%rdx
	movq	%rax, %rdx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L231:
	movl	%edx, -56(%rbp)
	movq	%r15, %rdi
	call	*%rax
	movl	-56(%rbp), %edx
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L242:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE29583:
	.size	_ZNK2v88internal15JavaScriptFrame13GetParametersEv.part.0, .-_ZNK2v88internal15JavaScriptFrame13GetParametersEv.part.0
	.section	.text._ZN2v88internal12_GLOBAL__N_120IsInterpreterFramePcEPNS0_7IsolateEmPNS0_10StackFrame5StateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_120IsInterpreterFramePcEPNS0_7IsolateEmPNS0_10StackFrame5StateE.isra.0, @function
_ZN2v88internal12_GLOBAL__N_120IsInterpreterFramePcEPNS0_7IsolateEmPNS0_10StackFrame5StateE.isra.0:
.LFB29572:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	41184(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movl	$57, %esi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movl	$64, %esi
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movl	$65, %esi
	movq	%r13, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	%rax, -48(%rbp)
	movq	-64(%rbp), %rax
	movl	43(%rax), %esi
	testl	%esi, %esi
	js	.L277
.L251:
	leaq	-1(%rax), %r13
	cmpq	%r13, %r12
	jnb	.L255
.L259:
	movq	-56(%rbp), %rax
	movl	43(%rax), %ecx
	testl	%ecx, %ecx
	js	.L278
.L257:
	leaq	-1(%rax), %r13
	cmpq	%r13, %r12
	jnb	.L261
.L264:
	movq	-48(%rbp), %rax
	movl	43(%rax), %edx
	testl	%edx, %edx
	js	.L279
.L263:
	leaq	-1(%rax), %r13
	cmpq	%r13, %r12
	jnb	.L266
.L269:
	cmpb	$0, _ZN2v88internal36FLAG_interpreted_frames_native_stackE(%rip)
	je	.L268
	movq	(%r14), %rax
	testb	$1, -8(%rax)
	je	.L268
	testb	$1, -16(%rax)
	jne	.L280
.L268:
	xorl	%eax, %eax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L255:
	movq	-1(%rax), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r13
	cmpq	%r13, %r12
	jnb	.L259
.L258:
	movl	$1, %eax
.L250:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L281
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	cmpq	%rax, %r12
	jnb	.L252
	movq	-64(%rbp), %rax
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L261:
	movq	-1(%rax), %rsi
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r13
	cmpq	%r13, %r12
	jb	.L258
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L252:
	movq	%r13, %rdi
	call	_ZNK2v88internal4Code21OffHeapInstructionEndEv@PLT
	movq	%rax, %r8
	movq	-64(%rbp), %rax
	cmpq	%r8, %r12
	jnb	.L251
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L266:
	movq	-1(%rax), %rsi
	leaq	-48(%rbp), %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r13
	cmpq	%r13, %r12
	jb	.L258
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L278:
	leaq	-56(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	cmpq	%rax, %r12
	jnb	.L260
.L275:
	movq	-56(%rbp), %rax
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L279:
	leaq	-48(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	cmpq	%rax, %r12
	jnb	.L265
.L276:
	movq	-48(%rbp), %rax
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L280:
	leaq	37592(%rbx), %r13
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4Heap11InSpaceSlowEmNS0_15AllocationSpaceE@PLT
	testb	%al, %al
	je	.L268
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal4Heap29GcSafeFindCodeForInnerPointerEm@PLT
	movq	%rax, -64(%rbp)
	movl	59(%rax), %edx
	leal	-64(%rdx), %eax
	cmpl	$1, %eax
	setbe	%al
	cmpl	$57, %edx
	sete	%dl
	orl	%edx, %eax
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%r13, %rdi
	call	_ZNK2v88internal4Code21OffHeapInstructionEndEv@PLT
	cmpq	%rax, %r12
	jnb	.L276
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%r13, %rdi
	call	_ZNK2v88internal4Code21OffHeapInstructionEndEv@PLT
	cmpq	%rax, %r12
	jnb	.L275
	jmp	.L258
.L281:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE29572:
	.size	_ZN2v88internal12_GLOBAL__N_120IsInterpreterFramePcEPNS0_7IsolateEmPNS0_10StackFrame5StateE.isra.0, .-_ZN2v88internal12_GLOBAL__N_120IsInterpreterFramePcEPNS0_7IsolateEmPNS0_10StackFrame5StateE.isra.0
	.section	.rodata._ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi.str1.1,"aMS",@progbits,1
.LC3:
	.string	"%5d: "
.LC4:
	.string	"[%d]: "
.LC5:
	.string	"NativeFrame"
.LC6:
	.string	"EntryFrame"
.LC7:
	.string	"ExitFrame"
.LC8:
	.string	"OptimizedFrame"
.LC9:
	.string	"WasmCompiledFrame"
.LC10:
	.string	"WasmToJsFrame"
.LC11:
	.string	"JsToWasmFrame"
.LC12:
	.string	"WasmInterpreterEntryFrame"
.LC13:
	.string	"CWasmEntryFrame"
.LC14:
	.string	"WasmExitFrame"
.LC15:
	.string	"WasmCompileLazyFrame"
.LC16:
	.string	"InterpretedFrame"
.LC17:
	.string	"StubFrame"
.LC18:
	.string	"BuiltinContinuationFrame"
	.section	.rodata._ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"JavaScriptBuiltinContinuationFrame"
	.align 8
.LC20:
	.string	"JavaScriptBuiltinContinuationWithCatchFrame"
	.section	.rodata._ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi.str1.1
.LC21:
	.string	"InternalFrame"
.LC22:
	.string	"ConstructFrame"
.LC23:
	.string	"ArgumentsAdaptorFrame"
.LC24:
	.string	"BuiltinFrame"
.LC25:
	.string	"BuiltinExitFrame"
.LC26:
	.string	"ConstructEntryFrame"
.LC27:
	.string	" [pc: %p]\n"
	.section	.text._ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.type	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi, @function
_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi:
.LFB23229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	.LC3(%rip), %rsi
	leaq	-48(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %edx
	leaq	.LC4(%rip), %rax
	movl	%ecx, -48(%rbp)
	sbbq	%r9, %r9
	movq	%r13, %rcx
	addq	$6, %r9
	testl	%edx, %edx
	cmovne	%rax, %rsi
	movq	%r9, %rdx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*8(%rax)
	cmpl	$22, %eax
	ja	.L284
	leaq	.L286(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi,"a",@progbits
	.align 4
	.align 4
.L286:
	.long	.L284-.L286
	.long	.L307-.L286
	.long	.L306-.L286
	.long	.L310-.L286
	.long	.L304-.L286
	.long	.L303-.L286
	.long	.L302-.L286
	.long	.L301-.L286
	.long	.L300-.L286
	.long	.L299-.L286
	.long	.L298-.L286
	.long	.L297-.L286
	.long	.L296-.L286
	.long	.L295-.L286
	.long	.L294-.L286
	.long	.L293-.L286
	.long	.L292-.L286
	.long	.L291-.L286
	.long	.L290-.L286
	.long	.L289-.L286
	.long	.L288-.L286
	.long	.L287-.L286
	.long	.L285-.L286
	.section	.text._ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.p2align 4,,10
	.p2align 3
.L310:
	movl	$9, %edx
	leaq	.LC7(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L305:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	40(%rbx), %rax
	movq	%r13, %rcx
	movq	%r12, %rdi
	movl	$1, %r8d
	leaq	.LC27(%rip), %rsi
	movl	$10, %edx
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L312
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movl	$19, %edx
	leaq	.LC26(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L292:
	movl	$43, %edx
	leaq	.LC20(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L293:
	movl	$34, %edx
	leaq	.LC19(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L294:
	movl	$24, %edx
	leaq	.LC18(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L295:
	movl	$9, %edx
	leaq	.LC17(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L296:
	movl	$16, %edx
	leaq	.LC16(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L297:
	movl	$20, %edx
	leaq	.LC15(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L298:
	movl	$13, %edx
	leaq	.LC14(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L299:
	movl	$15, %edx
	leaq	.LC13(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L288:
	movl	$12, %edx
	leaq	.LC24(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L289:
	movl	$21, %edx
	leaq	.LC23(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L290:
	movl	$14, %edx
	leaq	.LC22(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L291:
	movl	$13, %edx
	leaq	.LC21(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L285:
	movl	$11, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L287:
	movl	$16, %edx
	leaq	.LC25(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L304:
	movl	$14, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L300:
	movl	$25, %edx
	leaq	.LC12(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L301:
	movl	$13, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L302:
	movl	$13, %edx
	leaq	.LC10(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L303:
	movl	$17, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L307:
	movl	$10, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L305
.L284:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L312:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23229:
	.size	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi, .-_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.section	.text._ZNK2v88internal15JavaScriptFrame8receiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame8receiverEv
	.type	_ZNK2v88internal15JavaScriptFrame8receiverEv, @function
_ZNK2v88internal15JavaScriptFrame8receiverEv:
.LFB23269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal15JavaScriptFrame12GetParameterEi(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rax
	movq	112(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L314
	call	*120(%rax)
	leaq	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv(%rip), %rdx
	leal	0(,%rax,8), %ebx
	movq	(%r12), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L315
	movq	32(%r12), %rax
	movslq	%ebx, %rbx
	addq	$16, %rax
	movq	(%rbx,%rax), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	popq	%rbx
	movl	$-1, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movq	%r12, %rdi
	movslq	%ebx, %rbx
	call	*%rax
	movq	(%rbx,%rax), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23269:
	.size	_ZNK2v88internal15JavaScriptFrame8receiverEv, .-_ZNK2v88internal15JavaScriptFrame8receiverEv
	.section	.rodata._ZNK2v88internal25WasmInterpreterEntryFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi.str1.1,"aMS",@progbits,1
.LC28:
	.string	"WASM INTERPRETER ENTRY ["
.LC29:
	.string	"]"
.LC30:
	.string	"\n"
	.section	.text._ZNK2v88internal25WasmInterpreterEntryFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25WasmInterpreterEntryFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.type	_ZNK2v88internal25WasmInterpreterEntryFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi, @function
_ZNK2v88internal25WasmInterpreterEntryFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi:
.LFB23383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	.LC3(%rip), %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	movq	%r12, %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %edx
	leaq	.LC4(%rip), %rax
	movl	%ecx, -48(%rbp)
	sbbq	%rdx, %rdx
	leaq	-48(%rbp), %rcx
	addq	$6, %rdx
	testl	%ebx, %ebx
	cmovne	%rax, %rsi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$24, %edx
	leaq	.LC28(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	32(%r13), %rax
	movq	%r12, %rdi
	movq	-16(%rax), %rax
	movq	135(%rax), %rax
	movq	39(%rax), %rax
	movq	15(%rax), %rsi
	call	_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$1, %edx
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	testl	%ebx, %ebx
	jne	.L329
.L320:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L330
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L320
.L330:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23383:
	.size	_ZNK2v88internal25WasmInterpreterEntryFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi, .-_ZNK2v88internal25WasmInterpreterEntryFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.section	.rodata._ZNK2v88internal16BuiltinExitFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi.str1.1,"aMS",@progbits,1
.LC31:
	.string	"builtin exit frame: "
.LC32:
	.string	"new "
.LC33:
	.string	"(this=%o"
.LC34:
	.string	",%o"
.LC35:
	.string	")\n\n"
	.section	.text._ZNK2v88internal16BuiltinExitFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16BuiltinExitFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.type	_ZNK2v88internal16BuiltinExitFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi, @function
_ZNK2v88internal16BuiltinExitFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi:
.LFB23230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	%r15, %rdi
	movslq	36(%rax), %rdx
	movq	24(%rax), %r9
	leal	8(,%rdx,8), %edx
	movq	%r9, %rsi
	movq	%r9, -88(%rbp)
	movslq	%edx, %rdx
	movq	(%rdx,%rax), %r14
	call	_ZN2v88internal12StringStream27PrintSecurityTokenIfChangedENS0_10JSFunctionE@PLT
	cmpl	$1, %ebx
	movl	%r13d, -64(%rbp)
	leaq	-64(%rbp), %r13
	sbbq	%rdx, %rdx
	leaq	.LC4(%rip), %rax
	movq	%r13, %rcx
	movq	%r15, %rdi
	addq	$6, %rdx
	leaq	.LC3(%rip), %rsi
	testl	%ebx, %ebx
	movl	$1, %r8d
	cmovne	%rax, %rsi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$20, %edx
	leaq	.LC31(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	32(%r12), %rdx
	movq	16(%r12), %rax
	movq	$0, -72(%rbp)
	movq	-88(%rbp), %r9
	movq	88(%rax), %rax
	cmpq	%rax, 16(%rdx)
	je	.L333
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC32(%rip), %rsi
	movq	%r15, %rdi
	movl	$4, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	-88(%rbp), %r9
.L333:
	movq	%r9, %rsi
	leaq	-72(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal12StringStream13PrintFunctionENS0_10JSFunctionENS0_6ObjectEPNS0_4CodeE@PLT
	movl	$8, %edx
	movq	%r13, %rcx
	movq	%r15, %rdi
	movl	$1, %r8d
	leaq	.LC33(%rip), %rsi
	movq	%r14, -64(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	32(%r12), %rax
	movslq	36(%rax), %rdx
	cmpl	$4, %edx
	jle	.L334
	subl	$5, %edx
	movl	$48, %ebx
	leaq	.LC34(%rip), %r14
	leaq	56(,%rdx,8), %rdi
	movq	%rdi, -88(%rbp)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L339:
	movq	32(%r12), %rax
.L335:
	movq	(%rax,%rbx), %rax
	movq	%r13, %rcx
	movl	$1, %r8d
	movq	%r14, %rsi
	movl	$3, %edx
	movq	%r15, %rdi
	addq	$8, %rbx
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	cmpq	%rbx, -88(%rbp)
	jne	.L339
.L334:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC35(%rip), %rsi
	movq	%r15, %rdi
	movl	$3, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L340
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L340:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23230:
	.size	_ZNK2v88internal16BuiltinExitFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi, .-_ZNK2v88internal16BuiltinExitFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.section	.text._ZNK2v88internal10EntryFrame14GetCallerStateEPNS0_10StackFrame5StateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10EntryFrame14GetCallerStateEPNS0_10StackFrame5StateE
	.type	_ZNK2v88internal10EntryFrame14GetCallerStateEPNS0_10StackFrame5StateE, @function
_ZNK2v88internal10EntryFrame14GetCallerStateEPNS0_10StackFrame5StateE:
.LFB23210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %rax
	movq	-64(%rax), %rax
	testq	%rax, %rax
	je	.L341
	movq	-8(%rax), %rdx
	movq	%rsi, %rbx
	leaq	-16(%rax), %rdi
	testb	$1, %dl
	jne	.L350
	sarq	%rdx
	movl	%edx, %r12d
	cmpl	$21, %edx
	jbe	.L354
.L350:
	movl	$3, %r12d
.L343:
	movq	-16(%rax), %rdi
.L344:
	movq	%rdi, %xmm0
	movq	%rax, %xmm1
	subq	$8, %rdi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	testq	%rax, %rax
	je	.L355
	call	*%rax
.L346:
	movq	%rax, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
.L341:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L354:
	.cfi_restore_state
	movl	$2098184, %ecx
	btq	%rdx, %rcx
	jnc	.L350
	cmpl	$10, %edx
	jne	.L343
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L355:
	movq	%rdi, %rax
	jmp	.L346
	.cfi_endproc
.LFE23210:
	.size	_ZNK2v88internal10EntryFrame14GetCallerStateEPNS0_10StackFrame5StateE, .-_ZNK2v88internal10EntryFrame14GetCallerStateEPNS0_10StackFrame5StateE
	.section	.text._ZNK2v88internal15CWasmEntryFrame14GetCallerStateEPNS0_10StackFrame5StateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15CWasmEntryFrame14GetCallerStateEPNS0_10StackFrame5StateE
	.type	_ZNK2v88internal15CWasmEntryFrame14GetCallerStateEPNS0_10StackFrame5StateE, @function
_ZNK2v88internal15CWasmEntryFrame14GetCallerStateEPNS0_10StackFrame5StateE:
.LFB23211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	testq	%rax, %rax
	je	.L356
	movq	-8(%rax), %rdx
	movq	%rsi, %rbx
	leaq	-16(%rax), %rdi
	testb	$1, %dl
	jne	.L365
	sarq	%rdx
	movl	%edx, %r12d
	cmpl	$21, %edx
	jbe	.L369
.L365:
	movl	$3, %r12d
.L358:
	movq	-16(%rax), %rdi
.L359:
	movq	%rdi, %xmm0
	movq	%rax, %xmm1
	subq	$8, %rdi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	testq	%rax, %rax
	je	.L370
	call	*%rax
.L361:
	movq	%rax, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
.L356:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore_state
	movl	$2098184, %ecx
	btq	%rdx, %rcx
	jnc	.L365
	cmpl	$10, %edx
	jne	.L358
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L370:
	movq	%rdi, %rax
	jmp	.L361
	.cfi_endproc
.LFE23211:
	.size	_ZNK2v88internal15CWasmEntryFrame14GetCallerStateEPNS0_10StackFrame5StateE, .-_ZNK2v88internal15CWasmEntryFrame14GetCallerStateEPNS0_10StackFrame5StateE
	.section	.text._ZNK2v88internal10EntryFrame18ComputeCallerStateEPNS0_10StackFrame5StateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10EntryFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.type	_ZNK2v88internal10EntryFrame18ComputeCallerStateEPNS0_10StackFrame5StateE, @function
_ZNK2v88internal10EntryFrame18ComputeCallerStateEPNS0_10StackFrame5StateE:
.LFB23209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal10EntryFrame14GetCallerStateEPNS0_10StackFrame5StateE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	72(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L372
	movq	32(%rdi), %rax
	movq	-64(%rax), %rax
	testq	%rax, %rax
	je	.L371
	movq	-8(%rax), %rdx
	leaq	-16(%rax), %rdi
	testb	$1, %dl
	jne	.L374
	sarq	%rdx
	cmpl	$21, %edx
	ja	.L374
	movl	$2098184, %ecx
	btq	%rdx, %rcx
	jnc	.L374
	cmpl	$10, %edx
	je	.L375
.L374:
	movq	-16(%rax), %rdi
.L375:
	movq	%rdi, %xmm0
	movq	%rax, %xmm1
	subq	$8, %rdi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	testq	%rax, %rax
	je	.L385
	call	*%rax
.L377:
	movq	%rax, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
.L371:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	movq	%rdi, %rax
	jmp	.L377
	.cfi_endproc
.LFE23209:
	.size	_ZNK2v88internal10EntryFrame18ComputeCallerStateEPNS0_10StackFrame5StateE, .-_ZNK2v88internal10EntryFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.section	.rodata._ZN2v88internal12_GLOBAL__N_119PrintFunctionSourceEPNS0_12StringStreamENS0_18SharedFunctionInfoENS0_4CodeE.str1.8,"aMS",@progbits,1
	.align 8
.LC36:
	.string	"--------- s o u r c e   c o d e ---------\n"
	.align 8
.LC37:
	.string	"\n-----------------------------------------\n"
	.section	.text._ZN2v88internal12_GLOBAL__N_119PrintFunctionSourceEPNS0_12StringStreamENS0_18SharedFunctionInfoENS0_4CodeE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_119PrintFunctionSourceEPNS0_12StringStreamENS0_18SharedFunctionInfoENS0_4CodeE, @function
_ZN2v88internal12_GLOBAL__N_119PrintFunctionSourceEPNS0_12StringStreamENS0_18SharedFunctionInfoENS0_4CodeE:
.LFB23407:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$472, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	_ZN2v88internal34FLAG_max_stack_trace_source_lengthE(%rip), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L386
	testq	%rdx, %rdx
	jne	.L398
.L386:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L399
	addq	$472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	movq	.LC38(%rip), %xmm1
	leaq	-320(%rbp), %r12
	movq	%rdi, %r15
	movq	%rsi, -504(%rbp)
	movq	%r12, %rdi
	leaq	-432(%rbp), %r13
	leaq	-368(%rbp), %r14
	movhps	.LC39(%rip), %xmm1
	movaps	%xmm1, -496(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -88(%rbp)
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-496(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -496(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$42, %edx
	movq	%r13, %rdi
	leaq	.LC36(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-504(%rbp), %rcx
	movq	%r13, %rdi
	movl	_ZN2v88internal34FLAG_max_stack_trace_source_lengthE(%rip), %eax
	leaq	-480(%rbp), %rsi
	leaq	-448(%rbp), %r13
	movq	%rcx, -480(%rbp)
	movl	%eax, -472(%rbp)
	call	_ZN2v88internallsERSoRKNS0_12SourceCodeOfE@PLT
	movl	$43, %edx
	leaq	.LC37(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-384(%rbp), %rax
	movq	%r13, -464(%rbp)
	leaq	-464(%rbp), %rdi
	movq	$0, -456(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L388
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L389
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L390:
	movq	-464(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -504(%rbp)
	call	strlen@PLT
	movq	%r15, %rdi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	-504(%rbp), %rsi
	movq	%rax, %rdx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	-464(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L391
	call	_ZdlPv@PLT
.L391:
	movq	.LC38(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC40(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-496(%rbp), %rdi
	je	.L392
	call	_ZdlPv@PLT
.L392:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%rbx, -432(%rbp)
	movq	-24(%rbx), %rax
	movq	%r12, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%rbx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L389:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L388:
	leaq	-352(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L390
.L399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23407:
	.size	_ZN2v88internal12_GLOBAL__N_119PrintFunctionSourceEPNS0_12StringStreamENS0_18SharedFunctionInfoENS0_4CodeE, .-_ZN2v88internal12_GLOBAL__N_119PrintFunctionSourceEPNS0_12StringStreamENS0_18SharedFunctionInfoENS0_4CodeE
	.section	.rodata._ZNK2v88internal21ArgumentsAdaptorFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi.str1.8,"aMS",@progbits,1
	.align 8
.LC41:
	.string	"arguments adaptor frame: %d->%d"
	.section	.rodata._ZNK2v88internal21ArgumentsAdaptorFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi.str1.1,"aMS",@progbits,1
.LC42:
	.string	" {\n"
.LC43:
	.string	"}\n\n"
.LC44:
	.string	"  // actual arguments\n"
.LC45:
	.string	"  [%02d] : %o"
.LC46:
	.string	"  // not passed to callee"
	.section	.text._ZNK2v88internal21ArgumentsAdaptorFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal21ArgumentsAdaptorFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.type	_ZNK2v88internal21ArgumentsAdaptorFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi, @function
_ZNK2v88internal21ArgumentsAdaptorFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi:
.LFB23409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movq	(%rdi), %rdx
	leaq	_ZNK2v88internal21ArgumentsAdaptorFrame22ComputeParametersCountEv(%rip), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	120(%rdx), %rax
	cmpq	%rdi, %rax
	jne	.L401
	movq	144(%rdx), %rax
	leaq	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi(%rip), %rcx
	cmpq	%rcx, %rax
	jne	.L402
	movq	32(%r14), %rax
	subq	$24, %rax
.L403:
	movq	(%rax), %rax
	shrq	$32, %rax
	movq	%rax, -88(%rbp)
.L404:
	movq	152(%rdx), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L405
	movq	32(%r14), %rax
	movq	-16(%rax), %rax
.L406:
	movq	23(%rax), %rax
	cmpl	$1, %r13d
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	sbbq	%rdx, %rdx
	movl	$1, %r8d
	movzwl	41(%rax), %r15d
	addq	$6, %rdx
	testl	%r13d, %r13d
	movl	%ebx, -80(%rbp)
	leaq	.LC4(%rip), %rax
	leaq	-80(%rbp), %rbx
	cmovne	%rax, %rsi
	movq	%rbx, %rcx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movl	-88(%rbp), %eax
	movq	%rbx, %rcx
	movl	$31, %edx
	movl	$2, %r8d
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	movl	%r15d, -72(%rbp)
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	testl	%r13d, %r13d
	je	.L437
	leaq	.LC42(%rip), %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movl	-88(%rbp), %eax
	testl	%eax, %eax
	jg	.L410
.L421:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC43(%rip), %rsi
	movq	%r12, %rdi
	movl	$3, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
.L400:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L438
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC44(%rip), %rsi
	movq	%r12, %rdi
	movl	$22, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	testl	%r15d, %r15d
	jle	.L434
	movl	-88(%rbp), %eax
	leaq	_ZNK2v88internal15JavaScriptFrame12GetParameterEi(%rip), %r13
	cmpl	%r15d, %eax
	cmovle	%eax, %r15d
	movq	%rbx, %rax
	movq	%r14, %rbx
	movq	%rax, %r14
	movl	%r15d, -92(%rbp)
	xorl	%r15d, %r15d
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L440:
	movq	120(%rdx), %rax
	leaq	_ZNK2v88internal21ArgumentsAdaptorFrame22ComputeParametersCountEv(%rip), %rdi
	cmpq	%rdi, %rax
	jne	.L413
	movq	144(%rdx), %rax
	leaq	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi(%rip), %rdi
	cmpq	%rdi, %rax
	jne	.L414
	movq	32(%rbx), %rax
	subq	$24, %rax
.L415:
	movq	(%rax), %rax
	shrq	$32, %rax
.L416:
	subl	%r15d, %eax
	leaq	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv(%rip), %rsi
	leal	-8(,%rax,8), %ecx
	movq	56(%rdx), %rax
	cmpq	%rsi, %rax
	jne	.L417
	movq	32(%rbx), %rax
	addq	$16, %rax
.L418:
	movslq	%ecx, %rcx
	movq	(%rcx,%rax), %rax
.L419:
	movq	%r14, %rcx
	movl	$2, %r8d
	movq	%r12, %rdi
	movl	%r15d, -80(%rbp)
	leaq	.LC45(%rip), %rsi
	movl	$13, %edx
	movq	%rax, -72(%rbp)
	addl	$1, %r15d
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$1, %edx
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	cmpl	-92(%rbp), %r15d
	jge	.L439
.L420:
	movq	(%rbx), %rdx
	movq	112(%rdx), %rax
	cmpq	%r13, %rax
	je	.L440
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L417:
	movl	%ecx, -96(%rbp)
	movq	%rbx, %rdi
	call	*%rax
	movl	-96(%rbp), %ecx
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%rbx, %rdi
	call	*%rax
	movq	(%rbx), %rdx
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L414:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	movq	(%rbx), %rdx
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L437:
	leaq	.LC30(%rip), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L434:
	xorl	%r15d, %r15d
	leaq	_ZNK2v88internal15JavaScriptFrame12GetParameterEi(%rip), %r13
.L411:
	movq	%rbx, %rax
	movq	%r14, %rbx
	movq	%rax, %r14
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L441:
	movq	120(%rdx), %rax
	leaq	_ZNK2v88internal21ArgumentsAdaptorFrame22ComputeParametersCountEv(%rip), %rdi
	cmpq	%rdi, %rax
	jne	.L423
	movq	144(%rdx), %rax
	leaq	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi(%rip), %rsi
	cmpq	%rsi, %rax
	jne	.L424
	movq	32(%rbx), %rax
	subq	$24, %rax
.L425:
	movq	(%rax), %rax
	shrq	$32, %rax
.L426:
	subl	%r15d, %eax
	leaq	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv(%rip), %rsi
	leal	-8(,%rax,8), %ecx
	movq	56(%rdx), %rax
	cmpq	%rsi, %rax
	jne	.L427
	movq	32(%rbx), %rax
	addq	$16, %rax
.L428:
	movslq	%ecx, %rcx
	movq	(%rcx,%rax), %rax
.L429:
	movq	%r14, %rcx
	movl	$2, %r8d
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	leaq	.LC45(%rip), %rsi
	movl	$13, %edx
	movl	%r15d, -80(%rbp)
	addl	$1, %r15d
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$25, %edx
	leaq	.LC46(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$1, %edx
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	cmpl	%r15d, -88(%rbp)
	jle	.L421
.L431:
	movq	(%rbx), %rdx
	movq	112(%rdx), %rax
	cmpq	%r13, %rax
	je	.L441
	movl	%r15d, %esi
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L427:
	movl	%ecx, -92(%rbp)
	movq	%rbx, %rdi
	call	*%rax
	movl	-92(%rbp), %ecx
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L423:
	movq	%rbx, %rdi
	call	*%rax
	movq	(%rbx), %rdx
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L424:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	movq	(%rbx), %rdx
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L439:
	movq	%r14, %rax
	movq	%rbx, %r14
	movq	%rax, %rbx
	cmpl	%r15d, -88(%rbp)
	jg	.L411
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L405:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L401:
	movq	%r14, %rdi
	call	*%rax
	movq	(%r14), %rdx
	movl	%eax, -88(%rbp)
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L402:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	*%rax
	movq	(%r14), %rdx
	jmp	.L403
.L438:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23409:
	.size	_ZNK2v88internal21ArgumentsAdaptorFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi, .-_ZNK2v88internal21ArgumentsAdaptorFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.section	.rodata._ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi.str1.1,"aMS",@progbits,1
.LC47:
	.string	" [%p]"
.LC48:
	.string	" ["
.LC49:
	.string	":%d] [bytecode=%p offset=%d]"
.LC50:
	.string	":~%d] [pc=%p]"
.LC51:
	.string	")"
.LC52:
	.string	","
.LC53:
	.string	"%o"
.LC54:
	.string	" {\n// optimized frame\n"
.LC55:
	.string	"}\n"
.LC56:
	.string	"  // heap-allocated locals\n"
.LC57:
	.string	"  var "
.LC58:
	.string	" = "
	.section	.rodata._ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi.str1.8,"aMS",@progbits,1
	.align 8
.LC59:
	.string	"// warning: missing context slot - inconsistent frame?"
	.align 8
.LC60:
	.string	"// warning: no context found - inconsistent frame?"
	.align 8
.LC61:
	.string	"  // expression stack (top to bottom)\n"
	.section	.rodata._ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi.str1.1
.LC62:
	.string	"  [%02d] : %o\n"
	.section	.text._ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.type	_ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi, @function
_ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi:
.LFB23408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movl	%edx, -128(%rbp)
	movq	16(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	152(%rax), %rax
	cmpq	%r14, %rax
	jne	.L443
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
.L444:
	movq	23(%rax), %rsi
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L445
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, -120(%rbp)
	movq	%rax, %rsi
.L446:
	movq	16(%rbx), %rdi
	call	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*80(%rax)
	movq	%rax, %r13
	movq	(%rbx), %rax
	movq	152(%rax), %rax
	cmpq	%r14, %rax
	jne	.L448
	movq	32(%rbx), %rax
	movq	-16(%rax), %r14
.L449:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream27PrintSecurityTokenIfChangedENS0_10JSFunctionE@PLT
	movl	-128(%rbp), %eax
	movl	%r15d, -80(%rbp)
	leaq	-80(%rbp), %r15
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rcx
	movl	$1, %r8d
	movq	%r12, %rdi
	cmpl	$1, %eax
	sbbq	%rdx, %rdx
	addq	$6, %rdx
	testl	%eax, %eax
	leaq	.LC4(%rip), %rax
	cmovne	%rax, %rsi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	(%rbx), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*176(%rax)
	movq	(%rbx), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv(%rip), %rdx
	movq	$0, -112(%rbp)
	movq	128(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L451
	movq	32(%rbx), %rax
	movq	(%rax), %rdx
	movq	-8(%rdx), %rax
	cmpq	$38, %rax
	jne	.L452
	movq	(%rdx), %rax
	movq	-8(%rax), %rax
.L452:
	cmpq	$36, %rax
	sete	%al
.L453:
	testb	%al, %al
	jne	.L512
.L454:
	leaq	-112(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream13PrintFunctionENS0_10JSFunctionENS0_6ObjectEPNS0_4CodeE@PLT
	movl	$5, %edx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movl	$1, %r8d
	leaq	.LC47(%rip), %rsi
	movq	%r14, -80(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	-120(%rbp), %rax
	movq	(%rax), %rdx
	movq	15(%rdx), %rax
	testb	$1, %al
	jne	.L455
.L457:
	andq	$-262144, %rdx
	movq	24(%rdx), %rdi
	subq	$37592, %rdi
	call	_ZN2v88internal9ScopeInfo5EmptyEPNS0_7IsolateE@PLT
.L456:
	movq	%rax, -104(%rbp)
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	31(%rax), %rax
	testb	$1, %al
	jne	.L513
.L461:
	movq	%r15, %rcx
	movl	$1, %r8d
	movq	%r12, %rdi
	movq	%r13, -80(%rbp)
	leaq	.LC33(%rip), %rsi
	movl	$8, %edx
	xorl	%r13d, %r13d
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*120(%rax)
	movl	%eax, %r14d
	testl	%eax, %eax
	jg	.L469
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L514:
	movq	%rbx, %rdi
	call	*120(%rax)
	leaq	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv(%rip), %rcx
	subl	%r13d, %eax
	leal	-8(,%rax,8), %edx
	movq	(%rbx), %rax
	movq	56(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L473
	movq	32(%rbx), %rax
	addq	$16, %rax
.L474:
	movslq	%edx, %rdx
	movq	(%rdx,%rax), %rax
.L475:
	movq	%r15, %rcx
	movl	$1, %r8d
	movq	%r12, %rdi
	addl	$1, %r13d
	leaq	.LC53(%rip), %rsi
	movl	$2, %edx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	cmpl	%r13d, %r14d
	je	.L476
.L469:
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	.LC52(%rip), %rsi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	(%rbx), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame12GetParameterEi(%rip), %rcx
	movq	112(%rax), %rdx
	cmpq	%rcx, %rdx
	je	.L514
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L473:
	movl	%edx, -136(%rbp)
	movq	%rbx, %rdi
	call	*%rax
	movl	-136(%rbp), %edx
	jmp	.L474
	.p2align 4,,10
	.p2align 3
.L476:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC51(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movl	-128(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L515
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*8(%rax)
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	cmpl	$4, %eax
	je	.L516
	movl	$3, %edx
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	-104(%rbp), %rax
	movl	$0, -136(%rbp)
	movl	11(%rax), %edx
	testl	%edx, %edx
	jle	.L479
	movq	31(%rax), %rax
	shrq	$32, %rax
	movq	%rax, -136(%rbp)
.L479:
	movq	(%rbx), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*144(%rax)
	subq	24(%rbx), %rax
	movq	%rbx, %rdi
	addq	$8, %rax
	shrq	$3, %rax
	movq	%rax, -144(%rbp)
	movq	(%rbx), %rax
	call	*96(%rax)
	testb	$1, %al
	jne	.L517
.L480:
	xorl	%r14d, %r14d
.L481:
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	jg	.L483
	movq	-144(%rbp), %rax
	leal	-1(%rax), %r14d
	testl	%eax, %eax
	jg	.L518
.L508:
	testl	%r14d, %r14d
	js	.L493
.L491:
	leaq	.LC62(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L494:
	movq	(%rbx), %rax
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	*144(%rax)
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movl	$2, %r8d
	movl	%r14d, -80(%rbp)
	movl	$14, %edx
	subl	$1, %r14d
	movq	%rax, -72(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	cmpl	$-1, %r14d
	jne	.L494
.L493:
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal12_GLOBAL__N_119PrintFunctionSourceEPNS0_12StringStreamENS0_18SharedFunctionInfoENS0_4CodeE
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$3, %edx
	leaq	.LC43(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
.L442:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L519
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC61(%rip), %rsi
	movq	%r12, %rdi
	movl	$38, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L483:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC56(%rip), %rsi
	movq	%r12, %rdi
	movl	$27, %edx
	xorl	%r13d, %r13d
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	leaq	-104(%rbp), %rax
	movq	%rbx, -152(%rbp)
	movq	%r15, -128(%rbp)
	movq	%rax, %rbx
	movl	-136(%rbp), %r15d
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L521:
	leal	48(,%r13,8), %eax
	movl	$1, %r8d
	movl	$2, %edx
	movq	%r12, %rdi
	cltq
	leaq	.LC53(%rip), %rsi
	movq	-1(%r14,%rax), %rax
	movq	-128(%rbp), %rcx
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
.L488:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	addl	$1, %r13d
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	cmpl	%r15d, %r13d
	je	.L520
.L490:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$6, %edx
	movq	%r12, %rdi
	leaq	.LC57(%rip), %rsi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	_ZNK2v88internal9ScopeInfo16ContextLocalNameEi@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE@PLT
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$3, %edx
	leaq	.LC58(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	testq	%r14, %r14
	je	.L486
	leal	4(%r13), %eax
	cmpl	%eax, 11(%r14)
	jg	.L521
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC59(%rip), %rsi
	movq	%r12, %rdi
	movl	$54, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L517:
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	subw	$138, %ax
	cmpw	$9, %ax
	ja	.L480
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*96(%rax)
	movq	%rax, %r14
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L522:
	movq	23(%r14), %r14
.L482:
	movq	-1(%r14), %rax
	cmpw	$147, 11(%rax)
	je	.L522
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L486:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC60(%rip), %rsi
	movq	%r12, %rdi
	movl	$50, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L445:
	movq	41088(%r13), %rax
	movq	%rax, -120(%rbp)
	cmpq	%rax, 41096(%r13)
	je	.L523
.L447:
	movq	-120(%rbp), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rcx)
	movq	%rcx, %rsi
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L513:
	movq	-1(%rax), %rcx
	leaq	-1(%rax), %rdx
	cmpw	$86, 11(%rcx)
	je	.L524
.L495:
	movq	(%rdx), %rdx
	cmpw	$96, 11(%rdx)
	jne	.L461
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC48(%rip), %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	movq	%rax, -96(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	-96(%rbp), %rax
	movq	%r12, %rdi
	movq	15(%rax), %rsi
	call	_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE@PLT
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*8(%rax)
	cmpl	$12, %eax
	jne	.L463
	movq	(%rbx), %rax
	leaq	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi(%rip), %r14
	movq	144(%rax), %rdx
	cmpq	%r14, %rdx
	jne	.L464
	movq	32(%rbx), %rax
	subq	$24, %rax
.L465:
	movq	(%rax), %rcx
	cmpq	%r14, %rdx
	jne	.L466
	movq	32(%rbx), %rax
	subq	$32, %rax
.L467:
	movq	(%rax), %r14
	leaq	-88(%rbp), %rdi
	movq	%rcx, -88(%rbp)
	movq	%rcx, -136(%rbp)
	sarq	$32, %r14
	subl	$53, %r14d
	movl	%r14d, %esi
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	leaq	-96(%rbp), %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal6Script13GetLineNumberEi@PLT
	movq	-136(%rbp), %rcx
	movl	$3, %r8d
	leaq	.LC49(%rip), %rsi
	addl	$1, %eax
	movl	$28, %edx
	movq	%r12, %rdi
	movl	%r14d, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r15, %rcx
	movl	%eax, -80(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L455:
	movq	-1(%rax), %rcx
	cmpw	$136, 11(%rcx)
	jne	.L457
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L512:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	movl	$4, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L520:
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rbx
	movq	-128(%rbp), %r15
	leal	-1(%rax), %r14d
	testl	%eax, %eax
	jle	.L508
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L515:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L443:
	call	*%rax
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L448:
	movq	%rbx, %rdi
	call	*%rax
	movq	%rax, %r14
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L516:
	movq	%r12, %rdi
	leaq	.LC54(%rip), %rsi
	movl	$22, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	-120(%rbp), %rax
	movq	-112(%rbp), %rdx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal12_GLOBAL__N_119PrintFunctionSourceEPNS0_12StringStreamENS0_18SharedFunctionInfoENS0_4CodeE
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movl	$2, %edx
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L524:
	movq	23(%rax), %rax
	testb	$1, %al
	je	.L461
	leaq	-1(%rax), %rdx
	jmp	.L495
.L523:
	movq	%r13, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, -120(%rbp)
	jmp	.L447
.L463:
	movq	-120(%rbp), %rax
	leaq	-88(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo13StartPositionEv@PLT
	leaq	-96(%rbp), %rdi
	movl	%eax, %esi
	call	_ZNK2v88internal6Script13GetLineNumberEi@PLT
	movq	40(%rbx), %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	addl	$1, %eax
	movl	$2, %r8d
	leaq	.LC50(%rip), %rsi
	movq	(%rdx), %rdx
	movl	%eax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movl	$13, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L461
.L464:
	movl	$-2, %esi
	movq	%rbx, %rdi
	call	*%rdx
	movq	(%rbx), %rdx
	movq	144(%rdx), %rdx
	jmp	.L465
.L466:
	movq	%rcx, -136(%rbp)
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*%rdx
	movq	-136(%rbp), %rcx
	jmp	.L467
.L519:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23408:
	.size	_ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi, .-_ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.section	.text._ZN2v88internal22StackFrameIteratorBaseC2EPNS0_7IsolateEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StackFrameIteratorBaseC2EPNS0_7IsolateEb
	.type	_ZN2v88internal22StackFrameIteratorBaseC2EPNS0_7IsolateEb, @function
_ZN2v88internal22StackFrameIteratorBaseC2EPNS0_7IsolateEb:
.LFB23164:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN2v88internal10EntryFrameE(%rip), %rax
	movq	%rdi, %xmm1
	pxor	%xmm0, %xmm0
	movq	%rsi, (%rdi)
	movq	%rax, 8(%rdi)
	leaq	16+_ZTVN2v88internal19ConstructEntryFrameE(%rip), %rax
	movq	%rsi, %xmm2
	movq	%rax, 72(%rdi)
	leaq	16+_ZTVN2v88internal9ExitFrameE(%rip), %rax
	punpcklqdq	%xmm2, %xmm1
	movq	%rax, 136(%rdi)
	leaq	16+_ZTVN2v88internal14OptimizedFrameE(%rip), %rax
	movq	%rax, 200(%rdi)
	leaq	16+_ZTVN2v88internal17WasmCompiledFrameE(%rip), %rax
	movq	%rax, 264(%rdi)
	leaq	16+_ZTVN2v88internal13WasmToJsFrameE(%rip), %rax
	movq	%rax, 328(%rdi)
	leaq	16+_ZTVN2v88internal13JsToWasmFrameE(%rip), %rax
	movq	%rdi, 16(%rdi)
	movq	%rsi, 24(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 128(%rdi)
	movq	$0, 192(%rdi)
	movq	$0, 256(%rdi)
	movq	$0, 320(%rdi)
	movq	$0, 384(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm1, 80(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm1, 144(%rdi)
	movups	%xmm0, 160(%rdi)
	movups	%xmm0, 176(%rdi)
	movups	%xmm1, 208(%rdi)
	movups	%xmm0, 224(%rdi)
	movups	%xmm0, 240(%rdi)
	movups	%xmm1, 272(%rdi)
	movups	%xmm0, 288(%rdi)
	movups	%xmm0, 304(%rdi)
	movups	%xmm1, 336(%rdi)
	movups	%xmm0, 352(%rdi)
	movups	%xmm0, 368(%rdi)
	movups	%xmm1, 400(%rdi)
	movq	%rax, 392(%rdi)
	leaq	16+_ZTVN2v88internal25WasmInterpreterEntryFrameE(%rip), %rax
	movq	%rax, 456(%rdi)
	leaq	16+_ZTVN2v88internal15CWasmEntryFrameE(%rip), %rax
	movq	%rax, 520(%rdi)
	leaq	16+_ZTVN2v88internal13WasmExitFrameE(%rip), %rax
	movq	%rax, 584(%rdi)
	leaq	16+_ZTVN2v88internal20WasmCompileLazyFrameE(%rip), %rax
	movq	%rax, 648(%rdi)
	leaq	16+_ZTVN2v88internal16InterpretedFrameE(%rip), %rax
	movq	%rax, 712(%rdi)
	leaq	16+_ZTVN2v88internal9StubFrameE(%rip), %rax
	movq	$0, 448(%rdi)
	movq	$0, 512(%rdi)
	movq	$0, 576(%rdi)
	movq	$0, 640(%rdi)
	movq	$0, 704(%rdi)
	movq	$0, 768(%rdi)
	movups	%xmm0, 416(%rdi)
	movups	%xmm0, 432(%rdi)
	movups	%xmm1, 464(%rdi)
	movups	%xmm0, 480(%rdi)
	movups	%xmm0, 496(%rdi)
	movups	%xmm1, 528(%rdi)
	movups	%xmm0, 544(%rdi)
	movups	%xmm0, 560(%rdi)
	movups	%xmm1, 592(%rdi)
	movups	%xmm0, 608(%rdi)
	movups	%xmm0, 624(%rdi)
	movups	%xmm1, 656(%rdi)
	movups	%xmm0, 672(%rdi)
	movups	%xmm0, 688(%rdi)
	movups	%xmm1, 720(%rdi)
	movups	%xmm0, 736(%rdi)
	movups	%xmm0, 752(%rdi)
	movups	%xmm1, 784(%rdi)
	movups	%xmm0, 800(%rdi)
	movups	%xmm0, 816(%rdi)
	movq	$0, 832(%rdi)
	movq	%rax, 776(%rdi)
	leaq	16+_ZTVN2v88internal24BuiltinContinuationFrameE(%rip), %rax
	movq	%rax, 840(%rdi)
	leaq	16+_ZTVN2v88internal34JavaScriptBuiltinContinuationFrameE(%rip), %rax
	movq	%rax, 904(%rdi)
	leaq	16+_ZTVN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameE(%rip), %rax
	movq	%rax, 968(%rdi)
	leaq	16+_ZTVN2v88internal13InternalFrameE(%rip), %rax
	movq	%rax, 1032(%rdi)
	leaq	16+_ZTVN2v88internal14ConstructFrameE(%rip), %rax
	movq	%rax, 1096(%rdi)
	leaq	16+_ZTVN2v88internal21ArgumentsAdaptorFrameE(%rip), %rax
	movq	%rax, 1160(%rdi)
	leaq	16+_ZTVN2v88internal12BuiltinFrameE(%rip), %rax
	movq	$0, 896(%rdi)
	movq	$0, 960(%rdi)
	movq	$0, 1024(%rdi)
	movq	$0, 1088(%rdi)
	movq	$0, 1152(%rdi)
	movq	$0, 1216(%rdi)
	movups	%xmm1, 848(%rdi)
	movups	%xmm0, 864(%rdi)
	movups	%xmm0, 880(%rdi)
	movups	%xmm1, 912(%rdi)
	movups	%xmm0, 928(%rdi)
	movups	%xmm0, 944(%rdi)
	movups	%xmm1, 976(%rdi)
	movups	%xmm0, 992(%rdi)
	movups	%xmm0, 1008(%rdi)
	movups	%xmm1, 1040(%rdi)
	movups	%xmm0, 1056(%rdi)
	movups	%xmm0, 1072(%rdi)
	movups	%xmm1, 1104(%rdi)
	movups	%xmm0, 1120(%rdi)
	movups	%xmm0, 1136(%rdi)
	movups	%xmm1, 1168(%rdi)
	movups	%xmm0, 1184(%rdi)
	movups	%xmm0, 1200(%rdi)
	movups	%xmm1, 1232(%rdi)
	movups	%xmm0, 1248(%rdi)
	movq	%rax, 1224(%rdi)
	leaq	16+_ZTVN2v88internal16BuiltinExitFrameE(%rip), %rax
	movq	%rax, 1288(%rdi)
	leaq	16+_ZTVN2v88internal11NativeFrameE(%rip), %rax
	movq	$0, 1280(%rdi)
	movq	$0, 1344(%rdi)
	movq	%rax, 1352(%rdi)
	movq	$0, 1424(%rdi)
	movb	%dl, 1432(%rdi)
	movups	%xmm0, 1264(%rdi)
	movups	%xmm1, 1296(%rdi)
	movups	%xmm0, 1312(%rdi)
	movups	%xmm0, 1328(%rdi)
	movups	%xmm1, 1360(%rdi)
	movups	%xmm0, 1376(%rdi)
	movups	%xmm0, 1392(%rdi)
	movups	%xmm0, 1408(%rdi)
	ret
	.cfi_endproc
.LFE23164:
	.size	_ZN2v88internal22StackFrameIteratorBaseC2EPNS0_7IsolateEb, .-_ZN2v88internal22StackFrameIteratorBaseC2EPNS0_7IsolateEb
	.globl	_ZN2v88internal22StackFrameIteratorBaseC1EPNS0_7IsolateEb
	.set	_ZN2v88internal22StackFrameIteratorBaseC1EPNS0_7IsolateEb,_ZN2v88internal22StackFrameIteratorBaseC2EPNS0_7IsolateEb
	.section	.text._ZN2v88internal18StackFrameIterator7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18StackFrameIterator7AdvanceEv
	.type	_ZN2v88internal18StackFrameIterator7AdvanceEv, @function
_ZN2v88internal18StackFrameIterator7AdvanceEv:
.LFB23172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-80(%rbp), %rsi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	1416(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	1416(%rbx), %rdi
	movq	1424(%rbx), %r12
	movl	%eax, %r13d
	movq	(%rdi), %rax
	movq	32(%rdi), %r14
	call	*8(%rax)
	cmpl	$9, %eax
	jne	.L527
	movq	(%r12), %r12
.L527:
	testq	%r12, %r12
	je	.L528
	cmpq	%r14, %r12
	jbe	.L529
.L528:
	movq	%r12, 1424(%rbx)
	cmpl	$22, %r13d
	ja	.L557
	leaq	.L533(%rip), %rdx
	movslq	(%rdx,%r13,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal18StackFrameIterator7AdvanceEv,"a",@progbits
	.align 4
	.align 4
.L533:
	.long	.L557-.L533
	.long	.L554-.L533
	.long	.L553-.L533
	.long	.L552-.L533
	.long	.L551-.L533
	.long	.L550-.L533
	.long	.L549-.L533
	.long	.L548-.L533
	.long	.L547-.L533
	.long	.L546-.L533
	.long	.L545-.L533
	.long	.L544-.L533
	.long	.L543-.L533
	.long	.L542-.L533
	.long	.L541-.L533
	.long	.L540-.L533
	.long	.L539-.L533
	.long	.L538-.L533
	.long	.L537-.L533
	.long	.L536-.L533
	.long	.L535-.L533
	.long	.L534-.L533
	.long	.L532-.L533
	.section	.text._ZN2v88internal18StackFrameIterator7AdvanceEv
	.p2align 4,,10
	.p2align 3
.L530:
	cmpq	%r12, %r14
	jb	.L528
.L529:
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L530
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L532:
	leaq	1352(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L555:
	movq	-48(%rbp), %rdx
	movdqa	-80(%rbp), %xmm1
	movdqa	-64(%rbp), %xmm2
	movq	%rdx, 56(%rax)
	movups	%xmm1, 24(%rax)
	movups	%xmm2, 40(%rax)
.L531:
	movq	%rax, 1416(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L563
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L534:
	.cfi_restore_state
	leaq	1288(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L535:
	leaq	1224(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L536:
	leaq	1160(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L537:
	leaq	1096(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L538:
	leaq	1032(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L539:
	leaq	968(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L540:
	leaq	904(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L541:
	leaq	840(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L542:
	leaq	776(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L543:
	leaq	712(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L544:
	leaq	648(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L545:
	leaq	584(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L546:
	leaq	520(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L547:
	leaq	456(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L548:
	leaq	392(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L549:
	leaq	328(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L550:
	leaq	264(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L551:
	leaq	200(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L552:
	leaq	136(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L553:
	leaq	72(%rbx), %rax
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L554:
	leaq	8(%rbx), %rax
	jmp	.L555
.L563:
	call	__stack_chk_fail@PLT
.L557:
	xorl	%eax, %eax
	jmp	.L531
	.cfi_endproc
.LFE23172:
	.size	_ZN2v88internal18StackFrameIterator7AdvanceEv, .-_ZN2v88internal18StackFrameIterator7AdvanceEv
	.section	.text._ZN2v88internal18StackFrameIterator5ResetEPNS0_14ThreadLocalTopE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18StackFrameIterator5ResetEPNS0_14ThreadLocalTopE
	.type	_ZN2v88internal18StackFrameIterator5ResetEPNS0_14ThreadLocalTopE, @function
_ZN2v88internal18StackFrameIterator5ResetEPNS0_14ThreadLocalTopE:
.LFB23173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	112(%rsi), %r13
	testq	%r13, %r13
	je	.L602
	movq	-8(%r13), %rax
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rdx
	leaq	-16(%r13), %r14
	testb	$1, %al
	jne	.L567
	sarq	%rax
	movl	%eax, %r15d
	cmpl	$21, %eax
	ja	.L567
	movl	$2098184, %ecx
	btq	%rax, %rcx
	jnc	.L567
	cmpl	$10, %eax
	je	.L603
	movq	-16(%r13), %r14
	leaq	-8(%r14), %rdi
	movq	%rdi, %rax
	testq	%rdx, %rdx
	je	.L571
.L570:
	call	*%rdx
.L571:
	movq	120(%r12), %rdx
	movq	%rdx, 1424(%rbx)
	cmpl	$21, %r15d
	ja	.L596
	leaq	.L573(%rip), %rcx
	movslq	(%rcx,%r15,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN2v88internal18StackFrameIterator5ResetEPNS0_14ThreadLocalTopE,"a",@progbits
	.align 4
	.align 4
.L573:
	.long	.L596-.L573
	.long	.L593-.L573
	.long	.L592-.L573
	.long	.L591-.L573
	.long	.L590-.L573
	.long	.L589-.L573
	.long	.L588-.L573
	.long	.L587-.L573
	.long	.L586-.L573
	.long	.L585-.L573
	.long	.L584-.L573
	.long	.L583-.L573
	.long	.L582-.L573
	.long	.L581-.L573
	.long	.L580-.L573
	.long	.L579-.L573
	.long	.L578-.L573
	.long	.L577-.L573
	.long	.L576-.L573
	.long	.L575-.L573
	.long	.L574-.L573
	.long	.L572-.L573
	.section	.text._ZN2v88internal18StackFrameIterator5ResetEPNS0_14ThreadLocalTopE
	.p2align 4,,10
	.p2align 3
.L567:
	movq	-16(%r13), %r14
	leaq	-8(%r14), %rdi
	testq	%rdx, %rdx
	je	.L595
	movl	$3, %r15d
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L602:
	movq	120(%rsi), %rax
	xorl	%edx, %edx
	movq	%rax, 1424(%rdi)
.L566:
	movq	%rdx, 1416(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L603:
	.cfi_restore_state
	leaq	-24(%r13), %rdi
	testq	%rdx, %rdx
	jne	.L570
	movq	120(%rsi), %rdx
	movq	%rdi, %rax
	movq	%rdx, 1424(%rbx)
.L584:
	leaq	584(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L572:
	leaq	1288(%rbx), %rdx
	.p2align 4,,10
	.p2align 3
.L594:
	movq	%r14, %xmm0
	movq	%r13, %xmm1
	movq	%rax, 40(%rdx)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 48(%rdx)
	movq	$0, 56(%rdx)
	movups	%xmm0, 24(%rdx)
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L593:
	leaq	8(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L592:
	leaq	72(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L595:
	movq	120(%r12), %rdx
	movq	%rdi, %rax
	movq	%rdx, 1424(%rbx)
.L591:
	leaq	136(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L590:
	leaq	200(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L589:
	leaq	264(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L588:
	leaq	328(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L587:
	leaq	392(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L586:
	leaq	456(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L585:
	leaq	520(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L583:
	leaq	648(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L582:
	leaq	712(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L581:
	leaq	776(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L580:
	leaq	840(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L579:
	leaq	904(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L578:
	leaq	968(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L577:
	leaq	1032(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L576:
	leaq	1096(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L575:
	leaq	1160(%rbx), %rdx
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L574:
	leaq	1224(%rbx), %rdx
	jmp	.L594
.L596:
	xorl	%edx, %edx
	jmp	.L566
	.cfi_endproc
.LFE23173:
	.size	_ZN2v88internal18StackFrameIterator5ResetEPNS0_14ThreadLocalTopE, .-_ZN2v88internal18StackFrameIterator5ResetEPNS0_14ThreadLocalTopE
	.section	.text._ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeEPNS2_5StateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeEPNS2_5StateE
	.type	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeEPNS2_5StateE, @function
_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeEPNS2_5StateE:
.LFB23174:
	.cfi_startproc
	endbr64
	cmpl	$22, %esi
	ja	.L630
	leaq	.L607(%rip), %rcx
	movl	%esi, %esi
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeEPNS2_5StateE,"a",@progbits
	.align 4
	.align 4
.L607:
	.long	.L630-.L607
	.long	.L628-.L607
	.long	.L627-.L607
	.long	.L626-.L607
	.long	.L625-.L607
	.long	.L624-.L607
	.long	.L623-.L607
	.long	.L622-.L607
	.long	.L621-.L607
	.long	.L620-.L607
	.long	.L619-.L607
	.long	.L618-.L607
	.long	.L617-.L607
	.long	.L616-.L607
	.long	.L615-.L607
	.long	.L614-.L607
	.long	.L613-.L607
	.long	.L612-.L607
	.long	.L611-.L607
	.long	.L610-.L607
	.long	.L609-.L607
	.long	.L608-.L607
	.long	.L606-.L607
	.section	.text._ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeEPNS2_5StateE
	.p2align 4,,10
	.p2align 3
.L606:
	leaq	1352(%rdi), %rax
.L629:
	movdqu	(%rdx), %xmm0
	movups	%xmm0, 24(%rax)
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 40(%rax)
	movq	32(%rdx), %rdx
	movq	%rdx, 56(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L608:
	leaq	1288(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L609:
	leaq	1224(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L610:
	leaq	1160(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L611:
	leaq	1096(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L612:
	leaq	1032(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L613:
	leaq	968(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L614:
	leaq	904(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L615:
	leaq	840(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L616:
	leaq	776(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L617:
	leaq	712(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L618:
	leaq	648(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L619:
	leaq	584(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L620:
	leaq	520(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L621:
	leaq	456(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L622:
	leaq	392(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L623:
	leaq	328(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L624:
	leaq	264(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L625:
	leaq	200(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L626:
	leaq	136(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L627:
	leaq	72(%rdi), %rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L628:
	leaq	8(%rdi), %rax
	jmp	.L629
.L630:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23174:
	.size	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeEPNS2_5StateE, .-_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeEPNS2_5StateE
	.section	.text._ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE
	.type	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE, @function
_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE:
.LFB23175:
	.cfi_startproc
	endbr64
	cmpl	$22, %esi
	ja	.L656
	leaq	.L634(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE,"a",@progbits
	.align 4
	.align 4
.L634:
	.long	.L656-.L634
	.long	.L655-.L634
	.long	.L654-.L634
	.long	.L653-.L634
	.long	.L652-.L634
	.long	.L651-.L634
	.long	.L650-.L634
	.long	.L649-.L634
	.long	.L648-.L634
	.long	.L647-.L634
	.long	.L646-.L634
	.long	.L645-.L634
	.long	.L644-.L634
	.long	.L643-.L634
	.long	.L642-.L634
	.long	.L641-.L634
	.long	.L640-.L634
	.long	.L639-.L634
	.long	.L638-.L634
	.long	.L637-.L634
	.long	.L636-.L634
	.long	.L635-.L634
	.long	.L633-.L634
	.section	.text._ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	1352(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L635:
	leaq	1288(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L636:
	leaq	1224(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L637:
	leaq	1160(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L638:
	leaq	1096(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	leaq	1032(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L640:
	leaq	968(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L641:
	leaq	904(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	leaq	840(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L643:
	leaq	776(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L644:
	leaq	712(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L645:
	leaq	648(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L646:
	leaq	584(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L647:
	leaq	520(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L648:
	leaq	456(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L649:
	leaq	392(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L650:
	leaq	328(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	leaq	264(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L652:
	leaq	200(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L653:
	leaq	136(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L654:
	leaq	72(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	leaq	8(%rdi), %rax
	ret
.L656:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE23175:
	.size	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE, .-_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE
	.section	.text._ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateEPNS0_14ThreadLocalTopE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateEPNS0_14ThreadLocalTopE
	.type	_ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateEPNS0_14ThreadLocalTopE, @function
_ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateEPNS0_14ThreadLocalTopE:
.LFB23170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10EntryFrameE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, 8(%rdi)
	leaq	16+_ZTVN2v88internal19ConstructEntryFrameE(%rip), %rax
	movq	%rax, 72(%rdi)
	leaq	16+_ZTVN2v88internal9ExitFrameE(%rip), %rax
	movq	%rax, 136(%rdi)
	leaq	16+_ZTVN2v88internal14OptimizedFrameE(%rip), %rax
	movq	%rax, 200(%rdi)
	leaq	16+_ZTVN2v88internal17WasmCompiledFrameE(%rip), %rax
	movq	%rsi, (%rdi)
	movq	%rdi, 16(%rdi)
	movq	%rsi, 24(%rdi)
	movq	$0, 64(%rdi)
	movq	%rdi, 80(%rdi)
	movq	%rsi, 88(%rdi)
	movq	$0, 128(%rdi)
	movq	%rdi, 144(%rdi)
	movq	%rsi, 152(%rdi)
	movq	$0, 192(%rdi)
	movq	%rdi, 208(%rdi)
	movq	%rsi, 216(%rdi)
	movq	$0, 256(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 160(%rdi)
	movups	%xmm0, 176(%rdi)
	movups	%xmm0, 224(%rdi)
	movups	%xmm0, 240(%rdi)
	movq	%rdi, 272(%rdi)
	movq	%rax, 264(%rdi)
	leaq	16+_ZTVN2v88internal13WasmToJsFrameE(%rip), %rax
	movq	%rax, 328(%rdi)
	leaq	16+_ZTVN2v88internal13JsToWasmFrameE(%rip), %rax
	movq	%rax, 392(%rdi)
	leaq	16+_ZTVN2v88internal25WasmInterpreterEntryFrameE(%rip), %rax
	movq	%rax, 456(%rdi)
	leaq	16+_ZTVN2v88internal15CWasmEntryFrameE(%rip), %rax
	movq	%rax, 520(%rdi)
	leaq	16+_ZTVN2v88internal13WasmExitFrameE(%rip), %rax
	movq	%rsi, 280(%rdi)
	movq	$0, 320(%rdi)
	movq	%rdi, 336(%rdi)
	movq	%rsi, 344(%rdi)
	movq	$0, 384(%rdi)
	movq	%rdi, 400(%rdi)
	movq	%rsi, 408(%rdi)
	movq	$0, 448(%rdi)
	movq	%rdi, 464(%rdi)
	movq	%rsi, 472(%rdi)
	movq	$0, 512(%rdi)
	movq	%rdi, 528(%rdi)
	movq	%rsi, 536(%rdi)
	movq	$0, 576(%rdi)
	movq	%rdi, 592(%rdi)
	movq	%rsi, 600(%rdi)
	movups	%xmm0, 288(%rdi)
	movups	%xmm0, 304(%rdi)
	movups	%xmm0, 352(%rdi)
	movups	%xmm0, 368(%rdi)
	movups	%xmm0, 416(%rdi)
	movups	%xmm0, 432(%rdi)
	movups	%xmm0, 480(%rdi)
	movups	%xmm0, 496(%rdi)
	movups	%xmm0, 544(%rdi)
	movups	%xmm0, 560(%rdi)
	movups	%xmm0, 608(%rdi)
	movups	%xmm0, 624(%rdi)
	movq	%rax, 584(%rdi)
	leaq	16+_ZTVN2v88internal20WasmCompileLazyFrameE(%rip), %rax
	movq	%rax, 648(%rdi)
	leaq	16+_ZTVN2v88internal16InterpretedFrameE(%rip), %rax
	movq	%rax, 712(%rdi)
	leaq	16+_ZTVN2v88internal9StubFrameE(%rip), %rax
	movq	%rax, 776(%rdi)
	leaq	16+_ZTVN2v88internal24BuiltinContinuationFrameE(%rip), %rax
	movq	%rax, 840(%rdi)
	leaq	16+_ZTVN2v88internal34JavaScriptBuiltinContinuationFrameE(%rip), %rax
	movq	%rax, 904(%rdi)
	leaq	16+_ZTVN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameE(%rip), %rax
	movq	$0, 640(%rdi)
	movq	%rdi, 656(%rdi)
	movq	%rsi, 664(%rdi)
	movq	$0, 704(%rdi)
	movq	%rdi, 720(%rdi)
	movq	%rsi, 728(%rdi)
	movq	$0, 768(%rdi)
	movq	%rdi, 784(%rdi)
	movq	%rsi, 792(%rdi)
	movq	$0, 832(%rdi)
	movq	%rdi, 848(%rdi)
	movq	%rsi, 856(%rdi)
	movq	$0, 896(%rdi)
	movq	%rdi, 912(%rdi)
	movq	%rsi, 920(%rdi)
	movq	$0, 960(%rdi)
	movups	%xmm0, 672(%rdi)
	movups	%xmm0, 688(%rdi)
	movups	%xmm0, 736(%rdi)
	movups	%xmm0, 752(%rdi)
	movups	%xmm0, 800(%rdi)
	movups	%xmm0, 816(%rdi)
	movups	%xmm0, 864(%rdi)
	movups	%xmm0, 880(%rdi)
	movups	%xmm0, 928(%rdi)
	movups	%xmm0, 944(%rdi)
	movq	%rdi, 976(%rdi)
	movq	%rax, 968(%rdi)
	leaq	16+_ZTVN2v88internal13InternalFrameE(%rip), %rax
	movq	%rax, 1032(%rdi)
	leaq	16+_ZTVN2v88internal14ConstructFrameE(%rip), %rax
	movq	%rax, 1096(%rdi)
	leaq	16+_ZTVN2v88internal21ArgumentsAdaptorFrameE(%rip), %rax
	movq	%rax, 1160(%rdi)
	leaq	16+_ZTVN2v88internal12BuiltinFrameE(%rip), %rax
	movq	%rax, 1224(%rdi)
	leaq	16+_ZTVN2v88internal16BuiltinExitFrameE(%rip), %rax
	movq	%rsi, 984(%rdi)
	movq	$0, 1024(%rdi)
	movq	%rdi, 1040(%rdi)
	movq	%rsi, 1048(%rdi)
	movq	$0, 1088(%rdi)
	movq	%rdi, 1104(%rdi)
	movq	%rsi, 1112(%rdi)
	movq	$0, 1152(%rdi)
	movq	%rdi, 1168(%rdi)
	movq	%rsi, 1176(%rdi)
	movq	$0, 1216(%rdi)
	movq	%rdi, 1232(%rdi)
	movq	%rsi, 1240(%rdi)
	movq	$0, 1280(%rdi)
	movq	%rdi, 1296(%rdi)
	movq	%rsi, 1304(%rdi)
	movups	%xmm0, 992(%rdi)
	movups	%xmm0, 1008(%rdi)
	movups	%xmm0, 1056(%rdi)
	movups	%xmm0, 1072(%rdi)
	movups	%xmm0, 1120(%rdi)
	movups	%xmm0, 1136(%rdi)
	movups	%xmm0, 1184(%rdi)
	movups	%xmm0, 1200(%rdi)
	movups	%xmm0, 1248(%rdi)
	movups	%xmm0, 1264(%rdi)
	movups	%xmm0, 1312(%rdi)
	movups	%xmm0, 1328(%rdi)
	movq	%rax, 1288(%rdi)
	leaq	16+_ZTVN2v88internal11NativeFrameE(%rip), %rax
	movq	$0, 1344(%rdi)
	movq	%rdi, 1360(%rdi)
	movq	%rsi, 1368(%rdi)
	movq	%rax, 1352(%rdi)
	movq	$0, 1424(%rdi)
	movb	$1, 1432(%rdi)
	movups	%xmm0, 1376(%rdi)
	movups	%xmm0, 1392(%rdi)
	movups	%xmm0, 1408(%rdi)
	movq	112(%rdx), %r14
	testq	%r14, %r14
	je	.L664
	movq	-8(%r14), %rax
	leaq	-16(%r14), %r13
	testb	$1, %al
	jne	.L667
	sarq	%rax
	movl	%eax, %r15d
	cmpl	$21, %eax
	jbe	.L672
.L667:
	movl	$3, %r15d
.L659:
	movq	-16(%r14), %r13
.L660:
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	leaq	-8(%r13), %rdi
	movq	%rdi, %rcx
	testq	%rax, %rax
	je	.L658
	call	*%rax
	movq	%rax, %rcx
.L658:
	movq	120(%r12), %rax
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	%rax, 1424(%rbx)
	call	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE
	testq	%rax, %rax
	je	.L663
	movq	%r13, %xmm0
	movq	%r14, %xmm1
	movq	%rcx, 40(%rax)
	movq	$0, 48(%rax)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 56(%rax)
	movups	%xmm0, 24(%rax)
.L663:
	movq	%rax, 1416(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L672:
	.cfi_restore_state
	movl	$2098184, %edx
	btq	%rax, %rdx
	jnc	.L667
	cmpl	$10, %eax
	jne	.L659
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L664:
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	jmp	.L658
	.cfi_endproc
.LFE23170:
	.size	_ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateEPNS0_14ThreadLocalTopE, .-_ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateEPNS0_14ThreadLocalTopE
	.globl	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE
	.set	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateEPNS0_14ThreadLocalTopE,_ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateEPNS0_14ThreadLocalTopE
	.section	.text._ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateE
	.type	_ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateE, @function
_ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateE:
.LFB23167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10EntryFrameE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, 8(%rdi)
	leaq	16+_ZTVN2v88internal19ConstructEntryFrameE(%rip), %rax
	movq	%rax, 72(%rdi)
	leaq	16+_ZTVN2v88internal9ExitFrameE(%rip), %rax
	movq	%rax, 136(%rdi)
	leaq	16+_ZTVN2v88internal14OptimizedFrameE(%rip), %rax
	movq	%rax, 200(%rdi)
	leaq	16+_ZTVN2v88internal17WasmCompiledFrameE(%rip), %rax
	movq	%rsi, (%rdi)
	movq	%rdi, 16(%rdi)
	movq	%rsi, 24(%rdi)
	movq	$0, 64(%rdi)
	movq	%rdi, 80(%rdi)
	movq	%rsi, 88(%rdi)
	movq	$0, 128(%rdi)
	movq	%rdi, 144(%rdi)
	movq	%rsi, 152(%rdi)
	movq	$0, 192(%rdi)
	movq	%rdi, 208(%rdi)
	movq	%rsi, 216(%rdi)
	movq	$0, 256(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 160(%rdi)
	movups	%xmm0, 176(%rdi)
	movups	%xmm0, 224(%rdi)
	movups	%xmm0, 240(%rdi)
	movq	%rdi, 272(%rdi)
	movq	%rax, 264(%rdi)
	leaq	16+_ZTVN2v88internal13WasmToJsFrameE(%rip), %rax
	movq	%rax, 328(%rdi)
	leaq	16+_ZTVN2v88internal13JsToWasmFrameE(%rip), %rax
	movq	%rax, 392(%rdi)
	leaq	16+_ZTVN2v88internal25WasmInterpreterEntryFrameE(%rip), %rax
	movq	%rax, 456(%rdi)
	leaq	16+_ZTVN2v88internal15CWasmEntryFrameE(%rip), %rax
	movq	%rax, 520(%rdi)
	leaq	16+_ZTVN2v88internal13WasmExitFrameE(%rip), %rax
	movq	%rsi, 280(%rdi)
	movq	$0, 320(%rdi)
	movq	%rdi, 336(%rdi)
	movq	%rsi, 344(%rdi)
	movq	$0, 384(%rdi)
	movq	%rdi, 400(%rdi)
	movq	%rsi, 408(%rdi)
	movq	$0, 448(%rdi)
	movq	%rdi, 464(%rdi)
	movq	%rsi, 472(%rdi)
	movq	$0, 512(%rdi)
	movq	%rdi, 528(%rdi)
	movq	%rsi, 536(%rdi)
	movq	$0, 576(%rdi)
	movq	%rdi, 592(%rdi)
	movq	%rsi, 600(%rdi)
	movups	%xmm0, 288(%rdi)
	movups	%xmm0, 304(%rdi)
	movups	%xmm0, 352(%rdi)
	movups	%xmm0, 368(%rdi)
	movups	%xmm0, 416(%rdi)
	movups	%xmm0, 432(%rdi)
	movups	%xmm0, 480(%rdi)
	movups	%xmm0, 496(%rdi)
	movups	%xmm0, 544(%rdi)
	movups	%xmm0, 560(%rdi)
	movups	%xmm0, 608(%rdi)
	movups	%xmm0, 624(%rdi)
	movq	%rax, 584(%rdi)
	leaq	16+_ZTVN2v88internal20WasmCompileLazyFrameE(%rip), %rax
	movq	%rax, 648(%rdi)
	leaq	16+_ZTVN2v88internal16InterpretedFrameE(%rip), %rax
	movq	%rax, 712(%rdi)
	leaq	16+_ZTVN2v88internal9StubFrameE(%rip), %rax
	movq	%rax, 776(%rdi)
	leaq	16+_ZTVN2v88internal24BuiltinContinuationFrameE(%rip), %rax
	movq	%rax, 840(%rdi)
	leaq	16+_ZTVN2v88internal34JavaScriptBuiltinContinuationFrameE(%rip), %rax
	movq	%rax, 904(%rdi)
	leaq	16+_ZTVN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameE(%rip), %rax
	movq	$0, 640(%rdi)
	movq	%rdi, 656(%rdi)
	movq	%rsi, 664(%rdi)
	movq	$0, 704(%rdi)
	movq	%rdi, 720(%rdi)
	movq	%rsi, 728(%rdi)
	movq	$0, 768(%rdi)
	movq	%rdi, 784(%rdi)
	movq	%rsi, 792(%rdi)
	movq	$0, 832(%rdi)
	movq	%rdi, 848(%rdi)
	movq	%rsi, 856(%rdi)
	movq	$0, 896(%rdi)
	movq	%rdi, 912(%rdi)
	movq	%rsi, 920(%rdi)
	movq	$0, 960(%rdi)
	movups	%xmm0, 672(%rdi)
	movups	%xmm0, 688(%rdi)
	movups	%xmm0, 736(%rdi)
	movups	%xmm0, 752(%rdi)
	movups	%xmm0, 800(%rdi)
	movups	%xmm0, 816(%rdi)
	movups	%xmm0, 864(%rdi)
	movups	%xmm0, 880(%rdi)
	movups	%xmm0, 928(%rdi)
	movups	%xmm0, 944(%rdi)
	movq	%rdi, 976(%rdi)
	movq	%rax, 968(%rdi)
	leaq	16+_ZTVN2v88internal13InternalFrameE(%rip), %rax
	movq	%rax, 1032(%rdi)
	leaq	16+_ZTVN2v88internal14ConstructFrameE(%rip), %rax
	movq	%rax, 1096(%rdi)
	leaq	16+_ZTVN2v88internal21ArgumentsAdaptorFrameE(%rip), %rax
	movq	%rax, 1160(%rdi)
	leaq	16+_ZTVN2v88internal12BuiltinFrameE(%rip), %rax
	movq	%rax, 1224(%rdi)
	leaq	16+_ZTVN2v88internal16BuiltinExitFrameE(%rip), %rax
	movq	%rsi, 984(%rdi)
	movq	$0, 1024(%rdi)
	movq	%rdi, 1040(%rdi)
	movq	%rsi, 1048(%rdi)
	movq	$0, 1088(%rdi)
	movq	%rdi, 1104(%rdi)
	movq	%rsi, 1112(%rdi)
	movq	$0, 1152(%rdi)
	movq	%rdi, 1168(%rdi)
	movq	%rsi, 1176(%rdi)
	movq	$0, 1216(%rdi)
	movq	%rdi, 1232(%rdi)
	movq	%rsi, 1240(%rdi)
	movq	$0, 1280(%rdi)
	movq	%rdi, 1296(%rdi)
	movq	%rsi, 1304(%rdi)
	movups	%xmm0, 992(%rdi)
	movups	%xmm0, 1008(%rdi)
	movups	%xmm0, 1056(%rdi)
	movups	%xmm0, 1072(%rdi)
	movups	%xmm0, 1120(%rdi)
	movups	%xmm0, 1136(%rdi)
	movups	%xmm0, 1184(%rdi)
	movups	%xmm0, 1200(%rdi)
	movups	%xmm0, 1248(%rdi)
	movups	%xmm0, 1264(%rdi)
	movups	%xmm0, 1312(%rdi)
	movups	%xmm0, 1328(%rdi)
	movq	%rax, 1288(%rdi)
	leaq	16+_ZTVN2v88internal11NativeFrameE(%rip), %rax
	movq	$0, 1344(%rdi)
	movq	%rdi, 1360(%rdi)
	movq	%rsi, 1368(%rdi)
	movq	%rax, 1352(%rdi)
	movq	$0, 1424(%rdi)
	movb	$1, 1432(%rdi)
	movups	%xmm0, 1376(%rdi)
	movups	%xmm0, 1392(%rdi)
	movups	%xmm0, 1408(%rdi)
	movq	12560(%rsi), %r14
	testq	%r14, %r14
	je	.L680
	movq	-8(%r14), %rax
	leaq	-16(%r14), %r13
	testb	$1, %al
	jne	.L683
	sarq	%rax
	movl	%eax, %r15d
	cmpl	$21, %eax
	jbe	.L688
.L683:
	movl	$3, %r15d
.L675:
	movq	-16(%r14), %r13
.L676:
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	leaq	-8(%r13), %rdi
	movq	%rdi, %rcx
	testq	%rax, %rax
	je	.L674
	call	*%rax
	movq	%rax, %rcx
.L674:
	movq	12568(%r12), %rax
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	%rax, 1424(%rbx)
	call	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE
	testq	%rax, %rax
	je	.L679
	movq	%r13, %xmm0
	movq	%r14, %xmm1
	movq	%rcx, 40(%rax)
	movq	$0, 48(%rax)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 56(%rax)
	movups	%xmm0, 24(%rax)
.L679:
	movq	%rax, 1416(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	movl	$2098184, %edx
	btq	%rax, %rdx
	jnc	.L683
	cmpl	$10, %eax
	jne	.L675
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L680:
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	jmp	.L674
	.cfi_endproc
.LFE23167:
	.size	_ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateE, .-_ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateE
	.globl	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE
	.set	_ZN2v88internal18StackFrameIteratorC1EPNS0_7IsolateE,_ZN2v88internal18StackFrameIteratorC2EPNS0_7IsolateE
	.section	.text._ZN2v88internal23JavaScriptFrameIterator7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv
	.type	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv, @function
_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv:
.LFB23176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	$1150992, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L693:
	movq	%rbx, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv
	movq	1416(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L689
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$20, %eax
	ja	.L693
	btq	%rax, %r12
	jnc	.L693
.L689:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23176:
	.size	_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv, .-_ZN2v88internal23JavaScriptFrameIterator7AdvanceEv
	.section	.text._ZN2v88internal23StackTraceFrameIterator7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StackTraceFrameIterator7AdvanceEv
	.type	_ZN2v88internal23StackTraceFrameIterator7AdvanceEv, @function
_ZN2v88internal23StackTraceFrameIterator7AdvanceEv:
.LFB23186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L696:
	movq	%rbx, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv
	movq	1416(%rbx), %r12
	testq	%r12, %r12
	je	.L695
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movq	(%r12), %rdx
	cmpl	$20, %eax
	ja	.L699
	movl	$1150992, %ecx
	btq	%rax, %rcx
	jnc	.L699
	movq	152(%rdx), %rax
	cmpq	%r13, %rax
	jne	.L702
	movq	32(%r12), %rax
	movq	-16(%rax), %rax
.L703:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L696
	movq	(%r12), %rax
	movq	152(%rax), %rax
	cmpq	%r13, %rax
	jne	.L704
	movq	32(%r12), %rax
	movq	-16(%rax), %rax
.L705:
	movq	23(%rax), %r12
	movq	31(%r12), %rax
	testb	$1, %al
	jne	.L720
.L706:
	leaq	-48(%rbp), %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L696
	movq	7(%r12), %rax
	testb	$1, %al
	jne	.L721
.L695:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L722
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L699:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*8(%rdx)
	cmpl	$5, %eax
	je	.L695
	cmpl	$8, %eax
	jne	.L696
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L702:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L720:
	movq	-1(%rax), %rdx
	cmpw	$86, 11(%rdx)
	je	.L723
.L707:
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37504(%rdx)
	je	.L696
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L723:
	movq	23(%rax), %rax
	testb	$1, %al
	jne	.L707
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L721:
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	jne	.L695
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L704:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L705
.L722:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23186:
	.size	_ZN2v88internal23StackTraceFrameIterator7AdvanceEv, .-_ZN2v88internal23StackTraceFrameIterator7AdvanceEv
	.section	.text._ZNK2v88internal23StackTraceFrameIterator12IsValidFrameEPNS0_10StackFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal23StackTraceFrameIterator12IsValidFrameEPNS0_10StackFrameE
	.type	_ZNK2v88internal23StackTraceFrameIterator12IsValidFrameEPNS0_10StackFrameE, @function
_ZNK2v88internal23StackTraceFrameIterator12IsValidFrameEPNS0_10StackFrameE:
.LFB23187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	call	*8(%rax)
	cmpl	$20, %eax
	jbe	.L725
	movq	0(%r13), %rax
.L726:
	movq	%r13, %rdi
	call	*8(%rax)
	cmpl	$5, %eax
	sete	%r12b
	cmpl	$8, %eax
	sete	%al
	orl	%eax, %r12d
.L724:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L741
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L725:
	.cfi_restore_state
	movl	%eax, %ecx
	movl	$1150992, %r12d
	movq	0(%r13), %rax
	shrq	%cl, %r12
	notq	%r12
	andl	$1, %r12d
	jne	.L726
	movq	152(%rax), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %rbx
	cmpq	%rbx, %rax
	jne	.L729
	movq	32(%r13), %rax
	movq	-16(%rax), %rax
.L730:
	movq	-1(%rax), %rax
	cmpw	$1105, 11(%rax)
	jne	.L724
	movq	0(%r13), %rax
	movq	152(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L731
	movq	32(%r13), %rax
	movq	-16(%rax), %rax
.L732:
	movq	23(%rax), %rbx
	movq	31(%rbx), %rdx
	movq	%rdx, %r12
	notq	%r12
	andl	$1, %r12d
	je	.L742
.L733:
	leaq	-48(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L724
	movq	7(%rbx), %rax
	movq	%rax, %r12
	notq	%r12
	andl	$1, %r12d
	jne	.L724
	movq	-1(%rax), %rax
	cmpw	$83, 11(%rax)
	setne	%r12b
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L729:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L742:
	movq	-1(%rdx), %rax
	cmpw	$86, 11(%rax)
	je	.L743
.L734:
	movq	%rdx, %rax
	andq	$-262144, %rax
	movq	24(%rax), %rax
	cmpq	-37504(%rax), %rdx
	je	.L724
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L731:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L743:
	movq	23(%rdx), %rdx
	testb	$1, %dl
	jne	.L734
	jmp	.L733
.L741:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23187:
	.size	_ZNK2v88internal23StackTraceFrameIterator12IsValidFrameEPNS0_10StackFrameE, .-_ZNK2v88internal23StackTraceFrameIterator12IsValidFrameEPNS0_10StackFrameE
	.section	.text._ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateENS0_12StackFrameIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateENS0_12StackFrameIdE
	.type	_ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateENS0_12StackFrameIdE, @function
_ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateENS0_12StackFrameIdE:
.LFB23184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edx, %r12d
	movl	$1, %edx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZN2v88internal22StackFrameIteratorBaseC2EPNS0_7IsolateEb
	movq	12560(%rsi), %r15
	testq	%r15, %r15
	je	.L757
	movq	-8(%r15), %rax
	leaq	-16(%r15), %r13
	testb	$1, %al
	jne	.L760
	sarq	%rax
	movl	%eax, %esi
	cmpl	$21, %eax
	jbe	.L775
.L760:
	movl	$3, %esi
.L746:
	movq	-16(%r15), %r13
.L747:
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	leaq	-8(%r13), %rdi
	movq	%rdi, %rcx
	testq	%rax, %rax
	je	.L745
	movl	%esi, -52(%rbp)
	call	*%rax
	movl	-52(%rbp), %esi
	movq	%rax, %rcx
.L745:
	movq	12568(%r14), %rax
	movq	%rbx, %rdi
	movq	%rax, 1424(%rbx)
	call	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L750
	movq	%r13, %xmm0
	movq	%r15, %xmm1
	movq	%rcx, 40(%rax)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, 24(%rax)
	movq	%rax, 1416(%rbx)
	call	_ZNK2v88internal23StackTraceFrameIterator12IsValidFrameEPNS0_10StackFrameE
	testb	%al, %al
	je	.L753
	.p2align 4,,10
	.p2align 3
.L751:
	movq	1416(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L744
	movq	(%rdi), %rax
	call	*56(%rax)
	cmpl	%eax, %r12d
	jne	.L755
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L776:
	movq	%rbx, %rdi
	call	_ZNK2v88internal23StackTraceFrameIterator12IsValidFrameEPNS0_10StackFrameE
	testb	%al, %al
	jne	.L751
.L755:
	movq	%rbx, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv
	movq	1416(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L776
.L744:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L777:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZNK2v88internal23StackTraceFrameIterator12IsValidFrameEPNS0_10StackFrameE
	testb	%al, %al
	jne	.L751
.L753:
	movq	%rbx, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv
	movq	1416(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L777
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L775:
	movl	$2098184, %edx
	btq	%rax, %rdx
	jnc	.L760
	cmpl	$10, %eax
	jne	.L746
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L757:
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L750:
	movq	$0, 1416(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23184:
	.size	_ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateENS0_12StackFrameIdE, .-_ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateENS0_12StackFrameIdE
	.globl	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateENS0_12StackFrameIdE
	.set	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateENS0_12StackFrameIdE,_ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateENS0_12StackFrameIdE
	.section	.text._ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateE
	.type	_ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateE, @function
_ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateE:
.LFB23178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal10EntryFrameE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, 8(%rdi)
	leaq	16+_ZTVN2v88internal19ConstructEntryFrameE(%rip), %rax
	movq	%rax, 72(%rdi)
	leaq	16+_ZTVN2v88internal9ExitFrameE(%rip), %rax
	movq	%rax, 136(%rdi)
	leaq	16+_ZTVN2v88internal14OptimizedFrameE(%rip), %rax
	movq	%rax, 200(%rdi)
	leaq	16+_ZTVN2v88internal17WasmCompiledFrameE(%rip), %rax
	movq	%rsi, (%rdi)
	movq	%rdi, 16(%rdi)
	movq	%rsi, 24(%rdi)
	movq	$0, 64(%rdi)
	movq	%rdi, 80(%rdi)
	movq	%rsi, 88(%rdi)
	movq	$0, 128(%rdi)
	movq	%rdi, 144(%rdi)
	movq	%rsi, 152(%rdi)
	movq	$0, 192(%rdi)
	movq	%rdi, 208(%rdi)
	movq	%rsi, 216(%rdi)
	movq	$0, 256(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 160(%rdi)
	movups	%xmm0, 176(%rdi)
	movups	%xmm0, 224(%rdi)
	movups	%xmm0, 240(%rdi)
	movq	%rdi, 272(%rdi)
	movq	%rax, 264(%rdi)
	leaq	16+_ZTVN2v88internal13WasmToJsFrameE(%rip), %rax
	movq	%rax, 328(%rdi)
	leaq	16+_ZTVN2v88internal13JsToWasmFrameE(%rip), %rax
	movq	%rax, 392(%rdi)
	leaq	16+_ZTVN2v88internal25WasmInterpreterEntryFrameE(%rip), %rax
	movq	%rax, 456(%rdi)
	leaq	16+_ZTVN2v88internal15CWasmEntryFrameE(%rip), %rax
	movq	%rax, 520(%rdi)
	leaq	16+_ZTVN2v88internal13WasmExitFrameE(%rip), %rax
	movq	%rsi, 280(%rdi)
	movq	$0, 320(%rdi)
	movq	%rdi, 336(%rdi)
	movq	%rsi, 344(%rdi)
	movq	$0, 384(%rdi)
	movq	%rdi, 400(%rdi)
	movq	%rsi, 408(%rdi)
	movq	$0, 448(%rdi)
	movq	%rdi, 464(%rdi)
	movq	%rsi, 472(%rdi)
	movq	$0, 512(%rdi)
	movq	%rdi, 528(%rdi)
	movq	%rsi, 536(%rdi)
	movq	$0, 576(%rdi)
	movq	%rdi, 592(%rdi)
	movq	%rsi, 600(%rdi)
	movups	%xmm0, 288(%rdi)
	movups	%xmm0, 304(%rdi)
	movups	%xmm0, 352(%rdi)
	movups	%xmm0, 368(%rdi)
	movups	%xmm0, 416(%rdi)
	movups	%xmm0, 432(%rdi)
	movups	%xmm0, 480(%rdi)
	movups	%xmm0, 496(%rdi)
	movups	%xmm0, 544(%rdi)
	movups	%xmm0, 560(%rdi)
	movups	%xmm0, 608(%rdi)
	movups	%xmm0, 624(%rdi)
	movq	%rax, 584(%rdi)
	leaq	16+_ZTVN2v88internal20WasmCompileLazyFrameE(%rip), %rax
	movq	%rax, 648(%rdi)
	leaq	16+_ZTVN2v88internal16InterpretedFrameE(%rip), %rax
	movq	%rax, 712(%rdi)
	leaq	16+_ZTVN2v88internal9StubFrameE(%rip), %rax
	movq	%rax, 776(%rdi)
	leaq	16+_ZTVN2v88internal24BuiltinContinuationFrameE(%rip), %rax
	movq	%rax, 840(%rdi)
	leaq	16+_ZTVN2v88internal34JavaScriptBuiltinContinuationFrameE(%rip), %rax
	movq	%rax, 904(%rdi)
	leaq	16+_ZTVN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameE(%rip), %rax
	movq	$0, 640(%rdi)
	movq	%rdi, 656(%rdi)
	movq	%rsi, 664(%rdi)
	movq	$0, 704(%rdi)
	movq	%rdi, 720(%rdi)
	movq	%rsi, 728(%rdi)
	movq	$0, 768(%rdi)
	movq	%rdi, 784(%rdi)
	movq	%rsi, 792(%rdi)
	movq	$0, 832(%rdi)
	movq	%rdi, 848(%rdi)
	movq	%rsi, 856(%rdi)
	movq	$0, 896(%rdi)
	movq	%rdi, 912(%rdi)
	movq	%rsi, 920(%rdi)
	movq	$0, 960(%rdi)
	movups	%xmm0, 672(%rdi)
	movups	%xmm0, 688(%rdi)
	movups	%xmm0, 736(%rdi)
	movups	%xmm0, 752(%rdi)
	movups	%xmm0, 800(%rdi)
	movups	%xmm0, 816(%rdi)
	movups	%xmm0, 864(%rdi)
	movups	%xmm0, 880(%rdi)
	movups	%xmm0, 928(%rdi)
	movups	%xmm0, 944(%rdi)
	movq	%rdi, 976(%rdi)
	movq	%rax, 968(%rdi)
	leaq	16+_ZTVN2v88internal13InternalFrameE(%rip), %rax
	movq	%rax, 1032(%rdi)
	leaq	16+_ZTVN2v88internal14ConstructFrameE(%rip), %rax
	movq	%rax, 1096(%rdi)
	leaq	16+_ZTVN2v88internal21ArgumentsAdaptorFrameE(%rip), %rax
	movq	%rax, 1160(%rdi)
	leaq	16+_ZTVN2v88internal12BuiltinFrameE(%rip), %rax
	movq	%rax, 1224(%rdi)
	leaq	16+_ZTVN2v88internal16BuiltinExitFrameE(%rip), %rax
	movq	%rsi, 984(%rdi)
	movq	$0, 1024(%rdi)
	movq	%rdi, 1040(%rdi)
	movq	%rsi, 1048(%rdi)
	movq	$0, 1088(%rdi)
	movq	%rdi, 1104(%rdi)
	movq	%rsi, 1112(%rdi)
	movq	$0, 1152(%rdi)
	movq	%rdi, 1168(%rdi)
	movq	%rsi, 1176(%rdi)
	movq	$0, 1216(%rdi)
	movq	%rdi, 1232(%rdi)
	movq	%rsi, 1240(%rdi)
	movq	$0, 1280(%rdi)
	movq	%rdi, 1296(%rdi)
	movq	%rsi, 1304(%rdi)
	movups	%xmm0, 992(%rdi)
	movups	%xmm0, 1008(%rdi)
	movups	%xmm0, 1056(%rdi)
	movups	%xmm0, 1072(%rdi)
	movups	%xmm0, 1120(%rdi)
	movups	%xmm0, 1136(%rdi)
	movups	%xmm0, 1184(%rdi)
	movups	%xmm0, 1200(%rdi)
	movups	%xmm0, 1248(%rdi)
	movups	%xmm0, 1264(%rdi)
	movups	%xmm0, 1312(%rdi)
	movups	%xmm0, 1328(%rdi)
	movq	%rax, 1288(%rdi)
	leaq	16+_ZTVN2v88internal11NativeFrameE(%rip), %rax
	movq	$0, 1344(%rdi)
	movq	%rdi, 1360(%rdi)
	movq	%rsi, 1368(%rdi)
	movq	%rax, 1352(%rdi)
	movq	$0, 1424(%rdi)
	movb	$1, 1432(%rdi)
	movups	%xmm0, 1376(%rdi)
	movups	%xmm0, 1392(%rdi)
	movups	%xmm0, 1408(%rdi)
	movq	12560(%rsi), %r14
	testq	%r14, %r14
	je	.L788
	movq	-8(%r14), %rax
	leaq	-16(%r14), %r13
	testb	$1, %al
	jne	.L791
	sarq	%rax
	movl	%eax, %r15d
	cmpl	$21, %eax
	jbe	.L798
.L791:
	movl	$3, %r15d
.L780:
	movq	-16(%r14), %r13
.L781:
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	leaq	-8(%r13), %rdi
	movq	%rdi, %rcx
	testq	%rax, %rax
	je	.L779
	call	*%rax
	movq	%rax, %rcx
.L779:
	movq	12568(%r12), %rax
	movl	%r15d, %esi
	movq	%rbx, %rdi
	movq	%rax, 1424(%rbx)
	call	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L784
	movq	%r13, %xmm0
	movq	%r14, %xmm1
	movq	%rcx, 40(%rax)
	punpcklqdq	%xmm1, %xmm0
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movups	%xmm0, 24(%rax)
	movq	%rax, 1416(%rbx)
	call	_ZNK2v88internal23StackTraceFrameIterator12IsValidFrameEPNS0_10StackFrameE
	testb	%al, %al
	je	.L785
.L778:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZNK2v88internal23StackTraceFrameIterator12IsValidFrameEPNS0_10StackFrameE
	testb	%al, %al
	jne	.L778
.L785:
	movq	%rbx, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv
	movq	1416(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L799
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L798:
	movl	$2098184, %edx
	btq	%rax, %rdx
	jnc	.L791
	cmpl	$10, %eax
	jne	.L780
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L788:
	xorl	%r13d, %r13d
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L784:
	movq	$0, 1416(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23178:
	.size	_ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateE, .-_ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateE
	.globl	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateE
	.set	_ZN2v88internal23StackTraceFrameIteratorC1EPNS0_7IsolateE,_ZN2v88internal23StackTraceFrameIteratorC2EPNS0_7IsolateE
	.section	.text._ZNK2v88internal22SafeStackFrameIterator26IsNoFrameBytecodeHandlerPcEPNS0_7IsolateEmm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal22SafeStackFrameIterator26IsNoFrameBytecodeHandlerPcEPNS0_7IsolateEmm
	.type	_ZNK2v88internal22SafeStackFrameIterator26IsNoFrameBytecodeHandlerPcEPNS0_7IsolateEmm, @function
_ZNK2v88internal22SafeStackFrameIterator26IsNoFrameBytecodeHandlerPcEPNS0_7IsolateEmm:
.LFB23189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	testq	%rax, %rax
	je	.L803
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	%r14d, -56(%rbp)
	leaq	-64(%rbp), %r14
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal12EmbeddedData34InstructionStartOfBytecodeHandlersEv@PLT
	cmpq	%rbx, %rax
	jbe	.L808
.L803:
	xorl	%eax, %eax
.L800:
	movq	-40(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L809
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L808:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZNK2v88internal12EmbeddedData32InstructionEndOfBytecodeHandlersEv@PLT
	cmpq	%rbx, %rax
	jbe	.L803
	leaq	-8(%r13), %rax
	cmpq	1440(%r12), %rax
	jb	.L803
	cmpq	1448(%r12), %rax
	ja	.L803
	movq	-8(%r13), %rdx
	movl	$1, %eax
	testb	$1, %dl
	jne	.L800
	sarq	%rdx
	cmpl	$13, %edx
	setne	%al
	jmp	.L800
.L809:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23189:
	.size	_ZNK2v88internal22SafeStackFrameIterator26IsNoFrameBytecodeHandlerPcEPNS0_7IsolateEmm, .-_ZNK2v88internal22SafeStackFrameIterator26IsNoFrameBytecodeHandlerPcEPNS0_7IsolateEmm
	.section	.text._ZNK2v88internal22SafeStackFrameIterator10IsValidTopEPNS0_14ThreadLocalTopE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal22SafeStackFrameIterator10IsValidTopEPNS0_14ThreadLocalTopE
	.type	_ZNK2v88internal22SafeStackFrameIterator10IsValidTopEPNS0_14ThreadLocalTopE, @function
_ZNK2v88internal22SafeStackFrameIterator10IsValidTopEPNS0_14ThreadLocalTopE:
.LFB23193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	112(%rsi), %r12
	movq	1440(%rdi), %rax
	cmpq	%rax, %r12
	jb	.L813
	movq	1448(%rdi), %rdx
	cmpq	%rdx, %r12
	jbe	.L826
.L813:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L826:
	.cfi_restore_state
	movq	-16(%r12), %rdi
	cmpq	%rdi, %rdx
	jb	.L813
	cmpq	%rdi, %rax
	ja	.L813
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	subq	$8, %rdi
	testq	%rax, %rax
	je	.L827
	movq	%rsi, -24(%rbp)
	call	*%rax
	movq	-24(%rbp), %rsi
.L815:
	cmpq	$0, (%rax)
	je	.L813
	movq	120(%rsi), %rax
	testq	%rax, %rax
	je	.L813
	cmpq	%rax, %r12
	setb	%al
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L827:
	.cfi_restore_state
	movq	%rdi, %rax
	jmp	.L815
	.cfi_endproc
.LFE23193:
	.size	_ZNK2v88internal22SafeStackFrameIterator10IsValidTopEPNS0_14ThreadLocalTopE, .-_ZNK2v88internal22SafeStackFrameIterator10IsValidTopEPNS0_14ThreadLocalTopE
	.section	.text._ZNK2v88internal22SafeStackFrameIterator12IsValidFrameEPNS0_10StackFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal22SafeStackFrameIterator12IsValidFrameEPNS0_10StackFrameE
	.type	_ZNK2v88internal22SafeStackFrameIterator12IsValidFrameEPNS0_10StackFrameE, @function
_ZNK2v88internal22SafeStackFrameIterator12IsValidFrameEPNS0_10StackFrameE:
.LFB23195:
	.cfi_startproc
	endbr64
	movq	24(%rsi), %rdx
	movq	1440(%rdi), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	jb	.L828
	movq	1448(%rdi), %rdi
	cmpq	%rdx, %rdi
	jb	.L828
	movq	32(%rsi), %rdx
	cmpq	%rcx, %rdx
	setnb	%al
	cmpq	%rdx, %rdi
	setnb	%dl
	andl	%edx, %eax
.L828:
	ret
	.cfi_endproc
.LFE23195:
	.size	_ZNK2v88internal22SafeStackFrameIterator12IsValidFrameEPNS0_10StackFrameE, .-_ZNK2v88internal22SafeStackFrameIterator12IsValidFrameEPNS0_10StackFrameE
	.section	.text._ZN2v88internal22SafeStackFrameIterator13IsValidCallerEPNS0_10StackFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22SafeStackFrameIterator13IsValidCallerEPNS0_10StackFrameE
	.type	_ZN2v88internal22SafeStackFrameIterator13IsValidCallerEPNS0_10StackFrameE, @function
_ZN2v88internal22SafeStackFrameIterator13IsValidCallerEPNS0_10StackFrameE:
.LFB23196:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	$0, -48(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	call	*8(%rax)
	cmpl	$1, %eax
	je	.L836
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	cmpl	$2, %eax
	je	.L836
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	cmpl	$19, %eax
	je	.L863
.L842:
	movq	(%r12), %rax
	leaq	-80(%rbp), %r14
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	*64(%rax)
	movq	-80(%rbp), %rax
	movq	1440(%r13), %rdx
	cmpq	%rdx, %rax
	jnb	.L841
.L835:
	xorl	%eax, %eax
.L832:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L864
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L841:
	.cfi_restore_state
	movq	1448(%r13), %rcx
	cmpq	%rcx, %rax
	ja	.L835
	movq	-72(%rbp), %rax
	cmpq	%rdx, %rax
	jb	.L835
	cmpq	%rcx, %rax
	ja	.L835
	movq	(%r12), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*72(%rax)
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE
	testq	%rax, %rax
	setne	%al
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L836:
	movq	32(%r12), %rax
	movq	1440(%r13), %rdx
	movq	-64(%rax), %rax
	cmpq	%rdx, %rax
	jb	.L835
	movq	1448(%r13), %rcx
	cmpq	%rcx, %rax
	ja	.L835
	movq	-16(%rax), %rdi
	cmpq	%rdi, %rdx
	ja	.L835
	cmpq	%rdi, %rcx
	jb	.L835
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	subq	$8, %rdi
	testq	%rax, %rax
	je	.L865
	call	*%rax
.L840:
	cmpq	$0, (%rax)
	jne	.L842
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L863:
	movq	(%r12), %rax
	leaq	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi(%rip), %rdx
	movq	144(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L843
	movq	32(%r12), %rax
	subq	$24, %rax
.L844:
	testb	$1, (%rax)
	je	.L842
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L843:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L844
.L865:
	movq	%rdi, %rax
	jmp	.L840
.L864:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23196:
	.size	_ZN2v88internal22SafeStackFrameIterator13IsValidCallerEPNS0_10StackFrameE, .-_ZN2v88internal22SafeStackFrameIterator13IsValidCallerEPNS0_10StackFrameE
	.section	.text._ZN2v88internal22SafeStackFrameIterator15AdvanceOneFrameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22SafeStackFrameIterator15AdvanceOneFrameEv
	.type	_ZN2v88internal22SafeStackFrameIterator15AdvanceOneFrameEv, @function
_ZN2v88internal22SafeStackFrameIterator15AdvanceOneFrameEv:
.LFB23194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	1416(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	1440(%rdi), %rax
	movq	24(%rsi), %r12
	cmpq	%r12, %rax
	ja	.L869
	movq	32(%rsi), %r13
	movq	1448(%rdi), %rdx
	cmpq	%r13, %rax
	setbe	%cl
	cmpq	%r12, %rdx
	setnb	%al
	testb	%al, %cl
	je	.L869
	cmpq	%r13, %rdx
	jnb	.L882
.L869:
	movq	$0, 1416(%rbx)
.L866:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L883
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L882:
	.cfi_restore_state
	call	_ZN2v88internal22SafeStackFrameIterator13IsValidCallerEPNS0_10StackFrameE
	testb	%al, %al
	je	.L869
	movq	1416(%rbx), %rdi
	pxor	%xmm0, %xmm0
	movq	$0, -48(%rbp)
	leaq	-80(%rbp), %rsi
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	movq	(%rdi), %rax
	call	*72(%rax)
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE
	testq	%rax, %rax
	je	.L869
	movdqa	-80(%rbp), %xmm1
	movups	%xmm1, 24(%rax)
	movdqa	-64(%rbp), %xmm2
	movups	%xmm2, 40(%rax)
	movq	-48(%rbp), %rdx
	movq	%rdx, 56(%rax)
	movq	%rax, 1416(%rbx)
	cmpq	24(%rax), %r12
	jnb	.L869
	cmpq	32(%rax), %r13
	jnb	.L869
	jmp	.L866
.L883:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23194:
	.size	_ZN2v88internal22SafeStackFrameIterator15AdvanceOneFrameEv, .-_ZN2v88internal22SafeStackFrameIterator15AdvanceOneFrameEv
	.section	.text._ZNK2v88internal22SafeStackFrameIterator16IsValidExitFrameEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal22SafeStackFrameIterator16IsValidExitFrameEm
	.type	_ZNK2v88internal22SafeStackFrameIterator16IsValidExitFrameEm, @function
_ZNK2v88internal22SafeStackFrameIterator16IsValidExitFrameEm:
.LFB23197:
	.cfi_startproc
	endbr64
	movq	1440(%rdi), %rdx
	xorl	%eax, %eax
	cmpq	%rdx, %rsi
	jb	.L894
	movq	1448(%rdi), %rcx
	cmpq	%rcx, %rsi
	ja	.L894
	movq	-16(%rsi), %rdi
	cmpq	%rdi, %rdx
	setbe	%al
	cmpq	%rdi, %rcx
	setnb	%dl
	andb	%dl, %al
	jne	.L897
.L894:
	ret
	.p2align 4,,10
	.p2align 3
.L897:
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	subq	$8, %rdi
	testq	%rax, %rax
	je	.L898
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*%rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpq	$0, (%rax)
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L898:
	.cfi_restore 6
	cmpq	$0, (%rdi)
	setne	%al
	ret
	.cfi_endproc
.LFE23197:
	.size	_ZNK2v88internal22SafeStackFrameIterator16IsValidExitFrameEm, .-_ZNK2v88internal22SafeStackFrameIterator16IsValidExitFrameEm
	.section	.text._ZN2v88internal22SafeStackFrameIterator7AdvanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22SafeStackFrameIterator7AdvanceEv
	.type	_ZN2v88internal22SafeStackFrameIterator7AdvanceEv, @function
_ZN2v88internal22SafeStackFrameIterator7AdvanceEv:
.LFB23198:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L909:
	movq	%rbx, %rdi
	call	_ZN2v88internal22SafeStackFrameIterator15AdvanceOneFrameEv
	movq	1416(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L899
	movq	1472(%rbx), %rax
	xorl	%r12d, %r12d
	testq	%rax, %rax
	jne	.L903
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L921:
	movq	16(%rax), %rdx
	movq	%rax, %r12
	movq	%rdx, 1472(%rbx)
	testq	%rdx, %rdx
	je	.L902
	movq	%rdx, %rax
.L903:
	cmpq	32(%rdi), %rax
	jb	.L921
.L902:
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$20, %eax
	ja	.L904
	movl	$1150992, %edx
	btq	%rax, %rdx
	jnc	.L904
.L899:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L904:
	.cfi_restore_state
	movq	1416(%rbx), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$5, %eax
	je	.L899
	cmpl	$8, %eax
	je	.L899
	movq	1416(%rbx), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$3, %eax
	je	.L908
	movq	1416(%rbx), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$21, %eax
	jne	.L909
.L908:
	testq	%r12, %r12
	je	.L899
	cmpq	$0, 8(%r12)
	movq	1416(%rbx), %rax
	je	.L913
	addq	$8, %r12
.L910:
	movq	%r12, 40(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L913:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L910
	.cfi_endproc
.LFE23198:
	.size	_ZN2v88internal22SafeStackFrameIterator7AdvanceEv, .-_ZN2v88internal22SafeStackFrameIterator7AdvanceEv
	.section	.text._ZNK2v88internal10StackFrame10LookupCodeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10StackFrame10LookupCodeEv
	.type	_ZNK2v88internal10StackFrame10LookupCodeEv, @function
_ZNK2v88internal10StackFrame10LookupCodeEv:
.LFB23200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	40(%rdi), %rax
	movq	(%rax), %r13
	movq	16(%rdi), %rax
	movq	41144(%rax), %rbx
	movq	(%rbx), %rax
	movq	40960(%rax), %r12
	cmpb	$0, 6624(%r12)
	je	.L923
	movq	6616(%r12), %rax
.L924:
	testq	%rax, %rax
	je	.L925
	addl	$1, (%rax)
.L925:
	movq	(%rbx), %r12
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm@PLT
	movl	%r13d, %edx
	andl	$262143, %edx
	testb	%al, %al
	jne	.L940
.L927:
	movl	%edx, %eax
	movq	(%rbx), %rdi
	sall	$15, %eax
	subl	%edx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1023, %eax
	leaq	(%rax,%rax,4), %r12
	salq	$3, %r12
	leaq	(%rbx,%r12), %r15
	leaq	8(%rbx,%r12), %r14
	cmpq	8(%r15), %r13
	je	.L941
	addq	$37592, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internal4Heap29GcSafeFindCodeForInnerPointerEm@PLT
	movq	%rax, 8(%r14)
	leaq	24(%rbx,%r12), %rax
	movl	$0, (%rax)
	movq	$0, 8(%rax)
	movq	%r13, 8(%r15)
.L931:
	movq	8(%r14), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L923:
	.cfi_restore_state
	movb	$1, 6624(%r12)
	leaq	6600(%r12), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6616(%r12)
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L941:
	movq	40960(%rdi), %rbx
	cmpb	$0, 6656(%rbx)
	je	.L929
	movq	6648(%rbx), %rax
.L930:
	testq	%rax, %rax
	je	.L931
	addl	$1, (%rax)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L940:
	movq	%r12, %rdi
	call	_ZNK2v88internal7Isolate18embedded_blob_sizeEv@PLT
	movq	%r12, %rdi
	call	_ZNK2v88internal7Isolate13embedded_blobEv@PLT
	movl	%r13d, %edx
	subl	%eax, %edx
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L929:
	movb	$1, 6656(%rbx)
	leaq	6632(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6648(%rbx)
	jmp	.L930
	.cfi_endproc
.LFE23200:
	.size	_ZNK2v88internal10StackFrame10LookupCodeEv, .-_ZNK2v88internal10StackFrame10LookupCodeEv
	.section	.text._ZN2v88internal10StackFrame9IteratePcEPNS0_11RootVisitorEPmS4_NS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackFrame9IteratePcEPNS0_11RootVisitorEPmS4_NS0_4CodeE
	.type	_ZN2v88internal10StackFrame9IteratePcEPNS0_11RootVisitorEPmS4_NS0_4CodeE, @function
_ZN2v88internal10StackFrame9IteratePcEPNS0_11RootVisitorEPmS4_NS0_4CodeE:
.LFB23201:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%rcx, -56(%rbp)
	movq	(%rsi), %rbx
	movl	43(%rcx), %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	63(%rcx), %rax
	testl	%esi, %esi
	js	.L954
.L944:
	subl	%eax, %ebx
	movq	-56(%rbp), %rax
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdx
	leaq	-48(%rbp), %rcx
	movq	%rax, -48(%rbp)
	movq	0(%r13), %rax
	movq	24(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L945
	leaq	-40(%rbp), %r8
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r13, %rdi
	call	*16(%rax)
.L946:
	movq	-48(%rbp), %rdx
	cmpq	-56(%rbp), %rdx
	je	.L942
	movq	%rdx, -56(%rbp)
	leaq	63(%rdx), %rax
	movl	43(%rdx), %edx
	testl	%edx, %edx
	js	.L955
.L950:
	addq	%rax, %rbx
	movq	%rbx, (%r12)
.L942:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L956
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L954:
	.cfi_restore_state
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L955:
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L945:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r13, %rdi
	call	*%r8
	jmp	.L946
.L956:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23201:
	.size	_ZN2v88internal10StackFrame9IteratePcEPNS0_11RootVisitorEPmS4_NS0_4CodeE, .-_ZN2v88internal10StackFrame9IteratePcEPNS0_11RootVisitorEPmS4_NS0_4CodeE
	.section	.text._ZN2v88internal10StackFrame32SetReturnAddressLocationResolverEPFmmE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackFrame32SetReturnAddressLocationResolverEPFmmE
	.type	_ZN2v88internal10StackFrame32SetReturnAddressLocationResolverEPFmmE, @function
_ZN2v88internal10StackFrame32SetReturnAddressLocationResolverEPFmmE:
.LFB23202:
	.cfi_startproc
	endbr64
	movq	%rdi, _ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip)
	ret
	.cfi_endproc
.LFE23202:
	.size	_ZN2v88internal10StackFrame32SetReturnAddressLocationResolverEPFmmE, .-_ZN2v88internal10StackFrame32SetReturnAddressLocationResolverEPFmmE
	.section	.text._ZNK2v88internal10StackFrame10UnpaddedFPEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10StackFrame10UnpaddedFPEv
	.type	_ZNK2v88internal10StackFrame10UnpaddedFPEv, @function
_ZNK2v88internal10StackFrame10UnpaddedFPEv:
.LFB23205:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE23205:
	.size	_ZNK2v88internal10StackFrame10UnpaddedFPEv, .-_ZNK2v88internal10StackFrame10UnpaddedFPEv
	.section	.text._ZN2v88internal9ExitFrame23GetStateForFramePointerEmPNS0_10StackFrame5StateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ExitFrame23GetStateForFramePointerEmPNS0_10StackFrame5StateE
	.type	_ZN2v88internal9ExitFrame23GetStateForFramePointerEmPNS0_10StackFrame5StateE, @function
_ZN2v88internal9ExitFrame23GetStateForFramePointerEmPNS0_10StackFrame5StateE:
.LFB23217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rdi, %rdi
	je	.L959
	movq	-8(%rdi), %rdx
	movq	%rsi, %rbx
	leaq	-16(%rdi), %rax
	testb	$1, %dl
	jne	.L968
	sarq	%rdx
	movl	%edx, %r12d
	cmpl	$21, %edx
	jbe	.L972
.L968:
	movl	$3, %r12d
.L961:
	movq	-16(%rdi), %rax
.L962:
	movq	%rax, %xmm0
	movq	%rdi, %xmm1
	leaq	-8(%rax), %rdi
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	testq	%rax, %rax
	je	.L973
	call	*%rax
.L964:
	movq	%rax, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
.L959:
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L972:
	.cfi_restore_state
	movl	$2098184, %ecx
	btq	%rdx, %rcx
	jnc	.L968
	cmpl	$10, %edx
	jne	.L961
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L973:
	movq	%rdi, %rax
	jmp	.L964
	.cfi_endproc
.LFE23217:
	.size	_ZN2v88internal9ExitFrame23GetStateForFramePointerEmPNS0_10StackFrame5StateE, .-_ZN2v88internal9ExitFrame23GetStateForFramePointerEmPNS0_10StackFrame5StateE
	.section	.text._ZN2v88internal9ExitFrame16ComputeFrameTypeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ExitFrame16ComputeFrameTypeEm
	.type	_ZN2v88internal9ExitFrame16ComputeFrameTypeEm, @function
_ZN2v88internal9ExitFrame16ComputeFrameTypeEm:
.LFB23218:
	.cfi_startproc
	endbr64
	movq	-8(%rdi), %rcx
	movl	$3, %eax
	testb	$1, %cl
	jne	.L974
	sarq	%rcx
	movl	%ecx, %eax
	cmpl	$21, %ecx
	ja	.L977
	movl	$2098184, %edx
	shrq	%cl, %rdx
	andl	$1, %edx
	movl	$3, %edx
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L977:
	movl	$3, %eax
.L974:
	ret
	.cfi_endproc
.LFE23218:
	.size	_ZN2v88internal9ExitFrame16ComputeFrameTypeEm, .-_ZN2v88internal9ExitFrame16ComputeFrameTypeEm
	.section	.text._ZN2v88internal9ExitFrame19ComputeStackPointerEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ExitFrame19ComputeStackPointerEm
	.type	_ZN2v88internal9ExitFrame19ComputeStackPointerEm, @function
_ZN2v88internal9ExitFrame19ComputeStackPointerEm:
.LFB23219:
	.cfi_startproc
	endbr64
	movq	-16(%rdi), %rax
	ret
	.cfi_endproc
.LFE23219:
	.size	_ZN2v88internal9ExitFrame19ComputeStackPointerEm, .-_ZN2v88internal9ExitFrame19ComputeStackPointerEm
	.section	.text._ZN2v88internal13WasmExitFrame19ComputeStackPointerEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13WasmExitFrame19ComputeStackPointerEm
	.type	_ZN2v88internal13WasmExitFrame19ComputeStackPointerEm, @function
_ZN2v88internal13WasmExitFrame19ComputeStackPointerEm:
.LFB23220:
	.cfi_startproc
	endbr64
	leaq	-16(%rdi), %rax
	ret
	.cfi_endproc
.LFE23220:
	.size	_ZN2v88internal13WasmExitFrame19ComputeStackPointerEm, .-_ZN2v88internal13WasmExitFrame19ComputeStackPointerEm
	.section	.text._ZN2v88internal9ExitFrame9FillStateEmmPNS0_10StackFrame5StateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9ExitFrame9FillStateEmmPNS0_10StackFrame5StateE
	.type	_ZN2v88internal9ExitFrame9FillStateEmmPNS0_10StackFrame5StateE, @function
_ZN2v88internal9ExitFrame9FillStateEmmPNS0_10StackFrame5StateE:
.LFB23221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %xmm1
	movq	%rsi, %xmm0
	leaq	-8(%rsi), %rdi
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	subq	$8, %rsp
	movups	%xmm0, (%rdx)
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	testq	%rax, %rax
	je	.L987
	call	*%rax
.L985:
	movq	%rax, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L987:
	.cfi_restore_state
	movq	%rdi, %rax
	jmp	.L985
	.cfi_endproc
.LFE23221:
	.size	_ZN2v88internal9ExitFrame9FillStateEmmPNS0_10StackFrame5StateE, .-_ZN2v88internal9ExitFrame9FillStateEmmPNS0_10StackFrame5StateE
	.section	.text._ZNK2v88internal16BuiltinExitFrame8functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16BuiltinExitFrame8functionEv
	.type	_ZNK2v88internal16BuiltinExitFrame8functionEv, @function
_ZNK2v88internal16BuiltinExitFrame8functionEv:
.LFB23222:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	24(%rax), %rax
	ret
	.cfi_endproc
.LFE23222:
	.size	_ZNK2v88internal16BuiltinExitFrame8functionEv, .-_ZNK2v88internal16BuiltinExitFrame8functionEv
	.section	.text._ZNK2v88internal16BuiltinExitFrame8receiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16BuiltinExitFrame8receiverEv
	.type	_ZNK2v88internal16BuiltinExitFrame8receiverEv, @function
_ZNK2v88internal16BuiltinExitFrame8receiverEv:
.LFB23223:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	movslq	36(%rdx), %rax
	leal	8(,%rax,8), %eax
	cltq
	movq	(%rax,%rdx), %rax
	ret
	.cfi_endproc
.LFE23223:
	.size	_ZNK2v88internal16BuiltinExitFrame8receiverEv, .-_ZNK2v88internal16BuiltinExitFrame8receiverEv
	.section	.text._ZNK2v88internal16BuiltinExitFrame13IsConstructorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16BuiltinExitFrame13IsConstructorEv
	.type	_ZNK2v88internal16BuiltinExitFrame13IsConstructorEv, @function
_ZNK2v88internal16BuiltinExitFrame13IsConstructorEv:
.LFB23224:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	32(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rax, 88(%rdx)
	setne	%al
	ret
	.cfi_endproc
.LFE23224:
	.size	_ZNK2v88internal16BuiltinExitFrame13IsConstructorEv, .-_ZNK2v88internal16BuiltinExitFrame13IsConstructorEv
	.section	.text._ZNK2v88internal16BuiltinExitFrame12GetParameterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16BuiltinExitFrame12GetParameterEi
	.type	_ZNK2v88internal16BuiltinExitFrame12GetParameterEi, @function
_ZNK2v88internal16BuiltinExitFrame12GetParameterEi:
.LFB23225:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rdx
	leal	48(,%rsi,8), %eax
	cltq
	movq	(%rax,%rdx), %rax
	ret
	.cfi_endproc
.LFE23225:
	.size	_ZNK2v88internal16BuiltinExitFrame12GetParameterEi, .-_ZNK2v88internal16BuiltinExitFrame12GetParameterEi
	.section	.text._ZNK2v88internal16BuiltinExitFrame22ComputeParametersCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16BuiltinExitFrame22ComputeParametersCountEv
	.type	_ZNK2v88internal16BuiltinExitFrame22ComputeParametersCountEv, @function
_ZNK2v88internal16BuiltinExitFrame22ComputeParametersCountEv:
.LFB23226:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movslq	36(%rax), %rax
	subl	$4, %eax
	ret
	.cfi_endproc
.LFE23226:
	.size	_ZNK2v88internal16BuiltinExitFrame22ComputeParametersCountEv, .-_ZNK2v88internal16BuiltinExitFrame22ComputeParametersCountEv
	.section	.text._ZNK2v88internal13StandardFrame23ComputeExpressionsCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13StandardFrame23ComputeExpressionsCountEv
	.type	_ZNK2v88internal13StandardFrame23ComputeExpressionsCountEv, @function
_ZNK2v88internal13StandardFrame23ComputeExpressionsCountEv:
.LFB23237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*144(%rax)
	subq	24(%rbx), %rax
	addq	$8, %rsp
	addq	$8, %rax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	shrq	$3, %rax
	ret
	.cfi_endproc
.LFE23237:
	.size	_ZNK2v88internal13StandardFrame23ComputeExpressionsCountEv, .-_ZNK2v88internal13StandardFrame23ComputeExpressionsCountEv
	.section	.text._ZNK2v88internal15JavaScriptFrame17SetParameterValueEiNS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame17SetParameterValueEiNS0_6ObjectE
	.type	_ZNK2v88internal15JavaScriptFrame17SetParameterValueEiNS0_6ObjectE, @function
_ZNK2v88internal15JavaScriptFrame17SetParameterValueEiNS0_6ObjectE:
.LFB23249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*120(%rax)
	leaq	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv(%rip), %rdx
	subl	%r13d, %eax
	leal	-8(,%rax,8), %r13d
	movq	(%r12), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L996
	movq	32(%r12), %rax
	addq	$16, %rax
.L997:
	movslq	%r13d, %r13
	movq	%rbx, 0(%r13,%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L996:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L997
	.cfi_endproc
.LFE23249:
	.size	_ZNK2v88internal15JavaScriptFrame17SetParameterValueEiNS0_6ObjectE, .-_ZNK2v88internal15JavaScriptFrame17SetParameterValueEiNS0_6ObjectE
	.section	.text._ZNK2v88internal15JavaScriptFrame16HasInlinedFramesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame16HasInlinedFramesEv
	.type	_ZNK2v88internal15JavaScriptFrame16HasInlinedFramesEv, @function
_ZNK2v88internal15JavaScriptFrame16HasInlinedFramesEv:
.LFB23251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	leaq	-48(%rbp), %rsi
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	$0, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	*160(%rax)
	movq	-48(%rbp), %rdi
	movq	-40(%rbp), %rax
	subq	%rdi, %rax
	cmpq	$15, %rax
	seta	%r12b
	testq	%rdi, %rdi
	je	.L999
	call	_ZdlPv@PLT
.L999:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1006
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1006:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23251:
	.size	_ZNK2v88internal15JavaScriptFrame16HasInlinedFramesEv, .-_ZNK2v88internal15JavaScriptFrame16HasInlinedFramesEv
	.section	.text._ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE
	.type	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE, @function
_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE:
.LFB23265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	leaq	-80(%rbp), %rsi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	*160(%rax)
	movq	-80(%rbp), %rdi
	movq	-72(%rbp), %r13
	cmpq	%rdi, %r13
	je	.L1008
	movq	%r13, -88(%rbp)
	movq	%r15, %r13
	movq	%rdi, %r15
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1048:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	8(%r14), %r12
	movq	%rax, %rbx
	cmpq	16(%r14), %r12
	je	.L1014
.L1050:
	movq	%rbx, (%r12)
	addq	$8, %r15
	addq	$8, 8(%r14)
	cmpq	%r15, -88(%rbp)
	je	.L1047
.L1025:
	movq	0(%r13), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %rcx
	movq	152(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1009
	movq	32(%r13), %rax
	movq	-16(%rax), %rax
.L1010:
	andq	$-262144, %rax
	movq	(%r15), %rsi
	movq	24(%rax), %r12
	movq	3520(%r12), %rdi
	subq	$37592, %r12
	testq	%rdi, %rdi
	jne	.L1048
	movq	41088(%r12), %rbx
	cmpq	41096(%r12), %rbx
	je	.L1049
.L1013:
	leaq	8(%rbx), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%rbx)
	movq	8(%r14), %r12
	cmpq	16(%r14), %r12
	jne	.L1050
.L1014:
	movabsq	$1152921504606846975, %rcx
	movq	(%r14), %r8
	movq	%r12, %rdx
	subq	%r8, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1051
	testq	%rax, %rax
	je	.L1028
	movabsq	$9223372036854775800, %r9
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L1052
.L1017:
	movq	%r9, %rdi
	movq	%rdx, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %rdx
	leaq	8(%rax), %rsi
	addq	%rax, %r9
.L1018:
	movq	%rbx, (%rax,%rdx)
	cmpq	%r8, %r12
	je	.L1019
	leaq	-8(%r12), %rdi
	leaq	15(%rax), %rdx
	subq	%r8, %rdi
	subq	%r8, %rdx
	movq	%rdi, %r10
	shrq	$3, %r10
	cmpq	$30, %rdx
	jbe	.L1031
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %r10
	je	.L1031
	addq	$1, %r10
	xorl	%edx, %edx
	movq	%r10, %rsi
	shrq	%rsi
	salq	$4, %rsi
	.p2align 4,,10
	.p2align 3
.L1021:
	movdqu	(%r8,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rsi
	jne	.L1021
	movq	%r10, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %rsi
	leaq	(%r8,%rsi), %rdx
	addq	%rax, %rsi
	cmpq	%r10, %r11
	je	.L1023
	movq	(%rdx), %rdx
	movq	%rdx, (%rsi)
.L1023:
	leaq	16(%rax,%rdi), %rsi
.L1019:
	testq	%r8, %r8
	je	.L1024
	movq	%r8, %rdi
	movq	%rsi, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZdlPv@PLT
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %rax
.L1024:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%r9, 16(%r14)
	addq	$8, %r15
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%r14)
	cmpq	%r15, -88(%rbp)
	jne	.L1025
	.p2align 4,,10
	.p2align 3
.L1047:
	movq	-80(%rbp), %rdi
.L1008:
	testq	%rdi, %rdi
	je	.L1007
	call	_ZdlPv@PLT
.L1007:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1053
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1009:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1052:
	testq	%rsi, %rsi
	jne	.L1054
	movl	$8, %esi
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %rbx
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1028:
	movl	$8, %r9d
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	%rax, %rsi
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L1020:
	movq	(%rdx), %rcx
	addq	$8, %rdx
	addq	$8, %rsi
	movq	%rcx, -8(%rsi)
	cmpq	%rdx, %r12
	jne	.L1020
	jmp	.L1023
.L1051:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1054:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rsi
	movq	%rax, %r9
	cmovbe	%rsi, %r9
	salq	$3, %r9
	jmp	.L1017
.L1053:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23265:
	.size	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE, .-_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_6HandleINS0_18SharedFunctionInfoEEESaIS5_EE
	.section	.text._ZNK2v88internal15JavaScriptFrame18unchecked_functionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame18unchecked_functionEv
	.type	_ZNK2v88internal15JavaScriptFrame18unchecked_functionEv, @function
_ZNK2v88internal15JavaScriptFrame18unchecked_functionEv:
.LFB23268:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	ret
	.cfi_endproc
.LFE23268:
	.size	_ZNK2v88internal15JavaScriptFrame18unchecked_functionEv, .-_ZNK2v88internal15JavaScriptFrame18unchecked_functionEv
	.section	.rodata._ZN2v88internal15JavaScriptFrame22PrintFunctionAndOffsetENS0_10JSFunctionENS0_12AbstractCodeEiP8_IO_FILEb.str1.1,"aMS",@progbits,1
.LC63:
	.string	"*"
.LC64:
	.string	"~"
.LC65:
	.string	"%s"
.LC66:
	.string	"+%d"
.LC67:
	.string	" at %s:%d"
.LC68:
	.string	" at <unknown>:%d"
.LC69:
	.string	" at <unknown>:<unknown>"
	.section	.text._ZN2v88internal15JavaScriptFrame22PrintFunctionAndOffsetENS0_10JSFunctionENS0_12AbstractCodeEiP8_IO_FILEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JavaScriptFrame22PrintFunctionAndOffsetENS0_10JSFunctionENS0_12AbstractCodeEiP8_IO_FILEb
	.type	_ZN2v88internal15JavaScriptFrame22PrintFunctionAndOffsetENS0_10JSFunctionENS0_12AbstractCodeEiP8_IO_FILEb, @function
_ZN2v88internal15JavaScriptFrame22PrintFunctionAndOffsetENS0_10JSFunctionENS0_12AbstractCodeEiP8_IO_FILEb:
.LFB23273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%r8d, %ebx
	subq	$56, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -40(%rbp)
	xorl	%ecx, %ecx
	movq	47(%rdi), %rdx
	cmpl	$67, 59(%rdx)
	jne	.L1057
.L1061:
	leaq	.LC64(%rip), %rdx
.L1058:
	xorl	%eax, %eax
	leaq	.LC65(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	leaq	-72(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal10JSFunction9PrintNameEP8_IO_FILE@PLT
	xorl	%eax, %eax
	movl	%r13d, %edx
	movq	%r12, %rdi
	leaq	.LC66(%rip), %rsi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	testb	%bl, %bl
	jne	.L1076
.L1056:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1077
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1057:
	.cfi_restore_state
	movabsq	$287762808832, %rdx
	movq	23(%rdi), %rax
	movq	7(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1061
	testb	$1, %al
	jne	.L1078
.L1060:
	movq	-72(%rbp), %rax
	movq	47(%rax), %rdx
	testb	$62, 43(%rdx)
	jne	.L1061
	movq	47(%rax), %rax
	leaq	.LC63(%rip), %rdx
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	leaq	.LC64(%rip), %rax
	cmovne	%rax, %rdx
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1076:
	movq	-72(%rbp), %rax
	leaq	-80(%rbp), %rdi
	movl	%r13d, %esi
	movq	23(%rax), %rbx
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	movq	31(%rbx), %rdx
	testb	$1, %dl
	jne	.L1079
.L1065:
	leaq	.LC69(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1079:
	movq	-1(%rdx), %rsi
	leaq	-1(%rdx), %rcx
	cmpw	$86, 11(%rsi)
	je	.L1080
.L1066:
	movq	(%rcx), %rcx
	cmpw	$96, 11(%rcx)
	jne	.L1065
	leaq	-64(%rbp), %rdi
	movl	%eax, %esi
	movq	%rdx, -64(%rbp)
	call	_ZNK2v88internal6Script13GetLineNumberEi@PLT
	leal	1(%rax), %r13d
	movq	-64(%rbp), %rax
	movq	15(%rax), %rax
	testb	$1, %al
	jne	.L1081
.L1067:
	movl	%r13d, %edx
	leaq	.LC68(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L1056
	.p2align 4,,10
	.p2align 3
.L1078:
	movq	-1(%rax), %rdx
	cmpw	$165, 11(%rdx)
	je	.L1061
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L1060
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1080:
	movq	23(%rdx), %rdx
	testb	$1, %dl
	je	.L1065
	leaq	-1(%rdx), %rcx
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1067
	leaq	-48(%rbp), %rdi
	leaq	-56(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal6String9ToCStringENS0_14AllowNullsFlagENS0_14RobustnessFlagEPi@PLT
	movq	-48(%rbp), %rdx
	movq	%r12, %rdi
	movl	%r13d, %ecx
	leaq	.LC67(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1056
	call	_ZdaPv@PLT
	jmp	.L1056
.L1077:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23273:
	.size	_ZN2v88internal15JavaScriptFrame22PrintFunctionAndOffsetENS0_10JSFunctionENS0_12AbstractCodeEiP8_IO_FILEb, .-_ZN2v88internal15JavaScriptFrame22PrintFunctionAndOffsetENS0_10JSFunctionENS0_12AbstractCodeEiP8_IO_FILEb
	.section	.rodata._ZN2v88internal15JavaScriptFrame8PrintTopEPNS0_7IsolateEP8_IO_FILEbb.str1.1,"aMS",@progbits,1
.LC70:
	.string	"(this="
.LC71:
	.string	", "
	.section	.text._ZN2v88internal15JavaScriptFrame8PrintTopEPNS0_7IsolateEP8_IO_FILEbb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JavaScriptFrame8PrintTopEPNS0_7IsolateEP8_IO_FILEbb
	.type	_ZN2v88internal15JavaScriptFrame8PrintTopEPNS0_7IsolateEP8_IO_FILEbb, @function
_ZN2v88internal15JavaScriptFrame8PrintTopEPNS0_7IsolateEP8_IO_FILEbb:
.LFB23274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	$1, %edx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdi, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1504(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$1496, %rsp
	movl	%ecx, -1524(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal22StackFrameIteratorBaseC2EPNS0_7IsolateEb
	leaq	12448(%r9), %rsi
	call	_ZN2v88internal18StackFrameIterator5ResetEPNS0_14ThreadLocalTopE
	cmpq	$0, -88(%rbp)
	je	.L1082
	movl	$1150992, %r13d
	.p2align 4,,10
	.p2align 3
.L1131:
	movq	%rbx, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1082
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$20, %eax
	ja	.L1131
	btq	%rax, %r13
	jnc	.L1131
	movl	$1150992, %r13d
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1082
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$20, %eax
	ja	.L1132
	btq	%rax, %r13
	jnc	.L1132
	movq	-88(%rbp), %r15
	leaq	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv(%rip), %rdx
	movq	(%r15), %rax
	movq	128(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1087
	movq	32(%r15), %rax
	movq	(%rax), %rdx
	movq	-8(%rdx), %rax
	cmpq	$38, %rax
	jne	.L1088
	movq	(%rdx), %rax
	movq	-8(%rax), %rax
.L1088:
	cmpq	$36, %rax
	sete	%al
.L1089:
	testb	%al, %al
	jne	.L1139
.L1090:
	movq	(%r15), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %rcx
	movq	152(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1091
	movq	32(%r15), %rdx
	movq	-16(%rdx), %r13
.L1092:
	movq	%r15, %rdi
	call	*8(%rax)
	cmpl	$12, %eax
	movq	(%r15), %rax
	je	.L1140
	movq	%r15, %rdi
	call	*16(%rax)
	movq	%rax, -1512(%rbp)
	leaq	63(%rax), %rcx
	movq	40(%r15), %rdx
	movl	43(%rax), %eax
	movq	(%rdx), %rbx
	testl	%eax, %eax
	js	.L1141
.L1098:
	movl	%ebx, %edx
	subl	%ecx, %edx
.L1096:
	movzbl	-1524(%rbp), %r8d
	movq	47(%r13), %rax
	leaq	47(%r13), %rcx
	cmpl	$67, 59(%rax)
	je	.L1101
	movq	23(%r13), %rax
	leaq	23(%r13), %rsi
	movabsq	$287762808832, %rdi
	movq	7(%rax), %rax
	cmpq	%rdi, %rax
	je	.L1101
	testb	$1, %al
	jne	.L1142
.L1102:
	movq	(%rcx), %rax
	movl	59(%rax), %eax
	leal	-64(%rax), %edi
	cmpl	$1, %edi
	jbe	.L1104
	cmpl	$57, %eax
	jne	.L1143
.L1104:
	movq	(%rsi), %rax
	movq	31(%rax), %rcx
	testb	$1, %cl
	jne	.L1144
.L1106:
	movq	7(%rax), %rcx
	testb	$1, %cl
	jne	.L1145
.L1108:
	movq	7(%rax), %rax
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	7(%rax), %rsi
	call	_ZN2v88internal15JavaScriptFrame22PrintFunctionAndOffsetENS0_10JSFunctionENS0_12AbstractCodeEiP8_IO_FILEb
	testb	%r14b, %r14b
	jne	.L1146
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1147
	addq	$1496, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1132:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v88internal18StackFrameIterator7AdvanceEv
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1082
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$20, %eax
	ja	.L1132
	btq	%rax, %r13
	jnc	.L1132
	jmp	.L1117
.L1142:
	movq	-1(%rax), %rdi
	cmpw	$165, 11(%rdi)
	jne	.L1148
	.p2align 4,,10
	.p2align 3
.L1101:
	movq	(%rcx), %rsi
.L1109:
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal15JavaScriptFrame22PrintFunctionAndOffsetENS0_10JSFunctionENS0_12AbstractCodeEiP8_IO_FILEb
	testb	%r14b, %r14b
	je	.L1082
.L1146:
	leaq	.LC70(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	(%r15), %rax
	leaq	-1512(%rbp), %r14
	movq	%r15, %rdi
	call	*80(%rax)
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -1512(%rbp)
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*120(%rax)
	movl	%eax, %r13d
	testl	%eax, %eax
	jg	.L1110
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1149:
	movq	%r15, %rdi
	call	*120(%rax)
	leaq	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv(%rip), %rcx
	subl	%ebx, %eax
	leal	-8(,%rax,8), %edx
	movq	(%r15), %rax
	movq	56(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1112
	movq	32(%r15), %rax
	addq	$16, %rax
.L1113:
	movslq	%edx, %rdx
	movq	(%rdx,%rax), %rax
	movq	%rax, -1512(%rbp)
.L1114:
	movq	%r12, %rsi
	movq	%r14, %rdi
	addl	$1, %ebx
	call	_ZNK2v88internal6Object10ShortPrintEP8_IO_FILE@PLT
	cmpl	%ebx, %r13d
	je	.L1115
.L1110:
	xorl	%eax, %eax
	leaq	.LC71(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	movq	(%r15), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame12GetParameterEi(%rip), %rcx
	movq	112(%rax), %rdx
	cmpq	%rcx, %rdx
	je	.L1149
	movl	%ebx, %esi
	movq	%r15, %rdi
	call	*%rdx
	movq	%rax, -1512(%rbp)
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1087:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1112:
	movl	%edx, -1524(%rbp)
	movq	%r15, %rdi
	call	*%rax
	movl	-1524(%rbp), %edx
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1140:
	movq	144(%rax), %rax
	leaq	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L1094
	movq	32(%r15), %rax
	subq	$32, %rax
.L1095:
	movq	(%rax), %rdx
	sarq	$32, %rdx
	subl	$53, %edx
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1139:
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1115:
	leaq	.LC51(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN2v88internal6PrintFEP8_IO_FILEPKcz@PLT
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1091:
	movq	%r15, %rdi
	call	*%rdx
	movq	%rax, %r13
	movq	(%r15), %rax
	jmp	.L1092
.L1141:
	leaq	-1512(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	%rax, %rcx
	jmp	.L1098
.L1144:
	movq	-1(%rcx), %rsi
	cmpw	$86, 11(%rsi)
	jne	.L1106
	movq	39(%rcx), %rsi
	testb	$1, %sil
	je	.L1106
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L1106
	movq	31(%rcx), %rsi
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1145:
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L1108
	movq	7(%rax), %rsi
	jmp	.L1109
.L1148:
	movq	-1(%rax), %rax
	cmpw	$166, 11(%rax)
	jne	.L1102
	jmp	.L1101
.L1143:
	movq	(%rcx), %rax
	testb	$62, 43(%rax)
	jne	.L1101
	movq	(%rcx), %rax
	movq	31(%rax), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L1101
	jmp	.L1104
.L1094:
	movl	$-1, %esi
	movq	%r15, %rdi
	call	*%rax
	jmp	.L1095
.L1147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23274:
	.size	_ZN2v88internal15JavaScriptFrame8PrintTopEPNS0_7IsolateEP8_IO_FILEbb, .-_ZN2v88internal15JavaScriptFrame8PrintTopEPNS0_7IsolateEP8_IO_FILEbb
	.section	.text._ZN2v88internal15JavaScriptFrame34CollectFunctionAndOffsetForICStatsENS0_10JSFunctionENS0_12AbstractCodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal15JavaScriptFrame34CollectFunctionAndOffsetForICStatsENS0_10JSFunctionENS0_12AbstractCodeEi
	.type	_ZN2v88internal15JavaScriptFrame34CollectFunctionAndOffsetForICStatsENS0_10JSFunctionENS0_12AbstractCodeEi, @function
_ZN2v88internal15JavaScriptFrame34CollectFunctionAndOffsetForICStatsENS0_10JSFunctionENS0_12AbstractCodeEi:
.LFB23278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 3, -48
	movq	%rsi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movzbl	_ZN2v88internal7ICStats9instance_E(%rip), %eax
	cmpb	$2, %al
	jne	.L1165
.L1151:
	movslq	152+_ZN2v88internal7ICStats9instance_E(%rip), %rax
	movq	%r13, %rsi
	leaq	8+_ZN2v88internal7ICStats9instance_E(%rip), %rdi
	leaq	(%rax,%rax,8), %rbx
	salq	$4, %rbx
	addq	16+_ZN2v88internal7ICStats9instance_E(%rip), %rbx
	movq	23(%r13), %r14
	call	_ZN2v88internal7ICStats22GetOrCacheFunctionNameENS0_10JSFunctionE@PLT
	movl	%r12d, 40(%rbx)
	leaq	-104(%rbp), %rdi
	movl	%r12d, %esi
	movq	%rax, 32(%rbx)
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	movq	31(%r14), %rdx
	testb	$1, %dl
	jne	.L1166
.L1150:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1167
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1165:
	.cfi_restore_state
	leaq	_ZN2v84base16LazyInstanceImplINS_8internal7ICStatsENS0_32StaticallyAllocatedInstanceTraitIS3_EENS0_21DefaultConstructTraitIS3_EENS0_23ThreadSafeInitOnceTraitENS0_18LeakyInstanceTraitIS3_EEE12InitInstanceEPv(%rip), %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, %xmm0
	leaq	8+_ZN2v88internal7ICStats9instance_E(%rip), %rax
	leaq	-80(%rbp), %r14
	movq	%rax, -72(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEZN2v84base8CallOnceIvEEvPSt6atomicIhENS2_14OneArgFunctionIPT_E4typeES9_EUlvE_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%r14, %rsi
	leaq	_ZN2v88internal7ICStats9instance_E(%rip), %rdi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1151
	movl	$3, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1166:
	movq	-1(%rdx), %rsi
	leaq	-1(%rdx), %rcx
	cmpw	$86, 11(%rsi)
	je	.L1168
.L1155:
	movq	(%rcx), %rcx
	cmpw	$96, 11(%rcx)
	jne	.L1150
	leaq	-88(%rbp), %rdi
	movl	%eax, %esi
	movq	%rdx, -88(%rbp)
	call	_ZNK2v88internal6Script13GetLineNumberEi@PLT
	movq	-88(%rbp), %rsi
	leaq	8+_ZN2v88internal7ICStats9instance_E(%rip), %rdi
	addl	$1, %eax
	movl	%eax, 56(%rbx)
	call	_ZN2v88internal7ICStats20GetOrCacheScriptNameENS0_6ScriptE@PLT
	movq	%rax, 48(%rbx)
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1168:
	movq	23(%rdx), %rdx
	testb	$1, %dl
	je	.L1150
	leaq	-1(%rdx), %rcx
	jmp	.L1155
.L1167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23278:
	.size	_ZN2v88internal15JavaScriptFrame34CollectFunctionAndOffsetForICStatsENS0_10JSFunctionENS0_12AbstractCodeEi, .-_ZN2v88internal15JavaScriptFrame34CollectFunctionAndOffsetForICStatsENS0_10JSFunctionENS0_12AbstractCodeEi
	.section	.text._ZNK2v88internal15JavaScriptFrame13GetParametersEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame13GetParametersEv
	.type	_ZNK2v88internal15JavaScriptFrame13GetParametersEv, @function
_ZNK2v88internal15JavaScriptFrame13GetParametersEv:
.LFB23281:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal31FLAG_detailed_error_stack_traceE(%rip)
	jne	.L1170
	movq	16(%rdi), %rax
	addq	$288, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1170:
	jmp	_ZNK2v88internal15JavaScriptFrame13GetParametersEv.part.0
	.cfi_endproc
.LFE23281:
	.size	_ZNK2v88internal15JavaScriptFrame13GetParametersEv, .-_ZNK2v88internal15JavaScriptFrame13GetParametersEv
	.section	.text._ZNK2v88internal34JavaScriptBuiltinContinuationFrame14GetSPToFPDeltaEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame14GetSPToFPDeltaEv
	.type	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame14GetSPToFPDeltaEv, @function
_ZNK2v88internal34JavaScriptBuiltinContinuationFrame14GetSPToFPDeltaEv:
.LFB23283:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movslq	-20(%rax), %rax
	ret
	.cfi_endproc
.LFE23283:
	.size	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame14GetSPToFPDeltaEv, .-_ZNK2v88internal34JavaScriptBuiltinContinuationFrame14GetSPToFPDeltaEv
	.section	.rodata._ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrame12SetExceptionENS0_6ObjectE.str1.8,"aMS",@progbits,1
	.align 8
.LC72:
	.string	"ReadOnlyRoots(isolate()).the_hole_value() == Object(Memory<Address>(exception_argument_slot))"
	.section	.rodata._ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrame12SetExceptionENS0_6ObjectE.str1.1,"aMS",@progbits,1
.LC73:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrame12SetExceptionENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrame12SetExceptionENS0_6ObjectE
	.type	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrame12SetExceptionENS0_6ObjectE, @function
_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrame12SetExceptionENS0_6ObjectE:
.LFB23285:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	32(%rdi), %rax
	movq	96(%rdx), %rcx
	cmpq	%rcx, 24(%rax)
	jne	.L1179
	movq	%rsi, 24(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L1179:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC72(%rip), %rsi
	leaq	.LC73(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23285:
	.size	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrame12SetExceptionENS0_6ObjectE, .-_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrame12SetExceptionENS0_6ObjectE
	.section	.text._ZN2v88internal12FrameSummary22JavaScriptFrameSummaryC2EPNS0_7IsolateENS0_6ObjectENS0_10JSFunctionENS0_12AbstractCodeEibNS0_10FixedArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FrameSummary22JavaScriptFrameSummaryC2EPNS0_7IsolateENS0_6ObjectENS0_10JSFunctionENS0_12AbstractCodeEibNS0_10FixedArrayE
	.type	_ZN2v88internal12FrameSummary22JavaScriptFrameSummaryC2EPNS0_7IsolateENS0_6ObjectENS0_10JSFunctionENS0_12AbstractCodeEibNS0_10FixedArrayE, @function
_ZN2v88internal12FrameSummary22JavaScriptFrameSummaryC2EPNS0_7IsolateENS0_6ObjectENS0_10JSFunctionENS0_12AbstractCodeEibNS0_10FixedArrayE:
.LFB23287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r9d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$24, %rsp
	movl	16(%rbp), %r14d
	movq	%rbx, (%rdi)
	movl	$0, 8(%rdi)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1181
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, 16(%r15)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1184
.L1195:
	movq	%rcx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, 24(%r15)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1187
.L1197:
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1188:
	movq	%rax, 32(%r15)
	movl	%r12d, 40(%r15)
	movb	%r14b, 44(%r15)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1190
	movq	24(%rbp), %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1191:
	movq	%rax, 48(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1181:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1194
.L1183:
	leaq	8(%rax), %rdx
	movq	%rax, 16(%r15)
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L1195
.L1184:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1196
.L1186:
	leaq	8(%rax), %rdx
	movq	%rax, 24(%r15)
	movq	%rdx, 41088(%rbx)
	movq	%rcx, (%rax)
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L1197
.L1187:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1198
.L1189:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%r13, (%rax)
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1190:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1199
.L1192:
	movq	24(%rbp), %rcx
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rcx, (%rax)
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1194:
	movq	%rbx, %rdi
	movq	%rcx, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %rsi
	jmp	.L1183
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L1198:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	%rbx, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L1192
	.cfi_endproc
.LFE23287:
	.size	_ZN2v88internal12FrameSummary22JavaScriptFrameSummaryC2EPNS0_7IsolateENS0_6ObjectENS0_10JSFunctionENS0_12AbstractCodeEibNS0_10FixedArrayE, .-_ZN2v88internal12FrameSummary22JavaScriptFrameSummaryC2EPNS0_7IsolateENS0_6ObjectENS0_10JSFunctionENS0_12AbstractCodeEibNS0_10FixedArrayE
	.globl	_ZN2v88internal12FrameSummary22JavaScriptFrameSummaryC1EPNS0_7IsolateENS0_6ObjectENS0_10JSFunctionENS0_12AbstractCodeEibNS0_10FixedArrayE
	.set	_ZN2v88internal12FrameSummary22JavaScriptFrameSummaryC1EPNS0_7IsolateENS0_6ObjectENS0_10JSFunctionENS0_12AbstractCodeEibNS0_10FixedArrayE,_ZN2v88internal12FrameSummary22JavaScriptFrameSummaryC2EPNS0_7IsolateENS0_6ObjectENS0_10JSFunctionENS0_12AbstractCodeEibNS0_10FixedArrayE
	.section	.text._ZN2v88internal12FrameSummary30EnsureSourcePositionsAvailableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FrameSummary30EnsureSourcePositionsAvailableEv
	.type	_ZN2v88internal12FrameSummary30EnsureSourcePositionsAvailableEv, @function
_ZN2v88internal12FrameSummary30EnsureSourcePositionsAvailableEv:
.LFB23289:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	testl	%eax, %eax
	je	.L1208
	ret
	.p2align 4,,10
	.p2align 3
.L1208:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rax
	movq	(%rdi), %r12
	movq	(%rax), %rax
	movq	23(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1202
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1203:
	movq	(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	.p2align 4,,10
	.p2align 3
.L1202:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1209
.L1204:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1209:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1204
	.cfi_endproc
.LFE23289:
	.size	_ZN2v88internal12FrameSummary30EnsureSourcePositionsAvailableEv, .-_ZN2v88internal12FrameSummary30EnsureSourcePositionsAvailableEv
	.section	.text._ZNK2v88internal12FrameSummary27AreSourcePositionsAvailableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary27AreSourcePositionsAvailableEv
	.type	_ZNK2v88internal12FrameSummary27AreSourcePositionsAvailableEv, @function
_ZNK2v88internal12FrameSummary27AreSourcePositionsAvailableEv:
.LFB23290:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L1210
	cmpb	$0, _ZN2v88internal33FLAG_enable_lazy_source_positionsE(%rip)
	jne	.L1219
.L1210:
	ret
	.p2align 4,,10
	.p2align 3
.L1219:
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L1220
.L1212:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L1221
.L1214:
	movq	7(%rax), %rax
	movq	7(%rax), %rax
.L1213:
	movq	31(%rax), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jne	.L1210
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37504(%rcx), %rdx
	je	.L1210
	cmpq	%rdx, -37280(%rcx)
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1220:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1212
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L1212
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L1212
	movq	31(%rdx), %rax
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L1214
	movq	7(%rax), %rax
	jmp	.L1213
	.cfi_endproc
.LFE23290:
	.size	_ZNK2v88internal12FrameSummary27AreSourcePositionsAvailableEv, .-_ZNK2v88internal12FrameSummary27AreSourcePositionsAvailableEv
	.section	.text._ZN2v88internal12FrameSummary22JavaScriptFrameSummary30EnsureSourcePositionsAvailableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FrameSummary22JavaScriptFrameSummary30EnsureSourcePositionsAvailableEv
	.type	_ZN2v88internal12FrameSummary22JavaScriptFrameSummary30EnsureSourcePositionsAvailableEv, @function
_ZN2v88internal12FrameSummary22JavaScriptFrameSummary30EnsureSourcePositionsAvailableEv:
.LFB23291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rax
	movq	(%rdi), %r12
	movq	(%rax), %rax
	movq	23(%rax), %r13
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1223
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rsi
.L1224:
	movq	(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal18SharedFunctionInfo30EnsureSourcePositionsAvailableEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	.p2align 4,,10
	.p2align 3
.L1223:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1227
.L1225:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1227:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1225
	.cfi_endproc
.LFE23291:
	.size	_ZN2v88internal12FrameSummary22JavaScriptFrameSummary30EnsureSourcePositionsAvailableEv, .-_ZN2v88internal12FrameSummary22JavaScriptFrameSummary30EnsureSourcePositionsAvailableEv
	.section	.text._ZNK2v88internal12FrameSummary22JavaScriptFrameSummary27AreSourcePositionsAvailableEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary27AreSourcePositionsAvailableEv
	.type	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary27AreSourcePositionsAvailableEv, @function
_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary27AreSourcePositionsAvailableEv:
.LFB23292:
	.cfi_startproc
	endbr64
	cmpb	$0, _ZN2v88internal33FLAG_enable_lazy_source_positionsE(%rip)
	movl	$1, %eax
	jne	.L1236
.L1228:
	ret
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rdx
	testb	$1, %dl
	jne	.L1237
.L1230:
	movq	7(%rax), %rdx
	testb	$1, %dl
	jne	.L1238
.L1232:
	movq	7(%rax), %rax
	movq	7(%rax), %rax
.L1231:
	movq	31(%rax), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jne	.L1228
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	-37504(%rcx), %rdx
	je	.L1228
	cmpq	%rdx, -37280(%rcx)
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L1237:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	jne	.L1230
	movq	39(%rdx), %rcx
	testb	$1, %cl
	je	.L1230
	movq	-1(%rcx), %rcx
	cmpw	$72, 11(%rcx)
	jne	.L1230
	movq	31(%rdx), %rax
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1238:
	movq	-1(%rdx), %rdx
	cmpw	$72, 11(%rdx)
	jne	.L1232
	movq	7(%rax), %rax
	jmp	.L1231
	.cfi_endproc
.LFE23292:
	.size	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary27AreSourcePositionsAvailableEv, .-_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary27AreSourcePositionsAvailableEv
	.section	.text._ZNK2v88internal12FrameSummary22JavaScriptFrameSummary23is_subject_to_debuggingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary23is_subject_to_debuggingEv
	.type	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary23is_subject_to_debuggingEv, @function
_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary23is_subject_to_debuggingEv:
.LFB23293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rbx
	movq	31(%rbx), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	je	.L1248
.L1240:
	leaq	-32(%rbp), %rdi
	movq	%rdx, -32(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	jne	.L1249
.L1239:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L1250
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1249:
	.cfi_restore_state
	movq	7(%rbx), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jne	.L1239
	movq	-1(%rdx), %rax
	cmpw	$83, 11(%rax)
	setne	%al
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	je	.L1251
.L1241:
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	%rdx, -37504(%rcx)
	je	.L1239
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1251:
	movq	23(%rdx), %rdx
	testb	$1, %dl
	jne	.L1241
	jmp	.L1240
.L1250:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23293:
	.size	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary23is_subject_to_debuggingEv, .-_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary23is_subject_to_debuggingEv
	.section	.text._ZNK2v88internal12FrameSummary22JavaScriptFrameSummary14SourcePositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary14SourcePositionEv
	.type	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary14SourcePositionEv, @function
_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary14SourcePositionEv:
.LFB23294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	40(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	leaq	-16(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1255
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1255:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23294:
	.size	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary14SourcePositionEv, .-_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary14SourcePositionEv
	.section	.text._ZNK2v88internal12FrameSummary22JavaScriptFrameSummary23SourceStatementPositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary23SourceStatementPositionEv
	.type	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary23SourceStatementPositionEv, @function
_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary23SourceStatementPositionEv:
.LFB23295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	40(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	leaq	-16(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal12AbstractCode23SourceStatementPositionEi@PLT
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1259
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1259:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23295:
	.size	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary23SourceStatementPositionEv, .-_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary23SourceStatementPositionEv
	.section	.text._ZNK2v88internal12FrameSummary22JavaScriptFrameSummary6scriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary6scriptEv
	.type	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary6scriptEv, @function
_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary6scriptEv:
.LFB23296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	24(%rdi), %rax
	movq	(%rdi), %rbx
	movq	(%rax), %rax
	movq	23(%rax), %rax
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L1266
.L1261:
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1262
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1262:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1267
.L1264:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1266:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	cmpw	$86, 11(%rax)
	jne	.L1261
	movq	23(%rsi), %rsi
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1267:
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1264
	.cfi_endproc
.LFE23296:
	.size	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary6scriptEv, .-_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary6scriptEv
	.section	.text._ZNK2v88internal12FrameSummary22JavaScriptFrameSummary12FunctionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary12FunctionNameEv
	.type	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary12FunctionNameEv, @function
_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary12FunctionNameEv:
.LFB23297:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rdi
	jmp	_ZN2v88internal10JSFunction12GetDebugNameENS0_6HandleIS1_EE@PLT
	.cfi_endproc
.LFE23297:
	.size	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary12FunctionNameEv, .-_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary12FunctionNameEv
	.section	.text._ZNK2v88internal12FrameSummary22JavaScriptFrameSummary14native_contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary14native_contextEv
	.type	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary14native_contextEv, @function
_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary14native_contextEv:
.LFB23298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	24(%rdi), %rax
	movq	(%rdi), %rbx
	movq	(%rax), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1270
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1270:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1274
.L1272:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1274:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1272
	.cfi_endproc
.LFE23298:
	.size	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary14native_contextEv, .-_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary14native_contextEv
	.section	.text._ZN2v88internal12FrameSummary16WasmFrameSummaryC2EPNS0_7IsolateENS1_4KindENS0_6HandleINS0_18WasmInstanceObjectEEEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FrameSummary16WasmFrameSummaryC2EPNS0_7IsolateENS1_4KindENS0_6HandleINS0_18WasmInstanceObjectEEEb
	.type	_ZN2v88internal12FrameSummary16WasmFrameSummaryC2EPNS0_7IsolateENS1_4KindENS0_6HandleINS0_18WasmInstanceObjectEEEb, @function
_ZN2v88internal12FrameSummary16WasmFrameSummaryC2EPNS0_7IsolateENS1_4KindENS0_6HandleINS0_18WasmInstanceObjectEEEb:
.LFB23300:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	%edx, 8(%rdi)
	movq	%rcx, 16(%rdi)
	movb	%r8b, 24(%rdi)
	ret
	.cfi_endproc
.LFE23300:
	.size	_ZN2v88internal12FrameSummary16WasmFrameSummaryC2EPNS0_7IsolateENS1_4KindENS0_6HandleINS0_18WasmInstanceObjectEEEb, .-_ZN2v88internal12FrameSummary16WasmFrameSummaryC2EPNS0_7IsolateENS1_4KindENS0_6HandleINS0_18WasmInstanceObjectEEEb
	.globl	_ZN2v88internal12FrameSummary16WasmFrameSummaryC1EPNS0_7IsolateENS1_4KindENS0_6HandleINS0_18WasmInstanceObjectEEEb
	.set	_ZN2v88internal12FrameSummary16WasmFrameSummaryC1EPNS0_7IsolateENS1_4KindENS0_6HandleINS0_18WasmInstanceObjectEEEb,_ZN2v88internal12FrameSummary16WasmFrameSummaryC2EPNS0_7IsolateENS1_4KindENS0_6HandleINS0_18WasmInstanceObjectEEEb
	.section	.text._ZNK2v88internal12FrameSummary16WasmFrameSummary8receiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary16WasmFrameSummary8receiverEv
	.type	_ZNK2v88internal12FrameSummary16WasmFrameSummary8receiverEv, @function
_ZNK2v88internal12FrameSummary16WasmFrameSummary8receiverEv:
.LFB23302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	leaq	-32(%rbp), %rdi
	movq	(%rax), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	-25128(%rbx), %rax
	subq	$37592, %rbx
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1277
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1278:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1282
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1277:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1283
.L1279:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1278
	.p2align 4,,10
	.p2align 3
.L1283:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L1279
.L1282:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23302:
	.size	_ZNK2v88internal12FrameSummary16WasmFrameSummary8receiverEv, .-_ZNK2v88internal12FrameSummary16WasmFrameSummary8receiverEv
	.section	.text._ZNK2v88internal12FrameSummary16WasmFrameSummary14function_indexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary16WasmFrameSummary14function_indexEv
	.type	_ZNK2v88internal12FrameSummary16WasmFrameSummary14function_indexEv, @function
_ZNK2v88internal12FrameSummary16WasmFrameSummary14function_indexEv:
.LFB23303:
	.cfi_startproc
	endbr64
	cmpl	$1, 8(%rdi)
	je	.L1287
	movl	28(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	32(%rdi), %rax
	movl	56(%rax), %eax
	ret
	.cfi_endproc
.LFE23303:
	.size	_ZNK2v88internal12FrameSummary16WasmFrameSummary14function_indexEv, .-_ZNK2v88internal12FrameSummary16WasmFrameSummary14function_indexEv
	.section	.text._ZNK2v88internal12FrameSummary16WasmFrameSummary11byte_offsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary16WasmFrameSummary11byte_offsetEv
	.type	_ZNK2v88internal12FrameSummary16WasmFrameSummary11byte_offsetEv, @function
_ZNK2v88internal12FrameSummary16WasmFrameSummary11byte_offsetEv:
.LFB23304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 8(%rdi)
	je	.L1297
	movl	32(%rdi), %r12d
.L1288:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1298
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1297:
	.cfi_restore_state
	movq	32(%rdi), %rax
	leaq	-112(%rbp), %r13
	movl	40(%rdi), %ebx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE@PLT
	cmpl	$-1, -88(%rbp)
	je	.L1288
	cmpl	-80(%rbp), %ebx
	jle	.L1288
	.p2align 4,,10
	.p2align 3
.L1299:
	movq	-72(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	shrq	%r12
	andl	$1073741823, %r12d
	subl	$1, %r12d
	cmpl	$-1, -88(%rbp)
	je	.L1288
	cmpl	-80(%rbp), %ebx
	jg	.L1299
	jmp	.L1288
.L1298:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23304:
	.size	_ZNK2v88internal12FrameSummary16WasmFrameSummary11byte_offsetEv, .-_ZNK2v88internal12FrameSummary16WasmFrameSummary11byte_offsetEv
	.section	.text._ZNK2v88internal12FrameSummary16WasmFrameSummary14SourcePositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary16WasmFrameSummary14SourcePositionEv
	.type	_ZNK2v88internal12FrameSummary16WasmFrameSummary14SourcePositionEv, @function
_ZNK2v88internal12FrameSummary16WasmFrameSummary14SourcePositionEv:
.LFB23305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	41112(%r12), %rdi
	movq	(%rax), %rax
	movq	135(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1301
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpl	$1, 8(%rbx)
	movzbl	24(%rbx), %r15d
	movq	%rax, %r14
	je	.L1316
.L1304:
	movl	32(%rbx), %edx
.L1309:
	movl	28(%rbx), %esi
.L1310:
	movl	%r15d, %ecx
	movq	%r14, %rdi
	call	_ZN2v88internal16WasmModuleObject17GetSourcePositionENS0_6HandleIS1_EEjjb@PLT
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1317
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1301:
	.cfi_restore_state
	movq	41088(%r12), %r14
	cmpq	41096(%r12), %r14
	je	.L1318
.L1303:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r12)
	movq	%rsi, (%r14)
	cmpl	$1, 8(%rbx)
	movzbl	24(%rbx), %r15d
	jne	.L1304
.L1316:
	movq	32(%rbx), %rax
	leaq	-128(%rbp), %r12
	xorl	%ecx, %ecx
	movl	40(%rbx), %r13d
	movq	%r12, %rdi
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE@PLT
	cmpl	$-1, -104(%rbp)
	je	.L1312
	movl	$0, -136(%rbp)
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1306:
	movq	-88(%rbp), %rdx
	movq	%r12, %rdi
	shrq	%rdx
	andl	$1073741823, %edx
	leal	-1(%rdx), %eax
	movl	%eax, -136(%rbp)
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -104(%rbp)
	je	.L1315
.L1307:
	cmpl	-96(%rbp), %r13d
	jg	.L1306
.L1315:
	movl	-136(%rbp), %edx
.L1305:
	cmpl	$1, 8(%rbx)
	jne	.L1309
	movq	32(%rbx), %rax
	movl	56(%rax), %esi
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1318:
	movq	%r12, %rdi
	movq	%rsi, -136(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-136(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1312:
	xorl	%edx, %edx
	jmp	.L1305
.L1317:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23305:
	.size	_ZNK2v88internal12FrameSummary16WasmFrameSummary14SourcePositionEv, .-_ZNK2v88internal12FrameSummary16WasmFrameSummary14SourcePositionEv
	.section	.text._ZNK2v88internal17WasmCompiledFrame8positionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17WasmCompiledFrame8positionEv
	.type	_ZNK2v88internal17WasmCompiledFrame8positionEv, @function
_ZNK2v88internal17WasmCompiledFrame8positionEv:
.LFB23378:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-112(%rbp), %r12
	movq	%r12, %rsi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	*136(%rax)
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rdi
	movdqu	(%rax), %xmm1
	movaps	%xmm1, -80(%rbp)
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -64(%rbp)
	movdqu	32(%rax), %xmm3
	movaps	%xmm3, -48(%rbp)
	movq	48(%rax), %rdx
	movq	%rdx, -32(%rbp)
	cmpq	%rdi, %rax
	je	.L1320
	.p2align 4,,10
	.p2align 3
.L1322:
	cmpl	$2, 8(%rax)
	ja	.L1326
	addq	$56, %rax
	cmpq	%rax, %rdi
	jne	.L1322
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1323
.L1320:
	call	_ZdlPv@PLT
.L1323:
	movl	-72(%rbp), %eax
	cmpl	$1, %eax
	je	.L1325
	cmpl	$2, %eax
	je	.L1325
	testl	%eax, %eax
	je	.L1334
.L1326:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1325:
	leaq	-80(%rbp), %rdi
	call	_ZNK2v88internal12FrameSummary16WasmFrameSummary14SourcePositionEv
.L1327:
	cmpl	$2, -72(%rbp)
	ja	.L1326
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1335
	addq	$104, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1334:
	.cfi_restore_state
	movq	-48(%rbp), %rax
	movl	-40(%rbp), %esi
	movq	%r12, %rdi
	movq	(%rax), %rax
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	jmp	.L1327
.L1335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23378:
	.size	_ZNK2v88internal17WasmCompiledFrame8positionEv, .-_ZNK2v88internal17WasmCompiledFrame8positionEv
	.section	.rodata._ZNK2v88internal17WasmCompiledFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi.str1.1,"aMS",@progbits,1
.LC74:
	.string	"WASM ["
	.section	.rodata._ZNK2v88internal17WasmCompiledFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi.str1.8,"aMS",@progbits,1
	.align 8
.LC75:
	.string	"], function #%u ('%s'), pc=%p (+0x%x), pos=%d (+%d)\n"
	.section	.text._ZNK2v88internal17WasmCompiledFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17WasmCompiledFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.type	_ZNK2v88internal17WasmCompiledFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi, @function
_ZNK2v88internal17WasmCompiledFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi:
.LFB23369:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movl	$1, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$344, %rsp
	movl	%edx, -364(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	cmpl	$1, %edx
	leaq	.LC3(%rip), %rsi
	movl	%ecx, -176(%rbp)
	sbbq	%rdx, %rdx
	movq	%r14, %rcx
	addq	$6, %rdx
	testl	%eax, %eax
	leaq	.LC4(%rip), %rax
	cmovne	%rax, %rsi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movl	$6, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC74(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movq	(%rbx), %rax
	leaq	_ZNK2v88internal17WasmCompiledFrame6scriptEv(%rip), %rdx
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1338
	movq	32(%rbx), %rax
	movq	-16(%rax), %rax
	movq	135(%rax), %rax
	movq	39(%rax), %rax
.L1339:
	movq	15(%rax), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12StringStream9PrintNameENS0_6ObjectE@PLT
	movq	16(%rbx), %rdx
	movq	40(%rbx), %rax
	movq	45752(%rdx), %rdi
	movq	(%rax), %rsi
	addq	$280, %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	leaq	-336(%rbp), %rsi
	movq	(%rax), %rax
	movq	%rax, -360(%rbp)
	movq	32(%rbx), %rax
	movq	-16(%rax), %rax
	movq	135(%rax), %rax
	movaps	%xmm0, -336(%rbp)
	movq	$0, -320(%rbp)
	movq	%rax, -344(%rbp)
	movq	(%rbx), %rax
	call	*136(%rax)
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rdi
	movl	8(%rax), %r13d
	movq	32(%rax), %r15
	cmpq	%rdi, %rax
	je	.L1340
	.p2align 4,,10
	.p2align 3
.L1342:
	cmpl	$2, 8(%rax)
	ja	.L1344
	addq	$56, %rax
	cmpq	%rax, %rdi
	jne	.L1342
	movq	-336(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1343
.L1340:
	call	_ZdlPv@PLT
.L1343:
	movl	56(%r15), %esi
	cmpl	$2, %r13d
	ja	.L1344
	leaq	-344(%rbp), %rdi
	call	_ZN2v88internal16WasmModuleObject18GetRawFunctionNameEj@PLT
	movq	%rax, %rsi
	movslq	%edx, %r13
	cmpl	$63, %edx
	jle	.L1377
	movl	$64, %edx
	movl	$64, %r13d
.L1345:
	leaq	-128(%rbp), %r15
	movl	$65, %ecx
	movq	%r15, %rdi
	call	__memcpy_chk@PLT
	movq	(%rbx), %rax
	leaq	_ZNK2v88internal17WasmCompiledFrame8positionEv(%rip), %rcx
	movb	$0, -128(%rbp,%r13)
	movq	104(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1346
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	movq	$0, -288(%rbp)
	leaq	-304(%rbp), %rsi
	movaps	%xmm0, -304(%rbp)
	call	*136(%rax)
	movq	-304(%rbp), %rax
	movq	-296(%rbp), %rdi
	movdqu	(%rax), %xmm1
	movaps	%xmm1, -240(%rbp)
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -224(%rbp)
	movdqu	32(%rax), %xmm3
	movaps	%xmm3, -208(%rbp)
	movq	48(%rax), %rdx
	movq	%rdx, -192(%rbp)
	cmpq	%rdi, %rax
	je	.L1347
	.p2align 4,,10
	.p2align 3
.L1348:
	cmpl	$2, 8(%rax)
	ja	.L1344
	addq	$56, %rax
	cmpq	%rax, %rdi
	jne	.L1348
	movq	-304(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1349
.L1347:
	call	_ZdlPv@PLT
.L1349:
	movl	-232(%rbp), %eax
	cmpl	$1, %eax
	je	.L1351
	cmpl	$2, %eax
	je	.L1351
	testl	%eax, %eax
	je	.L1378
.L1344:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1351:
	leaq	-240(%rbp), %rdi
	call	_ZNK2v88internal12FrameSummary16WasmFrameSummary14SourcePositionEv
	movl	%eax, %r13d
.L1352:
	cmpl	$2, -232(%rbp)
	ja	.L1344
.L1353:
	movq	32(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	leaq	-272(%rbp), %rsi
	movq	-16(%rax), %rax
	movq	135(%rax), %rax
	movq	23(%rax), %rax
	movq	7(%rax), %rax
	movq	24(%rax), %rax
	movq	(%rax), %rax
	movq	208(%rax), %rax
	movaps	%xmm0, -272(%rbp)
	movq	$0, -256(%rbp)
	movq	%rax, -376(%rbp)
	movq	(%rbx), %rax
	call	*136(%rax)
	movq	-272(%rbp), %rax
	movq	-264(%rbp), %rdi
	movl	8(%rax), %ecx
	movq	32(%rax), %rdx
	cmpq	%rdi, %rax
	je	.L1354
	.p2align 4,,10
	.p2align 3
.L1355:
	cmpl	$2, 8(%rax)
	ja	.L1344
	addq	$56, %rax
	cmpq	%rax, %rdi
	jne	.L1355
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1356
.L1354:
	movq	%rdx, -384(%rbp)
	movl	%ecx, -368(%rbp)
	call	_ZdlPv@PLT
	movq	-384(%rbp), %rdx
	movl	-368(%rbp), %ecx
.L1356:
	movl	56(%rdx), %edx
	cmpl	$2, %ecx
	ja	.L1344
	movq	-376(%rbp), %rcx
	movslq	%edx, %rax
	movl	$6, %r8d
	movq	%r12, %rdi
	salq	$5, %rax
	leaq	.LC75(%rip), %rsi
	addq	136(%rcx), %rax
	movl	%r13d, %ecx
	subl	16(%rax), %ecx
	movq	40(%rbx), %rax
	movq	(%rax), %rax
	movl	%edx, -176(%rbp)
	movl	$52, %edx
	movl	%ecx, -136(%rbp)
	movq	%r14, %rcx
	movq	%rax, -160(%rbp)
	subl	-360(%rbp), %eax
	movl	%eax, -152(%rbp)
	movq	%r15, -168(%rbp)
	movl	%r13d, -144(%rbp)
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	movl	-364(%rbp), %eax
	testl	%eax, %eax
	jne	.L1379
.L1357:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1380
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1378:
	.cfi_restore_state
	movq	-208(%rbp), %rax
	movl	-200(%rbp), %esi
	leaq	-272(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -272(%rbp)
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	movl	%eax, %r13d
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1377:
	movslq	%edx, %rdx
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1379:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	.LC30(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v88internal12StringStream3AddENS0_6VectorIKcEENS2_INS1_6FmtElmEEE@PLT
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1338:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1339
	.p2align 4,,10
	.p2align 3
.L1346:
	movq	%rbx, %rdi
	call	*%rdx
	movl	%eax, %r13d
	jmp	.L1353
.L1380:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23369:
	.size	_ZNK2v88internal17WasmCompiledFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi, .-_ZNK2v88internal17WasmCompiledFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.section	.text._ZNK2v88internal12FrameSummary16WasmFrameSummary6scriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary16WasmFrameSummary6scriptEv
	.type	_ZNK2v88internal12FrameSummary16WasmFrameSummary6scriptEv, @function
_ZNK2v88internal12FrameSummary16WasmFrameSummary6scriptEv:
.LFB23306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	movq	135(%rax), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	39(%rax), %rsi
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L1382
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1382:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1386
.L1384:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1386:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1384
	.cfi_endproc
.LFE23306:
	.size	_ZNK2v88internal12FrameSummary16WasmFrameSummary6scriptEv, .-_ZNK2v88internal12FrameSummary16WasmFrameSummary6scriptEv
	.section	.text._ZNK2v88internal12FrameSummary16WasmFrameSummary12FunctionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary16WasmFrameSummary12FunctionNameEv
	.type	_ZNK2v88internal12FrameSummary16WasmFrameSummary12FunctionNameEv, @function
_ZNK2v88internal12FrameSummary16WasmFrameSummary12FunctionNameEv:
.LFB23307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rax
	movq	(%rdi), %r12
	movq	(%rax), %rax
	movq	41112(%r12), %rdi
	movq	135(%rax), %r13
	testq	%rdi, %rdi
	je	.L1388
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpl	$1, 8(%rbx)
	movq	%rax, %rsi
	je	.L1394
.L1391:
	movl	28(%rbx), %edx
.L1392:
	movq	(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16WasmModuleObject15GetFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	.p2align 4,,10
	.p2align 3
.L1388:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1395
.L1390:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	cmpl	$1, 8(%rbx)
	jne	.L1391
.L1394:
	movq	32(%rbx), %rax
	movl	56(%rax), %edx
	jmp	.L1392
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1390
	.cfi_endproc
.LFE23307:
	.size	_ZNK2v88internal12FrameSummary16WasmFrameSummary12FunctionNameEv, .-_ZNK2v88internal12FrameSummary16WasmFrameSummary12FunctionNameEv
	.section	.text._ZNK2v88internal12FrameSummary16WasmFrameSummary14native_contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary16WasmFrameSummary14native_contextEv
	.type	_ZNK2v88internal12FrameSummary16WasmFrameSummary14native_contextEv, @function
_ZNK2v88internal12FrameSummary16WasmFrameSummary14native_contextEv:
.LFB23308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	16(%rdi), %rax
	movq	(%rdi), %rbx
	movq	(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	151(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1397
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1397:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1401
.L1399:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1401:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1399
	.cfi_endproc
.LFE23308:
	.size	_ZNK2v88internal12FrameSummary16WasmFrameSummary14native_contextEv, .-_ZNK2v88internal12FrameSummary16WasmFrameSummary14native_contextEv
	.section	.text._ZN2v88internal12FrameSummary24WasmCompiledFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_4wasm8WasmCodeEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FrameSummary24WasmCompiledFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_4wasm8WasmCodeEib
	.type	_ZN2v88internal12FrameSummary24WasmCompiledFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_4wasm8WasmCodeEib, @function
_ZN2v88internal12FrameSummary24WasmCompiledFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_4wasm8WasmCodeEib:
.LFB23310:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	$1, 8(%rdi)
	movq	%rdx, 16(%rdi)
	movb	%r9b, 24(%rdi)
	movq	%rcx, 32(%rdi)
	movl	%r8d, 40(%rdi)
	ret
	.cfi_endproc
.LFE23310:
	.size	_ZN2v88internal12FrameSummary24WasmCompiledFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_4wasm8WasmCodeEib, .-_ZN2v88internal12FrameSummary24WasmCompiledFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_4wasm8WasmCodeEib
	.globl	_ZN2v88internal12FrameSummary24WasmCompiledFrameSummaryC1EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_4wasm8WasmCodeEib
	.set	_ZN2v88internal12FrameSummary24WasmCompiledFrameSummaryC1EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_4wasm8WasmCodeEib,_ZN2v88internal12FrameSummary24WasmCompiledFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEPNS0_4wasm8WasmCodeEib
	.section	.text._ZNK2v88internal12FrameSummary24WasmCompiledFrameSummary14function_indexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary24WasmCompiledFrameSummary14function_indexEv
	.type	_ZNK2v88internal12FrameSummary24WasmCompiledFrameSummary14function_indexEv, @function
_ZNK2v88internal12FrameSummary24WasmCompiledFrameSummary14function_indexEv:
.LFB23312:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movl	56(%rax), %eax
	ret
	.cfi_endproc
.LFE23312:
	.size	_ZNK2v88internal12FrameSummary24WasmCompiledFrameSummary14function_indexEv, .-_ZNK2v88internal12FrameSummary24WasmCompiledFrameSummary14function_indexEv
	.section	.text._ZN2v88internal12FrameSummary24WasmCompiledFrameSummary21GetWasmSourcePositionEPKNS0_4wasm8WasmCodeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FrameSummary24WasmCompiledFrameSummary21GetWasmSourcePositionEPKNS0_4wasm8WasmCodeEi
	.type	_ZN2v88internal12FrameSummary24WasmCompiledFrameSummary21GetWasmSourcePositionEPKNS0_4wasm8WasmCodeEi, @function
_ZN2v88internal12FrameSummary24WasmCompiledFrameSummary21GetWasmSourcePositionEPKNS0_4wasm8WasmCodeEi:
.LFB23313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r13
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$88, %rsp
	movq	40(%rdi), %rdx
	movq	32(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE@PLT
	cmpl	$-1, -88(%rbp)
	jne	.L1406
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1411:
	movq	-72(%rbp), %rax
	movq	%r13, %rdi
	shrq	%rax
	andl	$1073741823, %eax
	leal	-1(%rax), %r12d
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -88(%rbp)
	je	.L1404
.L1406:
	cmpl	-80(%rbp), %ebx
	jg	.L1411
.L1404:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1412
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1412:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23313:
	.size	_ZN2v88internal12FrameSummary24WasmCompiledFrameSummary21GetWasmSourcePositionEPKNS0_4wasm8WasmCodeEi, .-_ZN2v88internal12FrameSummary24WasmCompiledFrameSummary21GetWasmSourcePositionEPKNS0_4wasm8WasmCodeEi
	.section	.text._ZNK2v88internal12FrameSummary24WasmCompiledFrameSummary11byte_offsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary24WasmCompiledFrameSummary11byte_offsetEv
	.type	_ZNK2v88internal12FrameSummary24WasmCompiledFrameSummary11byte_offsetEv, @function
_ZNK2v88internal12FrameSummary24WasmCompiledFrameSummary11byte_offsetEv:
.LFB23314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-112(%rbp), %r13
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movl	40(%rdi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	%r13, %rdi
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE@PLT
	cmpl	$-1, -88(%rbp)
	jne	.L1415
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1420:
	movq	-72(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	shrq	%r12
	andl	$1073741823, %r12d
	subl	$1, %r12d
	cmpl	$-1, -88(%rbp)
	je	.L1413
.L1415:
	cmpl	-80(%rbp), %ebx
	jg	.L1420
.L1413:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1421
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1421:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23314:
	.size	_ZNK2v88internal12FrameSummary24WasmCompiledFrameSummary11byte_offsetEv, .-_ZNK2v88internal12FrameSummary24WasmCompiledFrameSummary11byte_offsetEv
	.section	.text._ZN2v88internal12FrameSummary27WasmInterpretedFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEji,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FrameSummary27WasmInterpretedFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEji
	.type	_ZN2v88internal12FrameSummary27WasmInterpretedFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEji, @function
_ZN2v88internal12FrameSummary27WasmInterpretedFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEji:
.LFB23316:
	.cfi_startproc
	endbr64
	movq	%rsi, (%rdi)
	movl	$2, 8(%rdi)
	movq	%rdx, 16(%rdi)
	movb	$0, 24(%rdi)
	movl	%ecx, 28(%rdi)
	movl	%r8d, 32(%rdi)
	ret
	.cfi_endproc
.LFE23316:
	.size	_ZN2v88internal12FrameSummary27WasmInterpretedFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEji, .-_ZN2v88internal12FrameSummary27WasmInterpretedFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEji
	.globl	_ZN2v88internal12FrameSummary27WasmInterpretedFrameSummaryC1EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEji
	.set	_ZN2v88internal12FrameSummary27WasmInterpretedFrameSummaryC1EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEji,_ZN2v88internal12FrameSummary27WasmInterpretedFrameSummaryC2EPNS0_7IsolateENS0_6HandleINS0_18WasmInstanceObjectEEEji
	.section	.text._ZN2v88internal12FrameSummaryD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FrameSummaryD2Ev
	.type	_ZN2v88internal12FrameSummaryD2Ev, @function
_ZN2v88internal12FrameSummaryD2Ev:
.LFB23319:
	.cfi_startproc
	endbr64
	cmpl	$2, 8(%rdi)
	ja	.L1428
	ret
.L1428:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23319:
	.size	_ZN2v88internal12FrameSummaryD2Ev, .-_ZN2v88internal12FrameSummaryD2Ev
	.globl	_ZN2v88internal12FrameSummaryD1Ev
	.set	_ZN2v88internal12FrameSummaryD1Ev,_ZN2v88internal12FrameSummaryD2Ev
	.section	.text._ZN2v88internal12FrameSummary6GetTopEPKNS0_13StandardFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FrameSummary6GetTopEPKNS0_13StandardFrameE
	.type	_ZN2v88internal12FrameSummary6GetTopEPKNS0_13StandardFrameE, @function
_ZN2v88internal12FrameSummary6GetTopEPKNS0_13StandardFrameE:
.LFB23321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	-48(%rbp), %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	$0, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	*136(%rax)
	movq	-40(%rbp), %rax
	movq	-48(%rbp), %rdi
	movdqu	-56(%rax), %xmm1
	movups	%xmm1, (%r12)
	movdqu	-40(%rax), %xmm2
	movups	%xmm2, 16(%r12)
	movdqu	-24(%rax), %xmm3
	movups	%xmm3, 32(%r12)
	movq	-8(%rax), %rdx
	movq	%rdx, 48(%r12)
	cmpq	%rdi, %rax
	je	.L1430
	.p2align 4,,10
	.p2align 3
.L1432:
	cmpl	$2, 8(%rdi)
	ja	.L1440
	addq	$56, %rdi
	cmpq	%rdi, %rax
	jne	.L1432
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1429
.L1430:
	call	_ZdlPv@PLT
.L1429:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1441
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1440:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1441:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23321:
	.size	_ZN2v88internal12FrameSummary6GetTopEPKNS0_13StandardFrameE, .-_ZN2v88internal12FrameSummary6GetTopEPKNS0_13StandardFrameE
	.section	.text._ZN2v88internal12FrameSummary9GetBottomEPKNS0_13StandardFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FrameSummary9GetBottomEPKNS0_13StandardFrameE
	.type	_ZN2v88internal12FrameSummary9GetBottomEPKNS0_13StandardFrameE, @function
_ZN2v88internal12FrameSummary9GetBottomEPKNS0_13StandardFrameE:
.LFB23331:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	-48(%rbp), %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	$0, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	*136(%rax)
	movq	-48(%rbp), %rdx
	movq	-40(%rbp), %rdi
	movdqu	(%rdx), %xmm1
	movups	%xmm1, (%r12)
	movdqu	16(%rdx), %xmm2
	movups	%xmm2, 16(%r12)
	movdqu	32(%rdx), %xmm3
	movups	%xmm3, 32(%r12)
	movq	48(%rdx), %rax
	movq	%rax, 48(%r12)
	cmpq	%rdi, %rdx
	je	.L1443
	.p2align 4,,10
	.p2align 3
.L1445:
	cmpl	$2, 8(%rdx)
	ja	.L1453
	addq	$56, %rdx
	cmpq	%rdx, %rdi
	jne	.L1445
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1442
.L1443:
	call	_ZdlPv@PLT
.L1442:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1454
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1453:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1454:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23331:
	.size	_ZN2v88internal12FrameSummary9GetBottomEPKNS0_13StandardFrameE, .-_ZN2v88internal12FrameSummary9GetBottomEPKNS0_13StandardFrameE
	.section	.text._ZN2v88internal12FrameSummary9GetSingleEPKNS0_13StandardFrameE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FrameSummary9GetSingleEPKNS0_13StandardFrameE
	.type	_ZN2v88internal12FrameSummary9GetSingleEPKNS0_13StandardFrameE, @function
_ZN2v88internal12FrameSummary9GetSingleEPKNS0_13StandardFrameE:
.LFB23332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	-48(%rbp), %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	$0, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	*136(%rax)
	movq	-48(%rbp), %rdx
	movq	-40(%rbp), %rdi
	movdqu	(%rdx), %xmm1
	movups	%xmm1, (%r12)
	movdqu	16(%rdx), %xmm2
	movups	%xmm2, 16(%r12)
	movdqu	32(%rdx), %xmm3
	movups	%xmm3, 32(%r12)
	movq	48(%rdx), %rax
	movq	%rax, 48(%r12)
	cmpq	%rdi, %rdx
	je	.L1456
	.p2align 4,,10
	.p2align 3
.L1458:
	cmpl	$2, 8(%rdx)
	ja	.L1466
	addq	$56, %rdx
	cmpq	%rdx, %rdi
	jne	.L1458
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1455
.L1456:
	call	_ZdlPv@PLT
.L1455:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1467
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1466:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1467:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23332:
	.size	_ZN2v88internal12FrameSummary9GetSingleEPKNS0_13StandardFrameE, .-_ZN2v88internal12FrameSummary9GetSingleEPKNS0_13StandardFrameE
	.section	.text._ZN2v88internal12FrameSummary3GetEPKNS0_13StandardFrameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal12FrameSummary3GetEPKNS0_13StandardFrameEi
	.type	_ZN2v88internal12FrameSummary3GetEPKNS0_13StandardFrameEi, @function
_ZN2v88internal12FrameSummary3GetEPKNS0_13StandardFrameEi:
.LFB23333:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	leaq	-48(%rbp), %rsi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	$0, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	*136(%rax)
	movslq	%ebx, %rdx
	movq	-48(%rbp), %rax
	movq	-40(%rbp), %rdi
	leaq	0(,%rdx,8), %rcx
	subq	%rdx, %rcx
	leaq	(%rax,%rcx,8), %rdx
	movdqu	(%rdx), %xmm1
	movups	%xmm1, (%r12)
	movdqu	16(%rdx), %xmm2
	movups	%xmm2, 16(%r12)
	movdqu	32(%rdx), %xmm3
	movups	%xmm3, 32(%r12)
	movq	48(%rdx), %rdx
	movq	%rdx, 48(%r12)
	cmpq	%rdi, %rax
	je	.L1469
	.p2align 4,,10
	.p2align 3
.L1471:
	cmpl	$2, 8(%rax)
	ja	.L1479
	addq	$56, %rax
	cmpq	%rax, %rdi
	jne	.L1471
	movq	-48(%rbp), %rdi
.L1469:
	testq	%rdi, %rdi
	je	.L1468
	call	_ZdlPv@PLT
.L1468:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1480
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1479:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1480:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23333:
	.size	_ZN2v88internal12FrameSummary3GetEPKNS0_13StandardFrameEi, .-_ZN2v88internal12FrameSummary3GetEPKNS0_13StandardFrameEi
	.section	.text._ZNK2v88internal12FrameSummary8receiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary8receiverEv
	.type	_ZNK2v88internal12FrameSummary8receiverEv, @function
_ZNK2v88internal12FrameSummary8receiverEv:
.LFB23334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	cmpl	$1, %eax
	je	.L1483
	cmpl	$2, %eax
	je	.L1483
	testl	%eax, %eax
	je	.L1498
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1483:
	movq	16(%rdi), %rax
	leaq	-32(%rbp), %rdi
	movq	(%rax), %rax
	andq	$-262144, %rax
	movq	24(%rax), %rbx
	movq	-25128(%rbx), %rax
	subq	$37592, %rbx
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal7Context12global_proxyEv@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L1489
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
.L1485:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1499
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1489:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1500
.L1491:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1498:
	movq	16(%rdi), %rax
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	%rbx, %rdi
	movq	%rsi, -40(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-40(%rbp), %rsi
	jmp	.L1491
.L1499:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23334:
	.size	_ZNK2v88internal12FrameSummary8receiverEv, .-_ZNK2v88internal12FrameSummary8receiverEv
	.section	.text._ZNK2v88internal12FrameSummary11code_offsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary11code_offsetEv
	.type	_ZNK2v88internal12FrameSummary11code_offsetEv, @function
_ZNK2v88internal12FrameSummary11code_offsetEv:
.LFB23335:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	cmpl	$1, %eax
	ja	.L1510
	movl	40(%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1510:
	cmpl	$2, %eax
	jne	.L1511
	movl	32(%rdi), %eax
	ret
.L1511:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23335:
	.size	_ZNK2v88internal12FrameSummary11code_offsetEv, .-_ZNK2v88internal12FrameSummary11code_offsetEv
	.section	.text._ZNK2v88internal12FrameSummary14is_constructorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary14is_constructorEv
	.type	_ZNK2v88internal12FrameSummary14is_constructorEv, @function
_ZNK2v88internal12FrameSummary14is_constructorEv:
.LFB23336:
	.cfi_startproc
	endbr64
	movl	8(%rdi), %eax
	testl	%eax, %eax
	je	.L1513
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L1514
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1513:
	movzbl	44(%rdi), %eax
	ret
.L1514:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23336:
	.size	_ZNK2v88internal12FrameSummary14is_constructorEv, .-_ZNK2v88internal12FrameSummary14is_constructorEv
	.section	.text._ZNK2v88internal12FrameSummary23is_subject_to_debuggingEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary23is_subject_to_debuggingEv
	.type	_ZNK2v88internal12FrameSummary23is_subject_to_debuggingEv, @function
_ZNK2v88internal12FrameSummary23is_subject_to_debuggingEv:
.LFB23337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	testl	%eax, %eax
	je	.L1519
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L1520
	movl	$1, %eax
.L1518:
	movq	-24(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L1530
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1519:
	.cfi_restore_state
	movq	24(%rdi), %rax
	movq	(%rax), %rax
	movq	23(%rax), %rbx
	movq	31(%rbx), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	je	.L1531
.L1522:
	leaq	-32(%rbp), %rdi
	movq	%rdx, -32(%rbp)
	call	_ZN2v88internal6Script16IsUserJavaScriptEv@PLT
	testb	%al, %al
	je	.L1518
	movq	7(%rbx), %rdx
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	jne	.L1518
	movq	-1(%rdx), %rax
	cmpw	$83, 11(%rax)
	setne	%al
	jmp	.L1518
	.p2align 4,,10
	.p2align 3
.L1531:
	movq	-1(%rdx), %rcx
	cmpw	$86, 11(%rcx)
	je	.L1532
.L1523:
	movq	%rdx, %rcx
	andq	$-262144, %rcx
	movq	24(%rcx), %rcx
	cmpq	%rdx, -37504(%rcx)
	je	.L1518
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1532:
	movq	23(%rdx), %rdx
	testb	$1, %dl
	jne	.L1523
	jmp	.L1522
.L1520:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1530:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23337:
	.size	_ZNK2v88internal12FrameSummary23is_subject_to_debuggingEv, .-_ZNK2v88internal12FrameSummary23is_subject_to_debuggingEv
	.section	.text._ZNK2v88internal12FrameSummary6scriptEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary6scriptEv
	.type	_ZNK2v88internal12FrameSummary6scriptEv, @function
_ZNK2v88internal12FrameSummary6scriptEv:
.LFB23338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movl	8(%rdi), %eax
	cmpl	$1, %eax
	je	.L1535
	cmpl	$2, %eax
	je	.L1535
	testl	%eax, %eax
	je	.L1549
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1535:
	movq	16(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	movq	135(%rax), %rax
	andq	$-262144, %rdx
	movq	24(%rdx), %rbx
	movq	39(%rax), %rsi
	movq	3520(%rbx), %rdi
	subq	$37592, %rbx
	testq	%rdi, %rdi
	je	.L1541
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1541:
	.cfi_restore_state
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1550
.L1543:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1549:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88internal12FrameSummary22JavaScriptFrameSummary6scriptEv
	.p2align 4,,10
	.p2align 3
.L1550:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1543
	.cfi_endproc
.LFE23338:
	.size	_ZNK2v88internal12FrameSummary6scriptEv, .-_ZNK2v88internal12FrameSummary6scriptEv
	.section	.text._ZNK2v88internal12FrameSummary14SourcePositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary14SourcePositionEv
	.type	_ZNK2v88internal12FrameSummary14SourcePositionEv, @function
_ZNK2v88internal12FrameSummary14SourcePositionEv:
.LFB23339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	testl	%eax, %eax
	je	.L1552
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L1559
	call	_ZNK2v88internal12FrameSummary16WasmFrameSummary14SourcePositionEv
.L1551:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1560
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1552:
	.cfi_restore_state
	movq	32(%rdi), %rax
	movl	40(%rdi), %esi
	leaq	-16(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	jmp	.L1551
.L1559:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1560:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23339:
	.size	_ZNK2v88internal12FrameSummary14SourcePositionEv, .-_ZNK2v88internal12FrameSummary14SourcePositionEv
	.section	.text._ZNK2v88internal12FrameSummary23SourceStatementPositionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary23SourceStatementPositionEv
	.type	_ZNK2v88internal12FrameSummary23SourceStatementPositionEv, @function
_ZNK2v88internal12FrameSummary23SourceStatementPositionEv:
.LFB23340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	8(%rdi), %eax
	testl	%eax, %eax
	je	.L1562
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L1569
	call	_ZNK2v88internal12FrameSummary16WasmFrameSummary14SourcePositionEv
.L1561:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1570
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1562:
	.cfi_restore_state
	movq	32(%rdi), %rax
	movl	40(%rdi), %esi
	leaq	-16(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	call	_ZN2v88internal12AbstractCode23SourceStatementPositionEi@PLT
	jmp	.L1561
.L1569:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1570:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23340:
	.size	_ZNK2v88internal12FrameSummary23SourceStatementPositionEv, .-_ZNK2v88internal12FrameSummary23SourceStatementPositionEv
	.section	.text._ZNK2v88internal12FrameSummary12FunctionNameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary12FunctionNameEv
	.type	_ZNK2v88internal12FrameSummary12FunctionNameEv, @function
_ZNK2v88internal12FrameSummary12FunctionNameEv:
.LFB23341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	8(%rdi), %eax
	cmpl	$1, %eax
	je	.L1573
	cmpl	$2, %eax
	je	.L1573
	testl	%eax, %eax
	je	.L1592
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	(%rbx), %r12
	movq	16(%rbx), %rax
	movq	41112(%r12), %rdi
	movq	(%rax), %rax
	movq	135(%rax), %r13
	testq	%rdi, %rdi
	je	.L1580
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpl	$1, 8(%rbx)
	movq	%rax, %rsi
	je	.L1593
.L1583:
	movl	28(%rbx), %edx
.L1584:
	movq	(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal16WasmModuleObject15GetFunctionNameEPNS0_7IsolateENS0_6HandleIS1_EEj@PLT
	.p2align 4,,10
	.p2align 3
.L1580:
	.cfi_restore_state
	movq	41088(%r12), %rsi
	cmpq	41096(%r12), %rsi
	je	.L1594
.L1582:
	leaq	8(%rsi), %rax
	movq	%rax, 41088(%r12)
	movq	%r13, (%rsi)
	cmpl	$1, 8(%rbx)
	jne	.L1583
.L1593:
	movq	32(%rbx), %rax
	movl	56(%rax), %edx
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	24(%rdi), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal10JSFunction12GetDebugNameENS0_6HandleIS1_EE@PLT
	.p2align 4,,10
	.p2align 3
.L1594:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rsi
	jmp	.L1582
	.cfi_endproc
.LFE23341:
	.size	_ZNK2v88internal12FrameSummary12FunctionNameEv, .-_ZNK2v88internal12FrameSummary12FunctionNameEv
	.section	.text._ZNK2v88internal12FrameSummary14native_contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal12FrameSummary14native_contextEv
	.type	_ZNK2v88internal12FrameSummary14native_contextEv, @function
_ZNK2v88internal12FrameSummary14native_contextEv:
.LFB23342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movl	8(%rdi), %eax
	cmpl	$1, %eax
	je	.L1597
	cmpl	$2, %eax
	je	.L1597
	testl	%eax, %eax
	je	.L1615
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	16(%rdi), %rax
	movq	(%rdi), %rbx
	movq	(%rax), %rax
	movq	41112(%rbx), %rdi
	movq	151(%rax), %rsi
	testq	%rdi, %rdi
	je	.L1606
.L1610:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1615:
	.cfi_restore_state
	movq	24(%rdi), %rax
	movq	(%rdi), %rbx
	movq	(%rax), %rax
	movq	31(%rax), %rax
	movq	39(%rax), %rsi
	movq	41112(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L1610
.L1606:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L1616
.L1608:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1616:
	.cfi_restore_state
	movq	%rbx, %rdi
	movq	%rsi, -24(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-24(%rbp), %rsi
	jmp	.L1608
	.cfi_endproc
.LFE23342:
	.size	_ZNK2v88internal12FrameSummary14native_contextEv, .-_ZNK2v88internal12FrameSummary14native_contextEv
	.section	.text._ZNK2v88internal14OptimizedFrame21GetDeoptimizationDataEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14OptimizedFrame21GetDeoptimizationDataEPi
	.type	_ZNK2v88internal14OptimizedFrame21GetDeoptimizationDataEPi, @function
_ZNK2v88internal14OptimizedFrame21GetDeoptimizationDataEPi:
.LFB23348:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	152(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1618
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
.L1619:
	movq	47(%rax), %rax
	movq	40(%rbx), %rdx
	leaq	-88(%rbp), %r13
	movq	%rax, -88(%rbp)
	movq	(%rdx), %r14
	movl	43(%rax), %edx
	testl	%edx, %edx
	js	.L1632
.L1620:
	leaq	-1(%rax), %r15
	cmpq	%r15, %r14
	jnb	.L1624
.L1626:
	movq	40(%rbx), %rax
	movq	16(%rbx), %rcx
	movq	(%rax), %rsi
	leaq	37592(%rcx), %rdi
	call	_ZN2v88internal4Heap29GcSafeFindCodeForInnerPointerEm@PLT
	movq	%rax, -88(%rbp)
.L1623:
	movq	40(%rbx), %rax
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	movq	(%rax), %rdx
	call	_ZN2v88internal4Code17GetSafepointEntryEm@PLT
	movl	-80(%rbp), %eax
	cmpl	$-1, %eax
	je	.L1627
	movl	%eax, (%r12)
	movq	-88(%rbp), %rax
	movq	15(%rax), %rax
.L1628:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1633
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1627:
	.cfi_restore_state
	movl	$-1, (%r12)
	xorl	%eax, %eax
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1624:
	movq	-1(%rax), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal10HeapObject11SizeFromMapENS0_3MapE@PLT
	cltq
	addq	%rax, %r15
	cmpq	%r15, %r14
	jnb	.L1626
	jmp	.L1623
	.p2align 4,,10
	.p2align 3
.L1632:
	movq	%r13, %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	cmpq	%rax, %r14
	jnb	.L1621
	movq	-88(%rbp), %rax
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1621:
	movq	%r13, %rdi
	call	_ZNK2v88internal4Code21OffHeapInstructionEndEv@PLT
	movq	%rax, %r8
	movq	-88(%rbp), %rax
	cmpq	%r8, %r14
	jnb	.L1620
	jmp	.L1623
	.p2align 4,,10
	.p2align 3
.L1618:
	call	*%rax
	jmp	.L1619
.L1633:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23348:
	.size	_ZNK2v88internal14OptimizedFrame21GetDeoptimizationDataEPi, .-_ZNK2v88internal14OptimizedFrame21GetDeoptimizationDataEPi
	.section	.text._ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi
	.type	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi, @function
_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi:
.LFB23351:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	subl	%edi, %eax
	sall	$3, %eax
	ret
	.cfi_endproc
.LFE23351:
	.size	_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi, .-_ZN2v88internal14OptimizedFrame27StackSlotOffsetRelativeToFpEi
	.section	.text._ZNK2v88internal14OptimizedFrame11StackSlotAtEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14OptimizedFrame11StackSlotAtEi
	.type	_ZNK2v88internal14OptimizedFrame11StackSlotAtEi, @function
_ZNK2v88internal14OptimizedFrame11StackSlotAtEi:
.LFB23352:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	movq	32(%rdi), %rdx
	subl	%esi, %eax
	sall	$3, %eax
	cltq
	movq	(%rax,%rdx), %rax
	ret
	.cfi_endproc
.LFE23352:
	.size	_ZNK2v88internal14OptimizedFrame11StackSlotAtEi, .-_ZNK2v88internal14OptimizedFrame11StackSlotAtEi
	.section	.text._ZNK2v88internal16InterpretedFrame17GetBytecodeOffsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16InterpretedFrame17GetBytecodeOffsetEv
	.type	_ZNK2v88internal16InterpretedFrame17GetBytecodeOffsetEv, @function
_ZNK2v88internal16InterpretedFrame17GetBytecodeOffsetEv:
.LFB23355:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi(%rip), %rdx
	movq	144(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1637
	movq	32(%rdi), %rax
	subq	$32, %rax
	movq	(%rax), %rax
	sarq	$32, %rax
	subl	$53, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1637:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*%rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rax), %rax
	sarq	$32, %rax
	subl	$53, %eax
	ret
	.cfi_endproc
.LFE23355:
	.size	_ZNK2v88internal16InterpretedFrame17GetBytecodeOffsetEv, .-_ZNK2v88internal16InterpretedFrame17GetBytecodeOffsetEv
	.section	.text._ZN2v88internal16InterpretedFrame17GetBytecodeOffsetEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16InterpretedFrame17GetBytecodeOffsetEm
	.type	_ZN2v88internal16InterpretedFrame17GetBytecodeOffsetEm, @function
_ZN2v88internal16InterpretedFrame17GetBytecodeOffsetEm:
.LFB23356:
	.cfi_startproc
	endbr64
	movslq	-28(%rdi), %rax
	subl	$53, %eax
	ret
	.cfi_endproc
.LFE23356:
	.size	_ZN2v88internal16InterpretedFrame17GetBytecodeOffsetEm, .-_ZN2v88internal16InterpretedFrame17GetBytecodeOffsetEm
	.section	.text._ZN2v88internal16InterpretedFrame19PatchBytecodeOffsetEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16InterpretedFrame19PatchBytecodeOffsetEi
	.type	_ZN2v88internal16InterpretedFrame19PatchBytecodeOffsetEi, @function
_ZN2v88internal16InterpretedFrame19PatchBytecodeOffsetEi:
.LFB23357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leal	53(%rsi), %ebx
	salq	$32, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	144(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1644
	movq	32(%rdi), %rax
	movq	%rbx, -32(%rax)
	addq	$8, %rsp
	subq	$32, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1644:
	.cfi_restore_state
	movl	$-1, %esi
	call	*%rax
	movq	%rbx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23357:
	.size	_ZN2v88internal16InterpretedFrame19PatchBytecodeOffsetEi, .-_ZN2v88internal16InterpretedFrame19PatchBytecodeOffsetEi
	.section	.text._ZNK2v88internal16InterpretedFrame16GetBytecodeArrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16InterpretedFrame16GetBytecodeArrayEv
	.type	_ZNK2v88internal16InterpretedFrame16GetBytecodeArrayEv, @function
_ZNK2v88internal16InterpretedFrame16GetBytecodeArrayEv:
.LFB23358:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi(%rip), %rdx
	movq	144(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1648
	movq	32(%rdi), %rax
	subq	$24, %rax
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1648:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-2, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*%rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE23358:
	.size	_ZNK2v88internal16InterpretedFrame16GetBytecodeArrayEv, .-_ZNK2v88internal16InterpretedFrame16GetBytecodeArrayEv
	.section	.text._ZN2v88internal16InterpretedFrame18PatchBytecodeArrayENS0_13BytecodeArrayE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16InterpretedFrame18PatchBytecodeArrayENS0_13BytecodeArrayE
	.type	_ZN2v88internal16InterpretedFrame18PatchBytecodeArrayENS0_13BytecodeArrayE, @function
_ZN2v88internal16InterpretedFrame18PatchBytecodeArrayENS0_13BytecodeArrayE:
.LFB23359:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	144(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1654
	movq	32(%rdi), %rax
	movq	%rbx, -24(%rax)
	addq	$8, %rsp
	subq	$24, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1654:
	.cfi_restore_state
	movl	$-2, %esi
	call	*%rax
	movq	%rbx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23359:
	.size	_ZN2v88internal16InterpretedFrame18PatchBytecodeArrayENS0_13BytecodeArrayE, .-_ZN2v88internal16InterpretedFrame18PatchBytecodeArrayENS0_13BytecodeArrayE
	.section	.text._ZNK2v88internal16InterpretedFrame23ReadInterpreterRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16InterpretedFrame23ReadInterpreterRegisterEi
	.type	_ZNK2v88internal16InterpretedFrame23ReadInterpreterRegisterEi, @function
_ZNK2v88internal16InterpretedFrame23ReadInterpreterRegisterEi:
.LFB23360:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi(%rip), %rdx
	movq	144(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1658
	movq	32(%rdi), %rax
	sall	$3, %esi
	movslq	%esi, %rsi
	subq	%rsi, %rax
	subq	$40, %rax
	movq	(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1658:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	*%rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	movq	(%rax), %rax
	ret
	.cfi_endproc
.LFE23360:
	.size	_ZNK2v88internal16InterpretedFrame23ReadInterpreterRegisterEi, .-_ZNK2v88internal16InterpretedFrame23ReadInterpreterRegisterEi
	.section	.text._ZN2v88internal16InterpretedFrame24WriteInterpreterRegisterEiNS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal16InterpretedFrame24WriteInterpreterRegisterEiNS0_6ObjectE
	.type	_ZN2v88internal16InterpretedFrame24WriteInterpreterRegisterEiNS0_6ObjectE, @function
_ZN2v88internal16InterpretedFrame24WriteInterpreterRegisterEiNS0_6ObjectE:
.LFB23361:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdx, %rbx
	leaq	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi(%rip), %rdx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	144(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1664
	movq	32(%rdi), %rax
	sall	$3, %esi
	movslq	%esi, %rsi
	subq	%rsi, %rax
	movq	%rbx, -40(%rax)
	addq	$8, %rsp
	subq	$40, %rax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1664:
	.cfi_restore_state
	call	*%rax
	movq	%rbx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23361:
	.size	_ZN2v88internal16InterpretedFrame24WriteInterpreterRegisterEiNS0_6ObjectE, .-_ZN2v88internal16InterpretedFrame24WriteInterpreterRegisterEiNS0_6ObjectE
	.section	.text._ZNK2v88internal17WasmCompiledFrame9wasm_codeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17WasmCompiledFrame9wasm_codeEv
	.type	_ZNK2v88internal17WasmCompiledFrame9wasm_codeEv, @function
_ZNK2v88internal17WasmCompiledFrame9wasm_codeEv:
.LFB23373:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdx
	movq	40(%rdi), %rax
	movq	45752(%rdx), %rdi
	movq	(%rax), %rsi
	addq	$280, %rdi
	jmp	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm@PLT
	.cfi_endproc
.LFE23373:
	.size	_ZNK2v88internal17WasmCompiledFrame9wasm_codeEv, .-_ZNK2v88internal17WasmCompiledFrame9wasm_codeEv
	.section	.text._ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv
	.type	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv, @function
_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv:
.LFB23374:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	ret
	.cfi_endproc
.LFE23374:
	.size	_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv, .-_ZNK2v88internal17WasmCompiledFrame13wasm_instanceEv
	.section	.text._ZNK2v88internal17WasmCompiledFrame13module_objectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17WasmCompiledFrame13module_objectEv
	.type	_ZNK2v88internal17WasmCompiledFrame13module_objectEv, @function
_ZNK2v88internal17WasmCompiledFrame13module_objectEv:
.LFB23375:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	movq	135(%rax), %rax
	ret
	.cfi_endproc
.LFE23375:
	.size	_ZNK2v88internal17WasmCompiledFrame13module_objectEv, .-_ZNK2v88internal17WasmCompiledFrame13module_objectEv
	.section	.text._ZNK2v88internal17WasmCompiledFrame14function_indexEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17WasmCompiledFrame14function_indexEv
	.type	_ZNK2v88internal17WasmCompiledFrame14function_indexEv, @function
_ZNK2v88internal17WasmCompiledFrame14function_indexEv:
.LFB23376:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	leaq	-48(%rbp), %rsi
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	$0, -32(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	*136(%rax)
	movq	-48(%rbp), %rax
	movq	-40(%rbp), %rdi
	movl	8(%rax), %ebx
	movq	32(%rax), %r12
	cmpq	%rdi, %rax
	je	.L1671
	.p2align 4,,10
	.p2align 3
.L1673:
	cmpl	$2, 8(%rax)
	ja	.L1675
	addq	$56, %rax
	cmpq	%rax, %rdi
	jne	.L1673
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1674
.L1671:
	call	_ZdlPv@PLT
.L1674:
	movl	56(%r12), %eax
	cmpl	$2, %ebx
	ja	.L1675
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L1682
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1675:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1682:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23376:
	.size	_ZNK2v88internal17WasmCompiledFrame14function_indexEv, .-_ZNK2v88internal17WasmCompiledFrame14function_indexEv
	.section	.text._ZNK2v88internal17WasmCompiledFrame23at_to_number_conversionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17WasmCompiledFrame23at_to_number_conversionEv
	.type	_ZNK2v88internal17WasmCompiledFrame23at_to_number_conversionEv, @function
_ZNK2v88internal17WasmCompiledFrame23at_to_number_conversionEv:
.LFB23380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	48(%rdi), %rax
	testq	%rax, %rax
	je	.L1693
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L1704
.L1693:
	xorl	%eax, %eax
.L1683:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1705
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1704:
	.cfi_restore_state
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movq	45752(%rax), %rdi
	addq	$280, %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm@PLT
	testq	%rax, %rax
	je	.L1693
	cmpl	$2, 60(%rax)
	jne	.L1693
	movq	48(%rbx), %rdx
	xorl	%r12d, %r12d
	testq	%rdx, %rdx
	je	.L1687
	movl	(%rdx), %r12d
.L1687:
	movq	40(%rax), %rdx
	movq	32(%rax), %rsi
	leaq	-112(%rbp), %r13
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	subl	(%rax), %r12d
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE@PLT
	cmpl	$-1, -88(%rbp)
	je	.L1693
	xorl	%ebx, %ebx
	jmp	.L1690
	.p2align 4,,10
	.p2align 3
.L1689:
	movq	-72(%rbp), %rax
	movq	%r13, %rdi
	shrq	%rax
	andl	$1073741823, %eax
	leal	-1(%rax), %ebx
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -88(%rbp)
	je	.L1703
.L1690:
	cmpl	-80(%rbp), %r12d
	jg	.L1689
.L1703:
	testl	%ebx, %ebx
	setne	%al
	jmp	.L1683
.L1705:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23380:
	.size	_ZNK2v88internal17WasmCompiledFrame23at_to_number_conversionEv, .-_ZNK2v88internal17WasmCompiledFrame23at_to_number_conversionEv
	.section	.text._ZN2v88internal17WasmCompiledFrame29LookupExceptionHandlerInTableEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17WasmCompiledFrame29LookupExceptionHandlerInTableEPi
	.type	_ZN2v88internal17WasmCompiledFrame29LookupExceptionHandlerInTableEPi, @function
_ZN2v88internal17WasmCompiledFrame29LookupExceptionHandlerInTableEPi:
.LFB23381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	45752(%rdx), %rdi
	movq	(%rax), %rsi
	addq	$280, %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm@PLT
	cmpl	$-1, 56(%rax)
	je	.L1709
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88internal4wasm8WasmCode18handler_table_sizeEv@PLT
	testl	%eax, %eax
	je	.L1709
	movq	%rbx, %rdi
	leaq	-80(%rbp), %r15
	call	_ZNK2v88internal4wasm8WasmCode18handler_table_sizeEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %r14d
	call	_ZNK2v88internal4wasm8WasmCode13handler_tableEv@PLT
	movq	%r15, %rdi
	movl	$1, %ecx
	movl	%r14d, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal12HandlerTableC1EmiNS1_12EncodingModeE@PLT
	movq	40(%r12), %rax
	movq	%r15, %rdi
	movq	(%rax), %rsi
	movl	72(%rbx), %eax
	subl	(%rbx), %esi
	movl	%eax, 0(%r13)
	call	_ZN2v88internal12HandlerTable12LookupReturnEi@PLT
.L1706:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1715
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1709:
	.cfi_restore_state
	movl	$-1, %eax
	jmp	.L1706
.L1715:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23381:
	.size	_ZN2v88internal17WasmCompiledFrame29LookupExceptionHandlerInTableEPi, .-_ZN2v88internal17WasmCompiledFrame29LookupExceptionHandlerInTableEPi
	.section	.text._ZNK2v88internal25WasmInterpreterEntryFrame13wasm_instanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25WasmInterpreterEntryFrame13wasm_instanceEv
	.type	_ZNK2v88internal25WasmInterpreterEntryFrame13wasm_instanceEv, @function
_ZNK2v88internal25WasmInterpreterEntryFrame13wasm_instanceEv:
.LFB23395:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	ret
	.cfi_endproc
.LFE23395:
	.size	_ZNK2v88internal25WasmInterpreterEntryFrame13wasm_instanceEv, .-_ZNK2v88internal25WasmInterpreterEntryFrame13wasm_instanceEv
	.section	.text._ZNK2v88internal25WasmInterpreterEntryFrame10debug_infoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25WasmInterpreterEntryFrame10debug_infoEv
	.type	_ZNK2v88internal25WasmInterpreterEntryFrame10debug_infoEv, @function
_ZNK2v88internal25WasmInterpreterEntryFrame10debug_infoEv:
.LFB23396:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	movq	191(%rax), %rax
	ret
	.cfi_endproc
.LFE23396:
	.size	_ZNK2v88internal25WasmInterpreterEntryFrame10debug_infoEv, .-_ZNK2v88internal25WasmInterpreterEntryFrame10debug_infoEv
	.section	.text._ZNK2v88internal25WasmInterpreterEntryFrame13module_objectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25WasmInterpreterEntryFrame13module_objectEv
	.type	_ZNK2v88internal25WasmInterpreterEntryFrame13module_objectEv, @function
_ZNK2v88internal25WasmInterpreterEntryFrame13module_objectEv:
.LFB23397:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	movq	135(%rax), %rax
	ret
	.cfi_endproc
.LFE23397:
	.size	_ZNK2v88internal25WasmInterpreterEntryFrame13module_objectEv, .-_ZNK2v88internal25WasmInterpreterEntryFrame13module_objectEv
	.section	.text._ZNK2v88internal20WasmCompileLazyFrame13wasm_instanceEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20WasmCompileLazyFrame13wasm_instanceEv
	.type	_ZNK2v88internal20WasmCompileLazyFrame13wasm_instanceEv, @function
_ZNK2v88internal20WasmCompileLazyFrame13wasm_instanceEv:
.LFB23403:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	-16(%rax), %rax
	ret
	.cfi_endproc
.LFE23403:
	.size	_ZNK2v88internal20WasmCompileLazyFrame13wasm_instanceEv, .-_ZNK2v88internal20WasmCompileLazyFrame13wasm_instanceEv
	.section	.text._ZNK2v88internal20WasmCompileLazyFrame18wasm_instance_slotEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal20WasmCompileLazyFrame18wasm_instance_slotEv
	.type	_ZNK2v88internal20WasmCompileLazyFrame18wasm_instance_slotEv, @function
_ZNK2v88internal20WasmCompileLazyFrame18wasm_instance_slotEv:
.LFB23404:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	subq	$16, %rax
	ret
	.cfi_endproc
.LFE23404:
	.size	_ZNK2v88internal20WasmCompileLazyFrame18wasm_instance_slotEv, .-_ZNK2v88internal20WasmCompileLazyFrame18wasm_instance_slotEv
	.section	.text._ZNK2v88internal13StandardFrame18IterateExpressionsEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13StandardFrame18IterateExpressionsEPNS0_11RootVisitorE
	.type	_ZNK2v88internal13StandardFrame18IterateExpressionsEPNS0_11RootVisitorE, @function
_ZNK2v88internal13StandardFrame18IterateExpressionsEPNS0_11RootVisitorE:
.LFB23411:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rdx
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movl	$6, %esi
	movq	24(%rax), %rcx
	movq	32(%rax), %r8
	movq	16(%rdx), %r9
	xorl	%edx, %edx
	jmp	*%r9
	.cfi_endproc
.LFE23411:
	.size	_ZNK2v88internal13StandardFrame18IterateExpressionsEPNS0_11RootVisitorE, .-_ZNK2v88internal13StandardFrame18IterateExpressionsEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	.type	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm, @function
_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm:
.LFB23415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	40960(%rax), %r13
	cmpb	$0, 6624(%r13)
	je	.L1723
	movq	6616(%r13), %rax
.L1724:
	testq	%rax, %rax
	je	.L1725
	addl	$1, (%rax)
.L1725:
	movq	(%rbx), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal17InstructionStream11PcIsOffHeapEPNS0_7IsolateEm@PLT
	movl	%r12d, %edx
	andl	$262143, %edx
	testb	%al, %al
	jne	.L1740
.L1727:
	movl	%edx, %eax
	movq	(%rbx), %rdi
	sall	$15, %eax
	subl	%edx, %eax
	subl	$1, %eax
	movl	%eax, %edx
	shrl	$12, %edx
	xorl	%edx, %eax
	leal	(%rax,%rax,4), %eax
	movl	%eax, %edx
	shrl	$4, %edx
	xorl	%edx, %eax
	imull	$2057, %eax, %eax
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%edx, %eax
	andl	$1023, %eax
	leaq	(%rax,%rax,4), %r13
	salq	$3, %r13
	leaq	(%rbx,%r13), %r15
	leaq	8(%rbx,%r13), %r14
	cmpq	%r12, 8(%r15)
	je	.L1741
	addq	$37592, %rdi
	movq	%r12, %rsi
	call	_ZN2v88internal4Heap29GcSafeFindCodeForInnerPointerEm@PLT
	movq	%rax, 8(%r14)
	leaq	24(%rbx,%r13), %rax
	movl	$0, (%rax)
	movq	$0, 8(%rax)
	movq	%r12, 8(%r15)
.L1722:
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1723:
	.cfi_restore_state
	movb	$1, 6624(%r13)
	leaq	6600(%r13), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6616(%r13)
	jmp	.L1724
	.p2align 4,,10
	.p2align 3
.L1741:
	movq	40960(%rdi), %rbx
	cmpb	$0, 6656(%rbx)
	je	.L1729
	movq	6648(%rbx), %rax
.L1730:
	testq	%rax, %rax
	je	.L1722
	addl	$1, (%rax)
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1740:
	movq	%r13, %rdi
	call	_ZNK2v88internal7Isolate18embedded_blob_sizeEv@PLT
	movq	%r13, %rdi
	call	_ZNK2v88internal7Isolate13embedded_blobEv@PLT
	movl	%r12d, %edx
	subl	%eax, %edx
	jmp	.L1727
	.p2align 4,,10
	.p2align 3
.L1729:
	movb	$1, 6656(%rbx)
	leaq	6632(%rbx), %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 6648(%rbx)
	jmp	.L1730
	.cfi_endproc
.LFE23415:
	.size	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm, .-_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	.section	.text._ZNK2v88internal14OptimizedFrame8receiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14OptimizedFrame8receiverEv
	.type	_ZNK2v88internal14OptimizedFrame8receiverEv, @function
_ZNK2v88internal14OptimizedFrame8receiverEv:
.LFB23349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	40(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	(%rax), %rsi
	movq	41144(%rdx), %rdi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	8(%rax), %rax
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$3, %eax
	je	.L1750
	movq	(%r12), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame12GetParameterEi(%rip), %rcx
	movq	112(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1745
	movq	%r12, %rdi
	call	*120(%rax)
	leaq	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv(%rip), %rdx
	leal	0(,%rax,8), %ebx
	movq	(%r12), %rax
	movq	56(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1746
	movq	32(%r12), %rax
	addq	$16, %rax
.L1747:
	movslq	%ebx, %rbx
	movq	(%rbx,%rax), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1750:
	.cfi_restore_state
	movq	32(%r12), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	-24(%rax), %rdx
	movq	16(%rax,%rdx,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1746:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1747
	.p2align 4,,10
	.p2align 3
.L1745:
	movq	%r12, %rdi
	movl	$-1, %esi
	call	*%rdx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23349:
	.size	_ZNK2v88internal14OptimizedFrame8receiverEv, .-_ZNK2v88internal14OptimizedFrame8receiverEv
	.section	.text._ZNK2v88internal9ExitFrame7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9ExitFrame7IterateEPNS0_11RootVisitorE
	.type	_ZNK2v88internal9ExitFrame7IterateEPNS0_11RootVisitorE, @function
_ZNK2v88internal9ExitFrame7IterateEPNS0_11RootVisitorE:
.LFB23215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	41144(%rdx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	40(%rbx), %r13
	movq	8(%rax), %rdx
	movq	%rdx, -56(%rbp)
	movl	43(%rdx), %ecx
	leaq	63(%rdx), %rax
	movq	0(%r13), %rbx
	testl	%ecx, %ecx
	js	.L1763
.L1753:
	subl	%eax, %ebx
	movq	-56(%rbp), %rax
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdx
	leaq	-48(%rbp), %rcx
	movq	%rax, -48(%rbp)
	movq	(%r12), %rax
	movq	24(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L1754
	leaq	-40(%rbp), %r8
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r12, %rdi
	call	*16(%rax)
.L1755:
	movq	-48(%rbp), %rdx
	cmpq	-56(%rbp), %rdx
	je	.L1751
	movq	%rdx, -56(%rbp)
	leaq	63(%rdx), %rax
	movl	43(%rdx), %edx
	testl	%edx, %edx
	js	.L1764
.L1759:
	addq	%rax, %rbx
	movq	%rbx, 0(%r13)
.L1751:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1765
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1763:
	.cfi_restore_state
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L1764:
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L1759
	.p2align 4,,10
	.p2align 3
.L1754:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r12, %rdi
	call	*%r8
	jmp	.L1755
.L1765:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23215:
	.size	_ZNK2v88internal9ExitFrame7IterateEPNS0_11RootVisitorE, .-_ZNK2v88internal9ExitFrame7IterateEPNS0_11RootVisitorE
	.section	.text._ZNK2v88internal10EntryFrame7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10EntryFrame7IterateEPNS0_11RootVisitorE
	.type	_ZNK2v88internal10EntryFrame7IterateEPNS0_11RootVisitorE, @function
_ZNK2v88internal10EntryFrame7IterateEPNS0_11RootVisitorE:
.LFB23410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	41144(%rdx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	40(%rbx), %r13
	movq	8(%rax), %rdx
	movq	%rdx, -56(%rbp)
	movl	43(%rdx), %ecx
	leaq	63(%rdx), %rax
	movq	0(%r13), %rbx
	testl	%ecx, %ecx
	js	.L1778
.L1768:
	subl	%eax, %ebx
	movq	-56(%rbp), %rax
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdx
	leaq	-48(%rbp), %rcx
	movq	%rax, -48(%rbp)
	movq	(%r12), %rax
	movq	24(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L1769
	leaq	-40(%rbp), %r8
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r12, %rdi
	call	*16(%rax)
.L1770:
	movq	-48(%rbp), %rdx
	cmpq	-56(%rbp), %rdx
	je	.L1766
	movq	%rdx, -56(%rbp)
	leaq	63(%rdx), %rax
	movl	43(%rdx), %edx
	testl	%edx, %edx
	js	.L1779
.L1774:
	addq	%rax, %rbx
	movq	%rbx, 0(%r13)
.L1766:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1780
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1778:
	.cfi_restore_state
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1779:
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L1774
	.p2align 4,,10
	.p2align 3
.L1769:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r12, %rdi
	call	*%r8
	jmp	.L1770
.L1780:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23410:
	.size	_ZNK2v88internal10EntryFrame7IterateEPNS0_11RootVisitorE, .-_ZNK2v88internal10EntryFrame7IterateEPNS0_11RootVisitorE
	.section	.text._ZNK2v88internal13StandardFrame8positionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13StandardFrame8positionEv
	.type	_ZNK2v88internal13StandardFrame8positionEv, @function
_ZNK2v88internal13StandardFrame8positionEv:
.LFB23236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	41144(%rdx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	8(%rax), %rdx
	movq	40(%rbx), %rax
	movq	%rdx, -40(%rbp)
	movq	(%rax), %rsi
	leaq	53(%rdx), %rax
	movq	-1(%rdx), %rcx
	cmpw	$69, 11(%rcx)
	je	.L1788
.L1785:
	subl	%eax, %esi
	leaq	-40(%rbp), %rdi
	call	_ZN2v88internal12AbstractCode14SourcePositionEi@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1789
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1788:
	.cfi_restore_state
	movq	%rdx, -32(%rbp)
	leaq	63(%rdx), %rax
	movl	43(%rdx), %edx
	testl	%edx, %edx
	jns	.L1785
	leaq	-32(%rbp), %rdi
	movq	%rsi, -56(%rbp)
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1785
.L1789:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23236:
	.size	_ZNK2v88internal13StandardFrame8positionEv, .-_ZNK2v88internal13StandardFrame8positionEv
	.section	.text._ZN2v88internal9StubFrame29LookupExceptionHandlerInTableEPi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal9StubFrame29LookupExceptionHandlerInTableEPi
	.type	_ZN2v88internal9StubFrame29LookupExceptionHandlerInTableEPi, @function
_ZN2v88internal9StubFrame29LookupExceptionHandlerInTableEPi:
.LFB23247:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	leaq	-64(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	41144(%rdx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal12HandlerTableC1ENS0_4CodeE@PLT
	movq	40(%rbx), %rax
	movq	(%rax), %rbx
	movq	-72(%rbp), %rax
	movl	43(%rax), %edx
	addq	$63, %rax
	testl	%edx, %edx
	js	.L1795
.L1792:
	shrl	$7, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	andl	$16777215, %edx
	subl	%eax, %esi
	movl	%edx, (%r12)
	call	_ZN2v88internal12HandlerTable12LookupReturnEi@PLT
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1796
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1795:
	.cfi_restore_state
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-72(%rbp), %rdx
	movl	43(%rdx), %edx
	jmp	.L1792
.L1796:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23247:
	.size	_ZN2v88internal9StubFrame29LookupExceptionHandlerInTableEPi, .-_ZN2v88internal9StubFrame29LookupExceptionHandlerInTableEPi
	.section	.text._ZNK2v88internal15JavaScriptFrame7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame7IterateEPNS0_11RootVisitorE
	.type	_ZNK2v88internal15JavaScriptFrame7IterateEPNS0_11RootVisitorE, @function
_ZNK2v88internal15JavaScriptFrame7IterateEPNS0_11RootVisitorE:
.LFB23412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	24(%rdi), %rcx
	movq	32(%rdi), %r8
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	$6, %esi
	call	*16(%rax)
	movq	16(%rbx), %rdx
	movq	40(%rbx), %rax
	movq	41144(%rdx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	40(%rbx), %r13
	movq	8(%rax), %rdx
	movq	%rdx, -56(%rbp)
	movl	43(%rdx), %ecx
	leaq	63(%rdx), %rax
	movq	0(%r13), %rbx
	testl	%ecx, %ecx
	js	.L1809
.L1799:
	subl	%eax, %ebx
	movq	-56(%rbp), %rax
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdx
	leaq	-48(%rbp), %rcx
	movq	%rax, -48(%rbp)
	movq	(%r12), %rax
	movq	24(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L1800
	leaq	-40(%rbp), %r8
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r12, %rdi
	call	*16(%rax)
.L1801:
	movq	-48(%rbp), %rdx
	cmpq	-56(%rbp), %rdx
	je	.L1797
	movq	%rdx, -56(%rbp)
	leaq	63(%rdx), %rax
	movl	43(%rdx), %edx
	testl	%edx, %edx
	js	.L1810
.L1805:
	addq	%rax, %rbx
	movq	%rbx, 0(%r13)
.L1797:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1811
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1809:
	.cfi_restore_state
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1810:
	leaq	-56(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L1805
	.p2align 4,,10
	.p2align 3
.L1800:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r12, %rdi
	call	*%r8
	jmp	.L1801
.L1811:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23412:
	.size	_ZNK2v88internal15JavaScriptFrame7IterateEPNS0_11RootVisitorE, .-_ZNK2v88internal15JavaScriptFrame7IterateEPNS0_11RootVisitorE
	.section	.text._ZNK2v88internal13InternalFrame7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13InternalFrame7IterateEPNS0_11RootVisitorE
	.type	_ZNK2v88internal13InternalFrame7IterateEPNS0_11RootVisitorE, @function
_ZNK2v88internal13InternalFrame7IterateEPNS0_11RootVisitorE:
.LFB23413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	41144(%rdx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	40(%r12), %r15
	movq	8(%rax), %r14
	movq	%r14, -72(%rbp)
	movl	43(%r14), %ecx
	leaq	63(%r14), %rax
	movq	(%r15), %rbx
	testl	%ecx, %ecx
	js	.L1829
.L1814:
	subl	%eax, %ebx
	movq	-72(%rbp), %rax
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rdx
	leaq	-64(%rbp), %rcx
	movq	%rax, -64(%rbp)
	movq	0(%r13), %rax
	movq	24(%rax), %r8
	cmpq	%rdx, %r8
	jne	.L1815
	leaq	-56(%rbp), %r8
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r13, %rdi
	call	*16(%rax)
.L1816:
	movq	-64(%rbp), %rdx
	cmpq	-72(%rbp), %rdx
	je	.L1818
	movq	%rdx, -72(%rbp)
	leaq	63(%rdx), %rax
	movl	43(%rdx), %edx
	testl	%edx, %edx
	js	.L1830
.L1820:
	addq	%rax, %rbx
	movq	%rbx, (%r15)
.L1818:
	movl	43(%r14), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$11, %eax
	jbe	.L1831
.L1821:
	movq	0(%r13), %rax
	movq	24(%r12), %rcx
	xorl	%edx, %edx
	movl	$6, %esi
	movq	32(%r12), %r8
	movq	%r13, %rdi
	call	*16(%rax)
.L1812:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1832
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1831:
	.cfi_restore_state
	movl	$2336, %edx
	btq	%rax, %rdx
	jc	.L1812
	jmp	.L1821
	.p2align 4,,10
	.p2align 3
.L1829:
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L1814
	.p2align 4,,10
	.p2align 3
.L1830:
	leaq	-72(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L1815:
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r13, %rdi
	call	*%r8
	jmp	.L1816
.L1832:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23413:
	.size	_ZNK2v88internal13InternalFrame7IterateEPNS0_11RootVisitorE, .-_ZNK2v88internal13InternalFrame7IterateEPNS0_11RootVisitorE
	.section	.text._ZNK2v88internal13StandardFrame20IterateCompiledFrameEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal13StandardFrame20IterateCompiledFrameEPNS0_11RootVisitorE
	.type	_ZNK2v88internal13StandardFrame20IterateCompiledFrameEPNS0_11RootVisitorE, @function
_ZNK2v88internal13StandardFrame20IterateCompiledFrameEPNS0_11RootVisitorE:
.LFB23243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	(%rax), %r13
	movq	16(%rdi), %rax
	movq	45752(%rax), %rdi
	movq	%r13, %rsi
	addq	$280, %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm@PLT
	testq	%rax, %rax
	je	.L1834
	movq	80(%rax), %rdx
	movq	(%rax), %rsi
	leaq	-112(%rbp), %r14
	xorl	%r8d, %r8d
	movl	72(%rax), %ecx
	movq	%r14, %rdi
	movq	%rax, %rbx
	call	_ZN2v88internal14SafepointTableC1Emmjb@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	leaq	-144(%rbp), %rdi
	call	_ZNK2v88internal14SafepointTable9FindEntryEm@PLT
	movl	76(%rbx), %eax
	cmpl	$1, 60(%rbx)
	movq	$0, -176(%rbp)
	movq	-136(%rbp), %r11
	movl	72(%rbx), %r14d
	seta	%dl
	movl	%eax, -164(%rbp)
.L1835:
	movq	32(%r12), %rdi
	leal	0(,%r14,8), %eax
	movq	-8(%rdi), %rcx
	leaq	-8(%rdi), %rsi
	movq	%rdi, -160(%rbp)
	movq	%rsi, -152(%rbp)
	testb	$1, %cl
	jne	.L1838
	sarq	%rcx
	cmpl	$24, %ecx
	ja	.L1839
	leaq	.L1841(%rip), %rsi
	movl	%ecx, %ecx
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	notrack jmp	*%rcx
	.section	.rodata._ZNK2v88internal13StandardFrame20IterateCompiledFrameEPNS0_11RootVisitorE,"a",@progbits
	.align 4
	.align 4
.L1841:
	.long	.L1840-.L1841
	.long	.L1853-.L1841
	.long	.L1853-.L1841
	.long	.L1853-.L1841
	.long	.L1840-.L1841
	.long	.L1839-.L1841
	.long	.L1839-.L1841
	.long	.L1853-.L1841
	.long	.L1839-.L1841
	.long	.L1853-.L1841
	.long	.L1839-.L1841
	.long	.L1839-.L1841
	.long	.L1840-.L1841
	.long	.L1853-.L1841
	.long	.L1853-.L1841
	.long	.L1853-.L1841
	.long	.L1853-.L1841
	.long	.L1853-.L1841
	.long	.L1853-.L1841
	.long	.L1853-.L1841
	.long	.L1840-.L1841
	.long	.L1853-.L1841
	.long	.L1840-.L1841
	.long	.L1840-.L1841
	.long	.L1840-.L1841
	.section	.text._ZNK2v88internal13StandardFrame20IterateCompiledFrameEPNS0_11RootVisitorE
	.p2align 4,,10
	.p2align 3
.L1838:
	movq	%rdi, %rsi
	subq	$16, %rsi
	movq	%rsi, -152(%rbp)
	movl	$16, %esi
.L1842:
	movq	-152(%rbp), %r13
	subl	$16, %eax
	movq	24(%r12), %rcx
	subl	%esi, %eax
	subq	%rax, %r13
	testb	%dl, %dl
	jne	.L1867
.L1843:
	xorl	%ebx, %ebx
	testl	%r14d, %r14d
	je	.L1849
	movq	%r12, -184(%rbp)
	movq	%r15, %r12
	movq	%r11, %r15
	.p2align 4,,10
	.p2align 3
.L1844:
	movl	%ebx, %eax
	shrl	$3, %eax
	movzbl	(%r15,%rax), %edx
	movl	%ebx, %eax
	andl	$7, %eax
	btl	%eax, %edx
	jnc	.L1847
	movslq	%ebx, %rax
	leaq	_ZN2v88internal11RootVisitor16VisitRootPointerENS0_4RootEPKcNS0_14FullObjectSlotE(%rip), %rsi
	leaq	0(%r13,%rax,8), %rcx
	movq	(%r12), %rax
	movq	24(%rax), %r8
	cmpq	%rsi, %r8
	jne	.L1848
	leaq	8(%rcx), %r8
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r12, %rdi
	call	*16(%rax)
.L1847:
	addl	$1, %ebx
	cmpl	%ebx, %r14d
	jne	.L1844
	movq	%r12, %r15
	movq	-184(%rbp), %r12
.L1849:
	movl	-164(%rbp), %eax
	testl	%eax, %eax
	jne	.L1845
	cmpq	$0, -176(%rbp)
	jne	.L1868
.L1850:
	movq	(%r15), %rax
	xorl	%edx, %edx
	movq	-160(%rbp), %r8
	movq	%r15, %rdi
	movq	-152(%rbp), %rcx
	movl	$6, %esi
	call	*16(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1869
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1848:
	.cfi_restore_state
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r12, %rdi
	call	*%r8
	jmp	.L1847
	.p2align 4,,10
	.p2align 3
.L1845:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*56(%rax)
	movslq	-164(%rbp), %rdx
	movl	$6, %esi
	movq	%r15, %rdi
	movq	%rax, %rcx
	movq	(%r15), %rax
	leaq	(%rcx,%rdx,8), %r8
	xorl	%edx, %edx
	call	*16(%rax)
	cmpq	$0, -176(%rbp)
	je	.L1850
.L1868:
	movq	56(%r12), %rdx
	movq	40(%r12), %rsi
	movq	%r15, %rdi
	movq	-176(%rbp), %rcx
	call	_ZN2v88internal10StackFrame9IteratePcEPNS0_11RootVisitorEPmS4_NS0_4CodeE
	jmp	.L1850
	.p2align 4,,10
	.p2align 3
.L1867:
	movq	(%r15), %rax
	movq	%r11, -184(%rbp)
	movq	%r13, %r8
	xorl	%edx, %edx
	movl	$6, %esi
	movq	%r15, %rdi
	call	*16(%rax)
	movq	-184(%rbp), %r11
	jmp	.L1843
	.p2align 4,,10
	.p2align 3
.L1853:
	movl	$8, %esi
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1839:
	movq	-160(%rbp), %rsi
	subq	$16, %rsi
	movq	%rsi, -152(%rbp)
	movl	$16, %esi
	jmp	.L1842
	.p2align 4,,10
	.p2align 3
.L1834:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	41144(%rax), %rdi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	24(%rax), %r11
	testq	%r11, %r11
	je	.L1870
.L1836:
	movq	8(%rax), %rax
	movl	$1, %edx
	movl	43(%rax), %ecx
	movq	%rax, -176(%rbp)
	movl	%ecx, %r10d
	shrl	%ecx
	shrl	$7, %r10d
	andl	$31, %ecx
	andl	$16777215, %r10d
	movl	%r10d, %r14d
	cmpl	$11, %ecx
	ja	.L1837
	movl	$2336, %edx
	shrq	%cl, %rdx
	notq	%rdx
	andl	$1, %edx
.L1837:
	movl	$0, -164(%rbp)
	jmp	.L1835
.L1870:
	leaq	8(%rax), %rsi
	movq	%r13, %rdx
	leaq	-112(%rbp), %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88internal4Code17GetSafepointEntryEm@PLT
	movq	-152(%rbp), %rax
	movdqa	-112(%rbp), %xmm0
	movups	%xmm0, 16(%rax)
	movl	-96(%rbp), %edx
	movq	24(%rax), %r11
	movl	%edx, 32(%rax)
	jmp	.L1836
.L1840:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1869:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23243:
	.size	_ZNK2v88internal13StandardFrame20IterateCompiledFrameEPNS0_11RootVisitorE, .-_ZNK2v88internal13StandardFrame20IterateCompiledFrameEPNS0_11RootVisitorE
	.section	.text._ZNK2v88internal9StubFrame7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal9StubFrame7IterateEPNS0_11RootVisitorE
	.type	_ZNK2v88internal9StubFrame7IterateEPNS0_11RootVisitorE, @function
_ZNK2v88internal9StubFrame7IterateEPNS0_11RootVisitorE:
.LFB23244:
	.cfi_startproc
	endbr64
	jmp	_ZNK2v88internal13StandardFrame20IterateCompiledFrameEPNS0_11RootVisitorE
	.cfi_endproc
.LFE23244:
	.size	_ZNK2v88internal9StubFrame7IterateEPNS0_11RootVisitorE, .-_ZNK2v88internal9StubFrame7IterateEPNS0_11RootVisitorE
	.section	.text._ZNK2v88internal14OptimizedFrame7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14OptimizedFrame7IterateEPNS0_11RootVisitorE
	.type	_ZNK2v88internal14OptimizedFrame7IterateEPNS0_11RootVisitorE, @function
_ZNK2v88internal14OptimizedFrame7IterateEPNS0_11RootVisitorE:
.LFB23248:
	.cfi_startproc
	endbr64
	jmp	_ZNK2v88internal13StandardFrame20IterateCompiledFrameEPNS0_11RootVisitorE
	.cfi_endproc
.LFE23248:
	.size	_ZNK2v88internal14OptimizedFrame7IterateEPNS0_11RootVisitorE, .-_ZNK2v88internal14OptimizedFrame7IterateEPNS0_11RootVisitorE
	.section	.text._ZNK2v88internal17WasmCompiledFrame7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17WasmCompiledFrame7IterateEPNS0_11RootVisitorE
	.type	_ZNK2v88internal17WasmCompiledFrame7IterateEPNS0_11RootVisitorE, @function
_ZNK2v88internal17WasmCompiledFrame7IterateEPNS0_11RootVisitorE:
.LFB23371:
	.cfi_startproc
	endbr64
	jmp	_ZNK2v88internal13StandardFrame20IterateCompiledFrameEPNS0_11RootVisitorE
	.cfi_endproc
.LFE23371:
	.size	_ZNK2v88internal17WasmCompiledFrame7IterateEPNS0_11RootVisitorE, .-_ZNK2v88internal17WasmCompiledFrame7IterateEPNS0_11RootVisitorE
	.section	.text._ZNK2v88internal25WasmInterpreterEntryFrame7IterateEPNS0_11RootVisitorE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25WasmInterpreterEntryFrame7IterateEPNS0_11RootVisitorE
	.type	_ZNK2v88internal25WasmInterpreterEntryFrame7IterateEPNS0_11RootVisitorE, @function
_ZNK2v88internal25WasmInterpreterEntryFrame7IterateEPNS0_11RootVisitorE:
.LFB23382:
	.cfi_startproc
	endbr64
	jmp	_ZNK2v88internal13StandardFrame20IterateCompiledFrameEPNS0_11RootVisitorE
	.cfi_endproc
.LFE23382:
	.size	_ZNK2v88internal25WasmInterpreterEntryFrame7IterateEPNS0_11RootVisitorE, .-_ZNK2v88internal25WasmInterpreterEntryFrame7IterateEPNS0_11RootVisitorE
	.section	.text._ZN2v88internal10StackFrame11ComputeTypeEPKNS0_22StackFrameIteratorBaseEPNS1_5StateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal10StackFrame11ComputeTypeEPKNS0_22StackFrameIteratorBaseEPNS1_5StateE
	.type	_ZN2v88internal10StackFrame11ComputeTypeEPKNS0_22StackFrameIteratorBaseEPNS1_5StateE, @function
_ZN2v88internal10StackFrame11ComputeTypeEPKNS0_22StackFrameIteratorBaseEPNS1_5StateE:
.LFB23203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	cmpb	$0, 1432(%rdi)
	movq	-8(%rax), %r12
	jne	.L1876
	testb	$1, %r12b
	je	.L1877
	movl	$22, %r12d
	testb	$1, -16(%rax)
	je	.L1875
	movq	16(%rsi), %rax
	leaq	8(%rsi), %rdx
	movq	(%rdi), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal12_GLOBAL__N_120IsInterpreterFramePcEPNS0_7IsolateEmPNS0_10StackFrame5StateE.isra.0
	cmpb	$1, %al
	sbbl	%r12d, %r12d
	andl	$-8, %r12d
	addl	$12, %r12d
.L1875:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1906
	addq	$80, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1883:
	.cfi_restore_state
	testb	$1, %r12b
	jne	.L1891
.L1888:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1877:
	sarq	%r12
	cmpl	$21, %r12d
	ja	.L1904
	leaq	.L1895(%rip), %rcx
	movl	%r12d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10StackFrame11ComputeTypeEPKNS0_22StackFrameIteratorBaseEPNS1_5StateE,"a",@progbits
	.align 4
	.align 4
.L1895:
	.long	.L1904-.L1895
	.long	.L1875-.L1895
	.long	.L1875-.L1895
	.long	.L1875-.L1895
	.long	.L1904-.L1895
	.long	.L1875-.L1895
	.long	.L1875-.L1895
	.long	.L1904-.L1895
	.long	.L1904-.L1895
	.long	.L1904-.L1895
	.long	.L1875-.L1895
	.long	.L1875-.L1895
	.long	.L1904-.L1895
	.long	.L1875-.L1895
	.long	.L1875-.L1895
	.long	.L1875-.L1895
	.long	.L1875-.L1895
	.long	.L1875-.L1895
	.long	.L1875-.L1895
	.long	.L1875-.L1895
	.long	.L1904-.L1895
	.long	.L1875-.L1895
	.section	.text._ZN2v88internal10StackFrame11ComputeTypeEPKNS0_22StackFrameIteratorBaseEPNS1_5StateE
	.p2align 4,,10
	.p2align 3
.L1876:
	movq	16(%rsi), %rax
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
	movq	(%rax), %r13
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	45752(%rax), %rdi
	addq	$280, %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm@PLT
	testq	%rax, %rax
	je	.L1879
	cmpl	$4, 60(%rax)
	ja	.L1880
	movl	60(%rax), %eax
	leaq	.L1882(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10StackFrame11ComputeTypeEPKNS0_22StackFrameIteratorBaseEPNS1_5StateE
	.align 4
	.align 4
.L1882:
	.long	.L1886-.L1882
	.long	.L1885-.L1882
	.long	.L1899-.L1882
	.long	.L1883-.L1882
	.long	.L1881-.L1882
	.section	.text._ZN2v88internal10StackFrame11ComputeTypeEPKNS0_22StackFrameIteratorBaseEPNS1_5StateE
	.p2align 4,,10
	.p2align 3
.L1899:
	movl	$6, %r12d
	.p2align 4,,10
	.p2align 3
.L1884:
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1885:
	movl	$10, %r12d
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1886:
	movl	$5, %r12d
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1881:
	movl	$8, %r12d
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1904:
	movl	$22, %r12d
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1879:
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	41144(%rax), %rdi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1900
	movl	43(%rdx), %esi
	movl	%esi, %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$11, %eax
	ja	.L1888
	leaq	.L1890(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal10StackFrame11ComputeTypeEPKNS0_22StackFrameIteratorBaseEPNS1_5StateE
	.align 4
	.align 4
.L1890:
	.long	.L1901-.L1890
	.long	.L1888-.L1890
	.long	.L1888-.L1890
	.long	.L1893-.L1890
	.long	.L1888-.L1890
	.long	.L1880-.L1890
	.long	.L1880-.L1890
	.long	.L1880-.L1890
	.long	.L1892-.L1890
	.long	.L1891-.L1890
	.long	.L1880-.L1890
	.long	.L1889-.L1890
	.section	.text._ZN2v88internal10StackFrame11ComputeTypeEPKNS0_22StackFrameIteratorBaseEPNS1_5StateE
	.p2align 4,,10
	.p2align 3
.L1900:
	movl	$22, %r12d
	jmp	.L1884
	.p2align 4,,10
	.p2align 3
.L1891:
	movl	$13, %r12d
	jmp	.L1884
.L1880:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1889:
	movl	$9, %r12d
	jmp	.L1884
.L1893:
	testb	$1, %r12b
	je	.L1888
	movl	59(%rdx), %eax
	leal	-64(%rax), %edx
	cmpl	$1, %edx
	jbe	.L1902
	cmpl	$57, %eax
	jne	.L1907
.L1902:
	movl	$12, %r12d
	jmp	.L1884
.L1901:
	movl	$4, %r12d
	jmp	.L1884
.L1892:
	movl	$7, %r12d
	jmp	.L1884
.L1907:
	andl	$64, %esi
	cmpl	$1, %esi
	sbbl	%r12d, %r12d
	andl	$16, %r12d
	addl	$4, %r12d
	jmp	.L1884
.L1906:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23203:
	.size	_ZN2v88internal10StackFrame11ComputeTypeEPKNS0_22StackFrameIteratorBaseEPNS1_5StateE, .-_ZN2v88internal10StackFrame11ComputeTypeEPKNS0_22StackFrameIteratorBaseEPNS1_5StateE
	.section	.text.unlikely._ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm,"ax",@progbits
	.align 2
.LCOLDB76:
	.section	.text._ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm,"ax",@progbits
.LHOTB76:
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm
	.type	_ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm, @function
_ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm:
.LFB23191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%r8, %xmm2
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$96, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal10EntryFrameE(%rip), %rax
	movq	%rsi, (%rdi)
	movq	%rax, 8(%rdi)
	leaq	16+_ZTVN2v88internal19ConstructEntryFrameE(%rip), %rax
	movq	%rax, 72(%rdi)
	leaq	16+_ZTVN2v88internal9ExitFrameE(%rip), %rax
	movq	%rax, 136(%rdi)
	leaq	16+_ZTVN2v88internal14OptimizedFrameE(%rip), %rax
	movq	%rdi, 16(%rdi)
	movq	%rsi, 24(%rdi)
	movq	$0, 64(%rdi)
	movq	%rdi, 80(%rdi)
	movq	%rsi, 88(%rdi)
	movq	$0, 128(%rdi)
	movq	%rdi, 144(%rdi)
	movq	%rsi, 152(%rdi)
	movq	$0, 192(%rdi)
	movq	%rdi, 208(%rdi)
	movq	%rsi, 216(%rdi)
	movq	$0, 256(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 96(%rdi)
	movups	%xmm0, 112(%rdi)
	movups	%xmm0, 160(%rdi)
	movups	%xmm0, 176(%rdi)
	movups	%xmm0, 224(%rdi)
	movups	%xmm0, 240(%rdi)
	movq	%rax, 200(%rdi)
	leaq	16+_ZTVN2v88internal17WasmCompiledFrameE(%rip), %rax
	movq	%rax, 264(%rdi)
	leaq	16+_ZTVN2v88internal13WasmToJsFrameE(%rip), %rax
	movq	%rax, 328(%rdi)
	leaq	16+_ZTVN2v88internal13JsToWasmFrameE(%rip), %rax
	movq	%rax, 392(%rdi)
	leaq	16+_ZTVN2v88internal25WasmInterpreterEntryFrameE(%rip), %rax
	movq	%rax, 456(%rdi)
	leaq	16+_ZTVN2v88internal15CWasmEntryFrameE(%rip), %rax
	movq	%rax, 520(%rdi)
	leaq	16+_ZTVN2v88internal13WasmExitFrameE(%rip), %rax
	movq	%rdi, 272(%rdi)
	movq	%rsi, 280(%rdi)
	movq	$0, 320(%rdi)
	movq	%rdi, 336(%rdi)
	movq	%rsi, 344(%rdi)
	movq	$0, 384(%rdi)
	movq	%rdi, 400(%rdi)
	movq	%rsi, 408(%rdi)
	movq	$0, 448(%rdi)
	movq	%rdi, 464(%rdi)
	movq	%rsi, 472(%rdi)
	movq	$0, 512(%rdi)
	movq	%rdi, 528(%rdi)
	movq	%rsi, 536(%rdi)
	movq	$0, 576(%rdi)
	movq	%rdi, 592(%rdi)
	movq	%rsi, 600(%rdi)
	movups	%xmm0, 288(%rdi)
	movups	%xmm0, 304(%rdi)
	movups	%xmm0, 352(%rdi)
	movups	%xmm0, 368(%rdi)
	movups	%xmm0, 416(%rdi)
	movups	%xmm0, 432(%rdi)
	movups	%xmm0, 480(%rdi)
	movups	%xmm0, 496(%rdi)
	movups	%xmm0, 544(%rdi)
	movups	%xmm0, 560(%rdi)
	movups	%xmm0, 608(%rdi)
	movq	%rax, 584(%rdi)
	leaq	16+_ZTVN2v88internal20WasmCompileLazyFrameE(%rip), %rax
	movq	%rax, 648(%rdi)
	leaq	16+_ZTVN2v88internal16InterpretedFrameE(%rip), %rax
	movq	%rax, 712(%rdi)
	leaq	16+_ZTVN2v88internal9StubFrameE(%rip), %rax
	movq	%rax, 776(%rdi)
	leaq	16+_ZTVN2v88internal24BuiltinContinuationFrameE(%rip), %rax
	movq	%rax, 840(%rdi)
	leaq	16+_ZTVN2v88internal34JavaScriptBuiltinContinuationFrameE(%rip), %rax
	movq	$0, 640(%rdi)
	movq	%rdi, 656(%rdi)
	movq	%rsi, 664(%rdi)
	movq	$0, 704(%rdi)
	movq	%rdi, 720(%rdi)
	movq	%rsi, 728(%rdi)
	movq	$0, 768(%rdi)
	movq	%rdi, 784(%rdi)
	movq	%rsi, 792(%rdi)
	movq	$0, 832(%rdi)
	movq	%rdi, 848(%rdi)
	movq	%rsi, 856(%rdi)
	movq	$0, 896(%rdi)
	movq	%rdi, 912(%rdi)
	movq	%rsi, 920(%rdi)
	movq	$0, 960(%rdi)
	movups	%xmm0, 624(%rdi)
	movups	%xmm0, 672(%rdi)
	movups	%xmm0, 688(%rdi)
	movups	%xmm0, 736(%rdi)
	movups	%xmm0, 752(%rdi)
	movups	%xmm0, 800(%rdi)
	movups	%xmm0, 816(%rdi)
	movups	%xmm0, 864(%rdi)
	movups	%xmm0, 880(%rdi)
	movups	%xmm0, 928(%rdi)
	movups	%xmm0, 944(%rdi)
	movq	%rax, 904(%rdi)
	leaq	16+_ZTVN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameE(%rip), %rax
	movq	%rax, 968(%rdi)
	leaq	16+_ZTVN2v88internal13InternalFrameE(%rip), %rax
	movq	%rax, 1032(%rdi)
	leaq	16+_ZTVN2v88internal14ConstructFrameE(%rip), %rax
	movq	%rax, 1096(%rdi)
	leaq	16+_ZTVN2v88internal21ArgumentsAdaptorFrameE(%rip), %rax
	movq	%rax, 1160(%rdi)
	leaq	16+_ZTVN2v88internal12BuiltinFrameE(%rip), %rax
	movq	%rax, 1224(%rdi)
	leaq	16+_ZTVN2v88internal16BuiltinExitFrameE(%rip), %rax
	movq	%rdi, 976(%rdi)
	movq	%rsi, 984(%rdi)
	movq	$0, 1024(%rdi)
	movq	%rdi, 1040(%rdi)
	movq	%rsi, 1048(%rdi)
	movq	$0, 1088(%rdi)
	movq	%rdi, 1104(%rdi)
	movq	%rsi, 1112(%rdi)
	movq	$0, 1152(%rdi)
	movq	%rdi, 1168(%rdi)
	movq	%rsi, 1176(%rdi)
	movq	$0, 1216(%rdi)
	movq	%rdi, 1232(%rdi)
	movq	%rsi, 1240(%rdi)
	movq	$0, 1280(%rdi)
	movq	%rdi, 1296(%rdi)
	movq	%rsi, 1304(%rdi)
	movups	%xmm0, 992(%rdi)
	movups	%xmm0, 1008(%rdi)
	movups	%xmm0, 1056(%rdi)
	movups	%xmm0, 1072(%rdi)
	movups	%xmm0, 1120(%rdi)
	movups	%xmm0, 1136(%rdi)
	movups	%xmm0, 1184(%rdi)
	movups	%xmm0, 1200(%rdi)
	movups	%xmm0, 1248(%rdi)
	movups	%xmm0, 1264(%rdi)
	movups	%xmm0, 1312(%rdi)
	movhps	16(%rbp), %xmm2
	movq	%rax, 1288(%rdi)
	leaq	16+_ZTVN2v88internal11NativeFrameE(%rip), %rax
	movq	$0, 1344(%rdi)
	movq	%rdi, 1360(%rdi)
	movq	%rsi, 1368(%rdi)
	movq	%rax, 1352(%rdi)
	movq	$0, 1424(%rdi)
	movb	$0, 1432(%rdi)
	movups	%xmm0, 1328(%rdi)
	movups	%xmm0, 1376(%rdi)
	movups	%xmm0, 1392(%rdi)
	movups	%xmm0, 1408(%rdi)
	movq	12608(%rsi), %rax
	cmpb	$0, 37568(%rsi)
	movl	$0, 1456(%rdi)
	movq	$0, 1464(%rdi)
	movq	%rax, 1472(%rdi)
	movq	%r9, 1480(%rdi)
	movq	$0, -48(%rbp)
	movups	%xmm2, 1440(%rdi)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	je	.L1908
	movq	37496(%rsi), %rax
	movq	%rsi, %r13
	movq	%r8, %xmm1
	testq	%rax, %rax
	je	.L1910
	movl	$4, 1456(%rdi)
	movq	%rax, %xmm3
	movl	$4, %esi
	addq	$37504, %r13
	punpcklqdq	%xmm3, %xmm1
	movq	%r13, -64(%rbp)
	movaps	%xmm1, -80(%rbp)
	call	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE
	testq	%rax, %rax
	je	.L1912
	movdqa	-80(%rbp), %xmm4
	movdqa	-64(%rbp), %xmm5
	movq	$0, 56(%rax)
	movups	%xmm4, 24(%rax)
	movups	%xmm5, 40(%rax)
	movq	%rax, 1416(%rdi)
.L1908:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1986
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1910:
	.cfi_restore_state
	movq	12560(%rsi), %rbx
	cmpq	%rbx, %r8
	ja	.L1947
	cmpq	%rbx, 16(%rbp)
	jb	.L1947
	movq	-16(%rbx), %rax
	cmpq	%rax, %r8
	ja	.L1947
	cmpq	%rax, 16(%rbp)
	jb	.L1947
	leaq	-8(%rax), %rdi
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	testq	%rax, %rax
	je	.L1987
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%r8, -120(%rbp)
	call	*%rax
	movq	-120(%rbp), %xmm1
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rdx
.L1915:
	cmpq	$0, (%rax)
	je	.L1917
	movq	12568(%r13), %rax
	cmpq	%rbx, %rax
	jbe	.L1917
	testq	%rax, %rax
	je	.L1917
	movq	12560(%r13), %rax
	xorl	%r13d, %r13d
	testq	%rax, %rax
	je	.L1918
	movq	-8(%rax), %rdx
	leaq	-16(%rax), %rdi
	testb	$1, %dl
	jne	.L1951
	sarq	%rdx
	movl	%edx, %r13d
	cmpl	$21, %edx
	ja	.L1951
	movl	$2098184, %ecx
	btq	%rdx, %rcx
	jnc	.L1951
	cmpl	$10, %edx
	je	.L1920
.L1919:
	movq	-16(%rax), %rdi
.L1920:
	movq	%rdi, %xmm0
	movq	%rax, %xmm7
	subq	$8, %rdi
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -80(%rbp)
	testq	%rax, %rax
	je	.L1988
	call	*%rax
.L1922:
	movq	%rax, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	$0, -48(%rbp)
.L1918:
	movl	%r13d, 1456(%r12)
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE
	testq	%rax, %rax
	je	.L1912
	movdqa	-80(%rbp), %xmm6
	movups	%xmm6, 24(%rax)
	movdqa	-64(%rbp), %xmm7
	movups	%xmm7, 40(%rax)
	movq	-48(%rbp), %rdx
	movq	%rdx, 56(%rax)
	movq	%rax, 1416(%r12)
	jmp	.L1944
	.p2align 4,,10
	.p2align 3
.L1947:
	movq	%xmm1, %rax
.L1913:
	cmpq	%rax, %rcx
	jb	.L1908
	cmpq	1448(%r12), %rcx
	ja	.L1908
	movdqa	%xmm1, %xmm0
	movq	%rcx, %xmm6
	movq	_ZN2v88internal10StackFrame33return_address_location_resolver_E(%rip), %rax
	leaq	8(%rcx), %rdi
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -80(%rbp)
	testq	%rax, %rax
	je	.L1989
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%xmm1, -120(%rbp)
	call	*%rax
	movq	-120(%rbp), %xmm1
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rdx
.L1925:
	movq	%rcx, -104(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rax, -64(%rbp)
	movq	%xmm1, -120(%rbp)
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movq	-104(%rbp), %rcx
	testq	%rax, %rax
	je	.L1985
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	leaq	-96(%rbp), %r14
	movl	%eax, %ebx
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movq	%r14, %rdi
	movl	%ebx, -88(%rbp)
	movq	%rax, -96(%rbp)
	call	_ZNK2v88internal12EmbeddedData34InstructionStartOfBytecodeHandlersEv@PLT
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rcx
	movq	-120(%rbp), %xmm1
	cmpq	%rax, %rdx
	jb	.L1985
	movq	%r14, %rdi
	movq	%rcx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%xmm1, -120(%rbp)
	call	_ZNK2v88internal12EmbeddedData32InstructionEndOfBytecodeHandlersEv@PLT
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rcx
	cmpq	%rax, %rdx
	jnb	.L1985
	movq	1440(%r12), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rax, %rdx
	jb	.L1984
	movq	1448(%r12), %rdi
	cmpq	%rdi, %rdx
	ja	.L1984
	movq	-8(%rcx), %rdx
	movq	-120(%rbp), %xmm1
	movq	%rdx, %rbx
	notq	%rbx
	andl	$1, %ebx
	je	.L1932
	sarq	%rdx
	cmpl	$13, %edx
	je	.L1928
.L1932:
	movq	1480(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1990
	leaq	1480(%r12), %rbx
.L1936:
	leaq	-72(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_120IsInterpreterFramePcEPNS0_7IsolateEmPNS0_10StackFrame5StateE.isra.0
	movq	-104(%rbp), %rcx
	testb	%al, %al
	je	.L1985
	movq	1440(%r12), %rax
	movq	%rbx, -64(%rbp)
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1928:
	leaq	-16(%rcx), %rdx
	cmpq	%rax, %rdx
	jb	.L1939
	cmpq	1448(%r12), %rdx
	ja	.L1939
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rcx, -104(%rbp)
	call	_ZN2v88internal10StackFrame11ComputeTypeEPKNS0_22StackFrameIteratorBaseEPNS1_5StateE
	movq	-104(%rbp), %rcx
	cmpl	$12, %eax
	movl	%eax, 1456(%r12)
	movl	%eax, %esi
	movl	$1, %eax
	cmovne	%eax, %ebx
	movq	-8(%rcx), %rax
	testb	$1, %al
	je	.L1941
	movq	%rax, 1464(%r12)
	.p2align 4,,10
	.p2align 3
.L1941:
	movq	%r12, %rdi
	call	_ZN2v88internal22StackFrameIteratorBase12SingletonForENS0_10StackFrame4TypeE
	testq	%rax, %rax
	je	.L1912
	movdqa	-80(%rbp), %xmm6
	movups	%xmm6, 24(%rax)
	movdqa	-64(%rbp), %xmm7
	movups	%xmm7, 40(%rax)
	movq	-48(%rbp), %rdx
	movq	%rdx, 56(%rax)
	movq	%rax, 1416(%r12)
	testb	%bl, %bl
	je	.L1908
.L1944:
	movq	%r12, %rdi
	call	_ZN2v88internal22SafeStackFrameIterator7AdvanceEv
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L1912:
	movq	$0, 1416(%r12)
	jmp	.L1908
	.p2align 4,,10
	.p2align 3
.L1917:
	movq	1440(%r12), %rax
	jmp	.L1913
	.p2align 4,,10
	.p2align 3
.L1985:
	movq	1440(%r12), %rax
.L1984:
	movl	$1, %ebx
	jmp	.L1928
	.p2align 4,,10
	.p2align 3
.L1939:
	movl	$0, 1456(%r12)
	movl	$4, %esi
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L1951:
	movl	$3, %r13d
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L1990:
	movq	%xmm1, %rbx
	cmpq	%rax, %rbx
	jb	.L1937
	cmpq	%rdi, %rbx
	ja	.L1937
	movq	(%rbx), %rsi
	jmp	.L1936
.L1989:
	movq	%rdi, %rax
	jmp	.L1925
.L1988:
	movq	%rdi, %rax
	jmp	.L1922
.L1987:
	movq	%rdi, %rax
	jmp	.L1915
.L1986:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely._ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm
	.cfi_startproc
	.type	_ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm.cold, @function
_ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm.cold:
.LFSB23191:
.L1937:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	0, %rax
	ud2
	.cfi_endproc
.LFE23191:
	.section	.text._ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm
	.size	_ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm, .-_ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm
	.section	.text.unlikely._ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm
	.size	_ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm.cold, .-_ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm.cold
.LCOLDE76:
	.section	.text._ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm
.LHOTE76:
	.globl	_ZN2v88internal22SafeStackFrameIteratorC1EPNS0_7IsolateEmmmmm
	.set	_ZN2v88internal22SafeStackFrameIteratorC1EPNS0_7IsolateEmmmmm,_ZN2v88internal22SafeStackFrameIteratorC2EPNS0_7IsolateEmmmmm
	.section	.text._ZNK2v88internal14OptimizedFrame22ComputeParametersCountEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14OptimizedFrame22ComputeParametersCountEv
	.type	_ZNK2v88internal14OptimizedFrame22ComputeParametersCountEv, @function
_ZNK2v88internal14OptimizedFrame22ComputeParametersCountEv:
.LFB23262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	40(%rdi), %rax
	movq	16(%rdi), %rdx
	movq	(%rax), %rsi
	movq	41144(%rdx), %rdi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	8(%rax), %rax
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$3, %eax
	jne	.L1992
	movq	32(%r12), %rax
	movl	-24(%rax), %eax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1992:
	.cfi_restore_state
	movq	(%r12), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %rdx
	movq	152(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1994
	movq	32(%r12), %rax
	movq	-16(%rax), %rax
.L1995:
	movq	23(%rax), %rax
	movzwl	41(%rax), %eax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1994:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1995
	.cfi_endproc
.LFE23262:
	.size	_ZNK2v88internal14OptimizedFrame22ComputeParametersCountEv, .-_ZNK2v88internal14OptimizedFrame22ComputeParametersCountEv
	.section	.text._ZN2v88internal14OptimizedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14OptimizedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE
	.type	_ZN2v88internal14OptimizedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE, @function
_ZN2v88internal14OptimizedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE:
.LFB23347:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	41144(%rdx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal12HandlerTableC1ENS0_4CodeE@PLT
	movq	-120(%rbp), %rcx
	movq	40(%rbx), %rax
	movl	43(%rcx), %edx
	movq	(%rax), %r13
	leaq	43(%rcx), %rdi
	leaq	63(%rcx), %rax
	testl	%edx, %edx
	js	.L2011
.L1999:
	subl	%eax, %r13d
	movl	%r13d, %esi
	testq	%r12, %r12
	je	.L2000
	shrl	$7, %edx
	andl	$16777215, %edx
	movl	%edx, (%r12)
	movl	(%rdi), %edx
.L2000:
	andl	$62, %edx
	je	.L2012
.L2001:
	movq	%r14, %rdi
	call	_ZN2v88internal12HandlerTable12LookupReturnEi@PLT
	movq	-40(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L2013
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2012:
	.cfi_restore_state
	movq	31(%rcx), %rax
	movl	15(%rax), %eax
	testb	$1, %al
	je	.L2001
	movq	-120(%rbp), %rsi
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v88internal14SafepointTableC1ENS0_4CodeE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14SafepointTable14find_return_pcEj@PLT
	movl	%eax, %esi
	jmp	.L2001
	.p2align 4,,10
	.p2align 3
.L2011:
	leaq	-120(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-120(%rbp), %rcx
	movl	43(%rcx), %edx
	leaq	43(%rcx), %rdi
	jmp	.L1999
.L2013:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23347:
	.size	_ZN2v88internal14OptimizedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE, .-_ZN2v88internal14OptimizedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE
	.section	.text._ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.type	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE, @function
_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE:
.LFB23204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	call	*64(%rax)
	movq	8(%r12), %r13
	movq	8(%rbx), %rax
	cmpb	$0, 1432(%r13)
	movq	-8(%rax), %r12
	jne	.L2015
	testb	$1, %r12b
	je	.L2016
	movl	$22, %r12d
	testb	$1, -16(%rax)
	je	.L2014
	movq	16(%rbx), %rax
	movq	0(%r13), %rdi
	leaq	8(%rbx), %rdx
	movq	(%rax), %rsi
	call	_ZN2v88internal12_GLOBAL__N_120IsInterpreterFramePcEPNS0_7IsolateEmPNS0_10StackFrame5StateE.isra.0
	cmpb	$1, %al
	sbbl	%r12d, %r12d
	andl	$-8, %r12d
	addl	$12, %r12d
.L2014:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2045
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2022:
	.cfi_restore_state
	testb	$1, %r12b
	jne	.L2030
.L2027:
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L2016:
	sarq	%r12
	cmpl	$21, %r12d
	ja	.L2043
	leaq	.L2034(%rip), %rcx
	movl	%r12d, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE,"a",@progbits
	.align 4
	.align 4
.L2034:
	.long	.L2043-.L2034
	.long	.L2014-.L2034
	.long	.L2014-.L2034
	.long	.L2014-.L2034
	.long	.L2043-.L2034
	.long	.L2014-.L2034
	.long	.L2014-.L2034
	.long	.L2043-.L2034
	.long	.L2043-.L2034
	.long	.L2043-.L2034
	.long	.L2014-.L2034
	.long	.L2014-.L2034
	.long	.L2043-.L2034
	.long	.L2014-.L2034
	.long	.L2014-.L2034
	.long	.L2014-.L2034
	.long	.L2014-.L2034
	.long	.L2014-.L2034
	.long	.L2014-.L2034
	.long	.L2014-.L2034
	.long	.L2043-.L2034
	.long	.L2014-.L2034
	.section	.text._ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.p2align 4,,10
	.p2align 3
.L2015:
	movq	16(%rbx), %rax
	leaq	-128(%rbp), %r15
	movq	%r15, %rdi
	movq	(%rax), %r14
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	0(%r13), %rax
	movq	%r14, %rsi
	movq	45752(%rax), %rdi
	addq	$280, %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm@PLT
	testq	%rax, %rax
	je	.L2018
	cmpl	$4, 60(%rax)
	ja	.L2019
	movl	60(%rax), %eax
	leaq	.L2021(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.align 4
	.align 4
.L2021:
	.long	.L2025-.L2021
	.long	.L2024-.L2021
	.long	.L2038-.L2021
	.long	.L2022-.L2021
	.long	.L2020-.L2021
	.section	.text._ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.p2align 4,,10
	.p2align 3
.L2038:
	movl	$6, %r12d
	.p2align 4,,10
	.p2align 3
.L2023:
	movq	%r15, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	jmp	.L2014
	.p2align 4,,10
	.p2align 3
.L2024:
	movl	$10, %r12d
	jmp	.L2023
	.p2align 4,,10
	.p2align 3
.L2025:
	movl	$5, %r12d
	jmp	.L2023
	.p2align 4,,10
	.p2align 3
.L2020:
	movl	$8, %r12d
	jmp	.L2023
	.p2align 4,,10
	.p2align 3
.L2043:
	movl	$22, %r12d
	jmp	.L2014
	.p2align 4,,10
	.p2align 3
.L2018:
	movq	0(%r13), %rax
	movq	%r14, %rsi
	movq	41144(%rax), %rdi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L2039
	movl	43(%rdx), %esi
	movl	%esi, %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$11, %eax
	ja	.L2027
	leaq	.L2029(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.align 4
	.align 4
.L2029:
	.long	.L2040-.L2029
	.long	.L2027-.L2029
	.long	.L2027-.L2029
	.long	.L2032-.L2029
	.long	.L2027-.L2029
	.long	.L2019-.L2029
	.long	.L2019-.L2029
	.long	.L2019-.L2029
	.long	.L2031-.L2029
	.long	.L2030-.L2029
	.long	.L2019-.L2029
	.long	.L2028-.L2029
	.section	.text._ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.p2align 4,,10
	.p2align 3
.L2039:
	movl	$22, %r12d
	jmp	.L2023
	.p2align 4,,10
	.p2align 3
.L2030:
	movl	$13, %r12d
	jmp	.L2023
.L2019:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2028:
	movl	$9, %r12d
	jmp	.L2023
.L2032:
	testb	$1, %r12b
	je	.L2027
	movl	59(%rdx), %eax
	leal	-64(%rax), %edx
	cmpl	$1, %edx
	jbe	.L2041
	cmpl	$57, %eax
	jne	.L2046
.L2041:
	movl	$12, %r12d
	jmp	.L2023
.L2040:
	movl	$4, %r12d
	jmp	.L2023
.L2031:
	movl	$7, %r12d
	jmp	.L2023
.L2046:
	andl	$64, %esi
	cmpl	$1, %esi
	sbbl	%r12d, %r12d
	andl	$16, %r12d
	addl	$4, %r12d
	jmp	.L2023
.L2045:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23204:
	.size	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE, .-_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.section	.text._ZN2v88internal20InterpretedFrameInfoC2EiibNS0_13FrameInfoKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal20InterpretedFrameInfoC2EiibNS0_13FrameInfoKindE
	.type	_ZN2v88internal20InterpretedFrameInfoC2EiibNS0_13FrameInfoKindE, @function
_ZN2v88internal20InterpretedFrameInfoC2EiibNS0_13FrameInfoKindE:
.LFB23420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	%edx, %edi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	call	_ZN2v88internal25InterpreterFrameConstants22RegisterStackSlotCountEi@PLT
	cmpl	$1, %r14d
	movl	%ebx, %edi
	movl	%eax, %edx
	movl	%eax, (%r12)
	sete	%al
	orl	%r13d, %eax
	movzbl	%al, %eax
	addl	%edx, %eax
	sall	$3, %eax
	movl	%eax, 4(%r12)
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	movzbl	%al, %eax
	leal	6(%rbx,%rax), %edx
	movl	4(%r12), %eax
	popq	%rbx
	leal	(%rax,%rdx,8), %eax
	movl	%eax, 8(%r12)
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23420:
	.size	_ZN2v88internal20InterpretedFrameInfoC2EiibNS0_13FrameInfoKindE, .-_ZN2v88internal20InterpretedFrameInfoC2EiibNS0_13FrameInfoKindE
	.globl	_ZN2v88internal20InterpretedFrameInfoC1EiibNS0_13FrameInfoKindE
	.set	_ZN2v88internal20InterpretedFrameInfoC1EiibNS0_13FrameInfoKindE,_ZN2v88internal20InterpretedFrameInfoC2EiibNS0_13FrameInfoKindE
	.section	.text._ZN2v88internal25ArgumentsAdaptorFrameInfoC2Ei,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal25ArgumentsAdaptorFrameInfoC2Ei
	.type	_ZN2v88internal25ArgumentsAdaptorFrameInfoC2Ei, @function
_ZN2v88internal25ArgumentsAdaptorFrameInfoC2Ei:
.LFB23423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	%esi, %edi
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	movzbl	%al, %eax
	addl	%r12d, %eax
	sall	$3, %eax
	movl	%eax, (%rbx)
	addl	$48, %eax
	movl	%eax, 4(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE23423:
	.size	_ZN2v88internal25ArgumentsAdaptorFrameInfoC2Ei, .-_ZN2v88internal25ArgumentsAdaptorFrameInfoC2Ei
	.globl	_ZN2v88internal25ArgumentsAdaptorFrameInfoC1Ei
	.set	_ZN2v88internal25ArgumentsAdaptorFrameInfoC1Ei,_ZN2v88internal25ArgumentsAdaptorFrameInfoC2Ei
	.section	.text._ZN2v88internal22ConstructStubFrameInfoC2EibNS0_13FrameInfoKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22ConstructStubFrameInfoC2EibNS0_13FrameInfoKindE
	.type	_ZN2v88internal22ConstructStubFrameInfoC2EibNS0_13FrameInfoKindE, @function
_ZN2v88internal22ConstructStubFrameInfoC2EibNS0_13FrameInfoKindE:
.LFB23426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	%esi, %edi
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%ecx, %ebx
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	movzbl	%al, %eax
	addl	%r14d, %eax
	cmpl	$1, %ebx
	je	.L2054
	testb	%r12b, %r12b
	jne	.L2054
.L2052:
	sall	$3, %eax
	popq	%rbx
	popq	%r12
	movl	%eax, 0(%r13)
	addl	$64, %eax
	movl	%eax, 4(%r13)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2054:
	.cfi_restore_state
	addl	$1, %eax
	jmp	.L2052
	.cfi_endproc
.LFE23426:
	.size	_ZN2v88internal22ConstructStubFrameInfoC2EibNS0_13FrameInfoKindE, .-_ZN2v88internal22ConstructStubFrameInfoC2EibNS0_13FrameInfoKindE
	.globl	_ZN2v88internal22ConstructStubFrameInfoC1EibNS0_13FrameInfoKindE
	.set	_ZN2v88internal22ConstructStubFrameInfoC1EibNS0_13FrameInfoKindE,_ZN2v88internal22ConstructStubFrameInfoC2EibNS0_13FrameInfoKindE
	.section	.text._ZN2v88internal28BuiltinContinuationFrameInfoC2EiRKNS0_23CallInterfaceDescriptorEPKNS0_21RegisterConfigurationEbNS0_14DeoptimizeKindENS0_23BuiltinContinuationModeENS0_13FrameInfoKindE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal28BuiltinContinuationFrameInfoC2EiRKNS0_23CallInterfaceDescriptorEPKNS0_21RegisterConfigurationEbNS0_14DeoptimizeKindENS0_23BuiltinContinuationModeENS0_13FrameInfoKindE
	.type	_ZN2v88internal28BuiltinContinuationFrameInfoC2EiRKNS0_23CallInterfaceDescriptorEPKNS0_21RegisterConfigurationEbNS0_14DeoptimizeKindENS0_23BuiltinContinuationModeENS0_13FrameInfoKindE, @function
_ZN2v88internal28BuiltinContinuationFrameInfoC2EiRKNS0_23CallInterfaceDescriptorEPKNS0_21RegisterConfigurationEbNS0_14DeoptimizeKindENS0_23BuiltinContinuationModeENS0_13FrameInfoKindE:
.LFB23429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movl	%r8d, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	24(%rbp), %edi
	movl	16(%rbp), %eax
	cmpl	$1, %edi
	sete	%r12b
	xorl	$1, %esi
	cmpb	$2, %r9b
	sete	%r9b
	orl	%r9d, %esi
	movb	%sil, (%rbx)
	testb	%sil, %sil
	jne	.L2063
	cmpl	$1, %edi
	je	.L2058
	movl	$1, %esi
	xorl	%r9d, %r9d
	cmpl	$1, %eax
	jle	.L2067
.L2059:
	subl	$2, %eax
	cmpl	$1, %eax
	ja	.L2061
.L2062:
	movl	24(%rcx), %r15d
	movl	%r8d, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movl	%r15d, %edi
	movl	%esi, -52(%rbp)
	call	_ZN2v88internal33BuiltinContinuationFrameConstants16PaddingSlotCountEi@PLT
	movq	-64(%rbp), %rdx
	movl	-52(%rbp), %esi
	movl	%r13d, %edi
	movl	%eax, %r14d
	movq	8(%rdx), %rax
	subl	(%rax), %edi
	movl	%edi, 4(%rbx)
	addl	%esi, %edi
	movl	%edi, 8(%rbx)
	call	_ZN2v88internal18ShouldPadArgumentsEi@PLT
	movl	-56(%rbp), %r8d
	movl	8(%rbx), %edx
	movzbl	%al, %eax
	orl	%r8d, %r12d
	leal	7(%r15,%rdx), %edx
	movzbl	%r12b, %r12d
	addl	%r12d, %edx
	addl	%eax, %edx
	addl	%r14d, %edx
	addl	%r15d, %r14d
	leal	5(%r12,%r14), %eax
	sall	$3, %edx
	sall	$3, %eax
	movl	%edx, 12(%rbx)
	movl	%eax, 16(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2063:
	.cfi_restore_state
	movl	$2, %esi
	movl	$1, %r9d
	cmpl	$1, %eax
	jg	.L2059
.L2067:
	testl	%eax, %eax
	jns	.L2068
.L2061:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2068:
	movzbl	%r12b, %edi
	leal	(%rdi,%r9), %esi
	jmp	.L2062
	.p2align 4,,10
	.p2align 3
.L2058:
	cmpl	$3, %eax
	ja	.L2061
	movl	$2, %esi
	jmp	.L2062
	.cfi_endproc
.LFE23429:
	.size	_ZN2v88internal28BuiltinContinuationFrameInfoC2EiRKNS0_23CallInterfaceDescriptorEPKNS0_21RegisterConfigurationEbNS0_14DeoptimizeKindENS0_23BuiltinContinuationModeENS0_13FrameInfoKindE, .-_ZN2v88internal28BuiltinContinuationFrameInfoC2EiRKNS0_23CallInterfaceDescriptorEPKNS0_21RegisterConfigurationEbNS0_14DeoptimizeKindENS0_23BuiltinContinuationModeENS0_13FrameInfoKindE
	.globl	_ZN2v88internal28BuiltinContinuationFrameInfoC1EiRKNS0_23CallInterfaceDescriptorEPKNS0_21RegisterConfigurationEbNS0_14DeoptimizeKindENS0_23BuiltinContinuationModeENS0_13FrameInfoKindE
	.set	_ZN2v88internal28BuiltinContinuationFrameInfoC1EiRKNS0_23CallInterfaceDescriptorEPKNS0_21RegisterConfigurationEbNS0_14DeoptimizeKindENS0_23BuiltinContinuationModeENS0_13FrameInfoKindE,_ZN2v88internal28BuiltinContinuationFrameInfoC2EiRKNS0_23CallInterfaceDescriptorEPKNS0_21RegisterConfigurationEbNS0_14DeoptimizeKindENS0_23BuiltinContinuationModeENS0_13FrameInfoKindE
	.section	.text._ZNSt6vectorIN2v88internal18SharedFunctionInfoESaIS2_EE12emplace_backIJS2_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal18SharedFunctionInfoESaIS2_EE12emplace_backIJS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal18SharedFunctionInfoESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	.type	_ZNSt6vectorIN2v88internal18SharedFunctionInfoESaIS2_EE12emplace_backIJS2_EEEvDpOT_, @function
_ZNSt6vectorIN2v88internal18SharedFunctionInfoESaIS2_EE12emplace_backIJS2_EEEvDpOT_:
.LFB26827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L2070
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2070:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L2096
	testq	%rax, %rax
	je	.L2081
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L2097
.L2073:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L2074:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L2075
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L2084
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L2084
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L2077:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L2077
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L2079
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L2079:
	leaq	16(%r13,%rsi), %rax
.L2075:
	testq	%r14, %r14
	je	.L2080
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L2080:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2097:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L2098
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L2074
	.p2align 4,,10
	.p2align 3
.L2081:
	movl	$8, %r15d
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2084:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L2076:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L2076
	jmp	.L2079
.L2096:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2098:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L2073
	.cfi_endproc
.LFE26827:
	.size	_ZNSt6vectorIN2v88internal18SharedFunctionInfoESaIS2_EE12emplace_backIJS2_EEEvDpOT_, .-_ZNSt6vectorIN2v88internal18SharedFunctionInfoESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	.section	.text._ZNK2v88internal14OptimizedFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14OptimizedFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE
	.type	_ZNK2v88internal14OptimizedFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE, @function
_ZNK2v88internal14OptimizedFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE:
.LFB23350:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	41144(%rdx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	8(%rax), %rax
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$3, %eax
	je	.L2120
	leaq	-92(%rbp), %rsi
	movq	%r12, %rdi
	movl	$-1, -92(%rbp)
	leaq	-80(%rbp), %rbx
	call	_ZNK2v88internal14OptimizedFrame21GetDeoptimizationDataEPi
	movq	%rbx, %rdi
	movq	31(%rax), %rcx
	movl	-92(%rbp), %edx
	leal	12(%rdx,%rdx,2), %edx
	movq	%rcx, -104(%rbp)
	sall	$3, %edx
	movslq	%edx, %rdx
	movq	-1(%rax,%rdx), %rdx
	movq	15(%rax), %rsi
	sarq	$32, %rdx
	call	_ZN2v88internal19TranslationIteratorC1ENS0_9ByteArrayEi@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	testl	%r13d, %r13d
	je	.L2099
	leaq	-88(%rbp), %rax
	movq	%rax, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L2103:
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movl	%eax, %r12d
	leal	-3(%rax), %eax
	cmpl	$1, %eax
	jbe	.L2112
	cmpl	$1, %r12d
	je	.L2112
	movl	%r12d, %edi
	call	_ZN2v88internal11Translation19NumberOfOperandsForENS1_6OpcodeE@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jle	.L2103
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L2109:
	movq	%rbx, %rdi
	addl	$1, %r15d
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	cmpl	%r15d, %r12d
	jne	.L2109
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L2112:
	movq	%rbx, %rdi
	subl	$1, %r13d
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	%rbx, %rdi
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	movq	-104(%rbp), %rcx
	movq	%r14, %rdi
	leal	16(,%rax,8), %eax
	cltq
	movq	-1(%rcx,%rax), %rax
	movq	-112(%rbp), %rsi
	movq	%rax, -88(%rbp)
	call	_ZNSt6vectorIN2v88internal18SharedFunctionInfoESaIS2_EE12emplace_backIJS2_EEEvDpOT_
	movl	%r12d, %edi
	call	_ZN2v88internal11Translation19NumberOfOperandsForENS1_6OpcodeE@PLT
	cmpl	$2, %eax
	jle	.L2106
	leal	-2(%rax), %r12d
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L2107:
	movq	%rbx, %rdi
	addl	$1, %r15d
	call	_ZN2v88internal19TranslationIterator4NextEv@PLT
	cmpl	%r15d, %r12d
	jne	.L2107
.L2106:
	testl	%r13d, %r13d
	jne	.L2103
.L2099:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2121
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2120:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE
	jmp	.L2099
.L2121:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23350:
	.size	_ZNK2v88internal14OptimizedFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE, .-_ZNK2v88internal14OptimizedFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE
	.section	.text._ZNSt6vectorIN2v88internal12FrameSummaryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal12FrameSummaryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal12FrameSummaryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal12FrameSummaryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIN2v88internal12FrameSummaryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB27625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	movabsq	$7905747460161236407, %rdx
	subq	$40, %rsp
	movq	8(%rdi), %rcx
	movq	(%rdi), %r8
	movabsq	$164703072086692425, %rdi
	movq	%rcx, %rax
	subq	%r8, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	cmpq	%rdi, %rax
	je	.L2144
	movq	%rsi, %rdx
	movq	%rsi, %r14
	movq	%rsi, %r13
	subq	%r8, %rdx
	testq	%rax, %rax
	je	.L2134
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L2145
	movabsq	$9223372036854775800, %rsi
.L2124:
	movq	%rsi, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rcx
	movq	%rax, %r12
.L2133:
	movdqu	(%rbx), %xmm7
	movdqu	32(%rbx), %xmm1
	movq	48(%rbx), %rax
	movups	%xmm7, (%r12,%rdx)
	movdqu	16(%rbx), %xmm7
	movq	%rax, 48(%r12,%rdx)
	movups	%xmm7, 16(%r12,%rdx)
	movups	%xmm1, 32(%r12,%rdx)
	movq	%r12, %rdx
	cmpq	%r8, %r14
	je	.L2126
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L2128:
	movdqu	(%rax), %xmm1
	movups	%xmm1, (%rdx)
	movdqu	16(%rax), %xmm2
	movups	%xmm2, 16(%rdx)
	movdqu	32(%rax), %xmm3
	movups	%xmm3, 32(%rdx)
	movq	48(%rax), %rdi
	movq	%rdi, 48(%rdx)
	cmpl	$2, 8(%rax)
	ja	.L2130
	addq	$56, %rax
	addq	$56, %rdx
	cmpq	%rax, %r14
	jne	.L2128
.L2126:
	leaq	56(%rdx), %rbx
	cmpq	%rcx, %r14
	je	.L2129
	.p2align 4,,10
	.p2align 3
.L2131:
	movdqu	0(%r13), %xmm4
	movdqu	16(%r13), %xmm5
	movdqu	32(%r13), %xmm6
	movq	48(%r13), %rax
	cmpl	$2, 8(%r13)
	movups	%xmm4, (%rbx)
	movq	%rax, 48(%rbx)
	movups	%xmm5, 16(%rbx)
	movups	%xmm6, 32(%rbx)
	ja	.L2130
	addq	$56, %r13
	addq	$56, %rbx
	cmpq	%rcx, %r13
	jne	.L2131
.L2129:
	testq	%r8, %r8
	je	.L2132
	movq	%r8, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rsi
.L2132:
	movq	%r12, %xmm0
	movq	%rbx, %xmm2
	addq	%rsi, %r12
	punpcklqdq	%xmm2, %xmm0
	movq	%r12, 16(%r15)
	movups	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2145:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L2125
	xorl	%r12d, %r12d
	jmp	.L2133
	.p2align 4,,10
	.p2align 3
.L2134:
	movl	$56, %esi
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2130:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2144:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2125:
	cmpq	%rdi, %rsi
	cmova	%rdi, %rsi
	imulq	$56, %rsi, %rsi
	jmp	.L2124
	.cfi_endproc
.LFE27625:
	.size	_ZNSt6vectorIN2v88internal12FrameSummaryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIN2v88internal12FrameSummaryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.section	.text._ZNK2v88internal15JavaScriptFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal15JavaScriptFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.type	_ZNK2v88internal15JavaScriptFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE, @function
_ZNK2v88internal15JavaScriptFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE:
.LFB23266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	41144(%rdx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	8(%rax), %rsi
	movq	40(%rbx), %rax
	movq	%rsi, -184(%rbp)
	movl	43(%rsi), %ecx
	movq	(%rax), %r14
	leaq	63(%rsi), %rax
	testl	%ecx, %ecx
	js	.L2165
.L2148:
	movq	16(%rbx), %r13
	subl	%eax, %r14d
	movq	41112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2149
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpb	$0, _ZN2v88internal31FLAG_detailed_error_stack_traceE(%rip)
	movq	%rax, %rdx
	jne	.L2152
.L2169:
	movq	16(%rbx), %rax
	addq	$288, %rax
.L2153:
	movq	(%rbx), %rsi
	movq	(%rax), %r15
	leaq	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv(%rip), %rcx
	movq	128(%rsi), %rax
	cmpq	%rcx, %rax
	jne	.L2154
	movq	32(%rbx), %rax
	movq	(%rax), %rcx
	movq	-8(%rcx), %rax
	cmpq	$38, %rax
	jne	.L2155
	movq	(%rcx), %rax
	movq	-8(%rax), %rax
.L2155:
	cmpq	$36, %rax
	sete	%r13b
.L2156:
	movq	152(%rsi), %rax
	movq	(%rdx), %r8
	movzbl	%r13b, %r13d
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L2157
	movq	32(%rbx), %rax
	movq	-16(%rax), %rcx
.L2158:
	movq	%r8, -208(%rbp)
	movq	%rbx, %rdi
	movq	%rcx, -200(%rbp)
	call	*80(%rsi)
	pushq	%r15
	movq	16(%rbx), %rsi
	leaq	-176(%rbp), %rdi
	pushq	%r13
	movq	-208(%rbp), %r8
	movq	%rax, %rdx
	movl	%r14d, %r9d
	movq	-200(%rbp), %rcx
	call	_ZN2v88internal12FrameSummary22JavaScriptFrameSummaryC1EPNS0_7IsolateENS0_6ObjectENS0_10JSFunctionENS0_12AbstractCodeEibNS0_10FixedArrayE
	movq	-176(%rbp), %rax
	movq	8(%r12), %rsi
	movdqa	-160(%rbp), %xmm0
	movq	%rax, -112(%rbp)
	movl	-168(%rbp), %eax
	movaps	%xmm0, -96(%rbp)
	movl	%eax, -104(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -80(%rbp)
	movl	-136(%rbp), %eax
	movl	%eax, -72(%rbp)
	movzbl	-132(%rbp), %eax
	movb	%al, -68(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -64(%rbp)
	popq	%rax
	popq	%rdx
	cmpq	16(%r12), %rsi
	je	.L2159
	movdqa	-112(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movdqa	-96(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, 32(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 48(%rsi)
	addq	$56, 8(%r12)
.L2160:
	cmpl	$2, -104(%rbp)
	ja	.L2166
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2167
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2149:
	.cfi_restore_state
	movq	41088(%r13), %rdx
	cmpq	41096(%r13), %rdx
	je	.L2168
.L2151:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	cmpb	$0, _ZN2v88internal31FLAG_detailed_error_stack_traceE(%rip)
	je	.L2169
.L2152:
	movq	%rbx, %rdi
	movq	%rdx, -200(%rbp)
	call	_ZNK2v88internal15JavaScriptFrame13GetParametersEv.part.0
	movq	-200(%rbp), %rdx
	jmp	.L2153
	.p2align 4,,10
	.p2align 3
.L2165:
	leaq	-184(%rbp), %rdi
	call	_ZNK2v88internal4Code23OffHeapInstructionStartEv@PLT
	movq	-184(%rbp), %rsi
	jmp	.L2148
	.p2align 4,,10
	.p2align 3
.L2154:
	movq	%rdx, -200(%rbp)
	movq	%rbx, %rdi
	call	*%rax
	movq	(%rbx), %rsi
	movq	-200(%rbp), %rdx
	movl	%eax, %r13d
	jmp	.L2156
	.p2align 4,,10
	.p2align 3
.L2157:
	movq	%r8, -200(%rbp)
	movq	%rbx, %rdi
	call	*%rax
	movq	(%rbx), %rsi
	movq	-200(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2159:
	leaq	-112(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal12FrameSummaryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2160
	.p2align 4,,10
	.p2align 3
.L2168:
	movq	%r13, %rdi
	movq	%rsi, -200(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L2151
.L2166:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23266:
	.size	_ZNK2v88internal15JavaScriptFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE, .-_ZNK2v88internal15JavaScriptFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.section	.text._ZNK2v88internal25WasmInterpreterEntryFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25WasmInterpreterEntryFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.type	_ZNK2v88internal25WasmInterpreterEntryFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE, @function
_ZNK2v88internal25WasmInterpreterEntryFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE:
.LFB23384:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	41112(%rbx), %rdi
	movq	-16(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2171
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	movq	%rax, %r15
.L2172:
	movq	191(%rsi), %rax
	movq	32(%r14), %rdx
	leaq	-112(%rbp), %r13
	leaq	-144(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -112(%rbp)
	call	_ZN2v88internal13WasmDebugInfo19GetInterpretedStackEm@PLT
	movq	-144(%rbp), %rdi
	movq	-136(%rbp), %rax
	cmpq	%rdi, %rax
	je	.L2174
	movq	%rdi, %rbx
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2188:
	movdqa	-112(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movdqa	-96(%rbp), %xmm1
	movups	%xmm1, 16(%rsi)
	movdqa	-80(%rbp), %xmm2
	movups	%xmm2, 32(%rsi)
	movq	-64(%rbp), %rdx
	movq	%rdx, 48(%rsi)
	addq	$56, 8(%r12)
.L2176:
	cmpl	$2, -104(%rbp)
	ja	.L2186
	addq	$8, %rbx
	cmpq	%rbx, %rax
	je	.L2187
.L2178:
	movl	4(%rbx), %edx
	movl	(%rbx), %ecx
	movl	$2, -104(%rbp)
	movq	16(%r14), %rsi
	movq	%r15, -96(%rbp)
	movb	$0, -88(%rbp)
	movq	%rsi, -112(%rbp)
	movq	8(%r12), %rsi
	movl	%ecx, -84(%rbp)
	movl	%edx, -80(%rbp)
	cmpq	16(%r12), %rsi
	jne	.L2188
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, -152(%rbp)
	call	_ZNSt6vectorIN2v88internal12FrameSummaryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	movq	-152(%rbp), %rax
	jmp	.L2176
	.p2align 4,,10
	.p2align 3
.L2187:
	movq	-144(%rbp), %rdi
.L2174:
	testq	%rdi, %rdi
	je	.L2170
	call	_ZdlPv@PLT
.L2170:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2189
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2171:
	.cfi_restore_state
	movq	41088(%rbx), %r15
	cmpq	41096(%rbx), %r15
	je	.L2190
.L2173:
	leaq	8(%r15), %rax
	movq	%rax, 41088(%rbx)
	movq	%rsi, (%r15)
	jmp	.L2172
.L2190:
	movq	%rbx, %rdi
	movq	%rsi, -152(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, %r15
	jmp	.L2173
.L2186:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2189:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23384:
	.size	_ZNK2v88internal25WasmInterpreterEntryFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE, .-_ZNK2v88internal25WasmInterpreterEntryFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.section	.text._ZNK2v88internal25WasmInterpreterEntryFrame8positionEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal25WasmInterpreterEntryFrame8positionEv
	.type	_ZNK2v88internal25WasmInterpreterEntryFrame8positionEv, @function
_ZNK2v88internal25WasmInterpreterEntryFrame8positionEv:
.LFB23399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rsi
	movq	$0, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZNK2v88internal25WasmInterpreterEntryFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %rdi
	movdqu	(%rax), %xmm1
	movaps	%xmm1, -64(%rbp)
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -48(%rbp)
	movdqu	32(%rax), %xmm3
	movaps	%xmm3, -32(%rbp)
	movq	48(%rax), %rdx
	movq	%rdx, -16(%rbp)
	cmpq	%rdi, %rax
	je	.L2192
	.p2align 4,,10
	.p2align 3
.L2194:
	cmpl	$2, 8(%rax)
	ja	.L2196
	addq	$56, %rax
	cmpq	%rax, %rdi
	jne	.L2194
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2195
.L2192:
	call	_ZdlPv@PLT
.L2195:
	leaq	-64(%rbp), %rdi
	call	_ZNK2v88internal12FrameSummary16WasmFrameSummary14SourcePositionEv
	cmpl	$2, -56(%rbp)
	ja	.L2196
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2203
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2196:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2203:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23399:
	.size	_ZNK2v88internal25WasmInterpreterEntryFrame8positionEv, .-_ZNK2v88internal25WasmInterpreterEntryFrame8positionEv
	.section	.rodata._ZNK2v88internal14OptimizedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE.str1.1,"aMS",@progbits,1
.LC77:
	.string	"data.is_null()"
	.section	.rodata._ZNK2v88internal14OptimizedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC78:
	.string	"Missing deoptimization information for OptimizedFrame::Summarize."
	.section	.text._ZNK2v88internal14OptimizedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14OptimizedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.type	_ZNK2v88internal14OptimizedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE, @function
_ZNK2v88internal14OptimizedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE:
.LFB23343:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	41144(%rdx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal23InnerPointerToCodeCache13GetCacheEntryEm
	movq	8(%rax), %rax
	movl	43(%rax), %eax
	shrl	%eax
	andl	$31, %eax
	cmpl	$3, %eax
	je	.L2273
	leaq	-388(%rbp), %rsi
	movq	%r14, %rdi
	movl	$-1, -388(%rbp)
	call	_ZNK2v88internal14OptimizedFrame21GetDeoptimizationDataEPi
	cmpl	$-1, -388(%rbp)
	jne	.L2207
	testq	%rax, %rax
	jne	.L2274
	leaq	.LC78(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2207:
	leaq	-208(%rbp), %r13
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal15TranslatedStateC1EPKNS0_15JavaScriptFrameE@PLT
	movq	32(%r14), %rsi
	movq	%r13, %rdi
	call	_ZN2v88internal15TranslatedState7PrepareEm@PLT
	movq	(%r14), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv(%rip), %rdx
	movq	128(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2209
	movq	32(%r14), %rax
	movq	(%rax), %rdx
	movq	-8(%rdx), %rax
	cmpq	$38, %rax
	jne	.L2210
	movq	(%rdx), %rax
	movq	-8(%rax), %rax
.L2210:
	cmpq	$36, %rax
	sete	%bl
.L2211:
	movq	-208(%rbp), %r10
	movq	-200(%rbp), %r13
	cmpq	%r13, %r10
	je	.L2212
	leaq	-384(%rbp), %rax
	movq	%r10, %r15
	movq	%rax, -408(%rbp)
	jmp	.L2243
	.p2align 4,,10
	.p2align 3
.L2254:
	movdqu	72(%r15), %xmm0
	movdqu	56(%r15), %xmm1
	movq	56(%r15), %rdi
	movq	16(%r15), %r13
	movl	$0, -352(%rbp)
	movaps	%xmm1, -384(%rbp)
	movaps	%xmm0, -368(%rbp)
	call	_ZN2v88internal15TranslatedValue8GetValueEv@PLT
	movq	-408(%rbp), %rdi
	addl	$1, -352(%rbp)
	movq	%rax, -416(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E@PLT
	movq	-384(%rbp), %rdi
	call	_ZN2v88internal15TranslatedValue8GetValueEv@PLT
	movq	-408(%rbp), %rdi
	addl	$1, -352(%rbp)
	movq	%rax, -424(%rbp)
	call	_ZN2v88internal15TranslatedFrame15AdvanceIteratorEPSt15_Deque_iteratorINS0_15TranslatedValueERS3_PS3_E@PLT
	movl	(%r15), %eax
	subl	$4, %eax
	cmpl	$1, %eax
	ja	.L2275
	movl	4(%r15), %edi
	movq	16(%r14), %r13
	call	_ZN2v88internal8Builtins23GetBuiltinFromBailoutIdENS0_9BailoutIdE@PLT
	leaq	41184(%r13), %rdi
	movl	%eax, %esi
	call	_ZN2v88internal8Builtins7builtinEi@PLT
	movq	41112(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L2232
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdx
.L2233:
	xorl	%r9d, %r9d
.L2235:
	cmpb	$0, _ZN2v88internal31FLAG_detailed_error_stack_traceE(%rip)
	jne	.L2238
.L2280:
	movq	16(%r14), %rsi
	leaq	288(%rsi), %rax
.L2239:
	movq	(%rdx), %r8
	movq	-416(%rbp), %rcx
	movzbl	%bl, %ebx
	leaq	-336(%rbp), %rdi
	movq	-424(%rbp), %rdx
	movq	(%rcx), %rcx
	movq	(%rdx), %rdx
	pushq	(%rax)
	pushq	%rbx
	call	_ZN2v88internal12FrameSummary22JavaScriptFrameSummaryC1EPNS0_7IsolateENS0_6ObjectENS0_10JSFunctionENS0_12AbstractCodeEibNS0_10FixedArrayE
	movq	-336(%rbp), %rax
	movq	8(%r12), %rsi
	movdqa	-320(%rbp), %xmm2
	movq	%rax, -272(%rbp)
	movl	-328(%rbp), %eax
	movaps	%xmm2, -256(%rbp)
	movl	%eax, -264(%rbp)
	movq	-304(%rbp), %rax
	movq	%rax, -240(%rbp)
	movl	-296(%rbp), %eax
	movl	%eax, -232(%rbp)
	movzbl	-292(%rbp), %eax
	movb	%al, -228(%rbp)
	movq	-288(%rbp), %rax
	movq	%rax, -224(%rbp)
	popq	%rax
	popq	%rdx
	cmpq	16(%r12), %rsi
	je	.L2240
	movdqa	-272(%rbp), %xmm3
	movups	%xmm3, (%rsi)
	movdqa	-256(%rbp), %xmm4
	movups	%xmm4, 16(%rsi)
	movdqa	-240(%rbp), %xmm5
	movups	%xmm5, 32(%rsi)
	movq	-224(%rbp), %rax
	movq	%rax, 48(%rsi)
	addq	$56, 8(%r12)
.L2241:
	cmpl	$2, -264(%rbp)
	ja	.L2276
	movq	-200(%rbp), %r13
	addq	$120, %r15
	xorl	%ebx, %ebx
	cmpq	%r15, %r13
	je	.L2212
.L2243:
	movl	(%r15), %eax
	leal	-4(%rax), %edx
	cmpl	$1, %edx
	jbe	.L2254
	testl	%eax, %eax
	je	.L2254
	cmpl	$2, %eax
	movl	$1, %eax
	cmove	%eax, %ebx
	addq	$120, %r15
	cmpq	%r15, %r13
	jne	.L2243
.L2212:
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2244
	movq	-88(%rbp), %rax
	movq	-120(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L2245
	.p2align 4,,10
	.p2align 3
.L2246:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L2246
	movq	-160(%rbp), %rdi
.L2245:
	call	_ZdlPv@PLT
	movq	-200(%rbp), %r13
.L2244:
	movq	-208(%rbp), %r12
	cmpq	%r13, %r12
	je	.L2247
	.p2align 4,,10
	.p2align 3
.L2251:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2248
	movq	112(%r12), %rax
	movq	80(%r12), %rbx
	leaq	8(%rax), %r14
	cmpq	%rbx, %r14
	jbe	.L2249
	.p2align 4,,10
	.p2align 3
.L2250:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r14
	ja	.L2250
	movq	40(%r12), %rdi
.L2249:
	call	_ZdlPv@PLT
.L2248:
	addq	$120, %r12
	cmpq	%r13, %r12
	jne	.L2251
	movq	-208(%rbp), %r13
.L2247:
	testq	%r13, %r13
	je	.L2204
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2204:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2277
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2275:
	.cfi_restore_state
	movq	0(%r13), %rax
	movl	4(%r15), %r9d
	movq	16(%r14), %rcx
	movq	%rax, -272(%rbp)
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L2218
.L2221:
	movq	-272(%rbp), %rax
	movq	7(%rax), %rax
	testb	$1, %al
	jne	.L2278
.L2219:
	leaq	-272(%rbp), %rdi
	movq	%rcx, -440(%rbp)
	movl	%r9d, -432(%rbp)
	call	_ZNK2v88internal18SharedFunctionInfo7GetCodeEv@PLT
	movq	-440(%rbp), %rcx
	movl	-432(%rbp), %r9d
	movq	%rax, %rsi
.L2229:
	movq	41112(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L2279
	movl	%r9d, -432(%rbp)
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpb	$0, _ZN2v88internal31FLAG_detailed_error_stack_traceE(%rip)
	movl	-432(%rbp), %r9d
	movq	%rax, %rdx
	je	.L2280
.L2238:
	movq	%r14, %rdi
	movq	%rdx, -440(%rbp)
	movl	%r9d, -432(%rbp)
	call	_ZNK2v88internal15JavaScriptFrame13GetParametersEv.part.0
	movq	16(%r14), %rsi
	movq	-440(%rbp), %rdx
	movl	-432(%rbp), %r9d
	jmp	.L2239
	.p2align 4,,10
	.p2align 3
.L2232:
	movq	41088(%r13), %rdx
	cmpq	%rdx, 41096(%r13)
	je	.L2281
.L2234:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rdx)
	jmp	.L2233
	.p2align 4,,10
	.p2align 3
.L2279:
	movq	41088(%rcx), %rdx
	cmpq	%rdx, 41096(%rcx)
	je	.L2282
.L2237:
	leaq	8(%rdx), %rax
	movq	%rax, 41088(%rcx)
	movq	%rsi, (%rdx)
	jmp	.L2235
	.p2align 4,,10
	.p2align 3
.L2240:
	leaq	-272(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal12FrameSummaryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2241
	.p2align 4,,10
	.p2align 3
.L2218:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L2221
	movq	-272(%rbp), %rdx
	movq	31(%rdx), %rax
	testb	$1, %al
	jne	.L2283
.L2222:
	movq	7(%rdx), %rax
	testb	$1, %al
	jne	.L2284
.L2228:
	movq	-272(%rbp), %rax
	movq	7(%rax), %rax
	movq	7(%rax), %rsi
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2278:
	movq	-1(%rax), %rax
	cmpw	$91, 11(%rax)
	jne	.L2219
	movq	-272(%rbp), %rdx
	movq	31(%rdx), %rax
	testb	$1, %al
	je	.L2222
	.p2align 4,,10
	.p2align 3
.L2283:
	movq	-1(%rax), %rsi
	cmpw	$86, 11(%rsi)
	jne	.L2222
	movq	39(%rax), %rsi
	testb	$1, %sil
	je	.L2222
	movq	-1(%rsi), %rsi
	cmpw	$72, 11(%rsi)
	jne	.L2222
	movq	31(%rax), %rsi
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2281:
	movq	%r13, %rdi
	movq	%rax, -432(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-432(%rbp), %rsi
	movq	%rax, %rdx
	jmp	.L2234
	.p2align 4,,10
	.p2align 3
.L2284:
	movq	-1(%rax), %rax
	cmpw	$72, 11(%rax)
	jne	.L2228
	movq	-272(%rbp), %rax
	movq	7(%rax), %rsi
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2273:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZNK2v88internal15JavaScriptFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	jmp	.L2204
	.p2align 4,,10
	.p2align 3
.L2282:
	movq	%rcx, %rdi
	movq	%rsi, -448(%rbp)
	movl	%r9d, -440(%rbp)
	movq	%rcx, -432(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-448(%rbp), %rsi
	movl	-440(%rbp), %r9d
	movq	-432(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L2237
	.p2align 4,,10
	.p2align 3
.L2209:
	movq	%r14, %rdi
	call	*%rax
	movl	%eax, %ebx
	jmp	.L2211
.L2274:
	leaq	.LC77(%rip), %rsi
	leaq	.LC73(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2276:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23343:
	.size	_ZNK2v88internal14OptimizedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE, .-_ZNK2v88internal14OptimizedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.section	.text._ZNK2v88internal17WasmCompiledFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal17WasmCompiledFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.type	_ZNK2v88internal17WasmCompiledFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE, @function
_ZNK2v88internal17WasmCompiledFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE:
.LFB23379:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal4wasm16WasmCodeRefScopeC1Ev@PLT
	movq	16(%rbx), %rdx
	movq	40(%rbx), %rax
	movq	45752(%rdx), %rdi
	movq	(%rax), %rsi
	addq	$280, %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm@PLT
	movq	16(%rbx), %r15
	movq	%rax, %rcx
	movq	%rax, -208(%rbp)
	movq	40(%rbx), %rax
	movq	41112(%r15), %rdi
	movq	(%rax), %r13
	movq	%rcx, %rax
	movl	%r13d, %ecx
	subl	(%rax), %ecx
	movq	32(%rbx), %rax
	movl	%ecx, -196(%rbp)
	movq	-16(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2286
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L2287:
	movq	48(%rbx), %rdx
	movq	16(%rbx), %rax
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.L2289
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.L2310
.L2289:
	movq	%rax, -128(%rbp)
	movq	-208(%rbp), %rax
	movl	$1, -120(%rbp)
	movq	8(%r12), %rsi
	movq	%rax, -96(%rbp)
	movl	-196(%rbp), %eax
	movq	%r13, -112(%rbp)
	movb	%cl, -104(%rbp)
	movl	%eax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L2296
	movdqa	-128(%rbp), %xmm0
	movups	%xmm0, (%rsi)
	movdqa	-112(%rbp), %xmm1
	movups	%xmm1, 16(%rsi)
	movdqa	-96(%rbp), %xmm2
	movups	%xmm2, 32(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 48(%rsi)
	addq	$56, 8(%r12)
.L2297:
	cmpl	$2, -120(%rbp)
	ja	.L2311
	movq	%r14, %rdi
	call	_ZN2v88internal4wasm16WasmCodeRefScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2312
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2310:
	.cfi_restore_state
	movq	45752(%rax), %rdi
	addq	$280, %rdi
	call	_ZNK2v88internal4wasm15WasmCodeManager10LookupCodeEm@PLT
	testq	%rax, %rax
	je	.L2291
	cmpl	$2, 60(%rax)
	jne	.L2291
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	testq	%rcx, %rcx
	je	.L2292
	movl	(%rcx), %edx
.L2292:
	subl	(%rax), %edx
	movq	32(%rax), %rsi
	leaq	-128(%rbp), %r15
	xorl	%ecx, %ecx
	movl	%edx, -200(%rbp)
	movq	40(%rax), %rdx
	movq	%r15, %rdi
	call	_ZN2v88internal27SourcePositionTableIteratorC1ENS0_6VectorIKhEENS1_15IterationFilterE@PLT
	cmpl	$-1, -104(%rbp)
	je	.L2303
	movl	$0, -216(%rbp)
	jmp	.L2295
	.p2align 4,,10
	.p2align 3
.L2294:
	movq	-88(%rbp), %rdx
	movq	%r15, %rdi
	shrq	%rdx
	andl	$1073741823, %edx
	leal	-1(%rdx), %eax
	movl	%eax, -216(%rbp)
	call	_ZN2v88internal27SourcePositionTableIterator7AdvanceEv@PLT
	cmpl	$-1, -104(%rbp)
	je	.L2309
.L2295:
	movl	-200(%rbp), %eax
	cmpl	-96(%rbp), %eax
	jg	.L2294
.L2309:
	movl	-216(%rbp), %eax
	testl	%eax, %eax
	setne	%cl
.L2293:
	movq	16(%rbx), %rax
	jmp	.L2289
	.p2align 4,,10
	.p2align 3
.L2286:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L2313
.L2288:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L2287
	.p2align 4,,10
	.p2align 3
.L2291:
	movq	16(%rbx), %rax
	xorl	%ecx, %ecx
	jmp	.L2289
	.p2align 4,,10
	.p2align 3
.L2296:
	leaq	-128(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal12FrameSummaryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2297
	.p2align 4,,10
	.p2align 3
.L2313:
	movq	%r15, %rdi
	movq	%rsi, -216(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-216(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L2288
	.p2align 4,,10
	.p2align 3
.L2303:
	xorl	%ecx, %ecx
	jmp	.L2293
.L2312:
	call	__stack_chk_fail@PLT
.L2311:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE23379:
	.size	_ZNK2v88internal17WasmCompiledFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE, .-_ZNK2v88internal17WasmCompiledFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.section	.text._ZNK2v88internal16InterpretedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal16InterpretedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.type	_ZNK2v88internal16InterpretedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE, @function
_ZNK2v88internal16InterpretedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE:
.LFB23362:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi(%rip), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	16(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	144(%rax), %rax
	cmpq	%r15, %rax
	jne	.L2315
	movq	32(%rdi), %rax
	subq	$24, %rax
.L2316:
	movq	41112(%r13), %rdi
	movq	(%rax), %rsi
	testq	%rdi, %rdi
	je	.L2317
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	cmpb	$0, _ZN2v88internal31FLAG_detailed_error_stack_traceE(%rip)
	movq	%rax, %rcx
	jne	.L2320
.L2343:
	movq	16(%rbx), %rax
	addq	$288, %rax
.L2321:
	movq	(%rbx), %rdx
	movq	(%rax), %r14
	leaq	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv(%rip), %rsi
	movq	128(%rdx), %rax
	cmpq	%rsi, %rax
	jne	.L2322
	movq	32(%rbx), %rax
	movq	(%rax), %rsi
	movq	-8(%rsi), %rax
	cmpq	$38, %rax
	jne	.L2323
	movq	(%rsi), %rax
	movq	-8(%rax), %rax
.L2323:
	cmpq	$36, %rax
	movq	144(%rdx), %rax
	sete	%r13b
	movzbl	%r13b, %r13d
	cmpq	%r15, %rax
	jne	.L2325
.L2344:
	movq	32(%rbx), %rax
	subq	$32, %rax
.L2326:
	movq	(%rax), %r15
	movq	152(%rdx), %rax
	movq	(%rcx), %r8
	leaq	_ZNK2v88internal15JavaScriptFrame8functionEv(%rip), %rcx
	sarq	$32, %r15
	subl	$53, %r15d
	cmpq	%rcx, %rax
	jne	.L2327
	movq	32(%rbx), %rax
	movq	-16(%rax), %rcx
.L2328:
	movq	80(%rdx), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame8receiverEv(%rip), %rsi
	cmpq	%rsi, %rax
	jne	.L2329
	movq	112(%rdx), %rax
	leaq	_ZNK2v88internal15JavaScriptFrame12GetParameterEi(%rip), %rsi
	movq	%r8, -192(%rbp)
	movq	%rcx, -184(%rbp)
	cmpq	%rsi, %rax
	jne	.L2330
	movq	%rbx, %rdi
	call	*120(%rdx)
	leaq	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv(%rip), %rsi
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %r8
	leal	0(,%rax,8), %edx
	movq	(%rbx), %rax
	movq	56(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2331
	movq	32(%rbx), %rax
	addq	$16, %rax
.L2332:
	movslq	%edx, %rdx
	movq	(%rdx,%rax), %rdx
.L2334:
	pushq	%r14
	movq	16(%rbx), %rsi
	leaq	-176(%rbp), %rdi
	movl	%r15d, %r9d
	pushq	%r13
	call	_ZN2v88internal12FrameSummary22JavaScriptFrameSummaryC1EPNS0_7IsolateENS0_6ObjectENS0_10JSFunctionENS0_12AbstractCodeEibNS0_10FixedArrayE
	movq	-176(%rbp), %rax
	movq	8(%r12), %rsi
	movdqa	-160(%rbp), %xmm0
	movq	%rax, -112(%rbp)
	movl	-168(%rbp), %eax
	movaps	%xmm0, -96(%rbp)
	movl	%eax, -104(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -80(%rbp)
	movl	-136(%rbp), %eax
	movl	%eax, -72(%rbp)
	movzbl	-132(%rbp), %eax
	movb	%al, -68(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -64(%rbp)
	popq	%rax
	popq	%rdx
	cmpq	16(%r12), %rsi
	je	.L2335
	movdqa	-112(%rbp), %xmm1
	movups	%xmm1, (%rsi)
	movdqa	-96(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, 32(%rsi)
	movq	-64(%rbp), %rax
	movq	%rax, 48(%rsi)
	addq	$56, 8(%r12)
.L2336:
	cmpl	$2, -104(%rbp)
	ja	.L2340
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2341
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2317:
	.cfi_restore_state
	movq	41088(%r13), %rcx
	cmpq	41096(%r13), %rcx
	je	.L2342
.L2319:
	leaq	8(%rcx), %rax
	movq	%rax, 41088(%r13)
	movq	%rsi, (%rcx)
	cmpb	$0, _ZN2v88internal31FLAG_detailed_error_stack_traceE(%rip)
	je	.L2343
.L2320:
	movq	%rbx, %rdi
	movq	%rcx, -184(%rbp)
	call	_ZNK2v88internal15JavaScriptFrame13GetParametersEv.part.0
	movq	-184(%rbp), %rcx
	jmp	.L2321
	.p2align 4,,10
	.p2align 3
.L2315:
	movl	$-2, %esi
	call	*%rax
	jmp	.L2316
	.p2align 4,,10
	.p2align 3
.L2322:
	movq	%rcx, -184(%rbp)
	movq	%rbx, %rdi
	call	*%rax
	movq	(%rbx), %rdx
	movq	-184(%rbp), %rcx
	movl	%eax, %r13d
	movq	144(%rdx), %rax
	movzbl	%r13b, %r13d
	cmpq	%r15, %rax
	je	.L2344
.L2325:
	movq	%rcx, -184(%rbp)
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*%rax
	movq	(%rbx), %rdx
	movq	-184(%rbp), %rcx
	jmp	.L2326
	.p2align 4,,10
	.p2align 3
.L2329:
	movq	%r8, -192(%rbp)
	movq	%rbx, %rdi
	movq	%rcx, -184(%rbp)
	call	*%rax
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L2327:
	movq	%r8, -184(%rbp)
	movq	%rbx, %rdi
	call	*%rax
	movq	(%rbx), %rdx
	movq	-184(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L2328
	.p2align 4,,10
	.p2align 3
.L2335:
	leaq	-112(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal12FrameSummaryESaIS2_EE17_M_realloc_insertIJS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L2336
	.p2align 4,,10
	.p2align 3
.L2330:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*%rax
	movq	-192(%rbp), %r8
	movq	-184(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L2331:
	movq	%r8, -200(%rbp)
	movq	%rbx, %rdi
	movq	%rcx, -192(%rbp)
	movl	%edx, -184(%rbp)
	call	*%rax
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %rcx
	movl	-184(%rbp), %edx
	jmp	.L2332
	.p2align 4,,10
	.p2align 3
.L2342:
	movq	%r13, %rdi
	movq	%rsi, -184(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-184(%rbp), %rsi
	movq	%rax, %rcx
	jmp	.L2319
.L2340:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2341:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE23362:
	.size	_ZNK2v88internal16InterpretedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE, .-_ZNK2v88internal16InterpretedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal10StackFrame33return_address_location_resolver_E,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal10StackFrame33return_address_location_resolver_E, @function
_GLOBAL__sub_I__ZN2v88internal10StackFrame33return_address_location_resolver_E:
.LFB29441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE29441:
	.size	_GLOBAL__sub_I__ZN2v88internal10StackFrame33return_address_location_resolver_E, .-_GLOBAL__sub_I__ZN2v88internal10StackFrame33return_address_location_resolver_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal10StackFrame33return_address_location_resolver_E
	.weak	_ZTVN2v88internal13WasmExitFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal13WasmExitFrameE,"awG",@progbits,_ZTVN2v88internal13WasmExitFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal13WasmExitFrameE, @object
	.size	_ZTVN2v88internal13WasmExitFrameE, 168
_ZTVN2v88internal13WasmExitFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal13WasmExitFrame4typeEv
	.quad	_ZNK2v88internal17WasmCompiledFrame14unchecked_codeEv
	.quad	_ZNK2v88internal17WasmCompiledFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal17WasmCompiledFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.quad	_ZN2v88internal13WasmExitFrameD1Ev
	.quad	_ZN2v88internal13WasmExitFrameD0Ev
	.quad	_ZNK2v88internal17WasmCompiledFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal13StandardFrame8receiverEv
	.quad	_ZNK2v88internal17WasmCompiledFrame6scriptEv
	.quad	_ZNK2v88internal13StandardFrame7contextEv
	.quad	_ZNK2v88internal17WasmCompiledFrame8positionEv
	.quad	_ZNK2v88internal13StandardFrame12GetParameterEi
	.quad	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal13StandardFrame13IsConstructorEv
	.quad	_ZNK2v88internal17WasmCompiledFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.weak	_ZTVN2v88internal13WasmToJsFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal13WasmToJsFrameE,"awG",@progbits,_ZTVN2v88internal13WasmToJsFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal13WasmToJsFrameE, @object
	.size	_ZTVN2v88internal13WasmToJsFrameE, 168
_ZTVN2v88internal13WasmToJsFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal13WasmToJsFrame4typeEv
	.quad	_ZNK2v88internal9StubFrame14unchecked_codeEv
	.quad	_ZNK2v88internal9StubFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	_ZN2v88internal13WasmToJsFrameD1Ev
	.quad	_ZN2v88internal13WasmToJsFrameD0Ev
	.quad	_ZNK2v88internal9StubFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal13StandardFrame8receiverEv
	.quad	_ZNK2v88internal13StandardFrame6scriptEv
	.quad	_ZNK2v88internal13StandardFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal13StandardFrame12GetParameterEi
	.quad	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal13StandardFrame13IsConstructorEv
	.quad	_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.weak	_ZTVN2v88internal13JsToWasmFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal13JsToWasmFrameE,"awG",@progbits,_ZTVN2v88internal13JsToWasmFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal13JsToWasmFrameE, @object
	.size	_ZTVN2v88internal13JsToWasmFrameE, 168
_ZTVN2v88internal13JsToWasmFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal13JsToWasmFrame4typeEv
	.quad	_ZNK2v88internal9StubFrame14unchecked_codeEv
	.quad	_ZNK2v88internal9StubFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	_ZN2v88internal13JsToWasmFrameD1Ev
	.quad	_ZN2v88internal13JsToWasmFrameD0Ev
	.quad	_ZNK2v88internal9StubFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal13StandardFrame8receiverEv
	.quad	_ZNK2v88internal13StandardFrame6scriptEv
	.quad	_ZNK2v88internal13StandardFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal13StandardFrame12GetParameterEi
	.quad	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal13StandardFrame13IsConstructorEv
	.quad	_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.weak	_ZTVN2v88internal14ConstructFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal14ConstructFrameE,"awG",@progbits,_ZTVN2v88internal14ConstructFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal14ConstructFrameE, @object
	.size	_ZTVN2v88internal14ConstructFrameE, 168
_ZTVN2v88internal14ConstructFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal14ConstructFrame4typeEv
	.quad	_ZNK2v88internal13InternalFrame14unchecked_codeEv
	.quad	_ZNK2v88internal13InternalFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	_ZN2v88internal14ConstructFrameD1Ev
	.quad	_ZN2v88internal14ConstructFrameD0Ev
	.quad	_ZNK2v88internal13InternalFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal13StandardFrame8receiverEv
	.quad	_ZNK2v88internal13StandardFrame6scriptEv
	.quad	_ZNK2v88internal13StandardFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal13StandardFrame12GetParameterEi
	.quad	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal13StandardFrame13IsConstructorEv
	.quad	_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.weak	_ZTVN2v88internal24BuiltinContinuationFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal24BuiltinContinuationFrameE,"awG",@progbits,_ZTVN2v88internal24BuiltinContinuationFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal24BuiltinContinuationFrameE, @object
	.size	_ZTVN2v88internal24BuiltinContinuationFrameE, 168
_ZTVN2v88internal24BuiltinContinuationFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal24BuiltinContinuationFrame4typeEv
	.quad	_ZNK2v88internal13InternalFrame14unchecked_codeEv
	.quad	_ZNK2v88internal13InternalFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	_ZN2v88internal24BuiltinContinuationFrameD1Ev
	.quad	_ZN2v88internal24BuiltinContinuationFrameD0Ev
	.quad	_ZNK2v88internal13InternalFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal13StandardFrame8receiverEv
	.quad	_ZNK2v88internal13StandardFrame6scriptEv
	.quad	_ZNK2v88internal13StandardFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal13StandardFrame12GetParameterEi
	.quad	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal13StandardFrame13IsConstructorEv
	.quad	_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.weak	_ZTVN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameE,"awG",@progbits,_ZTVN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameE, @object
	.size	_ZTVN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameE, 200
_ZTVN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal43JavaScriptBuiltinContinuationWithCatchFrame4typeEv
	.quad	_ZNK2v88internal15JavaScriptFrame14unchecked_codeEv
	.quad	_ZNK2v88internal15JavaScriptFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.quad	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD1Ev
	.quad	_ZN2v88internal43JavaScriptBuiltinContinuationWithCatchFrameD0Ev
	.quad	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal15JavaScriptFrame8receiverEv
	.quad	_ZNK2v88internal15JavaScriptFrame6scriptEv
	.quad	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal15JavaScriptFrame12GetParameterEi
	.quad	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv
	.quad	_ZNK2v88internal15JavaScriptFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.quad	_ZNK2v88internal15JavaScriptFrame8functionEv
	.quad	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE
	.quad	_ZN2v88internal15JavaScriptFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE
	.quad	_ZNK2v88internal15JavaScriptFrame14PrintFrameKindEPNS0_12StringStreamE
	.weak	_ZTVN2v88internal11NativeFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal11NativeFrameE,"awG",@progbits,_ZTVN2v88internal11NativeFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal11NativeFrameE, @object
	.size	_ZTVN2v88internal11NativeFrameE, 96
_ZTVN2v88internal11NativeFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal10StackFrame11is_standardEv
	.quad	_ZNK2v88internal11NativeFrame4typeEv
	.quad	_ZNK2v88internal11NativeFrame14unchecked_codeEv
	.quad	_ZNK2v88internal11NativeFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	_ZN2v88internal11NativeFrameD1Ev
	.quad	_ZN2v88internal11NativeFrameD0Ev
	.quad	_ZNK2v88internal11NativeFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal11NativeFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.weak	_ZTVN2v88internal10EntryFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal10EntryFrameE,"awG",@progbits,_ZTVN2v88internal10EntryFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal10EntryFrameE, @object
	.size	_ZTVN2v88internal10EntryFrameE, 96
_ZTVN2v88internal10EntryFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal10StackFrame11is_standardEv
	.quad	_ZNK2v88internal10EntryFrame4typeEv
	.quad	_ZNK2v88internal10EntryFrame14unchecked_codeEv
	.quad	_ZNK2v88internal10EntryFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	_ZN2v88internal10EntryFrameD1Ev
	.quad	_ZN2v88internal10EntryFrameD0Ev
	.quad	_ZNK2v88internal10EntryFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal10EntryFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10EntryFrame14GetCallerStateEPNS0_10StackFrame5StateE
	.weak	_ZTVN2v88internal15CWasmEntryFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal15CWasmEntryFrameE,"awG",@progbits,_ZTVN2v88internal15CWasmEntryFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal15CWasmEntryFrameE, @object
	.size	_ZTVN2v88internal15CWasmEntryFrameE, 168
_ZTVN2v88internal15CWasmEntryFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal15CWasmEntryFrame4typeEv
	.quad	_ZNK2v88internal9StubFrame14unchecked_codeEv
	.quad	_ZNK2v88internal9StubFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	_ZN2v88internal15CWasmEntryFrameD1Ev
	.quad	_ZN2v88internal15CWasmEntryFrameD0Ev
	.quad	_ZNK2v88internal9StubFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal15CWasmEntryFrame14GetCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal13StandardFrame8receiverEv
	.quad	_ZNK2v88internal13StandardFrame6scriptEv
	.quad	_ZNK2v88internal13StandardFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal13StandardFrame12GetParameterEi
	.quad	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal13StandardFrame13IsConstructorEv
	.quad	_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.weak	_ZTVN2v88internal19ConstructEntryFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal19ConstructEntryFrameE,"awG",@progbits,_ZTVN2v88internal19ConstructEntryFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal19ConstructEntryFrameE, @object
	.size	_ZTVN2v88internal19ConstructEntryFrameE, 96
_ZTVN2v88internal19ConstructEntryFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal10StackFrame11is_standardEv
	.quad	_ZNK2v88internal19ConstructEntryFrame4typeEv
	.quad	_ZNK2v88internal19ConstructEntryFrame14unchecked_codeEv
	.quad	_ZNK2v88internal10EntryFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	_ZN2v88internal19ConstructEntryFrameD1Ev
	.quad	_ZN2v88internal19ConstructEntryFrameD0Ev
	.quad	_ZNK2v88internal10EntryFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal10EntryFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10EntryFrame14GetCallerStateEPNS0_10StackFrame5StateE
	.weak	_ZTVN2v88internal9ExitFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal9ExitFrameE,"awG",@progbits,_ZTVN2v88internal9ExitFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal9ExitFrameE, @object
	.size	_ZTVN2v88internal9ExitFrameE, 96
_ZTVN2v88internal9ExitFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal10StackFrame11is_standardEv
	.quad	_ZNK2v88internal9ExitFrame4typeEv
	.quad	_ZNK2v88internal9ExitFrame14unchecked_codeEv
	.quad	_ZNK2v88internal9ExitFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	_ZN2v88internal9ExitFrameD1Ev
	.quad	_ZN2v88internal9ExitFrameD0Ev
	.quad	_ZNK2v88internal9ExitFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal9ExitFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.weak	_ZTVN2v88internal10StackFrameE
	.section	.data.rel.ro._ZTVN2v88internal10StackFrameE,"awG",@progbits,_ZTVN2v88internal10StackFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal10StackFrameE, @object
	.size	_ZTVN2v88internal10StackFrameE, 96
_ZTVN2v88internal10StackFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal10StackFrame11is_standardEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.weak	_ZTVN2v88internal16BuiltinExitFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal16BuiltinExitFrameE,"awG",@progbits,_ZTVN2v88internal16BuiltinExitFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal16BuiltinExitFrameE, @object
	.size	_ZTVN2v88internal16BuiltinExitFrameE, 96
_ZTVN2v88internal16BuiltinExitFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal10StackFrame11is_standardEv
	.quad	_ZNK2v88internal16BuiltinExitFrame4typeEv
	.quad	_ZNK2v88internal9ExitFrame14unchecked_codeEv
	.quad	_ZNK2v88internal9ExitFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal16BuiltinExitFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.quad	_ZN2v88internal16BuiltinExitFrameD1Ev
	.quad	_ZN2v88internal16BuiltinExitFrameD0Ev
	.quad	_ZNK2v88internal9ExitFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal9ExitFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.weak	_ZTVN2v88internal13StandardFrameE
	.section	.data.rel.ro._ZTVN2v88internal13StandardFrameE,"awG",@progbits,_ZTVN2v88internal13StandardFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal13StandardFrameE, @object
	.size	_ZTVN2v88internal13StandardFrameE, 168
_ZTVN2v88internal13StandardFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal13StandardFrame8receiverEv
	.quad	_ZNK2v88internal13StandardFrame6scriptEv
	.quad	_ZNK2v88internal13StandardFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal13StandardFrame12GetParameterEi
	.quad	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal13StandardFrame13IsConstructorEv
	.quad	_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.weak	_ZTVN2v88internal9StubFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal9StubFrameE,"awG",@progbits,_ZTVN2v88internal9StubFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal9StubFrameE, @object
	.size	_ZTVN2v88internal9StubFrameE, 168
_ZTVN2v88internal9StubFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal9StubFrame4typeEv
	.quad	_ZNK2v88internal9StubFrame14unchecked_codeEv
	.quad	_ZNK2v88internal9StubFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	_ZN2v88internal9StubFrameD1Ev
	.quad	_ZN2v88internal9StubFrameD0Ev
	.quad	_ZNK2v88internal9StubFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal13StandardFrame8receiverEv
	.quad	_ZNK2v88internal13StandardFrame6scriptEv
	.quad	_ZNK2v88internal13StandardFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal13StandardFrame12GetParameterEi
	.quad	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal13StandardFrame13IsConstructorEv
	.quad	_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.weak	_ZTVN2v88internal14OptimizedFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal14OptimizedFrameE,"awG",@progbits,_ZTVN2v88internal14OptimizedFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal14OptimizedFrameE, @object
	.size	_ZTVN2v88internal14OptimizedFrameE, 200
_ZTVN2v88internal14OptimizedFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal14OptimizedFrame4typeEv
	.quad	_ZNK2v88internal15JavaScriptFrame14unchecked_codeEv
	.quad	_ZNK2v88internal14OptimizedFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.quad	_ZN2v88internal14OptimizedFrameD1Ev
	.quad	_ZN2v88internal14OptimizedFrameD0Ev
	.quad	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal14OptimizedFrame8receiverEv
	.quad	_ZNK2v88internal15JavaScriptFrame6scriptEv
	.quad	_ZNK2v88internal15JavaScriptFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal15JavaScriptFrame12GetParameterEi
	.quad	_ZNK2v88internal14OptimizedFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv
	.quad	_ZNK2v88internal14OptimizedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.quad	_ZNK2v88internal15JavaScriptFrame8functionEv
	.quad	_ZNK2v88internal14OptimizedFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE
	.quad	_ZN2v88internal14OptimizedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE
	.quad	_ZNK2v88internal15JavaScriptFrame14PrintFrameKindEPNS0_12StringStreamE
	.weak	_ZTVN2v88internal15JavaScriptFrameE
	.section	.data.rel.ro._ZTVN2v88internal15JavaScriptFrameE,"awG",@progbits,_ZTVN2v88internal15JavaScriptFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal15JavaScriptFrameE, @object
	.size	_ZTVN2v88internal15JavaScriptFrameE, 200
_ZTVN2v88internal15JavaScriptFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	__cxa_pure_virtual
	.quad	_ZNK2v88internal15JavaScriptFrame14unchecked_codeEv
	.quad	_ZNK2v88internal15JavaScriptFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal15JavaScriptFrame8receiverEv
	.quad	_ZNK2v88internal15JavaScriptFrame6scriptEv
	.quad	_ZNK2v88internal15JavaScriptFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal15JavaScriptFrame12GetParameterEi
	.quad	_ZNK2v88internal15JavaScriptFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv
	.quad	_ZNK2v88internal15JavaScriptFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.quad	_ZNK2v88internal15JavaScriptFrame8functionEv
	.quad	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE
	.quad	_ZN2v88internal15JavaScriptFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE
	.quad	_ZNK2v88internal15JavaScriptFrame14PrintFrameKindEPNS0_12StringStreamE
	.weak	_ZTVN2v88internal34JavaScriptBuiltinContinuationFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal34JavaScriptBuiltinContinuationFrameE,"awG",@progbits,_ZTVN2v88internal34JavaScriptBuiltinContinuationFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal34JavaScriptBuiltinContinuationFrameE, @object
	.size	_ZTVN2v88internal34JavaScriptBuiltinContinuationFrameE, 200
_ZTVN2v88internal34JavaScriptBuiltinContinuationFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame4typeEv
	.quad	_ZNK2v88internal15JavaScriptFrame14unchecked_codeEv
	.quad	_ZNK2v88internal15JavaScriptFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.quad	_ZN2v88internal34JavaScriptBuiltinContinuationFrameD1Ev
	.quad	_ZN2v88internal34JavaScriptBuiltinContinuationFrameD0Ev
	.quad	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal15JavaScriptFrame8receiverEv
	.quad	_ZNK2v88internal15JavaScriptFrame6scriptEv
	.quad	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal15JavaScriptFrame12GetParameterEi
	.quad	_ZNK2v88internal34JavaScriptBuiltinContinuationFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv
	.quad	_ZNK2v88internal15JavaScriptFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.quad	_ZNK2v88internal15JavaScriptFrame8functionEv
	.quad	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE
	.quad	_ZN2v88internal15JavaScriptFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE
	.quad	_ZNK2v88internal15JavaScriptFrame14PrintFrameKindEPNS0_12StringStreamE
	.weak	_ZTVN2v88internal16InterpretedFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal16InterpretedFrameE,"awG",@progbits,_ZTVN2v88internal16InterpretedFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal16InterpretedFrameE, @object
	.size	_ZTVN2v88internal16InterpretedFrameE, 200
_ZTVN2v88internal16InterpretedFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal16InterpretedFrame4typeEv
	.quad	_ZNK2v88internal15JavaScriptFrame14unchecked_codeEv
	.quad	_ZNK2v88internal15JavaScriptFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.quad	_ZN2v88internal16InterpretedFrameD1Ev
	.quad	_ZN2v88internal16InterpretedFrameD0Ev
	.quad	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal15JavaScriptFrame8receiverEv
	.quad	_ZNK2v88internal15JavaScriptFrame6scriptEv
	.quad	_ZNK2v88internal15JavaScriptFrame7contextEv
	.quad	_ZNK2v88internal16InterpretedFrame8positionEv
	.quad	_ZNK2v88internal15JavaScriptFrame12GetParameterEi
	.quad	_ZNK2v88internal15JavaScriptFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv
	.quad	_ZNK2v88internal16InterpretedFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal16InterpretedFrame20GetExpressionAddressEi
	.quad	_ZNK2v88internal15JavaScriptFrame8functionEv
	.quad	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE
	.quad	_ZN2v88internal16InterpretedFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE
	.quad	_ZNK2v88internal15JavaScriptFrame14PrintFrameKindEPNS0_12StringStreamE
	.weak	_ZTVN2v88internal21ArgumentsAdaptorFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal21ArgumentsAdaptorFrameE,"awG",@progbits,_ZTVN2v88internal21ArgumentsAdaptorFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal21ArgumentsAdaptorFrameE, @object
	.size	_ZTVN2v88internal21ArgumentsAdaptorFrameE, 200
_ZTVN2v88internal21ArgumentsAdaptorFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal21ArgumentsAdaptorFrame4typeEv
	.quad	_ZNK2v88internal21ArgumentsAdaptorFrame14unchecked_codeEv
	.quad	_ZNK2v88internal15JavaScriptFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal21ArgumentsAdaptorFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.quad	_ZN2v88internal21ArgumentsAdaptorFrameD1Ev
	.quad	_ZN2v88internal21ArgumentsAdaptorFrameD0Ev
	.quad	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal15JavaScriptFrame8receiverEv
	.quad	_ZNK2v88internal15JavaScriptFrame6scriptEv
	.quad	_ZNK2v88internal15JavaScriptFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal15JavaScriptFrame12GetParameterEi
	.quad	_ZNK2v88internal21ArgumentsAdaptorFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv
	.quad	_ZNK2v88internal15JavaScriptFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.quad	_ZNK2v88internal15JavaScriptFrame8functionEv
	.quad	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE
	.quad	_ZN2v88internal15JavaScriptFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE
	.quad	_ZNK2v88internal15JavaScriptFrame14PrintFrameKindEPNS0_12StringStreamE
	.weak	_ZTVN2v88internal12BuiltinFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal12BuiltinFrameE,"awG",@progbits,_ZTVN2v88internal12BuiltinFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal12BuiltinFrameE, @object
	.size	_ZTVN2v88internal12BuiltinFrameE, 200
_ZTVN2v88internal12BuiltinFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal12BuiltinFrame4typeEv
	.quad	_ZNK2v88internal15JavaScriptFrame14unchecked_codeEv
	.quad	_ZNK2v88internal15JavaScriptFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal15JavaScriptFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.quad	_ZN2v88internal12BuiltinFrameD1Ev
	.quad	_ZN2v88internal12BuiltinFrameD0Ev
	.quad	_ZNK2v88internal15JavaScriptFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal15JavaScriptFrame8receiverEv
	.quad	_ZNK2v88internal15JavaScriptFrame6scriptEv
	.quad	_ZNK2v88internal15JavaScriptFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal15JavaScriptFrame12GetParameterEi
	.quad	_ZNK2v88internal12BuiltinFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal15JavaScriptFrame13IsConstructorEv
	.quad	_ZNK2v88internal15JavaScriptFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.quad	_ZNK2v88internal15JavaScriptFrame8functionEv
	.quad	_ZNK2v88internal15JavaScriptFrame12GetFunctionsEPSt6vectorINS0_18SharedFunctionInfoESaIS3_EE
	.quad	_ZN2v88internal15JavaScriptFrame29LookupExceptionHandlerInTableEPiPNS0_12HandlerTable15CatchPredictionE
	.quad	_ZNK2v88internal12BuiltinFrame14PrintFrameKindEPNS0_12StringStreamE
	.weak	_ZTVN2v88internal17WasmCompiledFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal17WasmCompiledFrameE,"awG",@progbits,_ZTVN2v88internal17WasmCompiledFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal17WasmCompiledFrameE, @object
	.size	_ZTVN2v88internal17WasmCompiledFrameE, 168
_ZTVN2v88internal17WasmCompiledFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal17WasmCompiledFrame4typeEv
	.quad	_ZNK2v88internal17WasmCompiledFrame14unchecked_codeEv
	.quad	_ZNK2v88internal17WasmCompiledFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal17WasmCompiledFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.quad	_ZN2v88internal17WasmCompiledFrameD1Ev
	.quad	_ZN2v88internal17WasmCompiledFrameD0Ev
	.quad	_ZNK2v88internal17WasmCompiledFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal13StandardFrame8receiverEv
	.quad	_ZNK2v88internal17WasmCompiledFrame6scriptEv
	.quad	_ZNK2v88internal13StandardFrame7contextEv
	.quad	_ZNK2v88internal17WasmCompiledFrame8positionEv
	.quad	_ZNK2v88internal13StandardFrame12GetParameterEi
	.quad	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal13StandardFrame13IsConstructorEv
	.quad	_ZNK2v88internal17WasmCompiledFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.weak	_ZTVN2v88internal25WasmInterpreterEntryFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal25WasmInterpreterEntryFrameE,"awG",@progbits,_ZTVN2v88internal25WasmInterpreterEntryFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal25WasmInterpreterEntryFrameE, @object
	.size	_ZTVN2v88internal25WasmInterpreterEntryFrameE, 168
_ZTVN2v88internal25WasmInterpreterEntryFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal25WasmInterpreterEntryFrame4typeEv
	.quad	_ZNK2v88internal25WasmInterpreterEntryFrame14unchecked_codeEv
	.quad	_ZNK2v88internal25WasmInterpreterEntryFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal25WasmInterpreterEntryFrame5PrintEPNS0_12StringStreamENS0_10StackFrame9PrintModeEi
	.quad	_ZN2v88internal25WasmInterpreterEntryFrameD1Ev
	.quad	_ZN2v88internal25WasmInterpreterEntryFrameD0Ev
	.quad	_ZNK2v88internal25WasmInterpreterEntryFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal13StandardFrame8receiverEv
	.quad	_ZNK2v88internal25WasmInterpreterEntryFrame6scriptEv
	.quad	_ZNK2v88internal25WasmInterpreterEntryFrame7contextEv
	.quad	_ZNK2v88internal25WasmInterpreterEntryFrame8positionEv
	.quad	_ZNK2v88internal13StandardFrame12GetParameterEi
	.quad	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal13StandardFrame13IsConstructorEv
	.quad	_ZNK2v88internal25WasmInterpreterEntryFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.weak	_ZTVN2v88internal20WasmCompileLazyFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal20WasmCompileLazyFrameE,"awG",@progbits,_ZTVN2v88internal20WasmCompileLazyFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal20WasmCompileLazyFrameE, @object
	.size	_ZTVN2v88internal20WasmCompileLazyFrameE, 168
_ZTVN2v88internal20WasmCompileLazyFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal20WasmCompileLazyFrame4typeEv
	.quad	_ZNK2v88internal20WasmCompileLazyFrame14unchecked_codeEv
	.quad	_ZNK2v88internal20WasmCompileLazyFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	_ZN2v88internal20WasmCompileLazyFrameD1Ev
	.quad	_ZN2v88internal20WasmCompileLazyFrameD0Ev
	.quad	_ZNK2v88internal20WasmCompileLazyFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal13StandardFrame8receiverEv
	.quad	_ZNK2v88internal13StandardFrame6scriptEv
	.quad	_ZNK2v88internal13StandardFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal13StandardFrame12GetParameterEi
	.quad	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal13StandardFrame13IsConstructorEv
	.quad	_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.weak	_ZTVN2v88internal13InternalFrameE
	.section	.data.rel.ro.local._ZTVN2v88internal13InternalFrameE,"awG",@progbits,_ZTVN2v88internal13InternalFrameE,comdat
	.align 8
	.type	_ZTVN2v88internal13InternalFrameE, @object
	.size	_ZTVN2v88internal13InternalFrameE, 168
_ZTVN2v88internal13InternalFrameE:
	.quad	0
	.quad	0
	.quad	_ZNK2v88internal13StandardFrame11is_standardEv
	.quad	_ZNK2v88internal13InternalFrame4typeEv
	.quad	_ZNK2v88internal13InternalFrame14unchecked_codeEv
	.quad	_ZNK2v88internal13InternalFrame7IterateEPNS0_11RootVisitorE
	.quad	_ZNK2v88internal10StackFrame5PrintEPNS0_12StringStreamENS1_9PrintModeEi
	.quad	_ZN2v88internal13InternalFrameD1Ev
	.quad	_ZN2v88internal13InternalFrameD0Ev
	.quad	_ZNK2v88internal13InternalFrame21GetCallerStackPointerEv
	.quad	_ZNK2v88internal13StandardFrame18ComputeCallerStateEPNS0_10StackFrame5StateE
	.quad	_ZNK2v88internal10StackFrame14GetCallerStateEPNS1_5StateE
	.quad	_ZNK2v88internal13StandardFrame8receiverEv
	.quad	_ZNK2v88internal13StandardFrame6scriptEv
	.quad	_ZNK2v88internal13StandardFrame7contextEv
	.quad	_ZNK2v88internal13StandardFrame8positionEv
	.quad	_ZNK2v88internal13StandardFrame12GetParameterEi
	.quad	_ZNK2v88internal13StandardFrame22ComputeParametersCountEv
	.quad	_ZNK2v88internal13StandardFrame13IsConstructorEv
	.quad	_ZNK2v88internal13StandardFrame9SummarizeEPSt6vectorINS0_12FrameSummaryESaIS3_EE
	.quad	_ZNK2v88internal13StandardFrame20GetExpressionAddressEi
	.globl	_ZN2v88internal10StackFrame33return_address_location_resolver_E
	.section	.bss._ZN2v88internal10StackFrame33return_address_location_resolver_E,"aw",@nobits
	.align 8
	.type	_ZN2v88internal10StackFrame33return_address_location_resolver_E, @object
	.size	_ZN2v88internal10StackFrame33return_address_location_resolver_E, 8
_ZN2v88internal10StackFrame33return_address_location_resolver_E:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC38:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC39:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC40:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
