	.file	"builtins-sharedarraybuffer.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.section	.rodata._ZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"V8.Builtin_AtomicsIsLockFree"
	.section	.text._ZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateE:
.LFB20834:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L56
.L16:
	movq	_ZZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateEE27trace_event_unique_atomic45(%rip), %rbx
	testq	%rbx, %rbx
	je	.L57
.L18:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L58
.L20:
	addl	$1, 41104(%r12)
	leaq	-8(%r14), %rsi
	leaq	88(%r12), %rax
	cmpl	$5, %r15d
	cmovle	%rax, %rsi
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r13
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L59
.L27:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L33:
	movsd	.LC2(%rip), %xmm1
	movl	$0, %edx
	movq	%r12, %rdi
	ucomisd	%xmm1, %xmm0
	setnp	%sil
	cmovne	%edx, %esi
	ucomisd	.LC3(%rip), %xmm0
	setnp	%al
	cmovne	%edx, %eax
	orl	%eax, %esi
	ucomisd	.LC4(%rip), %xmm0
	setnp	%al
	cmovne	%edx, %eax
	orl	%eax, %esi
	movzbl	%sil, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r14
.L31:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L36
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L36:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L60
.L15:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L28
	xorl	%edx, %edx
.L29:
	testb	%dl, %dl
	jne	.L27
	movsd	7(%rax), %xmm0
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L58:
	pxor	%xmm0, %xmm0
	xorl	%r13d, %r13d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L62
.L21:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r13, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L57:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L63
.L19:
	movq	%rbx, _ZZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateEE27trace_event_unique_atomic45(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L30
	movq	312(%r12), %r14
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L56:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$833, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L62:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r13
	addq	$64, %rsp
	jmp	.L21
.L61:
	call	__stack_chk_fail@PLT
.L30:
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L29
	.cfi_endproc
.LFE20834:
	.size	_ZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal25Builtin_AtomicsIsLockFreeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal25Builtin_AtomicsIsLockFreeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal25Builtin_AtomicsIsLockFreeEiPmPNS0_7IsolateE, @function
_ZN2v88internal25Builtin_AtomicsIsLockFreeEiPmPNS0_7IsolateE:
.LFB20835:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L79
	addl	$1, 41104(%rdx)
	leaq	88(%rdx), %rax
	subq	$8, %rsi
	cmpl	$5, %edi
	cmovle	%rax, %rsi
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r13
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L80
.L69:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L75:
	movsd	.LC2(%rip), %xmm1
	movl	$0, %edx
	movq	%r12, %rdi
	ucomisd	%xmm1, %xmm0
	setnp	%sil
	cmovne	%edx, %esi
	ucomisd	.LC3(%rip), %xmm0
	setnp	%al
	cmovne	%edx, %eax
	orl	%eax, %esi
	ucomisd	.LC4(%rip), %xmm0
	setnp	%al
	cmovne	%edx, %eax
	orl	%eax, %esi
	movzbl	%sil, %esi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r14
.L73:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r13
	je	.L64
	movq	%r13, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L64:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L70
	xorl	%edx, %edx
.L71:
	testb	%dl, %dl
	jne	.L69
	movsd	7(%rax), %xmm0
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	jne	.L72
	movq	312(%r12), %r14
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L79:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateE
.L72:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L71
	.cfi_endproc
.LFE20835:
	.size	_ZN2v88internal25Builtin_AtomicsIsLockFreeEiPmPNS0_7IsolateE, .-_ZN2v88internal25Builtin_AtomicsIsLockFreeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31ValidateSharedIntegerTypedArrayEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEb,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31ValidateSharedIntegerTypedArrayEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEb
	.type	_ZN2v88internal31ValidateSharedIntegerTypedArrayEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEb, @function
_ZN2v88internal31ValidateSharedIntegerTypedArrayEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEb:
.LFB20837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L110
.L83:
	xorl	%esi, %esi
	testb	%dl, %dl
	setne	%sil
	addl	$103, %esi
.L87:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	xorl	%eax, %eax
.L88:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L111
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$1086, 11(%rcx)
	jne	.L83
	leaq	-48(%rbp), %r14
	movl	%edx, -68(%rbp)
	movq	%r14, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal12JSTypedArray9GetBufferEv@PLT
	movq	(%rax), %rax
	movl	39(%rax), %eax
	movl	-68(%rbp), %edx
	testb	$8, %al
	je	.L83
	movq	(%r12), %rax
	testb	%dl, %dl
	jne	.L112
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	cmpl	$7, %eax
	je	.L90
	movq	(%r12), %rax
	leaq	-56(%rbp), %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	cmpl	$8, %eax
	je	.L90
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	cmpl	$9, %eax
	je	.L90
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%r12, %rax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	-56(%rbp), %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	cmpl	$5, %eax
	je	.L109
	movq	(%r12), %rax
	movq	%r14, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	movl	$104, %esi
	cmpl	$10, %eax
	jne	.L87
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L90:
	movl	$103, %esi
	jmp	.L87
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20837:
	.size	_ZN2v88internal31ValidateSharedIntegerTypedArrayEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEb, .-_ZN2v88internal31ValidateSharedIntegerTypedArrayEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEb
	.section	.text._ZN2v88internal20ValidateAtomicAccessEPNS0_7IsolateENS0_6HandleINS0_12JSTypedArrayEEENS3_INS0_6ObjectEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal20ValidateAtomicAccessEPNS0_7IsolateENS0_6HandleINS0_12JSTypedArrayEEENS3_INS0_6ObjectEEE
	.type	_ZN2v88internal20ValidateAtomicAccessEPNS0_7IsolateENS0_6HandleINS0_12JSTypedArrayEEENS3_INS0_6ObjectEEE, @function
_ZN2v88internal20ValidateAtomicAccessEPNS0_7IsolateENS0_6HandleINS0_12JSTypedArrayEEENS3_INS0_6ObjectEEE:
.LFB20838:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	movq	(%rdx), %rdx
	testb	$1, %dl
	jne	.L114
	sarq	$32, %rdx
	js	.L114
.L120:
	movq	(%rbx), %rax
	movq	23(%rax), %rcx
	movl	39(%rcx), %ecx
	andl	$4, %ecx
	jne	.L119
	cmpq	%rdx, 47(%rax)
	ja	.L132
	.p2align 4,,10
	.p2align 3
.L119:
	xorl	%edx, %edx
	movl	$190, %esi
	movq	%r12, %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	_ZN2v88internal7Factory13NewRangeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L131:
	popq	%rbx
	xorl	%eax, %eax
	xorl	%edx, %edx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movl	$190, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal6Object14ConvertToIndexEPNS0_7IsolateENS0_6HandleIS1_EENS0_15MessageTemplateE@PLT
	testq	%rax, %rax
	je	.L131
	movq	(%rax), %rdx
	testb	$1, %dl
	je	.L133
	movsd	7(%rdx), %xmm0
	comisd	.LC5(%rip), %xmm0
	jb	.L119
	movsd	.LC6(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L119
	movsd	.LC7(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L123
	cvttsd2siq	%xmm0, %rdx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L132:
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	sarq	$32, %rdx
	js	.L119
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L123:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rdx
	btcq	$63, %rdx
	jmp	.L120
	.cfi_endproc
.LFE20838:
	.size	_ZN2v88internal20ValidateAtomicAccessEPNS0_7IsolateENS0_6HandleINS0_12JSTypedArrayEEENS3_INS0_6ObjectEEE, .-_ZN2v88internal20ValidateAtomicAccessEPNS0_7IsolateENS0_6HandleINS0_12JSTypedArrayEEENS3_INS0_6ObjectEEE
	.section	.text._ZN2v88internal12_GLOBAL__N_111AtomicsWakeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_111AtomicsWakeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_, @function
_ZN2v88internal12_GLOBAL__N_111AtomicsWakeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_:
.LFB20841:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	$1, %edx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal31ValidateSharedIntegerTypedArrayEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEb
	testq	%rax, %rax
	je	.L141
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN2v88internal20ValidateAtomicAccessEPNS0_7IsolateENS0_6HandleINS0_12JSTypedArrayEEENS3_INS0_6ObjectEEE
	movq	%rdx, %r13
	testb	%al, %al
	jne	.L137
.L141:
	xorl	%eax, %eax
.L136:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L165
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	(%r14), %rax
	cmpq	%rax, 88(%rbx)
	je	.L153
	testb	$1, %al
	jne	.L166
.L140:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L143:
	pxor	%xmm1, %xmm1
	xorl	%edx, %edx
	comisd	%xmm0, %xmm1
	ja	.L138
	comisd	.LC8(%rip), %xmm0
	movl	$-1, %edx
	ja	.L138
	cvttsd2siq	%xmm0, %rdx
	.p2align 4,,10
	.p2align 3
.L138:
	movq	(%r12), %rax
	leaq	-64(%rbp), %r15
	movl	%edx, -72(%rbp)
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12JSTypedArray9GetBufferEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	movq	(%r12), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	movl	-72(%rbp), %edx
	cmpl	$10, %eax
	movq	(%r12), %rax
	movq	31(%rax), %rax
	leaq	(%rax,%r13,8), %rsi
	je	.L164
	leaq	(%rax,%r13,4), %rsi
.L164:
	movq	%r14, %rdi
	call	_ZN2v88internal14FutexEmulation4WakeENS0_6HandleINS0_13JSArrayBufferEEEmj@PLT
	movq	41112(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L149
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	testq	%rax, %rax
	je	.L141
	movq	(%rax), %rax
	testb	$1, %al
	je	.L140
	movsd	7(%rax), %xmm0
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L149:
	movq	41088(%rbx), %rax
	cmpq	41096(%rbx), %rax
	je	.L167
.L151:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%rbx)
	movq	%rsi, (%rax)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L153:
	movl	$-1, %edx
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L167:
	movq	%rbx, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-72(%rbp), %rsi
	jmp	.L151
.L165:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20841:
	.size	_ZN2v88internal12_GLOBAL__N_111AtomicsWakeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_, .-_ZN2v88internal12_GLOBAL__N_111AtomicsWakeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_
	.section	.rodata._ZN2v88internalL30Builtin_Impl_Stats_AtomicsWakeEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"V8.Builtin_AtomicsWake"
	.section	.text._ZN2v88internalL30Builtin_Impl_Stats_AtomicsWakeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Builtin_Impl_Stats_AtomicsWakeEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Builtin_Impl_Stats_AtomicsWakeEiPmPNS0_7IsolateE:
.LFB20842:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L207
.L169:
	movq	_ZZN2v88internalL30Builtin_Impl_Stats_AtomicsWakeEiPmPNS0_7IsolateEE28trace_event_unique_atomic160(%rip), %rbx
	testq	%rbx, %rbx
	je	.L208
.L171:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L209
.L173:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r15
	addl	$1, 41104(%r12)
	cmpl	$5, %r14d
	jg	.L177
	leaq	88(%r12), %r13
	movq	%r13, %r8
.L178:
	movq	%r13, %rdx
.L182:
	movl	$53, %esi
	movq	%r12, %rdi
	movq	%rdx, -176(%rbp)
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	-168(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-176(%rbp), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal12_GLOBAL__N_111AtomicsWakeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_
	testq	%rax, %rax
	je	.L210
	movq	(%rax), %r13
.L184:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L187
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L187:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L211
.L168:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L212
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	leaq	-8(%r13), %r8
	cmpl	$6, %r14d
	je	.L213
	leaq	-16(%r13), %rdx
	cmpl	$7, %r14d
	je	.L214
	subq	$24, %r13
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L209:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L215
.L174:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L175
	movq	(%rdi), %rax
	call	*8(%rax)
.L175:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L176
	movq	(%rdi), %rax
	call	*8(%rax)
.L176:
	leaq	.LC9(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L208:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L216
.L172:
	movq	%rbx, _ZZN2v88internalL30Builtin_Impl_Stats_AtomicsWakeEiPmPNS0_7IsolateEE28trace_event_unique_atomic160(%rip)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L210:
	movq	312(%r12), %r13
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L211:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L207:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$835, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L216:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L215:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC9(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L174
.L212:
	call	__stack_chk_fail@PLT
.L214:
	leaq	88(%r12), %r13
	jmp	.L182
.L213:
	leaq	88(%r12), %r13
	jmp	.L178
	.cfi_endproc
.LFE20842:
	.size	_ZN2v88internalL30Builtin_Impl_Stats_AtomicsWakeEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Builtin_Impl_Stats_AtomicsWakeEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL32Builtin_Impl_Stats_AtomicsNotifyEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"V8.Builtin_AtomicsNotify"
	.section	.text._ZN2v88internalL32Builtin_Impl_Stats_AtomicsNotifyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL32Builtin_Impl_Stats_AtomicsNotifyEiPmPNS0_7IsolateE, @function
_ZN2v88internalL32Builtin_Impl_Stats_AtomicsNotifyEiPmPNS0_7IsolateE:
.LFB20845:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L256
.L218:
	movq	_ZZN2v88internalL32Builtin_Impl_Stats_AtomicsNotifyEiPmPNS0_7IsolateEE28trace_event_unique_atomic172(%rip), %rbx
	testq	%rbx, %rbx
	je	.L257
.L220:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L258
.L222:
	movq	41088(%r12), %rbx
	movq	41096(%r12), %r15
	addl	$1, 41104(%r12)
	cmpl	$5, %r14d
	jg	.L226
	leaq	88(%r12), %r13
	movq	%r13, %r8
.L227:
	movq	%r13, %rdx
.L231:
	movl	$52, %esi
	movq	%r12, %rdi
	movq	%rdx, -176(%rbp)
	movq	%r8, -168(%rbp)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	-168(%rbp), %r8
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	-176(%rbp), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal12_GLOBAL__N_111AtomicsWakeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_
	testq	%rax, %rax
	je	.L259
	movq	(%rax), %r13
.L233:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r15
	je	.L236
	movq	%r15, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L236:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L260
.L217:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L261
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	leaq	-8(%r13), %r8
	cmpl	$6, %r14d
	je	.L262
	leaq	-16(%r13), %rdx
	cmpl	$7, %r14d
	je	.L263
	subq	$24, %r13
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L258:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L264
.L223:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L224
	movq	(%rdi), %rax
	call	*8(%rax)
.L224:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L225
	movq	(%rdi), %rax
	call	*8(%rax)
.L225:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L257:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L265
.L221:
	movq	%rbx, _ZZN2v88internalL32Builtin_Impl_Stats_AtomicsNotifyEiPmPNS0_7IsolateEE28trace_event_unique_atomic172(%rip)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L259:
	movq	312(%r12), %r13
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L260:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L256:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$832, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L265:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L264:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L223
.L261:
	call	__stack_chk_fail@PLT
.L263:
	leaq	88(%r12), %r13
	jmp	.L231
.L262:
	leaq	88(%r12), %r13
	jmp	.L227
	.cfi_endproc
.LFE20845:
	.size	_ZN2v88internalL32Builtin_Impl_Stats_AtomicsNotifyEiPmPNS0_7IsolateE, .-_ZN2v88internalL32Builtin_Impl_Stats_AtomicsNotifyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internalL24Builtin_Impl_AtomicsWaitENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL24Builtin_Impl_AtomicsWaitENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL24Builtin_Impl_AtomicsWaitENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB20850:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -72(%rbp)
	cmpl	$5, %edi
	jg	.L267
	leaq	88(%rdx), %r13
	movq	%r13, %r8
.L268:
	movq	%r13, %r15
.L270:
	movq	%r13, -80(%rbp)
.L274:
	movl	$1, %edx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal31ValidateSharedIntegerTypedArrayEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEEb
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L320
	movq	%r15, %rdx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal20ValidateAtomicAccessEPNS0_7IsolateENS0_6HandleINS0_12JSTypedArrayEEENS3_INS0_6ObjectEEE
	movq	%rdx, -88(%rbp)
	testb	%al, %al
	jne	.L277
.L320:
	movq	312(%r14), %r12
.L276:
	movq	-72(%rbp), %rax
	subl	$1, 41104(%r14)
	movq	%rax, 41088(%r14)
	cmpq	41096(%r14), %rbx
	je	.L306
	movq	%rbx, 41096(%r14)
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L306:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L324
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	leaq	-8(%rsi), %r8
	cmpl	$6, %edi
	je	.L325
	leaq	-16(%rsi), %r15
	cmpl	$7, %edi
	je	.L326
	leaq	-24(%rsi), %r13
	cmpl	$8, %edi
	je	.L327
	leaq	-32(%rsi), %rax
	movq	%rax, -80(%rbp)
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L277:
	movq	(%r12), %rax
	leaq	-64(%rbp), %r15
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	cmpl	$10, %eax
	je	.L328
	testb	$1, 0(%r13)
	jne	.L329
.L280:
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, 88(%r14)
	jne	.L282
.L322:
	movq	1104(%r14), %rax
	testb	$1, %al
	jne	.L293
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L284:
	cmpb	$0, 45680(%r14)
	jne	.L295
.L331:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$17, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r12
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L282:
	testb	$1, %al
	jne	.L330
.L286:
	sarq	$32, %rax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
.L291:
	ucomisd	%xmm0, %xmm0
	jp	.L322
	pxor	%xmm1, %xmm1
	cmpb	$0, 45680(%r14)
	maxsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	je	.L331
.L295:
	movq	(%r12), %rax
	movq	%r15, %rdi
	movsd	%xmm0, -96(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12JSTypedArray9GetBufferEv@PLT
	movq	%r15, %rdi
	movq	%rax, -80(%rbp)
	movq	(%r12), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	movq	-80(%rbp), %r8
	movsd	-96(%rbp), %xmm0
	cmpl	$10, %eax
	je	.L332
	movq	0(%r13), %rcx
	testb	$1, %cl
	jne	.L297
	shrq	$32, %rcx
.L298:
	movq	(%r12), %rax
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	movq	31(%rax), %rax
	leaq	(%rax,%rsi,4), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal14FutexEmulation8WaitJs32EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmid@PLT
	movq	%rax, %r12
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L329:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6Object14ConvertToInt32EPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L320
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L328:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L320
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	cmpq	%rax, 88(%r14)
	je	.L322
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L293:
	movsd	7(%rax), %xmm0
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L330:
	movq	-1(%rax), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L287
	xorl	%edx, %edx
.L288:
	testb	%dl, %dl
	jne	.L286
	movsd	7(%rax), %xmm0
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L297:
	movq	7(%rcx), %rdx
	movsd	.LC12(%rip), %xmm3
	movq	%rdx, %xmm2
	andpd	.LC11(%rip), %xmm2
	movq	%rdx, %xmm1
	ucomisd	%xmm2, %xmm3
	jb	.L299
	movsd	.LC13(%rip), %xmm2
	comisd	%xmm1, %xmm2
	jb	.L299
	comisd	.LC14(%rip), %xmm1
	jb	.L299
	cvttsd2sil	%xmm1, %ecx
	pxor	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	ucomisd	%xmm2, %xmm1
	jp	.L299
	je	.L298
	.p2align 4,,10
	.p2align 3
.L299:
	movabsq	$9218868437227405312, %rax
	testq	%rax, %rdx
	je	.L308
	movq	%rdx, %rsi
	xorl	%ecx, %ecx
	shrq	$52, %rsi
	andl	$2047, %esi
	movl	%esi, %edi
	subl	$1075, %edi
	js	.L333
	cmpl	$31, %edi
	jg	.L298
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rcx
	andq	%rdx, %rax
	addq	%rcx, %rax
	movl	%edi, %ecx
	salq	%cl, %rax
	movl	%eax, %ecx
.L304:
	sarq	$63, %rdx
	orl	$1, %edx
	imull	%edx, %ecx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L333:
	cmpl	$-52, %edi
	jl	.L298
	movabsq	$4503599627370495, %rcx
	movabsq	$4503599627370496, %rax
	andq	%rdx, %rcx
	addq	%rcx, %rax
	movl	$1075, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rax
	movq	%rax, %rcx
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L332:
	movq	0(%r13), %rax
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r8, -96(%rbp)
	movsd	%xmm0, -80(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal6BigInt7AsInt64EPb@PLT
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %r8
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	(%r12), %rax
	movsd	-80(%rbp), %xmm0
	movq	31(%rax), %rax
	leaq	(%rax,%rsi,8), %rdx
	movq	%r8, %rsi
	call	_ZN2v88internal14FutexEmulation8WaitJs64EPNS0_7IsolateENS0_6HandleINS0_13JSArrayBufferEEEmld@PLT
	movq	%rax, %r12
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L287:
	movq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	testq	%rax, %rax
	je	.L320
	movq	(%rax), %rax
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	jmp	.L288
.L308:
	xorl	%ecx, %ecx
	jmp	.L298
.L324:
	call	__stack_chk_fail@PLT
.L325:
	leaq	88(%rdx), %r13
	jmp	.L268
.L327:
	leaq	88(%rdx), %rax
	movq	%rax, -80(%rbp)
	jmp	.L274
.L326:
	leaq	88(%rdx), %r13
	jmp	.L270
	.cfi_endproc
.LFE20850:
	.size	_ZN2v88internalL24Builtin_Impl_AtomicsWaitENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL24Builtin_Impl_AtomicsWaitENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL30Builtin_Impl_Stats_AtomicsWaitEiPmPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC15:
	.string	"V8.Builtin_AtomicsWait"
	.section	.text._ZN2v88internalL30Builtin_Impl_Stats_AtomicsWaitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL30Builtin_Impl_Stats_AtomicsWaitEiPmPNS0_7IsolateE, @function
_ZN2v88internalL30Builtin_Impl_Stats_AtomicsWaitEiPmPNS0_7IsolateE:
.LFB20848:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L363
.L335:
	movq	_ZZN2v88internalL30Builtin_Impl_Stats_AtomicsWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic184(%rip), %rbx
	testq	%rbx, %rbx
	je	.L364
.L337:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L365
.L339:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL24Builtin_Impl_AtomicsWaitENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L366
.L343:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L367
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L368
.L338:
	movq	%rbx, _ZZN2v88internalL30Builtin_Impl_Stats_AtomicsWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic184(%rip)
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L365:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L369
.L340:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L341
	movq	(%rdi), %rax
	call	*8(%rax)
.L341:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L342
	movq	(%rdi), %rax
	call	*8(%rax)
.L342:
	leaq	.LC15(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L366:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L363:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$834, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L369:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC15(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L368:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L338
.L367:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE20848:
	.size	_ZN2v88internalL30Builtin_Impl_Stats_AtomicsWaitEiPmPNS0_7IsolateE, .-_ZN2v88internalL30Builtin_Impl_Stats_AtomicsWaitEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Builtin_AtomicsWakeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Builtin_AtomicsWakeEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Builtin_AtomicsWakeEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Builtin_AtomicsWakeEiPmPNS0_7IsolateE:
.LFB20843:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L383
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	cmpl	$5, %edi
	jle	.L384
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L385
	leaq	-16(%rsi), %rdx
	cmpl	$7, %edi
	je	.L386
	leaq	-24(%rsi), %r13
.L377:
	movl	$53, %esi
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	-56(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111AtomicsWakeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_
	testq	%rax, %rax
	je	.L387
	movq	(%rax), %r13
.L379:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L370
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L370:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	leaq	88(%rdx), %r13
	movq	%r13, %r15
.L373:
	movq	%r13, %rdx
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L387:
	movq	312(%r12), %r13
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L383:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL30Builtin_Impl_Stats_AtomicsWakeEiPmPNS0_7IsolateE
.L386:
	.cfi_restore_state
	leaq	88(%r12), %r13
	jmp	.L377
.L385:
	leaq	88(%rdx), %r13
	jmp	.L373
	.cfi_endproc
.LFE20843:
	.size	_ZN2v88internal19Builtin_AtomicsWakeEiPmPNS0_7IsolateE, .-_ZN2v88internal19Builtin_AtomicsWakeEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal21Builtin_AtomicsNotifyEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal21Builtin_AtomicsNotifyEiPmPNS0_7IsolateE
	.type	_ZN2v88internal21Builtin_AtomicsNotifyEiPmPNS0_7IsolateE, @function
_ZN2v88internal21Builtin_AtomicsNotifyEiPmPNS0_7IsolateE:
.LFB20846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L401
	addl	$1, 41104(%rdx)
	movq	41088(%rdx), %rbx
	movq	41096(%rdx), %r14
	cmpl	$5, %edi
	jle	.L402
	leaq	-8(%rsi), %r15
	cmpl	$6, %edi
	je	.L403
	leaq	-16(%rsi), %rdx
	cmpl	$7, %edi
	je	.L404
	leaq	-24(%rsi), %r13
.L395:
	movl	$52, %esi
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v88internal7Isolate10CountUsageENS_7Isolate17UseCounterFeatureE@PLT
	movq	-56(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal12_GLOBAL__N_111AtomicsWakeEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEES6_S6_
	testq	%rax, %rax
	je	.L405
	movq	(%rax), %r13
.L397:
	subl	$1, 41104(%r12)
	movq	%rbx, 41088(%r12)
	cmpq	41096(%r12), %r14
	je	.L388
	movq	%r14, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L388:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	leaq	88(%rdx), %r13
	movq	%r13, %r15
.L391:
	movq	%r13, %rdx
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L405:
	movq	312(%r12), %r13
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L401:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internalL32Builtin_Impl_Stats_AtomicsNotifyEiPmPNS0_7IsolateE
.L404:
	.cfi_restore_state
	leaq	88(%r12), %r13
	jmp	.L395
.L403:
	leaq	88(%rdx), %r13
	jmp	.L391
	.cfi_endproc
.LFE20846:
	.size	_ZN2v88internal21Builtin_AtomicsNotifyEiPmPNS0_7IsolateE, .-_ZN2v88internal21Builtin_AtomicsNotifyEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal19Builtin_AtomicsWaitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19Builtin_AtomicsWaitEiPmPNS0_7IsolateE
	.type	_ZN2v88internal19Builtin_AtomicsWaitEiPmPNS0_7IsolateE, @function
_ZN2v88internal19Builtin_AtomicsWaitEiPmPNS0_7IsolateE:
.LFB20849:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L410
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL24Builtin_Impl_AtomicsWaitENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	.cfi_restore 6
	jmp	_ZN2v88internalL30Builtin_Impl_Stats_AtomicsWaitEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE20849:
	.size	_ZN2v88internal19Builtin_AtomicsWaitEiPmPNS0_7IsolateE, .-_ZN2v88internal19Builtin_AtomicsWaitEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal25Builtin_AtomicsIsLockFreeEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal25Builtin_AtomicsIsLockFreeEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal25Builtin_AtomicsIsLockFreeEiPmPNS0_7IsolateE:
.LFB25398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE25398:
	.size	_GLOBAL__sub_I__ZN2v88internal25Builtin_AtomicsIsLockFreeEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal25Builtin_AtomicsIsLockFreeEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal25Builtin_AtomicsIsLockFreeEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL30Builtin_Impl_Stats_AtomicsWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic184,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Builtin_Impl_Stats_AtomicsWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic184, @object
	.size	_ZZN2v88internalL30Builtin_Impl_Stats_AtomicsWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic184, 8
_ZZN2v88internalL30Builtin_Impl_Stats_AtomicsWaitEiPmPNS0_7IsolateEE28trace_event_unique_atomic184:
	.zero	8
	.section	.bss._ZZN2v88internalL32Builtin_Impl_Stats_AtomicsNotifyEiPmPNS0_7IsolateEE28trace_event_unique_atomic172,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL32Builtin_Impl_Stats_AtomicsNotifyEiPmPNS0_7IsolateEE28trace_event_unique_atomic172, @object
	.size	_ZZN2v88internalL32Builtin_Impl_Stats_AtomicsNotifyEiPmPNS0_7IsolateEE28trace_event_unique_atomic172, 8
_ZZN2v88internalL32Builtin_Impl_Stats_AtomicsNotifyEiPmPNS0_7IsolateEE28trace_event_unique_atomic172:
	.zero	8
	.section	.bss._ZZN2v88internalL30Builtin_Impl_Stats_AtomicsWakeEiPmPNS0_7IsolateEE28trace_event_unique_atomic160,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL30Builtin_Impl_Stats_AtomicsWakeEiPmPNS0_7IsolateEE28trace_event_unique_atomic160, @object
	.size	_ZZN2v88internalL30Builtin_Impl_Stats_AtomicsWakeEiPmPNS0_7IsolateEE28trace_event_unique_atomic160, 8
_ZZN2v88internalL30Builtin_Impl_Stats_AtomicsWakeEiPmPNS0_7IsolateEE28trace_event_unique_atomic160:
	.zero	8
	.section	.bss._ZZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateEE27trace_event_unique_atomic45,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateEE27trace_event_unique_atomic45, @object
	.size	_ZZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateEE27trace_event_unique_atomic45, 8
_ZZN2v88internalL36Builtin_Impl_Stats_AtomicsIsLockFreeEiPmPNS0_7IsolateEE27trace_event_unique_atomic45:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	1072693248
	.align 8
.LC3:
	.long	0
	.long	1073741824
	.align 8
.LC4:
	.long	0
	.long	1074790400
	.align 8
.LC5:
	.long	0
	.long	0
	.align 8
.LC6:
	.long	0
	.long	1139802112
	.align 8
.LC7:
	.long	0
	.long	1138753536
	.align 8
.LC8:
	.long	4292870144
	.long	1106247679
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC11:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC12:
	.long	4294967295
	.long	2146435071
	.align 8
.LC13:
	.long	4290772992
	.long	1105199103
	.align 8
.LC14:
	.long	0
	.long	-1042284544
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
