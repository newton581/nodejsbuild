	.file	"machine-type.cc"
	.text
	.section	.text._ZN2v88internal9IsSubtypeENS0_21MachineRepresentationES1_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal9IsSubtypeENS0_21MachineRepresentationES1_
	.type	_ZN2v88internal9IsSubtypeENS0_21MachineRepresentationES1_, @function
_ZN2v88internal9IsSubtypeENS0_21MachineRepresentationES1_:
.LFB3274:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	cmpb	%sil, %dil
	je	.L1
	cmpb	$7, %dil
	jbe	.L10
	subl	$9, %edi
	xorl	%eax, %eax
	cmpb	$1, %dil
	ja	.L11
	cmpb	$11, %sil
	sete	%al
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	cmpb	$5, %dil
	ja	.L4
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	cmpb	$8, %sil
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE3274:
	.size	_ZN2v88internal9IsSubtypeENS0_21MachineRepresentationES1_, .-_ZN2v88internal9IsSubtypeENS0_21MachineRepresentationES1_
	.section	.rodata._ZN2v88internallsERSoNS0_21MachineRepresentationE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"kRepCompressedSigned"
.LC1:
	.string	"kRepCompressedPointer"
.LC2:
	.string	"kRepCompressed"
.LC3:
	.string	"kRepBit"
.LC4:
	.string	"kMachNone"
.LC5:
	.string	"kRepWord16"
.LC6:
	.string	"kRepWord32"
.LC7:
	.string	"kRepWord64"
.LC8:
	.string	"kRepFloat32"
.LC9:
	.string	"kRepFloat64"
.LC10:
	.string	"kRepSimd128"
.LC11:
	.string	"kRepTaggedSigned"
.LC12:
	.string	"kRepTaggedPointer"
.LC13:
	.string	"kRepTagged"
.LC14:
	.string	"kRepWord8"
.LC15:
	.string	"unreachable code"
	.section	.text._ZN2v88internallsERSoNS0_21MachineRepresentationE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_21MachineRepresentationE
	.type	_ZN2v88internallsERSoNS0_21MachineRepresentationE, @function
_ZN2v88internallsERSoNS0_21MachineRepresentationE:
.LFB3275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpb	$14, %sil
	ja	.L13
	leaq	.L15(%rip), %rcx
	movzbl	%sil, %edx
	movq	%rdi, %r12
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internallsERSoNS0_21MachineRepresentationE,"a",@progbits
	.align 4
	.align 4
.L15:
	.long	.L29-.L15
	.long	.L28-.L15
	.long	.L27-.L15
	.long	.L30-.L15
	.long	.L25-.L15
	.long	.L24-.L15
	.long	.L23-.L15
	.long	.L22-.L15
	.long	.L21-.L15
	.long	.L20-.L15
	.long	.L19-.L15
	.long	.L18-.L15
	.long	.L17-.L15
	.long	.L16-.L15
	.long	.L14-.L15
	.section	.text._ZN2v88internallsERSoNS0_21MachineRepresentationE
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$10, %edx
	leaq	.LC5(%rip), %rsi
.L26:
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movl	$9, %edx
	leaq	.LC14(%rip), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$10, %edx
	leaq	.LC6(%rip), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$10, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$16, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$17, %edx
	leaq	.LC12(%rip), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$10, %edx
	leaq	.LC13(%rip), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$20, %edx
	leaq	.LC0(%rip), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$7, %edx
	leaq	.LC3(%rip), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$11, %edx
	leaq	.LC10(%rip), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$9, %edx
	leaq	.LC4(%rip), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$21, %edx
	leaq	.LC1(%rip), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$14, %edx
	leaq	.LC2(%rip), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$11, %edx
	leaq	.LC8(%rip), %rsi
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$11, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L26
.L13:
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3275:
	.size	_ZN2v88internallsERSoNS0_21MachineRepresentationE, .-_ZN2v88internallsERSoNS0_21MachineRepresentationE
	.section	.text._ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE
	.type	_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE, @function
_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE:
.LFB3276:
	.cfi_startproc
	endbr64
	cmpb	$14, %dil
	ja	.L34
	leaq	.L36(%rip), %rcx
	movzbl	%dil, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE,"a",@progbits
	.align 4
	.align 4
.L36:
	.long	.L50-.L36
	.long	.L49-.L36
	.long	.L51-.L36
	.long	.L47-.L36
	.long	.L46-.L36
	.long	.L45-.L36
	.long	.L44-.L36
	.long	.L43-.L36
	.long	.L42-.L36
	.long	.L41-.L36
	.long	.L40-.L36
	.long	.L39-.L36
	.long	.L38-.L36
	.long	.L37-.L36
	.long	.L35-.L36
	.section	.text._ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	.LC14(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	.LC5(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	.LC6(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	.LC7(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	leaq	.LC12(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	.LC13(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	.LC4(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	.LC9(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	.LC0(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	.LC1(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	.LC8(%rip), %rax
	ret
.L34:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3276:
	.size	_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE, .-_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE
	.section	.rodata._ZN2v88internallsERSoNS0_15MachineSemanticE.str1.1,"aMS",@progbits,1
.LC16:
	.string	"kTypeBool"
.LC17:
	.string	"kTypeInt32"
.LC18:
	.string	"kTypeUint32"
.LC19:
	.string	"kTypeInt64"
.LC20:
	.string	"kTypeUint64"
.LC21:
	.string	"kTypeNumber"
.LC22:
	.string	"kTypeAny"
	.section	.text._ZN2v88internallsERSoNS0_15MachineSemanticE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_15MachineSemanticE
	.type	_ZN2v88internallsERSoNS0_15MachineSemanticE, @function
_ZN2v88internallsERSoNS0_15MachineSemanticE:
.LFB3277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpb	$7, %sil
	ja	.L55
	leaq	.L57(%rip), %rcx
	movzbl	%sil, %edx
	movq	%rdi, %r12
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internallsERSoNS0_15MachineSemanticE,"a",@progbits
	.align 4
	.align 4
.L57:
	.long	.L64-.L57
	.long	.L63-.L57
	.long	.L62-.L57
	.long	.L61-.L57
	.long	.L60-.L57
	.long	.L59-.L57
	.long	.L58-.L57
	.long	.L56-.L57
	.section	.text._ZN2v88internallsERSoNS0_15MachineSemanticE
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$11, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	movl	$8, %edx
	leaq	.LC22(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
	movl	$9, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	$9, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movl	$10, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	movl	$11, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	movl	$10, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movl	$11, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE3277:
	.size	_ZN2v88internallsERSoNS0_15MachineSemanticE, .-_ZN2v88internallsERSoNS0_15MachineSemanticE
	.section	.rodata._ZN2v88internallsERSoNS0_11MachineTypeE.str1.1,"aMS",@progbits,1
.LC23:
	.string	"|"
	.section	.text._ZN2v88internallsERSoNS0_11MachineTypeE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internallsERSoNS0_11MachineTypeE
	.type	_ZN2v88internallsERSoNS0_11MachineTypeE, @function
_ZN2v88internallsERSoNS0_11MachineTypeE:
.LFB3278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movzbl	%ah, %ebx
	subq	$8, %rsp
	testb	%sil, %sil
	je	.L78
	movl	%esi, %edi
	testb	%bl, %bl
	je	.L79
	call	_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L80
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L73:
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L77:
	addq	$8, %rsp
	movl	%ebx, %esi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internallsERSoNS0_15MachineSemanticE
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	call	_ZN2v88internal19MachineReprToStringENS0_21MachineRepresentationE
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L81
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L67:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	testb	%bl, %bl
	jne	.L77
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L80:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%r12), %rax
	movq	-24(%rax), %rdi
	addq	%r12, %rdi
	movl	32(%rdi), %esi
	orl	$1, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L67
	.cfi_endproc
.LFE3278:
	.size	_ZN2v88internallsERSoNS0_11MachineTypeE, .-_ZN2v88internallsERSoNS0_11MachineTypeE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
