	.file	"v8-value-utils.cc"
	.text
	.section	.text._ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_6ObjectEEENS1_INS0_4NameEEENS1_INS0_5ValueEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_6ObjectEEENS1_INS0_4NameEEENS1_INS0_5ValueEEE
	.type	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_6ObjectEEENS1_INS0_4NameEEENS1_INS0_5ValueEEE, @function
_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_6ObjectEEENS1_INS0_4NameEEENS1_INS0_5ValueEEE:
.LFB4430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-112(%rbp), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	leaq	-128(%rbp), %r8
	movl	$1, %edx
	movq	%r8, %rdi
	movq	%rax, %rsi
	movq	%r8, -136(%rbp)
	call	_ZN2v87Isolate32DisallowJavascriptExecutionScopeC1EPS0_NS1_9OnFailureE@PLT
	movq	%r12, %rsi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v86Object18CreateDataPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEE@PLT
	movq	-136(%rbp), %r8
	movl	%eax, %r12d
	movq	%r8, %rdi
	call	_ZN2v87Isolate32DisallowJavascriptExecutionScopeD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L5:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4430:
	.size	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_6ObjectEEENS1_INS0_4NameEEENS1_INS0_5ValueEEE, .-_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_6ObjectEEENS1_INS0_4NameEEENS1_INS0_5ValueEEE
	.section	.text._ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE
	.type	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE, @function
_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE:
.LFB4431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-112(%rbp), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	leaq	-128(%rbp), %r8
	movl	$1, %edx
	movq	%r8, %rdi
	movq	%rax, %rsi
	movq	%r8, -136(%rbp)
	call	_ZN2v87Isolate32DisallowJavascriptExecutionScopeC1EPS0_NS1_9OnFailureE@PLT
	movq	%r12, %rsi
	movq	%r15, %rcx
	movl	%r14d, %edx
	movq	%r13, %rdi
	call	_ZN2v86Object18CreateDataPropertyENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	movq	-136(%rbp), %r8
	movl	%eax, %r12d
	movq	%r8, %rdi
	call	_ZN2v87Isolate32DisallowJavascriptExecutionScopeD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4431:
	.size	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE, .-_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
