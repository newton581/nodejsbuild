	.file	"embedder-data-array.cc"
	.text
	.section	.text._ZN2v88internal17EmbedderDataArray14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal17EmbedderDataArray14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi
	.type	_ZN2v88internal17EmbedderDataArray14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_ZN2v88internal17EmbedderDataArray14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB17789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rsi), %rax
	movq	%rsi, %rbx
	cmpl	%edx, 11(%rax)
	jle	.L2
	popq	%rbx
	movq	%rsi, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	leal	1(%rdx), %esi
	xorl	%edx, %edx
	call	_ZN2v88internal7Factory20NewEmbedderDataArrayEiNS0_14AllocationTypeE@PLT
	movq	(%rbx), %rsi
	movq	%rax, %r12
	movq	(%rax), %rax
	movslq	11(%rsi), %rdx
	addq	$15, %rsi
	leaq	15(%rax), %rdi
	sall	$3, %edx
	movslq	%edx, %rdx
	call	memcpy@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17789:
	.size	_ZN2v88internal17EmbedderDataArray14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_ZN2v88internal17EmbedderDataArray14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal17EmbedderDataArray14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal17EmbedderDataArray14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi, @function
_GLOBAL__sub_I__ZN2v88internal17EmbedderDataArray14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi:
.LFB21465:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE21465:
	.size	_GLOBAL__sub_I__ZN2v88internal17EmbedderDataArray14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi, .-_GLOBAL__sub_I__ZN2v88internal17EmbedderDataArray14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal17EmbedderDataArray14EnsureCapacityEPNS0_7IsolateENS0_6HandleIS1_EEi
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
