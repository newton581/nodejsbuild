	.file	"control-flow-builders.cc"
	.text
	.section	.text._ZN2v88internal11interpreter27BreakableControlFlowBuilder15BindBreakTargetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter27BreakableControlFlowBuilder15BindBreakTargetEv
	.type	_ZN2v88internal11interpreter27BreakableControlFlowBuilder15BindBreakTargetEv, @function
_ZN2v88internal11interpreter27BreakableControlFlowBuilder15BindBreakTargetEv:
.LFB19391:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rsi
	addq	$16, %rdi
	jmp	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	.cfi_endproc
.LFE19391:
	.size	_ZN2v88internal11interpreter27BreakableControlFlowBuilder15BindBreakTargetEv, .-_ZN2v88internal11interpreter27BreakableControlFlowBuilder15BindBreakTargetEv
	.section	.text._ZN2v88internal11interpreter27BreakableControlFlowBuilder8EmitJumpEPNS1_14BytecodeLabelsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter27BreakableControlFlowBuilder8EmitJumpEPNS1_14BytecodeLabelsE
	.type	_ZN2v88internal11interpreter27BreakableControlFlowBuilder8EmitJumpEPNS1_14BytecodeLabelsE, @function
_ZN2v88internal11interpreter27BreakableControlFlowBuilder8EmitJumpEPNS1_14BytecodeLabelsE:
.LFB19392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%r8), %r12
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	.cfi_endproc
.LFE19392:
	.size	_ZN2v88internal11interpreter27BreakableControlFlowBuilder8EmitJumpEPNS1_14BytecodeLabelsE, .-_ZN2v88internal11interpreter27BreakableControlFlowBuilder8EmitJumpEPNS1_14BytecodeLabelsE
	.section	.text._ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfTrueENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfTrueENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE
	.type	_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfTrueENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE, @function
_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfTrueENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE:
.LFB19393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%r8), %r13
	movl	%esi, %r12d
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfTrueENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	.cfi_endproc
.LFE19393:
	.size	_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfTrueENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE, .-_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfTrueENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE
	.section	.text._ZN2v88internal11interpreter27BreakableControlFlowBuilder15EmitJumpIfFalseENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter27BreakableControlFlowBuilder15EmitJumpIfFalseENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE
	.type	_ZN2v88internal11interpreter27BreakableControlFlowBuilder15EmitJumpIfFalseENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE, @function
_ZN2v88internal11interpreter27BreakableControlFlowBuilder15EmitJumpIfFalseENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE:
.LFB19394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%r8), %r13
	movl	%esi, %r12d
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	movq	%rax, %rdx
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11JumpIfFalseENS2_13ToBooleanModeEPNS1_13BytecodeLabelE@PLT
	.cfi_endproc
.LFE19394:
	.size	_ZN2v88internal11interpreter27BreakableControlFlowBuilder15EmitJumpIfFalseENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE, .-_ZN2v88internal11interpreter27BreakableControlFlowBuilder15EmitJumpIfFalseENS1_20BytecodeArrayBuilder13ToBooleanModeEPNS1_14BytecodeLabelsE
	.section	.text._ZN2v88internal11interpreter27BreakableControlFlowBuilder19EmitJumpIfUndefinedEPNS1_14BytecodeLabelsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter27BreakableControlFlowBuilder19EmitJumpIfUndefinedEPNS1_14BytecodeLabelsE
	.type	_ZN2v88internal11interpreter27BreakableControlFlowBuilder19EmitJumpIfUndefinedEPNS1_14BytecodeLabelsE, @function
_ZN2v88internal11interpreter27BreakableControlFlowBuilder19EmitJumpIfUndefinedEPNS1_14BytecodeLabelsE:
.LFB19395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%r8), %r12
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder15JumpIfUndefinedEPNS1_13BytecodeLabelE@PLT
	.cfi_endproc
.LFE19395:
	.size	_ZN2v88internal11interpreter27BreakableControlFlowBuilder19EmitJumpIfUndefinedEPNS1_14BytecodeLabelsE, .-_ZN2v88internal11interpreter27BreakableControlFlowBuilder19EmitJumpIfUndefinedEPNS1_14BytecodeLabelsE
	.section	.text._ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfNullEPNS1_14BytecodeLabelsE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfNullEPNS1_14BytecodeLabelsE
	.type	_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfNullEPNS1_14BytecodeLabelsE, @function
_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfNullEPNS1_14BytecodeLabelsE:
.LFB19396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%r8), %r12
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder10JumpIfNullEPNS1_13BytecodeLabelE@PLT
	.cfi_endproc
.LFE19396:
	.size	_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfNullEPNS1_14BytecodeLabelsE, .-_ZN2v88internal11interpreter27BreakableControlFlowBuilder14EmitJumpIfNullEPNS1_14BytecodeLabelsE
	.section	.text._ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv
	.type	_ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv, @function
_ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv:
.LFB19401:
	.cfi_startproc
	endbr64
	leaq	72(%rdi), %rsi
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_18BytecodeLoopHeaderE@PLT
	.cfi_endproc
.LFE19401:
	.size	_ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv, .-_ZN2v88internal11interpreter11LoopBuilder10LoopHeaderEv
	.section	.text._ZN2v88internal11interpreter11LoopBuilder8LoopBodyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11LoopBuilder8LoopBodyEv
	.type	_ZN2v88internal11interpreter11LoopBuilder8LoopBodyEv, @function
_ZN2v88internal11interpreter11LoopBuilder8LoopBodyEv:
.LFB19402:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	testq	%rax, %rax
	je	.L14
	movl	120(%rdi), %esi
	cmpl	$-1, %esi
	je	.L14
	movq	32(%rax), %rdi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	ret
	.cfi_endproc
.LFE19402:
	.size	_ZN2v88internal11interpreter11LoopBuilder8LoopBodyEv, .-_ZN2v88internal11interpreter11LoopBuilder8LoopBodyEv
	.section	.text._ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi
	.type	_ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi, @function
_ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi:
.LFB19403:
	.cfi_startproc
	endbr64
	cmpl	$5, %esi
	movl	$5, %edx
	cmovle	%esi, %edx
	leaq	72(%rdi), %rsi
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder8JumpLoopEPNS1_18BytecodeLoopHeaderEi@PLT
	.cfi_endproc
.LFE19403:
	.size	_ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi, .-_ZN2v88internal11interpreter11LoopBuilder12JumpToHeaderEi
	.section	.text._ZN2v88internal11interpreter11LoopBuilder18BindContinueTargetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11LoopBuilder18BindContinueTargetEv
	.type	_ZN2v88internal11interpreter11LoopBuilder18BindContinueTargetEv, @function
_ZN2v88internal11interpreter11LoopBuilder18BindContinueTargetEv:
.LFB19405:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rsi
	addq	$80, %rdi
	jmp	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	.cfi_endproc
.LFE19405:
	.size	_ZN2v88internal11interpreter11LoopBuilder18BindContinueTargetEv, .-_ZN2v88internal11interpreter11LoopBuilder18BindContinueTargetEv
	.section	.text._ZN2v88internal11interpreter15TryCatchBuilder8BeginTryENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter15TryCatchBuilder8BeginTryENS1_8RegisterE
	.type	_ZN2v88internal11interpreter15TryCatchBuilder8BeginTryENS1_8RegisterE, @function
_ZN2v88internal11interpreter15TryCatchBuilder8BeginTryENS1_8RegisterE:
.LFB19415:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	movl	16(%rdi), %esi
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MarkTryBeginEiNS1_8RegisterE@PLT
	.cfi_endproc
.LFE19415:
	.size	_ZN2v88internal11interpreter15TryCatchBuilder8BeginTryENS1_8RegisterE, .-_ZN2v88internal11interpreter15TryCatchBuilder8BeginTryENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter15TryCatchBuilder8EndCatchEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter15TryCatchBuilder8EndCatchEv
	.type	_ZN2v88internal11interpreter15TryCatchBuilder8EndCatchEv, @function
_ZN2v88internal11interpreter15TryCatchBuilder8EndCatchEv:
.LFB19417:
	.cfi_startproc
	endbr64
	leaq	24(%rdi), %rsi
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	.cfi_endproc
.LFE19417:
	.size	_ZN2v88internal11interpreter15TryCatchBuilder8EndCatchEv, .-_ZN2v88internal11interpreter15TryCatchBuilder8EndCatchEv
	.section	.text._ZN2v88internal11interpreter17TryFinallyBuilder8BeginTryENS1_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17TryFinallyBuilder8BeginTryENS1_8RegisterE
	.type	_ZN2v88internal11interpreter17TryFinallyBuilder8BeginTryENS1_8RegisterE, @function
_ZN2v88internal11interpreter17TryFinallyBuilder8BeginTryENS1_8RegisterE:
.LFB19422:
	.cfi_startproc
	endbr64
	movl	%esi, %edx
	movl	16(%rdi), %esi
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder12MarkTryBeginEiNS1_8RegisterE@PLT
	.cfi_endproc
.LFE19422:
	.size	_ZN2v88internal11interpreter17TryFinallyBuilder8BeginTryENS1_8RegisterE, .-_ZN2v88internal11interpreter17TryFinallyBuilder8BeginTryENS1_8RegisterE
	.section	.text._ZN2v88internal11interpreter17TryFinallyBuilder8LeaveTryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17TryFinallyBuilder8LeaveTryEv
	.type	_ZN2v88internal11interpreter17TryFinallyBuilder8LeaveTryEv, @function
_ZN2v88internal11interpreter17TryFinallyBuilder8LeaveTryEv:
.LFB19423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$40, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	-32(%rdi), %r12
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	.cfi_endproc
.LFE19423:
	.size	_ZN2v88internal11interpreter17TryFinallyBuilder8LeaveTryEv, .-_ZN2v88internal11interpreter17TryFinallyBuilder8LeaveTryEv
	.section	.text._ZN2v88internal11interpreter17TryFinallyBuilder6EndTryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17TryFinallyBuilder6EndTryEv
	.type	_ZN2v88internal11interpreter17TryFinallyBuilder6EndTryEv, @function
_ZN2v88internal11interpreter17TryFinallyBuilder6EndTryEv:
.LFB19424:
	.cfi_startproc
	endbr64
	movl	16(%rdi), %esi
	movq	8(%rdi), %rdi
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder10MarkTryEndEi@PLT
	.cfi_endproc
.LFE19424:
	.size	_ZN2v88internal11interpreter17TryFinallyBuilder6EndTryEv, .-_ZN2v88internal11interpreter17TryFinallyBuilder6EndTryEv
	.section	.text._ZN2v88internal11interpreter17TryFinallyBuilder12BeginHandlerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17TryFinallyBuilder12BeginHandlerEv
	.type	_ZN2v88internal11interpreter17TryFinallyBuilder12BeginHandlerEv, @function
_ZN2v88internal11interpreter17TryFinallyBuilder12BeginHandlerEv:
.LFB19425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	24(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movl	20(%rbx), %edx
	movl	16(%rbx), %esi
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder11MarkHandlerEiNS0_12HandlerTable15CatchPredictionE@PLT
	.cfi_endproc
.LFE19425:
	.size	_ZN2v88internal11interpreter17TryFinallyBuilder12BeginHandlerEv, .-_ZN2v88internal11interpreter17TryFinallyBuilder12BeginHandlerEv
	.section	.text._ZN2v88internal11interpreter17TryFinallyBuilder10EndFinallyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17TryFinallyBuilder10EndFinallyEv
	.type	_ZN2v88internal11interpreter17TryFinallyBuilder10EndFinallyEv, @function
_ZN2v88internal11interpreter17TryFinallyBuilder10EndFinallyEv:
.LFB19427:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE19427:
	.size	_ZN2v88internal11interpreter17TryFinallyBuilder10EndFinallyEv, .-_ZN2v88internal11interpreter17TryFinallyBuilder10EndFinallyEv
	.section	.text._ZN2v88internal11interpreter29ConditionalControlFlowBuilder9JumpToEndEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder9JumpToEndEv
	.type	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder9JumpToEndEv, @function
_ZN2v88internal11interpreter29ConditionalControlFlowBuilder9JumpToEndEv:
.LFB19432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$16, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	-8(%rdi), %r12
	call	_ZN2v88internal11interpreter14BytecodeLabels3NewEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	.cfi_endproc
.LFE19432:
	.size	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder9JumpToEndEv, .-_ZN2v88internal11interpreter29ConditionalControlFlowBuilder9JumpToEndEv
	.section	.text._ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ThenEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ThenEv
	.type	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ThenEv, @function
_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ThenEv:
.LFB19433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$56, %rdi
	subq	$8, %rsp
	movq	-48(%rdi), %rsi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	152(%rbx), %rax
	testq	%rax, %rax
	je	.L35
	movl	144(%rbx), %esi
	cmpl	$-1, %esi
	je	.L35
	movq	32(%rax), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19433:
	.size	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ThenEv, .-_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ThenEv
	.section	.text._ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ElseEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ElseEv
	.type	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ElseEv, @function
_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ElseEv:
.LFB19434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$96, %rdi
	subq	$8, %rsp
	movq	-88(%rdi), %rsi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	152(%rbx), %rax
	testq	%rax, %rax
	je	.L44
	movl	148(%rbx), %esi
	cmpl	$-1, %esi
	je	.L44
	movq	32(%rax), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19434:
	.size	_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ElseEv, .-_ZN2v88internal11interpreter29ConditionalControlFlowBuilder4ElseEv
	.section	.rodata._ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_,"axG",@progbits,_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	.type	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_, @function
_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_:
.LFB21944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r14
	movq	8(%rdi), %r12
	movq	%r14, %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	$268435455, %rax
	je	.L91
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r12, %rdx
	testq	%rax, %rax
	je	.L69
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L92
	movl	$2147483640, %esi
	movl	$2147483640, %ecx
.L55:
	movq	0(%r13), %rdi
	movq	16(%rdi), %rax
	movq	24(%rdi), %r8
	subq	%rax, %r8
	cmpq	%rsi, %r8
	jb	.L93
	addq	%rax, %rsi
	movq	%rsi, 16(%rdi)
.L58:
	leaq	(%rax,%rcx), %rsi
	leaq	8(%rax), %rcx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L92:
	testq	%rcx, %rcx
	jne	.L94
	movl	$8, %ecx
	xorl	%esi, %esi
	xorl	%eax, %eax
.L56:
	movq	(%r15), %rdi
	movq	%rdi, (%rax,%rdx)
	cmpq	%r12, %rbx
	je	.L59
	leaq	-8(%rbx), %rdi
	leaq	15(%rax), %rcx
	movq	%r12, %rdx
	subq	%r12, %rdi
	subq	%r12, %rcx
	movq	%rdi, %r8
	shrq	$3, %r8
	cmpq	$30, %rcx
	jbe	.L72
	movabsq	$2305843009213693950, %rcx
	testq	%rcx, %r8
	je	.L72
	addq	$1, %r8
	xorl	%edx, %edx
	movq	%r8, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L61:
	movdqu	(%r12,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L61
	movq	%r8, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %rcx
	addq	%rcx, %r12
	addq	%rax, %rcx
	cmpq	%rdx, %r8
	je	.L63
	movq	(%r12), %rdx
	movq	%rdx, (%rcx)
.L63:
	leaq	16(%rax,%rdi), %rcx
.L59:
	cmpq	%r14, %rbx
	je	.L64
	movq	%r14, %rdi
	movq	%rbx, %rdx
	subq	%rbx, %rdi
	leaq	-8(%rdi), %r8
	leaq	15(%rbx), %rdi
	movq	%r8, %r9
	subq	%rcx, %rdi
	shrq	$3, %r9
	cmpq	$30, %rdi
	jbe	.L73
	movabsq	$2305843009213693950, %rdi
	testq	%rdi, %r9
	je	.L73
	addq	$1, %r9
	xorl	%edx, %edx
	movq	%r9, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L66:
	movdqu	(%rbx,%rdx), %xmm2
	movups	%xmm2, (%rcx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L66
	movq	%r9, %rdx
	andq	$-2, %rdx
	leaq	0(,%rdx,8), %r10
	leaq	(%rcx,%r10), %rdi
	addq	%r10, %rbx
	cmpq	%rdx, %r9
	je	.L68
	movq	(%rbx), %rdx
	movq	%rdx, (%rdi)
.L68:
	leaq	8(%rcx,%r8), %rcx
.L64:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rsi, 24(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movl	$8, %esi
	movl	$8, %ecx
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%rcx, %rdi
	.p2align 4,,10
	.p2align 3
.L65:
	movl	(%rdx), %r10d
	movl	4(%rdx), %r9d
	addq	$8, %rdx
	addq	$8, %rdi
	movl	%r10d, -8(%rdi)
	movl	%r9d, -4(%rdi)
	cmpq	%rdx, %r14
	jne	.L65
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L60:
	movl	(%rdx), %r9d
	movl	4(%rdx), %r8d
	addq	$8, %rdx
	addq	$8, %rcx
	movl	%r9d, -8(%rcx)
	movl	%r8d, -4(%rcx)
	cmpq	%rdx, %rbx
	jne	.L60
	jmp	.L63
.L93:
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZN2v88internal4Zone9NewExpandEm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	jmp	.L58
.L91:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L94:
	cmpq	$268435455, %rcx
	movl	$268435455, %eax
	cmova	%rax, %rcx
	salq	$3, %rcx
	movq	%rcx, %rsi
	jmp	.L55
	.cfi_endproc
.LFE21944:
	.size	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_, .-_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	.section	.text._ZN2v88internal11interpreter29ConditionalControlFlowBuilderD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD2Ev
	.type	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD2Ev, @function
_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD2Ev:
.LFB19429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal11interpreter29ConditionalControlFlowBuilderE(%rip), %rax
	cmpb	$0, 128(%rdi)
	movq	%rax, (%rdi)
	je	.L125
.L96:
	leaq	16(%rbx), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	152(%rbx), %r12
	testq	%r12, %r12
	je	.L98
	movq	136(%rbx), %rdx
	movzbl	4(%rdx), %eax
	andl	$63, %eax
	cmpb	$12, %al
	je	.L126
.L98:
	movq	104(%rbx), %rax
	leaq	104(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L108
	.p2align 4,,10
	.p2align 3
.L109:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L109
.L108:
	movq	64(%rbx), %rax
	leaq	64(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L110
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L111
.L110:
	movq	24(%rbx), %rax
	addq	$24, %rbx
	cmpq	%rbx, %rax
	je	.L95
	.p2align 4,,10
	.p2align 3
.L113:
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L113
.L95:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	leaq	96(%rdi), %rdi
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	8(%rbx), %rsi
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L126:
	movq	40(%r12), %rax
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L98
	movq	%rsi, %rcx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L102
.L101:
	cmpq	%rdx, 32(%rax)
	jnb	.L128
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L101
.L102:
	cmpq	%rcx, %rsi
	je	.L98
	cmpq	%rdx, 32(%rcx)
	ja	.L98
	movq	40(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L98
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L98
	movq	16(%r12), %rsi
	movq	%rsi, %r13
	subq	8(%r12), %r13
	sarq	$3, %r13
	cmpq	24(%r12), %rsi
	je	.L106
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L107:
	cmpl	$-1, %r13d
	je	.L98
	movq	32(%r12), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
	jmp	.L98
.L106:
	leaq	-48(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L107
.L127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19429:
	.size	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD2Ev, .-_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD2Ev
	.globl	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD1Ev
	.set	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD1Ev,_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD2Ev
	.section	.text._ZN2v88internal11interpreter29ConditionalControlFlowBuilderD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD0Ev
	.type	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD0Ev, @function
_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD0Ev:
.LFB19431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$160, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE19431:
	.size	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD0Ev, .-_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD0Ev
	.section	.text._ZN2v88internal11interpreter13SwitchBuilderD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter13SwitchBuilderD2Ev
	.type	_ZN2v88internal11interpreter13SwitchBuilderD2Ev, @function
_ZN2v88internal11interpreter13SwitchBuilderD2Ev:
.LFB19407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$16, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal11interpreter27BreakableControlFlowBuilderE(%rip), %rax
	movq	%rax, -16(%rdi)
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L133
	movq	40(%r12), %rax
	movq	56(%rbx), %rcx
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L133
	movq	%rsi, %rdx
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L154:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L136
.L135:
	cmpq	%rcx, 32(%rax)
	jnb	.L154
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L135
.L136:
	cmpq	%rdx, %rsi
	je	.L133
	cmpq	%rcx, 32(%rdx)
	ja	.L133
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L133
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L133
	movq	16(%r12), %rsi
	movq	%rsi, %r13
	subq	8(%r12), %r13
	sarq	$3, %r13
	cmpq	24(%r12), %rsi
	je	.L140
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L141:
	cmpl	$-1, %r13d
	je	.L133
	movq	32(%r12), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L133:
	movq	24(%rbx), %rax
	addq	$24, %rbx
	cmpq	%rbx, %rax
	je	.L131
	.p2align 4,,10
	.p2align 3
.L144:
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L144
.L131:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L155
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L141
.L155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19407:
	.size	_ZN2v88internal11interpreter13SwitchBuilderD2Ev, .-_ZN2v88internal11interpreter13SwitchBuilderD2Ev
	.globl	_ZN2v88internal11interpreter13SwitchBuilderD1Ev
	.set	_ZN2v88internal11interpreter13SwitchBuilderD1Ev,_ZN2v88internal11interpreter13SwitchBuilderD2Ev
	.section	.text._ZN2v88internal11interpreter17TryFinallyBuilderD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17TryFinallyBuilderD0Ev
	.type	_ZN2v88internal11interpreter17TryFinallyBuilderD0Ev, @function
_ZN2v88internal11interpreter17TryFinallyBuilderD0Ev:
.LFB19421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	80(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal11interpreter17TryFinallyBuilderE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L158
	movq	40(%rbx), %rax
	movq	88(%rdi), %rcx
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L158
	movq	%rsi, %rdx
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L179:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L161
.L160:
	cmpq	%rcx, 32(%rax)
	jnb	.L179
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L160
.L161:
	cmpq	%rdx, %rsi
	je	.L158
	cmpq	%rcx, 32(%rdx)
	ja	.L158
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L158
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L158
	movq	16(%rbx), %rsi
	movq	%rsi, %r13
	subq	8(%rbx), %r13
	sarq	$3, %r13
	cmpq	24(%rbx), %rsi
	je	.L165
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L166:
	cmpl	$-1, %r13d
	je	.L158
	movq	32(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L158:
	movq	48(%r12), %rax
	leaq	48(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L168
	.p2align 4,,10
	.p2align 3
.L169:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L169
.L168:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L180
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L166
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19421:
	.size	_ZN2v88internal11interpreter17TryFinallyBuilderD0Ev, .-_ZN2v88internal11interpreter17TryFinallyBuilderD0Ev
	.section	.text._ZN2v88internal11interpreter27BreakableControlFlowBuilderD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD0Ev
	.type	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD0Ev, @function
_ZN2v88internal11interpreter27BreakableControlFlowBuilderD0Ev:
.LFB19390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	-8(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal11interpreter27BreakableControlFlowBuilderE(%rip), %rax
	movq	%rax, -16(%rdi)
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L183
	movq	40(%rbx), %rax
	movq	56(%r12), %rcx
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L183
	movq	%rsi, %rdx
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L204:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L186
.L185:
	cmpq	%rcx, 32(%rax)
	jnb	.L204
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L185
.L186:
	cmpq	%rdx, %rsi
	je	.L183
	cmpq	%rcx, 32(%rdx)
	ja	.L183
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L183
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L183
	movq	16(%rbx), %rsi
	movq	%rsi, %r13
	subq	8(%rbx), %r13
	sarq	$3, %r13
	cmpq	24(%rbx), %rsi
	je	.L190
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L191:
	cmpl	$-1, %r13d
	je	.L183
	movq	32(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L183:
	movq	24(%r12), %rax
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L193
	.p2align 4,,10
	.p2align 3
.L194:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L194
.L193:
	movl	$72, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L205
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L191
.L205:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19390:
	.size	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD0Ev, .-_ZN2v88internal11interpreter27BreakableControlFlowBuilderD0Ev
	.section	.text._ZN2v88internal11interpreter13SwitchBuilderD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter13SwitchBuilderD0Ev
	.type	_ZN2v88internal11interpreter13SwitchBuilderD0Ev, @function
_ZN2v88internal11interpreter13SwitchBuilderD0Ev:
.LFB19409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	-8(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal11interpreter27BreakableControlFlowBuilderE(%rip), %rax
	movq	%rax, -16(%rdi)
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L208
	movq	40(%rbx), %rax
	movq	56(%r12), %rcx
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L208
	movq	%rsi, %rdx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L211
.L210:
	cmpq	%rcx, 32(%rax)
	jnb	.L229
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L210
.L211:
	cmpq	%rdx, %rsi
	je	.L208
	cmpq	%rcx, 32(%rdx)
	ja	.L208
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L208
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L208
	movq	16(%rbx), %rsi
	movq	%rsi, %r13
	subq	8(%rbx), %r13
	sarq	$3, %r13
	cmpq	24(%rbx), %rsi
	je	.L215
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L216:
	cmpl	$-1, %r13d
	je	.L208
	movq	32(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L208:
	movq	24(%r12), %rax
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L218
	.p2align 4,,10
	.p2align 3
.L219:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L219
.L218:
	movl	$104, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L230
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L216
.L230:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19409:
	.size	_ZN2v88internal11interpreter13SwitchBuilderD0Ev, .-_ZN2v88internal11interpreter13SwitchBuilderD0Ev
	.section	.text._ZN2v88internal11interpreter11LoopBuilderD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11LoopBuilderD2Ev
	.type	_ZN2v88internal11interpreter11LoopBuilderD2Ev, @function
_ZN2v88internal11interpreter11LoopBuilderD2Ev:
.LFB19398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	88(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal11interpreter11LoopBuilderE(%rip), %rax
	movq	%rax, (%rdi)
	movq	88(%rdi), %rax
	cmpq	%rdx, %rax
	je	.L232
	.p2align 4,,10
	.p2align 3
.L233:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L233
.L232:
	leaq	16+_ZTVN2v88internal11interpreter27BreakableControlFlowBuilderE(%rip), %rax
	movq	8(%rbx), %rsi
	leaq	16(%rbx), %rdi
	movq	%rax, (%rbx)
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L235
	movq	40(%r12), %rax
	movq	56(%rbx), %rdx
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L235
	movq	%rsi, %rcx
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L238
.L237:
	cmpq	%rdx, 32(%rax)
	jnb	.L257
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L237
.L238:
	cmpq	%rcx, %rsi
	je	.L235
	cmpq	%rdx, 32(%rcx)
	ja	.L235
	movq	40(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L235
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L235
	movq	16(%r12), %rsi
	movq	%rsi, %r13
	subq	8(%r12), %r13
	sarq	$3, %r13
	cmpq	24(%r12), %rsi
	je	.L242
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L243:
	cmpl	$-1, %r13d
	je	.L235
	movq	32(%r12), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L235:
	movq	24(%rbx), %rax
	addq	$24, %rbx
	cmpq	%rbx, %rax
	je	.L231
	.p2align 4,,10
	.p2align 3
.L246:
	movq	(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L246
.L231:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L258
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L242:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L243
.L258:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19398:
	.size	_ZN2v88internal11interpreter11LoopBuilderD2Ev, .-_ZN2v88internal11interpreter11LoopBuilderD2Ev
	.globl	_ZN2v88internal11interpreter11LoopBuilderD1Ev
	.set	_ZN2v88internal11interpreter11LoopBuilderD1Ev,_ZN2v88internal11interpreter11LoopBuilderD2Ev
	.section	.text._ZN2v88internal11interpreter15TryCatchBuilderD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter15TryCatchBuilderD2Ev
	.type	_ZN2v88internal11interpreter15TryCatchBuilderD2Ev, @function
_ZN2v88internal11interpreter15TryCatchBuilderD2Ev:
.LFB19412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	40(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal11interpreter15TryCatchBuilderE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L259
	movq	40(%rbx), %rax
	movq	48(%rdi), %rcx
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L259
	movq	%rsi, %rdx
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L279:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L264
.L263:
	cmpq	%rcx, 32(%rax)
	jnb	.L279
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L263
.L264:
	cmpq	%rdx, %rsi
	je	.L259
	cmpq	%rcx, 32(%rdx)
	ja	.L259
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L259
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -32(%rbp)
	cmpl	$-1, %eax
	je	.L259
	movq	16(%rbx), %rsi
	movq	%rsi, %r12
	subq	8(%rbx), %r12
	sarq	$3, %r12
	cmpq	24(%rbx), %rsi
	je	.L268
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L269:
	cmpl	$-1, %r12d
	je	.L259
	movq	32(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L259:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L280
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L269
.L280:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19412:
	.size	_ZN2v88internal11interpreter15TryCatchBuilderD2Ev, .-_ZN2v88internal11interpreter15TryCatchBuilderD2Ev
	.globl	_ZN2v88internal11interpreter15TryCatchBuilderD1Ev
	.set	_ZN2v88internal11interpreter15TryCatchBuilderD1Ev,_ZN2v88internal11interpreter15TryCatchBuilderD2Ev
	.section	.text._ZN2v88internal11interpreter17TryFinallyBuilder12BeginFinallyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17TryFinallyBuilder12BeginFinallyEv
	.type	_ZN2v88internal11interpreter17TryFinallyBuilder12BeginFinallyEv, @function
_ZN2v88internal11interpreter17TryFinallyBuilder12BeginFinallyEv:
.LFB19426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$16, %rsp
	movq	-32(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	80(%rbx), %r12
	testq	%r12, %r12
	je	.L281
	movq	40(%r12), %rax
	movq	88(%rbx), %rcx
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L281
	movq	%rsi, %rdx
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L301:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L286
.L285:
	cmpq	%rcx, 32(%rax)
	jnb	.L301
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L285
.L286:
	cmpq	%rdx, %rsi
	je	.L281
	cmpq	%rcx, 32(%rdx)
	ja	.L281
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L281
	movq	(%rdi), %rax
	movl	$4, %esi
	call	*16(%rax)
	movq	%rax, -32(%rbp)
	cmpl	$-1, %eax
	je	.L281
	movq	16(%r12), %rsi
	movq	%rsi, %rbx
	subq	8(%r12), %rbx
	sarq	$3, %rbx
	cmpq	24(%r12), %rsi
	je	.L290
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L291:
	cmpl	$-1, %ebx
	je	.L281
	movq	32(%r12), %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L281:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L302
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L291
.L302:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19426:
	.size	_ZN2v88internal11interpreter17TryFinallyBuilder12BeginFinallyEv, .-_ZN2v88internal11interpreter17TryFinallyBuilder12BeginFinallyEv
	.section	.text._ZN2v88internal11interpreter15TryCatchBuilderD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter15TryCatchBuilderD0Ev
	.type	_ZN2v88internal11interpreter15TryCatchBuilderD0Ev, @function
_ZN2v88internal11interpreter15TryCatchBuilderD0Ev:
.LFB19414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	40(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal11interpreter15TryCatchBuilderE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L305
	movq	40(%r13), %rax
	movq	48(%rdi), %rcx
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L305
	movq	%rsi, %rdx
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L308
.L307:
	cmpq	%rcx, 32(%rax)
	jnb	.L323
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L307
.L308:
	cmpq	%rdx, %rsi
	je	.L305
	cmpq	%rcx, 32(%rdx)
	ja	.L305
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L305
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L305
	movq	16(%r13), %rsi
	movq	%rsi, %rbx
	subq	8(%r13), %rbx
	sarq	$3, %rbx
	cmpq	24(%r13), %rsi
	je	.L312
	movq	%rax, (%rsi)
	addq	$8, 16(%r13)
.L313:
	cmpl	$-1, %ebx
	je	.L305
	movq	32(%r13), %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L305:
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L324
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L313
.L324:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19414:
	.size	_ZN2v88internal11interpreter15TryCatchBuilderD0Ev, .-_ZN2v88internal11interpreter15TryCatchBuilderD0Ev
	.section	.text._ZN2v88internal11interpreter17TryFinallyBuilderD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter17TryFinallyBuilderD2Ev
	.type	_ZN2v88internal11interpreter17TryFinallyBuilderD2Ev, @function
_ZN2v88internal11interpreter17TryFinallyBuilderD2Ev:
.LFB19419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	80(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal11interpreter17TryFinallyBuilderE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L327
	movq	40(%r12), %rax
	movq	88(%rdi), %rdx
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L327
	movq	%rsi, %rcx
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L348:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L330
.L329:
	cmpq	%rdx, 32(%rax)
	jnb	.L348
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L329
.L330:
	cmpq	%rcx, %rsi
	je	.L327
	cmpq	%rdx, 32(%rcx)
	ja	.L327
	movq	40(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L327
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L327
	movq	16(%r12), %rsi
	movq	%rsi, %r13
	subq	8(%r12), %r13
	sarq	$3, %r13
	cmpq	24(%r12), %rsi
	je	.L334
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L335:
	cmpl	$-1, %r13d
	je	.L327
	movq	32(%r12), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L327:
	movq	48(%rbx), %rax
	leaq	48(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L325
	.p2align 4,,10
	.p2align 3
.L338:
	movq	(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L338
.L325:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L349
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L334:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L335
.L349:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19419:
	.size	_ZN2v88internal11interpreter17TryFinallyBuilderD2Ev, .-_ZN2v88internal11interpreter17TryFinallyBuilderD2Ev
	.globl	_ZN2v88internal11interpreter17TryFinallyBuilderD1Ev
	.set	_ZN2v88internal11interpreter17TryFinallyBuilderD1Ev,_ZN2v88internal11interpreter17TryFinallyBuilderD2Ev
	.section	.text._ZN2v88internal11interpreter11LoopBuilderD0Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter11LoopBuilderD0Ev
	.type	_ZN2v88internal11interpreter11LoopBuilderD0Ev, @function
_ZN2v88internal11interpreter11LoopBuilderD0Ev:
.LFB19400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	88(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal11interpreter11LoopBuilderE(%rip), %rax
	movq	%rax, (%rdi)
	movq	88(%rdi), %rax
	cmpq	%rdx, %rax
	je	.L351
	.p2align 4,,10
	.p2align 3
.L352:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L352
.L351:
	leaq	16+_ZTVN2v88internal11interpreter27BreakableControlFlowBuilderE(%rip), %rax
	movq	8(%r12), %rsi
	leaq	16(%r12), %rdi
	movq	%rax, (%r12)
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L354
	movq	40(%rbx), %rax
	movq	56(%r12), %rdx
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L354
	movq	%rsi, %rcx
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L376:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L357
.L356:
	cmpq	%rdx, 32(%rax)
	jnb	.L376
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L356
.L357:
	cmpq	%rcx, %rsi
	je	.L354
	cmpq	%rdx, 32(%rcx)
	ja	.L354
	movq	40(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L354
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L354
	movq	16(%rbx), %rsi
	movq	%rsi, %r13
	subq	8(%rbx), %r13
	sarq	$3, %r13
	cmpq	24(%rbx), %rsi
	je	.L361
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L362:
	cmpl	$-1, %r13d
	je	.L354
	movq	32(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L354:
	movq	24(%r12), %rax
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L364
	.p2align 4,,10
	.p2align 3
.L365:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L365
.L364:
	movl	$128, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L377
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L361:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L362
.L377:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19400:
	.size	_ZN2v88internal11interpreter11LoopBuilderD0Ev, .-_ZN2v88internal11interpreter11LoopBuilderD0Ev
	.section	.text._ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev
	.type	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev, @function
_ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev:
.LFB19388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$16, %rdi
	subq	$24, %rsp
	movq	-8(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN2v88internal11interpreter27BreakableControlFlowBuilderE(%rip), %rax
	movq	%rax, -16(%rdi)
	call	_ZN2v88internal11interpreter14BytecodeLabels4BindEPNS1_20BytecodeArrayBuilderE@PLT
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L380
	movq	40(%r12), %rax
	movq	56(%rbx), %rdx
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L380
	movq	%rsi, %rcx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L401:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L383
.L382:
	cmpq	%rdx, 32(%rax)
	jnb	.L401
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L382
.L383:
	cmpq	%rcx, %rsi
	je	.L380
	cmpq	%rdx, 32(%rcx)
	ja	.L380
	movq	40(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L380
	movq	(%rdi), %rax
	movl	$2, %esi
	call	*16(%rax)
	movq	%rax, -48(%rbp)
	cmpl	$-1, %eax
	je	.L380
	movq	16(%r12), %rsi
	movq	%rsi, %r13
	subq	8(%r12), %r13
	sarq	$3, %r13
	cmpq	24(%r12), %rsi
	je	.L387
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L388:
	cmpl	$-1, %r13d
	je	.L380
	movq	32(%r12), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L380:
	movq	24(%rbx), %rax
	leaq	24(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L378
	.p2align 4,,10
	.p2align 3
.L391:
	movq	(%rax), %rax
	cmpq	%rdi, %rax
	jne	.L391
.L378:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L402
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L387:
	.cfi_restore_state
	leaq	-48(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L388
.L402:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19388:
	.size	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev, .-_ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev
	.globl	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD1Ev
	.set	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD1Ev,_ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev
	.section	.rodata._ZN2v88internal11interpreter13SwitchBuilder13SetCaseTargetEiPNS0_10CaseClauseE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"vector::_M_range_check: __n (which is %zu) >= this->size() (which is %zu)"
	.section	.text._ZN2v88internal11interpreter13SwitchBuilder13SetCaseTargetEiPNS0_10CaseClauseE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter13SwitchBuilder13SetCaseTargetEiPNS0_10CaseClauseE
	.type	_ZN2v88internal11interpreter13SwitchBuilder13SetCaseTargetEiPNS0_10CaseClauseE, @function
_ZN2v88internal11interpreter13SwitchBuilder13SetCaseTargetEiPNS0_10CaseClauseE:
.LFB19410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	88(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	80(%rdi), %rax
	subq	%rax, %r8
	sarq	$4, %r8
	cmpq	%r8, %rsi
	jnb	.L424
	movq	%rdi, %rbx
	salq	$4, %rsi
	movq	8(%rdi), %rdi
	movq	%rdx, %r12
	addq	%rax, %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4BindEPNS1_13BytecodeLabelE@PLT
	movq	64(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L403
	movq	40(%rbx), %rax
	leaq	16(%rax), %rcx
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L403
	movq	%rcx, %rdx
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L425:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L409
.L408:
	cmpq	%r12, 32(%rax)
	jnb	.L425
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L408
.L409:
	cmpq	%rdx, %rcx
	je	.L403
	cmpq	%r12, 32(%rdx)
	ja	.L403
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L403
	movq	(%rdi), %rax
	xorl	%esi, %esi
	call	*16(%rax)
	movq	%rax, -32(%rbp)
	cmpl	$-1, %eax
	je	.L403
	movq	16(%rbx), %rsi
	movq	%rsi, %r12
	subq	8(%rbx), %r12
	sarq	$3, %r12
	cmpq	24(%rbx), %rsi
	je	.L413
	movq	%rax, (%rsi)
	addq	$8, 16(%rbx)
.L414:
	cmpl	$-1, %r12d
	je	.L403
	movq	32(%rbx), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L403:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L426
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	movq	%rbx, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L414
.L426:
	call	__stack_chk_fail@PLT
.L424:
	movq	%r8, %rdx
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
	.cfi_endproc
.LFE19410:
	.size	_ZN2v88internal11interpreter13SwitchBuilder13SetCaseTargetEiPNS0_10CaseClauseE, .-_ZN2v88internal11interpreter13SwitchBuilder13SetCaseTargetEiPNS0_10CaseClauseE
	.section	.text._ZN2v88internal11interpreter15TryCatchBuilder6EndTryEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter15TryCatchBuilder6EndTryEv
	.type	_ZN2v88internal11interpreter15TryCatchBuilder6EndTryEv, @function
_ZN2v88internal11interpreter15TryCatchBuilder6EndTryEv:
.LFB19416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	16(%rdi), %esi
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder10MarkTryEndEi@PLT
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rsi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder4JumpEPNS1_13BytecodeLabelE@PLT
	movl	20(%rbx), %edx
	movl	16(%rbx), %esi
	movq	8(%rbx), %rdi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder11MarkHandlerEiNS0_12HandlerTable15CatchPredictionE@PLT
	movq	40(%rbx), %r12
	testq	%r12, %r12
	je	.L427
	movq	40(%r12), %rax
	movq	48(%rbx), %rcx
	leaq	16(%rax), %rsi
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L427
	movq	%rsi, %rdx
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L447:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	je	.L432
.L431:
	cmpq	%rcx, 32(%rax)
	jnb	.L447
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L431
.L432:
	cmpq	%rdx, %rsi
	je	.L427
	cmpq	%rcx, 32(%rdx)
	ja	.L427
	movq	40(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L427
	movq	(%rdi), %rax
	movl	$1, %esi
	call	*16(%rax)
	movq	%rax, -32(%rbp)
	cmpl	$-1, %eax
	je	.L427
	movq	16(%r12), %rsi
	movq	%rsi, %rbx
	subq	8(%r12), %rbx
	sarq	$3, %rbx
	cmpq	24(%r12), %rsi
	je	.L436
	movq	%rax, (%rsi)
	addq	$8, 16(%r12)
.L437:
	cmpl	$-1, %ebx
	je	.L427
	movq	32(%r12), %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal11interpreter20BytecodeArrayBuilder15IncBlockCounterEi@PLT
.L427:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L448
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	leaq	-32(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorIN2v88internal11SourceRangeENS1_13ZoneAllocatorIS2_EEE17_M_realloc_insertIJRS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S5_EEDpOT_
	jmp	.L437
.L448:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE19416:
	.size	_ZN2v88internal11interpreter15TryCatchBuilder6EndTryEv, .-_ZN2v88internal11interpreter15TryCatchBuilder6EndTryEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev:
.LFB24234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE24234:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev, .-_GLOBAL__sub_I__ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter27BreakableControlFlowBuilderD2Ev
	.weak	_ZTVN2v88internal11interpreter27BreakableControlFlowBuilderE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter27BreakableControlFlowBuilderE,"awG",@progbits,_ZTVN2v88internal11interpreter27BreakableControlFlowBuilderE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter27BreakableControlFlowBuilderE, @object
	.size	_ZTVN2v88internal11interpreter27BreakableControlFlowBuilderE, 32
_ZTVN2v88internal11interpreter27BreakableControlFlowBuilderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD1Ev
	.quad	_ZN2v88internal11interpreter27BreakableControlFlowBuilderD0Ev
	.weak	_ZTVN2v88internal11interpreter11LoopBuilderE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter11LoopBuilderE,"awG",@progbits,_ZTVN2v88internal11interpreter11LoopBuilderE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter11LoopBuilderE, @object
	.size	_ZTVN2v88internal11interpreter11LoopBuilderE, 32
_ZTVN2v88internal11interpreter11LoopBuilderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter11LoopBuilderD1Ev
	.quad	_ZN2v88internal11interpreter11LoopBuilderD0Ev
	.weak	_ZTVN2v88internal11interpreter13SwitchBuilderE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter13SwitchBuilderE,"awG",@progbits,_ZTVN2v88internal11interpreter13SwitchBuilderE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter13SwitchBuilderE, @object
	.size	_ZTVN2v88internal11interpreter13SwitchBuilderE, 32
_ZTVN2v88internal11interpreter13SwitchBuilderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter13SwitchBuilderD1Ev
	.quad	_ZN2v88internal11interpreter13SwitchBuilderD0Ev
	.weak	_ZTVN2v88internal11interpreter15TryCatchBuilderE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter15TryCatchBuilderE,"awG",@progbits,_ZTVN2v88internal11interpreter15TryCatchBuilderE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter15TryCatchBuilderE, @object
	.size	_ZTVN2v88internal11interpreter15TryCatchBuilderE, 32
_ZTVN2v88internal11interpreter15TryCatchBuilderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter15TryCatchBuilderD1Ev
	.quad	_ZN2v88internal11interpreter15TryCatchBuilderD0Ev
	.weak	_ZTVN2v88internal11interpreter17TryFinallyBuilderE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter17TryFinallyBuilderE,"awG",@progbits,_ZTVN2v88internal11interpreter17TryFinallyBuilderE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter17TryFinallyBuilderE, @object
	.size	_ZTVN2v88internal11interpreter17TryFinallyBuilderE, 32
_ZTVN2v88internal11interpreter17TryFinallyBuilderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter17TryFinallyBuilderD1Ev
	.quad	_ZN2v88internal11interpreter17TryFinallyBuilderD0Ev
	.weak	_ZTVN2v88internal11interpreter29ConditionalControlFlowBuilderE
	.section	.data.rel.ro.local._ZTVN2v88internal11interpreter29ConditionalControlFlowBuilderE,"awG",@progbits,_ZTVN2v88internal11interpreter29ConditionalControlFlowBuilderE,comdat
	.align 8
	.type	_ZTVN2v88internal11interpreter29ConditionalControlFlowBuilderE, @object
	.size	_ZTVN2v88internal11interpreter29ConditionalControlFlowBuilderE, 32
_ZTVN2v88internal11interpreter29ConditionalControlFlowBuilderE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD1Ev
	.quad	_ZN2v88internal11interpreter29ConditionalControlFlowBuilderD0Ev
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
