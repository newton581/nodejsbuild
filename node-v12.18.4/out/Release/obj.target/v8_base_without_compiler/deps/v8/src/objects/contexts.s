	.file	"contexts.cc"
	.text
	.section	.rodata._ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"used >= 0 && length > 0 && used < length"
	.section	.rodata._ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
.LC2:
	.string	"length < Smi::kMaxValue / 2"
	.section	.text._ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE
	.type	_ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE, @function
_ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE:
.LFB18074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rax
	movq	15(%rax), %rdx
	movslq	11(%rax), %rcx
	movq	%rdx, %r14
	shrq	$63, %rdx
	sarq	$32, %r14
	cmpl	%ecx, %r14d
	setge	%sil
	orb	%dl, %sil
	jne	.L2
	testl	%ecx, %ecx
	jle	.L2
	leal	1(%r14), %ebx
	movq	%rdi, %r12
	cmpl	%ecx, %ebx
	jne	.L4
	cmpl	$1073741822, %ebx
	jg	.L27
	movq	0(%r13), %rax
	movl	%ebx, %edx
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	andq	$-262144, %rax
	movq	24(%rax), %r15
	subq	$37592, %r15
	movq	%r15, %rdi
	call	_ZN2v88internal7Factory21CopyFixedArrayAndGrowENS0_6HandleINS0_10FixedArrayEEEiNS0_14AllocationTypeE@PLT
	movq	416(%r15), %rdx
	movq	(%rax), %rdi
	movq	%rax, %r12
	movq	%rdx, -1(%rdi)
	testq	%rdx, %rdx
	jne	.L28
.L6:
	movq	(%r12), %rax
.L4:
	salq	$32, %rbx
	movq	%rbx, 15(%rax)
	leal	24(,%r14,8), %eax
	cltq
	movq	(%r12), %r15
	movq	0(%r13), %r13
	leaq	-1(%r15,%rax), %r14
	movq	%r13, (%r14)
	testb	$1, %r13b
	je	.L12
	movq	%r13, %rbx
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L29
	testb	$24, %al
	je	.L12
.L31:
	movq	%r15, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L30
.L12:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L31
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	testb	$1, %dl
	je	.L6
	movq	%rdx, %rax
	andq	$-262144, %rax
	testb	$4, 10(%rax)
	je	.L6
	xorl	%esi, %esi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	.LC2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE18074:
	.size	_ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE, .-_ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE
	.section	.text._ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE
	.type	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE, @function
_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE:
.LFB18075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	15(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	xorl	%r15d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	leaq	10(%rcx), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	23(%rsi), %rbx
	subq	$24, %rsp
	movq	%rax, -56(%rbp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L39:
	movq	(%rbx), %rax
	leaq	9(%r13), %rcx
	leaq	8(%r13), %rdx
	movq	%r14, %r8
	movq	%r12, %rsi
	addq	$8, %rbx
	movq	15(%rax), %rdi
	call	_ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE@PLT
	testl	%eax, %eax
	jns	.L38
	addl	$1, %r15d
.L35:
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	sarq	$32, %rax
	cmpl	%eax, %r15d
	jl	.L39
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movl	%r15d, 0(%r13)
	movl	%eax, 4(%r13)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18075:
	.size	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE, .-_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE
	.section	.text._ZN2v88internal7Context22is_declaration_contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context22is_declaration_contextEv
	.type	_ZN2v88internal7Context22is_declaration_contextEv, @function
_ZN2v88internal7Context22is_declaration_contextEv:
.LFB18076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdx), %rax
	cmpw	$143, 11(%rax)
	je	.L43
	movq	-1(%rdx), %rax
	cmpw	$145, 11(%rax)
	je	.L43
	movq	-1(%rdx), %rax
	cmpw	$146, 11(%rax)
	je	.L43
	movq	-1(%rdx), %rax
	cmpw	$144, 11(%rax)
	je	.L43
	movq	-1(%rdx), %rax
	cmpw	$142, 11(%rax)
	je	.L48
	movq	-1(%rdx), %rcx
	xorl	%eax, %eax
	cmpw	$139, 11(%rcx)
	jne	.L40
	movq	15(%rdx), %rax
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZNK2v88internal9ScopeInfo20is_declaration_scopeEv@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$1, %eax
.L40:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L49
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	15(%rdx), %rax
	leaq	-16(%rbp), %rdi
	movq	%rax, -16(%rbp)
	call	_ZNK2v88internal9ScopeInfo13language_modeEv@PLT
	jmp	.L40
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18076:
	.size	_ZN2v88internal7Context22is_declaration_contextEv, .-_ZN2v88internal7Context22is_declaration_contextEv
	.section	.text._ZN2v88internal7Context19declaration_contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context19declaration_contextEv
	.type	_ZN2v88internal7Context19declaration_contextEv, @function
_ZN2v88internal7Context19declaration_contextEv:
.LFB18077:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-32(%rbp), %rbx
	subq	$16, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L63:
	movq	-1(%r12), %rax
	cmpw	$145, 11(%rax)
	je	.L51
	movq	(%rdx), %rax
	cmpw	$146, 11(%rax)
	je	.L51
	movq	(%rdx), %rax
	cmpw	$144, 11(%rax)
	je	.L51
	movq	(%rdx), %rax
	cmpw	$142, 11(%rax)
	je	.L62
	movq	(%rdx), %rax
	cmpw	$139, 11(%rax)
	je	.L54
.L56:
	movq	23(%r12), %r12
.L55:
	movq	-1(%r12), %rax
	leaq	-1(%r12), %rdx
	cmpw	$143, 11(%rax)
	jne	.L63
.L51:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movq	15(%r12), %rax
	movq	%rbx, %rdi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal9ScopeInfo13language_modeEv@PLT
.L53:
	testb	%al, %al
	je	.L56
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L54:
	movq	15(%r12), %rax
	movq	%rbx, %rdi
	movq	%rax, -32(%rbp)
	call	_ZNK2v88internal9ScopeInfo20is_declaration_scopeEv@PLT
	jmp	.L53
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18077:
	.size	_ZN2v88internal7Context19declaration_contextEv, .-_ZN2v88internal7Context19declaration_contextEv
	.section	.text._ZN2v88internal7Context15closure_contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context15closure_contextEv
	.type	_ZN2v88internal7Context15closure_contextEv, @function
_ZN2v88internal7Context15closure_contextEv:
.LFB18078:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L68:
	movq	-1(%rax), %rcx
	cmpw	$146, 11(%rcx)
	je	.L66
	movq	-1(%rax), %rcx
	cmpw	$144, 11(%rcx)
	je	.L66
	movq	-1(%rax), %rcx
	cmpw	$145, 11(%rcx)
	je	.L66
	movq	-1(%rax), %rdx
	cmpw	$142, 11(%rdx)
	je	.L66
	movq	23(%rax), %rax
.L67:
	movq	-1(%rax), %rcx
	cmpw	$143, 11(%rcx)
	jne	.L68
.L66:
	ret
	.cfi_endproc
.LFE18078:
	.size	_ZN2v88internal7Context15closure_contextEv, .-_ZN2v88internal7Context15closure_contextEv
	.section	.text._ZN2v88internal7Context16extension_objectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context16extension_objectEv
	.type	_ZN2v88internal7Context16extension_objectEv, @function
_ZN2v88internal7Context16extension_objectEv:
.LFB18079:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	31(%rax), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37496(%rdx)
	movl	$0, %edx
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE18079:
	.size	_ZN2v88internal7Context16extension_objectEv, .-_ZN2v88internal7Context16extension_objectEv
	.section	.text._ZN2v88internal7Context18extension_receiverEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context18extension_receiverEv
	.type	_ZN2v88internal7Context18extension_receiverEv, @function
_ZN2v88internal7Context18extension_receiverEv:
.LFB18080:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	-1(%rax), %rdx
	cmpw	$147, 11(%rdx)
	movq	31(%rax), %rax
	je	.L75
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %rdx
	cmpq	%rax, -37496(%rdx)
	movl	$0, %edx
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	ret
	.cfi_endproc
.LFE18080:
	.size	_ZN2v88internal7Context18extension_receiverEv, .-_ZN2v88internal7Context18extension_receiverEv
	.section	.text._ZN2v88internal7Context10scope_infoEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context10scope_infoEv
	.type	_ZN2v88internal7Context10scope_infoEv, @function
_ZN2v88internal7Context10scope_infoEv:
.LFB18081:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	15(%rax), %rax
	ret
	.cfi_endproc
.LFE18081:
	.size	_ZN2v88internal7Context10scope_infoEv, .-_ZN2v88internal7Context10scope_infoEv
	.section	.text._ZN2v88internal7Context6moduleEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context6moduleEv
	.type	_ZN2v88internal7Context6moduleEv, @function
_ZN2v88internal7Context6moduleEv:
.LFB18082:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L80:
	movq	23(%rax), %rax
.L79:
	movq	-1(%rax), %rdx
	cmpw	$144, 11(%rdx)
	jne	.L80
	movq	31(%rax), %rax
	ret
	.cfi_endproc
.LFE18082:
	.size	_ZN2v88internal7Context6moduleEv, .-_ZN2v88internal7Context6moduleEv
	.section	.text._ZN2v88internal7Context13global_objectEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context13global_objectEv
	.type	_ZN2v88internal7Context13global_objectEv, @function
_ZN2v88internal7Context13global_objectEv:
.LFB18083:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	39(%rax), %rax
	movq	31(%rax), %rax
	ret
	.cfi_endproc
.LFE18083:
	.size	_ZN2v88internal7Context13global_objectEv, .-_ZN2v88internal7Context13global_objectEv
	.section	.text._ZN2v88internal7Context14script_contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context14script_contextEv
	.type	_ZN2v88internal7Context14script_contextEv, @function
_ZN2v88internal7Context14script_contextEv:
.LFB18084:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L85:
	movq	23(%rax), %rax
.L84:
	movq	-1(%rax), %rdx
	cmpw	$146, 11(%rdx)
	jne	.L85
	ret
	.cfi_endproc
.LFE18084:
	.size	_ZN2v88internal7Context14script_contextEv, .-_ZN2v88internal7Context14script_contextEv
	.section	.text._ZN2v88internal7Context12global_proxyEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context12global_proxyEv
	.type	_ZN2v88internal7Context12global_proxyEv, @function
_ZN2v88internal7Context12global_proxyEv:
.LFB18085:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	39(%rax), %rax
	movq	47(%rax), %rax
	ret
	.cfi_endproc
.LFE18085:
	.size	_ZN2v88internal7Context12global_proxyEv, .-_ZN2v88internal7Context12global_proxyEv
	.section	.text._ZN2v88internal7Context16set_global_proxyENS0_13JSGlobalProxyE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context16set_global_proxyENS0_13JSGlobalProxyE
	.type	_ZN2v88internal7Context16set_global_proxyENS0_13JSGlobalProxyE, @function
_ZN2v88internal7Context16set_global_proxyENS0_13JSGlobalProxyE:
.LFB18086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	39(%rax), %r13
	movq	%rsi, 47(%r13)
	testb	$1, %sil
	je	.L87
	movq	%rsi, %rbx
	movq	%rsi, %r12
	leaq	47(%r13), %r14
	andq	$-262144, %rbx
	movq	8(%rbx), %rax
	testl	$262144, %eax
	jne	.L99
	testb	$24, %al
	je	.L87
.L101:
	movq	%r13, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L100
.L87:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	.cfi_restore_state
	movq	%rsi, %rdx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%rbx), %rax
	testb	$24, %al
	jne	.L101
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L100:
	popq	%rbx
	movq	%r12, %rdx
	movq	%r14, %rsi
	popq	%r12
	movq	%r13, %rdi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE18086:
	.size	_ZN2v88internal7Context16set_global_proxyENS0_13JSGlobalProxyE, .-_ZN2v88internal7Context16set_global_proxyENS0_13JSGlobalProxyE
	.section	.text._ZN2v88internal7Context6LookupENS0_6HandleIS1_EENS2_INS0_6StringEEENS0_18ContextLookupFlagsEPiPNS0_18PropertyAttributesEPNS0_18InitializationFlagEPNS0_12VariableModeEPb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context6LookupENS0_6HandleIS1_EENS2_INS0_6StringEEENS0_18ContextLookupFlagsEPiPNS0_18PropertyAttributesEPNS0_18InitializationFlagEPNS0_12VariableModeEPb
	.type	_ZN2v88internal7Context6LookupENS0_6HandleIS1_EENS2_INS0_6StringEEENS0_18ContextLookupFlagsEPiPNS0_18PropertyAttributesEPNS0_18InitializationFlagEPNS0_12VariableModeEPb, @function
_ZN2v88internal7Context6LookupENS0_6HandleIS1_EENS2_INS0_6StringEEENS0_18ContextLookupFlagsEPiPNS0_18PropertyAttributesEPNS0_18InitializationFlagEPNS0_12VariableModeEPb:
.LFB18089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rsi
	movq	24(%rbp), %rbx
	movq	%rcx, -280(%rbp)
	movq	%r8, -264(%rbp)
	movq	%r9, -288(%rbp)
	movq	%rsi, -296(%rbp)
	movq	%rbx, -304(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	andq	$-262144, %rax
	movq	24(%rax), %r11
	movl	%edx, %eax
	movl	$-1, (%rcx)
	andl	$1, %eax
	movl	$64, (%r8)
	movl	%eax, -244(%rbp)
	leaq	-37592(%r11), %r14
	movb	$1, (%r9)
	movb	$2, (%rsi)
	testq	%rbx, %rbx
	je	.L103
	movb	$0, (%rbx)
.L103:
	andl	$2, %edx
	movq	%r14, %rax
	xorl	%ebx, %ebx
	movq	%r15, %r14
	movl	%edx, -248(%rbp)
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L204:
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$145, 11(%rdx)
	je	.L107
	movq	-1(%rax), %rdx
	cmpw	$147, 11(%rdx)
	je	.L107
	movq	-1(%rax), %rdx
	cmpw	$143, 11(%rdx)
	je	.L107
	movq	-1(%rax), %rdx
	cmpw	$139, 11(%rdx)
	je	.L107
	.p2align 4,,10
	.p2align 3
.L110:
	movq	-1(%rax), %rdx
	cmpw	$143, 11(%rdx)
	je	.L162
	movq	-1(%rax), %rdx
	cmpw	$139, 11(%rdx)
	je	.L162
	movq	-1(%rax), %rdx
	cmpw	$146, 11(%rdx)
	je	.L162
	movq	-1(%rax), %rdx
	cmpw	$142, 11(%rdx)
	je	.L162
	movq	-1(%rax), %rdx
	cmpw	$144, 11(%rdx)
	jne	.L302
	.p2align 4,,10
	.p2align 3
.L162:
	movq	15(%rax), %rdi
	movq	(%r12), %rsi
	leaq	-231(%rbp), %rcx
	leaq	-232(%rbp), %rdx
	leaq	-230(%rbp), %r8
	movq	%rdi, -144(%rbp)
	call	_ZN2v88internal9ScopeInfo16ContextSlotIndexES1_NS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE@PLT
	testl	%eax, %eax
	jns	.L303
	movq	(%r14), %rax
	movl	-244(%rbp), %esi
	leaq	-1(%rax), %rdx
	testl	%esi, %esi
	je	.L168
	movq	-1(%rax), %rcx
	cmpw	$143, 11(%rcx)
	je	.L304
.L168:
	movq	(%rdx), %rdx
	cmpw	$144, 11(%rdx)
	je	.L305
.L164:
	movq	-1(%rax), %rax
	cmpw	$145, 11(%rax)
	jne	.L206
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L306:
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r14
	testb	%bl, %bl
	je	.L205
.L203:
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$146, 11(%rdx)
	je	.L205
	movq	-1(%rax), %rdx
	cmpw	$145, 11(%rdx)
	je	.L205
	movq	-1(%rax), %rdx
	cmpw	$147, 11(%rdx)
	je	.L205
	movq	-1(%rax), %rax
	cmpw	$144, 11(%rax)
	je	.L205
.L206:
	movq	(%r14), %rax
	movq	23(%rax), %rsi
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L306
	movq	41088(%r15), %r14
	cmpq	41096(%r15), %r14
	je	.L307
.L202:
	leaq	8(%r14), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r14)
	testb	%bl, %bl
	jne	.L203
	.p2align 4,,10
	.p2align 3
.L205:
	movl	-244(%rbp), %eax
	testl	%eax, %eax
	jne	.L204
.L199:
	xorl	%eax, %eax
.L161:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L308
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	leaq	-144(%rbp), %r13
	movq	%rax, -144(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88internal7Context18extension_receiverEv
	testq	%rax, %rax
	movq	(%r14), %rax
	je	.L110
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal7Context18extension_receiverEv
	movq	41112(%r15), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L283
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r8
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$145, 11(%rdx)
	je	.L309
.L113:
	movl	-248(%rbp), %edi
	movq	(%r8), %rax
	testl	%edi, %edi
	je	.L120
	movq	-1(%rax), %rdx
	cmpw	$1065, 11(%rdx)
	je	.L120
	movq	(%r14), %rdx
	movq	-1(%rdx), %rdx
	cmpw	$147, 11(%rdx)
	je	.L310
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %r9
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L311
.L154:
	movq	(%r12), %rcx
	movl	$3, %edx
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	je	.L312
	.p2align 4,,10
	.p2align 3
.L158:
	movabsq	$824633720832, %rcx
	movl	%edx, -224(%rbp)
	movq	%r12, %rdx
	movq	%rcx, -212(%rbp)
	movq	%r9, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L313
.L159:
	leaq	-224(%rbp), %rdi
	movq	%r8, -176(%rbp)
	movq	%r8, -160(%rbp)
	movq	%r8, -272(%rbp)
	movq	%rdi, -256(%rbp)
	movq	%rdx, -192(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -168(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	-272(%rbp), %r8
	movq	-256(%rbp), %rdi
.L157:
	movq	%r8, -256(%rbp)
	call	_ZN2v88internal10JSReceiver21GetPropertyAttributesEPNS0_14LookupIteratorE@PLT
	movq	-256(%rbp), %r8
	movl	%eax, %edx
	shrq	$32, %rax
	testb	%dl, %dl
	je	.L199
	movq	-264(%rbp), %rcx
	movl	%eax, (%rcx)
	cmpl	$64, %eax
	jne	.L152
	movq	(%r14), %rax
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%r15, %rdi
	movq	%rsi, -256(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L120:
	andq	$-262144, %rax
	movq	24(%rax), %rax
	leaq	-37592(%rax), %r9
	movq	(%r12), %rax
	movq	-1(%rax), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L314
.L125:
	movq	(%r12), %rcx
	movl	$1, %edx
	movq	-1(%rcx), %rsi
	cmpw	$64, 11(%rsi)
	jne	.L158
	movl	11(%rcx), %edx
	notl	%edx
	andl	$1, %edx
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L305:
	movq	(%r12), %rsi
	leaq	-228(%rbp), %rcx
	leaq	-229(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	leaq	-224(%rbp), %r8
	call	_ZN2v88internal9ScopeInfo11ModuleIndexENS0_6StringEPNS0_12VariableModeEPNS0_18InitializationFlagEPNS0_17MaybeAssignedFlagE@PLT
	testl	%eax, %eax
	jne	.L287
.L297:
	movq	(%r14), %rax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L283:
	movq	41088(%r15), %r8
	cmpq	%r8, 41096(%r15)
	je	.L315
.L112:
	leaq	8(%r8), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, (%r8)
	movq	(%r14), %rax
	movq	-1(%rax), %rdx
	cmpw	$145, 11(%rdx)
	jne	.L113
.L309:
	movq	%r8, -272(%rbp)
	movq	39(%rax), %rax
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	31(%rax), %rax
	movq	23(%rax), %rax
	movq	1135(%rax), %rsi
	movq	(%r12), %rdx
	movq	%rsi, -256(%rbp)
	call	_ZN2v88internal18ScriptContextTable6LookupEPNS0_7IsolateES1_NS0_6StringEPNS1_12LookupResultE
	movq	-256(%rbp), %rsi
	movq	-272(%rbp), %r8
	testb	%al, %al
	je	.L113
	movl	-144(%rbp), %eax
	movq	%r15, %r14
	leal	24(,%rax,8), %eax
	cltq
	movq	-1(%rsi,%rax), %r12
	movl	-140(%rbp), %eax
	movq	-280(%rbp), %rcx
	movzbl	-135(%rbp), %edx
	movl	%eax, (%rcx)
	movq	-296(%rbp), %rcx
	movzbl	-136(%rbp), %eax
	movb	%al, (%rcx)
	movq	-288(%rbp), %rcx
	movb	%dl, (%rcx)
	movl	$1, %edx
	cmpb	$1, %al
	je	.L115
	subl	$7, %eax
	xorl	%edx, %edx
	cmpb	$3, %al
	setbe	%dl
.L115:
	movq	-264(%rbp), %rax
	movl	%edx, (%rax)
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L298
.L179:
	movq	41088(%r14), %r10
	cmpq	41096(%r14), %r10
	je	.L316
.L181:
	leaq	8(%r10), %rax
	movq	%rax, 41088(%r14)
	movq	%r12, (%r10)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L304:
	leaq	-144(%rbp), %r13
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNK2v88internal9ScopeInfo24FunctionContextSlotIndexENS0_6StringE@PLT
	testl	%eax, %eax
	jns	.L170
	movq	(%r14), %rax
	leaq	-1(%rax), %rdx
	jmp	.L168
.L314:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %edx
	testb	$1, %dl
	jne	.L126
	andl	$2, %edx
	jne	.L125
.L126:
	leaq	-228(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r8, -272(%rbp)
	movq	%r9, -256(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-256(%rbp), %r9
	movq	-272(%rbp), %r8
	testb	%al, %al
	je	.L317
	movabsq	$824633720832, %rax
	movq	%r13, %rdi
	movq	%r8, -96(%rbp)
	movq	%rax, -132(%rbp)
	movl	-228(%rbp), %eax
	movq	%r8, -80(%rbp)
	movq	%r8, -256(%rbp)
	movl	$1, -144(%rbp)
	movq	%r9, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movq	%r12, -112(%rbp)
	movdqa	-128(%rbp), %xmm1
	leaq	-224(%rbp), %rdi
	movdqa	-144(%rbp), %xmm0
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movaps	%xmm1, -208(%rbp)
	movq	-256(%rbp), %r8
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm2, -192(%rbp)
	movaps	%xmm3, -176(%rbp)
	movaps	%xmm4, -160(%rbp)
	jmp	.L157
.L302:
	movq	-1(%rax), %rdx
	cmpw	$140, 11(%rdx)
	je	.L162
	movq	-1(%rax), %rdx
	cmpw	$141, 11(%rdx)
	jne	.L164
	movq	31(%rax), %rsi
	testb	$1, %sil
	jne	.L318
.L183:
	movq	47(%rax), %r13
	testb	$1, %r13b
	jne	.L319
.L192:
	movq	55(%rax), %rdx
	testb	$1, %dl
	je	.L164
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	subl	$126, %ecx
	cmpw	$9, %cx
	ja	.L164
	testb	%bl, %bl
	jne	.L164
	movq	%rdx, -144(%rbp)
	leaq	-144(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN2v88internal9StringSet3HasEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	xorl	$1, %eax
	movl	%eax, %ebx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%r12, %rsi
	movq	%r9, %rdi
	movq	%r8, -256(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-256(%rbp), %r8
	movq	%rax, %rdx
	jmp	.L159
.L315:
	movq	%r15, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	movq	%rax, %r8
	jmp	.L112
.L310:
	movq	(%r12), %rdi
	movq	%r8, -256(%rbp)
	call	_ZN2v88internal9ScopeInfo19VariableIsSyntheticENS0_6StringE@PLT
	movq	-256(%rbp), %r8
	testb	%al, %al
	je	.L132
.L151:
	movq	-264(%rbp), %rax
	movl	$64, (%rax)
	movq	(%r14), %rax
	jmp	.L110
.L303:
	movq	-280(%rbp), %rcx
	movzbl	-231(%rbp), %edx
	movq	%r14, %r15
	movl	%eax, (%rcx)
	movq	-296(%rbp), %rcx
	movzbl	-232(%rbp), %eax
	movb	%al, (%rcx)
	movq	-288(%rbp), %rcx
	movb	%dl, (%rcx)
	movl	$1, %edx
	cmpb	$1, %al
	je	.L166
	subl	$7, %eax
	xorl	%edx, %edx
	cmpb	$3, %al
	setbe	%dl
.L166:
	movq	-264(%rbp), %rax
	movl	%edx, (%rax)
	movq	%r15, %rax
	jmp	.L161
.L317:
	movq	(%r12), %rax
	jmp	.L125
.L311:
	movq	%rax, -144(%rbp)
	movl	7(%rax), %edx
	testb	$1, %dl
	jne	.L155
	andl	$2, %edx
	jne	.L154
.L155:
	leaq	-228(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r8, -272(%rbp)
	movq	%r9, -256(%rbp)
	call	_ZN2v88internal6String16SlowAsArrayIndexEPj@PLT
	movq	-256(%rbp), %r9
	movq	-272(%rbp), %r8
	testb	%al, %al
	je	.L320
	movabsq	$824633720832, %rax
	movq	%r13, %rdi
	movq	%r8, -96(%rbp)
	movq	%rax, -132(%rbp)
	movl	-228(%rbp), %eax
	movq	%r8, -80(%rbp)
	movq	%r8, -256(%rbp)
	movl	$3, -144(%rbp)
	movq	%r9, -120(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -88(%rbp)
	movl	%eax, -72(%rbp)
	movl	$-1, -68(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb1EEEvv@PLT
	movdqa	-128(%rbp), %xmm6
	movq	%r12, -112(%rbp)
	leaq	-224(%rbp), %rdi
	movdqa	-144(%rbp), %xmm5
	movdqa	-112(%rbp), %xmm7
	movaps	%xmm6, -208(%rbp)
	movdqa	-80(%rbp), %xmm6
	movq	-256(%rbp), %r8
	movaps	%xmm5, -224(%rbp)
	movdqa	-96(%rbp), %xmm5
	movaps	%xmm7, -192(%rbp)
	movaps	%xmm5, -176(%rbp)
	movaps	%xmm6, -160(%rbp)
	jmp	.L157
.L132:
	movq	(%r8), %rax
	movq	(%r12), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$3, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L134
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L134:
	movl	%eax, -224(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -212(%rbp)
	movq	(%r12), %rax
	movq	%rdi, -200(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r12, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L321
.L135:
	leaq	-224(%rbp), %rdi
	movq	%rax, -192(%rbp)
	movq	%r8, -176(%rbp)
	movq	%r8, -160(%rbp)
	movq	%r8, -312(%rbp)
	movq	%rdi, -272(%rbp)
	movq	$0, -184(%rbp)
	movq	$0, -168(%rbp)
	movq	$-1, -152(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	-200(%rbp), %rax
	movq	-272(%rbp), %rdi
	movq	%rax, -256(%rbp)
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	movzbl	%ah, %edx
	testb	%al, %al
	je	.L199
	testb	%dl, %dl
	movq	-312(%rbp), %r8
	je	.L151
	movq	-256(%rbp), %rax
	movq	-176(%rbp), %rdx
	movq	3928(%rax), %rcx
	leaq	3928(%rax), %rsi
	movl	$3, %eax
	movq	-1(%rcx), %rdi
	cmpw	$64, 11(%rdi)
	jne	.L138
	movl	11(%rcx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L138:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	-256(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	3928(%rax), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L322
.L139:
	movq	%r13, %rdi
	movq	%r8, -272(%rbp)
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movq	-272(%rbp), %r8
	jne	.L140
	movq	-120(%rbp), %rax
	leaq	88(%rax), %rdx
.L141:
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L323
.L145:
	movq	-264(%rbp), %rax
	movl	$0, (%rax)
.L152:
	movq	%r8, %rax
	jmp	.L161
.L312:
	movl	11(%rcx), %edx
	andl	$1, %edx
	cmpb	$1, %dl
	sbbl	%edx, %edx
	andl	$3, %edx
	jmp	.L158
.L287:
	movq	%r15, %rcx
	movq	%r14, %r15
	movzbl	-229(%rbp), %edx
	movl	%eax, %edi
	movq	%rcx, %r14
	movq	-280(%rbp), %rcx
	movl	%eax, (%rcx)
	movq	-296(%rbp), %rcx
	movb	%dl, (%rcx)
	movzbl	-228(%rbp), %edx
	movq	-288(%rbp), %rcx
	movb	%dl, (%rcx)
	call	_ZN2v88internal26SourceTextModuleDescriptor16GetCellIndexKindEi@PLT
	cmpl	$1, %eax
	je	.L324
	movl	$1, %eax
.L176:
	movq	-264(%rbp), %rcx
	movl	%eax, (%rcx)
	movq	(%r15), %rax
	jmp	.L178
.L325:
	movq	23(%rax), %rax
.L178:
	movq	-1(%rax), %rdx
	cmpw	$144, 11(%rdx)
	jne	.L325
	movq	31(%rax), %r12
	movq	41112(%r14), %rdi
	testq	%rdi, %rdi
	je	.L179
.L298:
	movq	%r12, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r10
.L180:
	movq	%r10, %rax
	jmp	.L161
.L324:
	movzbl	-229(%rbp), %edx
	cmpb	$1, %dl
	je	.L176
	subl	$7, %edx
	xorl	%eax, %eax
	cmpb	$3, %dl
	setbe	%al
	jmp	.L176
.L320:
	movq	(%r12), %rax
	jmp	.L154
.L170:
	movq	-280(%rbp), %rcx
	movq	-304(%rbp), %rbx
	movq	%r14, %r15
	movl	%eax, (%rcx)
	movq	-264(%rbp), %rax
	movl	$1, (%rax)
	movq	-288(%rbp), %rax
	movb	$1, (%rax)
	movq	-296(%rbp), %rax
	movb	$1, (%rax)
	testq	%rbx, %rbx
	je	.L172
	movq	%r13, %rdi
	call	_ZNK2v88internal9ScopeInfo13language_modeEv@PLT
	testb	%al, %al
	jne	.L172
	movb	$1, (%rbx)
.L172:
	movq	%r15, %rax
	jmp	.L161
.L321:
	movq	%r12, %rsi
	movq	%r8, -256(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-256(%rbp), %r8
	jmp	.L135
.L140:
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	-272(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdx
	jne	.L141
	jmp	.L199
.L319:
	movq	-1(%r13), %rdx
	movzwl	11(%rdx), %edx
	subw	$138, %dx
	cmpw	$9, %dx
	ja	.L192
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L194
	movq	%r13, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %rdi
.L195:
	pushq	$0
	movq	-280(%rbp), %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	pushq	-296(%rbp)
	movq	-288(%rbp), %r9
	movq	-264(%rbp), %r8
	call	_ZN2v88internal7Context6LookupENS0_6HandleIS1_EENS2_INS0_6StringEEENS0_18ContextLookupFlagsEPiPNS0_18PropertyAttributesEPNS0_18InitializationFlagEPNS0_12VariableModeEPb
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	jne	.L161
	movq	(%r14), %rax
	jmp	.L192
.L318:
	movq	-1(%rsi), %rdx
	cmpw	$1023, 11(%rdx)
	jbe	.L183
	movq	41112(%r15), %rdi
	testq	%rdi, %rdi
	je	.L185
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	%rax, %r13
.L186:
	movq	0(%r13), %rax
	movq	(%r12), %rdx
	andq	$-262144, %rax
	movq	24(%rax), %rdi
	movq	-1(%rdx), %rcx
	movl	$3, %eax
	subq	$37592, %rdi
	cmpw	$64, 11(%rcx)
	jne	.L188
	movl	11(%rdx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L188:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	(%r12), %rax
	movq	%rdi, -120(%rbp)
	movq	-1(%rax), %rax
	movzwl	11(%rax), %edx
	movq	%r12, %rax
	andl	$-32, %edx
	cmpl	$32, %edx
	je	.L326
.L189:
	leaq	-144(%rbp), %rdi
	movq	%rax, -112(%rbp)
	movq	%rdi, -256(%rbp)
	movq	$0, -104(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	movq	-256(%rbp), %rdi
	call	_ZN2v88internal10JSReceiver11HasPropertyEPNS0_14LookupIteratorE@PLT
	testb	%al, %al
	je	.L296
	shrw	$8, %ax
	jne	.L289
.L296:
	movq	(%r14), %rax
	jmp	.L183
.L316:
	movq	%r14, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %r10
	jmp	.L181
.L308:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L323:
	movq	-1(%rax), %rax
	cmpw	$1023, 11(%rax)
	jbe	.L145
	movq	-192(%rbp), %rsi
	movl	$3, %eax
	movq	(%rsi), %rcx
	movq	-1(%rcx), %rdi
	cmpw	$64, 11(%rdi)
	jne	.L146
	movl	11(%rcx), %eax
	andl	$1, %eax
	cmpb	$1, %al
	sbbl	%eax, %eax
	andl	$3, %eax
.L146:
	movl	%eax, -144(%rbp)
	movabsq	$824633720832, %rax
	movq	%rax, -132(%rbp)
	movq	-256(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	(%rsi), %rax
	movq	-1(%rax), %rax
	movzwl	11(%rax), %eax
	andl	$-32, %eax
	cmpl	$32, %eax
	je	.L327
.L147:
	movq	%r13, %rdi
	movq	%r8, -272(%rbp)
	movq	%rsi, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	$-1, -72(%rbp)
	call	_ZN2v88internal14LookupIterator5StartILb0EEEvv@PLT
	cmpl	$4, -140(%rbp)
	movq	-272(%rbp), %r8
	jne	.L148
	movq	-120(%rbp), %rax
	addq	$88, %rax
.L149:
	movq	(%rax), %rax
	movq	-256(%rbp), %rsi
	movq	%r13, %rdi
	movq	%r8, -272(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN2v88internal6Object12BooleanValueEPNS0_7IsolateE@PLT
	movq	-272(%rbp), %r8
	cmpb	$1, %al
	je	.L151
	jmp	.L145
.L322:
	movq	-256(%rbp), %rdi
	movq	%r8, -312(%rbp)
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-312(%rbp), %r8
	movq	-272(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L139
.L148:
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%r8, -272(%rbp)
	call	_ZN2v88internal6Object11GetPropertyEPNS0_14LookupIteratorENS0_13OnNonExistentE@PLT
	movq	-272(%rbp), %r8
	testq	%rax, %rax
	jne	.L149
	jmp	.L199
.L327:
	movq	-256(%rbp), %rdi
	movq	%r8, -312(%rbp)
	movq	%rdx, -272(%rbp)
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	movq	-312(%rbp), %r8
	movq	-272(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L147
.L326:
	movq	%r12, %rsi
	call	_ZN2v88internal11StringTable12LookupStringEPNS0_7IsolateENS0_6HandleINS0_6StringEEE@PLT
	jmp	.L189
.L289:
	movq	-264(%rbp), %rax
	movl	$0, (%rax)
	movq	%r13, %rax
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L185:
	movq	41088(%r15), %r13
	cmpq	41096(%r15), %r13
	je	.L328
.L187:
	leaq	8(%r13), %rax
	movq	%rax, 41088(%r15)
	movq	%rsi, 0(%r13)
	jmp	.L186
.L194:
	movq	41088(%r15), %rdi
	cmpq	41096(%r15), %rdi
	je	.L329
.L196:
	leaq	8(%rdi), %rax
	movq	%rax, 41088(%r15)
	movq	%r13, (%rdi)
	jmp	.L195
.L328:
	movq	%r15, %rdi
	movq	%rsi, -256(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-256(%rbp), %rsi
	movq	%rax, %r13
	jmp	.L187
.L329:
	movq	%r15, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	%rax, %rdi
	jmp	.L196
	.cfi_endproc
.LFE18089:
	.size	_ZN2v88internal7Context6LookupENS0_6HandleIS1_EENS2_INS0_6StringEEENS0_18ContextLookupFlagsEPiPNS0_18PropertyAttributesEPNS0_18InitializationFlagEPNS0_12VariableModeEPb, .-_ZN2v88internal7Context6LookupENS0_6HandleIS1_EENS2_INS0_6StringEEENS0_18ContextLookupFlagsEPiPNS0_18PropertyAttributesEPNS0_18InitializationFlagEPNS0_12VariableModeEPb
	.section	.text._ZN2v88internal13NativeContext16AddOptimizedCodeENS0_4CodeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13NativeContext16AddOptimizedCodeENS0_4CodeE
	.type	_ZN2v88internal13NativeContext16AddOptimizedCodeENS0_4CodeE, @function
_ZN2v88internal13NativeContext16AddOptimizedCodeENS0_4CodeE:
.LFB18090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	1943(%rax), %r13
	movq	31(%rsi), %r14
	movq	%r13, 7(%r14)
	testb	$1, %r13b
	je	.L336
	movq	%r13, %r15
	leaq	7(%r14), %rsi
	andq	$-262144, %r15
	movq	8(%r15), %rax
	testl	$262144, %eax
	jne	.L347
	testb	$24, %al
	je	.L336
.L350:
	movq	%r14, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L348
	.p2align 4,,10
	.p2align 3
.L336:
	movq	(%rbx), %rax
	movq	%r12, 1943(%rax)
	testb	$1, %r12b
	jne	.L349
.L330:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal23Heap_MarkingBarrierSlowENS0_10HeapObjectEmS1_@PLT
	movq	8(%r15), %rax
	movq	-56(%rbp), %rsi
	testb	$24, %al
	jne	.L350
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L349:
	movq	%r12, %rax
	movq	(%rbx), %rdi
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L330
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L330
	addq	$24, %rsp
	movq	%r12, %rdx
	leaq	1943(%rdi), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	jmp	.L336
	.cfi_endproc
.LFE18090:
	.size	_ZN2v88internal13NativeContext16AddOptimizedCodeENS0_4CodeE, .-_ZN2v88internal13NativeContext16AddOptimizedCodeENS0_4CodeE
	.section	.text._ZN2v88internal13NativeContext24SetOptimizedCodeListHeadENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13NativeContext24SetOptimizedCodeListHeadENS0_6ObjectE
	.type	_ZN2v88internal13NativeContext24SetOptimizedCodeListHeadENS0_6ObjectE, @function
_ZN2v88internal13NativeContext24SetOptimizedCodeListHeadENS0_6ObjectE:
.LFB18091:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rsi, %rdx
	movq	%rsi, 1943(%rax)
	testb	$1, %sil
	jne	.L357
.L351:
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	movq	%rsi, %rax
	movq	(%rdi), %rdi
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L351
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L351
	leaq	1943(%rdi), %rsi
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE18091:
	.size	_ZN2v88internal13NativeContext24SetOptimizedCodeListHeadENS0_6ObjectE, .-_ZN2v88internal13NativeContext24SetOptimizedCodeListHeadENS0_6ObjectE
	.section	.text._ZN2v88internal13NativeContext21OptimizedCodeListHeadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13NativeContext21OptimizedCodeListHeadEv
	.type	_ZN2v88internal13NativeContext21OptimizedCodeListHeadEv, @function
_ZN2v88internal13NativeContext21OptimizedCodeListHeadEv:
.LFB18092:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	1943(%rax), %rax
	ret
	.cfi_endproc
.LFE18092:
	.size	_ZN2v88internal13NativeContext21OptimizedCodeListHeadEv, .-_ZN2v88internal13NativeContext21OptimizedCodeListHeadEv
	.section	.text._ZN2v88internal13NativeContext26SetDeoptimizedCodeListHeadENS0_6ObjectE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13NativeContext26SetDeoptimizedCodeListHeadENS0_6ObjectE
	.type	_ZN2v88internal13NativeContext26SetDeoptimizedCodeListHeadENS0_6ObjectE, @function
_ZN2v88internal13NativeContext26SetDeoptimizedCodeListHeadENS0_6ObjectE:
.LFB18093:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	%rsi, %rdx
	movq	%rsi, 1951(%rax)
	testb	$1, %sil
	jne	.L365
.L359:
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	movq	%rsi, %rax
	movq	(%rdi), %rdi
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	je	.L359
	movq	%rdi, %rax
	andq	$-262144, %rax
	testb	$24, 8(%rax)
	jne	.L359
	leaq	1951(%rdi), %rsi
	jmp	_ZN2v88internal28Heap_GenerationalBarrierSlowENS0_10HeapObjectEmS1_@PLT
	.cfi_endproc
.LFE18093:
	.size	_ZN2v88internal13NativeContext26SetDeoptimizedCodeListHeadENS0_6ObjectE, .-_ZN2v88internal13NativeContext26SetDeoptimizedCodeListHeadENS0_6ObjectE
	.section	.text._ZN2v88internal13NativeContext23DeoptimizedCodeListHeadEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13NativeContext23DeoptimizedCodeListHeadEv
	.type	_ZN2v88internal13NativeContext23DeoptimizedCodeListHeadEv, @function
_ZN2v88internal13NativeContext23DeoptimizedCodeListHeadEv:
.LFB18094:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	1951(%rax), %rax
	ret
	.cfi_endproc
.LFE18094:
	.size	_ZN2v88internal13NativeContext23DeoptimizedCodeListHeadEv, .-_ZN2v88internal13NativeContext23DeoptimizedCodeListHeadEv
	.section	.rodata._ZN2v88internal7Context40ErrorMessageForCodeGenerationFromStringsEv.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"Code generation from strings disallowed for this context"
	.section	.rodata._ZN2v88internal7Context40ErrorMessageForCodeGenerationFromStringsEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"(location_) != nullptr"
	.section	.text._ZN2v88internal7Context40ErrorMessageForCodeGenerationFromStringsEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context40ErrorMessageForCodeGenerationFromStringsEv
	.type	_ZN2v88internal7Context40ErrorMessageForCodeGenerationFromStringsEv, @function
_ZN2v88internal7Context40ErrorMessageForCodeGenerationFromStringsEv:
.LFB18095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$56, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andq	$-262144, %rdx
	movq	24(%rdx), %r12
	subq	$37592, %r12
	movq	351(%rax), %rsi
	movq	41112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L368
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %rsi
	cmpq	%rsi, 88(%r12)
	je	.L376
.L372:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L377
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L368:
	.cfi_restore_state
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L378
.L370:
	leaq	8(%rax), %rdx
	movq	%rdx, 41088(%r12)
	movq	%rsi, (%rax)
	cmpq	%rsi, 88(%r12)
	jne	.L372
.L376:
	leaq	.LC3(%rip), %rax
	xorl	%edx, %edx
	leaq	-48(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	movq	$56, -40(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	testq	%rax, %rax
	jne	.L372
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L378:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	jmp	.L370
.L377:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18095:
	.size	_ZN2v88internal7Context40ErrorMessageForCodeGenerationFromStringsEv, .-_ZN2v88internal7Context40ErrorMessageForCodeGenerationFromStringsEv
	.section	.rodata._ZN2v88internal7Context21IntrinsicIndexForNameENS0_6HandleINS0_6StringEEE.str1.1,"aMS",@progbits,1
.LC5:
	.string	"generator_next_internal"
.LC6:
	.string	"make_error"
.LC7:
	.string	"make_range_error"
.LC8:
	.string	"make_syntax_error"
.LC9:
	.string	"make_type_error"
.LC10:
	.string	"make_uri_error"
.LC11:
	.string	"object_create"
.LC12:
	.string	"reflect_apply"
.LC13:
	.string	"reflect_construct"
.LC14:
	.string	"math_floor"
.LC15:
	.string	"math_pow"
.LC16:
	.string	"promise_internal_constructor"
.LC17:
	.string	"is_promise"
.LC18:
	.string	"promise_then"
	.section	.text._ZN2v88internal7Context21IntrinsicIndexForNameENS0_6HandleINS0_6StringEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context21IntrinsicIndexForNameENS0_6HandleINS0_6StringEEE
	.type	_ZN2v88internal7Context21IntrinsicIndexForNameENS0_6HandleINS0_6StringEEE, @function
_ZN2v88internal7Context21IntrinsicIndexForNameENS0_6HandleINS0_6StringEEE:
.LFB18096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC5(%rip), %rsi
	movl	$23, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-32(%rbp), %r12
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	movl	$227, %eax
	testb	%r8b, %r8b
	jne	.L379
	movq	(%rbx), %rax
	leaq	.LC6(%rip), %rsi
	movl	$10, %edx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	movl	$228, %eax
	testb	%r8b, %r8b
	jne	.L379
	movq	(%rbx), %rax
	leaq	.LC7(%rip), %rsi
	movl	$16, %edx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	movl	$229, %eax
	testb	%r8b, %r8b
	jne	.L379
	movq	(%rbx), %rax
	leaq	.LC8(%rip), %rsi
	movl	$17, %edx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	movl	$230, %eax
	testb	%r8b, %r8b
	jne	.L379
	movq	(%rbx), %rax
	leaq	.LC9(%rip), %rsi
	movl	$15, %edx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	movl	$231, %eax
	testb	%r8b, %r8b
	jne	.L379
	movq	(%rbx), %rax
	leaq	.LC10(%rip), %rsi
	movl	$14, %edx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	movl	$232, %eax
	testb	%r8b, %r8b
	jne	.L379
	movq	(%rbx), %rax
	leaq	.LC11(%rip), %rsi
	movl	$13, %edx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	movl	$233, %eax
	testb	%r8b, %r8b
	jne	.L379
	movq	(%rbx), %rax
	leaq	.LC12(%rip), %rsi
	movl	$13, %edx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	movl	$234, %eax
	testb	%r8b, %r8b
	jne	.L379
	movq	(%rbx), %rax
	leaq	.LC13(%rip), %rsi
	movl	$17, %edx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	movl	$235, %eax
	testb	%r8b, %r8b
	jne	.L379
	movq	(%rbx), %rax
	leaq	.LC14(%rip), %rsi
	movl	$10, %edx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	movl	$236, %eax
	testb	%r8b, %r8b
	jne	.L379
	movq	(%rbx), %rax
	leaq	.LC15(%rip), %rsi
	movl	$8, %edx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	movl	$237, %eax
	testb	%r8b, %r8b
	jne	.L379
	movq	(%rbx), %rax
	leaq	.LC16(%rip), %rsi
	movl	$28, %edx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	movl	$238, %eax
	testb	%r8b, %r8b
	jne	.L379
	movq	(%rbx), %rax
	leaq	.LC17(%rip), %rsi
	movl	$10, %edx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	movl	%eax, %r8d
	movl	$239, %eax
	testb	%r8b, %r8b
	jne	.L379
	movq	(%rbx), %rax
	leaq	.LC18(%rip), %rsi
	movl	$12, %edx
	movq	%r12, %rdi
	movq	%rax, -32(%rbp)
	call	_ZN2v88internal6String16IsOneByteEqualToENS0_6VectorIKhEE@PLT
	cmpb	$1, %al
	sbbl	%eax, %eax
	orb	$-16, %al
	.p2align 4,,10
	.p2align 3
.L379:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L397
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L397:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18096:
	.size	_ZN2v88internal7Context21IntrinsicIndexForNameENS0_6HandleINS0_6StringEEE, .-_ZN2v88internal7Context21IntrinsicIndexForNameENS0_6HandleINS0_6StringEEE
	.section	.text._ZN2v88internal7Context21IntrinsicIndexForNameEPKhi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Context21IntrinsicIndexForNameEPKhi
	.type	_ZN2v88internal7Context21IntrinsicIndexForNameEPKhi, @function
_ZN2v88internal7Context21IntrinsicIndexForNameEPKhi:
.LFB18110:
	.cfi_startproc
	endbr64
	movslq	%esi, %rax
	movl	$24, %ecx
	movq	%rdi, %rdx
	movl	$227, %r8d
	cmpq	$24, %rax
	leaq	.LC5(%rip), %rdi
	movq	%rdx, %rsi
	cmovbe	%rax, %rcx
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L398
	movl	$11, %r9d
	cmpq	$11, %rax
	movq	%rdx, %rsi
	movl	$228, %r8d
	movq	%r9, %rcx
	leaq	.LC6(%rip), %rdi
	cmovbe	%rax, %rcx
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%sil
	sbbb	$0, %sil
	testb	%sil, %sil
	je	.L398
	cmpq	$17, %rax
	movl	$17, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rdx, %rsi
	cmovbe	%rax, %rcx
	movl	$229, %r8d
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L398
	movl	$18, %r10d
	cmpq	$18, %rax
	movq	%rdx, %rsi
	movl	$230, %r8d
	movq	%r10, %rcx
	leaq	.LC8(%rip), %rdi
	cmovbe	%rax, %rcx
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%sil
	sbbb	$0, %sil
	testb	%sil, %sil
	je	.L398
	cmpq	$16, %rax
	movl	$16, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rdx, %rsi
	cmovbe	%rax, %rcx
	movl	$231, %r8d
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L398
	cmpq	$15, %rax
	movl	$15, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%rdx, %rsi
	cmovbe	%rax, %rcx
	movl	$232, %r8d
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L398
	movl	$14, %r11d
	cmpq	$14, %rax
	movq	%rdx, %rsi
	movl	$233, %r8d
	movq	%r11, %rcx
	leaq	.LC11(%rip), %rdi
	cmovbe	%rax, %rcx
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L398
	cmpq	$14, %rax
	movq	%r11, %rcx
	movq	%rdx, %rsi
	movl	$234, %r8d
	cmovbe	%rax, %rcx
	leaq	.LC12(%rip), %rdi
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L398
	cmpq	$18, %rax
	movq	%r10, %rcx
	movq	%rdx, %rsi
	movl	$235, %r8d
	cmovbe	%rax, %rcx
	leaq	.LC13(%rip), %rdi
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L398
	cmpq	$11, %rax
	movq	%r9, %rcx
	movq	%rdx, %rsi
	movl	$236, %r8d
	cmovbe	%rax, %rcx
	leaq	.LC14(%rip), %rdi
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L398
	cmpq	$9, %rax
	movl	$9, %ecx
	leaq	.LC15(%rip), %rdi
	movq	%rdx, %rsi
	cmovbe	%rax, %rcx
	movl	$237, %r8d
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L398
	cmpq	$29, %rax
	movl	$29, %ecx
	leaq	.LC16(%rip), %rdi
	movq	%rdx, %rsi
	cmovbe	%rax, %rcx
	movl	$238, %r8d
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L398
	cmpq	$11, %rax
	movl	$11, %ecx
	leaq	.LC17(%rip), %rdi
	movq	%rdx, %rsi
	cmovbe	%rax, %rcx
	movl	$239, %r8d
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%cl
	sbbb	$0, %cl
	testb	%cl, %cl
	je	.L398
	cmpq	$13, %rax
	movl	$13, %ecx
	leaq	.LC18(%rip), %rdi
	movq	%rdx, %rsi
	cmovbe	%rax, %rcx
	cmpq	%rcx, %rcx
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	movsbl	%al, %eax
	cmpl	$1, %eax
	sbbl	%r8d, %r8d
	andl	$241, %r8d
	subl	$1, %r8d
.L398:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE18110:
	.size	_ZN2v88internal7Context21IntrinsicIndexForNameEPKhi, .-_ZN2v88internal7Context21IntrinsicIndexForNameEPKhi
	.section	.text._ZN2v88internal13NativeContext17ResetErrorsThrownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13NativeContext17ResetErrorsThrownEv
	.type	_ZN2v88internal13NativeContext17ResetErrorsThrownEv, @function
_ZN2v88internal13NativeContext17ResetErrorsThrownEv:
.LFB18111:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	$0, 359(%rax)
	ret
	.cfi_endproc
.LFE18111:
	.size	_ZN2v88internal13NativeContext17ResetErrorsThrownEv, .-_ZN2v88internal13NativeContext17ResetErrorsThrownEv
	.section	.text._ZN2v88internal13NativeContext21IncrementErrorsThrownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13NativeContext21IncrementErrorsThrownEv
	.type	_ZN2v88internal13NativeContext21IncrementErrorsThrownEv, @function
_ZN2v88internal13NativeContext21IncrementErrorsThrownEv:
.LFB18112:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	359(%rdx), %rax
	sarq	$32, %rax
	addl	$1, %eax
	salq	$32, %rax
	movq	%rax, 359(%rdx)
	ret
	.cfi_endproc
.LFE18112:
	.size	_ZN2v88internal13NativeContext21IncrementErrorsThrownEv, .-_ZN2v88internal13NativeContext21IncrementErrorsThrownEv
	.section	.text._ZN2v88internal13NativeContext15GetErrorsThrownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal13NativeContext15GetErrorsThrownEv
	.type	_ZN2v88internal13NativeContext15GetErrorsThrownEv, @function
_ZN2v88internal13NativeContext15GetErrorsThrownEv:
.LFB18113:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	359(%rax), %rax
	sarq	$32, %rax
	ret
	.cfi_endproc
.LFE18113:
	.size	_ZN2v88internal13NativeContext15GetErrorsThrownEv, .-_ZN2v88internal13NativeContext15GetErrorsThrownEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE, @function
_GLOBAL__sub_I__ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE:
.LFB22296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22296:
	.size	_GLOBAL__sub_I__ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE, .-_GLOBAL__sub_I__ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal18ScriptContextTable6ExtendENS0_6HandleIS1_EENS2_INS0_7ContextEEE
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
