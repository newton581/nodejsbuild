	.file	"version.cc"
	.text
	.section	.rodata._ZN2v88internal7Version9GetStringENS0_6VectorIcEE.str1.1,"aMS",@progbits,1
.LC0:
	.string	" (candidate)"
.LC1:
	.string	""
.LC2:
	.string	"%d.%d.%d.%d%s%s"
.LC3:
	.string	"%d.%d.%d%s%s"
	.section	.text._ZN2v88internal7Version9GetStringENS0_6VectorIcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Version9GetStringENS0_6VectorIcEE
	.type	_ZN2v88internal7Version9GetStringENS0_6VectorIcEE, @function
_ZN2v88internal7Version9GetStringENS0_6VectorIcEE:
.LFB5041:
	.cfi_startproc
	endbr64
	leaq	.LC1(%rip), %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, _ZN2v88internal7Version10candidate_E(%rip)
	leaq	.LC0(%rip), %rax
	cmove	%rdx, %rax
	movl	_ZN2v88internal7Version6patch_E(%rip), %edx
	movq	_ZN2v88internal7Version9embedder_E(%rip), %r10
	movl	_ZN2v88internal7Version6build_E(%rip), %r9d
	movl	_ZN2v88internal7Version6minor_E(%rip), %r8d
	movl	_ZN2v88internal7Version6major_E(%rip), %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%edx, %edx
	jle	.L3
	subq	$8, %rsp
	pushq	%rax
	xorl	%eax, %eax
	pushq	%r10
	pushq	%rdx
	leaq	.LC2(%rip), %rdx
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	addq	$32, %rsp
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	pushq	%rax
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	pushq	%r10
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5041:
	.size	_ZN2v88internal7Version9GetStringENS0_6VectorIcEE, .-_ZN2v88internal7Version9GetStringENS0_6VectorIcEE
	.section	.rodata._ZN2v88internal7Version9GetSONAMEENS0_6VectorIcEE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"-candidate"
.LC5:
	.string	"libv8-%d.%d.%d.%d%s%s.so"
.LC6:
	.string	"libv8-%d.%d.%d%s%s.so"
.LC7:
	.string	"%s"
	.section	.text._ZN2v88internal7Version9GetSONAMEENS0_6VectorIcEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal7Version9GetSONAMEENS0_6VectorIcEE
	.type	_ZN2v88internal7Version9GetSONAMEENS0_6VectorIcEE, @function
_ZN2v88internal7Version9GetSONAMEENS0_6VectorIcEE:
.LFB5042:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal7Version7soname_E(%rip), %rcx
	testq	%rcx, %rcx
	je	.L9
	cmpb	$0, (%rcx)
	jne	.L10
.L9:
	leaq	.LC1(%rip), %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, _ZN2v88internal7Version10candidate_E(%rip)
	leaq	.LC4(%rip), %rax
	cmove	%rdx, %rax
	movl	_ZN2v88internal7Version6patch_E(%rip), %edx
	movq	_ZN2v88internal7Version9embedder_E(%rip), %r10
	movl	_ZN2v88internal7Version6build_E(%rip), %r9d
	movl	_ZN2v88internal7Version6minor_E(%rip), %r8d
	movl	_ZN2v88internal7Version6major_E(%rip), %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%edx, %edx
	jle	.L12
	subq	$8, %rsp
	pushq	%rax
	xorl	%eax, %eax
	pushq	%r10
	pushq	%rdx
	leaq	.LC5(%rip), %rdx
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	addq	$32, %rsp
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	pushq	%rax
	leaq	.LC6(%rip), %rdx
	xorl	%eax, %eax
	pushq	%r10
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore 6
	leaq	.LC7(%rip), %rdx
	xorl	%eax, %eax
	jmp	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	.cfi_endproc
.LFE5042:
	.size	_ZN2v88internal7Version9GetSONAMEENS0_6VectorIcEE, .-_ZN2v88internal7Version9GetSONAMEENS0_6VectorIcEE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal7Version6major_E,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal7Version6major_E, @function
_GLOBAL__sub_I__ZN2v88internal7Version6major_E:
.LFB5819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5819:
	.size	_GLOBAL__sub_I__ZN2v88internal7Version6major_E, .-_GLOBAL__sub_I__ZN2v88internal7Version6major_E
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal7Version6major_E
	.globl	_ZN2v88internal7Version15version_string_E
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC8:
	.string	"7.8.279.23-node.39"
	.section	.data.rel.local._ZN2v88internal7Version15version_string_E,"aw"
	.align 8
	.type	_ZN2v88internal7Version15version_string_E, @object
	.size	_ZN2v88internal7Version15version_string_E, 8
_ZN2v88internal7Version15version_string_E:
	.quad	.LC8
	.globl	_ZN2v88internal7Version7soname_E
	.section	.data.rel.local._ZN2v88internal7Version7soname_E,"aw"
	.align 8
	.type	_ZN2v88internal7Version7soname_E, @object
	.size	_ZN2v88internal7Version7soname_E, 8
_ZN2v88internal7Version7soname_E:
	.quad	.LC1
	.globl	_ZN2v88internal7Version10candidate_E
	.section	.bss._ZN2v88internal7Version10candidate_E,"aw",@nobits
	.type	_ZN2v88internal7Version10candidate_E, @object
	.size	_ZN2v88internal7Version10candidate_E, 1
_ZN2v88internal7Version10candidate_E:
	.zero	1
	.globl	_ZN2v88internal7Version9embedder_E
	.section	.rodata.str1.1
.LC9:
	.string	"-node.39"
	.section	.data.rel.local._ZN2v88internal7Version9embedder_E,"aw"
	.align 8
	.type	_ZN2v88internal7Version9embedder_E, @object
	.size	_ZN2v88internal7Version9embedder_E, 8
_ZN2v88internal7Version9embedder_E:
	.quad	.LC9
	.globl	_ZN2v88internal7Version6patch_E
	.section	.data._ZN2v88internal7Version6patch_E,"aw"
	.align 4
	.type	_ZN2v88internal7Version6patch_E, @object
	.size	_ZN2v88internal7Version6patch_E, 4
_ZN2v88internal7Version6patch_E:
	.long	23
	.globl	_ZN2v88internal7Version6build_E
	.section	.data._ZN2v88internal7Version6build_E,"aw"
	.align 4
	.type	_ZN2v88internal7Version6build_E, @object
	.size	_ZN2v88internal7Version6build_E, 4
_ZN2v88internal7Version6build_E:
	.long	279
	.globl	_ZN2v88internal7Version6minor_E
	.section	.data._ZN2v88internal7Version6minor_E,"aw"
	.align 4
	.type	_ZN2v88internal7Version6minor_E, @object
	.size	_ZN2v88internal7Version6minor_E, 4
_ZN2v88internal7Version6minor_E:
	.long	8
	.globl	_ZN2v88internal7Version6major_E
	.section	.data._ZN2v88internal7Version6major_E,"aw"
	.align 4
	.type	_ZN2v88internal7Version6major_E, @object
	.size	_ZN2v88internal7Version6major_E, 4
_ZN2v88internal7Version6major_E:
	.long	7
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
