	.file	"v8-console.cc"
	.text
	.section	.text._ZN12v8_inspector17V8InspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	.type	_ZN12v8_inspector17V8InspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE, @function
_ZN12v8_inspector17V8InspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE:
.LFB4580:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4580:
	.size	_ZN12v8_inspector17V8InspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE, .-_ZN12v8_inspector17V8InspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	.section	.text._ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE
	.type	_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE, @function
_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE:
.LFB4582:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4582:
	.size	_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE, .-_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE
	.section	.text._ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE
	.type	_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE, @function
_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE:
.LFB4583:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4583:
	.size	_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE, .-_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE
	.section	.text._ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE
	.type	_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE, @function
_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE:
.LFB4584:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4584:
	.size	_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE, .-_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE
	.section	.text._ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE
	.type	_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE, @function
_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE:
.LFB4585:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4585:
	.size	_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE, .-_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE
	.section	.text._ZN12v8_inspector17V8InspectorClient12consoleClearEi,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient12consoleClearEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient12consoleClearEi
	.type	_ZN12v8_inspector17V8InspectorClient12consoleClearEi, @function
_ZN12v8_inspector17V8InspectorClient12consoleClearEi:
.LFB4586:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4586:
	.size	_ZN12v8_inspector17V8InspectorClient12consoleClearEi, .-_ZN12v8_inspector17V8InspectorClient12consoleClearEi
	.section	.text._ZN12v8_inspector17V8InspectorClient13currentTimeMSEv,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv
	.type	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv, @function
_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv:
.LFB4587:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE4587:
	.size	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv, .-_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv
	.section	.text._ZN12v8_inspector12_GLOBAL__N_118returnDataCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_118returnDataCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN12v8_inspector12_GLOBAL__N_118returnDataCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8191:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	32(%rax), %rdx
	movq	%rdx, 24(%rax)
	ret
	.cfi_endproc
.LFE8191:
	.size	_ZN12v8_inspector12_GLOBAL__N_118returnDataCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN12v8_inspector12_GLOBAL__N_118returnDataCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS4_14ConsoleContextEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS4_14ConsoleContextEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS4_14ConsoleContextEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation:
.LFB11448:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L11
	cmpl	$3, %edx
	je	.L12
	cmpl	$1, %edx
	je	.L16
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11448:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS4_14ConsoleContextEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS4_14ConsoleContextEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation
	.section	.text._ZNSt14_Function_base13_Base_managerIZN12v8_inspector9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS4_14ConsoleContextEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation,"ax",@progbits
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS4_14ConsoleContextEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN12v8_inspector9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS4_14ConsoleContextEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation:
.LFB11453:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L18
	cmpl	$3, %edx
	je	.L19
	cmpl	$1, %edx
	je	.L23
.L19:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11453:
	.size	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS4_14ConsoleContextEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN12v8_inspector9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS4_14ConsoleContextEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation
	.section	.text._ZN12v8_inspector9V8ConsoleD2Ev,"axG",@progbits,_ZN12v8_inspector9V8ConsoleD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector9V8ConsoleD2Ev
	.type	_ZN12v8_inspector9V8ConsoleD2Ev, @function
_ZN12v8_inspector9V8ConsoleD2Ev:
.LFB15072:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE15072:
	.size	_ZN12v8_inspector9V8ConsoleD2Ev, .-_ZN12v8_inspector9V8ConsoleD2Ev
	.weak	_ZN12v8_inspector9V8ConsoleD1Ev
	.set	_ZN12v8_inspector9V8ConsoleD1Ev,_ZN12v8_inspector9V8ConsoleD2Ev
	.section	.text._ZN12v8_inspector9V8ConsoleD0Ev,"axG",@progbits,_ZN12v8_inspector9V8ConsoleD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector9V8ConsoleD0Ev
	.type	_ZN12v8_inspector9V8ConsoleD0Ev, @function
_ZN12v8_inspector9V8ConsoleD0Ev:
.LFB15074:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE15074:
	.size	_ZN12v8_inspector9V8ConsoleD0Ev, .-_ZN12v8_inspector9V8ConsoleD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev:
.LFB5479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L26
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5479:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv:
.LFB5488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L30
	movq	(%rdi), %rax
	call	*24(%rax)
.L30:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L37
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L37:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5488:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv:
.LFB5487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L38
	movq	(%rdi), %rax
	call	*24(%rax)
.L38:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L45:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5487:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv:
.LFB5534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L46
	movq	(%rdi), %rax
	call	*24(%rax)
.L46:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L53
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L53:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5534:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv:
.LFB5533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13ObjectPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L54
	movq	(%rdi), %rax
	call	*24(%rax)
.L54:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L61
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L61:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5533:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv:
.LFB5635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	movq	(%rdi), %rax
	call	*24(%rax)
.L62:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5635:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv:
.LFB5634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime12EntryPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L70
	movq	(%rdi), %rax
	call	*24(%rax)
.L70:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L77:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5634:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv:
.LFB5603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L78
	movq	(%rdi), %rax
	call	*24(%rax)
.L78:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L85
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L85:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5603:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv:
.LFB5602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime15PropertyPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L86
	movq	(%rdi), %rax
	call	*24(%rax)
.L86:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L93:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5602:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memorySetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memorySetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memorySetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memorySetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memorySetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_:
.LFB10156:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	addq	$32, %rdi
	jmp	_ZNK2v88External5ValueEv@PLT
	.cfi_endproc
.LFE10156:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memorySetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memorySetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_5TableERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5TableERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5TableERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5TableERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5TableERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE:
.LFB10176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-96(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	leaq	-112(%rbp), %rdx
	movq	%r13, %rsi
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	call	*56(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L98
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L98:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10176:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5TableERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5TableERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_5ClearERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5ClearERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5ClearERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5ClearERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5ClearERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE:
.LFB10175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-96(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	leaq	-112(%rbp), %rdx
	movq	%r13, %rsi
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	call	*96(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L102
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L102:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10175:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5ClearERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5ClearERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE:
.LFB10174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-96(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	leaq	-112(%rbp), %rdx
	movq	%r13, %rsi
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	call	*136(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L106:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10174:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE:
.LFB10173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-96(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	leaq	-112(%rbp), %rdx
	movq	%r13, %rsi
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	call	*128(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L110:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10173:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE:
.LFB10172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-96(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	leaq	-112(%rbp), %rdx
	movq	%r13, %rsi
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	call	*48(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L114
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L114:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10172:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_3DirERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_3DirERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_3DirERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_3DirERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_3DirERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE:
.LFB10171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-96(%rbp), %rbx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	leaq	-112(%rbp), %rdx
	movq	%r13, %rsi
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	call	*40(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L118:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10171:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_3DirERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_3DirERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"toString"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE, @function
_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE:
.LFB8192:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	movl	16(%rbp), %r9d
	movq	-104(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	testq	%rax, %rax
	je	.L119
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	testq	%r15, %r15
	je	.L128
	leaq	-96(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	movq	%r8, -104(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	-104(%rbp), %r8
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %r15
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L122
	call	_ZdlPv@PLT
.L122:
	movq	%r15, %rdx
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	_ZN12v8_inspector12_GLOBAL__N_118returnDataCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L128
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_6ObjectEEENS1_INS0_4NameEEENS1_INS0_5ValueEEE@PLT
.L128:
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_6ObjectEEENS1_INS0_4NameEEENS1_INS0_5ValueEEE@PLT
.L119:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L139
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L139:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8192:
	.size	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE, .-_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	.section	.text._ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.type	_ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, @function
_ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE:
.LFB8266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	leaq	40(%rax), %rdi
	call	_ZNK2v88External5ValueEv@PLT
	movq	%rax, %r12
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	cmpb	$0, 32(%r12)
	movq	%rax, %r14
	jne	.L162
	movq	8(%r12), %r15
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L140
	movq	(%rbx), %rax
	leaq	-96(%rbp), %rdi
	movq	%r13, %rdx
	movq	16(%rax), %rsi
	call	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE@PLT
	cmpq	$2, -88(%rbp)
	movq	-96(%rbp), %rdi
	jne	.L145
	cmpw	$36, (%rdi)
	je	.L163
.L145:
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	(%rbx), %rax
	movq	(%r12), %rdx
	movq	%rdx, 32(%rax)
.L140:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L164
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	%r13, %rdx
	movq	%rax, %rsi
	addq	$8, %rdi
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L163:
	movzwl	2(%rdi), %eax
	cmpw	$47, %ax
	jbe	.L145
	cmpw	$52, %ax
	jbe	.L146
	cmpw	$95, %ax
	jne	.L145
.L146:
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L150
	call	_ZdlPv@PLT
.L150:
	movq	(%rbx), %rax
	leaq	-128(%rbp), %r13
	movl	$1, %edx
	movq	%r13, %rdi
	movq	16(%rax), %rsi
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	testq	%rax, %rax
	je	.L149
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	%rax, 32(%rdx)
.L149:
	movq	%r13, %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	jmp	.L140
.L164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8266:
	.size	_ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, .-_ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.section	.text._ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorSetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIvEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorSetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIvEE
	.type	_ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorSetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIvEE, @function
_ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorSetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIvEE:
.LFB8267:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	(%rdx), %rax
	leaq	40(%rax), %rdi
	call	_ZNK2v88External5ValueEv@PLT
	movq	%rax, %r14
	movq	(%rbx), %rax
	movq	16(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdx
	movq	%rax, %r15
	movq	(%rbx), %rax
	movq	%r15, %rsi
	leaq	8(%rax), %rdi
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L165
	shrw	$8, %ax
	jne	.L177
.L165:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	addq	$8, %rdi
	call	_ZN2v86Object18CreateDataPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L165
	shrw	$8, %ax
	je	.L165
	movq	24(%r14), %rdi
	addq	$8, %rsp
	movq	%r12, %rdx
	movq	%r15, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v83Set6DeleteENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	.cfi_endproc
.LFE8267:
	.size	_ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorSetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIvEE, .-_ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorSetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIvEE
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev:
.LFB5481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L179
	call	_ZdlPv@PLT
.L179:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L180
	call	_ZdlPv@PLT
.L180:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5481:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_:
.LFB10151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	leaq	32(%rax), %rdi
	call	_ZNK2v88External5ValueEv@PLT
	movq	8(%rax), %rax
	movq	16(%rax), %r13
	movq	0(%r13), %rax
	movq	136(%rax), %r12
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdx
	leaq	_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE(%rip), %rax
	cmpq	%rax, %r12
	jne	.L190
.L182:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	*%r12
	testq	%rax, %rax
	je	.L182
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	%rax, 24(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10151:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev:
.LFB5513:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	160(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L192
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -64(%rbp)
	cmpq	%r12, %rax
	je	.L193
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r14
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L248:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L196
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L197
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L196:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L198
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r14, %rax
	jne	.L199
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L198:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L194:
	addq	$8, %r12
	cmpq	%r12, -64(%rbp)
	je	.L247
.L200:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L194
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L248
	movq	%r15, %rdi
	addq	$8, %r12
	call	*%rax
	cmpq	%r12, -64(%rbp)
	jne	.L200
	.p2align 4,,10
	.p2align 3
.L247:
	movq	0(%r13), %r12
.L193:
	testq	%r12, %r12
	je	.L201
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L201:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L192:
	movq	152(%rbx), %r13
	testq	%r13, %r13
	je	.L202
	movq	8(%r13), %r14
	movq	0(%r13), %r12
	cmpq	%r12, %r14
	jne	.L212
	testq	%r12, %r12
	je	.L213
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L213:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L202:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L214
	call	_ZdlPv@PLT
.L214:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L215
	call	_ZdlPv@PLT
.L215:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L191
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L206
	call	_ZdlPv@PLT
.L206:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L207
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L208
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L207:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L209
	call	_ZdlPv@PLT
.L209:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L210
	call	_ZdlPv@PLT
.L210:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L211
	call	_ZdlPv@PLT
.L211:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L204:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L249
.L212:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L204
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L250
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r12, %r14
	jne	.L212
	.p2align 4,,10
	.p2align 3
.L249:
	movq	0(%r13), %r12
	testq	%r12, %r12
	jne	.L251
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L191:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	.cfi_restore_state
	call	*%rax
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L197:
	call	*%rax
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L208:
	call	*%rax
	jmp	.L207
	.cfi_endproc
.LFE5513:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD2Ev
	.section	.text._ZN12v8_inspector9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8224:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$104, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -112(%rbp)
	movq	8(%r12), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, -80(%rbp)
	leaq	-64(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-80(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -72(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS4_14ConsoleContextEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation(%rip), %rcx
	movq	-80(%rbp), %rdi
	movq	%r12, %rdx
	movl	%eax, -68(%rbp)
	movl	%eax, %esi
	leaq	-112(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rax, -64(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS6_14ConsoleContextEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L252
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L252:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L259
	addq	$104, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L259:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8224:
	.size	_ZN12v8_inspector9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.text._ZN12v8_inspector9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$104, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -112(%rbp)
	movq	8(%r12), %rdi
	movq	%rdi, -96(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, -80(%rbp)
	leaq	-64(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-80(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -72(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN12v8_inspector9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS4_14ConsoleContextEEUlPNS1_22V8InspectorSessionImplEE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation(%rip), %rcx
	movq	-80(%rbp), %rdi
	movq	%r12, %rdx
	movl	%eax, -68(%rbp)
	movl	%eax, %esi
	leaq	-112(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%rax, -64(%rbp)
	leaq	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS6_14ConsoleContextEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_(%rip), %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl14forEachSessionEiRKSt8functionIFvPNS_22V8InspectorSessionImplEEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L260
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L260:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L267
	addq	$104, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L267:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8229:
	.size	_ZN12v8_inspector9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.text._ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev:
.LFB5515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	160(%rdi), %rax
	movq	%rdi, -64(%rbp)
	movq	%rcx, (%rdi)
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L269
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -112(%rbp)
	movq	%rax, -72(%rbp)
	cmpq	%rax, %rbx
	je	.L270
	.p2align 4,,10
	.p2align 3
.L477:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L271
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L272
	movq	16(%rcx), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%rbx, %rbx
	je	.L273
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%rcx, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L274
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L275
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rdx
	je	.L276
	movq	%rbx, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L333:
	movq	-80(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L277
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L278
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L279
	movq	(%r12), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L280
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L281
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L282
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rsi, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L973:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L285
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L286
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L285:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L287
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L288
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L287:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L283:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L972
.L289:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L283
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L973
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L289
	.p2align 4,,10
	.p2align 3
.L972:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L282:
	testq	%rbx, %rbx
	je	.L290
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L290:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L281:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L291
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L292
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L975:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L295
	call	_ZdlPv@PLT
.L295:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L296
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L297
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L296:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L298
	call	_ZdlPv@PLT
.L298:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L299
	call	_ZdlPv@PLT
.L299:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L300
	call	_ZdlPv@PLT
.L300:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L293:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L974
.L301:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L293
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L975
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L301
	.p2align 4,,10
	.p2align 3
.L974:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L292:
	testq	%r14, %r14
	je	.L302
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L302:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L291:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L303
	call	_ZdlPv@PLT
.L303:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L304
	call	_ZdlPv@PLT
.L304:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L305
	call	_ZdlPv@PLT
.L305:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L279:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L306
	movq	(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L307
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L308
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L309
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L977:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L312
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L313
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L312:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L314
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L315
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L314:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L310:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L976
.L316:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L310
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L977
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L316
	.p2align 4,,10
	.p2align 3
.L976:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L309:
	testq	%rbx, %rbx
	je	.L317
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L317:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L308:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L318
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L319
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L979:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L322
	call	_ZdlPv@PLT
.L322:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L323
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L324
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L323:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L325
	call	_ZdlPv@PLT
.L325:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L327
	call	_ZdlPv@PLT
.L327:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L320:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L978
.L328:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L320
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L979
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L328
	.p2align 4,,10
	.p2align 3
.L978:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L319:
	testq	%r14, %r14
	je	.L329
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L329:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L318:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L330
	call	_ZdlPv@PLT
.L330:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L331
	call	_ZdlPv@PLT
.L331:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L332
	call	_ZdlPv@PLT
.L332:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L306:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L277:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	jne	.L333
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
.L276:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L334
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L334:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L275:
	movq	152(%rbx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L335
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -104(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L336
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L370:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L337
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L338
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L339
	call	_ZdlPv@PLT
.L339:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L340
	movq	0(%r13), %rax
	movq	-56(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L341
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L342
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L343
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -136(%rbp)
	movq	%r13, -152(%rbp)
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L981:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L346
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L347
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L346:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L348
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L349
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L348:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L344:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L980
.L350:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L344
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L981
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L350
	.p2align 4,,10
	.p2align 3
.L980:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r13
	movq	(%rax), %r14
.L343:
	testq	%r14, %r14
	je	.L351
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L351:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L342:
	movq	152(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L352
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L353
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L983:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L356
	call	_ZdlPv@PLT
.L356:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L357
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L358
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L357:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L359
	call	_ZdlPv@PLT
.L359:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L360
	call	_ZdlPv@PLT
.L360:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L361
	call	_ZdlPv@PLT
.L361:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L354:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L982
.L362:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L354
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L983
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L362
	.p2align 4,,10
	.p2align 3
.L982:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L353:
	testq	%r14, %r14
	je	.L363
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L363:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L352:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L364
	call	_ZdlPv@PLT
.L364:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L365
	call	_ZdlPv@PLT
.L365:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L366
	call	_ZdlPv@PLT
.L366:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L340:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L367
	call	_ZdlPv@PLT
.L367:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L368
	call	_ZdlPv@PLT
.L368:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L369
	call	_ZdlPv@PLT
.L369:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L337:
	addq	$8, %rbx
	cmpq	%rbx, -104(%rbp)
	jne	.L370
	movq	-80(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L336:
	testq	%rdi, %rdi
	je	.L371
	call	_ZdlPv@PLT
.L371:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L335:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L372
	call	_ZdlPv@PLT
.L372:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L373
	call	_ZdlPv@PLT
.L373:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L374
	call	_ZdlPv@PLT
.L374:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L273:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L375
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%rsi, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L376
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L377
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -120(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rsi
	je	.L378
	movq	%rbx, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L435:
	movq	-80(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L379
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L380
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L381
	movq	(%r12), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L382
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L383
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L384
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rsi, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L985:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L387
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L388
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L387:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L389
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L390
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L389:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L385:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L984
.L391:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L385
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L985
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L391
	.p2align 4,,10
	.p2align 3
.L984:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L384:
	testq	%rbx, %rbx
	je	.L392
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L392:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L383:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L393
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L394
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L987:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L397
	call	_ZdlPv@PLT
.L397:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L398
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L399
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L398:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L400
	call	_ZdlPv@PLT
.L400:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L401
	call	_ZdlPv@PLT
.L401:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L402
	call	_ZdlPv@PLT
.L402:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L395:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L986
.L403:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L395
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L987
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L403
	.p2align 4,,10
	.p2align 3
.L986:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L394:
	testq	%r14, %r14
	je	.L404
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L404:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L393:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L405
	call	_ZdlPv@PLT
.L405:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L406
	call	_ZdlPv@PLT
.L406:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L407
	call	_ZdlPv@PLT
.L407:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L381:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L408
	movq	(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L409
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L410
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L411
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L989:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L414
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L415
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L414:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L416
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L417
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L416:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L412:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L988
.L418:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L412
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L989
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L418
	.p2align 4,,10
	.p2align 3
.L988:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L411:
	testq	%rbx, %rbx
	je	.L419
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L419:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L410:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L420
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L421
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L991:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L424
	call	_ZdlPv@PLT
.L424:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L425
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L426
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L425:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L427
	call	_ZdlPv@PLT
.L427:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L428
	call	_ZdlPv@PLT
.L428:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L429
	call	_ZdlPv@PLT
.L429:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L422:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L990
.L430:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L422
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L991
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L430
	.p2align 4,,10
	.p2align 3
.L990:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L421:
	testq	%r14, %r14
	je	.L431
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L431:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L420:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L432
	call	_ZdlPv@PLT
.L432:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L433
	call	_ZdlPv@PLT
.L433:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L434
	call	_ZdlPv@PLT
.L434:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L408:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L379:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	jne	.L435
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
.L378:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L436
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L436:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L377:
	movq	152(%rbx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L437
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -104(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L438
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L472:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L439
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L440
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L441
	call	_ZdlPv@PLT
.L441:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L442
	movq	0(%r13), %rax
	movq	-56(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L443
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L444
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L445
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -136(%rbp)
	movq	%r13, -152(%rbp)
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L993:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L448
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L449
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L448:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L450
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L451
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L450:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L446:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L992
.L452:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L446
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L993
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L452
	.p2align 4,,10
	.p2align 3
.L992:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r13
	movq	(%rax), %r14
.L445:
	testq	%r14, %r14
	je	.L453
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L453:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L444:
	movq	152(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L454
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L455
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L995:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L458
	call	_ZdlPv@PLT
.L458:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L459
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L460
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L459:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L461
	call	_ZdlPv@PLT
.L461:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L462
	call	_ZdlPv@PLT
.L462:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L463
	call	_ZdlPv@PLT
.L463:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L456:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L994
.L464:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L456
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L995
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L464
	.p2align 4,,10
	.p2align 3
.L994:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L455:
	testq	%r14, %r14
	je	.L465
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L465:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L454:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L466
	call	_ZdlPv@PLT
.L466:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L467
	call	_ZdlPv@PLT
.L467:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L468
	call	_ZdlPv@PLT
.L468:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L442:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L469
	call	_ZdlPv@PLT
.L469:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L470
	call	_ZdlPv@PLT
.L470:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L471
	call	_ZdlPv@PLT
.L471:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L439:
	addq	$8, %rbx
	cmpq	%rbx, -104(%rbp)
	jne	.L472
	movq	-80(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L438:
	testq	%rdi, %rdi
	je	.L473
	call	_ZdlPv@PLT
.L473:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L437:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L474
	call	_ZdlPv@PLT
.L474:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L475
	call	_ZdlPv@PLT
.L475:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L476
	call	_ZdlPv@PLT
.L476:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L375:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L271:
	addq	$8, -72(%rbp)
	movq	-72(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L477
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
.L270:
	movq	-72(%rbp), %rax
	testq	%rax, %rax
	je	.L478
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L478:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L269:
	movq	-64(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L479
	movq	8(%rax), %rbx
	movq	(%rax), %rax
	movq	%rbx, -112(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rbx
	je	.L480
	.p2align 4,,10
	.p2align 3
.L589:
	movq	-56(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L481
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L482
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L483
	call	_ZdlPv@PLT
.L483:
	movq	136(%rbx), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L484
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%rsi, -88(%rbp)
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L485
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L486
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -120(%rbp)
	movq	%rax, -80(%rbp)
	cmpq	%rax, %rcx
	je	.L487
	movq	%rbx, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L544:
	movq	-80(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L488
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L489
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L490
	movq	(%r12), %rax
	movq	-88(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L491
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L492
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L493
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rsi, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L997:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L496
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L497
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L496:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L498
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L499
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L498:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L494:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L996
.L500:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L494
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L997
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L500
	.p2align 4,,10
	.p2align 3
.L996:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L493:
	testq	%rbx, %rbx
	je	.L501
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L501:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L492:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L502
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L503
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L999:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L506
	call	_ZdlPv@PLT
.L506:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L507
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-88(%rbp), %rax
	jne	.L508
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L507:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L509
	call	_ZdlPv@PLT
.L509:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L510
	call	_ZdlPv@PLT
.L510:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L511
	call	_ZdlPv@PLT
.L511:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L504:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L998
.L512:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L504
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L999
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L512
	.p2align 4,,10
	.p2align 3
.L998:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L503:
	testq	%r14, %r14
	je	.L513
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L513:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L502:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L514
	call	_ZdlPv@PLT
.L514:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L515
	call	_ZdlPv@PLT
.L515:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L516
	call	_ZdlPv@PLT
.L516:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L490:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L517
	movq	(%r12), %rax
	movq	-88(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L518
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L519
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L520
	movq	%r12, -144(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -136(%rbp)
	movq	%r14, -152(%rbp)
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L523
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L524
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L523:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L525
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L526
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L525:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L521:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L1000
.L527:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L521
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1001
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L527
	.p2align 4,,10
	.p2align 3
.L1000:
	movq	-152(%rbp), %r14
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r14), %rbx
.L520:
	testq	%rbx, %rbx
	je	.L528
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L528:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L519:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L529
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L530
	movq	%r13, -136(%rbp)
	movq	%r12, -144(%rbp)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L1003:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L533
	call	_ZdlPv@PLT
.L533:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L534
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-88(%rbp), %rax
	jne	.L535
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L534:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L536
	call	_ZdlPv@PLT
.L536:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L537
	call	_ZdlPv@PLT
.L537:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L538
	call	_ZdlPv@PLT
.L538:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L531:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L1002
.L539:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L531
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1003
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L539
	.p2align 4,,10
	.p2align 3
.L1002:
	movq	-136(%rbp), %r13
	movq	-144(%rbp), %r12
	movq	(%r15), %r14
.L530:
	testq	%r14, %r14
	je	.L540
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L540:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L529:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L541
	call	_ZdlPv@PLT
.L541:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L542
	call	_ZdlPv@PLT
.L542:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L543
	call	_ZdlPv@PLT
.L543:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L517:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L488:
	addq	$8, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	jne	.L544
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -80(%rbp)
.L487:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L545
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L545:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L486:
	movq	-72(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L546
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -104(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L547
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L581:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L548
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L549
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L550
	call	_ZdlPv@PLT
.L550:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L551
	movq	0(%r13), %rax
	movq	-88(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L552
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L553
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L554
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -136(%rbp)
	movq	%r13, -152(%rbp)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L557
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L558
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L557:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L559
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L560
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L559:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L555:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1004
.L561:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L555
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L1005
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L561
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	-152(%rbp), %r13
	movq	(%rax), %r14
.L554:
	testq	%r14, %r14
	je	.L562
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L562:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L553:
	movq	152(%r13), %rax
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L563
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L564
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L1007:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L567
	call	_ZdlPv@PLT
.L567:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L568
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-88(%rbp), %rax
	jne	.L569
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L568:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L570
	call	_ZdlPv@PLT
.L570:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L571
	call	_ZdlPv@PLT
.L571:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L572
	call	_ZdlPv@PLT
.L572:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L565:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L1006
.L573:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L565
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1007
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L573
	.p2align 4,,10
	.p2align 3
.L1006:
	movq	-120(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L564:
	testq	%r14, %r14
	je	.L574
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L574:
	movq	-120(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L563:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L575
	call	_ZdlPv@PLT
.L575:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L576
	call	_ZdlPv@PLT
.L576:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L577
	call	_ZdlPv@PLT
.L577:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L551:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L578
	call	_ZdlPv@PLT
.L578:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L579
	call	_ZdlPv@PLT
.L579:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L580
	call	_ZdlPv@PLT
.L580:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L548:
	addq	$8, %rbx
	cmpq	%rbx, -104(%rbp)
	jne	.L581
	movq	-80(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L547:
	testq	%rdi, %rdi
	je	.L582
	call	_ZdlPv@PLT
.L582:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L546:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L583
	call	_ZdlPv@PLT
.L583:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L584
	call	_ZdlPv@PLT
.L584:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L585
	call	_ZdlPv@PLT
.L585:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L484:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L586
	call	_ZdlPv@PLT
.L586:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L587
	call	_ZdlPv@PLT
.L587:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L588
	call	_ZdlPv@PLT
.L588:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L481:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -112(%rbp)
	jne	.L589
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L480:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L590
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L590:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L479:
	movq	-64(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L591
	call	_ZdlPv@PLT
.L591:
	movq	-64(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L592
	call	_ZdlPv@PLT
.L592:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L593
	call	_ZdlPv@PLT
.L593:
	movq	-64(%rbp), %rdi
	addq	$120, %rsp
	movl	$168, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L482:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L549:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L489:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L338:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L440:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L380:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L485:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L376:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L518:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L280:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L409:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L341:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L491:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L524:
	call	*%rdx
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L449:
	call	*%rdx
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L415:
	call	*%rdx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L390:
	call	*%rdx
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L286:
	call	*%rdx
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L288:
	call	*%rdx
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L349:
	call	*%rdx
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L388:
	call	*%rdx
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L347:
	call	*%rdx
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L324:
	call	*%rax
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L315:
	call	*%rdx
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L558:
	call	*%rdx
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L497:
	call	*%rdx
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L526:
	call	*%rdx
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L499:
	call	*%rdx
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L426:
	call	*%rax
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L535:
	call	*%rax
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L399:
	call	*%rax
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L358:
	call	*%rax
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L460:
	call	*%rax
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L297:
	call	*%rax
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L569:
	call	*%rax
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L508:
	call	*%rax
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L417:
	call	*%rdx
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L313:
	call	*%rdx
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L451:
	call	*%rdx
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L560:
	call	*%rdx
	jmp	.L559
	.cfi_endproc
.LFE5515:
	.size	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev:
.LFB5626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L1009
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r13
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1010
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1011
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	movq	%rdx, -56(%rbp)
	cmpq	%r14, %rdx
	jne	.L1019
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1126:
	movq	16(%r15), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%rdi, %rdi
	je	.L1015
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1016
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1015:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1017
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1018
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1017:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1013:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L1125
.L1019:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L1013
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1126
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L1019
	.p2align 4,,10
	.p2align 3
.L1125:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L1012:
	testq	%r14, %r14
	je	.L1020
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1020:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1011:
	movq	152(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1021
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	movq	%rcx, -56(%rbp)
	cmpq	%r14, %rcx
	jne	.L1031
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1128:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1025
	call	_ZdlPv@PLT
.L1025:
	movq	136(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1026
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1027
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1026:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1028
	call	_ZdlPv@PLT
.L1028:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1029
	call	_ZdlPv@PLT
.L1029:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1030
	call	_ZdlPv@PLT
.L1030:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1023:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L1127
.L1031:
	movq	(%r14), %r15
	testq	%r15, %r15
	je	.L1023
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1128
	movq	%r15, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L1031
	.p2align 4,,10
	.p2align 3
.L1127:
	movq	-64(%rbp), %rax
	movq	(%rax), %r14
.L1022:
	testq	%r14, %r14
	je	.L1032
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1032:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1021:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1033
	call	_ZdlPv@PLT
.L1033:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1034
	call	_ZdlPv@PLT
.L1034:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1035
	call	_ZdlPv@PLT
.L1035:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1009:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L1008
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r13
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1037
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L1038
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	jne	.L1046
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1130:
	movq	16(%r8), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r8)
	testq	%rdi, %rdi
	je	.L1042
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1043
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L1042:
	movq	8(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1044
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1045
	movq	%r8, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-56(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %r8
.L1044:
	movl	$24, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L1040:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L1129
.L1046:
	movq	(%rbx), %r8
	testq	%r8, %r8
	je	.L1040
	movq	(%r8), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1130
	addq	$8, %rbx
	movq	%r8, %rdi
	call	*%rax
	cmpq	%rbx, %r15
	jne	.L1046
	.p2align 4,,10
	.p2align 3
.L1129:
	movq	(%r14), %rbx
.L1039:
	testq	%rbx, %rbx
	je	.L1047
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L1047:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1038:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L1048
	movq	8(%r15), %rax
	movq	(%r15), %r14
	movq	%rax, -56(%rbp)
	cmpq	%r14, %rax
	jne	.L1058
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1132:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1052
	call	_ZdlPv@PLT
.L1052:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1053
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%r13, %rax
	jne	.L1054
	movq	%rdi, -64(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1053:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1055
	call	_ZdlPv@PLT
.L1055:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1056
	call	_ZdlPv@PLT
.L1056:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1057
	call	_ZdlPv@PLT
.L1057:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1050:
	addq	$8, %r14
	cmpq	%r14, -56(%rbp)
	je	.L1131
.L1058:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L1050
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1132
	movq	%rbx, %rdi
	addq	$8, %r14
	call	*%rax
	cmpq	%r14, -56(%rbp)
	jne	.L1058
	.p2align 4,,10
	.p2align 3
.L1131:
	movq	(%r15), %r14
.L1049:
	testq	%r14, %r14
	je	.L1059
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1059:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1048:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1060
	call	_ZdlPv@PLT
.L1060:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1061
	call	_ZdlPv@PLT
.L1061:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1062
	call	_ZdlPv@PLT
.L1062:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1008:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1010:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1037:
	addq	$40, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1016:
	.cfi_restore_state
	call	*%rax
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1018:
	call	*%rax
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1027:
	call	*%rax
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	%r8, -56(%rbp)
	call	*%rax
	movq	-56(%rbp), %r8
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L1054:
	call	*%rax
	jmp	.L1053
	.cfi_endproc
.LFE5626:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev:
.LFB5586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	168(%rbx), %rax
	subq	$40, %rsp
	movq	%r15, (%rdi)
	movq	152(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L1134
	call	_ZdlPv@PLT
.L1134:
	movq	136(%rbx), %r12
	testq	%r12, %r12
	je	.L1135
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1136
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	movq	160(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1137
	movq	8(%rax), %rsi
	movq	(%rax), %r13
	movq	%rsi, -56(%rbp)
	cmpq	%r13, %rsi
	jne	.L1145
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	16(%r14), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L1141
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1142
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1141:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1143
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1144
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1143:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1139:
	addq	$8, %r13
	cmpq	%r13, -56(%rbp)
	je	.L1198
.L1145:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L1139
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1199
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -56(%rbp)
	jne	.L1145
	.p2align 4,,10
	.p2align 3
.L1198:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
.L1138:
	testq	%r13, %r13
	je	.L1146
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1146:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1137:
	movq	152(%r12), %rax
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L1147
	movq	8(%rax), %rcx
	movq	(%rax), %r13
	movq	%rcx, -56(%rbp)
	cmpq	%r13, %rcx
	jne	.L1157
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1201:
	movq	152(%r14), %rdi
	leaq	168(%r14), %rax
	movq	%r15, (%r14)
	cmpq	%rax, %rdi
	je	.L1151
	call	_ZdlPv@PLT
.L1151:
	movq	136(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1152
	movq	(%rdi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1153
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1152:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1154
	call	_ZdlPv@PLT
.L1154:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1155
	call	_ZdlPv@PLT
.L1155:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1156
	call	_ZdlPv@PLT
.L1156:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1149:
	addq	$8, %r13
	cmpq	%r13, -56(%rbp)
	je	.L1200
.L1157:
	movq	0(%r13), %r14
	testq	%r14, %r14
	je	.L1149
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1201
	movq	%r14, %rdi
	addq	$8, %r13
	call	*%rax
	cmpq	%r13, -56(%rbp)
	jne	.L1157
	.p2align 4,,10
	.p2align 3
.L1200:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
.L1148:
	testq	%r13, %r13
	je	.L1158
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1158:
	movq	-64(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1147:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1159
	call	_ZdlPv@PLT
.L1159:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1160
	call	_ZdlPv@PLT
.L1160:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1161
	call	_ZdlPv@PLT
.L1161:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1135:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1162
	call	_ZdlPv@PLT
.L1162:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1163
	call	_ZdlPv@PLT
.L1163:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L1133
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1133:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1136:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1153:
	call	*%rax
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1144:
	call	*%rax
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1142:
	call	*%rax
	jmp	.L1141
	.cfi_endproc
.LFE5586:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB5332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	leaq	-64(%rax), %rsi
	movq	%rax, %xmm1
	movq	%rsi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	312(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L1203
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1204
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1205
	call	_ZdlPv@PLT
.L1205:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1206
	call	_ZdlPv@PLT
.L1206:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1203:
	movq	304(%r14), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L1207
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1208
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L1209
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -96(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rcx
	je	.L1210
	movq	%r14, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L1492:
	movq	-56(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -80(%rbp)
	testq	%rcx, %rcx
	je	.L1211
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1212
	movq	16(%rcx), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%rbx, %rbx
	je	.L1213
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1214
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L1215
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L1216
	movq	%rbx, -120(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1217
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1218
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1219
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1220
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1221
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L1222
	movq	%rbx, -136(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -128(%rbp)
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L2307:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1225
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1226
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1225:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L1227
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1228
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1227:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1223:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2306
.L1229:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1223
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2307
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L2318:
	movq	-120(%rbp), %r13
	movq	0(%r13), %r12
.L1470:
	testq	%r12, %r12
	je	.L1480
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1480:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1469:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1481
	call	_ZdlPv@PLT
.L1481:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1482
	call	_ZdlPv@PLT
.L1482:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1483
	call	_ZdlPv@PLT
.L1483:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1407:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1484
	call	_ZdlPv@PLT
.L1484:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1485
	call	_ZdlPv@PLT
.L1485:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1486
	call	_ZdlPv@PLT
.L1486:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1404:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	jne	.L1487
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L1403:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1488
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1488:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1402:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1489
	call	_ZdlPv@PLT
.L1489:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1490
	call	_ZdlPv@PLT
.L1490:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1491
	call	_ZdlPv@PLT
.L1491:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1340:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1211:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -96(%rbp)
	jne	.L1492
	movq	-112(%rbp), %rax
	movq	-152(%rbp), %r14
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L1210:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1493
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1493:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1209:
	movq	-72(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L1494
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -96(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rdx
	je	.L1495
	movq	%r14, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L1704:
	movq	-56(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L1496
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1497
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	168(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1498
	call	_ZdlPv@PLT
.L1498:
	movq	136(%r13), %rcx
	movq	%rcx, -80(%rbp)
	testq	%rcx, %rcx
	je	.L1499
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1500
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L1501
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -104(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rsi
	je	.L1502
	movq	%r13, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L1634:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1503
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1504
	movq	16(%rbx), %rsi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%rsi, -88(%rbp)
	testq	%rsi, %rsi
	je	.L1505
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1506
	movq	160(%rsi), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	testq	%r12, %r12
	je	.L1507
	movq	8(%r12), %r15
	movq	(%r12), %r14
	cmpq	%r14, %r15
	je	.L1508
	movq	%r12, -120(%rbp)
	jmp	.L1515
	.p2align 4,,10
	.p2align 3
.L2309:
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L1511
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1512
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1511:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L1513
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1514
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1513:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1509:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2308
.L1515:
	movq	(%r14), %r13
	testq	%r13, %r13
	je	.L1509
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2309
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1509
	.p2align 4,,10
	.p2align 3
.L2322:
	movq	-120(%rbp), %r13
	movq	0(%r13), %r12
.L1679:
	testq	%r12, %r12
	je	.L1689
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1689:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1678:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1690
	call	_ZdlPv@PLT
.L1690:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1691
	call	_ZdlPv@PLT
.L1691:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1692
	call	_ZdlPv@PLT
.L1692:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1641:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1693
	call	_ZdlPv@PLT
.L1693:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1694
	call	_ZdlPv@PLT
.L1694:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1695
	call	_ZdlPv@PLT
.L1695:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1638:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	jne	.L1696
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %r13
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L1637:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1697
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1697:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1636:
	movq	-80(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1698
	call	_ZdlPv@PLT
.L1698:
	movq	-80(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1699
	call	_ZdlPv@PLT
.L1699:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1700
	call	_ZdlPv@PLT
.L1700:
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1499:
	movq	96(%r13), %rdi
	leaq	112(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1701
	call	_ZdlPv@PLT
.L1701:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1702
	call	_ZdlPv@PLT
.L1702:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1703
	call	_ZdlPv@PLT
.L1703:
	movl	$192, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1496:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -96(%rbp)
	jne	.L1704
	movq	-112(%rbp), %rax
	movq	-152(%rbp), %r14
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L1495:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L1705
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1705:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1494:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1706
	call	_ZdlPv@PLT
.L1706:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1707
	call	_ZdlPv@PLT
.L1707:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1708
	call	_ZdlPv@PLT
.L1708:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1207:
	movq	264(%r14), %rdi
	leaq	280(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1709
	call	_ZdlPv@PLT
.L1709:
	movq	216(%r14), %rdi
	leaq	232(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1710
	call	_ZdlPv@PLT
.L1710:
	movq	168(%r14), %rdi
	leaq	184(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1711
	call	_ZdlPv@PLT
.L1711:
	movq	152(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1712
	movq	(%rdi), %rax
	call	*24(%rax)
.L1712:
	movq	112(%r14), %rdi
	leaq	128(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1713
	call	_ZdlPv@PLT
.L1713:
	movq	64(%r14), %rdi
	leaq	80(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1714
	call	_ZdlPv@PLT
.L1714:
	movq	16(%r14), %rdi
	leaq	32(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1715
	call	_ZdlPv@PLT
.L1715:
	addq	$152, %rsp
	movq	%r14, %rdi
	movl	$320, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L2346:
	.cfi_restore_state
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L1612:
	testq	%r14, %r14
	je	.L1622
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1622:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1611:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1623
	call	_ZdlPv@PLT
.L1623:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1624
	call	_ZdlPv@PLT
.L1624:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1625
	call	_ZdlPv@PLT
.L1625:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1599:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1626
	call	_ZdlPv@PLT
.L1626:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1627
	call	_ZdlPv@PLT
.L1627:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1628
	call	_ZdlPv@PLT
.L1628:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1596:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L1629
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1595:
	testq	%rdi, %rdi
	je	.L1630
	call	_ZdlPv@PLT
.L1630:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1594:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1631
	call	_ZdlPv@PLT
.L1631:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1632
	call	_ZdlPv@PLT
.L1632:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1633
	call	_ZdlPv@PLT
.L1633:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1557:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1503:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	jne	.L1634
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %r13
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L1502:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1635
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1635:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1501:
	movq	-80(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L1636
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -104(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L1637
	movq	%r13, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1638
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1639
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1640
	call	_ZdlPv@PLT
.L1640:
	movq	136(%rbx), %rdx
	movq	%rdx, -88(%rbp)
	testq	%rdx, %rdx
	je	.L1641
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1642
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L1643
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L1644
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1676:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1645
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1646
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1647
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1648
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1649
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L1650
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L2311:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1653
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1654
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1653:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1655
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1656
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1655:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1651:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2310
.L1657:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1651
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2311
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1218:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1217:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L1273
	movq	-88(%rbp), %rax
	movq	-120(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1216:
	testq	%rdi, %rdi
	je	.L1274
	call	_ZdlPv@PLT
.L1274:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1215:
	movq	152(%rbx), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L1275
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -104(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rsi
	je	.L1276
	movq	%rbx, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L1335:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1277
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1278
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1279
	call	_ZdlPv@PLT
.L1279:
	movq	136(%rbx), %rsi
	movq	%rsi, -88(%rbp)
	testq	%rsi, %rsi
	je	.L1280
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1281
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L1282
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L1283
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1315:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1284
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1285
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1286
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1287
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1286:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1288
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1289
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1290
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L1291
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L2313:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1294
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1295
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1294:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1296
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1297
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1296:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1292:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2312
.L1298:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1292
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2313
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L2320:
	movq	-120(%rbp), %r13
	movq	0(%r13), %r12
.L1318:
	testq	%r12, %r12
	je	.L1328
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1328:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1317:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1329
	call	_ZdlPv@PLT
.L1329:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1330
	call	_ZdlPv@PLT
.L1330:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1331
	call	_ZdlPv@PLT
.L1331:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1280:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1332
	call	_ZdlPv@PLT
.L1332:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1333
	call	_ZdlPv@PLT
.L1333:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1334
	call	_ZdlPv@PLT
.L1334:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1277:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -104(%rbp)
	jne	.L1335
	movq	-128(%rbp), %rax
	movq	-160(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L1276:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L1336
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L1336:
	movq	-128(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1275:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1337
	call	_ZdlPv@PLT
.L1337:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1338
	call	_ZdlPv@PLT
.L1338:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1339
	call	_ZdlPv@PLT
.L1339:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L1213:
	movq	-80(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1340
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1341
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L1342
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -64(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L1343
	movq	%rbx, -120(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1400:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1344
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1345
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1346
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1347
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1348
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L1349
	movq	%rbx, -136(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -128(%rbp)
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L2315:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1352
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1353
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1352:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L1354
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1355
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1354:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1350:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2314
.L1356:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1350
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2315
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1345:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1344:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L1400
	movq	-88(%rbp), %rax
	movq	-120(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1343:
	testq	%rdi, %rdi
	je	.L1401
	call	_ZdlPv@PLT
.L1401:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1342:
	movq	152(%rbx), %rax
	movq	%rax, -128(%rbp)
	testq	%rax, %rax
	je	.L1402
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -104(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rdx
	je	.L1403
	movq	%rbx, -160(%rbp)
	.p2align 4,,10
	.p2align 3
.L1487:
	movq	-64(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1404
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1405
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1406
	call	_ZdlPv@PLT
.L1406:
	movq	136(%rbx), %rdx
	movq	%rdx, -88(%rbp)
	testq	%rdx, %rdx
	je	.L1407
	movq	(%rdx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1408
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rdx)
	movq	160(%rdx), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L1409
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L1410
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1467:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1411
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1412
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1413
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1414
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1415
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L1416
	movq	%rbx, -184(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -176(%rbp)
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L2317:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1419
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1420
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1419:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L1421
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1422
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1421:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1417:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2316
.L1423:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1417
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2317
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1412:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1411:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L1467
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1410:
	testq	%rdi, %rdi
	je	.L1468
	call	_ZdlPv@PLT
.L1468:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1409:
	movq	-88(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L1469
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L1470
	movq	%r13, -120(%rbp)
	jmp	.L1479
	.p2align 4,,10
	.p2align 3
.L2319:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1473
	call	_ZdlPv@PLT
.L1473:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L1474
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1475
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1474:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1476
	call	_ZdlPv@PLT
.L1476:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1477
	call	_ZdlPv@PLT
.L1477:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1478
	call	_ZdlPv@PLT
.L1478:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1471:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2318
.L1479:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L1471
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2319
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1471
	.p2align 4,,10
	.p2align 3
.L1285:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1284:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L1315
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1283:
	testq	%rdi, %rdi
	je	.L1316
	call	_ZdlPv@PLT
.L1316:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1282:
	movq	-88(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L1317
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L1318
	movq	%r13, -120(%rbp)
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L2321:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1321
	call	_ZdlPv@PLT
.L1321:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L1322
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1323
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1322:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1324
	call	_ZdlPv@PLT
.L1324:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1325
	call	_ZdlPv@PLT
.L1325:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1326
	call	_ZdlPv@PLT
.L1326:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1319:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2320
.L1327:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L1319
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2321
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L2350:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L1660:
	testq	%r14, %r14
	je	.L1670
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1670:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1659:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1671
	call	_ZdlPv@PLT
.L1671:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1672
	call	_ZdlPv@PLT
.L1672:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1673
	call	_ZdlPv@PLT
.L1673:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1647:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1674
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1675
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1674:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1645:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L1676
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1644:
	testq	%rdi, %rdi
	je	.L1677
	call	_ZdlPv@PLT
.L1677:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1643:
	movq	-88(%rbp), %rax
	movq	152(%rax), %r13
	testq	%r13, %r13
	je	.L1678
	movq	8(%r13), %r15
	movq	0(%r13), %r12
	cmpq	%r12, %r15
	je	.L1679
	movq	%r13, -120(%rbp)
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L2323:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r14), %rdi
	movq	%rax, (%r14)
	leaq	168(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1682
	call	_ZdlPv@PLT
.L1682:
	movq	136(%r14), %r13
	testq	%r13, %r13
	je	.L1683
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1684
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1683:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1685
	call	_ZdlPv@PLT
.L1685:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1686
	call	_ZdlPv@PLT
.L1686:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1687
	call	_ZdlPv@PLT
.L1687:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1680:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2322
.L1688:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L1680
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2323
	movq	%r14, %rdi
	call	*%rax
	jmp	.L1680
	.p2align 4,,10
	.p2align 3
.L2344:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L1535:
	testq	%r14, %r14
	je	.L1545
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1545:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1534:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1546
	call	_ZdlPv@PLT
.L1546:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1547
	call	_ZdlPv@PLT
.L1547:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1548
	call	_ZdlPv@PLT
.L1548:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1522:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1549
	call	_ZdlPv@PLT
.L1549:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1550
	call	_ZdlPv@PLT
.L1550:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1551
	call	_ZdlPv@PLT
.L1551:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1519:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L1552
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1518:
	testq	%rdi, %rdi
	je	.L1553
	call	_ZdlPv@PLT
.L1553:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1517:
	movq	-88(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L1554
	call	_ZdlPv@PLT
.L1554:
	movq	-88(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L1555
	call	_ZdlPv@PLT
.L1555:
	movq	-88(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L1556
	call	_ZdlPv@PLT
.L1556:
	movq	-88(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L1505:
	movq	8(%rbx), %rcx
	movq	%rcx, -88(%rbp)
	testq	%rcx, %rcx
	je	.L1557
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1558
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L1559
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L1560
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1561
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1562
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1563
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1564
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1563:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1565
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1566
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1567
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L1568
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L2325:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1571
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1572
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1571:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1573
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1574
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1573:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1569:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2324
.L1575:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1569
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2325
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1569
	.p2align 4,,10
	.p2align 3
.L1562:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L1561:
	addq	$8, %rbx
	cmpq	%rbx, -120(%rbp)
	jne	.L1592
	movq	-136(%rbp), %rax
	movq	-168(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L1560:
	testq	%rdi, %rdi
	je	.L1593
	call	_ZdlPv@PLT
.L1593:
	movq	-136(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1559:
	movq	-88(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L1594
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L1595
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1596
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1597
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1598
	call	_ZdlPv@PLT
.L1598:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1599
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1600
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1601
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L1602
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1609
	.p2align 4,,10
	.p2align 3
.L2327:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1605
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1606
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1605:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1607
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1608
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1607:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1603:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2326
.L1609:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1603
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2327
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L2340:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	movq	(%rax), %r14
.L1359:
	testq	%r14, %r14
	je	.L1369
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1369:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1358:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1370
	call	_ZdlPv@PLT
.L1370:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1371
	call	_ZdlPv@PLT
.L1371:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1372
	call	_ZdlPv@PLT
.L1372:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1346:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1373
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1374
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1375
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L1376
	movq	%rbx, -136(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -128(%rbp)
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L2329:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1379
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1380
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1379:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L1381
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1382
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1381:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1377:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2328
.L1383:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1377
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2329
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L2338:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	movq	(%rax), %r14
.L1386:
	testq	%r14, %r14
	je	.L1396
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1396:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1385:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1397
	call	_ZdlPv@PLT
.L1397:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1398
	call	_ZdlPv@PLT
.L1398:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1399
	call	_ZdlPv@PLT
.L1399:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1373:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L2342:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	movq	(%rax), %r14
.L1232:
	testq	%r14, %r14
	je	.L1242
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1242:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1231:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1243
	call	_ZdlPv@PLT
.L1243:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1244
	call	_ZdlPv@PLT
.L1244:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1245
	call	_ZdlPv@PLT
.L1245:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1219:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1246
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1247
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1248
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L1249
	movq	%rbx, -136(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -128(%rbp)
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L2331:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1252
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1253
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1252:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L1254
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1255
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1254:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1250:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2330
.L1256:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1250
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2331
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L2336:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	movq	(%rax), %r14
.L1259:
	testq	%r14, %r14
	je	.L1269
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1269:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1258:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1270
	call	_ZdlPv@PLT
.L1270:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1271
	call	_ZdlPv@PLT
.L1271:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1272
	call	_ZdlPv@PLT
.L1272:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1246:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L2348:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L1578:
	testq	%r14, %r14
	je	.L1588
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1588:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1577:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1589
	call	_ZdlPv@PLT
.L1589:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1590
	call	_ZdlPv@PLT
.L1590:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1591
	call	_ZdlPv@PLT
.L1591:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1565:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1561
	.p2align 4,,10
	.p2align 3
.L2352:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L1426:
	testq	%r14, %r14
	je	.L1436
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1436:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1425:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1437
	call	_ZdlPv@PLT
.L1437:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1438
	call	_ZdlPv@PLT
.L1438:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1439
	call	_ZdlPv@PLT
.L1439:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1413:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1440
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1441
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1442
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L1443
	movq	%rbx, -184(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -176(%rbp)
	jmp	.L1450
	.p2align 4,,10
	.p2align 3
.L2333:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L1446
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1447
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1446:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L1448
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1449
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1448:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L1444:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L2332
.L1450:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L1444
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2333
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L2354:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L1453:
	testq	%r14, %r14
	je	.L1463
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1463:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1452:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1464
	call	_ZdlPv@PLT
.L1464:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1465
	call	_ZdlPv@PLT
.L1465:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1466
	call	_ZdlPv@PLT
.L1466:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1440:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L2356:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %r13
	movq	(%rax), %r14
.L1301:
	testq	%r14, %r14
	je	.L1311
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1311:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1300:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1312
	call	_ZdlPv@PLT
.L1312:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1313
	call	_ZdlPv@PLT
.L1313:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1314
	call	_ZdlPv@PLT
.L1314:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1288:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L2308:
	movq	-120(%rbp), %r12
	movq	(%r12), %r14
.L1508:
	testq	%r14, %r14
	je	.L1516
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1516:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1507:
	movq	-88(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L1517
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -120(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L1518
	movq	%rbx, -168(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1519
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1520
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1521
	call	_ZdlPv@PLT
.L1521:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1522
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1523
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L1524
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L1525
	movq	%rbx, -176(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -144(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1532
	.p2align 4,,10
	.p2align 3
.L2335:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L1528
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1529
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1528:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L1530
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L1531
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1530:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1526:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2334
.L1532:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L1526
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2335
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L1526
	.p2align 4,,10
	.p2align 3
.L2330:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	(%rax), %r14
.L1249:
	testq	%r14, %r14
	je	.L1257
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1257:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1248:
	movq	152(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1258
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1259
	movq	%r12, -128(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L1268
	.p2align 4,,10
	.p2align 3
.L2337:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1262
	call	_ZdlPv@PLT
.L1262:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1263
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1264
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1263:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1265
	call	_ZdlPv@PLT
.L1265:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1266
	call	_ZdlPv@PLT
.L1266:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1267
	call	_ZdlPv@PLT
.L1267:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1260:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2336
.L1268:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1260
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2337
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L2328:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	(%rax), %r14
.L1376:
	testq	%r14, %r14
	je	.L1384
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1384:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1375:
	movq	152(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1385
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1386
	movq	%r12, -128(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L2339:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1389
	call	_ZdlPv@PLT
.L1389:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1390
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1391
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1390:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1392
	call	_ZdlPv@PLT
.L1392:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1393
	call	_ZdlPv@PLT
.L1393:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1394
	call	_ZdlPv@PLT
.L1394:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1387:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2338
.L1395:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1387
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2339
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1387
	.p2align 4,,10
	.p2align 3
.L2314:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	(%rax), %r14
.L1349:
	testq	%r14, %r14
	je	.L1357
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1357:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1348:
	movq	152(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1358
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1359
	movq	%r12, -128(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L2341:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1362
	call	_ZdlPv@PLT
.L1362:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1363
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1364
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1363:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1365
	call	_ZdlPv@PLT
.L1365:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1366
	call	_ZdlPv@PLT
.L1366:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1367
	call	_ZdlPv@PLT
.L1367:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1360:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2340
.L1368:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1360
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2341
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L2306:
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	(%rax), %r14
.L1222:
	testq	%r14, %r14
	je	.L1230
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1230:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1221:
	movq	152(%r13), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L1231
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1232
	movq	%r12, -128(%rbp)
	movq	%r13, -136(%rbp)
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L2343:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1235
	call	_ZdlPv@PLT
.L1235:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1236
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1237
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1236:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1238
	call	_ZdlPv@PLT
.L1238:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1239
	call	_ZdlPv@PLT
.L1239:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1240
	call	_ZdlPv@PLT
.L1240:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1233:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2342
.L1241:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1233
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2343
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L2334:
	movq	-144(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L1525:
	testq	%r15, %r15
	je	.L1533
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1533:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1524:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1534
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1535
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L2345:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1538
	call	_ZdlPv@PLT
.L1538:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1539
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1540
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1539:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1541
	call	_ZdlPv@PLT
.L1541:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1542
	call	_ZdlPv@PLT
.L1542:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1543
	call	_ZdlPv@PLT
.L1543:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1536:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2344
.L1544:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1536
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2345
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L2326:
	movq	-144(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L1602:
	testq	%r15, %r15
	je	.L1610
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1610:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1601:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1611
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1612
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L2347:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1615
	call	_ZdlPv@PLT
.L1615:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1616
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1617
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1616:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1618
	call	_ZdlPv@PLT
.L1618:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1619
	call	_ZdlPv@PLT
.L1619:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1620
	call	_ZdlPv@PLT
.L1620:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1613:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2346
.L1621:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1613
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2347
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L2324:
	movq	-144(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L1568:
	testq	%r15, %r15
	je	.L1576
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1576:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1567:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1577
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1578
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L2349:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1581
	call	_ZdlPv@PLT
.L1581:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1582
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1583
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1582:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1584
	call	_ZdlPv@PLT
.L1584:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1585
	call	_ZdlPv@PLT
.L1585:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1586
	call	_ZdlPv@PLT
.L1586:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1579:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2348
.L1587:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1579
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2349
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L2310:
	movq	-144(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L1650:
	testq	%r15, %r15
	je	.L1658
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1658:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1649:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1659
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1660
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1669
	.p2align 4,,10
	.p2align 3
.L2351:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1663
	call	_ZdlPv@PLT
.L1663:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1664
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1665
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1664:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1666
	call	_ZdlPv@PLT
.L1666:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1667
	call	_ZdlPv@PLT
.L1667:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1668
	call	_ZdlPv@PLT
.L1668:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1661:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2350
.L1669:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1661
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2351
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1661
	.p2align 4,,10
	.p2align 3
.L2316:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %rbx
	movq	(%rax), %r14
.L1416:
	testq	%r14, %r14
	je	.L1424
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1424:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1415:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1425
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1426
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1435
	.p2align 4,,10
	.p2align 3
.L2353:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1429
	call	_ZdlPv@PLT
.L1429:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1430
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1431
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1430:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1432
	call	_ZdlPv@PLT
.L1432:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1433
	call	_ZdlPv@PLT
.L1433:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1434
	call	_ZdlPv@PLT
.L1434:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1427:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2352
.L1435:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1427
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2353
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1427
	.p2align 4,,10
	.p2align 3
.L2332:
	movq	-144(%rbp), %rax
	movq	-176(%rbp), %r12
	movq	-184(%rbp), %rbx
	movq	(%rax), %r14
.L1443:
	testq	%r14, %r14
	je	.L1451
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1451:
	movq	-144(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L1442:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1452
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1453
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L2355:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1456
	call	_ZdlPv@PLT
.L1456:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1457
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1458
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1457:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1459
	call	_ZdlPv@PLT
.L1459:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1460
	call	_ZdlPv@PLT
.L1460:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1461
	call	_ZdlPv@PLT
.L1461:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1454:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2354
.L1462:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1454
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2355
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L2312:
	movq	-144(%rbp), %r12
	movq	-176(%rbp), %rbx
	movq	-184(%rbp), %r13
	movq	(%r14), %r15
.L1291:
	testq	%r15, %r15
	je	.L1299
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1299:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1290:
	movq	152(%r13), %rax
	movq	%rax, -144(%rbp)
	testq	%rax, %rax
	je	.L1300
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L1301
	movq	%r12, -176(%rbp)
	movq	%r13, -184(%rbp)
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L2357:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1304
	call	_ZdlPv@PLT
.L1304:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1305
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1306
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1305:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1307
	call	_ZdlPv@PLT
.L1307:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1308
	call	_ZdlPv@PLT
.L1308:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1309
	call	_ZdlPv@PLT
.L1309:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1302:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2356
.L1310:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L1302
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2357
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1212:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1496
	.p2align 4,,10
	.p2align 3
.L1204:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1208:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1503
	.p2align 4,,10
	.p2align 3
.L1639:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1638
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1341:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1340
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1499
	.p2align 4,,10
	.p2align 3
.L1214:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1646:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1408:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1220:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1558:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L1557
	.p2align 4,,10
	.p2align 3
.L1347:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1642:
	movq	%rdx, %rdi
	call	*%rax
	jmp	.L1641
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1374:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1247:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1506:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L1505
	.p2align 4,,10
	.p2align 3
.L1512:
	call	*%rax
	jmp	.L1511
	.p2align 4,,10
	.p2align 3
.L1364:
	call	*%rax
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1228:
	call	*%rdx
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1380:
	call	*%rdx
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1523:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1382:
	call	*%rdx
	jmp	.L1381
	.p2align 4,,10
	.p2align 3
.L1684:
	call	*%rax
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1648:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1566:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1289:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1253:
	call	*%rdx
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1391:
	call	*%rax
	jmp	.L1390
	.p2align 4,,10
	.p2align 3
.L1264:
	call	*%rax
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1323:
	call	*%rax
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1255:
	call	*%rdx
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1514:
	call	*%rax
	jmp	.L1513
	.p2align 4,,10
	.p2align 3
.L1226:
	call	*%rdx
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1353:
	call	*%rdx
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1675:
	call	*%rax
	jmp	.L1674
	.p2align 4,,10
	.p2align 3
.L1475:
	call	*%rax
	jmp	.L1474
	.p2align 4,,10
	.p2align 3
.L1564:
	call	*%rax
	jmp	.L1563
	.p2align 4,,10
	.p2align 3
.L1287:
	call	*%rax
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1600:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L1599
	.p2align 4,,10
	.p2align 3
.L1237:
	call	*%rax
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1355:
	call	*%rdx
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1572:
	call	*%rdx
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1529:
	call	*%rdx
	jmp	.L1528
	.p2align 4,,10
	.p2align 3
.L1449:
	call	*%rdx
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1656:
	call	*%rdx
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1654:
	call	*%rdx
	jmp	.L1653
	.p2align 4,,10
	.p2align 3
.L1447:
	call	*%rdx
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1574:
	call	*%rdx
	jmp	.L1573
	.p2align 4,,10
	.p2align 3
.L1608:
	call	*%rdx
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1606:
	call	*%rdx
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1422:
	call	*%rdx
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1420:
	call	*%rdx
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1297:
	call	*%rdx
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1295:
	call	*%rdx
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1306:
	call	*%rax
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1531:
	call	*%rdx
	jmp	.L1530
	.p2align 4,,10
	.p2align 3
.L1617:
	call	*%rax
	jmp	.L1616
	.p2align 4,,10
	.p2align 3
.L1540:
	call	*%rax
	jmp	.L1539
	.p2align 4,,10
	.p2align 3
.L1458:
	call	*%rax
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1431:
	call	*%rax
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1665:
	call	*%rax
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1583:
	call	*%rax
	jmp	.L1582
	.cfi_endproc
.LFE5332:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12EntryPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev:
.LFB5628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r12
	movq	%rdi, -72(%rbp)
	movq	%rcx, (%rdi)
	testq	%r12, %r12
	je	.L2359
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r15
	movq	%r15, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L2360
	movq	160(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2361
	movq	8(%r13), %rbx
	movq	0(%r13), %r14
	cmpq	%r14, %rbx
	je	.L2362
	movq	%r12, -64(%rbp)
	movq	%r13, -80(%rbp)
	jmp	.L2369
	.p2align 4,,10
	.p2align 3
.L2641:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2365
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L2366
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2365:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2367
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%r15, %rax
	jne	.L2368
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2367:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2363:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2640
.L2369:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2363
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2641
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L2369
	.p2align 4,,10
	.p2align 3
.L2640:
	movq	-80(%rbp), %r13
	movq	-64(%rbp), %r12
	movq	0(%r13), %r14
.L2362:
	testq	%r14, %r14
	je	.L2370
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2370:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2361:
	movq	152(%r12), %r14
	testq	%r14, %r14
	je	.L2371
	movq	8(%r14), %rbx
	movq	(%r14), %r13
	cmpq	%r13, %rbx
	je	.L2372
	movq	%r12, -64(%rbp)
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2643:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r15), %rdi
	movq	%rax, (%r15)
	leaq	168(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2375
	call	_ZdlPv@PLT
.L2375:
	movq	136(%r15), %r12
	testq	%r12, %r12
	je	.L2376
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2377
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2376:
	movq	96(%r15), %rdi
	leaq	112(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2378
	call	_ZdlPv@PLT
.L2378:
	movq	48(%r15), %rdi
	leaq	64(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2379
	call	_ZdlPv@PLT
.L2379:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2380
	call	_ZdlPv@PLT
.L2380:
	movl	$192, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2373:
	addq	$8, %r13
	cmpq	%r13, %rbx
	je	.L2642
.L2381:
	movq	0(%r13), %r15
	testq	%r15, %r15
	je	.L2373
	movq	(%r15), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2643
	addq	$8, %r13
	movq	%r15, %rdi
	call	*%rax
	cmpq	%r13, %rbx
	jne	.L2381
	.p2align 4,,10
	.p2align 3
.L2642:
	movq	-64(%rbp), %r12
	movq	(%r14), %r13
.L2372:
	testq	%r13, %r13
	je	.L2382
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2382:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2371:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2383
	call	_ZdlPv@PLT
.L2383:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2384
	call	_ZdlPv@PLT
.L2384:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2385
	call	_ZdlPv@PLT
.L2385:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2359:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L2386
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%rcx, -56(%rbp)
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2387
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L2388
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -80(%rbp)
	movq	%rax, -64(%rbp)
	cmpq	%rax, %rcx
	je	.L2389
	movq	%rbx, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L2446:
	movq	-64(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L2390
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2391
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L2392
	movq	(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2393
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L2394
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L2395
	movq	%r12, -112(%rbp)
	movq	%rbx, %r12
	movq	%rcx, %rbx
	movq	%r13, -104(%rbp)
	movq	%r14, -120(%rbp)
	jmp	.L2402
	.p2align 4,,10
	.p2align 3
.L2645:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2398
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2399
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2398:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2400
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2401
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2400:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2396:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2644
.L2402:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2396
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2645
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L2402
	.p2align 4,,10
	.p2align 3
.L2644:
	movq	-120(%rbp), %r14
	movq	-104(%rbp), %r13
	movq	-112(%rbp), %r12
	movq	(%r14), %rbx
.L2395:
	testq	%rbx, %rbx
	je	.L2403
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L2403:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2394:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L2404
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L2405
	movq	%r13, -104(%rbp)
	movq	%r12, -112(%rbp)
	jmp	.L2414
	.p2align 4,,10
	.p2align 3
.L2647:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2408
	call	_ZdlPv@PLT
.L2408:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2409
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2410
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2409:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2411
	call	_ZdlPv@PLT
.L2411:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2412
	call	_ZdlPv@PLT
.L2412:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2413
	call	_ZdlPv@PLT
.L2413:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2406:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2646
.L2414:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2406
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2647
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L2414
	.p2align 4,,10
	.p2align 3
.L2646:
	movq	-104(%rbp), %r13
	movq	-112(%rbp), %r12
	movq	(%r15), %r14
.L2405:
	testq	%r14, %r14
	je	.L2415
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2415:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2404:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2416
	call	_ZdlPv@PLT
.L2416:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2417
	call	_ZdlPv@PLT
.L2417:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2418
	call	_ZdlPv@PLT
.L2418:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2392:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L2419
	movq	(%r12), %rax
	movq	-56(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2420
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L2421
	movq	8(%r14), %r15
	movq	(%r14), %rbx
	cmpq	%rbx, %r15
	je	.L2422
	movq	%r12, -112(%rbp)
	movq	%rbx, %r12
	movq	%rdx, %rbx
	movq	%r13, -104(%rbp)
	movq	%r14, -120(%rbp)
	jmp	.L2429
	.p2align 4,,10
	.p2align 3
.L2649:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2425
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2426
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2425:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2427
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L2428
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2427:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2423:
	addq	$8, %r12
	cmpq	%r12, %r15
	je	.L2648
.L2429:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L2423
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2649
	addq	$8, %r12
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%r12, %r15
	jne	.L2429
	.p2align 4,,10
	.p2align 3
.L2648:
	movq	-120(%rbp), %r14
	movq	-104(%rbp), %r13
	movq	-112(%rbp), %r12
	movq	(%r14), %rbx
.L2422:
	testq	%rbx, %rbx
	je	.L2430
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L2430:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2421:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L2431
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L2432
	movq	%r13, -104(%rbp)
	movq	%r12, -112(%rbp)
	jmp	.L2441
	.p2align 4,,10
	.p2align 3
.L2651:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2435
	call	_ZdlPv@PLT
.L2435:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2436
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2437
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2436:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2438
	call	_ZdlPv@PLT
.L2438:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2439
	call	_ZdlPv@PLT
.L2439:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2440
	call	_ZdlPv@PLT
.L2440:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2433:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2650
.L2441:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2433
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2651
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L2441
	.p2align 4,,10
	.p2align 3
.L2650:
	movq	-104(%rbp), %r13
	movq	-112(%rbp), %r12
	movq	(%r15), %r14
.L2432:
	testq	%r14, %r14
	je	.L2442
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2442:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2431:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2443
	call	_ZdlPv@PLT
.L2443:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2444
	call	_ZdlPv@PLT
.L2444:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2445
	call	_ZdlPv@PLT
.L2445:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2419:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2390:
	addq	$8, -64(%rbp)
	movq	-64(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L2446
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -64(%rbp)
.L2389:
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L2447
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2447:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2388:
	movq	152(%rbx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L2448
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2449
	movq	%rbx, -96(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2483:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2450
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2451
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2452
	call	_ZdlPv@PLT
.L2452:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2453
	movq	0(%r13), %rax
	movq	-56(%rbp), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2454
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2455
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2456
	movq	%r12, -88(%rbp)
	movq	%rsi, %r12
	movq	%rbx, -104(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r13, -112(%rbp)
	movq	%r14, -120(%rbp)
	jmp	.L2463
	.p2align 4,,10
	.p2align 3
.L2653:
	movq	16(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2459
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2460
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2459:
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2461
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	24(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2462
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2461:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2457:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2652
.L2463:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L2457
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2653
	addq	$8, %rbx
	movq	%r13, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2463
	.p2align 4,,10
	.p2align 3
.L2652:
	movq	-120(%rbp), %r14
	movq	-88(%rbp), %r12
	movq	-104(%rbp), %rbx
	movq	-112(%rbp), %r13
	movq	(%r14), %r15
.L2456:
	testq	%r15, %r15
	je	.L2464
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2464:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2455:
	movq	152(%r13), %rax
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L2465
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2466
	movq	%r12, -104(%rbp)
	movq	%r13, -112(%rbp)
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L2655:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2469
	call	_ZdlPv@PLT
.L2469:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2470
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	-56(%rbp), %rax
	jne	.L2471
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2470:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2472
	call	_ZdlPv@PLT
.L2472:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2473
	call	_ZdlPv@PLT
.L2473:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2474
	call	_ZdlPv@PLT
.L2474:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2467:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2654
.L2475:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2467
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2655
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2475
	.p2align 4,,10
	.p2align 3
.L2654:
	movq	-88(%rbp), %rax
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %r13
	movq	(%rax), %r14
.L2466:
	testq	%r14, %r14
	je	.L2476
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2476:
	movq	-88(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2465:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2477
	call	_ZdlPv@PLT
.L2477:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2478
	call	_ZdlPv@PLT
.L2478:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2479
	call	_ZdlPv@PLT
.L2479:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2453:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2480
	call	_ZdlPv@PLT
.L2480:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2481
	call	_ZdlPv@PLT
.L2481:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2482
	call	_ZdlPv@PLT
.L2482:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2450:
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L2483
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2449:
	testq	%rdi, %rdi
	je	.L2484
	call	_ZdlPv@PLT
.L2484:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2448:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2485
	call	_ZdlPv@PLT
.L2485:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2486
	call	_ZdlPv@PLT
.L2486:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2487
	call	_ZdlPv@PLT
.L2487:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2386:
	movq	-72(%rbp), %rdi
	addq	$88, %rsp
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L2451:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2450
	.p2align 4,,10
	.p2align 3
.L2391:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2387:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2386
	.p2align 4,,10
	.p2align 3
.L2360:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2359
	.p2align 4,,10
	.p2align 3
.L2377:
	call	*%rax
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2368:
	call	*%rax
	jmp	.L2367
	.p2align 4,,10
	.p2align 3
.L2393:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2392
	.p2align 4,,10
	.p2align 3
.L2366:
	call	*%rax
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2420:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2454:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2453
	.p2align 4,,10
	.p2align 3
.L2401:
	call	*%rdx
	jmp	.L2400
	.p2align 4,,10
	.p2align 3
.L2428:
	call	*%rdx
	jmp	.L2427
	.p2align 4,,10
	.p2align 3
.L2410:
	call	*%rax
	jmp	.L2409
	.p2align 4,,10
	.p2align 3
.L2399:
	call	*%rdx
	jmp	.L2398
	.p2align 4,,10
	.p2align 3
.L2426:
	call	*%rdx
	jmp	.L2425
	.p2align 4,,10
	.p2align 3
.L2462:
	call	*%rdx
	jmp	.L2461
	.p2align 4,,10
	.p2align 3
.L2460:
	call	*%rdx
	jmp	.L2459
	.p2align 4,,10
	.p2align 3
.L2437:
	call	*%rax
	jmp	.L2436
	.p2align 4,,10
	.p2align 3
.L2471:
	call	*%rax
	jmp	.L2470
	.cfi_endproc
.LFE5628:
	.size	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev:
.LFB5588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%rax, (%rdi)
	movq	152(%rdi), %rdi
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2657
	call	_ZdlPv@PLT
.L2657:
	movq	136(%rbx), %rcx
	movq	%rcx, -64(%rbp)
	testq	%rcx, %rcx
	je	.L2658
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2659
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L2660
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -72(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rcx
	je	.L2661
	movq	%rbx, -88(%rbp)
	.p2align 4,,10
	.p2align 3
.L2718:
	movq	-56(%rbp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L2662
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2663
	movq	16(%r13), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L2664
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2665
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L2666
	movq	8(%r14), %rax
	movq	(%r14), %rbx
	cmpq	%rbx, %rax
	je	.L2667
	movq	%r12, -104(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
	movq	%r13, -96(%rbp)
	jmp	.L2674
	.p2align 4,,10
	.p2align 3
.L2884:
	movq	16(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L2670
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2671
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2670:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L2672
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2673
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2672:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2668:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L2883
.L2674:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L2668
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2884
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L2674
	.p2align 4,,10
	.p2align 3
.L2883:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r14), %rbx
.L2667:
	testq	%rbx, %rbx
	je	.L2675
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L2675:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2666:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L2676
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L2677
	movq	%r13, -96(%rbp)
	movq	%r12, -104(%rbp)
	jmp	.L2686
	.p2align 4,,10
	.p2align 3
.L2886:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2680
	call	_ZdlPv@PLT
.L2680:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2681
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2682
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2681:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2683
	call	_ZdlPv@PLT
.L2683:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2684
	call	_ZdlPv@PLT
.L2684:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2685
	call	_ZdlPv@PLT
.L2685:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2678:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2885
.L2686:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2678
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2886
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L2686
	.p2align 4,,10
	.p2align 3
.L2885:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r15), %r14
.L2677:
	testq	%r14, %r14
	je	.L2687
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2687:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2676:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2688
	call	_ZdlPv@PLT
.L2688:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2689
	call	_ZdlPv@PLT
.L2689:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2690
	call	_ZdlPv@PLT
.L2690:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2664:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L2691
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2692
	movq	160(%r12), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L2693
	movq	8(%r14), %rax
	movq	(%r14), %rbx
	cmpq	%rbx, %rax
	je	.L2694
	movq	%r12, -104(%rbp)
	movq	%rbx, %r12
	movq	%rax, %rbx
	movq	%r13, -96(%rbp)
	jmp	.L2701
	.p2align 4,,10
	.p2align 3
.L2888:
	movq	16(%r15), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L2697
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2698
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2697:
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L2699
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2700
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2699:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2695:
	addq	$8, %r12
	cmpq	%r12, %rbx
	je	.L2887
.L2701:
	movq	(%r12), %r15
	testq	%r15, %r15
	je	.L2695
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2888
	addq	$8, %r12
	movq	%r15, %rdi
	call	*%rdx
	cmpq	%r12, %rbx
	jne	.L2701
	.p2align 4,,10
	.p2align 3
.L2887:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r14), %rbx
.L2694:
	testq	%rbx, %rbx
	je	.L2702
	movq	%rbx, %rdi
	call	_ZdlPv@PLT
.L2702:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2693:
	movq	152(%r12), %r15
	testq	%r15, %r15
	je	.L2703
	movq	8(%r15), %rbx
	movq	(%r15), %r14
	cmpq	%r14, %rbx
	je	.L2704
	movq	%r13, -96(%rbp)
	movq	%r12, -104(%rbp)
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2890:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2707
	call	_ZdlPv@PLT
.L2707:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2708
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2709
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2708:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2710
	call	_ZdlPv@PLT
.L2710:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2711
	call	_ZdlPv@PLT
.L2711:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2712
	call	_ZdlPv@PLT
.L2712:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2705:
	addq	$8, %r14
	cmpq	%r14, %rbx
	je	.L2889
.L2713:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2705
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L2890
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %rbx
	jne	.L2713
	.p2align 4,,10
	.p2align 3
.L2889:
	movq	-96(%rbp), %r13
	movq	-104(%rbp), %r12
	movq	(%r15), %r14
.L2704:
	testq	%r14, %r14
	je	.L2714
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2714:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2703:
	movq	104(%r12), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2715
	call	_ZdlPv@PLT
.L2715:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2716
	call	_ZdlPv@PLT
.L2716:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2717
	call	_ZdlPv@PLT
.L2717:
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2691:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2662:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	jne	.L2718
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L2661:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L2719
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L2719:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2660:
	movq	-64(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L2720
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -56(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2721
	movq	%rbx, -88(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2755:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2722
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2723
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2724
	call	_ZdlPv@PLT
.L2724:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2725
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2726
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2727
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2728
	movq	%rbx, -96(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -80(%rbp)
	movq	%r13, -104(%rbp)
	jmp	.L2735
	.p2align 4,,10
	.p2align 3
.L2892:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2731
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2732
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2731:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2733
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2734
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2733:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2729:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L2891
.L2735:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2729
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L2892
	addq	$8, %rbx
	movq	%r12, %rdi
	call	*%rdx
	cmpq	%rbx, %r15
	jne	.L2735
	.p2align 4,,10
	.p2align 3
.L2891:
	movq	-80(%rbp), %r12
	movq	-96(%rbp), %rbx
	movq	-104(%rbp), %r13
	movq	(%r14), %r15
.L2728:
	testq	%r15, %r15
	je	.L2736
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2736:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2727:
	movq	152(%r13), %rax
	movq	%rax, -80(%rbp)
	testq	%rax, %rax
	je	.L2737
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2738
	movq	%r12, -96(%rbp)
	movq	%r13, -104(%rbp)
	jmp	.L2747
	.p2align 4,,10
	.p2align 3
.L2894:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2741
	call	_ZdlPv@PLT
.L2741:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2742
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2743
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2742:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2744
	call	_ZdlPv@PLT
.L2744:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2745
	call	_ZdlPv@PLT
.L2745:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2746
	call	_ZdlPv@PLT
.L2746:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2739:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L2893
.L2747:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2739
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L2894
	addq	$8, %r14
	movq	%r12, %rdi
	call	*%rax
	cmpq	%r14, %r15
	jne	.L2747
	.p2align 4,,10
	.p2align 3
.L2893:
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %r12
	movq	-104(%rbp), %r13
	movq	(%rax), %r14
.L2738:
	testq	%r14, %r14
	je	.L2748
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2748:
	movq	-80(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2737:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2749
	call	_ZdlPv@PLT
.L2749:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2750
	call	_ZdlPv@PLT
.L2750:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2751
	call	_ZdlPv@PLT
.L2751:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2725:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2752
	call	_ZdlPv@PLT
.L2752:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2753
	call	_ZdlPv@PLT
.L2753:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2754
	call	_ZdlPv@PLT
.L2754:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2722:
	addq	$8, %rbx
	cmpq	%rbx, -56(%rbp)
	jne	.L2755
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2721:
	testq	%rdi, %rdi
	je	.L2756
	call	_ZdlPv@PLT
.L2756:
	movq	-72(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2720:
	movq	-64(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L2757
	call	_ZdlPv@PLT
.L2757:
	movq	-64(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L2758
	call	_ZdlPv@PLT
.L2758:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L2759
	call	_ZdlPv@PLT
.L2759:
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2658:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2760
	call	_ZdlPv@PLT
.L2760:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2761
	call	_ZdlPv@PLT
.L2761:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L2762
	call	_ZdlPv@PLT
.L2762:
	addq	$72, %rsp
	movq	%rbx, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L2663:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2662
	.p2align 4,,10
	.p2align 3
.L2723:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2722
	.p2align 4,,10
	.p2align 3
.L2659:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2658
	.p2align 4,,10
	.p2align 3
.L2665:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2664
	.p2align 4,,10
	.p2align 3
.L2726:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2725
	.p2align 4,,10
	.p2align 3
.L2692:
	movq	%r12, %rdi
	call	*%rax
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2662
	.p2align 4,,10
	.p2align 3
.L2671:
	call	*%rdx
	jmp	.L2670
	.p2align 4,,10
	.p2align 3
.L2682:
	call	*%rax
	jmp	.L2681
	.p2align 4,,10
	.p2align 3
.L2709:
	call	*%rax
	jmp	.L2708
	.p2align 4,,10
	.p2align 3
.L2698:
	call	*%rdx
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2734:
	call	*%rdx
	jmp	.L2733
	.p2align 4,,10
	.p2align 3
.L2743:
	call	*%rax
	jmp	.L2742
	.p2align 4,,10
	.p2align 3
.L2700:
	call	*%rdx
	jmp	.L2699
	.p2align 4,,10
	.p2align 3
.L2732:
	call	*%rdx
	jmp	.L2731
	.p2align 4,,10
	.p2align 3
.L2673:
	call	*%rdx
	jmp	.L2672
	.cfi_endproc
.LFE5588:
	.size	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, @function
_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev:
.LFB5330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rax
	leaq	-64(%rax), %rcx
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	312(%rdi), %r12
	movups	%xmm0, (%rdi)
	testq	%r12, %r12
	je	.L2896
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2897
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	56(%r12), %rdi
	movq	%rax, (%r12)
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2898
	call	_ZdlPv@PLT
.L2898:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2899
	call	_ZdlPv@PLT
.L2899:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2896:
	movq	304(%rbx), %rcx
	movq	%rcx, -64(%rbp)
	testq	%rcx, %rcx
	je	.L2900
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2901
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	movq	160(%rcx), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L2902
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -80(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rsi
	je	.L2903
	movq	%rbx, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L3110:
	movq	-56(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -72(%rbp)
	testq	%rcx, %rcx
	je	.L2904
	movq	(%rcx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2905
	movq	16(%rcx), %rbx
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rcx)
	testq	%rbx, %rbx
	je	.L2906
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2907
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L2908
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L2909
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L2966:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2910
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2911
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2912
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2913
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2914
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L2915
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -136(%rbp)
	jmp	.L2922
	.p2align 4,,10
	.p2align 3
.L3622:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2918
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2919
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2918:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2920
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2921
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2920:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2916:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3621
.L2922:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2916
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3622
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L2916
	.p2align 4,,10
	.p2align 3
.L3645:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L3088:
	testq	%r14, %r14
	je	.L3098
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3098:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3087:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3099
	call	_ZdlPv@PLT
.L3099:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3100
	call	_ZdlPv@PLT
.L3100:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3101
	call	_ZdlPv@PLT
.L3101:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3075:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3102
	call	_ZdlPv@PLT
.L3102:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3103
	call	_ZdlPv@PLT
.L3103:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3104
	call	_ZdlPv@PLT
.L3104:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3072:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L3105
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3071:
	testq	%rdi, %rdi
	je	.L3106
	call	_ZdlPv@PLT
.L3106:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3070:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3107
	call	_ZdlPv@PLT
.L3107:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3108
	call	_ZdlPv@PLT
.L3108:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3109
	call	_ZdlPv@PLT
.L3109:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3008:
	movq	-72(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2904:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L3110
	movq	-96(%rbp), %rax
	movq	-120(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L2903:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L3111
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3111:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2902:
	movq	-64(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L3112
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -80(%rbp)
	movq	%rax, -56(%rbp)
	cmpq	%rax, %rcx
	je	.L3113
	movq	%rbx, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L3222:
	movq	-56(%rbp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3114
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3115
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%rbx), %rdi
	movq	%rax, (%rbx)
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3116
	call	_ZdlPv@PLT
.L3116:
	movq	136(%rbx), %rsi
	movq	%rsi, -72(%rbp)
	testq	%rsi, %rsi
	je	.L3117
	movq	(%rsi), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3118
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rsi)
	movq	160(%rsi), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L3119
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L3120
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3177:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3121
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3122
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3123
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3124
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3125
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L3126
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -136(%rbp)
	jmp	.L3133
	.p2align 4,,10
	.p2align 3
.L3624:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3129
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3130
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3129:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3131
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3132
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3131:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3127:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3623
.L3133:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3127
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3624
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L3127
	.p2align 4,,10
	.p2align 3
.L3651:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L3197:
	testq	%r14, %r14
	je	.L3207
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3207:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3196:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3208
	call	_ZdlPv@PLT
.L3208:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3209
	call	_ZdlPv@PLT
.L3209:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3210
	call	_ZdlPv@PLT
.L3210:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3184:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3211
	call	_ZdlPv@PLT
.L3211:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3212
	call	_ZdlPv@PLT
.L3212:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3213
	call	_ZdlPv@PLT
.L3213:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3181:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L3214
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3180:
	testq	%rdi, %rdi
	je	.L3215
	call	_ZdlPv@PLT
.L3215:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3179:
	movq	-72(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L3216
	call	_ZdlPv@PLT
.L3216:
	movq	-72(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L3217
	call	_ZdlPv@PLT
.L3217:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L3218
	call	_ZdlPv@PLT
.L3218:
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3117:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3219
	call	_ZdlPv@PLT
.L3219:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3220
	call	_ZdlPv@PLT
.L3220:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3221
	call	_ZdlPv@PLT
.L3221:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3114:
	addq	$8, -56(%rbp)
	movq	-56(%rbp), %rax
	cmpq	%rax, -80(%rbp)
	jne	.L3222
	movq	-96(%rbp), %rax
	movq	-120(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, -56(%rbp)
.L3113:
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	je	.L3223
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L3223:
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3112:
	movq	-64(%rbp), %rax
	movq	104(%rax), %rdi
	addq	$120, %rax
	cmpq	%rax, %rdi
	je	.L3224
	call	_ZdlPv@PLT
.L3224:
	movq	-64(%rbp), %rax
	movq	56(%rax), %rdi
	addq	$72, %rax
	cmpq	%rax, %rdi
	je	.L3225
	call	_ZdlPv@PLT
.L3225:
	movq	-64(%rbp), %rax
	movq	8(%rax), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L3226
	call	_ZdlPv@PLT
.L3226:
	movq	-64(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L2900:
	movq	264(%rbx), %rdi
	leaq	280(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3227
	call	_ZdlPv@PLT
.L3227:
	movq	216(%rbx), %rdi
	leaq	232(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3228
	call	_ZdlPv@PLT
.L3228:
	movq	168(%rbx), %rdi
	leaq	184(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3229
	call	_ZdlPv@PLT
.L3229:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3230
	movq	(%rdi), %rax
	call	*24(%rax)
.L3230:
	movq	112(%rbx), %rdi
	leaq	128(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3231
	call	_ZdlPv@PLT
.L3231:
	movq	64(%rbx), %rdi
	leaq	80(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3232
	call	_ZdlPv@PLT
.L3232:
	movq	16(%rbx), %r8
	leaq	32(%rbx), %rdi
	cmpq	%rdi, %r8
	je	.L2895
	addq	$104, %rsp
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L2895:
	.cfi_restore_state
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3122:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L3121:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L3177
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3120:
	testq	%rdi, %rdi
	je	.L3178
	call	_ZdlPv@PLT
.L3178:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3119:
	movq	-72(%rbp), %rax
	movq	152(%rax), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L3179
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L3180
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3214:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3181
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3182
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3183
	call	_ZdlPv@PLT
.L3183:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3184
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3185
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3186
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L3187
	movq	%rbx, -136(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -112(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3194
	.p2align 4,,10
	.p2align 3
.L3626:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3190
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3191
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3190:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3192
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3193
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3192:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3188:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3625
.L3194:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3188
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3626
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L3188
	.p2align 4,,10
	.p2align 3
.L2911:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L2910:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L2966
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2909:
	testq	%rdi, %rdi
	je	.L2967
	call	_ZdlPv@PLT
.L2967:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2908:
	movq	152(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L2968
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rsi
	je	.L2969
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3003:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2970
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2971
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2972
	call	_ZdlPv@PLT
.L2972:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2973
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2974
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2975
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L2976
	movq	%rbx, -136(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -112(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L2983
	.p2align 4,,10
	.p2align 3
.L3628:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L2979
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2980
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2979:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2981
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2982
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2981:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2977:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3627
.L2983:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L2977
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3628
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L2977
	.p2align 4,,10
	.p2align 3
.L3655:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L2986:
	testq	%r14, %r14
	je	.L2996
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2996:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2985:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2997
	call	_ZdlPv@PLT
.L2997:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2998
	call	_ZdlPv@PLT
.L2998:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2999
	call	_ZdlPv@PLT
.L2999:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2973:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3000
	call	_ZdlPv@PLT
.L3000:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3001
	call	_ZdlPv@PLT
.L3001:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3002
	call	_ZdlPv@PLT
.L3002:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2970:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L3003
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L2969:
	testq	%rdi, %rdi
	je	.L3004
	call	_ZdlPv@PLT
.L3004:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2968:
	movq	104(%rbx), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3005
	call	_ZdlPv@PLT
.L3005:
	movq	56(%rbx), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3006
	call	_ZdlPv@PLT
.L3006:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L3007
	call	_ZdlPv@PLT
.L3007:
	movl	$168, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2906:
	movq	-72(%rbp), %rax
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3008
	movq	(%rbx), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3009
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	movq	160(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L3010
	movq	8(%rax), %rdx
	movq	(%rax), %rax
	movq	%rdx, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rdx
	je	.L3011
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3068:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3012
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3013
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3014
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3015
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3016
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L3017
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -136(%rbp)
	jmp	.L3024
	.p2align 4,,10
	.p2align 3
.L3630:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3020
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3021
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3020:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3022
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3023
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3022:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3018:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3629
.L3024:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3018
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3630
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L3018
	.p2align 4,,10
	.p2align 3
.L3013:
	movq	%r12, %rdi
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L3012:
	addq	$8, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L3068
	movq	-104(%rbp), %rax
	movq	-128(%rbp), %rbx
	movq	(%rax), %rax
	movq	%rax, %rdi
.L3011:
	testq	%rdi, %rdi
	je	.L3069
	call	_ZdlPv@PLT
.L3069:
	movq	-104(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3010:
	movq	152(%rbx), %rax
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L3070
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	movq	%rcx, -88(%rbp)
	movq	%rax, %rdi
	cmpq	%rax, %rcx
	je	.L3071
	movq	%rbx, -128(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3105:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3072
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3073
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3074
	call	_ZdlPv@PLT
.L3074:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3075
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3076
	movq	160(%r13), %r14
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L3077
	movq	8(%r14), %rax
	movq	(%r14), %r15
	cmpq	%r15, %rax
	je	.L3078
	movq	%rbx, -136(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	movq	%r12, -112(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3085
	.p2align 4,,10
	.p2align 3
.L3632:
	movq	16(%r12), %r13
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L3081
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3082
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3081:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3083
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r13, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3084
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3083:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3079:
	addq	$8, %rbx
	cmpq	%rbx, %r15
	je	.L3631
.L3085:
	movq	(%rbx), %r12
	testq	%r12, %r12
	je	.L3079
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3632
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L3079
	.p2align 4,,10
	.p2align 3
.L3647:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L3027:
	testq	%r14, %r14
	je	.L3037
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3037:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3026:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3038
	call	_ZdlPv@PLT
.L3038:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3039
	call	_ZdlPv@PLT
.L3039:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3040
	call	_ZdlPv@PLT
.L3040:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3014:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3041
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3042
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3043
	movq	8(%rax), %rdx
	movq	(%rax), %r14
	cmpq	%r14, %rdx
	je	.L3044
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rdx, %r14
	movq	%r12, -136(%rbp)
	jmp	.L3051
	.p2align 4,,10
	.p2align 3
.L3634:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3047
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3048
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3047:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3049
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3050
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3049:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3045:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3633
.L3051:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3045
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3634
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L3045
	.p2align 4,,10
	.p2align 3
.L3643:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L3054:
	testq	%r14, %r14
	je	.L3064
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3064:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3053:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3065
	call	_ZdlPv@PLT
.L3065:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3066
	call	_ZdlPv@PLT
.L3066:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3067
	call	_ZdlPv@PLT
.L3067:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3041:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3012
	.p2align 4,,10
	.p2align 3
.L3653:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L2925:
	testq	%r14, %r14
	je	.L2935
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2935:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2924:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2936
	call	_ZdlPv@PLT
.L2936:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2937
	call	_ZdlPv@PLT
.L2937:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2938
	call	_ZdlPv@PLT
.L2938:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2912:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L2939
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2940
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2941
	movq	8(%rax), %rcx
	movq	(%rax), %r14
	cmpq	%r14, %rcx
	je	.L2942
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rcx, %r14
	movq	%r12, -136(%rbp)
	jmp	.L2949
	.p2align 4,,10
	.p2align 3
.L3636:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L2945
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2946
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2945:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L2947
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L2948
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2947:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L2943:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3635
.L2949:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L2943
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3636
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L2943
	.p2align 4,,10
	.p2align 3
.L3641:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L2952:
	testq	%r14, %r14
	je	.L2962
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2962:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2951:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2963
	call	_ZdlPv@PLT
.L2963:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2964
	call	_ZdlPv@PLT
.L2964:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2965
	call	_ZdlPv@PLT
.L2965:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2939:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2910
	.p2align 4,,10
	.p2align 3
.L3649:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L3136:
	testq	%r14, %r14
	je	.L3146
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3146:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3135:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3147
	call	_ZdlPv@PLT
.L3147:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3148
	call	_ZdlPv@PLT
.L3148:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3149
	call	_ZdlPv@PLT
.L3149:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3123:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L3150
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3151
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	movq	160(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3152
	movq	8(%rax), %rsi
	movq	(%rax), %r14
	cmpq	%r14, %rsi
	je	.L3153
	movq	%rbx, -144(%rbp)
	movq	%r14, %rbx
	movq	%rsi, %r14
	movq	%r12, -136(%rbp)
	jmp	.L3160
	.p2align 4,,10
	.p2align 3
.L3638:
	movq	16(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3156
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3157
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3156:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L3158
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rax
	movq	%r12, %rdi
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3159
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3158:
	movl	$24, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L3154:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	je	.L3637
.L3160:
	movq	(%rbx), %r15
	testq	%r15, %r15
	je	.L3154
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	je	.L3638
	movq	%r15, %rdi
	call	*%rdx
	jmp	.L3154
	.p2align 4,,10
	.p2align 3
.L3639:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %r13
	movq	(%rax), %r14
.L3163:
	testq	%r14, %r14
	je	.L3173
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3173:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3162:
	movq	104(%r13), %rdi
	leaq	120(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3174
	call	_ZdlPv@PLT
.L3174:
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3175
	call	_ZdlPv@PLT
.L3175:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	je	.L3176
	call	_ZdlPv@PLT
.L3176:
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3150:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3121
	.p2align 4,,10
	.p2align 3
.L3637:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L3153:
	testq	%r14, %r14
	je	.L3161
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3161:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3152:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3162
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3163
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3172
	.p2align 4,,10
	.p2align 3
.L3640:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3166
	call	_ZdlPv@PLT
.L3166:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3167
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3168
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3167:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3169
	call	_ZdlPv@PLT
.L3169:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3170
	call	_ZdlPv@PLT
.L3170:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3171
	call	_ZdlPv@PLT
.L3171:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3164:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3639
.L3172:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3164
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3640
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3164
	.p2align 4,,10
	.p2align 3
.L3635:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L2942:
	testq	%r14, %r14
	je	.L2950
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2950:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2941:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2951
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2952
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L2961
	.p2align 4,,10
	.p2align 3
.L3642:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2955
	call	_ZdlPv@PLT
.L2955:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2956
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2957
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2956:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2958
	call	_ZdlPv@PLT
.L2958:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2959
	call	_ZdlPv@PLT
.L2959:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2960
	call	_ZdlPv@PLT
.L2960:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2953:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3641
.L2961:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2953
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3642
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2953
	.p2align 4,,10
	.p2align 3
.L3633:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L3044:
	testq	%r14, %r14
	je	.L3052
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3052:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3043:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3053
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3054
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3063
	.p2align 4,,10
	.p2align 3
.L3644:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3057
	call	_ZdlPv@PLT
.L3057:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3058
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rcx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L3059
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3058:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3060
	call	_ZdlPv@PLT
.L3060:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3061
	call	_ZdlPv@PLT
.L3061:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3062
	call	_ZdlPv@PLT
.L3062:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3055:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3643
.L3063:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3055
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rcx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	je	.L3644
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3055
	.p2align 4,,10
	.p2align 3
.L3631:
	movq	-112(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r13
	movq	(%r14), %r15
.L3078:
	testq	%r15, %r15
	je	.L3086
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3086:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3077:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3087
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3088
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3097
	.p2align 4,,10
	.p2align 3
.L3646:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3091
	call	_ZdlPv@PLT
.L3091:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3092
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3093
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3092:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3094
	call	_ZdlPv@PLT
.L3094:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3095
	call	_ZdlPv@PLT
.L3095:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3096
	call	_ZdlPv@PLT
.L3096:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3089:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3645
.L3097:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3089
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3646
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3089
	.p2align 4,,10
	.p2align 3
.L3629:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L3017:
	testq	%r14, %r14
	je	.L3025
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3025:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3016:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3026
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3027
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3036
	.p2align 4,,10
	.p2align 3
.L3648:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3030
	call	_ZdlPv@PLT
.L3030:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3031
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3032
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3031:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3033
	call	_ZdlPv@PLT
.L3033:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3034
	call	_ZdlPv@PLT
.L3034:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3035
	call	_ZdlPv@PLT
.L3035:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3028:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3647
.L3036:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3028
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3648
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3028
	.p2align 4,,10
	.p2align 3
.L3623:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L3126:
	testq	%r14, %r14
	je	.L3134
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L3134:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3125:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3135
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3136
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3145
	.p2align 4,,10
	.p2align 3
.L3650:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3139
	call	_ZdlPv@PLT
.L3139:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3140
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rsi
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L3141
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3140:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3142
	call	_ZdlPv@PLT
.L3142:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3143
	call	_ZdlPv@PLT
.L3143:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3144
	call	_ZdlPv@PLT
.L3144:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3137:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3649
.L3145:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3137
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3650
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3137
	.p2align 4,,10
	.p2align 3
.L3625:
	movq	-112(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r13
	movq	(%r14), %r15
.L3187:
	testq	%r15, %r15
	je	.L3195
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L3195:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L3186:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L3196
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L3197
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L3206
	.p2align 4,,10
	.p2align 3
.L3652:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3200
	call	_ZdlPv@PLT
.L3200:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L3201
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3202
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3201:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3203
	call	_ZdlPv@PLT
.L3203:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3204
	call	_ZdlPv@PLT
.L3204:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L3205
	call	_ZdlPv@PLT
.L3205:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3198:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3651
.L3206:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L3198
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3652
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3198
	.p2align 4,,10
	.p2align 3
.L3621:
	movq	-112(%rbp), %rax
	movq	-136(%rbp), %r12
	movq	-144(%rbp), %rbx
	movq	(%rax), %r14
.L2915:
	testq	%r14, %r14
	je	.L2923
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L2923:
	movq	-112(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L2914:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2924
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2925
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L2934
	.p2align 4,,10
	.p2align 3
.L3654:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2928
	call	_ZdlPv@PLT
.L2928:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2929
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2930
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2929:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2931
	call	_ZdlPv@PLT
.L2931:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2932
	call	_ZdlPv@PLT
.L2932:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2933
	call	_ZdlPv@PLT
.L2933:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2926:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3653
.L2934:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2926
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rsi
	movq	24(%rax), %rax
	cmpq	%rsi, %rax
	je	.L3654
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2926
	.p2align 4,,10
	.p2align 3
.L3627:
	movq	-112(%rbp), %r12
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r13
	movq	(%r14), %r15
.L2976:
	testq	%r15, %r15
	je	.L2984
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2984:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L2975:
	movq	152(%r13), %rax
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L2985
	movq	8(%rax), %r15
	movq	(%rax), %r14
	cmpq	%r14, %r15
	je	.L2986
	movq	%r12, -136(%rbp)
	movq	%r13, -144(%rbp)
	jmp	.L2995
	.p2align 4,,10
	.p2align 3
.L3656:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	movq	152(%r12), %rdi
	movq	%rax, (%r12)
	leaq	168(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2989
	call	_ZdlPv@PLT
.L2989:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L2990
	movq	0(%r13), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2991
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movl	$168, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2990:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2992
	call	_ZdlPv@PLT
.L2992:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2993
	call	_ZdlPv@PLT
.L2993:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2994
	call	_ZdlPv@PLT
.L2994:
	movl	$192, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2987:
	addq	$8, %r14
	cmpq	%r14, %r15
	je	.L3655
.L2995:
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L2987
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L3656
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2987
	.p2align 4,,10
	.p2align 3
.L3115:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L3114
	.p2align 4,,10
	.p2align 3
.L2905:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2904
	.p2align 4,,10
	.p2align 3
.L2897:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2896
	.p2align 4,,10
	.p2align 3
.L2901:
	movq	%rcx, %rdi
	call	*%rax
	jmp	.L2900
	.p2align 4,,10
	.p2align 3
.L3182:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3181
	.p2align 4,,10
	.p2align 3
.L2971:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2970
	.p2align 4,,10
	.p2align 3
.L3073:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L3072
	.p2align 4,,10
	.p2align 3
.L2907:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L2906
	.p2align 4,,10
	.p2align 3
.L3009:
	movq	%rbx, %rdi
	call	*%rax
	jmp	.L3008
	.p2align 4,,10
	.p2align 3
.L3118:
	movq	%rsi, %rdi
	call	*%rax
	jmp	.L3117
	.p2align 4,,10
	.p2align 3
.L3124:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3123
	.p2align 4,,10
	.p2align 3
.L3042:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3041
	.p2align 4,,10
	.p2align 3
.L2974:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2973
	.p2align 4,,10
	.p2align 3
.L3015:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3014
	.p2align 4,,10
	.p2align 3
.L3076:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3075
	.p2align 4,,10
	.p2align 3
.L2913:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2912
	.p2align 4,,10
	.p2align 3
.L2940:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2939
	.p2align 4,,10
	.p2align 3
.L3185:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3184
	.p2align 4,,10
	.p2align 3
.L3151:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L3150
	.p2align 4,,10
	.p2align 3
.L3048:
	call	*%rdx
	jmp	.L3047
	.p2align 4,,10
	.p2align 3
.L2930:
	call	*%rax
	jmp	.L2929
	.p2align 4,,10
	.p2align 3
.L3159:
	call	*%rdx
	jmp	.L3158
	.p2align 4,,10
	.p2align 3
.L3193:
	call	*%rdx
	jmp	.L3192
	.p2align 4,,10
	.p2align 3
.L3050:
	call	*%rdx
	jmp	.L3049
	.p2align 4,,10
	.p2align 3
.L3132:
	call	*%rdx
	jmp	.L3131
	.p2align 4,,10
	.p2align 3
.L2919:
	call	*%rdx
	jmp	.L2918
	.p2align 4,,10
	.p2align 3
.L2921:
	call	*%rdx
	jmp	.L2920
	.p2align 4,,10
	.p2align 3
.L2991:
	call	*%rax
	jmp	.L2990
	.p2align 4,,10
	.p2align 3
.L3021:
	call	*%rdx
	jmp	.L3020
	.p2align 4,,10
	.p2align 3
.L3082:
	call	*%rdx
	jmp	.L3081
	.p2align 4,,10
	.p2align 3
.L2946:
	call	*%rdx
	jmp	.L2945
	.p2align 4,,10
	.p2align 3
.L3130:
	call	*%rdx
	jmp	.L3129
	.p2align 4,,10
	.p2align 3
.L2982:
	call	*%rdx
	jmp	.L2981
	.p2align 4,,10
	.p2align 3
.L3023:
	call	*%rdx
	jmp	.L3022
	.p2align 4,,10
	.p2align 3
.L3059:
	call	*%rax
	jmp	.L3058
	.p2align 4,,10
	.p2align 3
.L2957:
	call	*%rax
	jmp	.L2956
	.p2align 4,,10
	.p2align 3
.L3157:
	call	*%rdx
	jmp	.L3156
	.p2align 4,,10
	.p2align 3
.L3168:
	call	*%rax
	jmp	.L3167
	.p2align 4,,10
	.p2align 3
.L3084:
	call	*%rdx
	jmp	.L3083
	.p2align 4,,10
	.p2align 3
.L2948:
	call	*%rdx
	jmp	.L2947
	.p2align 4,,10
	.p2align 3
.L3191:
	call	*%rdx
	jmp	.L3190
	.p2align 4,,10
	.p2align 3
.L2980:
	call	*%rdx
	jmp	.L2979
	.p2align 4,,10
	.p2align 3
.L3032:
	call	*%rax
	jmp	.L3031
	.p2align 4,,10
	.p2align 3
.L3093:
	call	*%rax
	jmp	.L3092
	.p2align 4,,10
	.p2align 3
.L3202:
	call	*%rax
	jmp	.L3201
	.p2align 4,,10
	.p2align 3
.L3141:
	call	*%rax
	jmp	.L3140
	.cfi_endproc
.LFE5330:
	.size	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev, .-_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev
	.section	.rodata._ZN12v8_inspectorL11inspectImplERKN2v820FunctionCallbackInfoINS0_5ValueEEENS0_5LocalIS2_EEiNS_12_GLOBAL__N_114InspectRequestEPNS_15V8InspectorImplE.str1.1,"aMS",@progbits,1
.LC2:
	.string	""
.LC3:
	.string	"copyToClipboard"
.LC4:
	.string	"queryObjects"
	.section	.text._ZN12v8_inspectorL11inspectImplERKN2v820FunctionCallbackInfoINS0_5ValueEEENS0_5LocalIS2_EEiNS_12_GLOBAL__N_114InspectRequestEPNS_15V8InspectorImplE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspectorL11inspectImplERKN2v820FunctionCallbackInfoINS0_5ValueEEENS0_5LocalIS2_EEiNS_12_GLOBAL__N_114InspectRequestEPNS_15V8InspectorImplE, @function
_ZN12v8_inspectorL11inspectImplERKN2v820FunctionCallbackInfoINS0_5ValueEEENS0_5LocalIS2_EEiNS_12_GLOBAL__N_114InspectRequestEPNS_15V8InspectorImplE:
.LFB8250:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -276(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L3658
	movq	(%rdi), %rax
	testq	%rsi, %rsi
	je	.L3708
	movq	(%rsi), %rdx
	movq	%rdx, 24(%rax)
.L3658:
	movq	%r9, %rsi
	leaq	-240(%rbp), %rdi
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	movq	8(%r12), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	movl	%eax, %ebx
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movq	%r12, %rdi
	movl	%ebx, %edx
	movl	%eax, %esi
	movl	%eax, %r14d
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3657
	movl	%r15d, %esi
	call	_ZN12v8_inspector16InspectedContext17getInjectedScriptEi@PLT
	testq	%rax, %rax
	movq	%rax, -288(%rbp)
	je	.L3657
	leaq	-160(%rbp), %rbx
	leaq	.LC2(%rip), %rsi
	movq	$0, -264(%rbp)
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-112(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	-288(%rbp), %rax
	leaq	-264(%rbp), %r9
	movl	$1, %r8d
	movq	%rax, %rsi
	call	_ZN12v8_inspector14InjectedScript10wrapObjectEN2v85LocalINS1_5ValueEEERKNS_8String16ENS_8WrapModeEPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISC_EE@PLT
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	movq	%rax, -288(%rbp)
	cmpq	%rax, %rdi
	je	.L3661
	call	_ZdlPv@PLT
.L3661:
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L3662
.L3675:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3673
.L3712:
	call	_ZdlPv@PLT
.L3673:
	movq	-264(%rbp), %r12
	testq	%r12, %r12
	je	.L3657
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3674
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3657:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3709
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3662:
	.cfi_restore_state
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector8protocol15DictionaryValueC1Ev@PLT
	cmpl	$1, -276(%rbp)
	leaq	.LC3(%rip), %rsi
	je	.L3702
	cmpl	$2, -276(%rbp)
	je	.L3710
.L3668:
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl11sessionByIdEii@PLT
	testq	%rax, %rax
	je	.L3711
	movq	184(%rax), %rdi
	movq	-264(%rbp), %rax
	leaq	-248(%rbp), %rdx
	leaq	-256(%rbp), %rsi
	movq	%r13, -248(%rbp)
	movq	$0, -264(%rbp)
	movq	%rax, -256(%rbp)
	call	_ZN12v8_inspector18V8RuntimeAgentImpl7inspectESt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteIS4_EES1_INS2_15DictionaryValueES5_IS8_EE@PLT
	movq	-256(%rbp), %r12
	testq	%r12, %r12
	je	.L3670
	movq	(%r12), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3671
	call	_ZN12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3670:
	movq	-248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3675
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3712
	jmp	.L3673
	.p2align 4,,10
	.p2align 3
.L3711:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3712
	jmp	.L3673
	.p2align 4,,10
	.p2align 3
.L3708:
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L3658
	.p2align 4,,10
	.p2align 3
.L3710:
	leaq	.LC4(%rip), %rsi
.L3702:
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movl	$1, %edx
	movq	%rbx, %rsi
	call	_ZN12v8_inspector8protocol15DictionaryValue10setBooleanERKNS_8String16Eb@PLT
	movq	-160(%rbp), %rdi
	cmpq	-288(%rbp), %rdi
	je	.L3668
	call	_ZdlPv@PLT
	jmp	.L3668
	.p2align 4,,10
	.p2align 3
.L3674:
	call	*%rax
	jmp	.L3657
	.p2align 4,,10
	.p2align 3
.L3671:
	call	*%rax
	jmp	.L3670
.L3709:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8250:
	.size	_ZN12v8_inspectorL11inspectImplERKN2v820FunctionCallbackInfoINS0_5ValueEEENS0_5LocalIS2_EEiNS_12_GLOBAL__N_114InspectRequestEPNS_15V8InspectorImplE, .-_ZN12v8_inspectorL11inspectImplERKN2v820FunctionCallbackInfoINS0_5ValueEEENS0_5LocalIS2_EEiNS_12_GLOBAL__N_114InspectRequestEPNS_15V8InspectorImplE
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_15inspectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_15inspectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_15inspectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_15inspectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_15inspectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movl	16(%r12), %edx
	movq	-80(%rbp), %rax
	testl	%edx, %edx
	jle	.L3713
	movl	8(%rax), %edx
	movq	(%rax), %rax
	movq	8(%r12), %rsi
	movq	8(%rax), %r8
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3718
	addq	$72, %rsp
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspectorL11inspectImplERKN2v820FunctionCallbackInfoINS0_5ValueEEENS0_5LocalIS2_EEiNS_12_GLOBAL__N_114InspectRequestEPNS_15V8InspectorImplE
	.p2align 4,,10
	.p2align 3
.L3713:
	.cfi_restore_state
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3718
	addq	$72, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3718:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10183:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_15inspectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_15inspectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_12copyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12copyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12copyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12copyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12copyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movl	16(%r12), %edx
	movq	-80(%rbp), %rax
	testl	%edx, %edx
	jle	.L3719
	movl	8(%rax), %edx
	movq	(%rax), %rax
	movq	8(%r12), %rsi
	movq	8(%rax), %r8
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3724
	addq	$72, %rsp
	movq	%r12, %rdi
	movl	$1, %ecx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspectorL11inspectImplERKN2v820FunctionCallbackInfoINS0_5ValueEEENS0_5LocalIS2_EEiNS_12_GLOBAL__N_114InspectRequestEPNS_15V8InspectorImplE
	.p2align 4,,10
	.p2align 3
.L3719:
	.cfi_restore_state
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3724
	addq	$72, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3724:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10184:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12copyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12copyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.rodata._ZN12v8_inspector9V8Console4callIXadL_ZNS0_20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_.str1.1,"aMS",@progbits,1
.LC5:
	.string	"prototype"
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movl	16(%r12), %edx
	movq	-112(%rbp), %rax
	testl	%edx, %edx
	jle	.L3725
	movq	8(%r12), %r14
	movq	(%rax), %rbx
	movl	8(%rax), %r15d
	movq	%r14, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L3727
	movq	(%r12), %rax
	movq	%r13, %rdi
	movq	8(%rax), %r8
	movq	%r8, %rsi
	movq	%r8, -120(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	-120(%rbp), %r8
	leaq	.LC5(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	movq	-128(%rbp), %r8
	movq	%rax, -120(%rbp)
	movq	%r8, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-120(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L3730
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	-120(%rbp), %rdx
	testb	%al, %al
	cmovne	%rdx, %r14
.L3730:
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	movq	%r13, %rdi
	testb	%al, %al
	jne	.L3742
	call	_ZN2v88TryCatchD1Ev@PLT
.L3727:
	movq	8(%rbx), %r8
	movl	$2, %ecx
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspectorL11inspectImplERKN2v820FunctionCallbackInfoINS0_5ValueEEENS0_5LocalIS2_EEiNS_12_GLOBAL__N_114InspectRequestEPNS_15V8InspectorImplE
.L3725:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3743
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3742:
	.cfi_restore_state
	call	_ZN2v88TryCatch7ReThrowEv@PLT
	movq	%r13, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	jmp	.L3725
.L3743:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10185:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev:
.LFB15200:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rdx
	leaq	-64(%rdx), %rax
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	304(%rdi), %r12
	movups	%xmm0, -8(%rdi)
	testq	%r12, %r12
	je	.L3745
	movq	(%r12), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rcx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L3820
	movq	%r12, %rdi
	call	*%rdx
.L3745:
	movq	296(%r14), %r15
	testq	%r15, %r15
	je	.L3749
	movq	(%r15), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %r13
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	je	.L3821
	movq	%r15, %rdi
	call	*%rdx
.L3749:
	movq	256(%r14), %rdi
	leaq	272(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3776
	call	_ZdlPv@PLT
.L3776:
	movq	208(%r14), %rdi
	leaq	224(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3777
	call	_ZdlPv@PLT
.L3777:
	movq	160(%r14), %rdi
	leaq	176(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3778
	call	_ZdlPv@PLT
.L3778:
	movq	144(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3779
	movq	(%rdi), %rdx
	call	*24(%rdx)
.L3779:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3780
	call	_ZdlPv@PLT
.L3780:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3781
	call	_ZdlPv@PLT
.L3781:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L3744
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3744:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L3821:
	.cfi_restore_state
	movq	160(%r15), %r12
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r15)
	testq	%r12, %r12
	je	.L3751
	movq	8(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movq	%rax, -56(%rbp)
.L3759:
	movq	-56(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L3752
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3753
	movq	(%rbx), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3754
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L3755
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L3756
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3755:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3757
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L3758
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3757:
	movl	$24, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3753:
	addq	$8, -56(%rbp)
	jmp	.L3759
.L3754:
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L3753
.L3758:
	call	*%rdx
	jmp	.L3757
.L3752:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3760
	call	_ZdlPv@PLT
.L3760:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3751:
	movq	152(%r15), %r12
	testq	%r12, %r12
	je	.L3761
	movq	8(%r12), %rax
	movq	%rax, -64(%rbp)
	movq	(%r12), %rax
	movq	%rax, -56(%rbp)
.L3771:
	movq	-56(%rbp), %rax
	cmpq	%rax, -64(%rbp)
	je	.L3762
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L3763
	movq	(%rbx), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3764
	movq	152(%rbx), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	leaq	168(%rbx), %rdx
	movq	%rax, (%rbx)
	cmpq	%rdx, %rdi
	je	.L3765
	call	_ZdlPv@PLT
.L3765:
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3766
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L3767
	movq	%rdi, -72(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-72(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3766:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L3768
	call	_ZdlPv@PLT
.L3768:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L3769
	call	_ZdlPv@PLT
.L3769:
	movq	8(%rbx), %rdi
	leaq	24(%rbx), %rdx
	cmpq	%rdx, %rdi
	je	.L3770
	call	_ZdlPv@PLT
.L3770:
	movl	$192, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L3763:
	addq	$8, -56(%rbp)
	jmp	.L3771
.L3764:
	movq	%rbx, %rdi
	call	*%rdx
	jmp	.L3763
.L3767:
	call	*%rdx
	jmp	.L3766
.L3762:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3772
	call	_ZdlPv@PLT
.L3772:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L3761:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3773
	call	_ZdlPv@PLT
.L3773:
	movq	56(%r15), %rdi
	leaq	72(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3774
	call	_ZdlPv@PLT
.L3774:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3775
	call	_ZdlPv@PLT
.L3775:
	movl	$168, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3749
.L3820:
	movq	56(%r12), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	leaq	72(%r12), %rdx
	movq	%rax, (%r12)
	cmpq	%rdx, %rdi
	je	.L3747
	call	_ZdlPv@PLT
.L3747:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rdx
	cmpq	%rdx, %rdi
	je	.L3748
	call	_ZdlPv@PLT
.L3748:
	movl	$96, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3745
.L3756:
	call	*%rdx
	jmp	.L3755
	.cfi_endproc
.LFE15200:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD1Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime12RemoteObjectD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime12RemoteObjectD5Ev,comdat
	.p2align 4
	.weak	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.type	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, @function
_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev:
.LFB15199:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	80+_ZTVN12v8_inspector8protocol7Runtime12RemoteObjectE(%rip), %rdx
	leaq	-64(%rdx), %rax
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	punpcklqdq	%xmm1, %xmm0
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-8(%rdi), %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	304(%rdi), %r13
	movups	%xmm0, -8(%rdi)
	testq	%r13, %r13
	je	.L3823
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rcx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L3898
	movq	%r13, %rdi
	call	*%rdx
.L3823:
	movq	296(%r15), %r14
	testq	%r14, %r14
	je	.L3827
	movq	(%r14), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev(%rip), %rbx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	je	.L3899
	movq	%r14, %rdi
	call	*%rdx
.L3827:
	movq	256(%r15), %rdi
	leaq	272(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3854
	call	_ZdlPv@PLT
.L3854:
	movq	208(%r15), %rdi
	leaq	224(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3855
	call	_ZdlPv@PLT
.L3855:
	movq	160(%r15), %rdi
	leaq	176(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3856
	call	_ZdlPv@PLT
.L3856:
	movq	144(%r15), %rdi
	testq	%rdi, %rdi
	je	.L3857
	movq	(%rdi), %rdx
	call	*24(%rdx)
.L3857:
	movq	104(%r15), %rdi
	leaq	120(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3858
	call	_ZdlPv@PLT
.L3858:
	movq	56(%r15), %rdi
	leaq	72(%r15), %rdx
	cmpq	%rdx, %rdi
	je	.L3859
	call	_ZdlPv@PLT
.L3859:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3860
	call	_ZdlPv@PLT
.L3860:
	addq	$40, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
.L3898:
	.cfi_restore_state
	movq	56(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	leaq	72(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L3825
	call	_ZdlPv@PLT
.L3825:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3826
	call	_ZdlPv@PLT
.L3826:
	movl	$96, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3823
.L3899:
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE(%rip), %rax
	movq	%rax, (%r14)
	movq	160(%r14), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L3829
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, -64(%rbp)
.L3837:
	movq	-64(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L3830
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L3831
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3832
	movq	16(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L3833
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3834
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3833:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3835
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3836
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3835:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3831:
	addq	$8, -64(%rbp)
	jmp	.L3837
.L3830:
	movq	-56(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3838
	call	_ZdlPv@PLT
.L3838:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3829:
	movq	152(%r14), %rax
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L3839
	movq	8(%rax), %rsi
	movq	(%rax), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, -64(%rbp)
.L3849:
	movq	-64(%rbp), %rax
	cmpq	%rax, -72(%rbp)
	je	.L3840
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.L3841
	movq	0(%r13), %rdx
	leaq	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev(%rip), %rax
	movq	24(%rdx), %rdx
	cmpq	%rax, %rdx
	jne	.L3842
	movq	152(%r13), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE(%rip), %rax
	leaq	168(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L3843
	call	_ZdlPv@PLT
.L3843:
	movq	136(%r13), %rdi
	testq	%rdi, %rdi
	je	.L3844
	movq	(%rdi), %rdx
	movq	24(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L3845
	movq	%rdi, -80(%rbp)
	call	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	movq	-80(%rbp), %rdi
	movl	$168, %esi
	call	_ZdlPvm@PLT
.L3844:
	movq	96(%r13), %rdi
	leaq	112(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3846
	call	_ZdlPv@PLT
.L3846:
	movq	48(%r13), %rdi
	leaq	64(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3847
	call	_ZdlPv@PLT
.L3847:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rdx
	cmpq	%rdx, %rdi
	je	.L3848
	call	_ZdlPv@PLT
.L3848:
	movl	$192, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L3841:
	addq	$8, -64(%rbp)
	jmp	.L3849
.L3840:
	movq	-56(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L3850
	call	_ZdlPv@PLT
.L3850:
	movq	-56(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L3839:
	movq	104(%r14), %rdi
	leaq	120(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3851
	call	_ZdlPv@PLT
.L3851:
	movq	56(%r14), %rdi
	leaq	72(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3852
	call	_ZdlPv@PLT
.L3852:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rdx
	cmpq	%rdx, %rdi
	je	.L3853
	call	_ZdlPv@PLT
.L3853:
	movl	$168, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3827
.L3834:
	call	*%rdx
	jmp	.L3833
.L3842:
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L3841
.L3845:
	call	*%rdx
	jmp	.L3844
.L3832:
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L3831
.L3836:
	call	*%rdx
	jmp	.L3835
	.cfi_endproc
.LFE15199:
	.size	_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev, .-_ZThn8_N12v8_inspector8protocol7Runtime12RemoteObjectD0Ev
	.section	.text._ZN12v8_inspector9V8ConsoleC2EPNS_15V8InspectorImplE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8ConsoleC2EPNS_15V8InspectorImplE
	.type	_ZN12v8_inspector9V8ConsoleC2EPNS_15V8InspectorImplE, @function
_ZN12v8_inspector9V8ConsoleC2EPNS_15V8InspectorImplE:
.LFB8201:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12v8_inspector9V8ConsoleE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	ret
	.cfi_endproc
.LFE8201:
	.size	_ZN12v8_inspector9V8ConsoleC2EPNS_15V8InspectorImplE, .-_ZN12v8_inspector9V8ConsoleC2EPNS_15V8InspectorImplE
	.globl	_ZN12v8_inspector9V8ConsoleC1EPNS_15V8InspectorImplE
	.set	_ZN12v8_inspector9V8ConsoleC1EPNS_15V8InspectorImplE,_ZN12v8_inspector9V8ConsoleC2EPNS_15V8InspectorImplE
	.section	.text._ZN12v8_inspector9V8Console20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN12v8_inspector9V8Console20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN12v8_inspector9V8Console20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	16(%rax), %r13
	movq	0(%r13), %rax
	movq	136(%rax), %r12
	movq	(%rsi), %rax
	movq	8(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdx
	leaq	_ZN12v8_inspector17V8InspectorClient10memoryInfoEPN2v87IsolateENS1_5LocalINS1_7ContextEEE(%rip), %rax
	cmpq	%rax, %r12
	jne	.L3909
.L3901:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3909:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	*%r12
	testq	%rax, %rax
	je	.L3901
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	%rax, 24(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8237:
	.size	_ZN12v8_inspector9V8Console20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN12v8_inspector9V8Console20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN12v8_inspector9V8Console20memorySetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console20memorySetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN12v8_inspector9V8Console20memorySetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN12v8_inspector9V8Console20memorySetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8238:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8238:
	.size	_ZN12v8_inspector9V8Console20memorySetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN12v8_inspector9V8Console20memorySetterCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN12v8_inspector9V8Console12keysCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console12keysCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.type	_ZN12v8_inspector9V8Console12keysCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, @function
_ZN12v8_inspector9V8Console12keysCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi:
.LFB8239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$96, %rsp
	movq	(%rsi), %r14
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%r14), %r12
	movq	%r12, %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	testq	%rax, %rax
	je	.L3933
	movq	(%rax), %rax
.L3913:
	movq	%rax, 24(%r14)
	movq	%rbx, %rsi
	leaq	-128(%rbp), %rdi
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	movq	8(%r13), %r13
	movq	8(%r13), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	jg	.L3934
.L3911:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3935
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3934:
	.cfi_restore_state
	movq	-120(%rbp), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L3911
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jle	.L3936
	movq	-120(%rbp), %r13
.L3920:
	testq	%r13, %r13
	je	.L3911
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object19GetOwnPropertyNamesENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L3911
	movq	(%rbx), %rdx
	movq	(%rax), %rax
	movq	%rax, 24(%rdx)
	jmp	.L3911
	.p2align 4,,10
	.p2align 3
.L3936:
	movq	-128(%rbp), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L3920
	.p2align 4,,10
	.p2align 3
.L3933:
	movq	16(%r14), %rax
	jmp	.L3913
.L3935:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8239:
	.size	_ZN12v8_inspector9V8Console12keysCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, .-_ZN12v8_inspector9V8Console12keysCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_12keysCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12keysCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12keysCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12keysCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12keysCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3941
	movl	8(%rax), %edx
	movq	(%rax), %rdi
	addq	$72, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector9V8Console12keysCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
.L3941:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10177:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12keysCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12keysCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.text._ZN12v8_inspector9V8Console14valuesCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console14valuesCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.type	_ZN12v8_inspector9V8Console14valuesCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, @function
_ZN12v8_inspector9V8Console14valuesCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi:
.LFB8240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	(%rsi), %r12
	movq	%rsi, -152(%rbp)
	xorl	%esi, %esi
	movq	8(%r12), %r15
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	testq	%rax, %rax
	je	.L3976
	movq	(%rax), %rax
.L3944:
	movq	%rax, 24(%r12)
	movq	-152(%rbp), %rsi
	leaq	-144(%rbp), %rdi
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	movq	8(%rbx), %r12
	movq	8(%r12), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movl	-128(%rbp), %edx
	testl	%edx, %edx
	jg	.L3977
.L3942:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3978
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3977:
	.cfi_restore_state
	movq	-136(%rbp), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L3942
	movl	-128(%rbp), %eax
	testl	%eax, %eax
	jle	.L3979
	movq	-136(%rbp), %r14
.L3951:
	testq	%r14, %r14
	je	.L3942
	movq	%r15, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v86Object19GetOwnPropertyNamesENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L3942
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	_ZNK2v85Array6LengthEv@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	%rax, %r15
	jmp	.L3955
	.p2align 4,,10
	.p2align 3
.L3980:
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3958
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L3958
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector18createDataPropertyEN2v85LocalINS0_7ContextEEENS1_INS0_5ArrayEEEiNS1_INS0_5ValueEEE@PLT
.L3958:
	addl	$1, %ebx
.L3955:
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%ebx, %eax
	ja	.L3980
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	testq	%r15, %r15
	je	.L3981
	movq	(%r15), %rdx
.L3957:
	movq	%rdx, 24(%rax)
	jmp	.L3942
	.p2align 4,,10
	.p2align 3
.L3979:
	movq	-144(%rbp), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	jmp	.L3951
	.p2align 4,,10
	.p2align 3
.L3976:
	movq	16(%r12), %rax
	jmp	.L3944
	.p2align 4,,10
	.p2align 3
.L3981:
	movq	16(%rax), %rdx
	jmp	.L3957
.L3978:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8240:
	.size	_ZN12v8_inspector9V8Console14valuesCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, .-_ZN12v8_inspector9V8Console14valuesCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_14valuesCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_14valuesCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_14valuesCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_14valuesCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_14valuesCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3986
	movl	8(%rax), %edx
	movq	(%rax), %rdi
	addq	$72, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector9V8Console14valuesCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
.L3986:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10178:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_14valuesCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_14valuesCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.text._ZN12v8_inspector9V8Console21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.type	_ZN12v8_inspector9V8Console21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, @function
_ZN12v8_inspector9V8Console21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi:
.LFB8242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-144(%rbp), %rdi
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	movq	8(%r12), %r13
	movq	8(%r13), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movl	-128(%rbp), %edx
	testl	%edx, %edx
	jg	.L4012
.L3987:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4013
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4012:
	.cfi_restore_state
	movq	-136(%rbp), %rdi
	movl	%eax, %r15d
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L3987
	movl	-128(%rbp), %eax
	testl	%eax, %eax
	jle	.L4014
	movq	-136(%rbp), %r12
	jmp	.L3996
	.p2align 4,,10
	.p2align 3
.L4015:
	movq	%r12, %rdi
	call	_ZNK2v88Function16GetBoundFunctionEv@PLT
	movq	%rax, %r12
.L3996:
	movq	%r12, %rdi
	call	_ZNK2v88Function16GetBoundFunctionEv@PLT
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L4015
	testq	%r12, %r12
	je	.L3987
	xorl	%ebx, %ebx
	cmpl	$1, -128(%rbp)
	jle	.L3998
	movq	-136(%rbp), %rdx
	xorl	%ebx, %ebx
	movq	-8(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L3998
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L3998
	leaq	-8(%rdx), %rbx
	.p2align 4,,10
	.p2align 3
.L3998:
	movl	%r14d, %edx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl11sessionByIdEii@PLT
	testq	%rax, %rax
	je	.L3987
	movq	192(%rax), %rdi
	cmpb	$0, 32(%rdi)
	je	.L3987
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl16setBreakpointForEN2v85LocalINS1_8FunctionEEENS2_INS1_6StringEEENS0_16BreakpointSourceE@PLT
	jmp	.L3987
	.p2align 4,,10
	.p2align 3
.L4014:
	movq	-144(%rbp), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L3996
.L4013:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8242:
	.size	_ZN12v8_inspector9V8Console21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, .-_ZN12v8_inspector9V8Console21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4020
	movl	8(%rax), %edx
	movq	(%rax), %rdi
	addq	$72, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector9V8Console21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
.L4020:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10179:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.text._ZN12v8_inspector9V8Console23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.type	_ZN12v8_inspector9V8Console23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, @function
_ZN12v8_inspector9V8Console23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi:
.LFB8243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-128(%rbp), %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	movq	8(%r12), %r13
	movq	8(%r13), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	jg	.L4044
.L4021:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4045
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4044:
	.cfi_restore_state
	movq	-120(%rbp), %rdi
	movl	%eax, %r14d
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L4021
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jle	.L4046
	movq	-120(%rbp), %r12
	jmp	.L4030
	.p2align 4,,10
	.p2align 3
.L4047:
	movq	%r12, %rdi
	call	_ZNK2v88Function16GetBoundFunctionEv@PLT
	movq	%rax, %r12
.L4030:
	movq	%r12, %rdi
	call	_ZNK2v88Function16GetBoundFunctionEv@PLT
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L4047
	testq	%r12, %r12
	je	.L4021
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl11sessionByIdEii@PLT
	testq	%rax, %rax
	je	.L4021
	movq	192(%rax), %rdi
	cmpb	$0, 32(%rdi)
	je	.L4021
	movl	$1, %edx
	movq	%r12, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl19removeBreakpointForEN2v85LocalINS1_8FunctionEEENS0_16BreakpointSourceE@PLT
	jmp	.L4021
	.p2align 4,,10
	.p2align 3
.L4046:
	movq	-128(%rbp), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L4030
.L4045:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8243:
	.size	_ZN12v8_inspector9V8Console23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, .-_ZN12v8_inspector9V8Console23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10180:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4052
	movl	8(%rax), %edx
	movq	(%rax), %rdi
	addq	$72, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector9V8Console23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
.L4052:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10180:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.rodata._ZN12v8_inspector9V8Console23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi.str1.1,"aMS",@progbits,1
.LC6:
	.string	"console.log(\"function "
.LC7:
	.string	"(anonymous function)"
	.section	.rodata._ZN12v8_inspector9V8Console23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	" called\" + (arguments.length > 0 ? \" with arguments: \" + Array.prototype.join.call(arguments, \", \") : \"\")) && false"
	.section	.text._ZN12v8_inspector9V8Console23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.type	_ZN12v8_inspector9V8Console23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, @function
_ZN12v8_inspector9V8Console23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi:
.LFB8244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-256(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$248, %rsp
	movl	%edx, -272(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	movq	8(%r12), %r15
	movq	8(%r15), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r15, %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movl	-240(%rbp), %edx
	movl	%eax, -268(%rbp)
	testl	%edx, %edx
	jg	.L4092
.L4053:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4093
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4092:
	.cfi_restore_state
	movq	-248(%rbp), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L4053
	movl	-240(%rbp), %eax
	testl	%eax, %eax
	jle	.L4094
	movq	-248(%rbp), %r12
	jmp	.L4062
	.p2align 4,,10
	.p2align 3
.L4095:
	movq	%r12, %rdi
	call	_ZNK2v88Function16GetBoundFunctionEv@PLT
	movq	%rax, %r12
.L4062:
	movq	%r12, %rdi
	call	_ZNK2v88Function16GetBoundFunctionEv@PLT
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L4095
	testq	%r12, %r12
	je	.L4053
	movq	%r12, %rdi
	call	_ZNK2v88Function7GetNameEv@PLT
	movq	%rax, %rdx
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L4066
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L4064
.L4066:
	movq	%r12, %rdi
	call	_ZNK2v88Function15GetInferredNameEv@PLT
	movq	%rax, %rdx
.L4065:
	movq	(%rbx), %rax
	leaq	-144(%rbp), %r8
	leaq	-224(%rbp), %r14
	movq	%r8, %rdi
	movq	%r8, -280(%rbp)
	leaq	-96(%rbp), %r13
	movq	8(%rax), %rsi
	call	_ZN12v8_inspector29toProtocolStringWithTypeCheckEPN2v87IsolateENS0_5LocalINS0_5ValueEEE@PLT
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16BuilderC1Ev@PLT
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	-280(%rbp), %r8
	movq	%rax, -264(%rbp)
	cmpq	%rax, %rdi
	je	.L4067
	call	_ZdlPv@PLT
	movq	-280(%rbp), %r8
.L4067:
	cmpq	$0, -136(%rbp)
	jne	.L4068
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L4070
	call	_ZdlPv@PLT
.L4070:
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L4071
	call	_ZdlPv@PLT
.L4071:
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector15String16Builder8toStringEv@PLT
	movq	(%rbx), %rax
	movq	%r13, %rsi
	movq	8(%rax), %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movl	-272(%rbp), %edx
	movl	-268(%rbp), %esi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector15V8InspectorImpl11sessionByIdEii@PLT
	testq	%rax, %rax
	je	.L4072
	movq	192(%rax), %rdi
	cmpb	$0, 32(%rdi)
	je	.L4072
	movl	$2, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl16setBreakpointForEN2v85LocalINS1_8FunctionEEENS2_INS1_6StringEEENS0_16BreakpointSourceE@PLT
.L4072:
	movq	-96(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L4073
	call	_ZdlPv@PLT
.L4073:
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4074
	call	_ZdlPv@PLT
.L4074:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4053
	call	_ZdlPv@PLT
	jmp	.L4053
	.p2align 4,,10
	.p2align 3
.L4094:
	movq	-256(%rbp), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L4062
	.p2align 4,,10
	.p2align 3
.L4068:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector15String16Builder6appendERKNS_8String16E@PLT
	jmp	.L4070
	.p2align 4,,10
	.p2align 3
.L4064:
	movq	%rdx, %rdi
	movq	%rdx, -264(%rbp)
	call	_ZNK2v86String6LengthEv@PLT
	movq	-264(%rbp), %rdx
	testl	%eax, %eax
	je	.L4066
	jmp	.L4065
.L4093:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8244:
	.size	_ZN12v8_inspector9V8Console23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, .-_ZN12v8_inspector9V8Console23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10181:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4100
	movl	8(%rax), %edx
	movq	(%rax), %rdi
	addq	$72, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector9V8Console23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
.L4100:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10181:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.text._ZN12v8_inspector9V8Console25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.type	_ZN12v8_inspector9V8Console25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, @function
_ZN12v8_inspector9V8Console25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi:
.LFB8248:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-128(%rbp), %rdi
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	movq	8(%r12), %r13
	movq	8(%r13), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movl	-112(%rbp), %edx
	testl	%edx, %edx
	jg	.L4124
.L4101:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4125
	addq	$96, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4124:
	.cfi_restore_state
	movq	-120(%rbp), %rdi
	movl	%eax, %r14d
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L4101
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jle	.L4126
	movq	-120(%rbp), %r12
	jmp	.L4110
	.p2align 4,,10
	.p2align 3
.L4127:
	movq	%r12, %rdi
	call	_ZNK2v88Function16GetBoundFunctionEv@PLT
	movq	%rax, %r12
.L4110:
	movq	%r12, %rdi
	call	_ZNK2v88Function16GetBoundFunctionEv@PLT
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L4127
	testq	%r12, %r12
	je	.L4101
	movl	%r15d, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN12v8_inspector15V8InspectorImpl11sessionByIdEii@PLT
	testq	%rax, %rax
	je	.L4101
	movq	192(%rax), %rdi
	cmpb	$0, 32(%rdi)
	je	.L4101
	movl	$2, %edx
	movq	%r12, %rsi
	call	_ZN12v8_inspector19V8DebuggerAgentImpl19removeBreakpointForEN2v85LocalINS1_8FunctionEEENS0_16BreakpointSourceE@PLT
	jmp	.L4101
	.p2align 4,,10
	.p2align 3
.L4126:
	movq	-128(%rbp), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L4110
.L4125:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8248:
	.size	_ZN12v8_inspector9V8Console25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, .-_ZN12v8_inspector9V8Console25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4132
	movl	8(%rax), %edx
	movq	(%rax), %rdi
	addq	$72, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector9V8Console25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
.L4132:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10182:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.text._ZN12v8_inspector9V8Console28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.type	_ZN12v8_inspector9V8Console28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, @function
_ZN12v8_inspector9V8Console28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi:
.LFB8249:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	-128(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	movq	8(%r12), %r13
	movq	8(%r13), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	movl	%eax, %r12d
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movl	%r12d, %edx
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEii@PLT
	testq	%rax, %rax
	je	.L4133
	movq	%rax, %rdi
	movl	%r14d, %esi
	call	_ZN12v8_inspector16InspectedContext17getInjectedScriptEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4133
	movq	(%rbx), %rbx
	call	_ZNK12v8_inspector14InjectedScript20lastEvaluationResultEv@PLT
	testq	%rax, %rax
	je	.L4144
	movq	(%rax), %rax
.L4138:
	movq	%rax, 24(%rbx)
.L4133:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4145
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4144:
	.cfi_restore_state
	movq	16(%rbx), %rax
	jmp	.L4138
.L4145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8249:
	.size	_ZN12v8_inspector9V8Console28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, .-_ZN12v8_inspector9V8Console28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4150
	movl	8(%rax), %edx
	movq	(%rax), %rdi
	addq	$72, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector9V8Console28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
.L4150:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10186:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.text._ZN12v8_inspector9V8Console15inspectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console15inspectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.type	_ZN12v8_inspector9V8Console15inspectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, @function
_ZN12v8_inspector9V8Console15inspectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi:
.LFB8251:
	.cfi_startproc
	endbr64
	movl	16(%rsi), %ecx
	movq	%rdi, %rax
	movq	%rsi, %rdi
	testl	%ecx, %ecx
	jle	.L4151
	movq	8(%rsi), %rsi
	movq	8(%rax), %r8
	xorl	%ecx, %ecx
	jmp	_ZN12v8_inspectorL11inspectImplERKN2v820FunctionCallbackInfoINS0_5ValueEEENS0_5LocalIS2_EEiNS_12_GLOBAL__N_114InspectRequestEPNS_15V8InspectorImplE
	.p2align 4,,10
	.p2align 3
.L4151:
	ret
	.cfi_endproc
.LFE8251:
	.size	_ZN12v8_inspector9V8Console15inspectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, .-_ZN12v8_inspector9V8Console15inspectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.section	.text._ZN12v8_inspector9V8Console12copyCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console12copyCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.type	_ZN12v8_inspector9V8Console12copyCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, @function
_ZN12v8_inspector9V8Console12copyCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi:
.LFB8252:
	.cfi_startproc
	endbr64
	movl	16(%rsi), %ecx
	movq	%rdi, %rax
	movq	%rsi, %rdi
	testl	%ecx, %ecx
	jle	.L4153
	movq	8(%rsi), %rsi
	movq	8(%rax), %r8
	movl	$1, %ecx
	jmp	_ZN12v8_inspectorL11inspectImplERKN2v820FunctionCallbackInfoINS0_5ValueEEENS0_5LocalIS2_EEiNS_12_GLOBAL__N_114InspectRequestEPNS_15V8InspectorImplE
	.p2align 4,,10
	.p2align 3
.L4153:
	ret
	.cfi_endproc
.LFE8252:
	.size	_ZN12v8_inspector9V8Console12copyCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, .-_ZN12v8_inspector9V8Console12copyCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.section	.text._ZN12v8_inspector9V8Console20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.type	_ZN12v8_inspector9V8Console20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, @function
_ZN12v8_inspector9V8Console20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi:
.LFB8253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	16(%rsi), %eax
	testl	%eax, %eax
	jle	.L4155
	movq	8(%rsi), %r14
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movl	%edx, %r13d
	movq	%r14, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L4157
	movq	(%r12), %rax
	leaq	-112(%rbp), %r8
	movq	%r8, %rdi
	movq	%r8, -128(%rbp)
	movq	8(%rax), %r15
	movq	%r15, %rsi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	movq	%r15, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-120(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	-128(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L4160
	movq	%rax, %rdi
	movq	%r8, -120(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	-120(%rbp), %r8
	testb	%al, %al
	cmovne	%r15, %r14
.L4160:
	movq	%r8, %rdi
	movq	%r8, -120(%rbp)
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	movq	-120(%rbp), %r8
	testb	%al, %al
	movq	%r8, %rdi
	jne	.L4172
	call	_ZN2v88TryCatchD1Ev@PLT
.L4157:
	movq	8(%rbx), %r8
	movl	$2, %ecx
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspectorL11inspectImplERKN2v820FunctionCallbackInfoINS0_5ValueEEENS0_5LocalIS2_EEiNS_12_GLOBAL__N_114InspectRequestEPNS_15V8InspectorImplE
.L4155:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4173
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4172:
	.cfi_restore_state
	call	_ZN2v88TryCatch7ReThrowEv@PLT
	movq	-120(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	jmp	.L4155
.L4173:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8253:
	.size	_ZN12v8_inspector9V8Console20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, .-_ZN12v8_inspector9V8Console20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.section	.text._ZN12v8_inspector9V8Console15inspectedObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEEij,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console15inspectedObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEEij
	.type	_ZN12v8_inspector9V8Console15inspectedObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEEij, @function
_ZN12v8_inspector9V8Console15inspectedObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEEij:
.LFB8254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	-128(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v85debug20ConsoleCallArgumentsC1ERKNS_20FunctionCallbackInfoINS_5ValueEEE@PLT
	movq	8(%r13), %r13
	movq	8(%r13), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movl	%r14d, %edx
	movq	%r13, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector15V8InspectorImpl11sessionByIdEii@PLT
	testq	%rax, %rax
	je	.L4174
	movq	%rax, %rdi
	movl	%r12d, %esi
	call	_ZN12v8_inspector22V8InspectorSessionImpl15inspectedObjectEj@PLT
	movq	(%rbx), %rbx
	movq	%rax, %r12
	movq	8(%rbx), %rdi
	testq	%rax, %rax
	je	.L4176
	movq	(%rax), %rax
	movq	(%rax), %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	*%r13
	testq	%rax, %rax
	je	.L4184
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L4174:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4185
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4176:
	.cfi_restore_state
	cmpq	$-88, %rdi
	je	.L4184
	movq	88(%rdi), %rax
	movq	%rax, 24(%rbx)
	jmp	.L4174
	.p2align 4,,10
	.p2align 3
.L4184:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L4174
.L4185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8254:
	.size	_ZN12v8_inspector9V8Console15inspectedObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEEij, .-_ZN12v8_inspector9V8Console15inspectedObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEEij
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject4ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject4ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject4ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject4ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject4ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10191:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4190
	movl	8(%rax), %edx
	movq	(%rax), %rdi
	addq	$72, %rsp
	movq	%r12, %rsi
	movl	$4, %ecx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector9V8Console15inspectedObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEEij
.L4190:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10191:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject4ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject4ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject3ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject3ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject3ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject3ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject3ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4195
	movl	8(%rax), %edx
	movq	(%rax), %rdi
	addq	$72, %rsp
	movq	%r12, %rsi
	movl	$3, %ecx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector9V8Console15inspectedObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEEij
.L4195:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10190:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject3ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject3ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject2ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject2ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject2ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject2ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject2ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4200
	movl	8(%rax), %edx
	movq	(%rax), %rdi
	addq	$72, %rsp
	movq	%r12, %rsi
	movl	$2, %ecx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector9V8Console15inspectedObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEEij
.L4200:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10189:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject2ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject2ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject1ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject1ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject1ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject1ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject1ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10188:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4205
	movl	8(%rax), %edx
	movq	(%rax), %rdi
	addq	$72, %rsp
	movq	%r12, %rsi
	movl	$1, %ecx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector9V8Console15inspectedObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEEij
.L4205:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10188:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject1ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject1ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.text._ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject0ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,"axG",@progbits,_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject0ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_,comdat
	.p2align 4
	.weak	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject0ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.type	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject0ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, @function
_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject0ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_:
.LFB10187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	leaq	32(%rax), %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L4210
	movl	8(%rax), %edx
	movq	(%rax), %rdi
	addq	$72, %rsp
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector9V8Console15inspectedObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEEij
.L4210:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10187:
	.size	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject0ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_, .-_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject0ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_
	.section	.rodata._ZN12v8_inspector9V8Console19installMemoryGetterEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"memory"
	.section	.text._ZN12v8_inspector9V8Console19installMemoryGetterEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console19installMemoryGetterEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	.type	_ZN12v8_inspector9V8Console19installMemoryGetterEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE, @function
_ZN12v8_inspector9V8Console19installMemoryGetterEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE:
.LFB8255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v88External3NewEPNS_7IsolateEPv@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memorySetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4215
.L4212:
	movq	%r12, %rdi
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20memoryGetterCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEEEEEvS7_(%rip), %rsi
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4216
.L4213:
	movq	%r14, %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN12v8_inspector22toV8StringInternalizedEPN2v87IsolateEPKc@PLT
	addq	$8, %rsp
	movq	%r13, %rcx
	movq	%r12, %rdx
	popq	%rbx
	movq	%r15, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	xorl	%r9d, %r9d
	popq	%r14
	xorl	%r8d, %r8d
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v86Object19SetAccessorPropertyENS_5LocalINS_4NameEEENS1_INS_8FunctionEEES5_NS_17PropertyAttributeENS_13AccessControlE@PLT
	.p2align 4,,10
	.p2align 3
.L4215:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L4212
	.p2align 4,,10
	.p2align 3
.L4216:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L4213
	.cfi_endproc
.LFE8255:
	.size	_ZN12v8_inspector9V8Console19installMemoryGetterEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE, .-_ZN12v8_inspector9V8Console19installMemoryGetterEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"function dir(value) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1,"aMS",@progbits,1
.LC11:
	.string	"dir"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC12:
	.string	"function dirxml(value) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC13:
	.string	"dirxml"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC14:
	.string	"function profile(title) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC15:
	.string	"profile"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC16:
	.string	"function profileEnd(title) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC17:
	.string	"profileEnd"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC18:
	.string	"function clear() { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC19:
	.string	"clear"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC20:
	.string	"function table(data, [columns]) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC21:
	.string	"table"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC22:
	.string	"function keys(object) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC23:
	.string	"keys"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC24:
	.string	"function values(object) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC25:
	.string	"values"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC26:
	.string	"function debug(function, condition) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC27:
	.string	"debug"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC28:
	.string	"function undebug(function) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC29:
	.string	"undebug"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC30:
	.string	"function monitor(function) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC31:
	.string	"monitor"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC32:
	.string	"function unmonitor(function) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC33:
	.string	"unmonitor"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC34:
	.string	"function inspect(object) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC35:
	.string	"inspect"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC36:
	.string	"function copy(value) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC37:
	.string	"copy"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.8
	.align 8
.LC38:
	.string	"function queryObjects(constructor) { [Command Line API] }"
	.section	.rodata._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi.str1.1
.LC39:
	.string	"$_"
.LC40:
	.string	"$0"
.LC41:
	.string	"$1"
.LC42:
	.string	"$2"
.LC43:
	.string	"$3"
.LC44:
	.string	"$4"
	.section	.text._ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi
	.type	_ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi, @function
_ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi:
.LFB8256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$120, %rsp
	movl	%edx, -148(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %r14
	movq	%rax, %rsi
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	movq	%r14, %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	leaq	104(%r14), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v86Object12SetPrototypeENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%r14, %rdi
	movl	$16, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %rax
	subq	$8, %rsp
	movq	%r14, %rdx
	movl	-148(%rbp), %ecx
	leaq	.LC10(%rip), %r9
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, (%rax)
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_3DirERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE(%rip), %r8
	movl	%ecx, 8(%rax)
	leaq	.LC11(%rip), %rcx
	pushq	$0
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC12(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE(%rip), %r8
	movl	$0, (%rsp)
	leaq	.LC13(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC14(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE(%rip), %r8
	movl	$0, (%rsp)
	leaq	.LC15(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC16(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE(%rip), %r8
	movl	$0, (%rsp)
	leaq	.LC17(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC18(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5ClearERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE(%rip), %r8
	movl	$0, (%rsp)
	leaq	.LC19(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC20(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_5TableERKN2v85debug20ConsoleCallArgumentsERKNS3_14ConsoleContextEEEEEvRKNS2_20FunctionCallbackInfoINS2_5ValueEEE(%rip), %r8
	movl	$0, (%rsp)
	leaq	.LC21(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC22(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12keysCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	movl	$1, (%rsp)
	leaq	.LC23(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC24(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_14valuesCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	movl	$1, (%rsp)
	leaq	.LC25(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC26(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_21debugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	movl	$0, (%rsp)
	leaq	.LC27(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC28(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23undebugFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	movl	$0, (%rsp)
	leaq	.LC29(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC30(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_23monitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	movl	$0, (%rsp)
	leaq	.LC31(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC32(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_25unmonitorFunctionCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	movl	$0, (%rsp)
	leaq	.LC33(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC34(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_15inspectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	movl	$0, (%rsp)
	leaq	.LC35(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC36(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_12copyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	movl	$0, (%rsp)
	leaq	.LC37(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	.LC38(%rip), %r9
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_20queryObjectsCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	movl	$0, (%rsp)
	leaq	.LC4(%rip), %rcx
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	movq	%r13, %rsi
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_28lastEvaluationResultCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	leaq	.LC39(%rip), %rcx
	movq	%r12, %rdi
	movl	$1, (%rsp)
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	movq	%r13, %rsi
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject0ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	leaq	.LC40(%rip), %rcx
	movq	%r12, %rdi
	movl	$1, (%rsp)
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	movq	%r13, %rsi
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject1ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	leaq	.LC41(%rip), %rcx
	movq	%r12, %rdi
	movl	$1, (%rsp)
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	movq	%r13, %rsi
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject2ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	leaq	.LC42(%rip), %rcx
	movq	%r12, %rdi
	movl	$1, (%rsp)
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	movq	%r13, %rsi
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject3ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	leaq	.LC43(%rip), %rcx
	movq	%r12, %rdi
	movl	$1, (%rsp)
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	%r14, %rdx
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	leaq	.LC44(%rip), %rcx
	leaq	_ZN12v8_inspector9V8Console4callIXadL_ZNS0_16inspectedObject4ERKN2v820FunctionCallbackInfoINS2_5ValueEEEiEEEEvS7_(%rip), %r8
	movq	%r13, %rsi
	movl	$1, (%rsp)
	call	_ZN12v8_inspector12_GLOBAL__N_127createBoundFunctionPropertyEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEENS2_INS1_5ValueEEEPKcPFvRKNS1_20FunctionCallbackInfoIS7_EEESA_NS1_14SideEffectTypeE
	movq	8(%rbx), %rax
	popq	%rdx
	leaq	_ZN12v8_inspector17V8InspectorClient31installAdditionalCommandLineAPIEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE(%rip), %rdx
	popq	%rcx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	120(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4221
.L4218:
	movq	%r15, %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4222
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4221:
	.cfi_restore_state
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	*%rax
	jmp	.L4218
.L4222:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8256:
	.size	_ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi, .-_ZN12v8_inspector9V8Console20createCommandLineAPIEN2v85LocalINS1_7ContextEEEi
	.section	.text._ZN12v8_inspector9V8Console19CommandLineAPIScopeC2EN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEES7_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console19CommandLineAPIScopeC2EN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEES7_
	.type	_ZN12v8_inspector9V8Console19CommandLineAPIScopeC2EN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEES7_, @function
_ZN12v8_inspector9V8Console19CommandLineAPIScopeC2EN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEES7_:
.LFB8269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rsi, (%rdi)
	movq	%rdx, 8(%rdi)
	movq	%rcx, 16(%rdi)
	movq	%rsi, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v83Set3NewEPNS_7IsolateE@PLT
	movb	$0, 32(%r14)
	movq	%r13, %rdi
	movq	%rax, 24(%r14)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	$1, %edx
	movq	%rax, %rsi
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	movq	8(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN2v86Object19GetOwnPropertyNamesENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L4224
	movq	%r13, %rdi
	movq	%rax, %r12
	xorl	%ebx, %ebx
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88External3NewEPNS_7IsolateEPv@PLT
	movq	%r12, %rdi
	movq	%rax, -96(%rbp)
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%ebx, %eax
	jbe	.L4224
	.p2align 4,,10
	.p2align 3
.L4246:
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L4227
	movq	%rax, %rdi
	call	_ZNK2v85Value6IsNameEv@PLT
	testb	%al, %al
	je	.L4227
	movq	16(%r14), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN2v86Object3HasENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L4227
	shrw	$8, %ax
	jne	.L4227
	movq	24(%r14), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	_ZN2v83Set3AddENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L4245
	pushq	$0
	movq	16(%r14), %rdi
	leaq	_ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorSetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIvEE(%rip), %r8
	movq	%r15, %rdx
	pushq	$1
	movq	-96(%rbp), %r9
	leaq	_ZN12v8_inspector9V8Console19CommandLineAPIScope22accessorGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE(%rip), %rcx
	movq	%r13, %rsi
	pushq	$2
	movq	%rax, 24(%r14)
	pushq	$0
	call	_ZN2v86Object11SetAccessorENS_5LocalINS_7ContextEEENS1_INS_4NameEEEPFvS5_RKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS5_NS1_IS7_EERKNS6_IvEEENS_10MaybeLocalIS7_EENS_13AccessControlENS_17PropertyAttributeENS_14SideEffectTypeESN_@PLT
	addq	$32, %rsp
	testb	%al, %al
	je	.L4232
	shrw	$8, %ax
	je	.L4232
	.p2align 4,,10
	.p2align 3
.L4227:
	addl	$1, %ebx
.L4248:
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%ebx, %eax
	ja	.L4246
.L4224:
	movq	-88(%rbp), %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4247
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4232:
	.cfi_restore_state
	movq	24(%r14), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	addl	$1, %ebx
	call	_ZN2v83Set6DeleteENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	jmp	.L4248
	.p2align 4,,10
	.p2align 3
.L4245:
	movq	$0, 24(%r14)
	addl	$1, %ebx
	jmp	.L4248
.L4247:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8269:
	.size	_ZN12v8_inspector9V8Console19CommandLineAPIScopeC2EN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEES7_, .-_ZN12v8_inspector9V8Console19CommandLineAPIScopeC2EN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEES7_
	.globl	_ZN12v8_inspector9V8Console19CommandLineAPIScopeC1EN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEES7_
	.set	_ZN12v8_inspector9V8Console19CommandLineAPIScopeC1EN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEES7_,_ZN12v8_inspector9V8Console19CommandLineAPIScopeC2EN2v85LocalINS2_7ContextEEENS3_INS2_6ObjectEEES7_
	.section	.text._ZN12v8_inspector9V8Console19CommandLineAPIScopeD2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console19CommandLineAPIScopeD2Ev
	.type	_ZN12v8_inspector9V8Console19CommandLineAPIScopeD2Ev, @function
_ZN12v8_inspector9V8Console19CommandLineAPIScopeD2Ev:
.LFB8272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$40, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	movb	$1, 32(%r14)
	movq	24(%r14), %rdi
	call	_ZNK2v83Set7AsArrayEv@PLT
	movq	%rax, %r13
	jmp	.L4254
	.p2align 4,,10
	.p2align 3
.L4260:
	movq	(%r14), %rsi
	movl	%ebx, %edx
	movq	%r13, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L4252
	movq	%rax, %rdi
	call	_ZNK2v85Value6IsNameEv@PLT
	testb	%al, %al
	je	.L4252
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L4252
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L4259
	.p2align 4,,10
	.p2align 3
.L4252:
	addl	$1, %ebx
.L4254:
	movq	%r13, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%ebx, %eax
	ja	.L4260
	movq	%r15, %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4261
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4259:
	.cfi_restore_state
	movq	16(%r14), %rdi
	movq	(%r14), %rsi
	movq	%r12, %rdx
	call	_ZN2v86Object24GetOwnPropertyDescriptorENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	jmp	.L4252
.L4261:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8272:
	.size	_ZN12v8_inspector9V8Console19CommandLineAPIScopeD2Ev, .-_ZN12v8_inspector9V8Console19CommandLineAPIScopeD2Ev
	.globl	_ZN12v8_inspector9V8Console19CommandLineAPIScopeD1Ev
	.set	_ZN12v8_inspector9V8Console19CommandLineAPIScopeD1Ev,_ZN12v8_inspector9V8Console19CommandLineAPIScopeD2Ev
	.section	.rodata._ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_.str1.1,"aMS",@progbits,1
.LC45:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	.type	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_, @function
_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_:
.LFB11396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L4263
	movq	(%rsi), %rax
	movq	%rax, (%r12)
	addq	$8, 8(%rdi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4263:
	.cfi_restore_state
	movabsq	$1152921504606846975, %rcx
	movq	(%rdi), %r14
	movq	%r12, %rdx
	subq	%r14, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L4289
	testq	%rax, %rax
	je	.L4274
	movabsq	$9223372036854775800, %r15
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4290
.L4266:
	movq	%r15, %rdi
	movq	%rsi, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rax, %r13
	addq	%rax, %r15
	leaq	8(%rax), %rax
.L4267:
	movq	(%rsi), %rcx
	movq	%rcx, 0(%r13,%rdx)
	cmpq	%r14, %r12
	je	.L4268
	leaq	-8(%r12), %rsi
	leaq	15(%r13), %rax
	subq	%r14, %rsi
	subq	%r14, %rax
	movq	%rsi, %rcx
	shrq	$3, %rcx
	cmpq	$30, %rax
	jbe	.L4277
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rcx
	je	.L4277
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	shrq	%rax
	salq	$4, %rax
	.p2align 4,,10
	.p2align 3
.L4270:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, 0(%r13,%rdx)
	addq	$16, %rdx
	cmpq	%rax, %rdx
	jne	.L4270
	movq	%rcx, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r14,%rdx), %rax
	addq	%r13, %rdx
	cmpq	%rdi, %rcx
	je	.L4272
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.L4272:
	leaq	16(%r13,%rsi), %rax
.L4268:
	testq	%r14, %r14
	je	.L4273
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
.L4273:
	movq	%r13, %xmm0
	movq	%rax, %xmm2
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4290:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L4291
	movl	$8, %eax
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L4267
	.p2align 4,,10
	.p2align 3
.L4274:
	movl	$8, %r15d
	jmp	.L4266
	.p2align 4,,10
	.p2align 3
.L4277:
	movq	%r13, %rdx
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L4269:
	movq	(%rax), %rcx
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %r12
	jne	.L4269
	jmp	.L4272
.L4289:
	leaq	.LC45(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4291:
	cmpq	%rcx, %rdi
	cmovbe	%rdi, %rcx
	movq	%rcx, %r15
	salq	$3, %r15
	jmp	.L4266
	.cfi_endproc
.LFE11396:
	.size	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_, .-_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm.str1.1,"aMS",@progbits,1
.LC46:
	.string	"basic_string::_M_create"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm:
.LFB13541:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%r8, %r9
	subq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	addq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rax
	movq	%rsi, -72(%rbp)
	movq	%rax, %r13
	leaq	(%r9,%rax), %r15
	subq	%rsi, %r13
	cmpq	(%rdi), %r14
	je	.L4305
	movq	16(%rdi), %rax
.L4293:
	movabsq	$2305843009213693951, %rdx
	cmpq	%rdx, %r15
	ja	.L4326
	cmpq	%rax, %r15
	jbe	.L4295
	addq	%rax, %rax
	cmpq	%rax, %r15
	jnb	.L4295
	movabsq	$4611686018427387904, %rdi
	movq	%rdx, %r15
	cmpq	%rdx, %rax
	ja	.L4296
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L4295:
	leaq	2(%r15,%r15), %rdi
.L4296:
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	testq	%r12, %r12
	movq	(%rbx), %r11
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	movq	%rax, %r10
	je	.L4298
	cmpq	$1, %r12
	je	.L4327
	movq	%r12, %rdx
	addq	%rdx, %rdx
	je	.L4298
	movq	%r11, %rsi
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r11
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L4298:
	testq	%rcx, %rcx
	je	.L4300
	testq	%r8, %r8
	je	.L4300
	leaq	(%r10,%r12,2), %rdi
	cmpq	$1, %r8
	je	.L4328
	movq	%r8, %rdx
	addq	%rdx, %rdx
	je	.L4300
	movq	%rcx, %rsi
	movq	%r8, -80(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memcpy@PLT
	movq	-80(%rbp), %r8
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
.L4300:
	testq	%r13, %r13
	jne	.L4329
.L4302:
	cmpq	%r11, %r14
	je	.L4304
	movq	%r11, %rdi
	movq	%r10, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r10
.L4304:
	movq	%r15, 16(%rbx)
	movq	%r10, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4329:
	.cfi_restore_state
	movq	-72(%rbp), %rax
	addq	%r12, %r8
	leaq	(%r10,%r8,2), %rdi
	leaq	(%r11,%rax,2), %rsi
	cmpq	$1, %r13
	je	.L4330
	addq	%r13, %r13
	je	.L4302
	movq	%r13, %rdx
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r11
	jmp	.L4302
	.p2align 4,,10
	.p2align 3
.L4305:
	movl	$7, %eax
	jmp	.L4293
	.p2align 4,,10
	.p2align 3
.L4330:
	movzwl	(%rsi), %eax
	movw	%ax, (%rdi)
	jmp	.L4302
	.p2align 4,,10
	.p2align 3
.L4327:
	movzwl	(%r11), %eax
	movw	%ax, (%r10)
	jmp	.L4298
	.p2align 4,,10
	.p2align 3
.L4328:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	testq	%r13, %r13
	je	.L4302
	jmp	.L4329
.L4326:
	leaq	.LC46(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13541:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	.section	.rodata._ZNK12v8_inspector8String16plERKS0_.str1.8,"aMS",@progbits,1
	.align 8
.LC47:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNK12v8_inspector8String16plERKS0_,"axG",@progbits,_ZNK12v8_inspector8String16plERKS0_,comdat
	.align 2
	.p2align 4
	.weak	_ZNK12v8_inspector8String16plERKS0_
	.type	_ZNK12v8_inspector8String16plERKS0_, @function
_ZNK12v8_inspector8String16plERKS0_:
.LFB5062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	(%rsi), %r15
	movq	8(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -96(%rbp)
	leaq	(%rcx,%rcx), %r12
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L4332
	testq	%r15, %r15
	je	.L4361
.L4332:
	movq	%r12, %r8
	movq	%r14, %rdi
	sarq	%r8
	cmpq	$14, %r12
	ja	.L4362
.L4333:
	cmpq	$2, %r12
	je	.L4363
	testq	%r12, %r12
	je	.L4336
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	memmove@PLT
	movq	-96(%rbp), %rdi
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rcx
.L4336:
	xorl	%edx, %edx
	movq	%r8, -88(%rbp)
	movl	$7, %eax
	leaq	-96(%rbp), %r12
	movw	%dx, (%rdi,%rcx,2)
	movq	-96(%rbp), %rdx
	movq	8(%rbx), %r8
	movq	-88(%rbp), %rsi
	cmpq	%r14, %rdx
	cmovne	-80(%rbp), %rax
	movq	(%rbx), %rcx
	leaq	(%r8,%rsi), %rbx
	cmpq	%rax, %rbx
	ja	.L4338
	testq	%r8, %r8
	jne	.L4364
.L4339:
	xorl	%eax, %eax
	movq	%rbx, -88(%rbp)
	movq	%r13, %rdi
	movq	%r12, %rsi
	movw	%ax, (%rdx,%rbx,2)
	call	_ZN12v8_inspector8String16C1EONSt7__cxx1112basic_stringItSt11char_traitsItESaItEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L4331
	call	_ZdlPv@PLT
.L4331:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4365
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4364:
	.cfi_restore_state
	leaq	(%rdx,%rsi,2), %rdi
	cmpq	$1, %r8
	je	.L4366
	addq	%r8, %r8
	je	.L4339
	movq	%r8, %rdx
	movq	%rcx, %rsi
	call	memmove@PLT
	movq	-96(%rbp), %rdx
	jmp	.L4339
	.p2align 4,,10
	.p2align 3
.L4363:
	movzwl	(%r15), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rdi
	jmp	.L4336
	.p2align 4,,10
	.p2align 3
.L4362:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r8
	ja	.L4367
	leaq	2(%r12), %rdi
	movq	%r8, -112(%rbp)
	movq	%rcx, -104(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	%r8, -80(%rbp)
	jmp	.L4333
	.p2align 4,,10
	.p2align 3
.L4338:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_mutateEmmPKtm
	movq	-96(%rbp), %rdx
	jmp	.L4339
	.p2align 4,,10
	.p2align 3
.L4366:
	movzwl	(%rcx), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rdx
	jmp	.L4339
.L4361:
	leaq	.LC47(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L4365:
	call	__stack_chk_fail@PLT
.L4367:
	leaq	.LC46(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5062:
	.size	_ZNK12v8_inspector8String16plERKS0_, .-_ZNK12v8_inspector8String16plERKS0_
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_122consoleContextToStringEPN2v87IsolateERKNS1_5debug14ConsoleContextE.str1.1,"aMS",@progbits,1
.LC48:
	.string	"#"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_122consoleContextToStringEPN2v87IsolateERKNS1_5debug14ConsoleContextE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_122consoleContextToStringEPN2v87IsolateERKNS1_5debug14ConsoleContextE, @function
_ZN12v8_inspector12_GLOBAL__N_122consoleContextToStringEPN2v87IsolateERKNS1_5debug14ConsoleContextE:
.LFB8158:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 3, -56
	movl	(%rdx), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.L4369
	leaq	16(%rdi), %rax
	movq	$0, 32(%rdi)
	pxor	%xmm0, %xmm0
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, 16(%rdi)
.L4368:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4377
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4369:
	.cfi_restore_state
	leaq	-96(%rbp), %r14
	movq	%rdx, %rbx
	leaq	-240(%rbp), %r15
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	leaq	-192(%rbp), %r8
	leaq	.LC48(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -248(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	8(%rbx), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r13
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-248(%rbp), %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	%r12, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4371
	call	_ZdlPv@PLT
.L4371:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4372
	call	_ZdlPv@PLT
.L4372:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4373
	call	_ZdlPv@PLT
.L4373:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4368
	call	_ZdlPv@PLT
	jmp	.L4368
.L4377:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8158:
	.size	_ZN12v8_inspector12_GLOBAL__N_122consoleContextToStringEPN2v87IsolateERKNS1_5debug14ConsoleContextE, .-_ZN12v8_inspector12_GLOBAL__N_122consoleContextToStringEPN2v87IsolateERKNS1_5debug14ConsoleContextE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0, @function
_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0:
.LFB15180:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	xorl	%edx, %edx
	leaq	-96(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-104(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rsi
	call	_ZN12v8_inspector10V8Debugger17captureStackTraceEb@PLT
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_122consoleContextToStringEPN2v87IsolateERKNS1_5debug14ConsoleContextE
	movq	32(%rbx), %r8
	leaq	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv(%rip), %rdx
	pxor	%xmm0, %xmm0
	movq	16(%r8), %rdi
	movq	(%rdi), %rax
	movq	176(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4397
.L4379:
	subq	$8, %rsp
	movl	44(%rbx), %ecx
	movl	40(%rbx), %edx
	leaq	-112(%rbp), %rdi
	pushq	%r12
	movq	24(%rbx), %rsi
	movl	%r13d, %r9d
	pushq	%r15
	pushq	%r14
	call	_ZN12v8_inspector16V8ConsoleMessage19createForConsoleAPIEN2v85LocalINS1_7ContextEEEiiPNS_15V8InspectorImplEdNS_14ConsoleAPITypeERKSt6vectorINS2_INS1_5ValueEEESaISA_EERKNS_8String16ESt10unique_ptrINS_16V8StackTraceImplESt14default_deleteISJ_EE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	addq	$32, %rsp
	cmpq	%rax, %rdi
	je	.L4380
	call	_ZdlPv@PLT
.L4380:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4381
	movq	(%rdi), %rax
	call	*64(%rax)
.L4381:
	movl	44(%rbx), %esi
	movq	32(%rbx), %rdi
	call	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	-112(%rbp), %rax
	movq	$0, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS_16V8ConsoleMessageESt14default_deleteIS2_EE@PLT
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L4382
	movq	%r12, %rdi
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev@PLT
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4382:
	movq	-112(%rbp), %r12
	testq	%r12, %r12
	je	.L4378
	movq	%r12, %rdi
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev@PLT
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4378:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4398
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4397:
	.cfi_restore_state
	call	*%rax
	movq	32(%rbx), %r8
	jmp	.L4379
.L4398:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15180:
	.size	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0, .-_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE.str1.1,"aMS",@progbits,1
.LC49:
	.string	"vector::reserve"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE, @function
_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE:
.LFB8167:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movl	16(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L4455
.L4399:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4456
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4455:
	.cfi_restore_state
	movq	$0, -64(%rbp)
	movslq	16(%rax), %rcx
	pxor	%xmm0, %xmm0
	movabsq	$1152921504606846975, %rax
	movaps	%xmm0, -80(%rbp)
	cmpq	%rax, %rcx
	ja	.L4457
	movq	%rdi, %r15
	movl	%esi, %r12d
	testq	%rcx, %rcx
	jne	.L4458
.L4402:
	movl	44(%r15), %eax
	testl	%eax, %eax
	je	.L4423
	leaq	-80(%rbp), %rdx
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
.L4423:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4399
	call	_ZdlPv@PLT
	jmp	.L4399
	.p2align 4,,10
	.p2align 3
.L4458:
	leaq	0(,%rcx,8), %r13
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdi
	movq	%rax, %rbx
	cmpq	%rdi, %rcx
	je	.L4410
	leaq	-8(%rcx), %rdx
	leaq	15(%rdi), %rax
	subq	%rdi, %rdx
	subq	%rbx, %rax
	shrq	$3, %rdx
	cmpq	$30, %rax
	jbe	.L4426
	movabsq	$2305843009213693948, %rax
	testq	%rax, %rdx
	je	.L4426
	addq	$1, %rdx
	xorl	%eax, %eax
	movq	%rdx, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L4408:
	movdqu	(%rdi,%rax), %xmm2
	movups	%xmm2, (%rbx,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L4408
	movq	%rdx, %rsi
	andq	$-2, %rsi
	leaq	0(,%rsi,8), %rcx
	leaq	(%rdi,%rcx), %rax
	addq	%rbx, %rcx
	cmpq	%rsi, %rdx
	je	.L4404
	movq	(%rax), %rax
	movq	%rax, (%rcx)
.L4404:
	call	_ZdlPv@PLT
.L4405:
	movq	(%r15), %rax
	leaq	(%rbx,%r13), %rcx
	movq	%rbx, %xmm0
	movq	%rcx, -64(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movl	16(%rax), %edx
	movaps	%xmm0, -80(%rbp)
	testl	%edx, %edx
	jle	.L4402
	movabsq	$1152921504606846975, %r13
	xorl	%r14d, %r14d
	jmp	.L4422
	.p2align 4,,10
	.p2align 3
.L4459:
	movq	%rsi, (%rbx)
	movq	-72(%rbp), %rax
	leaq	8(%rax), %rbx
	movq	%rbx, -72(%rbp)
.L4412:
	movq	(%r15), %rax
	addq	$1, %r14
	cmpl	%r14d, 16(%rax)
	jle	.L4402
	movq	-64(%rbp), %rcx
.L4422:
	movq	8(%rax), %rax
	movq	%r14, %rsi
	negq	%rsi
	leaq	(%rax,%rsi,8), %rsi
	cmpq	%rcx, %rbx
	jne	.L4459
	movq	-80(%rbp), %r9
	movq	%rbx, %rcx
	subq	%r9, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%r13, %rax
	je	.L4460
	testq	%rax, %rax
	je	.L4427
	movabsq	$9223372036854775800, %r10
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4461
.L4414:
	movq	%r10, %rdi
	movq	%rcx, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %rcx
	leaq	8(%rax), %rdi
	addq	%rax, %r10
.L4415:
	movq	%rsi, (%rax,%rcx)
	cmpq	%r9, %rbx
	je	.L4430
	movq	%rbx, %rcx
	subq	%r9, %rcx
	leaq	-8(%rcx), %rsi
	leaq	15(%rax), %rcx
	movq	%rsi, %rdi
	subq	%r9, %rcx
	shrq	$3, %rdi
	cmpq	$30, %rcx
	jbe	.L4431
	movabsq	$2305843009213693948, %rdx
	testq	%rdx, %rdi
	je	.L4431
	leaq	1(%rdi), %r11
	xorl	%ecx, %ecx
	movq	%r11, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L4418:
	movdqu	(%r9,%rcx), %xmm1
	movups	%xmm1, (%rax,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rdi
	jne	.L4418
	movq	%r11, %rbx
	andq	$-2, %rbx
	leaq	0(,%rbx,8), %rdi
	leaq	(%r9,%rdi), %rcx
	addq	%rax, %rdi
	cmpq	%rbx, %r11
	je	.L4420
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
.L4420:
	leaq	16(%rax,%rsi), %rbx
	testq	%r9, %r9
	je	.L4421
.L4416:
	movq	%r9, %rdi
	movq	%rax, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %r10
.L4421:
	movq	%rax, %xmm0
	movq	%rbx, %xmm3
	movq	%r10, -64(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -80(%rbp)
	jmp	.L4412
	.p2align 4,,10
	.p2align 3
.L4461:
	testq	%rdi, %rdi
	jne	.L4462
	movl	$8, %edi
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	jmp	.L4415
	.p2align 4,,10
	.p2align 3
.L4427:
	movl	$8, %r10d
	jmp	.L4414
	.p2align 4,,10
	.p2align 3
.L4431:
	movq	%rax, %rdi
	movq	%r9, %rcx
	.p2align 4,,10
	.p2align 3
.L4417:
	movq	(%rcx), %r11
	addq	$8, %rcx
	addq	$8, %rdi
	movq	%r11, -8(%rdi)
	cmpq	%rbx, %rcx
	jne	.L4417
	jmp	.L4420
	.p2align 4,,10
	.p2align 3
.L4430:
	movq	%rdi, %rbx
	jmp	.L4416
.L4426:
	movq	%rbx, %rdx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L4406:
	movq	(%rax), %rsi
	addq	$8, %rax
	addq	$8, %rdx
	movq	%rsi, -8(%rdx)
	cmpq	%rax, %rcx
	jne	.L4406
.L4410:
	testq	%rdi, %rdi
	je	.L4405
	jmp	.L4404
.L4456:
	call	__stack_chk_fail@PLT
.L4460:
	leaq	.LC45(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4457:
	leaq	.LC49(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4462:
	cmpq	%r13, %rdi
	cmova	%r13, %rdi
	movq	%rdi, %r10
	salq	$3, %r10
	jmp	.L4414
	.cfi_endproc
.LFE8167:
	.size	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE, .-_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE
	.section	.text._ZN12v8_inspector9V8Console5DebugERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console5DebugERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console5DebugERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console5DebugERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -48(%rbp)
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -40(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	-80(%rbp), %rdi
	movl	$1, %esi
	movl	%eax, -36(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4466
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4466:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8203:
	.size	_ZN12v8_inspector9V8Console5DebugERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console5DebugERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.text._ZN12v8_inspector9V8Console5ErrorERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console5ErrorERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console5ErrorERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console5ErrorERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -48(%rbp)
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -40(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	-80(%rbp), %rdi
	movl	$3, %esi
	movl	%eax, -36(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4470
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4470:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8204:
	.size	_ZN12v8_inspector9V8Console5ErrorERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console5ErrorERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.text._ZN12v8_inspector9V8Console4InfoERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console4InfoERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console4InfoERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console4InfoERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -48(%rbp)
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -40(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	-80(%rbp), %rdi
	movl	$2, %esi
	movl	%eax, -36(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4474
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4474:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8205:
	.size	_ZN12v8_inspector9V8Console4InfoERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console4InfoERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.text._ZN12v8_inspector9V8Console3LogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console3LogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console3LogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console3LogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -48(%rbp)
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -40(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	xorl	%esi, %esi
	leaq	-80(%rbp), %rdi
	movl	%eax, -36(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4478
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4478:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8206:
	.size	_ZN12v8_inspector9V8Console3LogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console3LogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.text._ZN12v8_inspector9V8Console4WarnERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console4WarnERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console4WarnERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console4WarnERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8207:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -48(%rbp)
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -40(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	-80(%rbp), %rdi
	movl	$4, %esi
	movl	%eax, -36(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4482
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4482:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8207:
	.size	_ZN12v8_inspector9V8Console4WarnERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console4WarnERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.text._ZN12v8_inspector9V8Console3DirERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console3DirERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console3DirERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console3DirERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8208:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -48(%rbp)
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -40(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	-80(%rbp), %rdi
	movl	$5, %esi
	movl	%eax, -36(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4486
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4486:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8208:
	.size	_ZN12v8_inspector9V8Console3DirERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console3DirERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.text._ZN12v8_inspector9V8Console6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8209:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -48(%rbp)
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -40(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	-80(%rbp), %rdi
	movl	$6, %esi
	movl	%eax, -36(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4490
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4490:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8209:
	.size	_ZN12v8_inspector9V8Console6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.text._ZN12v8_inspector9V8Console5TableERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console5TableERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console5TableERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console5TableERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -24
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -48(%rbp)
	movq	%rax, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-48(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -40(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	-80(%rbp), %rdi
	movl	$7, %esi
	movl	%eax, -36(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeE
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4494
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4494:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8210:
	.size	_ZN12v8_inspector9V8Console5TableERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console5TableERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.rodata._ZN12v8_inspector9V8Console5ClearERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE.str1.1,"aMS",@progbits,1
.LC50:
	.string	"console.clear"
	.section	.text._ZN12v8_inspector9V8Console5ClearERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console5ClearERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console5ClearERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console5ClearERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8215:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -144(%rbp)
	movq	8(%r12), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, -112(%rbp)
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -104(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movl	%eax, -100(%rbp)
	testl	%eax, %eax
	je	.L4495
	movl	%eax, %esi
	movq	8(%rbx), %rax
	leaq	_ZN12v8_inspector17V8InspectorClient12consoleClearEi(%rip), %rdx
	movq	16(%rax), %rdi
	movq	(%rdi), %rax
	movq	168(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4536
.L4498:
	leaq	-96(%rbp), %r12
	leaq	.LC50(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-144(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	$0, -160(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	16(%rdx), %eax
	testl	%eax, %eax
	jle	.L4499
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	movabsq	$1152921504606846975, %r13
	jmp	.L4511
	.p2align 4,,10
	.p2align 3
.L4537:
	movq	%rcx, (%rbx)
	movq	-168(%rbp), %rax
	leaq	8(%rax), %rbx
	movq	%rbx, -168(%rbp)
.L4501:
	movq	-144(%rbp), %rdx
	addq	$1, %r15
	movl	16(%rdx), %eax
	cmpl	%r15d, %eax
	jle	.L4499
	movq	-160(%rbp), %rax
.L4511:
	movq	8(%rdx), %rdx
	movq	%r15, %rcx
	negq	%rcx
	leaq	(%rdx,%rcx,8), %rcx
	cmpq	%rbx, %rax
	jne	.L4537
	movq	-176(%rbp), %r8
	subq	%r8, %rax
	movq	%rax, %rdx
	sarq	$3, %rax
	cmpq	%r13, %rax
	je	.L4538
	testq	%rax, %rax
	je	.L4517
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4539
.L4503:
	movq	%r14, %rdi
	movq	%rdx, -216(%rbp)
	movq	%r8, -208(%rbp)
	movq	%rcx, -200(%rbp)
	call	_Znwm@PLT
	movq	-200(%rbp), %rcx
	movq	-216(%rbp), %rdx
	movq	-208(%rbp), %r8
	leaq	(%rax,%r14), %r9
	leaq	8(%rax), %rdi
	movq	%rcx, (%rax,%rdx)
	cmpq	%rbx, %r8
	je	.L4520
.L4542:
	movq	%rbx, %rdx
	subq	%r8, %rdx
	leaq	-8(%rdx), %rdi
	leaq	15(%rax), %rdx
	movq	%rdi, %rcx
	subq	%r8, %rdx
	shrq	$3, %rcx
	cmpq	$30, %rdx
	jbe	.L4521
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rcx
	je	.L4521
	addq	$1, %rcx
	xorl	%edx, %edx
	movq	%rcx, %r10
	shrq	%r10
	salq	$4, %r10
	.p2align 4,,10
	.p2align 3
.L4507:
	movdqu	(%r8,%rdx), %xmm2
	movups	%xmm2, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %r10
	jne	.L4507
	movq	%rcx, %r10
	andq	$-2, %r10
	leaq	0(,%r10,8), %rdx
	leaq	(%r8,%rdx), %r11
	addq	%rax, %rdx
	cmpq	%r10, %rcx
	je	.L4509
	movq	(%r11), %rcx
	movq	%rcx, (%rdx)
.L4509:
	leaq	16(%rax,%rdi), %rbx
.L4505:
	testq	%r8, %r8
	je	.L4510
	movq	%r8, %rdi
	movq	%rax, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rax
	movq	-200(%rbp), %r9
.L4510:
	movq	%rax, %xmm0
	movq	%rbx, %xmm3
	movq	%r9, -160(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -176(%rbp)
	jmp	.L4501
	.p2align 4,,10
	.p2align 3
.L4499:
	testl	%eax, %eax
	jne	.L4512
	movq	-128(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	leaq	-184(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	movq	%rax, -184(%rbp)
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
.L4512:
	movl	-100(%rbp), %eax
	testl	%eax, %eax
	je	.L4513
	leaq	-176(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	movl	$12, %esi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
.L4513:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4514
	call	_ZdlPv@PLT
.L4514:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4495
	call	_ZdlPv@PLT
.L4495:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4540
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4539:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L4541
	xorl	%eax, %eax
	movl	$8, %edi
	xorl	%r9d, %r9d
	movq	%rcx, (%rax,%rdx)
	cmpq	%rbx, %r8
	jne	.L4542
.L4520:
	movq	%rdi, %rbx
	jmp	.L4505
	.p2align 4,,10
	.p2align 3
.L4517:
	movl	$8, %r14d
	jmp	.L4503
	.p2align 4,,10
	.p2align 3
.L4521:
	movq	%rax, %rcx
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L4506:
	movq	(%rdx), %r10
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%r10, -8(%rcx)
	cmpq	%rbx, %rdx
	jne	.L4506
	jmp	.L4509
	.p2align 4,,10
	.p2align 3
.L4536:
	call	*%rax
	jmp	.L4498
.L4540:
	call	__stack_chk_fail@PLT
.L4538:
	leaq	.LC45(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4541:
	cmpq	%r13, %rdi
	cmova	%r13, %rdi
	leaq	0(,%rdi,8), %r14
	jmp	.L4503
	.cfi_endproc
.LFE8215:
	.size	_ZN12v8_inspector9V8Console5ClearERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console5ClearERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.rodata._ZN12v8_inspector9V8Console6AssertERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE.str1.1,"aMS",@progbits,1
.LC51:
	.string	"console.assert"
	.section	.text._ZN12v8_inspector9V8Console6AssertERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console6AssertERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console6AssertERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console6AssertERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rdx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -144(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -112(%rbp)
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -104(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	pxor	%xmm0, %xmm0
	cmpl	$1, 16(%r14)
	movq	$0, -160(%rbp)
	movl	%eax, -100(%rbp)
	movaps	%xmm0, -176(%rbp)
	jle	.L4559
	movq	%r13, -224(%rbp)
	movq	$-8, %r15
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	movl	$1, %r12d
	movq	%r14, %r13
	jmp	.L4544
	.p2align 4,,10
	.p2align 3
.L4587:
	movq	%rdx, (%rbx)
	movq	-168(%rbp), %rax
	leaq	8(%rax), %rbx
	movq	%rbx, -168(%rbp)
.L4548:
	movl	16(%r13), %eax
	addl	$1, %r12d
	subq	$8, %r15
	cmpl	%eax, %r12d
	jge	.L4558
	movq	-160(%rbp), %rcx
.L4544:
	movq	8(%r13), %rdx
	addq	%r15, %rdx
	cmpq	%rbx, %rcx
	jne	.L4587
	movq	-176(%rbp), %r8
	movabsq	$1152921504606846975, %rsi
	subq	%r8, %rcx
	movq	%rcx, %rax
	movq	%rcx, %r14
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L4588
	testq	%rax, %rax
	je	.L4565
	movabsq	$9223372036854775800, %r9
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L4589
.L4550:
	movq	%r9, %rdi
	movq	%r8, -216(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	_Znwm@PLT
	movq	-200(%rbp), %r9
	movq	-208(%rbp), %rdx
	movq	-216(%rbp), %r8
	leaq	8(%rax), %rcx
	movq	%rdx, (%rax,%r14)
	addq	%rax, %r9
	cmpq	%r8, %rbx
	je	.L4568
.L4591:
	movq	%rbx, %rdx
	subq	%r8, %rdx
	leaq	-8(%rdx), %rdi
	leaq	15(%rax), %rdx
	movq	%rdi, %r10
	subq	%r8, %rdx
	shrq	$3, %r10
	cmpq	$30, %rdx
	jbe	.L4569
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %r10
	je	.L4569
	addq	$1, %r10
	xorl	%edx, %edx
	movq	%r10, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L4554:
	movdqu	(%r8,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L4554
	movq	%r10, %r11
	andq	$-2, %r11
	leaq	0(,%r11,8), %rcx
	leaq	(%r8,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%r11, %r10
	je	.L4556
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L4556:
	leaq	16(%rax,%rdi), %rbx
.L4552:
	testq	%r8, %r8
	je	.L4557
	movq	%r8, %rdi
	movq	%rax, -208(%rbp)
	movq	%r9, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-208(%rbp), %rax
	movq	-200(%rbp), %r9
.L4557:
	movq	%rax, %xmm0
	movq	%rbx, %xmm2
	movq	%r9, -160(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -176(%rbp)
	jmp	.L4548
	.p2align 4,,10
	.p2align 3
.L4589:
	testq	%rcx, %rcx
	jne	.L4590
	xorl	%eax, %eax
	movl	$8, %ecx
	xorl	%r9d, %r9d
	movq	%rdx, (%rax,%r14)
	cmpq	%r8, %rbx
	jne	.L4591
.L4568:
	movq	%rcx, %rbx
	jmp	.L4552
	.p2align 4,,10
	.p2align 3
.L4558:
	movq	-224(%rbp), %r13
	cmpl	$1, %eax
	jle	.L4559
.L4560:
	movl	-100(%rbp), %esi
	testl	%esi, %esi
	je	.L4561
	movl	$13, %esi
	leaq	-176(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
	movl	-100(%rbp), %esi
.L4561:
	movq	8(%r13), %rax
	movq	24(%rax), %rdi
	call	_ZN12v8_inspector10V8Debugger20breakProgramOnAssertEi@PLT
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4543
	call	_ZdlPv@PLT
.L4543:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4592
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4565:
	.cfi_restore_state
	movl	$8, %r9d
	jmp	.L4550
	.p2align 4,,10
	.p2align 3
.L4559:
	leaq	-96(%rbp), %r12
	leaq	.LC51(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	8(%r13), %rax
	movq	%r12, %rsi
	movq	8(%rax), %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	leaq	-176(%rbp), %rdi
	leaq	-184(%rbp), %rsi
	movq	%rax, -184(%rbp)
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4560
	call	_ZdlPv@PLT
	jmp	.L4560
	.p2align 4,,10
	.p2align 3
.L4569:
	movq	%rax, %rcx
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L4553:
	movq	(%rdx), %rsi
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%rsi, -8(%rcx)
	cmpq	%rdx, %rbx
	jne	.L4553
	jmp	.L4556
.L4588:
	leaq	.LC45(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4592:
	call	__stack_chk_fail@PLT
.L4590:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	movq	%rax, %r9
	cmovbe	%rcx, %r9
	salq	$3, %r9
	jmp	.L4550
	.cfi_endproc
.LFE8223:
	.size	_ZN12v8_inspector9V8Console6AssertERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console6AssertERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.rodata._ZN12v8_inspector9V8Console5GroupERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE.str1.1,"aMS",@progbits,1
.LC52:
	.string	"console.group"
	.section	.text._ZN12v8_inspector9V8Console5GroupERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console5GroupERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console5GroupERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console5GroupERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -144(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -112(%rbp)
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -104(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	.LC52(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -100(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-144(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, -160(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	16(%rcx), %eax
	testl	%eax, %eax
	jle	.L4594
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	jmp	.L4606
	.p2align 4,,10
	.p2align 3
.L4631:
	movq	%r10, (%r15)
	movq	-168(%rbp), %rax
	leaq	8(%rax), %r15
	movq	%r15, -168(%rbp)
.L4596:
	movq	-144(%rbp), %rcx
	addq	$1, %r14
	movl	16(%rcx), %eax
	cmpl	%r14d, %eax
	jle	.L4594
	movq	-160(%rbp), %rax
.L4606:
	movq	8(%rcx), %rcx
	movq	%r14, %rdi
	negq	%rdi
	leaq	(%rcx,%rdi,8), %r10
	cmpq	%r15, %rax
	jne	.L4631
	movq	-176(%rbp), %r8
	movabsq	$1152921504606846975, %rsi
	subq	%r8, %rax
	movq	%rax, %rcx
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L4632
	testq	%rax, %rax
	je	.L4612
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4633
.L4598:
	movq	%r13, %rdi
	movq	%rcx, -216(%rbp)
	movq	%r8, -208(%rbp)
	movq	%r10, -200(%rbp)
	call	_Znwm@PLT
	movq	-200(%rbp), %r10
	movq	-216(%rbp), %rcx
	movq	-208(%rbp), %r8
	movq	%rax, %rbx
	leaq	(%rax,%r13), %r9
	leaq	8(%rax), %rax
	movq	%r10, (%rbx,%rcx)
	cmpq	%r15, %r8
	je	.L4615
.L4635:
	movq	%r15, %rax
	leaq	15(%rbx), %rdi
	subq	%r8, %rax
	subq	%r8, %rdi
	leaq	-8(%rax), %rcx
	movq	%rcx, %rax
	shrq	$3, %rax
	cmpq	$30, %rdi
	jbe	.L4616
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rax
	je	.L4616
	addq	$1, %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L4602:
	movdqu	(%r8,%rdx), %xmm2
	movups	%xmm2, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L4602
	movq	%rax, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r8,%rdx), %r10
	addq	%rbx, %rdx
	cmpq	%rdi, %rax
	je	.L4604
	movq	(%r10), %rax
	movq	%rax, (%rdx)
.L4604:
	leaq	16(%rbx,%rcx), %r15
.L4600:
	testq	%r8, %r8
	je	.L4605
	movq	%r8, %rdi
	movq	%r9, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %r9
.L4605:
	movq	%rbx, %xmm0
	movq	%r15, %xmm3
	movq	%r9, -160(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -176(%rbp)
	jmp	.L4596
	.p2align 4,,10
	.p2align 3
.L4633:
	testq	%rdi, %rdi
	jne	.L4634
	xorl	%ebx, %ebx
	movl	$8, %eax
	xorl	%r9d, %r9d
	movq	%r10, (%rbx,%rcx)
	cmpq	%r15, %r8
	jne	.L4635
.L4615:
	movq	%rax, %r15
	jmp	.L4600
	.p2align 4,,10
	.p2align 3
.L4594:
	testl	%eax, %eax
	je	.L4636
.L4607:
	movl	-100(%rbp), %eax
	testl	%eax, %eax
	je	.L4608
	leaq	-176(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	movl	$9, %esi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
.L4608:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4609
	call	_ZdlPv@PLT
.L4609:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4593
	call	_ZdlPv@PLT
.L4593:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4637
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4612:
	.cfi_restore_state
	movl	$8, %r13d
	jmp	.L4598
	.p2align 4,,10
	.p2align 3
.L4636:
	movq	-128(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	leaq	-184(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	movq	%rax, -184(%rbp)
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	jmp	.L4607
	.p2align 4,,10
	.p2align 3
.L4616:
	movq	%rbx, %rdi
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L4601:
	movq	(%rax), %r10
	addq	$8, %rax
	addq	$8, %rdi
	movq	%r10, -8(%rdi)
	cmpq	%rax, %r15
	jne	.L4601
	jmp	.L4604
.L4632:
	leaq	.LC45(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4637:
	call	__stack_chk_fail@PLT
.L4634:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	%rax, %r9
	cmovbe	%rdi, %r9
	leaq	0(,%r9,8), %r13
	jmp	.L4598
	.cfi_endproc
.LFE8212:
	.size	_ZN12v8_inspector9V8Console5GroupERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console5GroupERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.rodata._ZN12v8_inspector9V8Console5TraceERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE.str1.1,"aMS",@progbits,1
.LC53:
	.string	"console.trace"
	.section	.text._ZN12v8_inspector9V8Console5TraceERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console5TraceERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console5TraceERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console5TraceERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -144(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -112(%rbp)
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -104(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	.LC53(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -100(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-144(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, -160(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	16(%rcx), %eax
	testl	%eax, %eax
	jle	.L4639
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	jmp	.L4651
	.p2align 4,,10
	.p2align 3
.L4676:
	movq	%r10, (%r15)
	movq	-168(%rbp), %rax
	leaq	8(%rax), %r15
	movq	%r15, -168(%rbp)
.L4641:
	movq	-144(%rbp), %rcx
	addq	$1, %r14
	movl	16(%rcx), %eax
	cmpl	%r14d, %eax
	jle	.L4639
	movq	-160(%rbp), %rax
.L4651:
	movq	8(%rcx), %rcx
	movq	%r14, %rdi
	negq	%rdi
	leaq	(%rcx,%rdi,8), %r10
	cmpq	%r15, %rax
	jne	.L4676
	movq	-176(%rbp), %r8
	movabsq	$1152921504606846975, %rsi
	subq	%r8, %rax
	movq	%rax, %rcx
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L4677
	testq	%rax, %rax
	je	.L4657
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4678
.L4643:
	movq	%r13, %rdi
	movq	%rcx, -216(%rbp)
	movq	%r8, -208(%rbp)
	movq	%r10, -200(%rbp)
	call	_Znwm@PLT
	movq	-200(%rbp), %r10
	movq	-216(%rbp), %rcx
	movq	-208(%rbp), %r8
	movq	%rax, %rbx
	leaq	(%rax,%r13), %r9
	leaq	8(%rax), %rax
	movq	%r10, (%rbx,%rcx)
	cmpq	%r15, %r8
	je	.L4660
.L4680:
	movq	%r15, %rax
	leaq	15(%rbx), %rdi
	subq	%r8, %rax
	subq	%r8, %rdi
	leaq	-8(%rax), %rcx
	movq	%rcx, %rax
	shrq	$3, %rax
	cmpq	$30, %rdi
	jbe	.L4661
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rax
	je	.L4661
	addq	$1, %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L4647:
	movdqu	(%r8,%rdx), %xmm2
	movups	%xmm2, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L4647
	movq	%rax, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r8,%rdx), %r10
	addq	%rbx, %rdx
	cmpq	%rdi, %rax
	je	.L4649
	movq	(%r10), %rax
	movq	%rax, (%rdx)
.L4649:
	leaq	16(%rbx,%rcx), %r15
.L4645:
	testq	%r8, %r8
	je	.L4650
	movq	%r8, %rdi
	movq	%r9, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %r9
.L4650:
	movq	%rbx, %xmm0
	movq	%r15, %xmm3
	movq	%r9, -160(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -176(%rbp)
	jmp	.L4641
	.p2align 4,,10
	.p2align 3
.L4678:
	testq	%rdi, %rdi
	jne	.L4679
	xorl	%ebx, %ebx
	movl	$8, %eax
	xorl	%r9d, %r9d
	movq	%r10, (%rbx,%rcx)
	cmpq	%r15, %r8
	jne	.L4680
.L4660:
	movq	%rax, %r15
	jmp	.L4645
	.p2align 4,,10
	.p2align 3
.L4639:
	testl	%eax, %eax
	je	.L4681
.L4652:
	movl	-100(%rbp), %eax
	testl	%eax, %eax
	je	.L4653
	leaq	-176(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	movl	$8, %esi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
.L4653:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4654
	call	_ZdlPv@PLT
.L4654:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4638
	call	_ZdlPv@PLT
.L4638:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4682
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4657:
	.cfi_restore_state
	movl	$8, %r13d
	jmp	.L4643
	.p2align 4,,10
	.p2align 3
.L4681:
	movq	-128(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	leaq	-184(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	movq	%rax, -184(%rbp)
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	jmp	.L4652
	.p2align 4,,10
	.p2align 3
.L4661:
	movq	%rbx, %rdi
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L4646:
	movq	(%rax), %r10
	addq	$8, %rax
	addq	$8, %rdi
	movq	%r10, -8(%rdi)
	cmpq	%rax, %r15
	jne	.L4646
	jmp	.L4649
.L4677:
	leaq	.LC45(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4682:
	call	__stack_chk_fail@PLT
.L4679:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	%rax, %r9
	cmovbe	%rdi, %r9
	leaq	0(,%r9,8), %r13
	jmp	.L4643
	.cfi_endproc
.LFE8211:
	.size	_ZN12v8_inspector9V8Console5TraceERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console5TraceERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.rodata._ZN12v8_inspector9V8Console8GroupEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE.str1.1,"aMS",@progbits,1
.LC54:
	.string	"console.groupEnd"
	.section	.text._ZN12v8_inspector9V8Console8GroupEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console8GroupEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console8GroupEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console8GroupEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8214:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -144(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -112(%rbp)
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -104(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	.LC54(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -100(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-144(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, -160(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	16(%rcx), %eax
	testl	%eax, %eax
	jle	.L4684
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	jmp	.L4696
	.p2align 4,,10
	.p2align 3
.L4721:
	movq	%r10, (%r15)
	movq	-168(%rbp), %rax
	leaq	8(%rax), %r15
	movq	%r15, -168(%rbp)
.L4686:
	movq	-144(%rbp), %rcx
	addq	$1, %r14
	movl	16(%rcx), %eax
	cmpl	%r14d, %eax
	jle	.L4684
	movq	-160(%rbp), %rax
.L4696:
	movq	8(%rcx), %rcx
	movq	%r14, %rdi
	negq	%rdi
	leaq	(%rcx,%rdi,8), %r10
	cmpq	%r15, %rax
	jne	.L4721
	movq	-176(%rbp), %r8
	movabsq	$1152921504606846975, %rsi
	subq	%r8, %rax
	movq	%rax, %rcx
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L4722
	testq	%rax, %rax
	je	.L4702
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4723
.L4688:
	movq	%r13, %rdi
	movq	%rcx, -216(%rbp)
	movq	%r8, -208(%rbp)
	movq	%r10, -200(%rbp)
	call	_Znwm@PLT
	movq	-200(%rbp), %r10
	movq	-216(%rbp), %rcx
	movq	-208(%rbp), %r8
	movq	%rax, %rbx
	leaq	(%rax,%r13), %r9
	leaq	8(%rax), %rax
	movq	%r10, (%rbx,%rcx)
	cmpq	%r15, %r8
	je	.L4705
.L4725:
	movq	%r15, %rax
	leaq	15(%rbx), %rdi
	subq	%r8, %rax
	subq	%r8, %rdi
	leaq	-8(%rax), %rcx
	movq	%rcx, %rax
	shrq	$3, %rax
	cmpq	$30, %rdi
	jbe	.L4706
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rax
	je	.L4706
	addq	$1, %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L4692:
	movdqu	(%r8,%rdx), %xmm2
	movups	%xmm2, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L4692
	movq	%rax, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r8,%rdx), %r10
	addq	%rbx, %rdx
	cmpq	%rdi, %rax
	je	.L4694
	movq	(%r10), %rax
	movq	%rax, (%rdx)
.L4694:
	leaq	16(%rbx,%rcx), %r15
.L4690:
	testq	%r8, %r8
	je	.L4695
	movq	%r8, %rdi
	movq	%r9, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %r9
.L4695:
	movq	%rbx, %xmm0
	movq	%r15, %xmm3
	movq	%r9, -160(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -176(%rbp)
	jmp	.L4686
	.p2align 4,,10
	.p2align 3
.L4723:
	testq	%rdi, %rdi
	jne	.L4724
	xorl	%ebx, %ebx
	movl	$8, %eax
	xorl	%r9d, %r9d
	movq	%r10, (%rbx,%rcx)
	cmpq	%r15, %r8
	jne	.L4725
.L4705:
	movq	%rax, %r15
	jmp	.L4690
	.p2align 4,,10
	.p2align 3
.L4684:
	testl	%eax, %eax
	je	.L4726
.L4697:
	movl	-100(%rbp), %eax
	testl	%eax, %eax
	je	.L4698
	leaq	-176(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	movl	$11, %esi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
.L4698:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4699
	call	_ZdlPv@PLT
.L4699:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4683
	call	_ZdlPv@PLT
.L4683:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4727
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4702:
	.cfi_restore_state
	movl	$8, %r13d
	jmp	.L4688
	.p2align 4,,10
	.p2align 3
.L4726:
	movq	-128(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	leaq	-184(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	movq	%rax, -184(%rbp)
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	jmp	.L4697
	.p2align 4,,10
	.p2align 3
.L4706:
	movq	%rbx, %rdi
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L4691:
	movq	(%rax), %r10
	addq	$8, %rax
	addq	$8, %rdi
	movq	%r10, -8(%rdi)
	cmpq	%rax, %r15
	jne	.L4691
	jmp	.L4694
.L4722:
	leaq	.LC45(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4727:
	call	__stack_chk_fail@PLT
.L4724:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	%rax, %r9
	cmovbe	%rdi, %r9
	leaq	0(,%r9,8), %r13
	jmp	.L4688
	.cfi_endproc
.LFE8214:
	.size	_ZN12v8_inspector9V8Console8GroupEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console8GroupEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.rodata._ZN12v8_inspector9V8Console14GroupCollapsedERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE.str1.1,"aMS",@progbits,1
.LC55:
	.string	"console.groupCollapsed"
	.section	.text._ZN12v8_inspector9V8Console14GroupCollapsedERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console14GroupCollapsedERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console14GroupCollapsedERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console14GroupCollapsedERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -144(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -112(%rbp)
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-112(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -104(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -100(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-144(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, -160(%rbp)
	movaps	%xmm0, -176(%rbp)
	movl	16(%rcx), %eax
	testl	%eax, %eax
	jle	.L4729
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	jmp	.L4741
	.p2align 4,,10
	.p2align 3
.L4766:
	movq	%r10, (%r15)
	movq	-168(%rbp), %rax
	leaq	8(%rax), %r15
	movq	%r15, -168(%rbp)
.L4731:
	movq	-144(%rbp), %rcx
	addq	$1, %r14
	movl	16(%rcx), %eax
	cmpl	%r14d, %eax
	jle	.L4729
	movq	-160(%rbp), %rax
.L4741:
	movq	8(%rcx), %rcx
	movq	%r14, %rdi
	negq	%rdi
	leaq	(%rcx,%rdi,8), %r10
	cmpq	%r15, %rax
	jne	.L4766
	movq	-176(%rbp), %r8
	movabsq	$1152921504606846975, %rsi
	subq	%r8, %rax
	movq	%rax, %rcx
	sarq	$3, %rax
	cmpq	%rsi, %rax
	je	.L4767
	testq	%rax, %rax
	je	.L4747
	movabsq	$9223372036854775800, %r13
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L4768
.L4733:
	movq	%r13, %rdi
	movq	%rcx, -216(%rbp)
	movq	%r8, -208(%rbp)
	movq	%r10, -200(%rbp)
	call	_Znwm@PLT
	movq	-200(%rbp), %r10
	movq	-216(%rbp), %rcx
	movq	-208(%rbp), %r8
	movq	%rax, %rbx
	leaq	(%rax,%r13), %r9
	leaq	8(%rax), %rax
	movq	%r10, (%rbx,%rcx)
	cmpq	%r15, %r8
	je	.L4750
.L4770:
	movq	%r15, %rax
	leaq	15(%rbx), %rdi
	subq	%r8, %rax
	subq	%r8, %rdi
	leaq	-8(%rax), %rcx
	movq	%rcx, %rax
	shrq	$3, %rax
	cmpq	$30, %rdi
	jbe	.L4751
	movabsq	$2305843009213693948, %rsi
	testq	%rsi, %rax
	je	.L4751
	addq	$1, %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	shrq	%rdi
	salq	$4, %rdi
	.p2align 4,,10
	.p2align 3
.L4737:
	movdqu	(%r8,%rdx), %xmm2
	movups	%xmm2, (%rbx,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rdi
	jne	.L4737
	movq	%rax, %rdi
	andq	$-2, %rdi
	leaq	0(,%rdi,8), %rdx
	leaq	(%r8,%rdx), %r10
	addq	%rbx, %rdx
	cmpq	%rdi, %rax
	je	.L4739
	movq	(%r10), %rax
	movq	%rax, (%rdx)
.L4739:
	leaq	16(%rbx,%rcx), %r15
.L4735:
	testq	%r8, %r8
	je	.L4740
	movq	%r8, %rdi
	movq	%r9, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %r9
.L4740:
	movq	%rbx, %xmm0
	movq	%r15, %xmm3
	movq	%r9, -160(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -176(%rbp)
	jmp	.L4731
	.p2align 4,,10
	.p2align 3
.L4768:
	testq	%rdi, %rdi
	jne	.L4769
	xorl	%ebx, %ebx
	movl	$8, %eax
	xorl	%r9d, %r9d
	movq	%r10, (%rbx,%rcx)
	cmpq	%r15, %r8
	jne	.L4770
.L4750:
	movq	%rax, %r15
	jmp	.L4735
	.p2align 4,,10
	.p2align 3
.L4729:
	testl	%eax, %eax
	je	.L4771
.L4742:
	movl	-100(%rbp), %eax
	testl	%eax, %eax
	je	.L4743
	leaq	-176(%rbp), %rdx
	leaq	-144(%rbp), %rdi
	movl	$10, %esi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
.L4743:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4744
	call	_ZdlPv@PLT
.L4744:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4728
	call	_ZdlPv@PLT
.L4728:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4772
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4747:
	.cfi_restore_state
	movl	$8, %r13d
	jmp	.L4733
	.p2align 4,,10
	.p2align 3
.L4771:
	movq	-128(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	leaq	-184(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	movq	%rax, -184(%rbp)
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	jmp	.L4742
	.p2align 4,,10
	.p2align 3
.L4751:
	movq	%rbx, %rdi
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L4736:
	movq	(%rax), %r10
	addq	$8, %rax
	addq	$8, %rdi
	movq	%r10, -8(%rdi)
	cmpq	%rax, %r15
	jne	.L4736
	jmp	.L4739
.L4767:
	leaq	.LC45(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4772:
	call	__stack_chk_fail@PLT
.L4769:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdi
	movq	%rax, %r9
	cmovbe	%rdi, %r9
	leaq	0(,%r9,8), %r13
	jmp	.L4733
	.cfi_endproc
.LFE8213:
	.size	_ZN12v8_inspector9V8Console14GroupCollapsedERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console14GroupCollapsedERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.rodata._ZN12v8_inspectorL31identifierFromTitleOrStackTraceERKNS_8String16ERKNS_12_GLOBAL__N_113ConsoleHelperERKN2v85debug14ConsoleContextEPNS_15V8InspectorImplE.str1.1,"aMS",@progbits,1
.LC56:
	.string	":"
.LC57:
	.string	"@"
	.section	.text._ZN12v8_inspectorL31identifierFromTitleOrStackTraceERKNS_8String16ERKNS_12_GLOBAL__N_113ConsoleHelperERKN2v85debug14ConsoleContextEPNS_15V8InspectorImplE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspectorL31identifierFromTitleOrStackTraceERKNS_8String16ERKNS_12_GLOBAL__N_113ConsoleHelperERKN2v85debug14ConsoleContextEPNS_15V8InspectorImplE, @function
_ZN12v8_inspectorL31identifierFromTitleOrStackTraceERKNS_8String16ERKNS_12_GLOBAL__N_113ConsoleHelperERKN2v85debug14ConsoleContextEPNS_15V8InspectorImplE:
.LFB8216:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r11d, %r11d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -352(%rbp)
	movq	%r8, -360(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movw	%r11w, 16(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 32(%rdi)
	cmpq	$0, 8(%rsi)
	movq	%rax, -344(%rbp)
	jne	.L4774
	movl	44(%rdx), %edx
	movq	24(%r8), %rsi
	movl	$1, %ecx
	leaq	-328(%rbp), %rdi
	call	_ZN12v8_inspector16V8StackTraceImpl7captureEPNS_10V8DebuggerEii@PLT
	movq	-328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4843
	movq	(%rdi), %rax
	call	*8(%rax)
	testb	%al, %al
	jne	.L4844
	movq	-328(%rbp), %rdi
	leaq	-144(%rbp), %r12
	leaq	-240(%rbp), %r13
	leaq	-320(%rbp), %rbx
	leaq	-288(%rbp), %r15
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%r12, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	leaq	.LC56(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-328(%rbp), %rsi
	movq	%rbx, %rdi
	movq	(%rsi), %rax
	call	*16(%rax)
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	-80(%rbp), %rbx
	call	_ZN12v8_inspector10toString16ERKNS_10StringViewE@PLT
	leaq	-192(%rbp), %r8
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r8, %rdi
	movq	%r8, -368(%rbp)
	leaq	-96(%rbp), %r15
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-368(%rbp), %r8
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-96(%rbp), %rdx
	movq	(%r14), %rdi
	movq	-368(%rbp), %r8
	cmpq	%rbx, %rdx
	je	.L4845
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %rax
	cmpq	%rdi, -344(%rbp)
	je	.L4846
	movq	%rax, %xmm0
	movq	%rsi, %xmm5
	movq	16(%r14), %r10
	movq	%rdx, (%r14)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 8(%r14)
	testq	%rdi, %rdi
	je	.L4783
	movq	%rdi, -96(%rbp)
	movq	%r10, -80(%rbp)
.L4781:
	xorl	%r9d, %r9d
	movq	$0, -88(%rbp)
	movw	%r9w, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 32(%r14)
	cmpq	%rbx, %rdi
	je	.L4784
	movq	%r8, -368(%rbp)
	call	_ZdlPv@PLT
	movq	-368(%rbp), %r8
.L4784:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	movq	%rax, -392(%rbp)
	cmpq	%rax, %rdi
	je	.L4785
	movq	%r8, -368(%rbp)
	call	_ZdlPv@PLT
	movq	-368(%rbp), %r8
.L4785:
	movq	-288(%rbp), %rdi
	leaq	-272(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4786
	movq	%r8, -368(%rbp)
	call	_ZdlPv@PLT
	movq	-368(%rbp), %r8
.L4786:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	movq	%rax, -384(%rbp)
	cmpq	%rax, %rdi
	je	.L4787
	movq	%r8, -368(%rbp)
	call	_ZdlPv@PLT
	movq	-368(%rbp), %r8
.L4787:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	movq	%rax, -368(%rbp)
	cmpq	%rax, %rdi
	je	.L4777
	movq	%r8, -376(%rbp)
	call	_ZdlPv@PLT
	movq	-376(%rbp), %r8
	jmp	.L4777
	.p2align 4,,10
	.p2align 3
.L4774:
	leaq	-144(%rbp), %r12
	movq	%rsi, %r13
	leaq	-96(%rbp), %r15
	leaq	.LC57(%rip), %rsi
	movq	%r12, %rdi
	leaq	-80(%rbp), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-96(%rbp), %rdx
	movq	(%r14), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rdx
	je	.L4847
	movq	-80(%rbp), %rsi
	cmpq	%rdi, -344(%rbp)
	je	.L4848
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	16(%r14), %r8
	movq	%rdx, (%r14)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r14)
	testq	%rdi, %rdi
	je	.L4796
	movq	%rdi, -96(%rbp)
	movq	%r8, -80(%rbp)
.L4794:
	xorl	%esi, %esi
	movq	$0, -88(%rbp)
	movw	%si, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 32(%r14)
	cmpq	%rbx, %rdi
	je	.L4797
	call	_ZdlPv@PLT
.L4797:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	movq	%rax, -368(%rbp)
	cmpq	%rax, %rdi
	je	.L4842
	call	_ZdlPv@PLT
.L4842:
	leaq	-176(%rbp), %rax
	leaq	-240(%rbp), %r13
	movq	%rax, -392(%rbp)
	leaq	-224(%rbp), %rax
	leaq	-192(%rbp), %r8
	movq	%rax, -384(%rbp)
.L4790:
	movq	%r8, %rdi
	leaq	.LC57(%rip), %rsi
	movq	%r8, -376(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-360(%rbp), %rax
	movq	-352(%rbp), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_122consoleContextToStringEPN2v87IsolateERKNS1_5debug14ConsoleContextE
	movq	-376(%rbp), %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, %rdx
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-96(%rbp), %rdx
	movq	(%r14), %rdi
	movq	-88(%rbp), %rax
	cmpq	%rbx, %rdx
	je	.L4849
	movq	-80(%rbp), %rsi
	cmpq	%rdi, -344(%rbp)
	je	.L4850
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	16(%r14), %r8
	movq	%rdx, (%r14)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r14)
	testq	%rdi, %rdi
	je	.L4804
	movq	%rdi, -96(%rbp)
	movq	%r8, -80(%rbp)
.L4802:
	xorl	%eax, %eax
	movq	$0, -88(%rbp)
	movw	%ax, (%rdi)
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rdi
	movq	%rax, 32(%r14)
	cmpq	%rbx, %rdi
	je	.L4805
	call	_ZdlPv@PLT
.L4805:
	movq	-144(%rbp), %rdi
	cmpq	-368(%rbp), %rdi
	je	.L4806
	call	_ZdlPv@PLT
.L4806:
	movq	-240(%rbp), %rdi
	cmpq	-384(%rbp), %rdi
	je	.L4807
	call	_ZdlPv@PLT
.L4807:
	movq	-192(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L4773
	call	_ZdlPv@PLT
.L4773:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4851
	addq	$360, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4849:
	.cfi_restore_state
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L4800
	cmpq	$1, %rax
	je	.L4852
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L4800
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L4800:
	xorl	%ecx, %ecx
	movq	%rax, 8(%r14)
	movw	%cx, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L4802
	.p2align 4,,10
	.p2align 3
.L4850:
	movq	%rax, %xmm0
	movq	%rsi, %xmm3
	movq	%rdx, (%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%r14)
.L4804:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L4802
	.p2align 4,,10
	.p2align 3
.L4844:
	leaq	-176(%rbp), %rax
	leaq	-144(%rbp), %r12
	movq	%rax, -392(%rbp)
	leaq	-224(%rbp), %rax
	leaq	-240(%rbp), %r13
	movq	%rax, -384(%rbp)
	leaq	-128(%rbp), %rax
	leaq	-192(%rbp), %r8
	movq	%rax, -368(%rbp)
	leaq	-96(%rbp), %r15
	leaq	-80(%rbp), %rbx
.L4777:
	movq	-328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4790
	movq	(%rdi), %rax
	movq	%r8, -376(%rbp)
	call	*64(%rax)
	movq	-376(%rbp), %r8
	jmp	.L4790
	.p2align 4,,10
	.p2align 3
.L4847:
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L4792
	cmpq	$1, %rax
	je	.L4853
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L4792
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L4792:
	xorl	%r8d, %r8d
	movq	%rax, 8(%r14)
	movw	%r8w, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L4794
	.p2align 4,,10
	.p2align 3
.L4843:
	leaq	-176(%rbp), %rax
	leaq	-144(%rbp), %r12
	movq	%rax, -392(%rbp)
	leaq	-224(%rbp), %rax
	leaq	-240(%rbp), %r13
	movq	%rax, -384(%rbp)
	leaq	-128(%rbp), %rax
	leaq	-192(%rbp), %r8
	movq	%rax, -368(%rbp)
	leaq	-96(%rbp), %r15
	leaq	-80(%rbp), %rbx
	jmp	.L4790
	.p2align 4,,10
	.p2align 3
.L4848:
	movq	%rax, %xmm0
	movq	%rsi, %xmm4
	movq	%rdx, (%r14)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 8(%r14)
.L4796:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L4794
	.p2align 4,,10
	.p2align 3
.L4845:
	movq	-88(%rbp), %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L4779
	cmpq	$1, %rax
	je	.L4854
	movq	%rax, %rdx
	addq	%rdx, %rdx
	je	.L4779
	movq	%rbx, %rsi
	movq	%r8, -368(%rbp)
	call	memmove@PLT
	movq	-88(%rbp), %rax
	movq	(%r14), %rdi
	movq	-368(%rbp), %r8
	leaq	(%rax,%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L4779:
	xorl	%r10d, %r10d
	movq	%rax, 8(%r14)
	movw	%r10w, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L4781
	.p2align 4,,10
	.p2align 3
.L4852:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L4800
	.p2align 4,,10
	.p2align 3
.L4853:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L4792
	.p2align 4,,10
	.p2align 3
.L4846:
	movq	%rax, %xmm0
	movq	%rsi, %xmm6
	movq	%rdx, (%r14)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 8(%r14)
.L4783:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L4781
.L4854:
	movzwl	-80(%rbp), %eax
	movw	%ax, (%rdi)
	movq	-88(%rbp), %rax
	movq	(%r14), %rdi
	leaq	(%rax,%rax), %rdx
	jmp	.L4779
.L4851:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8216:
	.size	_ZN12v8_inspectorL31identifierFromTitleOrStackTraceERKNS_8String16ERKNS_12_GLOBAL__N_113ConsoleHelperERKN2v85debug14ConsoleContextEPNS_15V8InspectorImplE, .-_ZN12v8_inspectorL31identifierFromTitleOrStackTraceERKNS_8String16ERKNS_12_GLOBAL__N_113ConsoleHelperERKN2v85debug14ConsoleContextEPNS_15V8InspectorImplE
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag:
.LFB13542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	testq	%rsi, %rsi
	jne	.L4856
	testq	%rdx, %rdx
	jne	.L4872
.L4856:
	subq	%r13, %rbx
	movq	%rbx, %r14
	sarq	%r14
	cmpq	$15, %rbx
	ja	.L4857
	movq	(%r12), %rdi
.L4858:
	cmpq	$2, %rbx
	je	.L4873
	testq	%rbx, %rbx
	je	.L4861
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	(%r12), %rdi
.L4861:
	xorl	%eax, %eax
	movq	%r14, 8(%r12)
	movw	%ax, (%rdi,%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4873:
	.cfi_restore_state
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	(%r12), %rdi
	jmp	.L4861
	.p2align 4,,10
	.p2align 3
.L4857:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L4874
	leaq	2(%rbx), %rdi
	call	_Znwm@PLT
	movq	%r14, 16(%r12)
	movq	%rax, (%r12)
	movq	%rax, %rdi
	jmp	.L4858
.L4872:
	leaq	.LC47(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L4874:
	leaq	.LC46(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE13542:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	.section	.text._ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper16firstArgToStringERKNS_8String16Eb,"ax",@progbits
	.align 2
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper16firstArgToStringERKNS_8String16Eb, @function
_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper16firstArgToStringERKNS_8String16Eb:
.LFB8186:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movl	16(%rax), %edx
	testl	%edx, %edx
	jle	.L4884
	movq	8(%rax), %rdi
	movq	%rsi, %rbx
	testb	%cl, %cl
	jne	.L4877
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L4877
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L4885
	.p2align 4,,10
	.p2align 3
.L4877:
	movq	24(%rbx), %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L4884
	movq	24(%rbx), %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	jmp	.L4875
	.p2align 4,,10
	.p2align 3
.L4885:
	cmpl	$5, 43(%rax)
	jne	.L4877
	.p2align 4,,10
	.p2align 3
.L4884:
	leaq	16(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	8(%r13), %rax
	movq	0(%r13), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	32(%r13), %rax
	movq	%rax, 32(%r12)
.L4875:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4886
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4886:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8186:
	.size	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper16firstArgToStringERKNS_8String16Eb, .-_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper16firstArgToStringERKNS_8String16Eb
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS6_14ConsoleContextEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS6_14ConsoleContextEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS6_14ConsoleContextEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB11452:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movl	$1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-128(%rbp), %r12
	leaq	-80(%rbp), %rdx
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-64(%rbp), %rbx
	subq	$104, %rsp
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r8), %rax
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	208(%rax), %r13
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	movw	%ax, -64(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper16firstArgToStringERKNS_8String16Eb
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector19V8ProfilerAgentImpl17consoleProfileEndERKNS_8String16E@PLT
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4888
	call	_ZdlPv@PLT
.L4888:
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L4887
	call	_ZdlPv@PLT
.L4887:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4892
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4892:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11452:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS6_14ConsoleContextEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS6_14ConsoleContextEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.rodata._ZN12v8_inspector9V8Console10CountResetERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE.str1.1,"aMS",@progbits,1
.LC58:
	.string	"default"
.LC59:
	.string	"' does not exist"
.LC60:
	.string	"Count for '"
	.section	.text._ZN12v8_inspector9V8Console10CountResetERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console10CountResetERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console10CountResetERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console10CountResetERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-336(%rbp), %r14
	leaq	-80(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-288(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$344, %rsp
	movq	%rdx, -376(%rbp)
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movhps	-376(%rbp), %xmm0
	movaps	%xmm0, -336(%rbp)
	movq	8(%r12), %rdi
	movq	%rdi, -320(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, -304(%rbp)
	leaq	-96(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-304(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -296(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	.LC58(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -292(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper16firstArgToStringERKNS_8String16Eb
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4894
	call	_ZdlPv@PLT
.L4894:
	leaq	-240(%rbp), %r9
	movq	-376(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	8(%rbx), %r8
	movq	%r9, %rdi
	movq	%r9, -376(%rbp)
	call	_ZN12v8_inspectorL31identifierFromTitleOrStackTraceERKNS_8String16ERKNS_12_GLOBAL__N_113ConsoleHelperERKN2v85debug14ConsoleContextEPNS_15V8InspectorImplE
	movl	-292(%rbp), %esi
	movq	-304(%rbp), %rdi
	call	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi@PLT
	movq	-376(%rbp), %r9
	movl	-296(%rbp), %esi
	movq	%rax, %rdi
	movq	%r9, %rdx
	call	_ZN12v8_inspector23V8ConsoleMessageStorage10countResetEiRKNS_8String16E@PLT
	testb	%al, %al
	je	.L4909
.L4895:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4902
	call	_ZdlPv@PLT
.L4902:
	movq	-288(%rbp), %rdi
	leaq	-272(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4893
	call	_ZdlPv@PLT
.L4893:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4910
	addq	$344, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4909:
	.cfi_restore_state
	leaq	-144(%rbp), %r8
	leaq	.LC59(%rip), %rsi
	movq	%r8, %rdi
	movq	%r8, -376(%rbp)
	leaq	-192(%rbp), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	.LC60(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-96(%rbp), %rdi
	movq	-376(%rbp), %r8
	cmpq	%r15, %rdi
	je	.L4896
	call	_ZdlPv@PLT
	movq	-376(%rbp), %r8
.L4896:
	movq	%r8, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-320(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -352(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -368(%rbp)
	leaq	8(%rax), %rax
	movq	%rax, -352(%rbp)
	movq	%rax, -360(%rbp)
	movl	-292(%rbp), %eax
	movq	%rbx, (%rdi)
	testl	%eax, %eax
	jne	.L4911
.L4897:
	call	_ZdlPv@PLT
.L4898:
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L4899
	call	_ZdlPv@PLT
.L4899:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4900
	call	_ZdlPv@PLT
.L4900:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4895
	call	_ZdlPv@PLT
	jmp	.L4895
	.p2align 4,,10
	.p2align 3
.L4911:
	movq	%r14, %rdi
	leaq	-368(%rbp), %rdx
	movl	$4, %esi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4898
	jmp	.L4897
.L4910:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8222:
	.size	_ZN12v8_inspector9V8Console10CountResetERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console10CountResetERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.rodata._ZN12v8_inspectorL15timeEndFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE.str1.1,"aMS",@progbits,1
.LC61:
	.string	"Timer '"
.LC62:
	.string	"ms"
.LC63:
	.string	": "
	.section	.text._ZN12v8_inspectorL15timeEndFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspectorL15timeEndFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE, @function
_ZN12v8_inspectorL15timeEndFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE:
.LFB8232:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-480(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$616, %rsp
	movq	%rsi, -592(%rbp)
	movq	8(%rcx), %rdi
	movl	%edx, -612(%rbp)
	movhps	-592(%rbp), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -528(%rbp)
	movq	%rdi, -512(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -496(%rbp)
	movq	%rax, %rdi
	movq	%rax, -504(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-496(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -488(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	.LC58(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -484(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	leaq	-528(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -632(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper16firstArgToStringERKNS_8String16Eb
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -584(%rbp)
	cmpq	%rax, %rdi
	je	.L4913
	call	_ZdlPv@PLT
.L4913:
	movq	-592(%rbp), %rdx
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	leaq	-192(%rbp), %r14
	leaq	-144(%rbp), %r13
	call	_ZN12v8_inspector12_GLOBAL__N_122consoleContextToStringEPN2v87IsolateERKNS1_5debug14ConsoleContextE
	leaq	.LC57(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8String16plERKS0_
	leaq	-432(%rbp), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, -624(%rbp)
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	movq	%rax, -608(%rbp)
	cmpq	%rax, %rdi
	je	.L4914
	call	_ZdlPv@PLT
.L4914:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	movq	%rax, -600(%rbp)
	cmpq	%rax, %rdi
	je	.L4915
	call	_ZdlPv@PLT
.L4915:
	movq	-96(%rbp), %rdi
	cmpq	-584(%rbp), %rdi
	je	.L4916
	call	_ZdlPv@PLT
.L4916:
	movl	-484(%rbp), %esi
	movq	-496(%rbp), %rdi
	call	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi@PLT
	movq	-624(%rbp), %rdx
	movl	-488(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN12v8_inspector23V8ConsoleMessageStorage8hasTimerEiRKNS_8String16E@PLT
	testb	%al, %al
	je	.L4974
	movq	16(%rbx), %r8
	movq	%r15, %rsi
	movq	(%r8), %rax
	movq	%r8, -648(%rbp)
	movq	152(%rax), %rdx
	leaq	-560(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -624(%rbp)
	movq	%rdx, -640(%rbp)
	call	_ZN12v8_inspector12toStringViewERKNS_8String16E@PLT
	movq	-640(%rbp), %rdx
	leaq	_ZN12v8_inspector17V8InspectorClient14consoleTimeEndERKNS_10StringViewE(%rip), %rax
	movq	-648(%rbp), %r8
	cmpq	%rax, %rdx
	jne	.L4975
.L4927:
	movq	8(%rbx), %rsi
	movq	-592(%rbp), %rdx
	movq	%r12, %rdi
	leaq	-384(%rbp), %rbx
	call	_ZN12v8_inspector12_GLOBAL__N_122consoleContextToStringEPN2v87IsolateERKNS1_5debug14ConsoleContextE
	leaq	.LC57(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-144(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L4928
	call	_ZdlPv@PLT
.L4928:
	movq	-192(%rbp), %rdi
	cmpq	-600(%rbp), %rdi
	je	.L4929
	call	_ZdlPv@PLT
.L4929:
	movq	-96(%rbp), %rdi
	cmpq	-584(%rbp), %rdi
	je	.L4930
	call	_ZdlPv@PLT
.L4930:
	cmpb	$0, -612(%rbp)
	movq	-496(%rbp), %rdi
	movl	-484(%rbp), %esi
	jne	.L4976
	call	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi@PLT
	movl	-488(%rbp), %esi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	_ZN12v8_inspector23V8ConsoleMessageStorage7timeEndEiRKNS_8String16E@PLT
.L4932:
	leaq	.LC62(%rip), %rsi
	movq	%r12, %rdi
	movsd	%xmm0, -592(%rbp)
	leaq	-240(%rbp), %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movsd	-592(%rbp), %xmm0
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String1610fromDoubleEd@PLT
	leaq	-288(%rbp), %rdx
	leaq	.LC63(%rip), %rsi
	movq	%rdx, %rdi
	movq	%rdx, -592(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-592(%rbp), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK12v8_inspector8String16plERKS0_
	leaq	-336(%rbp), %r14
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-144(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	je	.L4933
	call	_ZdlPv@PLT
.L4933:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4934
	call	_ZdlPv@PLT
.L4934:
	movq	-288(%rbp), %rdi
	leaq	-272(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4935
	call	_ZdlPv@PLT
.L4935:
	movq	-192(%rbp), %rdi
	cmpq	-600(%rbp), %rdi
	je	.L4936
	call	_ZdlPv@PLT
.L4936:
	movq	-96(%rbp), %rdi
	cmpq	-584(%rbp), %rdi
	je	.L4937
	call	_ZdlPv@PLT
.L4937:
	cmpb	$0, -612(%rbp)
	movq	-512(%rbp), %rdi
	je	.L4938
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	$0, -544(%rbp)
	leaq	-568(%rbp), %r12
	movaps	%xmm0, -560(%rbp)
	movq	$-8, %r13
	movl	$1, %ebx
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-624(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, -568(%rbp)
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	movq	-528(%rbp), %rax
	movq	-624(%rbp), %r14
	cmpl	$1, 16(%rax)
	jle	.L4942
	.p2align 4,,10
	.p2align 3
.L4939:
	movq	8(%rax), %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	addl	$1, %ebx
	addq	%r13, %rcx
	subq	$8, %r13
	movq	%rcx, -568(%rbp)
	call	_ZNSt6vectorIN2v85LocalINS0_5ValueEEESaIS3_EE12emplace_backIJS3_EEEvDpOT_
	movq	-528(%rbp), %rax
	cmpl	%ebx, 16(%rax)
	jg	.L4939
.L4942:
	movl	-484(%rbp), %edx
	testl	%edx, %edx
	jne	.L4977
.L4969:
	movq	-560(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L4945
.L4944:
	movq	-336(%rbp), %rdi
	leaq	-320(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4947
	call	_ZdlPv@PLT
.L4947:
	movq	-384(%rbp), %rdi
	leaq	-368(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4948
.L4973:
	call	_ZdlPv@PLT
.L4948:
	movq	-432(%rbp), %rdi
	leaq	-416(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4949
	call	_ZdlPv@PLT
.L4949:
	movq	-480(%rbp), %rdi
	leaq	-464(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4912
	call	_ZdlPv@PLT
.L4912:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4978
	addq	$616, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4976:
	.cfi_restore_state
	call	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi@PLT
	movl	-488(%rbp), %esi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	_ZN12v8_inspector23V8ConsoleMessageStorage7timeLogEiRKNS_8String16E@PLT
	jmp	.L4932
	.p2align 4,,10
	.p2align 3
.L4938:
	movq	%r14, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -544(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -560(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -560(%rbp)
	leaq	8(%rax), %rax
	movq	%rax, -544(%rbp)
	movq	%rax, -552(%rbp)
	movl	-484(%rbp), %eax
	movq	%rbx, (%rdi)
	testl	%eax, %eax
	jne	.L4979
.L4945:
	call	_ZdlPv@PLT
	jmp	.L4944
	.p2align 4,,10
	.p2align 3
.L4974:
	leaq	.LC59(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	.LC61(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-96(%rbp), %rdi
	cmpq	-584(%rbp), %rdi
	je	.L4918
	call	_ZdlPv@PLT
.L4918:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-512(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -544(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -560(%rbp)
	call	_Znwm@PLT
	movl	-484(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rax, -560(%rbp)
	leaq	8(%rax), %rax
	movq	%rax, -544(%rbp)
	movq	%rbx, (%rdi)
	movq	%rax, -552(%rbp)
	testl	%ecx, %ecx
	jne	.L4980
.L4919:
	call	_ZdlPv@PLT
.L4920:
	movq	-96(%rbp), %rdi
	cmpq	-584(%rbp), %rdi
	je	.L4921
	call	_ZdlPv@PLT
.L4921:
	movq	-192(%rbp), %rdi
	cmpq	-600(%rbp), %rdi
	je	.L4922
	call	_ZdlPv@PLT
.L4922:
	movq	-144(%rbp), %rdi
	cmpq	-608(%rbp), %rdi
	jne	.L4973
	jmp	.L4948
	.p2align 4,,10
	.p2align 3
.L4980:
	movq	-632(%rbp), %rdi
	leaq	-560(%rbp), %rdx
	movl	$4, %esi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
	movq	-560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4920
	jmp	.L4919
	.p2align 4,,10
	.p2align 3
.L4977:
	movq	-624(%rbp), %rdx
	movq	-632(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
	jmp	.L4969
	.p2align 4,,10
	.p2align 3
.L4979:
	movq	-624(%rbp), %rdx
	movq	-632(%rbp), %rdi
	movl	$14, %esi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
	jmp	.L4969
	.p2align 4,,10
	.p2align 3
.L4975:
	movq	-624(%rbp), %rsi
	movq	%r8, %rdi
	call	*%rdx
	jmp	.L4927
.L4978:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8232:
	.size	_ZN12v8_inspectorL15timeEndFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE, .-_ZN12v8_inspectorL15timeEndFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE
	.section	.text._ZN12v8_inspector9V8Console7TimeLogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console7TimeLogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console7TimeLogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console7TimeLogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8234:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movl	$1, %edx
	movq	8(%r8), %rcx
	jmp	_ZN12v8_inspectorL15timeEndFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE
	.cfi_endproc
.LFE8234:
	.size	_ZN12v8_inspector9V8Console7TimeLogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console7TimeLogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.text._ZN12v8_inspector9V8Console7TimeEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console7TimeEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console7TimeEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console7TimeEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8235:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	xorl	%edx, %edx
	movq	8(%r8), %rcx
	jmp	_ZN12v8_inspectorL15timeEndFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE
	.cfi_endproc
.LFE8235:
	.size	_ZN12v8_inspector9V8Console7TimeEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console7TimeEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.rodata._ZN12v8_inspectorL12timeFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE.constprop.0.str1.1,"aMS",@progbits,1
.LC64:
	.string	"' already exists"
	.section	.text._ZN12v8_inspectorL12timeFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE.constprop.0,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspectorL12timeFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE.constprop.0, @function
_ZN12v8_inspectorL12timeFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE.constprop.0:
.LFB15210:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm1
	movq	%rdi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-288(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$376, %rsp
	movq	8(%rdx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -336(%rbp)
	movq	%rdi, -320(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, -304(%rbp)
	movq	%rax, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-304(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -296(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	.LC58(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -292(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	leaq	-336(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -408(%rbp)
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper16firstArgToStringERKNS_8String16Eb
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -376(%rbp)
	cmpq	%rax, %rdi
	je	.L4984
	call	_ZdlPv@PLT
.L4984:
	movq	8(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	-192(%rbp), %r14
	leaq	-144(%rbp), %r15
	call	_ZN12v8_inspector12_GLOBAL__N_122consoleContextToStringEPN2v87IsolateERKNS1_5debug14ConsoleContextE
	leaq	.LC57(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNK12v8_inspector8String16plERKS0_
	leaq	-240(%rbp), %rax
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -384(%rbp)
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	movq	%rax, -392(%rbp)
	cmpq	%rax, %rdi
	je	.L4985
	call	_ZdlPv@PLT
.L4985:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	movq	%rax, -400(%rbp)
	cmpq	%rax, %rdi
	je	.L4986
	call	_ZdlPv@PLT
.L4986:
	movq	-96(%rbp), %rdi
	cmpq	-376(%rbp), %rdi
	je	.L4987
	call	_ZdlPv@PLT
.L4987:
	movl	-292(%rbp), %esi
	movq	-304(%rbp), %rdi
	call	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi@PLT
	movq	-384(%rbp), %rdx
	movl	-296(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN12v8_inspector23V8ConsoleMessageStorage8hasTimerEiRKNS_8String16E@PLT
	testb	%al, %al
	jne	.L5011
	movq	16(%rbx), %r12
	leaq	-368(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	(%r12), %rax
	movq	144(%rax), %rbx
	call	_ZN12v8_inspector12toStringViewERKNS_8String16E@PLT
	leaq	_ZN12v8_inspector17V8InspectorClient11consoleTimeERKNS_10StringViewE(%rip), %rax
	cmpq	%rax, %rbx
	jne	.L5012
.L4998:
	movl	-292(%rbp), %esi
	movq	-304(%rbp), %rdi
	call	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi@PLT
	movq	-384(%rbp), %rdx
	movl	-296(%rbp), %esi
	movq	%rax, %rdi
	call	_ZN12v8_inspector23V8ConsoleMessageStorage4timeEiRKNS_8String16E@PLT
.L5010:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4999
	call	_ZdlPv@PLT
.L4999:
	movq	-288(%rbp), %rdi
	leaq	-272(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4983
	call	_ZdlPv@PLT
.L4983:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5013
	addq	$376, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5011:
	.cfi_restore_state
	leaq	.LC64(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	.LC61(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-96(%rbp), %rdi
	cmpq	-376(%rbp), %rdi
	je	.L4989
	call	_ZdlPv@PLT
.L4989:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-320(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -352(%rbp)
	movq	%rax, %rbx
	movaps	%xmm0, -368(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -368(%rbp)
	leaq	8(%rax), %rax
	movq	%rax, -352(%rbp)
	movq	%rax, -360(%rbp)
	movl	-292(%rbp), %eax
	movq	%rbx, (%rdi)
	testl	%eax, %eax
	jne	.L5014
.L4990:
	call	_ZdlPv@PLT
.L4991:
	movq	-96(%rbp), %rdi
	cmpq	-376(%rbp), %rdi
	je	.L4992
	call	_ZdlPv@PLT
.L4992:
	movq	-192(%rbp), %rdi
	cmpq	-400(%rbp), %rdi
	je	.L4993
	call	_ZdlPv@PLT
.L4993:
	movq	-144(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L5010
	call	_ZdlPv@PLT
	jmp	.L5010
	.p2align 4,,10
	.p2align 3
.L5014:
	movq	-408(%rbp), %rdi
	leaq	-368(%rbp), %rdx
	movl	$4, %esi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
	movq	-368(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L4991
	jmp	.L4990
	.p2align 4,,10
	.p2align 3
.L5012:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rbx
	jmp	.L4998
.L5013:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE15210:
	.size	_ZN12v8_inspectorL12timeFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE.constprop.0, .-_ZN12v8_inspectorL12timeFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE.constprop.0
	.section	.text._ZN12v8_inspector9V8Console4TimeERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console4TimeERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console4TimeERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console4TimeERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8233:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	8(%r8), %rdx
	jmp	_ZN12v8_inspectorL12timeFunctionERKN2v85debug20ConsoleCallArgumentsERKNS1_14ConsoleContextEbPNS_15V8InspectorImplE.constprop.0
	.cfi_endproc
.LFE8233:
	.size	_ZN12v8_inspector9V8Console4TimeERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console4TimeERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.text._ZN12v8_inspector9V8Console5CountERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console5CountERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console5CountERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console5CountERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8221:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-384(%rbp), %r15
	leaq	-336(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$392, %rsp
	movq	%rdx, -424(%rbp)
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movhps	-424(%rbp), %xmm0
	movaps	%xmm0, -384(%rbp)
	movq	8(%r12), %rdi
	movq	%rdi, -368(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, -352(%rbp)
	leaq	-96(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-352(%rbp), %rdi
	movl	%eax, %esi
	movl	%eax, -344(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	leaq	.LC58(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, -340(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r14, %rdi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper16firstArgToStringERKNS_8String16Eb
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5017
	call	_ZdlPv@PLT
.L5017:
	movq	8(%rbx), %r8
	movq	-424(%rbp), %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	leaq	-288(%rbp), %r9
	leaq	-240(%rbp), %rbx
	movq	%r9, %rdi
	movq	%r9, -424(%rbp)
	call	_ZN12v8_inspectorL31identifierFromTitleOrStackTraceERKNS_8String16ERKNS_12_GLOBAL__N_113ConsoleHelperERKN2v85debug14ConsoleContextEPNS_15V8InspectorImplE
	movl	-340(%rbp), %esi
	movq	-352(%rbp), %rdi
	call	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi@PLT
	movq	-424(%rbp), %r9
	movl	-344(%rbp), %esi
	movq	%rax, %rdi
	movq	%r9, %rdx
	call	_ZN12v8_inspector23V8ConsoleMessageStorage5countEiRKNS_8String16E@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	call	_ZN12v8_inspector8String1611fromIntegerEi@PLT
	cmpq	$0, -328(%rbp)
	jne	.L5018
	movq	-232(%rbp), %rax
	movq	%r12, %rdi
	movq	%r13, -96(%rbp)
	xorl	%ebx, %ebx
	movq	-240(%rbp), %rsi
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-208(%rbp), %rax
	movq	%rax, -64(%rbp)
.L5019:
	movq	-368(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -400(%rbp)
	movq	%rax, %r12
	movaps	%xmm0, -416(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)
	leaq	8(%rax), %rax
	movq	%rax, -400(%rbp)
	movq	%rax, -408(%rbp)
	movl	-340(%rbp), %eax
	movq	%r12, (%rdi)
	testl	%eax, %eax
	jne	.L5038
.L5020:
	call	_ZdlPv@PLT
.L5021:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5022
	call	_ZdlPv@PLT
.L5022:
	testb	%bl, %bl
	je	.L5028
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5024
	call	_ZdlPv@PLT
.L5024:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5028
	call	_ZdlPv@PLT
.L5028:
	movq	-240(%rbp), %rdi
	leaq	-224(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5025
	call	_ZdlPv@PLT
.L5025:
	movq	-288(%rbp), %rdi
	leaq	-272(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5026
	call	_ZdlPv@PLT
.L5026:
	movq	-336(%rbp), %rdi
	leaq	-320(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L5016
	call	_ZdlPv@PLT
.L5016:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5039
	addq	$392, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5018:
	.cfi_restore_state
	leaq	-192(%rbp), %rdx
	leaq	.LC63(%rip), %rsi
	movq	%rdx, %rdi
	movq	%rdx, -424(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	leaq	-144(%rbp), %r8
	movq	-424(%rbp), %rdx
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r8, -424(%rbp)
	call	_ZNK12v8_inspector8String16plERKS0_
	movq	-424(%rbp), %r8
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$1, %ebx
	movq	%r8, %rsi
	call	_ZNK12v8_inspector8String16plERKS0_
	jmp	.L5019
	.p2align 4,,10
	.p2align 3
.L5038:
	movq	%r15, %rdi
	leaq	-416(%rbp), %rdx
	movl	$15, %esi
	call	_ZN12v8_inspector12_GLOBAL__N_113ConsoleHelper10reportCallENS_14ConsoleAPITypeERKSt6vectorIN2v85LocalINS4_5ValueEEESaIS7_EE.part.0
	movq	-416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5021
	jmp	.L5020
.L5039:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8221:
	.size	_ZN12v8_inspector9V8Console5CountERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console5CountERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.section	.text._ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS6_14ConsoleContextEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_,"ax",@progbits
	.p2align 4
	.type	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS6_14ConsoleContextEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, @function
_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS6_14ConsoleContextEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_:
.LFB11446:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	leaq	-64(%rbp), %rbx
	subq	$96, %rsp
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rbx, -80(%rbp)
	movq	$0, -48(%rbp)
	movq	208(%rax), %r14
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movw	%ax, -64(%rbp)
	movq	0(%r13), %rax
	movl	16(%rax), %edx
	testl	%edx, %edx
	jle	.L5050
	movq	24(%r13), %rsi
	movq	8(%rax), %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L5051
	movq	24(%r13), %rdi
	leaq	-128(%rbp), %r13
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r12, %rdx
	movq	%r13, %rdi
	leaq	-112(%rbp), %r12
	movq	%rax, %rsi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
.L5042:
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN12v8_inspector19V8ProfilerAgentImpl14consoleProfileERKNS_8String16E@PLT
	movq	-128(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L5045
	call	_ZdlPv@PLT
.L5045:
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L5040
	call	_ZdlPv@PLT
.L5040:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5052
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5050:
	.cfi_restore_state
	leaq	-128(%rbp), %r13
	leaq	-112(%rbp), %r12
	movq	%rbx, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r12, -128(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-48(%rbp), %rax
	movq	%rax, -96(%rbp)
	jmp	.L5042
	.p2align 4,,10
	.p2align 3
.L5051:
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rsi
	leaq	-128(%rbp), %r13
	leaq	-112(%rbp), %r12
	movq	%r13, %rdi
	movq	%r12, -128(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-48(%rbp), %rax
	movq	%rax, -96(%rbp)
	jmp	.L5042
.L5052:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11446:
	.size	_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS6_14ConsoleContextEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_, .-_ZNSt17_Function_handlerIFvPN12v8_inspector22V8InspectorSessionImplEEZNS0_9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS6_14ConsoleContextEEUlS2_E_E9_M_invokeERKSt9_Any_dataOS2_
	.section	.text._ZN12v8_inspector9V8Console9TimeStampERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN12v8_inspector9V8Console9TimeStampERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.type	_ZN12v8_inspector9V8Console9TimeStampERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, @function
_ZN12v8_inspector9V8Console9TimeStampERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE:
.LFB8236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%r13), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r13, %rdi
	leaq	-80(%rbp), %r13
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movl	16(%r12), %edx
	xorl	%eax, %eax
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movw	%ax, -80(%rbp)
	movq	$0, -64(%rbp)
	testl	%edx, %edx
	jle	.L5064
	movq	8(%r12), %rdi
	movq	-168(%rbp), %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L5065
	movq	-168(%rbp), %rdi
	leaq	-144(%rbp), %r14
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	leaq	-128(%rbp), %r12
	movq	%rax, %rsi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
.L5055:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L5058
	call	_ZdlPv@PLT
.L5058:
	movq	8(%rbx), %rax
	leaq	-224(%rbp), %r15
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	16(%rax), %r13
	movq	0(%r13), %rax
	movq	160(%rax), %rbx
	call	_ZN12v8_inspector12toStringViewERKNS_8String16E@PLT
	leaq	_ZN12v8_inspector17V8InspectorClient16consoleTimeStampERKNS_10StringViewE(%rip), %rax
	cmpq	%rax, %rbx
	jne	.L5066
.L5059:
	movq	-144(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L5053
	call	_ZdlPv@PLT
.L5053:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5067
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5064:
	.cfi_restore_state
	leaq	-144(%rbp), %r14
	leaq	-128(%rbp), %r12
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r12, -144(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-64(%rbp), %rax
	movq	%rax, -112(%rbp)
	jmp	.L5055
	.p2align 4,,10
	.p2align 3
.L5065:
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rsi
	leaq	-144(%rbp), %r14
	leaq	-128(%rbp), %r12
	movq	%r14, %rdi
	movq	%r12, -144(%rbp)
	leaq	(%rsi,%rax,2), %rdx
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE12_M_constructIPtEEvT_S7_St20forward_iterator_tag
	movq	-64(%rbp), %rax
	movq	%rax, -112(%rbp)
	jmp	.L5055
	.p2align 4,,10
	.p2align 3
.L5066:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*%rbx
	jmp	.L5059
.L5067:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8236:
	.size	_ZN12v8_inspector9V8Console9TimeStampERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE, .-_ZN12v8_inspector9V8Console9TimeStampERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.weak	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13ObjectPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13ObjectPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime15PropertyPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime15PropertyPreviewD0Ev
	.weak	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime12EntryPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime12EntryPreviewD0Ev
	.weak	_ZTVN12v8_inspector9V8ConsoleE
	.section	.data.rel.ro.local._ZTVN12v8_inspector9V8ConsoleE,"awG",@progbits,_ZTVN12v8_inspector9V8ConsoleE,comdat
	.align 8
	.type	_ZTVN12v8_inspector9V8ConsoleE, @object
	.size	_ZTVN12v8_inspector9V8ConsoleE, 208
_ZTVN12v8_inspector9V8ConsoleE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector9V8Console5DebugERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console5ErrorERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console4InfoERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console3LogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console4WarnERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console3DirERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console6DirXmlERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console5TableERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console5TraceERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console5GroupERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console14GroupCollapsedERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console8GroupEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console5ClearERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console5CountERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console10CountResetERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console6AssertERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console7ProfileERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console10ProfileEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console4TimeERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console7TimeLogERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console7TimeEndERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8Console9TimeStampERKN2v85debug20ConsoleCallArgumentsERKNS2_14ConsoleContextE
	.quad	_ZN12v8_inspector9V8ConsoleD1Ev
	.quad	_ZN12v8_inspector9V8ConsoleD0Ev
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
