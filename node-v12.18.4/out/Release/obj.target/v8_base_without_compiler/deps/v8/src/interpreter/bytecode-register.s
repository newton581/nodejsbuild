	.file	"bytecode-register.cc"
	.text
	.section	.text._ZN2v88internal11interpreter8Register18FromParameterIndexEii,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter8Register18FromParameterIndexEii
	.type	_ZN2v88internal11interpreter8Register18FromParameterIndexEii, @function
_ZN2v88internal11interpreter8Register18FromParameterIndexEii:
.LFB5088:
	.cfi_startproc
	endbr64
	subl	%esi, %edi
	leal	-6(%rdi), %eax
	ret
	.cfi_endproc
.LFE5088:
	.size	_ZN2v88internal11interpreter8Register18FromParameterIndexEii, .-_ZN2v88internal11interpreter8Register18FromParameterIndexEii
	.section	.text._ZNK2v88internal11interpreter8Register16ToParameterIndexEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi
	.type	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi, @function
_ZNK2v88internal11interpreter8Register16ToParameterIndexEi:
.LFB5089:
	.cfi_startproc
	endbr64
	movl	(%rdi), %eax
	leal	6(%rax,%rsi), %eax
	ret
	.cfi_endproc
.LFE5089:
	.size	_ZNK2v88internal11interpreter8Register16ToParameterIndexEi, .-_ZNK2v88internal11interpreter8Register16ToParameterIndexEi
	.section	.text._ZN2v88internal11interpreter8Register16function_closureEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter8Register16function_closureEv
	.type	_ZN2v88internal11interpreter8Register16function_closureEv, @function
_ZN2v88internal11interpreter8Register16function_closureEv:
.LFB5090:
	.cfi_startproc
	endbr64
	movl	$-3, %eax
	ret
	.cfi_endproc
.LFE5090:
	.size	_ZN2v88internal11interpreter8Register16function_closureEv, .-_ZN2v88internal11interpreter8Register16function_closureEv
	.section	.text._ZNK2v88internal11interpreter8Register19is_function_closureEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter8Register19is_function_closureEv
	.type	_ZNK2v88internal11interpreter8Register19is_function_closureEv, @function
_ZNK2v88internal11interpreter8Register19is_function_closureEv:
.LFB5091:
	.cfi_startproc
	endbr64
	cmpl	$-3, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE5091:
	.size	_ZNK2v88internal11interpreter8Register19is_function_closureEv, .-_ZNK2v88internal11interpreter8Register19is_function_closureEv
	.section	.text._ZN2v88internal11interpreter8Register15current_contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter8Register15current_contextEv
	.type	_ZN2v88internal11interpreter8Register15current_contextEv, @function
_ZN2v88internal11interpreter8Register15current_contextEv:
.LFB5092:
	.cfi_startproc
	endbr64
	movl	$-4, %eax
	ret
	.cfi_endproc
.LFE5092:
	.size	_ZN2v88internal11interpreter8Register15current_contextEv, .-_ZN2v88internal11interpreter8Register15current_contextEv
	.section	.text._ZNK2v88internal11interpreter8Register18is_current_contextEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter8Register18is_current_contextEv
	.type	_ZNK2v88internal11interpreter8Register18is_current_contextEv, @function
_ZNK2v88internal11interpreter8Register18is_current_contextEv:
.LFB5093:
	.cfi_startproc
	endbr64
	cmpl	$-4, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE5093:
	.size	_ZNK2v88internal11interpreter8Register18is_current_contextEv, .-_ZNK2v88internal11interpreter8Register18is_current_contextEv
	.section	.text._ZN2v88internal11interpreter8Register14bytecode_arrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter8Register14bytecode_arrayEv
	.type	_ZN2v88internal11interpreter8Register14bytecode_arrayEv, @function
_ZN2v88internal11interpreter8Register14bytecode_arrayEv:
.LFB5094:
	.cfi_startproc
	endbr64
	movl	$-2, %eax
	ret
	.cfi_endproc
.LFE5094:
	.size	_ZN2v88internal11interpreter8Register14bytecode_arrayEv, .-_ZN2v88internal11interpreter8Register14bytecode_arrayEv
	.section	.text._ZNK2v88internal11interpreter8Register17is_bytecode_arrayEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter8Register17is_bytecode_arrayEv
	.type	_ZNK2v88internal11interpreter8Register17is_bytecode_arrayEv, @function
_ZNK2v88internal11interpreter8Register17is_bytecode_arrayEv:
.LFB5095:
	.cfi_startproc
	endbr64
	cmpl	$-2, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE5095:
	.size	_ZNK2v88internal11interpreter8Register17is_bytecode_arrayEv, .-_ZNK2v88internal11interpreter8Register17is_bytecode_arrayEv
	.section	.text._ZN2v88internal11interpreter8Register15bytecode_offsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter8Register15bytecode_offsetEv
	.type	_ZN2v88internal11interpreter8Register15bytecode_offsetEv, @function
_ZN2v88internal11interpreter8Register15bytecode_offsetEv:
.LFB5096:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE5096:
	.size	_ZN2v88internal11interpreter8Register15bytecode_offsetEv, .-_ZN2v88internal11interpreter8Register15bytecode_offsetEv
	.section	.text._ZNK2v88internal11interpreter8Register18is_bytecode_offsetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter8Register18is_bytecode_offsetEv
	.type	_ZNK2v88internal11interpreter8Register18is_bytecode_offsetEv, @function
_ZNK2v88internal11interpreter8Register18is_bytecode_offsetEv:
.LFB5097:
	.cfi_startproc
	endbr64
	cmpl	$-1, (%rdi)
	sete	%al
	ret
	.cfi_endproc
.LFE5097:
	.size	_ZNK2v88internal11interpreter8Register18is_bytecode_offsetEv, .-_ZNK2v88internal11interpreter8Register18is_bytecode_offsetEv
	.section	.text._ZN2v88internal11interpreter8Register19virtual_accumulatorEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter8Register19virtual_accumulatorEv
	.type	_ZN2v88internal11interpreter8Register19virtual_accumulatorEv, @function
_ZN2v88internal11interpreter8Register19virtual_accumulatorEv:
.LFB5098:
	.cfi_startproc
	endbr64
	movl	$-6, %eax
	ret
	.cfi_endproc
.LFE5098:
	.size	_ZN2v88internal11interpreter8Register19virtual_accumulatorEv, .-_ZN2v88internal11interpreter8Register19virtual_accumulatorEv
	.section	.text._ZNK2v88internal11interpreter8Register13SizeOfOperandEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter8Register13SizeOfOperandEv
	.type	_ZNK2v88internal11interpreter8Register13SizeOfOperandEv, @function
_ZNK2v88internal11interpreter8Register13SizeOfOperandEv:
.LFB5099:
	.cfi_startproc
	endbr64
	movl	$-5, %eax
	subl	(%rdi), %eax
	movl	$1, %r8d
	leal	128(%rax), %edx
	cmpl	$255, %edx
	jbe	.L13
	addl	$32768, %eax
	cmpl	$65536, %eax
	sbbl	%r8d, %r8d
	andl	$-2, %r8d
	addl	$4, %r8d
.L13:
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE5099:
	.size	_ZNK2v88internal11interpreter8Register13SizeOfOperandEv, .-_ZNK2v88internal11interpreter8Register13SizeOfOperandEv
	.section	.text._ZN2v88internal11interpreter8Register13AreContiguousES2_S2_S2_S2_S2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal11interpreter8Register13AreContiguousES2_S2_S2_S2_S2_
	.type	_ZN2v88internal11interpreter8Register13AreContiguousES2_S2_S2_S2_S2_, @function
_ZN2v88internal11interpreter8Register13AreContiguousES2_S2_S2_S2_S2_:
.LFB5100:
	.cfi_startproc
	endbr64
	leal	1(%rdi), %r9d
	xorl	%eax, %eax
	cmpl	%esi, %r9d
	jne	.L17
	cmpl	$2147483647, %edx
	je	.L19
	addl	$2, %edi
	cmpl	%edx, %edi
	jne	.L17
.L19:
	cmpl	$2147483647, %ecx
	je	.L20
	addl	$1, %edx
	xorl	%eax, %eax
	cmpl	%ecx, %edx
	je	.L20
.L17:
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$1, %eax
	cmpl	$2147483647, %r8d
	je	.L17
	addl	$1, %ecx
	cmpl	%r8d, %ecx
	sete	%al
	ret
	.cfi_endproc
.LFE5100:
	.size	_ZN2v88internal11interpreter8Register13AreContiguousES2_S2_S2_S2_S2_, .-_ZN2v88internal11interpreter8Register13AreContiguousES2_S2_S2_S2_S2_
	.section	.rodata._ZNK2v88internal11interpreter8Register8ToStringB5cxx11Ei.str1.1,"aMS",@progbits,1
.LC0:
	.string	"a"
.LC1:
	.string	"r"
	.section	.text._ZNK2v88internal11interpreter8Register8ToStringB5cxx11Ei,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal11interpreter8Register8ToStringB5cxx11Ei
	.type	_ZNK2v88internal11interpreter8Register8ToStringB5cxx11Ei, @function
_ZNK2v88internal11interpreter8Register8ToStringB5cxx11Ei:
.LFB5101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$456, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	(%rsi), %eax
	cmpl	$-4, %eax
	je	.L55
	cmpl	$-3, %eax
	je	.L56
	testl	%eax, %eax
	jns	.L36
	leal	7(%rax,%rdx), %ecx
	cmpl	$1, %ecx
	je	.L57
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-320(%rbp), %r12
	movq	.LC2(%rip), %xmm1
	movq	%r8, -488(%rbp)
	movq	%r12, %rdi
	movq	%rax, -448(%rbp)
	leaq	-432(%rbp), %r15
	leaq	-368(%rbp), %r13
	movl	%ecx, -480(%rbp)
	movhps	-448(%rbp), %xmm1
	movaps	%xmm1, -448(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r14, -432(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -320(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -464(%rbp)
	movq	-24(%r14), %rax
	movq	%rcx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-448(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -472(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -448(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -456(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-480(%rbp), %ecx
	movq	%r15, %rdi
	leal	-2(%rcx), %esi
	call	_ZNSolsEi@PLT
	movq	-488(%rbp), %r8
.L54:
	movq	-384(%rbp), %rax
	movq	%r8, (%rbx)
	movq	$0, 8(%rbx)
	movb	$0, 16(%rbx)
	testq	%rax, %rax
	je	.L42
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L43
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L44:
	movq	.LC2(%rip), %xmm0
	movq	-472(%rbp), %rax
	movq	-352(%rbp), %rdi
	movhps	-448(%rbp), %xmm0
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-456(%rbp), %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, -432(%rbp)
	movq	-24(%r14), %rax
	movq	%r12, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%rdx, -432(%rbp,%rax)
	movq	-464(%rbp), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L32:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$456, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-320(%rbp), %r12
	movq	.LC2(%rip), %xmm1
	movq	%r8, -480(%rbp)
	movq	%rax, -448(%rbp)
	movq	%r12, %rdi
	leaq	-432(%rbp), %r15
	leaq	-368(%rbp), %r13
	movq	%rsi, -488(%rbp)
	movhps	-448(%rbp), %xmm1
	movaps	%xmm1, -448(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %r14
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movups	%xmm0, -88(%rbp)
	movq	%rax, -320(%rbp)
	movq	%rax, -464(%rbp)
	xorl	%eax, %eax
	movq	%r14, -432(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	$0, -104(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%r14), %rax
	movq	%rdx, -432(%rbp,%rax)
	movq	-432(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-448(%rbp), %xmm1
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -472(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -448(%rbp)
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -456(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-488(%rbp), %rcx
	movq	%r15, %rdi
	movl	(%rcx), %esi
	call	_ZNSolsEi@PLT
	movq	-480(%rbp), %r8
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L43:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L55:
	movabsq	$8392569456348324668, %rax
	movq	%r8, (%rdi)
	movq	%rax, 16(%rdi)
	movb	$62, 8(%r8)
	movq	$9, 8(%rdi)
	movb	$0, 25(%rdi)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L56:
	movabsq	$7310034283826799420, %rax
	movq	%r8, (%rdi)
	movq	%rax, 16(%rdi)
	movb	$62, 8(%r8)
	movq	$9, 8(%rdi)
	movb	$0, 25(%rdi)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$15987, %ecx
	movq	%r8, (%rdi)
	movl	$1768453180, 16(%rdi)
	movw	%cx, 4(%r8)
	movq	$6, 8(%rdi)
	movb	$0, 22(%rdi)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	-352(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L44
.L58:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5101:
	.size	_ZNK2v88internal11interpreter8Register8ToStringB5cxx11Ei, .-_ZNK2v88internal11interpreter8Register8ToStringB5cxx11Ei
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal11interpreter8Register18FromParameterIndexEii,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal11interpreter8Register18FromParameterIndexEii, @function
_GLOBAL__sub_I__ZN2v88internal11interpreter8Register18FromParameterIndexEii:
.LFB5941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5941:
	.size	_GLOBAL__sub_I__ZN2v88internal11interpreter8Register18FromParameterIndexEii, .-_GLOBAL__sub_I__ZN2v88internal11interpreter8Register18FromParameterIndexEii
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal11interpreter8Register18FromParameterIndexEii
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC2:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
