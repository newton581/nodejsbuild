	.file	"macro-assembler-x64.cc"
	.text
	.section	.text._ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,"axG",@progbits,_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.type	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, @function
_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv:
.LFB11383:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11383:
	.size	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv, .-_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.section	.text._ZN2v88internal14TurboAssembler16LoadRootRelativeENS0_8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler16LoadRootRelativeENS0_8RegisterEi
	.type	_ZN2v88internal14TurboAssembler16LoadRootRelativeENS0_8RegisterEi, @function
_ZN2v88internal14TurboAssembler16LoadRootRelativeENS0_8RegisterEi:
.LFB24623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-48(%rbp), %rdi
	subq	$32, %rsp
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L6
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L6:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24623:
	.size	_ZN2v88internal14TurboAssembler16LoadRootRelativeENS0_8RegisterEi, .-_ZN2v88internal14TurboAssembler16LoadRootRelativeENS0_8RegisterEi
	.section	.text._ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE
	.type	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE, @function
_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE:
.LFB24627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	%edx, %edi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18TurboAssemblerBase30RootRegisterOffsetForRootIndexENS0_9RootIndexE@PLT
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-48(%rbp), %rdi
	movl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L10
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L10:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24627:
	.size	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE, .-_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE
	.section	.text._ZN2v88internal14TurboAssembler22LoadRootRegisterOffsetENS0_8RegisterEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler22LoadRootRegisterOffsetENS0_8RegisterEl
	.type	_ZN2v88internal14TurboAssembler22LoadRootRegisterOffsetENS0_8RegisterEl, @function
_ZN2v88internal14TurboAssembler22LoadRootRegisterOffsetENS0_8RegisterEl:
.LFB24622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L12
	cmpl	$13, %esi
	jne	.L19
.L11:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L20
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	$8, %ecx
	movl	$13, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movl	%r12d, %esi
	movl	$8, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L11
.L20:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24622:
	.size	_ZN2v88internal14TurboAssembler22LoadRootRegisterOffsetENS0_8RegisterEl, .-_ZN2v88internal14TurboAssembler22LoadRootRegisterOffsetENS0_8RegisterEl
	.section	.text._ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE:
.LFB24766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	call	*56(%rax)
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24766:
	.size	_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler4JumpERKNS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4JumpERKNS0_17ExternalReferenceE
	.type	_ZN2v88internal14TurboAssembler4JumpERKNS0_17ExternalReferenceE, @function
_ZN2v88internal14TurboAssembler4JumpERKNS0_17ExternalReferenceE:
.LFB24753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	512(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-36(%rbp), %rdi
	movl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-36(%rbp), %rsi
	movl	-28(%rbp), %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpENS0_7OperandE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L26:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24753:
	.size	_ZN2v88internal14TurboAssembler4JumpERKNS0_17ExternalReferenceE, .-_ZN2v88internal14TurboAssembler4JumpERKNS0_17ExternalReferenceE
	.section	.text._ZN2v88internal14TurboAssembler14JumpCodeObjectENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler14JumpCodeObjectENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler14JumpCodeObjectENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler14JumpCodeObjectENS0_8RegisterE:
.LFB24767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdi), %rax
	movq	%rdi, %r12
	call	*56(%rax)
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24767:
	.size	_ZN2v88internal14TurboAssembler14JumpCodeObjectENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler14JumpCodeObjectENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_
	.type	_ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_, @function
_ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_:
.LFB24765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 180(%rdi)
	jne	.L36
	cmpl	%esi, %edx
	jne	.L37
.L33:
	movl	$8, %r8d
	movl	%r13d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movabsq	$81604378687, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
.L29:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	movl	$8, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	-80(%rbp), %rdi
	movl	$43, %edx
	movl	%r14d, %esi
	movq	$0, -144(%rbp)
	movq	$0, -136(%rbp)
	leaq	-144(%rbp), %rbx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movq	%r12, %rdi
	movabsq	$83751862272, %rcx
	movl	$4, %r8d
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_9ImmediateEi@PLT
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	cmpl	%r13d, %r14d
	jne	.L39
.L31:
	movl	$8, %r8d
	movl	%r13d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movabsq	$81604378687, %rcx
	leaq	-136(%rbp), %r15
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	leaq	-92(%rbp), %rdi
	movl	%r14d, %esi
	movl	$59, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-92(%rbp), %rdx
	movl	-84(%rbp), %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$4, %r8d
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-128(%rbp), %rdi
	movl	%r13d, %edx
	movl	$12512, %r8d
	movl	$3, %ecx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-128(%rbp), %rdx
	movl	-120(%rbp), %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movq	%rdx, -92(%rbp)
	movl	%ecx, -84(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$8, %ecx
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	jmp	.L31
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24765:
	.size	_ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_, .-_ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler22LoadFromConstantsTableENS0_8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler22LoadFromConstantsTableENS0_8RegisterEi
	.type	_ZN2v88internal14TurboAssembler22LoadFromConstantsTableENS0_8RegisterEi, @function
_ZN2v88internal14TurboAssembler22LoadFromConstantsTableENS0_8RegisterEi:
.LFB24621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	leaq	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE(%rip), %rdx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L41
	movl	$573, %edi
	call	_ZN2v88internal18TurboAssemblerBase30RootRegisterOffsetForRootIndexENS0_9RootIndexE@PLT
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-52(%rbp), %rdi
	movl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rdx
	movl	-44(%rbp), %ecx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
.L42:
	leal	15(,%rbx,8), %edx
	leaq	-64(%rbp), %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rdx
	movl	-56(%rbp), %ecx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -52(%rbp)
	movl	%ecx, -44(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movl	$573, %edx
	call	*%rax
	jmp	.L42
.L45:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24621:
	.size	_ZN2v88internal14TurboAssembler22LoadFromConstantsTableENS0_8RegisterEi, .-_ZN2v88internal14TurboAssembler22LoadFromConstantsTableENS0_8RegisterEi
	.section	.text._ZN2v88internal14TurboAssembler18CallBuiltinByIndexENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler18CallBuiltinByIndexENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler18CallBuiltinByIndexENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler18CallBuiltinByIndexENS0_8RegisterE:
.LFB24763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %r8d
	movl	$7, %ecx
	movabsq	$81604378656, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	movl	%r13d, %edx
	leaq	-48(%rbp), %rdi
	movl	$12512, %r8d
	movl	$3, %ecx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-48(%rbp), %rdx
	movl	-40(%rbp), %r8d
	testb	$8, 1+_ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L47
	movq	%rdx, %rsi
	movq	%r12, %rdi
	movl	%r8d, %edx
	call	_ZN2v88internal9Assembler4callENS0_7OperandE@PLT
.L46:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movl	-40(%rbp), %ecx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	jmp	.L46
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24763:
	.size	_ZN2v88internal14TurboAssembler18CallBuiltinByIndexENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler18CallBuiltinByIndexENS0_8RegisterE
	.section	.text._ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi
	.type	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi, @function
_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi:
.LFB24614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	12(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	xorl	%eax, %eax
	testl	%edx, %edx
	movl	16(%rdi), %edx
	sete	%al
	xorl	%ecx, %ecx
	cmpl	$4, (%rdi)
	setne	%cl
	cmpl	$-1, 4(%rdi)
	leal	8(%rdx,%rcx,8), %edx
	je	.L59
	movl	4(%rdi), %r10d
	subl	$1, %eax
	leaq	-32(%rbp), %r9
	movl	$3, %ecx
	subl	%esi, %eax
	movl	(%rdi), %esi
	movq	%r9, %rdi
	leal	(%rdx,%rax,8), %r8d
	movl	%r10d, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-32(%rbp), %rax
	movq	%rax, -20(%rbp)
	movl	-24(%rbp), %eax
	movl	%eax, -12(%rbp)
.L55:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	movq	-20(%rbp), %rax
	movl	-12(%rbp), %edx
	jne	.L60
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	addl	8(%rdi), %eax
	leaq	-32(%rbp), %r8
	subl	$1, %eax
	subl	%esi, %eax
	movl	(%rdi), %esi
	movq	%r8, %rdi
	leal	(%rdx,%rax,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-32(%rbp), %rax
	movq	%rax, -20(%rbp)
	movl	-24(%rbp), %eax
	movl	%eax, -12(%rbp)
	jmp	.L55
.L60:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24614:
	.size	_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi, .-_ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi
	.section	.text._ZN2v88internal22StackArgumentsAccessorC2ENS0_8RegisterERKNS0_14ParameterCountENS0_34StackArgumentsAccessorReceiverModeEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal22StackArgumentsAccessorC2ENS0_8RegisterERKNS0_14ParameterCountENS0_34StackArgumentsAccessorReceiverModeEi
	.type	_ZN2v88internal22StackArgumentsAccessorC2ENS0_8RegisterERKNS0_14ParameterCountENS0_34StackArgumentsAccessorReceiverModeEi, @function
_ZN2v88internal22StackArgumentsAccessorC2ENS0_8RegisterERKNS0_14ParameterCountENS0_34StackArgumentsAccessorReceiverModeEi:
.LFB24616:
	.cfi_startproc
	endbr64
	movl	(%rdx), %eax
	movl	%esi, (%rdi)
	cmpl	$-1, %eax
	je	.L62
	movl	%eax, 4(%rdi)
	xorl	%eax, %eax
	movl	%eax, 8(%rdi)
	movl	%ecx, 12(%rdi)
	movl	%r8d, 16(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	movzwl	4(%rdx), %eax
	movl	$-1, 4(%rdi)
	movl	%ecx, 12(%rdi)
	movl	%eax, 8(%rdi)
	movl	%r8d, 16(%rdi)
	ret
	.cfi_endproc
.LFE24616:
	.size	_ZN2v88internal22StackArgumentsAccessorC2ENS0_8RegisterERKNS0_14ParameterCountENS0_34StackArgumentsAccessorReceiverModeEi, .-_ZN2v88internal22StackArgumentsAccessorC2ENS0_8RegisterERKNS0_14ParameterCountENS0_34StackArgumentsAccessorReceiverModeEi
	.globl	_ZN2v88internal22StackArgumentsAccessorC1ENS0_8RegisterERKNS0_14ParameterCountENS0_34StackArgumentsAccessorReceiverModeEi
	.set	_ZN2v88internal22StackArgumentsAccessorC1ENS0_8RegisterERKNS0_14ParameterCountENS0_34StackArgumentsAccessorReceiverModeEi,_ZN2v88internal22StackArgumentsAccessorC2ENS0_8RegisterERKNS0_14ParameterCountENS0_34StackArgumentsAccessorReceiverModeEi
	.section	.rodata._ZN2v88internal14MacroAssembler4LoadENS0_8RegisterENS0_17ExternalReferenceE.str1.1,"aMS",@progbits,1
.LC0:
	.string	"is_int32(offset)"
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal14MacroAssembler4LoadENS0_8RegisterENS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler4LoadENS0_8RegisterENS0_17ExternalReferenceE
	.type	_ZN2v88internal14MacroAssembler4LoadENS0_8RegisterENS0_17ExternalReferenceE, @function
_ZN2v88internal14MacroAssembler4LoadENS0_8RegisterENS0_17ExternalReferenceE:
.LFB24618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$80, %rsp
	movq	%rdx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 528(%rdi)
	je	.L65
	cmpb	$0, 178(%rdi)
	jne	.L84
.L65:
	testl	%r13d, %r13d
	jne	.L67
	cmpb	$0, 180(%r12)
	je	.L68
.L67:
	movq	-104(%rbp), %rax
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	cmpb	$0, 528(%r12)
	movq	%rax, -88(%rbp)
	jne	.L85
.L69:
	movq	-88(%rbp), %rdx
	movl	$8, %r8d
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L83:
	leaq	-64(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r14d, %esi
.L82:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movq	%rax, -52(%rbp)
	movl	-56(%rbp), %eax
	movq	-52(%rbp), %rdx
	movl	%eax, %ecx
	movl	%eax, -44(%rbp)
	movq	%rdx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
.L64:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	cmpb	$0, 178(%r12)
	jne	.L70
.L75:
	cmpb	$0, 180(%r12)
	je	.L69
	movq	512(%r12), %rdi
	leaq	-88(%rbp), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movq	512(%r12), %rdi
	movq	%r15, %rsi
	testb	%al, %al
	je	.L76
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-64(%rbp), %rdi
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L82
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L68:
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8load_raxENS0_17ExternalReferenceE@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L84:
	movq	512(%rdi), %rdi
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	ja	.L65
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rdx
	movl	-56(%rbp), %ecx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -52(%rbp)
	movl	%ecx, -44(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L70:
	movq	512(%r12), %rdi
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$2147483648, %edx
	movl	$4294967295, %ecx
	addq	%rax, %rdx
	cmpq	%rcx, %rdx
	jbe	.L87
	cmpb	$0, 528(%r12)
	jne	.L75
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L76:
	call	_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-52(%rbp), %rdi
	movl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rdx
	movl	-44(%rbp), %ecx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L87:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-64(%rbp), %rdi
	movl	%eax, %edx
	jmp	.L82
.L86:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24618:
	.size	_ZN2v88internal14MacroAssembler4LoadENS0_8RegisterENS0_17ExternalReferenceE, .-_ZN2v88internal14MacroAssembler4LoadENS0_8RegisterENS0_17ExternalReferenceE
	.section	.text._ZN2v88internal14MacroAssembler5StoreENS0_17ExternalReferenceENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler5StoreENS0_17ExternalReferenceENS0_8RegisterE
	.type	_ZN2v88internal14MacroAssembler5StoreENS0_17ExternalReferenceENS0_8RegisterE, @function
_ZN2v88internal14MacroAssembler5StoreENS0_17ExternalReferenceENS0_8RegisterE:
.LFB24620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$80, %rsp
	movq	%rsi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 528(%rdi)
	je	.L89
	cmpb	$0, 178(%rdi)
	jne	.L108
.L89:
	testl	%r13d, %r13d
	jne	.L91
	cmpb	$0, 180(%r12)
	je	.L92
.L91:
	movq	-104(%rbp), %rax
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	cmpb	$0, 528(%r12)
	movq	%rax, -88(%rbp)
	jne	.L109
.L93:
	movq	-88(%rbp), %rdx
	movl	$8, %r8d
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L107:
	leaq	-64(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r14d, %esi
.L106:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rax
	movl	%r13d, %ecx
	movq	%r12, %rdi
	movl	$8, %r8d
	movq	%rax, -52(%rbp)
	movl	-56(%rbp), %eax
	movq	-52(%rbp), %rsi
	movl	%eax, %edx
	movl	%eax, -44(%rbp)
	movq	%rsi, -64(%rbp)
	movl	%eax, -56(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
.L88:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L110
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	cmpb	$0, 178(%r12)
	jne	.L94
.L99:
	cmpb	$0, 180(%r12)
	je	.L93
	movq	512(%r12), %rdi
	leaq	-88(%rbp), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movq	512(%r12), %rdi
	movq	%r15, %rsi
	testb	%al, %al
	je	.L100
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-64(%rbp), %rdi
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L106
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L92:
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9store_raxENS0_17ExternalReferenceE@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L108:
	movq	512(%rdi), %rdi
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	ja	.L89
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rsi
	movl	-56(%rbp), %edx
	movl	%r13d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rsi, -52(%rbp)
	movl	%edx, -44(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L94:
	movq	512(%r12), %rdi
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$2147483648, %edx
	movl	$4294967295, %ecx
	addq	%rax, %rdx
	cmpq	%rcx, %rdx
	jbe	.L111
	cmpb	$0, 528(%r12)
	jne	.L99
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L100:
	call	_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-52(%rbp), %rdi
	movl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rdx
	movl	-44(%rbp), %ecx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rdx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L111:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-64(%rbp), %rdi
	movl	%eax, %edx
	jmp	.L106
.L110:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24620:
	.size	_ZN2v88internal14MacroAssembler5StoreENS0_17ExternalReferenceENS0_8RegisterE, .-_ZN2v88internal14MacroAssembler5StoreENS0_17ExternalReferenceENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE
	.type	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE, @function
_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE:
.LFB24624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%rdx, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 528(%rdi)
	je	.L113
	cmpb	$0, 178(%rdi)
	jne	.L114
	cmpb	$0, 180(%r12)
	jne	.L125
.L113:
	movq	-56(%rbp), %rdx
	movl	$8, %r8d
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L112:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	512(%rdi), %rdi
	leaq	-56(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L127
	cmpb	$0, 528(%r12)
	je	.L113
	cmpb	$0, 180(%r12)
	je	.L113
.L125:
	movq	-56(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18TurboAssemblerBase29IndirectLoadExternalReferenceENS0_8RegisterENS0_17ExternalReferenceE@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L127:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-48(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L112
.L126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24624:
	.size	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE, .-_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE
	.section	.text._ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE:
.LFB24625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	%rsi, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 528(%rdi)
	je	.L129
	cmpb	$0, 178(%rdi)
	jne	.L130
	cmpb	$0, 180(%r12)
	jne	.L146
.L129:
	movq	-72(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	leaq	-64(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r13d, %esi
.L143:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rax
	movq	%rax, -52(%rbp)
	movl	-56(%rbp), %eax
	movl	%eax, -44(%rbp)
	movq	-52(%rbp), %rax
	movl	-44(%rbp), %edx
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L147
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	movq	512(%r12), %rdi
	leaq	-72(%rbp), %r14
	movq	%r14, %rsi
	call	_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movq	512(%r12), %rdi
	movq	%r14, %rsi
	testb	%al, %al
	je	.L136
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L144
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L130:
	movq	512(%rdi), %rdi
	leaq	-72(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L144
	cmpb	$0, 528(%r12)
	je	.L129
	cmpb	$0, 180(%r12)
	je	.L129
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L136:
	call	_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	leaq	-64(%rbp), %r14
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	movl	%eax, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rdx
	movl	-56(%rbp), %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r14, %rdi
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L144:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-64(%rbp), %rdi
	jmp	.L143
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24625:
	.size	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE
	.section	.text._ZN2v88internal14MacroAssembler11PushAddressENS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler11PushAddressENS0_17ExternalReferenceE
	.type	_ZN2v88internal14MacroAssembler11PushAddressENS0_17ExternalReferenceE, @function
_ZN2v88internal14MacroAssembler11PushAddressENS0_17ExternalReferenceE:
.LFB24626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 528(%rdi)
	movq	%rsi, -56(%rbp)
	je	.L149
	cmpb	$0, 178(%rdi)
	jne	.L150
	cmpb	$0, 180(%r12)
	jne	.L162
.L149:
	movq	-56(%rbp), %rdx
	movl	$8, %r8d
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L154:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	movq	512(%rdi), %rdi
	leaq	-56(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L164
	cmpb	$0, 528(%r12)
	je	.L149
	cmpb	$0, 180(%r12)
	je	.L149
.L162:
	movq	-56(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18TurboAssemblerBase29IndirectLoadExternalReferenceENS0_8RegisterENS0_17ExternalReferenceE@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L164:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-36(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-28(%rbp), %ecx
	movq	-36(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L154
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24626:
	.size	_ZN2v88internal14MacroAssembler11PushAddressENS0_17ExternalReferenceE, .-_ZN2v88internal14MacroAssembler11PushAddressENS0_17ExternalReferenceE
	.section	.text._ZN2v88internal14MacroAssembler8PushRootENS0_9RootIndexE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler8PushRootENS0_9RootIndexE
	.type	_ZN2v88internal14MacroAssembler8PushRootENS0_9RootIndexE, @function
_ZN2v88internal14MacroAssembler8PushRootENS0_9RootIndexE:
.LFB24628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	%esi, %edi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18TurboAssemblerBase30RootRegisterOffsetForRootIndexENS0_9RootIndexE@PLT
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-48(%rbp), %rdi
	movl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-48(%rbp), %rsi
	movl	-40(%rbp), %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_7OperandE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L168
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L168:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24628:
	.size	_ZN2v88internal14MacroAssembler8PushRootENS0_9RootIndexE, .-_ZN2v88internal14MacroAssembler8PushRootENS0_9RootIndexE
	.section	.text._ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE
	.type	_ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE, @function
_ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE:
.LFB24629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	%edx, %edi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18TurboAssemblerBase30RootRegisterOffsetForRootIndexENS0_9RootIndexE@PLT
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-60(%rbp), %rdi
	movl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-60(%rbp), %rcx
	movl	-52(%rbp), %r8d
	movl	%r13d, %edx
	movl	$8, %r9d
	movl	$59, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L174:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24629:
	.size	_ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE, .-_ZN2v88internal14TurboAssembler11CompareRootENS0_8RegisterENS0_9RootIndexE
	.section	.text._ZN2v88internal14TurboAssembler11CompareRootENS0_7OperandENS0_9RootIndexE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler11CompareRootENS0_7OperandENS0_9RootIndexE
	.type	_ZN2v88internal14TurboAssembler11CompareRootENS0_7OperandENS0_9RootIndexE, @function
_ZN2v88internal14TurboAssembler11CompareRootENS0_7OperandENS0_9RootIndexE:
.LFB24631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -32
	movl	%edx, -56(%rbp)
	leaq	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE(%rip), %rdx
	movq	%rsi, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L176
	movl	%ecx, %edi
	call	_ZN2v88internal18TurboAssemblerBase30RootRegisterOffsetForRootIndexENS0_9RootIndexE@PLT
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-36(%rbp), %rdi
	movl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-28(%rbp), %ecx
	movq	-36(%rbp), %rdx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
.L177:
	movq	-64(%rbp), %rcx
	movl	-56(%rbp), %r8d
	movl	$10, %edx
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$57, %esi
	movq	%rcx, -36(%rbp)
	movl	%r8d, -28(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L182
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movl	%ecx, %edx
	call	*%rax
	jmp	.L177
.L182:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24631:
	.size	_ZN2v88internal14TurboAssembler11CompareRootENS0_7OperandENS0_9RootIndexE, .-_ZN2v88internal14TurboAssembler11CompareRootENS0_7OperandENS0_9RootIndexE
	.section	.text._ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE:
.LFB24632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	%ecx, -12(%rbp)
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	movl	-12(%rbp), %ecx
	movl	$8, %r8d
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
.L187:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24632:
	.size	_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler22LoadTaggedPointerFieldENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler18LoadAnyTaggedFieldENS0_8RegisterENS0_7OperandES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler18LoadAnyTaggedFieldENS0_8RegisterENS0_7OperandES2_
	.type	_ZN2v88internal14TurboAssembler18LoadAnyTaggedFieldENS0_8RegisterENS0_7OperandES2_, @function
_ZN2v88internal14TurboAssembler18LoadAnyTaggedFieldENS0_8RegisterENS0_7OperandES2_:
.LFB24633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	%ecx, -12(%rbp)
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L192
	movl	-12(%rbp), %ecx
	movl	$8, %r8d
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
.L192:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24633:
	.size	_ZN2v88internal14TurboAssembler18LoadAnyTaggedFieldENS0_8RegisterENS0_7OperandES2_, .-_ZN2v88internal14TurboAssembler18LoadAnyTaggedFieldENS0_8RegisterENS0_7OperandES2_
	.section	.text._ZN2v88internal14TurboAssembler22PushTaggedPointerFieldENS0_7OperandENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler22PushTaggedPointerFieldENS0_7OperandENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler22PushTaggedPointerFieldENS0_7OperandENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler22PushTaggedPointerFieldENS0_7OperandENS0_8RegisterE:
.LFB24634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5pushqENS0_7OperandE@PLT
.L197:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24634:
	.size	_ZN2v88internal14TurboAssembler22PushTaggedPointerFieldENS0_7OperandENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler22PushTaggedPointerFieldENS0_7OperandENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler18PushTaggedAnyFieldENS0_7OperandENS0_8RegisterES3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler18PushTaggedAnyFieldENS0_7OperandENS0_8RegisterES3_
	.type	_ZN2v88internal14TurboAssembler18PushTaggedAnyFieldENS0_7OperandENS0_8RegisterES3_, @function
_ZN2v88internal14TurboAssembler18PushTaggedAnyFieldENS0_7OperandENS0_8RegisterES3_:
.LFB24635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L202
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5pushqENS0_7OperandE@PLT
.L202:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24635:
	.size	_ZN2v88internal14TurboAssembler18PushTaggedAnyFieldENS0_7OperandENS0_8RegisterES3_, .-_ZN2v88internal14TurboAssembler18PushTaggedAnyFieldENS0_7OperandENS0_8RegisterES3_
	.section	.text._ZN2v88internal14TurboAssembler13SmiUntagFieldENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler13SmiUntagFieldENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler13SmiUntagFieldENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler13SmiUntagFieldENS0_8RegisterENS0_7OperandE:
.LFB24636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	leaq	-36(%rbp), %rdi
	.cfi_offset 12, -32
	movl	%esi, %r12d
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movl	$4, %ecx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ES1_i@PLT
	movl	-28(%rbp), %ecx
	movq	-36(%rbp), %rdx
	movl	%r12d, %esi
	movl	$4, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r12d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterES2_@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$64, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L206:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24636:
	.size	_ZN2v88internal14TurboAssembler13SmiUntagFieldENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler13SmiUntagFieldENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_9ImmediateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_9ImmediateE
	.type	_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_9ImmediateE, @function
_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_9ImmediateE:
.LFB24637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L211
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$8, %r8d
	jmp	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
.L211:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24637:
	.size	_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_9ImmediateE, .-_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_9ImmediateE
	.section	.text._ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_8RegisterE:
.LFB24638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	$8, %r8d
	jmp	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
.L216:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24638:
	.size	_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler16StoreTaggedFieldENS0_7OperandENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterENS0_7OperandE:
.LFB24639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%rdx, -112(%rbp)
	movl	%ecx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L224
.L218:
	movl	-104(%rbp), %ecx
	movq	-112(%rbp), %rdx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterENS0_7OperandE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L225
.L217:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L226
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	leaq	-80(%rbp), %r14
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
	movq	$24, -88(%rbp)
	movq	%r14, %rdi
	leaq	-64(%rbp), %r13
	movq	%r13, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-88(%rbp), %rdx
	leaq	40(%rbx), %rdi
	movdqa	.LC2(%rip), %xmm0
	movabsq	$7234309766868329573, %rcx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -72(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r14, %rdx
	movq	32(%rbx), %rsi
	subq	16(%rbx), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L218
	call	_ZdlPv@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L225:
	movq	32(%rbx), %rsi
	leaq	40(%rbx), %rdi
	leaq	-80(%rbp), %rdx
	subq	16(%rbx), %rsi
	movl	$93, %eax
	leaq	-64(%rbp), %r12
	movq	$1, -72(%rbp)
	movq	%r12, -80(%rbp)
	movw	%ax, -64(%rbp)
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-80(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L217
	call	_ZdlPv@PLT
	jmp	.L217
.L226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24639:
	.size	_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterES2_
	.type	_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterES2_, @function
_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterES2_:
.LFB24640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L234
.L228:
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterES2_@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L235
.L227:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L236
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	leaq	-96(%rbp), %r15
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	$24, -104(%rbp)
	movq	%r15, %rdi
	leaq	-80(%rbp), %r14
	movq	%r14, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	leaq	40(%rbx), %rdi
	movdqa	.LC2(%rip), %xmm0
	movabsq	$7234309766868329573, %rcx
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r15, %rdx
	movq	32(%rbx), %rsi
	subq	16(%rbx), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L228
	call	_ZdlPv@PLT
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L235:
	movq	32(%rbx), %rsi
	leaq	40(%rbx), %rdi
	leaq	-96(%rbp), %rdx
	subq	16(%rbx), %rsi
	movl	$93, %eax
	leaq	-80(%rbp), %r12
	movq	$1, -88(%rbp)
	movq	%r12, -96(%rbp)
	movw	%ax, -80(%rbp)
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L227
	call	_ZdlPv@PLT
	jmp	.L227
.L236:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24640:
	.size	_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterES2_, .-_ZN2v88internal14TurboAssembler22DecompressTaggedSignedENS0_8RegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterENS0_7OperandE:
.LFB24641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%rdx, -112(%rbp)
	movl	%ecx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L244
.L238:
	movl	-104(%rbp), %ecx
	movq	-112(%rbp), %rdx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterENS0_7OperandE@PLT
	movl	$13, %ecx
	movl	%r12d, %edx
	movq	%rbx, %rdi
	movl	$8, %r8d
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L245
.L237:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L246
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	leaq	-80(%rbp), %r14
	leaq	-88(%rbp), %rsi
	xorl	%edx, %edx
	movq	$25, -88(%rbp)
	movq	%r14, %rdi
	leaq	-64(%rbp), %r13
	movq	%r13, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-88(%rbp), %rdx
	leaq	40(%rbx), %rdi
	movdqa	.LC2(%rip), %xmm0
	movabsq	$7310589494247318629, %rcx
	movq	%rax, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rcx, 16(%rax)
	movb	$114, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rdx
	movq	%rax, -72(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r14, %rdx
	movq	32(%rbx), %rsi
	subq	16(%rbx), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L238
	call	_ZdlPv@PLT
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L245:
	movq	32(%rbx), %rsi
	leaq	40(%rbx), %rdi
	leaq	-80(%rbp), %rdx
	subq	16(%rbx), %rsi
	movl	$93, %eax
	leaq	-64(%rbp), %r12
	movq	$1, -72(%rbp)
	movq	%r12, -80(%rbp)
	movw	%ax, -64(%rbp)
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-80(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L237
	call	_ZdlPv@PLT
	jmp	.L237
.L246:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24641:
	.size	_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterES2_
	.type	_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterES2_, @function
_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterES2_:
.LFB24642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L254
.L248:
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterES2_@PLT
	movl	$13, %ecx
	movl	%r12d, %edx
	movq	%rbx, %rdi
	movl	$8, %r8d
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L255
.L247:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	leaq	-96(%rbp), %r15
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	$25, -104(%rbp)
	movq	%r15, %rdi
	leaq	-80(%rbp), %r14
	movq	%r14, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	leaq	40(%rbx), %rdi
	movdqa	.LC2(%rip), %xmm0
	movabsq	$7310589494247318629, %rcx
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rcx, 16(%rax)
	movb	$114, 24(%rax)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r15, %rdx
	movq	32(%rbx), %rsi
	subq	16(%rbx), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L248
	call	_ZdlPv@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L255:
	movq	32(%rbx), %rsi
	leaq	40(%rbx), %rdi
	leaq	-96(%rbp), %rdx
	subq	16(%rbx), %rsi
	movl	$93, %eax
	leaq	-80(%rbp), %r12
	movq	$1, -88(%rbp)
	movq	%r12, -96(%rbp)
	movw	%ax, -80(%rbp)
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L247
	call	_ZdlPv@PLT
	jmp	.L247
.L256:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24642:
	.size	_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterES2_, .-_ZN2v88internal14TurboAssembler23DecompressTaggedPointerENS0_8RegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler27DecompressRegisterAnyTaggedENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler27DecompressRegisterAnyTaggedENS0_8RegisterES2_
	.type	_ZN2v88internal14TurboAssembler27DecompressRegisterAnyTaggedENS0_8RegisterES2_, @function
_ZN2v88internal14TurboAssembler27DecompressRegisterAnyTaggedENS0_8RegisterES2_:
.LFB24643:
	.cfi_startproc
	endbr64
	movabsq	$81604378625, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-48(%rbp), %r14
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$13, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L260:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24643:
	.size	_ZN2v88internal14TurboAssembler27DecompressRegisterAnyTaggedENS0_8RegisterES2_, .-_ZN2v88internal14TurboAssembler27DecompressRegisterAnyTaggedENS0_8RegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterENS0_7OperandES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterENS0_7OperandES2_
	.type	_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterENS0_7OperandES2_, @function
_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterENS0_7OperandES2_:
.LFB24647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-104(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%rdx, -128(%rbp)
	movl	%ecx, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L269
.L262:
	movl	-120(%rbp), %ecx
	movq	-128(%rbp), %rdx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterENS0_7OperandE@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movabsq	$81604378625, %rdx
	movq	$0, -104(%rbp)
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$13, %ecx
	movl	%r12d, %edx
	movq	%rbx, %rdi
	movl	$8, %r8d
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L270
.L261:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L271
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_restore_state
	leaq	-96(%rbp), %r15
	xorl	%edx, %edx
	leaq	-80(%rbp), %r14
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	%r14, -96(%rbp)
	movq	$21, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	leaq	40(%rbx), %rdi
	movdqa	.LC3(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$1701275489, 16(%rax)
	movb	$100, 20(%rax)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r15, %rdx
	movq	32(%rbx), %rsi
	subq	16(%rbx), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L262
	call	_ZdlPv@PLT
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L270:
	movq	32(%rbx), %rsi
	leaq	40(%rbx), %rdi
	leaq	-96(%rbp), %rdx
	subq	16(%rbx), %rsi
	movl	$93, %eax
	leaq	-80(%rbp), %r12
	movq	$1, -88(%rbp)
	movq	%r12, -96(%rbp)
	movw	%ax, -80(%rbp)
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L261
	call	_ZdlPv@PLT
	jmp	.L261
.L271:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24647:
	.size	_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterENS0_7OperandES2_, .-_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterENS0_7OperandES2_
	.section	.text._ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterES2_S2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterES2_S2_
	.type	_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterES2_S2_, @function
_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterES2_S2_:
.LFB24648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-104(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L280
.L273:
	movl	%r13d, %edx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterES2_@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movabsq	$81604378625, %rdx
	movq	$0, -104(%rbp)
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$13, %ecx
	movl	%r12d, %edx
	movq	%rbx, %rdi
	movl	$8, %r8d
	movl	$3, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	cmpb	$0, _ZN2v88internal18FLAG_code_commentsE(%rip)
	jne	.L281
.L272:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L282
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	.cfi_restore_state
	leaq	-96(%rbp), %r8
	xorl	%edx, %edx
	leaq	-80(%rbp), %r15
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	%r8, -120(%rbp)
	movq	%r15, -96(%rbp)
	movq	$21, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-104(%rbp), %rdx
	movq	-120(%rbp), %r8
	leaq	40(%rbx), %rdi
	movdqa	.LC3(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movl	$1701275489, 16(%rax)
	movb	$100, 20(%rax)
	movups	%xmm0, (%rax)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	%r8, %rdx
	movq	32(%rbx), %rsi
	subq	16(%rbx), %rsi
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L273
	call	_ZdlPv@PLT
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L281:
	movq	32(%rbx), %rsi
	leaq	40(%rbx), %rdi
	leaq	-96(%rbp), %rdx
	subq	16(%rbx), %rsi
	movl	$93, %eax
	leaq	-80(%rbp), %r12
	movq	$1, -88(%rbp)
	movq	%r12, -96(%rbp)
	movw	%ax, -80(%rbp)
	call	_ZN2v88internal18CodeCommentsWriter3AddEjNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L272
	call	_ZdlPv@PLT
	jmp	.L272
.L282:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24648:
	.size	_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterES2_S2_, .-_ZN2v88internal14TurboAssembler19DecompressAnyTaggedENS0_8RegisterES2_S2_
	.section	.text._ZN2v88internal14TurboAssembler13SaveRegistersEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler13SaveRegistersEj
	.type	_ZN2v88internal14TurboAssembler13SaveRegistersEj, @function
_ZN2v88internal14TurboAssembler13SaveRegistersEj:
.LFB24650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	andl	$1, %esi
	jne	.L346
	testb	$2, %bl
	jne	.L347
.L285:
	testb	$4, %bl
	jne	.L348
.L286:
	testb	$8, %bl
	jne	.L349
.L287:
	testb	$16, %bl
	jne	.L350
.L288:
	testb	$32, %bl
	jne	.L351
.L289:
	testb	$64, %bl
	jne	.L352
.L290:
	testb	$-128, %bl
	jne	.L353
.L291:
	testb	$1, %bh
	jne	.L354
.L292:
	testb	$2, %bh
	jne	.L355
.L293:
	testb	$4, %bh
	jne	.L356
.L294:
	testb	$8, %bh
	jne	.L357
.L295:
	testb	$16, %bh
	jne	.L358
.L296:
	testb	$32, %bh
	jne	.L359
.L297:
	testb	$64, %bh
	jne	.L360
.L298:
	andb	$-128, %bh
	jne	.L361
.L283:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	xorl	%esi, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$2, %bl
	je	.L285
.L347:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$4, %bl
	je	.L286
.L348:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$8, %bl
	je	.L287
.L349:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$16, %bl
	je	.L288
.L350:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$32, %bl
	je	.L289
.L351:
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$64, %bl
	je	.L290
.L352:
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$-128, %bl
	je	.L291
.L353:
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$1, %bh
	je	.L292
.L354:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$2, %bh
	je	.L293
.L355:
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$4, %bh
	je	.L294
.L356:
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$8, %bh
	je	.L295
.L357:
	movl	$11, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$16, %bh
	je	.L296
.L358:
	movl	$12, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$32, %bh
	je	.L297
.L359:
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	testb	$64, %bh
	je	.L298
.L360:
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	andb	$-128, %bh
	je	.L283
.L361:
	popq	%rbx
	movq	%r12, %rdi
	movl	$15, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24650:
	.size	_ZN2v88internal14TurboAssembler13SaveRegistersEj, .-_ZN2v88internal14TurboAssembler13SaveRegistersEj
	.section	.text._ZN2v88internal14TurboAssembler16RestoreRegistersEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler16RestoreRegistersEj
	.type	_ZN2v88internal14TurboAssembler16RestoreRegistersEj, @function
_ZN2v88internal14TurboAssembler16RestoreRegistersEj:
.LFB24651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%esi, %ebx
	testb	$-128, %bh
	jne	.L425
	testb	$64, %bh
	jne	.L426
.L364:
	testb	$32, %bh
	jne	.L427
.L365:
	testb	$16, %bh
	jne	.L428
.L366:
	testb	$8, %bh
	jne	.L429
.L367:
	testb	$4, %bh
	jne	.L430
.L368:
	testb	$2, %bh
	jne	.L431
.L369:
	testb	$1, %bh
	jne	.L432
.L370:
	testb	$-128, %bl
	jne	.L433
.L371:
	testb	$64, %bl
	jne	.L434
.L372:
	testb	$32, %bl
	jne	.L435
.L373:
	testb	$16, %bl
	jne	.L436
.L374:
	testb	$8, %bl
	jne	.L437
.L375:
	testb	$4, %bl
	jne	.L438
.L376:
	testb	$2, %bl
	jne	.L439
.L377:
	andl	$1, %ebx
	jne	.L440
.L362:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	movl	$15, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$64, %bh
	je	.L364
.L426:
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$32, %bh
	je	.L365
.L427:
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$16, %bh
	je	.L366
.L428:
	movl	$12, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$8, %bh
	je	.L367
.L429:
	movl	$11, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$4, %bh
	je	.L368
.L430:
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$2, %bh
	je	.L369
.L431:
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$1, %bh
	je	.L370
.L432:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$-128, %bl
	je	.L371
.L433:
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$64, %bl
	je	.L372
.L434:
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$32, %bl
	je	.L373
.L435:
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$16, %bl
	je	.L374
.L436:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$8, %bl
	je	.L375
.L437:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$4, %bl
	je	.L376
.L438:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testb	$2, %bl
	je	.L377
.L439:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	andl	$1, %ebx
	je	.L362
.L440:
	popq	%rbx
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24651:
	.size	_ZN2v88internal14TurboAssembler16RestoreRegistersEj, .-_ZN2v88internal14TurboAssembler16RestoreRegistersEj
	.section	.text._ZN2v88internal14TurboAssembler19CheckStackAlignmentEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler19CheckStackAlignmentEv
	.type	_ZN2v88internal14TurboAssembler19CheckStackAlignmentEv, @function
_ZN2v88internal14TurboAssembler19CheckStackAlignmentEv:
.LFB24660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS24ActivationFrameAlignmentEv@PLT
	cmpl	$8, %eax
	jg	.L445
.L441:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L446
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	movl	_ZN2v88internalL3rspE(%rip), %esi
	leal	-1(%rax), %edx
	movq	%r12, %rdi
	leaq	-32(%rbp), %r13
	movabsq	$81604378624, %rax
	movl	$8, %ecx
	movq	$0, -32(%rbp)
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L441
.L446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24660:
	.size	_ZN2v88internal14TurboAssembler19CheckStackAlignmentEv, .-_ZN2v88internal14TurboAssembler19CheckStackAlignmentEv
	.section	.rodata._ZN2v88internal14MacroAssembler23JumpToExternalReferenceERKNS0_17ExternalReferenceEb.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"builtin_index != Builtins::kNoBuiltinId"
	.section	.text._ZN2v88internal14MacroAssembler23JumpToExternalReferenceERKNS0_17ExternalReferenceEb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler23JumpToExternalReferenceERKNS0_17ExternalReferenceEb
	.type	_ZN2v88internal14MacroAssembler23JumpToExternalReferenceERKNS0_17ExternalReferenceEb, @function
_ZN2v88internal14MacroAssembler23JumpToExternalReferenceERKNS0_17ExternalReferenceEb:
.LFB24665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movl	_ZN2v88internalL3rbxE(%rip), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	cmpb	$0, 528(%rdi)
	movq	%rax, -80(%rbp)
	je	.L448
	cmpb	$0, 178(%rdi)
	jne	.L449
	cmpb	$0, 180(%r12)
	jne	.L469
.L448:
	movq	-80(%rbp), %rdx
	movl	$8, %r8d
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L453:
	movq	512(%r12), %rdi
	movzbl	%r13b, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	cmpb	$0, 181(%r12)
	movq	%rax, %r13
	je	.L455
	movq	512(%r12), %rax
	leaq	-92(%rbp), %rdx
	movq	%r13, %rsi
	movl	$-1, -92(%rbp)
	leaq	41184(%rax), %rdi
	call	_ZNK2v88internal8Builtins15IsBuiltinHandleENS0_6HandleINS0_10HeapObjectEEEPi@PLT
	testb	%al, %al
	jne	.L470
.L455:
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
.L447:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L471
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	movl	-92(%rbp), %esi
	movq	%r12, %rdi
	movq	$0, -88(%rbp)
	call	_ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi@PLT
	cmpl	$-1, -92(%rbp)
	je	.L472
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r13d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	-92(%rbp), %esi
	leaq	-80(%rbp), %rdi
	movl	%r13d, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movl	$8, %r8d
	movl	$10, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	leaq	-88(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L449:
	movq	512(%rdi), %rdi
	leaq	-80(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L473
	cmpb	$0, 528(%r12)
	je	.L448
	cmpb	$0, 180(%r12)
	je	.L448
.L469:
	movq	-80(%rbp), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18TurboAssemblerBase29IndirectLoadExternalReferenceENS0_8RegisterENS0_17ExternalReferenceE@PLT
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L473:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-52(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-44(%rbp), %ecx
	movq	-52(%rbp), %rdx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L472:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L471:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24665:
	.size	_ZN2v88internal14MacroAssembler23JumpToExternalReferenceERKNS0_17ExternalReferenceEb, .-_ZN2v88internal14MacroAssembler23JumpToExternalReferenceERKNS0_17ExternalReferenceEb
	.section	.text._ZNK2v88internal14TurboAssembler31RequiredStackSizeForCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK2v88internal14TurboAssembler31RequiredStackSizeForCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_
	.type	_ZNK2v88internal14TurboAssembler31RequiredStackSizeForCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_, @function
_ZNK2v88internal14TurboAssembler31RequiredStackSizeForCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_:
.LFB24666:
	.cfi_startproc
	endbr64
	movdqa	_ZN2v88internalL10saved_regsE(%rip), %xmm3
	movd	%edx, %xmm6
	movd	%r8d, %xmm4
	movdqa	16+_ZN2v88internalL10saved_regsE(%rip), %xmm2
	pshufd	$0, %xmm6, %xmm1
	movd	%ecx, %xmm6
	pshufd	$0, %xmm6, %xmm5
	movdqa	%xmm3, %xmm0
	movdqa	%xmm3, %xmm7
	pcmpeqd	%xmm1, %xmm0
	pcmpeqd	%xmm5, %xmm7
	pshufd	$0, %xmm4, %xmm6
	pcmpeqd	%xmm2, %xmm1
	pcmpeqd	%xmm2, %xmm5
	pxor	%xmm4, %xmm4
	pcmpeqd	%xmm6, %xmm3
	pcmpeqd	%xmm6, %xmm2
	pcmpeqd	%xmm4, %xmm0
	pcmpeqd	%xmm4, %xmm7
	pcmpeqd	%xmm4, %xmm1
	pcmpeqd	%xmm4, %xmm5
	pcmpeqd	%xmm4, %xmm3
	pcmpeqd	%xmm4, %xmm2
	pand	%xmm7, %xmm0
	pand	%xmm5, %xmm1
	pand	%xmm3, %xmm0
	movdqa	.LC5(%rip), %xmm3
	pand	%xmm2, %xmm1
	pand	%xmm3, %xmm1
	pand	%xmm3, %xmm0
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$8, %xmm1
	paddd	%xmm1, %xmm0
	movdqa	%xmm0, %xmm1
	psrldq	$4, %xmm1
	paddd	%xmm1, %xmm0
	movd	%xmm0, %eax
	cmpl	$9, %edx
	je	.L475
	cmpl	$9, %ecx
	je	.L476
	leal	8(%rax), %edi
	cmpl	$9, %r8d
	cmovne	%edi, %eax
	cmpl	$10, %edx
	jne	.L475
.L481:
	cmpl	$11, %ecx
	je	.L484
.L485:
	leal	8(%rax), %edx
	cmpl	$11, %r8d
	cmovne	%edx, %eax
.L484:
	leal	128(%rax), %edx
	cmpl	$1, %esi
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L475:
	cmpl	$10, %ecx
	je	.L479
	leal	8(%rax), %edi
	cmpl	$10, %r8d
	cmovne	%edi, %eax
	cmpl	$11, %edx
	jne	.L481
.L504:
	leal	128(%rax), %edx
	cmpl	$1, %esi
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L479:
	cmpl	$11, %edx
	jne	.L485
	leal	128(%rax), %edx
	cmpl	$1, %esi
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	cmpl	$10, %edx
	je	.L485
	leal	8(%rax), %edi
	cmpl	$10, %r8d
	cmovne	%edi, %eax
	cmpl	$11, %edx
	je	.L504
	jmp	.L481
	.cfi_endproc
.LFE24666:
	.size	_ZNK2v88internal14TurboAssembler31RequiredStackSizeForCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_, .-_ZNK2v88internal14TurboAssembler31RequiredStackSizeForCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_
	.section	.text._ZN2v88internal14TurboAssembler15PushCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler15PushCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_
	.type	_ZN2v88internal14TurboAssembler15PushCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_, @function
_ZN2v88internal14TurboAssembler15PushCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_:
.LFB24668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movl	%esi, -132(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L562
	movl	%ecx, %r12d
	testl	%ecx, %ecx
	je	.L507
	movl	%r8d, %r12d
	testl	%r8d, %r8d
	je	.L508
	xorl	%esi, %esi
	movl	$8, %r12d
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L508:
	cmpl	$1, %r13d
	je	.L558
	cmpl	$1, %r15d
	je	.L510
.L560:
	cmpl	$1, %r14d
	je	.L511
	movl	$1, %esi
	movq	%rbx, %rdi
	addl	$8, %r12d
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L511:
	cmpl	$2, %r13d
	je	.L556
.L558:
	cmpl	$2, %r15d
	je	.L513
.L559:
	cmpl	$2, %r14d
	je	.L514
	movl	$2, %esi
	movq	%rbx, %rdi
	addl	$8, %r12d
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L514:
	cmpl	$3, %r13d
	je	.L554
.L556:
	cmpl	$3, %r15d
	je	.L516
.L557:
	cmpl	$3, %r14d
	je	.L517
	movl	$3, %esi
	movq	%rbx, %rdi
	addl	$8, %r12d
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L517:
	cmpl	$5, %r13d
	je	.L552
.L554:
	cmpl	$5, %r15d
	je	.L519
.L555:
	cmpl	$5, %r14d
	je	.L520
	movl	$5, %esi
	movq	%rbx, %rdi
	addl	$8, %r12d
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L520:
	cmpl	$6, %r13d
	je	.L550
.L552:
	cmpl	$6, %r15d
	je	.L522
.L553:
	cmpl	$6, %r14d
	je	.L523
	movl	$6, %esi
	movq	%rbx, %rdi
	addl	$8, %r12d
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L523:
	cmpl	$7, %r13d
	je	.L548
.L550:
	cmpl	$7, %r15d
	je	.L525
.L551:
	cmpl	$7, %r14d
	je	.L526
	movl	$7, %esi
	movq	%rbx, %rdi
	addl	$8, %r12d
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L526:
	cmpl	$8, %r13d
	je	.L546
.L548:
	cmpl	$8, %r15d
	je	.L528
.L549:
	cmpl	$8, %r14d
	je	.L529
	movl	$8, %esi
	movq	%rbx, %rdi
	addl	$8, %r12d
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L529:
	cmpl	$9, %r13d
	je	.L544
.L546:
	cmpl	$9, %r15d
	je	.L531
.L547:
	cmpl	$9, %r14d
	je	.L532
	movl	$9, %esi
	movq	%rbx, %rdi
	addl	$8, %r12d
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L532:
	cmpl	$10, %r13d
	je	.L542
.L544:
	cmpl	$10, %r15d
	je	.L534
.L545:
	cmpl	$10, %r14d
	je	.L535
	movl	$10, %esi
	movq	%rbx, %rdi
	addl	$8, %r12d
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L535:
	cmpl	$11, %r13d
	je	.L536
.L542:
	cmpl	$11, %r15d
	je	.L536
.L543:
	cmpl	$11, %r14d
	je	.L536
	movl	$11, %esi
	movq	%rbx, %rdi
	addl	$8, %r12d
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L536:
	cmpl	$1, -132(%rbp)
	je	.L637
.L505:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L638
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	.cfi_restore_state
	cmpl	$1, %edx
	je	.L559
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L562:
	xorl	%r12d, %r12d
	cmpl	$1, %r15d
	jne	.L560
.L510:
	cmpl	$2, %r13d
	je	.L557
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L534:
	cmpl	$11, %r13d
	jne	.L543
	cmpl	$1, -132(%rbp)
	jne	.L505
	.p2align 4,,10
	.p2align 3
.L637:
	movl	$8, %r8d
	movq	%rbx, %rdi
	leaq	-116(%rbp), %r13
	xorl	%r15d, %r15d
	movabsq	$81604378752, %rcx
	movl	$4, %edx
	movl	$5, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	_ZN2v88internalL3rspE(%rip), %r14d
	.p2align 4,,10
	.p2align 3
.L541:
	leal	0(,%r15,8), %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-116(%rbp), %r8
	movl	-108(%rbp), %edx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L538
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r9d
	movq	%rbx, %rdi
	pushq	$0
	movl	%r15d, %edx
	movl	$17, %esi
	addl	$1, %r15d
	pushq	$1
	pushq	$3
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	cmpl	$16, %r15d
	jne	.L541
	subl	$-128, %r12d
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L531:
	cmpl	$10, %r13d
	je	.L543
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L528:
	cmpl	$9, %r13d
	je	.L545
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L513:
	cmpl	$3, %r13d
	je	.L555
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L525:
	cmpl	$8, %r13d
	je	.L547
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L516:
	cmpl	$5, %r13d
	je	.L553
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L522:
	cmpl	$7, %r13d
	je	.L549
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L519:
	cmpl	$6, %r13d
	je	.L551
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L538:
	movl	%r15d, %ecx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	addl	$1, %r15d
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	cmpl	$16, %r15d
	jne	.L541
	subl	$-128, %r12d
	jmp	.L505
.L638:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24668:
	.size	_ZN2v88internal14TurboAssembler15PushCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_, .-_ZN2v88internal14TurboAssembler15PushCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_
	.section	.text._ZN2v88internal14TurboAssembler14PopCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler14PopCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_
	.type	_ZN2v88internal14TurboAssembler14PopCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_, @function
_ZN2v88internal14TurboAssembler14PopCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_:
.LFB24669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$104, %rsp
	movl	%ecx, -132(%rbp)
	movl	%r8d, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %esi
	je	.L775
.L640:
	cmpl	$11, %ebx
	je	.L645
	cmpl	$11, -132(%rbp)
	je	.L646
	cmpl	$11, -136(%rbp)
	je	.L647
	movl	$11, %esi
	movq	%r12, %rdi
	addl	$8, %r14d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
.L647:
	cmpl	$10, %ebx
	jne	.L645
.L692:
	cmpl	$9, -132(%rbp)
	je	.L652
.L693:
	cmpl	$9, -136(%rbp)
	je	.L653
	movl	$9, %esi
	movq	%r12, %rdi
	addl	$8, %r14d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
.L653:
	cmpl	$8, %ebx
	je	.L688
	cmpl	$8, -132(%rbp)
	je	.L655
.L691:
	cmpl	$8, -136(%rbp)
	je	.L656
	movl	$8, %esi
	movq	%r12, %rdi
	addl	$8, %r14d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
.L656:
	cmpl	$7, %ebx
	je	.L686
.L688:
	cmpl	$7, -132(%rbp)
	je	.L658
.L689:
	cmpl	$7, -136(%rbp)
	je	.L659
	movl	$7, %esi
	movq	%r12, %rdi
	addl	$8, %r14d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
.L659:
	cmpl	$6, %ebx
	je	.L684
.L686:
	cmpl	$6, -132(%rbp)
	je	.L661
.L687:
	cmpl	$6, -136(%rbp)
	je	.L662
	movl	$6, %esi
	movq	%r12, %rdi
	addl	$8, %r14d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
.L662:
	cmpl	$5, %ebx
	je	.L682
.L684:
	cmpl	$5, -132(%rbp)
	je	.L664
.L685:
	cmpl	$5, -136(%rbp)
	je	.L665
	movl	$5, %esi
	movq	%r12, %rdi
	addl	$8, %r14d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
.L665:
	cmpl	$3, %ebx
	je	.L680
.L682:
	cmpl	$3, -132(%rbp)
	je	.L667
.L683:
	cmpl	$3, -136(%rbp)
	je	.L668
	movl	$3, %esi
	movq	%r12, %rdi
	addl	$8, %r14d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
.L668:
	cmpl	$2, %ebx
	je	.L678
.L680:
	cmpl	$2, -132(%rbp)
	je	.L670
.L681:
	cmpl	$2, -136(%rbp)
	je	.L671
	movl	$2, %esi
	movq	%r12, %rdi
	addl	$8, %r14d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
.L671:
	cmpl	$1, %ebx
	je	.L676
.L678:
	cmpl	$1, -132(%rbp)
	je	.L673
.L679:
	cmpl	$1, -136(%rbp)
	je	.L674
	movl	$1, %esi
	movq	%r12, %rdi
	addl	$8, %r14d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
.L674:
	testl	%ebx, %ebx
	je	.L639
.L676:
	movl	-132(%rbp), %edx
	testl	%edx, %edx
	je	.L639
.L677:
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	je	.L639
	xorl	%esi, %esi
	movq	%r12, %rdi
	addl	$8, %r14d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
.L639:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L776
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	cmpl	$10, -132(%rbp)
	je	.L649
.L694:
	cmpl	$10, -136(%rbp)
	je	.L650
	movl	$10, %esi
	movq	%r12, %rdi
	addl	$8, %r14d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
.L650:
	cmpl	$9, %ebx
	jne	.L692
	cmpl	$8, -132(%rbp)
	jne	.L691
.L655:
	cmpl	$7, %ebx
	je	.L687
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L646:
	cmpl	$10, %ebx
	je	.L693
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L649:
	cmpl	$9, %ebx
	je	.L691
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L775:
	movl	_ZN2v88internalL3rspE(%rip), %r15d
	xorl	%r13d, %r13d
	leaq	-116(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L644:
	leal	0(,%r13,8), %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-108(%rbp), %r9d
	movq	-116(%rbp), %r8
	movl	%r9d, -84(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L641
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	pushq	$0
	movl	$16, %esi
	addl	$1, %r13d
	pushq	$1
	pushq	$3
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	cmpl	$16, %r13d
	jne	.L644
.L642:
	movl	$8, %r8d
	movl	$4, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movabsq	$81604378752, %rcx
	movl	$128, %r14d
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L641:
	movl	-84(%rbp), %ecx
	movl	%r13d, %esi
	movq	%r8, %rdx
	movq	%r12, %rdi
	addl	$1, %r13d
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	cmpl	$16, %r13d
	jne	.L644
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L673:
	testl	%ebx, %ebx
	je	.L639
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L664:
	cmpl	$3, %ebx
	je	.L681
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L670:
	cmpl	$1, %ebx
	je	.L677
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L667:
	cmpl	$2, %ebx
	je	.L679
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L661:
	cmpl	$5, %ebx
	je	.L683
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L652:
	cmpl	$8, %ebx
	je	.L689
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L658:
	cmpl	$6, %ebx
	je	.L685
	jmp	.L687
.L776:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24669:
	.size	_ZN2v88internal14TurboAssembler14PopCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_, .-_ZN2v88internal14TurboAssembler14PopCallerSavedENS0_14SaveFPRegsModeENS0_8RegisterES3_S3_
	.section	.text._ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterES2_
	.type	_ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterES2_, @function
_ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterES2_:
.LFB24670:
	.cfi_startproc
	endbr64
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L778
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movl	%edx, %ecx
	movl	$2, %r9d
	movl	%esi, %edx
	movl	$90, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$0
	pushq	$1
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L778:
	.cfi_restore 6
	jmp	_ZN2v88internal9Assembler8cvtss2sdENS0_11XMMRegisterES2_@PLT
	.cfi_endproc
.LFE24670:
	.size	_ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterES2_, .-_ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterENS0_7OperandE:
.LFB24671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	%ecx, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L785
	subq	$8, %rsp
	movq	%rdx, %r8
	movl	%ecx, %r9d
	movl	%esi, %edx
	pushq	$0
	movl	%esi, %ecx
	movl	$90, %esi
	pushq	$1
	pushq	$2
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L789
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L785:
	.cfi_restore_state
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L789
	movl	-40(%rbp), %ecx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler8cvtss2sdENS0_11XMMRegisterENS0_7OperandE@PLT
.L789:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24671:
	.size	_ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler8Cvtss2sdENS0_11XMMRegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterES2_
	.type	_ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterES2_, @function
_ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterES2_:
.LFB24672:
	.cfi_startproc
	endbr64
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L792
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movl	%edx, %ecx
	movl	$3, %r9d
	movl	%esi, %edx
	movl	$90, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$0
	pushq	$1
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L792:
	.cfi_restore 6
	jmp	_ZN2v88internal9Assembler8cvtsd2ssENS0_11XMMRegisterES2_@PLT
	.cfi_endproc
.LFE24672:
	.size	_ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterES2_, .-_ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterENS0_7OperandE:
.LFB24673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	%ecx, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L799
	subq	$8, %rsp
	movq	%rdx, %r8
	movl	%ecx, %r9d
	movl	%esi, %edx
	pushq	$0
	movl	%esi, %ecx
	movl	$90, %esi
	pushq	$1
	pushq	$3
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L803
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore_state
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L803
	movl	-40(%rbp), %ecx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler8cvtsd2ssENS0_11XMMRegisterENS0_7OperandE@PLT
.L803:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24673:
	.size	_ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler8Cvtsd2ssENS0_11XMMRegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_8RegisterE:
.LFB24674:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	subq	$8, %rsp
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L806
	movl	%esi, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$87, %esi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	pushq	$0
	movl	%r12d, %edx
	movl	%r14d, %r8d
	pushq	$1
	movl	%r12d, %ecx
	movq	%r13, %rdi
	movl	$3, %r9d
	movl	$42, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L806:
	.cfi_restore_state
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler5xorpdENS0_11XMMRegisterES2_@PLT
	leaq	-24(%rbp), %rsp
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvtlsi2sdENS0_11XMMRegisterENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24674:
	.size	_ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_7OperandE:
.LFB24675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	subq	$48, %rsp
	movq	%rdx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L811
	movl	%esi, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$87, %esi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	subq	$8, %rsp
	movq	-64(%rbp), %r8
	movl	%r12d, %ecx
	pushq	$0
	movl	-56(%rbp), %r9d
	movl	%r12d, %edx
	movl	$42, %esi
	pushq	$1
	movq	%r13, %rdi
	pushq	$3
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L815
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L811:
	.cfi_restore_state
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler5xorpdENS0_11XMMRegisterES2_@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L815
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	leaq	-16(%rbp), %rsp
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvtlsi2sdENS0_11XMMRegisterENS0_7OperandE@PLT
.L815:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24675:
	.size	_ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler9Cvtlsi2sdENS0_11XMMRegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_8RegisterE:
.LFB24676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	subq	$8, %rsp
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L818
	movl	%esi, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$87, %esi
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	pushq	$0
	movl	%r12d, %edx
	movl	%r14d, %r8d
	pushq	$1
	movl	%r12d, %ecx
	movq	%r13, %rdi
	movl	$2, %r9d
	movl	$42, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L818:
	.cfi_restore_state
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	leaq	-24(%rbp), %rsp
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvtlsi2ssENS0_11XMMRegisterENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24676:
	.size	_ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_7OperandE:
.LFB24677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	subq	$48, %rsp
	movq	%rdx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L823
	movl	%esi, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$87, %esi
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	subq	$8, %rsp
	movq	-64(%rbp), %r8
	movl	%r12d, %ecx
	pushq	$0
	movl	-56(%rbp), %r9d
	movl	%r12d, %edx
	movl	$42, %esi
	pushq	$1
	movq	%r13, %rdi
	pushq	$2
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L827
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L823:
	.cfi_restore_state
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L827
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	leaq	-16(%rbp), %rsp
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvtlsi2ssENS0_11XMMRegisterENS0_7OperandE@PLT
.L827:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24677:
	.size	_ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler9Cvtlsi2ssENS0_11XMMRegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_8RegisterE:
.LFB24678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	subq	$8, %rsp
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L830
	movl	%esi, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$87, %esi
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	pushq	$128
	movl	%r12d, %edx
	movl	%r14d, %r8d
	pushq	$1
	movl	%r12d, %ecx
	movq	%r13, %rdi
	movl	$2, %r9d
	movl	$42, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L830:
	.cfi_restore_state
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	leaq	-24(%rbp), %rsp
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvtqsi2ssENS0_11XMMRegisterENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24678:
	.size	_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_7OperandE:
.LFB24679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	subq	$48, %rsp
	movq	%rdx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L835
	movl	%esi, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$87, %esi
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	subq	$8, %rsp
	movq	-64(%rbp), %r8
	movl	%r12d, %ecx
	pushq	$128
	movl	-56(%rbp), %r9d
	movl	%r12d, %edx
	movq	%r13, %rdi
	pushq	$1
	movl	$42, %esi
	pushq	$2
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L839
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L835:
	.cfi_restore_state
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L839
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	leaq	-16(%rbp), %rsp
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvtqsi2ssENS0_11XMMRegisterENS0_7OperandE@PLT
.L839:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24679:
	.size	_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_8RegisterE:
.LFB24680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	subq	$8, %rsp
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L842
	movl	%esi, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$87, %esi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	pushq	$128
	movl	%r12d, %edx
	movl	%r14d, %r8d
	pushq	$1
	movl	%r12d, %ecx
	movq	%r13, %rdi
	movl	$3, %r9d
	movl	$42, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L842:
	.cfi_restore_state
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler5xorpdENS0_11XMMRegisterES2_@PLT
	leaq	-24(%rbp), %rsp
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvtqsi2sdENS0_11XMMRegisterENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24680:
	.size	_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_7OperandE:
.LFB24681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	subq	$48, %rsp
	movq	%rdx, -64(%rbp)
	movl	%ecx, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L847
	movl	%esi, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$87, %esi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	subq	$8, %rsp
	movq	-64(%rbp), %r8
	movl	%r12d, %ecx
	pushq	$128
	movl	-56(%rbp), %r9d
	movl	%r12d, %edx
	movq	%r13, %rdi
	pushq	$1
	movl	$42, %esi
	pushq	$3
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L851
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L847:
	.cfi_restore_state
	movl	%esi, %edx
	call	_ZN2v88internal9Assembler5xorpdENS0_11XMMRegisterES2_@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L851
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	leaq	-16(%rbp), %rsp
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvtqsi2sdENS0_11XMMRegisterENS0_7OperandE@PLT
.L851:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24681:
	.size	_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_8RegisterE:
.LFB24682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	subq	$8, %rsp
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L854
	movl	%r12d, %r8d
	movl	%r12d, %ecx
	movl	%r12d, %edx
	movq	%r13, %rdi
	movl	$87, %esi
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	pushq	$128
	movl	%r12d, %edx
	movl	%r12d, %ecx
	pushq	$1
	movq	%r13, %rdi
	movl	$2, %r9d
	movl	$42, %esi
	movl	$10, %r8d
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L854:
	.cfi_restore_state
	movl	%r12d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	leaq	-24(%rbp), %rsp
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvtqsi2ssENS0_11XMMRegisterENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24682:
	.size	_ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_7OperandE:
.LFB24683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %ecx
	movl	$4, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	subq	$56, %rsp
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L859
	movl	%r12d, %r8d
	movl	%r12d, %ecx
	movl	%r12d, %edx
	movl	$87, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	pushq	$128
	movl	%r12d, %edx
	movl	%r12d, %ecx
	pushq	$1
	movl	$2, %r9d
	movl	$42, %esi
	movq	%r13, %rdi
	movl	$10, %r8d
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L863
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L859:
	.cfi_restore_state
	movl	%r12d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L863
	leaq	-24(%rbp), %rsp
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvtqsi2ssENS0_11XMMRegisterENS0_8RegisterE@PLT
.L863:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24683:
	.size	_ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler9Cvtlui2ssENS0_11XMMRegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_8RegisterE:
.LFB24684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	subq	$8, %rsp
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L866
	movl	%r12d, %r8d
	movl	%r12d, %ecx
	movl	%r12d, %edx
	movq	%r13, %rdi
	movl	$87, %esi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	pushq	$128
	movl	%r12d, %edx
	movl	%r12d, %ecx
	pushq	$1
	movq	%r13, %rdi
	movl	$3, %r9d
	movl	$42, %esi
	movl	$10, %r8d
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore_state
	movl	%r12d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5xorpdENS0_11XMMRegisterES2_@PLT
	leaq	-24(%rbp), %rsp
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvtqsi2sdENS0_11XMMRegisterENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24684:
	.size	_ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_7OperandE:
.LFB24685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %ecx
	movl	$4, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movl	%esi, %r12d
	subq	$56, %rsp
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L871
	movl	%r12d, %r8d
	movl	%r12d, %ecx
	movl	%r12d, %edx
	movl	$87, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	pushq	$128
	movl	%r12d, %edx
	movl	%r12d, %ecx
	pushq	$1
	movl	$3, %r9d
	movl	$42, %esi
	movq	%r13, %rdi
	movl	$10, %r8d
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L875
	leaq	-24(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L871:
	.cfi_restore_state
	movl	%r12d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5xorpdENS0_11XMMRegisterES2_@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L875
	leaq	-24(%rbp), %rsp
	movl	%r14d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvtqsi2sdENS0_11XMMRegisterENS0_8RegisterE@PLT
.L875:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24685:
	.size	_ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler9Cvtlui2sdENS0_11XMMRegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_8RegisterE:
.LFB24686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	leaq	-72(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	call	_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_8RegisterE
	movl	$8, %ecx
	movl	%r14d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ebx
	cmpl	$10, %r14d
	je	.L878
	movl	$8, %ecx
	movl	%r14d, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
.L878:
	movl	$8, %r8d
	movl	$5, %ecx
	movl	%ebx, %esi
	movq	%r12, %rdi
	movabsq	$81604378624, %r14
	movq	%r14, %rdx
	orq	$1, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	leaq	-64(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$3, %esi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	movq	$0, -64(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r14, %rcx
	movl	$10, %edx
	movq	%r12, %rdi
	orq	$1, %rcx
	movl	$8, %r8d
	movl	$1, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-88(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler9Cvtqsi2ssENS0_11XMMRegisterENS0_8RegisterE
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L879
	movl	%r13d, %r8d
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_S2_@PLT
.L880:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L886
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L879:
	.cfi_restore_state
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5addssENS0_11XMMRegisterES2_@PLT
	jmp	.L880
.L886:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24686:
	.size	_ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_7OperandE:
.LFB24687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %ecx
	movl	$8, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L891
	addq	$56, %rsp
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_8RegisterE
.L891:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24687:
	.size	_ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler9Cvtqui2ssENS0_11XMMRegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_8RegisterE:
.LFB24688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	leaq	-72(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	call	_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_8RegisterE
	movl	$8, %ecx
	movl	%r14d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ebx
	cmpl	$10, %r14d
	je	.L893
	movl	$8, %ecx
	movl	%r14d, %edx
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
.L893:
	movl	$8, %r8d
	movl	$5, %ecx
	movl	%ebx, %esi
	movq	%r12, %rdi
	movabsq	$81604378624, %r14
	movq	%r14, %rdx
	orq	$1, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	leaq	-64(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$3, %esi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r9, -88(%rbp)
	movq	$0, -64(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r14, %rcx
	movl	$10, %edx
	movq	%r12, %rdi
	orq	$1, %rcx
	movl	$8, %r8d
	movl	$1, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-88(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler9Cvtqsi2sdENS0_11XMMRegisterENS0_8RegisterE
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L894
	pushq	$0
	movl	%r13d, %edx
	movl	$3, %r9d
	movl	%r13d, %r8d
	pushq	$1
	movl	%r13d, %ecx
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
.L895:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L901
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L894:
	.cfi_restore_state
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5addsdENS0_11XMMRegisterES2_@PLT
	jmp	.L895
.L901:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24688:
	.size	_ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_7OperandE:
.LFB24689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %ecx
	movl	$8, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L906
	addq	$56, %rsp
	movl	%r14d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_8RegisterE
.L906:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24689:
	.size	_ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler9Cvtqui2sdENS0_11XMMRegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_11XMMRegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_11XMMRegisterE
	.type	_ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_11XMMRegisterE, @function
_ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_11XMMRegisterE:
.LFB24690:
	.cfi_startproc
	endbr64
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L908
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r8d
	movl	$2, %r9d
	movl	%esi, %edx
	movl	$44, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$0
	pushq	$1
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L908:
	.cfi_restore 6
	jmp	_ZN2v88internal9Assembler9cvttss2siENS0_8RegisterENS0_11XMMRegisterE@PLT
	.cfi_endproc
.LFE24690:
	.size	_ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_11XMMRegisterE, .-_ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_11XMMRegisterE
	.section	.text._ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_7OperandE:
.LFB24691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	%ecx, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L915
	subq	$8, %rsp
	movl	%ecx, %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rdx, %r8
	pushq	$0
	movl	%esi, %edx
	movl	$44, %esi
	pushq	$1
	pushq	$2
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L919
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L915:
	.cfi_restore_state
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L919
	movl	-40(%rbp), %ecx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvttss2siENS0_8RegisterENS0_7OperandE@PLT
.L919:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24691:
	.size	_ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler9Cvttss2siENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_11XMMRegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_11XMMRegisterE
	.type	_ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_11XMMRegisterE, @function
_ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_11XMMRegisterE:
.LFB24692:
	.cfi_startproc
	endbr64
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L922
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r8d
	movl	$3, %r9d
	movl	%esi, %edx
	movl	$44, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$0
	pushq	$1
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L922:
	.cfi_restore 6
	jmp	_ZN2v88internal9Assembler9cvttsd2siENS0_8RegisterENS0_11XMMRegisterE@PLT
	.cfi_endproc
.LFE24692:
	.size	_ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_11XMMRegisterE, .-_ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_11XMMRegisterE
	.section	.text._ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_7OperandE:
.LFB24693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	%ecx, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L929
	subq	$8, %rsp
	movl	%ecx, %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rdx, %r8
	pushq	$0
	movl	%esi, %edx
	movl	$44, %esi
	pushq	$1
	pushq	$3
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L933
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L929:
	.cfi_restore_state
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L933
	movl	-40(%rbp), %ecx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9cvttsd2siENS0_8RegisterENS0_7OperandE@PLT
.L933:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24693:
	.size	_ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler9Cvttsd2siENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_11XMMRegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_11XMMRegisterE
	.type	_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_11XMMRegisterE, @function
_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_11XMMRegisterE:
.LFB24694:
	.cfi_startproc
	endbr64
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L936
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r8d
	movl	$2, %r9d
	movl	%esi, %edx
	movl	$44, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$128
	pushq	$1
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L936:
	.cfi_restore 6
	jmp	_ZN2v88internal9Assembler10cvttss2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	.cfi_endproc
.LFE24694:
	.size	_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_11XMMRegisterE, .-_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_11XMMRegisterE
	.section	.text._ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_7OperandE:
.LFB24695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	%ecx, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L943
	subq	$8, %rsp
	movl	%ecx, %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rdx, %r8
	pushq	$128
	movl	%esi, %edx
	movl	$44, %esi
	pushq	$1
	pushq	$2
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L947
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L943:
	.cfi_restore_state
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L947
	movl	-40(%rbp), %ecx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler10cvttss2siqENS0_8RegisterENS0_7OperandE@PLT
.L947:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24695:
	.size	_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler10Cvttss2siqENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE
	.type	_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE, @function
_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE:
.LFB24696:
	.cfi_startproc
	endbr64
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L950
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r8d
	movl	$3, %r9d
	movl	%esi, %edx
	movl	$44, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	$128
	pushq	$1
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L950:
	.cfi_restore 6
	jmp	_ZN2v88internal9Assembler10cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	.cfi_endproc
.LFE24696:
	.size	_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE, .-_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE
	.section	.text._ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_7OperandE:
.LFB24697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movl	%ecx, -40(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L957
	subq	$8, %rsp
	movl	%ecx, %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rdx, %r8
	pushq	$128
	movl	%esi, %edx
	movl	$44, %esi
	pushq	$1
	pushq	$3
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L961
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L957:
	.cfi_restore_state
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L961
	movl	-40(%rbp), %ecx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler10cvttsd2siqENS0_8RegisterENS0_7OperandE@PLT
.L961:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24697:
	.size	_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler10Cvttsd2siqENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl
	.type	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl, @function
_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl:
.LFB24703:
	.cfi_startproc
	endbr64
	testq	%rdx, %rdx
	je	.L967
	movq	%rdx, %rax
	shrq	$32, %rax
	je	.L968
	movl	$2147483648, %eax
	movl	$4294967295, %ecx
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L969
	movl	$8, %r8d
	movl	$19, %ecx
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	.p2align 4,,10
	.p2align 3
.L968:
	movl	%edx, %edx
	movl	$4, %ecx
	movabsq	$81604378624, %rax
	orq	%rax, %rdx
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	.p2align 4,,10
	.p2align 3
.L967:
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$4, %r8d
	movl	$51, %esi
	jmp	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L969:
	movl	%edx, %edx
	movl	$8, %ecx
	movabsq	$81604378624, %rax
	orq	%rax, %rdx
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	.cfi_endproc
.LFE24703:
	.size	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl, .-_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl
	.section	.text._ZN2v88internal14TurboAssembler21CallRuntimeWithCEntryENS0_7Runtime10FunctionIdENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler21CallRuntimeWithCEntryENS0_7Runtime10FunctionIdENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler21CallRuntimeWithCEntryENS0_7Runtime10FunctionIdENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler21CallRuntimeWithCEntryENS0_7Runtime10FunctionIdENS0_8RegisterE:
.LFB24662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movl	%esi, %edi
	subq	$8, %rsp
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%r12, %rdi
	movsbq	24(%rax), %rdx
	movq	%rax, %r14
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl
	movq	%r14, %rdi
	call	_ZN2v88internal17ExternalReference6CreateEPKNS0_7Runtime8FunctionE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE
	movq	(%r12), %rax
	leaq	_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE(%rip), %rcx
	movq	40(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L971
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	%r13d, %edx
	call	*56(%rax)
	addq	$8, %rsp
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L971:
	.cfi_restore_state
	addq	$8, %rsp
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.cfi_endproc
.LFE24662:
	.size	_ZN2v88internal14TurboAssembler21CallRuntimeWithCEntryENS0_7Runtime10FunctionIdENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler21CallRuntimeWithCEntryENS0_7Runtime10FunctionIdENS0_8RegisterE
	.section	.rodata._ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"f->nargs < 0 || f->nargs == num_arguments"
	.section	.text._ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE
	.type	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE, @function
_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE:
.LFB24663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movsbl	24(%rsi), %eax
	testb	%al, %al
	jns	.L984
.L974:
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movslq	%edx, %rdx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl
	movq	%rbx, %rdi
	call	_ZN2v88internal17ExternalReference6CreateEPKNS0_7Runtime8FunctionE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE
	movsbl	25(%rbx), %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	512(%r12), %rdi
	movl	%r13d, %edx
	call	_ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	cmpb	$0, 181(%r12)
	movq	%rax, %r13
	jne	.L985
.L975:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
.L973:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L986
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L985:
	.cfi_restore_state
	movq	512(%r12), %rax
	leaq	-68(%rbp), %rdx
	movq	%r13, %rsi
	movl	$-1, -68(%rbp)
	leaq	41184(%rax), %rdi
	call	_ZNK2v88internal8Builtins15IsBuiltinHandleENS0_6HandleINS0_10HeapObjectEEEPi@PLT
	testb	%al, %al
	je	.L975
	movl	-68(%rbp), %r13d
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi@PLT
	cmpl	$-1, %r13d
	je	.L987
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %ebx
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	leaq	-64(%rbp), %rdi
	movl	%r13d, %esi
	movl	%ebx, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$10, %ecx
	movq	%rax, %rdx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L984:
	cmpl	%edx, %eax
	je	.L974
	leaq	.LC6(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L987:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L986:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24663:
	.size	_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE, .-_ZN2v88internal14MacroAssembler11CallRuntimeEPKNS0_7Runtime8FunctionEiNS0_14SaveFPRegsModeE
	.section	.text._ZN2v88internal14TurboAssembler3SetENS0_7OperandEl,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler3SetENS0_7OperandEl
	.type	_ZN2v88internal14TurboAssembler3SetENS0_7OperandEl, @function
_ZN2v88internal14TurboAssembler3SetENS0_7OperandEl:
.LFB24704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%rsi, -64(%rbp)
	movl	%edx, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$-2147483648, %rcx
	jge	.L997
.L989:
	movq	%rcx, %rdx
	movl	$10, %esi
	movl	$19, %ecx
	movq	%r12, %rdi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L992:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	movq	-64(%rbp), %rsi
	movl	-56(%rbp), %edx
	jne	.L996
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %ecx
	addq	$56, %rsp
	movq	%r12, %rdi
	movl	$8, %r8d
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	.p2align 4,,10
	.p2align 3
.L997:
	.cfi_restore_state
	cmpq	$2147483647, %rcx
	jg	.L990
	movl	%edx, %r9d
	movl	%ecx, %ecx
	movabsq	$81604378624, %rdx
	orq	%rdx, %rcx
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L996
	addq	$56, %rsp
	movl	$8, %r8d
	movl	%r9d, %edx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
	.p2align 4,,10
	.p2align 3
.L990:
	.cfi_restore_state
	movq	%rcx, %rax
	shrq	$32, %rax
	jne	.L989
	movl	%ecx, %edx
	movl	$10, %esi
	movabsq	$81604378624, %rcx
	orq	%rcx, %rdx
	movl	$4, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L992
.L996:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24704:
	.size	_ZN2v88internal14TurboAssembler3SetENS0_7OperandEl, .-_ZN2v88internal14TurboAssembler3SetENS0_7OperandEl
	.section	.text._ZN2v88internal14TurboAssembler14GetSmiConstantENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler14GetSmiConstantENS0_3SmiE
	.type	_ZN2v88internal14TurboAssembler14GetSmiConstantENS0_3SmiE, @function
_ZN2v88internal14TurboAssembler14GetSmiConstantENS0_3SmiE:
.LFB24705:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rax
	shrq	$32, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L999
	movl	$4, %r8d
	movl	$10, %ecx
	movl	$10, %edx
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	$10, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L999:
	.cfi_restore_state
	movq	%rsi, %rdx
	movl	$8, %r8d
	movl	$19, %ecx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	$10, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24705:
	.size	_ZN2v88internal14TurboAssembler14GetSmiConstantENS0_3SmiE, .-_ZN2v88internal14TurboAssembler14GetSmiConstantENS0_3SmiE
	.section	.text._ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_3SmiE
	.type	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_3SmiE, @function
_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_3SmiE:
.LFB24706:
	.cfi_startproc
	endbr64
	movq	%rdx, %rax
	shrq	$32, %rax
	jne	.L1003
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$4, %r8d
	movl	$51, %esi
	jmp	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L1003:
	movl	$8, %r8d
	movl	$19, %ecx
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	.cfi_endproc
.LFE24706:
	.size	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_3SmiE, .-_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_3SmiE
	.section	.text._ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE
	.type	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE, @function
_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE:
.LFB24707:
	.cfi_startproc
	endbr64
	cmpb	$0, 528(%rdi)
	je	.L1005
	cmpb	$0, 180(%rdi)
	jne	.L1006
.L1005:
	movl	$8, %r8d
	movl	$7, %ecx
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	.p2align 4,,10
	.p2align 3
.L1006:
	jmp	_ZN2v88internal18TurboAssemblerBase29IndirectLoadExternalReferenceENS0_8RegisterENS0_17ExternalReferenceE@PLT
	.cfi_endproc
.LFE24707:
	.size	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE, .-_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_17ExternalReferenceE
	.section	.text._ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_
	.type	_ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_, @function
_ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_:
.LFB24708:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	cmpl	%edx, %esi
	je	.L1008
	movl	$8, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
.L1008:
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	movl	$8, %r8d
	popq	%r13
	movl	$4, %ecx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movabsq	$81604378656, %rdx
	jmp	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	.cfi_endproc
.LFE24708:
	.size	_ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_, .-_ZN2v88internal14MacroAssembler6SmiTagENS0_8RegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterES2_
	.type	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterES2_, @function
_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterES2_:
.LFB24709:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	cmpl	%edx, %esi
	je	.L1011
	movl	$8, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
.L1011:
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%r12
	movl	$8, %r8d
	popq	%r13
	movl	$7, %ecx
	popq	%rbp
	.cfi_def_cfa 7, 8
	movabsq	$81604378656, %rdx
	jmp	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	.cfi_endproc
.LFE24709:
	.size	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterES2_, .-_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterENS0_7OperandE:
.LFB24710:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	leaq	-48(%rbp), %rdi
	.cfi_offset 12, -32
	movl	%esi, %r12d
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movl	$4, %ecx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ES1_i@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movl	%r12d, %esi
	movl	$4, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r12d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler7movsxlqENS0_8RegisterES2_@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1016
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1016:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24710:
	.size	_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler8SmiUntagENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14MacroAssembler3CmpENS0_8RegisterENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler3CmpENS0_8RegisterENS0_3SmiE
	.type	_ZN2v88internal14MacroAssembler3CmpENS0_8RegisterENS0_3SmiE, @function
_ZN2v88internal14MacroAssembler3CmpENS0_8RegisterENS0_3SmiE:
.LFB24713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rax
	shrq	$32, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	jne	.L1018
	popq	%r12
	movl	$8, %ecx
	popq	%r13
	movl	%esi, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L1018:
	.cfi_restore_state
	movl	$8, %r8d
	movl	$19, %ecx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$10, %ecx
	popq	%r12
	movl	$8, %r8d
	popq	%r13
	movl	$59, %esi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	.cfi_endproc
.LFE24713:
	.size	_ZN2v88internal14MacroAssembler3CmpENS0_8RegisterENS0_3SmiE, .-_ZN2v88internal14MacroAssembler3CmpENS0_8RegisterENS0_3SmiE
	.section	.text._ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_3SmiE
	.type	_ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_3SmiE, @function
_ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_3SmiE:
.LFB24717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%rsi, -64(%rbp)
	movl	%edx, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rcx, %rax
	shrq	$32, %rax
	jne	.L1021
	movl	$4, %r8d
	movl	$10, %ecx
	movl	$10, %edx
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
.L1022:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	movq	-64(%rbp), %rcx
	movl	-56(%rbp), %r8d
	jne	.L1026
	addq	$56, %rsp
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$10, %edx
	popq	%r12
	movl	$57, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	.p2align 4,,10
	.p2align 3
.L1021:
	.cfi_restore_state
	movq	%rcx, %rdx
	movl	$8, %r8d
	movl	$19, %ecx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	jmp	.L1022
.L1026:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24717:
	.size	_ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_3SmiE, .-_ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_3SmiE
	.section	.text._ZN2v88internal14TurboAssembler8CheckSmiENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler8CheckSmiENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler8CheckSmiENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler8CheckSmiENS0_8RegisterE:
.LFB24718:
	.cfi_startproc
	endbr64
	movabsq	$81604378625, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	movl	$4, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24718:
	.size	_ZN2v88internal14TurboAssembler8CheckSmiENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler8CheckSmiENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler8CheckSmiENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler8CheckSmiENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler8CheckSmiENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler8CheckSmiENS0_7OperandE:
.LFB24719:
	.cfi_startproc
	endbr64
	movabsq	$81604378625, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_9ImmediateE@PLT
	movl	$4, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24719:
	.size	_ZN2v88internal14TurboAssembler8CheckSmiENS0_7OperandE, .-_ZN2v88internal14TurboAssembler8CheckSmiENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE
	.type	_ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE, @function
_ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE:
.LFB24720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	movabsq	$81604378625, %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	addq	$8, %rsp
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	.cfi_endproc
.LFE24720:
	.size	_ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE, .-_ZN2v88internal14TurboAssembler9JumpIfSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE
	.section	.text._ZN2v88internal14MacroAssembler12JumpIfNotSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler12JumpIfNotSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE
	.type	_ZN2v88internal14MacroAssembler12JumpIfNotSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE, @function
_ZN2v88internal14MacroAssembler12JumpIfNotSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE:
.LFB24721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	movabsq	$81604378625, %rdx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	addq	$8, %rsp
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	.cfi_endproc
.LFE24721:
	.size	_ZN2v88internal14MacroAssembler12JumpIfNotSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE, .-_ZN2v88internal14MacroAssembler12JumpIfNotSmiENS0_8RegisterEPNS0_5LabelENS3_8DistanceE
	.section	.text._ZN2v88internal14MacroAssembler12JumpIfNotSmiENS0_7OperandEPNS0_5LabelENS3_8DistanceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler12JumpIfNotSmiENS0_7OperandEPNS0_5LabelENS3_8DistanceE
	.type	_ZN2v88internal14MacroAssembler12JumpIfNotSmiENS0_7OperandEPNS0_5LabelENS3_8DistanceE, @function
_ZN2v88internal14MacroAssembler12JumpIfNotSmiENS0_7OperandEPNS0_5LabelENS3_8DistanceE:
.LFB24722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	movabsq	$81604378625, %rcx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_9ImmediateE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1039
	addq	$56, %rsp
	movl	%r14d, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L1039:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24722:
	.size	_ZN2v88internal14MacroAssembler12JumpIfNotSmiENS0_7OperandEPNS0_5LabelENS3_8DistanceE, .-_ZN2v88internal14MacroAssembler12JumpIfNotSmiENS0_7OperandEPNS0_5LabelENS3_8DistanceE
	.section	.text._ZN2v88internal14MacroAssembler14SmiAddConstantENS0_7OperandENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler14SmiAddConstantENS0_7OperandENS0_3SmiE
	.type	_ZN2v88internal14MacroAssembler14SmiAddConstantENS0_7OperandENS0_3SmiE, @function
_ZN2v88internal14MacroAssembler14SmiAddConstantENS0_7OperandENS0_3SmiE:
.LFB24723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rcx, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	sarq	$32, %rbx
	jne	.L1047
.L1040:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1048
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1047:
	.cfi_restore_state
	movq	%rdi, %r12
	movl	$4, %ecx
	leaq	-60(%rbp), %rdi
	movl	%ebx, %ebx
	call	_ZN2v88internal7OperandC1ES1_i@PLT
	movl	-52(%rbp), %r10d
	movq	-60(%rbp), %rdx
	xorl	%esi, %esi
	movabsq	$81604378624, %r8
	movl	$4, %r9d
	movq	%r12, %rdi
	orq	%rbx, %r8
	movq	%r10, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L1040
.L1048:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24723:
	.size	_ZN2v88internal14MacroAssembler14SmiAddConstantENS0_7OperandENS0_3SmiE, .-_ZN2v88internal14MacroAssembler14SmiAddConstantENS0_7OperandENS0_3SmiE
	.section	.text._ZN2v88internal14MacroAssembler10SmiToIndexENS0_8RegisterES2_i,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler10SmiToIndexENS0_8RegisterES2_i
	.type	_ZN2v88internal14MacroAssembler10SmiToIndexENS0_8RegisterES2_i, @function
_ZN2v88internal14MacroAssembler10SmiToIndexENS0_8RegisterES2_i:
.LFB24724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%ecx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	cmpl	%edx, %esi
	je	.L1050
	movl	$8, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
.L1050:
	cmpl	$31, %r12d
	jg	.L1051
	movl	$32, %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	movl	$8, %r8d
	movabsq	$81604378624, %rax
	subl	%r12d, %edx
	movl	$7, %ecx
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1051:
	.cfi_restore_state
	leal	-32(%r12), %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	movl	$8, %r8d
	movabsq	$81604378624, %r12
	movl	$4, %ecx
	orq	%r12, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24724:
	.size	_ZN2v88internal14MacroAssembler10SmiToIndexENS0_8RegisterES2_i, .-_ZN2v88internal14MacroAssembler10SmiToIndexENS0_8RegisterES2_i
	.section	.text._ZN2v88internal14TurboAssembler4PushENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4PushENS0_3SmiE
	.type	_ZN2v88internal14TurboAssembler4PushENS0_3SmiE, @function
_ZN2v88internal14TurboAssembler4PushENS0_3SmiE:
.LFB24725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$-2147483648, %rsi
	jl	.L1055
	cmpq	$2147483647, %rsi
	jle	.L1063
.L1055:
	bsrq	%r12, %rdx
	xorl	%eax, %eax
	movl	$63, %r14d
	xorq	$63, %rdx
	rep bsfq	%r12, %rax
	subl	%edx, %r14d
	shrl	$3, %eax
	shrl	$3, %r14d
	cmpl	%eax, %r14d
	je	.L1064
	movq	%r12, %rax
	shrq	$32, %rax
	jne	.L1058
	movl	$4, %r8d
	movl	$10, %ecx
	movl	$10, %edx
	movq	%r13, %rdi
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
.L1059:
	movl	$10, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L1054:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1065
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1063:
	.cfi_restore_state
	movabsq	$81604378624, %r12
	movl	%esi, %esi
	orq	%r12, %rsi
	call	_ZN2v88internal9Assembler5pushqENS0_9ImmediateE@PLT
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	%r12, %rdx
	movl	$19, %ecx
	movl	$10, %esi
	movq	%r13, %rdi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1064:
	movabsq	$81604378624, %rbx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88internal9Assembler5pushqENS0_9ImmediateE@PLT
	leal	0(,%r14,8), %ecx
	leaq	-52(%rbp), %rdi
	movl	%r14d, %edx
	sarq	%cl, %r12
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movsbl	%r12b, %r12d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	orq	%rbx, %r12
	movq	-52(%rbp), %rsi
	movl	-44(%rbp), %edx
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler4movbENS0_7OperandENS0_9ImmediateE@PLT
	jmp	.L1054
.L1065:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24725:
	.size	_ZN2v88internal14TurboAssembler4PushENS0_3SmiE, .-_ZN2v88internal14TurboAssembler4PushENS0_3SmiE
	.section	.text._ZN2v88internal14TurboAssembler4MoveENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterES2_
	.type	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterES2_, @function
_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterES2_:
.LFB24726:
	.cfi_startproc
	endbr64
	cmpl	%edx, %esi
	jne	.L1068
	ret
	.p2align 4,,10
	.p2align 3
.L1068:
	movl	$8, %ecx
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	.cfi_endproc
.LFE24726:
	.size	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterES2_, .-_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler8MovePairENS0_8RegisterES2_S2_S2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler8MovePairENS0_8RegisterES2_S2_S2_
	.type	_ZN2v88internal14TurboAssembler8MovePairENS0_8RegisterES2_S2_S2_, @function
_ZN2v88internal14TurboAssembler8MovePairENS0_8RegisterES2_S2_S2_:
.LFB24727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	subq	$16, %rsp
	cmpl	%r8d, %esi
	je	.L1070
	movl	%r8d, %r13d
	cmpl	%edx, %esi
	jne	.L1079
	cmpl	%r14d, %r13d
	jne	.L1080
.L1069:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1080:
	.cfi_restore_state
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r14d, %esi
.L1078:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L1079:
	.cfi_restore_state
	movl	$8, %ecx
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-40(%rbp), %rdi
	cmpl	%r14d, %r13d
	je	.L1069
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1070:
	cmpl	%edx, %ecx
	je	.L1074
	cmpl	%ecx, %esi
	jne	.L1081
	cmpl	%r15d, %r12d
	je	.L1069
.L1082:
	movl	$8, %ecx
	movl	%r15d, %edx
	movl	%r12d, %esi
	jmp	.L1078
	.p2align 4,,10
	.p2align 3
.L1081:
	movl	%esi, %edx
	movl	$8, %ecx
	movl	%r14d, %esi
	movq	%rdi, -40(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-40(%rbp), %rdi
	cmpl	%r15d, %r12d
	je	.L1069
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1074:
	addq	$16, %rsp
	movl	%r14d, %edx
	movl	$8, %ecx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9emit_xchgENS0_8RegisterES2_i@PLT
	.cfi_endproc
.LFE24727:
	.size	_ZN2v88internal14TurboAssembler8MovePairENS0_8RegisterES2_S2_S2_, .-_ZN2v88internal14TurboAssembler8MovePairENS0_8RegisterES2_S2_S2_
	.section	.text._ZN2v88internal14TurboAssembler23CallEphemeronKeyBarrierENS0_8RegisterES2_NS0_14SaveFPRegsModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler23CallEphemeronKeyBarrierENS0_8RegisterES2_NS0_14SaveFPRegsModeE
	.type	_ZN2v88internal14TurboAssembler23CallEphemeronKeyBarrierENS0_8RegisterES2_NS0_14SaveFPRegsModeE, @function
_ZN2v88internal14TurboAssembler23CallEphemeronKeyBarrierENS0_8RegisterES2_NS0_14SaveFPRegsModeE:
.LFB24652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movl	1296+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%ebx, %eax
	andl	$1, %eax
	movl	%eax, -104(%rbp)
	jne	.L1223
.L1084:
	movl	%ebx, %eax
	shrl	%eax
	andl	$1, %eax
	movl	%eax, -108(%rbp)
	jne	.L1224
.L1085:
	movl	%ebx, %eax
	shrl	$2, %eax
	andl	$1, %eax
	movl	%eax, -112(%rbp)
	jne	.L1225
.L1086:
	movl	%ebx, %eax
	shrl	$3, %eax
	andl	$1, %eax
	movl	%eax, -116(%rbp)
	jne	.L1226
.L1087:
	movl	%ebx, %eax
	shrl	$4, %eax
	andl	$1, %eax
	movl	%eax, -120(%rbp)
	jne	.L1227
.L1088:
	movl	%ebx, %eax
	shrl	$5, %eax
	andl	$1, %eax
	movl	%eax, -124(%rbp)
	jne	.L1228
.L1089:
	movl	%ebx, %eax
	shrl	$6, %eax
	andl	$1, %eax
	movl	%eax, -128(%rbp)
	jne	.L1229
.L1090:
	movl	%ebx, %eax
	shrl	$7, %eax
	andl	$1, %eax
	movl	%eax, -132(%rbp)
	jne	.L1230
.L1091:
	movl	%ebx, %eax
	shrl	$8, %eax
	andl	$1, %eax
	movl	%eax, -136(%rbp)
	jne	.L1231
.L1092:
	movl	%ebx, %eax
	shrl	$9, %eax
	andl	$1, %eax
	movl	%eax, -140(%rbp)
	jne	.L1232
.L1093:
	movl	%ebx, %eax
	shrl	$10, %eax
	andl	$1, %eax
	movl	%eax, -144(%rbp)
	jne	.L1233
.L1094:
	movl	%ebx, %eax
	shrl	$11, %eax
	andl	$1, %eax
	movl	%eax, -148(%rbp)
	jne	.L1234
.L1095:
	movl	%ebx, %eax
	shrl	$12, %eax
	andl	$1, %eax
	movl	%eax, -152(%rbp)
	jne	.L1235
.L1096:
	movl	%ebx, %eax
	shrl	$13, %eax
	andl	$1, %eax
	movl	%eax, -156(%rbp)
	jne	.L1236
.L1097:
	movl	%ebx, %eax
	shrl	$14, %eax
	andl	$1, %eax
	movl	%eax, -160(%rbp)
	jne	.L1237
.L1098:
	shrl	$15, %ebx
	andl	$1, %ebx
	jne	.L1238
.L1099:
	movq	1304+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movl	%r14d, %edx
	movl	%r15d, %r8d
	movq	%r12, %rdi
	movl	8(%rax), %r9d
	movl	(%rax), %ecx
	movl	4(%rax), %esi
	movl	%r9d, -100(%rbp)
	call	_ZN2v88internal14TurboAssembler8MovePairENS0_8RegisterES2_S2_S2_
	movq	%r13, %rdx
	movl	-100(%rbp), %r9d
	salq	$32, %rdx
	testl	%r13d, %r13d
	je	.L1239
	movl	$8, %r8d
	movl	$19, %ecx
	movl	%r9d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L1101:
	movq	512(%r12), %rax
	movl	$1, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	cmpb	$0, 181(%r12)
	movq	%rax, %r13
	jne	.L1240
.L1102:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
.L1105:
	testl	%ebx, %ebx
	jne	.L1241
.L1106:
	movl	-160(%rbp), %eax
	testl	%eax, %eax
	jne	.L1242
.L1107:
	movl	-156(%rbp), %eax
	testl	%eax, %eax
	jne	.L1243
.L1108:
	movl	-152(%rbp), %r15d
	testl	%r15d, %r15d
	jne	.L1244
.L1109:
	movl	-148(%rbp), %r14d
	testl	%r14d, %r14d
	jne	.L1245
.L1110:
	movl	-144(%rbp), %r13d
	testl	%r13d, %r13d
	jne	.L1246
.L1111:
	movl	-140(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L1247
.L1112:
	movl	-136(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L1248
.L1113:
	movl	-132(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L1249
.L1114:
	movl	-128(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L1250
.L1115:
	movl	-124(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L1251
.L1116:
	movl	-120(%rbp), %edi
	testl	%edi, %edi
	jne	.L1252
.L1117:
	movl	-116(%rbp), %esi
	testl	%esi, %esi
	jne	.L1253
.L1118:
	movl	-112(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L1254
.L1119:
	movl	-108(%rbp), %edx
	testl	%edx, %edx
	jne	.L1255
.L1120:
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	jne	.L1256
.L1083:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1257
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1240:
	.cfi_restore_state
	movq	512(%r12), %rax
	leaq	-84(%rbp), %rdx
	movq	%r13, %rsi
	movl	$-1, -84(%rbp)
	leaq	41184(%rax), %rdi
	call	_ZNK2v88internal8Builtins15IsBuiltinHandleENS0_6HandleINS0_10HeapObjectEEEPi@PLT
	testb	%al, %al
	je	.L1102
	movl	-84(%rbp), %r13d
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi@PLT
	cmpl	$-1, %r13d
	je	.L1258
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %r14d
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	leaq	-80(%rbp), %rdi
	movl	%r13d, %esi
	movl	%r14d, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$10, %ecx
	movq	%rax, %rdx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1239:
	movl	$4, %r8d
	movl	%r9d, %ecx
	movl	%r9d, %edx
	movq	%r12, %rdi
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1238:
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1237:
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1236:
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1235:
	movl	$12, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1234:
	movl	$11, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1233:
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1232:
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1231:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L1230:
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1229:
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1228:
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1227:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1226:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1225:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1224:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1085
	.p2align 4,,10
	.p2align 3
.L1223:
	xorl	%esi, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1256:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1255:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1254:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1119
	.p2align 4,,10
	.p2align 3
.L1253:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1252:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1251:
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1250:
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1249:
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1248:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1247:
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1246:
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1245:
	movl	$11, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1244:
	movl	$12, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1243:
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1242:
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1241:
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1258:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1257:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24652:
	.size	_ZN2v88internal14TurboAssembler23CallEphemeronKeyBarrierENS0_8RegisterES2_NS0_14SaveFPRegsModeE, .-_ZN2v88internal14TurboAssembler23CallEphemeronKeyBarrierENS0_8RegisterES2_NS0_14SaveFPRegsModeE
	.section	.text._ZN2v88internal14TurboAssembler10MoveNumberENS0_8RegisterEd,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler10MoveNumberENS0_8RegisterEd
	.type	_ZN2v88internal14TurboAssembler10MoveNumberENS0_8RegisterEd, @function
_ZN2v88internal14TurboAssembler10MoveNumberENS0_8RegisterEd:
.LFB24728:
	.cfi_startproc
	endbr64
	comisd	.LC7(%rip), %xmm0
	jb	.L1260
	movsd	.LC8(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L1260
	movabsq	$-9223372036854775808, %rax
	movq	%xmm0, %rcx
	cmpq	%rax, %rcx
	je	.L1260
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L1260
	jne	.L1260
	movq	%rax, %rdx
	salq	$32, %rdx
	testl	%eax, %eax
	jne	.L1264
	movl	%esi, %ecx
	movl	%esi, %edx
	movl	$4, %r8d
	movl	$51, %esi
	jmp	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L1260:
	jmp	_ZN2v88internal9Assembler16movq_heap_numberENS0_8RegisterEd@PLT
	.p2align 4,,10
	.p2align 3
.L1264:
	movl	$8, %r8d
	movl	$19, %ecx
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	.cfi_endproc
.LFE24728:
	.size	_ZN2v88internal14TurboAssembler10MoveNumberENS0_8RegisterEd, .-_ZN2v88internal14TurboAssembler10MoveNumberENS0_8RegisterEd
	.globl	__popcountdi2
	.section	.text._ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEj,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEj
	.type	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEj, @function
_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEj:
.LFB24729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testl	%edx, %edx
	je	.L1290
	movl	%edx, %ebx
	bsrl	%edx, %r15d
	xorl	%r14d, %r14d
	movq	%rbx, %rdi
	rep bsfl	%edx, %r14d
	xorl	$31, %r15d
	call	__popcountdi2@PLT
	movl	%eax, %r8d
	leal	(%r14,%r15), %eax
	addl	%r8d, %eax
	cmpl	$32, %eax
	je	.L1291
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movq	%r13, %rdi
	movl	$4, %ecx
	movabsq	$81604378624, %rdx
	orq	%rbx, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movl	$10, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1282
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5vmovdENS0_11XMMRegisterENS0_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L1291:
	.cfi_restore_state
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L1292
	movl	$118, %r9d
	movl	%r12d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	$15, %r8d
	movl	$102, %ecx
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
.L1275:
	testl	%r14d, %r14d
	jne	.L1293
.L1276:
	testl	%r15d, %r15d
	jne	.L1294
.L1269:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1290:
	.cfi_restore_state
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1271
	leaq	-40(%rbp), %rsp
	movl	%esi, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	popq	%rbx
	movl	$87, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_S2_@PLT
	.p2align 4,,10
	.p2align 3
.L1282:
	.cfi_restore_state
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4movdENS0_11XMMRegisterENS0_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L1271:
	.cfi_restore_state
	leaq	-40(%rbp), %rsp
	movl	%esi, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterES2_@PLT
	.p2align 4,,10
	.p2align 3
.L1292:
	.cfi_restore_state
	pushq	$0
	movl	%r12d, %r8d
	movq	%r13, %rdi
	movl	$1, %r9d
	pushq	$1
	movl	%r12d, %ecx
	movl	%r12d, %edx
	movl	$118, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rdi
	popq	%r8
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1294:
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1280
	pushq	$0
	movl	_ZN2v88internalL4xmm2E(%rip), %edx
	movl	%r12d, %r8d
	movl	%r12d, %ecx
	pushq	$1
	movl	$1, %r9d
	movl	$114, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	movq	32(%r13), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 32(%r13)
	movb	%r15b, (%rax)
	popq	%rax
	popq	%rdx
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1293:
	addl	%r15d, %r14d
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1277
	pushq	$0
	movl	_ZN2v88internalL4xmm6E(%rip), %edx
	movl	%r12d, %ecx
	movl	$114, %esi
	pushq	$1
	movl	$1, %r9d
	movl	%r12d, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	movq	32(%r13), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 32(%r13)
	movb	%r14b, (%rax)
	popq	%rcx
	popq	%rsi
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1277:
	movzbl	%r14b, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5pslldENS0_11XMMRegisterEh@PLT
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1280:
	leaq	-40(%rbp), %rsp
	movl	%r15d, %edx
	movl	%r12d, %esi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5psrldENS0_11XMMRegisterEh@PLT
	.cfi_endproc
.LFE24729:
	.size	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEj, .-_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEj
	.section	.text._ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE
	.type	_ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE, @function
_ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE:
.LFB24702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1296
	pushq	$128
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r8d
	movl	$2, %r9d
	pushq	$1
	movl	%esi, %edx
	movl	$44, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rcx
	popq	%rsi
.L1297:
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	leaq	-64(%rbp), %r15
	movl	$9, %esi
	movq	%r12, %rdi
	movq	%r15, %rdx
	movl	$1, %ecx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %esi
	movl	$-553648128, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEj
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1298
	movl	%ebx, %r8d
	movl	$15, %ecx
	movl	$15, %edx
	movq	%r12, %rdi
	movl	$88, %esi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_S2_@PLT
.L1299:
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1300
	pushq	$128
	movl	%r13d, %edx
	movl	$44, %esi
	movq	%r12, %rdi
	pushq	$1
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r8d
	movl	$2, %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
.L1301:
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	testq	%r14, %r14
	movl	$1, %ecx
	movq	%r12, %rdi
	cmove	%r15, %r14
	movl	$8, %esi
	movq	%r14, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$8, %r8d
	movl	$19, %ecx
	movq	%r12, %rdi
	movabsq	$-9223372036854775808, %rdx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	$10, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$11, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1306
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1296:
	.cfi_restore_state
	call	_ZN2v88internal9Assembler10cvttss2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	jmp	.L1297
	.p2align 4,,10
	.p2align 3
.L1300:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10cvttss2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1298:
	movl	%ebx, %edx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5addssENS0_11XMMRegisterES2_@PLT
	jmp	.L1299
.L1306:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24702:
	.size	_ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE, .-_ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE
	.section	.text._ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE
	.type	_ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE, @function
_ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE:
.LFB24701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movq	$0, -112(%rbp)
	movl	%ecx, -72(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1308
	subq	$8, %rsp
	movl	%ecx, %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rdx, %r8
	pushq	$128
	movl	%esi, %edx
	movl	$44, %esi
	pushq	$1
	pushq	$2
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
.L1309:
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	leaq	-112(%rbp), %r15
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r15, %rdx
	movl	$9, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %ebx
	movl	$-553648128, %edx
	movq	%r12, %rdi
	movl	%ebx, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEj
	movl	-96(%rbp), %r9d
	movq	-104(%rbp), %r8
	movl	%r9d, -72(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1310
	movl	$15, %ecx
	movl	$15, %edx
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
.L1311:
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1312
	pushq	$128
	movl	%r13d, %edx
	movl	%ebx, %r8d
	movl	$44, %esi
	pushq	$1
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$2, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
.L1313:
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	testq	%r14, %r14
	movl	$1, %ecx
	movq	%r12, %rdi
	cmove	%r15, %r14
	movl	$8, %esi
	movq	%r14, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$8, %r8d
	movl	$19, %ecx
	movq	%r12, %rdi
	movabsq	$-9223372036854775808, %rdx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	$10, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$11, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1318
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1308:
	.cfi_restore_state
	movl	-72(%rbp), %ecx
	call	_ZN2v88internal9Assembler10cvttss2siqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1312:
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10cvttss2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1310:
	movl	-72(%rbp), %ecx
	movq	%r8, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5addssENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1311
.L1318:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24701:
	.size	_ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE, .-_ZN2v88internal14TurboAssembler10Cvttss2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE
	.section	.text._ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEm
	.type	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEm, @function
_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEm:
.LFB24730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L1338
	bsrq	%rdx, %rbx
	xorl	%r14d, %r14d
	movq	%rdx, %rdi
	movq	%rdx, %r12
	xorq	$63, %rbx
	rep bsfq	%rdx, %r14
	movl	%ebx, -52(%rbp)
	addl	%r14d, %ebx
	call	__popcountdi2@PLT
	addl	%eax, %ebx
	cmpl	$64, %ebx
	je	.L1339
	movq	%r12, %rax
	shrq	$32, %rax
	jne	.L1332
	leaq	-40(%rbp), %rsp
	movl	%r12d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEj
	.p2align 4,,10
	.p2align 3
.L1332:
	.cfi_restore_state
	movq	%r12, %rdx
	movl	$10, %esi
	movq	%r15, %rdi
	movl	$19, %ecx
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	$10, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1333
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5vmovqENS0_11XMMRegisterENS0_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L1338:
	.cfi_restore_state
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1321
	leaq	-40(%rbp), %rsp
	movl	%esi, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	popq	%rbx
	movl	$87, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	.p2align 4,,10
	.p2align 3
.L1321:
	.cfi_restore_state
	leaq	-40(%rbp), %rsp
	movl	%esi, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5xorpdENS0_11XMMRegisterES2_@PLT
	.p2align 4,,10
	.p2align 3
.L1333:
	.cfi_restore_state
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4movqENS0_11XMMRegisterENS0_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L1339:
	.cfi_restore_state
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1324
	pushq	$0
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movl	$1, %r9d
	pushq	$1
	movl	%r13d, %r8d
	movl	$118, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rdx
	popq	%rcx
.L1325:
	testl	%r14d, %r14d
	jne	.L1340
.L1326:
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	jne	.L1341
.L1319:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1324:
	.cfi_restore_state
	movl	$118, %r9d
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	movl	$15, %r8d
	movl	$102, %ecx
	call	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1340:
	addb	-52(%rbp), %r14b
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1327
	movl	_ZN2v88internalL4xmm6E(%rip), %edx
	movl	%r13d, %r8d
	movl	%r13d, %ecx
	movq	%r15, %rdi
	movl	$115, %esi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	movq	32(%r15), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 32(%r15)
	movb	%r14b, (%rax)
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1341:
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1330
	movl	_ZN2v88internalL4xmm2E(%rip), %edx
	movl	%r13d, %ecx
	movl	%r13d, %r8d
	movq	%r15, %rdi
	movl	$115, %esi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	movq	32(%r15), %rax
	movzbl	-52(%rbp), %ecx
	leaq	1(%rax), %rdx
	movq	%rdx, 32(%r15)
	movb	%cl, (%rax)
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1327:
	movzbl	%r14b, %edx
	movl	%r13d, %esi
	movq	%r15, %rdi
	call	_ZN2v88internal9Assembler5psllqENS0_11XMMRegisterEh@PLT
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1330:
	movl	-52(%rbp), %edx
	leaq	-40(%rbp), %rsp
	movl	%r13d, %esi
	movq	%r15, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5psrlqENS0_11XMMRegisterEh@PLT
	.cfi_endproc
.LFE24730:
	.size	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEm, .-_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEm
	.section	.text._ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE
	.type	_ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE, @function
_ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE:
.LFB24700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1343
	pushq	$128
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, %r8d
	movl	$3, %r9d
	pushq	$1
	movl	%esi, %edx
	movl	$44, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rdi
	popq	%r8
.L1344:
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	leaq	-64(%rbp), %r15
	movl	$9, %esi
	movq	%r12, %rdi
	movq	%r15, %rdx
	movl	$1, %ecx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %esi
	movq	%r12, %rdi
	movabsq	$-4332462841530417152, %rdx
	call	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEm
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1345
	pushq	$0
	movl	$15, %ecx
	movl	$88, %esi
	movl	%ebx, %r8d
	pushq	$1
	movl	$3, %r9d
	movl	$15, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rcx
	popq	%rsi
.L1346:
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1347
	pushq	$128
	movl	%r13d, %edx
	movl	$44, %esi
	movq	%r12, %rdi
	pushq	$1
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %r8d
	movl	$3, %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
.L1348:
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	testq	%r14, %r14
	movl	$1, %ecx
	movq	%r12, %rdi
	cmove	%r15, %r14
	movl	$8, %esi
	movq	%r14, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$8, %r8d
	movl	$19, %ecx
	movq	%r12, %rdi
	movabsq	$-9223372036854775808, %rdx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	$10, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$11, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1353
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1343:
	.cfi_restore_state
	call	_ZN2v88internal9Assembler10cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1347:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	jmp	.L1348
	.p2align 4,,10
	.p2align 3
.L1345:
	movl	%ebx, %edx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5addsdENS0_11XMMRegisterES2_@PLT
	jmp	.L1346
.L1353:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24700:
	.size	_ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE, .-_ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_11XMMRegisterEPNS0_5LabelE
	.section	.text._ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE
	.type	_ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE, @function
_ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE:
.LFB24699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdx, -116(%rbp)
	movl	%ecx, -108(%rbp)
	movq	$0, -124(%rbp)
	movl	%ecx, -72(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1355
	subq	$8, %rsp
	movl	%ecx, %r9d
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movq	%rdx, %r8
	pushq	$128
	movl	%esi, %edx
	movl	$44, %esi
	pushq	$1
	pushq	$3
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
.L1356:
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	leaq	-124(%rbp), %r15
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r15, %rdx
	movl	$9, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %ebx
	movq	%r12, %rdi
	movabsq	$-4332462841530417152, %rdx
	movl	%ebx, %esi
	call	_ZN2v88internal14TurboAssembler4MoveENS0_11XMMRegisterEm
	movl	-108(%rbp), %r9d
	movq	-116(%rbp), %r8
	movl	%r9d, -84(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1357
	subq	$8, %rsp
	movl	$15, %ecx
	movl	$15, %edx
	movq	%r12, %rdi
	pushq	$0
	movl	$88, %esi
	pushq	$1
	pushq	$3
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
.L1358:
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1359
	pushq	$128
	movl	%r13d, %edx
	movl	%ebx, %r8d
	movl	$44, %esi
	pushq	$1
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	$3, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	popq	%rax
	popq	%rdx
.L1360:
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	testq	%r14, %r14
	movl	$1, %ecx
	movq	%r12, %rdi
	cmove	%r15, %r14
	movl	$8, %esi
	movq	%r14, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$8, %r8d
	movl	$19, %ecx
	movq	%r12, %rdi
	movabsq	$-9223372036854775808, %rdx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	$10, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$11, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1365
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1355:
	.cfi_restore_state
	movl	-72(%rbp), %ecx
	call	_ZN2v88internal9Assembler10cvttsd2siqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L1359:
	movl	%ebx, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler10cvttsd2siqENS0_8RegisterENS0_11XMMRegisterE@PLT
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1357:
	movl	-84(%rbp), %ecx
	movq	%r8, %rdx
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5addsdENS0_11XMMRegisterENS0_7OperandE@PLT
	jmp	.L1358
.L1365:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24699:
	.size	_ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE, .-_ZN2v88internal14TurboAssembler10Cvttsd2uiqENS0_8RegisterENS0_7OperandEPNS0_5LabelE
	.section	.text._ZN2v88internal14MacroAssembler5AbspsENS0_11XMMRegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler5AbspsENS0_11XMMRegisterE
	.type	_ZN2v88internal14MacroAssembler5AbspsENS0_11XMMRegisterE, @function
_ZN2v88internal14MacroAssembler5AbspsENS0_11XMMRegisterE:
.LFB24732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17ExternalReference29address_of_float_abs_constantEv@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	cmpb	$0, 528(%r12)
	movq	%rax, -112(%rbp)
	je	.L1367
	cmpb	$0, 178(%r12)
	jne	.L1368
	cmpb	$0, 180(%r12)
	jne	.L1385
.L1367:
	movq	-112(%rbp), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	leaq	-52(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r14d, %esi
.L1382:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rax
	movq	%rax, -64(%rbp)
	movl	-44(%rbp), %eax
	movq	-64(%rbp), %r8
	movl	%eax, %r9d
	movl	%eax, -56(%rbp)
	movl	%r9d, -68(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L1386
	movl	-68(%rbp), %ecx
	movq	%r8, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5andpsENS0_11XMMRegisterENS0_7OperandE@PLT
.L1366:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1387
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1386:
	.cfi_restore_state
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movl	$84, %esi
	movq	%r12, %rdi
	movq	%r8, -52(%rbp)
	movl	%r9d, -44(%rbp)
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1385:
	movq	512(%r12), %rdi
	leaq	-112(%rbp), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movq	512(%r12), %rdi
	movq	%r15, %rsi
	testb	%al, %al
	je	.L1374
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L1383
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1368:
	movq	512(%r12), %rdi
	leaq	-112(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L1383
	cmpb	$0, 528(%r12)
	je	.L1367
	cmpb	$0, 180(%r12)
	je	.L1367
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1374:
	call	_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	leaq	-52(%rbp), %r15
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	movl	%eax, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rdx
	movl	-44(%rbp), %ecx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1383:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-52(%rbp), %rdi
	jmp	.L1382
.L1387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24732:
	.size	_ZN2v88internal14MacroAssembler5AbspsENS0_11XMMRegisterE, .-_ZN2v88internal14MacroAssembler5AbspsENS0_11XMMRegisterE
	.section	.text._ZN2v88internal14MacroAssembler5NegpsENS0_11XMMRegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler5NegpsENS0_11XMMRegisterE
	.type	_ZN2v88internal14MacroAssembler5NegpsENS0_11XMMRegisterE, @function
_ZN2v88internal14MacroAssembler5NegpsENS0_11XMMRegisterE:
.LFB24733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17ExternalReference29address_of_float_neg_constantEv@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	cmpb	$0, 528(%r12)
	movq	%rax, -112(%rbp)
	je	.L1389
	cmpb	$0, 178(%r12)
	jne	.L1390
	cmpb	$0, 180(%r12)
	jne	.L1407
.L1389:
	movq	-112(%rbp), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	leaq	-52(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r14d, %esi
.L1404:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rax
	movq	%rax, -64(%rbp)
	movl	-44(%rbp), %eax
	movq	-64(%rbp), %r8
	movl	%eax, %r9d
	movl	%eax, -56(%rbp)
	movl	%r9d, -68(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L1408
	movl	-68(%rbp), %ecx
	movq	%r8, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterENS0_7OperandE@PLT
.L1388:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1409
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1408:
	.cfi_restore_state
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movl	$87, %esi
	movq	%r12, %rdi
	movq	%r8, -52(%rbp)
	movl	%r9d, -44(%rbp)
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1388
	.p2align 4,,10
	.p2align 3
.L1407:
	movq	512(%r12), %rdi
	leaq	-112(%rbp), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movq	512(%r12), %rdi
	movq	%r15, %rsi
	testb	%al, %al
	je	.L1396
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L1405
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1390:
	movq	512(%r12), %rdi
	leaq	-112(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L1405
	cmpb	$0, 528(%r12)
	je	.L1389
	cmpb	$0, 180(%r12)
	je	.L1389
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1396:
	call	_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	leaq	-52(%rbp), %r15
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	movl	%eax, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rdx
	movl	-44(%rbp), %ecx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1405:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-52(%rbp), %rdi
	jmp	.L1404
.L1409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24733:
	.size	_ZN2v88internal14MacroAssembler5NegpsENS0_11XMMRegisterE, .-_ZN2v88internal14MacroAssembler5NegpsENS0_11XMMRegisterE
	.section	.text._ZN2v88internal14MacroAssembler5AbspdENS0_11XMMRegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler5AbspdENS0_11XMMRegisterE
	.type	_ZN2v88internal14MacroAssembler5AbspdENS0_11XMMRegisterE, @function
_ZN2v88internal14MacroAssembler5AbspdENS0_11XMMRegisterE:
.LFB24734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17ExternalReference30address_of_double_abs_constantEv@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	cmpb	$0, 528(%r12)
	movq	%rax, -112(%rbp)
	je	.L1411
	cmpb	$0, 178(%r12)
	jne	.L1412
	cmpb	$0, 180(%r12)
	jne	.L1429
.L1411:
	movq	-112(%rbp), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	leaq	-52(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r14d, %esi
.L1426:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rax
	movq	%rax, -64(%rbp)
	movl	-44(%rbp), %eax
	movq	-64(%rbp), %r8
	movl	%eax, %r9d
	movl	%eax, -56(%rbp)
	movl	%r9d, -68(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L1430
	movl	-68(%rbp), %ecx
	movq	%r8, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5andpsENS0_11XMMRegisterENS0_7OperandE@PLT
.L1410:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1431
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1430:
	.cfi_restore_state
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movl	$84, %esi
	movq	%r12, %rdi
	movq	%r8, -52(%rbp)
	movl	%r9d, -44(%rbp)
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1429:
	movq	512(%r12), %rdi
	leaq	-112(%rbp), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movq	512(%r12), %rdi
	movq	%r15, %rsi
	testb	%al, %al
	je	.L1418
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L1427
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1412:
	movq	512(%r12), %rdi
	leaq	-112(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L1427
	cmpb	$0, 528(%r12)
	je	.L1411
	cmpb	$0, 180(%r12)
	je	.L1411
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L1418:
	call	_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	leaq	-52(%rbp), %r15
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	movl	%eax, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rdx
	movl	-44(%rbp), %ecx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1427:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-52(%rbp), %rdi
	jmp	.L1426
.L1431:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24734:
	.size	_ZN2v88internal14MacroAssembler5AbspdENS0_11XMMRegisterE, .-_ZN2v88internal14MacroAssembler5AbspdENS0_11XMMRegisterE
	.section	.text._ZN2v88internal14MacroAssembler5NegpdENS0_11XMMRegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler5NegpdENS0_11XMMRegisterE
	.type	_ZN2v88internal14MacroAssembler5NegpdENS0_11XMMRegisterE, @function
_ZN2v88internal14MacroAssembler5NegpdENS0_11XMMRegisterE:
.LFB24735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17ExternalReference30address_of_double_neg_constantEv@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	cmpb	$0, 528(%r12)
	movq	%rax, -112(%rbp)
	je	.L1433
	cmpb	$0, 178(%r12)
	jne	.L1434
	cmpb	$0, 180(%r12)
	jne	.L1451
.L1433:
	movq	-112(%rbp), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	leaq	-52(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r14d, %esi
.L1448:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rax
	movq	%rax, -64(%rbp)
	movl	-44(%rbp), %eax
	movq	-64(%rbp), %r8
	movl	%eax, %r9d
	movl	%eax, -56(%rbp)
	movl	%r9d, -68(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L1452
	movl	-68(%rbp), %ecx
	movq	%r8, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5xorpsENS0_11XMMRegisterENS0_7OperandE@PLT
.L1432:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1453
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1452:
	.cfi_restore_state
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movl	$87, %esi
	movq	%r12, %rdi
	movq	%r8, -52(%rbp)
	movl	%r9d, -44(%rbp)
	call	_ZN2v88internal9Assembler3vpsEhNS0_11XMMRegisterES2_NS0_7OperandE@PLT
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	512(%r12), %rdi
	leaq	-112(%rbp), %r15
	movq	%r15, %rsi
	call	_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movq	512(%r12), %rdi
	movq	%r15, %rsi
	testb	%al, %al
	je	.L1440
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L1449
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1434:
	movq	512(%r12), %rdi
	leaq	-112(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L1449
	cmpb	$0, 528(%r12)
	je	.L1433
	cmpb	$0, 180(%r12)
	je	.L1433
	jmp	.L1451
	.p2align 4,,10
	.p2align 3
.L1440:
	call	_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	leaq	-52(%rbp), %r15
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	movl	%eax, %edx
	movq	%r15, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rdx
	movl	-44(%rbp), %ecx
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1449:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-52(%rbp), %rdi
	jmp	.L1448
.L1453:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24735:
	.size	_ZN2v88internal14MacroAssembler5NegpdENS0_11XMMRegisterE, .-_ZN2v88internal14MacroAssembler5NegpdENS0_11XMMRegisterE
	.section	.text._ZN2v88internal14MacroAssembler3CmpENS0_8RegisterENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler3CmpENS0_8RegisterENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal14MacroAssembler3CmpENS0_8RegisterENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal14MacroAssembler3CmpENS0_8RegisterENS0_6HandleINS0_6ObjectEEE:
.LFB24736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	(%rdx), %r9
	movq	%rdi, %r12
	testb	$1, %r9b
	jne	.L1455
	movq	%r9, %rax
	shrq	$32, %rax
	jne	.L1456
	popq	%r12
	movl	$8, %ecx
	popq	%r13
	movl	%esi, %edx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L1455:
	.cfi_restore_state
	cmpb	$0, 528(%rdi)
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	je	.L1458
	cmpb	$0, 180(%rdi)
	jne	.L1462
.L1458:
	movl	$8, %r8d
	movl	$3, %ecx
.L1461:
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L1459:
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	popq	%r12
	movl	$10, %ecx
	popq	%r13
	movl	$59, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	.p2align 4,,10
	.p2align 3
.L1456:
	.cfi_restore_state
	movl	$8, %r8d
	movq	%r9, %rdx
	movl	$19, %ecx
	movl	$10, %esi
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1462:
	call	_ZN2v88internal18TurboAssemblerBase20IndirectLoadConstantENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEE@PLT
	jmp	.L1459
	.cfi_endproc
.LFE24736:
	.size	_ZN2v88internal14MacroAssembler3CmpENS0_8RegisterENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal14MacroAssembler3CmpENS0_8RegisterENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_6HandleINS0_6ObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_6HandleINS0_6ObjectEEE
	.type	_ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_6HandleINS0_6ObjectEEE, @function
_ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_6HandleINS0_6ObjectEEE:
.LFB24737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	%rsi, -80(%rbp)
	movq	(%rcx), %r10
	movl	%edx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	$1, %r10b
	jne	.L1464
	movq	%r10, %rax
	movq	%rsi, -60(%rbp)
	shrq	$32, %rax
	movl	%edx, -52(%rbp)
	jne	.L1465
	movl	$4, %r8d
	movl	$10, %ecx
	movl	$10, %edx
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
.L1466:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	movq	-60(%rbp), %rcx
	movl	-52(%rbp), %r8d
	je	.L1470
.L1473:
	call	__stack_chk_fail@PLT
	.p2align 4,,10
	.p2align 3
.L1464:
	cmpb	$0, 528(%rdi)
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	je	.L1468
	cmpb	$0, 180(%rdi)
	jne	.L1474
.L1468:
	movq	%rcx, %rdx
	movl	$8, %r8d
	movl	$3, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L1469:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	movq	-80(%rbp), %rcx
	movl	-72(%rbp), %r8d
	jne	.L1473
.L1470:
	addq	$72, %rsp
	movq	%r12, %rdi
	movl	$8, %r9d
	movl	$10, %edx
	popq	%r12
	movl	$57, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	.p2align 4,,10
	.p2align 3
.L1465:
	.cfi_restore_state
	movl	$8, %r8d
	movq	%r10, %rdx
	movl	$19, %ecx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1474:
	movq	%rcx, %rdx
	call	_ZN2v88internal18TurboAssemblerBase20IndirectLoadConstantENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEE@PLT
	jmp	.L1469
	.cfi_endproc
.LFE24737:
	.size	_ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_6HandleINS0_6ObjectEEE, .-_ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_6HandleINS0_6ObjectEEE
	.section	.text._ZN2v88internal14MacroAssembler15JumpIfIsInRangeENS0_8RegisterEjjPNS0_5LabelENS3_8DistanceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler15JumpIfIsInRangeENS0_8RegisterEjjPNS0_5LabelENS3_8DistanceE
	.type	_ZN2v88internal14MacroAssembler15JumpIfIsInRangeENS0_8RegisterEjjPNS0_5LabelENS3_8DistanceE, @function
_ZN2v88internal14MacroAssembler15JumpIfIsInRangeENS0_8RegisterEjjPNS0_5LabelENS3_8DistanceE:
.LFB24738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r9d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L1476
	movl	%edx, %r13d
	leaq	-80(%rbp), %rdi
	negl	%edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movl	$4, %r8d
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	%ebx, %ecx
	movl	$4, %r8d
	movq	%r12, %rdi
	subl	%r13d, %ecx
	movl	$10, %edx
	movl	$7, %esi
	movabsq	$81604378624, %rbx
	orq	%rbx, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
.L1477:
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1480
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1476:
	.cfi_restore_state
	movabsq	$81604378624, %rbx
	movl	%ecx, %ecx
	movl	%esi, %edx
	movl	$4, %r8d
	orq	%rbx, %rcx
	movl	$7, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1477
.L1480:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24738:
	.size	_ZN2v88internal14MacroAssembler15JumpIfIsInRangeENS0_8RegisterEjjPNS0_5LabelENS3_8DistanceE, .-_ZN2v88internal14MacroAssembler15JumpIfIsInRangeENS0_8RegisterEjjPNS0_5LabelENS3_8DistanceE
	.section	.text._ZN2v88internal14TurboAssembler4PushENS0_6HandleINS0_10HeapObjectEEE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4PushENS0_6HandleINS0_10HeapObjectEEE
	.type	_ZN2v88internal14TurboAssembler4PushENS0_6HandleINS0_10HeapObjectEEE, @function
_ZN2v88internal14TurboAssembler4PushENS0_6HandleINS0_10HeapObjectEEE:
.LFB24739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	pushq	%r12
	.cfi_offset 12, -32
	cmpb	$0, 528(%rdi)
	movq	%rdi, %r12
	je	.L1482
	cmpb	$0, 180(%rdi)
	jne	.L1485
.L1482:
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$3, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L1485:
	.cfi_restore_state
	movl	%r13d, %esi
	call	_ZN2v88internal18TurboAssemblerBase20IndirectLoadConstantENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24739:
	.size	_ZN2v88internal14TurboAssembler4PushENS0_6HandleINS0_10HeapObjectEEE, .-_ZN2v88internal14TurboAssembler4PushENS0_6HandleINS0_10HeapObjectEEE
	.section	.text._ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE
	.type	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE, @function
_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE:
.LFB24740:
	.cfi_startproc
	endbr64
	cmpb	$0, 528(%rdi)
	je	.L1487
	cmpb	$0, 180(%rdi)
	jne	.L1488
.L1487:
	xorl	%r9d, %r9d
	movl	$8, %r8d
	movb	%cl, %r9b
	movq	%r9, %rcx
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	.p2align 4,,10
	.p2align 3
.L1488:
	jmp	_ZN2v88internal18TurboAssemblerBase20IndirectLoadConstantENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEE@PLT
	.cfi_endproc
.LFE24740:
	.size	_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE, .-_ZN2v88internal14TurboAssembler4MoveENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE
	.section	.text._ZN2v88internal14TurboAssembler4MoveENS0_7OperandENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4MoveENS0_7OperandENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE
	.type	_ZN2v88internal14TurboAssembler4MoveENS0_7OperandENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE, @function
_ZN2v88internal14TurboAssembler4MoveENS0_7OperandENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE:
.LFB24741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r9d
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%rsi, -64(%rbp)
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movl	%r9d, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 528(%rdi)
	je	.L1490
	cmpb	$0, 180(%rdi)
	jne	.L1495
.L1490:
	xorl	%ecx, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movb	%r8b, %cl
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L1491:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	movq	-64(%rbp), %rsi
	movl	-56(%rbp), %edx
	jne	.L1496
	addq	$48, %rsp
	movl	%r13d, %ecx
	movq	%r12, %rdi
	movl	$8, %r8d
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	.p2align 4,,10
	.p2align 3
.L1495:
	.cfi_restore_state
	movl	%r13d, %esi
	call	_ZN2v88internal18TurboAssemblerBase20IndirectLoadConstantENS0_8RegisterENS0_6HandleINS0_10HeapObjectEEE@PLT
	jmp	.L1491
.L1496:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24741:
	.size	_ZN2v88internal14TurboAssembler4MoveENS0_7OperandENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE, .-_ZN2v88internal14TurboAssembler4MoveENS0_7OperandENS0_6HandleINS0_10HeapObjectEEENS0_9RelocInfo4ModeE
	.section	.text._ZN2v88internal14TurboAssembler18MoveStringConstantENS0_8RegisterEPKNS0_18StringConstantBaseENS0_9RelocInfo4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler18MoveStringConstantENS0_8RegisterEPKNS0_18StringConstantBaseENS0_9RelocInfo4ModeE
	.type	_ZN2v88internal14TurboAssembler18MoveStringConstantENS0_8RegisterEPKNS0_18StringConstantBaseENS0_9RelocInfo4ModeE, @function
_ZN2v88internal14TurboAssembler18MoveStringConstantENS0_8RegisterEPKNS0_18StringConstantBaseENS0_9RelocInfo4ModeE:
.LFB24742:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal9Assembler11movq_stringENS0_8RegisterEPKNS0_18StringConstantBaseE@PLT
	.cfi_endproc
.LFE24742:
	.size	_ZN2v88internal14TurboAssembler18MoveStringConstantENS0_8RegisterEPKNS0_18StringConstantBaseENS0_9RelocInfo4ModeE, .-_ZN2v88internal14TurboAssembler18MoveStringConstantENS0_8RegisterEPKNS0_18StringConstantBaseENS0_9RelocInfo4ModeE
	.section	.text._ZN2v88internal14MacroAssembler4DropEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler4DropEi
	.type	_ZN2v88internal14MacroAssembler4DropEi, @function
_ZN2v88internal14MacroAssembler4DropEi:
.LFB24743:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	jg	.L1500
	ret
	.p2align 4,,10
	.p2align 3
.L1500:
	leal	0(,%rsi,8), %ecx
	movl	$8, %r8d
	movl	$4, %edx
	movabsq	$81604378624, %rsi
	orq	%rsi, %rcx
	xorl	%esi, %esi
	jmp	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	.cfi_endproc
.LFE24743:
	.size	_ZN2v88internal14MacroAssembler4DropEi, .-_ZN2v88internal14MacroAssembler4DropEi
	.section	.text._ZN2v88internal14MacroAssembler22DropUnderReturnAddressEiNS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler22DropUnderReturnAddressEiNS0_8RegisterE
	.type	_ZN2v88internal14MacroAssembler22DropUnderReturnAddressEiNS0_8RegisterE, @function
_ZN2v88internal14MacroAssembler22DropUnderReturnAddressEiNS0_8RegisterE:
.LFB24744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, %esi
	je	.L1507
	movl	%esi, %ebx
	movl	%edx, %esi
	movl	%edx, %r13d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	testl	%ebx, %ebx
	jg	.L1508
.L1504:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L1501:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1509
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1508:
	.cfi_restore_state
	movl	$8, %r8d
	movl	$4, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	leal	0(,%rbx,8), %ecx
	movabsq	$81604378624, %rbx
	orq	%rbx, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L1504
	.p2align 4,,10
	.p2align 3
.L1507:
	movl	_ZN2v88internalL3rspE(%rip), %esi
	leaq	-52(%rbp), %rdi
	xorl	%edx, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rsi
	movl	-44(%rbp), %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_7OperandE@PLT
	jmp	.L1501
.L1509:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24744:
	.size	_ZN2v88internal14MacroAssembler22DropUnderReturnAddressEiNS0_8RegisterE, .-_ZN2v88internal14MacroAssembler22DropUnderReturnAddressEiNS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler4PushENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE:
.LFB24745:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24745:
	.size	_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler4PushENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler4PushENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler4PushENS0_7OperandE:
.LFB24746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5pushqENS0_7OperandE@PLT
	.cfi_endproc
.LFE24746:
	.size	_ZN2v88internal14TurboAssembler4PushENS0_7OperandE, .-_ZN2v88internal14TurboAssembler4PushENS0_7OperandE
	.section	.text._ZN2v88internal14MacroAssembler8PushQuadENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler8PushQuadENS0_7OperandE
	.type	_ZN2v88internal14MacroAssembler8PushQuadENS0_7OperandE, @function
_ZN2v88internal14MacroAssembler8PushQuadENS0_7OperandE:
.LFB24747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5pushqENS0_7OperandE@PLT
	.cfi_endproc
.LFE24747:
	.size	_ZN2v88internal14MacroAssembler8PushQuadENS0_7OperandE, .-_ZN2v88internal14MacroAssembler8PushQuadENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE
	.type	_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE, @function
_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE:
.LFB24748:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal9Assembler5pushqENS0_9ImmediateE@PLT
	.cfi_endproc
.LFE24748:
	.size	_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE, .-_ZN2v88internal14TurboAssembler4PushENS0_9ImmediateE
	.section	.text._ZN2v88internal14MacroAssembler9PushImm32Ei,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler9PushImm32Ei
	.type	_ZN2v88internal14MacroAssembler9PushImm32Ei, @function
_ZN2v88internal14MacroAssembler9PushImm32Ei:
.LFB24749:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal9Assembler11pushq_imm32Ei@PLT
	.cfi_endproc
.LFE24749:
	.size	_ZN2v88internal14MacroAssembler9PushImm32Ei, .-_ZN2v88internal14MacroAssembler9PushImm32Ei
	.section	.text._ZN2v88internal14MacroAssembler3PopENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE
	.type	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE, @function
_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE:
.LFB24750:
	.cfi_startproc
	endbr64
	jmp	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24750:
	.size	_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE, .-_ZN2v88internal14MacroAssembler3PopENS0_8RegisterE
	.section	.text._ZN2v88internal14MacroAssembler3PopENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler3PopENS0_7OperandE
	.type	_ZN2v88internal14MacroAssembler3PopENS0_7OperandE, @function
_ZN2v88internal14MacroAssembler3PopENS0_7OperandE:
.LFB24751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4popqENS0_7OperandE@PLT
	.cfi_endproc
.LFE24751:
	.size	_ZN2v88internal14MacroAssembler3PopENS0_7OperandE, .-_ZN2v88internal14MacroAssembler3PopENS0_7OperandE
	.section	.text._ZN2v88internal14MacroAssembler7PopQuadENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler7PopQuadENS0_7OperandE
	.type	_ZN2v88internal14MacroAssembler7PopQuadENS0_7OperandE, @function
_ZN2v88internal14MacroAssembler7PopQuadENS0_7OperandE:
.LFB31401:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1524
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4popqENS0_7OperandE@PLT
.L1524:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE31401:
	.size	_ZN2v88internal14MacroAssembler7PopQuadENS0_7OperandE, .-_ZN2v88internal14MacroAssembler7PopQuadENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler4JumpENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4JumpENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler4JumpENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler4JumpENS0_7OperandE:
.LFB24754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3jmpENS0_7OperandE@PLT
	.cfi_endproc
.LFE24754:
	.size	_ZN2v88internal14TurboAssembler4JumpENS0_7OperandE, .-_ZN2v88internal14TurboAssembler4JumpENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler4JumpEmNS0_9RelocInfo4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4JumpEmNS0_9RelocInfo4ModeE
	.type	_ZN2v88internal14TurboAssembler4JumpEmNS0_9RelocInfo4ModeE, @function
_ZN2v88internal14TurboAssembler4JumpEmNS0_9RelocInfo4ModeE:
.LFB24755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$8, %r8d
	movb	%dl, %cl
	movq	%rsi, %rdx
	movl	$10, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24755:
	.size	_ZN2v88internal14TurboAssembler4JumpEmNS0_9RelocInfo4ModeE, .-_ZN2v88internal14TurboAssembler4JumpEmNS0_9RelocInfo4ModeE
	.section	.text._ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE
	.type	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE, @function
_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE:
.LFB24756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 181(%rdi)
	je	.L1530
	movq	512(%rdi), %rax
	leaq	-76(%rbp), %rdx
	movl	$-1, -76(%rbp)
	leaq	41184(%rax), %rdi
	call	_ZNK2v88internal8Builtins15IsBuiltinHandleENS0_6HandleINS0_10HeapObjectEEEPi@PLT
	testb	%al, %al
	jne	.L1545
.L1530:
	movsbl	%bl, %ecx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
.L1529:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1546
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1545:
	.cfi_restore_state
	movq	$0, -72(%rbp)
	cmpl	$16, %r13d
	je	.L1533
	cmpl	$17, %r13d
	je	.L1529
	movl	%r13d, %esi
	leaq	-72(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	xorl	$1, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L1533:
	movl	-76(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi@PLT
	cmpl	$-1, -76(%rbp)
	je	.L1547
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %ebx
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	movl	-76(%rbp), %esi
	leaq	-64(%rbp), %rdi
	movl	%ebx, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movl	$8, %r8d
	movl	$10, %ecx
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	leaq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L1529
	.p2align 4,,10
	.p2align 3
.L1547:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1546:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24756:
	.size	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE, .-_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE
	.section	.text._ZN2v88internal14MacroAssembler15TailCallRuntimeENS0_7Runtime10FunctionIdE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler15TailCallRuntimeENS0_7Runtime10FunctionIdE
	.type	_ZN2v88internal14MacroAssembler15TailCallRuntimeENS0_7Runtime10FunctionIdE, @function
_ZN2v88internal14MacroAssembler15TailCallRuntimeENS0_7Runtime10FunctionIdE:
.LFB24664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	%esi, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	movsbq	24(%rax), %rdx
	testb	%dl, %dl
	js	.L1549
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl
.L1549:
	movl	%r13d, %edi
	call	_ZN2v88internal17ExternalReference6CreateENS0_7Runtime10FunctionIdE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	512(%r12), %rdi
	xorl	%r8d, %r8d
	call	_ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	movq	%r12, %rdi
	movl	$16, %ecx
	popq	%r12
	movq	%rax, %rsi
	popq	%r13
	xorl	%edx, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE
	.cfi_endproc
.LFE24664:
	.size	_ZN2v88internal14MacroAssembler15TailCallRuntimeENS0_7Runtime10FunctionIdE, .-_ZN2v88internal14MacroAssembler15TailCallRuntimeENS0_7Runtime10FunctionIdE
	.section	.text._ZN2v88internal14MacroAssembler23JumpToInstructionStreamEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler23JumpToInstructionStreamEm
	.type	_ZN2v88internal14MacroAssembler23JumpToInstructionStreamEm, @function
_ZN2v88internal14MacroAssembler23JumpToInstructionStreamEm:
.LFB24757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	movl	$8, %r8d
	movl	$10, %esi
	movl	$10, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	_ZN2v88internalL26kOffHeapTrampolineRegisterE(%rip), %esi
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24757:
	.size	_ZN2v88internal14MacroAssembler23JumpToInstructionStreamEm, .-_ZN2v88internal14MacroAssembler23JumpToInstructionStreamEm
	.section	.text._ZN2v88internal14TurboAssembler4CallENS0_17ExternalReferenceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4CallENS0_17ExternalReferenceE
	.type	_ZN2v88internal14TurboAssembler4CallENS0_17ExternalReferenceE, @function
_ZN2v88internal14TurboAssembler4CallENS0_17ExternalReferenceE:
.LFB24758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 528(%rdi)
	movq	%rsi, -56(%rbp)
	je	.L1554
	cmpb	$0, 178(%rdi)
	jne	.L1555
	cmpb	$0, 180(%r12)
	jne	.L1567
.L1554:
	movq	-56(%rbp), %rdx
	movl	$8, %r8d
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L1559:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1568
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1555:
	.cfi_restore_state
	movq	512(%rdi), %rdi
	leaq	-56(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L1569
	cmpb	$0, 528(%r12)
	je	.L1554
	cmpb	$0, 180(%r12)
	je	.L1554
.L1567:
	movq	-56(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18TurboAssemblerBase29IndirectLoadExternalReferenceENS0_8RegisterENS0_17ExternalReferenceE@PLT
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1569:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-36(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-28(%rbp), %ecx
	movq	-36(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L1559
.L1568:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24758:
	.size	_ZN2v88internal14TurboAssembler4CallENS0_17ExternalReferenceE, .-_ZN2v88internal14TurboAssembler4CallENS0_17ExternalReferenceE
	.section	.text._ZN2v88internal14TurboAssembler4CallENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4CallENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler4CallENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler4CallENS0_7OperandE:
.LFB24759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	$8, 1+_ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L1571
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1576
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4callENS0_7OperandE@PLT
	.p2align 4,,10
	.p2align 3
.L1571:
	.cfi_restore_state
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movl	%edx, %ecx
	movl	$8, %r8d
	movq	%rsi, %rdx
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1576
	addq	$48, %rsp
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
.L1576:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24759:
	.size	_ZN2v88internal14TurboAssembler4CallENS0_7OperandE, .-_ZN2v88internal14TurboAssembler4CallENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler4CallEmNS0_9RelocInfo4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4CallEmNS0_9RelocInfo4ModeE
	.type	_ZN2v88internal14TurboAssembler4CallEmNS0_9RelocInfo4ModeE, @function
_ZN2v88internal14TurboAssembler4CallEmNS0_9RelocInfo4ModeE:
.LFB24760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$8, %r8d
	movb	%dl, %cl
	movq	%rsi, %rdx
	movl	$10, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24760:
	.size	_ZN2v88internal14TurboAssembler4CallEmNS0_9RelocInfo4ModeE, .-_ZN2v88internal14TurboAssembler4CallEmNS0_9RelocInfo4ModeE
	.section	.text._ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE
	.type	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE, @function
_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE:
.LFB24761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 181(%rdi)
	je	.L1580
	movq	512(%rdi), %rax
	leaq	-68(%rbp), %rdx
	movl	$-1, -68(%rbp)
	leaq	41184(%rax), %rdi
	call	_ZNK2v88internal8Builtins15IsBuiltinHandleENS0_6HandleINS0_10HeapObjectEEEPi@PLT
	testb	%al, %al
	jne	.L1589
.L1580:
	movsbl	%bl, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE@PLT
.L1579:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1590
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1589:
	.cfi_restore_state
	movl	-68(%rbp), %r13d
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi@PLT
	cmpl	$-1, %r13d
	je	.L1591
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %ebx
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	leaq	-64(%rbp), %rdi
	movl	%r13d, %esi
	movl	%ebx, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$10, %ecx
	movq	%rax, %rdx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1591:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1590:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24761:
	.size	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE, .-_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE
	.section	.text._ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeENS0_6HandleINS0_4CodeEEEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeENS0_6HandleINS0_4CodeEEEm
	.type	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeENS0_6HandleINS0_4CodeEEEm, @function
_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeENS0_6HandleINS0_4CodeEEEm:
.LFB24655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	16(%rbp), %rax
	movl	2016+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %r15d
	movl	%ecx, -120(%rbp)
	movl	%r8d, -128(%rbp)
	movq	%rax, -136(%rbp)
	movl	%r15d, %eax
	andl	$1, %eax
	movl	%eax, -60(%rbp)
	jne	.L1727
.L1593:
	movl	%r15d, %eax
	shrl	%eax
	andl	$1, %eax
	movl	%eax, -64(%rbp)
	jne	.L1728
.L1594:
	movl	%r15d, %eax
	shrl	$2, %eax
	andl	$1, %eax
	movl	%eax, -68(%rbp)
	jne	.L1729
.L1595:
	movl	%r15d, %eax
	shrl	$3, %eax
	andl	$1, %eax
	movl	%eax, -72(%rbp)
	jne	.L1730
.L1596:
	movl	%r15d, %eax
	shrl	$4, %eax
	andl	$1, %eax
	movl	%eax, -76(%rbp)
	jne	.L1731
.L1597:
	movl	%r15d, %eax
	shrl	$5, %eax
	andl	$1, %eax
	movl	%eax, -80(%rbp)
	jne	.L1732
.L1598:
	movl	%r15d, %eax
	shrl	$6, %eax
	andl	$1, %eax
	movl	%eax, -84(%rbp)
	jne	.L1733
.L1599:
	movl	%r15d, %eax
	shrl	$7, %eax
	andl	$1, %eax
	movl	%eax, -88(%rbp)
	jne	.L1734
.L1600:
	movl	%r15d, %eax
	shrl	$8, %eax
	andl	$1, %eax
	movl	%eax, -92(%rbp)
	jne	.L1735
.L1601:
	movl	%r15d, %eax
	shrl	$9, %eax
	andl	$1, %eax
	movl	%eax, -96(%rbp)
	jne	.L1736
.L1602:
	movl	%r15d, %eax
	shrl	$10, %eax
	andl	$1, %eax
	movl	%eax, -100(%rbp)
	jne	.L1737
.L1603:
	movl	%r15d, %eax
	shrl	$11, %eax
	andl	$1, %eax
	movl	%eax, -104(%rbp)
	jne	.L1738
.L1604:
	movl	%r15d, %eax
	shrl	$12, %eax
	andl	$1, %eax
	movl	%eax, -108(%rbp)
	jne	.L1739
.L1605:
	movl	%r15d, %eax
	shrl	$13, %eax
	andl	$1, %eax
	movl	%eax, -112(%rbp)
	jne	.L1740
.L1606:
	movl	%r15d, %eax
	shrl	$14, %eax
	andl	$1, %eax
	movl	%eax, -116(%rbp)
	jne	.L1741
.L1607:
	shrl	$15, %r15d
	andl	$1, %r15d
	movl	%r15d, -56(%rbp)
	jne	.L1742
.L1608:
	movq	2024+_ZN2v88internal15CallDescriptors21call_descriptor_data_E(%rip), %rax
	movl	%r14d, %r8d
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	(%rax), %ecx
	movl	4(%rax), %esi
	movl	8(%rax), %r15d
	movl	12(%rax), %eax
	movl	%eax, -52(%rbp)
	call	_ZN2v88internal14TurboAssembler8MovePairENS0_8RegisterES2_S2_S2_
	movl	-120(%rbp), %eax
	movq	-128(%rbp), %r13
	movq	%rax, %r14
	salq	$32, %r13
	salq	$32, %r14
	testl	%eax, %eax
	je	.L1743
	movl	$8, %r8d
	movq	%r14, %rdx
	movl	%r15d, %esi
	movq	%r12, %rdi
	movl	$19, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	cmpq	%r14, %r13
	je	.L1611
.L1762:
	testq	%r13, %r13
	je	.L1744
	movl	-52(%rbp), %esi
	movl	$8, %r8d
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$19, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L1614:
	testq	%rbx, %rbx
	je	.L1745
.L1615:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	jne	.L1746
.L1617:
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	jne	.L1747
.L1618:
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	jne	.L1748
.L1619:
	movl	-108(%rbp), %r15d
	testl	%r15d, %r15d
	jne	.L1749
.L1620:
	movl	-104(%rbp), %r14d
	testl	%r14d, %r14d
	jne	.L1750
.L1621:
	movl	-100(%rbp), %r13d
	testl	%r13d, %r13d
	jne	.L1751
.L1622:
	movl	-96(%rbp), %ebx
	testl	%ebx, %ebx
	jne	.L1752
.L1623:
	movl	-92(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L1753
.L1624:
	movl	-88(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L1754
.L1625:
	movl	-84(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L1755
.L1626:
	movl	-80(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L1756
.L1627:
	movl	-76(%rbp), %edi
	testl	%edi, %edi
	jne	.L1757
.L1628:
	movl	-72(%rbp), %esi
	testl	%esi, %esi
	jne	.L1758
.L1629:
	movl	-68(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L1759
.L1630:
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	jne	.L1760
.L1631:
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	jne	.L1761
.L1592:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1743:
	.cfi_restore_state
	movl	$4, %r8d
	movl	%r15d, %ecx
	movl	%r15d, %edx
	movq	%r12, %rdi
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	cmpq	%r14, %r13
	jne	.L1762
.L1611:
	movl	-52(%rbp), %esi
	movl	$8, %ecx
	movl	%r15d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	testq	%rbx, %rbx
	jne	.L1615
.L1745:
	movq	-136(%rbp), %rsi
	movl	$5, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9near_callElNS0_9RelocInfo4ModeE@PLT
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	je	.L1617
.L1746:
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-116(%rbp), %eax
	testl	%eax, %eax
	je	.L1618
.L1747:
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-112(%rbp), %eax
	testl	%eax, %eax
	je	.L1619
.L1748:
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-108(%rbp), %r15d
	testl	%r15d, %r15d
	je	.L1620
.L1749:
	movl	$12, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-104(%rbp), %r14d
	testl	%r14d, %r14d
	je	.L1621
.L1750:
	movl	$11, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-100(%rbp), %r13d
	testl	%r13d, %r13d
	je	.L1622
.L1751:
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-96(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L1623
.L1752:
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-92(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L1624
.L1753:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-88(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L1625
.L1754:
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-84(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L1626
.L1755:
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-80(%rbp), %r8d
	testl	%r8d, %r8d
	je	.L1627
.L1756:
	movq	%r12, %rdi
	movl	$5, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-76(%rbp), %edi
	testl	%edi, %edi
	je	.L1628
.L1757:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-72(%rbp), %esi
	testl	%esi, %esi
	je	.L1629
.L1758:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-68(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1630
.L1759:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-64(%rbp), %edx
	testl	%edx, %edx
	je	.L1631
.L1760:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-60(%rbp), %eax
	testl	%eax, %eax
	je	.L1592
.L1761:
	addq	$104, %rsp
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L1744:
	.cfi_restore_state
	movl	-52(%rbp), %edx
	movl	$4, %r8d
	movl	$51, %esi
	movq	%r12, %rdi
	movl	%edx, %ecx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L1614
	.p2align 4,,10
	.p2align 3
.L1742:
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1741:
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1740:
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1739:
	movl	$12, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1605
	.p2align 4,,10
	.p2align 3
.L1738:
	movl	$11, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1737:
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1736:
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1602
	.p2align 4,,10
	.p2align 3
.L1735:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1734:
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1600
	.p2align 4,,10
	.p2align 3
.L1733:
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1599
	.p2align 4,,10
	.p2align 3
.L1732:
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1731:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1730:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1729:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1595
	.p2align 4,,10
	.p2align 3
.L1728:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1594
	.p2align 4,,10
	.p2align 3
.L1727:
	xorl	%esi, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	jmp	.L1593
	.cfi_endproc
.LFE24655:
	.size	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeENS0_6HandleINS0_4CodeEEEm, .-_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeENS0_6HandleINS0_4CodeEEEm
	.section	.text._ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeE
	.type	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeE, @function
_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeE:
.LFB24653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r8d, %ebx
	subq	$8, %rsp
	movq	512(%rdi), %rax
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	subq	$8, %rsp
	movl	%r14d, %edx
	movl	%ebx, %r8d
	pushq	$0
	movq	%rax, %r9
	movl	%r15d, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeENS0_6HandleINS0_4CodeEEEm
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24653:
	.size	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeE, .-_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeE
	.section	.text._ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeEm,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeEm
	.type	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeEm, @function
_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeEm:
.LFB24654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$8, %rsp
	pushq	%r9
	xorl	%r9d, %r9d
	call	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeENS0_6HandleINS0_4CodeEEEm
	popq	%rax
	popq	%rdx
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24654:
	.size	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeEm, .-_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeEm
	.section	.text._ZN2v88internal14TurboAssembler30EntryFromBuiltinIndexAsOperandENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler30EntryFromBuiltinIndexAsOperandENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler30EntryFromBuiltinIndexAsOperandENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler30EntryFromBuiltinIndexAsOperandENS0_8RegisterE:
.LFB24762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %r8d
	movl	$7, %ecx
	movabsq	$81604378656, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movl	%esi, %r12d
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	movl	%r12d, %edx
	leaq	-48(%rbp), %rdi
	movl	$3, %ecx
	movl	$12512, %r8d
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movq	-48(%rbp), %rax
	movl	-40(%rbp), %edx
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1770
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1770:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24762:
	.size	_ZN2v88internal14TurboAssembler30EntryFromBuiltinIndexAsOperandENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler30EntryFromBuiltinIndexAsOperandENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler11CallBuiltinEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler11CallBuiltinEi
	.type	_ZN2v88internal14TurboAssembler11CallBuiltinEi, @function
_ZN2v88internal14TurboAssembler11CallBuiltinEi:
.LFB24764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal18TurboAssemblerBase33RecordCommentForOffHeapTrampolineEi@PLT
	cmpl	$-1, %r12d
	je	.L1775
	call	_ZN2v88internal7Isolate23CurrentEmbeddedBlobSizeEv@PLT
	movl	%eax, %ebx
	call	_ZN2v88internal7Isolate19CurrentEmbeddedBlobEv@PLT
	leaq	-64(%rbp), %rdi
	movl	%r12d, %esi
	movl	%ebx, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNK2v88internal12EmbeddedData25InstructionStartOfBuiltinEi@PLT
	movl	$8, %r8d
	movl	$10, %ecx
	movq	%r13, %rdi
	movq	%rax, %rdx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1776
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1775:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1776:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24764:
	.size	_ZN2v88internal14TurboAssembler11CallBuiltinEi, .-_ZN2v88internal14TurboAssembler11CallBuiltinEi
	.section	.text._ZN2v88internal14TurboAssembler13RetpolineCallENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler13RetpolineCallENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler13RetpolineCallENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler13RetpolineCallENS0_8RegisterE:
.LFB24768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movq	%r15, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-104(%rbp), %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callEPNS0_5LabelE@PLT
	leaq	-88(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pauseEv@PLT
	movq	-120(%rbp), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	xorl	%edx, %edx
	leaq	-80(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rsi
	movl	%r13d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1780
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1780:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24768:
	.size	_ZN2v88internal14TurboAssembler13RetpolineCallENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler13RetpolineCallENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler13RetpolineCallEmNS0_9RelocInfo4ModeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler13RetpolineCallEmNS0_9RelocInfo4ModeE
	.type	_ZN2v88internal14TurboAssembler13RetpolineCallEmNS0_9RelocInfo4ModeE, @function
_ZN2v88internal14TurboAssembler13RetpolineCallEmNS0_9RelocInfo4ModeE:
.LFB24769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$8, %r8d
	movb	%dl, %cl
	movq	%rsi, %rdx
	movl	$10, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %esi
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14TurboAssembler13RetpolineCallENS0_8RegisterE
	.cfi_endproc
.LFE24769:
	.size	_ZN2v88internal14TurboAssembler13RetpolineCallEmNS0_9RelocInfo4ModeE, .-_ZN2v88internal14TurboAssembler13RetpolineCallEmNS0_9RelocInfo4ModeE
	.section	.text._ZN2v88internal14TurboAssembler13RetpolineJumpENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler13RetpolineJumpENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler13RetpolineJumpENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler13RetpolineJumpENS0_8RegisterE:
.LFB24770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	leaq	-72(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	movq	%r14, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v88internal9Assembler4callEPNS0_5LabelE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pauseEv@PLT
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	xorl	%edx, %edx
	leaq	-64(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-64(%rbp), %rsi
	movl	-56(%rbp), %edx
	movl	%r13d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3retEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1786
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1786:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24770:
	.size	_ZN2v88internal14TurboAssembler13RetpolineJumpENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler13RetpolineJumpENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler6PextrdENS0_8RegisterENS0_11XMMRegisterEa,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler6PextrdENS0_8RegisterENS0_11XMMRegisterEa
	.type	_ZN2v88internal14TurboAssembler6PextrdENS0_8RegisterENS0_11XMMRegisterEa, @function
_ZN2v88internal14TurboAssembler6PextrdENS0_8RegisterENS0_11XMMRegisterEa:
.LFB24771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internal11CpuFeatures10supported_E(%rip), %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	testb	%cl, %cl
	je	.L1793
	testb	$2, %al
	jne	.L1794
	call	_ZN2v88internal9Assembler4movqENS0_8RegisterENS0_11XMMRegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$5, %ecx
	popq	%r12
	movl	$8, %r8d
	popq	%r13
	movabsq	$81604378656, %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	.p2align 4,,10
	.p2align 3
.L1794:
	.cfi_restore_state
	popq	%r12
	movsbl	%cl, %ecx
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler6pextrdENS0_8RegisterENS0_11XMMRegisterEa@PLT
	.p2align 4,,10
	.p2align 3
.L1793:
	.cfi_restore_state
	testb	$32, %al
	je	.L1789
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5vmovdENS0_8RegisterENS0_11XMMRegisterE@PLT
	.p2align 4,,10
	.p2align 3
.L1789:
	.cfi_restore_state
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4movdENS0_8RegisterENS0_11XMMRegisterE@PLT
	.cfi_endproc
.LFE24771:
	.size	_ZN2v88internal14TurboAssembler6PextrdENS0_8RegisterENS0_11XMMRegisterEa, .-_ZN2v88internal14TurboAssembler6PextrdENS0_8RegisterENS0_11XMMRegisterEa
	.section	.text._ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_8RegisterEa,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_8RegisterEa
	.type	_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_8RegisterEa, @function
_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_8RegisterEa:
.LFB24772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%ecx, %ebx
	subq	$8, %rsp
	movl	_ZN2v88internal11CpuFeatures10supported_E(%rip), %eax
	testb	$2, %al
	jne	.L1802
	movl	$15, %esi
	testb	$32, %al
	jne	.L1803
	call	_ZN2v88internal9Assembler4movdENS0_11XMMRegisterENS0_8RegisterE@PLT
	cmpb	$1, %bl
	je	.L1804
.L1799:
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1800
	addq	$8, %rsp
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%rbx
	movl	$15, %r8d
	popq	%r12
	movl	$16, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_S2_@PLT
	.p2align 4,,10
	.p2align 3
.L1803:
	.cfi_restore_state
	call	_ZN2v88internal9Assembler5vmovdENS0_11XMMRegisterENS0_8RegisterE@PLT
	cmpb	$1, %bl
	jne	.L1799
.L1804:
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %edx
	addq	$8, %rsp
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%rbx
	movl	$98, %r9d
	popq	%r12
	movl	$15, %r8d
	popq	%r13
	movl	$102, %ecx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	.p2align 4,,10
	.p2align 3
.L1802:
	.cfi_restore_state
	addq	$8, %rsp
	movsbl	%cl, %ecx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler6pinsrdENS0_11XMMRegisterENS0_8RegisterEa@PLT
	.p2align 4,,10
	.p2align 3
.L1800:
	.cfi_restore_state
	addq	$8, %rsp
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$15, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterES2_@PLT
	.cfi_endproc
.LFE24772:
	.size	_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_8RegisterEa, .-_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_8RegisterEa
	.section	.text._ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_7OperandEa,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_7OperandEa
	.type	_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_7OperandEa, @function
_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_7OperandEa:
.LFB24773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%r8d, %ebx
	subq	$56, %rsp
	movl	%ecx, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal11CpuFeatures10supported_E(%rip), %eax
	testb	$2, %al
	jne	.L1819
	movl	%ecx, -44(%rbp)
	testb	$32, %al
	jne	.L1820
	movl	-44(%rbp), %ecx
	movl	$15, %esi
	call	_ZN2v88internal9Assembler4movdENS0_11XMMRegisterENS0_7OperandE@PLT
	cmpb	$1, %bl
	je	.L1821
.L1810:
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1812
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1818
	addq	$56, %rsp
	movl	%r13d, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	popq	%rbx
	movl	$15, %r8d
	popq	%r12
	movl	$16, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3vssEhNS0_11XMMRegisterES2_S2_@PLT
	.p2align 4,,10
	.p2align 3
.L1820:
	.cfi_restore_state
	movl	%ecx, %ecx
	movl	$15, %esi
	call	_ZN2v88internal9Assembler5vmovdENS0_11XMMRegisterENS0_7OperandE@PLT
	cmpb	$1, %bl
	jne	.L1810
.L1821:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1818
	movl	_ZN2v88internalL17kScratchDoubleRegE(%rip), %edx
	addq	$56, %rsp
	movl	%r13d, %esi
	movq	%r12, %rdi
	popq	%rbx
	movl	$98, %r9d
	popq	%r12
	movl	$15, %r8d
	popq	%r13
	movl	$102, %ecx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler10sse2_instrENS0_11XMMRegisterES2_hhh@PLT
	.p2align 4,,10
	.p2align 3
.L1819:
	.cfi_restore_state
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1818
	movl	-72(%rbp), %ecx
	addq	$56, %rsp
	movsbl	%r8b, %r8d
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler6pinsrdENS0_11XMMRegisterENS0_7OperandEa@PLT
	.p2align 4,,10
	.p2align 3
.L1812:
	.cfi_restore_state
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1818
	addq	$56, %rsp
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$15, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5movssENS0_11XMMRegisterES2_@PLT
.L1818:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24773:
	.size	_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_7OperandEa, .-_ZN2v88internal14TurboAssembler6PinsrdENS0_11XMMRegisterENS0_7OperandEa
	.section	.text._ZN2v88internal14TurboAssembler5PsllqENS0_11XMMRegisterEh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler5PsllqENS0_11XMMRegisterEh
	.type	_ZN2v88internal14TurboAssembler5PsllqENS0_11XMMRegisterEh, @function
_ZN2v88internal14TurboAssembler5PsllqENS0_11XMMRegisterEh:
.LFB24774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1823
	movl	_ZN2v88internalL4xmm6E(%rip), %edx
	movl	%esi, %r8d
	movl	$115, %esi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	movq	32(%r12), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 32(%r12)
	movb	%bl, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1823:
	.cfi_restore_state
	popq	%rbx
	movzbl	%dl, %edx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5psllqENS0_11XMMRegisterEh@PLT
	.cfi_endproc
.LFE24774:
	.size	_ZN2v88internal14TurboAssembler5PsllqENS0_11XMMRegisterEh, .-_ZN2v88internal14TurboAssembler5PsllqENS0_11XMMRegisterEh
	.section	.text._ZN2v88internal14TurboAssembler5PsrlqENS0_11XMMRegisterEh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler5PsrlqENS0_11XMMRegisterEh
	.type	_ZN2v88internal14TurboAssembler5PsrlqENS0_11XMMRegisterEh, @function
_ZN2v88internal14TurboAssembler5PsrlqENS0_11XMMRegisterEh:
.LFB24775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1828
	movl	_ZN2v88internalL4xmm2E(%rip), %edx
	movl	%esi, %r8d
	movl	$115, %esi
	call	_ZN2v88internal9Assembler3vpdEhNS0_11XMMRegisterES2_S2_@PLT
	movq	32(%r12), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 32(%r12)
	movb	%bl, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1828:
	.cfi_restore_state
	popq	%rbx
	movzbl	%dl, %edx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5psrlqENS0_11XMMRegisterEh@PLT
	.cfi_endproc
.LFE24775:
	.size	_ZN2v88internal14TurboAssembler5PsrlqENS0_11XMMRegisterEh, .-_ZN2v88internal14TurboAssembler5PsrlqENS0_11XMMRegisterEh
	.section	.text._ZN2v88internal14TurboAssembler5PslldENS0_11XMMRegisterEh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler5PslldENS0_11XMMRegisterEh
	.type	_ZN2v88internal14TurboAssembler5PslldENS0_11XMMRegisterEh, @function
_ZN2v88internal14TurboAssembler5PslldENS0_11XMMRegisterEh:
.LFB24776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1833
	pushq	$0
	movl	_ZN2v88internalL4xmm6E(%rip), %edx
	movl	%esi, %r8d
	movl	$1, %r9d
	pushq	$1
	movl	$114, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	movq	32(%r12), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 32(%r12)
	movb	%bl, (%rax)
	popq	%rax
	popq	%rdx
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1833:
	.cfi_restore_state
	leaq	-16(%rbp), %rsp
	movzbl	%dl, %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5pslldENS0_11XMMRegisterEh@PLT
	.cfi_endproc
.LFE24776:
	.size	_ZN2v88internal14TurboAssembler5PslldENS0_11XMMRegisterEh, .-_ZN2v88internal14TurboAssembler5PslldENS0_11XMMRegisterEh
	.section	.text._ZN2v88internal14TurboAssembler5PsrldENS0_11XMMRegisterEh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler5PsrldENS0_11XMMRegisterEh
	.type	_ZN2v88internal14TurboAssembler5PsrldENS0_11XMMRegisterEh, @function
_ZN2v88internal14TurboAssembler5PsrldENS0_11XMMRegisterEh:
.LFB24777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movl	%edx, %ebx
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1838
	pushq	$0
	movl	_ZN2v88internalL4xmm2E(%rip), %edx
	movl	%esi, %r8d
	movl	$1, %r9d
	pushq	$1
	movl	$114, %esi
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_S2_NS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	movq	32(%r12), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 32(%r12)
	movb	%bl, (%rax)
	popq	%rax
	popq	%rdx
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1838:
	.cfi_restore_state
	leaq	-16(%rbp), %rsp
	movzbl	%dl, %edx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5psrldENS0_11XMMRegisterEh@PLT
	.cfi_endproc
.LFE24777:
	.size	_ZN2v88internal14TurboAssembler5PsrldENS0_11XMMRegisterEh, .-_ZN2v88internal14TurboAssembler5PsrldENS0_11XMMRegisterEh
	.section	.text._ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterES2_
	.type	_ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterES2_, @function
_ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterES2_:
.LFB24778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	$2, 1+_ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L1847
	leaq	-48(%rbp), %r14
	movq	$0, -48(%rbp)
	movabsq	$81604378624, %rbx
	call	_ZN2v88internal9Assembler4bsrlENS0_8RegisterES2_@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%rbx, %rdx
	movl	$4, %ecx
	movl	%r13d, %esi
	orq	$63, %rdx
	movq	%r12, %rdi
	orq	$31, %rbx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%rbx, %rcx
	movl	%r13d, %edx
	movl	$6, %esi
	movl	$4, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
.L1842:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1848
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1847:
	.cfi_restore_state
	call	_ZN2v88internal9Assembler6lzcntlENS0_8RegisterES2_@PLT
	jmp	.L1842
.L1848:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24778:
	.size	_ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterES2_, .-_ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterENS0_7OperandE:
.LFB24779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movl	%ecx, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	$2, 1+_ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L1854
	movl	-56(%rbp), %ecx
	leaq	-48(%rbp), %r14
	movabsq	$81604378624, %rbx
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler4bsrlENS0_8RegisterENS0_7OperandE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%rbx, %rdx
	movl	$4, %ecx
	movl	%r13d, %esi
	orq	$63, %rdx
	movq	%r12, %rdi
	orq	$31, %rbx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%rbx, %rcx
	movl	%r13d, %edx
	movl	$6, %esi
	movl	$4, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
.L1849:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1855
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1854:
	.cfi_restore_state
	movl	%ecx, %ecx
	call	_ZN2v88internal9Assembler6lzcntlENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1849
.L1855:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24779:
	.size	_ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler6LzcntlENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterES2_
	.type	_ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterES2_, @function
_ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterES2_:
.LFB24780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	$2, 1+_ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L1861
	leaq	-48(%rbp), %r14
	movq	$0, -48(%rbp)
	movabsq	$81604378624, %rbx
	call	_ZN2v88internal9Assembler4bsrqENS0_8RegisterES2_@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%rbx, %rdx
	movl	$4, %ecx
	movl	%r13d, %esi
	orq	$127, %rdx
	movq	%r12, %rdi
	orq	$63, %rbx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%rbx, %rcx
	movl	%r13d, %edx
	movl	$6, %esi
	movl	$4, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
.L1856:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1862
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1861:
	.cfi_restore_state
	call	_ZN2v88internal9Assembler6lzcntqENS0_8RegisterES2_@PLT
	jmp	.L1856
.L1862:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24780:
	.size	_ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterES2_, .-_ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterENS0_7OperandE:
.LFB24781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movl	%ecx, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testb	$2, 1+_ZN2v88internal11CpuFeatures10supported_E(%rip)
	jne	.L1868
	movl	-56(%rbp), %ecx
	leaq	-48(%rbp), %r14
	movabsq	$81604378624, %rbx
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler4bsrqENS0_8RegisterENS0_7OperandE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%rbx, %rdx
	movl	$4, %ecx
	movl	%r13d, %esi
	orq	$127, %rdx
	movq	%r12, %rdi
	orq	$63, %rbx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	%rbx, %rcx
	movl	%r13d, %edx
	movl	$6, %esi
	movl	$4, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
.L1863:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1869
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1868:
	.cfi_restore_state
	movl	%ecx, %ecx
	call	_ZN2v88internal9Assembler6lzcntqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1863
.L1869:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24781:
	.size	_ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler6LzcntqENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterES2_
	.type	_ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterES2_, @function
_ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterES2_:
.LFB24782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	js	.L1875
	leaq	-48(%rbp), %r14
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler4bsfqENS0_8RegisterES2_@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$4, %ecx
	movabsq	$81604378688, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
.L1870:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1876
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1875:
	.cfi_restore_state
	call	_ZN2v88internal9Assembler6tzcntqENS0_8RegisterES2_@PLT
	jmp	.L1870
.L1876:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24782:
	.size	_ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterES2_, .-_ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterENS0_7OperandE:
.LFB24783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movl	%ecx, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	js	.L1882
	movl	-56(%rbp), %ecx
	leaq	-48(%rbp), %r14
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler4bsfqENS0_8RegisterENS0_7OperandE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$4, %ecx
	movabsq	$81604378688, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
.L1877:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1883
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1882:
	.cfi_restore_state
	movl	%ecx, %ecx
	call	_ZN2v88internal9Assembler6tzcntqENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1877
.L1883:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24783:
	.size	_ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler6TzcntqENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterES2_
	.type	_ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterES2_, @function
_ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterES2_:
.LFB24784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	js	.L1889
	leaq	-48(%rbp), %r14
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler4bsflENS0_8RegisterES2_@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$4, %ecx
	movabsq	$81604378656, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
.L1884:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1890
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1889:
	.cfi_restore_state
	call	_ZN2v88internal9Assembler6tzcntlENS0_8RegisterES2_@PLT
	jmp	.L1884
.L1890:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24784:
	.size	_ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterES2_, .-_ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterENS0_7OperandE:
.LFB24785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$40, %rsp
	movl	%ecx, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	js	.L1896
	movl	-56(%rbp), %ecx
	leaq	-48(%rbp), %r14
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler4bsflENS0_8RegisterENS0_7OperandE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$4, %ecx
	movabsq	$81604378656, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
.L1891:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1897
	addq	$40, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1896:
	.cfi_restore_state
	movl	%ecx, %ecx
	call	_ZN2v88internal9Assembler6tzcntlENS0_8RegisterENS0_7OperandE@PLT
	jmp	.L1891
.L1897:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24785:
	.size	_ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler6TzcntlENS0_8RegisterENS0_7OperandE
	.section	.rodata._ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterES2_.str1.1,"aMS",@progbits,1
.LC9:
	.string	"unreachable code"
	.section	.text._ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterES2_
	.type	_ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterES2_, @function
_ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterES2_:
.LFB24786:
	.cfi_startproc
	endbr64
	testb	$4, 1+_ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1899
	jmp	_ZN2v88internal9Assembler7popcntlENS0_8RegisterES2_@PLT
.L1899:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24786:
	.size	_ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterES2_, .-_ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterENS0_7OperandE:
.LFB24787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%ecx, -8(%rbp)
	testb	$4, 1+_ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1903
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%ecx, %ecx
	jmp	_ZN2v88internal9Assembler7popcntlENS0_8RegisterENS0_7OperandE@PLT
.L1903:
	.cfi_restore_state
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24787:
	.size	_ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler7PopcntlENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterES2_
	.type	_ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterES2_, @function
_ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterES2_:
.LFB24788:
	.cfi_startproc
	endbr64
	testb	$4, 1+_ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1906
	jmp	_ZN2v88internal9Assembler7popcntqENS0_8RegisterES2_@PLT
.L1906:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24788:
	.size	_ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterES2_, .-_ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterES2_
	.section	.text._ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterENS0_7OperandE:
.LFB24789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%ecx, -8(%rbp)
	testb	$4, 1+_ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L1910
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movl	%ecx, %ecx
	jmp	_ZN2v88internal9Assembler7popcntqENS0_8RegisterENS0_7OperandE@PLT
.L1910:
	.cfi_restore_state
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE24789:
	.size	_ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14TurboAssembler7PopcntqENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14MacroAssembler16PushStackHandlerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler16PushStackHandlerEv
	.type	_ZN2v88internal14MacroAssembler16PushStackHandlerEv, @function
_ZN2v88internal14MacroAssembler16PushStackHandlerEv:
.LFB24790:
	.cfi_startproc
	endbr64
	movabsq	$81604378624, %rsi
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal9Assembler5pushqENS0_9ImmediateE@PLT
	movq	512(%r12), %rsi
	xorl	%edi, %edi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r14d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	movl	%r14d, %edx
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler5pushqENS0_7OperandE@PLT
	movq	%r13, %rsi
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE
	movq	%rax, %rsi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1916
	movl	_ZN2v88internalL3rspE(%rip), %ecx
	addq	$56, %rsp
	movq	%r12, %rdi
	movl	$8, %r8d
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
.L1916:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24790:
	.size	_ZN2v88internal14MacroAssembler16PushStackHandlerEv, .-_ZN2v88internal14MacroAssembler16PushStackHandlerEv
	.section	.text._ZN2v88internal14MacroAssembler15PopStackHandlerEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler15PopStackHandlerEv
	.type	_ZN2v88internal14MacroAssembler15PopStackHandlerEv, @function
_ZN2v88internal14MacroAssembler15PopStackHandlerEv:
.LFB24791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$72, %rsp
	movq	512(%rdi), %rsi
	xorl	%edi, %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	cmpb	$0, 528(%r12)
	movq	%rax, -88(%rbp)
	je	.L1918
	cmpb	$0, 178(%r12)
	jne	.L1919
	cmpb	$0, 180(%r12)
	jne	.L1935
.L1918:
	movq	-88(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	leaq	-52(%rbp), %rdi
	xorl	%edx, %edx
	movl	%r13d, %esi
.L1932:
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movl	-44(%rbp), %eax
	movq	-64(%rbp), %rsi
	movl	%eax, %edx
	movl	%eax, -56(%rbp)
	movl	%eax, -44(%rbp)
	movq	%rsi, -52(%rbp)
	call	_ZN2v88internal9Assembler4popqENS0_7OperandE@PLT
	xorl	%esi, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movabsq	$81604378632, %rcx
	movl	$4, %edx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1936
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1935:
	.cfi_restore_state
	movq	512(%r12), %rdi
	leaq	-88(%rbp), %r14
	movq	%r14, %rsi
	call	_ZN2v88internal18TurboAssemblerBase32IsAddressableThroughRootRegisterEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movq	512(%r12), %rdi
	movq	%r14, %rsi
	testb	%al, %al
	je	.L1925
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L1933
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L1919:
	movq	512(%r12), %rdi
	leaq	-88(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L1933
	cmpb	$0, 528(%r12)
	je	.L1918
	cmpb	$0, 180(%r12)
	je	.L1918
	jmp	.L1935
	.p2align 4,,10
	.p2align 3
.L1925:
	call	_ZN2v88internal18TurboAssemblerBase48RootRegisterOffsetForExternalReferenceTableEntryEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	leaq	-52(%rbp), %r14
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	movl	%eax, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-52(%rbp), %rdx
	movl	-44(%rbp), %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movl	%r13d, %esi
	movq	%r14, %rdi
	jmp	.L1932
	.p2align 4,,10
	.p2align 3
.L1933:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-52(%rbp), %rdi
	jmp	.L1932
.L1936:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24791:
	.size	_ZN2v88internal14MacroAssembler15PopStackHandlerEv, .-_ZN2v88internal14MacroAssembler15PopStackHandlerEv
	.section	.text._ZN2v88internal14TurboAssembler3RetEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler3RetEv
	.type	_ZN2v88internal14TurboAssembler3RetEv, @function
_ZN2v88internal14TurboAssembler3RetEv:
.LFB24792:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN2v88internal9Assembler3retEi@PLT
	.cfi_endproc
.LFE24792:
	.size	_ZN2v88internal14TurboAssembler3RetEv, .-_ZN2v88internal14TurboAssembler3RetEv
	.section	.text._ZN2v88internal14TurboAssembler3RetEiNS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler3RetEiNS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler3RetEiNS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler3RetEiNS0_8RegisterE:
.LFB24793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rax, %r12
	movl	%r12d, %esi
	subq	$8, %rsp
	shrq	$16, %rax
	je	.L1941
	movl	%edx, %esi
	movl	%edx, %r14d
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	%r12d, %ecx
	movl	$8, %r8d
	xorl	%esi, %esi
	movabsq	$81604378624, %r12
	movl	$4, %edx
	movq	%r13, %rdi
	orq	%r12, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	xorl	%esi, %esi
.L1941:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler3retEi@PLT
	.cfi_endproc
.LFE24793:
	.size	_ZN2v88internal14TurboAssembler3RetEiNS0_8RegisterE, .-_ZN2v88internal14TurboAssembler3RetEiNS0_8RegisterE
	.section	.text._ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_
	.type	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_, @function
_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_:
.LFB24794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-52(%rbp), %r14
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	movl	$-1, %edx
	movzwl	%bx, %ebx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-44(%rbp), %ecx
	movq	-52(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	$11, %edx
	movl	%r13d, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-44(%rbp), %ecx
	movq	-52(%rbp), %rdx
	movq	%r12, %rdi
	movabsq	$81604378624, %r8
	movl	$7, %esi
	orq	%rbx, %r8
	call	_ZN2v88internal9Assembler26immediate_arithmetic_op_16EhNS0_7OperandENS0_9ImmediateE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1945
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1945:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24794:
	.size	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_, .-_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_
	.section	.text._ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE
	.type	_ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE, @function
_ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE:
.LFB24795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	leaq	-48(%rbp), %rdi
	.cfi_offset 3, -32
	movl	%edx, %ebx
	movl	$11, %edx
	movzwl	%bx, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-40(%rbp), %ecx
	movq	-48(%rbp), %rdx
	movq	%r12, %rdi
	movabsq	$81604378624, %r8
	movl	$7, %esi
	orq	%rbx, %r8
	call	_ZN2v88internal9Assembler26immediate_arithmetic_op_16EhNS0_7OperandENS0_9ImmediateE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1949
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1949:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24795:
	.size	_ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE, .-_ZN2v88internal14MacroAssembler15CmpInstanceTypeENS0_8RegisterENS0_12InstanceTypeE
	.section	.text._ZN2v88internal14MacroAssembler13LoadWeakValueENS0_8RegisterEPNS0_5LabelE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler13LoadWeakValueENS0_8RegisterEPNS0_5LabelE
	.type	_ZN2v88internal14MacroAssembler13LoadWeakValueENS0_8RegisterEPNS0_5LabelE, @function
_ZN2v88internal14MacroAssembler13LoadWeakValueENS0_8RegisterEPNS0_5LabelE:
.LFB24805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$4, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	movl	%esi, %edx
	pushq	%r13
	.cfi_offset 13, -32
	movl	%esi, %r13d
	movl	$7, %esi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movabsq	$81604378624, %rbx
	movq	%rbx, %rcx
	orq	$3, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$1, %ecx
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$4294967293, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	orq	%rbx, %rcx
	movl	$8, %r8d
	popq	%rbx
	movl	$4, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	.cfi_endproc
.LFE24805:
	.size	_ZN2v88internal14MacroAssembler13LoadWeakValueENS0_8RegisterEPNS0_5LabelE, .-_ZN2v88internal14MacroAssembler13LoadWeakValueENS0_8RegisterEPNS0_5LabelE
	.section	.text._ZN2v88internal14MacroAssembler16IncrementCounterEPNS0_12StatsCounterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler16IncrementCounterEPNS0_12StatsCounterEi
	.type	_ZN2v88internal14MacroAssembler16IncrementCounterEPNS0_12StatsCounterEi, @function
_ZN2v88internal14MacroAssembler16IncrementCounterEPNS0_12StatsCounterEi:
.LFB24806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal25FLAG_native_code_countersE(%rip)
	je	.L1952
	cmpb	$0, 24(%rsi)
	movq	%rdi, %r13
	movq	%rsi, %r12
	movl	%edx, %ebx
	je	.L1955
	movq	16(%rsi), %rax
.L1956:
	testq	%rax, %rax
	je	.L1952
	movq	%r12, %rdi
	call	_ZN2v88internal17ExternalReference6CreateEPNS0_12StatsCounterE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE
	cmpl	$1, %ebx
	je	.L1968
	movl	%edx, -44(%rbp)
	movl	%ebx, %r8d
	movabsq	$81604378624, %rdx
	orq	%rdx, %r8
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1967
	movl	-44(%rbp), %ecx
	addq	$56, %rsp
	movq	%r13, %rdi
	movl	$4, %r9d
	popq	%rbx
	movq	%rax, %rdx
	popq	%r12
	xorl	%esi, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	.p2align 4,,10
	.p2align 3
.L1952:
	.cfi_restore_state
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1967
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1955:
	.cfi_restore_state
	movb	$1, 24(%rsi)
	movq	%rsi, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 16(%r12)
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L1968:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1967
	addq	$56, %rsp
	movq	%r13, %rdi
	movl	$4, %ecx
	movq	%rax, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler8emit_incENS0_7OperandEi@PLT
.L1967:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24806:
	.size	_ZN2v88internal14MacroAssembler16IncrementCounterEPNS0_12StatsCounterEi, .-_ZN2v88internal14MacroAssembler16IncrementCounterEPNS0_12StatsCounterEi
	.section	.text._ZN2v88internal14MacroAssembler16DecrementCounterEPNS0_12StatsCounterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler16DecrementCounterEPNS0_12StatsCounterEi
	.type	_ZN2v88internal14MacroAssembler16DecrementCounterEPNS0_12StatsCounterEi, @function
_ZN2v88internal14MacroAssembler16DecrementCounterEPNS0_12StatsCounterEi:
.LFB24807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN2v88internal25FLAG_native_code_countersE(%rip)
	je	.L1969
	cmpb	$0, 24(%rsi)
	movq	%rdi, %r13
	movq	%rsi, %r12
	movl	%edx, %ebx
	je	.L1972
	movq	16(%rsi), %rax
.L1973:
	testq	%rax, %rax
	je	.L1969
	movq	%r12, %rdi
	call	_ZN2v88internal17ExternalReference6CreateEPNS0_12StatsCounterE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE
	cmpl	$1, %ebx
	je	.L1985
	movl	%edx, -44(%rbp)
	movl	%ebx, %r8d
	movabsq	$81604378624, %rdx
	orq	%rdx, %r8
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1984
	movl	-44(%rbp), %ecx
	addq	$56, %rsp
	movq	%r13, %rdi
	movl	$4, %r9d
	popq	%rbx
	movq	%rax, %rdx
	popq	%r12
	movl	$5, %esi
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	.p2align 4,,10
	.p2align 3
.L1969:
	.cfi_restore_state
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1984
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1972:
	.cfi_restore_state
	movb	$1, 24(%rsi)
	movq	%rsi, %rdi
	call	_ZNK2v88internal16StatsCounterBase24FindLocationInStatsTableEv@PLT
	movq	%rax, 16(%r12)
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1985:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1984
	addq	$56, %rsp
	movq	%r13, %rdi
	movl	$4, %ecx
	movq	%rax, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler8emit_decENS0_7OperandEi@PLT
.L1984:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24807:
	.size	_ZN2v88internal14MacroAssembler16DecrementCounterEPNS0_12StatsCounterEi, .-_ZN2v88internal14MacroAssembler16DecrementCounterEPNS0_12StatsCounterEi
	.section	.text._ZN2v88internal14MacroAssembler15MaybeDropFramesEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler15MaybeDropFramesEv
	.type	_ZN2v88internal14MacroAssembler15MaybeDropFramesEv, @function
_ZN2v88internal14MacroAssembler15MaybeDropFramesEv:
.LFB24808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	512(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17ExternalReference24debug_restart_fp_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %r13d
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	%r13d, %esi
	call	_ZN2v88internal14MacroAssembler4LoadENS0_8RegisterENS0_17ExternalReferenceE
	movl	%r13d, %edx
	movl	%r13d, %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	leaq	-32(%rbp), %r13
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	movq	$0, -32(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	512(%r12), %rax
	movl	$89, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movl	$16, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1989
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1989:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24808:
	.size	_ZN2v88internal14MacroAssembler15MaybeDropFramesEv, .-_ZN2v88internal14MacroAssembler15MaybeDropFramesEv
	.section	.text._ZN2v88internal14MacroAssembler14InvokePrologueERKNS0_14ParameterCountES4_PNS0_5LabelEPb10InvokeFlagNS5_8DistanceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler14InvokePrologueERKNS0_14ParameterCountES4_PNS0_5LabelEPb10InvokeFlagNS5_8DistanceE
	.type	_ZN2v88internal14MacroAssembler14InvokePrologueERKNS0_14ParameterCountES4_PNS0_5LabelEPb10InvokeFlagNS5_8DistanceE, @function
_ZN2v88internal14MacroAssembler14InvokePrologueERKNS0_14ParameterCountES4_PNS0_5LabelEPb10InvokeFlagNS5_8DistanceE:
.LFB24813:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r9d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movb	$0, (%r8)
	movl	(%rsi), %edx
	movq	$0, -64(%rbp)
	cmpl	$-1, %edx
	je	.L2007
	movl	(%rbx), %r9d
	cmpl	$-1, %r9d
	je	.L2008
	cmpl	%edx, %r9d
	jne	.L2009
	testl	%r9d, %r9d
	je	.L1990
	movl	$8, %ecx
	movl	%r9d, %edx
	xorl	%esi, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	jmp	.L1990
	.p2align 4,,10
	.p2align 3
.L2009:
	movl	$8, %r8d
	movl	%r9d, %ecx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
.L2006:
	leaq	-64(%rbp), %rbx
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
.L1995:
	movq	512(%r12), %rax
	movl	$3, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movq	%rax, %rsi
	testl	%r14d, %r14d
	jne	.L1999
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE
	cmpb	$0, 0(%r13)
	je	.L2010
.L2000:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
.L1990:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2011
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1999:
	.cfi_restore_state
	movl	$16, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler4JumpENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeENS0_9ConditionE
	jmp	.L2000
	.p2align 4,,10
	.p2align 3
.L2007:
	movq	%rsi, -72(%rbp)
	movzwl	4(%rbx), %edx
	movl	_ZN2v88internalL3raxE(%rip), %esi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl
	movq	-72(%rbp), %rax
	movzwl	4(%rax), %edx
	cmpw	%dx, 4(%rbx)
	je	.L1990
	cmpw	$-1, %dx
	je	.L1990
	movb	$1, 0(%r13)
	movzwl	4(%rax), %edx
	movq	%r12, %rdi
	leaq	-64(%rbp), %rbx
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl
	jmp	.L1995
	.p2align 4,,10
	.p2align 3
.L2010:
	movl	16(%rbp), %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	jmp	.L2000
	.p2align 4,,10
	.p2align 3
.L2008:
	movq	%rsi, -72(%rbp)
	movzwl	4(%rbx), %edx
	movl	_ZN2v88internalL3raxE(%rip), %esi
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl
	movq	-72(%rbp), %rax
	movzwl	4(%rbx), %ecx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$7, %esi
	movl	(%rax), %edx
	movabsq	$81604378624, %rax
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L2006
.L2011:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24813:
	.size	_ZN2v88internal14MacroAssembler14InvokePrologueERKNS0_14ParameterCountES4_PNS0_5LabelEPb10InvokeFlagNS5_8DistanceE, .-_ZN2v88internal14MacroAssembler14InvokePrologueERKNS0_14ParameterCountES4_PNS0_5LabelEPb10InvokeFlagNS5_8DistanceE
	.section	.text._ZN2v88internal14TurboAssembler12StubPrologueENS0_10StackFrame4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler12StubPrologueENS0_10StackFrame4TypeE
	.type	_ZN2v88internal14TurboAssembler12StubPrologueENS0_10StackFrame4TypeE, @function
_ZN2v88internal14TurboAssembler12StubPrologueENS0_10StackFrame4TypeE:
.LFB24815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$8, %ecx
	movl	_ZN2v88internalL3rspE(%rip), %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	addq	$8, %rsp
	leal	(%rbx,%rbx), %esi
	movq	%r12, %rdi
	movabsq	$81604378624, %rbx
	orq	%rbx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5pushqENS0_9ImmediateE@PLT
	.cfi_endproc
.LFE24815:
	.size	_ZN2v88internal14TurboAssembler12StubPrologueENS0_10StackFrame4TypeE, .-_ZN2v88internal14TurboAssembler12StubPrologueENS0_10StackFrame4TypeE
	.section	.text._ZN2v88internal14TurboAssembler8PrologueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler8PrologueEv
	.type	_ZN2v88internal14TurboAssembler8PrologueEv, @function
_ZN2v88internal14TurboAssembler8PrologueEv:
.LFB24816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$8, %ecx
	movl	_ZN2v88internalL3rspE(%rip), %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	.cfi_endproc
.LFE24816:
	.size	_ZN2v88internal14TurboAssembler8PrologueEv, .-_ZN2v88internal14TurboAssembler8PrologueEv
	.section	.text._ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE
	.type	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE, @function
_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE:
.LFB31403:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$8, %rsp
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$8, %ecx
	movl	_ZN2v88internalL3rspE(%rip), %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	addq	$8, %rsp
	leal	(%rbx,%rbx), %esi
	movq	%r12, %rdi
	movabsq	$81604378624, %rbx
	orq	%rbx, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler5pushqENS0_9ImmediateE@PLT
	.cfi_endproc
.LFE31403:
	.size	_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE, .-_ZN2v88internal14TurboAssembler10EnterFrameENS0_10StackFrame4TypeE
	.section	.text._ZN2v88internal14MacroAssembler22EnterExitFramePrologueEbNS0_10StackFrame4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler22EnterExitFramePrologueEbNS0_10StackFrame4TypeE
	.type	_ZN2v88internal14MacroAssembler22EnterExitFramePrologueEbNS0_10StackFrame4TypeE, @function
_ZN2v88internal14MacroAssembler22EnterExitFramePrologueEbNS0_10StackFrame4TypeE:
.LFB24819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%r13d, %esi
	movl	%edx, %ebx
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	$8, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	leal	(%rbx,%rbx), %esi
	movq	%r12, %rdi
	movabsq	$81604378624, %rbx
	orq	%rbx, %rsi
	call	_ZN2v88internal9Assembler5pushqENS0_9ImmediateE@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_9ImmediateE@PLT
	testb	%r14b, %r14b
	jne	.L2021
.L2019:
	movq	512(%r12), %rsi
	movl	$1, %edi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler5StoreENS0_17ExternalReferenceENS0_8RegisterE
	movq	512(%r12), %rsi
	movl	$3, %edi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL3rsiE(%rip), %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14MacroAssembler5StoreENS0_17ExternalReferenceENS0_8RegisterE
	movq	512(%r12), %rsi
	movl	$2, %edi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %edx
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14MacroAssembler5StoreENS0_17ExternalReferenceENS0_8RegisterE
	.p2align 4,,10
	.p2align 3
.L2021:
	.cfi_restore_state
	movl	_ZN2v88internalL3raxE(%rip), %edx
	movl	_ZN2v88internalL3r14E(%rip), %esi
	movl	$8, %ecx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	jmp	.L2019
	.cfi_endproc
.LFE24819:
	.size	_ZN2v88internal14MacroAssembler22EnterExitFramePrologueEbNS0_10StackFrame4TypeE, .-_ZN2v88internal14MacroAssembler22EnterExitFramePrologueEbNS0_10StackFrame4TypeE
	.section	.text._ZN2v88internal14MacroAssembler22EnterExitFrameEpilogueEib,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler22EnterExitFrameEpilogueEib
	.type	_ZN2v88internal14MacroAssembler22EnterExitFrameEpilogueEib, @function
_ZN2v88internal14MacroAssembler22EnterExitFrameEpilogueEib:
.LFB24820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L2034
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	testl	%esi, %esi
	jg	.L2035
.L2025:
	call	_ZN2v84base2OS24ActivationFrameAlignmentEv@PLT
	testl	%eax, %eax
	jle	.L2029
	negl	%eax
	movl	$8, %r8d
	movl	$4, %edx
	movq	%r12, %rdi
	movl	%eax, %ecx
	movl	$4, %esi
	movabsq	$81604378624, %rax
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
.L2029:
	leaq	-80(%rbp), %rdi
	movl	$-16, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %ecx
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2036
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2035:
	.cfi_restore_state
	leal	0(,%rsi,8), %ecx
	movl	$8, %r8d
	movl	$4, %edx
	movabsq	$81604378624, %rsi
	orq	%rsi, %rcx
	movl	$5, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2034:
	movabsq	$81604378624, %rax
	movl	$8, %r8d
	movl	$4, %edx
	leal	128(,%rsi,8), %ecx
	movl	$5, %esi
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	call	_ZN2v88internal21RegisterConfiguration7DefaultEv@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	movq	%rax, %rbx
	movl	32(%rax), %eax
	testl	%eax, %eax
	jle	.L2025
	leaq	-116(%rbp), %rax
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	xorl	%r15d, %r15d
	movq	%rax, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L2024:
	movq	192(%rbx), %rdx
	movq	-136(%rbp), %rdi
	movl	%r13d, %esi
	movl	(%rdx,%r15,4), %r14d
	movl	%r15d, %edx
	negl	%edx
	leal	-24(,%rdx,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-116(%rbp), %r8
	movl	-108(%rbp), %edx
	movq	%r8, -104(%rbp)
	movl	%edx, -96(%rbp)
	movq	%r8, -92(%rbp)
	movl	%edx, -84(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2026
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%edx, -72(%rbp)
	movl	%edx, %r9d
	pushq	$0
	movl	$17, %esi
	movq	%r12, %rdi
	addq	$1, %r15
	pushq	$1
	pushq	$3
	movl	%edx, -60(%rbp)
	movl	%r14d, %edx
	movq	%r8, -80(%rbp)
	movq	%r8, -68(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	cmpl	%r15d, 32(%rbx)
	jg	.L2024
	jmp	.L2025
	.p2align 4,,10
	.p2align 3
.L2026:
	movl	%r14d, %ecx
	movq	%r8, %rsi
	movq	%r12, %rdi
	addq	$1, %r15
	call	_ZN2v88internal9Assembler5movsdENS0_7OperandENS0_11XMMRegisterE@PLT
	cmpl	%r15d, 32(%rbx)
	jg	.L2024
	jmp	.L2025
.L2036:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24820:
	.size	_ZN2v88internal14MacroAssembler22EnterExitFrameEpilogueEib, .-_ZN2v88internal14MacroAssembler22EnterExitFrameEpilogueEib
	.section	.text._ZN2v88internal14MacroAssembler14EnterExitFrameEibNS0_10StackFrame4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler14EnterExitFrameEibNS0_10StackFrame4TypeE
	.type	_ZN2v88internal14MacroAssembler14EnterExitFrameEibNS0_10StackFrame4TypeE, @function
_ZN2v88internal14MacroAssembler14EnterExitFrameEibNS0_10StackFrame4TypeE:
.LFB24821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	movl	$1, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	movl	%ecx, %edx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal14MacroAssembler22EnterExitFramePrologueEbNS0_10StackFrame4TypeE
	movl	_ZN2v88internalL3r14E(%rip), %edx
	leaq	-64(%rbp), %rdi
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movl	$8, %r8d
	movl	$3, %ecx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3r15E(%rip), %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movzbl	%bl, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler22EnterExitFrameEpilogueEib
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2040
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2040:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24821:
	.size	_ZN2v88internal14MacroAssembler14EnterExitFrameEibNS0_10StackFrame4TypeE, .-_ZN2v88internal14MacroAssembler14EnterExitFrameEibNS0_10StackFrame4TypeE
	.section	.text._ZN2v88internal14MacroAssembler17EnterApiExitFrameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler17EnterApiExitFrameEi
	.type	_ZN2v88internal14MacroAssembler17EnterApiExitFrameEi, @function
_ZN2v88internal14MacroAssembler17EnterApiExitFrameEi:
.LFB24822:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	xorl	%esi, %esi
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	_ZN2v88internal14MacroAssembler22EnterExitFramePrologueEbNS0_10StackFrame4TypeE
	movl	%r13d, %esi
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14MacroAssembler22EnterExitFrameEpilogueEib
	.cfi_endproc
.LFE24822:
	.size	_ZN2v88internal14MacroAssembler17EnterApiExitFrameEi, .-_ZN2v88internal14MacroAssembler17EnterApiExitFrameEi
	.section	.text._ZN2v88internal14MacroAssembler22LeaveExitFrameEpilogueEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler22LeaveExitFrameEpilogueEv
	.type	_ZN2v88internal14MacroAssembler22LeaveExitFrameEpilogueEv, @function
_ZN2v88internal14MacroAssembler22LeaveExitFrameEpilogueEv:
.LFB24825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	512(%rdi), %rsi
	movl	$3, %edi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movq	%r12, %rdi
	movq	%rax, %rsi
	movl	%r13d, %edx
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%edx, %ecx
	movq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	512(%r12), %rsi
	movl	$1, %edi
	call	_ZN2v88internal17ExternalReference6CreateENS0_16IsolateAddressIdEPNS0_7IsolateE@PLT
	movl	%r13d, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE
	movq	%rax, %rsi
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2047
	addq	$48, %rsp
	movq	%r12, %rdi
	movl	$8, %r8d
	movabsq	$81604378624, %rcx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
.L2047:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24825:
	.size	_ZN2v88internal14MacroAssembler22LeaveExitFrameEpilogueEv, .-_ZN2v88internal14MacroAssembler22LeaveExitFrameEpilogueEv
	.section	.text._ZN2v88internal14MacroAssembler17LeaveApiExitFrameEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler17LeaveApiExitFrameEv
	.type	_ZN2v88internal14MacroAssembler17LeaveApiExitFrameEv, @function
_ZN2v88internal14MacroAssembler17LeaveApiExitFrameEv:
.LFB24824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	$8, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	%r13d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	%r12, %rdi
	movl	%r13d, %esi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal14MacroAssembler22LeaveExitFrameEpilogueEv
	.cfi_endproc
.LFE24824:
	.size	_ZN2v88internal14MacroAssembler17LeaveApiExitFrameEv, .-_ZN2v88internal14MacroAssembler17LeaveApiExitFrameEv
	.section	.text._ZN2v88internal14MacroAssembler14LeaveExitFrameEbb,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler14LeaveExitFrameEbb
	.type	_ZN2v88internal14MacroAssembler14LeaveExitFrameEbb, @function
_ZN2v88internal14MacroAssembler14LeaveExitFrameEbb:
.LFB24823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movl	%edx, -132(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%sil, %sil
	jne	.L2051
.L2054:
	cmpb	$0, -132(%rbp)
	je	.L2062
	movl	_ZN2v88internalL3rbpE(%rip), %r14d
	leaq	-80(%rbp), %r13
	movl	$8, %edx
	movq	%r13, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rcxE(%rip), %r15d
	movl	$8, %r8d
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3r15E(%rip), %esi
	movl	$8, %edx
	movq	%r13, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L2058:
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler22LeaveExitFrameEpilogueEv
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2063
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2062:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5leaveEv@PLT
	jmp	.L2058
	.p2align 4,,10
	.p2align 3
.L2051:
	call	_ZN2v88internal21RegisterConfiguration7DefaultEv@PLT
	movq	%rax, %rbx
	movl	32(%rax), %eax
	testl	%eax, %eax
	jle	.L2054
	xorl	%r13d, %r13d
	leaq	-116(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L2057:
	movq	192(%rbx), %rdx
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	%r14, %rdi
	movl	(%rdx,%r13,4), %r15d
	movl	%r13d, %edx
	negl	%edx
	leal	-24(,%rdx,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-116(%rbp), %r8
	movl	-108(%rbp), %r9d
	movq	%r8, -104(%rbp)
	movl	%r9d, -96(%rbp)
	movq	%r8, -92(%rbp)
	movl	%r9d, -84(%rbp)
	testb	$32, _ZN2v88internal11CpuFeatures10supported_E(%rip)
	je	.L2055
	subq	$8, %rsp
	movl	_ZN2v88internalL4xmm0E(%rip), %ecx
	movl	%r15d, %edx
	movq	%r12, %rdi
	pushq	$0
	movl	$16, %esi
	addq	$1, %r13
	pushq	$1
	pushq	$3
	movq	%r8, -80(%rbp)
	movl	%r9d, -72(%rbp)
	movq	%r8, -68(%rbp)
	movl	%r9d, -60(%rbp)
	call	_ZN2v88internal9Assembler6vinstrEhNS0_11XMMRegisterES2_NS0_7OperandENS1_10SIMDPrefixENS1_13LeadingOpcodeENS1_4VexWE@PLT
	addq	$32, %rsp
	cmpl	%r13d, 32(%rbx)
	jg	.L2057
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L2055:
	movl	-84(%rbp), %ecx
	movq	%r8, %rdx
	movl	%r15d, %esi
	movq	%r12, %rdi
	addq	$1, %r13
	call	_ZN2v88internal9Assembler5movsdENS0_11XMMRegisterENS0_7OperandE@PLT
	cmpl	%r13d, 32(%rbx)
	jg	.L2057
	jmp	.L2054
.L2063:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24823:
	.size	_ZN2v88internal14MacroAssembler14LeaveExitFrameEbb, .-_ZN2v88internal14MacroAssembler14LeaveExitFrameEbb
	.section	.text._ZN2v88internal14MacroAssembler21LoadNativeContextSlotEiNS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler21LoadNativeContextSlotEiNS0_8RegisterE
	.type	_ZN2v88internal14MacroAssembler21LoadNativeContextSlotEiNS0_8RegisterE, @function
_ZN2v88internal14MacroAssembler21LoadNativeContextSlotEiNS0_8RegisterE:
.LFB24826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	leaq	-52(%rbp), %rdi
	.cfi_offset 12, -32
	movl	%edx, %r12d
	movl	$39, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$104, %rsp
	movl	_ZN2v88internalL3rsiE(%rip), %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-44(%rbp), %ecx
	movq	-52(%rbp), %rdx
	movl	%r12d, %esi
	movl	$8, %r8d
	movq	%r13, %rdi
	movl	%ecx, -80(%rbp)
	movq	%rdx, -88(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	leal	15(,%rbx,8), %edx
	leaq	-88(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-80(%rbp), %ecx
	movq	-88(%rbp), %rdx
	movl	%r12d, %esi
	movl	$8, %r8d
	movq	%r13, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2067
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2067:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24826:
	.size	_ZN2v88internal14MacroAssembler21LoadNativeContextSlotEiNS0_8RegisterE, .-_ZN2v88internal14MacroAssembler21LoadNativeContextSlotEiNS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler34ArgumentStackSlotsForCFunctionCallEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler34ArgumentStackSlotsForCFunctionCallEi
	.type	_ZN2v88internal14TurboAssembler34ArgumentStackSlotsForCFunctionCallEi, @function
_ZN2v88internal14TurboAssembler34ArgumentStackSlotsForCFunctionCallEi:
.LFB24827:
	.cfi_startproc
	endbr64
	leal	-6(%rsi), %eax
	cmpl	$5, %esi
	movl	$0, %edx
	cmovle	%edx, %eax
	ret
	.cfi_endproc
.LFE24827:
	.size	_ZN2v88internal14TurboAssembler34ArgumentStackSlotsForCFunctionCallEi, .-_ZN2v88internal14TurboAssembler34ArgumentStackSlotsForCFunctionCallEi
	.section	.text._ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi
	.type	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi, @function
_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi:
.LFB24828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v84base2OS24ActivationFrameAlignmentEv@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r13d
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rspE(%rip), %r14d
	movl	%eax, %ebx
	movl	%r13d, %esi
	movl	%r14d, %edx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	cmpl	$5, %r15d
	jle	.L2074
	leal	-40(,%r15,8), %ecx
	leal	-8(%rcx), %r15d
.L2072:
	movl	$8, %r8d
	movl	$4, %edx
	movq	%r12, %rdi
	negl	%ebx
	movabsq	$81604378624, %rax
	movl	$5, %esi
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movl	%ebx, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movabsq	$81604378624, %rax
	movl	$4, %edx
	movl	$4, %esi
	orq	%rax, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	leaq	-80(%rbp), %rdi
	movl	%r15d, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movl	%r13d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2076
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2074:
	.cfi_restore_state
	xorl	%r15d, %r15d
	movl	$8, %ecx
	jmp	.L2072
.L2076:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24828:
	.size	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi, .-_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi
	.section	.text._ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	.type	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE, @function
_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE:
.LFB24661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, 529(%rdi)
	jne	.L2084
	cmpb	$0, 530(%rdi)
	jne	.L2087
	movzbl	%sil, %eax
	movq	%rax, %rdx
	salq	$32, %rdx
	testq	%rax, %rax
	je	.L2088
	movl	$8, %r8d
	movl	$19, %ecx
	movl	$2, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L2082:
	movq	512(%r12), %rax
	cmpb	$0, 536(%r12)
	leaq	41184(%rax), %rdi
	jne	.L2083
	movb	$1, 536(%r12)
	movl	$163, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE
	movb	$0, 536(%r12)
.L2084:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4int3Ev@PLT
	.p2align 4,,10
	.p2align 3
.L2088:
	.cfi_restore_state
	movl	$4, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2087:
	movzbl	536(%rdi), %ebx
	movzbl	%sil, %edx
	movb	$1, 536(%rdi)
	movabsq	$81604378624, %rsi
	orq	%rsi, %rdx
	movl	_ZN2v88internalL9arg_reg_1E(%rip), %esi
	movl	$4, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi
	call	_ZN2v88internal17ExternalReference17abort_with_reasonEv@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	%r13d, %esi
	call	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	movb	%bl, 536(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2083:
	.cfi_restore_state
	movl	$163, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE
	jmp	.L2084
	.cfi_endproc
.LFE24661:
	.size	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE, .-_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	.section	.text._ZN2v88internal14TurboAssembler5CheckENS0_9ConditionENS0_11AbortReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler5CheckENS0_9ConditionENS0_11AbortReasonE
	.type	_ZN2v88internal14TurboAssembler5CheckENS0_9ConditionENS0_11AbortReasonE, @function
_ZN2v88internal14TurboAssembler5CheckENS0_9ConditionENS0_11AbortReasonE:
.LFB24659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	movq	%r13, %rdx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2092
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2092:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24659:
	.size	_ZN2v88internal14TurboAssembler5CheckENS0_9ConditionENS0_11AbortReasonE, .-_ZN2v88internal14TurboAssembler5CheckENS0_9ConditionENS0_11AbortReasonE
	.section	.text._ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_3SmiE
	.type	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_3SmiE, @function
_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_3SmiE:
.LFB24712:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2099
.L2094:
	movq	%r14, %rax
	shrq	$32, %rax
	jne	.L2095
	movl	$8, %ecx
	movl	%r13d, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterES2_i@PLT
.L2093:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2100
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2095:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$19, %ecx
	movl	$10, %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	$10, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2099:
	movabsq	$81604378625, %rdx
	leaq	-48(%rbp), %r15
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$29, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2094
.L2100:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24712:
	.size	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_3SmiE, .-_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_3SmiE
	.section	.text._ZN2v88internal14TurboAssembler6AssertENS0_9ConditionENS0_11AbortReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler6AssertENS0_9ConditionENS0_11AbortReasonE
	.type	_ZN2v88internal14TurboAssembler6AssertENS0_9ConditionENS0_11AbortReasonE, @function
_ZN2v88internal14TurboAssembler6AssertENS0_9ConditionENS0_11AbortReasonE:
.LFB24657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2105
.L2101:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2106
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2105:
	.cfi_restore_state
	leaq	-48(%rbp), %r13
	movq	%rdi, %r12
	movl	%edx, %ebx
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2101
.L2106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24657:
	.size	_ZN2v88internal14TurboAssembler6AssertENS0_9ConditionENS0_11AbortReasonE, .-_ZN2v88internal14TurboAssembler6AssertENS0_9ConditionENS0_11AbortReasonE
	.section	.text._ZN2v88internal14MacroAssembler12AssertNotSmiENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler12AssertNotSmiENS0_8RegisterE
	.type	_ZN2v88internal14MacroAssembler12AssertNotSmiENS0_8RegisterE, @function
_ZN2v88internal14MacroAssembler12AssertNotSmiENS0_8RegisterE:
.LFB24796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2111
.L2107:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2112
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2111:
	.cfi_restore_state
	movq	%rdi, %r12
	leaq	-32(%rbp), %r13
	movabsq	$81604378625, %rdx
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	movq	$0, -32(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$19, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2107
.L2112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24796:
	.size	_ZN2v88internal14MacroAssembler12AssertNotSmiENS0_8RegisterE, .-_ZN2v88internal14MacroAssembler12AssertNotSmiENS0_8RegisterE
	.section	.text._ZN2v88internal14MacroAssembler9AssertSmiENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler9AssertSmiENS0_8RegisterE
	.type	_ZN2v88internal14MacroAssembler9AssertSmiENS0_8RegisterE, @function
_ZN2v88internal14MacroAssembler9AssertSmiENS0_8RegisterE:
.LFB24797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2117
.L2113:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2118
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2117:
	.cfi_restore_state
	movq	%rdi, %r12
	leaq	-32(%rbp), %r13
	movabsq	$81604378625, %rdx
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	movq	$0, -32(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$29, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2113
.L2118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24797:
	.size	_ZN2v88internal14MacroAssembler9AssertSmiENS0_8RegisterE, .-_ZN2v88internal14MacroAssembler9AssertSmiENS0_8RegisterE
	.section	.text._ZN2v88internal14MacroAssembler9AssertSmiENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler9AssertSmiENS0_7OperandE
	.type	_ZN2v88internal14MacroAssembler9AssertSmiENS0_7OperandE, @function
_ZN2v88internal14MacroAssembler9AssertSmiENS0_7OperandE:
.LFB24798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2123
.L2119:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2124
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2123:
	.cfi_restore_state
	movq	%rdi, %r12
	leaq	-44(%rbp), %r13
	movabsq	$81604378625, %rcx
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	movq	$0, -44(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$29, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2119
.L2124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24798:
	.size	_ZN2v88internal14MacroAssembler9AssertSmiENS0_7OperandE, .-_ZN2v88internal14MacroAssembler9AssertSmiENS0_7OperandE
	.section	.text._ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterES2_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterES2_
	.type	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterES2_, @function
_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterES2_:
.LFB24711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2131
.L2127:
	movl	$8, %r8d
	movl	%r14d, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2132
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2131:
	.cfi_restore_state
	movabsq	$81604378624, %rbx
	leaq	-64(%rbp), %r15
	movq	%rbx, %rdx
	orq	$1, %rdx
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$29, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	cmpb	$0, 208(%r12)
	je	.L2127
	movl	$1, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	movq	$0, -64(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$29, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2127
.L2132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24711:
	.size	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterES2_, .-_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterES2_
	.section	.text._ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_7OperandE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_7OperandE
	.type	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_7OperandE, @function
_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_7OperandE:
.LFB24714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r8
	movq	%rdx, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -48
	movq	%rdx, -96(%rbp)
	movl	%ecx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2141
.L2134:
	movl	$8, %r9d
	movq	%r10, %rcx
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2142
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2141:
	.cfi_restore_state
	movabsq	$81604378624, %rbx
	leaq	-72(%rbp), %r14
	movq	%rbx, %rdx
	orq	$1, %rdx
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	movq	$0, -72(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$29, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-96(%rbp), %r10
	movl	-88(%rbp), %r8d
	cmpb	$0, 208(%r12)
	je	.L2134
	movl	$1, %ecx
	movq	%r10, %rsi
	movl	%r8d, %edx
	movq	%r12, %rdi
	orq	%rbx, %rcx
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	movq	$0, -72(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$29, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-96(%rbp), %r10
	movl	-88(%rbp), %r8d
	jmp	.L2134
.L2142:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24714:
	.size	_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_7OperandE, .-_ZN2v88internal14MacroAssembler10SmiCompareENS0_8RegisterENS0_7OperandE
	.section	.text._ZN2v88internal14MacroAssembler10SmiCompareENS0_7OperandENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler10SmiCompareENS0_7OperandENS0_8RegisterE
	.type	_ZN2v88internal14MacroAssembler10SmiCompareENS0_7OperandENS0_8RegisterE, @function
_ZN2v88internal14MacroAssembler10SmiCompareENS0_7OperandENS0_8RegisterE:
.LFB24715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -48
	movq	%rsi, -96(%rbp)
	movl	%edx, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2151
.L2145:
	movq	-96(%rbp), %rcx
	movl	-88(%rbp), %r8d
	movl	%r13d, %edx
	movl	$57, %esi
	movl	$8, %r9d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2152
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2151:
	.cfi_restore_state
	movabsq	$81604378624, %rbx
	leaq	-72(%rbp), %r14
	movq	%rbx, %rcx
	orq	$1, %rcx
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	movq	$0, -72(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$29, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	cmpb	$0, 208(%r12)
	je	.L2145
	movl	$1, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	orq	%rbx, %rdx
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	movq	$0, -72(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$29, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2145
.L2152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24715:
	.size	_ZN2v88internal14MacroAssembler10SmiCompareENS0_7OperandENS0_8RegisterE, .-_ZN2v88internal14MacroAssembler10SmiCompareENS0_7OperandENS0_8RegisterE
	.section	.text._ZN2v88internal14MacroAssembler10SmiCompareENS0_7OperandENS0_3SmiE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler10SmiCompareENS0_7OperandENS0_3SmiE
	.type	_ZN2v88internal14MacroAssembler10SmiCompareENS0_7OperandENS0_3SmiE, @function
_ZN2v88internal14MacroAssembler10SmiCompareENS0_7OperandENS0_3SmiE:
.LFB24716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rcx, %rbx
	subq	$88, %rsp
	movq	%rsi, -112(%rbp)
	movl	%edx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2157
.L2154:
	movq	-112(%rbp), %rsi
	movl	-104(%rbp), %edx
	sarq	$32, %rbx
	leaq	-76(%rbp), %rdi
	movl	$4, %ecx
	movl	%ebx, %ebx
	call	_ZN2v88internal7OperandC1ES1_i@PLT
	movl	-68(%rbp), %ecx
	movq	-76(%rbp), %rdx
	movq	%r12, %rdi
	movabsq	$81604378624, %r8
	movl	$4, %r9d
	movl	$7, %esi
	orq	%rbx, %r8
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2158
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2157:
	.cfi_restore_state
	movabsq	$81604378625, %rcx
	leaq	-84(%rbp), %r13
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	movq	$0, -84(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$29, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2154
.L2158:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24716:
	.size	_ZN2v88internal14MacroAssembler10SmiCompareENS0_7OperandENS0_3SmiE, .-_ZN2v88internal14MacroAssembler10SmiCompareENS0_7OperandENS0_3SmiE
	.section	.text._ZN2v88internal14TurboAssembler18AssertZeroExtendedENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler18AssertZeroExtendedENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler18AssertZeroExtendedENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler18AssertZeroExtendedENS0_8RegisterE:
.LFB24799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2163
.L2159:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2164
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2163:
	.cfi_restore_state
	movq	%rdi, %r12
	movl	%esi, %r13d
	movl	$8, %r8d
	movabsq	$4294967296, %rdx
	movl	$19, %ecx
	movl	$10, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	%r13d, %ecx
	movq	%r12, %rdi
	leaq	-32(%rbp), %r13
	movl	$8, %r8d
	movl	$10, %edx
	movl	$59, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	movl	$3, %esi
	movq	$0, -32(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2159
.L2164:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24799:
	.size	_ZN2v88internal14TurboAssembler18AssertZeroExtendedENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler18AssertZeroExtendedENS0_8RegisterE
	.section	.text._ZN2v88internal14MacroAssembler14AssertFunctionENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler14AssertFunctionENS0_8RegisterE
	.type	_ZN2v88internal14MacroAssembler14AssertFunctionENS0_8RegisterE, @function
_ZN2v88internal14MacroAssembler14AssertFunctionENS0_8RegisterE:
.LFB24801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2169
.L2165:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2170
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2169:
	.cfi_restore_state
	movq	%rdi, %r12
	leaq	-48(%rbp), %r14
	movl	%esi, %r13d
	movabsq	$81604378625, %rdx
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$22, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	%r13d, %ecx
	movl	$1105, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$27, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2165
.L2170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24801:
	.size	_ZN2v88internal14MacroAssembler14AssertFunctionENS0_8RegisterE, .-_ZN2v88internal14MacroAssembler14AssertFunctionENS0_8RegisterE
	.section	.text._ZN2v88internal14MacroAssembler19AssertBoundFunctionENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler19AssertBoundFunctionENS0_8RegisterE
	.type	_ZN2v88internal14MacroAssembler19AssertBoundFunctionENS0_8RegisterE, @function
_ZN2v88internal14MacroAssembler19AssertBoundFunctionENS0_8RegisterE:
.LFB24802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2175
.L2171:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2176
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2175:
	.cfi_restore_state
	movq	%rdi, %r12
	leaq	-48(%rbp), %r14
	movl	%esi, %r13d
	movabsq	$81604378625, %rdx
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$20, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	%r13d, %ecx
	movl	$1104, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler13CmpObjectTypeENS0_8RegisterENS0_12InstanceTypeES2_
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	movq	$0, -48(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$24, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2171
.L2176:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24802:
	.size	_ZN2v88internal14MacroAssembler19AssertBoundFunctionENS0_8RegisterE, .-_ZN2v88internal14MacroAssembler19AssertBoundFunctionENS0_8RegisterE
	.section	.text._ZN2v88internal14MacroAssembler21AssertGeneratorObjectENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler21AssertGeneratorObjectENS0_8RegisterE
	.type	_ZN2v88internal14MacroAssembler21AssertGeneratorObjectENS0_8RegisterE, @function
_ZN2v88internal14MacroAssembler21AssertGeneratorObjectENS0_8RegisterE:
.LFB24803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	je	.L2177
	movabsq	$81604378624, %rbx
	movq	%rdi, %r12
	leaq	-172(%rbp), %r14
	movl	%esi, %r13d
	movq	%rbx, %rdx
	leaq	-180(%rbp), %r15
	orq	$1, %rdx
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$5, %esi
	movq	$0, -172(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$23, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	leaq	-68(%rbp), %rdi
	movl	$-1, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%ecx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	leaq	-80(%rbp), %rdi
	movl	$11, %edx
	movl	%r13d, %esi
	movq	$0, -180(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%rbx, %r8
	orq	$1068, %r8
	movl	$7, %esi
	movq	%r12, %rdi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler26immediate_arithmetic_op_16EhNS0_7OperandENS0_9ImmediateE@PLT
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-104(%rbp), %rdi
	movl	$11, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-96(%rbp), %ecx
	movq	-104(%rbp), %rdx
	movq	%rbx, %r8
	orq	$1063, %r8
	movl	$7, %esi
	movq	%r12, %rdi
	orq	$1064, %rbx
	call	_ZN2v88internal9Assembler26immediate_arithmetic_op_16EhNS0_7OperandENS0_9ImmediateE@PLT
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	leaq	-128(%rbp), %rdi
	movl	$11, %edx
	movl	%r13d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-120(%rbp), %ecx
	movq	-128(%rbp), %rdx
	movq	%rbx, %r8
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler26immediate_arithmetic_op_16EhNS0_7OperandENS0_9ImmediateE@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$4, %esi
	movq	$0, -172(%rbp)
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$28, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
.L2177:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2182
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2182:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24803:
	.size	_ZN2v88internal14MacroAssembler21AssertGeneratorObjectENS0_8RegisterE, .-_ZN2v88internal14MacroAssembler21AssertGeneratorObjectENS0_8RegisterE
	.section	.text._ZN2v88internal14MacroAssembler17AssertConstructorENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler17AssertConstructorENS0_8RegisterE
	.type	_ZN2v88internal14MacroAssembler17AssertConstructorENS0_8RegisterE, @function
_ZN2v88internal14MacroAssembler17AssertConstructorENS0_8RegisterE:
.LFB24800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2187
.L2183:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2188
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2187:
	.cfi_restore_state
	movabsq	$81604378624, %rbx
	movq	%rdi, %r12
	leaq	-96(%rbp), %r14
	movl	%esi, %r13d
	movq	%rbx, %rdx
	orq	$64, %rbx
	orq	$1, %rdx
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	movq	$0, -96(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$21, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	leaq	-64(%rbp), %rdi
	movl	%r13d, %esi
	movl	$-1, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movl	%ecx, -68(%rbp)
	movq	%rdx, -76(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	leaq	-76(%rbp), %rdi
	movl	%r13d, %esi
	movl	$13, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-68(%rbp), %edx
	movq	-76(%rbp), %rsi
	movq	%rbx, %rcx
	movq	%r12, %rdi
	movl	%edx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_9ImmediateE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$5, %esi
	movq	$0, -96(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$25, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2183
.L2188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24800:
	.size	_ZN2v88internal14MacroAssembler17AssertConstructorENS0_8RegisterE, .-_ZN2v88internal14MacroAssembler17AssertConstructorENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE
	.type	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE, @function
_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE:
.LFB24818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 3, -48
	movl	_ZN2v88internalL3rbpE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2194
.L2190:
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	$8, %ecx
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2195
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2194:
	.cfi_restore_state
	movl	%esi, %ebx
	leaq	-76(%rbp), %rdi
	movl	%r13d, %esi
	movl	$-8, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-68(%rbp), %ecx
	movq	-76(%rbp), %rdx
	leal	(%rbx,%rbx), %esi
	movl	$8, %r9d
	movq	%r12, %rdi
	leaq	-84(%rbp), %r14
	movabsq	$81604378624, %r8
	orq	%rsi, %r8
	movl	$7, %esi
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_7OperandENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	movq	$0, -84(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$36, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2190
.L2195:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24818:
	.size	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE, .-_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE
	.section	.text._ZN2v88internal14MacroAssembler14CheckDebugHookENS0_8RegisterES2_RKNS0_14ParameterCountES5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler14CheckDebugHookENS0_8RegisterES2_RKNS0_14ParameterCountES5_
	.type	_ZN2v88internal14MacroAssembler14CheckDebugHookENS0_8RegisterES2_RKNS0_14ParameterCountES5_, @function
_ZN2v88internal14MacroAssembler14CheckDebugHookENS0_8RegisterES2_RKNS0_14ParameterCountES5_:
.LFB24814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	leaq	-152(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$152, %rsp
	movl	%edx, -164(%rbp)
	movq	512(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -152(%rbp)
	call	_ZN2v88internal17ExternalReference35debug_hook_on_function_call_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE
	movl	$7, %esi
	movq	%r12, %rdi
	movabsq	$81604378624, %r8
	movl	%edx, %ecx
	movl	%edx, -108(%rbp)
	movl	%edx, -60(%rbp)
	movq	%rax, %rdx
	movq	%rax, -116(%rbp)
	movq	%rax, -68(%rbp)
	call	_ZN2v88internal9Assembler25immediate_arithmetic_op_8EhNS0_7OperandENS0_9ImmediateE@PLT
	movl	$1, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movzbl	536(%r12), %eax
	movb	$1, 536(%r12)
	movb	%al, -177(%rbp)
	testb	%al, %al
	je	.L2197
	movl	$0, -168(%rbp)
.L2198:
	movl	(%r14), %esi
	cmpl	$-1, %esi
	je	.L2199
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$4, %ecx
	movabsq	$81604378656, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	movl	(%r14), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L2199:
	movl	(%rbx), %esi
	cmpl	$-1, %esi
	je	.L2200
	movl	$8, %r8d
	movl	$4, %ecx
	movq	%r12, %rdi
	movabsq	$81604378624, %rax
	movq	%rax, %rdx
	orq	$32, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
	movl	(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	(%rbx), %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movabsq	$81604378624, %rax
	movl	$7, %ecx
	orq	$32, %rax
	movq	%rax, %rdx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
.L2200:
	movl	-164(%rbp), %eax
	cmpl	$-1, %eax
	je	.L2201
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
.L2201:
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	(%rbx), %edx
	movl	$5, -144(%rbp)
	cmpl	$-1, %edx
	je	.L2202
	leaq	-68(%rbp), %rdi
	movl	$16, %r8d
	movl	$3, %ecx
	movl	$5, %esi
	movl	%edx, -140(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
.L2231:
	movq	-68(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movl	-60(%rbp), %eax
	movq	-80(%rbp), %rsi
	movl	%eax, %edx
	movl	%eax, -72(%rbp)
	movq	%rsi, -68(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rsi, -92(%rbp)
	movl	%eax, -60(%rbp)
	movl	%eax, -96(%rbp)
	movl	%eax, -84(%rbp)
	call	_ZN2v88internal9Assembler5pushqENS0_7OperandE@PLT
	movl	$72, %edi
	call	_ZN2v88internal7Runtime13FunctionForIdENS1_10FunctionIdE@PLT
	movl	_ZN2v88internalL3raxE(%rip), %esi
	movq	%r12, %rdi
	movsbq	24(%rax), %rdx
	movq	%rax, -176(%rbp)
	call	_ZN2v88internal14TurboAssembler3SetENS0_8RegisterEl
	movq	-176(%rbp), %rcx
	movq	%rcx, %rdi
	call	_ZN2v88internal17ExternalReference6CreateEPKNS0_7Runtime8FunctionE@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE
	movq	-176(%rbp), %rcx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	512(%r12), %rdi
	movsbl	25(%rcx), %esi
	xorl	%ecx, %ecx
	call	_ZN2v88internal11CodeFactory6CEntryEPNS0_7IsolateEiNS0_14SaveFPRegsModeENS0_8ArgvModeEb@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	-164(%rbp), %eax
	cmpl	$-1, %eax
	je	.L2204
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
.L2204:
	movl	(%rbx), %esi
	cmpl	$-1, %esi
	je	.L2205
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	(%rbx), %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	movabsq	$81604378656, %rdx
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
.L2205:
	movl	(%r14), %esi
	cmpl	$-1, %esi
	je	.L2206
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4popqENS0_8RegisterE@PLT
	movl	(%r14), %esi
	movl	$7, %ecx
	movq	%r12, %rdi
	movabsq	$81604378656, %rdx
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler5shiftENS0_8RegisterENS0_9ImmediateEii@PLT
.L2206:
	movl	-168(%rbp), %eax
	testl	%eax, %eax
	jne	.L2232
.L2207:
	movzbl	-177(%rbp), %eax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movb	%al, 536(%r12)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2233
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2197:
	.cfi_restore_state
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5pushqENS0_8RegisterE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbpE(%rip), %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movl	$34, %esi
	movq	%r12, %rdi
	movabsq	$81604378624, %r8
	orq	%r8, %rsi
	call	_ZN2v88internal9Assembler5pushqENS0_9ImmediateE@PLT
	movl	$17, -168(%rbp)
	jmp	.L2198
	.p2align 4,,10
	.p2align 3
.L2202:
	movzwl	4(%rbx), %eax
	leaq	-68(%rbp), %rdi
	movl	$5, %esi
	leal	16(,%rax,8), %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	jmp	.L2231
	.p2align 4,,10
	.p2align 3
.L2232:
	movl	-168(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler10LeaveFrameENS0_10StackFrame4TypeE
	jmp	.L2207
.L2233:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24814:
	.size	_ZN2v88internal14MacroAssembler14CheckDebugHookENS0_8RegisterES2_RKNS0_14ParameterCountES5_, .-_ZN2v88internal14MacroAssembler14CheckDebugHookENS0_8RegisterES2_RKNS0_14ParameterCountES5_
	.section	.text._ZN2v88internal14MacroAssembler18InvokeFunctionCodeENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler18InvokeFunctionCodeENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag
	.type	_ZN2v88internal14MacroAssembler18InvokeFunctionCodeENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag, @function
_ZN2v88internal14MacroAssembler18InvokeFunctionCodeENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag:
.LFB24812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r8, -136(%rbp)
	call	_ZN2v88internal14MacroAssembler14CheckDebugHookENS0_8RegisterES2_RKNS0_14ParameterCountES5_
	cmpl	$-1, %r15d
	movq	-136(%rbp), %r10
	je	.L2244
.L2235:
	subq	$8, %rsp
	leaq	-112(%rbp), %r15
	movq	%r10, %rdx
	movl	%ebx, %r9d
	pushq	$0
	leaq	-113(%rbp), %r8
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, -112(%rbp)
	movb	$0, -113(%rbp)
	call	_ZN2v88internal14MacroAssembler14InvokePrologueERKNS0_14ParameterCountES4_PNS0_5LabelEPb10InvokeFlagNS5_8DistanceE
	cmpb	$0, -113(%rbp)
	popq	%rax
	popq	%rdx
	je	.L2245
.L2234:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2246
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2244:
	.cfi_restore_state
	movq	(%r12), %rax
	leaq	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE(%rip), %rdx
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2236
	movl	$4, %edi
	call	_ZN2v88internal18TurboAssemblerBase30RootRegisterOffsetForRootIndexENS0_9RootIndexE@PLT
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-68(%rbp), %rdi
	movl	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rdx
	movl	-60(%rbp), %ecx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$2, %esi
	movq	%rdx, -80(%rbp)
	movl	%ecx, -72(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-136(%rbp), %r10
	jmp	.L2235
	.p2align 4,,10
	.p2align 3
.L2245:
	leaq	-80(%rbp), %rdi
	movl	$47, %edx
	movl	%r14d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$1, %esi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	movq	%rdx, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movq	%rdx, -92(%rbp)
	movl	%ecx, -84(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	(%r12), %rax
	testl	%ebx, %ebx
	je	.L2247
	movq	48(%rax), %rdx
	leaq	_ZN2v88internal14TurboAssembler14JumpCodeObjectENS0_8RegisterE(%rip), %rcx
	cmpq	%rcx, %rdx
	jne	.L2241
	movl	_ZN2v88internalL3rcxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%r13d, %esi
	movl	%r13d, %edx
	call	*56(%rax)
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler3jmpENS0_8RegisterE@PLT
.L2240:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2234
	.p2align 4,,10
	.p2align 3
.L2247:
	movq	40(%rax), %rdx
	leaq	_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE(%rip), %rcx
	cmpq	%rcx, %rdx
	jne	.L2241
	movl	_ZN2v88internalL3rcxE(%rip), %r13d
	movq	%r12, %rdi
	movl	%r13d, %esi
	movl	%r13d, %edx
	call	*56(%rax)
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	jmp	.L2240
	.p2align 4,,10
	.p2align 3
.L2236:
	movq	%r10, -136(%rbp)
	movl	_ZN2v88internalL3rdxE(%rip), %esi
	movl	$4, %edx
	movq	%r12, %rdi
	call	*%rax
	movq	-136(%rbp), %r10
	jmp	.L2235
	.p2align 4,,10
	.p2align 3
.L2241:
	movl	_ZN2v88internalL3rcxE(%rip), %esi
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L2240
.L2246:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24812:
	.size	_ZN2v88internal14MacroAssembler18InvokeFunctionCodeENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag, .-_ZN2v88internal14MacroAssembler18InvokeFunctionCodeENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag
	.section	.text._ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag
	.type	_ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag, @function
_ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag:
.LFB24811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r9d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	movl	$31, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$6, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r15d, %r9d
	movq	%rbx, %r8
	movq	%r14, %rcx
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	movl	%r13d, %edx
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler18InvokeFunctionCodeENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2251
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2251:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24811:
	.size	_ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag, .-_ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag
	.section	.text._ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountE10InvokeFlag,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountE10InvokeFlag
	.type	_ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountE10InvokeFlag, @function
_ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountE10InvokeFlag:
.LFB24810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	leaq	-68(%rbp), %r9
	.cfi_offset 14, -32
	movl	%edx, %r14d
	movl	$23, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r9, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r9, -168(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$3, %esi
	movl	%ecx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movl	$41, %edx
	movq	-168(%rbp), %r9
	movq	%r9, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-60(%rbp), %ecx
	movq	-68(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL3rbxE(%rip), %esi
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler11emit_movzxwENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%eax, %eax
	leaq	-80(%rbp), %rdi
	movl	$31, %edx
	movl	%r13d, %esi
	movw	%ax, -156(%rbp)
	movl	$3, -160(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rdx
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$6, %esi
	movq	%rdx, -68(%rbp)
	movl	%ecx, -60(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	%r15d, %r9d
	movq	%rbx, %r8
	movl	%r14d, %edx
	movl	_ZN2v88internalL3rdiE(%rip), %esi
	leaq	-160(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler18InvokeFunctionCodeENS0_8RegisterES2_RKNS0_14ParameterCountES5_10InvokeFlag
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2255
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2255:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24810:
	.size	_ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountE10InvokeFlag, .-_ZN2v88internal14MacroAssembler14InvokeFunctionENS0_8RegisterES2_RKNS0_14ParameterCountE10InvokeFlag
	.section	.text._ZN2v88internal14MacroAssembler31AssertUndefinedOrAllocationSiteENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler31AssertUndefinedOrAllocationSiteENS0_8RegisterE
	.type	_ZN2v88internal14MacroAssembler31AssertUndefinedOrAllocationSiteENS0_8RegisterE, @function
_ZN2v88internal14MacroAssembler31AssertUndefinedOrAllocationSiteENS0_8RegisterE:
.LFB24804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2261
.L2256:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2262
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2261:
	.cfi_restore_state
	movq	%rdi, %r12
	leaq	-88(%rbp), %r15
	leaq	-96(%rbp), %rbx
	movl	%esi, %r13d
	movabsq	$81604378625, %rdx
	movq	$0, -96(%rbp)
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	$0, -88(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$19, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	512(%r12), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	leaq	88(%rax), %rdx
	call	_ZN2v88internal14MacroAssembler3CmpENS0_8RegisterENS0_6HandleINS0_6ObjectEEE
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	512(%r12), %rax
	leaq	-80(%rbp), %rdi
	movl	%r13d, %esi
	movl	$-1, %edx
	leaq	4296(%rax), %r14
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler3CmpENS0_7OperandENS0_6HandleINS0_6ObjectEEE
	cmpb	$0, 208(%r12)
	jne	.L2263
.L2258:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2263:
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	movq	$0, -88(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	movl	$6, %esi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2258
.L2262:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24804:
	.size	_ZN2v88internal14MacroAssembler31AssertUndefinedOrAllocationSiteENS0_8RegisterE, .-_ZN2v88internal14MacroAssembler31AssertUndefinedOrAllocationSiteENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler18PrepareForTailCallERKNS0_14ParameterCountENS0_8RegisterES5_S5_,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler18PrepareForTailCallERKNS0_14ParameterCountENS0_8RegisterES5_S5_
	.type	_ZN2v88internal14TurboAssembler18PrepareForTailCallERKNS0_14ParameterCountENS0_8RegisterES5_S5_, @function
_ZN2v88internal14TurboAssembler18PrepareForTailCallERKNS0_14ParameterCountENS0_8RegisterES5_S5_:
.LFB24809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$88, %rsp
	movq	%rsi, -112(%rbp)
	movl	(%rsi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$-1, %ecx
	je	.L2265
	movl	$8, %r8d
	movl	$43, %esi
	leaq	-80(%rbp), %r14
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movl	_ZN2v88internalL3rbpE(%rip), %r9d
	movl	$8, %r8d
.L2273:
	movl	%r9d, %esi
	movl	$3, %ecx
	movl	%r13d, %edx
	movq	%r14, %rdi
	movl	%r9d, -104(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%ebx, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	leaq	-88(%rbp), %rax
	cmpb	$0, _ZN2v88internal15FLAG_debug_codeE(%rip)
	movl	-104(%rbp), %r9d
	movq	%rax, -104(%rbp)
	jne	.L2274
.L2267:
	movl	%r9d, %esi
	movl	$8, %edx
	movq	%r14, %rdi
	movl	%r9d, -116(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r15d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movl	%r15d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movl	-116(%rbp), %r9d
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	%r9d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-116(%rbp), %r9d
	movl	-72(%rbp), %ecx
	movq	%r12, %rdi
	movq	-80(%rbp), %rdx
	movl	$8, %r8d
	movl	%r9d, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-112(%rbp), %rax
	movl	(%rax), %esi
	cmpl	$-1, %esi
	je	.L2268
	movl	$2, %edx
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
.L2269:
	movq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN2v88internal9Assembler3jmpEPNS0_5LabelENS2_8DistanceE@PLT
	leaq	-96(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	movq	%r9, -112(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	$8, %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_decENS0_8RegisterEi@PLT
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	%r13d, %edx
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r15d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	xorl	%r8d, %r8d
	movl	$3, %ecx
	movl	%r13d, %edx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterES2_NS0_11ScaleFactorEi@PLT
	movl	-72(%rbp), %edx
	movq	-80(%rbp), %rsi
	movl	%r15d, %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	%r13d, %edx
	movl	$7, %esi
	movq	%r12, %rdi
	movabsq	$81604378624, %rcx
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	movq	-112(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	_ZN2v88internalL3rspE(%rip), %esi
	movl	%ebx, %edx
	movq	%r12, %rdi
	movl	$8, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterES2_i@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2275
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2265:
	.cfi_restore_state
	movzwl	4(%rsi), %eax
	movl	$1, %r8d
	movl	_ZN2v88internalL3rbpE(%rip), %r9d
	leaq	-80(%rbp), %r14
	subl	%eax, %r8d
	sall	$3, %r8d
	jmp	.L2273
	.p2align 4,,10
	.p2align 3
.L2268:
	movzwl	4(%rax), %edx
	movl	$8, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movabsq	$81604378624, %rax
	addq	$2, %rdx
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L2269
	.p2align 4,,10
	.p2align 3
.L2274:
	movl	$8, %r8d
	movl	%ebx, %ecx
	movl	$4, %edx
	movq	%r12, %rdi
	movl	$59, %esi
	movl	%r9d, -116(%rbp)
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
	movq	-104(%rbp), %rdx
	xorl	%ecx, %ecx
	movl	$2, %esi
	movq	%r12, %rdi
	movq	$0, -88(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$35, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	-104(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	-116(%rbp), %r9d
	jmp	.L2267
.L2275:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24809:
	.size	_ZN2v88internal14TurboAssembler18PrepareForTailCallERKNS0_14ParameterCountENS0_8RegisterES5_S5_, .-_ZN2v88internal14TurboAssembler18PrepareForTailCallERKNS0_14ParameterCountENS0_8RegisterES5_S5_
	.section	.text._ZN2v88internal14TurboAssembler17AssertUnreachableENS0_11AbortReasonE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler17AssertUnreachableENS0_11AbortReasonE
	.type	_ZN2v88internal14TurboAssembler17AssertUnreachableENS0_11AbortReasonE, @function
_ZN2v88internal14TurboAssembler17AssertUnreachableENS0_11AbortReasonE:
.LFB24658:
	.cfi_startproc
	endbr64
	cmpb	$0, 208(%rdi)
	jne	.L2288
	ret
	.p2align 4,,10
	.p2align 3
.L2288:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	cmpb	$0, 529(%rdi)
	jne	.L2283
	cmpb	$0, 530(%rdi)
	jne	.L2289
	movzbl	%sil, %eax
	movq	%rax, %rdx
	salq	$32, %rdx
	testq	%rax, %rax
	jne	.L2280
	movl	$4, %r8d
	movl	$2, %ecx
	movl	$2, %edx
	movl	$51, %esi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
.L2281:
	movq	512(%r12), %rax
	cmpb	$0, 536(%r12)
	leaq	41184(%rax), %rdi
	jne	.L2282
	movb	$1, 536(%r12)
	movl	$163, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE
	movb	$0, 536(%r12)
.L2283:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal9Assembler4int3Ev@PLT
	.p2align 4,,10
	.p2align 3
.L2289:
	.cfi_restore_state
	movzbl	536(%rdi), %ebx
	movzbl	%sil, %edx
	movb	$1, 536(%rdi)
	movabsq	$81604378624, %rsi
	orq	%rsi, %rdx
	movl	_ZN2v88internalL9arg_reg_1E(%rip), %esi
	movl	$4, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v88internal14TurboAssembler20PrepareCallCFunctionEi
	call	_ZN2v88internal17ExternalReference17abort_with_reasonEv@PLT
	movl	_ZN2v88internalL3raxE(%rip), %r13d
	movq	%r12, %rdi
	movq	%rax, %rdx
	movl	%r13d, %esi
	call	_ZN2v88internal14TurboAssembler11LoadAddressENS0_8RegisterENS0_17ExternalReferenceE
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	movb	%bl, 536(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2282:
	.cfi_restore_state
	movl	$163, %esi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler4CallENS0_6HandleINS0_4CodeEEENS0_9RelocInfo4ModeE
	jmp	.L2283
	.p2align 4,,10
	.p2align 3
.L2280:
	movl	$8, %r8d
	movl	$19, %ecx
	movl	$2, %esi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	jmp	.L2281
	.cfi_endproc
.LFE24658:
	.size	_ZN2v88internal14TurboAssembler17AssertUnreachableENS0_11AbortReasonE, .-_ZN2v88internal14TurboAssembler17AssertUnreachableENS0_11AbortReasonE
	.section	.text._ZN2v88internal14TurboAssembler13CallCFunctionENS0_8RegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_8RegisterEi
	.type	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_8RegisterEi, @function
_ZN2v88internal14TurboAssembler13CallCFunctionENS0_8RegisterEi:
.LFB24830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$104, %rsp
	movl	_ZN2v88internalL3rspE(%rip), %r13d
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2304
.L2292:
	cmpq	$0, 512(%r12)
	je	.L2294
	leaq	-124(%rbp), %r9
	leaq	-80(%rbp), %rdi
	xorl	%edx, %edx
	movq	$0, -124(%rbp)
	movq	%r9, %rsi
	movq	%r9, -136(%rbp)
	call	_ZN2v88internal7OperandC1EPNS0_5LabelEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movq	%r12, %rdi
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %r15d
	movl	$8, %r8d
	movl	%r15d, %esi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movq	-136(%rbp), %r9
	movq	%r12, %rdi
	movq	%r9, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movq	512(%r12), %rdi
	call	_ZN2v88internal17ExternalReference29fast_c_call_caller_pc_addressEPNS0_7IsolateE@PLT
	movl	%r15d, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE
	movl	$8, %r8d
	movl	%r15d, %ecx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
	movq	512(%r12), %rdi
	call	_ZN2v88internal17ExternalReference29fast_c_call_caller_fp_addressEPNS0_7IsolateE@PLT
	movq	%r12, %rdi
	movl	%r15d, %edx
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE
	movl	_ZN2v88internalL3rbpE(%rip), %ecx
	movl	$8, %r8d
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_8RegisterEi@PLT
.L2294:
	movq	%r12, %rdi
	movl	%r14d, %esi
	call	_ZN2v88internal9Assembler4callENS0_8RegisterE@PLT
	movq	512(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2295
	call	_ZN2v88internal17ExternalReference29fast_c_call_caller_fp_addressEPNS0_7IsolateE@PLT
	movl	_ZN2v88internalL16kScratchRegisterE(%rip), %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v88internal14TurboAssembler26ExternalReferenceAsOperandENS0_17ExternalReferenceENS0_8RegisterE
	movl	$8, %r8d
	movq	%r12, %rdi
	movabsq	$81604378624, %rcx
	movq	%rax, %rsi
	movq	%rax, -80(%rbp)
	movl	%edx, -72(%rbp)
	call	_ZN2v88internal9Assembler8emit_movENS0_7OperandENS0_9ImmediateEi@PLT
.L2295:
	cmpl	$5, %ebx
	movl	$0, %eax
	leaq	-116(%rbp), %rdi
	movl	%r13d, %esi
	leal	-48(,%rbx,8), %edx
	cmovle	%eax, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-108(%rbp), %ecx
	movq	-116(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_7OperandEi@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2305
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2304:
	.cfi_restore_state
	call	_ZN2v84base2OS24ActivationFrameAlignmentEv@PLT
	movl	_ZN2v88internalL3rspE(%rip), %r13d
	cmpl	$8, %eax
	jle	.L2292
	leal	-1(%rax), %edx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$8, %ecx
	movabsq	$81604378624, %rax
	leaq	-124(%rbp), %r15
	movq	$0, -124(%rbp)
	orq	%rax, %rdx
	call	_ZN2v88internal9Assembler9emit_testENS0_8RegisterENS0_9ImmediateEi@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2292
.L2305:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24830:
	.size	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_8RegisterEi, .-_ZN2v88internal14TurboAssembler13CallCFunctionENS0_8RegisterEi
	.section	.text._ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi
	.type	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi, @function
_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi:
.LFB24829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movl	_ZN2v88internalL3raxE(%rip), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 528(%rdi)
	movq	%rsi, -72(%rbp)
	je	.L2307
	cmpb	$0, 178(%rdi)
	jne	.L2308
	cmpb	$0, 180(%r12)
	jne	.L2320
.L2307:
	movq	-72(%rbp), %rdx
	movl	$8, %r8d
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$7, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
.L2312:
	movl	%r13d, %edx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_8RegisterEi
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2321
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2308:
	.cfi_restore_state
	movq	512(%rdi), %rdi
	leaq	-72(%rbp), %rsi
	call	_ZN2v88internal18TurboAssemblerBase38RootRegisterOffsetForExternalReferenceEPNS0_7IsolateERKNS0_17ExternalReferenceE@PLT
	movl	$4294967295, %ecx
	movq	%rax, %rdx
	movl	$2147483648, %eax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	jbe	.L2322
	cmpb	$0, 528(%r12)
	je	.L2307
	cmpb	$0, 180(%r12)
	je	.L2307
.L2320:
	movq	-72(%rbp), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal18TurboAssemblerBase29IndirectLoadExternalReferenceENS0_8RegisterENS0_17ExternalReferenceE@PLT
	jmp	.L2312
	.p2align 4,,10
	.p2align 3
.L2322:
	movl	_ZN2v88internalL13kRootRegisterE(%rip), %esi
	leaq	-52(%rbp), %rdi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-44(%rbp), %ecx
	movq	-52(%rbp), %rdx
	movl	%r14d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	jmp	.L2312
.L2321:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24829:
	.size	_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi, .-_ZN2v88internal14TurboAssembler13CallCFunctionENS0_17ExternalReferenceEi
	.section	.text._ZN2v88internal14TurboAssembler13CheckPageFlagENS0_8RegisterES2_iNS0_9ConditionEPNS0_5LabelENS4_8DistanceE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler13CheckPageFlagENS0_8RegisterES2_iNS0_9ConditionEPNS0_5LabelENS4_8DistanceE
	.type	_ZN2v88internal14TurboAssembler13CheckPageFlagENS0_8RegisterES2_iNS0_9ConditionEPNS0_5LabelENS4_8DistanceE, @function
_ZN2v88internal14TurboAssembler13CheckPageFlagENS0_8RegisterES2_iNS0_9ConditionEPNS0_5LabelENS4_8DistanceE:
.LFB24831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	%esi, %edx
	je	.L2330
	movl	%esi, -84(%rbp)
	movl	$8, %ecx
	movl	%r13d, %esi
	movabsq	$85899083776, %rdx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movl	-84(%rbp), %r10d
	movl	%r13d, %edx
	movq	%r12, %rdi
	movl	$8, %r8d
	movl	$35, %esi
	movl	%r10d, %ecx
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterES2_i@PLT
.L2325:
	cmpl	$255, %ebx
	jg	.L2326
	leaq	-68(%rbp), %rdi
	movl	$8, %edx
	movl	%r13d, %esi
	movzbl	%bl, %ebx
	movabsq	$81604378624, %rcx
	orq	%rcx, %rbx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-68(%rbp), %rsi
	movl	-60(%rbp), %edx
	movq	%rbx, %rcx
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5testbENS0_7OperandENS0_9ImmediateE@PLT
.L2327:
	movl	16(%rbp), %ecx
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2331
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2326:
	.cfi_restore_state
	leaq	-80(%rbp), %rdi
	movl	%r13d, %esi
	movl	$8, %edx
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movq	-80(%rbp), %rsi
	movl	-72(%rbp), %edx
	movl	%ebx, %ecx
	movabsq	$81604378624, %rbx
	movl	$4, %r8d
	movq	%r12, %rdi
	orq	%rbx, %rcx
	movq	%rsi, -68(%rbp)
	movl	%edx, -60(%rbp)
	call	_ZN2v88internal9Assembler9emit_testENS0_7OperandENS0_9ImmediateEi@PLT
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2330:
	movl	$8, %r8d
	movl	$4, %esi
	movabsq	$85899083776, %rcx
	call	_ZN2v88internal9Assembler23immediate_arithmetic_opEhNS0_8RegisterENS0_9ImmediateEi@PLT
	jmp	.L2325
.L2331:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24831:
	.size	_ZN2v88internal14TurboAssembler13CheckPageFlagENS0_8RegisterES2_iNS0_9ConditionEPNS0_5LabelENS4_8DistanceE, .-_ZN2v88internal14TurboAssembler13CheckPageFlagENS0_8RegisterES2_iNS0_9ConditionEPNS0_5LabelENS4_8DistanceE
	.section	.text._ZN2v88internal14MacroAssembler11RecordWriteENS0_8RegisterES2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler11RecordWriteENS0_8RegisterES2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE
	.type	_ZN2v88internal14MacroAssembler11RecordWriteENS0_8RegisterES2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE, @function
_ZN2v88internal14MacroAssembler11RecordWriteENS0_8RegisterES2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE:
.LFB24656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$88, %rsp
	movl	%edx, -116(%rbp)
	movl	%r8d, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 208(%rdi)
	jne	.L2344
	leaq	-100(%rbp), %r14
	cmpl	$1, %r9d
	je	.L2338
.L2336:
	movl	16(%rbp), %ecx
	movq	$0, -100(%rbp)
	testl	%ecx, %ecx
	je	.L2345
.L2337:
	subq	$8, %rsp
	movq	%r14, %r9
	movl	$4, %r8d
	movl	%r15d, %edx
	pushq	$0
	movl	$2, %ecx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler13CheckPageFlagENS0_8RegisterES2_iNS0_9ConditionEPNS0_5LabelENS4_8DistanceE
	movq	%r14, %r9
	movl	$4, %ecx
	movl	%r15d, %edx
	movl	$4, %r8d
	movl	%r13d, %esi
	movq	%r12, %rdi
	movl	$0, (%rsp)
	call	_ZN2v88internal14TurboAssembler13CheckPageFlagENS0_8RegisterES2_iNS0_9ConditionEPNS0_5LabelENS4_8DistanceE
	movq	512(%r12), %rax
	xorl	%esi, %esi
	leaq	41184(%rax), %rdi
	call	_ZN2v88internal8Builtins14builtin_handleEi@PLT
	movl	-116(%rbp), %edx
	movl	-120(%rbp), %r8d
	movl	%ebx, %ecx
	movq	%rax, %r9
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	$0, (%rsp)
	call	_ZN2v88internal14TurboAssembler19CallRecordWriteStubENS0_8RegisterES2_NS0_19RememberedSetActionENS0_14SaveFPRegsModeENS0_6HandleINS0_4CodeEEEm
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	popq	%rax
	popq	%rdx
	cmpb	$0, 208(%r12)
	jne	.L2346
.L2332:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2347
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2344:
	.cfi_restore_state
	movabsq	$81604378625, %rdx
	leaq	-100(%rbp), %r14
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$5, %esi
	movq	%r12, %rdi
	movq	$0, -100(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	$19, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14TurboAssembler5AbortENS0_11AbortReasonE
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	cmpl	$1, %ebx
	jne	.L2334
.L2338:
	cmpb	$0, _ZN2v88internal24FLAG_incremental_markingE(%rip)
	je	.L2332
	leaq	-100(%rbp), %r14
.L2334:
	cmpb	$0, 208(%r12)
	je	.L2336
	movl	-116(%rbp), %esi
	leaq	-92(%rbp), %rdi
	xorl	%edx, %edx
	movq	$0, -100(%rbp)
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-84(%rbp), %r8d
	movq	-92(%rbp), %rcx
	movl	%r15d, %edx
	movl	$8, %r9d
	movl	$59, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler13arithmetic_opEhNS0_8RegisterENS0_7OperandEi@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	16(%rbp), %ecx
	movq	$0, -100(%rbp)
	testl	%ecx, %ecx
	jne	.L2337
.L2345:
	movabsq	$81604378625, %rdx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	jmp	.L2337
	.p2align 4,,10
	.p2align 3
.L2346:
	movl	-116(%rbp), %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movabsq	$-2401053098003022097, %rdx
	movl	$19, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	$19, %ecx
	movl	%r15d, %esi
	movq	%r12, %rdi
	movabsq	$-2401053098003022097, %rdx
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	jmp	.L2332
.L2347:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24656:
	.size	_ZN2v88internal14MacroAssembler11RecordWriteENS0_8RegisterES2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE, .-_ZN2v88internal14MacroAssembler11RecordWriteENS0_8RegisterES2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE
	.section	.text._ZN2v88internal14MacroAssembler16RecordWriteFieldENS0_8RegisterEiS2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14MacroAssembler16RecordWriteFieldENS0_8RegisterEiS2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE
	.type	_ZN2v88internal14MacroAssembler16RecordWriteFieldENS0_8RegisterEiS2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE, @function
_ZN2v88internal14MacroAssembler16RecordWriteFieldENS0_8RegisterEiS2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE:
.LFB24649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	leaq	-108(%rbp), %rcx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%r8d, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$88, %rsp
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	%rcx, -120(%rbp)
	movl	24(%rbp), %ecx
	movq	$0, -108(%rbp)
	testl	%ecx, %ecx
	je	.L2355
.L2349:
	leal	-1(%rax), %edx
	leaq	-80(%rbp), %rdi
	movl	%r15d, %esi
	call	_ZN2v88internal7OperandC1ENS0_8RegisterEi@PLT
	movl	-72(%rbp), %ecx
	movq	-80(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	cmpb	$0, 208(%r12)
	jne	.L2356
.L2350:
	subq	$8, %rsp
	movl	16(%rbp), %r9d
	movl	%r13d, %edx
	movl	%ebx, %r8d
	pushq	$1
	movl	%r14d, %ecx
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal14MacroAssembler11RecordWriteENS0_8RegisterES2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE
	movq	-120(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	popq	%rax
	popq	%rdx
	cmpb	$0, 208(%r12)
	jne	.L2357
.L2348:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2358
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2355:
	.cfi_restore_state
	movl	%edx, -128(%rbp)
	movl	%r14d, %esi
	movabsq	$81604378625, %rdx
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	movq	-120(%rbp), %rdx
	movl	$1, %ecx
	movq	%r12, %rdi
	movl	$4, %esi
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movl	-128(%rbp), %eax
	jmp	.L2349
	.p2align 4,,10
	.p2align 3
.L2357:
	movl	%r14d, %esi
	movq	%r12, %rdi
	movl	$8, %r8d
	movabsq	$-2401053098003022097, %rdx
	movl	$19, %ecx
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	movl	$19, %ecx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movabsq	$-2401053098003022097, %rdx
	movl	$8, %r8d
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_11Immediate64Ei@PLT
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L2356:
	movl	%r13d, %esi
	movq	%r12, %rdi
	movabsq	$81604378631, %rdx
	movq	$0, -100(%rbp)
	call	_ZN2v88internal9Assembler5testbENS0_8RegisterENS0_9ImmediateE@PLT
	leaq	-100(%rbp), %r8
	xorl	%ecx, %ecx
	movl	$4, %esi
	movq	%r8, %rdx
	movq	%r12, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN2v88internal9Assembler1jENS0_9ConditionEPNS0_5LabelENS3_8DistanceE@PLT
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler4int3Ev@PLT
	movq	-128(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	jmp	.L2350
.L2358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24649:
	.size	_ZN2v88internal14MacroAssembler16RecordWriteFieldENS0_8RegisterEiS2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE, .-_ZN2v88internal14MacroAssembler16RecordWriteFieldENS0_8RegisterEiS2_S2_NS0_14SaveFPRegsModeENS0_19RememberedSetActionENS0_8SmiCheckE
	.section	.text._ZN2v88internal14TurboAssembler23ComputeCodeStartAddressENS0_8RegisterE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler23ComputeCodeStartAddressENS0_8RegisterE
	.type	_ZN2v88internal14TurboAssembler23ComputeCodeStartAddressENS0_8RegisterE, @function
_ZN2v88internal14TurboAssembler23ComputeCodeStartAddressENS0_8RegisterE:
.LFB24832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-72(%rbp), %r14
	movl	%esi, %r13d
	pushq	%r12
	movq	%r14, %rsi
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	call	_ZN2v88internal9Assembler4bindEPNS0_5LabelE@PLT
	movl	16(%r12), %edx
	leaq	-64(%rbp), %rdi
	movq	%r14, %rsi
	subl	32(%r12), %edx
	call	_ZN2v88internal7OperandC1EPNS0_5LabelEi@PLT
	movl	-56(%rbp), %ecx
	movq	-64(%rbp), %rdx
	movl	%r13d, %esi
	movl	$8, %r8d
	movq	%r12, %rdi
	call	_ZN2v88internal9Assembler8emit_leaENS0_8RegisterENS0_7OperandEi@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2362
	addq	$56, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2362:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE24832:
	.size	_ZN2v88internal14TurboAssembler23ComputeCodeStartAddressENS0_8RegisterE, .-_ZN2v88internal14TurboAssembler23ComputeCodeStartAddressENS0_8RegisterE
	.section	.text._ZN2v88internal14TurboAssembler30ResetSpeculationPoisonRegisterEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler30ResetSpeculationPoisonRegisterEv
	.type	_ZN2v88internal14TurboAssembler30ResetSpeculationPoisonRegisterEv, @function
_ZN2v88internal14TurboAssembler30ResetSpeculationPoisonRegisterEv:
.LFB24833:
	.cfi_startproc
	endbr64
	movl	$8, %ecx
	movl	$12, %esi
	movabsq	$85899345919, %rdx
	jmp	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	.cfi_endproc
.LFE24833:
	.size	_ZN2v88internal14TurboAssembler30ResetSpeculationPoisonRegisterEv, .-_ZN2v88internal14TurboAssembler30ResetSpeculationPoisonRegisterEv
	.section	.text._ZN2v88internal14TurboAssembler21CallForDeoptimizationEmi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal14TurboAssembler21CallForDeoptimizationEmi
	.type	_ZN2v88internal14TurboAssembler21CallForDeoptimizationEmi, @function
_ZN2v88internal14TurboAssembler21CallForDeoptimizationEmi:
.LFB24834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %edx
	movl	$8, %ecx
	movabsq	$81604378624, %rax
	orq	%rax, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movzbl	528(%rdi), %r13d
	movl	_ZN2v88internalL3r13E(%rip), %esi
	movb	$0, 528(%rdi)
	call	_ZN2v88internal9Assembler8emit_movENS0_8RegisterENS0_9ImmediateEi@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	$6, %edx
	call	_ZN2v88internal9Assembler4callEmNS0_9RelocInfo4ModeE@PLT
	movb	%r13b, 528(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE24834:
	.size	_ZN2v88internal14TurboAssembler21CallForDeoptimizationEmi, .-_ZN2v88internal14TurboAssembler21CallForDeoptimizationEmi
	.section	.text._ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,"axG",@progbits,_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.type	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, @function
_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E:
.LFB27410:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L2374
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
.L2368:
	movq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%rbx, %rdi
	movq	16(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2368
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2374:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE27410:
	.size	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E, .-_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	.section	.text._ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,"axG",@progbits,_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.type	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, @function
_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E:
.LFB31233:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L2392
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L2381:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r12), %rdi
	movq	16(%r12), %rbx
	testq	%rdi, %rdi
	je	.L2379
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L2377
.L2380:
	movq	%rbx, %r12
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2379:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2380
.L2377:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2392:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE31233:
	.size	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E, .-_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	.section	.text._ZN2v88internal9AssemblerD2Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD2Ev
	.type	_ZN2v88internal9AssemblerD2Ev, @function
_ZN2v88internal9AssemblerD2Ev:
.LFB30298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L2399
.L2396:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L2396
.L2399:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L2397
.L2398:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L2402
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L2397
.L2403:
	movq	%rbx, %r13
	jmp	.L2398
	.p2align 4,,10
	.p2align 3
.L2402:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2403
.L2397:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2401
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L2404
	.p2align 4,,10
	.p2align 3
.L2405:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L2405
	movq	328(%r12), %rdi
.L2404:
	call	_ZdlPv@PLT
.L2401:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2406
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L2407
	.p2align 4,,10
	.p2align 3
.L2408:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L2408
	movq	240(%r12), %rdi
.L2407:
	call	_ZdlPv@PLT
.L2406:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.cfi_endproc
.LFE30298:
	.size	_ZN2v88internal9AssemblerD2Ev, .-_ZN2v88internal9AssemblerD2Ev
	.weak	_ZN2v88internal9AssemblerD1Ev
	.set	_ZN2v88internal9AssemblerD1Ev,_ZN2v88internal9AssemblerD2Ev
	.section	.text._ZN2v88internal14TurboAssemblerD2Ev,"axG",@progbits,_ZN2v88internal14TurboAssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14TurboAssemblerD2Ev
	.type	_ZN2v88internal14TurboAssemblerD2Ev, @function
_ZN2v88internal14TurboAssemblerD2Ev:
.LFB30304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L2433
.L2430:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L2430
.L2433:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L2431
.L2432:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L2436
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L2431
.L2437:
	movq	%rbx, %r13
	jmp	.L2432
	.p2align 4,,10
	.p2align 3
.L2436:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2437
.L2431:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2435
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L2438
	.p2align 4,,10
	.p2align 3
.L2439:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L2439
	movq	328(%r12), %rdi
.L2438:
	call	_ZdlPv@PLT
.L2435:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2440
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L2441
	.p2align 4,,10
	.p2align 3
.L2442:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L2442
	movq	240(%r12), %rdi
.L2441:
	call	_ZdlPv@PLT
.L2440:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	.cfi_endproc
.LFE30304:
	.size	_ZN2v88internal14TurboAssemblerD2Ev, .-_ZN2v88internal14TurboAssemblerD2Ev
	.weak	_ZN2v88internal14TurboAssemblerD1Ev
	.set	_ZN2v88internal14TurboAssemblerD1Ev,_ZN2v88internal14TurboAssemblerD2Ev
	.section	.text._ZN2v88internal9AssemblerD0Ev,"axG",@progbits,_ZN2v88internal9AssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal9AssemblerD0Ev
	.type	_ZN2v88internal9AssemblerD0Ev, @function
_ZN2v88internal9AssemblerD0Ev:
.LFB30300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L2467
.L2464:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L2464
.L2467:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L2465
.L2466:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L2470
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L2465
.L2471:
	movq	%rbx, %r13
	jmp	.L2466
	.p2align 4,,10
	.p2align 3
.L2470:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2471
.L2465:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2469
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L2472
	.p2align 4,,10
	.p2align 3
.L2473:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L2473
	movq	328(%r12), %rdi
.L2472:
	call	_ZdlPv@PLT
.L2469:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2474
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L2475
	.p2align 4,,10
	.p2align 3
.L2476:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L2476
	movq	240(%r12), %rdi
.L2475:
	call	_ZdlPv@PLT
.L2474:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE30300:
	.size	_ZN2v88internal9AssemblerD0Ev, .-_ZN2v88internal9AssemblerD0Ev
	.section	.text._ZN2v88internal14TurboAssemblerD0Ev,"axG",@progbits,_ZN2v88internal14TurboAssemblerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal14TurboAssemblerD0Ev
	.type	_ZN2v88internal14TurboAssemblerD0Ev, @function
_ZN2v88internal14TurboAssemblerD0Ev:
.LFB30306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN2v88internal9AssemblerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	480(%rdi), %r13
	leaq	464(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L2501
.L2498:
	movq	24(%r13), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeImSt4pairIKmiESt10_Select1stIS2_ESt4lessImESaIS2_EE8_M_eraseEPSt13_Rb_tree_nodeIS2_E
	movq	%r13, %rdi
	movq	16(%r13), %r13
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L2498
.L2501:
	movq	424(%r12), %r13
	leaq	408(%r12), %r14
	testq	%r13, %r13
	je	.L2499
.L2500:
	movq	24(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeIPN2v88internal5LabelESt4pairIKS3_St6vectorIiSaIiEEESt10_Select1stIS9_ESt4lessIS3_ESaIS9_EE8_M_eraseEPSt13_Rb_tree_nodeIS9_E
	movq	40(%r13), %rdi
	movq	16(%r13), %rbx
	testq	%rdi, %rdi
	je	.L2504
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L2499
.L2505:
	movq	%rbx, %r13
	jmp	.L2500
	.p2align 4,,10
	.p2align 3
.L2504:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L2505
.L2499:
	movq	328(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2503
	movq	400(%r12), %rax
	movq	368(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L2506
	.p2align 4,,10
	.p2align 3
.L2507:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L2507
	movq	328(%r12), %rdi
.L2506:
	call	_ZdlPv@PLT
.L2503:
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2508
	movq	312(%r12), %rax
	movq	280(%r12), %r13
	leaq	8(%rax), %rbx
	cmpq	%r13, %rbx
	jbe	.L2509
	.p2align 4,,10
	.p2align 3
.L2510:
	movq	0(%r13), %rdi
	addq	$8, %r13
	call	_ZdlPv@PLT
	cmpq	%r13, %rbx
	ja	.L2510
	movq	240(%r12), %rdi
.L2509:
	call	_ZdlPv@PLT
.L2508:
	movq	%r12, %rdi
	call	_ZN2v88internal13AssemblerBaseD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8MalloceddlEPv@PLT
	.cfi_endproc
.LFE30306:
	.size	_ZN2v88internal14TurboAssemblerD0Ev, .-_ZN2v88internal14TurboAssemblerD0Ev
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi, @function
_GLOBAL__sub_I__ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi:
.LFB31303:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE31303:
	.size	_GLOBAL__sub_I__ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi, .-_GLOBAL__sub_I__ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal22StackArgumentsAccessor18GetArgumentOperandEi
	.weak	_ZTVN2v88internal9AssemblerE
	.section	.data.rel.ro.local._ZTVN2v88internal9AssemblerE,"awG",@progbits,_ZTVN2v88internal9AssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal9AssemblerE, @object
	.size	_ZTVN2v88internal9AssemblerE, 40
_ZTVN2v88internal9AssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal9AssemblerD1Ev
	.quad	_ZN2v88internal9AssemblerD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.weak	_ZTVN2v88internal14TurboAssemblerE
	.section	.data.rel.ro.local._ZTVN2v88internal14TurboAssemblerE,"awG",@progbits,_ZTVN2v88internal14TurboAssemblerE,comdat
	.align 8
	.type	_ZTVN2v88internal14TurboAssemblerE, @object
	.size	_ZTVN2v88internal14TurboAssemblerE, 112
_ZTVN2v88internal14TurboAssemblerE:
	.quad	0
	.quad	0
	.quad	_ZN2v88internal14TurboAssemblerD1Ev
	.quad	_ZN2v88internal14TurboAssemblerD0Ev
	.quad	_ZN2v88internal13AssemblerBase21AbortedCodeGenerationEv
	.quad	_ZN2v88internal14TurboAssembler4JumpERKNS0_17ExternalReferenceE
	.quad	_ZN2v88internal14TurboAssembler18CallBuiltinByIndexENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler14CallCodeObjectENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler14JumpCodeObjectENS0_8RegisterE
	.quad	_ZN2v88internal14TurboAssembler19LoadCodeObjectEntryENS0_8RegisterES2_
	.quad	_ZN2v88internal14TurboAssembler22LoadFromConstantsTableENS0_8RegisterEi
	.quad	_ZN2v88internal14TurboAssembler22LoadRootRegisterOffsetENS0_8RegisterEl
	.quad	_ZN2v88internal14TurboAssembler16LoadRootRelativeENS0_8RegisterEi
	.quad	_ZN2v88internal14TurboAssembler8LoadRootENS0_8RegisterENS0_9RootIndexE
	.globl	_ZN2v88internal14MacroAssembler29kSafepointPushRegisterIndicesE
	.section	.rodata._ZN2v88internal14MacroAssembler29kSafepointPushRegisterIndicesE,"a"
	.align 32
	.type	_ZN2v88internal14MacroAssembler29kSafepointPushRegisterIndicesE, @object
	.size	_ZN2v88internal14MacroAssembler29kSafepointPushRegisterIndicesE, 64
_ZN2v88internal14MacroAssembler29kSafepointPushRegisterIndicesE:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	-1
	.long	-1
	.long	4
	.long	5
	.long	6
	.long	7
	.long	-1
	.long	8
	.long	9
	.long	-1
	.long	10
	.long	11
	.section	.rodata._ZN2v88internalL10saved_regsE,"a"
	.align 32
	.type	_ZN2v88internalL10saved_regsE, @object
	.size	_ZN2v88internalL10saved_regsE, 44
_ZN2v88internalL10saved_regsE:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	10
	.long	11
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.rodata._ZN2v88internalL17kScratchDoubleRegE,"a"
	.align 4
	.type	_ZN2v88internalL17kScratchDoubleRegE, @object
	.size	_ZN2v88internalL17kScratchDoubleRegE, 4
_ZN2v88internalL17kScratchDoubleRegE:
	.long	15
	.section	.rodata._ZN2v88internalL16kScratchRegisterE,"a"
	.align 4
	.type	_ZN2v88internalL16kScratchRegisterE, @object
	.size	_ZN2v88internalL16kScratchRegisterE, 4
_ZN2v88internalL16kScratchRegisterE:
	.long	10
	.set	_ZN2v88internalL26kOffHeapTrampolineRegisterE,_ZN2v88internalL16kScratchRegisterE
	.section	.rodata._ZN2v88internalL4xmm6E,"a"
	.align 4
	.type	_ZN2v88internalL4xmm6E, @object
	.size	_ZN2v88internalL4xmm6E, 4
_ZN2v88internalL4xmm6E:
	.long	6
	.section	.rodata._ZN2v88internalL4xmm2E,"a"
	.align 4
	.type	_ZN2v88internalL4xmm2E, @object
	.size	_ZN2v88internalL4xmm2E, 4
_ZN2v88internalL4xmm2E:
	.long	2
	.section	.rodata._ZN2v88internalL4xmm0E,"a"
	.align 4
	.type	_ZN2v88internalL4xmm0E, @object
	.size	_ZN2v88internalL4xmm0E, 4
_ZN2v88internalL4xmm0E:
	.zero	4
	.section	.rodata._ZN2v88internalL3r15E,"a"
	.align 4
	.type	_ZN2v88internalL3r15E, @object
	.size	_ZN2v88internalL3r15E, 4
_ZN2v88internalL3r15E:
	.long	15
	.section	.rodata._ZN2v88internalL3r14E,"a"
	.align 4
	.type	_ZN2v88internalL3r14E, @object
	.size	_ZN2v88internalL3r14E, 4
_ZN2v88internalL3r14E:
	.long	14
	.section	.rodata._ZN2v88internalL3r13E,"a"
	.align 4
	.type	_ZN2v88internalL3r13E, @object
	.size	_ZN2v88internalL3r13E, 4
_ZN2v88internalL3r13E:
	.long	13
	.set	_ZN2v88internalL13kRootRegisterE,_ZN2v88internalL3r13E
	.section	.rodata._ZN2v88internalL3rdiE,"a"
	.align 4
	.type	_ZN2v88internalL3rdiE, @object
	.size	_ZN2v88internalL3rdiE, 4
_ZN2v88internalL3rdiE:
	.long	7
	.set	_ZN2v88internalL9arg_reg_1E,_ZN2v88internalL3rdiE
	.section	.rodata._ZN2v88internalL3rsiE,"a"
	.align 4
	.type	_ZN2v88internalL3rsiE, @object
	.size	_ZN2v88internalL3rsiE, 4
_ZN2v88internalL3rsiE:
	.long	6
	.section	.rodata._ZN2v88internalL3rbpE,"a"
	.align 4
	.type	_ZN2v88internalL3rbpE, @object
	.size	_ZN2v88internalL3rbpE, 4
_ZN2v88internalL3rbpE:
	.long	5
	.section	.rodata._ZN2v88internalL3rspE,"a"
	.align 4
	.type	_ZN2v88internalL3rspE, @object
	.size	_ZN2v88internalL3rspE, 4
_ZN2v88internalL3rspE:
	.long	4
	.section	.rodata._ZN2v88internalL3rbxE,"a"
	.align 4
	.type	_ZN2v88internalL3rbxE, @object
	.size	_ZN2v88internalL3rbxE, 4
_ZN2v88internalL3rbxE:
	.long	3
	.section	.rodata._ZN2v88internalL3rdxE,"a"
	.align 4
	.type	_ZN2v88internalL3rdxE, @object
	.size	_ZN2v88internalL3rdxE, 4
_ZN2v88internalL3rdxE:
	.long	2
	.section	.rodata._ZN2v88internalL3rcxE,"a"
	.align 4
	.type	_ZN2v88internalL3rcxE, @object
	.size	_ZN2v88internalL3rcxE, 4
_ZN2v88internalL3rcxE:
	.long	1
	.section	.rodata._ZN2v88internalL3raxE,"a"
	.align 4
	.type	_ZN2v88internalL3raxE, @object
	.size	_ZN2v88internalL3raxE, 4
_ZN2v88internalL3raxE:
	.zero	4
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	8101253777400799323
	.quad	7451031123849864562
	.align 16
.LC3:
	.quad	8101253777400799323
	.quad	6087017598756808050
	.align 16
.LC5:
	.long	8
	.long	8
	.long	8
	.long	8
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	0
	.long	-1042284544
	.align 8
.LC8:
	.long	4290772992
	.long	1105199103
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
