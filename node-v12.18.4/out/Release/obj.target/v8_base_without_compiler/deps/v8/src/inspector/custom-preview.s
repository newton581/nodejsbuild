	.file	"custom-preview.cc"
	.text
	.section	.text._ZN12v8_inspector17V8InspectorClient13currentTimeMSEv,"axG",@progbits,_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv
	.type	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv, @function
_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv:
.LFB3646:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	ret
	.cfi_endproc
.LFE3646:
	.size	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv, .-_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev:
.LFB4606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L4
	call	_ZdlPv@PLT
.L4:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L3
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4606:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.set	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD2Ev
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv:
.LFB4615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L7
	movq	(%rdi), %rax
	call	*24(%rax)
.L7:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L14
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L14:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4615:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv:
.LFB4614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK12v8_inspector8protocol7Runtime13CustomPreview7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L15
	movq	(%rdi), %rax
	call	*24(%rax)
.L15:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L22
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L22:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4614:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv, .-_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.section	.text._ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev,"axG",@progbits,_ZN12v8_inspector8protocol7Runtime13CustomPreviewD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.type	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, @function
_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev:
.LFB4608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdi
	leaq	72(%r12), %rax
	cmpq	%rax, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4608:
	.size	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev, .-_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Custom Formatter Failed: "
	.section	.text._ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE, @function
_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE:
.LFB7626:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v85debug12GetInspectorEPNS_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%rbx, %rdi
	movl	%eax, %esi
	movl	%eax, -164(%rbp)
	call	_ZNK12v8_inspector15V8InspectorImpl14contextGroupIdEi@PLT
	movq	%r15, %rdi
	leaq	-96(%rbp), %r15
	movl	%eax, -152(%rbp)
	call	_ZNK2v88TryCatch7MessageEv@PLT
	movq	%rax, %rdi
	call	_ZNK2v87Message3GetEv@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, %rsi
	leaq	-80(%rbp), %rax
	movq	%rax, -160(%rbp)
	cmpq	%rax, %rdi
	je	.L28
	movq	%rsi, -176(%rbp)
	call	_ZdlPv@PLT
	movq	-176(%rbp), %rsi
.L28:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	pxor	%xmm0, %xmm0
	movl	$8, %edi
	movq	$0, -112(%rbp)
	movq	%rax, %r13
	movaps	%xmm0, -128(%rbp)
	call	_Znwm@PLT
	movl	-152(%rbp), %esi
	movq	%rbx, %rdi
	movq	%r13, (%rax)
	leaq	8(%rax), %rdx
	movq	%rax, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rdx, -112(%rbp)
	call	_ZN12v8_inspector15V8InspectorImpl27ensureConsoleMessageStorageEi@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L35
	movq	-160(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -88(%rbp)
	leaq	_ZN12v8_inspector17V8InspectorClient13currentTimeMSEv(%rip), %rdx
	movq	$0, -64(%rbp)
	movq	%rax, -96(%rbp)
	xorl	%eax, %eax
	movw	%ax, -80(%rbp)
	movq	16(%rbx), %rdi
	movq	$0, -144(%rbp)
	movq	(%rdi), %rax
	movq	176(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L51
.L32:
	subq	$8, %rsp
	movl	-152(%rbp), %ecx
	movq	%r12, %rsi
	movq	%rbx, %r8
	leaq	-144(%rbp), %rax
	leaq	-136(%rbp), %r11
	movl	-164(%rbp), %edx
	movl	$3, %r9d
	pushq	%rax
	leaq	-128(%rbp), %rax
	movq	%r11, %rdi
	pushq	%r15
	pushq	%rax
	movq	%r10, -176(%rbp)
	movq	%r11, -152(%rbp)
	call	_ZN12v8_inspector16V8ConsoleMessage19createForConsoleAPIEN2v85LocalINS1_7ContextEEEiiPNS_15V8InspectorImplEdNS_14ConsoleAPITypeERKSt6vectorINS2_INS1_5ValueEEESaISA_EERKNS_8String16ESt10unique_ptrINS_16V8StackTraceImplESt14default_deleteISJ_EE@PLT
	movq	-152(%rbp), %r11
	movq	-176(%rbp), %r10
	addq	$32, %rsp
	movq	%r11, %rsi
	movq	%r10, %rdi
	call	_ZN12v8_inspector23V8ConsoleMessageStorage10addMessageESt10unique_ptrINS_16V8ConsoleMessageESt14default_deleteIS2_EE@PLT
	movq	-136(%rbp), %r12
	testq	%r12, %r12
	je	.L33
	movq	%r12, %rdi
	call	_ZN12v8_inspector16V8ConsoleMessageD1Ev@PLT
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L33:
	movq	-96(%rbp), %rdi
	cmpq	-160(%rbp), %rdi
	je	.L34
	call	_ZdlPv@PLT
.L34:
	movq	-144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L35
	movq	(%rdi), %rax
	call	*64(%rax)
.L35:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	%r10, -176(%rbp)
	call	*%rax
	movq	-176(%rbp), %r10
	jmp	.L32
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7626:
	.size	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE, .-_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE
	.section	.text._ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchERKNS_8String16E,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchERKNS_8String16E, @function
_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchERKNS_8String16E:
.LFB7638:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE
	.cfi_endproc
.LFE7638:
	.size	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchERKNS_8String16E, .-_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchERKNS_8String16E
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_120substituteObjectTagsEiRKNS_8String16EN2v85LocalINS4_7ContextEEENS5_INS4_5ArrayEEEi.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"Too deep hierarchy of inlined custom previews"
	.align 8
.LC3:
	.string	"attributes should be an Object"
	.align 8
.LC4:
	.string	"obligatory attribute \"object\" isn't specified"
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_120substituteObjectTagsEiRKNS_8String16EN2v85LocalINS4_7ContextEEENS5_INS4_5ArrayEEEi.str1.1,"aMS",@progbits,1
.LC5:
	.string	"config"
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_120substituteObjectTagsEiRKNS_8String16EN2v85LocalINS4_7ContextEEENS5_INS4_5ArrayEEEi.str1.8
	.align 8
.LC6:
	.string	"cannot find context with specified id"
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_120substituteObjectTagsEiRKNS_8String16EN2v85LocalINS4_7ContextEEENS5_INS4_5ArrayEEEi.str1.1
.LC7:
	.string	"cannot wrap value"
.LC8:
	.string	"object"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_120substituteObjectTagsEiRKNS_8String16EN2v85LocalINS4_7ContextEEENS5_INS4_5ArrayEEEi,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_120substituteObjectTagsEiRKNS_8String16EN2v85LocalINS4_7ContextEEENS5_INS4_5ArrayEEEi, @function
_ZN12v8_inspector12_GLOBAL__N_120substituteObjectTagsEiRKNS_8String16EN2v85LocalINS4_7ContextEEENS5_INS4_5ArrayEEEi:
.LFB7640:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 3, -56
	movl	%edi, -276(%rbp)
	movq	%rcx, %rdi
	movq	%rsi, -288(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, %r8d
	movl	$1, %eax
	testl	%r8d, %r8d
	jne	.L155
.L55:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L156
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movq	%r13, %rdi
	leaq	-256(%rbp), %r15
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	testl	%r14d, %r14d
	jle	.L157
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, -304(%rbp)
	testq	%rax, %rax
	je	.L60
	leaq	-112(%rbp), %rax
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-296(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, -312(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -320(%rbp)
	cmpq	%rax, %rdi
	je	.L108
	call	_ZdlPv@PLT
.L108:
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	$2, %eax
	je	.L62
.L64:
	leal	-1(%r14), %eax
	xorl	%ebx, %ebx
	movl	%eax, -296(%rbp)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L93:
	addl	$1, %ebx
.L63:
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%ebx, %eax
	jbe	.L111
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L60
	movq	%rax, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L93
	movq	%r14, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	testl	%eax, %eax
	je	.L93
	movl	-296(%rbp), %r8d
	movq	-288(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	-276(%rbp), %edi
	call	_ZN12v8_inspector12_GLOBAL__N_120substituteObjectTagsEiRKNS_8String16EN2v85LocalINS4_7ContextEEENS5_INS4_5ArrayEEEi
	testb	%al, %al
	jne	.L93
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%r15, %rdi
	movb	%al, -276(%rbp)
	call	_ZN2v88TryCatchD1Ev@PLT
	movzbl	-276(%rbp), %eax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L62:
	movq	-304(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L64
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L64
	movq	-312(%rbp), %rsi
	movq	-304(%rbp), %rdi
	call	_ZN2v86String12StringEqualsENS_5LocalIS0_EE@PLT
	testb	%al, %al
	je	.L64
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	testq	%rax, %rax
	je	.L60
	movq	%rax, %rdi
	movq	%rax, -304(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	-304(%rbp), %r8
	testb	%al, %al
	je	.L158
	movq	-312(%rbp), %rdx
	movq	%r8, %rdi
	movq	%r13, %rsi
	movq	%r8, -312(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	-312(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, -304(%rbp)
	je	.L60
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L71
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L71
	cmpl	$5, 43(%rax)
	leaq	.LC4(%rip), %rsi
	je	.L152
.L71:
	movq	-296(%rbp), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%r8, -312(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-296(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-312(%rbp), %r8
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%r8, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, -312(%rbp)
	testq	%rax, %rax
	je	.L159
	movq	-112(%rbp), %rdi
	cmpq	-320(%rbp), %rdi
	je	.L99
	call	_ZdlPv@PLT
.L99:
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v85debug12GetInspectorEPNS_7IsolateE@PLT
	movq	%r13, %rdi
	movq	%rax, -328(%rbp)
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	-328(%rbp), %r8
	movl	%eax, %esi
	movq	%r8, %rdi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L76
	movl	-276(%rbp), %esi
	call	_ZN12v8_inspector16InspectedContext17getInjectedScriptEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L76
	leaq	-272(%rbp), %rax
	subl	$1, %r14d
	movq	-288(%rbp), %rcx
	movq	-304(%rbp), %rdx
	pushq	%rax
	movq	-312(%rbp), %r9
	movl	$1, %r8d
	pushq	%r14
	movq	-296(%rbp), %rdi
	movq	$0, -272(%rbp)
	call	_ZN12v8_inspector14InjectedScript10wrapObjectEN2v85LocalINS1_5ValueEEERKNS_8String16ENS_8WrapModeENS1_10MaybeLocalIS3_EEiPSt10unique_ptrINS_8protocol7Runtime12RemoteObjectESt14default_deleteISE_EE@PLT
	movl	-112(%rbp), %ecx
	popq	%rax
	popq	%rdx
	testl	%ecx, %ecx
	jne	.L79
	movq	-272(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L79
	leaq	-264(%rbp), %rdi
	leaq	-208(%rbp), %r14
	call	_ZNK12v8_inspector8protocol7Runtime12RemoteObject7toValueEv@PLT
	movq	-264(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNK12v8_inspector8protocol5Value12toJSONStringEv@PLT
	movq	-264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L80
	movq	(%rdi), %rax
	call	*24(%rax)
.L80:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v84JSON5ParseENS_5LocalINS_7ContextEEENS1_INS_6StringEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L160
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L161
	movq	-208(%rbp), %rdi
	leaq	-192(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L86
	movb	%al, -276(%rbp)
	call	_ZdlPv@PLT
	movzbl	-276(%rbp), %eax
.L86:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L87
	movb	%al, -276(%rbp)
	call	_ZdlPv@PLT
	movzbl	-276(%rbp), %eax
.L87:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L59
	movq	(%rdi), %rdx
	movb	%al, -276(%rbp)
	call	*24(%rdx)
	movzbl	-276(%rbp), %eax
	jmp	.L59
.L159:
	movq	-112(%rbp), %rdi
	cmpq	-320(%rbp), %rdi
	je	.L60
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE
.L150:
	xorl	%eax, %eax
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$1, %eax
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L157:
	leaq	-112(%rbp), %r14
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L150
.L149:
	call	_ZdlPv@PLT
	xorl	%eax, %eax
	jmp	.L59
.L158:
	movq	-296(%rbp), %rbx
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE
	movq	-112(%rbp), %rdi
	cmpq	-320(%rbp), %rdi
	jne	.L149
	xorl	%eax, %eax
	jmp	.L59
.L79:
	leaq	-160(%rbp), %r12
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchERKNS_8String16E
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L84
.L148:
	call	_ZdlPv@PLT
.L84:
	movq	-104(%rbp), %rdi
	leaq	-88(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L90
	call	_ZdlPv@PLT
.L90:
	movq	-272(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L150
	movq	(%rdi), %rax
	call	*24(%rax)
	xorl	%eax, %eax
	jmp	.L59
.L76:
	leaq	.LC6(%rip), %rsi
.L152:
	movq	-296(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchERKNS_8String16E
	movq	-112(%rbp), %rdi
	cmpq	-320(%rbp), %rdi
	jne	.L149
	xorl	%eax, %eax
	jmp	.L59
.L156:
	call	__stack_chk_fail@PLT
.L161:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE
.L85:
	movq	-208(%rbp), %rdi
	leaq	-192(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L148
	jmp	.L84
.L160:
	leaq	-160(%rbp), %r12
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchERKNS_8String16E
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L85
	call	_ZdlPv@PLT
	jmp	.L85
	.cfi_endproc
.LFE7640:
	.size	_ZN12v8_inspector12_GLOBAL__N_120substituteObjectTagsEiRKNS_8String16EN2v85LocalINS4_7ContextEEENS5_INS4_5ArrayEEEi, .-_ZN12v8_inspector12_GLOBAL__N_120substituteObjectTagsEiRKNS_8String16EN2v85LocalINS4_7ContextEEENS5_INS4_5ArrayEEEi
	.section	.rodata._ZN12v8_inspector12_GLOBAL__N_112bodyCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE.str1.1,"aMS",@progbits,1
.LC9:
	.string	"object should be an Object"
.LC10:
	.string	"formatter"
.LC11:
	.string	"formatter should be an Object"
.LC12:
	.string	"body"
.LC13:
	.string	"body should be a Function"
.LC14:
	.string	"sessionId"
.LC15:
	.string	"sessionId should be an Int32"
.LC16:
	.string	"groupName"
.LC17:
	.string	"groupName should be a string"
.LC18:
	.string	"body should return an Array"
	.section	.text._ZN12v8_inspector12_GLOBAL__N_112bodyCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE,"ax",@progbits
	.p2align 4
	.type	_ZN12v8_inspector12_GLOBAL__N_112bodyCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN12v8_inspector12_GLOBAL__N_112bodyCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r15, %rdi
	movq	8(%rax), %r14
	movq	%r14, %rsi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	(%rbx), %rax
	addq	$32, %rax
	movq	%rax, -168(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-168(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, -184(%rbp)
	testq	%rax, %rax
	je	.L251
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -176(%rbp)
	cmpq	%rax, %rdi
	je	.L225
	call	_ZdlPv@PLT
.L225:
	movq	-184(%rbp), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	leaq	.LC9(%rip), %rsi
	testb	%al, %al
	je	.L245
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-168(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L168
	movq	-96(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L220
	call	_ZdlPv@PLT
.L220:
	movq	-192(%rbp), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	leaq	.LC11(%rip), %rsi
	testb	%al, %al
	je	.L245
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-192(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L168
	movq	-96(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L213
	call	_ZdlPv@PLT
.L213:
	movq	-200(%rbp), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	leaq	.LC13(%rip), %rsi
	testb	%al, %al
	je	.L245
	leaq	.LC5(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-168(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, -216(%rbp)
	testq	%rax, %rax
	je	.L168
	movq	-96(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L210
	call	_ZdlPv@PLT
.L210:
	leaq	.LC14(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-168(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L168
	movq	-96(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L205
	call	_ZdlPv@PLT
.L205:
	movq	-208(%rbp), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	leaq	.LC15(%rip), %rsi
	testb	%al, %al
	je	.L245
	leaq	.LC16(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-168(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L168
	movq	-96(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L200
	movq	%rax, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %r9
.L200:
	movq	(%r9), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L184
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L184
	movq	-184(%rbp), %xmm0
	leaq	-112(%rbp), %r8
	movl	$2, %ecx
	movq	%r12, %rsi
	movq	-192(%rbp), %rdx
	movq	-200(%rbp), %rdi
	movq	%r9, -168(%rbp)
	movhps	-216(%rbp), %xmm0
	movaps	%xmm0, -112(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	-168(%rbp), %r9
	testq	%rax, %rax
	je	.L187
	movq	%rax, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -168(%rbp)
	call	_ZNK2v85Value7IsArrayEv@PLT
	movq	-168(%rbp), %rcx
	movq	-184(%rbp), %r9
	testb	%al, %al
	je	.L252
	movq	%rcx, %rdi
	movq	%r9, -184(%rbp)
	movq	%rcx, -168(%rbp)
	call	_ZNK2v85Array6LengthEv@PLT
	movq	-168(%rbp), %rcx
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	jne	.L253
.L194:
	movq	(%rbx), %rax
	movq	(%rcx), %rdx
	movq	%r15, %rdi
	movq	%rdx, 24(%rax)
	call	_ZN2v88TryCatchD1Ev@PLT
	jmp	.L162
.L252:
	leaq	.LC18(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%r13, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE
	movq	-96(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L165
.L241:
	call	_ZdlPv@PLT
.L165:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
.L162:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L254
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	.cfi_restore_state
	movq	-96(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L187
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L251:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L187
	call	_ZdlPv@PLT
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L184:
	leaq	.LC17(%rip), %rsi
	jmp	.L245
.L253:
	movq	%r9, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-208(%rbp), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	movq	-168(%rbp), %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	%eax, %edi
	movl	$20, %r8d
	call	_ZN12v8_inspector12_GLOBAL__N_120substituteObjectTagsEiRKNS_8String16EN2v85LocalINS4_7ContextEEENS5_INS4_5ArrayEEEi
	movq	-96(%rbp), %rdi
	movq	-168(%rbp), %rcx
	testb	%al, %al
	jne	.L191
	cmpq	-176(%rbp), %rdi
	jne	.L241
	jmp	.L165
.L191:
	cmpq	-176(%rbp), %rdi
	je	.L194
	movq	%rcx, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %rcx
	jmp	.L194
.L254:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7647:
	.size	_ZN12v8_inspector12_GLOBAL__N_112bodyCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN12v8_inspector12_GLOBAL__N_112bodyCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_.str1.1,"aMS",@progbits,1
.LC19:
	.string	"basic_string::_M_create"
	.section	.text._ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,"axG",@progbits,_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.type	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, @function
_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_:
.LFB10574:
	.cfi_startproc
	endbr64
	cmpq	%rsi, %rdi
	je	.L274
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r13
	movq	8(%rsi), %r15
	cmpq	%r13, %rdx
	je	.L265
	movq	16(%rdi), %rax
.L257:
	cmpq	%r15, %rax
	jb	.L277
	leaq	(%r15,%r15), %rdx
	testq	%r15, %r15
	je	.L261
.L280:
	movq	(%r12), %rsi
	cmpq	$1, %r15
	je	.L278
	testq	%rdx, %rdx
	je	.L261
	movq	%r13, %rdi
	call	memmove@PLT
	movq	(%rbx), %r13
.L261:
	xorl	%eax, %eax
	movq	%r15, 8(%rbx)
	movw	%ax, 0(%r13,%r15,2)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	movabsq	$2305843009213693951, %rcx
	cmpq	%rcx, %r15
	ja	.L279
	addq	%rax, %rax
	movq	%r15, %r14
	cmpq	%rax, %r15
	jnb	.L260
	cmpq	%rcx, %rax
	cmovbe	%rax, %rcx
	movq	%rcx, %r14
.L260:
	leaq	2(%r14,%r14), %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rdx
	movq	%rax, %r13
	cmpq	%rdi, %rdx
	je	.L264
	call	_ZdlPv@PLT
.L264:
	movq	%r13, (%rbx)
	leaq	(%r15,%r15), %rdx
	movq	%r14, 16(%rbx)
	testq	%r15, %r15
	je	.L261
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	$7, %eax
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L278:
	movzwl	(%rsi), %eax
	movw	%ax, 0(%r13)
	movq	(%rbx), %r13
	jmp	.L261
.L279:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10574:
	.size	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_, .-_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	.section	.rodata._ZN12v8_inspector21generateCustomPreviewEiRKNS_8String16EN2v85LocalINS3_6ObjectEEENS3_10MaybeLocalINS3_5ValueEEEiPSt10unique_ptrINS_8protocol7Runtime13CustomPreviewESt14default_deleteISD_EE.str1.1,"aMS",@progbits,1
.LC20:
	.string	"devtoolsFormatters"
.LC21:
	.string	"header"
.LC22:
	.string	"hasBody"
.LC23:
	.string	"header should be a Function"
	.section	.rodata._ZN12v8_inspector21generateCustomPreviewEiRKNS_8String16EN2v85LocalINS3_6ObjectEEENS3_10MaybeLocalINS3_5ValueEEEiPSt10unique_ptrINS_8protocol7Runtime13CustomPreviewESt14default_deleteISD_EE.str1.8,"aMS",@progbits,1
	.align 8
.LC24:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZN12v8_inspector21generateCustomPreviewEiRKNS_8String16EN2v85LocalINS3_6ObjectEEENS3_10MaybeLocalINS3_5ValueEEEiPSt10unique_ptrINS_8protocol7Runtime13CustomPreviewESt14default_deleteISD_EE,"ax",@progbits
	.p2align 4
	.globl	_ZN12v8_inspector21generateCustomPreviewEiRKNS_8String16EN2v85LocalINS3_6ObjectEEENS3_10MaybeLocalINS3_5ValueEEEiPSt10unique_ptrINS_8protocol7Runtime13CustomPreviewESt14default_deleteISD_EE
	.type	_ZN12v8_inspector21generateCustomPreviewEiRKNS_8String16EN2v85LocalINS3_6ObjectEEENS3_10MaybeLocalINS3_5ValueEEEiPSt10unique_ptrINS_8protocol7Runtime13CustomPreviewESt14default_deleteISD_EE, @function
_ZN12v8_inspector21generateCustomPreviewEiRKNS_8String16EN2v85LocalINS3_6ObjectEEENS3_10MaybeLocalINS3_5ValueEEEiPSt10unique_ptrINS_8protocol7Runtime13CustomPreviewESt14default_deleteISD_EE:
.LFB7648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$328, %rsp
	movl	%r8d, -328(%rbp)
	movq	%r9, -344(%rbp)
	movq	%rsi, -336(%rbp)
	movq	%rdx, -312(%rbp)
	movl	%edi, -324(%rbp)
	movq	%rdx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v86Object15CreationContextEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	$1, %edx
	movq	%rax, %r15
	leaq	-240(%rbp), %rax
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rax, -264(%rbp)
	call	_ZN2v815MicrotasksScopeC1EPNS_7IsolateENS0_4TypeE@PLT
	leaq	-208(%rbp), %rax
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, -248(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	testq	%rbx, %rbx
	leaq	88(%r15), %rcx
	movq	%r12, %rdi
	cmove	%rcx, %rbx
	movq	%rbx, -288(%rbp)
	call	_ZN2v87Context6GlobalEv@PLT
	leaq	.LC20(%rip), %rsi
	movq	%rax, %r13
	leaq	-96(%rbp), %rax
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%rax, -256(%rbp)
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L397
	movq	-96(%rbp), %rdi
	movq	%rax, %rbx
	leaq	-80(%rbp), %rax
	movq	%rax, -272(%rbp)
	cmpq	%rax, %rdi
	je	.L343
	call	_ZdlPv@PLT
.L343:
	movq	%rbx, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	jne	.L398
.L285:
	movq	-248(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-264(%rbp), %rdi
	call	_ZN2v815MicrotasksScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L399
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L398:
	.cfi_restore_state
	movq	-256(%rbp), %r14
	leaq	.LC21(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -280(%rbp)
	cmpq	-272(%rbp), %rdi
	je	.L286
	call	_ZdlPv@PLT
.L286:
	movq	-256(%rbp), %r14
	leaq	.LC22(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-96(%rbp), %rdi
	movq	%rax, -360(%rbp)
	cmpq	-272(%rbp), %rdi
	je	.L287
	call	_ZdlPv@PLT
.L287:
	movq	-312(%rbp), %xmm2
	xorl	%eax, %eax
	movq	%r15, -368(%rbp)
	movl	%eax, %r15d
	movhps	-288(%rbp), %xmm2
	movaps	%xmm2, -304(%rbp)
.L329:
	movq	%rbx, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%r15d, %eax
	jbe	.L285
	movl	%r15d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L292
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L400
	movq	-280(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L292
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L401
	leaq	-160(%rbp), %rax
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	movdqa	-304(%rbp), %xmm1
	movq	%rax, %r8
	movl	$2, %ecx
	movq	%rax, -320(%rbp)
	movaps	%xmm1, -160(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L292
	movq	%rax, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L334
	movq	-360(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L292
	movq	%rax, -352(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-352(%rbp), %rdi
	testb	%al, %al
	je	.L334
	movq	-320(%rbp), %r8
	movl	$2, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	-368(%rbp), %r15
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L292
	movq	%r15, %rsi
	call	_ZNK2v85Value9ToBooleanEPNS_7IsolateE@PLT
	movq	%rax, %rdi
	call	_ZNK2v87Boolean5ValueEv@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	_ZNK2v85Array6LengthEv@PLT
	testl	%eax, %eax
	jne	.L332
.L301:
	movq	%r14, %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v84JSON9StringifyENS_5LocalINS_7ContextEEENS1_INS_5ValueEEENS1_INS_6StringEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L292
	testb	%bl, %bl
	jne	.L331
	xorl	%r13d, %r13d
.L330:
	movl	$96, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	%r15, %rsi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rcx
	movq	%rax, %rbx
	leaq	8(%rax), %r8
	movq	-256(%rbp), %r15
	movq	%rcx, (%rax)
	leaq	24(%rax), %rax
	movq	%rax, 8(%rbx)
	leaq	72(%rbx), %rax
	movq	%r15, %rdi
	movq	%rax, 56(%rbx)
	movw	%dx, 24(%rbx)
	movq	%r14, %rdx
	movq	$0, 16(%rbx)
	movq	$0, 40(%rbx)
	movb	$0, 48(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 64(%rbx)
	movups	%xmm0, 72(%rbx)
	movq	%r8, -280(%rbp)
	call	_ZN12v8_inspector16toProtocolStringEPN2v87IsolateENS0_5LocalINS0_6StringEEE@PLT
	movq	-280(%rbp), %r8
	movq	%r15, %rsi
	movq	%r8, %rdi
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movq	%rax, 40(%rbx)
	movq	-344(%rbp), %rax
	movq	(%rax), %r14
	movq	%rbx, (%rax)
	testq	%r14, %r14
	je	.L312
	movq	(%r14), %rax
	leaq	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L313
	movq	56(%r14), %rdi
	leaq	16+_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE(%rip), %rcx
	leaq	72(%r14), %rax
	movq	%rcx, (%r14)
	cmpq	%rax, %rdi
	je	.L314
	call	_ZdlPv@PLT
.L314:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L315
	call	_ZdlPv@PLT
.L315:
	movl	$96, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L312:
	movq	-96(%rbp), %rdi
	cmpq	-272(%rbp), %rdi
	je	.L316
	call	_ZdlPv@PLT
.L316:
	testq	%r13, %r13
	je	.L285
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v85debug12GetInspectorEPNS_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN12v8_inspector16InspectedContext9contextIdEN2v85LocalINS1_7ContextEEE@PLT
	movq	%r14, %rdi
	movl	%eax, %esi
	call	_ZNK12v8_inspector15V8InspectorImpl10getContextEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L319
	movl	-324(%rbp), %esi
	call	_ZN12v8_inspector16InspectedContext17getInjectedScriptEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L319
	movq	-344(%rbp), %rax
	movq	-336(%rbp), %rcx
	movq	%r13, %rdx
	leaq	-144(%rbp), %rdi
	movq	(%rax), %rbx
	call	_ZN12v8_inspector14InjectedScript10bindObjectEN2v85LocalINS1_5ValueEEERKNS_8String16E@PLT
	movq	-272(%rbp), %rax
	movq	-144(%rbp), %r13
	movq	%rax, -96(%rbp)
	movq	-136(%rbp), %rax
	leaq	(%rax,%rax), %r12
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L322
	testq	%r13, %r13
	je	.L402
.L322:
	movq	%r12, %r14
	movq	-272(%rbp), %rdi
	sarq	%r14
	cmpq	$14, %r12
	ja	.L403
.L323:
	cmpq	$2, %r12
	je	.L404
	testq	%r12, %r12
	je	.L326
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	movq	-96(%rbp), %rdi
.L326:
	xorl	%eax, %eax
	movq	%r14, -88(%rbp)
	movq	-256(%rbp), %rsi
	movw	%ax, (%rdi,%r12)
	movq	-112(%rbp), %rax
	leaq	56(%rbx), %rdi
	movq	%rax, -64(%rbp)
	call	_ZNSt7__cxx1112basic_stringItSt11char_traitsItESaItEE9_M_assignERKS4_
	movq	-64(%rbp), %rax
	movb	$1, 48(%rbx)
	movq	%rax, 88(%rbx)
	movq	-96(%rbp), %rdi
	cmpq	-272(%rbp), %rdi
	je	.L327
	call	_ZdlPv@PLT
.L327:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L285
.L393:
	call	_ZdlPv@PLT
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L397:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L292
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L292:
	movq	-248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L334:
	addl	$1, %r15d
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L332:
	movl	-328(%rbp), %r8d
	movq	-336(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdx
	movl	-324(%rbp), %edi
	call	_ZN12v8_inspector12_GLOBAL__N_120substituteObjectTagsEiRKNS_8String16EN2v85LocalINS4_7ContextEEENS5_INS4_5ArrayEEEi
	testb	%al, %al
	jne	.L301
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L331:
	movq	%r15, %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movl	-324(%rbp), %esi
	movq	%r15, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-256(%rbp), %rdi
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rbx
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	-256(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-280(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object18CreateDataPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEE@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %ebx
	cmpq	-272(%rbp), %rdi
	je	.L304
	call	_ZdlPv@PLT
.L304:
	testb	%bl, %bl
	je	.L292
	movq	-256(%rbp), %rbx
	leaq	.LC10(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-280(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object18CreateDataPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEE@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %ebx
	cmpq	-272(%rbp), %rdi
	je	.L306
	call	_ZdlPv@PLT
.L306:
	testb	%bl, %bl
	je	.L292
	movq	-336(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-256(%rbp), %rbx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %r13
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-280(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object18CreateDataPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEE@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %ebx
	cmpq	-272(%rbp), %rdi
	je	.L307
	call	_ZdlPv@PLT
.L307:
	testb	%bl, %bl
	je	.L292
	movq	-256(%rbp), %rbx
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-280(%rbp), %rdi
	movq	-288(%rbp), %rcx
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object18CreateDataPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEE@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %ebx
	cmpq	-272(%rbp), %rdi
	je	.L308
	call	_ZdlPv@PLT
.L308:
	testb	%bl, %bl
	je	.L292
	movq	-256(%rbp), %rbx
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	-280(%rbp), %rdi
	movq	-312(%rbp), %rcx
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN2v86Object18CreateDataPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEE@PLT
	movq	-96(%rbp), %rdi
	movl	%eax, %ebx
	cmpq	-272(%rbp), %rdi
	je	.L309
	call	_ZdlPv@PLT
.L309:
	testb	%bl, %bl
	je	.L292
	movq	-280(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	$1, %r8d
	leaq	_ZN12v8_inspector12_GLOBAL__N_112bodyCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L330
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L400:
	leaq	.LC11(%rip), %rsi
.L395:
	movq	-256(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN12v8_inspector8String16C1EPKc@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN12v8_inspector10toV8StringEPN2v87IsolateERKNS_8String16E@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	-248(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN12v8_inspector12_GLOBAL__N_111reportErrorEN2v85LocalINS1_7ContextEEERKNS1_8TryCatchE
	movq	-96(%rbp), %rdi
	cmpq	-272(%rbp), %rdi
	jne	.L393
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L401:
	leaq	.LC23(%rip), %rsi
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L404:
	movzwl	0(%r13), %eax
	movw	%ax, (%rdi)
	movq	-96(%rbp), %rdi
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L403:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L405
	leaq	2(%r12), %rdi
	call	_Znwm@PLT
	movq	%r14, -80(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	jmp	.L323
.L319:
	leaq	.LC6(%rip), %rsi
	jmp	.L395
.L399:
	call	__stack_chk_fail@PLT
.L402:
	leaq	.LC24(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L405:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7648:
	.size	_ZN12v8_inspector21generateCustomPreviewEiRKNS_8String16EN2v85LocalINS3_6ObjectEEENS3_10MaybeLocalINS3_5ValueEEEiPSt10unique_ptrINS_8protocol7Runtime13CustomPreviewESt14default_deleteISD_EE, .-_ZN12v8_inspector21generateCustomPreviewEiRKNS_8String16EN2v85LocalINS3_6ObjectEEENS3_10MaybeLocalINS3_5ValueEEEiPSt10unique_ptrINS_8protocol7Runtime13CustomPreviewESt14default_deleteISD_EE
	.weak	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE
	.section	.data.rel.ro.local._ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,"awG",@progbits,_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE,comdat
	.align 8
	.type	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, @object
	.size	_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE, 48
_ZTVN12v8_inspector8protocol7Runtime13CustomPreviewE:
	.quad	0
	.quad	0
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview15serializeToJSONEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreview17serializeToBinaryEv
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD1Ev
	.quad	_ZN12v8_inspector8protocol7Runtime13CustomPreviewD0Ev
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
