	.file	"fast-dtoa.cc"
	.text
	.section	.rodata._ZN2v88internalL15BiggestPowerTenEjiPjPi.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unreachable code"
	.section	.text._ZN2v88internalL15BiggestPowerTenEjiPjPi,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL15BiggestPowerTenEjiPjPi, @function
_ZN2v88internalL15BiggestPowerTenEjiPjPi:
.LFB5042:
	.cfi_startproc
	cmpl	$32, %esi
	ja	.L2
	leaq	.L4(%rip), %r8
	movl	%esi, %esi
	movslq	(%r8,%rsi,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN2v88internalL15BiggestPowerTenEjiPjPi,"a",@progbits
	.align 4
	.align 4
.L4:
	.long	.L14-.L4
	.long	.L13-.L4
	.long	.L13-.L4
	.long	.L13-.L4
	.long	.L12-.L4
	.long	.L12-.L4
	.long	.L12-.L4
	.long	.L11-.L4
	.long	.L11-.L4
	.long	.L11-.L4
	.long	.L10-.L4
	.long	.L10-.L4
	.long	.L10-.L4
	.long	.L10-.L4
	.long	.L9-.L4
	.long	.L9-.L4
	.long	.L9-.L4
	.long	.L8-.L4
	.long	.L8-.L4
	.long	.L8-.L4
	.long	.L7-.L4
	.long	.L7-.L4
	.long	.L7-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L6-.L4
	.long	.L6-.L4
	.long	.L5-.L4
	.long	.L5-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.section	.text._ZN2v88internalL15BiggestPowerTenEjiPjPi
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$999, %edi
	jbe	.L11
	movl	$1000, (%rdx)
	movl	$3, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	cmpl	$999999, %edi
	jbe	.L8
	movl	$1000000, (%rdx)
	movl	$6, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	testl	%edi, %edi
	je	.L14
	movl	$1, (%rdx)
	movl	$0, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	cmpl	$9, %edi
	jbe	.L13
	movl	$10, (%rdx)
	movl	$1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	cmpl	$99, %edi
	jbe	.L12
	movl	$100, (%rdx)
	movl	$2, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	cmpl	$9999, %edi
	jbe	.L10
	movl	$10000, (%rdx)
	movl	$4, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$99999, %edi
	jbe	.L9
	movl	$100000, (%rdx)
	movl	$5, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	$9999999, %edi
	jbe	.L7
	movl	$10000000, (%rdx)
	movl	$7, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$999999999, %edi
	ja	.L22
.L5:
	cmpl	$99999999, %edi
	jbe	.L6
	movl	$100000000, (%rdx)
	movl	$8, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$0, (%rdx)
	movl	$-1, (%rcx)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$1000000000, (%rdx)
	movl	$9, (%rcx)
	ret
.L2:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movl	$0, (%rdx)
	movl	$0, (%rcx)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5042:
	.size	_ZN2v88internalL15BiggestPowerTenEjiPjPi, .-_ZN2v88internalL15BiggestPowerTenEjiPjPi
	.section	.text._ZN2v88internalL16RoundWeedCountedENS0_6VectorIcEEimmmPi.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL16RoundWeedCountedENS0_6VectorIcEEimmmPi.isra.0, @function
_ZN2v88internalL16RoundWeedCountedENS0_6VectorIcEEimmmPi.isra.0:
.LFB5827:
	.cfi_startproc
	xorl	%eax, %eax
	cmpq	%rcx, %r8
	jnb	.L23
	movq	%rcx, %r10
	subq	%r8, %r10
	cmpq	%r10, %r8
	jnb	.L23
	subq	%rdx, %rcx
	cmpq	%rcx, %rdx
	jnb	.L25
	movq	%rcx, %r10
	leaq	(%r8,%r8), %rax
	subq	%rdx, %r10
	cmpq	%rax, %r10
	jnb	.L35
.L25:
	xorl	%eax, %eax
	cmpq	%rdx, %r8
	jnb	.L23
	addq	%r8, %rcx
	subq	%r8, %rdx
	cmpq	%rdx, %rcx
	ja	.L23
	leal	-1(%rsi), %edx
	movslq	%edx, %rax
	addq	%rdi, %rax
	addb	$1, (%rax)
	testl	%edx, %edx
	jle	.L29
	movslq	%esi, %rdx
	leal	-2(%rsi), %ecx
	leaq	-2(%rdi,%rdx), %rdx
	subq	%rcx, %rdx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L36:
	addb	$1, -1(%rax)
	subq	$1, %rax
	movb	$48, 1(%rax)
	cmpq	%rdx, %rax
	je	.L29
.L30:
	cmpb	$58, (%rax)
	je	.L36
.L29:
	cmpb	$58, (%rdi)
	je	.L37
.L35:
	movl	$1, %eax
.L23:
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movb	$49, (%rdi)
	addl	$1, (%r9)
	jmp	.L35
	.cfi_endproc
.LFE5827:
	.size	_ZN2v88internalL16RoundWeedCountedENS0_6VectorIcEEimmmPi.isra.0, .-_ZN2v88internalL16RoundWeedCountedENS0_6VectorIcEEimmmPi.isra.0
	.section	.text._ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_
	.type	_ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_, @function
_ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_:
.LFB5047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%xmm0, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r8, %r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%r9, -136(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%edi, %edi
	je	.L39
	cmpl	$1, %edi
	jne	.L115
	movabsq	$4503599627370495, %r13
	movl	%esi, %r14d
	movabsq	$9218868437227405312, %rax
	andq	%rcx, %r13
	testq	%rax, %rcx
	je	.L89
	movabsq	$4503599627370496, %rax
	shrq	$52, %rcx
	andl	$2047, %ecx
	addq	%rax, %r13
	leal	-1075(%rcx), %eax
.L71:
	leal	-11(%rax), %ebx
	leaq	-96(%rbp), %r9
	movl	$-96, %esi
	salq	$11, %r13
	movl	$-124, %edi
	subl	%ebx, %esi
	movq	%r9, %rdx
	movq	%r9, -120(%rbp)
	subl	%ebx, %edi
	leaq	-108(%rbp), %rcx
	movq	$0, -96(%rbp)
	movl	$0, -88(%rbp)
	call	_ZN2v88internal16PowersOfTenCache36GetCachedPowerForBinaryExponentRangeEiiPNS0_5DiyFpEPi@PLT
	movq	-120(%rbp), %r9
	leaq	-80(%rbp), %r8
	movl	%ebx, -72(%rbp)
	movq	%r8, %rdi
	movq	%r13, -80(%rbp)
	movq	%r9, %rsi
	movq	%r8, -152(%rbp)
	call	_ZN2v88internal5DiyFp8MultiplyERKS1_@PLT
	movl	-72(%rbp), %esi
	movq	-80(%rbp), %r13
	leaq	-100(%rbp), %rdx
	movq	-152(%rbp), %r8
	movl	$1, %r10d
	movl	%esi, %r9d
	movq	%r13, %rdi
	addl	$64, %esi
	negl	%r9d
	movl	%r9d, %ecx
	movl	%r9d, -144(%rbp)
	salq	%cl, %r10
	shrq	%cl, %rdi
	movq	%r8, %rcx
	leaq	-1(%r10), %r11
	movq	%r10, -128(%rbp)
	movl	%edi, %ebx
	andq	%r11, %r13
	movq	%r11, -120(%rbp)
	call	_ZN2v88internalL15BiggestPowerTenEjiPjPi
	movl	-80(%rbp), %edi
	movq	-120(%rbp), %r11
	movl	$0, (%r12)
	movq	-128(%rbp), %r10
	movl	-144(%rbp), %r9d
	leal	1(%rdi), %eax
	testl	%eax, %eax
	movl	%eax, -104(%rbp)
	jle	.L72
	movl	-100(%rbp), %ecx
	movl	%ebx, %eax
	xorl	%edx, %edx
	movl	%edi, -104(%rbp)
	divl	%ecx
	addl	$48, %eax
	movl	%edx, %ebx
	movb	%al, (%r15)
	movl	(%r12), %eax
	leal	1(%rax), %esi
	movl	%esi, (%r12)
	subl	$1, %r14d
	je	.L73
	xorl	%eax, %eax
	movl	$3435973837, %r8d
	movl	%eax, %edx
	movl	%ebx, %eax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L75:
	xorl	%edx, %edx
	movslq	%esi, %rsi
	subl	$1, %edi
	divl	%ecx
	addl	$48, %eax
	movb	%al, (%r15,%rsi)
	movl	(%r12), %eax
	leal	1(%rax), %esi
	movl	%edx, %eax
	movl	$1, %edx
	movl	%esi, (%r12)
	subl	$1, %r14d
	je	.L116
.L74:
	movl	%ecx, %ecx
	imulq	%r8, %rcx
	shrq	$35, %rcx
	testl	%edi, %edi
	jne	.L75
	movl	%ecx, -100(%rbp)
	testb	%dl, %dl
	je	.L77
	movl	$0, -104(%rbp)
.L77:
	testl	%r14d, %r14d
	jle	.L83
	cmpq	$1, %r13
	jbe	.L83
	movl	-104(%rbp), %ebx
	movl	%r14d, %eax
	movl	$1, %edi
	movl	%r9d, %ecx
	.p2align 4,,10
	.p2align 3
.L81:
	leaq	0(%r13,%r13,4), %r13
	movslq	%esi, %rsi
	leaq	(%rdi,%rdi,4), %r8
	subl	$1, %eax
	addq	%r13, %r13
	addq	%r8, %r8
	movq	%r13, %rdx
	movq	%r8, %rdi
	andq	%r11, %r13
	shrq	%cl, %rdx
	addl	$48, %edx
	movb	%dl, (%r15,%rsi)
	movl	(%r12), %edx
	leal	1(%rdx), %esi
	leal	(%rax,%rbx), %edx
	movl	%esi, (%r12)
	subl	%r14d, %edx
	testl	%eax, %eax
	jle	.L91
	cmpq	%r13, %r8
	jb	.L81
.L91:
	movl	%edx, -104(%rbp)
	testl	%eax, %eax
	jne	.L83
	leaq	-104(%rbp), %r9
	movq	%r10, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	call	_ZN2v88internalL16RoundWeedCountedENS0_6VectorIcEEimmmPi.isra.0
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L89:
	movabsq	$4503599627370496, %rdx
	movl	$-1074, %eax
	.p2align 4,,10
	.p2align 3
.L70:
	addq	%r13, %r13
	subl	$1, %eax
	testq	%rdx, %r13
	je	.L70
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L122:
	movl	%edi, -100(%rbp)
	movslq	%r9d, %rdx
	movl	%ecx, %r9d
.L51:
	movq	-152(%rbp), %r10
	subq	-120(%rbp), %r10
	movl	%edi, %edi
	movl	%r9d, %ecx
	leaq	-1(%r10), %rax
	salq	%cl, %rdi
	cmpq	%rax, %rsi
	jnb	.L53
	movq	%r13, %rcx
	subq	%rsi, %rcx
	cmpq	%rcx, %rdi
	ja	.L57
	movslq	%edx, %r8
	subq	%rdi, %rcx
	addq	%r15, %r8
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rsi, %r9
	addq	%rdi, %rsi
	cmpq	%rsi, %rax
	ja	.L55
	movq	-120(%rbp), %rcx
	subq	-128(%rbp), %rcx
	subq	%r9, %rax
	addq	%rsi, %rcx
	cmpq	%rcx, %rax
	jb	.L117
	subb	$1, (%r15,%rdx)
.L53:
	addq	$1, %r10
	cmpq	%rsi, %r10
	jbe	.L57
	subq	%rsi, %r13
	cmpq	%r13, %rdi
	ja	.L57
	addq	%rsi, %rdi
	cmpq	%rdi, %r10
	ja	.L83
	movq	%r10, %rax
	subq	%r10, %rdi
	subq	%rsi, %rax
	cmpq	%rdi, %rax
	ja	.L83
	.p2align 4,,10
	.p2align 3
.L57:
	cmpq	$1, %rsi
	ja	.L118
	.p2align 4,,10
	.p2align 3
.L83:
	xorl	%eax, %eax
.L38:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L119
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movabsq	$4503599627370495, %rbx
	movabsq	$9218868437227405312, %rax
	andq	%rcx, %rbx
	testq	%rax, %rcx
	jne	.L120
	movq	%rbx, %r14
	movl	$-1074, %eax
	movabsq	$4503599627370496, %rdx
	.p2align 4,,10
	.p2align 3
.L42:
	addq	%r14, %r14
	subl	$1, %eax
	testq	%rdx, %r14
	je	.L42
	movl	$-1075, %ecx
	xorl	%edx, %edx
	movl	$-1074, %esi
.L43:
	addq	%rbx, %rbx
	salq	$11, %r14
	subl	$11, %eax
	movl	%ecx, %r8d
	movabsq	$-18014398509481984, %rdi
	leaq	1(%rbx), %r13
	.p2align 4,,10
	.p2align 3
.L44:
	salq	$10, %r13
	subl	$10, %r8d
	testq	%rdi, %r13
	je	.L44
	testq	%r13, %r13
	js	.L45
	.p2align 4,,10
	.p2align 3
.L46:
	subl	$1, %r8d
	addq	%r13, %r13
	jns	.L46
.L45:
	subq	$1, %rbx
	testb	%dl, %dl
	jne	.L121
.L49:
	subl	%r8d, %ecx
	movl	$-96, %esi
	movl	$-124, %edi
	movl	%eax, -120(%rbp)
	salq	%cl, %rbx
	subl	%eax, %esi
	leaq	-104(%rbp), %rcx
	subl	%eax, %edi
	movq	%rbx, -128(%rbp)
	leaq	-96(%rbp), %rbx
	movq	%rbx, %rdx
	movl	%r8d, -144(%rbp)
	movq	$0, -96(%rbp)
	movl	$0, -88(%rbp)
	call	_ZN2v88internal16PowersOfTenCache36GetCachedPowerForBinaryExponentRangeEiiPNS0_5DiyFpEPi@PLT
	movl	-120(%rbp), %eax
	movq	%r14, -80(%rbp)
	leaq	-80(%rbp), %r14
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movl	%eax, -72(%rbp)
	call	_ZN2v88internal5DiyFp8MultiplyERKS1_@PLT
	movl	-72(%rbp), %eax
	movq	-80(%rbp), %rdi
	movq	%rbx, %rsi
	movl	-144(%rbp), %r8d
	movl	%eax, -156(%rbp)
	movq	-128(%rbp), %rax
	movq	%rdi, -120(%rbp)
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	movl	%r8d, -72(%rbp)
	movl	%r8d, -128(%rbp)
	call	_ZN2v88internal5DiyFp8MultiplyERKS1_@PLT
	movq	-80(%rbp), %rax
	movl	-128(%rbp), %r8d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	movq	%r13, -80(%rbp)
	movq	%rax, -144(%rbp)
	movl	%r8d, -72(%rbp)
	call	_ZN2v88internal5DiyFp8MultiplyERKS1_@PLT
	movq	-80(%rbp), %rax
	movl	$1, %r8d
	leaq	1(%rax), %rbx
	movq	%rax, -128(%rbp)
	movq	%rbx, %rax
	subq	-144(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rbx, -152(%rbp)
	movq	%rax, -176(%rbp)
	leaq	1(%rax), %r13
	movl	-156(%rbp), %eax
	movl	%eax, %r9d
	leal	64(%rax), %esi
	negl	%r9d
	movl	%r9d, %ecx
	movl	%r9d, -156(%rbp)
	salq	%cl, %r8
	shrq	%cl, %rdi
	movq	%r14, %rcx
	leaq	-1(%r8), %rdx
	movq	%r8, -144(%rbp)
	movq	%rdx, -168(%rbp)
	andq	%rdx, %rbx
	leaq	-100(%rbp), %rdx
	movl	%edi, -160(%rbp)
	call	_ZN2v88internalL15BiggestPowerTenEjiPjPi
	movl	-80(%rbp), %r11d
	movl	$0, (%r12)
	movq	-144(%rbp), %r8
	movl	-156(%rbp), %r9d
	leal	1(%r11), %eax
	testl	%eax, %eax
	jle	.L88
	movl	-100(%rbp), %edi
	movl	-160(%rbp), %eax
	xorl	%edx, %edx
	divl	%edi
	movl	%edx, %ecx
	addl	$48, %eax
	movb	%al, (%r15)
	movl	%ecx, %esi
	movslq	(%r12), %rdx
	movl	%r9d, %ecx
	movq	%rsi, %rax
	salq	%cl, %rsi
	leal	1(%rdx), %r10d
	addq	%rbx, %rsi
	movl	%r10d, (%r12)
	cmpq	%rsi, %r13
	ja	.L51
	movl	$3435973837, %r14d
	movl	%r9d, %ecx
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L63:
	xorl	%edx, %edx
	divl	%edi
	addl	$48, %eax
	movb	%al, (%rsi)
	movl	%edx, %esi
	movl	(%r12), %r9d
	movq	%rsi, %rax
	salq	%cl, %rsi
	leal	1(%r9), %r10d
	addq	%rbx, %rsi
	movl	%r10d, (%r12)
	cmpq	%rsi, %r13
	ja	.L122
.L52:
	movl	%edi, %edi
	movslq	%r10d, %r10
	subl	$1, %r11d
	imulq	%r14, %rdi
	leaq	(%r15,%r10), %rsi
	shrq	$35, %rdi
	cmpl	$-1, %r11d
	jne	.L63
	movl	%edi, -100(%rbp)
	movl	%ecx, %r9d
	xorl	%eax, %eax
.L50:
	movq	-168(%rbp), %r14
	movl	$1, %r11d
	movl	%r9d, %ecx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L123:
	movslq	%r10d, %r10
.L64:
	leaq	(%rbx,%rbx,4), %rbx
	leaq	(%r11,%r11,4), %rdx
	movq	%r11, %rdi
	subl	$1, %eax
	addq	%rbx, %rbx
	leaq	0(%r13,%r13,4), %rsi
	addq	%rdx, %rdx
	movq	%rbx, %r9
	addq	%rsi, %rsi
	andq	%r14, %rbx
	movq	%rdx, %r11
	shrq	%cl, %r9
	movq	%rsi, %r13
	addl	$48, %r9d
	movb	%r9b, (%r15,%r10)
	movl	(%r12), %r9d
	leal	1(%r9), %r10d
	movl	%r10d, (%r12)
	cmpq	%rbx, %rsi
	jbe	.L123
	movslq	%r9d, %rcx
	movq	-152(%rbp), %r9
	subq	-120(%rbp), %r9
	imulq	%rdx, %r9
	movq	%r9, %r10
	subq	%rdx, %r10
	cmpq	%r10, %rbx
	jnb	.L65
	movq	%rsi, %r11
	subq	%rbx, %r11
	cmpq	%r11, %r8
	ja	.L69
	subq	%r8, %r13
	movslq	%ecx, %r11
	subq	%rbx, %r13
	addq	%r15, %r11
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%rbx, %r14
	addq	%r8, %rbx
	cmpq	%rbx, %r10
	ja	.L67
	movq	%rdx, %r11
	subq	%r14, %r10
	subq	%r9, %r11
	addq	%rbx, %r11
	cmpq	%r11, %r10
	jb	.L124
	subb	$1, (%r15,%rcx)
.L65:
	addq	%r9, %rdx
	cmpq	%rdx, %rbx
	jnb	.L69
	movq	%rsi, %rcx
	subq	%rbx, %rcx
	cmpq	%rcx, %r8
	ja	.L69
	addq	%rbx, %r8
	cmpq	%r8, %rdx
	ja	.L83
	movq	%rdx, %rcx
	subq	%rdx, %r8
	subq	%rbx, %rcx
	cmpq	%r8, %rcx
	ja	.L83
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	(%rdi,%rdi,4), %rdx
	leaq	0(,%rdx,4), %rcx
	cmpq	%rbx, %rcx
	ja	.L83
	salq	$3, %rdx
	subq	%rdx, %rsi
	cmpq	%rbx, %rsi
	jb	.L83
	subl	-104(%rbp), %eax
	movl	%eax, %r11d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%r13, %r14
	subb	$1, (%r11)
	subq	%r8, %r14
	cmpq	%r13, %r8
	ja	.L69
	movq	%r14, %r13
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L120:
	movabsq	$4503599627370496, %rdx
	shrq	$52, %rcx
	leaq	(%rbx,%rdx), %r14
	andl	$2047, %ecx
	cmpq	%rdx, %r14
	leal	-1075(%rcx), %eax
	movq	%r14, %rbx
	sete	%sil
	cmpl	$1, %ecx
	setne	%dl
	subl	$1076, %ecx
	andl	%esi, %edx
	movl	%eax, %esi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%rcx, %r9
	subb	$1, (%r8)
	subq	%rdi, %r9
	cmpq	%rcx, %rdi
	ja	.L57
	movq	%r9, %rcx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L116:
	movl	%ecx, -100(%rbp)
	movl	%eax, %ebx
	movl	%edi, -104(%rbp)
.L73:
	movl	%ecx, %r10d
	movl	%ebx, %edx
	movl	%r9d, %ecx
	movl	$1, %r8d
	salq	%cl, %rdx
	salq	%cl, %r10
	leaq	-104(%rbp), %r9
	movq	%r15, %rdi
	addq	%r13, %rdx
	movq	%r10, %rcx
	call	_ZN2v88internalL16RoundWeedCountedENS0_6VectorIcEEimmmPi.isra.0
.L78:
	movl	-104(%rbp), %r11d
	subl	-108(%rbp), %r11d
	testb	%al, %al
	je	.L38
.L62:
	movq	-136(%rbp), %rax
	addl	(%r12), %r11d
	movl	%r11d, (%rax)
	movslq	(%r12), %rax
	movb	$0, (%r15,%rax)
	movl	$1, %eax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L118:
	movq	-176(%rbp), %rax
	subq	$3, %rax
	cmpq	%rsi, %rax
	jb	.L83
	subl	-104(%rbp), %r11d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L121:
	movabsq	$18014398509481983, %rbx
	leal	-2(%rsi), %ecx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L72:
	testl	%r14d, %r14d
	je	.L125
	xorl	%esi, %esi
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r14, %rbx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r9, %rsi
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L88:
	xorl	%r10d, %r10d
	jmp	.L50
.L115:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L119:
	call	__stack_chk_fail@PLT
.L125:
	movl	-100(%rbp), %ecx
	xorl	%esi, %esi
	jmp	.L73
	.cfi_endproc
.LFE5047:
	.size	_ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_, .-_ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_, @function
_GLOBAL__sub_I__ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_:
.LFB5815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5815:
	.size	_GLOBAL__sub_I__ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_, .-_GLOBAL__sub_I__ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal8FastDtoaEdNS0_12FastDtoaModeEiNS0_6VectorIcEEPiS4_
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
