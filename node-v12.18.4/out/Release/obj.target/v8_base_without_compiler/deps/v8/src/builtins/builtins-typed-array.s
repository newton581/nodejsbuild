	.file	"builtins-typed-array.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB5450:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE5450:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB5451:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5451:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB5453:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5453:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal12_GLOBAL__N_116CapRelativeIndexENS0_6HandleINS0_6ObjectEEEll,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internal12_GLOBAL__N_116CapRelativeIndexENS0_6HandleINS0_6ObjectEEEll, @function
_ZN2v88internal12_GLOBAL__N_116CapRelativeIndexENS0_6HandleINS0_6ObjectEEEll:
.LFB18353:
	.cfi_startproc
	movq	(%rdi), %rax
	testb	$1, %al
	jne	.L6
	sarq	$32, %rax
	js	.L17
	cmpq	%rdx, %rax
	cmovg	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	addq	%rdx, %rax
	cmpq	%rsi, %rax
	cmovl	%rsi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movsd	7(%rax), %xmm2
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	comisd	%xmm2, %xmm1
	ja	.L18
	minsd	%xmm2, %xmm0
	cvttsd2siq	%xmm0, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	pxor	%xmm1, %xmm1
	addsd	%xmm2, %xmm0
	cvtsi2sdq	%rsi, %xmm1
	maxsd	%xmm0, %xmm1
	cvttsd2siq	%xmm1, %rax
	ret
	.cfi_endproc
.LFE18353:
	.size	_ZN2v88internal12_GLOBAL__N_116CapRelativeIndexENS0_6HandleINS0_6ObjectEEEll, .-_ZN2v88internal12_GLOBAL__N_116CapRelativeIndexENS0_6HandleINS0_6ObjectEEEll
	.section	.rodata._ZN2v88internalL38Builtin_Impl_TypedArrayPrototypeBufferENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"get %TypedArray%.prototype.buffer"
	.section	.rodata._ZN2v88internalL38Builtin_Impl_TypedArrayPrototypeBufferENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"(location_) != nullptr"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL38Builtin_Impl_TypedArrayPrototypeBufferENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL38Builtin_Impl_TypedArrayPrototypeBufferENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL38Builtin_Impl_TypedArrayPrototypeBufferENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18352:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L20
.L23:
	leaq	.LC1(%rip), %rax
	xorl	%edx, %edx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$33, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L31
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movl	$62, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	%rax, %r13
.L24:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L26
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L26:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L32
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1086, 11(%rdx)
	jne	.L23
	leaq	-64(%rbp), %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12JSTypedArray9GetBufferEv@PLT
	movq	(%rax), %r13
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L32:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18352:
	.size	_ZN2v88internalL38Builtin_Impl_TypedArrayPrototypeBufferENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL38Builtin_Impl_TypedArrayPrototypeBufferENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL36Builtin_Impl_TypedArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.1,"aMS",@progbits,1
.LC4:
	.string	"%TypedArray%.prototype.fill"
	.section	.text._ZN2v88internalL36Builtin_Impl_TypedArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL36Builtin_Impl_TypedArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL36Builtin_Impl_TypedArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18361:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L34
.L36:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$100, %esi
.L71:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L35:
	movq	312(%r12), %r13
.L43:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L60
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L60:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L72
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1086, 11(%rdx)
	jne	.L36
	movq	23(%rax), %rdx
	movl	39(%rdx), %edx
	andl	$4, %edx
	jne	.L73
	movq	-1(%rax), %rdx
	leaq	-8(%rsi), %r15
	movq	%rdi, %r9
	movq	%rsi, %r13
	movzbl	14(%rdx), %r10d
	leaq	88(%r12), %rdx
	shrl	$3, %r10d
	cmpl	$5, %edi
	cmovle	%rdx, %r15
	leal	-26(%r10), %edx
	cmpb	$1, %dl
	jbe	.L74
	movq	(%r15), %rdx
	testb	$1, %dl
	je	.L44
	movq	-1(%rdx), %rdx
	cmpw	$65, 11(%rdx)
	jne	.L75
.L44:
	movq	47(%rax), %r8
	cmpl	$6, %r9d
	jle	.L63
	movq	-16(%r13), %rdx
	movq	88(%r12), %r11
	leaq	-16(%r13), %rdi
	cmpq	%rdx, %r11
	jne	.L76
.L63:
	movq	%r8, %rdx
	xorl	%ecx, %ecx
.L49:
	testq	%rdx, %rdx
	jg	.L58
.L70:
	movq	%rax, %r13
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L74:
	movq	%rdi, -96(%rbp)
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%r10d, -88(%rbp)
	call	_ZN2v88internal6BigInt10FromObjectEPNS0_7IsolateENS0_6HandleINS0_6ObjectEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L35
	movq	0(%r13), %rax
	movl	-88(%rbp), %r10d
	movq	-96(%rbp), %r9
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L58:
	movq	23(%rax), %rdx
	movl	39(%rdx), %edx
	andl	$4, %edx
	jne	.L70
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rax
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	(%rax,%r10,8), %rdi
	movq	(%rdi), %rax
	call	*192(%rax)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L76:
	andl	$1, %edx
	jne	.L77
.L51:
	movq	%r8, %rdx
	xorl	%esi, %esi
	movq	%r9, -104(%rbp)
	movl	%r10d, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_116CapRelativeIndexENS0_6HandleINS0_6ObjectEEEll
	movq	-104(%rbp), %r9
	leaq	-24(%r13), %rdi
	movq	-88(%rbp), %r8
	movq	%rax, %rcx
	leaq	88(%r12), %rax
	movl	-96(%rbp), %r10d
	cmpl	$7, %r9d
	cmove	%rax, %rdi
	movq	(%rdi), %rax
	cmpq	%r11, %rax
	jne	.L55
	movq	%r8, %rdx
	movq	0(%r13), %rax
	subq	%rcx, %rdx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%rdi, -96(%rbp)
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movl	%r10d, -88(%rbp)
	call	_ZN2v88internal6Object24ConvertToNumberOrNumericEPNS0_7IsolateENS0_6HandleIS1_EENS1_10ConversionE@PLT
	movl	-88(%rbp), %r10d
	movq	-96(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L35
	movq	0(%r13), %rax
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	.LC4(%rip), %rax
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$27, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$50, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L71
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	testb	$1, %al
	jne	.L78
.L57:
	movq	%r8, %rdx
	xorl	%esi, %esi
	movl	%r10d, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_116CapRelativeIndexENS0_6HandleINS0_6ObjectEEEll
	movq	-88(%rbp), %rcx
	movl	-96(%rbp), %r10d
	movq	%rax, %rdx
	movq	%rax, %r8
	movq	0(%r13), %rax
	subq	%rcx, %rdx
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%rdi, %rsi
	movq	%r12, %rdi
	movq	%r9, -104(%rbp)
	movl	%r10d, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-88(%rbp), %r8
	movl	-96(%rbp), %r10d
	testq	%rax, %rax
	movq	-104(%rbp), %r9
	movq	%rax, %rdi
	je	.L35
	movq	88(%r12), %r11
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%rdi, %rsi
	movq	%r12, %rdi
	movl	%r10d, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	movl	-104(%rbp), %r10d
	movq	%rax, %rdi
	je	.L35
	jmp	.L57
.L72:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18361:
	.size	_ZN2v88internalL36Builtin_Impl_TypedArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL36Builtin_Impl_TypedArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL39Builtin_Impl_TypedArrayPrototypeIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"%TypedArray%.prototype.indexOf"
	.section	.text._ZN2v88internalL39Builtin_Impl_TypedArrayPrototypeIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL39Builtin_Impl_TypedArrayPrototypeIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL39Builtin_Impl_TypedArrayPrototypeIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18367:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rcx
	testb	$1, %cl
	jne	.L80
.L82:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$100, %esi
.L110:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L81:
	movq	312(%r12), %r13
.L86:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L103
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L103:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L111
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	movq	-1(%rcx), %rax
	cmpw	$1086, 11(%rax)
	jne	.L82
	movq	23(%rcx), %rax
	movl	39(%rax), %edx
	andl	$4, %edx
	jne	.L112
	movq	47(%rcx), %r15
	testq	%r15, %r15
	jne	.L85
.L91:
	movabsq	$-4294967296, %r13
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rsi, %r13
	cmpl	$6, %edi
	jle	.L87
	leaq	-16(%rsi), %rdi
	testb	$1, -16(%rsi)
	jne	.L113
.L89:
	movq	%r15, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal12_GLOBAL__N_116CapRelativeIndexENS0_6HandleINS0_6ObjectEEEll
	movq	23(%rcx), %rdx
	movl	%eax, %r8d
	movl	39(%rdx), %edx
	andl	$4, %edx
	jne	.L91
.L94:
	leaq	-8(%r13), %r10
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L87:
	movl	39(%rax), %r8d
	andl	$4, %r8d
	jne	.L91
	leaq	88(%r12), %r10
	cmpl	$6, %edi
	je	.L94
.L95:
	movq	-1(%rcx), %rax
	movl	%r15d, %r9d
	movq	%r10, %rcx
	movq	%r12, %rsi
	movzbl	14(%rax), %edx
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rax
	shrl	$3, %edx
	movq	(%rax,%rdx,8), %rdi
	movq	%r13, %rdx
	movq	(%rdi), %rax
	call	*208(%rax)
	testb	%al, %al
	je	.L81
	movl	$2147483648, %eax
	movl	$4294967295, %ecx
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	ja	.L97
	movq	41112(%r12), %rdi
	salq	$32, %rdx
	movq	%rdx, %r13
	testq	%rdi, %rdi
	je	.L98
	movq	%rdx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L81
	movq	0(%r13), %rcx
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	.LC5(%rip), %rax
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$30, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$50, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L110
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L97:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	cvtsi2sdq	%rdx, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L98:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L114
.L100:
	movq	%r13, (%rax)
	jmp	.L86
.L114:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L100
.L111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18367:
	.size	_ZN2v88internalL39Builtin_Impl_TypedArrayPrototypeIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL39Builtin_Impl_TypedArrayPrototypeIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL40Builtin_Impl_TypedArrayPrototypeIncludesENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"%TypedArray%.prototype.includes"
	.section	.text._ZN2v88internalL40Builtin_Impl_TypedArrayPrototypeIncludesENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL40Builtin_Impl_TypedArrayPrototypeIncludesENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL40Builtin_Impl_TypedArrayPrototypeIncludesENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18364:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r13
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rcx
	testb	$1, %cl
	jne	.L116
.L118:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$100, %esi
.L140:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L117:
	movq	312(%r12), %r14
.L122:
	subl	$1, 41104(%r12)
	movq	%r13, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L131
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L131:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	movq	-1(%rcx), %rax
	cmpw	$1086, 11(%rax)
	jne	.L118
	movq	23(%rcx), %rax
	movl	39(%rax), %eax
	testb	$4, %al
	jne	.L142
	cmpl	$5, %edi
	jle	.L139
	movq	47(%rcx), %r15
	testq	%r15, %r15
	je	.L139
	movq	%rsi, %r14
	cmpl	$6, %edi
	je	.L133
	leaq	-16(%rsi), %rdi
	testb	$1, -16(%rsi)
	jne	.L143
.L126:
	movq	%r15, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal12_GLOBAL__N_116CapRelativeIndexENS0_6HandleINS0_6ObjectEEEll
	movq	%rax, %r8
.L124:
	movq	23(%rcx), %rax
	movl	39(%rax), %eax
	testb	$4, %al
	jne	.L139
	movq	-1(%rcx), %rax
	leaq	-8(%r14), %r10
	movl	%r15d, %r9d
	movq	%r12, %rsi
	movq	%r10, %rcx
	movzbl	14(%rax), %edx
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rax
	shrl	$3, %edx
	movq	(%rax,%rdx,8), %rdi
	movq	%r14, %rdx
	movq	(%rdi), %rax
	call	*200(%rax)
	testb	%al, %al
	je	.L117
	movzbl	%ah, %esi
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory9ToBooleanEb@PLT
	movq	(%rax), %r14
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L139:
	movq	120(%r12), %r14
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L133:
	xorl	%r8d, %r8d
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	.LC6(%rip), %rax
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$31, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$50, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L140
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L143:
	movq	%rdi, %rsi
	movq	%rdx, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L117
	movq	(%r14), %rcx
	jmp	.L126
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18364:
	.size	_ZN2v88internalL40Builtin_Impl_TypedArrayPrototypeIncludesENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL40Builtin_Impl_TypedArrayPrototypeIncludesENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL43Builtin_Impl_TypedArrayPrototypeLastIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"%TypedArray%.prototype.lastIndexOf"
	.section	.text._ZN2v88internalL43Builtin_Impl_TypedArrayPrototypeLastIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL43Builtin_Impl_TypedArrayPrototypeLastIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL43Builtin_Impl_TypedArrayPrototypeLastIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18370:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L145
.L147:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$100, %esi
.L169:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L146:
	movq	312(%r12), %r13
.L151:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L165
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L165:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L170
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1086, 11(%rdx)
	jne	.L147
	movq	23(%rax), %rdx
	movl	39(%rdx), %edx
	andl	$4, %edx
	jne	.L171
	movq	47(%rax), %r15
	testq	%r15, %r15
	je	.L155
	movq	%rdi, %r8
	movq	%rsi, %r13
	leaq	-1(%r15), %rcx
	cmpl	$6, %edi
	jg	.L172
.L152:
	testq	%rcx, %rcx
	jns	.L173
.L155:
	movabsq	$-4294967296, %r13
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L173:
	movq	0(%r13), %rax
	movq	23(%rax), %rdx
	movl	39(%rdx), %edx
	andl	$4, %edx
	jne	.L155
	movq	-1(%rax), %rax
	cmpl	$5, %r8d
	leaq	-8(%r13), %rsi
	leaq	88(%r12), %rdx
	cmovg	%rsi, %rdx
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rsi
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rsi,%rax,8), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*216(%rax)
	testb	%al, %al
	je	.L146
	movl	$2147483648, %eax
	movl	$4294967295, %ecx
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	ja	.L159
	movq	41112(%r12), %rdi
	salq	$32, %rdx
	movq	%rdx, %r13
	testq	%rdi, %rdi
	je	.L160
	movq	%rdx, %rsi
	call	_ZN2v88internal20CanonicalHandleScope6LookupEm@PLT
	movq	(%rax), %r13
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	-16(%rsi), %rdi
	testb	$1, -16(%rsi)
	jne	.L174
.L154:
	movq	%r15, %rdx
	movq	$-1, %rsi
	movq	%r8, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_116CapRelativeIndexENS0_6HandleINS0_6ObjectEEEll
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %r8
	cmpq	%rax, %rcx
	cmovg	%rax, %rcx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	.LC7(%rip), %rax
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$34, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$50, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L169
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L159:
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r12, %rdi
	cvtsi2sdq	%rdx, %xmm0
	call	_ZN2v88internal7Factory9NewNumberEdNS0_14AllocationTypeE@PLT
	movq	(%rax), %r13
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%rdi, %rsi
	movq	%r12, %rdi
	movq	%r8, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L146
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L160:
	movq	41088(%r12), %rax
	cmpq	41096(%r12), %rax
	je	.L175
.L162:
	movq	%r13, (%rax)
	jmp	.L151
.L175:
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope6ExtendEPNS0_7IsolateE@PLT
	jmp	.L162
.L170:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18370:
	.size	_ZN2v88internalL43Builtin_Impl_TypedArrayPrototypeLastIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL43Builtin_Impl_TypedArrayPrototypeLastIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_TypedArrayPrototypeCopyWithinENS0_16BuiltinArgumentsEPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"%TypedArray%.prototype.copyWithin"
	.section	.text._ZN2v88internalL42Builtin_Impl_TypedArrayPrototypeCopyWithinENS0_16BuiltinArgumentsEPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_TypedArrayPrototypeCopyWithinENS0_16BuiltinArgumentsEPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_TypedArrayPrototypeCopyWithinENS0_16BuiltinArgumentsEPNS0_7IsolateE:
.LFB18358:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	41096(%rdx), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	%rax, -88(%rbp)
	movq	(%rsi), %rax
	testb	$1, %al
	jne	.L177
.L179:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$100, %esi
.L205:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
.L178:
	movq	312(%r12), %r13
.L194:
	subl	$1, 41104(%r12)
	movq	-88(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L198
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L198:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L206
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movq	-1(%rax), %rdx
	cmpw	$1086, 11(%rdx)
	jne	.L179
	movq	23(%rax), %rdx
	movl	39(%rdx), %edx
	andl	$4, %edx
	jne	.L207
	movq	47(%rax), %r15
	movq	%rdi, %r14
	movq	%rsi, %r13
	cmpl	$5, %edi
	jle	.L200
	leaq	-8(%rsi), %rdi
	testb	$1, -8(%rsi)
	jne	.L208
.L184:
	movq	%r15, %rdx
	xorl	%esi, %esi
	call	_ZN2v88internal12_GLOBAL__N_116CapRelativeIndexENS0_6HandleINS0_6ObjectEEEll
	xorl	%r9d, %r9d
	movq	%rax, %r8
	movq	%r15, %rax
	cmpl	$6, %r14d
	jne	.L209
.L186:
	subq	%r8, %r15
	cmpq	%rax, %r15
	cmovg	%rax, %r15
	movq	0(%r13), %rax
.L182:
	testq	%r15, %r15
	jg	.L195
.L204:
	movq	%rax, %r13
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L195:
	movq	23(%rax), %rdx
	movl	39(%rdx), %edx
	andl	$4, %edx
	jne	.L204
	leaq	-80(%rbp), %rdi
	movq	%r8, -104(%rbp)
	movq	%r9, -96(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88internal12JSTypedArray12element_sizeEv@PLT
	movq	0(%r13), %rdx
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %r8
	movq	55(%rdx), %rcx
	imulq	%rax, %r9
	addq	63(%rdx), %rcx
	movq	%r15, %rdx
	imulq	%rax, %r8
	imulq	%rax, %rdx
	leaq	(%rcx,%r9), %rsi
	leaq	(%rcx,%r8), %rdi
	call	memmove@PLT
	movq	0(%r13), %r13
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%rdi, %rsi
	movq	%r12, %rdi
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L178
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L209:
	leaq	-16(%r13), %rdi
	testb	$1, -16(%r13)
	jne	.L210
.L188:
	xorl	%esi, %esi
	movq	%r15, %rdx
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_116CapRelativeIndexENS0_6HandleINS0_6ObjectEEEll
	cmpl	$7, %r14d
	leaq	-24(%r13), %rdi
	movq	-96(%rbp), %r8
	movq	%rax, %r9
	leaq	88(%r12), %rax
	cmove	%rax, %rdi
	movq	(%rdi), %rax
	cmpq	%rax, 88(%r12)
	jne	.L191
	movq	%r15, %rax
	subq	%r9, %rax
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%rdi, %rsi
	movq	%r12, %rdi
	movq	%r8, -96(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L178
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L207:
	leaq	.LC8(%rip), %rax
	leaq	-80(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	movq	$33, -72(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$50, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L205
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L200:
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L191:
	testb	$1, %al
	jne	.L211
.L193:
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%r8, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal12_GLOBAL__N_116CapRelativeIndexENS0_6HandleINS0_6ObjectEEEll
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %r8
	subq	%r9, %rax
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L211:
	movq	%rdi, %rsi
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	_ZN2v88internal6Object16ConvertToIntegerEPNS0_7IsolateENS0_6HandleIS1_EE@PLT
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L178
	jmp	.L193
.L206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18358:
	.size	_ZN2v88internalL42Builtin_Impl_TypedArrayPrototypeCopyWithinENS0_16BuiltinArgumentsEPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_TypedArrayPrototypeCopyWithinENS0_16BuiltinArgumentsEPNS0_7IsolateE
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8771:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L218
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L221
.L212:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L212
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8771:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL44Builtin_Impl_Stats_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC10:
	.string	"V8.Builtin_TypedArrayPrototypeBuffer"
	.section	.text._ZN2v88internalL44Builtin_Impl_Stats_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL44Builtin_Impl_Stats_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE, @function
_ZN2v88internalL44Builtin_Impl_Stats_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE:
.LFB18350:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L251
.L223:
	movq	_ZZN2v88internalL44Builtin_Impl_Stats_TypedArrayPrototypeBufferEiPmPNS0_7IsolateEE27trace_event_unique_atomic20(%rip), %rbx
	testq	%rbx, %rbx
	je	.L252
.L225:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L253
.L227:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL38Builtin_Impl_TypedArrayPrototypeBufferENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L254
.L231:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L255
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L256
.L226:
	movq	%rbx, _ZZN2v88internalL44Builtin_Impl_Stats_TypedArrayPrototypeBufferEiPmPNS0_7IsolateEE27trace_event_unique_atomic20(%rip)
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L253:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L257
.L228:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L229
	movq	(%rdi), %rax
	call	*8(%rax)
.L229:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L230
	movq	(%rdi), %rax
	call	*8(%rax)
.L230:
	leaq	.LC10(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L251:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$843, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L257:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC10(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L256:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L226
.L255:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18350:
	.size	_ZN2v88internalL44Builtin_Impl_Stats_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE, .-_ZN2v88internalL44Builtin_Impl_Stats_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC11:
	.string	"V8.Builtin_TypedArrayPrototypeReverse"
	.align 8
.LC12:
	.string	"%TypedArray%.prototype.reverse"
	.section	.text._ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE:
.LFB18371:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	addq	$-128, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L296
.L259:
	movq	_ZZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeReverseEiPmPNS0_7IsolateEE28trace_event_unique_atomic261(%rip), %rbx
	testq	%rbx, %rbx
	je	.L297
.L261:
	movq	$0, -144(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L298
.L263:
	movq	41088(%r12), %r14
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rsi
	testb	$1, %sil
	jne	.L267
.L269:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$100, %esi
.L295:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	312(%r12), %r13
.L272:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L275
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L275:
	leaq	-144(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L299
.L258:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L300
	leaq	-32(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	cmpw	$1086, 11(%rax)
	jne	.L269
	movq	23(%rsi), %rax
	movl	39(%rax), %eax
	testb	$4, %al
	jne	.L301
	movq	-1(%rsi), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	(%rdi), %rax
	call	*224(%rax)
	movq	0(%r13), %r13
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L298:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L302
.L264:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L265
	movq	(%rdi), %rax
	call	*8(%rax)
.L265:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L266
	movq	(%rdi), %rax
	call	*8(%rax)
.L266:
	leaq	.LC11(%rip), %rax
	movq	%rbx, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L297:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L303
.L262:
	movq	%rbx, _ZZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeReverseEiPmPNS0_7IsolateEE28trace_event_unique_atomic261(%rip)
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L296:
	movq	40960(%rdx), %rax
	leaq	-104(%rbp), %rsi
	movl	$849, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L299:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L301:
	leaq	.LC12(%rip), %rax
	leaq	-160(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -160(%rbp)
	movq	$30, -152(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$50, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L295
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L302:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC11(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L303:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L262
.L300:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18371:
	.size	_ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL42Builtin_Impl_Stats_TypedArrayPrototypeFillEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"V8.Builtin_TypedArrayPrototypeFill"
	.section	.text._ZN2v88internalL42Builtin_Impl_Stats_TypedArrayPrototypeFillEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL42Builtin_Impl_Stats_TypedArrayPrototypeFillEiPmPNS0_7IsolateE, @function
_ZN2v88internalL42Builtin_Impl_Stats_TypedArrayPrototypeFillEiPmPNS0_7IsolateE:
.LFB18359:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L333
.L305:
	movq	_ZZN2v88internalL42Builtin_Impl_Stats_TypedArrayPrototypeFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic107(%rip), %rbx
	testq	%rbx, %rbx
	je	.L334
.L307:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L335
.L309:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL36Builtin_Impl_TypedArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L336
.L313:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L337
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L338
.L308:
	movq	%rbx, _ZZN2v88internalL42Builtin_Impl_Stats_TypedArrayPrototypeFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic107(%rip)
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L335:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L339
.L310:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L311
	movq	(%rdi), %rax
	call	*8(%rax)
.L311:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L312
	movq	(%rdi), %rax
	call	*8(%rax)
.L312:
	leaq	.LC13(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L336:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L333:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$845, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L339:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC13(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L338:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L308
.L337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18359:
	.size	_ZN2v88internalL42Builtin_Impl_Stats_TypedArrayPrototypeFillEiPmPNS0_7IsolateE, .-_ZN2v88internalL42Builtin_Impl_Stats_TypedArrayPrototypeFillEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL46Builtin_Impl_Stats_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"V8.Builtin_TypedArrayPrototypeIncludes"
	.section	.text._ZN2v88internalL46Builtin_Impl_Stats_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL46Builtin_Impl_Stats_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE, @function
_ZN2v88internalL46Builtin_Impl_Stats_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE:
.LFB18362:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L369
.L341:
	movq	_ZZN2v88internalL46Builtin_Impl_Stats_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateEE28trace_event_unique_atomic162(%rip), %rbx
	testq	%rbx, %rbx
	je	.L370
.L343:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L371
.L345:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL40Builtin_Impl_TypedArrayPrototypeIncludesENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L372
.L349:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L373
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L374
.L344:
	movq	%rbx, _ZZN2v88internalL46Builtin_Impl_Stats_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateEE28trace_event_unique_atomic162(%rip)
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L371:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L375
.L346:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L347
	movq	(%rdi), %rax
	call	*8(%rax)
.L347:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L348
	movq	(%rdi), %rax
	call	*8(%rax)
.L348:
	leaq	.LC14(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L372:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L369:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$846, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L375:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC14(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L344
.L373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18362:
	.size	_ZN2v88internalL46Builtin_Impl_Stats_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE, .-_ZN2v88internalL46Builtin_Impl_Stats_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC15:
	.string	"V8.Builtin_TypedArrayPrototypeIndexOf"
	.section	.text._ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE, @function
_ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE:
.LFB18365:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L405
.L377:
	movq	_ZZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic196(%rip), %rbx
	testq	%rbx, %rbx
	je	.L406
.L379:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L407
.L381:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL39Builtin_Impl_TypedArrayPrototypeIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L408
.L385:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L409
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L410
.L380:
	movq	%rbx, _ZZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic196(%rip)
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L407:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L411
.L382:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L383
	movq	(%rdi), %rax
	call	*8(%rax)
.L383:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L384
	movq	(%rdi), %rax
	call	*8(%rax)
.L384:
	leaq	.LC15(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L408:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L405:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$847, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L411:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC15(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L380
.L409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18365:
	.size	_ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE, .-_ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL49Builtin_Impl_Stats_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"V8.Builtin_TypedArrayPrototypeLastIndexOf"
	.section	.text._ZN2v88internalL49Builtin_Impl_Stats_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL49Builtin_Impl_Stats_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE, @function
_ZN2v88internalL49Builtin_Impl_Stats_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE:
.LFB18368:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L441
.L413:
	movq	_ZZN2v88internalL49Builtin_Impl_Stats_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic227(%rip), %rbx
	testq	%rbx, %rbx
	je	.L442
.L415:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L443
.L417:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL43Builtin_Impl_TypedArrayPrototypeLastIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L444
.L421:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L445
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L446
.L416:
	movq	%rbx, _ZZN2v88internalL49Builtin_Impl_Stats_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic227(%rip)
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L443:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L447
.L418:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L419
	movq	(%rdi), %rax
	call	*8(%rax)
.L419:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L420
	movq	(%rdi), %rax
	call	*8(%rax)
.L420:
	leaq	.LC16(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L444:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L441:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$848, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L447:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC16(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L446:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L416
.L445:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18368:
	.size	_ZN2v88internalL49Builtin_Impl_Stats_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE, .-_ZN2v88internalL49Builtin_Impl_Stats_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE
	.section	.rodata._ZN2v88internalL48Builtin_Impl_Stats_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"V8.Builtin_TypedArrayPrototypeCopyWithin"
	.section	.text._ZN2v88internalL48Builtin_Impl_Stats_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL48Builtin_Impl_Stats_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE, @function
_ZN2v88internalL48Builtin_Impl_Stats_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE:
.LFB18356:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movslq	%edi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L477
.L449:
	movq	_ZZN2v88internalL48Builtin_Impl_Stats_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateEE27trace_event_unique_atomic46(%rip), %rbx
	testq	%rbx, %rbx
	je	.L478
.L451:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L479
.L453:
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v88internalL42Builtin_Impl_TypedArrayPrototypeCopyWithinENS0_16BuiltinArgumentsEPNS0_7IsolateE
	leaq	-160(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L480
.L457:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L481
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L478:
	.cfi_restore_state
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L482
.L452:
	movq	%rbx, _ZZN2v88internalL48Builtin_Impl_Stats_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateEE27trace_event_unique_atomic46(%rip)
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L479:
	pxor	%xmm0, %xmm0
	xorl	%r15d, %r15d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L483
.L454:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L455
	movq	(%rdi), %rax
	call	*8(%rax)
.L455:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L456
	movq	(%rdi), %rax
	call	*8(%rax)
.L456:
	leaq	.LC17(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r15, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L480:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L477:
	movq	40960(%rdx), %rax
	leaq	-120(%rbp), %rsi
	movl	$844, %edx
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L483:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC17(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L482:
	leaq	.LC9(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L452
.L481:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18356:
	.size	_ZN2v88internalL48Builtin_Impl_Stats_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE, .-_ZN2v88internalL48Builtin_Impl_Stats_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal33Builtin_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal33Builtin_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE
	.type	_ZN2v88internal33Builtin_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE, @function
_ZN2v88internal33Builtin_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE:
.LFB18351:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L488
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL38Builtin_Impl_TypedArrayPrototypeBufferENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore 6
	jmp	_ZN2v88internalL44Builtin_Impl_Stats_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18351:
	.size	_ZN2v88internal33Builtin_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE, .-_ZN2v88internal33Builtin_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal37Builtin_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal37Builtin_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE
	.type	_ZN2v88internal37Builtin_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE, @function
_ZN2v88internal37Builtin_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE:
.LFB18357:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L493
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL42Builtin_Impl_TypedArrayPrototypeCopyWithinENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L493:
	.cfi_restore 6
	jmp	_ZN2v88internalL48Builtin_Impl_Stats_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18357:
	.size	_ZN2v88internal37Builtin_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE, .-_ZN2v88internal37Builtin_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal31Builtin_TypedArrayPrototypeFillEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal31Builtin_TypedArrayPrototypeFillEiPmPNS0_7IsolateE
	.type	_ZN2v88internal31Builtin_TypedArrayPrototypeFillEiPmPNS0_7IsolateE, @function
_ZN2v88internal31Builtin_TypedArrayPrototypeFillEiPmPNS0_7IsolateE:
.LFB18360:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L498
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL36Builtin_Impl_TypedArrayPrototypeFillENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore 6
	jmp	_ZN2v88internalL42Builtin_Impl_Stats_TypedArrayPrototypeFillEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18360:
	.size	_ZN2v88internal31Builtin_TypedArrayPrototypeFillEiPmPNS0_7IsolateE, .-_ZN2v88internal31Builtin_TypedArrayPrototypeFillEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal35Builtin_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Builtin_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Builtin_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Builtin_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE:
.LFB18363:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L503
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL40Builtin_Impl_TypedArrayPrototypeIncludesENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore 6
	jmp	_ZN2v88internalL46Builtin_Impl_Stats_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18363:
	.size	_ZN2v88internal35Builtin_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE, .-_ZN2v88internal35Builtin_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Builtin_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Builtin_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Builtin_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Builtin_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE:
.LFB18366:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L508
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL39Builtin_Impl_TypedArrayPrototypeIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	.cfi_restore 6
	jmp	_ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18366:
	.size	_ZN2v88internal34Builtin_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE, .-_ZN2v88internal34Builtin_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal38Builtin_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal38Builtin_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE
	.type	_ZN2v88internal38Builtin_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE, @function
_ZN2v88internal38Builtin_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE:
.LFB18369:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L513
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%edi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internalL43Builtin_Impl_TypedArrayPrototypeLastIndexOfENS0_16BuiltinArgumentsEPNS0_7IsolateE
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore 6
	jmp	_ZN2v88internalL49Builtin_Impl_Stats_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE
	.cfi_endproc
.LFE18369:
	.size	_ZN2v88internal38Builtin_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE, .-_ZN2v88internal38Builtin_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal34Builtin_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal34Builtin_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE
	.type	_ZN2v88internal34Builtin_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE, @function
_ZN2v88internal34Builtin_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE:
.LFB18372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L528
	addl	$1, 41104(%rdx)
	movq	(%rsi), %rsi
	movq	41088(%rdx), %r14
	movq	41096(%rdx), %rbx
	testb	$1, %sil
	jne	.L517
.L519:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$100, %esi
.L527:
	movq	%r12, %rdi
	call	_ZN2v88internal7Factory12NewTypeErrorENS0_15MessageTemplateENS0_6HandleINS0_6ObjectEEES5_S5_@PLT
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	(%rax), %rsi
	call	_ZN2v88internal7Isolate5ThrowENS0_6ObjectEPNS0_15MessageLocationE@PLT
	movq	312(%r12), %r13
.L522:
	subl	$1, 41104(%r12)
	movq	%r14, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L514
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L514:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L529
	addq	$32, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L517:
	.cfi_restore_state
	movq	-1(%rsi), %rax
	cmpw	$1086, 11(%rax)
	jne	.L519
	movq	23(%rsi), %rax
	movl	39(%rax), %eax
	testb	$4, %al
	jne	.L530
	movq	-1(%rsi), %rax
	movq	_ZN2v88internal16ElementsAccessor19elements_accessors_E(%rip), %rdx
	movzbl	14(%rax), %eax
	shrl	$3, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	(%rdi), %rax
	call	*224(%rax)
	movq	0(%r13), %r13
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L528:
	call	_ZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE
	movq	%rax, %r13
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L530:
	leaq	.LC12(%rip), %rax
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, -64(%rbp)
	movq	$30, -56(%rbp)
	call	_ZN2v88internal7Factory20NewStringFromOneByteERKNS0_6VectorIKhEENS0_14AllocationTypeE@PLT
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$50, %esi
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L527
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L529:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18372:
	.size	_ZN2v88internal34Builtin_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE, .-_ZN2v88internal34Builtin_TypedArrayPrototypeReverseEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal33Builtin_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal33Builtin_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal33Builtin_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE:
.LFB22116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22116:
	.size	_GLOBAL__sub_I__ZN2v88internal33Builtin_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal33Builtin_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal33Builtin_TypedArrayPrototypeBufferEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeReverseEiPmPNS0_7IsolateEE28trace_event_unique_atomic261,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeReverseEiPmPNS0_7IsolateEE28trace_event_unique_atomic261, @object
	.size	_ZZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeReverseEiPmPNS0_7IsolateEE28trace_event_unique_atomic261, 8
_ZZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeReverseEiPmPNS0_7IsolateEE28trace_event_unique_atomic261:
	.zero	8
	.section	.bss._ZZN2v88internalL49Builtin_Impl_Stats_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic227,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL49Builtin_Impl_Stats_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic227, @object
	.size	_ZZN2v88internalL49Builtin_Impl_Stats_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic227, 8
_ZZN2v88internalL49Builtin_Impl_Stats_TypedArrayPrototypeLastIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic227:
	.zero	8
	.section	.bss._ZZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic196,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic196, @object
	.size	_ZZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic196, 8
_ZZN2v88internalL45Builtin_Impl_Stats_TypedArrayPrototypeIndexOfEiPmPNS0_7IsolateEE28trace_event_unique_atomic196:
	.zero	8
	.section	.bss._ZZN2v88internalL46Builtin_Impl_Stats_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateEE28trace_event_unique_atomic162,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL46Builtin_Impl_Stats_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateEE28trace_event_unique_atomic162, @object
	.size	_ZZN2v88internalL46Builtin_Impl_Stats_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateEE28trace_event_unique_atomic162, 8
_ZZN2v88internalL46Builtin_Impl_Stats_TypedArrayPrototypeIncludesEiPmPNS0_7IsolateEE28trace_event_unique_atomic162:
	.zero	8
	.section	.bss._ZZN2v88internalL42Builtin_Impl_Stats_TypedArrayPrototypeFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic107,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL42Builtin_Impl_Stats_TypedArrayPrototypeFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic107, @object
	.size	_ZZN2v88internalL42Builtin_Impl_Stats_TypedArrayPrototypeFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic107, 8
_ZZN2v88internalL42Builtin_Impl_Stats_TypedArrayPrototypeFillEiPmPNS0_7IsolateEE28trace_event_unique_atomic107:
	.zero	8
	.section	.bss._ZZN2v88internalL48Builtin_Impl_Stats_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateEE27trace_event_unique_atomic46,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL48Builtin_Impl_Stats_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateEE27trace_event_unique_atomic46, @object
	.size	_ZZN2v88internalL48Builtin_Impl_Stats_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateEE27trace_event_unique_atomic46, 8
_ZZN2v88internalL48Builtin_Impl_Stats_TypedArrayPrototypeCopyWithinEiPmPNS0_7IsolateEE27trace_event_unique_atomic46:
	.zero	8
	.section	.bss._ZZN2v88internalL44Builtin_Impl_Stats_TypedArrayPrototypeBufferEiPmPNS0_7IsolateEE27trace_event_unique_atomic20,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL44Builtin_Impl_Stats_TypedArrayPrototypeBufferEiPmPNS0_7IsolateEE27trace_event_unique_atomic20, @object
	.size	_ZZN2v88internalL44Builtin_Impl_Stats_TypedArrayPrototypeBufferEiPmPNS0_7IsolateEE27trace_event_unique_atomic20, 8
_ZZN2v88internalL44Builtin_Impl_Stats_TypedArrayPrototypeBufferEiPmPNS0_7IsolateEE27trace_event_unique_atomic20:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
