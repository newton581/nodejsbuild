	.file	"runtime-futex.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4479:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4479:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4480:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4480:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,"axG",@progbits,_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.type	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, @function
_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm:
.LFB4482:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4482:
	.size	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm, .-_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm
	.section	.text._ZN2v88internal7tracing12ScopedTracerD2Ev,"axG",@progbits,_ZN2v88internal7tracing12ScopedTracerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88internal7tracing12ScopedTracerD2Ev
	.type	_ZN2v88internal7tracing12ScopedTracerD2Ev, @function
_ZN2v88internal7tracing12ScopedTracerD2Ev:
.LFB8772:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rdi)
	je	.L11
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L14
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	.cfi_restore 3
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController24UpdateTraceEventDurationEPKhPKcm(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	je	.L5
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	8(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE8772:
	.size	_ZN2v88internal7tracing12ScopedTracerD2Ev, .-_ZN2v88internal7tracing12ScopedTracerD2Ev
	.weak	_ZN2v88internal7tracing12ScopedTracerD1Ev
	.set	_ZN2v88internal7tracing12ScopedTracerD1Ev,_ZN2v88internal7tracing12ScopedTracerD2Ev
	.section	.rodata._ZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"disabled-by-default-v8.runtime"
	.align 8
.LC1:
	.string	"V8.Runtime_Runtime_SetAllowAtomicsWait"
	.section	.rodata._ZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC2:
	.string	"args[0].IsBoolean()"
.LC3:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE.isra.0:
.LFB22075:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L51
.L16:
	movq	_ZZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateEE27trace_event_unique_atomic39(%rip), %r12
	testq	%r12, %r12
	je	.L52
.L18:
	movq	$0, -144(%rbp)
	movzbl	(%r12), %eax
	testb	$5, %al
	jne	.L53
.L20:
	addl	$1, 41104(%rbx)
	movq	0(%r13), %rax
	testb	$1, %al
	jne	.L54
.L25:
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L52:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L55
.L19:
	movq	%r12, _ZZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateEE27trace_event_unique_atomic39(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L54:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L25
	testb	$-2, 43(%rax)
	jne	.L25
	cmpq	%rax, 112(%rbx)
	leaq	-144(%rbp), %rdi
	movq	88(%rbx), %r12
	sete	45680(%rbx)
	subl	$1, 41104(%rbx)
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L56
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -64(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L58
.L21:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	leaq	.LC1(%rip), %rax
	movq	%r12, -136(%rbp)
	movq	%rax, -128(%rbp)
	leaq	-136(%rbp), %rax
	movq	%r14, -120(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L51:
	movq	40960(%rsi), %rax
	movl	$224, %edx
	leaq	-104(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -112(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	-104(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L58:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC1(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L21
.L57:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22075:
	.size	_ZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE.isra.0
	.section	.rodata._ZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE.isra.0.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"V8.Runtime_Runtime_AtomicsNumWaitersForTesting"
	.section	.rodata._ZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE.isra.0.str1.1,"aMS",@progbits,1
.LC5:
	.string	"args[0].IsJSTypedArray()"
.LC6:
	.string	"args[1].IsNumber()"
	.section	.rodata._ZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC10:
	.string	"TryNumberToSize(*index_object, &index)"
	.section	.rodata._ZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE.isra.0.str1.1
.LC11:
	.string	"!sta->WasDetached()"
.LC12:
	.string	"sta->GetBuffer()->is_shared()"
.LC13:
	.string	"index < sta->length()"
	.section	.rodata._ZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE.isra.0.str1.8
	.align 8
.LC14:
	.string	"sta->type() == kExternalInt32Array"
	.section	.text._ZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE.isra.0,"ax",@progbits
	.p2align 4
	.type	_ZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE.isra.0, @function
_ZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE.isra.0:
.LFB22082:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -96(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L107
.L60:
	movq	_ZZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateEE27trace_event_unique_atomic23(%rip), %rbx
	testq	%rbx, %rbx
	je	.L108
.L62:
	movq	$0, -160(%rbp)
	movzbl	(%rbx), %eax
	testb	$5, %al
	jne	.L109
.L64:
	movq	41088(%r12), %rax
	movq	41096(%r12), %rbx
	addl	$1, 41104(%r12)
	movq	0(%r13), %rdx
	movq	%rax, -184(%rbp)
	testb	$1, %dl
	jne	.L110
.L68:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L111
.L63:
	movq	%rbx, _ZZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateEE27trace_event_unique_atomic23(%rip)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L110:
	movq	-1(%rdx), %rax
	cmpw	$1086, 11(%rax)
	jne	.L68
	movq	-8(%r13), %rax
	testb	$1, %al
	jne	.L112
	sarq	$32, %rax
	movq	%rax, %r15
	js	.L73
.L74:
	movq	23(%rdx), %rax
	movl	39(%rax), %eax
	testb	$4, %al
	jne	.L113
	leaq	-168(%rbp), %r14
	movq	%rdx, -168(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal12JSTypedArray9GetBufferEv@PLT
	movq	(%rax), %rax
	movl	39(%rax), %eax
	testb	$8, %al
	je	.L114
	movq	0(%r13), %rax
	cmpq	%r15, 47(%rax)
	jbe	.L115
	movq	%r14, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	cmpl	$5, %eax
	jne	.L116
	movq	0(%r13), %rax
	movq	%r14, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88internal12JSTypedArray9GetBufferEv@PLT
	movq	%rax, %rdi
	movq	0(%r13), %rax
	movq	31(%rax), %rax
	leaq	(%rax,%r15,4), %rsi
	call	_ZN2v88internal14FutexEmulation20NumWaitersForTestingENS0_6HandleINS0_13JSArrayBufferEEEm@PLT
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	movq	-184(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L83
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L83:
	leaq	-160(%rbp), %rdi
	call	_ZN2v88internal7tracing12ScopedTracerD1Ev
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L117
.L59:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L118
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L119
	movsd	7(%rax), %xmm0
	comisd	.LC7(%rip), %xmm0
	jb	.L73
	movsd	.LC8(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L73
	movsd	.LC9(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L77
	cvttsd2siq	%xmm0, %r15
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	.LC10(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L109:
	pxor	%xmm0, %xmm0
	xorl	%r14d, %r14d
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v88internal7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L120
.L65:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L66
	movq	(%rdi), %rax
	call	*8(%rax)
.L66:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L67
	movq	(%rdi), %rax
	call	*8(%rax)
.L67:
	leaq	.LC4(%rip), %rax
	movq	%rbx, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-152(%rbp), %rax
	movq	%r14, -136(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L107:
	movq	40960(%rsi), %rax
	movl	$220, %edx
	leaq	-120(%rbp), %rsi
	leaq	23240(%rax), %rdi
	movq	%rdi, -128(%rbp)
	call	_ZN2v88internal16RuntimeCallStats5EnterEPNS0_16RuntimeCallTimerENS0_20RuntimeCallCounterIdE@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L119:
	leaq	.LC6(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %r15
	btcq	$63, %r15
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L113:
	leaq	.LC11(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L120:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC4(%rip), %rcx
	movl	$88, %esi
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r14
	addq	$64, %rsp
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L114:
	leaq	.LC12(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L115:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L116:
	leaq	.LC14(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L117:
	leaq	-120(%rbp), %rsi
	call	_ZN2v88internal16RuntimeCallStats5LeaveEPNS0_16RuntimeCallTimerE@PLT
	jmp	.L59
.L118:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE22082:
	.size	_ZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE.isra.0, .-_ZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE.isra.0
	.section	.text._ZN2v88internal35Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal35Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE
	.type	_ZN2v88internal35Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE, @function
_ZN2v88internal35Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE:
.LFB18342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	testl	%eax, %eax
	jne	.L145
	movq	41088(%rdx), %rax
	addl	$1, 41104(%rdx)
	movq	41096(%rdx), %rbx
	movq	(%rsi), %rdx
	movq	%rax, -72(%rbp)
	testb	$1, %dl
	jne	.L146
.L124:
	leaq	.LC5(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L146:
	movq	-1(%rdx), %rax
	cmpw	$1086, 11(%rax)
	jne	.L124
	movq	-8(%rsi), %rax
	testb	$1, %al
	jne	.L147
	sarq	$32, %rax
	movq	%rax, %r15
	js	.L129
.L130:
	movq	23(%rdx), %rax
	movl	39(%rax), %eax
	testb	$4, %al
	jne	.L148
	leaq	-64(%rbp), %r14
	movq	%rdx, -64(%rbp)
	movq	%r14, %rdi
	call	_ZN2v88internal12JSTypedArray9GetBufferEv@PLT
	movq	(%rax), %rax
	movl	39(%rax), %eax
	testb	$8, %al
	je	.L149
	movq	0(%r13), %rax
	cmpq	%r15, 47(%rax)
	jbe	.L150
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12JSTypedArray4typeEv@PLT
	cmpl	$5, %eax
	jne	.L151
	movq	0(%r13), %rax
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v88internal12JSTypedArray9GetBufferEv@PLT
	movq	%rax, %rdi
	movq	0(%r13), %rax
	movq	31(%rax), %rax
	leaq	(%rax,%r15,4), %rsi
	call	_ZN2v88internal14FutexEmulation20NumWaitersForTestingENS0_6HandleINS0_13JSArrayBufferEEEm@PLT
	subl	$1, 41104(%r12)
	movq	%rax, %r13
	movq	-72(%rbp), %rax
	movq	%rax, 41088(%r12)
	cmpq	41096(%r12), %rbx
	je	.L139
	movq	%rbx, 41096(%r12)
	movq	%r12, %rdi
	call	_ZN2v88internal11HandleScope16DeleteExtensionsEPNS0_7IsolateE@PLT
.L139:
	movq	%r13, %rax
.L121:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L152
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$65, 11(%rcx)
	jne	.L153
	movsd	7(%rax), %xmm0
	comisd	.LC7(%rip), %xmm0
	jb	.L129
	movsd	.LC8(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L129
	movsd	.LC9(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L133
	cvttsd2siq	%xmm0, %r15
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L129:
	leaq	.LC10(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%rdx, %rsi
	movq	%r13, %rdi
	call	_ZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE.isra.0
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L153:
	leaq	.LC6(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L133:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %r15
	btcq	$63, %r15
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L148:
	leaq	.LC11(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L149:
	leaq	.LC12(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	.LC13(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L151:
	leaq	.LC14(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L152:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE18342:
	.size	_ZN2v88internal35Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE, .-_ZN2v88internal35Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE
	.section	.text._ZN2v88internal27Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.globl	_ZN2v88internal27Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE
	.type	_ZN2v88internal27Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE, @function
_ZN2v88internal27Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE:
.LFB18345:
	.cfi_startproc
	endbr64
	movl	_ZN2v88internal12TracingFlags13runtime_statsE(%rip), %eax
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	testl	%eax, %eax
	jne	.L165
	addl	$1, 41104(%rdx)
	movq	(%rdi), %rax
	testb	$1, %al
	jne	.L166
.L157:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L157
	testb	$-2, 43(%rax)
	jne	.L157
	cmpq	%rax, 112(%rsi)
	movq	88(%rsi), %rax
	sete	45680(%rsi)
	subl	$1, 41104(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	jmp	_ZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE.isra.0
	.cfi_endproc
.LFE18345:
	.size	_ZN2v88internal27Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE, .-_ZN2v88internal27Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateE
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal35Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal35Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE, @function
_GLOBAL__sub_I__ZN2v88internal35Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE:
.LFB22064:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE22064:
	.size	_GLOBAL__sub_I__ZN2v88internal35Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE, .-_GLOBAL__sub_I__ZN2v88internal35Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal35Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateE
	.section	.bss._ZZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateEE27trace_event_unique_atomic39,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateEE27trace_event_unique_atomic39, @object
	.size	_ZZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateEE27trace_event_unique_atomic39, 8
_ZZN2v88internalL33Stats_Runtime_SetAllowAtomicsWaitEiPmPNS0_7IsolateEE27trace_event_unique_atomic39:
	.zero	8
	.section	.bss._ZZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateEE27trace_event_unique_atomic23,"aw",@nobits
	.align 8
	.type	_ZZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateEE27trace_event_unique_atomic23, @object
	.size	_ZZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateEE27trace_event_unique_atomic23, 8
_ZZN2v88internalL41Stats_Runtime_AtomicsNumWaitersForTestingEiPmPNS0_7IsolateEE27trace_event_unique_atomic23:
	.zero	8
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	0
	.long	0
	.align 8
.LC8:
	.long	0
	.long	1139802112
	.align 8
.LC9:
	.long	0
	.long	1138753536
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
