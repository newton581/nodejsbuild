	.file	"v8.cc"
	.text
	.section	.text._ZN2v88Platform20GetStackTracePrinterEv,"axG",@progbits,_ZN2v88Platform20GetStackTracePrinterEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v88Platform20GetStackTracePrinterEv
	.type	_ZN2v88Platform20GetStackTracePrinterEv, @function
_ZN2v88Platform20GetStackTracePrinterEv:
.LFB4994:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4994:
	.size	_ZN2v88Platform20GetStackTracePrinterEv, .-_ZN2v88Platform20GetStackTracePrinterEv
	.section	.text._ZNSt17_Function_handlerIFvvEPS0_E9_M_invokeERKSt9_Any_data,"axG",@progbits,_ZNSt17_Function_handlerIFvvEPS0_E9_M_invokeERKSt9_Any_data,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFvvEPS0_E9_M_invokeERKSt9_Any_data
	.type	_ZNSt17_Function_handlerIFvvEPS0_E9_M_invokeERKSt9_Any_data, @function
_ZNSt17_Function_handlerIFvvEPS0_E9_M_invokeERKSt9_Any_data:
.LFB24712:
	.cfi_startproc
	endbr64
	jmp	*(%rdi)
	.cfi_endproc
.LFE24712:
	.size	_ZNSt17_Function_handlerIFvvEPS0_E9_M_invokeERKSt9_Any_data, .-_ZNSt17_Function_handlerIFvvEPS0_E9_M_invokeERKSt9_Any_data
	.section	.text._ZNSt14_Function_base13_Base_managerIPFvvEE10_M_managerERSt9_Any_dataRKS4_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIPFvvEE10_M_managerERSt9_Any_dataRKS4_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIPFvvEE10_M_managerERSt9_Any_dataRKS4_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIPFvvEE10_M_managerERSt9_Any_dataRKS4_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIPFvvEE10_M_managerERSt9_Any_dataRKS4_St18_Manager_operation:
.LFB24713:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L5
	cmpl	$2, %edx
	jne	.L7
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
.L7:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE24713:
	.size	_ZNSt14_Function_base13_Base_managerIPFvvEE10_M_managerERSt9_Any_dataRKS4_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIPFvvEE10_M_managerERSt9_Any_dataRKS4_St18_Manager_operation
	.section	.rodata._ZN2v88internal2V828InitializeOncePerProcessImplEv.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The --jitless and --interpreted-frames-native-stack flags are incompatible."
	.section	.rodata._ZN2v88internal2V828InitializeOncePerProcessImplEv.str1.1,"aMS",@progbits,1
.LC1:
	.string	"Check failed: %s."
	.section	.text._ZN2v88internal2V828InitializeOncePerProcessImplEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2V828InitializeOncePerProcessImplEv
	.type	_ZN2v88internal2V828InitializeOncePerProcessImplEv, @function
_ZN2v88internal2V828InitializeOncePerProcessImplEv:
.LFB21439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$584, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88internal8FlagList23EnforceFlagImplicationsEv@PLT
	cmpb	$0, _ZN2v88internal16FLAG_predictableE(%rip)
	je	.L9
	movl	_ZN2v88internal16FLAG_random_seedE(%rip), %edx
	testl	%edx, %edx
	je	.L26
.L9:
	cmpb	$0, _ZN2v88internal22FLAG_stress_compactionE(%rip)
	je	.L10
	movb	$1, _ZN2v88internal34FLAG_force_marking_deque_overflowsE(%rip)
	movb	$1, _ZN2v88internal14FLAG_gc_globalE(%rip)
	movq	$1, _ZN2v88internal24FLAG_max_semi_space_sizeE(%rip)
.L10:
	cmpb	$0, _ZN2v88internal16FLAG_trace_turboE(%rip)
	jne	.L27
.L11:
	cmpb	$0, _ZN2v88internal12FLAG_jitlessE(%rip)
	je	.L15
	cmpb	$0, _ZN2v88internal36FLAG_correctness_fuzzer_suppressionsE(%rip)
	jne	.L16
	movb	$0, _ZN2v88internal16FLAG_expose_wasmE(%rip)
.L16:
	cmpb	$0, _ZN2v88internal36FLAG_interpreted_frames_native_stackE(%rip)
	jne	.L28
.L15:
	movzbl	_ZN2v88internal15FLAG_hard_abortE(%rip), %edi
	movq	_ZN2v88internal17FLAG_gc_fake_mmapE(%rip), %rsi
	call	_ZN2v84base2OS10InitializeEbPKc@PLT
	movslq	_ZN2v88internal16FLAG_random_seedE(%rip), %rdi
	testl	%edi, %edi
	jne	.L29
.L17:
	call	_ZN2v88internal7Isolate24InitializeOncePerProcessEv@PLT
	cmpb	$0, _ZN2v88internal11CpuFeatures12initialized_E(%rip)
	jne	.L18
	xorl	%edi, %edi
	movb	$1, _ZN2v88internal11CpuFeatures12initialized_E(%rip)
	call	_ZN2v88internal11CpuFeatures9ProbeImplEb@PLT
.L18:
	call	_ZN2v88internal16ElementsAccessor24InitializeOncePerProcessEv@PLT
	call	_ZN2v88internal12Bootstrapper24InitializeOncePerProcessEv@PLT
	call	_ZN2v88internal15CallDescriptors24InitializeOncePerProcessEv@PLT
	call	_ZN2v88internal4wasm10WasmEngine24InitializeOncePerProcessEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L30
	addq	$584, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	movl	$12347, _ZN2v88internal16FLAG_random_seedE(%rip)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L27:
	xorl	%esi, %esi
	leaq	-608(%rbp), %rdi
	leaq	-328(%rbp), %r15
	call	_ZN2v88internal7Isolate19GetTurboCfgFileNameB5cxx11EPS1_@PLT
	movq	-608(%rbp), %r8
	movq	%r15, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %r14
	leaq	-576(%rbp), %rbx
	leaq	-568(%rbp), %r13
	movq	%r8, -616(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %r12
	xorl	%eax, %eax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	16+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rcx
	movw	%ax, -104(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	%r12, -576(%rbp)
	movq	%r14, -328(%rbp)
	movq	$0, -112(%rbp)
	movq	-24(%r12), %rax
	movq	%rcx, -576(%rbp,%rax)
	movq	-576(%rbp), %rax
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	24+_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -576(%rbp)
	addq	$40, %rax
	movq	%rax, -328(%rbp)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEEC1Ev@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-616(%rbp), %r8
	movl	$48, %edx
	movq	%r13, %rdi
	movq	%r8, %rsi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	testq	%rax, %rax
	movq	-576(%rbp), %rax
	je	.L31
	addq	-24(%rax), %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.L13:
	leaq	64+_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	.LC2(%rip), %xmm0
	movq	%rax, -328(%rbp)
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	leaq	-464(%rbp), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-512(%rbp), %rdi
	movq	%rax, -568(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	%r12, -576(%rbp)
	movq	-24(%r12), %rax
	movq	%r15, %rdi
	movq	16+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rdx
	movq	%rdx, -576(%rbp,%rax)
	movq	%r14, -328(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-608(%rbp), %rdi
	leaq	-592(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L11
	call	_ZdlPv@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L29:
	call	_ZN2v88internal17SetRandomMmapSeedEl@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L31:
	addq	-24(%rax), %rbx
	movl	32(%rbx), %esi
	movq	%rbx, %rdi
	orl	$4, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L30:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21439:
	.size	_ZN2v88internal2V828InitializeOncePerProcessImplEv, .-_ZN2v88internal2V828InitializeOncePerProcessImplEv
	.section	.text._ZN2v88internal2V810InitializeEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2V810InitializeEv
	.type	_ZN2v88internal2V810InitializeEv, @function
_ZN2v88internal2V810InitializeEv:
.LFB21437:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt14_Function_base13_Base_managerIPFvvEE10_M_managerERSt9_Any_dataRKS4_St18_Manager_operation(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$88, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	_ZN2v88internal2V828InitializeOncePerProcessImplEv(%rip), %rax
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEPS0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movzbl	_ZN2v88internal9init_onceE(%rip), %eax
	cmpb	$2, %al
	je	.L33
	movq	-80(%rbp), %rax
	movq	$0, -48(%rbp)
	leaq	-64(%rbp), %r12
	testq	%rax, %rax
	je	.L34
	leaq	-96(%rbp), %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	call	*%rax
	movdqa	-80(%rbp), %xmm2
	movaps	%xmm2, -48(%rbp)
.L34:
	movq	%r12, %rsi
	leaq	_ZN2v88internal9init_onceE(%rip), %rdi
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L33
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L33:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L36
	leaq	-96(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L36:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$88, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L51:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21437:
	.size	_ZN2v88internal2V810InitializeEv, .-_ZN2v88internal2V810InitializeEv
	.section	.text._ZN2v88internal2V88TearDownEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2V88TearDownEv
	.type	_ZN2v88internal2V88TearDownEv, @function
_ZN2v88internal2V88TearDownEv:
.LFB21438:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v88internal4wasm10WasmEngine14GlobalTearDownEv@PLT
	call	_ZN2v88internal15CallDescriptors8TearDownEv@PLT
	call	_ZN2v88internal16ElementsAccessor8TearDownEv@PLT
	call	_ZN2v819RegisteredExtension13UnregisterAllEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88internal8FlagList13ResetAllFlagsEv@PLT
	.cfi_endproc
.LFE21438:
	.size	_ZN2v88internal2V88TearDownEv, .-_ZN2v88internal2V88TearDownEv
	.section	.text._ZN2v88internal2V824InitializeOncePerProcessEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2V824InitializeOncePerProcessEv
	.type	_ZN2v88internal2V824InitializeOncePerProcessEv, @function
_ZN2v88internal2V824InitializeOncePerProcessEv:
.LFB21440:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt14_Function_base13_Base_managerIPFvvEE10_M_managerERSt9_Any_dataRKS4_St18_Manager_operation(%rip), %rcx
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$88, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	_ZN2v88internal2V828InitializeOncePerProcessImplEv(%rip), %rax
	movq	%rax, -96(%rbp)
	leaq	_ZNSt17_Function_handlerIFvvEPS0_E9_M_invokeERKSt9_Any_data(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -80(%rbp)
	movzbl	_ZN2v88internal9init_onceE(%rip), %eax
	cmpb	$2, %al
	je	.L55
	movq	-80(%rbp), %rax
	movq	$0, -48(%rbp)
	leaq	-64(%rbp), %r12
	testq	%rax, %rax
	je	.L56
	leaq	-96(%rbp), %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	call	*%rax
	movdqa	-80(%rbp), %xmm2
	movaps	%xmm2, -48(%rbp)
.L56:
	movq	%r12, %rsi
	leaq	_ZN2v88internal9init_onceE(%rip), %rdi
	call	_ZN2v84base12CallOnceImplEPSt6atomicIhESt8functionIFvvEE@PLT
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L55
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L55:
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L54
	leaq	-96(%rbp), %rdi
	movl	$3, %edx
	movq	%rdi, %rsi
	call	*%rax
.L54:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L73
	addq	$88, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L73:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE21440:
	.size	_ZN2v88internal2V824InitializeOncePerProcessEv, .-_ZN2v88internal2V824InitializeOncePerProcessEv
	.section	.rodata._ZN2v88internal2V818InitializePlatformEPNS_8PlatformE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"!platform_"
.LC4:
	.string	"platform"
	.section	.text._ZN2v88internal2V818InitializePlatformEPNS_8PlatformE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2V818InitializePlatformEPNS_8PlatformE
	.type	_ZN2v88internal2V818InitializePlatformEPNS_8PlatformE, @function
_ZN2v88internal2V818InitializePlatformEPNS_8PlatformE:
.LFB21441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpq	$0, _ZN2v88internal2V89platform_E(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	jne	.L80
	testq	%rdi, %rdi
	je	.L81
	movq	%rdi, _ZN2v88internal2V89platform_E(%rip)
	movq	(%rdi), %rax
	leaq	_ZN2v88Platform20GetStackTracePrinterEv(%rip), %rdx
	xorl	%r8d, %r8d
	movq	136(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L82
	movq	%r8, %rdi
	call	_ZN2v84base18SetPrintStackTraceEPFvvE@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87tracing23TracingCategoryObserver5SetUpEv@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	.cfi_restore_state
	call	*%rax
	movq	%rax, %r8
	movq	%r8, %rdi
	call	_ZN2v84base18SetPrintStackTraceEPFvvE@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87tracing23TracingCategoryObserver5SetUpEv@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
	leaq	.LC3(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	leaq	.LC4(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21441:
	.size	_ZN2v88internal2V818InitializePlatformEPNS_8PlatformE, .-_ZN2v88internal2V818InitializePlatformEPNS_8PlatformE
	.section	.rodata._ZN2v88internal2V816ShutdownPlatformEv.str1.1,"aMS",@progbits,1
.LC5:
	.string	"platform_"
	.section	.text._ZN2v88internal2V816ShutdownPlatformEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2V816ShutdownPlatformEv
	.type	_ZN2v88internal2V816ShutdownPlatformEv, @function
_ZN2v88internal2V816ShutdownPlatformEv:
.LFB21442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpq	$0, _ZN2v88internal2V89platform_E(%rip)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	je	.L86
	call	_ZN2v87tracing23TracingCategoryObserver8TearDownEv@PLT
	xorl	%edi, %edi
	call	_ZN2v84base18SetPrintStackTraceEPFvvE@PLT
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$0, _ZN2v88internal2V89platform_E(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	leaq	.LC5(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21442:
	.size	_ZN2v88internal2V816ShutdownPlatformEv, .-_ZN2v88internal2V816ShutdownPlatformEv
	.section	.text._ZN2v88internal2V818GetCurrentPlatformEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2V818GetCurrentPlatformEv
	.type	_ZN2v88internal2V818GetCurrentPlatformEv, @function
_ZN2v88internal2V818GetCurrentPlatformEv:
.LFB21443:
	.cfi_startproc
	endbr64
	movq	_ZN2v88internal2V89platform_E(%rip), %rax
	ret
	.cfi_endproc
.LFE21443:
	.size	_ZN2v88internal2V818GetCurrentPlatformEv, .-_ZN2v88internal2V818GetCurrentPlatformEv
	.section	.text._ZN2v88internal2V821SetPlatformForTestingEPNS_8PlatformE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2V821SetPlatformForTestingEPNS_8PlatformE
	.type	_ZN2v88internal2V821SetPlatformForTestingEPNS_8PlatformE, @function
_ZN2v88internal2V821SetPlatformForTestingEPNS_8PlatformE:
.LFB21444:
	.cfi_startproc
	endbr64
	movq	%rdi, _ZN2v88internal2V89platform_E(%rip)
	ret
	.cfi_endproc
.LFE21444:
	.size	_ZN2v88internal2V821SetPlatformForTestingEPNS_8PlatformE, .-_ZN2v88internal2V821SetPlatformForTestingEPNS_8PlatformE
	.section	.rodata._ZN2v88internal2V814SetNativesBlobEPNS_11StartupDataE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"unreachable code"
	.section	.text._ZN2v88internal2V814SetNativesBlobEPNS_11StartupDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2V814SetNativesBlobEPNS_11StartupDataE
	.type	_ZN2v88internal2V814SetNativesBlobEPNS_11StartupDataE, @function
_ZN2v88internal2V814SetNativesBlobEPNS_11StartupDataE:
.LFB21445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE21445:
	.size	_ZN2v88internal2V814SetNativesBlobEPNS_11StartupDataE, .-_ZN2v88internal2V814SetNativesBlobEPNS_11StartupDataE
	.section	.text._ZN2v88internal2V815SetSnapshotBlobEPNS_11StartupDataE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88internal2V815SetSnapshotBlobEPNS_11StartupDataE
	.type	_ZN2v88internal2V815SetSnapshotBlobEPNS_11StartupDataE, @function
_ZN2v88internal2V815SetSnapshotBlobEPNS_11StartupDataE:
.LFB26767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE26767:
	.size	_ZN2v88internal2V815SetSnapshotBlobEPNS_11StartupDataE, .-_ZN2v88internal2V815SetSnapshotBlobEPNS_11StartupDataE
	.section	.text._ZN2v88Platform21SystemClockTimeMillisEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN2v88Platform21SystemClockTimeMillisEv
	.type	_ZN2v88Platform21SystemClockTimeMillisEv, @function
_ZN2v88Platform21SystemClockTimeMillisEv:
.LFB21447:
	.cfi_startproc
	endbr64
	jmp	_ZN2v84base2OS17TimeCurrentMillisEv@PLT
	.cfi_endproc
.LFE21447:
	.size	_ZN2v88Platform21SystemClockTimeMillisEv, .-_ZN2v88Platform21SystemClockTimeMillisEv
	.section	.text.startup._GLOBAL__sub_I__ZN2v88internal9init_onceE,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN2v88internal9init_onceE, @function
_GLOBAL__sub_I__ZN2v88internal9init_onceE:
.LFB26751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE26751:
	.size	_GLOBAL__sub_I__ZN2v88internal9init_onceE, .-_GLOBAL__sub_I__ZN2v88internal9init_onceE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN2v88internal9init_onceE
	.globl	_ZN2v88internal2V89platform_E
	.section	.bss._ZN2v88internal2V89platform_E,"aw",@nobits
	.align 8
	.type	_ZN2v88internal2V89platform_E, @object
	.size	_ZN2v88internal2V89platform_E, 8
_ZN2v88internal2V89platform_E:
	.zero	8
	.globl	_ZN2v88internal9init_onceE
	.section	.bss._ZN2v88internal9init_onceE,"aw",@nobits
	.type	_ZN2v88internal9init_onceE, @object
	.size	_ZN2v88internal9init_onceE, 1
_ZN2v88internal9init_onceE:
	.zero	1
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.section	.data.rel.ro,"aw"
	.align 8
.LC2:
	.quad	_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE+24
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
