	.file	"disasm-x64.cc"
	.text
	.section	.rodata._ZNK6disasm13NameConverter17NameOfCPURegisterEi.str1.1,"aMS",@progbits,1
.LC0:
	.string	"noreg"
	.section	.text._ZNK6disasm13NameConverter17NameOfCPURegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK6disasm13NameConverter17NameOfCPURegisterEi
	.type	_ZNK6disasm13NameConverter17NameOfCPURegisterEi, @function
_ZNK6disasm13NameConverter17NameOfCPURegisterEi:
.LFB5180:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	cmpl	$15, %esi
	jbe	.L5
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE5180:
	.size	_ZNK6disasm13NameConverter17NameOfCPURegisterEi, .-_ZNK6disasm13NameConverter17NameOfCPURegisterEi
	.section	.text._ZNK6disasm13NameConverter21NameOfByteCPURegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK6disasm13NameConverter21NameOfByteCPURegisterEi
	.type	_ZNK6disasm13NameConverter21NameOfByteCPURegisterEi, @function
_ZNK6disasm13NameConverter21NameOfByteCPURegisterEi:
.LFB5181:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	cmpl	$15, %esi
	jbe	.L9
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL13byte_cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE5181:
	.size	_ZNK6disasm13NameConverter21NameOfByteCPURegisterEi, .-_ZNK6disasm13NameConverter21NameOfByteCPURegisterEi
	.section	.rodata._ZNK6disasm13NameConverter17NameOfXMMRegisterEi.str1.1,"aMS",@progbits,1
.LC1:
	.string	"noxmmreg"
	.section	.text._ZNK6disasm13NameConverter17NameOfXMMRegisterEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi
	.type	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi, @function
_ZNK6disasm13NameConverter17NameOfXMMRegisterEi:
.LFB5182:
	.cfi_startproc
	endbr64
	leaq	.LC1(%rip), %rax
	cmpl	$15, %esi
	jbe	.L13
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.cfi_endproc
.LFE5182:
	.size	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi, .-_ZNK6disasm13NameConverter17NameOfXMMRegisterEi
	.section	.text._ZN6disasm13NameConverterD2Ev,"axG",@progbits,_ZN6disasm13NameConverterD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6disasm13NameConverterD2Ev
	.type	_ZN6disasm13NameConverterD2Ev, @function
_ZN6disasm13NameConverterD2Ev:
.LFB5958:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5958:
	.size	_ZN6disasm13NameConverterD2Ev, .-_ZN6disasm13NameConverterD2Ev
	.weak	_ZN6disasm13NameConverterD1Ev
	.set	_ZN6disasm13NameConverterD1Ev,_ZN6disasm13NameConverterD2Ev
	.section	.rodata._ZNK6disasm13NameConverter16RootRelativeNameEi.str1.1,"aMS",@progbits,1
.LC2:
	.string	"unreachable code"
	.section	.text._ZNK6disasm13NameConverter16RootRelativeNameEi,"axG",@progbits,_ZNK6disasm13NameConverter16RootRelativeNameEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6disasm13NameConverter16RootRelativeNameEi
	.type	_ZNK6disasm13NameConverter16RootRelativeNameEi, @function
_ZNK6disasm13NameConverter16RootRelativeNameEi:
.LFB4083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE4083:
	.size	_ZNK6disasm13NameConverter16RootRelativeNameEi, .-_ZNK6disasm13NameConverter16RootRelativeNameEi
	.section	.text._ZNK6disasm13NameConverter10NameInCodeEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK6disasm13NameConverter10NameInCodeEPh
	.type	_ZNK6disasm13NameConverter10NameInCodeEPh, @function
_ZNK6disasm13NameConverter10NameInCodeEPh:
.LFB5183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5183:
	.size	_ZNK6disasm13NameConverter10NameInCodeEPh, .-_ZNK6disasm13NameConverter10NameInCodeEPh
	.section	.rodata._ZNK6disasm13NameConverter13NameOfAddressEPh.str1.1,"aMS",@progbits,1
.LC3:
	.string	"%p"
	.section	.text._ZNK6disasm13NameConverter13NameOfAddressEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK6disasm13NameConverter13NameOfAddressEPh
	.type	_ZNK6disasm13NameConverter13NameOfAddressEPh, @function
_ZNK6disasm13NameConverter13NameOfAddressEPh:
.LFB5178:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rcx
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rbx), %rsi
	movq	8(%rdi), %rdi
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	8(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5178:
	.size	_ZNK6disasm13NameConverter13NameOfAddressEPh, .-_ZNK6disasm13NameConverter13NameOfAddressEPh
	.section	.text._ZN6disasm13NameConverterD0Ev,"axG",@progbits,_ZN6disasm13NameConverterD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6disasm13NameConverterD0Ev
	.type	_ZN6disasm13NameConverterD0Ev, @function
_ZN6disasm13NameConverterD0Ev:
.LFB5960:
	.cfi_startproc
	endbr64
	movl	$152, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5960:
	.size	_ZN6disasm13NameConverterD0Ev, .-_ZN6disasm13NameConverterD0Ev
	.section	.text._ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi,"axG",@progbits,_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi
	.type	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi, @function
_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi:
.LFB5149:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L23
	leaq	.LC0(%rip), %rax
	cmpl	$15, %esi
	jbe	.L26
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	jmp	*%rax
	.cfi_endproc
.LFE5149:
	.size	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi, .-_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi
	.section	.text._ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi,"axG",@progbits,_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi
	.type	_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi, @function
_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi:
.LFB5150:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	leaq	_ZNK6disasm13NameConverter21NameOfByteCPURegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L28
	leaq	.LC0(%rip), %rax
	cmpl	$15, %esi
	jbe	.L31
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL13byte_cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	jmp	*%rax
	.cfi_endproc
.LFE5150:
	.size	_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi, .-_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi
	.section	.text._ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi,"axG",@progbits,_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi,comdat
	.align 2
	.p2align 4
	.weak	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi
	.type	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi, @function
_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi:
.LFB5151:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L33
	leaq	.LC1(%rip), %rax
	cmpl	$15, %esi
	jbe	.L36
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	jmp	*%rax
	.cfi_endproc
.LFE5151:
	.size	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi, .-_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi
	.section	.text._ZNK6disasm13NameConverter14NameOfConstantEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZNK6disasm13NameConverter14NameOfConstantEPh
	.type	_ZNK6disasm13NameConverter14NameOfConstantEPh, @function
_ZNK6disasm13NameConverter14NameOfConstantEPh:
.LFB5179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6disasm13NameConverter13NameOfAddressEPh(%rip), %rdx
	movq	%rsi, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L38
	movq	16(%r12), %rsi
	movq	8(%rdi), %rdi
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	8(%r12), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE5179:
	.size	_ZNK6disasm13NameConverter14NameOfConstantEPh, .-_ZNK6disasm13NameConverter14NameOfConstantEPh
	.section	.rodata._ZN6disasm16InstructionTable5ClearEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"(bad)"
	.section	.text._ZN6disasm16InstructionTable5ClearEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm16InstructionTable5ClearEv
	.type	_ZN6disasm16InstructionTable5ClearEv, @function
_ZN6disasm16InstructionTable5ClearEv:
.LFB5113:
	.cfi_startproc
	endbr64
	leaq	.LC4(%rip), %rdx
	leaq	8(%rdi), %rax
	leaq	6152(%rdi), %rcx
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%rdx, -8(%rax)
	addq	$384, %rax
	movq	%rdx, -368(%rax)
	movq	%rdx, -344(%rax)
	movq	%rdx, -320(%rax)
	movq	%rdx, -296(%rax)
	movq	%rdx, -272(%rax)
	movq	%rdx, -248(%rax)
	movq	%rdx, -224(%rax)
	movq	%rdx, -200(%rax)
	movq	%rdx, -176(%rax)
	movq	%rdx, -152(%rax)
	movq	%rdx, -128(%rax)
	movq	%rdx, -104(%rax)
	movq	%rdx, -80(%rax)
	movq	%rdx, -56(%rax)
	movq	%rdx, -32(%rax)
	movq	$0, -384(%rax)
	movq	$0, -360(%rax)
	movq	$0, -336(%rax)
	movq	$0, -312(%rax)
	movq	$0, -288(%rax)
	movq	$0, -264(%rax)
	movq	$0, -240(%rax)
	movq	$0, -216(%rax)
	movq	$0, -192(%rax)
	movq	$0, -168(%rax)
	movq	$0, -144(%rax)
	movq	$0, -120(%rax)
	movq	$0, -96(%rax)
	movq	$0, -72(%rax)
	movq	$0, -48(%rax)
	movq	$0, -24(%rax)
	movb	$0, -376(%rax)
	movb	$0, -352(%rax)
	movb	$0, -328(%rax)
	movb	$0, -304(%rax)
	movb	$0, -280(%rax)
	movb	$0, -256(%rax)
	movb	$0, -232(%rax)
	movb	$0, -208(%rax)
	movb	$0, -184(%rax)
	movb	$0, -160(%rax)
	movb	$0, -136(%rax)
	movb	$0, -112(%rax)
	movb	$0, -88(%rax)
	movb	$0, -64(%rax)
	movb	$0, -40(%rax)
	movb	$0, -16(%rax)
	cmpq	%rcx, %rax
	jne	.L43
	ret
	.cfi_endproc
.LFE5113:
	.size	_ZN6disasm16InstructionTable5ClearEv, .-_ZN6disasm16InstructionTable5ClearEv
	.section	.rodata._ZN6disasm16InstructionTable4InitEv.str1.1,"aMS",@progbits,1
.LC5:
	.string	"push"
.LC6:
	.string	"pop"
.LC7:
	.string	"mov"
	.section	.text._ZN6disasm16InstructionTable4InitEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm16InstructionTable4InitEv
	.type	_ZN6disasm16InstructionTable4InitEv, @function
_ZN6disasm16InstructionTable4InitEv:
.LFB5114:
	.cfi_startproc
	endbr64
	leaq	16+_ZN6disasmL18two_operands_instrE(%rip), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L46:
	movq	-8(%rdx), %rsi
	leaq	(%rax,%rax,2), %rax
	addq	$16, %rdx
	leaq	(%rdi,%rax,8), %rcx
	movq	%rsi, (%rcx)
	movl	-28(%rdx), %eax
	movl	$2, 8(%rcx)
	movl	%eax, %esi
	shrl	$2, %eax
	andl	$1, %eax
	andl	$-5, %esi
	movl	%esi, 12(%rcx)
	movb	%al, 16(%rcx)
	movslq	-16(%rdx), %rax
	testl	%eax, %eax
	jns	.L46
	leaq	16+_ZN6disasmL19zero_operands_instrE(%rip), %rdx
	movl	$195, %eax
	.p2align 4,,10
	.p2align 3
.L47:
	movq	-8(%rdx), %rsi
	leaq	(%rax,%rax,2), %rax
	addq	$16, %rdx
	leaq	(%rdi,%rax,8), %rcx
	movq	%rsi, (%rcx)
	movl	-28(%rdx), %eax
	movl	$1, 8(%rcx)
	movl	%eax, %esi
	shrl	$2, %eax
	andl	$1, %eax
	andl	$-5, %esi
	movl	%esi, 12(%rcx)
	movb	%al, 16(%rcx)
	movslq	-16(%rdx), %rax
	testl	%eax, %eax
	jns	.L47
	leaq	16+_ZN6disasmL15call_jump_instrE(%rip), %rdx
	movl	$232, %eax
	.p2align 4,,10
	.p2align 3
.L48:
	movq	-8(%rdx), %rsi
	leaq	(%rax,%rax,2), %rax
	addq	$16, %rdx
	leaq	(%rdi,%rax,8), %rcx
	movq	%rsi, (%rcx)
	movl	-28(%rdx), %eax
	movl	$7, 8(%rcx)
	movl	%eax, %esi
	shrl	$2, %eax
	andl	$1, %eax
	andl	$-5, %esi
	movl	%esi, 12(%rcx)
	movb	%al, 16(%rcx)
	movslq	-16(%rdx), %rax
	testl	%eax, %eax
	jns	.L48
	leaq	16+_ZN6disasmL21short_immediate_instrE(%rip), %rdx
	movl	$5, %eax
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-8(%rdx), %rsi
	leaq	(%rax,%rax,2), %rax
	addq	$16, %rdx
	leaq	(%rdi,%rax,8), %rcx
	movq	%rsi, (%rcx)
	movl	-28(%rdx), %eax
	movl	$8, 8(%rcx)
	movl	%eax, %esi
	shrl	$2, %eax
	andl	$1, %eax
	andl	$-5, %esi
	movl	%esi, 12(%rcx)
	movb	%al, 16(%rcx)
	movslq	-16(%rdx), %rax
	testl	%eax, %eax
	jns	.L49
	leaq	.LC5(%rip), %rax
	movq	$0, 2688(%rdi)
	movl	$3, 2696(%rdi)
	movq	$0, 2712(%rdi)
	movl	$3, 2720(%rdi)
	movq	$0, 2736(%rdi)
	movl	$3, 2744(%rdi)
	movq	$0, 2760(%rdi)
	movl	$3, 2768(%rdi)
	movq	$0, 2784(%rdi)
	movl	$3, 2792(%rdi)
	movq	$0, 2808(%rdi)
	movl	$3, 2816(%rdi)
	movq	$0, 2832(%rdi)
	movl	$3, 2840(%rdi)
	movq	$0, 2856(%rdi)
	movl	$3, 2864(%rdi)
	movq	$0, 2880(%rdi)
	movl	$3, 2888(%rdi)
	movq	$0, 2904(%rdi)
	movl	$3, 2912(%rdi)
	movq	$0, 2928(%rdi)
	movl	$3, 2936(%rdi)
	movq	$0, 2952(%rdi)
	movl	$3, 2960(%rdi)
	movq	$0, 2976(%rdi)
	movl	$3, 2984(%rdi)
	movq	$0, 3000(%rdi)
	movl	$3, 3008(%rdi)
	movq	$0, 3024(%rdi)
	movl	$3, 3032(%rdi)
	movq	$0, 3048(%rdi)
	movl	$3, 3056(%rdi)
	movq	%rax, 1920(%rdi)
	movq	%rax, 1944(%rdi)
	movq	%rax, 1968(%rdi)
	movq	%rax, 1992(%rdi)
	movq	%rax, 2016(%rdi)
	movq	%rax, 2040(%rdi)
	movq	%rax, 2064(%rdi)
	movq	%rax, 2088(%rdi)
	leaq	.LC6(%rip), %rax
	movq	%rax, 2112(%rdi)
	movq	%rax, 2136(%rdi)
	movq	%rax, 2160(%rdi)
	movl	$5, 1928(%rdi)
	movb	$0, 1936(%rdi)
	movl	$5, 1952(%rdi)
	movb	$0, 1960(%rdi)
	movl	$5, 1976(%rdi)
	movb	$0, 1984(%rdi)
	movl	$5, 2000(%rdi)
	movb	$0, 2008(%rdi)
	movl	$5, 2024(%rdi)
	movb	$0, 2032(%rdi)
	movl	$5, 2048(%rdi)
	movb	$0, 2056(%rdi)
	movl	$5, 2072(%rdi)
	movb	$0, 2080(%rdi)
	movl	$5, 2096(%rdi)
	movb	$0, 2104(%rdi)
	movl	$5, 2120(%rdi)
	movb	$0, 2128(%rdi)
	movl	$5, 2144(%rdi)
	movb	$0, 2152(%rdi)
	movl	$5, 2168(%rdi)
	movb	$0, 2176(%rdi)
	movq	%rax, 2184(%rdi)
	movq	%rax, 2208(%rdi)
	movq	%rax, 2232(%rdi)
	movq	%rax, 2256(%rdi)
	movq	%rax, 2280(%rdi)
	leaq	.LC7(%rip), %rax
	movl	$5, 2192(%rdi)
	movb	$0, 2200(%rdi)
	movl	$5, 2216(%rdi)
	movb	$0, 2224(%rdi)
	movl	$5, 2240(%rdi)
	movb	$0, 2248(%rdi)
	movl	$5, 2264(%rdi)
	movb	$0, 2272(%rdi)
	movl	$5, 2288(%rdi)
	movb	$0, 2296(%rdi)
	movq	%rax, 4416(%rdi)
	movl	$6, 4424(%rdi)
	movb	$0, 4432(%rdi)
	movq	%rax, 4440(%rdi)
	movl	$6, 4448(%rdi)
	movb	$0, 4456(%rdi)
	movq	%rax, 4464(%rdi)
	movl	$6, 4472(%rdi)
	movb	$0, 4480(%rdi)
	movq	%rax, 4488(%rdi)
	movl	$6, 4496(%rdi)
	movb	$0, 4504(%rdi)
	movq	%rax, 4512(%rdi)
	movl	$6, 4520(%rdi)
	movb	$0, 4528(%rdi)
	movq	%rax, 4536(%rdi)
	movl	$6, 4544(%rdi)
	movb	$0, 4552(%rdi)
	movq	%rax, 4560(%rdi)
	movl	$6, 4568(%rdi)
	movb	$0, 4576(%rdi)
	movq	%rax, 4584(%rdi)
	movl	$6, 4592(%rdi)
	movb	$0, 4600(%rdi)
	ret
	.cfi_endproc
.LFE5114:
	.size	_ZN6disasm16InstructionTable4InitEv, .-_ZN6disasm16InstructionTable4InitEv
	.section	.text._ZN6disasm16InstructionTableC2Ev,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm16InstructionTableC2Ev
	.type	_ZN6disasm16InstructionTableC2Ev, @function
_ZN6disasm16InstructionTableC2Ev:
.LFB5111:
	.cfi_startproc
	endbr64
	leaq	.LC4(%rip), %rdx
	leaq	8(%rdi), %rax
	leaq	6152(%rdi), %rcx
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%rdx, -8(%rax)
	addq	$384, %rax
	movq	%rdx, -368(%rax)
	movq	%rdx, -344(%rax)
	movq	%rdx, -320(%rax)
	movq	%rdx, -296(%rax)
	movq	%rdx, -272(%rax)
	movq	%rdx, -248(%rax)
	movq	%rdx, -224(%rax)
	movq	%rdx, -200(%rax)
	movq	%rdx, -176(%rax)
	movq	%rdx, -152(%rax)
	movq	%rdx, -128(%rax)
	movq	%rdx, -104(%rax)
	movq	%rdx, -80(%rax)
	movq	%rdx, -56(%rax)
	movq	%rdx, -32(%rax)
	movq	$0, -384(%rax)
	movq	$0, -360(%rax)
	movq	$0, -336(%rax)
	movq	$0, -312(%rax)
	movq	$0, -288(%rax)
	movq	$0, -264(%rax)
	movq	$0, -240(%rax)
	movq	$0, -216(%rax)
	movq	$0, -192(%rax)
	movq	$0, -168(%rax)
	movq	$0, -144(%rax)
	movq	$0, -120(%rax)
	movq	$0, -96(%rax)
	movq	$0, -72(%rax)
	movq	$0, -48(%rax)
	movq	$0, -24(%rax)
	movb	$0, -376(%rax)
	movb	$0, -352(%rax)
	movb	$0, -328(%rax)
	movb	$0, -304(%rax)
	movb	$0, -280(%rax)
	movb	$0, -256(%rax)
	movb	$0, -232(%rax)
	movb	$0, -208(%rax)
	movb	$0, -184(%rax)
	movb	$0, -160(%rax)
	movb	$0, -136(%rax)
	movb	$0, -112(%rax)
	movb	$0, -88(%rax)
	movb	$0, -64(%rax)
	movb	$0, -40(%rax)
	movb	$0, -16(%rax)
	cmpq	%rcx, %rax
	jne	.L55
	jmp	_ZN6disasm16InstructionTable4InitEv
	.cfi_endproc
.LFE5111:
	.size	_ZN6disasm16InstructionTableC2Ev, .-_ZN6disasm16InstructionTableC2Ev
	.globl	_ZN6disasm16InstructionTableC1Ev
	.set	_ZN6disasm16InstructionTableC1Ev,_ZN6disasm16InstructionTableC2Ev
	.section	.text._ZN6disasm16InstructionTable9CopyTableEPKNS_12ByteMnemonicENS_15InstructionTypeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm16InstructionTable9CopyTableEPKNS_12ByteMnemonicENS_15InstructionTypeE
	.type	_ZN6disasm16InstructionTable9CopyTableEPKNS_12ByteMnemonicENS_15InstructionTypeE, @function
_ZN6disasm16InstructionTable9CopyTableEPKNS_12ByteMnemonicENS_15InstructionTypeE:
.LFB5115:
	.cfi_startproc
	endbr64
	movslq	(%rsi), %rax
	testl	%eax, %eax
	js	.L57
	addq	$16, %rsi
	.p2align 4,,10
	.p2align 3
.L59:
	movq	-8(%rsi), %r8
	leaq	(%rax,%rax,2), %rax
	addq	$16, %rsi
	leaq	(%rdi,%rax,8), %rcx
	movq	%r8, (%rcx)
	movl	-28(%rsi), %eax
	movl	%edx, 8(%rcx)
	movl	%eax, %r8d
	shrl	$2, %eax
	andl	$1, %eax
	andl	$-5, %r8d
	movl	%r8d, 12(%rcx)
	movb	%al, 16(%rcx)
	movslq	-16(%rsi), %rax
	testl	%eax, %eax
	jns	.L59
.L57:
	ret
	.cfi_endproc
.LFE5115:
	.size	_ZN6disasm16InstructionTable9CopyTableEPKNS_12ByteMnemonicENS_15InstructionTypeE, .-_ZN6disasm16InstructionTable9CopyTableEPKNS_12ByteMnemonicENS_15InstructionTypeE
	.section	.text._ZN6disasm16InstructionTable13SetTableRangeENS_15InstructionTypeEhhbPKc,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm16InstructionTable13SetTableRangeENS_15InstructionTypeEhhbPKc
	.type	_ZN6disasm16InstructionTable13SetTableRangeENS_15InstructionTypeEhhbPKc, @function
_ZN6disasm16InstructionTable13SetTableRangeENS_15InstructionTypeEhhbPKc:
.LFB5116:
	.cfi_startproc
	endbr64
	movl	%edx, %eax
	movl	%ecx, %r10d
	cmpb	%cl, %dl
	ja	.L61
	.p2align 4,,10
	.p2align 3
.L63:
	movzbl	%al, %edx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	movq	%r9, (%rdx)
	movl	%esi, 8(%rdx)
	movzbl	%al, %edx
	addl	$1, %eax
	leaq	(%rdx,%rdx,2), %rdx
	movb	%r8b, 16(%rdi,%rdx,8)
	cmpb	%al, %r10b
	jnb	.L63
.L61:
	ret
	.cfi_endproc
.LFE5116:
	.size	_ZN6disasm16InstructionTable13SetTableRangeENS_15InstructionTypeEhhbPKc, .-_ZN6disasm16InstructionTable13SetTableRangeENS_15InstructionTypeEhhbPKc
	.section	.text._ZN6disasm16InstructionTable23AddJumpConditionalShortEv,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm16InstructionTable23AddJumpConditionalShortEv
	.type	_ZN6disasm16InstructionTable23AddJumpConditionalShortEv, @function
_ZN6disasm16InstructionTable23AddJumpConditionalShortEv:
.LFB5117:
	.cfi_startproc
	endbr64
	movq	$0, 2688(%rdi)
	movl	$3, 2696(%rdi)
	movq	$0, 2712(%rdi)
	movl	$3, 2720(%rdi)
	movq	$0, 2736(%rdi)
	movl	$3, 2744(%rdi)
	movq	$0, 2760(%rdi)
	movl	$3, 2768(%rdi)
	movq	$0, 2784(%rdi)
	movl	$3, 2792(%rdi)
	movq	$0, 2808(%rdi)
	movl	$3, 2816(%rdi)
	movq	$0, 2832(%rdi)
	movl	$3, 2840(%rdi)
	movq	$0, 2856(%rdi)
	movl	$3, 2864(%rdi)
	movq	$0, 2880(%rdi)
	movl	$3, 2888(%rdi)
	movq	$0, 2904(%rdi)
	movl	$3, 2912(%rdi)
	movq	$0, 2928(%rdi)
	movl	$3, 2936(%rdi)
	movq	$0, 2952(%rdi)
	movl	$3, 2960(%rdi)
	movq	$0, 2976(%rdi)
	movl	$3, 2984(%rdi)
	movq	$0, 3000(%rdi)
	movl	$3, 3008(%rdi)
	movq	$0, 3024(%rdi)
	movl	$3, 3032(%rdi)
	movq	$0, 3048(%rdi)
	movl	$3, 3056(%rdi)
	ret
	.cfi_endproc
.LFE5117:
	.size	_ZN6disasm16InstructionTable23AddJumpConditionalShortEv, .-_ZN6disasm16InstructionTable23AddJumpConditionalShortEv
	.section	.text._ZN6disasm15DisassemblerX6414AppendToBufferEPKcz,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	.type	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz, @function
_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz:
.LFB5156:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$216, %rsp
	movq	%rdx, -176(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L67
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L67:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movl	152(%rbx), %edi
	movq	16(%rbx), %rsi
	leaq	16(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	%r10, %rdx
	leaq	-192(%rbp), %rax
	leaq	-224(%rbp), %rcx
	subq	%rdi, %rsi
	addq	8(%rbx), %rdi
	movl	$16, -224(%rbp)
	movl	$48, -220(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZN2v88internal9VSNPrintFENS0_6VectorIcEEPKcP13__va_list_tag@PLT
	addl	%eax, 152(%rbx)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	addq	$216, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L70:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5156:
	.size	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz, .-_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	.section	.rodata._ZN6disasm15DisassemblerX6424UnimplementedInstructionEv.str1.1,"aMS",@progbits,1
.LC8:
	.string	"'Unimplemented Instruction'"
	.section	.text._ZN6disasm15DisassemblerX6424UnimplementedInstructionEv,"axG",@progbits,_ZN6disasm15DisassemblerX6424UnimplementedInstructionEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN6disasm15DisassemblerX6424UnimplementedInstructionEv
	.type	_ZN6disasm15DisassemblerX6424UnimplementedInstructionEv, @function
_ZN6disasm15DisassemblerX6424UnimplementedInstructionEv:
.LFB5155:
	.cfi_startproc
	endbr64
	cmpb	$0, 156(%rdi)
	jne	.L76
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	jmp	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
.L76:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5155:
	.size	_ZN6disasm15DisassemblerX6424UnimplementedInstructionEv, .-_ZN6disasm15DisassemblerX6424UnimplementedInstructionEv
	.section	.rodata._ZN6disasm15DisassemblerX6425TryAppendRootRelativeNameEi.str1.1,"aMS",@progbits,1
.LC9:
	.string	" (%s)"
	.section	.text._ZN6disasm15DisassemblerX6425TryAppendRootRelativeNameEi,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6425TryAppendRootRelativeNameEi
	.type	_ZN6disasm15DisassemblerX6425TryAppendRootRelativeNameEi, @function
_ZN6disasm15DisassemblerX6425TryAppendRootRelativeNameEi:
.LFB5157:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	call	*64(%rax)
	testq	%rax, %rax
	je	.L77
	addq	$8, %rsp
	movq	%rax, %rdx
	xorl	%eax, %eax
	movq	%r12, %rdi
	leaq	.LC9(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5157:
	.size	_ZN6disasm15DisassemblerX6425TryAppendRootRelativeNameEi, .-_ZN6disasm15DisassemblerX6425TryAppendRootRelativeNameEi
	.section	.rodata._ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE.str1.1,"aMS",@progbits,1
.LC10:
	.string	"-"
.LC11:
	.string	"+"
.LC12:
	.string	"[rip+0x%x]"
.LC13:
	.string	"[%s]"
.LC14:
	.string	"[%s*%d%s0x%x]"
.LC15:
	.string	"[%s+%s*%d]"
.LC16:
	.string	"[%s%s0x%x]"
.LC17:
	.string	"[%s+%s*%d%s0x%x]"
.LC18:
	.string	"%s"
	.section	.text._ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	.type	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE, @function
_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE:
.LFB5158:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movzbl	(%rsi), %eax
	movzbl	157(%rdi), %edi
	movl	%eax, %ebx
	movl	%eax, %r8d
	andl	$7, %eax
	shrb	$6, %bl
	andl	$7, %r8d
	testb	$1, %dil
	je	.L137
	movl	%r8d, %r13d
	movl	$8, %r9d
	orl	$8, %r13d
	movzbl	%r13b, %r13d
	cmpb	$3, %bl
	je	.L169
.L82:
	testb	%bl, %bl
	je	.L170
	cmpb	$4, %r8b
	je	.L171
	cmpb	$2, %bl
	je	.L172
	movsbl	1(%rsi), %r14d
.L110:
	movl	%r14d, %eax
	movq	(%r12), %rdi
	leaq	.LC11(%rip), %rcx
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	sarl	$31, %eax
	movl	%eax, %r8d
	xorl	%r14d, %r8d
	subl	%eax, %r8d
	leaq	.LC10(%rip), %rax
	testl	%r14d, %r14d
	cmovs	%rax, %rcx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L135
	movq	%rcx, -64(%rbp)
	movl	%r13d, %esi
	movl	%r8d, -56(%rbp)
	call	*%rax
	movq	-64(%rbp), %rcx
	movl	-56(%rbp), %r8d
	movq	%rax, %rdx
.L134:
	xorl	%eax, %eax
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	cmpl	$13, %r13d
	je	.L173
.L114:
	xorl	%eax, %eax
	cmpb	$2, %bl
	sete	%al
	leaq	-40(%rbp), %rsp
	popq	%rbx
	leal	2(%rax,%rax,2), %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movslq	%eax, %r13
	xorl	%r9d, %r9d
	cmpb	$3, %bl
	jne	.L82
.L169:
	leaq	(%r12,%rcx), %rdi
	testb	$1, %dl
	jne	.L83
.L84:
	movl	%r13d, %esi
	call	*%rdx
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$1, %eax
.L80:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	cmpb	$5, %r8b
	je	.L174
	cmpl	$4, %eax
	jne	.L88
	movzbl	1(%rsi), %eax
	movl	%eax, %r10d
	movl	%eax, %ecx
	shrl	$3, %r10d
	shrb	$6, %cl
	andl	$7, %r10d
	andl	$2, %edi
	jne	.L89
	andl	$7, %eax
	orl	%eax, %r9d
	cmpl	$4, %r10d
	jne	.L90
	movl	%r9d, %eax
	andl	$7, %eax
	cmpl	$4, %eax
	jne	.L91
	testb	%cl, %cl
	je	.L175
.L91:
	cmpl	$5, %r9d
	je	.L176
	cmpb	$0, 156(%r12)
	jne	.L177
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$1, %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L83:
	movq	(%rdi), %rax
	movq	-1(%rax,%rdx), %rdx
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L88:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L98
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r13,8), %rdx
.L99:
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC13(%rip), %rsi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movl	1(%rsi), %r14d
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L171:
	movzbl	1(%rsi), %r13d
	movl	%r13d, %r10d
	movl	%r13d, %ecx
	shrl	$3, %r10d
	shrb	$6, %cl
	andl	$7, %r10d
	movl	%r10d, %eax
	orl	$8, %eax
	andl	$2, %edi
	cmovne	%eax, %r10d
	andl	$7, %r13d
	orl	%r9d, %r13d
	cmpb	$2, %bl
	je	.L178
	movsbl	2(%rsi), %eax
.L103:
	cltd
	movl	%edx, %r14d
	xorl	%eax, %r14d
	subl	%edx, %r14d
	cmpl	$4, %r10d
	je	.L179
.L104:
	movq	(%r12), %rdi
	testl	%eax, %eax
	leaq	.LC11(%rip), %r9
	leaq	.LC10(%rip), %rax
	cmovs	%rax, %r9
	movl	$1, %r8d
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %r15
	movq	(%rdi), %rax
	sall	%cl, %r8d
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	je	.L132
	movl	%r8d, -64(%rbp)
	movl	%r10d, %esi
	movq	%r9, -56(%rbp)
	call	*%rax
	movq	(%r12), %rdi
	movq	-56(%rbp), %r9
	movq	%rax, %rcx
	movl	-64(%rbp), %r8d
	movq	(%rdi), %rax
	movq	16(%rax), %rax
.L108:
	cmpq	%r15, %rax
	je	.L180
	movl	%r8d, -68(%rbp)
	movl	%r13d, %esi
	movq	%r9, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	*%rax
	movl	-68(%rbp), %r8d
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	%rax, %rdx
.L129:
	subq	$8, %rsp
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rsi
	movq	%r12, %rdi
	pushq	%r14
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	popq	%rax
	popq	%rdx
.L106:
	xorl	%eax, %eax
	cmpb	$2, %bl
	sete	%al
	leaq	-40(%rbp), %rsp
	popq	%rbx
	leal	3(%rax,%rax,2), %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	.cfi_restore_state
	andl	$7, %eax
	orl	$8, %r10d
	orl	%eax, %r9d
.L90:
	movq	(%r12), %rdi
	movl	$1, %r14d
	sall	%cl, %r14d
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpl	$5, %r9d
	je	.L124
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %r15
	cmpq	%r15, %rax
	jne	.L95
	movslq	%r10d, %r10
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rdx
	movq	(%rdx,%r10,8), %rcx
.L96:
	cmpq	%r15, %rax
	je	.L181
	movq	%rcx, -56(%rbp)
	movl	%r9d, %esi
	call	*%rax
	movq	-56(%rbp), %rcx
	movq	%rax, %rdx
.L119:
	xorl	%eax, %eax
	movl	%r14d, %r8d
	leaq	.LC15(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$2, %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L135:
	movslq	%r13d, %rax
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L173:
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	(%rdi), %rax
	call	*64(%rax)
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L114
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L178:
	movl	2(%rsi), %eax
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L179:
	movl	%r13d, %edx
	andl	$7, %edx
	cmpl	$4, %edx
	jne	.L104
	testb	%cl, %cl
	jne	.L104
	movq	(%r12), %rdi
	testl	%eax, %eax
	leaq	.LC10(%rip), %rcx
	leaq	.LC11(%rip), %rax
	cmovns	%rax, %rcx
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L127
	movq	%rcx, -56(%rbp)
	movl	%r13d, %esi
	call	*%rax
	movq	-56(%rbp), %rcx
	movq	%rax, %rdx
.L126:
	movl	%r14d, %r8d
	leaq	.LC16(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L98:
	movl	%r13d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L132:
	movslq	%r10d, %rsi
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rdx
	movq	(%rdx,%rsi,8), %rcx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L180:
	movslq	%r13d, %r13
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r13,8), %rdx
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L181:
	movslq	%r9d, %r13
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rdx
	movq	(%rdx,%r13,8), %rdx
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L174:
	movl	1(%rsi), %edx
	xorl	%eax, %eax
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$5, %eax
	jmp	.L80
.L176:
	movq	(%r12), %rdi
	movl	$1, %r14d
	sall	%cl, %r14d
	movq	(%rdi), %rax
	movq	16(%rax), %rax
.L124:
	movl	2(%rsi), %edx
	leaq	.LC11(%rip), %r8
	movl	%edx, %ecx
	sarl	$31, %ecx
	movl	%ecx, %r9d
	xorl	%edx, %r9d
	subl	%ecx, %r9d
	testl	%edx, %edx
	leaq	.LC10(%rip), %rdx
	cmovs	%rdx, %r8
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L117
	movq	%r8, -64(%rbp)
	movl	%r10d, %esi
	movl	%r9d, -56(%rbp)
	call	*%rax
	movq	-64(%rbp), %r8
	movl	-56(%rbp), %r9d
	movq	%rax, %rdx
.L116:
	xorl	%eax, %eax
	movl	%r14d, %ecx
	leaq	.LC14(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$6, %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L95:
	movl	%r9d, -56(%rbp)
	movl	%r10d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movl	-56(%rbp), %r9d
	movq	%rax, %rcx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	jmp	.L96
.L117:
	movslq	%r10d, %r10
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r10,8), %rdx
	jmp	.L116
.L175:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L92
	movslq	%r9d, %r13
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r13,8), %rdx
.L93:
	xorl	%eax, %eax
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$2, %eax
	jmp	.L80
.L127:
	movslq	%r13d, %r13
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r13,8), %rdx
	jmp	.L126
.L92:
	movl	%r9d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L93
.L177:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5158:
	.size	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE, .-_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	.section	.rodata._ZN6disasm15DisassemblerX6414PrintImmediateEPhNS0_11OperandSizeE.str1.1,"aMS",@progbits,1
.LC19:
	.string	"%lx"
	.section	.text._ZN6disasm15DisassemblerX6414PrintImmediateEPhNS0_11OperandSizeE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6414PrintImmediateEPhNS0_11OperandSizeE
	.type	_ZN6disasm15DisassemblerX6414PrintImmediateEPhNS0_11OperandSizeE, @function
_ZN6disasm15DisassemblerX6414PrintImmediateEPhNS0_11OperandSizeE:
.LFB5159:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpl	$2, %edx
	je	.L183
	ja	.L184
	testl	%edx, %edx
	je	.L193
	movswq	(%rsi), %rdx
	movl	$2, %r12d
.L189:
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rsi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	cmpl	$3, %edx
	jne	.L194
	movslq	(%rsi), %rdx
	movl	$4, %r12d
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L183:
	movl	(%rsi), %edx
	movl	$4, %r12d
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L193:
	movzbl	(%rsi), %edx
	movl	$1, %r12d
	jmp	.L189
.L194:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5159:
	.size	_ZN6disasm15DisassemblerX6414PrintImmediateEPhNS0_11OperandSizeE, .-_ZN6disasm15DisassemblerX6414PrintImmediateEPhNS0_11OperandSizeE
	.section	.text._ZN6disasm15DisassemblerX6417PrintRightOperandEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6417PrintRightOperandEPh
	.type	_ZN6disasm15DisassemblerX6417PrintRightOperandEPh, @function
_ZN6disasm15DisassemblerX6417PrintRightOperandEPh:
.LFB5160:
	.cfi_startproc
	endbr64
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	xorl	%ecx, %ecx
	jmp	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	.cfi_endproc
.LFE5160:
	.size	_ZN6disasm15DisassemblerX6417PrintRightOperandEPh, .-_ZN6disasm15DisassemblerX6417PrintRightOperandEPh
	.section	.text._ZN6disasm15DisassemblerX6421PrintRightByteOperandEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6421PrintRightByteOperandEPh
	.type	_ZN6disasm15DisassemblerX6421PrintRightByteOperandEPh, @function
_ZN6disasm15DisassemblerX6421PrintRightByteOperandEPh:
.LFB5161:
	.cfi_startproc
	endbr64
	leaq	_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi(%rip), %rdx
	xorl	%ecx, %ecx
	jmp	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	.cfi_endproc
.LFE5161:
	.size	_ZN6disasm15DisassemblerX6421PrintRightByteOperandEPh, .-_ZN6disasm15DisassemblerX6421PrintRightByteOperandEPh
	.section	.text._ZN6disasm15DisassemblerX6420PrintRightXMMOperandEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6420PrintRightXMMOperandEPh
	.type	_ZN6disasm15DisassemblerX6420PrintRightXMMOperandEPh, @function
_ZN6disasm15DisassemblerX6420PrintRightXMMOperandEPh:
.LFB5162:
	.cfi_startproc
	endbr64
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	xorl	%ecx, %ecx
	jmp	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	.cfi_endproc
.LFE5162:
	.size	_ZN6disasm15DisassemblerX6420PrintRightXMMOperandEPh, .-_ZN6disasm15DisassemblerX6420PrintRightXMMOperandEPh
	.section	.rodata._ZN6disasm15DisassemblerX6413PrintOperandsEPKcNS_11OperandTypeEPh.str1.1,"aMS",@progbits,1
.LC21:
	.string	"%s%c %s,"
.LC22:
	.string	"%s%c "
.LC23:
	.string	",%s"
.LC20:
	.string	"bwlq"
	.section	.text._ZN6disasm15DisassemblerX6413PrintOperandsEPKcNS_11OperandTypeEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6413PrintOperandsEPKcNS_11OperandTypeEPh
	.type	_ZN6disasm15DisassemblerX6413PrintOperandsEPKcNS_11OperandTypeEPh, @function
_ZN6disasm15DisassemblerX6413PrintOperandsEPKcNS_11OperandTypeEPh:
.LFB5163:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$8, %rsp
	movzbl	(%rcx), %esi
	movzbl	157(%rdi), %eax
	movq	(%rdi), %rdi
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %edx
	orl	$8, %edx
	testb	$4, %al
	cmovne	%edx, %esi
	movq	(%rdi), %rdx
	cmpb	$0, 163(%r12)
	je	.L200
	movq	24(%rdx), %rax
	leaq	_ZNK6disasm13NameConverter21NameOfByteCPURegisterEi(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L201
	leaq	_ZN6disasmL13byte_cpu_regsE(%rip), %rax
	movslq	%esi, %rsi
	movq	(%rax,%rsi,8), %r15
	xorl	%eax, %eax
	cmpl	$1, %ebx
	jne	.L235
.L202:
	cltq
	leaq	.LC20(%rip), %rdx
	movq	%r15, %r8
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %ecx
	leaq	.LC21(%rip), %rsi
	movq	%r14, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi(%rip), %rdx
	cmpb	$0, 163(%r12)
	jne	.L234
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
.L234:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	16(%rdx), %rdx
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %r8
	cmpq	%r8, %rdx
	jne	.L206
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rdx
	movl	%eax, %ecx
	movq	(%rdx,%rsi,8), %r15
	cmpl	$1, %ebx
	je	.L207
	cmpl	$2, %ebx
	jne	.L204
.L208:
	movl	$3, %edx
	testb	$8, %al
	jne	.L203
	xorl	%edx, %edx
	cmpb	$0, 158(%r12)
	sete	%dl
	addl	$1, %edx
.L203:
	movslq	%edx, %rdx
	leaq	.LC20(%rip), %rax
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rax,%rdx), %ecx
	movq	%r14, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi(%rip), %rdx
	cmpb	$0, 163(%r12)
	jne	.L233
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
.L233:
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	.LC23(%rip), %rsi
	movl	%eax, %r13d
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	cmpl	$2, %ebx
	jne	.L204
.L216:
	xorl	%edx, %edx
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L209:
	cmpb	$0, 163(%r12)
	jne	.L217
	movzbl	157(%r12), %ecx
.L207:
	andl	$8, %ecx
	movl	$3, %eax
	jne	.L202
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L206:
	call	*%rdx
	movq	%rax, %r15
.L205:
	cmpl	$1, %ebx
	je	.L209
	cmpl	$2, %ebx
	jne	.L204
	cmpb	$0, 163(%r12)
	jne	.L216
	movzbl	157(%r12), %eax
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L201:
	call	*%rax
	movq	%rax, %r15
	jmp	.L205
.L204:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L217:
	xorl	%eax, %eax
	jmp	.L202
	.cfi_endproc
.LFE5163:
	.size	_ZN6disasm15DisassemblerX6413PrintOperandsEPKcNS_11OperandTypeEPh, .-_ZN6disasm15DisassemblerX6413PrintOperandsEPKcNS_11OperandTypeEPh
	.section	.rodata._ZN6disasm15DisassemblerX6416PrintImmediateOpEPh.str1.1,"aMS",@progbits,1
.LC24:
	.string	"Imm???"
.LC25:
	.string	"add"
.LC26:
	.string	"adc"
.LC27:
	.string	"sbb"
.LC28:
	.string	"and"
.LC29:
	.string	"sub"
.LC30:
	.string	"xor"
.LC31:
	.string	"cmp"
.LC32:
	.string	"or"
.LC33:
	.string	",0x"
	.section	.text._ZN6disasm15DisassemblerX6416PrintImmediateOpEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6416PrintImmediateOpEPh
	.type	_ZN6disasm15DisassemblerX6416PrintImmediateOpEPh, @function
_ZN6disasm15DisassemblerX6416PrintImmediateOpEPh:
.LFB5164:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	1(%rsi), %eax
	movzbl	(%rsi), %ebx
	sarl	$3, %eax
	andl	$2, %ebx
	andl	$7, %eax
	testb	$4, 157(%rdi)
	je	.L259
	cmpb	$0, 156(%rdi)
	jne	.L260
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	.LC24(%rip), %rdx
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L259:
	leaq	.L239(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6416PrintImmediateOpEPh,"a",@progbits
	.align 4
	.align 4
.L239:
	.long	.L254-.L239
	.long	.L245-.L239
	.long	.L244-.L239
	.long	.L243-.L239
	.long	.L242-.L239
	.long	.L241-.L239
	.long	.L240-.L239
	.long	.L238-.L239
	.section	.text._ZN6disasm15DisassemblerX6416PrintImmediateOpEPh
	.p2align 4,,10
	.p2align 3
.L245:
	leaq	.LC32(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L246:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L248
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L248
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L248:
	cltq
	leaq	.LC20(%rip), %rcx
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rcx,%rax), %ecx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	1(%r13), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r14d
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movslq	%r14d, %rax
	leaq	1(%r13,%rax), %rax
	testb	%bl, %bl
	je	.L249
.L250:
	movzbl	(%rax), %edx
	movl	$1, %ebx
.L253:
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rsi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leal	1(%r14,%rbx), %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	leaq	.LC25(%rip), %rdx
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L244:
	leaq	.LC26(%rip), %rdx
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L243:
	leaq	.LC27(%rip), %rdx
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L242:
	leaq	.LC28(%rip), %rdx
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L241:
	leaq	.LC29(%rip), %rdx
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L240:
	leaq	.LC30(%rip), %rdx
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L238:
	leaq	.LC31(%rip), %rdx
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L249:
	cmpb	$0, 163(%r12)
	jne	.L250
	testb	$8, 157(%r12)
	jne	.L251
	cmpb	$0, 158(%r12)
	jne	.L252
	movl	(%rax), %edx
	movl	$4, %ebx
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L251:
	movslq	(%rax), %rdx
	movl	$4, %ebx
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L252:
	movswq	(%rax), %rdx
	movl	$2, %ebx
	jmp	.L253
.L260:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5164:
	.size	_ZN6disasm15DisassemblerX6416PrintImmediateOpEPh, .-_ZN6disasm15DisassemblerX6416PrintImmediateOpEPh
	.section	.rodata._ZN6disasm15DisassemblerX6415F6F7InstructionEPh.str1.1,"aMS",@progbits,1
.LC34:
	.string	"not"
.LC35:
	.string	"idiv"
.LC36:
	.string	"div"
.LC37:
	.string	"imul"
.LC38:
	.string	"neg"
.LC39:
	.string	"mul"
.LC40:
	.string	"%s%c %s"
.LC41:
	.string	"test%c "
	.section	.text._ZN6disasm15DisassemblerX6415F6F7InstructionEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6415F6F7InstructionEPh
	.type	_ZN6disasm15DisassemblerX6415F6F7InstructionEPh, @function
_ZN6disasm15DisassemblerX6415F6F7InstructionEPh:
.LFB5165:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movzbl	1(%rsi), %edx
	movzbl	157(%rdi), %ecx
	movl	%edx, %eax
	movl	%edx, %esi
	shrl	$3, %eax
	shrb	$6, %sil
	andl	$7, %eax
	movl	%eax, %edi
	orl	$8, %edi
	testb	$4, %cl
	cmovne	%rdi, %rax
	andl	$7, %edx
	movl	%edx, %edi
	orl	$8, %edi
	testb	$1, %cl
	cmovne	%edi, %edx
	cmpb	$3, %sil
	je	.L303
	testl	%eax, %eax
	jne	.L279
.L265:
	cmpb	$0, 163(%r12)
	movl	$98, %edx
	jne	.L280
	andl	$8, %ecx
	movl	$113, %edx
	jne	.L280
	cmpb	$1, 158(%r12)
	sbbl	%edx, %edx
	andl	$-11, %edx
	addl	$119, %edx
.L280:
	xorl	%eax, %eax
	leaq	.LC41(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	leaq	1(%rbx), %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	movl	%eax, %r13d
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movslq	%r13d, %rax
	cmpb	$0, 163(%r12)
	jne	.L304
	leaq	1(%rbx,%rax), %rax
	testb	$8, 157(%r12)
	jne	.L283
	cmpb	$0, 158(%r12)
	jne	.L305
	movl	(%rax), %edx
	movl	$4, %ebx
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L304:
	movzbl	1(%rbx,%rax), %edx
	movl	$1, %ebx
.L282:
	xorl	%eax, %eax
	leaq	.LC19(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leal	1(%r13,%rbx), %eax
.L261:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	.cfi_restore_state
	testl	%eax, %eax
	je	.L265
	movzbl	%dl, %edx
	cmpl	$7, %eax
	ja	.L266
	leaq	.L268(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6415F6F7InstructionEPh,"a",@progbits
	.align 4
	.align 4
.L268:
	.long	.L266-.L268
	.long	.L266-.L268
	.long	.L273-.L268
	.long	.L272-.L268
	.long	.L271-.L268
	.long	.L287-.L268
	.long	.L269-.L268
	.long	.L267-.L268
	.section	.text._ZN6disasm15DisassemblerX6415F6F7InstructionEPh
.L287:
	leaq	.LC37(%rip), %r13
.L270:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	je	.L286
.L275:
	movl	%edx, %esi
	call	*%rax
	movq	%rax, %r8
	jmp	.L276
.L271:
	leaq	.LC39(%rip), %r13
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L279:
	cmpb	$0, 156(%r12)
	jne	.L285
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	addq	$24, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movswq	(%rax), %rdx
	movl	$2, %ebx
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L283:
	movslq	(%rax), %rdx
	movl	$4, %ebx
	jmp	.L282
.L267:
	leaq	.LC35(%rip), %r13
	jmp	.L270
.L266:
	cmpb	$0, 156(%r12)
	jne	.L285
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	%edx, -36(%rbp)
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%r13d, %r13d
	movslq	-36(%rbp), %rdx
	jmp	.L270
.L273:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rcx
	leaq	.LC34(%rip), %r13
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L275
.L286:
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rdx,8), %r8
.L276:
	xorl	%edx, %edx
	cmpb	$0, 163(%r12)
	jne	.L277
	movl	$3, %edx
	testb	$8, 157(%r12)
	jne	.L277
	xorl	%edx, %edx
	cmpb	$0, 158(%r12)
	sete	%dl
	addl	$1, %edx
.L277:
	movslq	%edx, %rdx
	leaq	.LC20(%rip), %rcx
	xorl	%eax, %eax
	movq	%r12, %rdi
	movsbl	(%rcx,%rdx), %ecx
	leaq	.LC40(%rip), %rsi
	movq	%r13, %rdx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$2, %eax
	jmp	.L261
.L269:
	leaq	.LC36(%rip), %r13
	jmp	.L270
.L272:
	leaq	.LC38(%rip), %r13
	jmp	.L270
.L285:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5165:
	.size	_ZN6disasm15DisassemblerX6415F6F7InstructionEPh, .-_ZN6disasm15DisassemblerX6415F6F7InstructionEPh
	.section	.rodata._ZN6disasm15DisassemblerX6416ShiftInstructionEPh.str1.1,"aMS",@progbits,1
.LC42:
	.string	"sar"
.LC43:
	.string	"rol"
.LC44:
	.string	"rcl"
.LC45:
	.string	"rcr"
.LC46:
	.string	"shl"
.LC47:
	.string	"shr"
.LC48:
	.string	"ror"
.LC49:
	.string	", cl"
.LC50:
	.string	", %d"
	.section	.text._ZN6disasm15DisassemblerX6416ShiftInstructionEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6416ShiftInstructionEPh
	.type	_ZN6disasm15DisassemblerX6416ShiftInstructionEPh, @function
_ZN6disasm15DisassemblerX6416ShiftInstructionEPh:
.LFB5166:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	(%rsi), %eax
	movq	%rsi, %rbx
	movl	%eax, %r13d
	andl	$-18, %eax
	andl	$-2, %r13d
	cmpb	$-64, %al
	je	.L307
	cmpb	$-46, %r13b
	jne	.L336
.L307:
	movzbl	1(%rbx), %eax
	leaq	.L312(%rip), %rdx
	movzbl	157(%r12), %esi
	sarl	$3, %eax
	andl	$7, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6416ShiftInstructionEPh,"a",@progbits
	.align 4
	.align 4
.L312:
	.long	.L318-.L312
	.long	.L317-.L312
	.long	.L323-.L312
	.long	.L315-.L312
	.long	.L314-.L312
	.long	.L313-.L312
	.long	.L310-.L312
	.long	.L311-.L312
	.section	.text._ZN6disasm15DisassemblerX6416ShiftInstructionEPh
	.p2align 4,,10
	.p2align 3
.L336:
	cmpb	$0, 156(%rdi)
	jne	.L319
	xorl	%eax, %eax
	movl	$1, %r14d
	leaq	.LC8(%rip), %rsi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	leaq	.LC44(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L316:
	cmpb	$0, 163(%r12)
	movl	$98, %ecx
	jne	.L320
	andl	$8, %esi
	movl	$113, %ecx
	jne	.L320
	cmpb	$1, 158(%r12)
	sbbl	%ecx, %ecx
	andl	$-11, %ecx
	addl	$119, %ecx
.L320:
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	1(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	1(%rax), %r14d
	cmpb	$-46, %r13b
	je	.L337
	movl	$1, %edx
	cmpb	$-48, %r13b
	je	.L322
	movslq	%r14d, %r14
	movzbl	(%rbx,%r14), %edx
	leal	2(%rax), %r14d
.L322:
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC50(%rip), %rsi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	leaq	.LC48(%rip), %rdx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L311:
	leaq	.LC42(%rip), %rdx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L318:
	leaq	.LC43(%rip), %rdx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L315:
	leaq	.LC45(%rip), %rdx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L314:
	leaq	.LC46(%rip), %rdx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L313:
	leaq	.LC47(%rip), %rdx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L310:
	cmpb	$0, 156(%r12)
	jne	.L319
	movq	%r12, %rdi
	xorl	%eax, %eax
	movl	$2, %r14d
	leaq	.LC8(%rip), %rsi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC49(%rip), %rsi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L319:
	.cfi_restore_state
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5166:
	.size	_ZN6disasm15DisassemblerX6416ShiftInstructionEPh, .-_ZN6disasm15DisassemblerX6416ShiftInstructionEPh
	.section	.rodata._ZN6disasm15DisassemblerX649JumpShortEPh.str1.1,"aMS",@progbits,1
.LC51:
	.string	"jmp %s"
	.section	.text._ZN6disasm15DisassemblerX649JumpShortEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX649JumpShortEPh
	.type	_ZN6disasm15DisassemblerX649JumpShortEPh, @function
_ZN6disasm15DisassemblerX649JumpShortEPh:
.LFB5167:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNK6disasm13NameConverter13NameOfAddressEPh(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movsbq	1(%rsi), %rax
	movq	%rdi, %r12
	movq	(%rdi), %r13
	leaq	2(%rax,%rsi), %rcx
	movq	0(%r13), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L339
	movq	8(%r13), %rdi
	movq	16(%r13), %rsi
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	8(%r13), %rdx
.L340:
	movq	%r12, %rdi
	xorl	%eax, %eax
	leaq	.LC51(%rip), %rsi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	popq	%r12
	movl	$2, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	movq	%rcx, %rsi
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L340
	.cfi_endproc
.LFE5167:
	.size	_ZN6disasm15DisassemblerX649JumpShortEPh, .-_ZN6disasm15DisassemblerX649JumpShortEPh
	.section	.rodata._ZN6disasm15DisassemblerX6415JumpConditionalEPh.str1.1,"aMS",@progbits,1
.LC52:
	.string	"j%s %s"
	.section	.text._ZN6disasm15DisassemblerX6415JumpConditionalEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6415JumpConditionalEPh
	.type	_ZN6disasm15DisassemblerX6415JumpConditionalEPh, @function
_ZN6disasm15DisassemblerX6415JumpConditionalEPh:
.LFB5168:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6disasmL23conditional_code_suffixE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movslq	2(%rsi), %rax
	movq	(%rdi), %r13
	leaq	6(%rsi,%rax), %rcx
	movzbl	1(%rsi), %eax
	andl	$15, %eax
	movq	(%rdx,%rax,8), %r14
	movq	0(%r13), %rax
	leaq	_ZNK6disasm13NameConverter13NameOfAddressEPh(%rip), %rdx
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L343
	movq	8(%r13), %rdi
	movq	16(%r13), %rsi
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	8(%r13), %rcx
.L344:
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	.LC52(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	addq	$8, %rsp
	movl	$6, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	movq	%rcx, %rsi
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %rcx
	jmp	.L344
	.cfi_endproc
.LFE5168:
	.size	_ZN6disasm15DisassemblerX6415JumpConditionalEPh, .-_ZN6disasm15DisassemblerX6415JumpConditionalEPh
	.section	.text._ZN6disasm15DisassemblerX6420JumpConditionalShortEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6420JumpConditionalShortEPh
	.type	_ZN6disasm15DisassemblerX6420JumpConditionalShortEPh, @function
_ZN6disasm15DisassemblerX6420JumpConditionalShortEPh:
.LFB5169:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6disasmL23conditional_code_suffixE(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movsbq	1(%rsi), %rax
	movq	(%rdi), %r13
	leaq	2(%rsi,%rax), %rcx
	movzbl	(%rsi), %eax
	andl	$15, %eax
	movq	(%rdx,%rax,8), %r14
	movq	0(%r13), %rax
	leaq	_ZNK6disasm13NameConverter13NameOfAddressEPh(%rip), %rdx
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L347
	movq	8(%r13), %rdi
	movq	16(%r13), %rsi
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	8(%r13), %rcx
.L348:
	movq	%r14, %rdx
	movq	%r12, %rdi
	leaq	.LC52(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	addq	$8, %rsp
	movl	$2, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	movq	%rcx, %rsi
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %rcx
	jmp	.L348
	.cfi_endproc
.LFE5169:
	.size	_ZN6disasm15DisassemblerX6420JumpConditionalShortEPh, .-_ZN6disasm15DisassemblerX6420JumpConditionalShortEPh
	.section	.rodata._ZN6disasm15DisassemblerX645SetCCEPh.str1.1,"aMS",@progbits,1
.LC53:
	.string	"set%s%c "
	.section	.text._ZN6disasm15DisassemblerX645SetCCEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX645SetCCEPh
	.type	_ZN6disasm15DisassemblerX645SetCCEPh, @function
_ZN6disasm15DisassemblerX645SetCCEPh:
.LFB5170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN6disasmL23conditional_code_suffixE(%rip), %rdx
	movl	$98, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movzbl	1(%rsi), %eax
	movq	%rsi, %rbx
	andl	$15, %eax
	cmpb	$0, 163(%rdi)
	movq	(%rdx,%rax,8), %rdx
	jne	.L351
	movl	$113, %ecx
	testb	$8, 157(%rdi)
	jne	.L351
	cmpb	$1, 158(%rdi)
	sbbl	%ecx, %ecx
	andl	$-11, %ecx
	addl	$119, %ecx
.L351:
	movq	%r12, %rdi
	leaq	.LC53(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	2(%rbx), %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	popq	%rbx
	movl	$3, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5170:
	.size	_ZN6disasm15DisassemblerX645SetCCEPh, .-_ZN6disasm15DisassemblerX645SetCCEPh
	.section	.rodata._ZN6disasm15DisassemblerX6414AVXInstructionEPh.str1.1,"aMS",@progbits,1
.LC54:
	.string	"vcvtlsi2ss"
.LC55:
	.string	"vcvtqsi2ss"
.LC56:
	.string	""
.LC57:
	.string	"q"
.LC58:
	.string	"vcvtlsi2sd"
.LC59:
	.string	"vcvtqsi2sd"
.LC60:
	.string	"blsr"
.LC61:
	.string	"?"
.LC62:
	.string	"blsmsk"
.LC63:
	.string	"blsi"
.LC65:
	.string	"vfmadd132s%c %s,%s,"
.LC66:
	.string	"vfmadd213s%c %s,%s,"
.LC67:
	.string	"vfmadd231s%c %s,%s,"
.LC68:
	.string	"vfmsub132s%c %s,%s,"
.LC69:
	.string	"vfmsub213s%c %s,%s,"
.LC70:
	.string	"vfmsub231s%c %s,%s,"
.LC71:
	.string	"vfnmadd132s%c %s,%s,"
.LC72:
	.string	"vfnmadd213s%c %s,%s,"
.LC73:
	.string	"vfnmadd231s%c %s,%s,"
.LC74:
	.string	"vfnmsub132s%c %s,%s,"
.LC75:
	.string	"vfnmsub213s%c %s,%s,"
.LC76:
	.string	"vfnmsub231s%c %s,%s,"
.LC77:
	.string	"shlx%c %s,"
.LC78:
	.string	"vpabsb %s,%s,"
.LC79:
	.string	"vpabsw %s,%s,"
.LC80:
	.string	"vpabsd %s,%s,"
.LC81:
	.string	"vphaddd %s,%s,"
.LC82:
	.string	"vphaddw %s,%s,"
.LC83:
	.string	"vpshufb %s,%s,"
.LC84:
	.string	"vpsignb %s,%s,"
.LC85:
	.string	"vpsignw %s,%s,"
.LC86:
	.string	"vpsignd %s,%s,"
.LC87:
	.string	"vblendvpd %s,%s,"
.LC88:
	.string	"vpcmpeqq %s,%s,"
.LC89:
	.string	"vptest %s,%s,"
.LC90:
	.string	"vpmovsxbw %s,%s,"
.LC91:
	.string	"vpmovsxwd %s,%s,"
.LC92:
	.string	"vpackusdw %s,%s,"
.LC93:
	.string	"vpmovzxbw %s,%s,"
.LC94:
	.string	"vpmovzxwd %s,%s,"
.LC95:
	.string	"vpminsb %s,%s,"
.LC96:
	.string	"vpminsd %s,%s,"
.LC97:
	.string	"vpminuw %s,%s,"
.LC98:
	.string	"vpminud %s,%s,"
.LC99:
	.string	"vpmaxsb %s,%s,"
.LC100:
	.string	"vpmaxsd %s,%s,"
.LC101:
	.string	"vpmaxuw %s,%s,"
.LC102:
	.string	"vpmaxud %s,%s,"
.LC103:
	.string	"vpmulld %s,%s,"
.LC104:
	.string	"vroundss %s,%s,"
.LC105:
	.string	",0x%x"
.LC106:
	.string	"vroundsd %s,%s,"
.LC107:
	.string	"vpextrb "
.LC108:
	.string	",%s,0x%x,"
.LC109:
	.string	"vpextrw "
.LC110:
	.string	"vpextrd "
.LC111:
	.string	"vpinsrb %s,%s,"
.LC112:
	.string	"vpinsrd %s,%s,"
.LC113:
	.string	"vmovss %s,"
.LC114:
	.string	"%s,"
.LC115:
	.string	"vmovss "
.LC116:
	.string	"%s %s,%s,"
.LC117:
	.string	"vcvttss2si%s %s,"
.LC118:
	.string	"vsqrtss %s,%s,"
.LC119:
	.string	"vaddss %s,%s,"
.LC120:
	.string	"vmulss %s,%s,"
.LC121:
	.string	"vcvtss2sd %s,%s,"
.LC122:
	.string	"vsubss %s,%s,"
.LC123:
	.string	"vminss %s,%s,"
.LC124:
	.string	"vdivss %s,%s,"
.LC125:
	.string	"vmaxss %s,%s,"
.LC126:
	.string	"vmovsd %s,"
.LC127:
	.string	"vmovsd "
.LC128:
	.string	"vcvttsd2si%s %s,"
.LC129:
	.string	"vcvtsd2si%s %s,"
.LC130:
	.string	"vsqrtsd %s,%s,"
.LC131:
	.string	"vaddsd %s,%s,"
.LC132:
	.string	"vmulsd %s,%s,"
.LC133:
	.string	"vcvtsd2ss %s,%s,"
.LC134:
	.string	"vsubsd %s,%s,"
.LC135:
	.string	"vminsd %s,%s,"
.LC136:
	.string	"vdivsd %s,%s,"
.LC137:
	.string	"vmaxsd %s,%s,"
.LC138:
	.string	"vlddqu %s,"
.LC139:
	.string	"vhaddps %s,%s,"
.LC140:
	.string	"andn%c %s,%s,"
.LC141:
	.string	"bzhi%c %s,"
.LC142:
	.string	"bextr%c %s,"
.LC143:
	.string	"pdep%c %s,%s,"
.LC144:
	.string	"mulx%c %s,%s,"
.LC145:
	.string	"shrx%c %s,"
.LC146:
	.string	"pext%c %s,%s,"
.LC147:
	.string	"sarx%c %s,"
.LC148:
	.string	"rorx%c %s,"
.LC149:
	.string	",%d"
.LC150:
	.string	"vmovups %s,"
.LC151:
	.string	"vmovups "
.LC152:
	.string	"vmovaps %s,"
.LC153:
	.string	"vmovaps "
.LC154:
	.string	"vucomiss %s,"
.LC155:
	.string	"vmovmskps %s,"
.LC156:
	.string	"vandps %s,%s,"
.LC157:
	.string	"vandnps %s,%s,"
.LC158:
	.string	"vxorps %s,%s,"
.LC159:
	.string	"eq"
.LC160:
	.string	"lt"
.LC161:
	.string	"le"
.LC162:
	.string	"unord"
.LC163:
	.string	"neq"
.LC164:
	.string	"nlt"
.LC165:
	.string	"nle"
.LC166:
	.string	"ord"
.LC167:
	.string	"vcmpps %s,%s,"
.LC168:
	.string	", (%s)"
.LC169:
	.string	"vmovupd %s,"
.LC170:
	.string	"vmovupd "
.LC171:
	.string	"vmovapd %s,"
.LC172:
	.string	"vmovapd "
.LC173:
	.string	"vucomisd %s,"
.LC174:
	.string	"vmovmskpd %s,"
.LC175:
	.string	"vandpd %s,%s,"
.LC176:
	.string	"vorpd %s,%s,"
.LC177:
	.string	"vxorpd %s,%s,"
.LC178:
	.string	"vmov%c %s,"
.LC179:
	.string	"vpshufd %s,"
.LC180:
	.string	"vps%sw %s,"
.LC181:
	.string	",%u"
.LC182:
	.string	"vps%sd %s,"
.LC183:
	.string	"vps%sq %s,"
.LC184:
	.string	"vmov%c "
.LC185:
	.string	"vcmppd %s,%s,"
.LC186:
	.string	"vpinsrw %s,%s,"
.LC187:
	.string	"vpextrw %s,"
.LC188:
	.string	"vandnpd %s,%s,"
.LC189:
	.string	"vaddpd %s,%s,"
.LC190:
	.string	"vmulpd %s,%s,"
.LC191:
	.string	"vcvtps2dq %s,%s,"
.LC192:
	.string	"vsubpd %s,%s,"
.LC193:
	.string	"vminpd %s,%s,"
.LC194:
	.string	"vmaxpd %s,%s,"
.LC195:
	.string	"vdivpd %s,%s,"
.LC196:
	.string	"vpunpcklbw %s,%s,"
.LC197:
	.string	"vpunpcklwd %s,%s,"
.LC198:
	.string	"vpunpckldq %s,%s,"
.LC199:
	.string	"vpacksswb %s,%s,"
.LC200:
	.string	"vpackuswb %s,%s,"
.LC201:
	.string	"vpunpckhbw %s,%s,"
.LC202:
	.string	"vpunpckhwd %s,%s,"
.LC203:
	.string	"vpunpckhdq %s,%s,"
.LC204:
	.string	"vpackssdw %s,%s,"
.LC205:
	.string	"vpunpcklqdq %s,%s,"
.LC206:
	.string	"vpunpckhqdq %s,%s,"
.LC207:
	.string	"vpaddb %s,%s,"
.LC208:
	.string	"vpaddw %s,%s,"
.LC209:
	.string	"vpaddd %s,%s,"
.LC210:
	.string	"vpaddq %s,%s,"
.LC211:
	.string	"vpaddsb %s,%s,"
.LC212:
	.string	"vpaddsw %s,%s,"
.LC213:
	.string	"vpaddusb %s,%s,"
.LC214:
	.string	"vpaddusw %s,%s,"
.LC215:
	.string	"vpcmpeqb %s,%s,"
.LC216:
	.string	"vpcmpeqw %s,%s,"
.LC217:
	.string	"vpcmpeqd %s,%s,"
.LC218:
	.string	"vpcmpgtb %s,%s,"
.LC219:
	.string	"vpcmpgtw %s,%s,"
.LC220:
	.string	"vpcmpgtd %s,%s,"
.LC221:
	.string	"vpmaxsw %s,%s,"
.LC222:
	.string	"vpmaxub %s,%s,"
.LC223:
	.string	"vpminsw %s,%s,"
.LC224:
	.string	"vpminub %s,%s,"
.LC225:
	.string	"vpmullw %s,%s,"
.LC226:
	.string	"vpmuludq %s,%s,"
.LC227:
	.string	"vpsllw %s,%s,"
.LC228:
	.string	"vpslld %s,%s,"
.LC229:
	.string	"vpsllq %s,%s,"
.LC230:
	.string	"vpsraw %s,%s,"
.LC231:
	.string	"vpsrad %s,%s,"
.LC232:
	.string	"vpsrlw %s,%s,"
.LC233:
	.string	"vpsrld %s,%s,"
.LC234:
	.string	"vpsrlq %s,%s,"
.LC235:
	.string	"vpsubb %s,%s,"
.LC236:
	.string	"vpsubw %s,%s,"
.LC237:
	.string	"vpsubd %s,%s,"
.LC238:
	.string	"vpsubq %s,%s,"
.LC239:
	.string	"vpsubsb %s,%s,"
.LC240:
	.string	"vpsubsw %s,%s,"
.LC241:
	.string	"vpsubusb %s,%s,"
.LC242:
	.string	"vpsubusw %s,%s,"
.LC243:
	.string	"vpand %s,%s,"
.LC244:
	.string	"vpor %s,%s,"
.LC245:
	.string	"vpxor %s,%s,"
.LC64:
	.string	"sd"
	.section	.text._ZN6disasm15DisassemblerX6414AVXInstructionEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6414AVXInstructionEPh
	.type	_ZN6disasm15DisassemblerX6414AVXInstructionEPh, @function
_ZN6disasm15DisassemblerX6414AVXInstructionEPh:
.LFB5171:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	1(%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movzbl	160(%rdi), %edi
	movzbl	161(%r12), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	(%rsi), %eax
	movzbl	162(%r12), %edx
	cmpb	$-60, %dil
	je	.L357
	movl	%ecx, %esi
	andl	$3, %esi
	cmpb	$1, %sil
	je	.L358
	movzbl	%cl, %r8d
	cmpb	$2, %sil
	je	.L1181
.L511:
	cmpb	$3, %sil
	je	.L1182
	testb	%sil, %sil
	jne	.L618
	cmpb	$-59, %dil
	je	.L619
	andl	$3, %ecx
	cmpb	$2, %cl
	jne	.L620
	movzbl	1(%rbx), %edx
	sarl	$3, %r8d
	notl	%r8d
	sarl	$3, %edx
	andl	$15, %r8d
	andl	$7, %edx
	movl	%r8d, %r14d
	movl	%edx, %ecx
	orl	$8, %ecx
	testb	$4, 157(%r12)
	cmovne	%ecx, %edx
	cmpb	$-11, %al
	je	.L622
	ja	.L623
	cmpb	$-14, %al
	je	.L624
	cmpb	$-13, %al
	jne	.L626
	cmpl	$2, %edx
	je	.L1042
	cmpl	$3, %edx
	je	.L1043
	cmpl	$1, %edx
	je	.L642
	cmpb	$0, 156(%r12)
	jne	.L510
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	.LC61(%rip), %rdx
.L641:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	je	.L885
.L643:
	movq	%rdx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %rdx
	movq	%rax, %r8
.L644:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L645
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L645
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L645:
	cltq
	leaq	.LC20(%rip), %rcx
	leaq	.LC21(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rcx,%rax), %ecx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L357:
	movl	%edx, %esi
	andl	$3, %esi
	cmpb	$1, %sil
	je	.L1183
	movzbl	%dl, %r8d
	cmpb	$2, %sil
	jne	.L511
	andl	$3, %ecx
	cmpb	$1, %cl
	jne	.L513
.L512:
	movzbl	1(%rbx), %r14d
	movzbl	%dl, %r15d
	sarl	$3, %r15d
	movl	%r14d, %r8d
	shrl	$3, %r14d
	notl	%r15d
	andl	$7, %r14d
	andl	$15, %r15d
	shrb	$6, %r8b
	movl	%r14d, %edx
	orl	$8, %edx
	testb	$4, 157(%r12)
	cmovne	%edx, %r14d
	subl	$16, %eax
	cmpb	$79, %al
	ja	.L515
	leaq	.L517(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6414AVXInstructionEPh,"a",@progbits
	.align 4
	.align 4
.L517:
	.long	.L528-.L517
	.long	.L527-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L526-.L517
	.long	.L515-.L517
	.long	.L525-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L524-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L515-.L517
	.long	.L523-.L517
	.long	.L522-.L517
	.long	.L521-.L517
	.long	.L515-.L517
	.long	.L520-.L517
	.long	.L519-.L517
	.long	.L518-.L517
	.long	.L516-.L517
	.section	.text._ZN6disasm15DisassemblerX6414AVXInstructionEPh
	.p2align 4,,10
	.p2align 3
.L1182:
	cmpb	$-59, %dil
	je	.L560
	andl	$3, %ecx
	cmpb	$1, %cl
	je	.L560
	cmpb	$2, %cl
	jne	.L646
	movzbl	1(%rbx), %edx
	sarl	$3, %r8d
	notl	%r8d
	sarl	$3, %edx
	andl	$15, %r8d
	andl	$7, %edx
	movl	%r8d, %r14d
	movl	%edx, %ecx
	orl	$8, %ecx
	testb	$4, 157(%r12)
	cmovne	%ecx, %edx
	cmpb	$-10, %al
	je	.L648
	cmpb	$-9, %al
	je	.L649
	cmpb	$-11, %al
	je	.L1184
	cmpb	$0, 156(%r12)
	jne	.L510
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$1, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L618:
	cmpb	$1, %sil
	jne	.L662
	andl	$3, %ecx
	cmpb	$-59, %dil
	je	.L1067
.L485:
	subb	$1, %cl
	jne	.L662
	.p2align 4,,10
	.p2align 3
.L1067:
	movl	%r8d, %ecx
	.p2align 4,,10
	.p2align 3
.L358:
	movzbl	1(%rbx), %r14d
	movzbl	%cl, %esi
	sarl	$3, %esi
	sarl	$3, %r14d
	notl	%esi
	andl	$7, %r14d
	andl	$15, %esi
	movl	%r14d, %ecx
	orl	$8, %ecx
	testb	$4, 157(%r12)
	cmovne	%ecx, %r14d
	subl	$16, %eax
	cmpb	$-18, %al
	ja	.L718
	leaq	.L720(%rip), %rcx
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6414AVXInstructionEPh
	.align 4
	.align 4
.L720:
	.long	.L795-.L720
	.long	.L794-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L793-.L720
	.long	.L792-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L791-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L790-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L789-.L720
	.long	.L788-.L720
	.long	.L787-.L720
	.long	.L786-.L720
	.long	.L785-.L720
	.long	.L784-.L720
	.long	.L718-.L720
	.long	.L783-.L720
	.long	.L782-.L720
	.long	.L781-.L720
	.long	.L780-.L720
	.long	.L779-.L720
	.long	.L778-.L720
	.long	.L777-.L720
	.long	.L776-.L720
	.long	.L775-.L720
	.long	.L774-.L720
	.long	.L773-.L720
	.long	.L772-.L720
	.long	.L771-.L720
	.long	.L770-.L720
	.long	.L769-.L720
	.long	.L768-.L720
	.long	.L767-.L720
	.long	.L766-.L720
	.long	.L765-.L720
	.long	.L764-.L720
	.long	.L718-.L720
	.long	.L763-.L720
	.long	.L762-.L720
	.long	.L761-.L720
	.long	.L760-.L720
	.long	.L759-.L720
	.long	.L758-.L720
	.long	.L757-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L756-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L755-.L720
	.long	.L718-.L720
	.long	.L754-.L720
	.long	.L753-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L752-.L720
	.long	.L751-.L720
	.long	.L750-.L720
	.long	.L749-.L720
	.long	.L748-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L747-.L720
	.long	.L746-.L720
	.long	.L745-.L720
	.long	.L744-.L720
	.long	.L743-.L720
	.long	.L742-.L720
	.long	.L741-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L740-.L720
	.long	.L739-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L738-.L720
	.long	.L737-.L720
	.long	.L736-.L720
	.long	.L735-.L720
	.long	.L734-.L720
	.long	.L733-.L720
	.long	.L732-.L720
	.long	.L731-.L720
	.long	.L718-.L720
	.long	.L730-.L720
	.long	.L729-.L720
	.long	.L728-.L720
	.long	.L727-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L718-.L720
	.long	.L726-.L720
	.long	.L725-.L720
	.long	.L724-.L720
	.long	.L723-.L720
	.long	.L722-.L720
	.long	.L721-.L720
	.long	.L719-.L720
	.section	.text._ZN6disasm15DisassemblerX6414AVXInstructionEPh
	.p2align 4,,10
	.p2align 3
.L1181:
	cmpb	$-59, %dil
	je	.L1022
	movzbl	%cl, %edx
	andl	$3, %ecx
	cmpb	$1, %cl
	je	.L512
.L513:
	cmpb	$2, %cl
	jne	.L662
	sarl	$3, %edx
	notl	%edx
	andl	$15, %edx
	movslq	%edx, %r14
	movzbl	1(%rbx), %edx
	sarl	$3, %edx
	andl	$7, %edx
	movl	%edx, %ecx
	orl	$8, %ecx
	testb	$4, 157(%r12)
	cmovne	%ecx, %edx
	cmpb	$-11, %al
	je	.L664
	cmpb	$-9, %al
	je	.L665
	cmpb	$0, 156(%r12)
	jne	.L510
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$1, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L620:
	cmpb	$1, %cl
	jne	.L662
.L619:
	movzbl	1(%rbx), %r14d
	movzbl	%r8b, %esi
	sarl	$3, %esi
	sarl	$3, %r14d
	notl	%esi
	andl	$7, %r14d
	andl	$15, %esi
	movl	%r14d, %edx
	orl	$8, %edx
	testb	$4, 157(%r12)
	cmovne	%edx, %r14d
	cmpb	$87, %al
	ja	.L684
	cmpb	$15, %al
	jbe	.L685
	subl	$16, %eax
	cmpb	$71, %al
	ja	.L685
	leaq	.L687(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6414AVXInstructionEPh
	.align 4
	.align 4
.L687:
	.long	.L695-.L687
	.long	.L694-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L693-.L687
	.long	.L692-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L691-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L690-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L685-.L687
	.long	.L689-.L687
	.long	.L688-.L687
	.long	.L685-.L687
	.long	.L686-.L687
	.section	.text._ZN6disasm15DisassemblerX6414AVXInstructionEPh
	.p2align 4,,10
	.p2align 3
.L646:
	cmpb	$3, %cl
	je	.L1185
.L662:
	cmpb	$0, 156(%r12)
	jne	.L510
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L356:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1186
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	movzbl	1(%rbx), %r15d
	movzbl	%r8b, %r14d
	sarl	$3, %r14d
	movl	%r15d, %r8d
	shrl	$3, %r15d
	notl	%r14d
	andl	$7, %r15d
	andl	$15, %r14d
	shrb	$6, %r8b
	movl	%r15d, %edx
	orl	$8, %edx
	testb	$4, 157(%r12)
	cmovne	%edx, %r15d
	cmpb	$124, %al
	ja	.L563
	cmpb	$15, %al
	jbe	.L564
	subl	$16, %eax
	cmpb	$108, %al
	ja	.L564
	leaq	.L566(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6414AVXInstructionEPh
	.align 4
	.align 4
.L566:
	.long	.L579-.L566
	.long	.L578-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L577-.L566
	.long	.L564-.L566
	.long	.L576-.L566
	.long	.L575-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L574-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L573-.L566
	.long	.L572-.L566
	.long	.L571-.L566
	.long	.L564-.L566
	.long	.L570-.L566
	.long	.L569-.L566
	.long	.L568-.L566
	.long	.L567-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L564-.L566
	.long	.L565-.L566
	.section	.text._ZN6disasm15DisassemblerX6414AVXInstructionEPh
	.p2align 4,,10
	.p2align 3
.L1183:
	andl	$3, %ecx
	cmpb	$2, %cl
	jne	.L1187
	movzbl	1(%rbx), %r14d
	sarl	$3, %edx
	notl	%edx
	sarl	$3, %r14d
	andl	$15, %edx
	andl	$7, %r14d
	movl	%edx, %r8d
	movl	%r14d, %edx
	orl	$8, %edx
	testb	$4, 157(%r12)
	cmovne	%edx, %r14d
	cmpb	$-9, %al
	ja	.L361
	leaq	.L363(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6414AVXInstructionEPh
	.align 4
	.align 4
.L363:
	.long	.L401-.L363
	.long	.L400-.L363
	.long	.L399-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L398-.L363
	.long	.L397-.L363
	.long	.L396-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L395-.L363
	.long	.L361-.L363
	.long	.L394-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L393-.L363
	.long	.L392-.L363
	.long	.L391-.L363
	.long	.L361-.L363
	.long	.L390-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L389-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L388-.L363
	.long	.L361-.L363
	.long	.L387-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L386-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L385-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L384-.L363
	.long	.L383-.L363
	.long	.L382-.L363
	.long	.L381-.L363
	.long	.L380-.L363
	.long	.L379-.L363
	.long	.L378-.L363
	.long	.L377-.L363
	.long	.L376-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L375-.L363
	.long	.L361-.L363
	.long	.L374-.L363
	.long	.L361-.L363
	.long	.L373-.L363
	.long	.L361-.L363
	.long	.L372-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L371-.L363
	.long	.L361-.L363
	.long	.L370-.L363
	.long	.L361-.L363
	.long	.L369-.L363
	.long	.L361-.L363
	.long	.L368-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L367-.L363
	.long	.L361-.L363
	.long	.L366-.L363
	.long	.L361-.L363
	.long	.L365-.L363
	.long	.L361-.L363
	.long	.L364-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L361-.L363
	.long	.L362-.L363
	.section	.text._ZN6disasm15DisassemblerX6414AVXInstructionEPh
	.p2align 4,,10
	.p2align 3
.L563:
	cmpb	$-16, %al
	jne	.L564
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L614
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L615:
	leaq	.LC138(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L1187:
	cmpb	$3, %cl
	jne	.L1020
	movzbl	1(%rbx), %r14d
	movzbl	%dl, %esi
	sarl	$3, %esi
	sarl	$3, %r14d
	notl	%esi
	andl	$7, %r14d
	andl	$15, %esi
	movl	%r14d, %edx
	orl	$8, %edx
	testb	$4, 157(%r12)
	cmovne	%edx, %r14d
	subl	$10, %eax
	cmpb	$24, %al
	ja	.L487
	leaq	.L489(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6414AVXInstructionEPh
	.align 4
	.align 4
.L489:
	.long	.L495-.L489
	.long	.L494-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L493-.L489
	.long	.L492-.L489
	.long	.L491-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L487-.L489
	.long	.L490-.L489
	.long	.L487-.L489
	.long	.L488-.L489
	.section	.text._ZN6disasm15DisassemblerX6414AVXInstructionEPh
	.p2align 4,,10
	.p2align 3
.L623:
	cmpb	$-9, %al
	jne	.L626
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %r15
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L636
	movslq	%edx, %rdx
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rdx,8), %rcx
.L637:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L638
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L638
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L638:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC142(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	cltq
	addq	%rax, %r13
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L639
	movslq	%r14d, %r8
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r8,8), %rdx
.L640:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L684:
	cmpb	$-62, %al
	jne	.L685
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L715
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rsi,8), %r15
.L716:
	cmpq	%rcx, %rax
	je	.L1188
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L870:
	movq	%r15, %rcx
	leaq	.LC167(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC159(%rip), %rcx
	leaq	.LC168(%rip), %rsi
	movq	%r12, %rdi
	cltq
	movq	%rcx, %xmm0
	leaq	.LC161(%rip), %rcx
	addq	%rax, %r13
	leaq	.LC160(%rip), %rax
	movq	%rax, %xmm1
	leaq	.LC162(%rip), %rax
	addq	$1, %r13
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %xmm2
	leaq	.LC164(%rip), %rax
	movaps	%xmm0, -128(%rbp)
	movq	%rcx, %xmm0
	leaq	.LC163(%rip), %rcx
	movq	%rax, %xmm3
	punpcklqdq	%xmm2, %xmm0
	leaq	.LC166(%rip), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rcx, %xmm0
	leaq	.LC165(%rip), %rcx
	movq	%rax, %xmm4
	punpcklqdq	%xmm3, %xmm0
	movzbl	-1(%r13), %eax
	movaps	%xmm0, -96(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	-128(%rbp,%rax,8), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L1022:
	movl	%ecx, %edx
	jmp	.L512
.L565:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L616
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r14,8), %r14
.L617:
	cmpq	%rcx, %rax
	je	.L1189
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
.L858:
	movq	%r14, %rcx
	leaq	.LC139(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L567:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L612
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r14,8), %r14
.L613:
	cmpq	%rcx, %rax
	je	.L1190
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
.L855:
	movq	%r14, %rcx
	leaq	.LC137(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L568:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L610
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r14,8), %r14
.L611:
	cmpq	%rcx, %rax
	je	.L1191
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
.L852:
	movq	%r14, %rcx
	leaq	.LC136(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L569:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L608
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r14,8), %r14
.L609:
	cmpq	%rcx, %rax
	je	.L1192
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
.L849:
	movq	%r14, %rcx
	leaq	.LC135(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L570:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L606
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r14,8), %r14
.L607:
	cmpq	%rcx, %rax
	je	.L1193
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
.L846:
	movq	%r14, %rcx
	leaq	.LC134(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L571:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L604
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r14,8), %r14
.L605:
	cmpq	%rcx, %rax
	je	.L1194
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
.L843:
	movq	%r14, %rcx
	leaq	.LC133(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L574:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L598
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r14,8), %r14
.L599:
	cmpq	%rcx, %rax
	je	.L1195
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
.L834:
	movq	%r14, %rcx
	leaq	.LC130(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L575:
	movq	(%r12), %r8
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%r8), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L595
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rcx
.L596:
	leaq	.LC56(%rip), %rdx
	cmpb	$-60, %dil
	jne	.L597
	cmpb	$0, 162(%r12)
	leaq	.LC57(%rip), %rax
	cmovs	%rax, %rdx
.L597:
	leaq	.LC129(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L572:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L602
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r14,8), %r14
.L603:
	cmpq	%rcx, %rax
	je	.L1196
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
.L840:
	movq	%r14, %rcx
	leaq	.LC132(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L573:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L600
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r14,8), %r14
.L601:
	cmpq	%rcx, %rax
	je	.L1197
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
.L837:
	movq	%r14, %rcx
	leaq	.LC131(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L576:
	movq	(%r12), %r8
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%r8), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L592
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rcx
.L593:
	leaq	.LC56(%rip), %rdx
	cmpb	$-60, %dil
	jne	.L594
	cmpb	$0, 162(%r12)
	leaq	.LC57(%rip), %rax
	cmovs	%rax, %rdx
.L594:
	leaq	.LC128(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L577:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L589
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r14,8), %r8
.L590:
	cmpq	%rcx, %rax
	je	.L1198
	movq	%r8, -136(%rbp)
	movl	%r15d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L831:
	cmpb	$-60, 160(%r12)
	leaq	.LC58(%rip), %rdx
	jne	.L591
	cmpb	$0, 162(%r12)
	leaq	.LC59(%rip), %rax
	cmovs	%rax, %rdx
.L591:
	leaq	.LC116(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L578:
	leaq	.LC127(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movb	%r8b, -136(%rbp)
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movzbl	-136(%rbp), %r8d
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	cltq
	addq	%rax, %r13
	cmpb	$3, %r8b
	je	.L1199
.L586:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1200
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
.L828:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L579:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L581
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L582:
	xorl	%eax, %eax
	leaq	.LC126(%rip), %rsi
	movq	%r12, %rdi
	movq	%rcx, -144(%rbp)
	movb	%r8b, -136(%rbp)
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movzbl	-136(%rbp), %r8d
	movq	-144(%rbp), %rcx
	cmpb	$3, %r8b
	je	.L1201
.L583:
	movq	%r13, %rsi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L564:
	cmpb	$0, 156(%r12)
	jne	.L510
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$1, %eax
	jmp	.L356
.L690:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L707
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L708:
	leaq	.LC155(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L691:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L705
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L706:
	leaq	.LC154(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L686:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L713
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rsi,8), %r15
.L714:
	cmpq	%rcx, %rax
	je	.L1202
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L867:
	movq	%r15, %rcx
	leaq	.LC158(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L694:
	leaq	.LC151(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %r13
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L699
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L700:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L695:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L697
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L698:
	leaq	.LC150(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L688:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L711
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rsi,8), %r15
.L712:
	cmpq	%rcx, %rax
	je	.L1203
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L864:
	movq	%r15, %rcx
	leaq	.LC157(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L692:
	leaq	.LC153(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %r13
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L703
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L704:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L693:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L701
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L702:
	leaq	.LC152(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L689:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L709
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rsi,8), %r15
.L710:
	cmpq	%rcx, %rax
	je	.L1204
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L861:
	movq	%r15, %rcx
	leaq	.LC156(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L685:
	cmpb	$0, 156(%r12)
	jne	.L510
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$1, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L515:
	cmpb	$0, 156(%r12)
	jne	.L510
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$1, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L361:
	cmpb	$0, 156(%r12)
	jne	.L510
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$1, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L487:
	cmpb	$0, 156(%r12)
	jne	.L510
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$1, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L1185:
	movzbl	1(%rbx), %esi
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %edx
	orl	$8, %edx
	testb	$4, 157(%r12)
	cmovne	%edx, %esi
	cmpb	$-16, %al
	jne	.L676
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L677
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rcx
.L678:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L679
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L679
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L679:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC148(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	cltq
	addq	%rax, %r13
	cmpb	$0, 163(%r12)
	jne	.L680
	testb	$8, 157(%r12)
	jne	.L681
	cmpb	$0, 158(%r12)
	jne	.L680
	movzbl	0(%r13), %edx
	leaq	.LC149(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	andl	$31, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
.L682:
	addq	$1, %r13
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L718:
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6424UnimplementedInstructionEv
	movl	$1, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %r15
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L651
	movslq	%r8d, %r8
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rcx
	movq	(%rcx,%r8,8), %r8
.L652:
	cmpq	%r15, %rax
	je	.L1205
	movq	%r8, -136(%rbp)
	movl	%edx, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L876:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L653
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L653
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L653:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC143(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L649:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %r15
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L657
	movslq	%edx, %rdx
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rdx,8), %rcx
.L658:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L659
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L659
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L659:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC145(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	cltq
	addq	%rax, %r13
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L660
	movslq	%r14d, %r8
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r8,8), %rdx
.L661:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L648:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %r15
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L654
	movslq	%r8d, %r8
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rcx
	movq	(%rcx,%r8,8), %r8
.L655:
	cmpq	%r15, %rax
	je	.L1206
	movq	%r8, -136(%rbp)
	movl	%edx, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L879:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L656
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L656
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L656:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC144(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L495:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L496
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rsi,8), %r15
.L497:
	cmpq	%rcx, %rax
	je	.L1207
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L886:
	movq	%r15, %rcx
	leaq	.LC104(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r13
	xorl	%eax, %eax
	movzbl	0(%r13), %edx
	addq	$1, %r13
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L491:
	leaq	.LC110(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%r13, %rax
	movzbl	(%rax), %ecx
	leaq	1(%rax), %r13
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L504
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L505:
	xorl	%eax, %eax
	leaq	.LC108(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L492:
	leaq	.LC109(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%r13, %rax
	movzbl	(%rax), %ecx
	leaq	1(%rax), %r13
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L502
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L503:
	xorl	%eax, %eax
	leaq	.LC108(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L488:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L508
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rsi,8), %r15
.L509:
	cmpq	%rcx, %rax
	je	.L1208
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L895:
	movq	%r15, %rcx
	leaq	.LC112(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r13
	xorl	%eax, %eax
	movzbl	0(%r13), %edx
	addq	$1, %r13
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L493:
	leaq	.LC107(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%r13, %rax
	movzbl	(%rax), %ecx
	leaq	1(%rax), %r13
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L500
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L501:
	xorl	%eax, %eax
	leaq	.LC108(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L494:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L498
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rsi,8), %r15
.L499:
	cmpq	%rcx, %rax
	je	.L1209
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L889:
	movq	%r15, %rcx
	leaq	.LC106(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r13
	xorl	%eax, %eax
	movzbl	0(%r13), %edx
	addq	$1, %r13
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L490:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L506
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rsi,8), %r15
.L507:
	cmpq	%rcx, %rax
	je	.L1210
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L892:
	movq	%r15, %rcx
	leaq	.LC111(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r13
	xorl	%eax, %eax
	movzbl	0(%r13), %edx
	addq	$1, %r13
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L1042:
	leaq	.LC62(%rip), %rdx
	jmp	.L641
.L636:
	movl	%edx, %esi
	call	*%rax
	movq	%rax, %rcx
	jmp	.L637
.L516:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L557
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r15,8), %r15
.L558:
	cmpq	%rcx, %rax
	je	.L1211
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L825:
	movq	%r15, %rcx
	leaq	.LC125(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L520:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L551
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r15,8), %r15
.L552:
	cmpq	%rcx, %rax
	je	.L1212
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L816:
	movq	%r15, %rcx
	leaq	.LC122(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L521:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L549
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r15,8), %r15
.L550:
	cmpq	%rcx, %rax
	je	.L1213
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L813:
	movq	%r15, %rcx
	leaq	.LC121(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L518:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L555
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r15,8), %r15
.L556:
	cmpq	%rcx, %rax
	je	.L1214
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L822:
	movq	%r15, %rcx
	leaq	.LC124(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L519:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L553
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r15,8), %r15
.L554:
	cmpq	%rcx, %rax
	je	.L1215
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L819:
	movq	%r15, %rcx
	leaq	.LC123(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L522:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L547
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r15,8), %r15
.L548:
	cmpq	%rcx, %rax
	je	.L1216
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L810:
	movq	%r15, %rcx
	leaq	.LC120(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L523:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L545
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r15,8), %r15
.L546:
	cmpq	%rcx, %rax
	je	.L1217
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L807:
	movq	%r15, %rcx
	leaq	.LC119(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L524:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L543
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r15,8), %r15
.L544:
	cmpq	%rcx, %rax
	je	.L1218
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L804:
	movq	%r15, %rcx
	leaq	.LC118(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L525:
	movq	(%r12), %r8
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%r8), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L540
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
.L541:
	leaq	.LC56(%rip), %rdx
	cmpb	$-60, %dil
	jne	.L542
	cmpb	$0, 162(%r12)
	leaq	.LC57(%rip), %rax
	cmovs	%rax, %rdx
.L542:
	leaq	.LC117(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L528:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L529
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L530:
	xorl	%eax, %eax
	leaq	.LC113(%rip), %rsi
	movq	%r12, %rdi
	movq	%rcx, -144(%rbp)
	movb	%r8b, -136(%rbp)
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movzbl	-136(%rbp), %r8d
	movq	-144(%rbp), %rcx
	cmpb	$3, %r8b
	je	.L1219
.L531:
	movq	%r13, %rsi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L526:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L537
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%r15,8), %r8
.L538:
	cmpq	%rcx, %rax
	je	.L1220
	movq	%r8, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L801:
	cmpb	$-60, 160(%r12)
	leaq	.LC54(%rip), %rdx
	jne	.L539
	cmpb	$0, 162(%r12)
	leaq	.LC55(%rip), %rax
	cmovs	%rax, %rdx
.L539:
	leaq	.LC116(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L527:
	leaq	.LC115(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movb	%r8b, -136(%rbp)
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movzbl	-136(%rbp), %r8d
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	cltq
	addq	%rax, %r13
	cmpb	$3, %r8b
	je	.L1221
.L534:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	je	.L1222
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L798:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L614:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L615
.L362:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %r15
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L427
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
.L428:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L429
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L429
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L429:
	cltq
	leaq	.LC20(%rip), %rdx
	movq	%r12, %rdi
	movl	%r8d, -136(%rbp)
	movsbl	(%rdx,%rax), %edx
	leaq	.LC77(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	movl	-136(%rbp), %r8d
	cltq
	addq	%rax, %r13
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L430
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rdx,8), %rdx
.L431:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L364:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L425
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r8
.L426:
	cmpq	%rcx, %rax
	je	.L1223
	movq	%r8, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L1009:
	movzbl	157(%r12), %eax
	leaq	.LC64(%rip), %rdx
	leaq	.LC76(%rip), %rsi
	movq	%r12, %rdi
	shrb	$3, %al
	andl	$1, %eax
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L389:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L458
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L459:
	cmpq	%rcx, %rax
	je	.L1224
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L934:
	movq	%r15, %rcx
	leaq	.LC91(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L390:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L456
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L457:
	cmpq	%rcx, %rax
	je	.L1225
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L931:
	movq	%r15, %rcx
	leaq	.LC90(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L401:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L442
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L443:
	cmpq	%rcx, %rax
	je	.L1226
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L898:
	movq	%r15, %rcx
	leaq	.LC83(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L393:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L432
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L433:
	cmpq	%rcx, %rax
	je	.L1227
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L922:
	movq	%r15, %rcx
	leaq	.LC78(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L394:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L454
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L455:
	cmpq	%rcx, %rax
	je	.L1228
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L919:
	movq	%r15, %rcx
	leaq	.LC89(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L391:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L436
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L437:
	cmpq	%rcx, %rax
	je	.L1229
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L928:
	movq	%r15, %rcx
	leaq	.LC80(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L392:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L434
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L435:
	cmpq	%rcx, %rax
	je	.L1230
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L925:
	movq	%r15, %rcx
	leaq	.LC79(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L395:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L450
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L451:
	cmpq	%rcx, %rax
	je	.L1231
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L916:
	movq	%r15, %rcx
	leaq	.LC87(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L396:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L448
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L449:
	cmpq	%rcx, %rax
	je	.L1232
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L913:
	movq	%r15, %rcx
	leaq	.LC86(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L373:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L415
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r8
.L416:
	cmpq	%rcx, %rax
	je	.L1233
	movq	%r8, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L982:
	movzbl	157(%r12), %eax
	leaq	.LC64(%rip), %rdx
	leaq	.LC71(%rip), %rsi
	movq	%r12, %rdi
	shrb	$3, %al
	andl	$1, %eax
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L374:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L409
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r8
.L410:
	cmpq	%rcx, %rax
	je	.L1234
	movq	%r8, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L979:
	movzbl	157(%r12), %eax
	leaq	.LC64(%rip), %rdx
	leaq	.LC68(%rip), %rsi
	movq	%r12, %rdi
	shrb	$3, %al
	andl	$1, %eax
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L365:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L419
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r8
.L420:
	cmpq	%rcx, %rax
	je	.L1235
	movq	%r8, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L1006:
	movzbl	157(%r12), %eax
	leaq	.LC64(%rip), %rdx
	leaq	.LC73(%rip), %rsi
	movq	%r12, %rdi
	shrb	$3, %al
	andl	$1, %eax
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L366:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L413
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r8
.L414:
	cmpq	%rcx, %rax
	je	.L1236
	movq	%r8, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L1003:
	movzbl	157(%r12), %eax
	leaq	.LC64(%rip), %rdx
	leaq	.LC70(%rip), %rsi
	movq	%r12, %rdi
	shrb	$3, %al
	andl	$1, %eax
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L397:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L446
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L447:
	cmpq	%rcx, %rax
	je	.L1237
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L910:
	movq	%r15, %rcx
	leaq	.LC85(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L398:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L444
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L445:
	cmpq	%rcx, %rax
	je	.L1238
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L907:
	movq	%r15, %rcx
	leaq	.LC84(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L369:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L417
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r8
.L418:
	cmpq	%rcx, %rax
	je	.L1239
	movq	%r8, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L994:
	movzbl	157(%r12), %eax
	leaq	.LC64(%rip), %rdx
	leaq	.LC72(%rip), %rsi
	movq	%r12, %rdi
	shrb	$3, %al
	andl	$1, %eax
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L370:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L411
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r8
.L412:
	cmpq	%rcx, %rax
	je	.L1240
	movq	%r8, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L991:
	movzbl	157(%r12), %eax
	leaq	.LC64(%rip), %rdx
	leaq	.LC69(%rip), %rsi
	movq	%r12, %rdi
	shrb	$3, %al
	andl	$1, %eax
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L381:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L472
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L473:
	cmpq	%rcx, %rax
	je	.L1241
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L958:
	movq	%r15, %rcx
	leaq	.LC98(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L382:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L470
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L471:
	cmpq	%rcx, %rax
	je	.L1242
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L955:
	movq	%r15, %rcx
	leaq	.LC97(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L367:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L407
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r8
.L408:
	cmpq	%rcx, %rax
	je	.L1243
	movq	%r8, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L1000:
	movzbl	157(%r12), %eax
	leaq	.LC64(%rip), %rdx
	leaq	.LC67(%rip), %rsi
	movq	%r12, %rdi
	shrb	$3, %al
	andl	$1, %eax
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L368:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L423
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r8
.L424:
	cmpq	%rcx, %rax
	je	.L1244
	movq	%r8, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L997:
	movzbl	157(%r12), %eax
	leaq	.LC64(%rip), %rdx
	leaq	.LC75(%rip), %rsi
	movq	%r12, %rdi
	shrb	$3, %al
	andl	$1, %eax
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L399:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L438
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L439:
	cmpq	%rcx, %rax
	je	.L1245
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L904:
	movq	%r15, %rcx
	leaq	.LC81(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L400:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L440
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L441:
	cmpq	%rcx, %rax
	je	.L1246
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L901:
	movq	%r15, %rcx
	leaq	.LC82(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L371:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L405
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r8
.L406:
	cmpq	%rcx, %rax
	je	.L1247
	movq	%r8, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L988:
	movzbl	157(%r12), %eax
	leaq	.LC64(%rip), %rdx
	leaq	.LC66(%rip), %rsi
	movq	%r12, %rdi
	shrb	$3, %al
	andl	$1, %eax
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L372:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L421
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r8
.L422:
	cmpq	%rcx, %rax
	je	.L1248
	movq	%r8, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L985:
	movzbl	157(%r12), %eax
	leaq	.LC64(%rip), %rdx
	leaq	.LC74(%rip), %rsi
	movq	%r12, %rdi
	shrb	$3, %al
	andl	$1, %eax
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L377:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L480
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L481:
	cmpq	%rcx, %rax
	je	.L1249
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L970:
	movq	%r15, %rcx
	leaq	.LC102(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L378:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L478
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L479:
	cmpq	%rcx, %rax
	je	.L1250
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L967:
	movq	%r15, %rcx
	leaq	.LC101(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L385:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L464
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L465:
	cmpq	%rcx, %rax
	je	.L1251
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L946:
	movq	%r15, %rcx
	leaq	.LC94(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L386:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L462
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L463:
	cmpq	%rcx, %rax
	je	.L1252
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L943:
	movq	%r15, %rcx
	leaq	.LC93(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L375:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L402
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r8
.L403:
	cmpq	%rcx, %rax
	je	.L1253
	movq	%r8, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L976:
	movzbl	157(%r12), %eax
	leaq	.LC64(%rip), %rdx
	leaq	.LC65(%rip), %rsi
	movq	%r12, %rdi
	shrb	$3, %al
	andl	$1, %eax
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L376:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L482
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L483:
	cmpq	%rcx, %rax
	je	.L1254
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L973:
	movq	%r15, %rcx
	leaq	.LC103(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L383:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L468
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L469:
	cmpq	%rcx, %rax
	je	.L1255
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L952:
	movq	%r15, %rcx
	leaq	.LC96(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L384:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L466
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L467:
	cmpq	%rcx, %rax
	je	.L1256
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L949:
	movq	%r15, %rcx
	leaq	.LC95(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L379:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L476
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L477:
	cmpq	%rcx, %rax
	je	.L1257
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L964:
	movq	%r15, %rcx
	leaq	.LC100(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L380:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L474
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L475:
	cmpq	%rcx, %rax
	je	.L1258
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L961:
	movq	%r15, %rcx
	leaq	.LC99(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L387:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L460
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L461:
	cmpq	%rcx, %rax
	je	.L1259
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L940:
	movq	%r15, %rcx
	leaq	.LC92(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L388:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L452
	movslq	%r8d, %rdx
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rsi
	movq	(%rsi,%rdx,8), %r15
.L453:
	cmpq	%rcx, %rax
	je	.L1260
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
.L937:
	movq	%r15, %rcx
	leaq	.LC88(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L732:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC221(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L733:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC212(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L724:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC237(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L725:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC236(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L726:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC235(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L727:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC226(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L728:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC229(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L729:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC228(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L730:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC227(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L731:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC245(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L719:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC209(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L721:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC208(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L722:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC207(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L723:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC238(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L764:
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movl	$100, %edx
	cmpb	$-60, 160(%r12)
	movq	%rax, %rcx
	jne	.L796
	movsbl	162(%r12), %edx
	sarl	$31, %edx
	andl	$13, %edx
	addl	$100, %edx
.L796:
	leaq	.LC178(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L765:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC206(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L766:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC205(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L767:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC204(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L768:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC203(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L769:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC202(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L770:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC201(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L771:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC200(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L772:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC220(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L773:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC219(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L774:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC218(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L775:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC199(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L776:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC198(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L777:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC197(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L778:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC196(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L779:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC194(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L780:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC195(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L781:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC193(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L782:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC192(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L783:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC191(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L784:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC190(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L785:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC189(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L786:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC177(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L787:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC176(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L788:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC188(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L789:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC175(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L790:
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	leaq	.LC174(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L791:
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC173(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L792:
	leaq	.LC172(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	movl	%r14d, %esi
	cltq
	addq	%rax, %r13
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L793:
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC171(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L794:
	leaq	.LC170(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	movl	%r14d, %esi
	cltq
	addq	%rax, %r13
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L795:
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC169(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L748:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC225(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L749:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC210(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L750:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC234(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L751:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC233(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L752:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC232(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L753:
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	leaq	.LC187(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r13
	xorl	%eax, %eax
	movzbl	0(%r13), %edx
	addq	$1, %r13
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L754:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC186(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r13
	xorl	%eax, %eax
	movzbl	0(%r13), %edx
	addq	$1, %r13
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L755:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC185(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC159(%rip), %rcx
	leaq	.LC168(%rip), %rsi
	movq	%r12, %rdi
	cltq
	movq	%rcx, %xmm0
	leaq	.LC161(%rip), %rcx
	addq	%rax, %r13
	leaq	.LC160(%rip), %rax
	movq	%rax, %xmm5
	leaq	.LC162(%rip), %rax
	addq	$1, %r13
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, %xmm6
	leaq	.LC164(%rip), %rax
	movaps	%xmm0, -128(%rbp)
	movq	%rcx, %xmm0
	leaq	.LC163(%rip), %rcx
	movq	%rax, %xmm7
	punpcklqdq	%xmm6, %xmm0
	leaq	.LC166(%rip), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rcx, %xmm0
	leaq	.LC165(%rip), %rcx
	movq	%rax, %xmm5
	punpcklqdq	%xmm7, %xmm0
	movzbl	-1(%r13), %eax
	movaps	%xmm0, -96(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	-128(%rbp,%rax,8), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L756:
	movl	$100, %r8d
	cmpb	$-60, %dil
	jne	.L797
	movsbl	%dl, %edx
	sarl	$31, %edx
	movl	%edx, %r8d
	andl	$13, %r8d
	addl	$100, %r8d
.L797:
	movl	%r8d, %edx
	leaq	.LC184(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	movl	%r14d, %esi
	cltq
	addq	%rax, %r13
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L757:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC217(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L758:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC216(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L759:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC215(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L760:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	_ZN6disasm6sf_strE(%rip), %rdx
	leaq	.LC183(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	%r14d, %eax
	sarl	%eax
	cltq
	movq	(%rdx,%rax,8), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC181(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r13
	xorl	%eax, %eax
	movzbl	0(%r13), %edx
	addq	$1, %r13
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L761:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	_ZN6disasm6sf_strE(%rip), %rdx
	leaq	.LC182(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	%r14d, %eax
	sarl	%eax
	cltq
	movq	(%rdx,%rax,8), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC181(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r13
	xorl	%eax, %eax
	movzbl	0(%r13), %edx
	addq	$1, %r13
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L762:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	_ZN6disasm6sf_strE(%rip), %rdx
	leaq	.LC180(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	%r14d, %eax
	sarl	%eax
	cltq
	movq	(%rdx,%rax,8), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC181(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r13
	xorl	%eax, %eax
	movzbl	0(%r13), %edx
	addq	$1, %r13
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L763:
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC179(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r13
	xorl	%eax, %eax
	movzbl	0(%r13), %edx
	addq	$1, %r13
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L740:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC230(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L741:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC222(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L742:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC214(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L743:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC213(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L744:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC243(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L745:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC224(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L746:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC242(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L747:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC241(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L736:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC223(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L737:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC240(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L738:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC239(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L739:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC231(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L734:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC211(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
.L735:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %r15
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r15, %rcx
	leaq	.LC244(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L622:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %r15
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L631
	movslq	%edx, %rdx
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rdx,8), %rcx
.L632:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L633
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L633
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L633:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC141(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	cltq
	addq	%rax, %r13
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L634
	movslq	%r14d, %r8
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r8,8), %rdx
.L635:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L676:
	cmpb	$0, 156(%r12)
	jne	.L510
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$1, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L626:
	cmpb	$0, 156(%r12)
	jne	.L510
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$1, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L624:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %r15
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L628
	movslq	%r8d, %r8
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rcx
	movq	(%rcx,%r8,8), %r8
.L629:
	cmpq	%r15, %rax
	je	.L1261
	movq	%r8, -136(%rbp)
	movl	%edx, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L882:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L630
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L630
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L630:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC140(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L665:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %r15
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L670
	movslq	%edx, %rdx
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rdx,8), %rcx
.L671:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L672
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L672
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L672:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC147(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	cltq
	addq	%rax, %r13
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L673
	movslq	%r14d, %rdx
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rdx,8), %rdx
.L674:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L664:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %r15
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%r15, %rax
	jne	.L667
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rcx
	movq	(%rcx,%r14,8), %r8
.L668:
	cmpq	%r15, %rax
	je	.L1262
	movq	%r8, -136(%rbp)
	movl	%edx, %esi
	call	*%rax
	movq	-136(%rbp), %r8
	movq	%rax, %rcx
.L873:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L669
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L669
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L669:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC146(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L680:
	cmpb	$0, 156(%r12)
	jne	.L510
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L682
.L1043:
	leaq	.LC63(%rip), %rdx
	jmp	.L641
.L1261:
	movslq	%edx, %rdx
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rdx,8), %rcx
	jmp	.L882
.L642:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	leaq	.LC60(%rip), %rdx
	jne	.L643
.L885:
	movslq	%r14d, %r8
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r8,8), %r8
	jmp	.L644
.L1221:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L535
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L536:
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movq	%rcx, -136(%rbp)
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	-136(%rbp), %rcx
	jmp	.L534
.L1262:
	movslq	%edx, %rdx
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rdx,8), %rcx
	jmp	.L873
.L1209:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L889
.L1207:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L886
.L1208:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L895
.L1210:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L892
.L1219:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L532
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L533:
	leaq	.LC114(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L531
.L677:
	call	*%rax
	movq	%rax, %rcx
	jmp	.L678
.L639:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L640
.L634:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L635
.L628:
	movl	%edx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movl	-136(%rbp), %edx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	jmp	.L629
.L631:
	movl	%edx, %esi
	call	*%rax
	movq	%rax, %rcx
	jmp	.L632
.L1206:
	movslq	%edx, %rdx
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rdx,8), %rcx
	jmp	.L879
.L1205:
	movslq	%edx, %rdx
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rdx,8), %rcx
	jmp	.L876
.L1222:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L798
.L1213:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L813
.L1211:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L825
.L1215:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L819
.L1220:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L801
.L1212:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L816
.L1214:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L822
.L1216:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L810
.L1218:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L804
.L1217:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L807
.L498:
	movq	%rcx, -136(%rbp)
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L499
.L496:
	movq	%rcx, -136(%rbp)
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L497
.L508:
	movq	%rcx, -136(%rbp)
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L509
.L506:
	movq	%rcx, -136(%rbp)
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L507
.L504:
	movl	%ecx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movl	-136(%rbp), %ecx
	movq	%rax, %rdx
	jmp	.L505
.L500:
	movl	%ecx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movl	-136(%rbp), %ecx
	movq	%rax, %rdx
	jmp	.L501
.L502:
	movl	%ecx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movl	-136(%rbp), %ecx
	movq	%rax, %rdx
	jmp	.L503
.L670:
	movl	%edx, %esi
	call	*%rax
	movq	%rax, %rcx
	jmp	.L671
.L673:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L674
.L667:
	movl	%edx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movl	-136(%rbp), %edx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	jmp	.L668
.L681:
	movzbl	0(%r13), %edx
	leaq	.LC149(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	andl	$63, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L682
.L1201:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L584
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L585:
	leaq	.LC114(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L583
.L1199:
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L587
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L588:
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	movq	%rcx, -136(%rbp)
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	-136(%rbp), %rcx
	jmp	.L586
.L657:
	movl	%edx, %esi
	call	*%rax
	movq	%rax, %rcx
	jmp	.L658
.L651:
	movl	%edx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movl	-136(%rbp), %edx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	jmp	.L652
.L654:
	movl	%edx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movl	-136(%rbp), %edx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	jmp	.L655
.L660:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L661
.L553:
	movq	%rcx, -136(%rbp)
	movl	%r15d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L554
.L537:
	movq	%rcx, -136(%rbp)
	movl	%r15d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L538
.L529:
	movq	%rcx, -144(%rbp)
	movl	%r14d, %esi
	movb	%r8b, -136(%rbp)
	call	*%rax
	movq	-144(%rbp), %rcx
	movzbl	-136(%rbp), %r8d
	movq	%rax, %rdx
	jmp	.L530
.L545:
	movq	%rcx, -136(%rbp)
	movl	%r15d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L546
.L540:
	movq	%r8, %rdi
	movl	%r14d, %esi
	call	*%rax
	movzbl	160(%r12), %edi
	movq	%rax, %rcx
	jmp	.L541
.L547:
	movq	%rcx, -136(%rbp)
	movl	%r15d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L548
.L543:
	movq	%rcx, -136(%rbp)
	movl	%r15d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L544
.L551:
	movq	%rcx, -136(%rbp)
	movl	%r15d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L552
.L555:
	movq	%rcx, -136(%rbp)
	movl	%r15d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L556
.L549:
	movq	%rcx, -136(%rbp)
	movl	%r15d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L550
.L557:
	movq	%rcx, -136(%rbp)
	movl	%r15d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L558
.L1195:
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	jmp	.L834
.L1200:
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	jmp	.L828
.L1198:
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rcx
	jmp	.L831
.L1193:
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	jmp	.L846
.L1191:
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	jmp	.L852
.L1189:
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	jmp	.L858
.L1190:
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	jmp	.L855
.L1192:
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	jmp	.L849
.L1196:
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	jmp	.L840
.L1194:
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	jmp	.L843
.L1197:
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	jmp	.L837
.L1203:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L864
.L1202:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L867
.L1204:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L861
.L1188:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L870
.L589:
	movq	%rcx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L590
.L581:
	movq	%rcx, -144(%rbp)
	movl	%r15d, %esi
	movb	%r8b, -136(%rbp)
	call	*%rax
	movq	-144(%rbp), %rcx
	movzbl	-136(%rbp), %r8d
	movq	%rax, %rdx
	jmp	.L582
.L598:
	movq	%rcx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L599
.L592:
	movq	%r8, %rdi
	movl	%r15d, %esi
	call	*%rax
	movzbl	160(%r12), %edi
	movq	%rax, %rcx
	jmp	.L593
.L595:
	movq	%r8, %rdi
	movl	%r15d, %esi
	call	*%rax
	movzbl	160(%r12), %edi
	movq	%rax, %rcx
	jmp	.L596
.L604:
	movq	%rcx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L605
.L600:
	movq	%rcx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L601
.L608:
	movq	%rcx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L609
.L602:
	movq	%rcx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L603
.L616:
	movq	%rcx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L617
.L612:
	movq	%rcx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L613
.L606:
	movq	%rcx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L607
.L610:
	movq	%rcx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L611
.L699:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L700
.L703:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L704
.L707:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L708
.L711:
	movq	%rcx, -136(%rbp)
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L712
.L713:
	movq	%rcx, -136(%rbp)
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L714
.L705:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L706
.L697:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L698
.L701:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L702
.L709:
	movq	%rcx, -136(%rbp)
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L710
.L715:
	movq	%rcx, -136(%rbp)
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L716
.L1224:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L934
.L1227:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L922
.L1236:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L1003
.L1244:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L997
.L1253:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L976
.L1255:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L952
.L1257:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L964
.L1259:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L940
.L1260:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L937
.L1225:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L931
.L1229:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L928
.L1231:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L916
.L1233:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L982
.L1235:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L1006
.L1223:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L1009
.L1228:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L919
.L1226:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L898
.L1230:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L925
.L1234:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L979
.L1242:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L955
.L1238:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L907
.L1246:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L901
.L1237:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L910
.L1239:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L994
.L1241:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L958
.L1243:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L1000
.L1245:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L904
.L1247:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L988
.L1249:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L970
.L1251:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L946
.L1232:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L913
.L1252:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L943
.L1248:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L985
.L1256:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L949
.L1240:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L991
.L1254:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L973
.L1250:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L967
.L1258:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
	jmp	.L961
.L535:
	movq	%rcx, -136(%rbp)
	movl	%r15d, %esi
	call	*%rax
	movq	-136(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L536
.L532:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L533
.L462:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L463
.L421:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L422
.L466:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L467
.L411:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L412
.L417:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L418
.L472:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L473
.L407:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L408
.L438:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L439
.L405:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L406
.L480:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L481
.L464:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L465
.L448:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L449
.L482:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L483
.L478:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L479
.L456:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L457
.L436:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L437
.L450:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L451
.L415:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L416
.L419:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L420
.L425:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L426
.L454:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L455
.L442:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L443
.L434:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L435
.L409:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L410
.L470:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L471
.L444:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L445
.L440:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L441
.L446:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L447
.L474:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L475
.L427:
	movl	%r8d, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movl	-136(%rbp), %r8d
	movq	%rax, %rcx
	jmp	.L428
.L430:
	movl	%r8d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L431
.L458:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L459
.L432:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L433
.L413:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L414
.L423:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L424
.L402:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r8
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L403
.L468:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L469
.L476:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L477
.L460:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L461
.L452:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %esi
	call	*%rax
	movq	(%r12), %rdi
	movq	-136(%rbp), %rcx
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	jmp	.L453
.L584:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L585
.L587:
	movq	%rcx, -136(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movq	-136(%rbp), %rcx
	movq	%rax, %rdx
	jmp	.L588
.L1186:
	call	__stack_chk_fail@PLT
.L510:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L1020:
	movl	%edx, %r8d
	jmp	.L485
	.cfi_endproc
.LFE5171:
	.size	_ZN6disasm15DisassemblerX6414AVXInstructionEPh, .-_ZN6disasm15DisassemblerX6414AVXInstructionEPh
	.section	.rodata._ZN6disasm15DisassemblerX6414FPUInstructionEPh.str1.1,"aMS",@progbits,1
.LC246:
	.string	"fstp"
.LC247:
	.string	"ffree"
.LC248:
	.string	"fdivp"
.LC249:
	.string	"faddp"
.LC250:
	.string	"fld"
.LC251:
	.string	"fsub"
.LC252:
	.string	"fadd"
.LC253:
	.string	"fxch"
.LC254:
	.string	"fucomi"
.LC255:
	.string	"fmulp"
.LC256:
	.string	"fdiv"
.LC257:
	.string	"fmul"
.LC258:
	.string	"fsubp"
.LC259:
	.string	"fucomip"
.LC260:
	.string	"fninit"
.LC261:
	.string	"fclex"
.LC262:
	.string	"fabs"
.LC263:
	.string	"ftst"
.LC264:
	.string	"fld1"
.LC265:
	.string	"fldpi"
.LC266:
	.string	"fldln2"
.LC267:
	.string	"fldz"
.LC268:
	.string	"f2xm1"
.LC269:
	.string	"fyl2x"
.LC270:
	.string	"fptan"
.LC271:
	.string	"fprem1"
.LC272:
	.string	"fincstp"
.LC273:
	.string	"fprem"
.LC274:
	.string	"frndint"
.LC275:
	.string	"fscale"
.LC276:
	.string	"fsin"
.LC277:
	.string	"fcos"
.LC278:
	.string	"fnstsw_ax"
.LC279:
	.string	"fchs"
.LC280:
	.string	"fucompp"
.LC281:
	.string	"fcompp"
.LC282:
	.string	"fistp_s"
.LC283:
	.string	"fild_d"
.LC284:
	.string	"fstp_s"
.LC285:
	.string	"fstcw"
.LC286:
	.string	"fild_s"
.LC287:
	.string	"fld_s"
.LC288:
	.string	"fisttp_s"
.LC289:
	.string	"fstp_d"
.LC290:
	.string	"fist_s"
.LC291:
	.string	"fld_d"
.LC292:
	.string	"fistp_d"
.LC293:
	.string	"%s st%d"
.LC294:
	.string	"%s "
	.section	.text._ZN6disasm15DisassemblerX6414FPUInstructionEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6414FPUInstructionEPh
	.type	_ZN6disasm15DisassemblerX6414FPUInstructionEPh, @function
_ZN6disasm15DisassemblerX6414FPUInstructionEPh:
.LFB5172:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movzbl	1(%rsi), %ebx
	movzbl	(%rsi), %eax
	cmpb	$-65, %bl
	ja	.L1347
	shrl	$3, %ebx
	leaq	1(%rsi), %r12
	andl	$7, %ebx
	cmpb	$-35, %al
	je	.L1302
	ja	.L1303
	cmpb	$-39, %al
	je	.L1304
	cmpb	$-37, %al
	jne	.L1306
	cmpl	$2, %ebx
	je	.L1333
	jg	.L1309
	testl	%ebx, %ebx
	je	.L1334
	cmpl	$1, %ebx
	jne	.L1306
	leaq	.LC288(%rip), %rdx
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1303:
	cmpb	$-33, %al
	jne	.L1306
	cmpl	$5, %ebx
	je	.L1339
	leaq	.LC292(%rip), %rdx
	cmpl	$7, %ebx
	je	.L1308
	.p2align 4,,10
	.p2align 3
.L1306:
	cmpb	$0, 156(%r13)
	jne	.L1296
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	.LC61(%rip), %rdx
.L1308:
	movq	%r13, %rdi
	leaq	.LC294(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addq	$8, %rsp
	popq	%rbx
	addl	$1, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1347:
	.cfi_restore_state
	addl	$39, %eax
	cmpb	$6, %al
	ja	.L1265
	leaq	.L1267(%rip), %rdx
	movzbl	%al, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6414FPUInstructionEPh,"a",@progbits
	.align 4
	.align 4
.L1267:
	.long	.L1273-.L1267
	.long	.L1272-.L1267
	.long	.L1271-.L1267
	.long	.L1270-.L1267
	.long	.L1269-.L1267
	.long	.L1268-.L1267
	.long	.L1266-.L1267
	.section	.text._ZN6disasm15DisassemblerX6414FPUInstructionEPh
.L1268:
	cmpb	$-39, %bl
	je	.L1323
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$-24, %al
	je	.L1324
	ja	.L1300
	cmpb	$-64, %al
	je	.L1325
	cmpb	$-56, %al
	jne	.L1299
	leaq	.LC255(%rip), %rdx
	jmp	.L1276
.L1266:
	cmpb	$-32, %bl
	je	.L1328
	movl	%ebx, %eax
	leaq	.LC61(%rip), %rdx
	andl	$-8, %eax
	cmpb	$-24, %al
	jne	.L1275
	leaq	.LC259(%rip), %rdx
	jmp	.L1276
.L1273:
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$-64, %al
	je	.L1310
	cmpb	$-56, %al
	je	.L1311
	leal	32(%rbx), %edx
	cmpb	$-32, %bl
	jb	.L1277
	leaq	.L1279(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6414FPUInstructionEPh
	.align 4
	.align 4
.L1279:
	.long	.L1295-.L1279
	.long	.L1312-.L1279
	.long	.L1277-.L1279
	.long	.L1294-.L1279
	.long	.L1293-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1292-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1291-.L1279
	.long	.L1277-.L1279
	.long	.L1290-.L1279
	.long	.L1289-.L1279
	.long	.L1277-.L1279
	.long	.L1288-.L1279
	.long	.L1287-.L1279
	.long	.L1286-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1285-.L1279
	.long	.L1277-.L1279
	.long	.L1284-.L1279
	.long	.L1283-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1277-.L1279
	.long	.L1282-.L1279
	.long	.L1281-.L1279
	.long	.L1280-.L1279
	.long	.L1278-.L1279
	.section	.text._ZN6disasm15DisassemblerX6414FPUInstructionEPh
.L1272:
	leaq	.LC280(%rip), %rdx
	cmpb	$-23, %bl
	je	.L1275
.L1265:
	cmpb	$0, 156(%r13)
	jne	.L1296
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	.LC61(%rip), %rdx
	jmp	.L1275
.L1270:
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$-24, %al
	je	.L1317
	ja	.L1298
	cmpb	$-64, %al
	je	.L1318
	cmpb	$-56, %al
	jne	.L1299
	leaq	.LC257(%rip), %rdx
.L1276:
	movl	%ebx, %ecx
	movq	%r13, %rdi
	leaq	.LC293(%rip), %rsi
	xorl	%eax, %eax
	andl	$7, %ecx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	addq	$8, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1271:
	.cfi_restore_state
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$-24, %al
	je	.L1314
	cmpb	$-30, %bl
	je	.L1315
	leaq	.LC260(%rip), %rdx
	cmpb	$-29, %bl
	je	.L1275
	jmp	.L1265
.L1269:
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$-64, %al
	je	.L1321
	cmpb	$-40, %al
	je	.L1322
.L1299:
	cmpb	$0, 156(%r13)
	jne	.L1296
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	.LC61(%rip), %rdx
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1304:
	cmpl	$3, %ebx
	je	.L1330
	cmpl	$7, %ebx
	je	.L1331
	testl	%ebx, %ebx
	jne	.L1306
	leaq	.LC287(%rip), %rdx
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1302:
	testl	%ebx, %ebx
	je	.L1337
	cmpl	$3, %ebx
	jne	.L1306
	leaq	.LC289(%rip), %rdx
	jmp	.L1308
.L1312:
	leaq	.LC262(%rip), %rdx
.L1275:
	movq	%r13, %rdi
	xorl	%eax, %eax
	leaq	.LC18(%rip), %rsi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	addq	$8, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1295:
	.cfi_restore_state
	leaq	.LC279(%rip), %rdx
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1298:
	leaq	.LC256(%rip), %rdx
	cmpb	$-8, %al
	je	.L1276
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1309:
	leaq	.LC282(%rip), %rdx
	cmpl	$3, %ebx
	je	.L1308
	jmp	.L1306
.L1278:
	leaq	.LC277(%rip), %rdx
	jmp	.L1275
.L1280:
	leaq	.LC276(%rip), %rdx
	jmp	.L1275
.L1281:
	leaq	.LC275(%rip), %rdx
	jmp	.L1275
.L1282:
	leaq	.LC274(%rip), %rdx
	jmp	.L1275
.L1283:
	leaq	.LC273(%rip), %rdx
	jmp	.L1275
.L1284:
	leaq	.LC272(%rip), %rdx
	jmp	.L1275
.L1285:
	leaq	.LC271(%rip), %rdx
	jmp	.L1275
.L1286:
	leaq	.LC270(%rip), %rdx
	jmp	.L1275
.L1287:
	leaq	.LC269(%rip), %rdx
	jmp	.L1275
.L1288:
	leaq	.LC268(%rip), %rdx
	jmp	.L1275
.L1289:
	leaq	.LC267(%rip), %rdx
	jmp	.L1275
.L1290:
	leaq	.LC266(%rip), %rdx
	jmp	.L1275
.L1291:
	leaq	.LC265(%rip), %rdx
	jmp	.L1275
.L1292:
	leaq	.LC264(%rip), %rdx
	jmp	.L1275
.L1293:
	leaq	.LC263(%rip), %rdx
	jmp	.L1275
.L1294:
	leaq	.LC260(%rip), %rdx
	jmp	.L1275
.L1277:
	movq	%r13, %rdi
	call	_ZN6disasm15DisassemblerX6424UnimplementedInstructionEv
	leaq	.LC61(%rip), %rdx
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1311:
	leaq	.LC253(%rip), %rdx
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1322:
	leaq	.LC246(%rip), %rdx
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1318:
	leaq	.LC252(%rip), %rdx
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1333:
	leaq	.LC290(%rip), %rdx
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1328:
	leaq	.LC278(%rip), %rdx
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1323:
	leaq	.LC281(%rip), %rdx
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1314:
	leaq	.LC254(%rip), %rdx
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1310:
	leaq	.LC250(%rip), %rdx
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1321:
	leaq	.LC247(%rip), %rdx
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1317:
	leaq	.LC251(%rip), %rdx
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1334:
	leaq	.LC286(%rip), %rdx
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1300:
	leaq	.LC248(%rip), %rdx
	cmpb	$-8, %al
	je	.L1276
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1339:
	leaq	.LC283(%rip), %rdx
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1331:
	leaq	.LC285(%rip), %rdx
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1337:
	leaq	.LC291(%rip), %rdx
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1330:
	leaq	.LC284(%rip), %rdx
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1325:
	leaq	.LC249(%rip), %rdx
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1315:
	leaq	.LC261(%rip), %rdx
	jmp	.L1275
.L1324:
	leaq	.LC258(%rip), %rdx
	jmp	.L1276
.L1296:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5172:
	.size	_ZN6disasm15DisassemblerX6414FPUInstructionEPh, .-_ZN6disasm15DisassemblerX6414FPUInstructionEPh
	.section	.text._ZN6disasm15DisassemblerX6420MemoryFPUInstructionEiiPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6420MemoryFPUInstructionEiiPh
	.type	_ZN6disasm15DisassemblerX6420MemoryFPUInstructionEiiPh, @function
_ZN6disasm15DisassemblerX6420MemoryFPUInstructionEiiPh:
.LFB5173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	sarl	$3, %edx
	andl	$7, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rcx, %r12
	cmpl	$221, %esi
	je	.L1349
	jg	.L1350
	cmpl	$217, %esi
	je	.L1351
	cmpl	$219, %esi
	jne	.L1353
	cmpl	$2, %edx
	je	.L1361
	jg	.L1357
	testl	%edx, %edx
	je	.L1362
	cmpl	$1, %edx
	jne	.L1353
	leaq	.LC288(%rip), %r8
.L1355:
	movq	%r8, %rdx
	movq	%r13, %rdi
	leaq	.LC294(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	popq	%r12
	popq	%r13
	addl	$1, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1350:
	.cfi_restore_state
	cmpl	$223, %esi
	jne	.L1353
	cmpl	$5, %edx
	je	.L1367
	leaq	.LC292(%rip), %r8
	cmpl	$7, %edx
	je	.L1355
	.p2align 4,,10
	.p2align 3
.L1353:
	cmpb	$0, 156(%r13)
	jne	.L1370
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	.LC61(%rip), %r8
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1357:
	leaq	.LC282(%rip), %r8
	cmpl	$3, %edx
	je	.L1355
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1349:
	testl	%edx, %edx
	je	.L1365
	cmpl	$3, %edx
	jne	.L1353
	leaq	.LC289(%rip), %r8
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1351:
	cmpl	$3, %edx
	je	.L1358
	cmpl	$7, %edx
	je	.L1359
	testl	%edx, %edx
	jne	.L1353
	leaq	.LC287(%rip), %r8
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1362:
	leaq	.LC286(%rip), %r8
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1359:
	leaq	.LC285(%rip), %r8
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1365:
	leaq	.LC291(%rip), %r8
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1367:
	leaq	.LC283(%rip), %r8
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1358:
	leaq	.LC284(%rip), %r8
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1361:
	leaq	.LC290(%rip), %r8
	jmp	.L1355
.L1370:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5173:
	.size	_ZN6disasm15DisassemblerX6420MemoryFPUInstructionEiiPh, .-_ZN6disasm15DisassemblerX6420MemoryFPUInstructionEiiPh
	.section	.text._ZN6disasm15DisassemblerX6422RegisterFPUInstructionEih,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6422RegisterFPUInstructionEih
	.type	_ZN6disasm15DisassemblerX6422RegisterFPUInstructionEih, @function
_ZN6disasm15DisassemblerX6422RegisterFPUInstructionEih:
.LFB5174:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subl	$217, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	cmpl	$6, %esi
	ja	.L1372
	movl	%edx, %ebx
	leaq	.L1374(%rip), %rdx
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6422RegisterFPUInstructionEih,"a",@progbits
	.align 4
	.align 4
.L1374:
	.long	.L1380-.L1374
	.long	.L1379-.L1374
	.long	.L1378-.L1374
	.long	.L1377-.L1374
	.long	.L1376-.L1374
	.long	.L1375-.L1374
	.long	.L1373-.L1374
	.section	.text._ZN6disasm15DisassemblerX6422RegisterFPUInstructionEih
	.p2align 4,,10
	.p2align 3
.L1379:
	leaq	.LC280(%rip), %rdx
	cmpb	$-23, %bl
	je	.L1382
.L1372:
	cmpb	$0, 156(%rdi)
	jne	.L1402
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	movq	%rdi, -24(%rbp)
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	-24(%rbp), %rdi
	leaq	.LC61(%rip), %rdx
.L1382:
	xorl	%eax, %eax
	leaq	.LC18(%rip), %rsi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	addq	$24, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1375:
	.cfi_restore_state
	cmpb	$-39, %bl
	je	.L1421
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$-24, %al
	je	.L1422
	ja	.L1406
	cmpb	$-64, %al
	je	.L1423
	cmpb	$-56, %al
	jne	.L1405
	leaq	.LC255(%rip), %rdx
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1373:
	cmpb	$-32, %bl
	je	.L1426
	movl	%ebx, %eax
	leaq	.LC61(%rip), %rdx
	andl	$-8, %eax
	cmpb	$-24, %al
	jne	.L1382
	leaq	.LC259(%rip), %rdx
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1380:
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$-64, %al
	je	.L1408
	cmpb	$-56, %al
	jne	.L1432
	leaq	.LC253(%rip), %rdx
.L1383:
	movl	%ebx, %ecx
	xorl	%eax, %eax
	leaq	.LC293(%rip), %rsi
	andl	$7, %ecx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	addq	$24, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1377:
	.cfi_restore_state
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$-24, %al
	je	.L1415
	ja	.L1404
	cmpb	$-64, %al
	je	.L1416
	cmpb	$-56, %al
	jne	.L1405
	leaq	.LC257(%rip), %rdx
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1376:
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$-64, %al
	je	.L1419
	cmpb	$-40, %al
	jne	.L1405
	leaq	.LC246(%rip), %rdx
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1378:
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$-24, %al
	je	.L1412
	cmpb	$-30, %bl
	je	.L1413
	leaq	.LC260(%rip), %rdx
	cmpb	$-29, %bl
	je	.L1382
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1406:
	leaq	.LC248(%rip), %rdx
	cmpb	$-8, %al
	je	.L1383
.L1405:
	cmpb	$0, 156(%rdi)
	jne	.L1402
	leaq	.LC8(%rip), %rsi
	xorl	%eax, %eax
	movq	%rdi, -24(%rbp)
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	-24(%rbp), %rdi
	leaq	.LC61(%rip), %rdx
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1432:
	leal	32(%rbx), %edx
	cmpb	$-32, %bl
	jb	.L1372
	leaq	.L1385(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6422RegisterFPUInstructionEih
	.align 4
	.align 4
.L1385:
	.long	.L1401-.L1385
	.long	.L1410-.L1385
	.long	.L1372-.L1385
	.long	.L1400-.L1385
	.long	.L1399-.L1385
	.long	.L1372-.L1385
	.long	.L1372-.L1385
	.long	.L1372-.L1385
	.long	.L1398-.L1385
	.long	.L1372-.L1385
	.long	.L1372-.L1385
	.long	.L1397-.L1385
	.long	.L1372-.L1385
	.long	.L1396-.L1385
	.long	.L1395-.L1385
	.long	.L1372-.L1385
	.long	.L1394-.L1385
	.long	.L1393-.L1385
	.long	.L1392-.L1385
	.long	.L1372-.L1385
	.long	.L1372-.L1385
	.long	.L1391-.L1385
	.long	.L1372-.L1385
	.long	.L1390-.L1385
	.long	.L1389-.L1385
	.long	.L1372-.L1385
	.long	.L1372-.L1385
	.long	.L1372-.L1385
	.long	.L1388-.L1385
	.long	.L1387-.L1385
	.long	.L1386-.L1385
	.long	.L1384-.L1385
	.section	.text._ZN6disasm15DisassemblerX6422RegisterFPUInstructionEih
.L1410:
	leaq	.LC262(%rip), %rdx
	jmp	.L1382
.L1401:
	leaq	.LC279(%rip), %rdx
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1404:
	leaq	.LC256(%rip), %rdx
	cmpb	$-8, %al
	je	.L1383
	jmp	.L1405
.L1384:
	leaq	.LC277(%rip), %rdx
	jmp	.L1382
.L1386:
	leaq	.LC276(%rip), %rdx
	jmp	.L1382
.L1387:
	leaq	.LC275(%rip), %rdx
	jmp	.L1382
.L1388:
	leaq	.LC274(%rip), %rdx
	jmp	.L1382
.L1389:
	leaq	.LC273(%rip), %rdx
	jmp	.L1382
.L1390:
	leaq	.LC272(%rip), %rdx
	jmp	.L1382
.L1391:
	leaq	.LC271(%rip), %rdx
	jmp	.L1382
.L1392:
	leaq	.LC270(%rip), %rdx
	jmp	.L1382
.L1393:
	leaq	.LC269(%rip), %rdx
	jmp	.L1382
.L1394:
	leaq	.LC268(%rip), %rdx
	jmp	.L1382
.L1395:
	leaq	.LC267(%rip), %rdx
	jmp	.L1382
.L1396:
	leaq	.LC266(%rip), %rdx
	jmp	.L1382
.L1397:
	leaq	.LC265(%rip), %rdx
	jmp	.L1382
.L1398:
	leaq	.LC264(%rip), %rdx
	jmp	.L1382
.L1399:
	leaq	.LC263(%rip), %rdx
	jmp	.L1382
.L1400:
	leaq	.LC260(%rip), %rdx
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1423:
	leaq	.LC249(%rip), %rdx
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1413:
	leaq	.LC261(%rip), %rdx
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1416:
	leaq	.LC252(%rip), %rdx
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1412:
	leaq	.LC254(%rip), %rdx
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1426:
	leaq	.LC278(%rip), %rdx
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1421:
	leaq	.LC281(%rip), %rdx
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1408:
	leaq	.LC250(%rip), %rdx
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1419:
	leaq	.LC247(%rip), %rdx
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1415:
	leaq	.LC251(%rip), %rdx
	jmp	.L1383
	.p2align 4,,10
	.p2align 3
.L1422:
	leaq	.LC258(%rip), %rdx
	jmp	.L1383
.L1402:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.cfi_endproc
.LFE5174:
	.size	_ZN6disasm15DisassemblerX6422RegisterFPUInstructionEih, .-_ZN6disasm15DisassemblerX6422RegisterFPUInstructionEih
	.section	.rodata._ZN6disasm15DisassemblerX6424TwoByteOpcodeInstructionEPh.str1.1,"aMS",@progbits,1
.LC295:
	.string	"cmpxchg"
.LC296:
	.string	"bswap"
.LC297:
	.string	"cpuid"
.LC298:
	.string	"cvtsi2sd"
.LC299:
	.string	"bts"
.LC300:
	.string	"btr"
.LC301:
	.string	"psubd"
.LC302:
	.string	"psubw"
.LC303:
	.string	"psubb"
.LC304:
	.string	"pmuludq"
.LC305:
	.string	"psllq"
.LC306:
	.string	"pslld"
.LC307:
	.string	"psllw"
.LC308:
	.string	"pxor"
.LC309:
	.string	"pmaxsw"
.LC310:
	.string	"paddsw"
.LC311:
	.string	"paddsb"
.LC312:
	.string	"por"
.LC313:
	.string	"pminsw"
.LC314:
	.string	"psubsw"
.LC315:
	.string	"psubsb"
.LC316:
	.string	"psrad"
.LC317:
	.string	"psraw"
.LC318:
	.string	"pmaxub"
.LC319:
	.string	"paddusw"
.LC320:
	.string	"paddusb"
.LC321:
	.string	"pand"
.LC322:
	.string	"pminub"
.LC323:
	.string	"psubusw"
.LC324:
	.string	"psubusb"
.LC325:
	.string	"pmovmskb"
.LC326:
	.string	"pmullw"
.LC327:
	.string	"paddq"
.LC328:
	.string	"psrlw"
.LC329:
	.string	"pcmpeqd"
.LC330:
	.string	"pcmpeqw"
.LC331:
	.string	"pcmpeqb"
.LC332:
	.string	"comisd"
.LC333:
	.string	"ucomisd"
.LC334:
	.string	"punpckhqdq"
.LC335:
	.string	"punpcklqdq"
.LC336:
	.string	"packssdw"
.LC337:
	.string	"punpckhdq"
.LC338:
	.string	"punpckhwd"
.LC339:
	.string	"punpckhbw"
.LC340:
	.string	"packuswb"
.LC341:
	.string	"pcmpgtd"
.LC342:
	.string	"pcmpgtw"
.LC343:
	.string	"pcmpgtb"
.LC344:
	.string	"packsswb"
.LC345:
	.string	"punpckldq"
.LC346:
	.string	"punpcklwd"
.LC347:
	.string	"punpcklbw"
.LC348:
	.string	"maxpd"
.LC349:
	.string	"divpd"
.LC350:
	.string	"minpd"
.LC351:
	.string	"subpd"
.LC352:
	.string	"cvtps2dq"
.LC353:
	.string	"mulpd"
.LC354:
	.string	"addpd"
.LC355:
	.string	"xorpd"
.LC356:
	.string	"orpd"
.LC357:
	.string	"andnpd"
.LC358:
	.string	"andpd"
.LC359:
	.string	"paddw"
.LC360:
	.string	"paddb"
.LC361:
	.string	"psubq"
.LC362:
	.string	"paddd"
.LC363:
	.string	"bt"
.LC364:
	.string	"shrd"
.LC365:
	.string	"movzxb"
.LC366:
	.string	"movsxb"
.LC367:
	.string	"movzxw"
.LC368:
	.string	"nop"
.LC369:
	.string	"bsf"
.LC370:
	.string	"movsxw"
.LC371:
	.string	"bsr"
.LC372:
	.string	"shld"
.LC373:
	.string	"divss"
.LC374:
	.string	"subss"
.LC375:
	.string	"minss"
.LC376:
	.string	"cvtss2sd"
.LC377:
	.string	"mulss"
.LC378:
	.string	"addss"
.LC379:
	.string	"sqrtss"
.LC380:
	.string	"cvtsi2ss"
.LC381:
	.string	"maxss"
.LC382:
	.string	"divsd"
.LC383:
	.string	"subsd"
.LC384:
	.string	"minsd"
.LC385:
	.string	"cvtsd2ss"
.LC386:
	.string	"mulsd"
.LC387:
	.string	"addsd"
.LC388:
	.string	"sqrtsd"
.LC389:
	.string	"maxsd"
.LC390:
	.string	"pabsb %s,"
.LC391:
	.string	"pabsw %s,"
.LC392:
	.string	"pabsd %s,"
.LC393:
	.string	"phaddd %s,"
.LC394:
	.string	"phaddw %s,"
.LC395:
	.string	"pshufb %s,"
.LC396:
	.string	"psignb %s,"
.LC397:
	.string	"psignw %s,"
.LC398:
	.string	"psignd %s,"
.LC399:
	.string	"blendvpd %s,"
.LC400:
	.string	"pcmpeqq %s,"
.LC401:
	.string	"ptest %s,"
.LC402:
	.string	"pmovsxbw %s,"
.LC403:
	.string	"pmovsxwd %s,"
.LC404:
	.string	"packusdw %s,"
.LC405:
	.string	"pmovzxbw %s,"
.LC406:
	.string	"pmovzxwd %s,"
.LC407:
	.string	"pminsb %s,"
.LC408:
	.string	"pminsd %s,"
.LC409:
	.string	"pminuw %s,"
.LC410:
	.string	"pminud %s,"
.LC411:
	.string	"pmaxsb %s,"
.LC412:
	.string	"pmaxsd %s,"
.LC413:
	.string	"pmaxuw %s,"
.LC414:
	.string	"pmaxud %s,"
.LC415:
	.string	"pmulld %s,"
.LC416:
	.string	"pcmpgtq %s,"
.LC417:
	.string	"extractps "
.LC418:
	.string	",%s,%d"
.LC419:
	.string	"roundss %s,"
.LC420:
	.string	"roundsd %s,"
.LC421:
	.string	"pblendw %s,"
.LC422:
	.string	"palignr %s,"
.LC423:
	.string	"pextrb "
.LC424:
	.string	"pextrw "
.LC425:
	.string	"pextr%c "
.LC426:
	.string	"pinsrb "
.LC427:
	.string	" %s,"
.LC428:
	.string	"insertps %s,"
.LC429:
	.string	"pinsr%c "
.LC430:
	.string	"movupd %s,"
.LC431:
	.string	"movupd "
.LC432:
	.string	"movapd %s,"
.LC433:
	.string	"movapd "
.LC434:
	.string	"mov%c %s,"
.LC435:
	.string	"movdqa %s,"
.LC436:
	.string	"movdqa "
.LC437:
	.string	"movq "
.LC438:
	.string	"movmskpd %s,"
.LC439:
	.string	"pshufd %s,"
.LC440:
	.string	"ps%sw %s,%d"
.LC441:
	.string	"ps%sd %s,%d"
.LC442:
	.string	"ps%sq %s,%d"
.LC443:
	.string	"pinsrw %s,"
.LC444:
	.string	"psrld"
.LC445:
	.string	"%s %s,"
.LC446:
	.string	"movsd "
.LC447:
	.string	"movddup %s,"
.LC448:
	.string	"cvttsd2si%c %s,"
.LC449:
	.string	"cvtsd2si%c %s,"
.LC450:
	.string	"cvttps2dq%c %s,"
.LC451:
	.string	"pshuflw %s, "
.LC452:
	.string	"cmpeqsd"
.LC453:
	.string	"cmpltsd"
.LC454:
	.string	"cmplesd"
.LC455:
	.string	"cmpunordsd"
.LC456:
	.string	"cmpneqsd"
.LC457:
	.string	"cmpnltsd"
.LC458:
	.string	"cmpnlesd"
.LC459:
	.string	"cmpordsd"
.LC460:
	.string	"%s %s,%s"
.LC461:
	.string	"lddqu %s,"
.LC462:
	.string	"haddps %s,"
.LC463:
	.string	"movss "
.LC464:
	.string	"cvttss2si%c %s,"
.LC465:
	.string	"pshufhw %s, "
.LC466:
	.string	"movdqu %s,"
.LC467:
	.string	"movq %s,"
.LC468:
	.string	"popcnt%c %s,"
.LC469:
	.string	"tzcnt%c %s,"
.LC470:
	.string	"lzcnt%c %s,"
.LC471:
	.string	"cmpeqss"
.LC472:
	.string	"cmpltss"
.LC473:
	.string	"cmpless"
.LC474:
	.string	"cmpunordss"
.LC475:
	.string	"cmpneqss"
.LC476:
	.string	"cmpnltss"
.LC477:
	.string	"cmpnless"
.LC478:
	.string	"cmpordss"
.LC479:
	.string	"movups "
.LC480:
	.string	"movaps %s,"
.LC481:
	.string	"ucomiss %s,"
.LC482:
	.string	"sqrtps"
.LC483:
	.string	"rsqrtps"
.LC484:
	.string	"rcpps"
.LC485:
	.string	"andps"
.LC486:
	.string	"andnps"
.LC487:
	.string	"orps"
.LC488:
	.string	"xorps"
.LC489:
	.string	"addps"
.LC490:
	.string	"mulps"
.LC491:
	.string	"cvtps2pd"
.LC492:
	.string	"cvtdq2ps"
.LC493:
	.string	"subps"
.LC494:
	.string	"minps"
.LC495:
	.string	"divps"
.LC496:
	.string	"maxps"
.LC497:
	.string	"cmpps %s, "
.LC498:
	.string	", %s"
.LC499:
	.string	"shufps %s, "
.LC500:
	.string	"bswap%c %s"
.LC501:
	.string	"movmskps %s,"
.LC502:
	.string	"pshufw %s, "
.LC503:
	.string	",%s,cl"
.LC504:
	.string	"ud2"
.LC505:
	.string	"mfence"
.LC506:
	.string	"lfence"
.LC507:
	.string	"mov%c "
.LC508:
	.string	"movdqu "
.LC509:
	.string	"movaps "
.LC510:
	.string	"cmppd"
	.section	.text._ZN6disasm15DisassemblerX6424TwoByteOpcodeInstructionEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6424TwoByteOpcodeInstructionEPh
	.type	_ZN6disasm15DisassemblerX6424TwoByteOpcodeInstructionEPh, @function
_ZN6disasm15DisassemblerX6424TwoByteOpcodeInstructionEPh:
.LFB5175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	2(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 3, -56
	movzbl	1(%rsi), %ebx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	56(%rbx), %esi
	cmpb	$7, %sil
	jbe	.L1867
	leal	-31(%rbx), %eax
	cmpb	$-96, %al
	ja	.L1435
	leaq	.L1437(%rip), %rcx
	movzbl	%al, %eax
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6424TwoByteOpcodeInstructionEPh,"a",@progbits
	.align 4
	.align 4
.L1437:
	.long	.L1458-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1457-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1456-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1455-.L1437
	.long	.L1454-.L1437
	.long	.L1453-.L1437
	.long	.L1435-.L1437
	.long	.L1452-.L1437
	.long	.L1451-.L1437
	.long	.L1450-.L1437
	.long	.L1449-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1448-.L1437
	.long	.L1868-.L1437
	.long	.L1435-.L1437
	.long	.L1446-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1445-.L1437
	.long	.L1435-.L1437
	.long	.L1444-.L1437
	.long	.L1435-.L1437
	.long	.L1443-.L1437
	.long	.L1869-.L1437
	.long	.L1869-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1442-.L1437
	.long	.L1441-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1435-.L1437
	.long	.L1440-.L1437
	.long	.L1439-.L1437
	.long	.L1438-.L1437
	.long	.L1436-.L1437
	.section	.text._ZN6disasm15DisassemblerX6424TwoByteOpcodeInstructionEPh
.L1869:
	leaq	.LC295(%rip), %r15
.L1434:
	movzbl	158(%r12), %edi
	cmpb	$102, %dil
	je	.L2037
.L1462:
	movzbl	159(%r12), %eax
	leal	-16(%rbx), %edx
	cmpb	$-14, %al
	je	.L2038
.L1633:
	cmpb	$-13, %al
	je	.L2039
	cmpb	$1, %dl
	jbe	.L2040
	cmpb	$31, %bl
	je	.L2041
	cmpb	$40, %bl
	je	.L2042
	cmpb	$41, %bl
	je	.L2043
	cmpb	$46, %bl
	je	.L2044
	cmpb	$-94, %bl
	je	.L2045
	movl	%ebx, %eax
	andl	$-16, %eax
	cmpb	$64, %al
	je	.L2046
	leal	-81(%rbx), %ecx
	movzbl	%bl, %edx
	cmpb	$14, %cl
	ja	.L1741
	leaq	.LC483(%rip), %rax
	leaq	.LC482(%rip), %rcx
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	leaq	.LC484(%rip), %rcx
	punpcklqdq	%xmm1, %xmm0
	leaq	.LC485(%rip), %rax
	sarl	$3, %esi
	movq	%rax, %xmm2
	movaps	%xmm0, -176(%rbp)
	movq	%rcx, %xmm0
	andl	$7, %esi
	leaq	.LC487(%rip), %rax
	punpcklqdq	%xmm2, %xmm0
	leaq	.LC486(%rip), %rcx
	movq	%rax, %xmm3
	leaq	.LC489(%rip), %rax
	movaps	%xmm0, -160(%rbp)
	movq	%rcx, %xmm0
	movq	%rax, %xmm4
	leaq	.LC491(%rip), %rax
	punpcklqdq	%xmm3, %xmm0
	leaq	.LC488(%rip), %rcx
	movq	%rax, %xmm5
	leaq	.LC493(%rip), %rax
	movaps	%xmm0, -144(%rbp)
	movq	%rax, %xmm6
	movq	%rcx, %xmm0
	leaq	.LC495(%rip), %rax
	punpcklqdq	%xmm4, %xmm0
	leaq	.LC490(%rip), %rcx
	movq	%rax, %xmm7
	leaq	.LC496(%rip), %rax
	movaps	%xmm0, -128(%rbp)
	movq	%rcx, %xmm0
	leaq	.LC492(%rip), %rcx
	movq	%rax, -64(%rbp)
	movl	%esi, %eax
	punpcklqdq	%xmm5, %xmm0
	orl	$8, %eax
	movaps	%xmm0, -112(%rbp)
	movq	%rcx, %xmm0
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	punpcklqdq	%xmm6, %xmm0
	leaq	.LC494(%rip), %rcx
	movaps	%xmm0, -96(%rbp)
	movq	%rcx, %xmm0
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rcx
	movq	32(%rax), %rax
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -80(%rbp)
	cmpq	%rcx, %rax
	je	.L1858
	movl	%edx, -184(%rbp)
	call	*%rax
	movl	-184(%rbp), %edx
	movq	%rax, %rcx
.L1857:
	leal	-81(%rdx), %eax
	leaq	.LC445(%rip), %rsi
	movq	%r12, %rdi
	cltq
	movq	-176(%rbp,%rax,8), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1446:
	leaq	.LC372(%rip), %r15
.L1447:
	movzbl	158(%r12), %edi
	cmpb	$102, %dil
	jne	.L1462
.L2036:
	movzbl	2(%r13), %eax
.L1550:
	movzbl	157(%r12), %edi
	movl	%eax, %r15d
	movl	%eax, %esi
	shrl	$3, %r15d
	andl	$7, %r15d
	movl	%r15d, %ecx
	orl	$8, %ecx
	testb	$4, %dil
	cmovne	%ecx, %r15d
	andl	$7, %esi
	testb	$1, %dil
	jne	.L1588
	movzbl	%sil, %r9d
	cmpb	$31, %bl
	je	.L2047
.L1589:
	cmpb	$16, %bl
	je	.L2048
	cmpb	$17, %bl
	je	.L2049
	cmpb	$40, %bl
	je	.L2050
	cmpb	$41, %bl
	je	.L2051
	cmpb	$110, %bl
	je	.L2052
	cmpb	$111, %bl
	je	.L2053
	cmpb	$126, %bl
	je	.L2054
	cmpb	$127, %bl
	je	.L2055
	cmpb	$-42, %bl
	je	.L2056
	cmpb	$80, %bl
	je	.L2057
	cmpb	$112, %bl
	je	.L2058
	cmpb	$113, %bl
	je	.L2059
	cmpb	$114, %bl
	je	.L2060
	cmpb	$115, %bl
	je	.L2061
	cmpb	$-79, %bl
	je	.L2062
	cmpb	$-60, %bl
	je	.L2063
	movq	%rdx, -184(%rbp)
	cmpb	$84, %bl
	je	.L1886
	cmpb	$85, %bl
	je	.L1887
	cmpb	$86, %bl
	je	.L1888
	cmpb	$87, %bl
	je	.L1889
	cmpb	$88, %bl
	je	.L1890
	cmpb	$89, %bl
	je	.L1891
	cmpb	$91, %bl
	je	.L1892
	cmpb	$92, %bl
	je	.L1893
	cmpb	$93, %bl
	je	.L1894
	cmpb	$94, %bl
	je	.L1895
	cmpb	$95, %bl
	je	.L1896
	cmpb	$96, %bl
	je	.L1897
	cmpb	$97, %bl
	je	.L1898
	cmpb	$98, %bl
	je	.L1899
	cmpb	$99, %bl
	je	.L1900
	cmpb	$100, %bl
	je	.L1901
	cmpb	$101, %bl
	je	.L1902
	cmpb	$102, %bl
	je	.L1903
	cmpb	$103, %bl
	je	.L1904
	cmpb	$104, %bl
	je	.L1905
	cmpb	$105, %bl
	je	.L1906
	cmpb	$106, %bl
	je	.L1907
	cmpb	$107, %bl
	je	.L1908
	cmpb	$108, %bl
	je	.L1909
	cmpb	$109, %bl
	je	.L1910
	cmpb	$46, %bl
	je	.L1911
	cmpb	$47, %bl
	je	.L1912
	cmpb	$116, %bl
	je	.L1913
	cmpb	$117, %bl
	je	.L1914
	cmpb	$118, %bl
	je	.L1915
	cmpb	$-47, %bl
	je	.L1916
	leal	46(%rbx), %eax
	cmpb	$1, %al
	ja	.L2064
	movq	(%r12), %rdi
	movl	%r15d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC444(%rip), %rdx
	leaq	.LC445(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1868:
	leaq	.LC363(%rip), %r15
	jmp	.L1447
.L1435:
	movzbl	158(%r12), %edi
	xorl	%r15d, %r15d
	cmpb	$102, %dil
	jne	.L1462
.L2037:
	movzbl	2(%r13), %eax
	cmpb	$56, %bl
	jne	.L1463
	movzbl	3(%r13), %esi
	leaq	3(%r13), %rbx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %edx
	orl	$8, %edx
	testb	$4, 157(%r12)
	cmovne	%edx, %esi
	cmpb	$64, %al
	ja	.L1465
	leaq	.L1467(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6424TwoByteOpcodeInstructionEPh
	.align 4
	.align 4
.L1467:
	.long	.L1493-.L1467
	.long	.L1492-.L1467
	.long	.L1491-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1490-.L1467
	.long	.L1489-.L1467
	.long	.L1488-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1487-.L1467
	.long	.L1465-.L1467
	.long	.L1486-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1485-.L1467
	.long	.L1484-.L1467
	.long	.L1483-.L1467
	.long	.L1465-.L1467
	.long	.L1482-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1481-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1480-.L1467
	.long	.L1465-.L1467
	.long	.L1479-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1478-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1477-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1465-.L1467
	.long	.L1476-.L1467
	.long	.L1475-.L1467
	.long	.L1474-.L1467
	.long	.L1473-.L1467
	.long	.L1472-.L1467
	.long	.L1471-.L1467
	.long	.L1470-.L1467
	.long	.L1469-.L1467
	.long	.L1468-.L1467
	.long	.L1466-.L1467
	.section	.text._ZN6disasm15DisassemblerX6424TwoByteOpcodeInstructionEPh
	.p2align 4,,10
	.p2align 3
.L1867:
	leaq	.LC296(%rip), %r15
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L2040:
	movzbl	2(%r13), %r15d
	leaq	.LC479(%rip), %rsi
	movq	%r12, %rdi
	sarl	$3, %r15d
	andl	$7, %r15d
	movl	%r15d, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %r15d
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	cmpb	$17, %bl
	je	.L2065
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1724
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L1725:
	leaq	.LC114(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	.p2align 4,,10
	.p2align 3
.L1433:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2066
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2038:
	.cfi_restore_state
	cmpb	$1, %dl
	jbe	.L2067
	cmpb	$18, %bl
	je	.L2068
	cmpb	$42, %bl
	je	.L1865
	cmpb	$44, %bl
	je	.L2069
	cmpb	$45, %bl
	jne	.L1651
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1804
	call	*%rax
	movq	%rax, %rcx
.L1803:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L1654
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L1654
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L1654:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC449(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2039:
	cmpb	$1, %dl
	jbe	.L2070
	cmpb	$42, %bl
	je	.L2071
	cmpb	$44, %bl
	je	.L2072
	cmpb	$112, %bl
	je	.L2073
	cmpb	$111, %bl
	je	.L2074
	cmpb	$126, %bl
	je	.L2075
	cmpb	$127, %bl
	je	.L2076
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$88, %al
	je	.L1991
	cmpb	$81, %bl
	je	.L1991
	cmpb	$-72, %bl
	je	.L2077
	cmpb	$-68, %bl
	je	.L2078
	cmpb	$-67, %bl
	je	.L2079
	cmpb	$-62, %bl
	jne	.L1716
	movzbl	2(%r13), %ecx
	leaq	.LC471(%rip), %rbx
	movq	(%r12), %rdi
	movzbl	157(%r12), %eax
	movq	%rbx, %xmm0
	leaq	.LC473(%rip), %rbx
	movl	%ecx, %r14d
	andl	$7, %ecx
	leal	(%rax,%rax), %edx
	shrl	$3, %r14d
	sall	$3, %eax
	andl	$8, %edx
	andl	$7, %r14d
	andl	$8, %eax
	orl	%edx, %r14d
	leaq	.LC472(%rip), %rdx
	orl	%eax, %ecx
	movq	(%rdi), %rax
	movq	%rdx, %xmm6
	leaq	.LC474(%rip), %rdx
	movl	%ecx, %esi
	punpcklqdq	%xmm6, %xmm0
	movq	%rdx, %xmm7
	leaq	.LC476(%rip), %rdx
	movaps	%xmm0, -176(%rbp)
	movq	%rbx, %xmm0
	leaq	.LC475(%rip), %rbx
	movq	%rdx, %xmm1
	punpcklqdq	%xmm7, %xmm0
	leaq	.LC478(%rip), %rdx
	movaps	%xmm0, -160(%rbp)
	movq	%rbx, %xmm0
	leaq	.LC477(%rip), %rbx
	movq	%rdx, %xmm2
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -128(%rbp)
	call	*32(%rax)
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	%rax, %rbx
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%rbx, %r8
	leaq	.LC460(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movzbl	3(%r13), %eax
	movq	-176(%rbp,%rax,8), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$4, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1463:
	cmpb	$58, %bl
	jne	.L1550
	leaq	3(%r13), %rbx
	cmpb	$23, %al
	je	.L2080
	cmpb	$10, %al
	je	.L2081
	cmpb	$11, %al
	je	.L2082
	cmpb	$14, %al
	je	.L2083
	cmpb	$15, %al
	je	.L2084
	cmpb	$20, %al
	je	.L2085
	cmpb	$21, %al
	je	.L2086
	cmpb	$22, %al
	je	.L2087
	cmpb	$32, %al
	je	.L2088
	cmpb	$33, %al
	je	.L2089
	cmpb	$34, %al
	jne	.L1584
	movzbl	3(%r13), %r14d
	leaq	.LC429(%rip), %rsi
	movq	%r12, %rdi
	movzbl	157(%r12), %eax
	sarl	$3, %r14d
	leal	(%rax,%rax), %edx
	andl	$8, %eax
	andl	$8, %edx
	andl	$7, %r14d
	orl	%edx, %r14d
	cmpb	$1, %al
	sbbl	%edx, %edx
	xorl	%eax, %eax
	andl	$-13, %edx
	addl	$113, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC427(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%rbx, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC149(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %rbx
	xorl	%eax, %eax
	movzbl	(%rbx), %edx
	addq	$1, %rbx
	andl	$3, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%ebx, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1588:
	cmpb	$31, %bl
	je	.L1863
	orl	$8, %esi
	movzbl	%sil, %r9d
	jmp	.L1589
	.p2align 4,,10
	.p2align 3
.L2041:
	movzbl	2(%r13), %eax
	leaq	3(%r13), %r14
	movl	%eax, %edx
	shrb	$6, %dl
	testb	$1, 157(%r12)
	je	.L2090
.L1727:
	cmpb	$1, %dl
	je	.L2091
	leaq	4(%r14), %rax
	cmpb	$2, %dl
	cmove	%rax, %r14
.L1729:
	xorl	%eax, %eax
	leaq	.LC368(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2067:
	xorl	%eax, %eax
	leaq	.LC446(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movzbl	2(%r13), %eax
	sarl	$3, %eax
	andl	$7, %eax
	movl	%eax, %r15d
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %r15d
	cmpb	$17, %bl
	je	.L2092
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1639
	movslq	%r15d, %rax
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
.L1640:
	leaq	.LC114(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2068:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1795
	call	*%rax
	movq	%rax, %rdx
.L1794:
	leaq	.LC447(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1436:
	leaq	.LC370(%rip), %r15
	jmp	.L1447
.L1438:
	leaq	.LC366(%rip), %r15
	jmp	.L1447
.L1439:
	leaq	.LC371(%rip), %r15
	jmp	.L1447
.L1440:
	leaq	.LC369(%rip), %r15
	jmp	.L1447
.L1441:
	leaq	.LC367(%rip), %r15
	jmp	.L1447
.L1442:
	leaq	.LC365(%rip), %r15
	jmp	.L1447
.L1443:
	leaq	.LC37(%rip), %r15
	jmp	.L1447
.L1444:
	leaq	.LC364(%rip), %r15
	jmp	.L1447
.L1445:
	leaq	.LC299(%rip), %r15
	jmp	.L1447
.L1448:
	movzbl	158(%rdi), %edi
	cmpb	$102, %dil
	je	.L2036
	leaq	.LC297(%rip), %r15
	jmp	.L1462
.L1449:
	cmpb	$-14, 159(%rdi)
	leaq	.LC381(%rip), %r15
	je	.L2093
	.p2align 4,,10
	.p2align 3
.L1460:
	movzbl	158(%r12), %edi
	cmpb	$102, %dil
	je	.L2036
	movzbl	159(%r12), %eax
	leal	-16(%rbx), %edx
	jmp	.L1633
.L1450:
	cmpb	$-14, 159(%rdi)
	leaq	.LC373(%rip), %r15
	jne	.L1460
	leaq	.LC382(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L1461:
	cmpb	$102, 158(%r12)
	je	.L2036
.L1655:
	movl	%ebx, %eax
	andl	$-8, %eax
	cmpb	$88, %al
	je	.L1990
	cmpb	$81, %bl
	je	.L1990
	cmpb	$112, %bl
	je	.L2094
	cmpb	$-62, %bl
	je	.L2095
	cmpb	$-16, %bl
	je	.L2096
	cmpb	$124, %bl
	jne	.L1673
	movzbl	2(%r13), %esi
	xorl	%eax, %eax
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	testb	$4, 157(%r12)
	setne	%al
	sall	$3, %eax
	orl	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1822
	call	*%rax
	movq	%rax, %rdx
.L1821:
	leaq	.LC462(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1451:
	cmpb	$-14, 159(%rdi)
	leaq	.LC375(%rip), %r15
	jne	.L1460
	leaq	.LC384(%rip), %r15
	jmp	.L1461
.L1452:
	cmpb	$-14, 159(%rdi)
	leaq	.LC374(%rip), %r15
	jne	.L1460
	leaq	.LC383(%rip), %r15
	jmp	.L1461
.L1455:
	cmpb	$-14, 159(%rdi)
	leaq	.LC378(%rip), %r15
	jne	.L1460
	leaq	.LC387(%rip), %r15
	jmp	.L1461
.L1456:
	cmpb	$-14, 159(%rdi)
	leaq	.LC379(%rip), %r15
	jne	.L1460
	leaq	.LC388(%rip), %r15
	jmp	.L1461
.L1457:
	cmpb	$-14, 159(%rdi)
	leaq	.LC380(%rip), %r15
	jne	.L1460
	cmpb	$102, 158(%rdi)
	leaq	.LC298(%rip), %r15
	je	.L2036
	.p2align 4,,10
	.p2align 3
.L1865:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1798
	call	*%rax
	movq	%rax, %rcx
.L1797:
	movq	%r15, %rdx
	leaq	.LC445(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1458:
	leaq	.LC368(%rip), %r15
	jmp	.L1447
.L1453:
	cmpb	$-14, 159(%rdi)
	leaq	.LC376(%rip), %r15
	jne	.L1460
	leaq	.LC385(%rip), %r15
	jmp	.L1461
.L1454:
	cmpb	$-14, 159(%rdi)
	leaq	.LC377(%rip), %r15
	jne	.L1460
	leaq	.LC386(%rip), %r15
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1465:
	cmpb	$0, 156(%r12)
	jne	.L1675
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$3, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2090:
	andl	$7, %eax
	leaq	4(%r13), %rcx
	cmpb	$4, %al
	cmove	%rcx, %r14
	jmp	.L1727
	.p2align 4,,10
	.p2align 3
.L1863:
	leaq	3(%r13), %r14
.L1591:
	shrb	$6, %al
	cmpb	$1, %al
	je	.L2097
	leaq	4(%r14), %rdx
	cmpb	$2, %al
	cmove	%rdx, %r14
.L1593:
	xorl	%eax, %eax
	leaq	.LC368(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2047:
	xorl	%r14d, %r14d
	cmpb	$4, %sil
	sete	%r14b
	leaq	3(%r14,%r13), %r14
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L2043:
	movzbl	2(%r13), %eax
	leaq	.LC509(%rip), %rsi
	movq	%r12, %rdi
	sarl	$3, %eax
	andl	$7, %eax
	movl	%eax, %r15d
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %r15d
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1852
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
.L1851:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2072:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1828
	call	*%rax
	movq	%rax, %rcx
.L1827:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L1690
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L1690
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L1690:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC464(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2069:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1801
	call	*%rax
	movq	%rax, %rcx
.L1800:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L1650
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L1650
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L1650:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC448(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2091:
	addq	$1, %r14
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L2050:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1601
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L1602:
	leaq	.LC432(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2081:
	movzbl	3(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1780
	call	*%rax
	movq	%rax, %rdx
.L1779:
	leaq	.LC419(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	movslq	%eax, %r14
	xorl	%eax, %eax
	addq	%rbx, %r14
	movzbl	(%r14), %edx
	addq	$1, %r14
	andl	$3, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1795:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L2092:
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1637
	movslq	%r15d, %rax
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
.L1638:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1482:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1519
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1520:
	leaq	.LC402(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1486:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1517
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1518:
	leaq	.LC401(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1484:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1497
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1498:
	leaq	.LC391(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1488:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1511
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1512:
	leaq	.LC398(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1483:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1499
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1500:
	leaq	.LC392(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1487:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1513
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1514:
	leaq	.LC399(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1485:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1494
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1495:
	leaq	.LC390(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1489:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1509
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1510:
	leaq	.LC397(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1466:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1545
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1546:
	leaq	.LC415(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1474:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1531
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1532:
	leaq	.LC408(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1470:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1539
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1540:
	leaq	.LC412(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1478:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1525
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1526:
	leaq	.LC405(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1468:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1543
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1544:
	leaq	.LC414(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1476:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1547
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1548:
	leaq	.LC416(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1472:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1535
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1536:
	leaq	.LC410(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1480:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1515
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1516:
	leaq	.LC400(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1469:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1541
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1542:
	leaq	.LC413(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1475:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1529
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1530:
	leaq	.LC407(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1471:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1537
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1538:
	leaq	.LC411(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1479:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1523
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1524:
	leaq	.LC404(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1473:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1533
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1534:
	leaq	.LC409(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1477:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1527
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1528:
	leaq	.LC406(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1481:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1521
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1522:
	leaq	.LC403(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1490:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1507
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1508:
	leaq	.LC396(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1492:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1503
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1504:
	leaq	.LC394(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1491:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1501
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1502:
	leaq	.LC393(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1493:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1505
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1506:
	leaq	.LC395(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leal	(%rax,%rbx), %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2042:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1849
	call	*%rax
	movq	%rax, %rdx
.L1848:
	leaq	.LC480(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2070:
	xorl	%eax, %eax
	leaq	.LC463(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movzbl	2(%r13), %eax
	sarl	$3, %eax
	andl	$7, %eax
	movl	%eax, %r15d
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %r15d
	cmpb	$17, %bl
	je	.L2098
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1682
	movslq	%r15d, %rax
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
.L1683:
	leaq	.LC114(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2071:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1825
	call	*%rax
	movq	%rax, %rcx
.L1824:
	movq	%r15, %rdx
	leaq	.LC445(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1595
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L1596:
	leaq	.LC430(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2080:
	movzbl	3(%r13), %eax
	leaq	.LC417(%rip), %rsi
	movq	%r12, %rdi
	sarl	$3, %eax
	andl	$7, %eax
	movl	%eax, %r15d
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %r15d
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movslq	%eax, %r14
	movq	(%rdi), %rax
	addq	%rbx, %r14
	movzbl	(%r14), %ecx
	movq	32(%rax), %rax
	andl	$3, %ecx
	cmpq	%rdx, %rax
	jne	.L1553
	movslq	%r15d, %rax
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
.L1554:
	xorl	%eax, %eax
	leaq	.LC418(%rip), %rsi
	movq	%r12, %rdi
	addq	$1, %r14
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2065:
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1722
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L1723:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1724:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1725
	.p2align 4,,10
	.p2align 3
.L2097:
	addq	$1, %r14
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L2044:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1855
	call	*%rax
	movq	%rax, %rdx
.L1854:
	leaq	.LC481(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2073:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1831
	call	*%rax
	movq	%rax, %rdx
.L1830:
	leaq	.LC465(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC50(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r14
	xorl	%eax, %eax
	movzbl	(%r14), %edx
	addq	$1, %r14
	andl	$7, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2049:
	leaq	.LC431(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1598
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L1599:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1990:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1810
	call	*%rax
	movq	%rax, %rcx
.L1809:
	movq	%r15, %rdx
	leaq	.LC445(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2082:
	movzbl	3(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1783
	call	*%rax
	movq	%rax, %rdx
.L1782:
	leaq	.LC420(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%rbx, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %rbx
	xorl	%eax, %eax
	movzbl	(%rbx), %edx
	leaq	1(%rbx), %r14
	andl	$3, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2074:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1834
	call	*%rax
	movq	%rax, %rdx
.L1833:
	leaq	.LC466(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1651:
	cmpb	$91, %bl
	jne	.L1655
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1807
	call	*%rax
	movq	%rax, %rcx
.L1806:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L1658
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L1658
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L1658:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC450(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1849:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1848
	.p2align 4,,10
	.p2align 3
.L1741:
	cmpb	$-62, %bl
	je	.L2099
	cmpb	$-58, %bl
	je	.L2100
	cmpb	$7, %sil
	ja	.L1751
	leal	-200(%rdx), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1753
	leaq	.LC0(%rip), %rcx
	cmpl	$15, %esi
	ja	.L1754
	movslq	%esi, %rdx
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rdx,8), %rcx
.L1754:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L1755
	movl	$3, %eax
	testb	$8, 157(%r12)
	jne	.L1755
	xorl	%eax, %eax
	cmpb	$0, 158(%r12)
	sete	%al
	addl	$1, %eax
.L1755:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC500(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$2, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1825:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rcx
	jmp	.L1824
	.p2align 4,,10
	.p2align 3
.L1798:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rcx
	jmp	.L1797
.L2093:
	leaq	.LC389(%rip), %r15
	jmp	.L1461
.L2052:
	movq	(%r12), %r8
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%r8), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1607
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rcx
.L1608:
	andl	$8, %edi
	leaq	.LC434(%rip), %rsi
	cmpb	$1, %dil
	movq	%r12, %rdi
	sbbl	%edx, %edx
	xorl	%eax, %eax
	andl	$-13, %edx
	addl	$113, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1639:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1640
.L2098:
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1680
	movslq	%r15d, %rax
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
.L1681:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2083:
	movzbl	3(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1786
	call	*%rax
	movq	%rax, %rdx
.L1785:
	leaq	.LC421(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %rbx
	xorl	%eax, %eax
	movzbl	(%rbx), %edx
	leaq	1(%rbx), %r14
	andl	$3, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2075:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1837
	call	*%rax
	movq	%rax, %rdx
.L1836:
	leaq	.LC467(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2051:
	leaq	.LC433(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1604
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L1605:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1852:
	movslq	%r15d, %rax
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
	jmp	.L1851
.L1828:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rcx
	jmp	.L1827
.L1595:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1596
.L1801:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rcx
	jmp	.L1800
.L2045:
	xorl	%eax, %eax
	movq	%r15, %rdx
	leaq	.LC18(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$2, %eax
	jmp	.L1433
.L2053:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1611
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L1612:
	leaq	.LC435(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1682:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1683
.L2046:
	andl	$15, %ebx
	leaq	_ZN6disasmL17cmov_instructionsE(%rip), %rax
	movq	%r14, %rcx
	movq	%r12, %rdi
	leaq	(%rbx,%rbx,2), %rdx
	leaq	(%rax,%rdx,8), %rax
	movzbl	16(%rax), %edx
	movq	(%rax), %rsi
	movb	%dl, 163(%r12)
	movl	12(%rax), %edx
	call	_ZN6disasm15DisassemblerX6413PrintOperandsEPKcNS_11OperandTypeEPh
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1553:
	movl	%ecx, -184(%rbp)
	movl	%r15d, %esi
	call	*%rax
	movl	-184(%rbp), %ecx
	movq	%rax, %rdx
	jmp	.L1554
.L1991:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1843
	call	*%rax
	movq	%rax, %rcx
.L1842:
	movq	%r15, %rdx
	leaq	.LC445(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%eax, %r14d
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2084:
	movzbl	0(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1789
	call	*%rax
	movq	%rax, %rdx
.L1788:
	leaq	.LC422(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %rbx
	xorl	%eax, %eax
	movzbl	(%rbx), %edx
	leaq	1(%rbx), %r14
	andl	$3, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1722:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1723
.L2076:
	movzbl	2(%r13), %eax
	leaq	.LC508(%rip), %rsi
	movq	%r12, %rdi
	sarl	$3, %eax
	andl	$7, %eax
	movl	%eax, %r15d
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %r15d
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1840
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
.L1839:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1780:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1779
.L1855:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1854
.L1804:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rcx
	jmp	.L1803
.L1831:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1830
.L2054:
	andl	$8, %edi
	leaq	.LC507(%rip), %rsi
	cmpb	$1, %dil
	movq	%r12, %rdi
	sbbl	%edx, %edx
	xorl	%eax, %eax
	andl	$-13, %edx
	addl	$113, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1792
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
.L1791:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2099:
	movzbl	2(%r13), %esi
	leaq	.LC159(%rip), %rcx
	movq	(%r12), %rdi
	leaq	.LC161(%rip), %rbx
	movq	%rcx, %xmm0
	leaq	.LC163(%rip), %rcx
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	leaq	.LC160(%rip), %rax
	movq	%rax, %xmm1
	leaq	.LC162(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, %xmm2
	leaq	.LC164(%rip), %rax
	movaps	%xmm0, -176(%rbp)
	movq	%rbx, %xmm0
	movq	%rax, %xmm3
	leaq	.LC166(%rip), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, %xmm4
	movq	(%rdi), %rax
	leaq	.LC165(%rip), %rbx
	movaps	%xmm0, -160(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movq	32(%rax), %rax
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -128(%rbp)
	cmpq	%rdx, %rax
	jne	.L1746
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L1747:
	leaq	.LC497(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC498(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r14
	movzbl	(%r14), %eax
	addq	$1, %r14
	movq	-176(%rbp,%rax,8), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1601:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1602
.L2085:
	movzbl	3(%r13), %r14d
	leaq	.LC423(%rip), %rsi
	movq	%r12, %rdi
	sarl	$3, %r14d
	andl	$7, %r14d
	movl	%r14d, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %r14d
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %rbx
	movq	(%rdi), %rax
	movzbl	(%rbx), %ecx
	movq	32(%rax), %rax
	andl	$3, %ecx
	cmpq	%rdx, %rax
	jne	.L1569
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L1570:
	xorl	%eax, %eax
	leaq	.LC418(%rip), %rsi
	leaq	1(%rbx), %r14
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1810:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rcx
	jmp	.L1809
.L1783:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1782
.L2094:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1813
	call	*%rax
	movq	%rax, %rdx
.L1812:
	leaq	.LC451(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC50(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r14
	xorl	%eax, %eax
	movzbl	(%r14), %edx
	addq	$1, %r14
	andl	$7, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1834:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1833
.L1807:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rcx
	jmp	.L1806
.L1858:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rcx
	jmp	.L1857
.L1598:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1599
.L1637:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1638
.L2100:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	movl	%esi, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1861
	call	*%rax
	movq	%rax, %rdx
.L1860:
	leaq	.LC499(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC50(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r14
	xorl	%eax, %eax
	movzbl	(%r14), %edx
	addq	$1, %r14
	andl	$3, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1751:
	cmpb	$80, %bl
	je	.L2101
	cmpb	$112, %bl
	je	.L2102
	cmpb	$-128, %al
	je	.L2103
	movl	%ebx, %edx
	andl	$-10, %edx
	cmpb	$-74, %dl
	je	.L1761
	cmpb	$-81, %bl
	je	.L1761
	cmpb	$-112, %al
	je	.L2104
	movl	%ebx, %eax
	andl	$-9, %eax
	addl	$93, %eax
	testb	$-3, %al
	je	.L1764
	cmpb	$-70, %bl
	je	.L2105
	leal	68(%rbx), %eax
	cmpb	$1, %al
	jbe	.L1992
	cmpb	$-72, %bl
	je	.L1992
	cmpb	$11, %bl
	je	.L2106
	leal	80(%rbx), %eax
	cmpb	$1, %al
	jbe	.L2107
	cmpb	$-82, %bl
	je	.L2108
.L1777:
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6424UnimplementedInstructionEv
	movl	$2, %eax
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L2086:
	movzbl	3(%r13), %r14d
	leaq	.LC424(%rip), %rsi
	movq	%r12, %rdi
	sarl	$3, %r14d
	andl	$7, %r14d
	movl	%r14d, %eax
	orl	$8, %eax
	testb	$4, 157(%r12)
	cmovne	%eax, %r14d
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%rbx, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %rbx
	movq	(%rdi), %rax
	movzbl	(%rbx), %r15d
	movq	32(%rax), %rax
	andl	$7, %r15d
	cmpq	%rdx, %rax
	jne	.L1573
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rdx
.L1574:
	xorl	%eax, %eax
	movl	%r15d, %ecx
	leaq	.LC418(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	1(%rbx), %r14
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2095:
	movzbl	2(%r13), %eax
	leaq	.LC454(%rip), %rbx
	movq	(%r12), %rdi
	movzbl	157(%r12), %edx
	movl	%eax, %r14d
	shrl	$3, %r14d
	andl	$7, %r14d
	movl	%r14d, %ecx
	orl	$8, %ecx
	testb	$4, %dl
	cmovne	%ecx, %r14d
	andl	$7, %eax
	movl	%eax, %ecx
	orl	$8, %ecx
	andl	$1, %edx
	cmovne	%ecx, %eax
	leaq	.LC452(%rip), %rcx
	movq	%rcx, %xmm0
	leaq	.LC456(%rip), %rcx
	movzbl	%al, %esi
	leaq	.LC453(%rip), %rax
	movq	%rax, %xmm5
	leaq	.LC455(%rip), %rax
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, %xmm6
	leaq	.LC457(%rip), %rax
	movaps	%xmm0, -176(%rbp)
	movq	%rbx, %xmm0
	movq	%rax, %xmm7
	leaq	.LC459(%rip), %rax
	punpcklqdq	%xmm6, %xmm0
	movq	%rax, %xmm5
	movq	(%rdi), %rax
	leaq	.LC458(%rip), %rbx
	movaps	%xmm0, -160(%rbp)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movq	32(%rax), %rdx
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rbx
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -128(%rbp)
	cmpq	%rbx, %rdx
	jne	.L1669
	movslq	%esi, %rax
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rcx
	movq	(%rcx,%rax,8), %r15
.L1670:
	cmpq	%rbx, %rdx
	je	.L2109
	movl	%r14d, %esi
	call	*%rdx
	movq	%rax, %rcx
.L1815:
	movzbl	3(%r13), %eax
	movq	%r15, %r8
	leaq	.LC460(%rip), %rsi
	movq	%r12, %rdi
	movq	-176(%rbp,%rax,8), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$4, %eax
	jmp	.L1433
.L1786:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1785
.L1837:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1836
.L1607:
	movq	%r8, %rdi
	movl	%r15d, %esi
	call	*%rax
	movzbl	157(%r12), %edi
	movq	%rax, %rcx
	jmp	.L1608
.L2057:
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1622
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L1623:
	leaq	.LC438(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2055:
	leaq	.LC436(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1616
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L1617:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1680:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1681
.L1604:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1605
.L2096:
	movzbl	2(%r13), %esi
	xorl	%eax, %eax
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	testb	$4, 157(%r12)
	setne	%al
	sall	$3, %eax
	orl	%eax, %esi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1819
	call	*%rax
	movq	%rax, %rdx
.L1818:
	leaq	.LC461(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1843:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rcx
	jmp	.L1842
.L2087:
	movzbl	3(%r13), %eax
	xorl	%r14d, %r14d
	movq	%r12, %rdi
	movzbl	157(%r12), %edx
	leaq	.LC425(%rip), %rsi
	sarl	$3, %eax
	andl	$7, %eax
	testb	$4, %dl
	setne	%r14b
	andl	$8, %edx
	sall	$3, %r14d
	orl	%eax, %r14d
	cmpb	$1, %dl
	sbbl	%edx, %edx
	xorl	%eax, %eax
	andl	$-13, %edx
	addl	$113, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%rbx, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %rbx
	movq	(%rdi), %rax
	movzbl	(%rbx), %r15d
	movq	32(%rax), %rax
	andl	$3, %r15d
	cmpq	%rdx, %rax
	jne	.L1578
	movslq	%r14d, %rax
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
.L1579:
	xorl	%eax, %eax
	movl	%r15d, %ecx
	leaq	.LC418(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	addq	$1, %rbx
	movl	%ebx, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2077:
	movzbl	2(%r13), %esi
	xorl	%eax, %eax
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	sarl	$3, %esi
	andl	$7, %esi
	testb	$4, 157(%r12)
	setne	%al
	sall	$3, %eax
	orl	%eax, %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1846
	call	*%rax
	movq	%rax, %rcx
.L1845:
	xorl	%eax, %eax
	cmpb	$0, 163(%r12)
	jne	.L1709
	testb	$8, 157(%r12)
	jne	.L1964
	cmpb	$1, 158(%r12)
	sbbl	%eax, %eax
	notl	%eax
	addl	$2, %eax
.L1709:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC468(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1840:
	movslq	%r15d, %rax
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
	jmp	.L1839
.L1789:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1788
.L1673:
	cmpb	$0, 156(%r12)
	jne	.L1675
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$2, %eax
	jmp	.L1433
.L2056:
	leaq	.LC437(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi(%rip), %rdx
	cltq
	addq	%rax, %r14
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1619
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
.L1620:
	xorl	%eax, %eax
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1611:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1612
.L1792:
	movslq	%r15d, %r15
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r15,8), %rdx
	jmp	.L1791
.L2058:
	movq	(%r12), %rdi
	movl	%r15d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC439(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r14
	xorl	%eax, %eax
	movzbl	(%r14), %edx
	addq	$1, %r14
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2078:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	movzbl	157(%r12), %eax
	sarl	$3, %esi
	addl	%eax, %eax
	andl	$7, %esi
	andl	$8, %eax
	orl	%eax, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	cmpb	$0, 163(%r12)
	movq	%rax, %rcx
	jne	.L1967
	testb	$8, 157(%r12)
	jne	.L1968
	cmpb	$1, 158(%r12)
	sbbl	%eax, %eax
	notl	%eax
	addl	$2, %eax
.L1712:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC469(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2088:
	movzbl	3(%r13), %r14d
	leaq	.LC426(%rip), %rsi
	movq	%r12, %rdi
	movzbl	157(%r12), %eax
	sarl	$3, %r14d
	addl	%eax, %eax
	andl	$8, %eax
	andl	$7, %r14d
	orl	%eax, %r14d
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	(%r12), %rdi
	movl	%r14d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC427(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%rbx, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC149(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %rbx
	xorl	%eax, %eax
	movzbl	(%rbx), %edx
	addq	$1, %rbx
	andl	$3, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%ebx, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1813:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1812
.L1519:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1520
.L1499:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1500
.L1497:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1498
.L1494:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1495
.L1545:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1546
.L1541:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1542
.L1543:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1544
.L1533:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1534
.L1539:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1540
.L1537:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1538
.L1535:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1536
.L1527:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1528
.L1531:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1532
.L1529:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1530
.L1547:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1548
.L1523:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1524
.L1525:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1526
.L1515:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1516
.L1509:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1510
.L1505:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1506
.L1511:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1512
.L2101:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	movzbl	157(%r12), %eax
	sarl	$3, %esi
	addl	%eax, %eax
	andl	$7, %esi
	andl	$8, %eax
	orl	%eax, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	leaq	.LC501(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1521:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1522
.L1503:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1504
.L1507:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1508
.L1501:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1502
.L1517:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1518
.L1513:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1514
.L1569:
	movl	%ecx, -184(%rbp)
	movl	%r14d, %esi
	call	*%rax
	movl	-184(%rbp), %ecx
	movq	%rax, %rdx
	jmp	.L1570
.L1746:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1747
.L2059:
	movq	(%r12), %rdi
	movl	%r9d, %esi
	movzbl	3(%r13), %ebx
	movq	(%rdi), %rax
	andl	$127, %ebx
	call	*32(%rax)
	leaq	_ZN6disasm6sf_strE(%rip), %rdx
	movl	%ebx, %r8d
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	%r15d, %eax
	leaq	.LC440(%rip), %rsi
	sarl	%eax
	cltq
	movq	(%rdx,%rax,8), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$4, %eax
	jmp	.L1433
.L2089:
	movzbl	3(%r13), %esi
	movq	(%r12), %rdi
	movzbl	157(%r12), %eax
	sarl	$3, %esi
	addl	%eax, %eax
	andl	$7, %esi
	andl	$8, %eax
	orl	%eax, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC428(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%rbx, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %rbx
	xorl	%eax, %eax
	movzbl	(%rbx), %edx
	addq	$1, %rbx
	andl	$3, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%ebx, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1861:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1860
.L2079:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	movzbl	157(%r12), %eax
	sarl	$3, %esi
	addl	%eax, %eax
	andl	$7, %esi
	andl	$8, %eax
	orl	%eax, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	cmpb	$0, 163(%r12)
	movq	%rax, %rcx
	jne	.L1971
	testb	$8, 157(%r12)
	jne	.L1972
	cmpb	$1, 158(%r12)
	sbbl	%eax, %eax
	notl	%eax
	addl	$2, %eax
.L1715:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC470(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2109:
	movslq	%r14d, %r14
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%r14,8), %rcx
	jmp	.L1815
.L1716:
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6424UnimplementedInstructionEv
	movl	$2, %eax
	jmp	.L1433
.L1584:
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6424UnimplementedInstructionEv
	movl	$3, %eax
	jmp	.L1433
.L2102:
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	movzbl	157(%r12), %eax
	sarl	$3, %esi
	addl	%eax, %eax
	andl	$7, %esi
	andl	$8, %eax
	orl	%eax, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC502(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC50(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r14
	xorl	%eax, %eax
	movzbl	(%r14), %edx
	addq	$1, %r14
	andl	$3, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1761:
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6413PrintOperandsEPKcNS_11OperandTypeEPh
	addl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2060:
	movq	(%r12), %rdi
	movl	%r9d, %esi
	movzbl	3(%r13), %ebx
	movq	(%rdi), %rax
	andl	$127, %ebx
	call	*32(%rax)
	leaq	_ZN6disasm6sf_strE(%rip), %rdx
	movl	%ebx, %r8d
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	%r15d, %eax
	leaq	.LC441(%rip), %rsi
	sarl	%eax
	cltq
	movq	(%rdx,%rax,8), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$4, %eax
	jmp	.L1433
.L1669:
	call	*%rdx
	movq	(%r12), %rdi
	movq	%rax, %r15
	movq	(%rdi), %rax
	movq	32(%rax), %rdx
	jmp	.L1670
.L1846:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rcx
	jmp	.L1845
.L2066:
	call	__stack_chk_fail@PLT
.L1573:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1574
.L1822:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1821
.L1819:
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8xmm_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
	jmp	.L1818
.L1753:
	call	*%rax
	movq	%rax, %rcx
	jmp	.L1754
.L1967:
	xorl	%eax, %eax
	jmp	.L1712
.L1578:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1579
.L1622:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1623
.L1616:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1617
.L1619:
	movl	%r15d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L1620
.L2061:
	movq	(%r12), %rdi
	movl	%r9d, %esi
	movzbl	3(%r13), %ebx
	movq	(%rdi), %rax
	andl	$127, %ebx
	call	*32(%rax)
	leaq	_ZN6disasm6sf_strE(%rip), %rdx
	movl	%ebx, %r8d
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	%r15d, %eax
	leaq	.LC442(%rip), %rsi
	sarl	%eax
	cltq
	movq	(%rdx,%rax,8), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$4, %eax
	jmp	.L1433
.L2103:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6415JumpConditionalEPh
	jmp	.L1433
.L1971:
	xorl	%eax, %eax
	jmp	.L1715
.L2063:
	movq	(%r12), %rdi
	movl	%r15d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC443(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r14
	xorl	%eax, %eax
	movzbl	(%r14), %edx
	addq	$1, %r14
	andl	$7, %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1964:
	movl	$3, %eax
	jmp	.L1709
.L1972:
	movl	$3, %eax
	jmp	.L1715
.L1886:
	leaq	.LC358(%rip), %rdx
.L1630:
	movq	(%r12), %rdi
	movq	%rdx, -184(%rbp)
	movl	%r15d, %esi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-184(%rbp), %rdx
	leaq	.LC445(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1887:
	leaq	.LC357(%rip), %rdx
	jmp	.L1630
.L2062:
	movq	%r14, %rcx
	movl	$2, %edx
	leaq	.LC295(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6413PrintOperandsEPKcNS_11OperandTypeEPh
	addl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1968:
	movl	$3, %eax
	jmp	.L1712
.L1764:
	movq	%r15, %rdx
	leaq	.LC294(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movzbl	2(%r13), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movzbl	157(%r12), %eax
	xorl	%ecx, %ecx
	sarl	$3, %edx
	addl	%eax, %eax
	andl	$7, %edx
	andl	$8, %eax
	orl	%eax, %edx
	movl	%edx, %r15d
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%r12), %rdi
	movl	%r15d, %esi
	cltq
	addq	%rax, %r14
	movq	(%rdi), %rax
	cmpb	$-85, %bl
	je	.L2110
	call	*16(%rax)
	leaq	.LC503(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2104:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX645SetCCEPh
	jmp	.L1433
.L2110:
	call	*16(%rax)
	leaq	.LC23(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1675:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2064:
	cmpb	$-44, %bl
	je	.L1917
	cmpb	$-43, %bl
	je	.L1918
	cmpb	$-41, %bl
	je	.L1919
	cmpb	$-40, %bl
	je	.L1920
	cmpb	$-39, %bl
	je	.L1921
	cmpb	$-38, %bl
	je	.L1922
	cmpb	$-37, %bl
	je	.L1923
	cmpb	$-36, %bl
	je	.L1924
	cmpb	$-35, %bl
	je	.L1925
	cmpb	$-34, %bl
	je	.L1926
	cmpb	$-31, %bl
	je	.L1927
	cmpb	$-30, %bl
	je	.L1928
	cmpb	$-24, %bl
	je	.L1929
	cmpb	$-23, %bl
	je	.L1930
	cmpb	$-22, %bl
	je	.L1931
	cmpb	$-21, %bl
	je	.L1932
	cmpb	$-20, %bl
	je	.L1933
	cmpb	$-19, %bl
	je	.L1934
	cmpb	$-18, %bl
	je	.L1935
	cmpb	$-17, %bl
	je	.L1936
	cmpb	$-15, %bl
	je	.L1937
	cmpb	$-14, %bl
	je	.L1938
	cmpb	$-13, %bl
	je	.L1939
	cmpb	$-12, %bl
	je	.L1940
	cmpb	$-8, %bl
	je	.L1941
	cmpb	$-7, %bl
	je	.L1942
	cmpb	$-6, %bl
	je	.L1943
	cmpb	$-5, %bl
	je	.L1944
	cmpb	$-4, %bl
	je	.L1945
	cmpb	$-3, %bl
	je	.L1946
	cmpb	$-2, %bl
	je	.L1947
	cmpb	$-62, %bl
	je	.L1632
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6424UnimplementedInstructionEv
	movq	-184(%rbp), %rdx
	jmp	.L1630
.L1632:
	movq	(%r12), %rdi
	movl	%r15d, %esi
	leaq	.LC165(%rip), %rbx
	movq	(%rdi), %rax
	call	*32(%rax)
	leaq	.LC510(%rip), %rdx
	leaq	.LC445(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfXMMRegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC159(%rip), %rcx
	leaq	.LC168(%rip), %rsi
	movq	%r12, %rdi
	cltq
	movq	%rcx, %xmm0
	leaq	.LC161(%rip), %rcx
	addq	%rax, %r14
	leaq	.LC160(%rip), %rax
	movq	%rax, %xmm3
	leaq	.LC162(%rip), %rax
	addq	$1, %r14
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, %xmm4
	leaq	.LC164(%rip), %rax
	movaps	%xmm0, -176(%rbp)
	movq	%rcx, %xmm0
	leaq	.LC163(%rip), %rcx
	movq	%rax, %xmm6
	punpcklqdq	%xmm4, %xmm0
	leaq	.LC166(%rip), %rax
	movaps	%xmm0, -160(%rbp)
	movq	%rcx, %xmm0
	movq	%rax, %xmm7
	movzbl	-1(%r14), %eax
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -144(%rbp)
	movq	%rbx, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	-176(%rbp,%rax,8), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1947:
	leaq	.LC362(%rip), %rdx
	jmp	.L1630
.L1946:
	leaq	.LC359(%rip), %rdx
	jmp	.L1630
.L1945:
	leaq	.LC360(%rip), %rdx
	jmp	.L1630
.L1944:
	leaq	.LC361(%rip), %rdx
	jmp	.L1630
.L1943:
	leaq	.LC301(%rip), %rdx
	jmp	.L1630
.L1942:
	leaq	.LC302(%rip), %rdx
	jmp	.L1630
.L1941:
	leaq	.LC303(%rip), %rdx
	jmp	.L1630
.L1940:
	leaq	.LC304(%rip), %rdx
	jmp	.L1630
.L1939:
	leaq	.LC305(%rip), %rdx
	jmp	.L1630
.L1938:
	leaq	.LC306(%rip), %rdx
	jmp	.L1630
.L1937:
	leaq	.LC307(%rip), %rdx
	jmp	.L1630
.L1936:
	leaq	.LC308(%rip), %rdx
	jmp	.L1630
.L1935:
	leaq	.LC309(%rip), %rdx
	jmp	.L1630
.L1934:
	leaq	.LC310(%rip), %rdx
	jmp	.L1630
.L1933:
	leaq	.LC311(%rip), %rdx
	jmp	.L1630
.L1932:
	leaq	.LC312(%rip), %rdx
	jmp	.L1630
.L1931:
	leaq	.LC313(%rip), %rdx
	jmp	.L1630
.L1930:
	leaq	.LC314(%rip), %rdx
	jmp	.L1630
.L1929:
	leaq	.LC315(%rip), %rdx
	jmp	.L1630
.L1928:
	leaq	.LC316(%rip), %rdx
	jmp	.L1630
.L1927:
	leaq	.LC317(%rip), %rdx
	jmp	.L1630
.L1926:
	leaq	.LC318(%rip), %rdx
	jmp	.L1630
.L1925:
	leaq	.LC319(%rip), %rdx
	jmp	.L1630
.L1924:
	leaq	.LC320(%rip), %rdx
	jmp	.L1630
.L1923:
	leaq	.LC321(%rip), %rdx
	jmp	.L1630
.L1922:
	leaq	.LC322(%rip), %rdx
	jmp	.L1630
.L1921:
	leaq	.LC323(%rip), %rdx
	jmp	.L1630
.L1920:
	leaq	.LC324(%rip), %rdx
	jmp	.L1630
.L1919:
	leaq	.LC325(%rip), %rdx
	jmp	.L1630
.L1918:
	leaq	.LC326(%rip), %rdx
	jmp	.L1630
.L1917:
	leaq	.LC327(%rip), %rdx
	jmp	.L1630
.L1992:
	cmpb	$0, 163(%r12)
	jne	.L1986
	testb	$8, 157(%r12)
	jne	.L1987
	cmpb	$1, %dil
	sbbl	%eax, %eax
	notl	%eax
	addl	$2, %eax
.L1772:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC22(%rip), %rsi
	movq	%r12, %rdi
	movsbl	(%rdx,%rax), %ecx
	movq	%r15, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movzbl	2(%r13), %esi
	movq	(%r12), %rdi
	movzbl	157(%r12), %eax
	sarl	$3, %esi
	addl	%eax, %eax
	andl	$7, %esi
	andl	$8, %eax
	orl	%eax, %esi
	movq	(%rdi), %rax
	call	*16(%rax)
	leaq	.LC114(%rip), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	addl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2105:
	movzbl	2(%r13), %eax
	leaq	.LC61(%rip), %rdx
	testb	$4, 157(%r12)
	jne	.L1769
	shrl	$3, %eax
	leaq	.LC299(%rip), %rdx
	andl	$7, %eax
	cmpl	$5, %eax
	je	.L1769
	cmpl	$6, %eax
	leaq	.LC61(%rip), %rdx
	leaq	.LC300(%rip), %rax
	cmove	%rax, %rdx
.L1769:
	leaq	.LC294(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r14, %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC149(%rip), %rsi
	movq	%r12, %rdi
	cltq
	addq	%rax, %r14
	xorl	%eax, %eax
	movzbl	(%r14), %edx
	addq	$1, %r14
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L1987:
	movl	$3, %eax
	jmp	.L1772
.L1986:
	xorl	%eax, %eax
	jmp	.L1772
.L2108:
	movzbl	2(%r13), %eax
	andl	$-8, %eax
	cmpb	$-16, %al
	je	.L2111
	cmpb	$-24, %al
	jne	.L1777
	xorl	%eax, %eax
	leaq	.LC506(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$3, %eax
	jmp	.L1433
.L1888:
	leaq	.LC356(%rip), %rdx
	jmp	.L1630
.L2111:
	xorl	%eax, %eax
	leaq	.LC505(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$3, %eax
	jmp	.L1433
.L2107:
	cmpb	$-80, %bl
	jne	.L1776
	movb	$1, 163(%r12)
.L1776:
	movq	%r14, %rcx
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6413PrintOperandsEPKcNS_11OperandTypeEPh
	addl	%r14d, %eax
	subl	%r13d, %eax
	jmp	.L1433
.L2106:
	xorl	%eax, %eax
	leaq	.LC504(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	$2, %eax
	jmp	.L1433
.L1916:
	leaq	.LC328(%rip), %rdx
	jmp	.L1630
.L1915:
	leaq	.LC329(%rip), %rdx
	jmp	.L1630
.L1914:
	leaq	.LC330(%rip), %rdx
	jmp	.L1630
.L1913:
	leaq	.LC331(%rip), %rdx
	jmp	.L1630
.L1912:
	leaq	.LC332(%rip), %rdx
	jmp	.L1630
.L1911:
	leaq	.LC333(%rip), %rdx
	jmp	.L1630
.L1910:
	leaq	.LC334(%rip), %rdx
	jmp	.L1630
.L1909:
	leaq	.LC335(%rip), %rdx
	jmp	.L1630
.L1908:
	leaq	.LC336(%rip), %rdx
	jmp	.L1630
.L1907:
	leaq	.LC337(%rip), %rdx
	jmp	.L1630
.L1906:
	leaq	.LC338(%rip), %rdx
	jmp	.L1630
.L1905:
	leaq	.LC339(%rip), %rdx
	jmp	.L1630
.L1904:
	leaq	.LC340(%rip), %rdx
	jmp	.L1630
.L1903:
	leaq	.LC341(%rip), %rdx
	jmp	.L1630
.L1902:
	leaq	.LC342(%rip), %rdx
	jmp	.L1630
.L1901:
	leaq	.LC343(%rip), %rdx
	jmp	.L1630
.L1900:
	leaq	.LC344(%rip), %rdx
	jmp	.L1630
.L1899:
	leaq	.LC345(%rip), %rdx
	jmp	.L1630
.L1898:
	leaq	.LC346(%rip), %rdx
	jmp	.L1630
.L1897:
	leaq	.LC347(%rip), %rdx
	jmp	.L1630
.L1896:
	leaq	.LC348(%rip), %rdx
	jmp	.L1630
.L1895:
	leaq	.LC349(%rip), %rdx
	jmp	.L1630
.L1894:
	leaq	.LC350(%rip), %rdx
	jmp	.L1630
.L1893:
	leaq	.LC351(%rip), %rdx
	jmp	.L1630
.L1892:
	leaq	.LC352(%rip), %rdx
	jmp	.L1630
.L1891:
	leaq	.LC353(%rip), %rdx
	jmp	.L1630
.L1890:
	leaq	.LC354(%rip), %rdx
	jmp	.L1630
.L1889:
	leaq	.LC355(%rip), %rdx
	jmp	.L1630
	.cfi_endproc
.LFE5175:
	.size	_ZN6disasm15DisassemblerX6424TwoByteOpcodeInstructionEPh, .-_ZN6disasm15DisassemblerX6424TwoByteOpcodeInstructionEPh
	.section	.text._ZN6disasm15DisassemblerX6415TwoByteMnemonicEh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6415TwoByteMnemonicEh
	.type	_ZN6disasm15DisassemblerX6415TwoByteMnemonicEh, @function
_ZN6disasm15DisassemblerX6415TwoByteMnemonicEh:
.LFB5176:
	.cfi_startproc
	endbr64
	leal	56(%rsi), %edx
	leaq	.LC296(%rip), %rax
	cmpb	$7, %dl
	jbe	.L2112
	subl	$31, %esi
	cmpb	$-96, %sil
	ja	.L2114
	leaq	.L2116(%rip), %rdx
	movzbl	%sil, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6415TwoByteMnemonicEh,"a",@progbits
	.align 4
	.align 4
.L2116:
	.long	.L2139-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2137-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2136-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2135-.L2116
	.long	.L2134-.L2116
	.long	.L2133-.L2116
	.long	.L2114-.L2116
	.long	.L2132-.L2116
	.long	.L2131-.L2116
	.long	.L2130-.L2116
	.long	.L2129-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2128-.L2116
	.long	.L2127-.L2116
	.long	.L2114-.L2116
	.long	.L2126-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2125-.L2116
	.long	.L2114-.L2116
	.long	.L2124-.L2116
	.long	.L2114-.L2116
	.long	.L2123-.L2116
	.long	.L2122-.L2116
	.long	.L2122-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2121-.L2116
	.long	.L2120-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2114-.L2116
	.long	.L2119-.L2116
	.long	.L2118-.L2116
	.long	.L2117-.L2116
	.long	.L2115-.L2116
	.section	.text._ZN6disasm15DisassemblerX6415TwoByteMnemonicEh
.L2128:
	leaq	.LC297(%rip), %rax
	ret
.L2139:
	leaq	.LC368(%rip), %rax
.L2112:
	ret
.L2115:
	leaq	.LC370(%rip), %rax
	ret
.L2117:
	leaq	.LC366(%rip), %rax
	ret
.L2118:
	leaq	.LC371(%rip), %rax
	ret
.L2119:
	leaq	.LC369(%rip), %rax
	ret
.L2120:
	leaq	.LC367(%rip), %rax
	ret
.L2121:
	leaq	.LC365(%rip), %rax
	ret
.L2122:
	leaq	.LC295(%rip), %rax
	ret
.L2123:
	leaq	.LC37(%rip), %rax
	ret
.L2124:
	leaq	.LC364(%rip), %rax
	ret
.L2125:
	leaq	.LC299(%rip), %rax
	ret
.L2126:
	leaq	.LC372(%rip), %rax
	ret
.L2127:
	leaq	.LC363(%rip), %rax
	ret
.L2129:
	cmpb	$-14, 159(%rdi)
	leaq	.LC381(%rip), %rdx
	leaq	.LC389(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L2130:
	cmpb	$-14, 159(%rdi)
	leaq	.LC373(%rip), %rdx
	leaq	.LC382(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L2131:
	cmpb	$-14, 159(%rdi)
	leaq	.LC375(%rip), %rdx
	leaq	.LC384(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L2132:
	cmpb	$-14, 159(%rdi)
	leaq	.LC374(%rip), %rdx
	leaq	.LC383(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L2133:
	cmpb	$-14, 159(%rdi)
	leaq	.LC376(%rip), %rdx
	leaq	.LC385(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L2134:
	cmpb	$-14, 159(%rdi)
	leaq	.LC377(%rip), %rdx
	leaq	.LC386(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L2135:
	cmpb	$-14, 159(%rdi)
	leaq	.LC378(%rip), %rdx
	leaq	.LC387(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L2136:
	cmpb	$-14, 159(%rdi)
	leaq	.LC379(%rip), %rdx
	leaq	.LC388(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L2137:
	cmpb	$-14, 159(%rdi)
	leaq	.LC380(%rip), %rdx
	leaq	.LC298(%rip), %rax
	cmovne	%rdx, %rax
	ret
.L2114:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5176:
	.size	_ZN6disasm15DisassemblerX6415TwoByteMnemonicEh, .-_ZN6disasm15DisassemblerX6415TwoByteMnemonicEh
	.section	.rodata._ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh.str1.1,"aMS",@progbits,1
.LC511:
	.string	"dec"
.LC512:
	.string	"inc"
.LC513:
	.string	"call"
.LC514:
	.string	"???"
.LC515:
	.string	"jmp"
.LC516:
	.string	"REX.W "
.LC517:
	.string	"lock "
.LC518:
	.string	"rep "
.LC519:
	.string	"%s%c"
.LC520:
	.string	"%s %s"
.LC521:
	.string	"mov%c %s,%s"
.LC522:
	.string	"%s rax,0x%x"
.LC523:
	.string	"unimplemented code"
.LC524:
	.string	"ret 0x%x"
.LC525:
	.string	"pop "
.LC526:
	.string	"movb "
.LC527:
	.string	"cmpb "
.LC528:
	.string	"pause"
.LC529:
	.string	"xchg%c rax,%s"
.LC530:
	.string	"movb %s,"
.LC531:
	.string	"decb "
.LC532:
	.string	"push 0x%x"
.LC533:
	.string	"movzxlq rax,(%s)"
.LC534:
	.string	"movzxlq (%s),rax"
.LC535:
	.string	"movq rax,(%s)"
.LC536:
	.string	"movq (%s),rax"
.LC537:
	.string	"test al,0x%x"
.LC538:
	.string	"test%c rax,0x%lx"
.LC539:
	.string	"cmp al,0x%x"
.LC540:
	.string	"%02x"
.LC541:
	.string	"  "
.LC542:
	.string	" %s"
	.section	.text._ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh
	.type	_ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh, @function
_ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh:
.LFB5177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	.LC517(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rdx, -56(%rbp)
	movl	$0, 152(%rdi)
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2150:
	movl	%r9d, %eax
	andl	$-16, %eax
	cmpb	$64, %al
	je	.L2350
	movl	%r9d, %eax
	andl	$-2, %eax
	cmpb	$-14, %al
	je	.L2351
	cmpb	$-16, %r9b
	jne	.L2154
	movq	%r15, %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
.L2151:
	addq	$1, %r14
.L2158:
	movzbl	(%r14), %r9d
	cmpb	$102, %r9b
	jne	.L2150
	movb	$102, 158(%rbx)
	jmp	.L2151
	.p2align 4,,10
	.p2align 3
.L2350:
	movb	%r9b, 157(%rbx)
	andl	$8, %r9d
	je	.L2151
	leaq	.LC516(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2151
	.p2align 4,,10
	.p2align 3
.L2351:
	movb	%r9b, 159(%rbx)
	jmp	.L2151
	.p2align 4,,10
	.p2align 3
.L2154:
	cmpb	$-60, %r9b
	je	.L2352
	cmpb	$-59, %r9b
	je	.L2353
	cmpb	$0, 160(%rbx)
	jne	.L2295
	movq	168(%rbx), %rax
	movzbl	%r9b, %edx
	leaq	(%rdx,%rdx,2), %rdx
	movzbl	16(%rax,%rdx,8), %ecx
	movzbl	%r9b, %edx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %r15
	movb	%cl, 163(%rbx)
	cmpl	$8, 8(%r15)
	ja	.L2161
	movl	8(%r15), %eax
	leaq	.L2163(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh,"a",@progbits
	.align 4
	.align 4
.L2163:
	.long	.L2171-.L2163
	.long	.L2170-.L2163
	.long	.L2169-.L2163
	.long	.L2168-.L2163
	.long	.L2167-.L2163
	.long	.L2166-.L2163
	.long	.L2165-.L2163
	.long	.L2164-.L2163
	.long	.L2162-.L2163
	.section	.text._ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh
.L2353:
	movb	$-59, 160(%rbx)
	movzbl	1(%r14), %eax
	addq	$2, %r14
	movb	%al, 161(%rbx)
	shrb	$5, %al
	notl	%eax
	andl	$4, %eax
	orl	$64, %eax
	movb	%al, 157(%rbx)
.L2295:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6414AVXInstructionEPh
	cltq
	addq	%r14, %rax
	movq	%rax, -64(%rbp)
.L2160:
	movl	152(%rbx), %eax
	cmpl	$143, %eax
	ja	.L2282
	movq	8(%rbx), %rdx
	movb	$0, (%rdx,%rax)
.L2282:
	movq	-64(%rbp), %rax
	movq	%rax, %rcx
	subq	%r12, %rcx
	movq	%rcx, -72(%rbp)
	cmpq	%r12, %rax
	jbe	.L2324
	xorl	%r14d, %r14d
	leaq	.LC540(%rip), %r15
	.p2align 4,,10
	.p2align 3
.L2284:
	movq	-56(%rbp), %rsi
	movslq	%r14d, %rax
	movzbl	(%r12), %ecx
	movq	%r15, %rdx
	leaq	0(%r13,%rax), %rdi
	addq	$1, %r12
	subq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	addl	%eax, %r14d
	cmpq	%r12, -64(%rbp)
	jne	.L2284
	movq	-56(%rbp), %rsi
	movslq	%r14d, %rdi
	subq	%rdi, %rsi
	addq	%r13, %rdi
.L2283:
	movl	$6, %r15d
	subl	-72(%rbp), %r15d
	js	.L2285
	leaq	.LC541(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L2286:
	movq	-56(%rbp), %rsi
	movslq	%r14d, %rax
	movq	%r12, %rdx
	subl	$1, %r15d
	leaq	0(%r13,%rax), %rdi
	subq	%rax, %rsi
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	addl	%eax, %r14d
	cmpl	$-1, %r15d
	jne	.L2286
	movq	-56(%rbp), %rsi
	movslq	%r14d, %r8
	leaq	0(%r13,%r8), %rdi
	subq	%r8, %rsi
.L2285:
	movq	8(%rbx), %rcx
	leaq	.LC542(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movl	-72(%rbp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2171:
	.cfi_restore_state
	movzbl	(%r14), %eax
	leal	-15(%rax), %edx
	cmpb	$14, %al
	jbe	.L2197
	leaq	.L2199(%rip), %rsi
	movzbl	%dl, %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata._ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh
	.align 4
	.align 4
.L2199:
	.long	.L2222-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2221-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2220-.L2199
	.long	.L2218-.L2199
	.long	.L2219-.L2199
	.long	.L2218-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2217-.L2199
	.long	.L2216-.L2199
	.long	.L2197-.L2199
	.long	.L2216-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2215-.L2199
	.long	.L2215-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2214-.L2199
	.long	.L2213-.L2199
	.long	.L2213-.L2199
	.long	.L2213-.L2199
	.long	.L2213-.L2199
	.long	.L2213-.L2199
	.long	.L2213-.L2199
	.long	.L2213-.L2199
	.long	.L2213-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2212-.L2199
	.long	.L2197-.L2199
	.long	.L2212-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2211-.L2199
	.long	.L2210-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2209-.L2199
	.long	.L2206-.L2199
	.long	.L2343-.L2199
	.long	.L2208-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2207-.L2199
	.long	.L2207-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2206-.L2199
	.long	.L2343-.L2199
	.long	.L2206-.L2199
	.long	.L2343-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2204-.L2199
	.long	.L2204-.L2199
	.long	.L2204-.L2199
	.long	.L2204-.L2199
	.long	.L2204-.L2199
	.long	.L2204-.L2199
	.long	.L2204-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2203-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2202-.L2199
	.long	.L2201-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2197-.L2199
	.long	.L2200-.L2199
	.long	.L2198-.L2199
	.section	.text._ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh
	.p2align 4,,10
	.p2align 3
.L2352:
	movb	$-60, 160(%rbx)
	movzbl	1(%r14), %eax
	addq	$3, %r14
	movb	%al, 161(%rbx)
	movzbl	-1(%r14), %edx
	shrb	$5, %al
	xorl	$71, %eax
	movb	%dl, 162(%rbx)
	shrb	$4, %dl
	andl	$8, %edx
	orl	%edx, %eax
	movb	%al, 157(%rbx)
	jmp	.L2295
.L2170:
	addl	$92, %r9d
	cmpb	$3, %r9b
	jbe	.L2354
	xorl	%eax, %eax
	testb	%cl, %cl
	jne	.L2178
.L2349:
	movl	$3, %eax
	testb	$8, 157(%rbx)
	jne	.L2178
.L2348:
	xorl	%eax, %eax
	cmpb	$0, 158(%rbx)
	sete	%al
	addl	$1, %eax
.L2178:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC519(%rip), %rsi
	movq	%rbx, %rdi
	movsbl	(%rdx,%rax), %ecx
	movq	(%r15), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2342
.L2169:
	addq	$1, %r14
	movl	12(%r15), %edx
	movq	(%r15), %rsi
	movq	%rbx, %rdi
	movq	%r14, %rcx
	call	_ZN6disasm15DisassemblerX6413PrintOperandsEPKcNS_11OperandTypeEPh
	cltq
	addq	%r14, %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2168:
	movsbq	1(%r14), %rax
	movq	(%rbx), %r15
	leaq	_ZN6disasmL23conditional_code_suffixE(%rip), %rdx
	leaq	2(%r14,%rax), %rcx
	movzbl	(%r14), %eax
	andl	$15, %eax
	movq	(%rdx,%rax,8), %r8
	movq	(%r15), %rax
	leaq	_ZNK6disasm13NameConverter13NameOfAddressEPh(%rip), %rdx
	movq	40(%rax), %rax
	movq	%r8, -64(%rbp)
	cmpq	%rdx, %rax
	jne	.L2179
	movq	8(%r15), %rdi
	movq	16(%r15), %rsi
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	8(%r15), %rcx
	movq	-64(%rbp), %r8
.L2180:
	xorl	%eax, %eax
	movq	%r8, %rdx
	leaq	.LC52(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	2(%r14), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2167:
	movq	(%rbx), %rdi
	movzbl	157(%rbx), %esi
	andl	$7, %r9d
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	sall	$3, %esi
	andl	$8, %esi
	movq	16(%rax), %rax
	orl	%r9d, %esi
	cmpq	%rdx, %rax
	jne	.L2181
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %r8
.L2182:
	xorl	%eax, %eax
	testb	%cl, %cl
	jne	.L2183
	movl	$3, %eax
	testb	$8, 157(%rbx)
	jne	.L2183
	xorl	%eax, %eax
	cmpb	$0, 158(%rbx)
	sete	%al
	addl	$1, %eax
.L2183:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC40(%rip), %rsi
	movq	%rbx, %rdi
	movsbl	(%rdx,%rax), %ecx
	movq	(%r15), %rdx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	1(%r14), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2166:
	movq	(%rbx), %rdi
	movzbl	157(%rbx), %esi
	andl	$7, %r9d
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	sall	$3, %esi
	andl	$8, %esi
	movq	16(%rax), %rax
	orl	%r9d, %esi
	cmpq	%rdx, %rax
	jne	.L2184
	movslq	%esi, %rsi
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rcx
.L2185:
	movq	(%r15), %rdx
	xorl	%eax, %eax
	leaq	.LC520(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	1(%r14), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2164:
	movslq	1(%r14), %rax
	movq	(%rbx), %r8
	leaq	_ZNK6disasm13NameConverter13NameOfAddressEPh(%rip), %rdx
	leaq	5(%r14,%rax), %rcx
	movq	(%r8), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2193
	movq	8(%r8), %rdi
	movq	16(%r8), %rsi
	xorl	%eax, %eax
	movq	%r8, -64(%rbp)
	leaq	.LC3(%rip), %rdx
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	-64(%rbp), %r8
	movq	8(%r8), %rcx
.L2194:
	movq	(%r15), %rdx
	xorl	%eax, %eax
	leaq	.LC520(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	5(%r14), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2162:
	testb	%cl, %cl
	jne	.L2195
	testb	$8, 157(%rbx)
	jne	.L2195
	cmpb	$0, 158(%rbx)
	je	.L2195
	leaq	3(%r14), %rax
	movswl	1(%r14), %ecx
	movq	%rax, -64(%rbp)
	jmp	.L2196
.L2165:
	testb	%cl, %cl
	jne	.L2276
	testb	$8, 157(%rbx)
	jne	.L2187
	cmpb	$0, 158(%rbx)
	je	.L2188
	leaq	3(%r14), %rax
	movswq	1(%r14), %rcx
	movq	%rax, -64(%rbp)
.L2189:
	movq	(%rbx), %r15
	leaq	_ZNK6disasm13NameConverter13NameOfAddressEPh(%rip), %rdx
	movb	%r9b, -72(%rbp)
	movq	(%r15), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2190
	movq	8(%r15), %rdi
	movq	16(%r15), %rsi
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	8(%r15), %r15
	movzbl	-72(%rbp), %r9d
.L2191:
	movzbl	157(%rbx), %eax
	movq	(%rbx), %rdi
	andl	$7, %r9d
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	sall	$3, %eax
	andl	$8, %eax
	orl	%eax, %r9d
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2355
	movl	%r9d, %esi
	call	*%rax
	movq	%rax, %rcx
.L2289:
	xorl	%eax, %eax
	cmpb	$0, 163(%rbx)
	jne	.L2192
	movl	$3, %eax
	testb	$8, 157(%rbx)
	jne	.L2192
	xorl	%eax, %eax
	cmpb	$0, 158(%rbx)
	sete	%al
	addl	$1, %eax
.L2192:
	cltq
	leaq	.LC20(%rip), %rdx
	movq	%r15, %r8
	movq	%rbx, %rdi
	movsbl	(%rdx,%rax), %edx
	leaq	.LC521(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2160
.L2195:
	leaq	5(%r14), %rax
	movl	1(%r14), %ecx
	movq	%rax, -64(%rbp)
.L2196:
	movq	(%r15), %rdx
	leaq	.LC522(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2160
.L2324:
	movq	-56(%rbp), %rsi
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	jmp	.L2283
	.p2align 4,,10
	.p2align 3
.L2197:
	cmpb	$0, 156(%rbx)
	jne	.L2275
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
.L2342:
	leaq	1(%r14), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2354:
	cmpb	$-13, 159(%rbx)
	je	.L2356
.L2173:
	testb	$8, 157(%rbx)
	jne	.L2357
	cmpb	$0, 163(%rbx)
	je	.L2348
	xorl	%eax, %eax
	jmp	.L2178
.L2209:
	movl	%eax, %esi
	movq	(%rbx), %rdi
	andl	$7, %esi
	movl	%esi, %edx
	orl	$8, %edx
	testb	$1, 157(%rbx)
	cmovne	%edx, %esi
	movq	(%rdi), %rdx
	movzbl	%sil, %esi
	cmpb	$-73, %al
	jbe	.L2255
	movq	16(%rdx), %rax
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L2256
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %r8
.L2257:
	xorl	%eax, %eax
	testb	%cl, %cl
	jne	.L2258
	movl	$3, %eax
	testb	$8, 157(%rbx)
	jne	.L2258
	xorl	%eax, %eax
	cmpb	$0, 158(%rbx)
	sete	%al
	addl	$1, %eax
.L2258:
	cltq
	leaq	.LC20(%rip), %rdx
	movq	%r8, %rcx
	movq	%rbx, %rdi
	movsbl	(%rdx,%rax), %edx
	leaq	.LC434(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movl	1(%r14), %edx
	leaq	.LC19(%rip), %rsi
.L2345:
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
.L2273:
	leaq	5(%r14), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2276:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2213:
	movl	%eax, %edx
	andl	$7, %edx
	cmpb	$-13, 159(%rbx)
	sete	%sil
	cmpb	$-112, %al
	sete	%al
	andl	%esi, %eax
	testb	$1, 157(%rbx)
	jne	.L2247
	testb	%al, %al
	jne	.L2292
	movzbl	%dl, %esi
	testb	%dl, %dl
	jne	.L2250
	leaq	.LC368(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2342
.L2204:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6414FPUInstructionEPh
	cltq
	addq	%r14, %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2206:
	movb	$1, 163(%rbx)
.L2343:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6416ShiftInstructionEPh
	cltq
	addq	%r14, %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2202:
	movb	$1, 163(%rbx)
.L2201:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6415F6F7InstructionEPh
	cltq
	addq	%r14, %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2222:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6424TwoByteOpcodeInstructionEPh
	cltq
	addq	%r14, %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2221:
	movsbl	1(%r14), %edx
	leaq	.LC539(%rip), %rsi
.L2341:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	2(%r14), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2220:
	movl	1(%r14), %edx
	xorl	%eax, %eax
	leaq	.LC532(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	5(%r14), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2218:
	leaq	1(%r14), %rcx
	movl	$1, %edx
	leaq	.LC37(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6413PrintOperandsEPKcNS_11OperandTypeEPh
	leaq	.LC33(%rip), %rsi
	movq	%rbx, %rdi
	movl	%eax, %r15d
	leal	1(%rax), %eax
	movl	%eax, -64(%rbp)
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movslq	-64(%rbp), %rax
	addq	%r14, %rax
	cmpb	$105, (%r14)
	jne	.L2223
	cmpb	$0, 163(%rbx)
	jne	.L2224
	testb	$8, 157(%rbx)
	jne	.L2225
	cmpb	$0, 158(%rbx)
	je	.L2226
	movswq	(%rax), %rdx
	movl	$2, %r15d
.L2227:
	leaq	.LC19(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	addl	-64(%rbp), %r15d
.L2228:
	movslq	%r15d, %r15
	leaq	(%r14,%r15), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2219:
	movsbl	1(%r14), %edx
	leaq	.LC532(%rip), %rsi
	jmp	.L2341
.L2217:
	leaq	1(%r14), %r15
	leaq	.LC527(%rip), %rsi
.L2346:
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%r15, %rsi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	leaq	_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	leaq	.LC105(%rip), %rsi
	movq	%rbx, %rdi
	cltq
	addq	%rax, %r15
	xorl	%eax, %eax
	movzbl	(%r15), %edx
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	1(%r15), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2216:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6416PrintImmediateOpEPh
	cltq
	addq	%r14, %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2215:
	movzbl	1(%r14), %r8d
	movzbl	157(%rbx), %edx
	leaq	1(%r14), %r15
	sarl	$3, %r8d
	andl	$7, %r8d
	movl	%r8d, %esi
	movl	%r8d, %r14d
	orl	$8, %esi
	testb	$4, %dl
	cmovne	%esi, %r14d
	cmpb	$-120, %al
	je	.L2358
	xorl	%eax, %eax
	testb	%cl, %cl
	jne	.L2244
	andl	$8, %edx
	movl	$3, %eax
	jne	.L2244
	xorl	%eax, %eax
	cmpb	$0, 158(%rbx)
	sete	%al
	addl	$1, %eax
.L2244:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC507(%rip), %rsi
	movq	%rbx, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%rbx), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	cltq
	addq	%r15, %rax
	movq	%rax, -64(%rbp)
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2245
	movslq	%r14d, %r8
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r8,8), %rdx
.L2246:
	leaq	.LC23(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2160
.L2212:
	testb	%cl, %cl
	jne	.L2263
	testb	$8, 157(%rbx)
	jne	.L2264
	cmpb	$0, 158(%rbx)
	jne	.L2263
	movq	(%rbx), %r15
	leaq	_ZNK6disasm13NameConverter13NameOfAddressEPh(%rip), %rdx
	movslq	1(%r14), %rcx
	movq	(%r15), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2265
	movq	8(%r15), %rdi
	movq	16(%r15), %rsi
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	8(%r15), %rdx
.L2266:
	cmpb	$-95, (%r14)
	leaq	.LC534(%rip), %rsi
	jne	.L2345
	leaq	.LC533(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2273
	.p2align 4,,10
	.p2align 3
.L2214:
	leaq	1(%r14), %r15
	movq	%r15, -64(%rbp)
	testb	$4, 157(%rbx)
	jne	.L2160
	testb	$56, 1(%r14)
	jne	.L2160
	leaq	.LC525(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
.L2347:
	movq	%r15, %rsi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	cltq
	addq	%rax, %r15
	movq	%r15, -64(%rbp)
	jmp	.L2160
.L2208:
	movzwl	1(%r14), %edx
	xorl	%eax, %eax
	leaq	.LC524(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	3(%r14), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2207:
	leaq	1(%r14), %r15
	leaq	.LC526(%rip), %rsi
	cmpb	$-58, %al
	je	.L2346
	xorl	%eax, %eax
	testb	%cl, %cl
	jne	.L2238
	movl	$3, %eax
	testb	$8, 157(%rbx)
	jne	.L2238
	xorl	%eax, %eax
	cmpb	$0, 158(%rbx)
	sete	%al
	addl	$1, %eax
.L2238:
	cltq
	leaq	.LC20(%rip), %rdx
	leaq	.LC507(%rip), %rsi
	movq	%rbx, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	cltq
	addq	%rax, %r15
	cmpb	$0, 163(%rbx)
	jne	.L2239
	testb	$8, 157(%rbx)
	jne	.L2239
	cmpb	$0, 158(%rbx)
	je	.L2239
	movswl	(%r15), %edx
	xorl	%eax, %eax
	leaq	.LC105(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	2(%r15), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2211:
	movzbl	1(%r14), %edx
	leaq	.LC537(%rip), %rsi
	jmp	.L2341
.L2200:
	leaq	1(%r14), %r15
	movq	%r15, -64(%rbp)
	testb	$4, 157(%rbx)
	jne	.L2261
	movzbl	1(%r14), %eax
	sarl	$3, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	jne	.L2261
	leaq	.LC531(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi(%rip), %rdx
	jmp	.L2347
.L2198:
	movzbl	1(%r14), %eax
	movzbl	157(%rbx), %esi
	leaq	1(%r14), %r15
	sarl	$3, %eax
	andl	$7, %eax
	testb	$4, %sil
	je	.L2359
.L2307:
	leaq	.LC514(%rip), %rdx
.L2229:
	leaq	.LC294(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
.L2288:
	leaq	_ZNK6disasm15DisassemblerX6417NameOfCPURegisterEi(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	cltq
	addq	%r15, %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2203:
	movsbq	1(%r14), %rax
	movq	(%rbx), %r15
	leaq	_ZNK6disasm13NameConverter13NameOfAddressEPh(%rip), %rdx
	leaq	2(%r14,%rax), %rcx
	movq	(%r15), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2280
	movq	8(%r15), %rdi
	movq	16(%r15), %rsi
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	8(%r15), %rdx
.L2281:
	leaq	.LC51(%rip), %rsi
.L2344:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	2(%r14), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2210:
	testb	%cl, %cl
	jne	.L2276
	testb	$8, 157(%rbx)
	jne	.L2277
	cmpb	$0, 158(%rbx)
	je	.L2278
	leaq	3(%r14), %rax
	movzwl	1(%r14), %ecx
	movq	%rax, -64(%rbp)
	movl	$1, %eax
.L2279:
	leaq	.LC20(%rip), %rdx
	leaq	.LC538(%rip), %rsi
	movq	%rbx, %rdi
	movsbl	(%rdx,%rax), %edx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2160
.L2188:
	leaq	5(%r14), %rax
	movl	1(%r14), %ecx
	movq	%rax, -64(%rbp)
	jmp	.L2189
.L2181:
	call	*%rax
	movzbl	163(%rbx), %ecx
	movq	%rax, %r8
	jmp	.L2182
.L2179:
	movq	%rcx, %rsi
	movq	%r15, %rdi
	call	*%rax
	movq	-64(%rbp), %r8
	movq	%rax, %rcx
	jmp	.L2180
.L2184:
	call	*%rax
	movq	%rax, %rcx
	jmp	.L2185
.L2193:
	movq	%rcx, %rsi
	movq	%r8, %rdi
	call	*%rax
	movq	%rax, %rcx
	jmp	.L2194
.L2355:
	movslq	%r9d, %r9
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%r9,8), %rcx
	jmp	.L2289
.L2250:
	movq	(%rbx), %rdi
	leaq	_ZNK6disasm13NameConverter17NameOfCPURegisterEi(%rip), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2251
	leaq	_ZN6disasmL8cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %r8
.L2252:
	xorl	%eax, %eax
	testb	%cl, %cl
	jne	.L2253
	movl	$3, %eax
	testb	$8, 157(%rbx)
	jne	.L2253
	xorl	%eax, %eax
	cmpb	$0, 158(%rbx)
	sete	%al
	addl	$1, %eax
.L2253:
	cltq
	leaq	.LC20(%rip), %rdx
	movq	%r8, %rcx
	movq	%rbx, %rdi
	movsbl	(%rdx,%rax), %edx
	leaq	.LC529(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2342
.L2239:
	movl	(%r15), %edx
	xorl	%eax, %eax
	leaq	.LC105(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	4(%r15), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2263:
	cmpb	$0, 156(%rbx)
	jne	.L2275
	xorl	%eax, %eax
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	leaq	2(%r14), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2357:
	xorl	%eax, %eax
	leaq	.LC516(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	cmpb	$0, 163(%rbx)
	je	.L2349
	xorl	%eax, %eax
	jmp	.L2178
.L2261:
	cmpb	$0, 156(%rbx)
	jne	.L2275
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2160
.L2359:
	cmpl	$6, %eax
	ja	.L2307
	leaq	.L2231(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh
	.align 4
	.align 4
.L2231:
	.long	.L2235-.L2231
	.long	.L2308-.L2231
	.long	.L2233-.L2231
	.long	.L2307-.L2231
	.long	.L2232-.L2231
	.long	.L2307-.L2231
	.long	.L2309-.L2231
	.section	.text._ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh
.L2309:
	leaq	.LC5(%rip), %rdx
	jmp	.L2229
.L2232:
	leaq	.LC515(%rip), %rdx
	jmp	.L2229
.L2233:
	leaq	.LC513(%rip), %rdx
	jmp	.L2229
.L2308:
	leaq	.LC511(%rip), %rdx
.L2234:
	xorl	%eax, %eax
	testb	%cl, %cl
	jne	.L2236
	andl	$8, %esi
	movl	$3, %eax
	jne	.L2236
	xorl	%eax, %eax
	cmpb	$0, 158(%rbx)
	sete	%al
	addl	$1, %eax
.L2236:
	cltq
	leaq	.LC20(%rip), %rcx
	leaq	.LC22(%rip), %rsi
	movq	%rbx, %rdi
	movsbl	(%rcx,%rax), %ecx
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2288
.L2235:
	leaq	.LC512(%rip), %rdx
	jmp	.L2234
.L2190:
	movq	%r15, %rdi
	movq	%rcx, %rsi
	call	*%rax
	movzbl	-72(%rbp), %r9d
	movq	%rax, %r15
	jmp	.L2191
.L2255:
	movq	24(%rdx), %rax
	leaq	_ZNK6disasm13NameConverter21NameOfByteCPURegisterEi(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L2259
	leaq	_ZN6disasmL13byte_cpu_regsE(%rip), %rax
	movq	(%rax,%rsi,8), %rdx
.L2260:
	leaq	.LC530(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movzbl	1(%r14), %edx
	leaq	.LC19(%rip), %rsi
	jmp	.L2344
.L2223:
	movzbl	(%rax), %edx
	leaq	.LC19(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	addl	$2, %r15d
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2228
.L2247:
	orl	$8, %edx
	movzbl	%dl, %esi
	testb	%al, %al
	je	.L2250
.L2292:
	leaq	.LC528(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2342
.L2187:
	leaq	9(%r14), %rax
	movq	1(%r14), %rcx
	movq	%rax, -64(%rbp)
	jmp	.L2189
.L2356:
	leaq	.LC518(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2173
.L2278:
	leaq	5(%r14), %rax
	movl	1(%r14), %ecx
	movq	%rax, -64(%rbp)
	movl	$2, %eax
	jmp	.L2279
.L2256:
	call	*%rax
	movzbl	163(%rbx), %ecx
	movq	%rax, %r8
	jmp	.L2257
.L2245:
	movl	%r14d, %esi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L2246
.L2264:
	movq	(%rbx), %r15
	leaq	_ZNK6disasm13NameConverter13NameOfAddressEPh(%rip), %rdx
	movq	1(%r14), %rcx
	movq	(%r15), %rax
	movq	40(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2269
	movq	8(%r15), %rdi
	movq	16(%r15), %rsi
	leaq	.LC3(%rip), %rdx
	xorl	%eax, %eax
	call	_ZN2v88internal8SNPrintFENS0_6VectorIcEEPKcz@PLT
	movq	8(%r15), %rdx
.L2270:
	cmpb	$-95, (%r14)
	je	.L2360
	leaq	.LC536(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
.L2274:
	leaq	9(%r14), %rax
	movq	%rax, -64(%rbp)
	jmp	.L2160
.L2280:
	movq	%rcx, %rsi
	movq	%r15, %rdi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L2281
.L2277:
	leaq	5(%r14), %rax
	movslq	1(%r14), %rcx
	movq	%rax, -64(%rbp)
	movl	$3, %eax
	jmp	.L2279
.L2251:
	call	*%rax
	movzbl	163(%rbx), %ecx
	movq	%rax, %r8
	jmp	.L2252
.L2358:
	leaq	.LC526(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	leaq	_ZNK6disasm15DisassemblerX6421NameOfByteCPURegisterEi(%rip), %rdx
	call	_ZN6disasm15DisassemblerX6423PrintRightOperandHelperEPhMS0_KFPKciE
	movq	(%rbx), %rdi
	leaq	_ZNK6disasm13NameConverter21NameOfByteCPURegisterEi(%rip), %rdx
	cltq
	addq	%r15, %rax
	movq	%rax, -64(%rbp)
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2245
	movslq	%r14d, %r8
	leaq	_ZN6disasmL13byte_cpu_regsE(%rip), %rax
	movq	(%rax,%r8,8), %rdx
	jmp	.L2246
.L2226:
	movl	(%rax), %edx
	movl	$4, %r15d
	jmp	.L2227
.L2225:
	movslq	(%rax), %rdx
	movl	$4, %r15d
	jmp	.L2227
.L2224:
	movzbl	(%rax), %edx
	movl	$1, %r15d
	jmp	.L2227
.L2259:
	call	*%rax
	movq	%rax, %rdx
	jmp	.L2260
.L2161:
	leaq	.LC523(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
.L2275:
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_Z8V8_FatalPKcz@PLT
	.p2align 4,,10
	.p2align 3
.L2265:
	movq	%rcx, %rsi
	movq	%r15, %rdi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L2266
.L2360:
	leaq	.LC535(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	_ZN6disasm15DisassemblerX6414AppendToBufferEPKcz
	jmp	.L2274
.L2269:
	movq	%rcx, %rsi
	movq	%r15, %rdi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L2270
	.cfi_endproc
.LFE5177:
	.size	_ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh, .-_ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh
	.section	.text._ZN6disasm12Disassembler17InstructionDecodeEN2v88internal6VectorIcEEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm12Disassembler17InstructionDecodeEN2v88internal6VectorIcEEPh
	.type	_ZN6disasm12Disassembler17InstructionDecodeEN2v88internal6VectorIcEEPh, @function
_ZN6disasm12Disassembler17InstructionDecodeEN2v88internal6VectorIcEEPh:
.LFB5184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$200, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	cmpb	$1, 8(%rdi)
	movl	$0, -72(%rbp)
	sete	-68(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-200(%rbp), %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	movq	$128, -208(%rbp)
	movb	$0, -67(%rbp)
	movw	%ax, -66(%rbp)
	movl	$0, -64(%rbp)
	movzbl	_ZGVZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object(%rip), %eax
	testb	%al, %al
	je	.L2372
.L2363:
	leaq	_ZZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object(%rip), %rax
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%rax, -56(%rbp)
	movq	-216(%rbp), %rax
	leaq	-224(%rbp), %rdi
	movb	$0, (%rax)
	call	_ZN6disasm15DisassemblerX6417InstructionDecodeEN2v88internal6VectorIcEEPh
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2373
	addq	$200, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2372:
	.cfi_restore_state
	leaq	_ZGVZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L2363
	leaq	8+_ZZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object(%rip), %rax
	leaq	.LC4(%rip), %rdi
	leaq	6144(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L2365:
	movq	%rdi, -8(%rax)
	addq	$384, %rax
	movq	%rdi, -368(%rax)
	movq	%rdi, -344(%rax)
	movq	%rdi, -320(%rax)
	movq	%rdi, -296(%rax)
	movq	%rdi, -272(%rax)
	movq	%rdi, -248(%rax)
	movq	%rdi, -224(%rax)
	movq	%rdi, -200(%rax)
	movq	%rdi, -176(%rax)
	movq	%rdi, -152(%rax)
	movq	%rdi, -128(%rax)
	movq	%rdi, -104(%rax)
	movq	%rdi, -80(%rax)
	movq	%rdi, -56(%rax)
	movq	%rdi, -32(%rax)
	movq	$0, -384(%rax)
	movq	$0, -360(%rax)
	movq	$0, -336(%rax)
	movq	$0, -312(%rax)
	movq	$0, -288(%rax)
	movq	$0, -264(%rax)
	movq	$0, -240(%rax)
	movq	$0, -216(%rax)
	movq	$0, -192(%rax)
	movq	$0, -168(%rax)
	movq	$0, -144(%rax)
	movq	$0, -120(%rax)
	movq	$0, -96(%rax)
	movq	$0, -72(%rax)
	movq	$0, -48(%rax)
	movq	$0, -24(%rax)
	movb	$0, -376(%rax)
	movb	$0, -352(%rax)
	movb	$0, -328(%rax)
	movb	$0, -304(%rax)
	movb	$0, -280(%rax)
	movb	$0, -256(%rax)
	movb	$0, -232(%rax)
	movb	$0, -208(%rax)
	movb	$0, -184(%rax)
	movb	$0, -160(%rax)
	movb	$0, -136(%rax)
	movb	$0, -112(%rax)
	movb	$0, -88(%rax)
	movb	$0, -64(%rax)
	movb	$0, -40(%rax)
	movb	$0, -16(%rax)
	cmpq	%rdx, %rax
	jne	.L2365
	leaq	_ZZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object(%rip), %rdi
	call	_ZN6disasm16InstructionTable4InitEv
	leaq	_ZGVZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L2363
.L2373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5184:
	.size	_ZN6disasm12Disassembler17InstructionDecodeEN2v88internal6VectorIcEEPh, .-_ZN6disasm12Disassembler17InstructionDecodeEN2v88internal6VectorIcEEPh
	.section	.text._ZN6disasm12Disassembler18ConstantPoolSizeAtEPh,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm12Disassembler18ConstantPoolSizeAtEPh
	.type	_ZN6disasm12Disassembler18ConstantPoolSizeAtEPh, @function
_ZN6disasm12Disassembler18ConstantPoolSizeAtEPh:
.LFB5185:
	.cfi_startproc
	endbr64
	movl	$-1, %eax
	ret
	.cfi_endproc
.LFE5185:
	.size	_ZN6disasm12Disassembler18ConstantPoolSizeAtEPh, .-_ZN6disasm12Disassembler18ConstantPoolSizeAtEPh
	.section	.rodata._ZN6disasm12Disassembler11DisassembleEP8_IO_FILEPhS3_NS0_25UnimplementedOpcodeActionE.str1.1,"aMS",@progbits,1
.LC543:
	.string	"    "
.LC544:
	.string	"  %s\n"
	.section	.text._ZN6disasm12Disassembler11DisassembleEP8_IO_FILEPhS3_NS0_25UnimplementedOpcodeActionE,"ax",@progbits
	.align 2
	.p2align 4
	.globl	_ZN6disasm12Disassembler11DisassembleEP8_IO_FILEPhS3_NS0_25UnimplementedOpcodeActionE
	.type	_ZN6disasm12Disassembler11DisassembleEP8_IO_FILEPhS3_NS0_25UnimplementedOpcodeActionE, @function
_ZN6disasm12Disassembler11DisassembleEP8_IO_FILEPhS3_NS0_25UnimplementedOpcodeActionE:
.LFB5186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	16+_ZTVN6disasm13NameConverterE(%rip), %rdi
	movq	%rdi, %xmm0
	subq	$360, %rsp
	movq	%rdx, -384(%rbp)
	leaq	-184(%rbp), %rdx
	movq	%rdx, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-208(%rbp), %rax
	movb	%cl, -360(%rbp)
	movq	$128, -192(%rbp)
	movq	%rax, -368(%rbp)
	movaps	%xmm0, -208(%rbp)
	cmpq	%rsi, %r12
	jnb	.L2375
	leaq	-336(%rbp), %rax
	leaq	.LC540(%rip), %r14
	movq	%rax, -400(%rbp)
	leaq	-368(%rbp), %rax
	leaq	.LC541(%rip), %r13
	movq	%rax, -392(%rbp)
	.p2align 4,,10
	.p2align 3
.L2381:
	movq	-400(%rbp), %rax
	movq	%r12, %rcx
	movl	$128, %edx
	movq	%r12, %r15
	movq	-392(%rbp), %rdi
	movb	$0, -336(%rbp)
	movq	%rax, %rsi
	movq	%rax, -352(%rbp)
	movq	$128, -344(%rbp)
	call	_ZN6disasm12Disassembler17InstructionDecodeEN2v88internal6VectorIcEEPh
	movq	%r15, %rcx
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%eax, -372(%rbp)
	cltq
	leaq	.LC3(%rip), %rdx
	addq	%rax, %r12
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	movq	%rbx, %rcx
	movl	$4, %edx
	movl	$1, %esi
	leaq	.LC543(%rip), %rdi
	call	fwrite@PLT
	cmpq	%r15, %r12
	jbe	.L2377
	.p2align 4,,10
	.p2align 3
.L2378:
	movzbl	(%r15), %ecx
	movq	%r14, %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	addq	$1, %r15
	call	__fprintf_chk@PLT
	cmpq	%r15, %r12
	jne	.L2378
.L2377:
	movl	$6, %r15d
	subl	-372(%rbp), %r15d
	js	.L2379
	.p2align 4,,10
	.p2align 3
.L2380:
	movq	%rbx, %rcx
	movl	$2, %edx
	movl	$1, %esi
	movq	%r13, %rdi
	call	fwrite@PLT
	subl	$1, %r15d
	cmpl	$-1, %r15d
	jne	.L2380
.L2379:
	movq	-352(%rbp), %rcx
	xorl	%eax, %eax
	movl	$1, %esi
	movq	%rbx, %rdi
	leaq	.LC544(%rip), %rdx
	call	__fprintf_chk@PLT
	cmpq	%r12, -384(%rbp)
	ja	.L2381
.L2375:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2387
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2387:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5186:
	.size	_ZN6disasm12Disassembler11DisassembleEP8_IO_FILEPhS3_NS0_25UnimplementedOpcodeActionE, .-_ZN6disasm12Disassembler11DisassembleEP8_IO_FILEPhS3_NS0_25UnimplementedOpcodeActionE
	.section	.text.startup._GLOBAL__sub_I__ZN6disasm16InstructionTableC2Ev,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN6disasm16InstructionTableC2Ev, @function
_GLOBAL__sub_I__ZN6disasm16InstructionTableC2Ev:
.LFB5970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE5970:
	.size	_GLOBAL__sub_I__ZN6disasm16InstructionTableC2Ev, .-_GLOBAL__sub_I__ZN6disasm16InstructionTableC2Ev
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN6disasm16InstructionTableC2Ev
	.weak	_ZTVN6disasm13NameConverterE
	.section	.data.rel.ro.local._ZTVN6disasm13NameConverterE,"awG",@progbits,_ZTVN6disasm13NameConverterE,comdat
	.align 8
	.type	_ZTVN6disasm13NameConverterE, @object
	.size	_ZTVN6disasm13NameConverterE, 88
_ZTVN6disasm13NameConverterE:
	.quad	0
	.quad	0
	.quad	_ZN6disasm13NameConverterD1Ev
	.quad	_ZN6disasm13NameConverterD0Ev
	.quad	_ZNK6disasm13NameConverter17NameOfCPURegisterEi
	.quad	_ZNK6disasm13NameConverter21NameOfByteCPURegisterEi
	.quad	_ZNK6disasm13NameConverter17NameOfXMMRegisterEi
	.quad	_ZNK6disasm13NameConverter13NameOfAddressEPh
	.quad	_ZNK6disasm13NameConverter14NameOfConstantEPh
	.quad	_ZNK6disasm13NameConverter10NameInCodeEPh
	.quad	_ZNK6disasm13NameConverter16RootRelativeNameEi
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC545:
	.string	"xmm0"
.LC546:
	.string	"xmm1"
.LC547:
	.string	"xmm2"
.LC548:
	.string	"xmm3"
.LC549:
	.string	"xmm4"
.LC550:
	.string	"xmm5"
.LC551:
	.string	"xmm6"
.LC552:
	.string	"xmm7"
.LC553:
	.string	"xmm8"
.LC554:
	.string	"xmm9"
.LC555:
	.string	"xmm10"
.LC556:
	.string	"xmm11"
.LC557:
	.string	"xmm12"
.LC558:
	.string	"xmm13"
.LC559:
	.string	"xmm14"
.LC560:
	.string	"xmm15"
	.section	.data.rel.ro.local._ZN6disasmL8xmm_regsE,"aw"
	.align 32
	.type	_ZN6disasmL8xmm_regsE, @object
	.size	_ZN6disasmL8xmm_regsE, 128
_ZN6disasmL8xmm_regsE:
	.quad	.LC545
	.quad	.LC546
	.quad	.LC547
	.quad	.LC548
	.quad	.LC549
	.quad	.LC550
	.quad	.LC551
	.quad	.LC552
	.quad	.LC553
	.quad	.LC554
	.quad	.LC555
	.quad	.LC556
	.quad	.LC557
	.quad	.LC558
	.quad	.LC559
	.quad	.LC560
	.section	.rodata.str1.1
.LC561:
	.string	"al"
.LC562:
	.string	"cl"
.LC563:
	.string	"dl"
.LC564:
	.string	"bl"
.LC565:
	.string	"spl"
.LC566:
	.string	"bpl"
.LC567:
	.string	"sil"
.LC568:
	.string	"dil"
.LC569:
	.string	"r8l"
.LC570:
	.string	"r9l"
.LC571:
	.string	"r10l"
.LC572:
	.string	"r11l"
.LC573:
	.string	"r12l"
.LC574:
	.string	"r13l"
.LC575:
	.string	"r14l"
.LC576:
	.string	"r15l"
	.section	.data.rel.ro.local._ZN6disasmL13byte_cpu_regsE,"aw"
	.align 32
	.type	_ZN6disasmL13byte_cpu_regsE, @object
	.size	_ZN6disasmL13byte_cpu_regsE, 128
_ZN6disasmL13byte_cpu_regsE:
	.quad	.LC561
	.quad	.LC562
	.quad	.LC563
	.quad	.LC564
	.quad	.LC565
	.quad	.LC566
	.quad	.LC567
	.quad	.LC568
	.quad	.LC569
	.quad	.LC570
	.quad	.LC571
	.quad	.LC572
	.quad	.LC573
	.quad	.LC574
	.quad	.LC575
	.quad	.LC576
	.section	.rodata.str1.1
.LC577:
	.string	"rax"
.LC578:
	.string	"rcx"
.LC579:
	.string	"rdx"
.LC580:
	.string	"rbx"
.LC581:
	.string	"rsp"
.LC582:
	.string	"rbp"
.LC583:
	.string	"rsi"
.LC584:
	.string	"rdi"
.LC585:
	.string	"r8"
.LC586:
	.string	"r9"
.LC587:
	.string	"r10"
.LC588:
	.string	"r11"
.LC589:
	.string	"r12"
.LC590:
	.string	"r13"
.LC591:
	.string	"r14"
.LC592:
	.string	"r15"
	.section	.data.rel.ro.local._ZN6disasmL8cpu_regsE,"aw"
	.align 32
	.type	_ZN6disasmL8cpu_regsE, @object
	.size	_ZN6disasmL8cpu_regsE, 128
_ZN6disasmL8cpu_regsE:
	.quad	.LC577
	.quad	.LC578
	.quad	.LC579
	.quad	.LC580
	.quad	.LC581
	.quad	.LC582
	.quad	.LC583
	.quad	.LC584
	.quad	.LC585
	.quad	.LC586
	.quad	.LC587
	.quad	.LC588
	.quad	.LC589
	.quad	.LC590
	.quad	.LC591
	.quad	.LC592
	.globl	_ZN6disasm6sf_strE
	.section	.rodata.str1.1
.LC593:
	.string	"rl"
.LC594:
	.string	"ra"
.LC595:
	.string	"ll"
	.section	.data.rel.local._ZN6disasm6sf_strE,"aw"
	.align 32
	.type	_ZN6disasm6sf_strE, @object
	.size	_ZN6disasm6sf_strE, 32
_ZN6disasm6sf_strE:
	.quad	.LC56
	.quad	.LC593
	.quad	.LC594
	.quad	.LC595
	.section	.rodata.str1.1
.LC596:
	.string	"cmovo"
.LC597:
	.string	"cmovno"
.LC598:
	.string	"cmovc"
.LC599:
	.string	"cmovnc"
.LC600:
	.string	"cmovz"
.LC601:
	.string	"cmovnz"
.LC602:
	.string	"cmovna"
.LC603:
	.string	"cmova"
.LC604:
	.string	"cmovs"
.LC605:
	.string	"cmovns"
.LC606:
	.string	"cmovpe"
.LC607:
	.string	"cmovpo"
.LC608:
	.string	"cmovl"
.LC609:
	.string	"cmovge"
.LC610:
	.string	"cmovle"
.LC611:
	.string	"cmovg"
	.section	.data.rel.ro.local._ZN6disasmL17cmov_instructionsE,"aw"
	.align 32
	.type	_ZN6disasmL17cmov_instructionsE, @object
	.size	_ZN6disasmL17cmov_instructionsE, 384
_ZN6disasmL17cmov_instructionsE:
	.quad	.LC596
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC597
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC598
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC599
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC600
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC601
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC602
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC603
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC604
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC605
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC606
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC607
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC608
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC609
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC610
	.long	2
	.long	1
	.byte	0
	.zero	7
	.quad	.LC611
	.long	2
	.long	1
	.byte	0
	.zero	7
	.section	.bss._ZGVZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object,"aw",@nobits
	.align 8
	.type	_ZGVZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object, @object
	.size	_ZGVZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object, 8
_ZGVZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object:
	.zero	8
	.section	.bss._ZZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object,"aw",@nobits
	.align 32
	.type	_ZZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object, @object
	.size	_ZZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object, 6144
_ZZN6disasm12_GLOBAL__N_119GetInstructionTableEvE6object:
	.zero	6144
	.section	.rodata.str1.1
.LC612:
	.string	"o"
.LC613:
	.string	"no"
.LC614:
	.string	"c"
.LC615:
	.string	"nc"
.LC616:
	.string	"z"
.LC617:
	.string	"nz"
.LC618:
	.string	"na"
.LC619:
	.string	"a"
.LC620:
	.string	"s"
.LC621:
	.string	"ns"
.LC622:
	.string	"pe"
.LC623:
	.string	"po"
.LC624:
	.string	"l"
.LC625:
	.string	"ge"
.LC626:
	.string	"g"
	.section	.data.rel.ro.local._ZN6disasmL23conditional_code_suffixE,"aw"
	.align 32
	.type	_ZN6disasmL23conditional_code_suffixE, @object
	.size	_ZN6disasmL23conditional_code_suffixE, 128
_ZN6disasmL23conditional_code_suffixE:
	.quad	.LC612
	.quad	.LC613
	.quad	.LC614
	.quad	.LC615
	.quad	.LC616
	.quad	.LC617
	.quad	.LC618
	.quad	.LC619
	.quad	.LC620
	.quad	.LC621
	.quad	.LC622
	.quad	.LC623
	.quad	.LC624
	.quad	.LC625
	.quad	.LC161
	.quad	.LC626
	.section	.data.rel.ro.local._ZN6disasmL21short_immediate_instrE,"aw"
	.align 32
	.type	_ZN6disasmL21short_immediate_instrE, @object
	.size	_ZN6disasmL21short_immediate_instrE, 144
_ZN6disasmL21short_immediate_instrE:
	.long	5
	.long	0
	.quad	.LC25
	.long	13
	.long	0
	.quad	.LC32
	.long	21
	.long	0
	.quad	.LC26
	.long	29
	.long	0
	.quad	.LC27
	.long	37
	.long	0
	.quad	.LC28
	.long	45
	.long	0
	.quad	.LC29
	.long	53
	.long	0
	.quad	.LC30
	.long	61
	.long	0
	.quad	.LC31
	.long	-1
	.long	0
	.quad	.LC56
	.section	.data.rel.ro.local._ZN6disasmL15call_jump_instrE,"aw"
	.align 32
	.type	_ZN6disasmL15call_jump_instrE, @object
	.size	_ZN6disasmL15call_jump_instrE, 48
_ZN6disasmL15call_jump_instrE:
	.long	232
	.long	0
	.quad	.LC513
	.long	233
	.long	0
	.quad	.LC515
	.long	-1
	.long	0
	.quad	.LC56
	.section	.rodata.str1.1
.LC627:
	.string	"ret"
.LC628:
	.string	"leave"
.LC629:
	.string	"hlt"
.LC630:
	.string	"cld"
.LC631:
	.string	"int3"
.LC632:
	.string	"pushad"
.LC633:
	.string	"popad"
.LC634:
	.string	"pushfd"
.LC635:
	.string	"popfd"
.LC636:
	.string	"sahf"
.LC637:
	.string	"cdq"
.LC638:
	.string	"fwait"
.LC639:
	.string	"movs"
.LC640:
	.string	"cmps"
	.section	.data.rel.ro.local._ZN6disasmL19zero_operands_instrE,"aw"
	.align 32
	.type	_ZN6disasmL19zero_operands_instrE, @object
	.size	_ZN6disasmL19zero_operands_instrE, 272
_ZN6disasmL19zero_operands_instrE:
	.long	195
	.long	0
	.quad	.LC627
	.long	201
	.long	0
	.quad	.LC628
	.long	244
	.long	0
	.quad	.LC629
	.long	252
	.long	0
	.quad	.LC630
	.long	204
	.long	0
	.quad	.LC631
	.long	96
	.long	0
	.quad	.LC632
	.long	97
	.long	0
	.quad	.LC633
	.long	156
	.long	0
	.quad	.LC634
	.long	157
	.long	0
	.quad	.LC635
	.long	158
	.long	0
	.quad	.LC636
	.long	153
	.long	0
	.quad	.LC637
	.long	155
	.long	0
	.quad	.LC638
	.long	164
	.long	0
	.quad	.LC639
	.long	165
	.long	0
	.quad	.LC639
	.long	166
	.long	0
	.quad	.LC640
	.long	167
	.long	0
	.quad	.LC640
	.long	-1
	.long	0
	.quad	.LC56
	.section	.rodata.str1.1
.LC641:
	.string	"movsxl"
.LC642:
	.string	"test"
.LC643:
	.string	"xchg"
.LC644:
	.string	"lea"
	.section	.data.rel.ro.local._ZN6disasmL18two_operands_instrE,"aw"
	.align 32
	.type	_ZN6disasmL18two_operands_instrE, @object
	.size	_ZN6disasmL18two_operands_instrE, 688
_ZN6disasmL18two_operands_instrE:
	.long	0
	.long	6
	.quad	.LC25
	.long	1
	.long	2
	.quad	.LC25
	.long	2
	.long	5
	.quad	.LC25
	.long	3
	.long	1
	.quad	.LC25
	.long	8
	.long	6
	.quad	.LC32
	.long	9
	.long	2
	.quad	.LC32
	.long	10
	.long	5
	.quad	.LC32
	.long	11
	.long	1
	.quad	.LC32
	.long	16
	.long	6
	.quad	.LC26
	.long	17
	.long	2
	.quad	.LC26
	.long	18
	.long	5
	.quad	.LC26
	.long	19
	.long	1
	.quad	.LC26
	.long	24
	.long	6
	.quad	.LC27
	.long	25
	.long	2
	.quad	.LC27
	.long	26
	.long	5
	.quad	.LC27
	.long	27
	.long	1
	.quad	.LC27
	.long	32
	.long	6
	.quad	.LC28
	.long	33
	.long	2
	.quad	.LC28
	.long	34
	.long	5
	.quad	.LC28
	.long	35
	.long	1
	.quad	.LC28
	.long	40
	.long	6
	.quad	.LC29
	.long	41
	.long	2
	.quad	.LC29
	.long	42
	.long	5
	.quad	.LC29
	.long	43
	.long	1
	.quad	.LC29
	.long	48
	.long	6
	.quad	.LC30
	.long	49
	.long	2
	.quad	.LC30
	.long	50
	.long	5
	.quad	.LC30
	.long	51
	.long	1
	.quad	.LC30
	.long	56
	.long	6
	.quad	.LC31
	.long	57
	.long	2
	.quad	.LC31
	.long	58
	.long	5
	.quad	.LC31
	.long	59
	.long	1
	.quad	.LC31
	.long	99
	.long	1
	.quad	.LC641
	.long	132
	.long	5
	.quad	.LC642
	.long	133
	.long	1
	.quad	.LC642
	.long	134
	.long	5
	.quad	.LC643
	.long	135
	.long	1
	.quad	.LC643
	.long	136
	.long	6
	.quad	.LC7
	.long	137
	.long	2
	.quad	.LC7
	.long	138
	.long	5
	.quad	.LC7
	.long	139
	.long	1
	.quad	.LC7
	.long	141
	.long	1
	.quad	.LC644
	.long	-1
	.long	0
	.quad	.LC56
	.section	.bss._ZStL8__ioinit,"aw",@nobits
	.type	_ZStL8__ioinit, @object
	.size	_ZStL8__ioinit, 1
_ZStL8__ioinit:
	.zero	1
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
